package com.neomind.fusion.custom.orsegups.fap.factory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.exception.NaoEncontradoException;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class FapFactory {
 
    /**
     * Factory com filtro simples de tipo String
     * A entidade filtro pode ser qualquer objeto (NeoPaper, NeoObject, Formulário)
     * @param nomeFormulario
     * @param campoFiltro
     * @param valorDoCampo
     * @return NeoObject
     * @throws NotFoundException
     */
    public static NeoObject neoObjectFactory(String entidadeFiltro, String atributoFiltro, String valorDoCampo) throws NaoEncontradoException, Exception {
	String entidade = entidadeFiltro;
	String atributo = atributoFiltro;
	String valor = valorDoCampo;
	
	NeoObject objeto = null;
	QLEqualsFilter filtro = new QLEqualsFilter(atributo, valor);

	try {
	    objeto = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass(entidade), filtro);
	    if(objeto != null){
		return objeto;
	    } else {
		throw new NaoEncontradoException("Objeto do tipo " + entidade + "não encontrado com o valor " + valor + " para o atributo "  + atributo);
	    }

	} catch (NaoEncontradoException e) {
	    throw e;
	} catch (Exception e){
	    throw e;
	}
    }
    
    /**
     * Factory com filtro simples de tipo int
     * @param nomeFormulario
     * @param campoFiltro
     * @param valorDoCampo
     * @return NeoObject
     * @throws NotFoundException
     */
    public static NeoObject neoObjectFactory(String entidadeFiltro, String atributoFiltro, int valorDoCampo) throws NaoEncontradoException, Exception {
	String entidade = entidadeFiltro;
	String atributo = atributoFiltro;
	int valor = valorDoCampo;
	
	NeoObject objeto = null;
	QLEqualsFilter filtro = new QLEqualsFilter(atributo, valor);
	try {
	    objeto = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass(entidade), filtro);
	    if(objeto != null){
		return objeto;
	    } else {
		throw new NaoEncontradoException("Objeto do tipo " + entidade + "não encontrado com o valor " + valor + " para o atributo "  + atributo);
	    }
	} catch (NaoEncontradoException e) {
	    throw e;
	} catch (Exception e){
	    throw e;
	}
    }
    
    
    /**
     * Factory com filtro simples de tipo Object
     * Pode ser usado qualquer objeto que herde esta classe.
     * @param nomeFormulario
     * @param campoFiltro
     * @param valorFiltro
     * @return NeoObject
     * @throws NotFoundException
     */
    public static NeoObject neoObjectFactory(String entidadeFiltro, String atributoFiltro, Object valorFiltro) throws NaoEncontradoException, Exception {
	String entidade = entidadeFiltro;
	String atributo = atributoFiltro;
	Object valor = valorFiltro;

	NeoObject objeto = null;
	QLEqualsFilter filtro = QLEqualsFilter.buildFromObject(atributo, valor);
	
	try {
	    objeto = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass(entidade), filtro);
	    if(objeto != null){
		return objeto;
	     } else {
		 throw new NaoEncontradoException("Objeto do tipo " + entidade + "não encontrado com o valor " + valor + " para o atributo "  + atributo);
	     }
	    
	} catch (NaoEncontradoException e) {
	    throw e;
	} catch (Exception e){
	    throw e;
	}
    } 
    
    /**
     * Factory de NeoPaper com filtro tipo Object
     * @param campoFiltro
     * @param valorFiltro
     * @return NeoPaper
     * @throws NotFoundException
     */
    public static NeoPaper neoPaperFactory(String atributoFiltro, Object valorFiltro) throws NaoEncontradoException, Exception {
	NeoPaper papel = null;
	String atributo = atributoFiltro;
	Object valor = valorFiltro;
	String entidade = "NeoPaper";
	
	QLEqualsFilter filtro = QLEqualsFilter.buildFromObject(atributo, valor);
	try {
	    papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass(entidade), filtro);
	    if(papel != null){
		return papel;
	    } else {
		throw new NaoEncontradoException("Papel " + atributo + " não encontrado com o valor " + valor);
	    }
	} catch (NaoEncontradoException e) {
	    throw e;
	} catch (Exception e){
	    throw e;
	}
    }
}
