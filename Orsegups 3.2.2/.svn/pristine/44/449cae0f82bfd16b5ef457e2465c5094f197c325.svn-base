package com.neomind.fusion.custom.orsegups.ti;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoUtils;


public class RelatorioLigacao
{
    private static final Log log = LogFactory.getLog(RelatorioLigacao.class);

    /**
     * Gera o PDF do relatório, utilizando JasperReports
     */
    public static File geraPDF(NeoObject objeto) 
    {
	Map<String, Object> paramMap = new HashMap<String, Object>();
	InputStream is = null;
	
	try {
	    Collection<RelatorioLigacaoDataSource> dados = new ArrayList<RelatorioLigacaoDataSource>();
	    if (objeto != null){
		EntityWrapper wrapper = new EntityWrapper(objeto);
		NeoUser responsavel = (NeoUser) wrapper.findValue("responsavel"); 
		String colaboradorExecutor = "";		
		colaboradorExecutor =  responsavel.getCode();
		Collection<NeoGroup> grupos = (Collection<NeoGroup>) wrapper.findValue("gruposDep");
		String idsGrupos = "";
		if (grupos != null){
		    for (NeoGroup neoGroup : grupos) {
			if (idsGrupos.isEmpty()){
			    idsGrupos = neoGroup.getNeoId()+"";
			}else{
			    idsGrupos += ","+neoGroup.getNeoId();
			}
		    }
		    
		}
		Long idRegional = (Long) wrapper.findValue("regional.codigo");  
		if (idRegional != null && idRegional.equals(0l)){
		    idRegional = 1l;
		}
		Long neoId = objeto.getNeoId();
		GregorianCalendar datIni = new GregorianCalendar();
		GregorianCalendar datFim = new GregorianCalendar();
		datIni.set(Calendar.DAY_OF_MONTH, 1);	
		datIni.add(Calendar.MONTH, -1);
		
		datFim.set(Calendar.DAY_OF_MONTH, 1);
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();
		StringBuilder descricaoTarefa = new StringBuilder();
		
		descricaoTarefa.append("<table style=\"width:100%\">");
		descricaoTarefa.append("   <tr>");
		descricaoTarefa.append("      <td colspan=\"5\">Prezado Gestor,<br><b>Estas são as ligações dos seus colaboradores que excederam 10 minutos de duração. Favor verificar e justificar se estão de acordo com o trabalho do setor.</b></td>");
		descricaoTarefa.append("   </tr>");
		descricaoTarefa.append("   <tr>");
		descricaoTarefa.append("      <td><b>Colaborador</b></td>");
		descricaoTarefa.append("      <td><b>Telefone Origem</b></td>");
		descricaoTarefa.append("      <td><b>Telefone Destino</b></td>");
		descricaoTarefa.append("      <td><b>Data da Ligação</b></td>");
		descricaoTarefa.append("      <td><b>Duração</b></td>");
		descricaoTarefa.append("   </tr>");
		
		try {
		    int cont = 0;
		    
		    conn = PersistEngine.getConnection("SNEP_PROD");
		    
		    sql.append(" select reg.Codigo as nomeRegional, ");
		    sql.append(" coalesce(NULLIF((select rdl.departamento from [CACUPE\\SQL02].Fusion_Producao.dbo.D_responsavelDepartamentoLigacao rdl where rdl.neoId = ?),''),sg.name) as departamento, ");
		    sql.append(" se.name as usuario, cdr.src as origem, cdr.dst as destino, cdr.duration as duracao, cdr.calldate as dataligacao ");
		    sql.append(" from [FSOODB03\\SQL01].SNEP.dbo.cdr cdr with(nolock) ");
		    sql.append(" inner join [FSOODB03\\SQL01].SNEP.dbo.cdr_orsegups cdr_ors with(nolock) on cdr.uniqueid = cdr_ors.UID and cdr.idRegional = cdr_ors.idRegional ");
		    sql.append(" left join [FSOODB03\\SQL01].SNEP.dbo.Regionais reg with(nolock) on reg.idRegional = cdr.idRegional ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_TICadeado cadeado on cadeado.codigoLigador = cdr_ors.CODIGO ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.NeoUser usuario on usuario.neoId = cadeado.usuarioFusion_neoId ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.SecurityEntity se on se.neoId = usuario.neoId ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.NeoGroup grupo on grupo.neoId = usuario.group_neoId ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.SecurityEntity sg on sg.neoId = grupo.neoId ");
		    sql.append(" where cdr.disposition = 'ANSWERED' ");
		    sql.append(" and len(cdr.dst) != 4 and cdr.dst not in ('s','asterisk','','i','t','h') ");
		    sql.append(" and cdr.billsec > 0 ");
		    sql.append(" and cdr.dcontext != 'transferencias' ");
		    sql.append(" and cdr.dcontext != 'conferences' ");
		    sql.append(" and cdr.calldate between ? and ? ");
		    sql.append(" and reg.idRegional = ? ");
		    sql.append(" and grupo.neoId in (");
		    sql.append(idsGrupos);
		    sql.append(")");
		    sql.append(" order by reg.Codigo, departamento, se.name, cdr.calldate desc ");
		    
		    pstm = conn.prepareStatement(sql.toString());
		    
		    pstm.setLong(1, neoId); // responsavel
		    pstm.setDate(2, new java.sql.Date(datIni.getTime().getTime()));
		    pstm.setDate(3, new java.sql.Date(datFim.getTime().getTime()));
		    pstm.setLong(4, idRegional);
		    
		    rs = pstm.executeQuery();		    
		    while (rs.next()) {
			RelatorioLigacaoDataSource dto = new RelatorioLigacaoDataSource();
			dto.setNomeRegional(rs.getString("nomeRegional"));
			dto.setDepartamento(rs.getString("departamento"));
			dto.setUsuario(rs.getString("usuario"));
			dto.setOrigem(rs.getString("origem"));
			dto.setDestino(rs.getString("destino"));
			dto.setDuracao(rs.getLong("duracao"));
			dto.setDataLigacao(rs.getTimestamp("dataligacao"));
			Long duracao = rs.getLong("duracao");
			if (duracao>=600L && cont <= 10) {
			    descricaoTarefa.append("   <tr>");
			    descricaoTarefa.append("      <td>");
			    descricaoTarefa.append(dto.getUsuario());
			    descricaoTarefa.append("      </td>");
			    descricaoTarefa.append("      <td>");
			    descricaoTarefa.append(dto.getOrigem());
			    descricaoTarefa.append("      </td>");
			    descricaoTarefa.append("      <td>");
			    descricaoTarefa.append(dto.getDestino());
			    descricaoTarefa.append("      </td>");
			    descricaoTarefa.append("      <td>");
			    
			    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
			    String str = fmt.format(dto.getDataLigacao());
			    
			    descricaoTarefa.append(str);
			    descricaoTarefa.append("      </td>");
			    descricaoTarefa.append("      <td>");
			    descricaoTarefa.append((Integer.parseInt(""+dto.getDuracao())/60)+" minutos e " + (Integer.parseInt(""+dto.getDuracao())%60) + " segundos");
			    descricaoTarefa.append("      </td>");
			    descricaoTarefa.append("   </tr>");		
			    cont++;
			}
			GregorianCalendar g = new GregorianCalendar();
			g.set(Calendar.MILLISECOND, 0);
			g.set(Calendar.SECOND, 0);
			g.set(Calendar.MINUTE, 0);
			g.set(Calendar.HOUR, 0);
			g.set(Calendar.HOUR_OF_DAY, 0);
			g.add(Calendar.SECOND, duracao.intValue());
			dto.setDuracaoLigacao(new Timestamp(g.getTimeInMillis()));
			dados.add(dto);
		    }
		    if (cont > 0 && cont < 10) {
			descricaoTarefa.append("</table>");
			abrirTarefa(descricaoTarefa.toString(),colaboradorExecutor);
		    } else if (cont > 0){
			descricaoTarefa.append("  <tr>");
			descricaoTarefa.append("    <td colspan=\"5\">");
			descricaoTarefa.append("      <br>As demais ligações que ultrapassaram 10 minutos devem ser verificadas no relatorio completo.");
			descricaoTarefa.append("    </td>");
			descricaoTarefa.append("  </tr>"); 
			descricaoTarefa.append("</table>");
			abrirTarefa(descricaoTarefa.toString(),colaboradorExecutor);
		    }
		    
		    File file = File.createTempFile("RelatorioLigacoes_"+idRegional+"_", ".pdf");
		    file.deleteOnExit();
		    String caminho = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "LigacoesRegional.jasper";
//		    String caminho = "C:\\ambiente_neomind\\workspace\\FusionOrsegups3.2.2\\src\\main\\resources\\reports\\LigacoesRegional.jasper";

		    is = new BufferedInputStream(new FileInputStream(caminho));
		    JasperPrint impressao = null;
		    JRDataSource jrdts = new JRBeanCollectionDataSource(dados);
		    impressao = JasperFillManager.fillReport(is, paramMap, jrdts);
		    JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
		    return file;
		} catch (SQLException e) {
		    e.printStackTrace();
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	    }
	    
	    return null;

	} catch (Exception e) {
	    log.error("Erro ao gerar o PDF do Relatório de Ligações!! ", e);
	    e.printStackTrace();
	}
	return null;
    }
    
    private static String abrirTarefa(String descricao, String executor) throws Exception {
	String tarefa = "";

	String solicitante = "kelli.cunha";

	GregorianCalendar prazo = new GregorianCalendar();
	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);

	String titulo = "Seguem 10 Ligações Acima de 10 minutos";

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

	tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);

	return tarefa;
    }
    
    /**
     * Gera o PDF do relatório, utilizando JasperReports
     */
    public static File geraPDFSintetico(NeoObject objeto) 
    {
	Map<String, Object> paramMap = new HashMap<String, Object>();
	InputStream is = null;
	
	try {
	    Collection<RelatorioLigacaoDataSourceSintetico> dados = new ArrayList<RelatorioLigacaoDataSourceSintetico>();
	    if (objeto != null){
		EntityWrapper wrapper = new EntityWrapper(objeto);
		Collection<NeoGroup> grupos = (Collection<NeoGroup>) wrapper.findValue("gruposDep");
		String idsGrupos = "";
		if (grupos != null){
		    for (NeoGroup neoGroup : grupos) {
			if (idsGrupos.isEmpty()){
			    idsGrupos = neoGroup.getNeoId()+"";
			}else{
			    idsGrupos += ","+neoGroup.getNeoId();
			}
		    }
		    
		}
		Long idRegional = (Long) wrapper.findValue("regional.codigo");  
		if (idRegional != null && idRegional.equals(0l)){
		    idRegional = 1l;
		}
		Long neoId = objeto.getNeoId();
		GregorianCalendar datIni = new GregorianCalendar();
		GregorianCalendar datFim = new GregorianCalendar();
		datIni.set(Calendar.DAY_OF_MONTH, 1);	
		datIni.add(Calendar.MONTH, -1);
		
		datFim.set(Calendar.DAY_OF_MONTH, 1);
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();

		try {
		    conn = PersistEngine.getConnection("SNEP_PROD");
		    
		  
		    
		    sql.append(" select reg.Codigo as nomeRegional,  ");
		    sql.append(" coalesce(NULLIF(rpl.departamento,''),sg.name) as departamento, ");
		    sql.append(" se.name as usuario,  ");
		    sql.append(" SUM(cdr.duration) as duracao, ");
		    sql.append(" COUNT(cdr.src) as totalLigacao ");
		    sql.append(" from cdr cdr with(nolock) ");
		    sql.append(" inner join cdr_orsegups cdr_ors with(nolock) on cdr.uniqueid = cdr_ors.UID and cdr.idRegional = cdr_ors.idRegional ");
		    sql.append(" left join Regionais reg with(nolock) on reg.idRegional = cdr.idRegional ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_TICadeado cadeado on cadeado.codigoLigador = cdr_ors.CODIGO ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.NeoUser usuario on usuario.neoId = cadeado.usuarioFusion_neoId ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.SecurityEntity se on se.neoId = usuario.neoId ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.NeoGroup grupo on grupo.neoId = usuario.group_neoId ");
		    sql.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.SecurityEntity sg on sg.neoId = grupo.neoId ");
		    sql.append(" left join [CACUPE\\SQL02].Fusion_Producao.dbo.D_responsavelDepartamentoLigacao rpl on rpl.neoId = ? ");
		    sql.append(" where cdr.disposition = 'ANSWERED' ");
		    sql.append(" and len(cdr.dst) != 4 and cdr.dst not in ('s','asterisk','','i','t','h') ");
		    sql.append(" and cdr.billsec > 0 ");
		    sql.append(" and cdr.dcontext != 'transferencias' ");
		    sql.append(" and cdr.dcontext != 'conferences' ");
		    sql.append(" and cdr.calldate between ? and ? ");
		    sql.append(" and reg.idRegional = ? ");
		    sql.append(" and grupo.neoId in (");
		    sql.append(idsGrupos);
		    sql.append(")");
		    sql.append(" group by reg.Codigo,coalesce(NULLIF(rpl.departamento,''),sg.name),se.name ");
		    sql.append(" order by reg.Codigo,departamento, duracao desc ");
		    
		    pstm = conn.prepareStatement(sql.toString());
		    
		    pstm.setLong(1, neoId);
		    pstm.setDate(2, new java.sql.Date(datIni.getTime().getTime()));
		    pstm.setDate(3, new java.sql.Date(datFim.getTime().getTime()));
		    pstm.setLong(4, idRegional);
		    
		    rs = pstm.executeQuery();		    
		    while (rs.next()) {
			RelatorioLigacaoDataSourceSintetico dto = new RelatorioLigacaoDataSourceSintetico();
			dto.setNomeRegional(rs.getString("nomeRegional"));
			dto.setDepartamento(rs.getString("departamento"));
			dto.setUsuario(rs.getString("usuario"));
			dto.setDuracao(rs.getLong("duracao"));
			dto.setTotalLigacao(rs.getLong("totalLigacao"));
			GregorianCalendar gc = new GregorianCalendar();
			gc.add(Calendar.MONTH, -1);
			dto.setCompetencia(NeoUtils.safeDateFormat(gc, "MM/yyyy"));
			Long duracao = rs.getLong("duracao");
			GregorianCalendar g = new GregorianCalendar();
			g.set(Calendar.MILLISECOND, 0);
			g.set(Calendar.SECOND, 0);
			g.set(Calendar.MINUTE, 0);
			g.set(Calendar.HOUR, 0);
			g.set(Calendar.HOUR_OF_DAY, 0);
			g.add(Calendar.SECOND, duracao.intValue());
			dto.setDuracaoLigacao(new Timestamp(g.getTimeInMillis()));
			dados.add(dto);
		    }
		    File file = File.createTempFile("RelatorioLigacoesSintetico_"+idRegional+"_", ".pdf");
		    file.deleteOnExit();
		    String caminho = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "LigacoesRegionalSintetico.jasper";
//		    String caminho = "C:\\ambiente_neomind\\workspace\\FusionOrsegups3.2.2\\src\\main\\resources\\reports\\LigacoesRegionalSintetico.jasper";

		    is = new BufferedInputStream(new FileInputStream(caminho));
		    JasperPrint impressao = null;
		    JRDataSource jrdts = new JRBeanCollectionDataSource(dados);
		    impressao = JasperFillManager.fillReport(is, paramMap, jrdts);
		    JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
		    return file;
		} catch (SQLException e) {
		    e.printStackTrace();
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	    }
	    
	    return null;

	} catch (Exception e) {
	    log.error("Erro ao gerar o PDF do Relatório de Ligações Sintético!! ", e);
	    e.printStackTrace();
	}
	return null;
    }

    
}
