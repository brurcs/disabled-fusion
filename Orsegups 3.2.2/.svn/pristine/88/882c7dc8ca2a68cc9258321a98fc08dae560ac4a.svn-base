package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPCopiaDeCamposGLNSliparPagamento implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "";
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		try
		{
			Boolean abrirTitulo = (Boolean) processEntity.findValue("abrirTitulo");
			Long tipoLancamento = (Long) processEntity.findValue("lancarNotaOuTitulo.codigoLancamento");
			String usuarioSolicitante = (String) processEntity.findValue("solicitanteOrcamento.code");

			QLGroupFilter filtroCondicaoPagamento = new QLGroupFilter("AND");
			filtroCondicaoPagamento.addFilter(new QLEqualsFilter("codemp", ((Long) processEntity.findValue("empresa.codemp"))));
			filtroCondicaoPagamento.addFilter(new QLEqualsFilter("codcpg", "AV"));
			List<NeoObject> condicaoPagamento = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CondicaoPagamento"), filtroCondicaoPagamento);
			if (condicaoPagamento != null && condicaoPagamento.size() > 0)
			{
				condicaoPagamento.get(0);
			}
			else
			{
				mensagem = "Não encontrado o Código da Condição de Pagamentos para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLEqualsFilter fitroUnidadeMedida = new QLEqualsFilter("unimed", "UN");
			List<NeoObject> lstUnidadeMedida = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE015MED"), fitroUnidadeMedida);
			if (lstUnidadeMedida != null && lstUnidadeMedida.size() > 0)
			{
				lstUnidadeMedida.get(0);
			}
			else
			{
				mensagem = "Não encontrado a Unidade de Medida para lançamento da Nota Fiscal de Entrada!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroFormaPagamento = new QLGroupFilter("AND");
			filtroFormaPagamento.addFilter(new QLEqualsFilter("codemp", ((Long) processEntity.findValue("empresa.codemp"))));
			filtroFormaPagamento.addFilter(new QLEqualsFilter("codfpg", ((Long) processEntity.findValue("formaPagamento.codigoFormaPagamento"))));
			List<NeoObject> formaPagamento = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE066FPG"), filtroFormaPagamento);
			if (formaPagamento != null && formaPagamento.size() > 0)
			{
				formaPagamento.get(0);
			}
			else
			{
				mensagem = "Não encontrado o Código da Forma de Pagamento para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroUsuarioSolicitante = new QLGroupFilter("AND");
			filtroUsuarioSolicitante.addFilter(new QLEqualsFilter("nomusu", (usuarioSolicitante)));
			List<NeoObject> solicitante = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), filtroUsuarioSolicitante);
			if (solicitante != null && solicitante.size() > 0)
			{
				solicitante.get(0);
			}
			else
			{
				mensagem = "Não encontrado o Código do usuário Solicitante informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			if (!abrirTitulo && tipoLancamento == 1L)
			{
				InstantiableEntityInfo gestaLancamentoDeNota = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNota = gestaLancamentoDeNota.createNewInstance();
				EntityWrapper wrpGestaLancamentoDeNota = new EntityWrapper(oGestaLancamentoDeNota);

				wrpGestaLancamentoDeNota.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaLancamentoDeNota.setValue("automatico", Boolean.TRUE);
				wrpGestaLancamentoDeNota.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaLancamentoDeNota.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaLancamentoDeNota.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaLancamentoDeNota.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaLancamentoDeNota.setValue("formaPagamento", formaPagamento.get(0));
				wrpGestaLancamentoDeNota.setValue("valorLiquido", processEntity.findValue("valorTotal"));
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaLancamentoDeNota.setValue("usuarioResponsavel", solicitante.get(0));
				}

				BigDecimal valorTotal = new BigDecimal(0.0);
				BigDecimal valorTotalProduto = new BigDecimal(0.0);
				BigDecimal valorTotalServico = new BigDecimal(0.0);

				List<NeoObject> lstCentroCustoProduto = new ArrayList<NeoObject>();
				List<NeoObject> lstCentroCustoProdutoObject = new ArrayList<NeoObject>();
				List<NeoObject> lstItensDeProduto = new ArrayList<NeoObject>();
				NeoObject oItensDeProduto = AdapterUtils.createNewEntityInstance("GLNItensDeProduto");
				EntityWrapper wrpItensDeProduto = new EntityWrapper(oItensDeProduto);
				InserirContaItensProdutoServico(processEntity, wrpItensDeProduto);

				List<NeoObject> lstItensFapCentroCusto = (List<NeoObject>) processEntity.findValue("listaCentroDeCusto");
				List<NeoObject> lstItensFapProduto = (List<NeoObject>) processEntity.findValue("itemOrcamento");

				if (lstItensFapProduto != null && !lstItensFapProduto.isEmpty())
				{
					valorTotalProduto = totalizaItemFapPorTipo("PRODUTO", lstItensFapProduto);

					if (valorTotalProduto.compareTo(new BigDecimal(0.00)) > 0)
					{
						if (lstItensFapCentroCusto != null && !lstItensFapCentroCusto.isEmpty())
						{
							lstCentroCustoProduto = verificaCentroCustoSapiens(((Long) processEntity.findValue("empresa.codemp")), lstItensFapCentroCusto);
							if (lstCentroCustoProduto != null && !lstCentroCustoProduto.isEmpty())
							{
								lstCentroCustoProdutoObject = insereCentroCustoFusion(((Long) processEntity.findValue("empresa.codemp")), lstCentroCustoProduto, valorTotalProduto);
							}
							else
							{
								mensagem = "Centro de Custo não localizado! Por favor, verificar o Centro de Custo associado ao FAP e se o mesmo existe na Empresa de lançamento da Nota!";
								throw new WorkflowException(mensagem);
							}
						}

						wrpItensDeProduto.setValue("unidadeMedida", lstUnidadeMedida.get(0));
						wrpItensDeProduto.setValue("quantidadeRecebida", 1L);
						wrpItensDeProduto.setValue("precoUnitarioProduto", valorTotalProduto);
						wrpItensDeProduto.setValue("listaCentroCusto", lstCentroCustoProdutoObject);

						lstItensDeProduto.add(oItensDeProduto);
						PersistEngine.persist(oItensDeProduto);

						wrpGestaLancamentoDeNota.setValue("listaItensDeProduto", lstItensDeProduto);
					}
				}

				List<NeoObject> lstCentroCustoServico = new ArrayList<NeoObject>();
				List<NeoObject> lstCentroCustoServicoObject = new ArrayList<NeoObject>();
				List<NeoObject> lstItensDeServico = new ArrayList<NeoObject>();
				NeoObject oItensDeServico = AdapterUtils.createNewEntityInstance("GLNItensDeServico");
				EntityWrapper wrpItensDeServico = new EntityWrapper(oItensDeServico);
				InserirContaItensProdutoServico(processEntity, wrpItensDeServico);

				List<NeoObject> lstItensFapServico = (List<NeoObject>) processEntity.findValue("itemOrcamento");
				if (lstItensFapServico != null && !lstItensFapServico.isEmpty())
				{
					valorTotalServico = totalizaItemFapPorTipo("SERVIÇO", lstItensFapProduto);

					if (valorTotalServico.compareTo(new BigDecimal(0.00)) > 0)
					{
						if (lstItensFapCentroCusto != null && !lstItensFapCentroCusto.isEmpty())
						{
							if (lstItensFapCentroCusto != null && !lstItensFapCentroCusto.isEmpty())
							{
								lstCentroCustoServico = verificaCentroCustoSapiens(((Long) processEntity.findValue("empresa.codemp")), lstItensFapCentroCusto);
								if (lstCentroCustoServico != null && !lstCentroCustoServico.isEmpty())
								{
									lstCentroCustoServicoObject = insereCentroCustoFusion(((Long) processEntity.findValue("empresa.codemp")), lstCentroCustoServico, valorTotalServico);
								}
								else
								{
									mensagem = "Centro de Custo não localizado! Por favor, verificar o Centro de Custo associado ao FAP e se o mesmo existe na Empresa de lançamento da Nota!";
									throw new WorkflowException(mensagem);
								}
							}
						}

						wrpItensDeServico.setValue("unidadeMedida", lstUnidadeMedida.get(0));
						wrpItensDeServico.setValue("quantidadeRecebida", 1L);
						wrpItensDeServico.setValue("precoUnitarioServico", valorTotalServico);
						wrpItensDeServico.setValue("listaCentroCusto", lstCentroCustoServicoObject);

						lstItensDeServico.add(oItensDeServico);
						PersistEngine.persist(oItensDeServico);

						wrpGestaLancamentoDeNota.setValue("listaItensDeServico", lstItensDeServico);
					}
				}

				String observacao = "";
				Long aplicacaoPagamento = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
				String nomeAplicacao = (String) processEntity.findValue("aplicacaoPagamento.nome");
				String code = (String) processEntity.findValue("wfprocess.code");

				if (aplicacaoPagamento == 1L)
				{
					String viaturaPlaca = (String) processEntity.findValue("viatura.placa");
					String tipoViatura = (String) processEntity.findValue("tipoViatura");
					String codigoRegional = (String) processEntity.findValue("codigoRegional");
					String anoViatura = (String) processEntity.findValue("anoViatura");

					if (anoViatura != null)
					{
						anoViatura = (String) processEntity.findValue("anoViatura");
					}
					else
					{
						anoViatura = "Ano não informado";
					}

					observacao = nomeAplicacao + " - " + viaturaPlaca + " - " + tipoViatura + " - " + anoViatura + " - Regional: " + codigoRegional + " - FAP: " + code;
				}
				else if ((aplicacaoPagamento == 2) || (aplicacaoPagamento == 3) || (aplicacaoPagamento == 5) || 
						 (aplicacaoPagamento == 6) || (aplicacaoPagamento == 8) || (aplicacaoPagamento == 9))
				{
					String codigoConta = (String) processEntity.findValue("codigoConta");
					String razaoSocialConta = (String) processEntity.findValue("razaoSocialConta");
					observacao = nomeAplicacao + " - " + codigoConta + " - " + razaoSocialConta + " - FAP: " + code;
				}else if (aplicacaoPagamento == 4L){
				    observacao = "Aplicação: "+aplicacaoPagamento+" - "+nomeAplicacao+" - FAP: "+code;
				} 
				else if(aplicacaoPagamento == 12L){
					String codigoProduto = processEntity.findGenericValue("patrimonio.codigo");
					String nomeproduto = processEntity.findGenericValue("patrimonio.produto.despro");
					
					observacao = nomeAplicacao + " " + codigoProduto + " - " + nomeproduto + " - FAP: " + code;
				}

				List<NeoObject> lstParcelas = new ArrayList<NeoObject>();
				valorTotal = valorTotal.add(valorTotalProduto).add(valorTotalServico);
				lstParcelas = insereParcelasFusion(valorTotal, observacao);
				wrpGestaLancamentoDeNota.setValue("listaParcelas", lstParcelas);
				wrpGestaLancamentoDeNota.setValue("observacao", observacao);

				processEntity.findField("GlnGestaoDeLancamentoDeNota").setValue(oGestaLancamentoDeNota);
				PersistEngine.persist(oGestaLancamentoDeNota);
			}
			else if (abrirTitulo && tipoLancamento == 1L)
			{
				String observacao = "";

				Long aplicacaoPagamento = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
				String nomeAplicacao = (String) processEntity.findValue("aplicacaoPagamento.nome");
				String code = (String) processEntity.findValue("wfprocess.code");

				if (aplicacaoPagamento == 1L)
				{
					String viaturaPlaca = (String) processEntity.findValue("viatura.placa");
					String tipoViatura = (String) processEntity.findValue("tipoViatura");
					String codigoRegional = (String) processEntity.findValue("codigoRegional");
					String anoViatura = (String) processEntity.findValue("anoViatura");

					if (anoViatura != null)
					{
						anoViatura = (String) processEntity.findValue("anoViatura");
					}
					else
					{
						anoViatura = "Ano não informado";
					}

					observacao = nomeAplicacao + " - " + viaturaPlaca + " - " + tipoViatura + " - " + anoViatura + " - Regional: " + codigoRegional + " - FAP: " + code;
				}
				else if ((aplicacaoPagamento == 2) || (aplicacaoPagamento == 3) || (aplicacaoPagamento == 5) || 
						 (aplicacaoPagamento == 6) || (aplicacaoPagamento == 8) || (aplicacaoPagamento == 9))
				{
					String codigoConta = (String) processEntity.findValue("codigoConta");
					String razaoSocialConta = (String) processEntity.findValue("razaoSocialConta");
					observacao = nomeAplicacao + " - " + codigoConta + " - " + razaoSocialConta + " - FAP: " + code;
				}else if (aplicacaoPagamento == 4L){
				    observacao = "Aplicação: "+aplicacaoPagamento+" - "+nomeAplicacao+" - FAP: "+code;
				}

				//Insere os Itens de Produto
				InstantiableEntityInfo gestaLancamentoDeNotaProduto = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNotaProduto = gestaLancamentoDeNotaProduto.createNewInstance();
				EntityWrapper wrpGestaoLancamentoDeNotaProduto = new EntityWrapper(oGestaLancamentoDeNotaProduto);

				wrpGestaoLancamentoDeNotaProduto.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaoLancamentoDeNotaProduto.setValue("automatico", Boolean.TRUE);
				wrpGestaoLancamentoDeNotaProduto.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaoLancamentoDeNotaProduto.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaoLancamentoDeNotaProduto.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaoLancamentoDeNotaProduto.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaoLancamentoDeNotaProduto.setValue("formaPagamento", formaPagamento.get(0));
				wrpGestaoLancamentoDeNotaProduto.setValue("valorLiquido", processEntity.findValue("valorTotalProduto"));
				wrpGestaoLancamentoDeNotaProduto.setValue("produtoServico", "P");
				wrpGestaoLancamentoDeNotaProduto.setValue("observacao", observacao);
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaoLancamentoDeNotaProduto.setValue("usuarioResponsavel", solicitante.get(0));
				}

				BigDecimal valorTotalProduto = new BigDecimal(0.0);
				BigDecimal valorTotalServico = new BigDecimal(0.0);

				List<NeoObject> lstCentroCustoProduto = new ArrayList<NeoObject>();
				List<NeoObject> lstCentroCustoProdutoObject = new ArrayList<NeoObject>();
				List<NeoObject> lstItensDeProduto = new ArrayList<NeoObject>();
				NeoObject oItensDeProduto = AdapterUtils.createNewEntityInstance("GLNItensDeProduto");
				EntityWrapper wrpItensDeProduto = new EntityWrapper(oItensDeProduto);
				InserirContaItensProdutoServico(processEntity, wrpItensDeProduto);

				List<NeoObject> lstItensFapCentroCusto = (List<NeoObject>) processEntity.findValue("listaCentroDeCusto");
				List<NeoObject> lstItensFapProduto = (List<NeoObject>) processEntity.findValue("itemOrcamento");

				if (lstItensFapProduto != null && !lstItensFapProduto.isEmpty())
				{
					valorTotalProduto = (BigDecimal) processEntity.findValue("valorTotalProduto");

					if (valorTotalProduto.compareTo(new BigDecimal(0.00)) > 0)
					{
						if (lstItensFapCentroCusto != null && !lstItensFapCentroCusto.isEmpty())
						{
							lstCentroCustoProduto = verificaCentroCustoSapiens(((Long) processEntity.findValue("empresa.codemp")), lstItensFapCentroCusto);
							if (lstCentroCustoProduto != null && !lstCentroCustoProduto.isEmpty())
							{
								lstCentroCustoProdutoObject = insereCentroCustoFusion(((Long) processEntity.findValue("empresa.codemp")), lstCentroCustoProduto, valorTotalProduto);
							}
							else
							{
								mensagem = "Centro de Custo não localizado! Por favor, verificar o Centro de Custo associado ao FAP e se o mesmo existe na Empresa de lançamento da Nota!";
								throw new WorkflowException(mensagem);
							}
						}

						wrpItensDeProduto.setValue("unidadeMedida", lstUnidadeMedida.get(0));
						wrpItensDeProduto.setValue("quantidadeRecebida", 1L);
						wrpItensDeProduto.setValue("precoUnitarioProduto", valorTotalProduto);
						wrpItensDeProduto.setValue("listaCentroCusto", lstCentroCustoProdutoObject);

						lstItensDeProduto.add(oItensDeProduto);
						PersistEngine.persist(oItensDeProduto);

						wrpGestaoLancamentoDeNotaProduto.setValue("listaItensDeProduto", lstItensDeProduto);
					}
				}

				List<NeoObject> lstParcelasProduto = new ArrayList<NeoObject>();
				lstParcelasProduto = insereParcelasFusion(valorTotalProduto, observacao);
				wrpGestaoLancamentoDeNotaProduto.setValue("observacao", observacao);
				wrpGestaoLancamentoDeNotaProduto.setValue("listaParcelas", lstParcelasProduto);

				processEntity.findField("GlnGestaoDeLancamentoDeNotaProduto").setValue(oGestaLancamentoDeNotaProduto);
				PersistEngine.persist(oGestaLancamentoDeNotaProduto);

				//Insere os Itens de Serviço
				InstantiableEntityInfo gestaLancamentoDeNotaServico = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNotaServico = gestaLancamentoDeNotaServico.createNewInstance();
				EntityWrapper wrpGestaoLancamentoDeNotaServico = new EntityWrapper(oGestaLancamentoDeNotaServico);

				wrpGestaoLancamentoDeNotaServico.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaoLancamentoDeNotaServico.setValue("automatico", Boolean.TRUE);
				wrpGestaoLancamentoDeNotaServico.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaoLancamentoDeNotaServico.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaoLancamentoDeNotaServico.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaoLancamentoDeNotaServico.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaoLancamentoDeNotaServico.setValue("formaPagamento", formaPagamento.get(0));
				wrpGestaoLancamentoDeNotaServico.setValue("valorLiquido", processEntity.findValue("valorTotalServicos"));
				wrpGestaoLancamentoDeNotaServico.setValue("produtoServico", "S");
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaoLancamentoDeNotaServico.setValue("usuarioResponsavel", solicitante.get(0));
				}

				List<NeoObject> lstCentroCustoServico = new ArrayList<NeoObject>();
				List<NeoObject> lstCentroCustoServicoObject = new ArrayList<NeoObject>();
				List<NeoObject> lstItensDeServico = new ArrayList<NeoObject>();
				NeoObject oItensDeServico = AdapterUtils.createNewEntityInstance("GLNItensDeServico");
				EntityWrapper wrpItensDeServico = new EntityWrapper(oItensDeServico);
				InserirContaItensProdutoServico(processEntity, wrpItensDeServico);

				List<NeoObject> lstItensFapServico = (List<NeoObject>) processEntity.findValue("itemOrcamento");
				if (lstItensFapServico != null && !lstItensFapServico.isEmpty())
				{
					valorTotalServico = (BigDecimal) processEntity.findValue("valorTotalServicos");

					if (valorTotalServico.compareTo(new BigDecimal(0.00)) > 0)
					{
						if (lstItensFapCentroCusto != null && !lstItensFapCentroCusto.isEmpty())
						{
							if (lstItensFapCentroCusto != null && !lstItensFapCentroCusto.isEmpty())
							{
								lstCentroCustoServico = verificaCentroCustoSapiens(((Long) processEntity.findValue("empresa.codemp")), lstItensFapCentroCusto);
								if (lstCentroCustoServico != null && !lstCentroCustoServico.isEmpty())
								{
									lstCentroCustoServicoObject = insereCentroCustoFusion(((Long) processEntity.findValue("empresa.codemp")), lstCentroCustoServico, valorTotalServico);
								}
								else
								{
									mensagem = "Centro de Custo não localizado! Por favor, verificar o Centro de Custo associado ao FAP e se o mesmo existe na Empresa de lançamento da Nota!";
									throw new WorkflowException(mensagem);
								}
							}
						}

						wrpItensDeServico.setValue("unidadeMedida", lstUnidadeMedida.get(0));
						wrpItensDeServico.setValue("quantidadeRecebida", 1L);
						wrpItensDeServico.setValue("precoUnitarioServico", valorTotalServico);
						wrpItensDeServico.setValue("listaCentroCusto", lstCentroCustoServicoObject);

						lstItensDeServico.add(oItensDeServico);
						PersistEngine.persist(oItensDeServico);

						wrpGestaoLancamentoDeNotaServico.setValue("listaItensDeServico", lstItensDeServico);
					}
				}

				List<NeoObject> lstParcelasServico = new ArrayList<NeoObject>();
				lstParcelasServico = insereParcelasFusion(valorTotalServico, observacao);
				wrpGestaoLancamentoDeNotaServico.setValue("observacao", observacao);
				wrpGestaoLancamentoDeNotaServico.setValue("listaParcelas", lstParcelasServico);

				processEntity.findField("GlnGestaoDeLancamentoDeNotaServico").setValue(oGestaLancamentoDeNotaServico);
				PersistEngine.persist(oGestaLancamentoDeNotaServico);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
	}

	public static BigDecimal totalizaItemFapPorTipo(String tipoItem, List<NeoObject> lstItensFap)
	{
		BigDecimal valorTotal = new BigDecimal(0.00);
		for (NeoObject oItemFap : lstItensFap)
		{
			EntityWrapper wrpItem = new EntityWrapper(oItemFap);
			if (wrpItem.findValue("tipoItem.descricao") != null && ((String) wrpItem.findValue("tipoItem.descricao")).equals(tipoItem))
			{
				valorTotal = valorTotal.add((BigDecimal) wrpItem.findValue("valor"));
			}
		}
		return valorTotal;
	}

	public static List<NeoObject> verificaCentroCustoSapiens(Long codigoEmpresa, List<NeoObject> lstItensFapCentroCusto)
	{
		List<NeoObject> lstCentroCusto = new ArrayList<NeoObject>();
		List<NeoObject> externoCentroCusto = new ArrayList<NeoObject>();

		for (NeoObject oItemCentroCusto : lstItensFapCentroCusto)
		{
			EntityWrapper wrapper = new EntityWrapper(oItemCentroCusto);
			if (wrapper != null)
			{
				String codigoCentroCusto = (String) wrapper.findValue("codigoCentroCusto");

				QLGroupFilter filterCcu = new QLGroupFilter("AND");
				filterCcu.addFilter(new QLEqualsFilter("codccu", codigoCentroCusto));
				filterCcu.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
				filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));
				externoCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

				if (externoCentroCusto == null || externoCentroCusto.isEmpty())
				{
					lstCentroCusto.clear();
					break;
				}
				else
				{
					lstCentroCusto.add(externoCentroCusto.get(0));
				}
			}
		}

		return lstCentroCusto;
	}

	public static List<NeoObject> insereParcelasFusion(BigDecimal valorTotal, String observacao)
	{
		List<NeoObject> lstParcelas = new ArrayList<NeoObject>();

		NeoObject oParcelas = AdapterUtils.createNewEntityInstance("GLNParcelas");
		EntityWrapper wrpParcelas = new EntityWrapper(oParcelas);

		wrpParcelas.setValue("titulo", "1");
		wrpParcelas.setValue("vencimentoParcela", new GregorianCalendar());
		wrpParcelas.setValue("valorParcela", valorTotal);
		wrpParcelas.setValue("observacaoParcela", observacao);

		lstParcelas.add(oParcelas);
		PersistEngine.persist(oParcelas);

		return lstParcelas;
	}

	public static List<NeoObject> insereCentroCustoFusion(Long codigoEmpresa, List<NeoObject> lstItensFapCentroCusto, BigDecimal valorTotal)
	{
		BigDecimal valorRateio = new BigDecimal(0.00);
		BigDecimal valorTotalRateio = new BigDecimal(0.00);
		List<NeoObject> lstCentroCusto = new ArrayList<NeoObject>();

		int qtdCentroCusto = lstItensFapCentroCusto.size();
		valorRateio = valorTotal.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);

		for (NeoObject oItemCentroCusto : lstItensFapCentroCusto)
		{
			if (oItemCentroCusto != null)
			{
				NeoObject oItensCentroCusto = AdapterUtils.createNewEntityInstance("GLNListaDeCentroDeCusto");
				EntityWrapper wrpCentroCusto = new EntityWrapper(oItensCentroCusto);

				if (lstItensFapCentroCusto != null && !lstItensFapCentroCusto.isEmpty())
				{
					wrpCentroCusto.setValue("codigoCentroCusto", oItemCentroCusto);
					wrpCentroCusto.setValue("valorRateio", valorRateio);

					valorTotalRateio = valorTotalRateio.add(valorRateio);
					lstCentroCusto.add(oItensCentroCusto);
					PersistEngine.persist(oItensCentroCusto);
				}
			}
		}

		BigDecimal valorAjustado = new BigDecimal(0.00);
		int resultado = valorTotal.compareTo(valorTotalRateio);
		BigDecimal valorSobra = new BigDecimal(0.00);
		valorTotal.plus(new MathContext(2, RoundingMode.HALF_UP));
		valorSobra = valorTotalRateio.subtract(valorTotal);

		NeoObject oCentroCusto = lstCentroCusto.get(lstCentroCusto.size() - 1);
		EntityWrapper wrapperCentroCusto = new EntityWrapper(oCentroCusto);
		valorRateio = (BigDecimal) wrapperCentroCusto.findValue("valorRateio");

		if (resultado == 1)
		{
			BigDecimal retorno = valorRateio.add(valorSobra);
			valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
			wrapperCentroCusto.setValue("valorRateio", valorAjustado);
		}
		else if (resultado == -1)
		{
			BigDecimal retorno = valorRateio.subtract(valorSobra);
			valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
			wrapperCentroCusto.setValue("valorRateio", valorAjustado);
		}

		return lstCentroCusto;
	}

	public static void InserirContaItensProdutoServico(EntityWrapper processEntity, EntityWrapper wrpItensDeProdutoServico)
	{

		String mensagem;
		Long codigoTipoAplicacao = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
		Long codigoEmpresa = (Long) processEntity.findValue("empresa.codemp");

		if (codigoTipoAplicacao == 1)
		{
			// seta conta contabil  para FAP de Frota
			QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
			filtroContaContabil.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
			filtroContaContabil.addFilter(new QLEqualsFilter("ctared", ((Long) 4770l)));
			List<NeoObject> contaContabil = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
			if (contaContabil != null && contaContabil.size() > 0)
			{
				wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
			}
			else
			{
				mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			// seta conta financeira para FAP de Frota
			QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
			filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
			filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", ((Long) 79l)));
			List<NeoObject> contaFinanceira = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
			if (contaFinanceira != null && contaFinanceira.size() > 0)
			{
				wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
			}
			else
			{
				mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

		}
		else
		{
			if ((codigoTipoAplicacao == 2) || (codigoTipoAplicacao == 3) || (codigoTipoAplicacao == 5) || 
				(codigoTipoAplicacao == 6) || (codigoTipoAplicacao == 8) || (codigoTipoAplicacao == 9))
			{
				// seta conta contabil  para FAP de CFTV
				QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
				filtroContaContabil.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
				filtroContaContabil.addFilter(new QLEqualsFilter("ctared", ((Long) 4690l)));
				List<NeoObject> contaContabil = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
				if (contaContabil != null && contaContabil.size() > 0)
				{
					wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

				// seta conta financeira para FAP de CFTV
				QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
				filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
				filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", ((Long) 377l)));
				List<NeoObject> contaFinanceira = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
				if (contaFinanceira != null && contaFinanceira.size() > 0)
				{
					wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

			}
			else
			{

				// seta conta contabil  para FAP de Rastreamento
				QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
				filtroContaContabil.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
				filtroContaContabil.addFilter(new QLEqualsFilter("ctared", ((Long) 38895l)));
				List<NeoObject> contaContabil = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
				if (contaContabil != null && contaContabil.size() > 0)
				{
					wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

				// seta conta financeira para FAP de Rastreamento
				QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
				filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
				filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", ((Long) 707l)));
				List<NeoObject> contaFinanceira = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
				if (contaFinanceira != null && contaFinanceira.size() > 0)
				{
					wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}
}
