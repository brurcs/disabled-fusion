package com.neomind.fusion.custom.orsegups.fap.scheduler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.JobTecnicoTaticoDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.FornecedorDTO;
import com.neomind.fusion.custom.orsegups.fap.exception.NaoEncontradoException;
import com.neomind.fusion.custom.orsegups.fap.factory.FapFactory;
import com.neomind.fusion.custom.orsegups.fap.utils.FapUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;
/**
 * JOB que abre as FAP's para o tipo de aplicação tático.
 * @author diogo.silva
 *
 */
public class RotinaAbreFapTatico implements CustomJobAdapter {

    private static final Log log = LogFactory.getLog(RotinaAbreFapTatico.class);

    @Override
    public void execute(CustomJobContext ctx) {
	System.out.println("INÍCIO DO JOB:" + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm"));
	NeoUser solicitante = null;
	NeoUser aprovador = null;
	Long codigoRota = null;
	String regional = null;
	String nomeRota = null;

	List<FornecedorDTO> listaFornecedores = null;
	List<String> listaRotasParceiras = null;
	List<FornecedorDTO> listaNaoGerouTarefa = new ArrayList<>();
	List<FornecedorDTO> listaNaoAvancouAtividade = new ArrayList<>();

	int totalTarefas = 0;
	int abertas = 0;

	try { 
	    listaRotasParceiras = JobTecnicoTaticoDAO.getRotasParceiras();
	    listaFornecedores = JobTecnicoTaticoDAO.getFornecedoresTaticos(listaRotasParceiras);
	    /* Variável usada no envio do resumo das tarefas abertas */			
	    totalTarefas = listaFornecedores.size();
	    /************************************/
	    log.warn("ROTINA ABRE FAP TATICO - INICIANDO JOB ABRE FAP TATICO - TOTAL DE TAREFAS: " + listaFornecedores.size());
	    if(NeoUtils.safeIsNotNull(listaFornecedores)) {
		for(FornecedorDTO fornecedor : listaFornecedores) {		  
		    try {
			//Instancia os e-forms 
			codigoRota = fornecedor.getCodigoRota();
			NeoObject sigmaRota = PersistEngine.getObject(AdapterUtils.getEntityClass("SIGMAROTA"), new QLEqualsFilter("cd_rota", codigoRota));					
			NeoObject aplicacaoTatico = FapFactory.neoObjectFactory("FAPAplicacaoTatico", "rotaSigma", sigmaRota);
			NeoObject aplicacaoPagamento = FapFactory.neoObjectFactory("FAPAplicacaoDoPagamento", "codigo", 11L);
			NeoObject wfFap = AdapterUtils.createNewEntityInstance("FAPAutorizacaoDePagamento");  
			EntityWrapper eWFap = new EntityWrapper(wfFap);

			// Sigla da regional está no início do nome do tático
			nomeRota = fornecedor.getNomeRota();			 
			regional = nomeRota.substring(0,3);
			
			// Solicitante  e Aprovador de acordo com a regional responsável pelo Tático
			solicitante = FapUtils.getSolicitante(regional);			
			aprovador = FapUtils.getAprovador(regional);

			//Campos obrigatórios na atividade Solicitar Orçamento
			eWFap.findField("solicitanteOrcamento").setValue(solicitante);
			eWFap.findField("rotaSigma").setValue(aplicacaoTatico);
			eWFap.findField("aplicacaoPagamento").setValue(aplicacaoPagamento);

			//Preenche o aprovador da FAP
			eWFap.findField("aprovador").setValue(aprovador);

			// Controla a competência do pagamento que será sempre referente ao mês anterior
			GregorianCalendar competencia  = new GregorianCalendar();
			competencia.add(Calendar.MONTH, -1);
			competencia.set(Calendar.DAY_OF_MONTH, 1);
			competencia.set(Calendar.HOUR_OF_DAY, 0);
			competencia.set(Calendar.MINUTE, 0);
			competencia.set(Calendar.SECOND, 0);
			competencia.set(Calendar.MILLISECOND, 0);
			eWFap.findField("competenciaPagamento").setValue(competencia);
			
			//Process model
			final ProcessModel pm = PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "F001 - FAP - AutorizacaoDePagamento"));

			PersistEngine.persist(wfFap);

			// Inicia a FAP
			log.warn("ROTINA ABRE FAP TATICO - Abrindo tarefa para a rota " + fornecedor.getCodigoRota() + " - " + nomeRota);
			String code = null;
			code = OrsegupsWorkflowHelper.iniciaProcesso(pm, wfFap, false, solicitante, true, null);

			log.warn("ROTINA ABRE FAP TATICO - Aberta tarefa para a rota " + fornecedor.getCodigoRota() + " - " + nomeRota +". Código: " + code);

			if(NeoUtils.safeIsNotNull(code)) {
			    abertas = abertas + 1;
			    fornecedor.setCodigoTarefa(code);
			    // Com a inserção na tabela de controle, não haverá abertura de tarefas em duplicidade (Rota x Compentecia)
			    NeoObject controleDeAbertura = AdapterUtils.createNewEntityInstance("FAPTaticoControleAberturaTarefaViaJob");
			    EntityWrapper ewControleDeAbertura = new EntityWrapper(controleDeAbertura);
			    ewControleDeAbertura.findField("codigoRota").setValue(fornecedor.getCodigoRota());
			    ewControleDeAbertura.findField("dataAbertura").setValue(new GregorianCalendar());
			    ewControleDeAbertura.findField("competencia").setValue(competencia);
			    
			    PersistEngine.persist(controleDeAbertura);
			    
			    try {
				log.warn("ROTINA ABRE FAP TATICO - Avançando a primeira atividade da tarefa " + code);
				FapUtils.avancaAtividade(code); 
				log.warn("ROTINA ABRE FAP TATICO - Tarefa " + code + " avançada com sucesso.");				  
			    } catch (NaoEncontradoException e) {
				log.warn("ROTINA ABRE FAP TATICO - Tarefa " + code + " não pode ser avançada.");			
				List<String> listaErros = new ArrayList<>();
				listaErros.add(e.getMessage());
				if(NeoUtils.safeIsNotNull(listaErros)){
				    fornecedor.setListaErros(listaErros);
				    listaNaoAvancouAtividade.add(fornecedor);
				}
			    } catch (WorkflowException e) {
				log.warn("ROTINA ABRE FAP TATICO - Tarefa " + code + " não pode ser avançada.");
				List<String> listaErros = FapUtils.getErrosEform(e);
				if(NeoUtils.safeIsNotNull(listaErros)){
				    fornecedor.setListaErros(listaErros);
				    listaNaoAvancouAtividade.add(fornecedor);
				}				    
			    } catch (Exception e){
				log.warn("ROTINA ABRE FAP TATICO - Tarefa " + code + " não pode ser avançada. Rota: " + nomeRota);
				e.printStackTrace();
				String erro = "Erro desconhecido ao avançar a atividade Finalizar Orçamento. Favor entrar em contato com a TI.";
				List<String> listaErros = new ArrayList<>();
				listaErros.add(erro);
				if(NeoUtils.safeIsNotNull(listaErros)) {
				    fornecedor.setListaErros(listaErros);
				    listaNaoGerouTarefa.add(fornecedor);
				}
			    }
			} else {
			    log.warn("ROTINA ABRE FAP TATICO - Não foi possível iniciar uma tarefa para a rota " + nomeRota);
			    listaNaoGerouTarefa.add(fornecedor);			    
			} 

		    } catch (NaoEncontradoException e) {
			log.warn("ROTINA ABRE FAP TATICO - Problema durante a execução da rotina, não chegou a abrir tarefa. Rota: " + nomeRota);
			String erro = e.getMessage();
			List<String> listaErros = new ArrayList<>();
			listaErros.add(erro);		
			if(NeoUtils.safeIsNotNull(listaErros)) {
			    fornecedor.setListaErros(listaErros);
			    listaNaoGerouTarefa.add(fornecedor);
			}
		    } catch (WorkflowException e) {
			log.warn("ROTINA ABRE FAP TATICO - Problema durante a execução da rotina, não chegou a abrir tarefa. Rota: " + nomeRota);
			List<String> listaErros = new ArrayList<>();
			listaErros.add(e.getMessage());
			if(NeoUtils.safeIsNotNull(listaErros)) {
			    fornecedor.setListaErros(listaErros);
			    listaNaoGerouTarefa.add(fornecedor);
			}
		    } catch (Exception e) {
			log.warn("ROTINA ABRE FAP TATICO - Problema durante a execução da rotina, erro durante a abertura da tarefa. Rota: " + nomeRota);
			e.printStackTrace();
			String erro = "Erro desconhecido ao abrir tarefa para esta rota. Favor entrar em contato com a TI.";
			List<String> listaErros = new ArrayList<>();
			listaErros.add(erro);
			if(NeoUtils.safeIsNotNull(listaErros)) {
			    fornecedor.setListaErros(listaErros);
			    listaNaoGerouTarefa.add(fornecedor);
			}
		    }	   
		}		    
	    }
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	    e.printStackTrace();
	} finally {
	    // Tratar o que não foi gerado ou não avançou atividade
	    try {
		FapUtils.naoGerouTarefaTaticos(listaNaoGerouTarefa); 
		FapUtils.naoAvancouAtividadeTaticos(listaNaoAvancouAtividade);
		FapUtils.enviaResumoJobTatico(totalTarefas,abertas, listaNaoGerouTarefa.size(),listaNaoAvancouAtividade.size());
		System.out.println("FIM DO JOB:" + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm"));
	    } catch (Exception e2) {
		// TODO: handle exception
	    }
	}
    }
}
