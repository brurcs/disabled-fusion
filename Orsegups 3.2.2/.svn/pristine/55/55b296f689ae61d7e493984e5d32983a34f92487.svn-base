package com.neomind.fusion.custom.orsegups.mobile.engine;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.utils.MobileUtils;
import com.neomind.fusion.custom.orsegups.mobile.vo.Inspecao;
import com.neomind.fusion.custom.orsegups.mobile.vo.Resposta;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;

/**
 * 
 * @author rejane.gomes
 * 
 */
public class SaveInspection
{
	private static final Log log = LogFactory.getLog(SaveInspection.class);
	private NeoObject entity;

	/*
	 * (non-Javadoc)
	 * @see com.neomind.fusion.custom.casvig.mobile.xml.entity.MobileXmlInterface#setInstance(Document
	 * document)
	 */
	public List<NeoObject> saveInspection(Inspecao inspection) throws Exception
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		System.out.println("["+key+"][INSP] Iniciando salvamento da inspeção");
		List<NeoObject> entidadesProcesso = new ArrayList<NeoObject>();

		List<NeoObject> respostasOK = new ArrayList<NeoObject>();
		List<NeoObject> respostasNOK = new ArrayList<NeoObject>();
		for (Resposta resposta : inspection.getRespostas())
		{
			NeoObject respostaNeo = AdapterUtils.createNewEntityInstance("Resposta");
			PersistEngine.persist(respostaNeo);
			EntityWrapper wrapperResposta = new EntityWrapper(respostaNeo);

			//TODO encontrar a pergunta pelo assunto, pergunta e pesquisa
			wrapperResposta.setValue("descperg", resposta.getPergunta());
			
			wrapperResposta.setValue("assuntoId", NeoUtils.safeLong( String.valueOf(resposta.getAssunto_id()) ));
			wrapperResposta.setValue("pesquisaId", NeoUtils.safeLong( String.valueOf(resposta.getPesquisa_id()) ));
			wrapperResposta.setValue("perguntaId", NeoUtils.safeLong( String.valueOf(resposta.getPergunta_id()) ));
			
			if(resposta.getImagemInspecao() != null && !resposta.getImagemInspecao().equals("")){
				System.out.println("[DBUG IMG]Imagem encontrada....");
				String img64 =  resposta.getImagemInspecao();
				byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(img64);

				BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imageBytes));
				String nomeImg = "Inspecao_"+new GregorianCalendar().getTimeInMillis()+".jpg";
				File imageFile = new File(nomeImg);
				ImageIO.write(bufferedImage, "png", imageFile);
				NeoFile anexo = null;
				anexo = OrsegupsUtils.criaNeoFile(imageFile);
				wrapperResposta.setValue("imgResposta", anexo);
			}
			
			System.out.println("["+key+"][INSP] " + resposta.getPergunta() + "=>" + resposta.getResposta());
			wrapperResposta.setValue("respperg", retornaTextoBooleano(isNaoConforme(resposta))
					+ " - (OBS) - " + NeoUtils.safeOutputString(resposta.getObservacao()));

			// valor logical
			wrapperResposta.setValue("codeperg", new Long(resposta.getId()));

			if (isNaoConforme(resposta))
			{
				respostasOK.add(respostaNeo);
			}
			else
			{
				respostasNOK.add(respostaNeo);
			}
		}

		if (!respostasNOK.isEmpty())
		{
			System.out.println("["+key+"][INSP] Respostas NOK para abrir tarefa, prosseguindo com a abertura das tarefas");
			for (NeoObject respostaNOK : respostasNOK)
			{
				sincDataInspection(inspection, entidadesProcesso, respostasOK, respostasNOK, respostaNOK);
			}
		}
		else
		{
			System.out.println("["+key+"][INSP] Não há respostas NOK");
			NeoObject resposta = respostasOK.get(0);
			sincDataInspection(inspection, entidadesProcesso, respostasOK, respostasNOK, resposta);
		}
		System.out.println("["+key+"][INSP] Finalizando salvamento da inspeção");
		return entidadesProcesso;
	}

	private void finishReinspectionActivity(Inspecao inspection, Boolean notConform)
	{
		CasvigMobilePool mobilepool = null;
		//Ação reinsção
		Activity activity = null;
		Task task = null;
		List<CasvigMobilePool> mobilePool = (List<CasvigMobilePool>) PersistEngine
				.getObjects(CasvigMobilePool.class);
		for (CasvigMobilePool pool : mobilePool)
		{
			if (pool.getTask().getNeoId() == NeoUtils.safeLong(inspection.getTaskId()))
			{
				// salva a task e activity
				activity = pool.getActivity();
				task = pool.getTask();
				mobilepool = pool;

				// remover identificador da reinspe��o
				break;
			}
		}

		if (task == null)
		{
			log.error("Task not found in CasvigMobilePool");
			return;
		}

		if (task != null)
		{
			
			// Instancia o E-Form IM_Inspetoria
			NeoObject inspetoriaObj = task.getProcess().getEntity();
			EntityWrapper inspetoriaWrapper = new EntityWrapper(inspetoriaObj);

			if (mobilepool.getIsReinspection())
			{
				inspetoriaWrapper.setValue("gerouNaoConformidade", new Boolean(notConform));
				inspetoriaWrapper.setValue("dataEnvioReinspecao", new GregorianCalendar());
				PersistEngine.persist(inspetoriaObj);
				log.info("Persist new reinspection" + ((notConform) ? " -> no conformity " : " - OK")
						+ " - " + inspection.getPosto());
			}
			if(mobilepool.getIsEfficacy())
			{
				inspetoriaWrapper.setValue("gerouReincidencia", new Boolean(notConform));
				inspetoriaWrapper.setValue("dataEnvioEficacia", new GregorianCalendar());
				PersistEngine.persist(inspetoriaObj);
				log.info("Persist new Efficacy" + ((notConform) ? " -> no efficacy " : " - OK")
						+ " - " + inspection.getPosto());
			}
		}

		// continua workflow
		if (task != null && activity != null)
		{
			OrsegupsWorkflowHelper.finishTaskByActivity(activity);
			
			if (mobilepool != null)
				PersistEngine.removeById(mobilepool.getNeoId());
		}
	}

	private String retornaTextoBooleano(Boolean b)
	{
		if (b)
			return "Sim";
		else
			return "Não";
	}

	private Boolean isNaoConforme(Resposta resposta)
	{
		return resposta.getResposta() != null && !resposta.getResposta().equalsIgnoreCase("Não");
	}

	private void sincDataInspection(Inspecao inspection, List<NeoObject> entidadesProcesso,
			List<NeoObject> respostasOK, List<NeoObject> respostasNOK, NeoObject resposta) throws Exception
	{
		NeoObject pesquisaresultado = null;
		if (inspection.getIsReinspecao())
		{
			Long taskId = NeoUtils.safeLong(inspection.getTaskId());
			Task task = (Task) PersistEngine.getNeoObject(taskId);
			if (task != null)
			{
				NeoObject entity = task.getProcessEntity();
				EntityWrapper wrapper = new EntityWrapper(entity);
				pesquisaresultado = (NeoObject) wrapper.findValue("relatorioInspecao");
			}
			else
				return;
		}
		else
			pesquisaresultado = AdapterUtils.createNewEntityInstance("PesquisaResultado");

		PersistEngine.persist(pesquisaresultado);

		if (pesquisaresultado != null)
		{
			List<NeoObject> respnok = new ArrayList<NeoObject>();

			String resp = (String) new EntityWrapper(resposta).findValue("respperg");
			if (!resp.startsWith("Sim"))
			{
				respnok.add(resposta);
			}

			entidadesProcesso.add(pesquisaresultado);
			boolean notConform = !respostasNOK.isEmpty();
			EntityWrapper wrapperPesquisaResultado = new EntityWrapper(pesquisaresultado);

			salvaInspecao(inspection, wrapperPesquisaResultado);
			String token = inspection.getToken();

			NeoUser inspetor = MobileUtils.getUserFromToken(token);
			
			
			// encontra a pergunta correta registrada na resposta
			QLGroupFilter gpf = new QLGroupFilter("AND");
			gpf.addFilter(new QLRawFilter("neoId="+ new EntityWrapper(resposta).findValue("perguntaId") + ""));
			NeoObject pergunta = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("Pergunta"), gpf);
			System.out.println( "pergunta: " + new EntityWrapper(resposta).findValue("descperg") );
			
			// pega o departamento respons�vel
			NeoObject tipoInspecao = pergunta != null ? (NeoObject) new EntityWrapper(pergunta)
					.getValue("departamento") : null;

			for (NeoObject neoObject : respostasOK)
			{
				wrapperPesquisaResultado.findField("RespostasOK").addValue(neoObject);
			}

			wrapperPesquisaResultado.findField("neoidResposta").setValue(respnok);

			//	Long codePesq = Long.valueOf(inspection.getPesquisa());
			//	NeoObject pesquisa = getPesquisa(codePesq);

			//		long neoIdPergunta = NeoUtils.safeLong(String.valueOf(inspection.getId()));
			//		NeoObject pergunta = PersistEngine.getNeoObject(neoIdPergunta);
			// pega o departamento respons�vel
			//	NeoObject tipoInspecao = (NeoObject) new EntityWrapper(pergunta).getValue("departamento");

			if (!inspection.getIsReinspecao())
				startWorkflow(pesquisaresultado, null, null, tipoInspecao, notConform, inspetor);
			else
				finishReinspectionActivity(inspection, respostasNOK.size() > 0);
		}
	}

	private NeoObject getPesquisa(Long codePesq)
	{
		return PersistEngine.getNeoObject(codePesq);
	}

	private void salvaInspecao(Inspecao inspection, EntityWrapper wrapperPesquisaResultado)
	{

		String token = inspection.getToken();

		NeoUser inspetor = MobileUtils.getUserFromToken(token);
		if (inspetor != null)
			wrapperPesquisaResultado.setValue("nomeinspetor", inspetor.getFullName());

		wrapperPesquisaResultado.setValue("mobileNeoId", new BigDecimal(inspection.getId()));
		// valor cod_pesquisa
		//	Long codePesq = Long.valueOf(inspection.getPesquisa());
		//	NeoObject pesquisa = getPesquisa(codePesq);

		// valor cod_postoTrabalho - Neomind entregou gravando nome da lotação
		wrapperPesquisaResultado.setValue("codposto", inspection.getPosto());
		
		// valor de centro de custo do posto
		wrapperPesquisaResultado.setValue("codCcu", inspection.getCodCcu());

		// valor nome_empresa
		wrapperPesquisaResultado.setValue("nomeempresa", inspection.getEmpresa());

		// valor nome_cliente
		wrapperPesquisaResultado.setValue("nomecliente", inspection.getCliente());

		// valor nome_lotacao
		wrapperPesquisaResultado.setValue("nomelotacao", inspection.getLotacao());

		// valor nome_postoTrabalho
		String postoTrabalho = inspection.getPosto();
		wrapperPesquisaResultado.setValue("nomeposto", postoTrabalho);

		// valor gestor_cliente
		wrapperPesquisaResultado.setValue("gestorcliente", inspection.getGestor());

		// valor nome_colaborador
		wrapperPesquisaResultado.setValue("colaboradorentrevistado", inspection.getColaborador());

		// valor funcao_colaborador
		wrapperPesquisaResultado.setValue("funccolaborador", inspection.getFuncaoColaborador());
		
		wrapperPesquisaResultado.setValue("siglaRegional", inspection.getSiglaRegional());

		// valor nome_inspetor
		//	String inspetor = new String(elemInsp.getAttribute("nome_inspetor"));
		//	objWrapper.setValue("nomeinspetor", elemInsp.getAttribute("nome_inspetor"));
		//	groupFilter.addFilter(MobileUtils.createFilter("nomeinspetor",
		//			elemInsp.getAttribute("nome_inspetor")));

		// exemplo: aaaa mm dd
		String data = inspection.getDataInspecao();
		if (data != null)
		{

			String[] split = data.split("-");

			Integer dia = new Integer(split[0]);

			Integer mes = new Integer(split[1]) - 1;

			Integer ano = new Integer(split[2]);

			GregorianCalendar calendar = new GregorianCalendar(ano, mes, dia);
			wrapperPesquisaResultado.setValue("datainspecao", calendar);

			// valor descrisao_pesquisa
			wrapperPesquisaResultado.setValue("descpesquisa", inspection.getPesquisa());

			// valor observacao
			wrapperPesquisaResultado.setValue("Observacao", truncaCampo( inspection.getObs() , 255) );
		}
	}
	
	public static String truncaCampo(String campo, int maxSize) {
		String retorno = "";
		if (campo != null && !campo.isEmpty()){
			retorno = campo.substring(0, (campo.length()>maxSize? 79:campo.length()-1));
		}
		
		return retorno;
	}

	public void startWorkflow(NeoObject newObj, String inspetor, NeoObject pesquisa,
			NeoObject tipoInspecao, boolean notConform, NeoUser requester) throws Exception
	{
		try{
			Long key = GregorianCalendar.getInstance().getTimeInMillis();
			System.out.println("[INSPETORIA MOBILE] ["+key+"] - SaveInspection - startWorkFlow - requester:" + requester == null? "requester Nulo" : requester.getCode());
			// Instancia o E-Form IM_Inspetoria
			InstantiableEntityInfo entityInfo = (InstantiableEntityInfo) EntityRegister.getInstance()
					.getCache().getByType("IMInspetoria");
			NeoObject inspetoriaObj = entityInfo.createNewInstance();
			PersistEngine.persist(inspetoriaObj);
			EntityWrapper inspetoriaWrapper = new EntityWrapper(inspetoriaObj);
			inspetoriaWrapper.setValue("relatorioInspecao", newObj);
			inspetoriaWrapper.setValue("tipoInspecao", tipoInspecao);
			inspetoriaWrapper.setValue("gerouInconsistencia", new Boolean(notConform));
			//	inspetoriaWrapper.setValue("PesquisaMobile", pesquisa);
	
			PersistEngine.persist(inspetoriaObj);
			System.out.println("[INSPETORIA MOBILE] ["+key+"] - SaveInspection - startWorkFlow - inspetoriaObj neoId " + inspetoriaWrapper.findValue("neoId"));
	
			final ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("Name", "Q003 - Inspetoria Mobile"));
			
			/**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
			 * @date 12/03/2015
			 */
			
			WorkflowService.startProcess(processModel, inspetoriaObj, false, requester);
			
			/**
			 * FIM ALTERAÇÕES - NEOMIND
			 */
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

}
