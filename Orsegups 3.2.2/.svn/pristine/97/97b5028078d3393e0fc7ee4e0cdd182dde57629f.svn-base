package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class EmailDeliveryAnulacaoPonto implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliveryAnulacaoPonto.class);

    @SuppressWarnings({ "static-access", "unchecked", "deprecation" })
    @Override
    public void execute(CustomJobContext arg0) {

	int adicionados = 0;
	log.warn("E-Mail Anulação de Ponto Inicio execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailAutomaticoDesvioDeHabito");
	InstantiableEntityInfo ExcecoesEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CadastroExcecoesEmailsDesvioDeHabito");
	InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailDelivery");

	Connection conn = null;
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	String ultimaExecucaoRotina = OrsegupsEmailUtils.ultimaExecucaoRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_ANULACAO_PONTO);

	try {

	    strSigma.append(" 	 SELECT c.OBSERVACAO, c.CGCCPF,c.FANTASIA, c.RAZAO, c.ID_CENTRAL, c.PARTICAO, c.ENDERECO, cid.NOME AS NM_CIDADE, ");
	    strSigma.append(" 	 bai.NOME AS NM_BAIRRO, h.DT_FECHAMENTO, c.EMAILRESP, c.CD_CLIENTE, ");
	    strSigma.append(" 	 hfe.NM_FRASE_EVENTO as NM_FRASE_EVENTO, ");
	    strSigma.append(" 	 h.TX_OBSERVACAO_FECHAMENTO, h.CD_HISTORICO_SEM_CONTROLE AS CD_HISTORICO, "); 
	    strSigma.append(" 	 h.CD_EVENTO, c.CD_CLIENTE,  c.NU_LATITUDE, c.NU_LONGITUDE, h.DT_RECEBIDO, c.ID_EMPRESA "); 
	    strSigma.append(" 	 FROM HISTORICO_SEM_CONTROLE H WITH(NOLOCK) ");
	    strSigma.append(" 	 INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO ");  
	    strSigma.append(" 	 LEFT JOIN dbCIDADE cid WITH(NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE ");
	    strSigma.append(" 	 LEFT JOIN dbBAIRRO bai WITH(NOLOCK) ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO ");
	    strSigma.append(" 	 WHERE C.TP_PESSOA != 2 ");
	    strSigma.append(" 	 AND C.CTRL_CENTRAL = 1 AND C.FG_ATIVO = 1 ");
	    strSigma.append(" 	 AND EXISTS (SELECT RAE.CD_EVENTO FROM [FSOODB04\\SQL02].TIDB.DBO.RAT_ANULACAO_EVENTOS RAE WHERE RAE.CD_EVENTO = H.CD_EVENTO) ");
	    strSigma.append(" 	 AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_EmailAutomaticoDesvioDeHabito em where em.historico = h.CD_HISTORICO_SEM_CONTROLE) ");  
	    strSigma.append(" 	 AND ( DT_RECEBIDO > '" + ultimaExecucaoRotina + "' )  ");

	    conn = PersistEngine.getConnection("SIGMA90");
        
	    pstm = conn.prepareStatement(strSigma.toString());
	    OrsegupsEmailUtils.inserirFimRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_ANULACAO_PONTO);
	    rs = pstm.executeQuery();
        
	    String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
	    // String calendarDM = NeoDateUtils.safeDateFormat(new
	    // GregorianCalendar(), "d/M");

	    while (rs.next()) {

	    int codigoClienteSigma = rs.getInt("CD_CLIENTE");
	    String usuarioArmacao = getUsuarioArmacao(codigoClienteSigma); 
		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));
		String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");
		if (!clienteComExcecao) {

            String fantasia = rs.getString("FANTASIA");
		    String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
		    String particao = rs.getString("PARTICAO");
		    String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
		    String cidade = (rs.getString("NM_CIDADE") == null ? "" : rs.getString("NM_CIDADE"));
		    String bairro = (rs.getString("NM_BAIRRO") == null ? "" : rs.getString("NM_BAIRRO"));
		    Timestamp dtViaturaNoLocal = rs.getTimestamp("DT_FECHAMENTO");

		    String nomeFraseEvento = rs.getString("NM_FRASE_EVENTO");
		    String txObsevacaoFechamento = rs.getString("TX_OBSERVACAO_FECHAMENTO");
		    String evento = rs.getString("CD_EVENTO");
		    String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
		    String cdCliente = rs.getString("CD_CLIENTE");
		    String dataAtendimento = NeoDateUtils.safeDateFormat(dtViaturaNoLocal, "dd/MM/yyyy");
		    String horaAtendimento = rs.getString("DT_FECHAMENTO").substring(11, 16);
		    String historico = rs.getString("CD_HISTORICO");
		    Timestamp dtRecebido = rs.getTimestamp("DT_RECEBIDO");
		    // //String dtRecebidoStr =
		    // NeoDateUtils.safeDateFormat(dtRecebido,
		    // "dd/MM/yyyy HH:mm:ss");
		    // String central = rs.getString("ID_CENTRAL");
		    int empresa = rs.getInt("ID_EMPRESA");
		    log.warn("[Anulação de Ponto] fantasia:" + fantasia + ", razao:" + razao + ", email" + email);

		   

		    boolean flagAtualizadoNAC = observacao.contains("#AC");

		    List<String> emailClie = OrsegupsEmailUtils.validarEmail(email, cdCliente, flagAtualizadoNAC);
		    if ((emailClie != null) && (!emailClie.isEmpty())) {
			log.warn("[Anulação de Ponto] email do cliente validado");

			final String tipo = OrsegupsEmailUtils.TIPO_RAT_ANULACAO_PONTO;

			Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

			String pasta = null;
			String remetente = null;
			String grupo = null;

			if (params != null) {

			    pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
			    remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
			    grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
			    String neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);
			    for (String emailFor : emailClie) {

				StringBuilder noUserMsg = new StringBuilder();
				List<NeoObject> neoObjects = null;
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("email", emailFor));
				groupFilter.addFilter(new QLEqualsFilter("tratarXXX2", Boolean.TRUE));

				neoObjects = PersistEngine.getObjects(ExcecoesEmail.getEntityClass(), groupFilter);

				if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty())) {

				    noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta));

				    noUserMsg.append("\n <table width=\"600\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr>");

				    noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    
				    
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr>");
//				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
					    
				    noUserMsg.append("\n <tr> ");
				    noUserMsg.append("\n <td colspan=\"6\" style=\"padding:5px;\"><h2 style=\"border-bottom:1px solid #CCC;border-top:1px solid #CCC;margin:0px 15px 0px 5px;padding-bottom:10px;padding-top:10px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">ALARME ATIVADO COM SENSORES ANULADOS</td>");
				    noUserMsg.append("\n </tr>");
				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"6\">");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + txObsevacaoFechamento + "</p></td>");
					noUserMsg.append("\n </tr><br/>");
				    }
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td colspan=\"4\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:14px;\">");
				    noUserMsg.append("\n <img src=\"https://maps.googleapis.com/maps/api/staticmap?center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
					    + "&zoom=19&size=300x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
					    + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "\" width=\"300\" height=\"300\" alt=\"\"/></p><p style=\"font-family: 'Verdana';font-weight:normal;font-size:8px;width: 300px;\"></p></td>");
				    noUserMsg.append("\n <td width=\"10%\"><p><br>");
				    noUserMsg.append("\n </p></td>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Prezado Cliente, </br> ");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Viemos atrav&eacute;s deste informar que seu sistema de alarme foi armado com sensores anulados conforme descrito abaixo. </br> ");				    
				    noUserMsg.append("\n <td width=\"51%\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Local </br> <strong>" + razao + "</strong></br>");
				    noUserMsg.append("\n <strong>" + endereco + "</strong></br>");
				    noUserMsg.append("\n <strong>" + bairro + "</strong></br>");
				    noUserMsg.append("\n <strong>" + cidade + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Data <br> <strong>" + dataAtendimento + "</strong></br>");
				    noUserMsg.append("\n Hora  </br> <strong>" + horaAtendimento + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Evento </br> ");
				    noUserMsg.append("\n <strong>" + nomeFraseEvento + "</strong></p> ");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Usu&aacute;rio Arma&ccedil;&atilde;o </br> ");
				    noUserMsg.append("\n <strong>" + usuarioArmacao + "</strong></p> ");				  
				    noUserMsg.append("\n </tr>");				    
				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n </table>");

				    noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, email, remetente));
				    // ADD na lista de e-mail

				    NeoObject emailHis = infoHis.createNewInstance();
				    EntityWrapper emailHisWp = new EntityWrapper(emailHis);
				    adicionados = adicionados + 1;
				    emailHisWp.findField("fantasia").setValue(fantasia);
				    emailHisWp.findField("razao").setValue(razao);
				    emailHisWp.findField("particao").setValue(particao);
				    emailHisWp.findField("endereco").setValue(endereco);
				    emailHisWp.findField("cidade").setValue(cidade);
				    emailHisWp.findField("bairro").setValue(bairro);
				    emailHisWp.findField("dataFechamento").setValue(dataAtendimento);
				    emailHisWp.findField("horaFechamento").setValue(horaAtendimento);
				    emailHisWp.findField("evento").setValue(evento);
				    emailHisWp.findField("enviadoPara").setValue(emailClie.toString());
				    emailHisWp.findField("nomeFraseEvento").setValue(nomeFraseEvento);
				    emailHisWp.findField("historico").setValue(historico);
				    // PersistEngine.persist(emailHis);

				    String assunto = "Relatório de Atendimento Anulação de Ponto - " + fantasia;
				    //TESTE E-MAIL UTILIZANDO RECURSO FUSION
//				    List<String> emailClieStr = new ArrayList<String>();
//				    emailClieStr.add("diogo.silva@orsegups.com.br");
//				    enviaEmailCliente(emailClieStr, noUserMsg, assunto);

				    GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
				    NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
				    EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
				    emailEnvioWp.findField("de").setValue("cm.rat" + remetente);
				    emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br;copia@orsegups.com.br");
				    emailEnvioWp.findField("assunto").setValue("Relatório de Atendimento Anulação de Ponto - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
				    emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
				    emailEnvioWp.findField("datCad").setValue(dataCad);
				    PersistEngine.persist(emaiEnvio);

				    log.warn("Cliente [Anulação de Ponto]: " + fantasia + ", E-mail: " + emailFor);
				    
				  //Regra para gerar os pushs
				    ObjRatMobile objRat = new ObjRatMobile();
					//
					/**
					 * Tipos de Rats para mobile: 
					 * RATATENDIMENTOTATICO(0L, "Atendimento Tático"), 
					 * RATDESVIODEHABITO(1L, "Desvio de hábito");
					 */
					objRat.setTipRat(2L);
					//
					objRat.setInformativo(nomeFraseEvento);
					objRat.setEvento(nomeFraseEvento);
					objRat.setResultado("");
					objRat.setHashId("Relatório de Atendimento Anulação de Ponto - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
					objRat.setAtendidoPor("");
					objRat.setLocal(razao+" - "+endereco+" - "+bairro+" - "+cidade);
					objRat.setDataAtendimento(dataAtendimento);
					objRat.setHoraAtendimento(horaAtendimento);
					
					if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
						objRat.setObservacao(txObsevacaoFechamento);	
					}else{
						objRat.setObservacao("");
					}
					objRat.setEmpRat(grupo);
					objRat.setNeoId(neoId);
					objRat.setLnkFotoAit("");
					objRat.setLnkFotoLocal("https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyDie-lwDjXfRv0diccYyQtC8ZuGt-P4scs&center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
						    + "&zoom=19&size=300x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
						    + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE"));
					if(rs.getString("CGCCPF") != null && !rs.getString("CGCCPF").equals("")){
						String cgcCpf = rs.getString("CGCCPF");
						cgcCpf = cgcCpf.replaceAll("([.\\-/])", "");
						if(!cgcCpf.equals("")){
							cgcCpf = cgcCpf.trim();
							objRat.setCgcCpf(Long.parseLong(cgcCpf));
							IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();							
							integracao.inserirInformacoesPush(objRat);
						}else{
							objRat.setCgcCpf(0L);	
						}
					}else{
						objRat.setCgcCpf(0L);
					}					
				}
			    }
			   
			    OrsegupsUtils.sendEmailWhatsAppRAT(Long.valueOf(cdCliente), "Relatório de Atendimento Anulação de Ponto - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento, endereco+" - "+bairro+" - "+cidade, dataAtendimento + " " + horaAtendimento);
			} else {
			    if (!empresasNotificadas.contains(empresa)) {
				
			    	OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
				empresasNotificadas.add(empresa);
			    }
			}

		    } else {
			log.warn("[Anulação de Ponto] Email do cliente invalido vou vazio ");
		    }
		}
	    }
	    log.warn("E-Mail Anulação de Ponto Fim execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	} catch (Exception e) {

	    log.error("E-Mail Anulação de Ponto erro no processamento:");
	    System.out.println("[" + key + "] E-Mail Anulação de Ponto erro no processamento: " + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    @SuppressWarnings("unused")
    private void enviaEmailCliente(List<String> emailCliente, StringBuilder msn, String assunto) {

	try {

	    MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

	    MailSettings mailClone = new MailSettings();
	    mailClone.setMinutesInterval(settings.getMinutesInterval());
	    mailClone.setFromEMail(settings.getFromEMail());
	    mailClone.setFromName(settings.getFromName());
	    mailClone.setRenderServer(settings.getRenderServer());
	    mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
	    mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
	    mailClone.setSmtpSettings(settings.getSmtpSettings());
	    mailClone.setPort(settings.getPort());
	    mailClone.setSmtpServer(settings.getSmtpServer());
	    mailClone.setEnabled(settings.isEnabled());

	    mailClone.setFromEMail("cm.rat@orsegups.com.br");
	    mailClone.setFromName("Orsegups Participações S.A.");

	    if (mailClone != null) {

		HtmlEmail noUserEmail = new HtmlEmail();
		StringBuilder noUserMsg = new StringBuilder();
		noUserEmail.setCharset("UTF-8");

		
		for (String email : emailCliente) {
		    if (email != null && !email.isEmpty() && email.contains(";")) {
			String[] arrayEmail = email.split(";");

			for (String emailStr : arrayEmail) {
			    if (validEmail(emailStr))
				noUserEmail.addTo(emailStr);
			}
		    } else {
			if (validEmail(email))
			    noUserEmail.addTo(email);
		    }
		}
		noUserEmail.addTo("diogo.silva@orsegups.com.br");
		noUserEmail.setSubject(assunto);

		noUserMsg.append(msn.toString());

		noUserEmail.setHtmlMsg(noUserMsg.toString());
		noUserEmail.setContent(noUserMsg.toString(), "text/html");
		mailClone.applyConfig(noUserEmail);

		noUserEmail.send();
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO ao enviar comunicado de férias para cliente - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	}

    }

    public boolean validEmail(String email) {
	Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
	Matcher m = p.matcher(email);
	if (m.find()) {
	    return true;
	} else {
	    return false;
	}
    }
    
    private String getUsuarioArmacao(int codigoClienteSigma) {
    	String usuario = null;
    	Connection conn = null;
    	PreparedStatement pstm = null;
    	ResultSet rs = null;
    	StringBuilder sql = new StringBuilder();
    	int cdCliente = codigoClienteSigma;

    	try {
			conn = PersistEngine.getConnection("SIGMA90");
			
			sql.append("");
			sql.append("SELECT TOP 1 A.NOME FROM HISTORICO_ARME H  ");
			sql.append("INNER JOIN dbACESSO A ON A.CD_CLIENTE = H.CD_CLIENTE ");
			sql.append("WHERE H.CD_CLIENTE = ? AND H.CD_CODE = 'ARM' AND H.NU_AUXILIAR != 0  ");
			sql.append("AND A.ID_ACESSO = H.NU_AUXILIAR "); 
			sql.append("ORDER BY H.DT_RECEBIDO DESC ");			
			
			pstm = conn.prepareStatement(sql.toString());
			pstm.setInt(1, cdCliente);
			
			rs = pstm.executeQuery();
			
			if(rs.next()){
				usuario = rs.getString("NOME");
			}
    		
		} catch (Exception e) {
			e.printStackTrace();
			return usuario = "";
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
    	
    	return usuario;
    	
    }


    public static List<String> dias(GregorianCalendar calendar) {
	List<String> cptList = null;
	if (NeoUtils.safeIsNotNull(calendar)) {
	    long milisecInicial = calendar.getTime().getTime();
	    long milisecFinal = new GregorianCalendar().getTime().getTime();
	    long dif = milisecFinal - milisecInicial;

	    long dias = (((dif / 1000) / 60) / 60) / 24;
	    cptList = new ArrayList<String>();
	    Calendar dia = Calendar.getInstance();
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
	    for (int i = 0; i <= dias; i++) {
		String diaStr = dateFormat.format(dia.getTime());
		cptList.add(diaStr);
		dia.add(Calendar.DAY_OF_MONTH, -1);
	    }

	}
	return cptList;
    }
}