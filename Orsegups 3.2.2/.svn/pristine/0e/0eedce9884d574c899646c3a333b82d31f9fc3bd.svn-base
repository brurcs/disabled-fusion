package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RSCEscalarNivelCategoriaAlteracaoBoletoNFEficacia implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCEscalarNivelCategoriaAlteracaoBoletoNFEficacia.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String erro = "Por favor, contatar o administrador do sistema!";

		try
		{
			RSCUtils rscUtils = new RSCUtils();
			GregorianCalendar prazoDeadLine = new GregorianCalendar();

			NeoPaper superiorResponsavelExecutor = null;

			if (origin != null)
			{
				if (((Long) wrapper.findValue("etapaVerificacaoEficacia")) == 2L)
				{
					NeoUser responsavelExecutor = (NeoUser) wrapper.findValue("responsavelExecutor");
					wrapper.setValue("superiorResponsavelExecutor", rscUtils.retornaSuperiorResponsavelExecutor(responsavelExecutor));
					superiorResponsavelExecutor = rscUtils.retornaSuperiorResponsavelExecutor(responsavelExecutor);

					persisteUltimoUsuario(wrapper, superiorResponsavelExecutor);
				}
				else
				{
					superiorResponsavelExecutor = (NeoPaper) wrapper.findValue("superiorResponsavelExecutor");

					if (verificarPapelPresidente(wrapper, superiorResponsavelExecutor))
					{
						erro = "Tarefa já escalou para o último nível de hierarquia. Por favor, proceder com a finalização da tarefa!";
						throw new WorkflowException(erro);
					}

					NeoUser ultimoUsuario = (NeoUser) wrapper.findValue("ultimoUsuario");

					wrapper.setValue("superiorResponsavelExecutor", rscUtils.retornaSuperiorResponsavelExecutor(ultimoUsuario));
					superiorResponsavelExecutor = rscUtils.retornaSuperiorResponsavelExecutor(ultimoUsuario);

					persisteUltimoUsuario(wrapper, superiorResponsavelExecutor);
				}

				if(superiorResponsavelExecutor.getName().contains("Diretor") || superiorResponsavelExecutor.getName().contains("Presidente"))
				{
					wrapper.findField("souPessoaResponsavel").setValue(false);
					wrapper.findField("continuarRSC").setValue(true);

					if(superiorResponsavelExecutor.getName().contains("Diretor"))
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Atender Solicitação - Escalada Diretoria", new GregorianCalendar());
						wrapper.findField("prazoAtenderSolicitacaoEscaladaDiretoria").setValue(prazoDeadLine);
					}
					else
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Atender Solicitação - Escalada Presidência", new GregorianCalendar());
						wrapper.findField("prazoAtenderSolicitacaoEscaladaPresidencia").setValue(prazoDeadLine);
					}
				}
				else
				{
					prazoDeadLine = rscUtils.retornaPrazoDeadLine("Atender Solicitação - Escalada", new GregorianCalendar());
					wrapper.findField("prazoAtenderSolicitacaoEscalada").setValue(prazoDeadLine);
				}
				
				/*Pego meu último valor da minha lista de Registro de Atividades e altero o valor do campo prazo*/
				List<NeoObject> objLstRegistroAtividades = (List<NeoObject>) wrapper.findValue("RscRelatorioSolicitacaoCliente.registroAtividades");
				if (NeoUtils.safeIsNotNull(objLstRegistroAtividades))
				{
					NeoObject ultima = objLstRegistroAtividades.get(objLstRegistroAtividades.size() - 1);
					EntityWrapper ewUltima = new EntityWrapper(ultima);
					
					ewUltima.findField("prazo").setValue(prazoDeadLine);
				}
			}
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
	}

	private Boolean verificarPapelPresidente(EntityWrapper wrapper, NeoPaper superiorResponsavelExecutor)
	{

		for (NeoUser user : superiorResponsavelExecutor.getAllUsers())
		{

			if (user.getCode().equals("dilmoberger"))
			{
				return true;
			}
		}
		return false;
	}

	private void persisteUltimoUsuario(EntityWrapper wrapper, NeoPaper superiorResponsavelExecutor)
	{

		for (NeoUser user : superiorResponsavelExecutor.getAllUsers())
		{
			if (user != null)
			{
				wrapper.findField("ultimoUsuario").setValue(user);
			}
		}

	}

	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}
