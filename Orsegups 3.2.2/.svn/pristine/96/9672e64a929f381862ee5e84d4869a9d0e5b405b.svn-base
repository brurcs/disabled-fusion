package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesAgendamentoReciclagens implements CustomJobAdapter
{
	
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesAgendamentoReciclagens.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection("VETORH");

		GregorianCalendar dataInicio = new GregorianCalendar();
		dataInicio.add(Calendar.MONTH, +3);
		dataInicio.set(Calendar.DAY_OF_MONTH, 1);
		dataInicio.set(Calendar.HOUR_OF_DAY, 0);
		dataInicio.set(Calendar.MINUTE, 0);
		dataInicio.set(Calendar.SECOND, 0);
		dataInicio.set(Calendar.MILLISECOND, 0);

		GregorianCalendar dataFim = new GregorianCalendar();
		dataFim.add(Calendar.MONTH, +4);
		dataFim.set(Calendar.DAY_OF_MONTH, 1);
		dataFim.set(Calendar.HOUR_OF_DAY, 0);
		dataFim.set(Calendar.MINUTE, 0);
		dataFim.set(Calendar.SECOND, 0);
		dataFim.set(Calendar.MILLISECOND, 0);

		GregorianCalendar dataPrazo = (GregorianCalendar) dataInicio.clone();

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 

		try
		{
			System.out.println();
			sql.append(" Select fun.numemp, fun.numcad, fun.nomfun, fun.datadm, fun.numcpf, ");
			sql.append("		CASE WHEN cur.USU_DatRec = '1900-12-31' THEN DATEADD(yy,2,cur.USU_DatFor) ELSE DATEADD(yy,2,cur.USU_DatRec) END AS datven, ");
			sql.append(" 		usu_codccu, reg.USU_CodReg, reg.USU_NomReg, car.titred ");
			sql.append(" From USU_TCADCUR cur ");
			sql.append(" Inner Join R034FUN fun On fun.numemp = cur.USU_NumEmp And fun.tipcol = cur.USU_TipCol And fun.numcad = cur.USU_NumCad ");
			sql.append(" Inner Join R016ORN orn On orn.numloc = fun.numloc And orn.taborg = fun.taborg ");
			sql.append(" Inner Join R024CAR car On car.CodCar = fun.CodCar And car.EstCar = fun.estcar ");
			sql.append(" Inner Join USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg ");
			sql.append(" Where ((cur.USU_DatRec = '1900-12-31' And DATEADD(yy,2,cur.USU_DatFor) >= ? And DATEADD(yy,2,cur.USU_DatFor) < ?) Or  ");
			sql.append(" 		(cur.USU_DatRec <> '1900-12-31' And DATEADD(yy,2,cur.USU_DatRec) >= ? And DATEADD(yy,2,cur.USU_DatRec) < ?)) ");
			sql.append(" And fun.sitafa in (1,2,11,12,14,15,20,21,23,24,25,26,27,28,29,30,49,51,52,55,61,62,63,64,65,66,78,81,82,83,84,85,86,117,119,120,121) ");
			sql.append(" And car.titred Like 'VIG%' ");
			sql.append(" And NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaAgendamentoReciclagens ar WITH (NOLOCK) ");
			sql.append(" 			  	 WHERE ar.numcpf = fun.numcpf AND CAST(floor(CAST(ar.datven AS FLOAT)) AS datetime) = CAST(floor(CAST(datven AS FLOAT)) AS datetime)) ");
			sql.append(" Order By reg.USU_CodReg, fun.numemp, datven ");

			pstm = conn.prepareStatement(sql.toString());
			System.out.println(NeoUtils.safeDateFormat(dataInicio, "dd/MM/yyyy HH:mm:ss"));
			System.out.println(NeoUtils.safeDateFormat(dataFim, "dd/MM/yyyy HH:mm:ss"));
			pstm.setTimestamp(1, new Timestamp(dataInicio.getTimeInMillis()));
			pstm.setTimestamp(2, new Timestamp(dataFim.getTimeInMillis()));
			pstm.setTimestamp(3, new Timestamp(dataInicio.getTimeInMillis()));
			pstm.setTimestamp(4, new Timestamp(dataFim.getTimeInMillis()));
			rs = pstm.executeQuery();
			
			String descricaoMes = NeoUtils.safeDateFormat(dataInicio, "MMMMM");
			
			dataPrazo = OrsegupsUtils.getNextWorkDay(dataPrazo);
			dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
			dataPrazo.set(Calendar.MINUTE, 59);
			dataPrazo.set(Calendar.SECOND, 59);
			
			while (rs.next())
			{
				Long numemp = rs.getLong("numemp");
				Long numcad = rs.getLong("numcad");
				String nomfun = rs.getString("nomfun");
				Long numcpf = rs.getLong("numcpf");
				Long codreg = rs.getLong("USU_CodReg");
				String nomreg = rs.getString("USU_NomReg");
				Long codccu = rs.getLong("usu_codccu");
				String titcar = rs.getString("titred");

				GregorianCalendar datadm = new GregorianCalendar();
				datadm.setTime(rs.getDate("datadm"));

				GregorianCalendar datven = new GregorianCalendar();
				datven.setTime(rs.getDate("datven"));

				
				
				String solicitante = "luana.martins";
				String titulo = "Agendamento de Reciclagem - " + descricaoMes + " - " + nomreg + " - " + numemp + "/" + numcad + " - " + nomfun;
				
				String descricao = "";
				descricao = " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomfun + "<br>";
				descricao = descricao + " <strong>Cargo:</strong> " + titcar + "<br>";
				descricao = descricao + " <strong>Admissão:</strong> " + NeoUtils.safeDateFormat(datadm, "dd/MM/yyyy") + "<br>";
				descricao = descricao + " <strong>Vencimento:</strong> " + NeoUtils.safeDateFormat(datven, "dd/MM/yyyy") + "<br>";
				descricao = descricao + " <strong>Centro de Custo:</strong> " + codccu + "<br>";
				descricao = descricao + " <strong><u>Obs: Encaminhar o vigilante para reciclagem 30 dias antes do vencimento (no mínimo)..</u></strong><br>";
				
				NeoPaper responsavel = new NeoPaper();
				String executor = "";
				
				responsavel = OrsegupsUtils.getPapelAgendamentoReciclagem(codreg);
				
				if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
				{
					for (NeoUser user : responsavel.getUsers())
					{
						executor = user.getCode();
						break;
					}
				}
				else
				{
					continue;
				}
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", dataPrazo);
				
				InstantiableEntityInfo tarefaReciclagem = AdapterUtils.getInstantiableEntityInfo("TarefaAgendamentoReciclagens");
				NeoObject noAJ = tarefaReciclagem.createNewInstance();
				EntityWrapper ajWrapper = new EntityWrapper(noAJ);
				ajWrapper.findField("numcpf").setValue(numcpf);
				ajWrapper.findField("datven").setValue(datven);
				ajWrapper.findField("tarefa").setValue(tarefa);
				PersistEngine.persist(noAJ);
				log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens - CPF: "+numcpf+" Data Vencimento: "+NeoUtils.safeDateFormat(datven, "dd/MM/yyyy HH:mm:ss")+" Tarefa: "+tarefa+" Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				System.out.println("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens - CPF: "+numcpf+" Data Vencimento: "+NeoUtils.safeDateFormat(datven, "dd/MM/yyyy HH:mm:ss")+" Tarefa: "+tarefa+" Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
