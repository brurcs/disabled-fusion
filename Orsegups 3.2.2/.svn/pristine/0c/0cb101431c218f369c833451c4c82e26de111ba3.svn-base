package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;

/**
 * 
 * @author mateus.batista
 *
 */
@SuppressWarnings("deprecation")
public class FAPUtils {
    
    public static final String ERRO_USUARIO_NAO_ENCONTRADO = "Erro #1 - Usuário não encontrado.";
    public static final String ERRO_CONTA_SIGMA_NAO_ENCONTRADA = "Erro #2 Conta Sigma não encontrada.";
    public static final String ERRO_AVANCAR_PRIMEIRA_TAREFA = "Erro #3  Erro ao avançar a primeira tarefa";
    
    
    /**
     * Abre novo processo de FAP do tipo Instalaçaõ Alarme e CFTV - Mercado Privado
     * @param codigoUsuario Código do usuário a ser o solicitante da FAP. Campo <code>code</code> do objeto <code>NeoUser</code> ou tabela <code>SecurityEntity</code>
     * @param contaSigma Número do código do cliente <code>SIGMA</code>. Campo <code> CD_CLIENTE </code>
     * @param relatoServico Relato do serviço a ser descrito na FAP.
     * @return Retorna o código do processo aberto ou um dos erros abaixo: <br>
     * {@link #ERRO_USUARIO_NAO_ENCONTRADO} <br>
     * {@link #ERRO_CONTA_SIGMA_NAO_ENCONTRADA} <br>
     * {@link #ERRO_AVANCAR_PRIMEIRA_TAREFA} <br>
     */
    public static String abrirFAPInstalacaoMercadoPrivado(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAP(codigoUsuario, contaSigma, relatoServico, 5L);
		
    }
    
    /**
     * Abre novo processo de FAP do tipo Instalaçaõ Alarme e CFTV - Mercado Publico
     * @param codigoUsuario Código do usuário a ser o solicitante da FAP. Campo <code>code</code> do objeto <code>NeoUser</code> ou tabela <code>SecurityEntity</code>
     * @param contaSigma Número do código do cliente <code>SIGMA</code>. Campo <code> CD_CLIENTE </code>
     * @param relatoServico Relato do serviço a ser descrito na FAP.
     * @return Retorna o código do processo aberto ou um dos erros abaixo: <br>
     * {@link #ERRO_USUARIO_NAO_ENCONTRADO} <br>
     * {@link #ERRO_CONTA_SIGMA_NAO_ENCONTRADA} <br>
     * {@link #ERRO_AVANCAR_PRIMEIRA_TAREFA} <br>
     */
    public static String abrirFAPInstalacaoMercadoPublico(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAP(codigoUsuario, contaSigma, relatoServico, 6L);
		
    }
    
    /**
     * @deprecated
     * Depreciado devido a nome de interpretação ambigua.
     * Favor utilizar o método {@link #abrirFAPManutencaoMercadoPrivado}
     * 
     */
    public static String abrirFAPMercadoPrivado(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAPManutencaoMercadoPrivado(codigoUsuario, contaSigma, relatoServico);
		
    }
    
    /**
     * Abre novo processo de FAP do tipo Manutenção Alarme e CFTV - Mercado Privado
     * @param codigoUsuario Código do usuário a ser o solicitante da FAP. Campo <code>code</code> do objeto <code>NeoUser</code> ou tabela <code>SecurityEntity</code>
     * @param contaSigma Número do código do cliente <code>SIGMA</code>. Campo <code> CD_CLIENTE </code>
     * @param relatoServico Relato do serviço a ser descrito na FAP.
     * @return Retorna o código do processo aberto ou um dos erros abaixo: <br>
     * {@link #ERRO_USUARIO_NAO_ENCONTRADO} <br>
     * {@link #ERRO_CONTA_SIGMA_NAO_ENCONTRADA} <br>
     * {@link #ERRO_AVANCAR_PRIMEIRA_TAREFA} <br>
     */
    public static String abrirFAPManutencaoMercadoPrivado(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAP(codigoUsuario, contaSigma, relatoServico, 2L);
		
    }
    
    /**
     * 
     * Depreciado devido a nome de interpretação ambigua.
     * Favor utilizar o método {@link #abrirFAPManutencaoMercadoPublico}
     * @deprecated
     */
    public static String abrirFAPMercadoPublico(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAPManutencaoMercadoPublico(codigoUsuario, contaSigma, relatoServico);
		
    }
    
    

    /**
     * Abre novo processo de FAP do tipo Manutenção Alarme e CFTV - Mercado Publico
     * @param codigoUsuario Código do usuário a ser o solicitante da FAP. Campo <code>code</code> do objeto <code>NeoUser</code> ou tabela <code>SecurityEntity</code>
     * @param contaSigma Número do código do cliente <code>SIGMA</code>. Campo <code> CD_CLIENTE </code>
     * @param relatoServico Relato do serviço a ser descrito na FAP.
     * @return Retorna o código do processo aberto ou um dos erros abaixo: <br>
     * {@link #ERRO_USUARIO_NAO_ENCONTRADO} <br>
     * {@link #ERRO_CONTA_SIGMA_NAO_ENCONTRADA} <br>
     * {@link #ERRO_AVANCAR_PRIMEIRA_TAREFA} <br>
     */
    public static String abrirFAPManutencaoMercadoPublico(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAP(codigoUsuario, contaSigma, relatoServico, 3L);
		
    }
    
    /**
     * 
     * Depreciado devido à divisão das FAPs de Rastreamento
     * Favor utilizar os métodos {@link #abrirFAPManutencaoRastreamento} e {@link #abrirFAPInstalacaoRastreamento}
     * @deprecated
     */
    public static String abrirFAPRastreamento(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAPManutencaoRastreamento(codigoUsuario, contaSigma, relatoServico);
		
    }
    
    /**
     * Abre novo processo de FAP do tipo Manutenção - Rastreador Veicular
     * @param codigoUsuario Código do usuário a ser o solicitante da FAP. Campo <code>code</code> do objeto <code>NeoUser</code> ou tabela <code>SecurityEntity</code>
     * @param contaSigma Número do código do cliente <code>SIGMA</code>. Campo <code> CD_CLIENTE </code>
     * @param relatoServico Relato do serviço a ser descrito na FAP.
     * @return Retorna o código do processo aberto ou um dos erros abaixo: <br>
     * {@link #ERRO_USUARIO_NAO_ENCONTRADO} <br>
     * {@link #ERRO_CONTA_SIGMA_NAO_ENCONTRADA} <br>
     * {@link #ERRO_AVANCAR_PRIMEIRA_TAREFA} <br>
     */
    public static String abrirFAPManutencaoRastreamento(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAP(codigoUsuario, contaSigma, relatoServico, 4L);
		
    }
    
    /**
     * Abre novo processo de FAP do tipo Instalação - Rastreador Veicular
     * @param codigoUsuario Código do usuário a ser o solicitante da FAP. Campo <code>code</code> do objeto <code>NeoUser</code> ou tabela <code>SecurityEntity</code>
     * @param contaSigma Número do código do cliente <code>SIGMA</code>. Campo <code> CD_CLIENTE </code>
     * @param relatoServico Relato do serviço a ser descrito na FAP.
     * @return Retorna o código do processo aberto ou um dos erros abaixo: <br>
     * {@link #ERRO_USUARIO_NAO_ENCONTRADO} <br>
     * {@link #ERRO_CONTA_SIGMA_NAO_ENCONTRADA} <br>
     * {@link #ERRO_AVANCAR_PRIMEIRA_TAREFA} <br>
     */
    public static String abrirFAPInstalacaoRastreamento(String codigoUsuario,long contaSigma, String relatoServico){
	
	return abrirFAP(codigoUsuario, contaSigma, relatoServico, 7L);
		
    }
    
    
    
    private static String abrirFAP(String codigoUsuario,long contaSigma, String relatoServico, long tipoFAP){
	
	boolean isAvanca = true;
	
	NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", codigoUsuario));
	
	if (objSolicitante != null){
	    
	    final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "F001 - FAP - AutorizacaoDePagamento"));
	    
	    /**
	     * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
	     * @date 11/03/2015
	     */
	    
	    final WFProcess processo = WorkflowService.startProcess(pm, false, objSolicitante);
	    processo.setSaved(true);
	    final NeoObject formularioProcesso = processo.getEntity();
	    final EntityWrapper formularioWrapper = new EntityWrapper(formularioProcesso);
	    
	    formularioWrapper.findField("solicitanteOrcamento").setValue(objSolicitante);
	    
	    List<NeoObject> listaAplicacao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPAplicacaoDoPagamento"), new QLEqualsFilter("codigo", tipoFAP));
	    
	    NeoObject aplicacao = null;
	    
	    if (listaAplicacao != null && !listaAplicacao.isEmpty()){
		aplicacao = listaAplicacao.get(0);
	    }
	    
	    formularioWrapper.findField("aplicacaoPagamento").setValue(aplicacao);
	    
	    //FAPAplicacaoDoPagamento
	    List<NeoObject> listaCentral = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACENTRAL"), new QLEqualsFilter("cd_cliente", contaSigma));
	    
	    NeoObject central = null;
	    
	    if(listaCentral != null && !listaCentral.isEmpty()){
		central = listaCentral.get(0);
	    }else{
		return FAPUtils.ERRO_CONTA_SIGMA_NAO_ENCONTRADA;
	    }
	    
	    
	    formularioWrapper.findField("contaSigma").setValue(central);
	    	    
	    InstantiableEntityInfo insRegArq = AdapterUtils.getInstantiableEntityInfo("FAPLaudoOrdemServicoEletronica");
	    NeoObject objReArq = insRegArq.createNewInstance();
	    EntityWrapper wRegArq = new EntityWrapper(objReArq);
	    
	    wRegArq.setValue("solicitante", objSolicitante);
	    wRegArq.setValue("dataSolicitacao", new GregorianCalendar());
	    wRegArq.setValue("contaSigma", central);
	    wRegArq.setValue("laudo", "");
	    
	    
	    if(relatoServico.length() > 7999){
		relatoServico = relatoServico.substring(0, 7999);		
	    }
	    
	    wRegArq.setValue("relato", relatoServico);
	    
	    PersistEngine.persist(objReArq);
	    
	    
	    formularioWrapper.findField("laudoEletronica").setValue(objReArq);
	    
	    
	    PersistEngine.persist(formularioProcesso);

	    Long cdCliente = contaSigma;
	    
	    if (possuiVinculoSapiensSigma(cdCliente.intValue())){
		try {
		    new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(objSolicitante, isAvanca);
		} catch (Exception e) {
		    e.printStackTrace();
		    return ERRO_AVANCAR_PRIMEIRA_TAREFA;
		}		
//	    }else{
//		return OrsegupsWorkflowHelper.iniciaProcesso(pm, formularioProcesso, true, objSolicitante, false, objSolicitante);
	    }
	    

	    return processo.getCode();
	    
	}else{
	    return FAPUtils.ERRO_USUARIO_NAO_ENCONTRADO;
	}
	
    }
    
    public static String getSolicitanteRegional(String regional){
	
	Map<String, String> s = new  TreeMap<String, String>();
	
	s.put("SOO", "SolicitanteFAPSOO");
	s.put("IAI", "SolicitanteFAPIAI");
	s.put("BQE", "SolicitanteFAPBQE"); 
	s.put("JLE", "SolicitanteFAPJLE");
	s.put("LGS", "SolicitanteFAPLGS");
	s.put("CUA", "SolicitanteFAPCUA");
	s.put("GPR", "SolicitanteFAPGPR");
	//s.put("SOO",9L);
	s.put("CCO", "SolicitanteFAPCCO");
	s.put("RSL", "SolicitanteFAPRSL");
	s.put("JGS", "SolicitanteFAPJGS");
	s.put("CTA", "SolicitanteFAPCTA");
	s.put("CSC", "SolicitanteFAPCSC");
	s.put("TRO", "SolicitanteFAPTRO");
	
	s.put("NHO", "SolicitanteFAPNHO");
	s.put("TRI", "SolicitanteFAPTRI");
	s.put("CAS", "SolicitanteFAPCAS");
	s.put("GNA", "SolicitanteFAPGNA");
	s.put("PMJ", "SolicitanteFAPPMJ");
	s.put("SRR", "SolicitanteFAPSRR");
	s.put("XLN", "SolicitanteFAPXLN");
	
	String user = OrsegupsUtils.getUserNeoPaper(s.get(regional));
	
	return user;
    }

    private static boolean possuiVinculoSapiensSigma(int cdCliente){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append("SELECT TOP 1 1 FROM usu_t160sig WHERE usu_codcli=? ");
	
	try {
	    conn = PersistEngine.getConnection("SAPIENS");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, cdCliente);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return false;
    }
    
}
