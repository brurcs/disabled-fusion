package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.FaltaEfetivoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class CancelaTarefasFaltaEfetivo implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(CancelaTarefasFaltaEfetivo.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = null;
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		log.warn("##### INICIO AGENDADOR DE TAREFA: Cancelar Tarefa Falta de Efetivo - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		try
		{
			conn = PersistEngine.getConnection("VETORH");
			List<FaltaEfetivoVO> faltaEfetivoVOs = new ArrayList<FaltaEfetivoVO>();
			sql.append(" Select orn.usu_codreg, orn.usu_codccu, orn.numloc, orn.taborg, orn.usu_cliorn, orn.usu_lotorn, orn.nomloc, orn.usu_qtdcon, hlo.NumCad ");
			sql.append(" From R016ORN orn  ");
			sql.append(" Inner Join R016HIE hie On hie.NumLoc = orn.numloc And hie.TabOrg = orn.taborg  ");
			sql.append(" Left Join R038HLO  hlo On hlo.TabOrg = orn.taborg  And hlo.numloc = orn.numloc And hlo.DatAlt = (Select Max(hlo2.datalt) From R038HLO hlo2 Where hlo2.numemp = hlo.numemp And hlo2.tipcol = hlo.tipcol And hlo2.numcad = hlo.numcad And hlo2.DatAlt <= GetDate()) ");
			sql.append(" Left Join R034FUN fun On hlo.NumEmp = fun.numemp And hlo.tipcol = fun.tipcol  And hlo.numcad = fun.numcad And fun.tipcol = 1 ");
			sql.append(" where orn.usu_sitati = 'S'  ");
			sql.append(" And orn.usu_codreg <> 9  ");
			sql.append(" And orn.usu_codreg <> 0  ");
			sql.append(" And orn.usu_codreg <> 8   ");
			sql.append("  AND (LEN(hie.CodLoc) = 27) ");
			sql.append(" And orn.usu_numemp In (1, 7, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");
			sql.append(" And (fun.sitafa <> 7 or fun.numcad is null)  ");
			sql.append(" Order By orn.usu_codreg, orn.usu_codccu, hlo.NumCad ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			FaltaEfetivoVO faltaEfetivoVO = null;
			while (rs.next())
			{
				faltaEfetivoVO = new FaltaEfetivoVO();
				Long codreg = rs.getLong("usu_codreg");
				Long codccu = rs.getLong("usu_codccu");
				Long numloc = rs.getLong("numloc");
				Long taborg = rs.getLong("taborg");
				Long qtdCon = rs.getLong("usu_qtdcon");

				String lotorn = rs.getString("usu_lotorn");
				String nomloc = rs.getString("nomloc");
				String nomFun = rs.getString("NumCad");

				faltaEfetivoVO.setCodreg(codreg);
				faltaEfetivoVO.setCodccu(codccu);
				faltaEfetivoVO.setNumloc(numloc);
				faltaEfetivoVO.setTaborg(taborg);
				faltaEfetivoVO.setLotorn(lotorn);
				faltaEfetivoVO.setNomloc(nomloc);
				faltaEfetivoVO.setNomFun(nomFun);
				faltaEfetivoVO.setQtdCon(qtdCon.intValue());

				faltaEfetivoVOs.add(faltaEfetivoVO);

			}

			int count = 0;
			ListIterator<FaltaEfetivoVO> faltaVOIterator = faltaEfetivoVOs.listIterator();
			List<FaltaEfetivoVO> faltaEfetivoVOsAux = new ArrayList<FaltaEfetivoVO>();
			Map<Long, FaltaEfetivoVO> listaFaltaEfetivo = new HashMap<Long, FaltaEfetivoVO>();
			while (faltaVOIterator.hasNext())
			{
				FaltaEfetivoVO atual = faltaVOIterator.next();
				FaltaEfetivoVO proximo = null;

				if (faltaVOIterator.hasNext())
				{
					proximo = faltaVOIterator.next();
					faltaVOIterator.previous();

					if (atual.getNumloc().equals(proximo.getNumloc()) && atual.getNomFun() != null && !atual.getNomFun().isEmpty())
					{
						count++;
					}
					else if (!proximo.getNumloc().equals(atual.getNumloc()))
					{
						count++;
						if (atual.getQtdCon() > count || atual.getQtdCon() == 1 && ((atual.getNomFun() != null && atual.getNomFun().isEmpty()) || (atual.getNomFun() == null)))
						{
							log.debug(proximo.getNumloc() + " - " + atual.getNumloc() + " - " + atual.getQtdCon() + " > - " + count);
							faltaEfetivoVO = new FaltaEfetivoVO();
							faltaEfetivoVO.setCodreg(atual.getCodreg());
							faltaEfetivoVO.setCodccu(atual.getCodccu());
							faltaEfetivoVO.setNumloc(atual.getNumloc());
							faltaEfetivoVO.setTaborg(atual.getTaborg());
							faltaEfetivoVO.setLotorn(atual.getLotorn());
							faltaEfetivoVO.setNomloc(atual.getNomloc());
							faltaEfetivoVO.setQtdCon(atual.getQtdCon());
							faltaEfetivoVO.setQtdFun(count);
							faltaEfetivoVOsAux.add(faltaEfetivoVO);
							listaFaltaEfetivo.put(Long.parseLong(atual.getNumloc() + "" + atual.getTaborg()), faltaEfetivoVO);

						}
						count = 0;
					}
				}

			}

			OrsegupsUtils.closeConnection(conn, pstm, rs);
			conn = PersistEngine.getConnection("");
			StringBuilder sqlConsulta = new StringBuilder();
			sqlConsulta.append(" select distinct(tarefa) as tarefa,f.taborg,f.numloc,p.neoId,f.processo from D_TarefaFaltaEfetivo f inner join ");
			sqlConsulta.append(" WFProcess p With (NOLOCK) on p.code = f.tarefa ");
			sqlConsulta.append(" inner join ProcessModel pm on pm.neoId = p.model_neoId ");
			sqlConsulta.append(" where processState = 0 and saved = 1 and finishDate is null and pm.name = processo ");
			pstm = conn.prepareStatement(sqlConsulta.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				Long taborg = rs.getLong("taborg");
				Long numloc = rs.getLong("numloc");
				String tarefa = rs.getString("tarefa");
				Long neoId = rs.getLong("neoId");
				String processo = rs.getString("processo");

				if (listaFaltaEfetivo != null && !listaFaltaEfetivo.isEmpty() && !listaFaltaEfetivo.containsKey(Long.parseLong(numloc + "" + taborg)))
				{
					WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", neoId));
					if (proc != null)
					{
						RuntimeEngine.getProcessService().cancel(proc, "Cancelado devido ao efetivo no posto.");
						String texto = "Tarefa referente ao processo, " + processo + " esta sendo cancelada devido a efetivo no posto. Código, " + tarefa + ".";
						String codccu = retornaCcu(taborg, numloc);
						saveLog(codccu, texto);
					}
				}
			}
		}
		catch (Exception e)
		{

			log.error("##### ERRO AGENDADOR DE TAREFA: Cancelar Tarefa Falta de Efetivo");
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Cancelar Tarefa Falta de Efetivo");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

			log.warn("##### FIM AGENDADOR DE TAREFA: Cancelar Tarefa Falta de Efetivo - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	public static void saveLog(String codccu, String texto)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("usu_codccu", codccu));
		groupFilter.addFilter(new QLEqualsFilter("usu_sitati", "S"));

		List<NeoObject> postos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8"), groupFilter);

		if (postos != null && !postos.isEmpty())
		{
			NeoObject postoObject = postos.get(0);
			NeoObject logPosto = AdapterUtils.createNewEntityInstance("QLLogPresencaPosto");

			if (logPosto != null)
			{
				NeoPaper papel = new NeoPaper();
				papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
				NeoUser usuarioResponsavel = new NeoUser();
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						usuarioResponsavel = user;
						break;
					}
				}
				if (usuarioResponsavel != null)
				{
					EntityWrapper postoWrapper = new EntityWrapper(logPosto);
					postoWrapper.setValue("posto", postoObject);
					postoWrapper.setValue("dataLog", new GregorianCalendar());
					postoWrapper.setValue("textoLog", texto);
					postoWrapper.setValue("usuario", usuarioResponsavel);

					PersistEngine.persist(logPosto);
					PersistEngine.commit(true);
				}
			}
		}
	}

	public static String retornaCcu(Long taborg, Long numloc)
	{
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try
		{
			conn = PersistEngine.getConnection("VETORH");
			StringBuilder builder = new StringBuilder();
			builder.append(" select usu_codccu from r016orn with(nolock) where taborg = " + taborg + " and numloc = " + numloc + " ");
			statement = conn.prepareStatement(builder.toString());
			resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				return resultSet.getString("usu_codccu");
			}

			OrsegupsUtils.closeConnection(conn, statement, resultSet);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, statement, resultSet);
		}
		return "";
	}
}
