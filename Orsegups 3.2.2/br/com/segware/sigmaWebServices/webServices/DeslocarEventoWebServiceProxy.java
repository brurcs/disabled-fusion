package br.com.segware.sigmaWebServices.webServices;

public class DeslocarEventoWebServiceProxy implements br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebService_PortType {
  private String _endpoint = null;
  private br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebService_PortType deslocarEventoWebService_PortType = null;
  
  public DeslocarEventoWebServiceProxy() {
    _initDeslocarEventoWebServiceProxy();
  }
  
  public DeslocarEventoWebServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initDeslocarEventoWebServiceProxy();
  }
  
  private void _initDeslocarEventoWebServiceProxy() {
    try {
      deslocarEventoWebService_PortType = (new br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebService_ServiceLocator()).getDeslocarEventoWebServicePort();
      if (deslocarEventoWebService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)deslocarEventoWebService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)deslocarEventoWebService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (deslocarEventoWebService_PortType != null)
      ((javax.xml.rpc.Stub)deslocarEventoWebService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebService_PortType getDeslocarEventoWebService_PortType() {
    if (deslocarEventoWebService_PortType == null)
      _initDeslocarEventoWebServiceProxy();
    return deslocarEventoWebService_PortType;
  }
  
  public java.lang.String deslocarEvento(java.lang.Long idEvento, java.lang.String rastreador, java.lang.Long idViatura) throws java.rmi.RemoteException{
    if (deslocarEventoWebService_PortType == null)
      _initDeslocarEventoWebServiceProxy();
    return deslocarEventoWebService_PortType.deslocarEvento(idEvento, rastreador, idViatura);
  }
  
  
}