/**
 * DeslocarEventoWebService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.segware.sigmaWebServices.webServices;

public interface DeslocarEventoWebService_PortType extends java.rmi.Remote {
    public java.lang.String deslocarEvento(java.lang.Long idEvento, java.lang.String rastreador, java.lang.Long idViatura) throws java.rmi.RemoteException;
}
