/**
 * EventoRecebido.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.segware.sigmaWebServices.webServices;

public class EventoRecebido  implements java.io.Serializable {
    private java.lang.String auxiliar;

    private java.lang.Integer cdCliente;

    private java.lang.String codigo;

    private java.util.Calendar data;

    private java.lang.String descricaoReceptora;

    private java.util.Calendar dtUltimaLocalizacao;

    private java.lang.Long empresa;

    private java.lang.String idCentral;

    private java.lang.String identificadorCliente;

    private java.lang.String nmFraseEvento;

    private java.lang.String nuLatitude;

    private java.lang.String nuLongitude;

    private java.lang.String particao;
    
    private java.lang.String logEvento;
    
    private java.lang.String nomePessoa;

    private byte protocolo;

    private byte tipoIntegracao;

    public EventoRecebido() {
    }

    public EventoRecebido(
           java.lang.String auxiliar,
           java.lang.Integer cdCliente,
           java.lang.String codigo,
           java.util.Calendar data,
           java.lang.String descricaoReceptora,
           java.util.Calendar dtUltimaLocalizacao,
           java.lang.Long empresa,
           java.lang.String idCentral,
           java.lang.String identificadorCliente,
           java.lang.String nmFraseEvento,
           java.lang.String nuLatitude,
           java.lang.String nuLongitude,
           java.lang.String particao,
           java.lang.String logEvento,
           java.lang.String nomePessoa,
           byte protocolo,
           byte tipoIntegracao) {
           this.auxiliar = auxiliar;
           this.cdCliente = cdCliente;
           this.codigo = codigo;
           this.data = data;
           this.descricaoReceptora = descricaoReceptora;
           this.dtUltimaLocalizacao = dtUltimaLocalizacao;
           this.empresa = empresa;
           this.idCentral = idCentral;
           this.identificadorCliente = identificadorCliente;
           this.nmFraseEvento = nmFraseEvento;
           this.nuLatitude = nuLatitude;
           this.nuLongitude = nuLongitude;
           this.particao = particao;
           this.protocolo = protocolo;
           this.tipoIntegracao = tipoIntegracao;
           this.logEvento = logEvento;
           this.nomePessoa = nomePessoa;
    }
    
    public java.lang.String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(java.lang.String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}

	public java.lang.String getLogEvento() {
		return logEvento;
	}

	public void setLogEvento(java.lang.String logEvento) {
		this.logEvento = logEvento;
	}

	/**
     * Gets the auxiliar value for this EventoRecebido.
     * 
     * @return auxiliar
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }


    /**
     * Sets the auxiliar value for this EventoRecebido.
     * 
     * @param auxiliar
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }


    /**
     * Gets the cdCliente value for this EventoRecebido.
     * 
     * @return cdCliente
     */
    public java.lang.Integer getCdCliente() {
        return cdCliente;
    }


    /**
     * Sets the cdCliente value for this EventoRecebido.
     * 
     * @param cdCliente
     */
    public void setCdCliente(java.lang.Integer cdCliente) {
        this.cdCliente = cdCliente;
    }


    /**
     * Gets the codigo value for this EventoRecebido.
     * 
     * @return codigo
     */
    public java.lang.String getCodigo() {
        return codigo;
    }


    /**
     * Sets the codigo value for this EventoRecebido.
     * 
     * @param codigo
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }


    /**
     * Gets the data value for this EventoRecebido.
     * 
     * @return data
     */
    public java.util.Calendar getData() {
        return data;
    }


    /**
     * Sets the data value for this EventoRecebido.
     * 
     * @param data
     */
    public void setData(java.util.Calendar data) {
        this.data = data;
    }


    /**
     * Gets the descricaoReceptora value for this EventoRecebido.
     * 
     * @return descricaoReceptora
     */
    public java.lang.String getDescricaoReceptora() {
        return descricaoReceptora;
    }


    /**
     * Sets the descricaoReceptora value for this EventoRecebido.
     * 
     * @param descricaoReceptora
     */
    public void setDescricaoReceptora(java.lang.String descricaoReceptora) {
        this.descricaoReceptora = descricaoReceptora;
    }


    /**
     * Gets the dtUltimaLocalizacao value for this EventoRecebido.
     * 
     * @return dtUltimaLocalizacao
     */
    public java.util.Calendar getDtUltimaLocalizacao() {
        return dtUltimaLocalizacao;
    }


    /**
     * Sets the dtUltimaLocalizacao value for this EventoRecebido.
     * 
     * @param dtUltimaLocalizacao
     */
    public void setDtUltimaLocalizacao(java.util.Calendar dtUltimaLocalizacao) {
        this.dtUltimaLocalizacao = dtUltimaLocalizacao;
    }


    /**
     * Gets the empresa value for this EventoRecebido.
     * 
     * @return empresa
     */
    public java.lang.Long getEmpresa() {
        return empresa;
    }


    /**
     * Sets the empresa value for this EventoRecebido.
     * 
     * @param empresa
     */
    public void setEmpresa(java.lang.Long empresa) {
        this.empresa = empresa;
    }


    /**
     * Gets the idCentral value for this EventoRecebido.
     * 
     * @return idCentral
     */
    public java.lang.String getIdCentral() {
        return idCentral;
    }


    /**
     * Sets the idCentral value for this EventoRecebido.
     * 
     * @param idCentral
     */
    public void setIdCentral(java.lang.String idCentral) {
        this.idCentral = idCentral;
    }


    /**
     * Gets the identificadorCliente value for this EventoRecebido.
     * 
     * @return identificadorCliente
     */
    public java.lang.String getIdentificadorCliente() {
        return identificadorCliente;
    }


    /**
     * Sets the identificadorCliente value for this EventoRecebido.
     * 
     * @param identificadorCliente
     */
    public void setIdentificadorCliente(java.lang.String identificadorCliente) {
        this.identificadorCliente = identificadorCliente;
    }


    /**
     * Gets the nmFraseEvento value for this EventoRecebido.
     * 
     * @return nmFraseEvento
     */
    public java.lang.String getNmFraseEvento() {
        return nmFraseEvento;
    }


    /**
     * Sets the nmFraseEvento value for this EventoRecebido.
     * 
     * @param nmFraseEvento
     */
    public void setNmFraseEvento(java.lang.String nmFraseEvento) {
        this.nmFraseEvento = nmFraseEvento;
    }


    /**
     * Gets the nuLatitude value for this EventoRecebido.
     * 
     * @return nuLatitude
     */
    public java.lang.String getNuLatitude() {
        return nuLatitude;
    }


    /**
     * Sets the nuLatitude value for this EventoRecebido.
     * 
     * @param nuLatitude
     */
    public void setNuLatitude(java.lang.String nuLatitude) {
        this.nuLatitude = nuLatitude;
    }


    /**
     * Gets the nuLongitude value for this EventoRecebido.
     * 
     * @return nuLongitude
     */
    public java.lang.String getNuLongitude() {
        return nuLongitude;
    }


    /**
     * Sets the nuLongitude value for this EventoRecebido.
     * 
     * @param nuLongitude
     */
    public void setNuLongitude(java.lang.String nuLongitude) {
        this.nuLongitude = nuLongitude;
    }


    /**
     * Gets the particao value for this EventoRecebido.
     * 
     * @return particao
     */
    public java.lang.String getParticao() {
        return particao;
    }


    /**
     * Sets the particao value for this EventoRecebido.
     * 
     * @param particao
     */
    public void setParticao(java.lang.String particao) {
        this.particao = particao;
    }


    /**
     * Gets the protocolo value for this EventoRecebido.
     * 
     * @return protocolo
     */
    public byte getProtocolo() {
        return protocolo;
    }


    /**
     * Sets the protocolo value for this EventoRecebido.
     * 
     * @param protocolo
     */
    public void setProtocolo(byte protocolo) {
        this.protocolo = protocolo;
    }


    /**
     * Gets the tipoIntegracao value for this EventoRecebido.
     * 
     * @return tipoIntegracao
     */
    public byte getTipoIntegracao() {
        return tipoIntegracao;
    }


    /**
     * Sets the tipoIntegracao value for this EventoRecebido.
     * 
     * @param tipoIntegracao
     */
    public void setTipoIntegracao(byte tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventoRecebido)) return false;
        EventoRecebido other = (EventoRecebido) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
        	((this.nomePessoa==null && other.getNomePessoa()==null) || 
        	  (this.nomePessoa!=null &&
        	  this.nomePessoa.equals(other.getNomePessoa()))) &&
        	((this.logEvento==null && other.getLogEvento()==null) || 
        	  (this.logEvento!=null &&
        	  this.logEvento.equals(other.getLogEvento()))) &&
            ((this.auxiliar==null && other.getAuxiliar()==null) || 
             (this.auxiliar!=null &&
              this.auxiliar.equals(other.getAuxiliar()))) &&
            ((this.cdCliente==null && other.getCdCliente()==null) || 
             (this.cdCliente!=null &&
              this.cdCliente.equals(other.getCdCliente()))) &&
            ((this.codigo==null && other.getCodigo()==null) || 
             (this.codigo!=null &&
              this.codigo.equals(other.getCodigo()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData()))) &&
            ((this.descricaoReceptora==null && other.getDescricaoReceptora()==null) || 
             (this.descricaoReceptora!=null &&
              this.descricaoReceptora.equals(other.getDescricaoReceptora()))) &&
            ((this.dtUltimaLocalizacao==null && other.getDtUltimaLocalizacao()==null) || 
             (this.dtUltimaLocalizacao!=null &&
              this.dtUltimaLocalizacao.equals(other.getDtUltimaLocalizacao()))) &&
            ((this.empresa==null && other.getEmpresa()==null) || 
             (this.empresa!=null &&
              this.empresa.equals(other.getEmpresa()))) &&
            ((this.idCentral==null && other.getIdCentral()==null) || 
             (this.idCentral!=null &&
              this.idCentral.equals(other.getIdCentral()))) &&
            ((this.identificadorCliente==null && other.getIdentificadorCliente()==null) || 
             (this.identificadorCliente!=null &&
              this.identificadorCliente.equals(other.getIdentificadorCliente()))) &&
            ((this.nmFraseEvento==null && other.getNmFraseEvento()==null) || 
             (this.nmFraseEvento!=null &&
              this.nmFraseEvento.equals(other.getNmFraseEvento()))) &&
            ((this.nuLatitude==null && other.getNuLatitude()==null) || 
             (this.nuLatitude!=null &&
              this.nuLatitude.equals(other.getNuLatitude()))) &&
            ((this.nuLongitude==null && other.getNuLongitude()==null) || 
             (this.nuLongitude!=null &&
              this.nuLongitude.equals(other.getNuLongitude()))) &&
            ((this.particao==null && other.getParticao()==null) || 
             (this.particao!=null &&
              this.particao.equals(other.getParticao()))) &&
            this.protocolo == other.getProtocolo() &&
            this.tipoIntegracao == other.getTipoIntegracao();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuxiliar() != null) {
            _hashCode += getAuxiliar().hashCode();
        }
        if (getCdCliente() != null) {
            _hashCode += getCdCliente().hashCode();
        }
        if (getCodigo() != null) {
            _hashCode += getCodigo().hashCode();
        }
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        if (getDescricaoReceptora() != null) {
            _hashCode += getDescricaoReceptora().hashCode();
        }
        if (getDtUltimaLocalizacao() != null) {
            _hashCode += getDtUltimaLocalizacao().hashCode();
        }
        if (getEmpresa() != null) {
            _hashCode += getEmpresa().hashCode();
        }
        if (getIdCentral() != null) {
            _hashCode += getIdCentral().hashCode();
        }
        if (getIdentificadorCliente() != null) {
            _hashCode += getIdentificadorCliente().hashCode();
        }
        if (getNmFraseEvento() != null) {
            _hashCode += getNmFraseEvento().hashCode();
        }
        if (getNuLatitude() != null) {
            _hashCode += getNuLatitude().hashCode();
        }
        if (getNuLongitude() != null) {
            _hashCode += getNuLongitude().hashCode();
        }
        if (getParticao() != null) {
            _hashCode += getParticao().hashCode();
        }
        if (getLogEvento() != null) {
            _hashCode += getLogEvento().hashCode();
        }
        if (getNomePessoa() != null) {
            _hashCode += getNomePessoa().hashCode();
        }
        _hashCode += getProtocolo();
        _hashCode += getTipoIntegracao();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventoRecebido.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webServices.sigmaWebServices.segware.com.br/", "eventoRecebido"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("auxiliar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "auxiliar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cdCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cdCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("", "data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoReceptora");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricaoReceptora"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dtUltimaLocalizacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dtUltimaLocalizacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "empresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCentral");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idCentral"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificadorCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identificadorCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nmFraseEvento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nmFraseEvento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nuLatitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nuLatitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nuLongitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nuLongitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("particao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "particao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "protocolo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoIntegracao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoIntegracao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logEvento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "logEvento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomePessoa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomePessoa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
