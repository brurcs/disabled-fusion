/**
 * OrdemcomprareprovarOutRetorno.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public class OrdemcomprareprovarOutRetorno  implements java.io.Serializable {
    private java.lang.Integer codigoEmpresa;

    private java.lang.Integer codigoFilial;

    private java.lang.String mensagemErro;

    private java.lang.Integer numero;

    private java.lang.Integer resultado;

    private java.lang.Integer sequencia;

    public OrdemcomprareprovarOutRetorno() {
    }

    public OrdemcomprareprovarOutRetorno(
           java.lang.Integer codigoEmpresa,
           java.lang.Integer codigoFilial,
           java.lang.String mensagemErro,
           java.lang.Integer numero,
           java.lang.Integer resultado,
           java.lang.Integer sequencia) {
           this.codigoEmpresa = codigoEmpresa;
           this.codigoFilial = codigoFilial;
           this.mensagemErro = mensagemErro;
           this.numero = numero;
           this.resultado = resultado;
           this.sequencia = sequencia;
    }


    /**
     * Gets the codigoEmpresa value for this OrdemcomprareprovarOutRetorno.
     * 
     * @return codigoEmpresa
     */
    public java.lang.Integer getCodigoEmpresa() {
        return codigoEmpresa;
    }


    /**
     * Sets the codigoEmpresa value for this OrdemcomprareprovarOutRetorno.
     * 
     * @param codigoEmpresa
     */
    public void setCodigoEmpresa(java.lang.Integer codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }


    /**
     * Gets the codigoFilial value for this OrdemcomprareprovarOutRetorno.
     * 
     * @return codigoFilial
     */
    public java.lang.Integer getCodigoFilial() {
        return codigoFilial;
    }


    /**
     * Sets the codigoFilial value for this OrdemcomprareprovarOutRetorno.
     * 
     * @param codigoFilial
     */
    public void setCodigoFilial(java.lang.Integer codigoFilial) {
        this.codigoFilial = codigoFilial;
    }


    /**
     * Gets the mensagemErro value for this OrdemcomprareprovarOutRetorno.
     * 
     * @return mensagemErro
     */
    public java.lang.String getMensagemErro() {
        return mensagemErro;
    }


    /**
     * Sets the mensagemErro value for this OrdemcomprareprovarOutRetorno.
     * 
     * @param mensagemErro
     */
    public void setMensagemErro(java.lang.String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }


    /**
     * Gets the numero value for this OrdemcomprareprovarOutRetorno.
     * 
     * @return numero
     */
    public java.lang.Integer getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this OrdemcomprareprovarOutRetorno.
     * 
     * @param numero
     */
    public void setNumero(java.lang.Integer numero) {
        this.numero = numero;
    }


    /**
     * Gets the resultado value for this OrdemcomprareprovarOutRetorno.
     * 
     * @return resultado
     */
    public java.lang.Integer getResultado() {
        return resultado;
    }


    /**
     * Sets the resultado value for this OrdemcomprareprovarOutRetorno.
     * 
     * @param resultado
     */
    public void setResultado(java.lang.Integer resultado) {
        this.resultado = resultado;
    }


    /**
     * Gets the sequencia value for this OrdemcomprareprovarOutRetorno.
     * 
     * @return sequencia
     */
    public java.lang.Integer getSequencia() {
        return sequencia;
    }


    /**
     * Sets the sequencia value for this OrdemcomprareprovarOutRetorno.
     * 
     * @param sequencia
     */
    public void setSequencia(java.lang.Integer sequencia) {
        this.sequencia = sequencia;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrdemcomprareprovarOutRetorno)) return false;
        OrdemcomprareprovarOutRetorno other = (OrdemcomprareprovarOutRetorno) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoEmpresa==null && other.getCodigoEmpresa()==null) || 
             (this.codigoEmpresa!=null &&
              this.codigoEmpresa.equals(other.getCodigoEmpresa()))) &&
            ((this.codigoFilial==null && other.getCodigoFilial()==null) || 
             (this.codigoFilial!=null &&
              this.codigoFilial.equals(other.getCodigoFilial()))) &&
            ((this.mensagemErro==null && other.getMensagemErro()==null) || 
             (this.mensagemErro!=null &&
              this.mensagemErro.equals(other.getMensagemErro()))) &&
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.resultado==null && other.getResultado()==null) || 
             (this.resultado!=null &&
              this.resultado.equals(other.getResultado()))) &&
            ((this.sequencia==null && other.getSequencia()==null) || 
             (this.sequencia!=null &&
              this.sequencia.equals(other.getSequencia())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoEmpresa() != null) {
            _hashCode += getCodigoEmpresa().hashCode();
        }
        if (getCodigoFilial() != null) {
            _hashCode += getCodigoFilial().hashCode();
        }
        if (getMensagemErro() != null) {
            _hashCode += getMensagemErro().hashCode();
        }
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getResultado() != null) {
            _hashCode += getResultado().hashCode();
        }
        if (getSequencia() != null) {
            _hashCode += getSequencia().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrdemcomprareprovarOutRetorno.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprareprovarOutRetorno"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoEmpresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoEmpresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemErro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemErro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sequencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sequencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
