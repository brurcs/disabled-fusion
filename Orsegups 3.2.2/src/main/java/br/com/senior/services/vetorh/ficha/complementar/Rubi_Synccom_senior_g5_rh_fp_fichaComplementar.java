/**
 * Rubi_Synccom_senior_g5_rh_fp_fichaComplementar.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.complementar;

public interface Rubi_Synccom_senior_g5_rh_fp_fichaComplementar extends java.rmi.Remote {
    public br.com.senior.services.vetorh.ficha.complementar.FichaComplementarDocumentoOut documento(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.complementar.FichaComplementarDocumentoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementarOut fichaComplementar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar2Out fichaComplementar_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar3Out fichaComplementar_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar3In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar4Out fichaComplementar_4(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar4In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar5Out fichaComplementar_5(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar5In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar6Out fichaComplementar_6(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar6In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar7Out fichaComplementar_7(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar7In parameters) throws java.rmi.RemoteException;
}
