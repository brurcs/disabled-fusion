/**
 * HistoricosSalario2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosSalario2In  implements java.io.Serializable {
    private java.lang.String claSal;

    private java.lang.Integer codEst;

    private java.lang.Integer codIdm;

    private java.lang.Integer codMot;

    private java.lang.Double cplEst;

    private java.lang.Double cplSal;

    private java.lang.String datAlt;

    private java.lang.String datDis;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String nivSal;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Double salEst;

    private java.lang.Integer seqAlt;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    private java.lang.Integer tipSal;

    private java.lang.Double valSal;

    public HistoricosSalario2In() {
    }

    public HistoricosSalario2In(
           java.lang.String claSal,
           java.lang.Integer codEst,
           java.lang.Integer codIdm,
           java.lang.Integer codMot,
           java.lang.Double cplEst,
           java.lang.Double cplSal,
           java.lang.String datAlt,
           java.lang.String datDis,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String nivSal,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Double salEst,
           java.lang.Integer seqAlt,
           java.lang.Integer tipCol,
           java.lang.String tipOpe,
           java.lang.Integer tipSal,
           java.lang.Double valSal) {
           this.claSal = claSal;
           this.codEst = codEst;
           this.codIdm = codIdm;
           this.codMot = codMot;
           this.cplEst = cplEst;
           this.cplSal = cplSal;
           this.datAlt = datAlt;
           this.datDis = datDis;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.nivSal = nivSal;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.salEst = salEst;
           this.seqAlt = seqAlt;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
           this.tipSal = tipSal;
           this.valSal = valSal;
    }


    /**
     * Gets the claSal value for this HistoricosSalario2In.
     * 
     * @return claSal
     */
    public java.lang.String getClaSal() {
        return claSal;
    }


    /**
     * Sets the claSal value for this HistoricosSalario2In.
     * 
     * @param claSal
     */
    public void setClaSal(java.lang.String claSal) {
        this.claSal = claSal;
    }


    /**
     * Gets the codEst value for this HistoricosSalario2In.
     * 
     * @return codEst
     */
    public java.lang.Integer getCodEst() {
        return codEst;
    }


    /**
     * Sets the codEst value for this HistoricosSalario2In.
     * 
     * @param codEst
     */
    public void setCodEst(java.lang.Integer codEst) {
        this.codEst = codEst;
    }


    /**
     * Gets the codIdm value for this HistoricosSalario2In.
     * 
     * @return codIdm
     */
    public java.lang.Integer getCodIdm() {
        return codIdm;
    }


    /**
     * Sets the codIdm value for this HistoricosSalario2In.
     * 
     * @param codIdm
     */
    public void setCodIdm(java.lang.Integer codIdm) {
        this.codIdm = codIdm;
    }


    /**
     * Gets the codMot value for this HistoricosSalario2In.
     * 
     * @return codMot
     */
    public java.lang.Integer getCodMot() {
        return codMot;
    }


    /**
     * Sets the codMot value for this HistoricosSalario2In.
     * 
     * @param codMot
     */
    public void setCodMot(java.lang.Integer codMot) {
        this.codMot = codMot;
    }


    /**
     * Gets the cplEst value for this HistoricosSalario2In.
     * 
     * @return cplEst
     */
    public java.lang.Double getCplEst() {
        return cplEst;
    }


    /**
     * Sets the cplEst value for this HistoricosSalario2In.
     * 
     * @param cplEst
     */
    public void setCplEst(java.lang.Double cplEst) {
        this.cplEst = cplEst;
    }


    /**
     * Gets the cplSal value for this HistoricosSalario2In.
     * 
     * @return cplSal
     */
    public java.lang.Double getCplSal() {
        return cplSal;
    }


    /**
     * Sets the cplSal value for this HistoricosSalario2In.
     * 
     * @param cplSal
     */
    public void setCplSal(java.lang.Double cplSal) {
        this.cplSal = cplSal;
    }


    /**
     * Gets the datAlt value for this HistoricosSalario2In.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosSalario2In.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the datDis value for this HistoricosSalario2In.
     * 
     * @return datDis
     */
    public java.lang.String getDatDis() {
        return datDis;
    }


    /**
     * Sets the datDis value for this HistoricosSalario2In.
     * 
     * @param datDis
     */
    public void setDatDis(java.lang.String datDis) {
        this.datDis = datDis;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosSalario2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosSalario2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosSalario2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosSalario2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the nivSal value for this HistoricosSalario2In.
     * 
     * @return nivSal
     */
    public java.lang.String getNivSal() {
        return nivSal;
    }


    /**
     * Sets the nivSal value for this HistoricosSalario2In.
     * 
     * @param nivSal
     */
    public void setNivSal(java.lang.String nivSal) {
        this.nivSal = nivSal;
    }


    /**
     * Gets the numCad value for this HistoricosSalario2In.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosSalario2In.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosSalario2In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosSalario2In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the salEst value for this HistoricosSalario2In.
     * 
     * @return salEst
     */
    public java.lang.Double getSalEst() {
        return salEst;
    }


    /**
     * Sets the salEst value for this HistoricosSalario2In.
     * 
     * @param salEst
     */
    public void setSalEst(java.lang.Double salEst) {
        this.salEst = salEst;
    }


    /**
     * Gets the seqAlt value for this HistoricosSalario2In.
     * 
     * @return seqAlt
     */
    public java.lang.Integer getSeqAlt() {
        return seqAlt;
    }


    /**
     * Sets the seqAlt value for this HistoricosSalario2In.
     * 
     * @param seqAlt
     */
    public void setSeqAlt(java.lang.Integer seqAlt) {
        this.seqAlt = seqAlt;
    }


    /**
     * Gets the tipCol value for this HistoricosSalario2In.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosSalario2In.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosSalario2In.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosSalario2In.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the tipSal value for this HistoricosSalario2In.
     * 
     * @return tipSal
     */
    public java.lang.Integer getTipSal() {
        return tipSal;
    }


    /**
     * Sets the tipSal value for this HistoricosSalario2In.
     * 
     * @param tipSal
     */
    public void setTipSal(java.lang.Integer tipSal) {
        this.tipSal = tipSal;
    }


    /**
     * Gets the valSal value for this HistoricosSalario2In.
     * 
     * @return valSal
     */
    public java.lang.Double getValSal() {
        return valSal;
    }


    /**
     * Sets the valSal value for this HistoricosSalario2In.
     * 
     * @param valSal
     */
    public void setValSal(java.lang.Double valSal) {
        this.valSal = valSal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosSalario2In)) return false;
        HistoricosSalario2In other = (HistoricosSalario2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.claSal==null && other.getClaSal()==null) || 
             (this.claSal!=null &&
              this.claSal.equals(other.getClaSal()))) &&
            ((this.codEst==null && other.getCodEst()==null) || 
             (this.codEst!=null &&
              this.codEst.equals(other.getCodEst()))) &&
            ((this.codIdm==null && other.getCodIdm()==null) || 
             (this.codIdm!=null &&
              this.codIdm.equals(other.getCodIdm()))) &&
            ((this.codMot==null && other.getCodMot()==null) || 
             (this.codMot!=null &&
              this.codMot.equals(other.getCodMot()))) &&
            ((this.cplEst==null && other.getCplEst()==null) || 
             (this.cplEst!=null &&
              this.cplEst.equals(other.getCplEst()))) &&
            ((this.cplSal==null && other.getCplSal()==null) || 
             (this.cplSal!=null &&
              this.cplSal.equals(other.getCplSal()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.datDis==null && other.getDatDis()==null) || 
             (this.datDis!=null &&
              this.datDis.equals(other.getDatDis()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.nivSal==null && other.getNivSal()==null) || 
             (this.nivSal!=null &&
              this.nivSal.equals(other.getNivSal()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.salEst==null && other.getSalEst()==null) || 
             (this.salEst!=null &&
              this.salEst.equals(other.getSalEst()))) &&
            ((this.seqAlt==null && other.getSeqAlt()==null) || 
             (this.seqAlt!=null &&
              this.seqAlt.equals(other.getSeqAlt()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.tipSal==null && other.getTipSal()==null) || 
             (this.tipSal!=null &&
              this.tipSal.equals(other.getTipSal()))) &&
            ((this.valSal==null && other.getValSal()==null) || 
             (this.valSal!=null &&
              this.valSal.equals(other.getValSal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClaSal() != null) {
            _hashCode += getClaSal().hashCode();
        }
        if (getCodEst() != null) {
            _hashCode += getCodEst().hashCode();
        }
        if (getCodIdm() != null) {
            _hashCode += getCodIdm().hashCode();
        }
        if (getCodMot() != null) {
            _hashCode += getCodMot().hashCode();
        }
        if (getCplEst() != null) {
            _hashCode += getCplEst().hashCode();
        }
        if (getCplSal() != null) {
            _hashCode += getCplSal().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getDatDis() != null) {
            _hashCode += getDatDis().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNivSal() != null) {
            _hashCode += getNivSal().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getSalEst() != null) {
            _hashCode += getSalEst().hashCode();
        }
        if (getSeqAlt() != null) {
            _hashCode += getSeqAlt().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTipSal() != null) {
            _hashCode += getTipSal().hashCode();
        }
        if (getValSal() != null) {
            _hashCode += getValSal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosSalario2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosSalario2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codIdm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codIdm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nivSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nivSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
