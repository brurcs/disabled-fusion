/**
 * ClientesConsultarFiscalOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesConsultarFiscalOut  implements java.io.Serializable {
    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutCliente[] cliente;

    private java.lang.String erroExecucao;

    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutErros[] erros;

    private java.lang.String mensagemRetorno;

    private java.lang.Integer tipoRetorno;

    public ClientesConsultarFiscalOut() {
    }

    public ClientesConsultarFiscalOut(
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutCliente[] cliente,
           java.lang.String erroExecucao,
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutErros[] erros,
           java.lang.String mensagemRetorno,
           java.lang.Integer tipoRetorno) {
           this.cliente = cliente;
           this.erroExecucao = erroExecucao;
           this.erros = erros;
           this.mensagemRetorno = mensagemRetorno;
           this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the cliente value for this ClientesConsultarFiscalOut.
     * 
     * @return cliente
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutCliente[] getCliente() {
        return cliente;
    }


    /**
     * Sets the cliente value for this ClientesConsultarFiscalOut.
     * 
     * @param cliente
     */
    public void setCliente(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutCliente[] cliente) {
        this.cliente = cliente;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutCliente getCliente(int i) {
        return this.cliente[i];
    }

    public void setCliente(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutCliente _value) {
        this.cliente[i] = _value;
    }


    /**
     * Gets the erroExecucao value for this ClientesConsultarFiscalOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this ClientesConsultarFiscalOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the erros value for this ClientesConsultarFiscalOut.
     * 
     * @return erros
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutErros[] getErros() {
        return erros;
    }


    /**
     * Sets the erros value for this ClientesConsultarFiscalOut.
     * 
     * @param erros
     */
    public void setErros(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutErros[] erros) {
        this.erros = erros;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutErros getErros(int i) {
        return this.erros[i];
    }

    public void setErros(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutErros _value) {
        this.erros[i] = _value;
    }


    /**
     * Gets the mensagemRetorno value for this ClientesConsultarFiscalOut.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this ClientesConsultarFiscalOut.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the tipoRetorno value for this ClientesConsultarFiscalOut.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this ClientesConsultarFiscalOut.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesConsultarFiscalOut)) return false;
        ClientesConsultarFiscalOut other = (ClientesConsultarFiscalOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cliente==null && other.getCliente()==null) || 
             (this.cliente!=null &&
              java.util.Arrays.equals(this.cliente, other.getCliente()))) &&
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.erros==null && other.getErros()==null) || 
             (this.erros!=null &&
              java.util.Arrays.equals(this.erros, other.getErros()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCliente() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCliente());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCliente(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getErros() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErros());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErros(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesConsultarFiscalOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalOutCliente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalOutErros"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
