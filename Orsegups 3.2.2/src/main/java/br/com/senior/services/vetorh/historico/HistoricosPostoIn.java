/**
 * HistoricosPostoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosPostoIn  implements java.io.Serializable {
    private java.lang.String excHis;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String funEso;

    private java.lang.String iniAtu;

    private java.lang.Integer motPos;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.String obsPos;

    private java.lang.String posTra;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    public HistoricosPostoIn() {
    }

    public HistoricosPostoIn(
           java.lang.String excHis,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String funEso,
           java.lang.String iniAtu,
           java.lang.Integer motPos,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.String obsPos,
           java.lang.String posTra,
           java.lang.Integer tipCol,
           java.lang.String tipOpe) {
           this.excHis = excHis;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.funEso = funEso;
           this.iniAtu = iniAtu;
           this.motPos = motPos;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.obsPos = obsPos;
           this.posTra = posTra;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the excHis value for this HistoricosPostoIn.
     * 
     * @return excHis
     */
    public java.lang.String getExcHis() {
        return excHis;
    }


    /**
     * Sets the excHis value for this HistoricosPostoIn.
     * 
     * @param excHis
     */
    public void setExcHis(java.lang.String excHis) {
        this.excHis = excHis;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosPostoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosPostoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosPostoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosPostoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the funEso value for this HistoricosPostoIn.
     * 
     * @return funEso
     */
    public java.lang.String getFunEso() {
        return funEso;
    }


    /**
     * Sets the funEso value for this HistoricosPostoIn.
     * 
     * @param funEso
     */
    public void setFunEso(java.lang.String funEso) {
        this.funEso = funEso;
    }


    /**
     * Gets the iniAtu value for this HistoricosPostoIn.
     * 
     * @return iniAtu
     */
    public java.lang.String getIniAtu() {
        return iniAtu;
    }


    /**
     * Sets the iniAtu value for this HistoricosPostoIn.
     * 
     * @param iniAtu
     */
    public void setIniAtu(java.lang.String iniAtu) {
        this.iniAtu = iniAtu;
    }


    /**
     * Gets the motPos value for this HistoricosPostoIn.
     * 
     * @return motPos
     */
    public java.lang.Integer getMotPos() {
        return motPos;
    }


    /**
     * Sets the motPos value for this HistoricosPostoIn.
     * 
     * @param motPos
     */
    public void setMotPos(java.lang.Integer motPos) {
        this.motPos = motPos;
    }


    /**
     * Gets the numCad value for this HistoricosPostoIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosPostoIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosPostoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosPostoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the obsPos value for this HistoricosPostoIn.
     * 
     * @return obsPos
     */
    public java.lang.String getObsPos() {
        return obsPos;
    }


    /**
     * Sets the obsPos value for this HistoricosPostoIn.
     * 
     * @param obsPos
     */
    public void setObsPos(java.lang.String obsPos) {
        this.obsPos = obsPos;
    }


    /**
     * Gets the posTra value for this HistoricosPostoIn.
     * 
     * @return posTra
     */
    public java.lang.String getPosTra() {
        return posTra;
    }


    /**
     * Sets the posTra value for this HistoricosPostoIn.
     * 
     * @param posTra
     */
    public void setPosTra(java.lang.String posTra) {
        this.posTra = posTra;
    }


    /**
     * Gets the tipCol value for this HistoricosPostoIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosPostoIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosPostoIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosPostoIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosPostoIn)) return false;
        HistoricosPostoIn other = (HistoricosPostoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.excHis==null && other.getExcHis()==null) || 
             (this.excHis!=null &&
              this.excHis.equals(other.getExcHis()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.funEso==null && other.getFunEso()==null) || 
             (this.funEso!=null &&
              this.funEso.equals(other.getFunEso()))) &&
            ((this.iniAtu==null && other.getIniAtu()==null) || 
             (this.iniAtu!=null &&
              this.iniAtu.equals(other.getIniAtu()))) &&
            ((this.motPos==null && other.getMotPos()==null) || 
             (this.motPos!=null &&
              this.motPos.equals(other.getMotPos()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.obsPos==null && other.getObsPos()==null) || 
             (this.obsPos!=null &&
              this.obsPos.equals(other.getObsPos()))) &&
            ((this.posTra==null && other.getPosTra()==null) || 
             (this.posTra!=null &&
              this.posTra.equals(other.getPosTra()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExcHis() != null) {
            _hashCode += getExcHis().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getFunEso() != null) {
            _hashCode += getFunEso().hashCode();
        }
        if (getIniAtu() != null) {
            _hashCode += getIniAtu().hashCode();
        }
        if (getMotPos() != null) {
            _hashCode += getMotPos().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getObsPos() != null) {
            _hashCode += getObsPos().hashCode();
        }
        if (getPosTra() != null) {
            _hashCode += getPosTra().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosPostoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosPostoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excHis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "excHis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("funEso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "funEso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motPos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motPos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsPos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsPos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "posTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
