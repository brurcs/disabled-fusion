/**
 * G5SeniorServicesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

import br.com.senior.services.vetorh.constants.WSConfigs;


public class G5SeniorServicesLocator extends org.apache.axis.client.Service implements br.com.senior.services.G5SeniorServices {

    public G5SeniorServicesLocator() {
    }


    public G5SeniorServicesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public G5SeniorServicesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort
    private java.lang.String ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort_address = "http://"+(new WSConfigs()).getHostService()+"/g5-senior-services/ronda_Synccom_senior_g5_rh_hr_calculoApuracao";

    public java.lang.String getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortAddress() {
        return ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortWSDDServiceName = "ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort";

    public java.lang.String getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortWSDDServiceName() {
        return ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortWSDDServiceName;
    }

    public void setronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortWSDDServiceName(java.lang.String name) {
        ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortWSDDServiceName = name;
    }

    public br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracao getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort(endpoint);
    }

    public br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracao getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortBindingStub _stub = new br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortBindingStub(portAddress, this);
            _stub.setPortName(getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortEndpointAddress(java.lang.String address) {
        ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracao.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortBindingStub _stub = new br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortBindingStub(new java.net.URL(ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort_address), this);
                _stub.setPortName(getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort".equals(inputPortName)) {
            return getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.senior.com.br", "g5-senior-services");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort".equals(portName)) {
            setronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
