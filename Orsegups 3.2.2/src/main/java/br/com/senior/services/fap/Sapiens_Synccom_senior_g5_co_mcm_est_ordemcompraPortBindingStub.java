/**
 * Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public class Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub extends org.apache.axis.client.Stub implements br.com.senior.services.fap.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[6];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("reprovar");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprareprovarIn"), br.com.senior.services.fap.OrdemcomprareprovarIn.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprareprovarOut"));
        oper.setReturnClass(br.com.senior.services.fap.OrdemcomprareprovarOut.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("aprovar");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcompraaprovarIn"), br.com.senior.services.fap.OrdemcompraaprovarIn.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcompraaprovarOut"));
        oper.setReturnClass(br.com.senior.services.fap.OrdemcompraaprovarOut.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("buscarPendentes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesIn"), br.com.senior.services.fap.OrdemcomprabuscarPendentesIn.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOut"));
        oper.setReturnClass(br.com.senior.services.fap.OrdemcomprabuscarPendentesOut.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("buscarPendentes_2");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2In"), br.com.senior.services.fap.OrdemcomprabuscarPendentes2In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2Out"));
        oper.setReturnClass(br.com.senior.services.fap.OrdemcomprabuscarPendentes2Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("buscarPendentes_3");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3In"), br.com.senior.services.fap.OrdemcomprabuscarPendentes3In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3Out"));
        oper.setReturnClass(br.com.senior.services.fap.OrdemcomprabuscarPendentes3Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("buscarPendentes_4");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes4In"), br.com.senior.services.fap.OrdemcomprabuscarPendentes4In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes4Out"));
        oper.setReturnClass(br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

    }

    public Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcompraaprovarIn");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcompraaprovarIn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcompraaprovarInOrdemCompra");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcompraaprovarInOrdemCompra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcompraaprovarOut");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcompraaprovarOut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcompraaprovarOutRetorno");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcompraaprovarOutRetorno.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes2In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes2Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2OutOrdemCompra");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes2OutOrdemCompra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2OutOrdemCompraItem");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes2OutOrdemCompraItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2OutOrdemCompraItemRateio");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes2OutOrdemCompraItemRateio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2OutOrdemCompraRateio");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes2OutOrdemCompraRateio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes3In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes3Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3OutOrdemCompra");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3OutOrdemCompraItem");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3OutOrdemCompraItemRateio");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraItemRateio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3OutOrdemCompraRateio");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraRateio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes4In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes4In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes4Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes4OutOrdemCompra");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes4OutOrdemCompra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes4OutOrdemCompraItem");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes4OutOrdemCompraItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes4OutOrdemCompraItemRateio");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes4OutOrdemCompraItemRateio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes4OutOrdemCompraRateio");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentes4OutOrdemCompraRateio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesIn");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentesIn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOut");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentesOut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompra");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompraItem");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompraItemRateio");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraItemRateio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompraRateio");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraRateio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprareprovarIn");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprareprovarIn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprareprovarInOrdemCompra");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprareprovarInOrdemCompra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprareprovarOut");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprareprovarOut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprareprovarOutRetorno");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.fap.OrdemcomprareprovarOutRetorno.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public br.com.senior.services.fap.OrdemcomprareprovarOut reprovar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprareprovarIn parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "reprovar"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.fap.OrdemcomprareprovarOut) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.fap.OrdemcomprareprovarOut) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.fap.OrdemcomprareprovarOut.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.fap.OrdemcompraaprovarOut aprovar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcompraaprovarIn parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "aprovar"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.fap.OrdemcompraaprovarOut) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.fap.OrdemcompraaprovarOut) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.fap.OrdemcompraaprovarOut.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.fap.OrdemcomprabuscarPendentesOut buscarPendentes(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentesIn parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "buscarPendentes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.fap.OrdemcomprabuscarPendentesOut) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.fap.OrdemcomprabuscarPendentesOut) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.fap.OrdemcomprabuscarPendentesOut.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.fap.OrdemcomprabuscarPendentes2Out buscarPendentes_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes2In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "buscarPendentes_2"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.fap.OrdemcomprabuscarPendentes2Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.fap.OrdemcomprabuscarPendentes2Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.fap.OrdemcomprabuscarPendentes2Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.fap.OrdemcomprabuscarPendentes3Out buscarPendentes_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes3In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "buscarPendentes_3"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.fap.OrdemcomprabuscarPendentes3Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.fap.OrdemcomprabuscarPendentes3Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.fap.OrdemcomprabuscarPendentes3Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out buscarPendentes_4(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes4In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "buscarPendentes_4"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
