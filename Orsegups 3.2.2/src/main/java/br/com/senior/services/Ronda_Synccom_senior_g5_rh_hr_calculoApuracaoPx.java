package br.com.senior.services;

public class Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx implements br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracao {
  private String _endpoint = null;
  private br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracao ronda_Synccom_senior_g5_rh_hr_calculoApuracao = null;
  
  public Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx() {
    _initRonda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
  }
  
  public Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx(String endpoint) {
    _endpoint = endpoint;
    _initRonda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
  }
  
  private void _initRonda_Synccom_senior_g5_rh_hr_calculoApuracaoPx() {
    try {
      ronda_Synccom_senior_g5_rh_hr_calculoApuracao = (new br.com.senior.services.G5SeniorServicesLocator()).getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort();
      if (ronda_Synccom_senior_g5_rh_hr_calculoApuracao != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ronda_Synccom_senior_g5_rh_hr_calculoApuracao)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ronda_Synccom_senior_g5_rh_hr_calculoApuracao)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ronda_Synccom_senior_g5_rh_hr_calculoApuracao != null)
      ((javax.xml.rpc.Stub)ronda_Synccom_senior_g5_rh_hr_calculoApuracao)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracao getRonda_Synccom_senior_g5_rh_hr_calculoApuracao() {
    if (ronda_Synccom_senior_g5_rh_hr_calculoApuracao == null)
      _initRonda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
    return ronda_Synccom_senior_g5_rh_hr_calculoApuracao;
  }
  
  public br.com.senior.services.CalculoApuracaoCalcularOut calcular(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.CalculoApuracaoCalcularIn parameters) throws java.rmi.RemoteException{
    if (ronda_Synccom_senior_g5_rh_hr_calculoApuracao == null)
      _initRonda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
    return ronda_Synccom_senior_g5_rh_hr_calculoApuracao.calcular(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.CalculoApuracaoGravarOut gravar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.CalculoApuracaoGravarIn parameters) throws java.rmi.RemoteException{
    if (ronda_Synccom_senior_g5_rh_hr_calculoApuracao == null)
      _initRonda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
    return ronda_Synccom_senior_g5_rh_hr_calculoApuracao.gravar(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.CalculoApuracaoEscalonarOut escalonar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.CalculoApuracaoEscalonarIn parameters) throws java.rmi.RemoteException{
    if (ronda_Synccom_senior_g5_rh_hr_calculoApuracao == null)
      _initRonda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
    return ronda_Synccom_senior_g5_rh_hr_calculoApuracao.escalonar(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.CalculoApuracaoAcertarOut acertar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.CalculoApuracaoAcertarIn parameters) throws java.rmi.RemoteException{
    if (ronda_Synccom_senior_g5_rh_hr_calculoApuracao == null)
      _initRonda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
    return ronda_Synccom_senior_g5_rh_hr_calculoApuracao.acertar(user, password, encryption, parameters);
  }
  
  
}