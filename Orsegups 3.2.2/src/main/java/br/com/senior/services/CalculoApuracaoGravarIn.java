/**
 * CalculoApuracaoGravarIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class CalculoApuracaoGravarIn  implements java.io.Serializable {
    private java.lang.Integer codBnf;

    private java.lang.Integer codDsp;

    private java.lang.Integer codFnc;

    private java.lang.Integer codPlt;

    private java.lang.Integer codRef;

    private java.lang.Integer codRlg;

    private java.lang.Integer codSor;

    private java.lang.String datAcc;

    private java.lang.String datApu;

    private java.lang.String dirAcc;

    private java.lang.String excPon;

    private java.lang.Integer flaAcc;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String horAcc;

    private java.lang.Integer numCad;

    private java.lang.String numCra;

    private java.lang.Integer numEmp;

    private java.lang.String oriAcc;

    private java.lang.Double qtdAcc;

    private java.lang.Integer seqAcc;

    private java.lang.Integer staRlg;

    private java.lang.Integer tipAcc;

    private java.lang.Integer tipCol;

    private java.lang.Integer usoMar;

    private java.lang.Integer usoRef;

    private java.lang.Double valRef;

    public CalculoApuracaoGravarIn() {
    }

    public CalculoApuracaoGravarIn(
           java.lang.Integer codBnf,
           java.lang.Integer codDsp,
           java.lang.Integer codFnc,
           java.lang.Integer codPlt,
           java.lang.Integer codRef,
           java.lang.Integer codRlg,
           java.lang.Integer codSor,
           java.lang.String datAcc,
           java.lang.String datApu,
           java.lang.String dirAcc,
           java.lang.String excPon,
           java.lang.Integer flaAcc,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String horAcc,
           java.lang.Integer numCad,
           java.lang.String numCra,
           java.lang.Integer numEmp,
           java.lang.String oriAcc,
           java.lang.Double qtdAcc,
           java.lang.Integer seqAcc,
           java.lang.Integer staRlg,
           java.lang.Integer tipAcc,
           java.lang.Integer tipCol,
           java.lang.Integer usoMar,
           java.lang.Integer usoRef,
           java.lang.Double valRef) {
           this.codBnf = codBnf;
           this.codDsp = codDsp;
           this.codFnc = codFnc;
           this.codPlt = codPlt;
           this.codRef = codRef;
           this.codRlg = codRlg;
           this.codSor = codSor;
           this.datAcc = datAcc;
           this.datApu = datApu;
           this.dirAcc = dirAcc;
           this.excPon = excPon;
           this.flaAcc = flaAcc;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.horAcc = horAcc;
           this.numCad = numCad;
           this.numCra = numCra;
           this.numEmp = numEmp;
           this.oriAcc = oriAcc;
           this.qtdAcc = qtdAcc;
           this.seqAcc = seqAcc;
           this.staRlg = staRlg;
           this.tipAcc = tipAcc;
           this.tipCol = tipCol;
           this.usoMar = usoMar;
           this.usoRef = usoRef;
           this.valRef = valRef;
    }


    /**
     * Gets the codBnf value for this CalculoApuracaoGravarIn.
     * 
     * @return codBnf
     */
    public java.lang.Integer getCodBnf() {
        return codBnf;
    }


    /**
     * Sets the codBnf value for this CalculoApuracaoGravarIn.
     * 
     * @param codBnf
     */
    public void setCodBnf(java.lang.Integer codBnf) {
        this.codBnf = codBnf;
    }


    /**
     * Gets the codDsp value for this CalculoApuracaoGravarIn.
     * 
     * @return codDsp
     */
    public java.lang.Integer getCodDsp() {
        return codDsp;
    }


    /**
     * Sets the codDsp value for this CalculoApuracaoGravarIn.
     * 
     * @param codDsp
     */
    public void setCodDsp(java.lang.Integer codDsp) {
        this.codDsp = codDsp;
    }


    /**
     * Gets the codFnc value for this CalculoApuracaoGravarIn.
     * 
     * @return codFnc
     */
    public java.lang.Integer getCodFnc() {
        return codFnc;
    }


    /**
     * Sets the codFnc value for this CalculoApuracaoGravarIn.
     * 
     * @param codFnc
     */
    public void setCodFnc(java.lang.Integer codFnc) {
        this.codFnc = codFnc;
    }


    /**
     * Gets the codPlt value for this CalculoApuracaoGravarIn.
     * 
     * @return codPlt
     */
    public java.lang.Integer getCodPlt() {
        return codPlt;
    }


    /**
     * Sets the codPlt value for this CalculoApuracaoGravarIn.
     * 
     * @param codPlt
     */
    public void setCodPlt(java.lang.Integer codPlt) {
        this.codPlt = codPlt;
    }


    /**
     * Gets the codRef value for this CalculoApuracaoGravarIn.
     * 
     * @return codRef
     */
    public java.lang.Integer getCodRef() {
        return codRef;
    }


    /**
     * Sets the codRef value for this CalculoApuracaoGravarIn.
     * 
     * @param codRef
     */
    public void setCodRef(java.lang.Integer codRef) {
        this.codRef = codRef;
    }


    /**
     * Gets the codRlg value for this CalculoApuracaoGravarIn.
     * 
     * @return codRlg
     */
    public java.lang.Integer getCodRlg() {
        return codRlg;
    }


    /**
     * Sets the codRlg value for this CalculoApuracaoGravarIn.
     * 
     * @param codRlg
     */
    public void setCodRlg(java.lang.Integer codRlg) {
        this.codRlg = codRlg;
    }


    /**
     * Gets the codSor value for this CalculoApuracaoGravarIn.
     * 
     * @return codSor
     */
    public java.lang.Integer getCodSor() {
        return codSor;
    }


    /**
     * Sets the codSor value for this CalculoApuracaoGravarIn.
     * 
     * @param codSor
     */
    public void setCodSor(java.lang.Integer codSor) {
        this.codSor = codSor;
    }


    /**
     * Gets the datAcc value for this CalculoApuracaoGravarIn.
     * 
     * @return datAcc
     */
    public java.lang.String getDatAcc() {
        return datAcc;
    }


    /**
     * Sets the datAcc value for this CalculoApuracaoGravarIn.
     * 
     * @param datAcc
     */
    public void setDatAcc(java.lang.String datAcc) {
        this.datAcc = datAcc;
    }


    /**
     * Gets the datApu value for this CalculoApuracaoGravarIn.
     * 
     * @return datApu
     */
    public java.lang.String getDatApu() {
        return datApu;
    }


    /**
     * Sets the datApu value for this CalculoApuracaoGravarIn.
     * 
     * @param datApu
     */
    public void setDatApu(java.lang.String datApu) {
        this.datApu = datApu;
    }


    /**
     * Gets the dirAcc value for this CalculoApuracaoGravarIn.
     * 
     * @return dirAcc
     */
    public java.lang.String getDirAcc() {
        return dirAcc;
    }


    /**
     * Sets the dirAcc value for this CalculoApuracaoGravarIn.
     * 
     * @param dirAcc
     */
    public void setDirAcc(java.lang.String dirAcc) {
        this.dirAcc = dirAcc;
    }


    /**
     * Gets the excPon value for this CalculoApuracaoGravarIn.
     * 
     * @return excPon
     */
    public java.lang.String getExcPon() {
        return excPon;
    }


    /**
     * Sets the excPon value for this CalculoApuracaoGravarIn.
     * 
     * @param excPon
     */
    public void setExcPon(java.lang.String excPon) {
        this.excPon = excPon;
    }


    /**
     * Gets the flaAcc value for this CalculoApuracaoGravarIn.
     * 
     * @return flaAcc
     */
    public java.lang.Integer getFlaAcc() {
        return flaAcc;
    }


    /**
     * Sets the flaAcc value for this CalculoApuracaoGravarIn.
     * 
     * @param flaAcc
     */
    public void setFlaAcc(java.lang.Integer flaAcc) {
        this.flaAcc = flaAcc;
    }


    /**
     * Gets the flowInstanceID value for this CalculoApuracaoGravarIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this CalculoApuracaoGravarIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this CalculoApuracaoGravarIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this CalculoApuracaoGravarIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the horAcc value for this CalculoApuracaoGravarIn.
     * 
     * @return horAcc
     */
    public java.lang.String getHorAcc() {
        return horAcc;
    }


    /**
     * Sets the horAcc value for this CalculoApuracaoGravarIn.
     * 
     * @param horAcc
     */
    public void setHorAcc(java.lang.String horAcc) {
        this.horAcc = horAcc;
    }


    /**
     * Gets the numCad value for this CalculoApuracaoGravarIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this CalculoApuracaoGravarIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numCra value for this CalculoApuracaoGravarIn.
     * 
     * @return numCra
     */
    public java.lang.String getNumCra() {
        return numCra;
    }


    /**
     * Sets the numCra value for this CalculoApuracaoGravarIn.
     * 
     * @param numCra
     */
    public void setNumCra(java.lang.String numCra) {
        this.numCra = numCra;
    }


    /**
     * Gets the numEmp value for this CalculoApuracaoGravarIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this CalculoApuracaoGravarIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the oriAcc value for this CalculoApuracaoGravarIn.
     * 
     * @return oriAcc
     */
    public java.lang.String getOriAcc() {
        return oriAcc;
    }


    /**
     * Sets the oriAcc value for this CalculoApuracaoGravarIn.
     * 
     * @param oriAcc
     */
    public void setOriAcc(java.lang.String oriAcc) {
        this.oriAcc = oriAcc;
    }


    /**
     * Gets the qtdAcc value for this CalculoApuracaoGravarIn.
     * 
     * @return qtdAcc
     */
    public java.lang.Double getQtdAcc() {
        return qtdAcc;
    }


    /**
     * Sets the qtdAcc value for this CalculoApuracaoGravarIn.
     * 
     * @param qtdAcc
     */
    public void setQtdAcc(java.lang.Double qtdAcc) {
        this.qtdAcc = qtdAcc;
    }


    /**
     * Gets the seqAcc value for this CalculoApuracaoGravarIn.
     * 
     * @return seqAcc
     */
    public java.lang.Integer getSeqAcc() {
        return seqAcc;
    }


    /**
     * Sets the seqAcc value for this CalculoApuracaoGravarIn.
     * 
     * @param seqAcc
     */
    public void setSeqAcc(java.lang.Integer seqAcc) {
        this.seqAcc = seqAcc;
    }


    /**
     * Gets the staRlg value for this CalculoApuracaoGravarIn.
     * 
     * @return staRlg
     */
    public java.lang.Integer getStaRlg() {
        return staRlg;
    }


    /**
     * Sets the staRlg value for this CalculoApuracaoGravarIn.
     * 
     * @param staRlg
     */
    public void setStaRlg(java.lang.Integer staRlg) {
        this.staRlg = staRlg;
    }


    /**
     * Gets the tipAcc value for this CalculoApuracaoGravarIn.
     * 
     * @return tipAcc
     */
    public java.lang.Integer getTipAcc() {
        return tipAcc;
    }


    /**
     * Sets the tipAcc value for this CalculoApuracaoGravarIn.
     * 
     * @param tipAcc
     */
    public void setTipAcc(java.lang.Integer tipAcc) {
        this.tipAcc = tipAcc;
    }


    /**
     * Gets the tipCol value for this CalculoApuracaoGravarIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this CalculoApuracaoGravarIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the usoMar value for this CalculoApuracaoGravarIn.
     * 
     * @return usoMar
     */
    public java.lang.Integer getUsoMar() {
        return usoMar;
    }


    /**
     * Sets the usoMar value for this CalculoApuracaoGravarIn.
     * 
     * @param usoMar
     */
    public void setUsoMar(java.lang.Integer usoMar) {
        this.usoMar = usoMar;
    }


    /**
     * Gets the usoRef value for this CalculoApuracaoGravarIn.
     * 
     * @return usoRef
     */
    public java.lang.Integer getUsoRef() {
        return usoRef;
    }


    /**
     * Sets the usoRef value for this CalculoApuracaoGravarIn.
     * 
     * @param usoRef
     */
    public void setUsoRef(java.lang.Integer usoRef) {
        this.usoRef = usoRef;
    }


    /**
     * Gets the valRef value for this CalculoApuracaoGravarIn.
     * 
     * @return valRef
     */
    public java.lang.Double getValRef() {
        return valRef;
    }


    /**
     * Sets the valRef value for this CalculoApuracaoGravarIn.
     * 
     * @param valRef
     */
    public void setValRef(java.lang.Double valRef) {
        this.valRef = valRef;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalculoApuracaoGravarIn)) return false;
        CalculoApuracaoGravarIn other = (CalculoApuracaoGravarIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codBnf==null && other.getCodBnf()==null) || 
             (this.codBnf!=null &&
              this.codBnf.equals(other.getCodBnf()))) &&
            ((this.codDsp==null && other.getCodDsp()==null) || 
             (this.codDsp!=null &&
              this.codDsp.equals(other.getCodDsp()))) &&
            ((this.codFnc==null && other.getCodFnc()==null) || 
             (this.codFnc!=null &&
              this.codFnc.equals(other.getCodFnc()))) &&
            ((this.codPlt==null && other.getCodPlt()==null) || 
             (this.codPlt!=null &&
              this.codPlt.equals(other.getCodPlt()))) &&
            ((this.codRef==null && other.getCodRef()==null) || 
             (this.codRef!=null &&
              this.codRef.equals(other.getCodRef()))) &&
            ((this.codRlg==null && other.getCodRlg()==null) || 
             (this.codRlg!=null &&
              this.codRlg.equals(other.getCodRlg()))) &&
            ((this.codSor==null && other.getCodSor()==null) || 
             (this.codSor!=null &&
              this.codSor.equals(other.getCodSor()))) &&
            ((this.datAcc==null && other.getDatAcc()==null) || 
             (this.datAcc!=null &&
              this.datAcc.equals(other.getDatAcc()))) &&
            ((this.datApu==null && other.getDatApu()==null) || 
             (this.datApu!=null &&
              this.datApu.equals(other.getDatApu()))) &&
            ((this.dirAcc==null && other.getDirAcc()==null) || 
             (this.dirAcc!=null &&
              this.dirAcc.equals(other.getDirAcc()))) &&
            ((this.excPon==null && other.getExcPon()==null) || 
             (this.excPon!=null &&
              this.excPon.equals(other.getExcPon()))) &&
            ((this.flaAcc==null && other.getFlaAcc()==null) || 
             (this.flaAcc!=null &&
              this.flaAcc.equals(other.getFlaAcc()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.horAcc==null && other.getHorAcc()==null) || 
             (this.horAcc!=null &&
              this.horAcc.equals(other.getHorAcc()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numCra==null && other.getNumCra()==null) || 
             (this.numCra!=null &&
              this.numCra.equals(other.getNumCra()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.oriAcc==null && other.getOriAcc()==null) || 
             (this.oriAcc!=null &&
              this.oriAcc.equals(other.getOriAcc()))) &&
            ((this.qtdAcc==null && other.getQtdAcc()==null) || 
             (this.qtdAcc!=null &&
              this.qtdAcc.equals(other.getQtdAcc()))) &&
            ((this.seqAcc==null && other.getSeqAcc()==null) || 
             (this.seqAcc!=null &&
              this.seqAcc.equals(other.getSeqAcc()))) &&
            ((this.staRlg==null && other.getStaRlg()==null) || 
             (this.staRlg!=null &&
              this.staRlg.equals(other.getStaRlg()))) &&
            ((this.tipAcc==null && other.getTipAcc()==null) || 
             (this.tipAcc!=null &&
              this.tipAcc.equals(other.getTipAcc()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.usoMar==null && other.getUsoMar()==null) || 
             (this.usoMar!=null &&
              this.usoMar.equals(other.getUsoMar()))) &&
            ((this.usoRef==null && other.getUsoRef()==null) || 
             (this.usoRef!=null &&
              this.usoRef.equals(other.getUsoRef()))) &&
            ((this.valRef==null && other.getValRef()==null) || 
             (this.valRef!=null &&
              this.valRef.equals(other.getValRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodBnf() != null) {
            _hashCode += getCodBnf().hashCode();
        }
        if (getCodDsp() != null) {
            _hashCode += getCodDsp().hashCode();
        }
        if (getCodFnc() != null) {
            _hashCode += getCodFnc().hashCode();
        }
        if (getCodPlt() != null) {
            _hashCode += getCodPlt().hashCode();
        }
        if (getCodRef() != null) {
            _hashCode += getCodRef().hashCode();
        }
        if (getCodRlg() != null) {
            _hashCode += getCodRlg().hashCode();
        }
        if (getCodSor() != null) {
            _hashCode += getCodSor().hashCode();
        }
        if (getDatAcc() != null) {
            _hashCode += getDatAcc().hashCode();
        }
        if (getDatApu() != null) {
            _hashCode += getDatApu().hashCode();
        }
        if (getDirAcc() != null) {
            _hashCode += getDirAcc().hashCode();
        }
        if (getExcPon() != null) {
            _hashCode += getExcPon().hashCode();
        }
        if (getFlaAcc() != null) {
            _hashCode += getFlaAcc().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getHorAcc() != null) {
            _hashCode += getHorAcc().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumCra() != null) {
            _hashCode += getNumCra().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getOriAcc() != null) {
            _hashCode += getOriAcc().hashCode();
        }
        if (getQtdAcc() != null) {
            _hashCode += getQtdAcc().hashCode();
        }
        if (getSeqAcc() != null) {
            _hashCode += getSeqAcc().hashCode();
        }
        if (getStaRlg() != null) {
            _hashCode += getStaRlg().hashCode();
        }
        if (getTipAcc() != null) {
            _hashCode += getTipAcc().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getUsoMar() != null) {
            _hashCode += getUsoMar().hashCode();
        }
        if (getUsoRef() != null) {
            _hashCode += getUsoRef().hashCode();
        }
        if (getValRef() != null) {
            _hashCode += getValRef().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalculoApuracaoGravarIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoGravarIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBnf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBnf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRlg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRlg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datApu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datApu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dirAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dirAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excPon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "excPon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flaAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flaAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oriAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "oriAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("staRlg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "staRlg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usoMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usoMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usoRef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usoRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valRef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
