/**
 * DependentesNascimentoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.dependentes;

public class DependentesNascimentoIn  implements java.io.Serializable {
    private java.lang.Integer codDep;

    private java.lang.String entCer;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer locNas;

    private java.lang.String matNas;

    private java.lang.String nasViv;

    private java.lang.String nomCar;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.String numFol;

    private java.lang.String numLiv;

    private java.lang.String numReg;

    private java.lang.Integer tipCol;

    public DependentesNascimentoIn() {
    }

    public DependentesNascimentoIn(
           java.lang.Integer codDep,
           java.lang.String entCer,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer locNas,
           java.lang.String matNas,
           java.lang.String nasViv,
           java.lang.String nomCar,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.String numFol,
           java.lang.String numLiv,
           java.lang.String numReg,
           java.lang.Integer tipCol) {
           this.codDep = codDep;
           this.entCer = entCer;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.locNas = locNas;
           this.matNas = matNas;
           this.nasViv = nasViv;
           this.nomCar = nomCar;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.numFol = numFol;
           this.numLiv = numLiv;
           this.numReg = numReg;
           this.tipCol = tipCol;
    }


    /**
     * Gets the codDep value for this DependentesNascimentoIn.
     * 
     * @return codDep
     */
    public java.lang.Integer getCodDep() {
        return codDep;
    }


    /**
     * Sets the codDep value for this DependentesNascimentoIn.
     * 
     * @param codDep
     */
    public void setCodDep(java.lang.Integer codDep) {
        this.codDep = codDep;
    }


    /**
     * Gets the entCer value for this DependentesNascimentoIn.
     * 
     * @return entCer
     */
    public java.lang.String getEntCer() {
        return entCer;
    }


    /**
     * Sets the entCer value for this DependentesNascimentoIn.
     * 
     * @param entCer
     */
    public void setEntCer(java.lang.String entCer) {
        this.entCer = entCer;
    }


    /**
     * Gets the flowInstanceID value for this DependentesNascimentoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this DependentesNascimentoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this DependentesNascimentoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this DependentesNascimentoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the locNas value for this DependentesNascimentoIn.
     * 
     * @return locNas
     */
    public java.lang.Integer getLocNas() {
        return locNas;
    }


    /**
     * Sets the locNas value for this DependentesNascimentoIn.
     * 
     * @param locNas
     */
    public void setLocNas(java.lang.Integer locNas) {
        this.locNas = locNas;
    }


    /**
     * Gets the matNas value for this DependentesNascimentoIn.
     * 
     * @return matNas
     */
    public java.lang.String getMatNas() {
        return matNas;
    }


    /**
     * Sets the matNas value for this DependentesNascimentoIn.
     * 
     * @param matNas
     */
    public void setMatNas(java.lang.String matNas) {
        this.matNas = matNas;
    }


    /**
     * Gets the nasViv value for this DependentesNascimentoIn.
     * 
     * @return nasViv
     */
    public java.lang.String getNasViv() {
        return nasViv;
    }


    /**
     * Sets the nasViv value for this DependentesNascimentoIn.
     * 
     * @param nasViv
     */
    public void setNasViv(java.lang.String nasViv) {
        this.nasViv = nasViv;
    }


    /**
     * Gets the nomCar value for this DependentesNascimentoIn.
     * 
     * @return nomCar
     */
    public java.lang.String getNomCar() {
        return nomCar;
    }


    /**
     * Sets the nomCar value for this DependentesNascimentoIn.
     * 
     * @param nomCar
     */
    public void setNomCar(java.lang.String nomCar) {
        this.nomCar = nomCar;
    }


    /**
     * Gets the numCad value for this DependentesNascimentoIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this DependentesNascimentoIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this DependentesNascimentoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this DependentesNascimentoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the numFol value for this DependentesNascimentoIn.
     * 
     * @return numFol
     */
    public java.lang.String getNumFol() {
        return numFol;
    }


    /**
     * Sets the numFol value for this DependentesNascimentoIn.
     * 
     * @param numFol
     */
    public void setNumFol(java.lang.String numFol) {
        this.numFol = numFol;
    }


    /**
     * Gets the numLiv value for this DependentesNascimentoIn.
     * 
     * @return numLiv
     */
    public java.lang.String getNumLiv() {
        return numLiv;
    }


    /**
     * Sets the numLiv value for this DependentesNascimentoIn.
     * 
     * @param numLiv
     */
    public void setNumLiv(java.lang.String numLiv) {
        this.numLiv = numLiv;
    }


    /**
     * Gets the numReg value for this DependentesNascimentoIn.
     * 
     * @return numReg
     */
    public java.lang.String getNumReg() {
        return numReg;
    }


    /**
     * Sets the numReg value for this DependentesNascimentoIn.
     * 
     * @param numReg
     */
    public void setNumReg(java.lang.String numReg) {
        this.numReg = numReg;
    }


    /**
     * Gets the tipCol value for this DependentesNascimentoIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this DependentesNascimentoIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DependentesNascimentoIn)) return false;
        DependentesNascimentoIn other = (DependentesNascimentoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codDep==null && other.getCodDep()==null) || 
             (this.codDep!=null &&
              this.codDep.equals(other.getCodDep()))) &&
            ((this.entCer==null && other.getEntCer()==null) || 
             (this.entCer!=null &&
              this.entCer.equals(other.getEntCer()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.locNas==null && other.getLocNas()==null) || 
             (this.locNas!=null &&
              this.locNas.equals(other.getLocNas()))) &&
            ((this.matNas==null && other.getMatNas()==null) || 
             (this.matNas!=null &&
              this.matNas.equals(other.getMatNas()))) &&
            ((this.nasViv==null && other.getNasViv()==null) || 
             (this.nasViv!=null &&
              this.nasViv.equals(other.getNasViv()))) &&
            ((this.nomCar==null && other.getNomCar()==null) || 
             (this.nomCar!=null &&
              this.nomCar.equals(other.getNomCar()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.numFol==null && other.getNumFol()==null) || 
             (this.numFol!=null &&
              this.numFol.equals(other.getNumFol()))) &&
            ((this.numLiv==null && other.getNumLiv()==null) || 
             (this.numLiv!=null &&
              this.numLiv.equals(other.getNumLiv()))) &&
            ((this.numReg==null && other.getNumReg()==null) || 
             (this.numReg!=null &&
              this.numReg.equals(other.getNumReg()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodDep() != null) {
            _hashCode += getCodDep().hashCode();
        }
        if (getEntCer() != null) {
            _hashCode += getEntCer().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getLocNas() != null) {
            _hashCode += getLocNas().hashCode();
        }
        if (getMatNas() != null) {
            _hashCode += getMatNas().hashCode();
        }
        if (getNasViv() != null) {
            _hashCode += getNasViv().hashCode();
        }
        if (getNomCar() != null) {
            _hashCode += getNomCar().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getNumFol() != null) {
            _hashCode += getNumFol().hashCode();
        }
        if (getNumLiv() != null) {
            _hashCode += getNumLiv().hashCode();
        }
        if (getNumReg() != null) {
            _hashCode += getNumReg().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DependentesNascimentoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "dependentesNascimentoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entCer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entCer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "locNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "matNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nasViv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nasViv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numFol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numFol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numLiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numLiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numReg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numReg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
