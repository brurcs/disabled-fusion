/**
 * DependentesComprovanteInComprovantes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.dependentes;

public class DependentesComprovanteInComprovantes  implements java.io.Serializable {
    private java.lang.Integer anoEsc;

    private java.lang.Integer codOem;

    private java.lang.String datVac;

    private java.lang.String depBol;

    private java.lang.String entMai;

    private java.lang.String entNov;

    private java.lang.Double perMai;

    private java.lang.Double perNov;

    private java.lang.Integer serIns;

    private java.lang.String tipOpe;

    public DependentesComprovanteInComprovantes() {
    }

    public DependentesComprovanteInComprovantes(
           java.lang.Integer anoEsc,
           java.lang.Integer codOem,
           java.lang.String datVac,
           java.lang.String depBol,
           java.lang.String entMai,
           java.lang.String entNov,
           java.lang.Double perMai,
           java.lang.Double perNov,
           java.lang.Integer serIns,
           java.lang.String tipOpe) {
           this.anoEsc = anoEsc;
           this.codOem = codOem;
           this.datVac = datVac;
           this.depBol = depBol;
           this.entMai = entMai;
           this.entNov = entNov;
           this.perMai = perMai;
           this.perNov = perNov;
           this.serIns = serIns;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the anoEsc value for this DependentesComprovanteInComprovantes.
     * 
     * @return anoEsc
     */
    public java.lang.Integer getAnoEsc() {
        return anoEsc;
    }


    /**
     * Sets the anoEsc value for this DependentesComprovanteInComprovantes.
     * 
     * @param anoEsc
     */
    public void setAnoEsc(java.lang.Integer anoEsc) {
        this.anoEsc = anoEsc;
    }


    /**
     * Gets the codOem value for this DependentesComprovanteInComprovantes.
     * 
     * @return codOem
     */
    public java.lang.Integer getCodOem() {
        return codOem;
    }


    /**
     * Sets the codOem value for this DependentesComprovanteInComprovantes.
     * 
     * @param codOem
     */
    public void setCodOem(java.lang.Integer codOem) {
        this.codOem = codOem;
    }


    /**
     * Gets the datVac value for this DependentesComprovanteInComprovantes.
     * 
     * @return datVac
     */
    public java.lang.String getDatVac() {
        return datVac;
    }


    /**
     * Sets the datVac value for this DependentesComprovanteInComprovantes.
     * 
     * @param datVac
     */
    public void setDatVac(java.lang.String datVac) {
        this.datVac = datVac;
    }


    /**
     * Gets the depBol value for this DependentesComprovanteInComprovantes.
     * 
     * @return depBol
     */
    public java.lang.String getDepBol() {
        return depBol;
    }


    /**
     * Sets the depBol value for this DependentesComprovanteInComprovantes.
     * 
     * @param depBol
     */
    public void setDepBol(java.lang.String depBol) {
        this.depBol = depBol;
    }


    /**
     * Gets the entMai value for this DependentesComprovanteInComprovantes.
     * 
     * @return entMai
     */
    public java.lang.String getEntMai() {
        return entMai;
    }


    /**
     * Sets the entMai value for this DependentesComprovanteInComprovantes.
     * 
     * @param entMai
     */
    public void setEntMai(java.lang.String entMai) {
        this.entMai = entMai;
    }


    /**
     * Gets the entNov value for this DependentesComprovanteInComprovantes.
     * 
     * @return entNov
     */
    public java.lang.String getEntNov() {
        return entNov;
    }


    /**
     * Sets the entNov value for this DependentesComprovanteInComprovantes.
     * 
     * @param entNov
     */
    public void setEntNov(java.lang.String entNov) {
        this.entNov = entNov;
    }


    /**
     * Gets the perMai value for this DependentesComprovanteInComprovantes.
     * 
     * @return perMai
     */
    public java.lang.Double getPerMai() {
        return perMai;
    }


    /**
     * Sets the perMai value for this DependentesComprovanteInComprovantes.
     * 
     * @param perMai
     */
    public void setPerMai(java.lang.Double perMai) {
        this.perMai = perMai;
    }


    /**
     * Gets the perNov value for this DependentesComprovanteInComprovantes.
     * 
     * @return perNov
     */
    public java.lang.Double getPerNov() {
        return perNov;
    }


    /**
     * Sets the perNov value for this DependentesComprovanteInComprovantes.
     * 
     * @param perNov
     */
    public void setPerNov(java.lang.Double perNov) {
        this.perNov = perNov;
    }


    /**
     * Gets the serIns value for this DependentesComprovanteInComprovantes.
     * 
     * @return serIns
     */
    public java.lang.Integer getSerIns() {
        return serIns;
    }


    /**
     * Sets the serIns value for this DependentesComprovanteInComprovantes.
     * 
     * @param serIns
     */
    public void setSerIns(java.lang.Integer serIns) {
        this.serIns = serIns;
    }


    /**
     * Gets the tipOpe value for this DependentesComprovanteInComprovantes.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this DependentesComprovanteInComprovantes.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DependentesComprovanteInComprovantes)) return false;
        DependentesComprovanteInComprovantes other = (DependentesComprovanteInComprovantes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.anoEsc==null && other.getAnoEsc()==null) || 
             (this.anoEsc!=null &&
              this.anoEsc.equals(other.getAnoEsc()))) &&
            ((this.codOem==null && other.getCodOem()==null) || 
             (this.codOem!=null &&
              this.codOem.equals(other.getCodOem()))) &&
            ((this.datVac==null && other.getDatVac()==null) || 
             (this.datVac!=null &&
              this.datVac.equals(other.getDatVac()))) &&
            ((this.depBol==null && other.getDepBol()==null) || 
             (this.depBol!=null &&
              this.depBol.equals(other.getDepBol()))) &&
            ((this.entMai==null && other.getEntMai()==null) || 
             (this.entMai!=null &&
              this.entMai.equals(other.getEntMai()))) &&
            ((this.entNov==null && other.getEntNov()==null) || 
             (this.entNov!=null &&
              this.entNov.equals(other.getEntNov()))) &&
            ((this.perMai==null && other.getPerMai()==null) || 
             (this.perMai!=null &&
              this.perMai.equals(other.getPerMai()))) &&
            ((this.perNov==null && other.getPerNov()==null) || 
             (this.perNov!=null &&
              this.perNov.equals(other.getPerNov()))) &&
            ((this.serIns==null && other.getSerIns()==null) || 
             (this.serIns!=null &&
              this.serIns.equals(other.getSerIns()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAnoEsc() != null) {
            _hashCode += getAnoEsc().hashCode();
        }
        if (getCodOem() != null) {
            _hashCode += getCodOem().hashCode();
        }
        if (getDatVac() != null) {
            _hashCode += getDatVac().hashCode();
        }
        if (getDepBol() != null) {
            _hashCode += getDepBol().hashCode();
        }
        if (getEntMai() != null) {
            _hashCode += getEntMai().hashCode();
        }
        if (getEntNov() != null) {
            _hashCode += getEntNov().hashCode();
        }
        if (getPerMai() != null) {
            _hashCode += getPerMai().hashCode();
        }
        if (getPerNov() != null) {
            _hashCode += getPerNov().hashCode();
        }
        if (getSerIns() != null) {
            _hashCode += getSerIns().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DependentesComprovanteInComprovantes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "dependentesComprovanteInComprovantes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anoEsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "anoEsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codOem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codOem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datVac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datVac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depBol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "depBol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entMai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entMai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entNov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entNov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perMai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perMai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perNov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perNov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serIns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serIns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
