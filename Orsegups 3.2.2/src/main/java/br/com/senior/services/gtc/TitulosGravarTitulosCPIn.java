/**
 * TitulosGravarTitulosCPIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosGravarTitulosCPIn  implements java.io.Serializable {
    private java.lang.String dataBuild;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private TitulosGravarTitulosCPInTitulos[] titulos;

    public TitulosGravarTitulosCPIn() {
    }

    public TitulosGravarTitulosCPIn(
           java.lang.String dataBuild,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           TitulosGravarTitulosCPInTitulos[] titulos) {
           this.dataBuild = dataBuild;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.titulos = titulos;
    }


    /**
     * Gets the dataBuild value for this TitulosGravarTitulosCPIn.
     * 
     * @return dataBuild
     */
    public java.lang.String getDataBuild() {
        return dataBuild;
    }


    /**
     * Sets the dataBuild value for this TitulosGravarTitulosCPIn.
     * 
     * @param dataBuild
     */
    public void setDataBuild(java.lang.String dataBuild) {
        this.dataBuild = dataBuild;
    }


    /**
     * Gets the flowInstanceID value for this TitulosGravarTitulosCPIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosGravarTitulosCPIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosGravarTitulosCPIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosGravarTitulosCPIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the titulos value for this TitulosGravarTitulosCPIn.
     * 
     * @return titulos
     */
    public TitulosGravarTitulosCPInTitulos[] getTitulos() {
        return titulos;
    }


    /**
     * Sets the titulos value for this TitulosGravarTitulosCPIn.
     * 
     * @param titulos
     */
    public void setTitulos(TitulosGravarTitulosCPInTitulos[] titulos) {
        this.titulos = titulos;
    }

    public TitulosGravarTitulosCPInTitulos getTitulos(int i) {
        return this.titulos[i];
    }

    public void setTitulos(int i, TitulosGravarTitulosCPInTitulos _value) {
        this.titulos[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosGravarTitulosCPIn)) return false;
        TitulosGravarTitulosCPIn other = (TitulosGravarTitulosCPIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dataBuild==null && other.getDataBuild()==null) || 
             (this.dataBuild!=null &&
              this.dataBuild.equals(other.getDataBuild()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.titulos==null && other.getTitulos()==null) || 
             (this.titulos!=null &&
              java.util.Arrays.equals(this.titulos, other.getTitulos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDataBuild() != null) {
            _hashCode += getDataBuild().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getTitulos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosGravarTitulosCPIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGravarTitulosCPIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataBuild");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataBuild"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGravarTitulosCPInTitulos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
