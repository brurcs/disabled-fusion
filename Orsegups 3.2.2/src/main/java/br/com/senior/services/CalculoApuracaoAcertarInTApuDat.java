/**
 * CalculoApuracaoAcertarInTApuDat.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class CalculoApuracaoAcertarInTApuDat  implements java.io.Serializable {
    private java.lang.Integer codRat;

    private java.lang.Integer codSit;

    private java.lang.String desObs;

    private java.lang.Integer motSit;

    private java.lang.String qtdHor;

    public CalculoApuracaoAcertarInTApuDat() {
    }

    public CalculoApuracaoAcertarInTApuDat(
           java.lang.Integer codRat,
           java.lang.Integer codSit,
           java.lang.String desObs,
           java.lang.Integer motSit,
           java.lang.String qtdHor) {
           this.codRat = codRat;
           this.codSit = codSit;
           this.desObs = desObs;
           this.motSit = motSit;
           this.qtdHor = qtdHor;
    }


    /**
     * Gets the codRat value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @return codRat
     */
    public java.lang.Integer getCodRat() {
        return codRat;
    }


    /**
     * Sets the codRat value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @param codRat
     */
    public void setCodRat(java.lang.Integer codRat) {
        this.codRat = codRat;
    }


    /**
     * Gets the codSit value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @return codSit
     */
    public java.lang.Integer getCodSit() {
        return codSit;
    }


    /**
     * Sets the codSit value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @param codSit
     */
    public void setCodSit(java.lang.Integer codSit) {
        this.codSit = codSit;
    }


    /**
     * Gets the desObs value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @return desObs
     */
    public java.lang.String getDesObs() {
        return desObs;
    }


    /**
     * Sets the desObs value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @param desObs
     */
    public void setDesObs(java.lang.String desObs) {
        this.desObs = desObs;
    }


    /**
     * Gets the motSit value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @return motSit
     */
    public java.lang.Integer getMotSit() {
        return motSit;
    }


    /**
     * Sets the motSit value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @param motSit
     */
    public void setMotSit(java.lang.Integer motSit) {
        this.motSit = motSit;
    }


    /**
     * Gets the qtdHor value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @return qtdHor
     */
    public java.lang.String getQtdHor() {
        return qtdHor;
    }


    /**
     * Sets the qtdHor value for this CalculoApuracaoAcertarInTApuDat.
     * 
     * @param qtdHor
     */
    public void setQtdHor(java.lang.String qtdHor) {
        this.qtdHor = qtdHor;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalculoApuracaoAcertarInTApuDat)) return false;
        CalculoApuracaoAcertarInTApuDat other = (CalculoApuracaoAcertarInTApuDat) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codRat==null && other.getCodRat()==null) || 
             (this.codRat!=null &&
              this.codRat.equals(other.getCodRat()))) &&
            ((this.codSit==null && other.getCodSit()==null) || 
             (this.codSit!=null &&
              this.codSit.equals(other.getCodSit()))) &&
            ((this.desObs==null && other.getDesObs()==null) || 
             (this.desObs!=null &&
              this.desObs.equals(other.getDesObs()))) &&
            ((this.motSit==null && other.getMotSit()==null) || 
             (this.motSit!=null &&
              this.motSit.equals(other.getMotSit()))) &&
            ((this.qtdHor==null && other.getQtdHor()==null) || 
             (this.qtdHor!=null &&
              this.qtdHor.equals(other.getQtdHor())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodRat() != null) {
            _hashCode += getCodRat().hashCode();
        }
        if (getCodSit() != null) {
            _hashCode += getCodSit().hashCode();
        }
        if (getDesObs() != null) {
            _hashCode += getDesObs().hashCode();
        }
        if (getMotSit() != null) {
            _hashCode += getMotSit().hashCode();
        }
        if (getQtdHor() != null) {
            _hashCode += getQtdHor().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalculoApuracaoAcertarInTApuDat.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoAcertarInTApuDat"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motSit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motSit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdHor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdHor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
