/**
 * OrdemcomprabuscarPendentesOutOrdemCompra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public class OrdemcomprabuscarPendentesOutOrdemCompra  implements java.io.Serializable {
    private java.lang.Integer codigoEmpresa;

    private java.lang.Integer codigoFilial;

    private java.lang.String dataEmissao;

    private br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraItem[] item;

    private java.lang.String moedaOC;

    private java.lang.String nomeEmpresa;

    private java.lang.String nomeFilial;

    private java.lang.String nomeFornecedor;

    private java.lang.String nomeSolicitante;

    private java.lang.Integer numero;

    private java.lang.String observacao;

    private br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraRateio[] rateio;

    private java.lang.Integer usuarioSolicitante;

    private java.lang.Double valorAproximado;

    public OrdemcomprabuscarPendentesOutOrdemCompra() {
    }

    public OrdemcomprabuscarPendentesOutOrdemCompra(
           java.lang.Integer codigoEmpresa,
           java.lang.Integer codigoFilial,
           java.lang.String dataEmissao,
           br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraItem[] item,
           java.lang.String moedaOC,
           java.lang.String nomeEmpresa,
           java.lang.String nomeFilial,
           java.lang.String nomeFornecedor,
           java.lang.String nomeSolicitante,
           java.lang.Integer numero,
           java.lang.String observacao,
           br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraRateio[] rateio,
           java.lang.Integer usuarioSolicitante,
           java.lang.Double valorAproximado) {
           this.codigoEmpresa = codigoEmpresa;
           this.codigoFilial = codigoFilial;
           this.dataEmissao = dataEmissao;
           this.item = item;
           this.moedaOC = moedaOC;
           this.nomeEmpresa = nomeEmpresa;
           this.nomeFilial = nomeFilial;
           this.nomeFornecedor = nomeFornecedor;
           this.nomeSolicitante = nomeSolicitante;
           this.numero = numero;
           this.observacao = observacao;
           this.rateio = rateio;
           this.usuarioSolicitante = usuarioSolicitante;
           this.valorAproximado = valorAproximado;
    }


    /**
     * Gets the codigoEmpresa value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return codigoEmpresa
     */
    public java.lang.Integer getCodigoEmpresa() {
        return codigoEmpresa;
    }


    /**
     * Sets the codigoEmpresa value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param codigoEmpresa
     */
    public void setCodigoEmpresa(java.lang.Integer codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }


    /**
     * Gets the codigoFilial value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return codigoFilial
     */
    public java.lang.Integer getCodigoFilial() {
        return codigoFilial;
    }


    /**
     * Sets the codigoFilial value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param codigoFilial
     */
    public void setCodigoFilial(java.lang.Integer codigoFilial) {
        this.codigoFilial = codigoFilial;
    }


    /**
     * Gets the dataEmissao value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return dataEmissao
     */
    public java.lang.String getDataEmissao() {
        return dataEmissao;
    }


    /**
     * Sets the dataEmissao value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param dataEmissao
     */
    public void setDataEmissao(java.lang.String dataEmissao) {
        this.dataEmissao = dataEmissao;
    }


    /**
     * Gets the item value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return item
     */
    public br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraItem[] getItem() {
        return item;
    }


    /**
     * Sets the item value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param item
     */
    public void setItem(br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraItem[] item) {
        this.item = item;
    }

    public br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraItem getItem(int i) {
        return this.item[i];
    }

    public void setItem(int i, br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraItem _value) {
        this.item[i] = _value;
    }


    /**
     * Gets the moedaOC value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return moedaOC
     */
    public java.lang.String getMoedaOC() {
        return moedaOC;
    }


    /**
     * Sets the moedaOC value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param moedaOC
     */
    public void setMoedaOC(java.lang.String moedaOC) {
        this.moedaOC = moedaOC;
    }


    /**
     * Gets the nomeEmpresa value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return nomeEmpresa
     */
    public java.lang.String getNomeEmpresa() {
        return nomeEmpresa;
    }


    /**
     * Sets the nomeEmpresa value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param nomeEmpresa
     */
    public void setNomeEmpresa(java.lang.String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }


    /**
     * Gets the nomeFilial value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return nomeFilial
     */
    public java.lang.String getNomeFilial() {
        return nomeFilial;
    }


    /**
     * Sets the nomeFilial value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param nomeFilial
     */
    public void setNomeFilial(java.lang.String nomeFilial) {
        this.nomeFilial = nomeFilial;
    }


    /**
     * Gets the nomeFornecedor value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return nomeFornecedor
     */
    public java.lang.String getNomeFornecedor() {
        return nomeFornecedor;
    }


    /**
     * Sets the nomeFornecedor value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param nomeFornecedor
     */
    public void setNomeFornecedor(java.lang.String nomeFornecedor) {
        this.nomeFornecedor = nomeFornecedor;
    }


    /**
     * Gets the nomeSolicitante value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return nomeSolicitante
     */
    public java.lang.String getNomeSolicitante() {
        return nomeSolicitante;
    }


    /**
     * Sets the nomeSolicitante value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param nomeSolicitante
     */
    public void setNomeSolicitante(java.lang.String nomeSolicitante) {
        this.nomeSolicitante = nomeSolicitante;
    }


    /**
     * Gets the numero value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return numero
     */
    public java.lang.Integer getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param numero
     */
    public void setNumero(java.lang.Integer numero) {
        this.numero = numero;
    }


    /**
     * Gets the observacao value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return observacao
     */
    public java.lang.String getObservacao() {
        return observacao;
    }


    /**
     * Sets the observacao value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param observacao
     */
    public void setObservacao(java.lang.String observacao) {
        this.observacao = observacao;
    }


    /**
     * Gets the rateio value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return rateio
     */
    public br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraRateio[] getRateio() {
        return rateio;
    }


    /**
     * Sets the rateio value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param rateio
     */
    public void setRateio(br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraRateio[] rateio) {
        this.rateio = rateio;
    }

    public br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraRateio getRateio(int i) {
        return this.rateio[i];
    }

    public void setRateio(int i, br.com.senior.services.fap.OrdemcomprabuscarPendentesOutOrdemCompraRateio _value) {
        this.rateio[i] = _value;
    }


    /**
     * Gets the usuarioSolicitante value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return usuarioSolicitante
     */
    public java.lang.Integer getUsuarioSolicitante() {
        return usuarioSolicitante;
    }


    /**
     * Sets the usuarioSolicitante value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param usuarioSolicitante
     */
    public void setUsuarioSolicitante(java.lang.Integer usuarioSolicitante) {
        this.usuarioSolicitante = usuarioSolicitante;
    }


    /**
     * Gets the valorAproximado value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @return valorAproximado
     */
    public java.lang.Double getValorAproximado() {
        return valorAproximado;
    }


    /**
     * Sets the valorAproximado value for this OrdemcomprabuscarPendentesOutOrdemCompra.
     * 
     * @param valorAproximado
     */
    public void setValorAproximado(java.lang.Double valorAproximado) {
        this.valorAproximado = valorAproximado;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrdemcomprabuscarPendentesOutOrdemCompra)) return false;
        OrdemcomprabuscarPendentesOutOrdemCompra other = (OrdemcomprabuscarPendentesOutOrdemCompra) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoEmpresa==null && other.getCodigoEmpresa()==null) || 
             (this.codigoEmpresa!=null &&
              this.codigoEmpresa.equals(other.getCodigoEmpresa()))) &&
            ((this.codigoFilial==null && other.getCodigoFilial()==null) || 
             (this.codigoFilial!=null &&
              this.codigoFilial.equals(other.getCodigoFilial()))) &&
            ((this.dataEmissao==null && other.getDataEmissao()==null) || 
             (this.dataEmissao!=null &&
              this.dataEmissao.equals(other.getDataEmissao()))) &&
            ((this.item==null && other.getItem()==null) || 
             (this.item!=null &&
              java.util.Arrays.equals(this.item, other.getItem()))) &&
            ((this.moedaOC==null && other.getMoedaOC()==null) || 
             (this.moedaOC!=null &&
              this.moedaOC.equals(other.getMoedaOC()))) &&
            ((this.nomeEmpresa==null && other.getNomeEmpresa()==null) || 
             (this.nomeEmpresa!=null &&
              this.nomeEmpresa.equals(other.getNomeEmpresa()))) &&
            ((this.nomeFilial==null && other.getNomeFilial()==null) || 
             (this.nomeFilial!=null &&
              this.nomeFilial.equals(other.getNomeFilial()))) &&
            ((this.nomeFornecedor==null && other.getNomeFornecedor()==null) || 
             (this.nomeFornecedor!=null &&
              this.nomeFornecedor.equals(other.getNomeFornecedor()))) &&
            ((this.nomeSolicitante==null && other.getNomeSolicitante()==null) || 
             (this.nomeSolicitante!=null &&
              this.nomeSolicitante.equals(other.getNomeSolicitante()))) &&
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.observacao==null && other.getObservacao()==null) || 
             (this.observacao!=null &&
              this.observacao.equals(other.getObservacao()))) &&
            ((this.rateio==null && other.getRateio()==null) || 
             (this.rateio!=null &&
              java.util.Arrays.equals(this.rateio, other.getRateio()))) &&
            ((this.usuarioSolicitante==null && other.getUsuarioSolicitante()==null) || 
             (this.usuarioSolicitante!=null &&
              this.usuarioSolicitante.equals(other.getUsuarioSolicitante()))) &&
            ((this.valorAproximado==null && other.getValorAproximado()==null) || 
             (this.valorAproximado!=null &&
              this.valorAproximado.equals(other.getValorAproximado())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoEmpresa() != null) {
            _hashCode += getCodigoEmpresa().hashCode();
        }
        if (getCodigoFilial() != null) {
            _hashCode += getCodigoFilial().hashCode();
        }
        if (getDataEmissao() != null) {
            _hashCode += getDataEmissao().hashCode();
        }
        if (getItem() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItem());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItem(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMoedaOC() != null) {
            _hashCode += getMoedaOC().hashCode();
        }
        if (getNomeEmpresa() != null) {
            _hashCode += getNomeEmpresa().hashCode();
        }
        if (getNomeFilial() != null) {
            _hashCode += getNomeFilial().hashCode();
        }
        if (getNomeFornecedor() != null) {
            _hashCode += getNomeFornecedor().hashCode();
        }
        if (getNomeSolicitante() != null) {
            _hashCode += getNomeSolicitante().hashCode();
        }
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getObservacao() != null) {
            _hashCode += getObservacao().hashCode();
        }
        if (getRateio() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRateio());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRateio(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUsuarioSolicitante() != null) {
            _hashCode += getUsuarioSolicitante().hashCode();
        }
        if (getValorAproximado() != null) {
            _hashCode += getValorAproximado().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrdemcomprabuscarPendentesOutOrdemCompra.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompra"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoEmpresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoEmpresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEmissao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataEmissao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("item");
        elemField.setXmlName(new javax.xml.namespace.QName("", "item"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompraItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moedaOC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "moedaOC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeEmpresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomeEmpresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomeFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeFornecedor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomeFornecedor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeSolicitante");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomeSolicitante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rateio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompraRateio"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuarioSolicitante");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuarioSolicitante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorAproximado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorAproximado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
