/**
 * TitulosSubstituirTitulosCPInTitulosBaixar.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosSubstituirTitulosCPInTitulosBaixar  implements java.io.Serializable {
    private java.lang.String codCcu;

    private java.lang.Integer codFil;

    private java.lang.Integer codFor;

    private java.lang.Integer codFpg;

    private java.lang.Integer codFpj;

    private java.lang.String codTpt;

    private java.lang.Integer ctaFin;

    private java.lang.Integer ctaRed;

    private java.lang.Integer numPrj;

    private java.lang.String numTit;

    private java.lang.String obsMcp;

    private java.lang.Double vlrBai;

    private java.lang.Double vlrCor;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrEnc;

    private java.lang.Double vlrJrs;

    private java.lang.Double vlrMul;

    private java.lang.Double vlrOac;

    private java.lang.Double vlrOde;

    public TitulosSubstituirTitulosCPInTitulosBaixar() {
    }

    public TitulosSubstituirTitulosCPInTitulosBaixar(
           java.lang.String codCcu,
           java.lang.Integer codFil,
           java.lang.Integer codFor,
           java.lang.Integer codFpg,
           java.lang.Integer codFpj,
           java.lang.String codTpt,
           java.lang.Integer ctaFin,
           java.lang.Integer ctaRed,
           java.lang.Integer numPrj,
           java.lang.String numTit,
           java.lang.String obsMcp,
           java.lang.Double vlrBai,
           java.lang.Double vlrCor,
           java.lang.Double vlrDsc,
           java.lang.Double vlrEnc,
           java.lang.Double vlrJrs,
           java.lang.Double vlrMul,
           java.lang.Double vlrOac,
           java.lang.Double vlrOde) {
           this.codCcu = codCcu;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codFpg = codFpg;
           this.codFpj = codFpj;
           this.codTpt = codTpt;
           this.ctaFin = ctaFin;
           this.ctaRed = ctaRed;
           this.numPrj = numPrj;
           this.numTit = numTit;
           this.obsMcp = obsMcp;
           this.vlrBai = vlrBai;
           this.vlrCor = vlrCor;
           this.vlrDsc = vlrDsc;
           this.vlrEnc = vlrEnc;
           this.vlrJrs = vlrJrs;
           this.vlrMul = vlrMul;
           this.vlrOac = vlrOac;
           this.vlrOde = vlrOde;
    }


    /**
     * Gets the codCcu value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codFil value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codFpg value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFpj value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return codFpj
     */
    public java.lang.Integer getCodFpj() {
        return codFpj;
    }


    /**
     * Sets the codFpj value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param codFpj
     */
    public void setCodFpj(java.lang.Integer codFpj) {
        this.codFpj = codFpj;
    }


    /**
     * Gets the codTpt value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the ctaFin value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return ctaFin
     */
    public java.lang.Integer getCtaFin() {
        return ctaFin;
    }


    /**
     * Sets the ctaFin value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param ctaFin
     */
    public void setCtaFin(java.lang.Integer ctaFin) {
        this.ctaFin = ctaFin;
    }


    /**
     * Gets the ctaRed value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the numPrj value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return numPrj
     */
    public java.lang.Integer getNumPrj() {
        return numPrj;
    }


    /**
     * Sets the numPrj value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param numPrj
     */
    public void setNumPrj(java.lang.Integer numPrj) {
        this.numPrj = numPrj;
    }


    /**
     * Gets the numTit value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the obsMcp value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return obsMcp
     */
    public java.lang.String getObsMcp() {
        return obsMcp;
    }


    /**
     * Sets the obsMcp value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param obsMcp
     */
    public void setObsMcp(java.lang.String obsMcp) {
        this.obsMcp = obsMcp;
    }


    /**
     * Gets the vlrBai value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return vlrBai
     */
    public java.lang.Double getVlrBai() {
        return vlrBai;
    }


    /**
     * Sets the vlrBai value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param vlrBai
     */
    public void setVlrBai(java.lang.Double vlrBai) {
        this.vlrBai = vlrBai;
    }


    /**
     * Gets the vlrCor value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return vlrCor
     */
    public java.lang.Double getVlrCor() {
        return vlrCor;
    }


    /**
     * Sets the vlrCor value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param vlrCor
     */
    public void setVlrCor(java.lang.Double vlrCor) {
        this.vlrCor = vlrCor;
    }


    /**
     * Gets the vlrDsc value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrEnc value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return vlrEnc
     */
    public java.lang.Double getVlrEnc() {
        return vlrEnc;
    }


    /**
     * Sets the vlrEnc value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param vlrEnc
     */
    public void setVlrEnc(java.lang.Double vlrEnc) {
        this.vlrEnc = vlrEnc;
    }


    /**
     * Gets the vlrJrs value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return vlrJrs
     */
    public java.lang.Double getVlrJrs() {
        return vlrJrs;
    }


    /**
     * Sets the vlrJrs value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param vlrJrs
     */
    public void setVlrJrs(java.lang.Double vlrJrs) {
        this.vlrJrs = vlrJrs;
    }


    /**
     * Gets the vlrMul value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return vlrMul
     */
    public java.lang.Double getVlrMul() {
        return vlrMul;
    }


    /**
     * Sets the vlrMul value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param vlrMul
     */
    public void setVlrMul(java.lang.Double vlrMul) {
        this.vlrMul = vlrMul;
    }


    /**
     * Gets the vlrOac value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return vlrOac
     */
    public java.lang.Double getVlrOac() {
        return vlrOac;
    }


    /**
     * Sets the vlrOac value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param vlrOac
     */
    public void setVlrOac(java.lang.Double vlrOac) {
        this.vlrOac = vlrOac;
    }


    /**
     * Gets the vlrOde value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @return vlrOde
     */
    public java.lang.Double getVlrOde() {
        return vlrOde;
    }


    /**
     * Sets the vlrOde value for this TitulosSubstituirTitulosCPInTitulosBaixar.
     * 
     * @param vlrOde
     */
    public void setVlrOde(java.lang.Double vlrOde) {
        this.vlrOde = vlrOde;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosSubstituirTitulosCPInTitulosBaixar)) return false;
        TitulosSubstituirTitulosCPInTitulosBaixar other = (TitulosSubstituirTitulosCPInTitulosBaixar) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFpj==null && other.getCodFpj()==null) || 
             (this.codFpj!=null &&
              this.codFpj.equals(other.getCodFpj()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.ctaFin==null && other.getCtaFin()==null) || 
             (this.ctaFin!=null &&
              this.ctaFin.equals(other.getCtaFin()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.numPrj==null && other.getNumPrj()==null) || 
             (this.numPrj!=null &&
              this.numPrj.equals(other.getNumPrj()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.obsMcp==null && other.getObsMcp()==null) || 
             (this.obsMcp!=null &&
              this.obsMcp.equals(other.getObsMcp()))) &&
            ((this.vlrBai==null && other.getVlrBai()==null) || 
             (this.vlrBai!=null &&
              this.vlrBai.equals(other.getVlrBai()))) &&
            ((this.vlrCor==null && other.getVlrCor()==null) || 
             (this.vlrCor!=null &&
              this.vlrCor.equals(other.getVlrCor()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrEnc==null && other.getVlrEnc()==null) || 
             (this.vlrEnc!=null &&
              this.vlrEnc.equals(other.getVlrEnc()))) &&
            ((this.vlrJrs==null && other.getVlrJrs()==null) || 
             (this.vlrJrs!=null &&
              this.vlrJrs.equals(other.getVlrJrs()))) &&
            ((this.vlrMul==null && other.getVlrMul()==null) || 
             (this.vlrMul!=null &&
              this.vlrMul.equals(other.getVlrMul()))) &&
            ((this.vlrOac==null && other.getVlrOac()==null) || 
             (this.vlrOac!=null &&
              this.vlrOac.equals(other.getVlrOac()))) &&
            ((this.vlrOde==null && other.getVlrOde()==null) || 
             (this.vlrOde!=null &&
              this.vlrOde.equals(other.getVlrOde())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFpj() != null) {
            _hashCode += getCodFpj().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getCtaFin() != null) {
            _hashCode += getCtaFin().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getNumPrj() != null) {
            _hashCode += getNumPrj().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getObsMcp() != null) {
            _hashCode += getObsMcp().hashCode();
        }
        if (getVlrBai() != null) {
            _hashCode += getVlrBai().hashCode();
        }
        if (getVlrCor() != null) {
            _hashCode += getVlrCor().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrEnc() != null) {
            _hashCode += getVlrEnc().hashCode();
        }
        if (getVlrJrs() != null) {
            _hashCode += getVlrJrs().hashCode();
        }
        if (getVlrMul() != null) {
            _hashCode += getVlrMul().hashCode();
        }
        if (getVlrOac() != null) {
            _hashCode += getVlrOac().hashCode();
        }
        if (getVlrOde() != null) {
            _hashCode += getVlrOde().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosSubstituirTitulosCPInTitulosBaixar.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCPInTitulosBaixar"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsMcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsMcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrBai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrBai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOde");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOde"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
