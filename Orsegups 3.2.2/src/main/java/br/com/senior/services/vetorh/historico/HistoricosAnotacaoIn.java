/**
 * HistoricosAnotacaoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosAnotacaoIn  implements java.io.Serializable {
    private java.lang.Integer cadDig;

    private java.lang.String datNot;

    private java.lang.Integer empDig;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String notFic;

    private java.lang.String nroDoc;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Integer seqNot;

    private java.lang.Integer tipCol;

    private java.lang.Integer tipDig;

    private java.lang.Integer tipNot;

    private java.lang.String tipOpe;

    public HistoricosAnotacaoIn() {
    }

    public HistoricosAnotacaoIn(
           java.lang.Integer cadDig,
           java.lang.String datNot,
           java.lang.Integer empDig,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String notFic,
           java.lang.String nroDoc,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Integer seqNot,
           java.lang.Integer tipCol,
           java.lang.Integer tipDig,
           java.lang.Integer tipNot,
           java.lang.String tipOpe) {
           this.cadDig = cadDig;
           this.datNot = datNot;
           this.empDig = empDig;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.notFic = notFic;
           this.nroDoc = nroDoc;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.seqNot = seqNot;
           this.tipCol = tipCol;
           this.tipDig = tipDig;
           this.tipNot = tipNot;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the cadDig value for this HistoricosAnotacaoIn.
     * 
     * @return cadDig
     */
    public java.lang.Integer getCadDig() {
        return cadDig;
    }


    /**
     * Sets the cadDig value for this HistoricosAnotacaoIn.
     * 
     * @param cadDig
     */
    public void setCadDig(java.lang.Integer cadDig) {
        this.cadDig = cadDig;
    }


    /**
     * Gets the datNot value for this HistoricosAnotacaoIn.
     * 
     * @return datNot
     */
    public java.lang.String getDatNot() {
        return datNot;
    }


    /**
     * Sets the datNot value for this HistoricosAnotacaoIn.
     * 
     * @param datNot
     */
    public void setDatNot(java.lang.String datNot) {
        this.datNot = datNot;
    }


    /**
     * Gets the empDig value for this HistoricosAnotacaoIn.
     * 
     * @return empDig
     */
    public java.lang.Integer getEmpDig() {
        return empDig;
    }


    /**
     * Sets the empDig value for this HistoricosAnotacaoIn.
     * 
     * @param empDig
     */
    public void setEmpDig(java.lang.Integer empDig) {
        this.empDig = empDig;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosAnotacaoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosAnotacaoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosAnotacaoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosAnotacaoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the notFic value for this HistoricosAnotacaoIn.
     * 
     * @return notFic
     */
    public java.lang.String getNotFic() {
        return notFic;
    }


    /**
     * Sets the notFic value for this HistoricosAnotacaoIn.
     * 
     * @param notFic
     */
    public void setNotFic(java.lang.String notFic) {
        this.notFic = notFic;
    }


    /**
     * Gets the nroDoc value for this HistoricosAnotacaoIn.
     * 
     * @return nroDoc
     */
    public java.lang.String getNroDoc() {
        return nroDoc;
    }


    /**
     * Sets the nroDoc value for this HistoricosAnotacaoIn.
     * 
     * @param nroDoc
     */
    public void setNroDoc(java.lang.String nroDoc) {
        this.nroDoc = nroDoc;
    }


    /**
     * Gets the numCad value for this HistoricosAnotacaoIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosAnotacaoIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosAnotacaoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosAnotacaoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the seqNot value for this HistoricosAnotacaoIn.
     * 
     * @return seqNot
     */
    public java.lang.Integer getSeqNot() {
        return seqNot;
    }


    /**
     * Sets the seqNot value for this HistoricosAnotacaoIn.
     * 
     * @param seqNot
     */
    public void setSeqNot(java.lang.Integer seqNot) {
        this.seqNot = seqNot;
    }


    /**
     * Gets the tipCol value for this HistoricosAnotacaoIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosAnotacaoIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipDig value for this HistoricosAnotacaoIn.
     * 
     * @return tipDig
     */
    public java.lang.Integer getTipDig() {
        return tipDig;
    }


    /**
     * Sets the tipDig value for this HistoricosAnotacaoIn.
     * 
     * @param tipDig
     */
    public void setTipDig(java.lang.Integer tipDig) {
        this.tipDig = tipDig;
    }


    /**
     * Gets the tipNot value for this HistoricosAnotacaoIn.
     * 
     * @return tipNot
     */
    public java.lang.Integer getTipNot() {
        return tipNot;
    }


    /**
     * Sets the tipNot value for this HistoricosAnotacaoIn.
     * 
     * @param tipNot
     */
    public void setTipNot(java.lang.Integer tipNot) {
        this.tipNot = tipNot;
    }


    /**
     * Gets the tipOpe value for this HistoricosAnotacaoIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosAnotacaoIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosAnotacaoIn)) return false;
        HistoricosAnotacaoIn other = (HistoricosAnotacaoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cadDig==null && other.getCadDig()==null) || 
             (this.cadDig!=null &&
              this.cadDig.equals(other.getCadDig()))) &&
            ((this.datNot==null && other.getDatNot()==null) || 
             (this.datNot!=null &&
              this.datNot.equals(other.getDatNot()))) &&
            ((this.empDig==null && other.getEmpDig()==null) || 
             (this.empDig!=null &&
              this.empDig.equals(other.getEmpDig()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.notFic==null && other.getNotFic()==null) || 
             (this.notFic!=null &&
              this.notFic.equals(other.getNotFic()))) &&
            ((this.nroDoc==null && other.getNroDoc()==null) || 
             (this.nroDoc!=null &&
              this.nroDoc.equals(other.getNroDoc()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.seqNot==null && other.getSeqNot()==null) || 
             (this.seqNot!=null &&
              this.seqNot.equals(other.getSeqNot()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipDig==null && other.getTipDig()==null) || 
             (this.tipDig!=null &&
              this.tipDig.equals(other.getTipDig()))) &&
            ((this.tipNot==null && other.getTipNot()==null) || 
             (this.tipNot!=null &&
              this.tipNot.equals(other.getTipNot()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCadDig() != null) {
            _hashCode += getCadDig().hashCode();
        }
        if (getDatNot() != null) {
            _hashCode += getDatNot().hashCode();
        }
        if (getEmpDig() != null) {
            _hashCode += getEmpDig().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNotFic() != null) {
            _hashCode += getNotFic().hashCode();
        }
        if (getNroDoc() != null) {
            _hashCode += getNroDoc().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getSeqNot() != null) {
            _hashCode += getSeqNot().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipDig() != null) {
            _hashCode += getTipDig().hashCode();
        }
        if (getTipNot() != null) {
            _hashCode += getTipNot().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosAnotacaoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosAnotacaoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cadDig");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cadDig"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empDig");
        elemField.setXmlName(new javax.xml.namespace.QName("", "empDig"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notFic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notFic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqNot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqNot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipDig");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipDig"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipNot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipNot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
