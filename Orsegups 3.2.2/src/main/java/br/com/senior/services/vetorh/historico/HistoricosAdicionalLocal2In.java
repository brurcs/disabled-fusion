/**
 * HistoricosAdicionalLocal2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosAdicionalLocal2In  implements java.io.Serializable {
    private java.lang.Integer apoEsp;

    private java.lang.String cmpTer;

    private java.lang.String datAlt;

    private java.lang.Double fatTph;

    private java.lang.Double fatTpm;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer numEmp;

    private java.lang.String numLoc;

    private java.lang.Double perIns;

    private java.lang.Double perPer;

    private java.lang.String tipOpe;

    public HistoricosAdicionalLocal2In() {
    }

    public HistoricosAdicionalLocal2In(
           java.lang.Integer apoEsp,
           java.lang.String cmpTer,
           java.lang.String datAlt,
           java.lang.Double fatTph,
           java.lang.Double fatTpm,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer numEmp,
           java.lang.String numLoc,
           java.lang.Double perIns,
           java.lang.Double perPer,
           java.lang.String tipOpe) {
           this.apoEsp = apoEsp;
           this.cmpTer = cmpTer;
           this.datAlt = datAlt;
           this.fatTph = fatTph;
           this.fatTpm = fatTpm;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.numEmp = numEmp;
           this.numLoc = numLoc;
           this.perIns = perIns;
           this.perPer = perPer;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the apoEsp value for this HistoricosAdicionalLocal2In.
     * 
     * @return apoEsp
     */
    public java.lang.Integer getApoEsp() {
        return apoEsp;
    }


    /**
     * Sets the apoEsp value for this HistoricosAdicionalLocal2In.
     * 
     * @param apoEsp
     */
    public void setApoEsp(java.lang.Integer apoEsp) {
        this.apoEsp = apoEsp;
    }


    /**
     * Gets the cmpTer value for this HistoricosAdicionalLocal2In.
     * 
     * @return cmpTer
     */
    public java.lang.String getCmpTer() {
        return cmpTer;
    }


    /**
     * Sets the cmpTer value for this HistoricosAdicionalLocal2In.
     * 
     * @param cmpTer
     */
    public void setCmpTer(java.lang.String cmpTer) {
        this.cmpTer = cmpTer;
    }


    /**
     * Gets the datAlt value for this HistoricosAdicionalLocal2In.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosAdicionalLocal2In.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the fatTph value for this HistoricosAdicionalLocal2In.
     * 
     * @return fatTph
     */
    public java.lang.Double getFatTph() {
        return fatTph;
    }


    /**
     * Sets the fatTph value for this HistoricosAdicionalLocal2In.
     * 
     * @param fatTph
     */
    public void setFatTph(java.lang.Double fatTph) {
        this.fatTph = fatTph;
    }


    /**
     * Gets the fatTpm value for this HistoricosAdicionalLocal2In.
     * 
     * @return fatTpm
     */
    public java.lang.Double getFatTpm() {
        return fatTpm;
    }


    /**
     * Sets the fatTpm value for this HistoricosAdicionalLocal2In.
     * 
     * @param fatTpm
     */
    public void setFatTpm(java.lang.Double fatTpm) {
        this.fatTpm = fatTpm;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosAdicionalLocal2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosAdicionalLocal2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosAdicionalLocal2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosAdicionalLocal2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the numEmp value for this HistoricosAdicionalLocal2In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosAdicionalLocal2In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the numLoc value for this HistoricosAdicionalLocal2In.
     * 
     * @return numLoc
     */
    public java.lang.String getNumLoc() {
        return numLoc;
    }


    /**
     * Sets the numLoc value for this HistoricosAdicionalLocal2In.
     * 
     * @param numLoc
     */
    public void setNumLoc(java.lang.String numLoc) {
        this.numLoc = numLoc;
    }


    /**
     * Gets the perIns value for this HistoricosAdicionalLocal2In.
     * 
     * @return perIns
     */
    public java.lang.Double getPerIns() {
        return perIns;
    }


    /**
     * Sets the perIns value for this HistoricosAdicionalLocal2In.
     * 
     * @param perIns
     */
    public void setPerIns(java.lang.Double perIns) {
        this.perIns = perIns;
    }


    /**
     * Gets the perPer value for this HistoricosAdicionalLocal2In.
     * 
     * @return perPer
     */
    public java.lang.Double getPerPer() {
        return perPer;
    }


    /**
     * Sets the perPer value for this HistoricosAdicionalLocal2In.
     * 
     * @param perPer
     */
    public void setPerPer(java.lang.Double perPer) {
        this.perPer = perPer;
    }


    /**
     * Gets the tipOpe value for this HistoricosAdicionalLocal2In.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosAdicionalLocal2In.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosAdicionalLocal2In)) return false;
        HistoricosAdicionalLocal2In other = (HistoricosAdicionalLocal2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apoEsp==null && other.getApoEsp()==null) || 
             (this.apoEsp!=null &&
              this.apoEsp.equals(other.getApoEsp()))) &&
            ((this.cmpTer==null && other.getCmpTer()==null) || 
             (this.cmpTer!=null &&
              this.cmpTer.equals(other.getCmpTer()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.fatTph==null && other.getFatTph()==null) || 
             (this.fatTph!=null &&
              this.fatTph.equals(other.getFatTph()))) &&
            ((this.fatTpm==null && other.getFatTpm()==null) || 
             (this.fatTpm!=null &&
              this.fatTpm.equals(other.getFatTpm()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.numLoc==null && other.getNumLoc()==null) || 
             (this.numLoc!=null &&
              this.numLoc.equals(other.getNumLoc()))) &&
            ((this.perIns==null && other.getPerIns()==null) || 
             (this.perIns!=null &&
              this.perIns.equals(other.getPerIns()))) &&
            ((this.perPer==null && other.getPerPer()==null) || 
             (this.perPer!=null &&
              this.perPer.equals(other.getPerPer()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApoEsp() != null) {
            _hashCode += getApoEsp().hashCode();
        }
        if (getCmpTer() != null) {
            _hashCode += getCmpTer().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getFatTph() != null) {
            _hashCode += getFatTph().hashCode();
        }
        if (getFatTpm() != null) {
            _hashCode += getFatTpm().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getNumLoc() != null) {
            _hashCode += getNumLoc().hashCode();
        }
        if (getPerIns() != null) {
            _hashCode += getPerIns().hashCode();
        }
        if (getPerPer() != null) {
            _hashCode += getPerPer().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosAdicionalLocal2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosAdicionalLocal2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apoEsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apoEsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cmpTer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cmpTer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fatTph");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fatTph"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fatTpm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fatTpm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numLoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numLoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perPer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perPer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
