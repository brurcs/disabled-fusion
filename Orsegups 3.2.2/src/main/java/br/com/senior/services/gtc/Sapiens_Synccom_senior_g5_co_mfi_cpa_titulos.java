/**
 * Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;


public interface Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos extends java.rmi.Remote
{
	public br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPOut entradaTitulosLoteCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPIn parameters) throws java.rmi.RemoteException;
}
