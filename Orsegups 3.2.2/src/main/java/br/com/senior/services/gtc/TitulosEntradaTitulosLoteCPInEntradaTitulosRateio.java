/**
 * TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosEntradaTitulosLoteCPInEntradaTitulosRateio implements java.io.Serializable
{
	private java.lang.String codCcu;

	private java.lang.Integer codFpj;

	private java.lang.Integer ctaFin;

	private java.lang.Integer ctaRed;

	private java.lang.Integer numPrj;

	private java.lang.String obsRat;

	private java.lang.Double perCta;

	private java.lang.Double perRat;

	private java.lang.Double vlrCta;

	private java.lang.Double vlrRat;

	public TitulosEntradaTitulosLoteCPInEntradaTitulosRateio()
	{
	}

	public TitulosEntradaTitulosLoteCPInEntradaTitulosRateio(java.lang.String codCcu, java.lang.Integer codFpj, java.lang.Integer ctaFin, java.lang.Integer ctaRed, java.lang.Integer numPrj, java.lang.String obsRat, java.lang.Double perCta, java.lang.Double perRat, java.lang.Double vlrCta, java.lang.Double vlrRat)
	{
		this.codCcu = codCcu;
		this.codFpj = codFpj;
		this.ctaFin = ctaFin;
		this.ctaRed = ctaRed;
		this.numPrj = numPrj;
		this.obsRat = obsRat;
		this.perCta = perCta;
		this.perRat = perRat;
		this.vlrCta = vlrCta;
		this.vlrRat = vlrRat;
	}

	/**
	 * Gets the codCcu value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return codCcu
	 */
	public java.lang.String getCodCcu()
	{
		return codCcu;
	}

	/**
	 * Sets the codCcu value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param codCcu
	 */
	public void setCodCcu(java.lang.String codCcu)
	{
		this.codCcu = codCcu;
	}

	/**
	 * Gets the codFpj value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return codFpj
	 */
	public java.lang.Integer getCodFpj()
	{
		return codFpj;
	}

	/**
	 * Sets the codFpj value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param codFpj
	 */
	public void setCodFpj(java.lang.Integer codFpj)
	{
		this.codFpj = codFpj;
	}

	/**
	 * Gets the ctaFin value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return ctaFin
	 */
	public java.lang.Integer getCtaFin()
	{
		return ctaFin;
	}

	/**
	 * Sets the ctaFin value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param ctaFin
	 */
	public void setCtaFin(java.lang.Integer ctaFin)
	{
		this.ctaFin = ctaFin;
	}

	/**
	 * Gets the ctaRed value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return ctaRed
	 */
	public java.lang.Integer getCtaRed()
	{
		return ctaRed;
	}

	/**
	 * Sets the ctaRed value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param ctaRed
	 */
	public void setCtaRed(java.lang.Integer ctaRed)
	{
		this.ctaRed = ctaRed;
	}

	/**
	 * Gets the numPrj value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return numPrj
	 */
	public java.lang.Integer getNumPrj()
	{
		return numPrj;
	}

	/**
	 * Sets the numPrj value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param numPrj
	 */
	public void setNumPrj(java.lang.Integer numPrj)
	{
		this.numPrj = numPrj;
	}

	/**
	 * Gets the obsRat value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return obsRat
	 */
	public java.lang.String getObsRat()
	{
		return obsRat;
	}

	/**
	 * Sets the obsRat value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param obsRat
	 */
	public void setObsRat(java.lang.String obsRat)
	{
		this.obsRat = obsRat;
	}

	/**
	 * Gets the perCta value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return perCta
	 */
	public java.lang.Double getPerCta()
	{
		return perCta;
	}

	/**
	 * Sets the perCta value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param perCta
	 */
	public void setPerCta(java.lang.Double perCta)
	{
		this.perCta = perCta;
	}

	/**
	 * Gets the perRat value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return perRat
	 */
	public java.lang.Double getPerRat()
	{
		return perRat;
	}

	/**
	 * Sets the perRat value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param perRat
	 */
	public void setPerRat(java.lang.Double perRat)
	{
		this.perRat = perRat;
	}

	/**
	 * Gets the vlrCta value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return vlrCta
	 */
	public java.lang.Double getVlrCta()
	{
		return vlrCta;
	}

	/**
	 * Sets the vlrCta value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param vlrCta
	 */
	public void setVlrCta(java.lang.Double vlrCta)
	{
		this.vlrCta = vlrCta;
	}

	/**
	 * Gets the vlrRat value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @return vlrRat
	 */
	public java.lang.Double getVlrRat()
	{
		return vlrRat;
	}

	/**
	 * Sets the vlrRat value for this TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.
	 * 
	 * @param vlrRat
	 */
	public void setVlrRat(java.lang.Double vlrRat)
	{
		this.vlrRat = vlrRat;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj)
	{
		if (!(obj instanceof TitulosEntradaTitulosLoteCPInEntradaTitulosRateio))
			return false;
		TitulosEntradaTitulosLoteCPInEntradaTitulosRateio other = (TitulosEntradaTitulosLoteCPInEntradaTitulosRateio) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null)
		{
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.codCcu == null && other.getCodCcu() == null) || (this.codCcu != null && this.codCcu.equals(other.getCodCcu()))) && ((this.codFpj == null && other.getCodFpj() == null) || (this.codFpj != null && this.codFpj.equals(other.getCodFpj()))) && ((this.ctaFin == null && other.getCtaFin() == null) || (this.ctaFin != null && this.ctaFin.equals(other.getCtaFin())))
				&& ((this.ctaRed == null && other.getCtaRed() == null) || (this.ctaRed != null && this.ctaRed.equals(other.getCtaRed()))) && ((this.numPrj == null && other.getNumPrj() == null) || (this.numPrj != null && this.numPrj.equals(other.getNumPrj()))) && ((this.obsRat == null && other.getObsRat() == null) || (this.obsRat != null && this.obsRat.equals(other.getObsRat())))
				&& ((this.perCta == null && other.getPerCta() == null) || (this.perCta != null && this.perCta.equals(other.getPerCta()))) && ((this.perRat == null && other.getPerRat() == null) || (this.perRat != null && this.perRat.equals(other.getPerRat()))) && ((this.vlrCta == null && other.getVlrCta() == null) || (this.vlrCta != null && this.vlrCta.equals(other.getVlrCta())))
				&& ((this.vlrRat == null && other.getVlrRat() == null) || (this.vlrRat != null && this.vlrRat.equals(other.getVlrRat())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode()
	{
		if (__hashCodeCalc)
		{
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getCodCcu() != null)
		{
			_hashCode += getCodCcu().hashCode();
		}
		if (getCodFpj() != null)
		{
			_hashCode += getCodFpj().hashCode();
		}
		if (getCtaFin() != null)
		{
			_hashCode += getCtaFin().hashCode();
		}
		if (getCtaRed() != null)
		{
			_hashCode += getCtaRed().hashCode();
		}
		if (getNumPrj() != null)
		{
			_hashCode += getNumPrj().hashCode();
		}
		if (getObsRat() != null)
		{
			_hashCode += getObsRat().hashCode();
		}
		if (getPerCta() != null)
		{
			_hashCode += getPerCta().hashCode();
		}
		if (getPerRat() != null)
		{
			_hashCode += getPerRat().hashCode();
		}
		if (getVlrCta() != null)
		{
			_hashCode += getVlrCta().hashCode();
		}
		if (getVlrRat() != null)
		{
			_hashCode += getVlrRat().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(TitulosEntradaTitulosLoteCPInEntradaTitulosRateio.class, true);

	static
	{
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEntradaTitulosLoteCPInEntradaTitulosRateio"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codCcu");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codFpj");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ctaFin");
		elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ctaRed");
		elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("numPrj");
		elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("obsRat");
		elemField.setXmlName(new javax.xml.namespace.QName("", "obsRat"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("perCta");
		elemField.setXmlName(new javax.xml.namespace.QName("", "perCta"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("perRat");
		elemField.setXmlName(new javax.xml.namespace.QName("", "perRat"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlrCta");
		elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCta"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlrRat");
		elemField.setXmlName(new javax.xml.namespace.QName("", "vlrRat"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc()
	{
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
	{
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
	{
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
