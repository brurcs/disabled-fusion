/**
 * ClientesExportarClientesCompletoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesExportarClientesCompletoIn  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String dataFinal;

    private java.lang.String dataInicial;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String identificadorSistema;

    private java.lang.Integer quantidadeRegistros;

    private java.lang.String tipoExportacao;

    public ClientesExportarClientesCompletoIn() {
    }

    public ClientesExportarClientesCompletoIn(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String dataFinal,
           java.lang.String dataInicial,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String identificadorSistema,
           java.lang.Integer quantidadeRegistros,
           java.lang.String tipoExportacao) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.dataFinal = dataFinal;
           this.dataInicial = dataInicial;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.identificadorSistema = identificadorSistema;
           this.quantidadeRegistros = quantidadeRegistros;
           this.tipoExportacao = tipoExportacao;
    }


    /**
     * Gets the codEmp value for this ClientesExportarClientesCompletoIn.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this ClientesExportarClientesCompletoIn.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this ClientesExportarClientesCompletoIn.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this ClientesExportarClientesCompletoIn.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the dataFinal value for this ClientesExportarClientesCompletoIn.
     * 
     * @return dataFinal
     */
    public java.lang.String getDataFinal() {
        return dataFinal;
    }


    /**
     * Sets the dataFinal value for this ClientesExportarClientesCompletoIn.
     * 
     * @param dataFinal
     */
    public void setDataFinal(java.lang.String dataFinal) {
        this.dataFinal = dataFinal;
    }


    /**
     * Gets the dataInicial value for this ClientesExportarClientesCompletoIn.
     * 
     * @return dataInicial
     */
    public java.lang.String getDataInicial() {
        return dataInicial;
    }


    /**
     * Sets the dataInicial value for this ClientesExportarClientesCompletoIn.
     * 
     * @param dataInicial
     */
    public void setDataInicial(java.lang.String dataInicial) {
        this.dataInicial = dataInicial;
    }


    /**
     * Gets the flowInstanceID value for this ClientesExportarClientesCompletoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this ClientesExportarClientesCompletoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this ClientesExportarClientesCompletoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this ClientesExportarClientesCompletoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the identificadorSistema value for this ClientesExportarClientesCompletoIn.
     * 
     * @return identificadorSistema
     */
    public java.lang.String getIdentificadorSistema() {
        return identificadorSistema;
    }


    /**
     * Sets the identificadorSistema value for this ClientesExportarClientesCompletoIn.
     * 
     * @param identificadorSistema
     */
    public void setIdentificadorSistema(java.lang.String identificadorSistema) {
        this.identificadorSistema = identificadorSistema;
    }


    /**
     * Gets the quantidadeRegistros value for this ClientesExportarClientesCompletoIn.
     * 
     * @return quantidadeRegistros
     */
    public java.lang.Integer getQuantidadeRegistros() {
        return quantidadeRegistros;
    }


    /**
     * Sets the quantidadeRegistros value for this ClientesExportarClientesCompletoIn.
     * 
     * @param quantidadeRegistros
     */
    public void setQuantidadeRegistros(java.lang.Integer quantidadeRegistros) {
        this.quantidadeRegistros = quantidadeRegistros;
    }


    /**
     * Gets the tipoExportacao value for this ClientesExportarClientesCompletoIn.
     * 
     * @return tipoExportacao
     */
    public java.lang.String getTipoExportacao() {
        return tipoExportacao;
    }


    /**
     * Sets the tipoExportacao value for this ClientesExportarClientesCompletoIn.
     * 
     * @param tipoExportacao
     */
    public void setTipoExportacao(java.lang.String tipoExportacao) {
        this.tipoExportacao = tipoExportacao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesExportarClientesCompletoIn)) return false;
        ClientesExportarClientesCompletoIn other = (ClientesExportarClientesCompletoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.dataFinal==null && other.getDataFinal()==null) || 
             (this.dataFinal!=null &&
              this.dataFinal.equals(other.getDataFinal()))) &&
            ((this.dataInicial==null && other.getDataInicial()==null) || 
             (this.dataInicial!=null &&
              this.dataInicial.equals(other.getDataInicial()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.identificadorSistema==null && other.getIdentificadorSistema()==null) || 
             (this.identificadorSistema!=null &&
              this.identificadorSistema.equals(other.getIdentificadorSistema()))) &&
            ((this.quantidadeRegistros==null && other.getQuantidadeRegistros()==null) || 
             (this.quantidadeRegistros!=null &&
              this.quantidadeRegistros.equals(other.getQuantidadeRegistros()))) &&
            ((this.tipoExportacao==null && other.getTipoExportacao()==null) || 
             (this.tipoExportacao!=null &&
              this.tipoExportacao.equals(other.getTipoExportacao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getDataFinal() != null) {
            _hashCode += getDataFinal().hashCode();
        }
        if (getDataInicial() != null) {
            _hashCode += getDataInicial().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIdentificadorSistema() != null) {
            _hashCode += getIdentificadorSistema().hashCode();
        }
        if (getQuantidadeRegistros() != null) {
            _hashCode += getQuantidadeRegistros().hashCode();
        }
        if (getTipoExportacao() != null) {
            _hashCode += getTipoExportacao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesExportarClientesCompletoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarClientesCompletoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataFinal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataFinal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataInicial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataInicial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificadorSistema");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identificadorSistema"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeRegistros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quantidadeRegistros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoExportacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoExportacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
