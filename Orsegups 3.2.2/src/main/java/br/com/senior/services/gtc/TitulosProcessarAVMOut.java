/**
 * TitulosProcessarAVMOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosProcessarAVMOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private java.lang.String mensagemRetorno;

    private java.lang.Integer tipoRetorno;

    private TitulosProcessarAVMOutTitulosCalculados[] titulosCalculados;

    public TitulosProcessarAVMOut() {
    }

    public TitulosProcessarAVMOut(
           java.lang.String erroExecucao,
           java.lang.String mensagemRetorno,
           java.lang.Integer tipoRetorno,
           TitulosProcessarAVMOutTitulosCalculados[] titulosCalculados) {
           this.erroExecucao = erroExecucao;
           this.mensagemRetorno = mensagemRetorno;
           this.tipoRetorno = tipoRetorno;
           this.titulosCalculados = titulosCalculados;
    }


    /**
     * Gets the erroExecucao value for this TitulosProcessarAVMOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this TitulosProcessarAVMOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the mensagemRetorno value for this TitulosProcessarAVMOut.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this TitulosProcessarAVMOut.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the tipoRetorno value for this TitulosProcessarAVMOut.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this TitulosProcessarAVMOut.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the titulosCalculados value for this TitulosProcessarAVMOut.
     * 
     * @return titulosCalculados
     */
    public TitulosProcessarAVMOutTitulosCalculados[] getTitulosCalculados() {
        return titulosCalculados;
    }


    /**
     * Sets the titulosCalculados value for this TitulosProcessarAVMOut.
     * 
     * @param titulosCalculados
     */
    public void setTitulosCalculados(TitulosProcessarAVMOutTitulosCalculados[] titulosCalculados) {
        this.titulosCalculados = titulosCalculados;
    }

    public TitulosProcessarAVMOutTitulosCalculados getTitulosCalculados(int i) {
        return this.titulosCalculados[i];
    }

    public void setTitulosCalculados(int i, TitulosProcessarAVMOutTitulosCalculados _value) {
        this.titulosCalculados[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosProcessarAVMOut)) return false;
        TitulosProcessarAVMOut other = (TitulosProcessarAVMOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno()))) &&
            ((this.titulosCalculados==null && other.getTitulosCalculados()==null) || 
             (this.titulosCalculados!=null &&
              java.util.Arrays.equals(this.titulosCalculados, other.getTitulosCalculados())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        if (getTitulosCalculados() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosCalculados());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosCalculados(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosProcessarAVMOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosProcessarAVMOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosCalculados");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosCalculados"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosProcessarAVMOutTitulosCalculados"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
