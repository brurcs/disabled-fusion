/**
 * ClientesConsultarCadastroOutClienteHistorico.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesConsultarCadastroOutClienteHistorico  implements java.io.Serializable {
    private br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistoricoCampoUsuarioDefinicoes[] campoUsuarioDefinicoes;

    private java.lang.String cifFob;

    private java.lang.Integer codCli;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.Integer codFin;

    private java.lang.String codMar;

    private java.lang.String indPre;

    public ClientesConsultarCadastroOutClienteHistorico() {
    }

    public ClientesConsultarCadastroOutClienteHistorico(
           br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistoricoCampoUsuarioDefinicoes[] campoUsuarioDefinicoes,
           java.lang.String cifFob,
           java.lang.Integer codCli,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.Integer codFin,
           java.lang.String codMar,
           java.lang.String indPre) {
           this.campoUsuarioDefinicoes = campoUsuarioDefinicoes;
           this.cifFob = cifFob;
           this.codCli = codCli;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFin = codFin;
           this.codMar = codMar;
           this.indPre = indPre;
    }


    /**
     * Gets the campoUsuarioDefinicoes value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @return campoUsuarioDefinicoes
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistoricoCampoUsuarioDefinicoes[] getCampoUsuarioDefinicoes() {
        return campoUsuarioDefinicoes;
    }


    /**
     * Sets the campoUsuarioDefinicoes value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @param campoUsuarioDefinicoes
     */
    public void setCampoUsuarioDefinicoes(br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistoricoCampoUsuarioDefinicoes[] campoUsuarioDefinicoes) {
        this.campoUsuarioDefinicoes = campoUsuarioDefinicoes;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistoricoCampoUsuarioDefinicoes getCampoUsuarioDefinicoes(int i) {
        return this.campoUsuarioDefinicoes[i];
    }

    public void setCampoUsuarioDefinicoes(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistoricoCampoUsuarioDefinicoes _value) {
        this.campoUsuarioDefinicoes[i] = _value;
    }


    /**
     * Gets the cifFob value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @return cifFob
     */
    public java.lang.String getCifFob() {
        return cifFob;
    }


    /**
     * Sets the cifFob value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @param cifFob
     */
    public void setCifFob(java.lang.String cifFob) {
        this.cifFob = cifFob;
    }


    /**
     * Gets the codCli value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codEmp value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFin value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @return codFin
     */
    public java.lang.Integer getCodFin() {
        return codFin;
    }


    /**
     * Sets the codFin value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @param codFin
     */
    public void setCodFin(java.lang.Integer codFin) {
        this.codFin = codFin;
    }


    /**
     * Gets the codMar value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @return codMar
     */
    public java.lang.String getCodMar() {
        return codMar;
    }


    /**
     * Sets the codMar value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @param codMar
     */
    public void setCodMar(java.lang.String codMar) {
        this.codMar = codMar;
    }


    /**
     * Gets the indPre value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @return indPre
     */
    public java.lang.String getIndPre() {
        return indPre;
    }


    /**
     * Sets the indPre value for this ClientesConsultarCadastroOutClienteHistorico.
     * 
     * @param indPre
     */
    public void setIndPre(java.lang.String indPre) {
        this.indPre = indPre;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesConsultarCadastroOutClienteHistorico)) return false;
        ClientesConsultarCadastroOutClienteHistorico other = (ClientesConsultarCadastroOutClienteHistorico) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.campoUsuarioDefinicoes==null && other.getCampoUsuarioDefinicoes()==null) || 
             (this.campoUsuarioDefinicoes!=null &&
              java.util.Arrays.equals(this.campoUsuarioDefinicoes, other.getCampoUsuarioDefinicoes()))) &&
            ((this.cifFob==null && other.getCifFob()==null) || 
             (this.cifFob!=null &&
              this.cifFob.equals(other.getCifFob()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFin==null && other.getCodFin()==null) || 
             (this.codFin!=null &&
              this.codFin.equals(other.getCodFin()))) &&
            ((this.codMar==null && other.getCodMar()==null) || 
             (this.codMar!=null &&
              this.codMar.equals(other.getCodMar()))) &&
            ((this.indPre==null && other.getIndPre()==null) || 
             (this.indPre!=null &&
              this.indPre.equals(other.getIndPre())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCampoUsuarioDefinicoes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCampoUsuarioDefinicoes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCampoUsuarioDefinicoes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCifFob() != null) {
            _hashCode += getCifFob().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFin() != null) {
            _hashCode += getCodFin().hashCode();
        }
        if (getCodMar() != null) {
            _hashCode += getCodMar().hashCode();
        }
        if (getIndPre() != null) {
            _hashCode += getIndPre().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesConsultarCadastroOutClienteHistorico.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarCadastroOutClienteHistorico"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campoUsuarioDefinicoes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "campoUsuarioDefinicoes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarCadastroOutClienteHistoricoCampoUsuarioDefinicoes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cifFob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cifFob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indPre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indPre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
