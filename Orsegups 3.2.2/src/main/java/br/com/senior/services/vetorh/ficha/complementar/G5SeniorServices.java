/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.complementar;

public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getrubi_Synccom_senior_g5_rh_fp_fichaComplementarPortAddress();

    public br.com.senior.services.vetorh.ficha.complementar.Rubi_Synccom_senior_g5_rh_fp_fichaComplementar getrubi_Synccom_senior_g5_rh_fp_fichaComplementarPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.vetorh.ficha.complementar.Rubi_Synccom_senior_g5_rh_fp_fichaComplementar getrubi_Synccom_senior_g5_rh_fp_fichaComplementarPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
