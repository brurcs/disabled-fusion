/**
 * FichaComplementarDocumentoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.complementar;

public class FichaComplementarDocumentoIn  implements java.io.Serializable {
    private java.lang.String arqDoc;

    private java.lang.String datDoc;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String nomArq;

    private java.lang.String nomeArquivo;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.String obsDoc;

    private java.lang.Integer seqDoc;

    private java.lang.Integer tipCol;

    private java.lang.Integer tipDoc;

    private java.lang.String tipOpe;

    public FichaComplementarDocumentoIn() {
    }

    public FichaComplementarDocumentoIn(
           java.lang.String arqDoc,
           java.lang.String datDoc,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String nomArq,
           java.lang.String nomeArquivo,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.String obsDoc,
           java.lang.Integer seqDoc,
           java.lang.Integer tipCol,
           java.lang.Integer tipDoc,
           java.lang.String tipOpe) {
           this.arqDoc = arqDoc;
           this.datDoc = datDoc;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.nomArq = nomArq;
           this.nomeArquivo = nomeArquivo;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.obsDoc = obsDoc;
           this.seqDoc = seqDoc;
           this.tipCol = tipCol;
           this.tipDoc = tipDoc;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the arqDoc value for this FichaComplementarDocumentoIn.
     * 
     * @return arqDoc
     */
    public java.lang.String getArqDoc() {
        return arqDoc;
    }


    /**
     * Sets the arqDoc value for this FichaComplementarDocumentoIn.
     * 
     * @param arqDoc
     */
    public void setArqDoc(java.lang.String arqDoc) {
        this.arqDoc = arqDoc;
    }


    /**
     * Gets the datDoc value for this FichaComplementarDocumentoIn.
     * 
     * @return datDoc
     */
    public java.lang.String getDatDoc() {
        return datDoc;
    }


    /**
     * Sets the datDoc value for this FichaComplementarDocumentoIn.
     * 
     * @param datDoc
     */
    public void setDatDoc(java.lang.String datDoc) {
        this.datDoc = datDoc;
    }


    /**
     * Gets the flowInstanceID value for this FichaComplementarDocumentoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this FichaComplementarDocumentoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this FichaComplementarDocumentoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this FichaComplementarDocumentoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the nomArq value for this FichaComplementarDocumentoIn.
     * 
     * @return nomArq
     */
    public java.lang.String getNomArq() {
        return nomArq;
    }


    /**
     * Sets the nomArq value for this FichaComplementarDocumentoIn.
     * 
     * @param nomArq
     */
    public void setNomArq(java.lang.String nomArq) {
        this.nomArq = nomArq;
    }


    /**
     * Gets the nomeArquivo value for this FichaComplementarDocumentoIn.
     * 
     * @return nomeArquivo
     */
    public java.lang.String getNomeArquivo() {
        return nomeArquivo;
    }


    /**
     * Sets the nomeArquivo value for this FichaComplementarDocumentoIn.
     * 
     * @param nomeArquivo
     */
    public void setNomeArquivo(java.lang.String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }


    /**
     * Gets the numCad value for this FichaComplementarDocumentoIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this FichaComplementarDocumentoIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this FichaComplementarDocumentoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this FichaComplementarDocumentoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the obsDoc value for this FichaComplementarDocumentoIn.
     * 
     * @return obsDoc
     */
    public java.lang.String getObsDoc() {
        return obsDoc;
    }


    /**
     * Sets the obsDoc value for this FichaComplementarDocumentoIn.
     * 
     * @param obsDoc
     */
    public void setObsDoc(java.lang.String obsDoc) {
        this.obsDoc = obsDoc;
    }


    /**
     * Gets the seqDoc value for this FichaComplementarDocumentoIn.
     * 
     * @return seqDoc
     */
    public java.lang.Integer getSeqDoc() {
        return seqDoc;
    }


    /**
     * Sets the seqDoc value for this FichaComplementarDocumentoIn.
     * 
     * @param seqDoc
     */
    public void setSeqDoc(java.lang.Integer seqDoc) {
        this.seqDoc = seqDoc;
    }


    /**
     * Gets the tipCol value for this FichaComplementarDocumentoIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this FichaComplementarDocumentoIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipDoc value for this FichaComplementarDocumentoIn.
     * 
     * @return tipDoc
     */
    public java.lang.Integer getTipDoc() {
        return tipDoc;
    }


    /**
     * Sets the tipDoc value for this FichaComplementarDocumentoIn.
     * 
     * @param tipDoc
     */
    public void setTipDoc(java.lang.Integer tipDoc) {
        this.tipDoc = tipDoc;
    }


    /**
     * Gets the tipOpe value for this FichaComplementarDocumentoIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaComplementarDocumentoIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaComplementarDocumentoIn)) return false;
        FichaComplementarDocumentoIn other = (FichaComplementarDocumentoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.arqDoc==null && other.getArqDoc()==null) || 
             (this.arqDoc!=null &&
              this.arqDoc.equals(other.getArqDoc()))) &&
            ((this.datDoc==null && other.getDatDoc()==null) || 
             (this.datDoc!=null &&
              this.datDoc.equals(other.getDatDoc()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.nomArq==null && other.getNomArq()==null) || 
             (this.nomArq!=null &&
              this.nomArq.equals(other.getNomArq()))) &&
            ((this.nomeArquivo==null && other.getNomeArquivo()==null) || 
             (this.nomeArquivo!=null &&
              this.nomeArquivo.equals(other.getNomeArquivo()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.obsDoc==null && other.getObsDoc()==null) || 
             (this.obsDoc!=null &&
              this.obsDoc.equals(other.getObsDoc()))) &&
            ((this.seqDoc==null && other.getSeqDoc()==null) || 
             (this.seqDoc!=null &&
              this.seqDoc.equals(other.getSeqDoc()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipDoc==null && other.getTipDoc()==null) || 
             (this.tipDoc!=null &&
              this.tipDoc.equals(other.getTipDoc()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getArqDoc() != null) {
            _hashCode += getArqDoc().hashCode();
        }
        if (getDatDoc() != null) {
            _hashCode += getDatDoc().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNomArq() != null) {
            _hashCode += getNomArq().hashCode();
        }
        if (getNomeArquivo() != null) {
            _hashCode += getNomeArquivo().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getObsDoc() != null) {
            _hashCode += getObsDoc().hashCode();
        }
        if (getSeqDoc() != null) {
            _hashCode += getSeqDoc().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipDoc() != null) {
            _hashCode += getTipDoc().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaComplementarDocumentoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaComplementarDocumentoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arqDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "arqDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomArq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomArq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeArquivo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomeArquivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
