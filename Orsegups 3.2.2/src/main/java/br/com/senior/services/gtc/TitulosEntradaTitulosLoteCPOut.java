/**
 * TitulosEntradaTitulosLoteCPOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosEntradaTitulosLoteCPOut implements java.io.Serializable
{
	private java.lang.String erroExecucao;

	private TitulosEntradaTitulosLoteCPOutGridResult[] gridResult;

	private java.lang.String resultado;

	public TitulosEntradaTitulosLoteCPOut()
	{
	}

	public TitulosEntradaTitulosLoteCPOut(java.lang.String erroExecucao, TitulosEntradaTitulosLoteCPOutGridResult[] gridResult, java.lang.String resultado)
	{
		this.erroExecucao = erroExecucao;
		this.gridResult = gridResult;
		this.resultado = resultado;
	}

	/**
	 * Gets the erroExecucao value for this TitulosEntradaTitulosLoteCPOut.
	 * 
	 * @return erroExecucao
	 */
	public java.lang.String getErroExecucao()
	{
		return erroExecucao;
	}

	/**
	 * Sets the erroExecucao value for this TitulosEntradaTitulosLoteCPOut.
	 * 
	 * @param erroExecucao
	 */
	public void setErroExecucao(java.lang.String erroExecucao)
	{
		this.erroExecucao = erroExecucao;
	}

	/**
	 * Gets the gridResult value for this TitulosEntradaTitulosLoteCPOut.
	 * 
	 * @return gridResult
	 */
	public TitulosEntradaTitulosLoteCPOutGridResult[] getGridResult()
	{
		return gridResult;
	}

	/**
	 * Sets the gridResult value for this TitulosEntradaTitulosLoteCPOut.
	 * 
	 * @param gridResult
	 */
	public void setGridResult(TitulosEntradaTitulosLoteCPOutGridResult[] gridResult)
	{
		this.gridResult = gridResult;
	}

	public TitulosEntradaTitulosLoteCPOutGridResult getGridResult(int i)
	{
		return this.gridResult[i];
	}

	public void setGridResult(int i, TitulosEntradaTitulosLoteCPOutGridResult _value)
	{
		this.gridResult[i] = _value;
	}

	/**
	 * Gets the resultado value for this TitulosEntradaTitulosLoteCPOut.
	 * 
	 * @return resultado
	 */
	public java.lang.String getResultado()
	{
		return resultado;
	}

	/**
	 * Sets the resultado value for this TitulosEntradaTitulosLoteCPOut.
	 * 
	 * @param resultado
	 */
	public void setResultado(java.lang.String resultado)
	{
		this.resultado = resultado;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj)
	{
		if (!(obj instanceof TitulosEntradaTitulosLoteCPOut))
			return false;
		TitulosEntradaTitulosLoteCPOut other = (TitulosEntradaTitulosLoteCPOut) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null)
		{
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.erroExecucao == null && other.getErroExecucao() == null) || (this.erroExecucao != null && this.erroExecucao.equals(other.getErroExecucao()))) && ((this.gridResult == null && other.getGridResult() == null) || (this.gridResult != null && java.util.Arrays.equals(this.gridResult, other.getGridResult()))) && ((this.resultado == null && other.getResultado() == null) || (this.resultado != null && this.resultado.equals(other.getResultado())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode()
	{
		if (__hashCodeCalc)
		{
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getErroExecucao() != null)
		{
			_hashCode += getErroExecucao().hashCode();
		}
		if (getGridResult() != null)
		{
			for (int i = 0; i < java.lang.reflect.Array.getLength(getGridResult()); i++)
			{
				java.lang.Object obj = java.lang.reflect.Array.get(getGridResult(), i);
				if (obj != null && !obj.getClass().isArray())
				{
					_hashCode += obj.hashCode();
				}
			}
		}
		if (getResultado() != null)
		{
			_hashCode += getResultado().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(TitulosEntradaTitulosLoteCPOut.class, true);

	static
	{
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEntradaTitulosLoteCPOut"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("erroExecucao");
		elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("gridResult");
		elemField.setXmlName(new javax.xml.namespace.QName("", "gridResult"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEntradaTitulosLoteCPOutGridResult"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		elemField.setMaxOccursUnbounded(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("resultado");
		elemField.setXmlName(new javax.xml.namespace.QName("", "resultado"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc()
	{
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
	{
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
	{
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
