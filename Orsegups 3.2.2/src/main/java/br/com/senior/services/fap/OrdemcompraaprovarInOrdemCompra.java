/**
 * OrdemcompraaprovarInOrdemCompra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public class OrdemcompraaprovarInOrdemCompra  implements java.io.Serializable {
    private java.lang.Integer codigoEmpresa;

    private java.lang.Integer codigoFilial;

    private java.lang.Integer numero;

    private java.lang.Integer sequencia;

    public OrdemcompraaprovarInOrdemCompra() {
    }

    public OrdemcompraaprovarInOrdemCompra(
           java.lang.Integer codigoEmpresa,
           java.lang.Integer codigoFilial,
           java.lang.Integer numero,
           java.lang.Integer sequencia) {
           this.codigoEmpresa = codigoEmpresa;
           this.codigoFilial = codigoFilial;
           this.numero = numero;
           this.sequencia = sequencia;
    }


    /**
     * Gets the codigoEmpresa value for this OrdemcompraaprovarInOrdemCompra.
     * 
     * @return codigoEmpresa
     */
    public java.lang.Integer getCodigoEmpresa() {
        return codigoEmpresa;
    }


    /**
     * Sets the codigoEmpresa value for this OrdemcompraaprovarInOrdemCompra.
     * 
     * @param codigoEmpresa
     */
    public void setCodigoEmpresa(java.lang.Integer codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }


    /**
     * Gets the codigoFilial value for this OrdemcompraaprovarInOrdemCompra.
     * 
     * @return codigoFilial
     */
    public java.lang.Integer getCodigoFilial() {
        return codigoFilial;
    }


    /**
     * Sets the codigoFilial value for this OrdemcompraaprovarInOrdemCompra.
     * 
     * @param codigoFilial
     */
    public void setCodigoFilial(java.lang.Integer codigoFilial) {
        this.codigoFilial = codigoFilial;
    }


    /**
     * Gets the numero value for this OrdemcompraaprovarInOrdemCompra.
     * 
     * @return numero
     */
    public java.lang.Integer getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this OrdemcompraaprovarInOrdemCompra.
     * 
     * @param numero
     */
    public void setNumero(java.lang.Integer numero) {
        this.numero = numero;
    }


    /**
     * Gets the sequencia value for this OrdemcompraaprovarInOrdemCompra.
     * 
     * @return sequencia
     */
    public java.lang.Integer getSequencia() {
        return sequencia;
    }


    /**
     * Sets the sequencia value for this OrdemcompraaprovarInOrdemCompra.
     * 
     * @param sequencia
     */
    public void setSequencia(java.lang.Integer sequencia) {
        this.sequencia = sequencia;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrdemcompraaprovarInOrdemCompra)) return false;
        OrdemcompraaprovarInOrdemCompra other = (OrdemcompraaprovarInOrdemCompra) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoEmpresa==null && other.getCodigoEmpresa()==null) || 
             (this.codigoEmpresa!=null &&
              this.codigoEmpresa.equals(other.getCodigoEmpresa()))) &&
            ((this.codigoFilial==null && other.getCodigoFilial()==null) || 
             (this.codigoFilial!=null &&
              this.codigoFilial.equals(other.getCodigoFilial()))) &&
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.sequencia==null && other.getSequencia()==null) || 
             (this.sequencia!=null &&
              this.sequencia.equals(other.getSequencia())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoEmpresa() != null) {
            _hashCode += getCodigoEmpresa().hashCode();
        }
        if (getCodigoFilial() != null) {
            _hashCode += getCodigoFilial().hashCode();
        }
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getSequencia() != null) {
            _hashCode += getSequencia().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrdemcompraaprovarInOrdemCompra.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcompraaprovarInOrdemCompra"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoEmpresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoEmpresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sequencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sequencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
