/**
 * HistoricosCargo2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosCargo2In  implements java.io.Serializable {
    private java.lang.String carVag;

    private java.lang.String codCar;

    private java.lang.Integer codCrr;

    private java.lang.Integer codMot;

    private java.lang.String datAlt;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String funEso;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Integer qtdConf;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    public HistoricosCargo2In() {
    }

    public HistoricosCargo2In(
           java.lang.String carVag,
           java.lang.String codCar,
           java.lang.Integer codCrr,
           java.lang.Integer codMot,
           java.lang.String datAlt,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String funEso,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Integer qtdConf,
           java.lang.Integer tipCol,
           java.lang.String tipOpe) {
           this.carVag = carVag;
           this.codCar = codCar;
           this.codCrr = codCrr;
           this.codMot = codMot;
           this.datAlt = datAlt;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.funEso = funEso;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.qtdConf = qtdConf;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the carVag value for this HistoricosCargo2In.
     * 
     * @return carVag
     */
    public java.lang.String getCarVag() {
        return carVag;
    }


    /**
     * Sets the carVag value for this HistoricosCargo2In.
     * 
     * @param carVag
     */
    public void setCarVag(java.lang.String carVag) {
        this.carVag = carVag;
    }


    /**
     * Gets the codCar value for this HistoricosCargo2In.
     * 
     * @return codCar
     */
    public java.lang.String getCodCar() {
        return codCar;
    }


    /**
     * Sets the codCar value for this HistoricosCargo2In.
     * 
     * @param codCar
     */
    public void setCodCar(java.lang.String codCar) {
        this.codCar = codCar;
    }


    /**
     * Gets the codCrr value for this HistoricosCargo2In.
     * 
     * @return codCrr
     */
    public java.lang.Integer getCodCrr() {
        return codCrr;
    }


    /**
     * Sets the codCrr value for this HistoricosCargo2In.
     * 
     * @param codCrr
     */
    public void setCodCrr(java.lang.Integer codCrr) {
        this.codCrr = codCrr;
    }


    /**
     * Gets the codMot value for this HistoricosCargo2In.
     * 
     * @return codMot
     */
    public java.lang.Integer getCodMot() {
        return codMot;
    }


    /**
     * Sets the codMot value for this HistoricosCargo2In.
     * 
     * @param codMot
     */
    public void setCodMot(java.lang.Integer codMot) {
        this.codMot = codMot;
    }


    /**
     * Gets the datAlt value for this HistoricosCargo2In.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosCargo2In.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosCargo2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosCargo2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosCargo2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosCargo2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the funEso value for this HistoricosCargo2In.
     * 
     * @return funEso
     */
    public java.lang.String getFunEso() {
        return funEso;
    }


    /**
     * Sets the funEso value for this HistoricosCargo2In.
     * 
     * @param funEso
     */
    public void setFunEso(java.lang.String funEso) {
        this.funEso = funEso;
    }


    /**
     * Gets the numCad value for this HistoricosCargo2In.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosCargo2In.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosCargo2In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosCargo2In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the qtdConf value for this HistoricosCargo2In.
     * 
     * @return qtdConf
     */
    public java.lang.Integer getQtdConf() {
        return qtdConf;
    }


    /**
     * Sets the qtdConf value for this HistoricosCargo2In.
     * 
     * @param qtdConf
     */
    public void setQtdConf(java.lang.Integer qtdConf) {
        this.qtdConf = qtdConf;
    }


    /**
     * Gets the tipCol value for this HistoricosCargo2In.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosCargo2In.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosCargo2In.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosCargo2In.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosCargo2In)) return false;
        HistoricosCargo2In other = (HistoricosCargo2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.carVag==null && other.getCarVag()==null) || 
             (this.carVag!=null &&
              this.carVag.equals(other.getCarVag()))) &&
            ((this.codCar==null && other.getCodCar()==null) || 
             (this.codCar!=null &&
              this.codCar.equals(other.getCodCar()))) &&
            ((this.codCrr==null && other.getCodCrr()==null) || 
             (this.codCrr!=null &&
              this.codCrr.equals(other.getCodCrr()))) &&
            ((this.codMot==null && other.getCodMot()==null) || 
             (this.codMot!=null &&
              this.codMot.equals(other.getCodMot()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.funEso==null && other.getFunEso()==null) || 
             (this.funEso!=null &&
              this.funEso.equals(other.getFunEso()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.qtdConf==null && other.getQtdConf()==null) || 
             (this.qtdConf!=null &&
              this.qtdConf.equals(other.getQtdConf()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCarVag() != null) {
            _hashCode += getCarVag().hashCode();
        }
        if (getCodCar() != null) {
            _hashCode += getCodCar().hashCode();
        }
        if (getCodCrr() != null) {
            _hashCode += getCodCrr().hashCode();
        }
        if (getCodMot() != null) {
            _hashCode += getCodMot().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getFunEso() != null) {
            _hashCode += getFunEso().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getQtdConf() != null) {
            _hashCode += getQtdConf().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosCargo2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosCargo2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carVag");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carVag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("funEso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "funEso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdConf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdConf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
