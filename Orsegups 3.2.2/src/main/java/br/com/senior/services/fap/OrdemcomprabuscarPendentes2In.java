/**
 * OrdemcomprabuscarPendentes2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public class OrdemcomprabuscarPendentes2In  implements java.io.Serializable {
    private java.lang.Integer codUsu;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer indicePagina;

    private java.lang.Integer limitePagina;

    public OrdemcomprabuscarPendentes2In() {
    }

    public OrdemcomprabuscarPendentes2In(
           java.lang.Integer codUsu,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer indicePagina,
           java.lang.Integer limitePagina) {
           this.codUsu = codUsu;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.indicePagina = indicePagina;
           this.limitePagina = limitePagina;
    }


    /**
     * Gets the codUsu value for this OrdemcomprabuscarPendentes2In.
     * 
     * @return codUsu
     */
    public java.lang.Integer getCodUsu() {
        return codUsu;
    }


    /**
     * Sets the codUsu value for this OrdemcomprabuscarPendentes2In.
     * 
     * @param codUsu
     */
    public void setCodUsu(java.lang.Integer codUsu) {
        this.codUsu = codUsu;
    }


    /**
     * Gets the flowInstanceID value for this OrdemcomprabuscarPendentes2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this OrdemcomprabuscarPendentes2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this OrdemcomprabuscarPendentes2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this OrdemcomprabuscarPendentes2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the indicePagina value for this OrdemcomprabuscarPendentes2In.
     * 
     * @return indicePagina
     */
    public java.lang.Integer getIndicePagina() {
        return indicePagina;
    }


    /**
     * Sets the indicePagina value for this OrdemcomprabuscarPendentes2In.
     * 
     * @param indicePagina
     */
    public void setIndicePagina(java.lang.Integer indicePagina) {
        this.indicePagina = indicePagina;
    }


    /**
     * Gets the limitePagina value for this OrdemcomprabuscarPendentes2In.
     * 
     * @return limitePagina
     */
    public java.lang.Integer getLimitePagina() {
        return limitePagina;
    }


    /**
     * Sets the limitePagina value for this OrdemcomprabuscarPendentes2In.
     * 
     * @param limitePagina
     */
    public void setLimitePagina(java.lang.Integer limitePagina) {
        this.limitePagina = limitePagina;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrdemcomprabuscarPendentes2In)) return false;
        OrdemcomprabuscarPendentes2In other = (OrdemcomprabuscarPendentes2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codUsu==null && other.getCodUsu()==null) || 
             (this.codUsu!=null &&
              this.codUsu.equals(other.getCodUsu()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.indicePagina==null && other.getIndicePagina()==null) || 
             (this.indicePagina!=null &&
              this.indicePagina.equals(other.getIndicePagina()))) &&
            ((this.limitePagina==null && other.getLimitePagina()==null) || 
             (this.limitePagina!=null &&
              this.limitePagina.equals(other.getLimitePagina())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodUsu() != null) {
            _hashCode += getCodUsu().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIndicePagina() != null) {
            _hashCode += getIndicePagina().hashCode();
        }
        if (getLimitePagina() != null) {
            _hashCode += getLimitePagina().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrdemcomprabuscarPendentes2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codUsu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codUsu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indicePagina");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indicePagina"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limitePagina");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limitePagina"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
