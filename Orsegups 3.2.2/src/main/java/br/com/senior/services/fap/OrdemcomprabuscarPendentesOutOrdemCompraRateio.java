/**
 * OrdemcomprabuscarPendentesOutOrdemCompraRateio.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public class OrdemcomprabuscarPendentesOutOrdemCompraRateio  implements java.io.Serializable {
    private java.lang.String abreviaturaCentroCustos;

    private java.lang.String codigoCentroCustos;

    private java.lang.Double percentualRateio;

    private java.lang.Double valorRateio;

    public OrdemcomprabuscarPendentesOutOrdemCompraRateio() {
    }

    public OrdemcomprabuscarPendentesOutOrdemCompraRateio(
           java.lang.String abreviaturaCentroCustos,
           java.lang.String codigoCentroCustos,
           java.lang.Double percentualRateio,
           java.lang.Double valorRateio) {
           this.abreviaturaCentroCustos = abreviaturaCentroCustos;
           this.codigoCentroCustos = codigoCentroCustos;
           this.percentualRateio = percentualRateio;
           this.valorRateio = valorRateio;
    }


    /**
     * Gets the abreviaturaCentroCustos value for this OrdemcomprabuscarPendentesOutOrdemCompraRateio.
     * 
     * @return abreviaturaCentroCustos
     */
    public java.lang.String getAbreviaturaCentroCustos() {
        return abreviaturaCentroCustos;
    }


    /**
     * Sets the abreviaturaCentroCustos value for this OrdemcomprabuscarPendentesOutOrdemCompraRateio.
     * 
     * @param abreviaturaCentroCustos
     */
    public void setAbreviaturaCentroCustos(java.lang.String abreviaturaCentroCustos) {
        this.abreviaturaCentroCustos = abreviaturaCentroCustos;
    }


    /**
     * Gets the codigoCentroCustos value for this OrdemcomprabuscarPendentesOutOrdemCompraRateio.
     * 
     * @return codigoCentroCustos
     */
    public java.lang.String getCodigoCentroCustos() {
        return codigoCentroCustos;
    }


    /**
     * Sets the codigoCentroCustos value for this OrdemcomprabuscarPendentesOutOrdemCompraRateio.
     * 
     * @param codigoCentroCustos
     */
    public void setCodigoCentroCustos(java.lang.String codigoCentroCustos) {
        this.codigoCentroCustos = codigoCentroCustos;
    }


    /**
     * Gets the percentualRateio value for this OrdemcomprabuscarPendentesOutOrdemCompraRateio.
     * 
     * @return percentualRateio
     */
    public java.lang.Double getPercentualRateio() {
        return percentualRateio;
    }


    /**
     * Sets the percentualRateio value for this OrdemcomprabuscarPendentesOutOrdemCompraRateio.
     * 
     * @param percentualRateio
     */
    public void setPercentualRateio(java.lang.Double percentualRateio) {
        this.percentualRateio = percentualRateio;
    }


    /**
     * Gets the valorRateio value for this OrdemcomprabuscarPendentesOutOrdemCompraRateio.
     * 
     * @return valorRateio
     */
    public java.lang.Double getValorRateio() {
        return valorRateio;
    }


    /**
     * Sets the valorRateio value for this OrdemcomprabuscarPendentesOutOrdemCompraRateio.
     * 
     * @param valorRateio
     */
    public void setValorRateio(java.lang.Double valorRateio) {
        this.valorRateio = valorRateio;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrdemcomprabuscarPendentesOutOrdemCompraRateio)) return false;
        OrdemcomprabuscarPendentesOutOrdemCompraRateio other = (OrdemcomprabuscarPendentesOutOrdemCompraRateio) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.abreviaturaCentroCustos==null && other.getAbreviaturaCentroCustos()==null) || 
             (this.abreviaturaCentroCustos!=null &&
              this.abreviaturaCentroCustos.equals(other.getAbreviaturaCentroCustos()))) &&
            ((this.codigoCentroCustos==null && other.getCodigoCentroCustos()==null) || 
             (this.codigoCentroCustos!=null &&
              this.codigoCentroCustos.equals(other.getCodigoCentroCustos()))) &&
            ((this.percentualRateio==null && other.getPercentualRateio()==null) || 
             (this.percentualRateio!=null &&
              this.percentualRateio.equals(other.getPercentualRateio()))) &&
            ((this.valorRateio==null && other.getValorRateio()==null) || 
             (this.valorRateio!=null &&
              this.valorRateio.equals(other.getValorRateio())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAbreviaturaCentroCustos() != null) {
            _hashCode += getAbreviaturaCentroCustos().hashCode();
        }
        if (getCodigoCentroCustos() != null) {
            _hashCode += getCodigoCentroCustos().hashCode();
        }
        if (getPercentualRateio() != null) {
            _hashCode += getPercentualRateio().hashCode();
        }
        if (getValorRateio() != null) {
            _hashCode += getValorRateio().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrdemcomprabuscarPendentesOutOrdemCompraRateio.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompraRateio"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abreviaturaCentroCustos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abreviaturaCentroCustos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoCentroCustos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoCentroCustos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("percentualRateio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "percentualRateio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorRateio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorRateio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
