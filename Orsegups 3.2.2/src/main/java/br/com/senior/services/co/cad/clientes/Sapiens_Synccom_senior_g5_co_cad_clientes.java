/**
 * Sapiens_Synccom_senior_g5_co_cad_clientes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public interface Sapiens_Synccom_senior_g5_co_cad_clientes extends java.rmi.Remote {
    public br.com.senior.services.co.cad.clientes.ClientesCadastrarContatoClienteOut cadastrarContatoCliente(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesCadastrarContatoClienteIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesConsultarContatoClienteOut consultarContatoCliente(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesConsultarContatoClienteIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesCadastrarCartaoClienteOut cadastrarCartaoCliente(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesCadastrarCartaoClienteIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesExportarClientesCompletoOut exportarClientesCompleto(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesExportarClientesCompletoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesConsultarGeral2Out consultarGeral_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesConsultarGeral2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesExportar2Out exportar_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesExportar2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesExportarOut exportar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesExportarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesConsultarGeralOut consultarGeral(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesConsultarGeralIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOut consultarFiscal(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOut consultarCadastro(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroIn parameters) throws java.rmi.RemoteException;
}
