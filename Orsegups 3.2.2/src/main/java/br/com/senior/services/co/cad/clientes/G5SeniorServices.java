/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getsapiens_Synccom_senior_g5_co_cad_clientesPortAddress();

    public br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientes getsapiens_Synccom_senior_g5_co_cad_clientesPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientes getsapiens_Synccom_senior_g5_co_cad_clientesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
