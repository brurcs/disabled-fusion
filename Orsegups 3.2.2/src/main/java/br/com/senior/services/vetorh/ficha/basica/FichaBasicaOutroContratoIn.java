/**
 * FichaBasicaOutroContratoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaOutroContratoIn  implements java.io.Serializable {
    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Integer outCad;

    private java.lang.String outCon;

    private java.lang.Integer outEmp;

    private java.lang.String outTet;

    private java.lang.Integer outTip;

    public FichaBasicaOutroContratoIn() {
    }

    public FichaBasicaOutroContratoIn(
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Integer outCad,
           java.lang.String outCon,
           java.lang.Integer outEmp,
           java.lang.String outTet,
           java.lang.Integer outTip) {
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.outCad = outCad;
           this.outCon = outCon;
           this.outEmp = outEmp;
           this.outTet = outTet;
           this.outTip = outTip;
    }


    /**
     * Gets the flowInstanceID value for this FichaBasicaOutroContratoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this FichaBasicaOutroContratoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this FichaBasicaOutroContratoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this FichaBasicaOutroContratoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the numCad value for this FichaBasicaOutroContratoIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this FichaBasicaOutroContratoIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this FichaBasicaOutroContratoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this FichaBasicaOutroContratoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the outCad value for this FichaBasicaOutroContratoIn.
     * 
     * @return outCad
     */
    public java.lang.Integer getOutCad() {
        return outCad;
    }


    /**
     * Sets the outCad value for this FichaBasicaOutroContratoIn.
     * 
     * @param outCad
     */
    public void setOutCad(java.lang.Integer outCad) {
        this.outCad = outCad;
    }


    /**
     * Gets the outCon value for this FichaBasicaOutroContratoIn.
     * 
     * @return outCon
     */
    public java.lang.String getOutCon() {
        return outCon;
    }


    /**
     * Sets the outCon value for this FichaBasicaOutroContratoIn.
     * 
     * @param outCon
     */
    public void setOutCon(java.lang.String outCon) {
        this.outCon = outCon;
    }


    /**
     * Gets the outEmp value for this FichaBasicaOutroContratoIn.
     * 
     * @return outEmp
     */
    public java.lang.Integer getOutEmp() {
        return outEmp;
    }


    /**
     * Sets the outEmp value for this FichaBasicaOutroContratoIn.
     * 
     * @param outEmp
     */
    public void setOutEmp(java.lang.Integer outEmp) {
        this.outEmp = outEmp;
    }


    /**
     * Gets the outTet value for this FichaBasicaOutroContratoIn.
     * 
     * @return outTet
     */
    public java.lang.String getOutTet() {
        return outTet;
    }


    /**
     * Sets the outTet value for this FichaBasicaOutroContratoIn.
     * 
     * @param outTet
     */
    public void setOutTet(java.lang.String outTet) {
        this.outTet = outTet;
    }


    /**
     * Gets the outTip value for this FichaBasicaOutroContratoIn.
     * 
     * @return outTip
     */
    public java.lang.Integer getOutTip() {
        return outTip;
    }


    /**
     * Sets the outTip value for this FichaBasicaOutroContratoIn.
     * 
     * @param outTip
     */
    public void setOutTip(java.lang.Integer outTip) {
        this.outTip = outTip;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaOutroContratoIn)) return false;
        FichaBasicaOutroContratoIn other = (FichaBasicaOutroContratoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.outCad==null && other.getOutCad()==null) || 
             (this.outCad!=null &&
              this.outCad.equals(other.getOutCad()))) &&
            ((this.outCon==null && other.getOutCon()==null) || 
             (this.outCon!=null &&
              this.outCon.equals(other.getOutCon()))) &&
            ((this.outEmp==null && other.getOutEmp()==null) || 
             (this.outEmp!=null &&
              this.outEmp.equals(other.getOutEmp()))) &&
            ((this.outTet==null && other.getOutTet()==null) || 
             (this.outTet!=null &&
              this.outTet.equals(other.getOutTet()))) &&
            ((this.outTip==null && other.getOutTip()==null) || 
             (this.outTip!=null &&
              this.outTip.equals(other.getOutTip())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getOutCad() != null) {
            _hashCode += getOutCad().hashCode();
        }
        if (getOutCon() != null) {
            _hashCode += getOutCon().hashCode();
        }
        if (getOutEmp() != null) {
            _hashCode += getOutEmp().hashCode();
        }
        if (getOutTet() != null) {
            _hashCode += getOutTet().hashCode();
        }
        if (getOutTip() != null) {
            _hashCode += getOutTip().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaOutroContratoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaOutroContratoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outTet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outTet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outTip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outTip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
