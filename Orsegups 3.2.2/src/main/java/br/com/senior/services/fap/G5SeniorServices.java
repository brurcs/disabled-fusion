/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortAddress();

    public br.com.senior.services.fap.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.fap.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
