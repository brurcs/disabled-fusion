/**
 * OrdemcomprabuscarPendentes3OutOrdemCompraItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public class OrdemcomprabuscarPendentes3OutOrdemCompraItem  implements java.io.Serializable {
    private java.lang.String descricao;

    private java.lang.String descricaoMoeda;

    private java.lang.String observacao;

    private java.lang.Double quantidadeSolicitada;

    private br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraItemRateio[] rateio;

    private java.lang.String siglaMoeda;

    private java.lang.String unidadeMedida;

    private java.lang.Double valorLiquido;

    private java.lang.Double valorLiquidoMoeda;

    private java.lang.Double valorUnitario;

    private java.lang.Double vlrBru;

    private java.lang.Double vlrDs1;

    private java.lang.Double vlrDs2;

    private java.lang.Double vlrDs3;

    private java.lang.Double vlrDs4;

    private java.lang.Double vlrDs5;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrEmb;

    private java.lang.Double vlrFre;

    private java.lang.Double vlrLpr;

    private java.lang.Double vlrLse;

    private java.lang.Double vlrOut;

    private java.lang.Double vlrSeg;

    public OrdemcomprabuscarPendentes3OutOrdemCompraItem() {
    }

    public OrdemcomprabuscarPendentes3OutOrdemCompraItem(
           java.lang.String descricao,
           java.lang.String descricaoMoeda,
           java.lang.String observacao,
           java.lang.Double quantidadeSolicitada,
           br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraItemRateio[] rateio,
           java.lang.String siglaMoeda,
           java.lang.String unidadeMedida,
           java.lang.Double valorLiquido,
           java.lang.Double valorLiquidoMoeda,
           java.lang.Double valorUnitario,
           java.lang.Double vlrBru,
           java.lang.Double vlrDs1,
           java.lang.Double vlrDs2,
           java.lang.Double vlrDs3,
           java.lang.Double vlrDs4,
           java.lang.Double vlrDs5,
           java.lang.Double vlrDsc,
           java.lang.Double vlrEmb,
           java.lang.Double vlrFre,
           java.lang.Double vlrLpr,
           java.lang.Double vlrLse,
           java.lang.Double vlrOut,
           java.lang.Double vlrSeg) {
           this.descricao = descricao;
           this.descricaoMoeda = descricaoMoeda;
           this.observacao = observacao;
           this.quantidadeSolicitada = quantidadeSolicitada;
           this.rateio = rateio;
           this.siglaMoeda = siglaMoeda;
           this.unidadeMedida = unidadeMedida;
           this.valorLiquido = valorLiquido;
           this.valorLiquidoMoeda = valorLiquidoMoeda;
           this.valorUnitario = valorUnitario;
           this.vlrBru = vlrBru;
           this.vlrDs1 = vlrDs1;
           this.vlrDs2 = vlrDs2;
           this.vlrDs3 = vlrDs3;
           this.vlrDs4 = vlrDs4;
           this.vlrDs5 = vlrDs5;
           this.vlrDsc = vlrDsc;
           this.vlrEmb = vlrEmb;
           this.vlrFre = vlrFre;
           this.vlrLpr = vlrLpr;
           this.vlrLse = vlrLse;
           this.vlrOut = vlrOut;
           this.vlrSeg = vlrSeg;
    }


    /**
     * Gets the descricao value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return descricao
     */
    public java.lang.String getDescricao() {
        return descricao;
    }


    /**
     * Sets the descricao value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param descricao
     */
    public void setDescricao(java.lang.String descricao) {
        this.descricao = descricao;
    }


    /**
     * Gets the descricaoMoeda value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return descricaoMoeda
     */
    public java.lang.String getDescricaoMoeda() {
        return descricaoMoeda;
    }


    /**
     * Sets the descricaoMoeda value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param descricaoMoeda
     */
    public void setDescricaoMoeda(java.lang.String descricaoMoeda) {
        this.descricaoMoeda = descricaoMoeda;
    }


    /**
     * Gets the observacao value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return observacao
     */
    public java.lang.String getObservacao() {
        return observacao;
    }


    /**
     * Sets the observacao value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param observacao
     */
    public void setObservacao(java.lang.String observacao) {
        this.observacao = observacao;
    }


    /**
     * Gets the quantidadeSolicitada value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return quantidadeSolicitada
     */
    public java.lang.Double getQuantidadeSolicitada() {
        return quantidadeSolicitada;
    }


    /**
     * Sets the quantidadeSolicitada value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param quantidadeSolicitada
     */
    public void setQuantidadeSolicitada(java.lang.Double quantidadeSolicitada) {
        this.quantidadeSolicitada = quantidadeSolicitada;
    }


    /**
     * Gets the rateio value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return rateio
     */
    public br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraItemRateio[] getRateio() {
        return rateio;
    }


    /**
     * Sets the rateio value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param rateio
     */
    public void setRateio(br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraItemRateio[] rateio) {
        this.rateio = rateio;
    }

    public br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraItemRateio getRateio(int i) {
        return this.rateio[i];
    }

    public void setRateio(int i, br.com.senior.services.fap.OrdemcomprabuscarPendentes3OutOrdemCompraItemRateio _value) {
        this.rateio[i] = _value;
    }


    /**
     * Gets the siglaMoeda value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return siglaMoeda
     */
    public java.lang.String getSiglaMoeda() {
        return siglaMoeda;
    }


    /**
     * Sets the siglaMoeda value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param siglaMoeda
     */
    public void setSiglaMoeda(java.lang.String siglaMoeda) {
        this.siglaMoeda = siglaMoeda;
    }


    /**
     * Gets the unidadeMedida value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return unidadeMedida
     */
    public java.lang.String getUnidadeMedida() {
        return unidadeMedida;
    }


    /**
     * Sets the unidadeMedida value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param unidadeMedida
     */
    public void setUnidadeMedida(java.lang.String unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }


    /**
     * Gets the valorLiquido value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return valorLiquido
     */
    public java.lang.Double getValorLiquido() {
        return valorLiquido;
    }


    /**
     * Sets the valorLiquido value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param valorLiquido
     */
    public void setValorLiquido(java.lang.Double valorLiquido) {
        this.valorLiquido = valorLiquido;
    }


    /**
     * Gets the valorLiquidoMoeda value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return valorLiquidoMoeda
     */
    public java.lang.Double getValorLiquidoMoeda() {
        return valorLiquidoMoeda;
    }


    /**
     * Sets the valorLiquidoMoeda value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param valorLiquidoMoeda
     */
    public void setValorLiquidoMoeda(java.lang.Double valorLiquidoMoeda) {
        this.valorLiquidoMoeda = valorLiquidoMoeda;
    }


    /**
     * Gets the valorUnitario value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return valorUnitario
     */
    public java.lang.Double getValorUnitario() {
        return valorUnitario;
    }


    /**
     * Sets the valorUnitario value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param valorUnitario
     */
    public void setValorUnitario(java.lang.Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }


    /**
     * Gets the vlrBru value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrBru
     */
    public java.lang.Double getVlrBru() {
        return vlrBru;
    }


    /**
     * Sets the vlrBru value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrBru
     */
    public void setVlrBru(java.lang.Double vlrBru) {
        this.vlrBru = vlrBru;
    }


    /**
     * Gets the vlrDs1 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrDs1
     */
    public java.lang.Double getVlrDs1() {
        return vlrDs1;
    }


    /**
     * Sets the vlrDs1 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrDs1
     */
    public void setVlrDs1(java.lang.Double vlrDs1) {
        this.vlrDs1 = vlrDs1;
    }


    /**
     * Gets the vlrDs2 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrDs2
     */
    public java.lang.Double getVlrDs2() {
        return vlrDs2;
    }


    /**
     * Sets the vlrDs2 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrDs2
     */
    public void setVlrDs2(java.lang.Double vlrDs2) {
        this.vlrDs2 = vlrDs2;
    }


    /**
     * Gets the vlrDs3 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrDs3
     */
    public java.lang.Double getVlrDs3() {
        return vlrDs3;
    }


    /**
     * Sets the vlrDs3 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrDs3
     */
    public void setVlrDs3(java.lang.Double vlrDs3) {
        this.vlrDs3 = vlrDs3;
    }


    /**
     * Gets the vlrDs4 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrDs4
     */
    public java.lang.Double getVlrDs4() {
        return vlrDs4;
    }


    /**
     * Sets the vlrDs4 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrDs4
     */
    public void setVlrDs4(java.lang.Double vlrDs4) {
        this.vlrDs4 = vlrDs4;
    }


    /**
     * Gets the vlrDs5 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrDs5
     */
    public java.lang.Double getVlrDs5() {
        return vlrDs5;
    }


    /**
     * Sets the vlrDs5 value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrDs5
     */
    public void setVlrDs5(java.lang.Double vlrDs5) {
        this.vlrDs5 = vlrDs5;
    }


    /**
     * Gets the vlrDsc value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrEmb value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrEmb
     */
    public java.lang.Double getVlrEmb() {
        return vlrEmb;
    }


    /**
     * Sets the vlrEmb value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrEmb
     */
    public void setVlrEmb(java.lang.Double vlrEmb) {
        this.vlrEmb = vlrEmb;
    }


    /**
     * Gets the vlrFre value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrFre
     */
    public java.lang.Double getVlrFre() {
        return vlrFre;
    }


    /**
     * Sets the vlrFre value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrFre
     */
    public void setVlrFre(java.lang.Double vlrFre) {
        this.vlrFre = vlrFre;
    }


    /**
     * Gets the vlrLpr value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrLpr
     */
    public java.lang.Double getVlrLpr() {
        return vlrLpr;
    }


    /**
     * Sets the vlrLpr value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrLpr
     */
    public void setVlrLpr(java.lang.Double vlrLpr) {
        this.vlrLpr = vlrLpr;
    }


    /**
     * Gets the vlrLse value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrLse
     */
    public java.lang.Double getVlrLse() {
        return vlrLse;
    }


    /**
     * Sets the vlrLse value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrLse
     */
    public void setVlrLse(java.lang.Double vlrLse) {
        this.vlrLse = vlrLse;
    }


    /**
     * Gets the vlrOut value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrOut
     */
    public java.lang.Double getVlrOut() {
        return vlrOut;
    }


    /**
     * Sets the vlrOut value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrOut
     */
    public void setVlrOut(java.lang.Double vlrOut) {
        this.vlrOut = vlrOut;
    }


    /**
     * Gets the vlrSeg value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @return vlrSeg
     */
    public java.lang.Double getVlrSeg() {
        return vlrSeg;
    }


    /**
     * Sets the vlrSeg value for this OrdemcomprabuscarPendentes3OutOrdemCompraItem.
     * 
     * @param vlrSeg
     */
    public void setVlrSeg(java.lang.Double vlrSeg) {
        this.vlrSeg = vlrSeg;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrdemcomprabuscarPendentes3OutOrdemCompraItem)) return false;
        OrdemcomprabuscarPendentes3OutOrdemCompraItem other = (OrdemcomprabuscarPendentes3OutOrdemCompraItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.descricao==null && other.getDescricao()==null) || 
             (this.descricao!=null &&
              this.descricao.equals(other.getDescricao()))) &&
            ((this.descricaoMoeda==null && other.getDescricaoMoeda()==null) || 
             (this.descricaoMoeda!=null &&
              this.descricaoMoeda.equals(other.getDescricaoMoeda()))) &&
            ((this.observacao==null && other.getObservacao()==null) || 
             (this.observacao!=null &&
              this.observacao.equals(other.getObservacao()))) &&
            ((this.quantidadeSolicitada==null && other.getQuantidadeSolicitada()==null) || 
             (this.quantidadeSolicitada!=null &&
              this.quantidadeSolicitada.equals(other.getQuantidadeSolicitada()))) &&
            ((this.rateio==null && other.getRateio()==null) || 
             (this.rateio!=null &&
              java.util.Arrays.equals(this.rateio, other.getRateio()))) &&
            ((this.siglaMoeda==null && other.getSiglaMoeda()==null) || 
             (this.siglaMoeda!=null &&
              this.siglaMoeda.equals(other.getSiglaMoeda()))) &&
            ((this.unidadeMedida==null && other.getUnidadeMedida()==null) || 
             (this.unidadeMedida!=null &&
              this.unidadeMedida.equals(other.getUnidadeMedida()))) &&
            ((this.valorLiquido==null && other.getValorLiquido()==null) || 
             (this.valorLiquido!=null &&
              this.valorLiquido.equals(other.getValorLiquido()))) &&
            ((this.valorLiquidoMoeda==null && other.getValorLiquidoMoeda()==null) || 
             (this.valorLiquidoMoeda!=null &&
              this.valorLiquidoMoeda.equals(other.getValorLiquidoMoeda()))) &&
            ((this.valorUnitario==null && other.getValorUnitario()==null) || 
             (this.valorUnitario!=null &&
              this.valorUnitario.equals(other.getValorUnitario()))) &&
            ((this.vlrBru==null && other.getVlrBru()==null) || 
             (this.vlrBru!=null &&
              this.vlrBru.equals(other.getVlrBru()))) &&
            ((this.vlrDs1==null && other.getVlrDs1()==null) || 
             (this.vlrDs1!=null &&
              this.vlrDs1.equals(other.getVlrDs1()))) &&
            ((this.vlrDs2==null && other.getVlrDs2()==null) || 
             (this.vlrDs2!=null &&
              this.vlrDs2.equals(other.getVlrDs2()))) &&
            ((this.vlrDs3==null && other.getVlrDs3()==null) || 
             (this.vlrDs3!=null &&
              this.vlrDs3.equals(other.getVlrDs3()))) &&
            ((this.vlrDs4==null && other.getVlrDs4()==null) || 
             (this.vlrDs4!=null &&
              this.vlrDs4.equals(other.getVlrDs4()))) &&
            ((this.vlrDs5==null && other.getVlrDs5()==null) || 
             (this.vlrDs5!=null &&
              this.vlrDs5.equals(other.getVlrDs5()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrEmb==null && other.getVlrEmb()==null) || 
             (this.vlrEmb!=null &&
              this.vlrEmb.equals(other.getVlrEmb()))) &&
            ((this.vlrFre==null && other.getVlrFre()==null) || 
             (this.vlrFre!=null &&
              this.vlrFre.equals(other.getVlrFre()))) &&
            ((this.vlrLpr==null && other.getVlrLpr()==null) || 
             (this.vlrLpr!=null &&
              this.vlrLpr.equals(other.getVlrLpr()))) &&
            ((this.vlrLse==null && other.getVlrLse()==null) || 
             (this.vlrLse!=null &&
              this.vlrLse.equals(other.getVlrLse()))) &&
            ((this.vlrOut==null && other.getVlrOut()==null) || 
             (this.vlrOut!=null &&
              this.vlrOut.equals(other.getVlrOut()))) &&
            ((this.vlrSeg==null && other.getVlrSeg()==null) || 
             (this.vlrSeg!=null &&
              this.vlrSeg.equals(other.getVlrSeg())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescricao() != null) {
            _hashCode += getDescricao().hashCode();
        }
        if (getDescricaoMoeda() != null) {
            _hashCode += getDescricaoMoeda().hashCode();
        }
        if (getObservacao() != null) {
            _hashCode += getObservacao().hashCode();
        }
        if (getQuantidadeSolicitada() != null) {
            _hashCode += getQuantidadeSolicitada().hashCode();
        }
        if (getRateio() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRateio());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRateio(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSiglaMoeda() != null) {
            _hashCode += getSiglaMoeda().hashCode();
        }
        if (getUnidadeMedida() != null) {
            _hashCode += getUnidadeMedida().hashCode();
        }
        if (getValorLiquido() != null) {
            _hashCode += getValorLiquido().hashCode();
        }
        if (getValorLiquidoMoeda() != null) {
            _hashCode += getValorLiquidoMoeda().hashCode();
        }
        if (getValorUnitario() != null) {
            _hashCode += getValorUnitario().hashCode();
        }
        if (getVlrBru() != null) {
            _hashCode += getVlrBru().hashCode();
        }
        if (getVlrDs1() != null) {
            _hashCode += getVlrDs1().hashCode();
        }
        if (getVlrDs2() != null) {
            _hashCode += getVlrDs2().hashCode();
        }
        if (getVlrDs3() != null) {
            _hashCode += getVlrDs3().hashCode();
        }
        if (getVlrDs4() != null) {
            _hashCode += getVlrDs4().hashCode();
        }
        if (getVlrDs5() != null) {
            _hashCode += getVlrDs5().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrEmb() != null) {
            _hashCode += getVlrEmb().hashCode();
        }
        if (getVlrFre() != null) {
            _hashCode += getVlrFre().hashCode();
        }
        if (getVlrLpr() != null) {
            _hashCode += getVlrLpr().hashCode();
        }
        if (getVlrLse() != null) {
            _hashCode += getVlrLse().hashCode();
        }
        if (getVlrOut() != null) {
            _hashCode += getVlrOut().hashCode();
        }
        if (getVlrSeg() != null) {
            _hashCode += getVlrSeg().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrdemcomprabuscarPendentes3OutOrdemCompraItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3OutOrdemCompraItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoMoeda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricaoMoeda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeSolicitada");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quantidadeSolicitada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rateio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentes3OutOrdemCompraItemRateio"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siglaMoeda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "siglaMoeda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unidadeMedida");
        elemField.setXmlName(new javax.xml.namespace.QName("", "unidadeMedida"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorLiquido");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorLiquido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorLiquidoMoeda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorLiquidoMoeda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorUnitario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorUnitario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrBru");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrBru"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDs1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDs1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDs2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDs2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDs3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDs3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDs4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDs4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDs5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDs5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrEmb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrEmb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrLpr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrLpr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrLse");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrLse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrSeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrSeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
