/**
 * HistoricosDocumentosAfastamentoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosDocumentosAfastamentoIn  implements java.io.Serializable {
    private java.lang.String datAfa;

    private java.lang.String datArq;

    private java.lang.String desArq;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String horAfa;

    private java.lang.String imagem;

    private java.lang.String nomArq;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Integer tipCol;

    private java.lang.String URL;

    public HistoricosDocumentosAfastamentoIn() {
    }

    public HistoricosDocumentosAfastamentoIn(
           java.lang.String datAfa,
           java.lang.String datArq,
           java.lang.String desArq,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String horAfa,
           java.lang.String imagem,
           java.lang.String nomArq,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Integer tipCol,
           java.lang.String URL) {
           this.datAfa = datAfa;
           this.datArq = datArq;
           this.desArq = desArq;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.horAfa = horAfa;
           this.imagem = imagem;
           this.nomArq = nomArq;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.tipCol = tipCol;
           this.URL = URL;
    }


    /**
     * Gets the datAfa value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return datAfa
     */
    public java.lang.String getDatAfa() {
        return datAfa;
    }


    /**
     * Sets the datAfa value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param datAfa
     */
    public void setDatAfa(java.lang.String datAfa) {
        this.datAfa = datAfa;
    }


    /**
     * Gets the datArq value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return datArq
     */
    public java.lang.String getDatArq() {
        return datArq;
    }


    /**
     * Sets the datArq value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param datArq
     */
    public void setDatArq(java.lang.String datArq) {
        this.datArq = datArq;
    }


    /**
     * Gets the desArq value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return desArq
     */
    public java.lang.String getDesArq() {
        return desArq;
    }


    /**
     * Sets the desArq value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param desArq
     */
    public void setDesArq(java.lang.String desArq) {
        this.desArq = desArq;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the horAfa value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return horAfa
     */
    public java.lang.String getHorAfa() {
        return horAfa;
    }


    /**
     * Sets the horAfa value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param horAfa
     */
    public void setHorAfa(java.lang.String horAfa) {
        this.horAfa = horAfa;
    }


    /**
     * Gets the imagem value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return imagem
     */
    public java.lang.String getImagem() {
        return imagem;
    }


    /**
     * Sets the imagem value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param imagem
     */
    public void setImagem(java.lang.String imagem) {
        this.imagem = imagem;
    }


    /**
     * Gets the nomArq value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return nomArq
     */
    public java.lang.String getNomArq() {
        return nomArq;
    }


    /**
     * Sets the nomArq value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param nomArq
     */
    public void setNomArq(java.lang.String nomArq) {
        this.nomArq = nomArq;
    }


    /**
     * Gets the numCad value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the tipCol value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the URL value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @return URL
     */
    public java.lang.String getURL() {
        return URL;
    }


    /**
     * Sets the URL value for this HistoricosDocumentosAfastamentoIn.
     * 
     * @param URL
     */
    public void setURL(java.lang.String URL) {
        this.URL = URL;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosDocumentosAfastamentoIn)) return false;
        HistoricosDocumentosAfastamentoIn other = (HistoricosDocumentosAfastamentoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.datAfa==null && other.getDatAfa()==null) || 
             (this.datAfa!=null &&
              this.datAfa.equals(other.getDatAfa()))) &&
            ((this.datArq==null && other.getDatArq()==null) || 
             (this.datArq!=null &&
              this.datArq.equals(other.getDatArq()))) &&
            ((this.desArq==null && other.getDesArq()==null) || 
             (this.desArq!=null &&
              this.desArq.equals(other.getDesArq()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.horAfa==null && other.getHorAfa()==null) || 
             (this.horAfa!=null &&
              this.horAfa.equals(other.getHorAfa()))) &&
            ((this.imagem==null && other.getImagem()==null) || 
             (this.imagem!=null &&
              this.imagem.equals(other.getImagem()))) &&
            ((this.nomArq==null && other.getNomArq()==null) || 
             (this.nomArq!=null &&
              this.nomArq.equals(other.getNomArq()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.URL==null && other.getURL()==null) || 
             (this.URL!=null &&
              this.URL.equals(other.getURL())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDatAfa() != null) {
            _hashCode += getDatAfa().hashCode();
        }
        if (getDatArq() != null) {
            _hashCode += getDatArq().hashCode();
        }
        if (getDesArq() != null) {
            _hashCode += getDesArq().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getHorAfa() != null) {
            _hashCode += getHorAfa().hashCode();
        }
        if (getImagem() != null) {
            _hashCode += getImagem().hashCode();
        }
        if (getNomArq() != null) {
            _hashCode += getNomArq().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getURL() != null) {
            _hashCode += getURL().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosDocumentosAfastamentoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosDocumentosAfastamentoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datArq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datArq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desArq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desArq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imagem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomArq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomArq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("URL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "URL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
