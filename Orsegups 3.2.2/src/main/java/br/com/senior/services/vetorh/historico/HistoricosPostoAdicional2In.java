/**
 * HistoricosPostoAdicional2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosPostoAdicional2In  implements java.io.Serializable {
    private java.lang.Integer cadSbs;

    private java.lang.Integer codTap;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String funEso;

    private java.lang.String iniAtu;

    private java.lang.Integer motAlt;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.String obsHpo;

    private java.lang.Integer perAdi;

    private java.lang.String posTra;

    private java.lang.Integer seqHis;

    private java.lang.String terAtu;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    private java.lang.Integer tipSbs;

    public HistoricosPostoAdicional2In() {
    }

    public HistoricosPostoAdicional2In(
           java.lang.Integer cadSbs,
           java.lang.Integer codTap,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String funEso,
           java.lang.String iniAtu,
           java.lang.Integer motAlt,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.String obsHpo,
           java.lang.Integer perAdi,
           java.lang.String posTra,
           java.lang.Integer seqHis,
           java.lang.String terAtu,
           java.lang.Integer tipCol,
           java.lang.String tipOpe,
           java.lang.Integer tipSbs) {
           this.cadSbs = cadSbs;
           this.codTap = codTap;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.funEso = funEso;
           this.iniAtu = iniAtu;
           this.motAlt = motAlt;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.obsHpo = obsHpo;
           this.perAdi = perAdi;
           this.posTra = posTra;
           this.seqHis = seqHis;
           this.terAtu = terAtu;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
           this.tipSbs = tipSbs;
    }


    /**
     * Gets the cadSbs value for this HistoricosPostoAdicional2In.
     * 
     * @return cadSbs
     */
    public java.lang.Integer getCadSbs() {
        return cadSbs;
    }


    /**
     * Sets the cadSbs value for this HistoricosPostoAdicional2In.
     * 
     * @param cadSbs
     */
    public void setCadSbs(java.lang.Integer cadSbs) {
        this.cadSbs = cadSbs;
    }


    /**
     * Gets the codTap value for this HistoricosPostoAdicional2In.
     * 
     * @return codTap
     */
    public java.lang.Integer getCodTap() {
        return codTap;
    }


    /**
     * Sets the codTap value for this HistoricosPostoAdicional2In.
     * 
     * @param codTap
     */
    public void setCodTap(java.lang.Integer codTap) {
        this.codTap = codTap;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosPostoAdicional2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosPostoAdicional2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosPostoAdicional2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosPostoAdicional2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the funEso value for this HistoricosPostoAdicional2In.
     * 
     * @return funEso
     */
    public java.lang.String getFunEso() {
        return funEso;
    }


    /**
     * Sets the funEso value for this HistoricosPostoAdicional2In.
     * 
     * @param funEso
     */
    public void setFunEso(java.lang.String funEso) {
        this.funEso = funEso;
    }


    /**
     * Gets the iniAtu value for this HistoricosPostoAdicional2In.
     * 
     * @return iniAtu
     */
    public java.lang.String getIniAtu() {
        return iniAtu;
    }


    /**
     * Sets the iniAtu value for this HistoricosPostoAdicional2In.
     * 
     * @param iniAtu
     */
    public void setIniAtu(java.lang.String iniAtu) {
        this.iniAtu = iniAtu;
    }


    /**
     * Gets the motAlt value for this HistoricosPostoAdicional2In.
     * 
     * @return motAlt
     */
    public java.lang.Integer getMotAlt() {
        return motAlt;
    }


    /**
     * Sets the motAlt value for this HistoricosPostoAdicional2In.
     * 
     * @param motAlt
     */
    public void setMotAlt(java.lang.Integer motAlt) {
        this.motAlt = motAlt;
    }


    /**
     * Gets the numCad value for this HistoricosPostoAdicional2In.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosPostoAdicional2In.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosPostoAdicional2In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosPostoAdicional2In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the obsHpo value for this HistoricosPostoAdicional2In.
     * 
     * @return obsHpo
     */
    public java.lang.String getObsHpo() {
        return obsHpo;
    }


    /**
     * Sets the obsHpo value for this HistoricosPostoAdicional2In.
     * 
     * @param obsHpo
     */
    public void setObsHpo(java.lang.String obsHpo) {
        this.obsHpo = obsHpo;
    }


    /**
     * Gets the perAdi value for this HistoricosPostoAdicional2In.
     * 
     * @return perAdi
     */
    public java.lang.Integer getPerAdi() {
        return perAdi;
    }


    /**
     * Sets the perAdi value for this HistoricosPostoAdicional2In.
     * 
     * @param perAdi
     */
    public void setPerAdi(java.lang.Integer perAdi) {
        this.perAdi = perAdi;
    }


    /**
     * Gets the posTra value for this HistoricosPostoAdicional2In.
     * 
     * @return posTra
     */
    public java.lang.String getPosTra() {
        return posTra;
    }


    /**
     * Sets the posTra value for this HistoricosPostoAdicional2In.
     * 
     * @param posTra
     */
    public void setPosTra(java.lang.String posTra) {
        this.posTra = posTra;
    }


    /**
     * Gets the seqHis value for this HistoricosPostoAdicional2In.
     * 
     * @return seqHis
     */
    public java.lang.Integer getSeqHis() {
        return seqHis;
    }


    /**
     * Sets the seqHis value for this HistoricosPostoAdicional2In.
     * 
     * @param seqHis
     */
    public void setSeqHis(java.lang.Integer seqHis) {
        this.seqHis = seqHis;
    }


    /**
     * Gets the terAtu value for this HistoricosPostoAdicional2In.
     * 
     * @return terAtu
     */
    public java.lang.String getTerAtu() {
        return terAtu;
    }


    /**
     * Sets the terAtu value for this HistoricosPostoAdicional2In.
     * 
     * @param terAtu
     */
    public void setTerAtu(java.lang.String terAtu) {
        this.terAtu = terAtu;
    }


    /**
     * Gets the tipCol value for this HistoricosPostoAdicional2In.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosPostoAdicional2In.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosPostoAdicional2In.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosPostoAdicional2In.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the tipSbs value for this HistoricosPostoAdicional2In.
     * 
     * @return tipSbs
     */
    public java.lang.Integer getTipSbs() {
        return tipSbs;
    }


    /**
     * Sets the tipSbs value for this HistoricosPostoAdicional2In.
     * 
     * @param tipSbs
     */
    public void setTipSbs(java.lang.Integer tipSbs) {
        this.tipSbs = tipSbs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosPostoAdicional2In)) return false;
        HistoricosPostoAdicional2In other = (HistoricosPostoAdicional2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cadSbs==null && other.getCadSbs()==null) || 
             (this.cadSbs!=null &&
              this.cadSbs.equals(other.getCadSbs()))) &&
            ((this.codTap==null && other.getCodTap()==null) || 
             (this.codTap!=null &&
              this.codTap.equals(other.getCodTap()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.funEso==null && other.getFunEso()==null) || 
             (this.funEso!=null &&
              this.funEso.equals(other.getFunEso()))) &&
            ((this.iniAtu==null && other.getIniAtu()==null) || 
             (this.iniAtu!=null &&
              this.iniAtu.equals(other.getIniAtu()))) &&
            ((this.motAlt==null && other.getMotAlt()==null) || 
             (this.motAlt!=null &&
              this.motAlt.equals(other.getMotAlt()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.obsHpo==null && other.getObsHpo()==null) || 
             (this.obsHpo!=null &&
              this.obsHpo.equals(other.getObsHpo()))) &&
            ((this.perAdi==null && other.getPerAdi()==null) || 
             (this.perAdi!=null &&
              this.perAdi.equals(other.getPerAdi()))) &&
            ((this.posTra==null && other.getPosTra()==null) || 
             (this.posTra!=null &&
              this.posTra.equals(other.getPosTra()))) &&
            ((this.seqHis==null && other.getSeqHis()==null) || 
             (this.seqHis!=null &&
              this.seqHis.equals(other.getSeqHis()))) &&
            ((this.terAtu==null && other.getTerAtu()==null) || 
             (this.terAtu!=null &&
              this.terAtu.equals(other.getTerAtu()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.tipSbs==null && other.getTipSbs()==null) || 
             (this.tipSbs!=null &&
              this.tipSbs.equals(other.getTipSbs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCadSbs() != null) {
            _hashCode += getCadSbs().hashCode();
        }
        if (getCodTap() != null) {
            _hashCode += getCodTap().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getFunEso() != null) {
            _hashCode += getFunEso().hashCode();
        }
        if (getIniAtu() != null) {
            _hashCode += getIniAtu().hashCode();
        }
        if (getMotAlt() != null) {
            _hashCode += getMotAlt().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getObsHpo() != null) {
            _hashCode += getObsHpo().hashCode();
        }
        if (getPerAdi() != null) {
            _hashCode += getPerAdi().hashCode();
        }
        if (getPosTra() != null) {
            _hashCode += getPosTra().hashCode();
        }
        if (getSeqHis() != null) {
            _hashCode += getSeqHis().hashCode();
        }
        if (getTerAtu() != null) {
            _hashCode += getTerAtu().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTipSbs() != null) {
            _hashCode += getTipSbs().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosPostoAdicional2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosPostoAdicional2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cadSbs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cadSbs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTap");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTap"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("funEso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "funEso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsHpo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsHpo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perAdi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perAdi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "posTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqHis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqHis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "terAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipSbs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipSbs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
