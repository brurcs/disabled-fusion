/**
 * TitulosConsultarTitulosAbertosCP2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosConsultarTitulosAbertosCP2In  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String codFor;

    private java.lang.Integer filCtr;

    private java.lang.Integer filNfc;

    private java.lang.Integer filNff;

    private java.lang.Integer filNfv;

    private java.lang.Integer filOcp;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer forNfc;

    private java.lang.Integer forNff;

    private java.lang.Integer numCtr;

    private java.lang.Integer numNfc;

    private java.lang.Integer numNff;

    private java.lang.Integer numNfv;

    private java.lang.Integer numOcp;

    private java.lang.Integer ocpFre;

    private java.lang.Integer ocpNre;

    private java.lang.String retRat;

    private java.lang.Integer seqImo;

    private java.lang.String snfNfc;

    private java.lang.String snfNfv;

    private java.lang.String vctFim;

    private java.lang.String vctIni;

    public TitulosConsultarTitulosAbertosCP2In() {
    }

    public TitulosConsultarTitulosAbertosCP2In(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String codFor,
           java.lang.Integer filCtr,
           java.lang.Integer filNfc,
           java.lang.Integer filNff,
           java.lang.Integer filNfv,
           java.lang.Integer filOcp,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer forNfc,
           java.lang.Integer forNff,
           java.lang.Integer numCtr,
           java.lang.Integer numNfc,
           java.lang.Integer numNff,
           java.lang.Integer numNfv,
           java.lang.Integer numOcp,
           java.lang.Integer ocpFre,
           java.lang.Integer ocpNre,
           java.lang.String retRat,
           java.lang.Integer seqImo,
           java.lang.String snfNfc,
           java.lang.String snfNfv,
           java.lang.String vctFim,
           java.lang.String vctIni) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.filCtr = filCtr;
           this.filNfc = filNfc;
           this.filNff = filNff;
           this.filNfv = filNfv;
           this.filOcp = filOcp;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.forNfc = forNfc;
           this.forNff = forNff;
           this.numCtr = numCtr;
           this.numNfc = numNfc;
           this.numNff = numNff;
           this.numNfv = numNfv;
           this.numOcp = numOcp;
           this.ocpFre = ocpFre;
           this.ocpNre = ocpNre;
           this.retRat = retRat;
           this.seqImo = seqImo;
           this.snfNfc = snfNfc;
           this.snfNfv = snfNfv;
           this.vctFim = vctFim;
           this.vctIni = vctIni;
    }


    /**
     * Gets the codEmp value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return codFor
     */
    public java.lang.String getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.String codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the filCtr value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return filCtr
     */
    public java.lang.Integer getFilCtr() {
        return filCtr;
    }


    /**
     * Sets the filCtr value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param filCtr
     */
    public void setFilCtr(java.lang.Integer filCtr) {
        this.filCtr = filCtr;
    }


    /**
     * Gets the filNfc value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return filNfc
     */
    public java.lang.Integer getFilNfc() {
        return filNfc;
    }


    /**
     * Sets the filNfc value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param filNfc
     */
    public void setFilNfc(java.lang.Integer filNfc) {
        this.filNfc = filNfc;
    }


    /**
     * Gets the filNff value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return filNff
     */
    public java.lang.Integer getFilNff() {
        return filNff;
    }


    /**
     * Sets the filNff value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param filNff
     */
    public void setFilNff(java.lang.Integer filNff) {
        this.filNff = filNff;
    }


    /**
     * Gets the filNfv value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return filNfv
     */
    public java.lang.Integer getFilNfv() {
        return filNfv;
    }


    /**
     * Sets the filNfv value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param filNfv
     */
    public void setFilNfv(java.lang.Integer filNfv) {
        this.filNfv = filNfv;
    }


    /**
     * Gets the filOcp value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return filOcp
     */
    public java.lang.Integer getFilOcp() {
        return filOcp;
    }


    /**
     * Sets the filOcp value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param filOcp
     */
    public void setFilOcp(java.lang.Integer filOcp) {
        this.filOcp = filOcp;
    }


    /**
     * Gets the flowInstanceID value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the forNfc value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return forNfc
     */
    public java.lang.Integer getForNfc() {
        return forNfc;
    }


    /**
     * Sets the forNfc value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param forNfc
     */
    public void setForNfc(java.lang.Integer forNfc) {
        this.forNfc = forNfc;
    }


    /**
     * Gets the forNff value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return forNff
     */
    public java.lang.Integer getForNff() {
        return forNff;
    }


    /**
     * Sets the forNff value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param forNff
     */
    public void setForNff(java.lang.Integer forNff) {
        this.forNff = forNff;
    }


    /**
     * Gets the numCtr value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return numCtr
     */
    public java.lang.Integer getNumCtr() {
        return numCtr;
    }


    /**
     * Sets the numCtr value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param numCtr
     */
    public void setNumCtr(java.lang.Integer numCtr) {
        this.numCtr = numCtr;
    }


    /**
     * Gets the numNfc value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return numNfc
     */
    public java.lang.Integer getNumNfc() {
        return numNfc;
    }


    /**
     * Sets the numNfc value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param numNfc
     */
    public void setNumNfc(java.lang.Integer numNfc) {
        this.numNfc = numNfc;
    }


    /**
     * Gets the numNff value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return numNff
     */
    public java.lang.Integer getNumNff() {
        return numNff;
    }


    /**
     * Sets the numNff value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param numNff
     */
    public void setNumNff(java.lang.Integer numNff) {
        this.numNff = numNff;
    }


    /**
     * Gets the numNfv value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return numNfv
     */
    public java.lang.Integer getNumNfv() {
        return numNfv;
    }


    /**
     * Sets the numNfv value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param numNfv
     */
    public void setNumNfv(java.lang.Integer numNfv) {
        this.numNfv = numNfv;
    }


    /**
     * Gets the numOcp value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return numOcp
     */
    public java.lang.Integer getNumOcp() {
        return numOcp;
    }


    /**
     * Sets the numOcp value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param numOcp
     */
    public void setNumOcp(java.lang.Integer numOcp) {
        this.numOcp = numOcp;
    }


    /**
     * Gets the ocpFre value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return ocpFre
     */
    public java.lang.Integer getOcpFre() {
        return ocpFre;
    }


    /**
     * Sets the ocpFre value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param ocpFre
     */
    public void setOcpFre(java.lang.Integer ocpFre) {
        this.ocpFre = ocpFre;
    }


    /**
     * Gets the ocpNre value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return ocpNre
     */
    public java.lang.Integer getOcpNre() {
        return ocpNre;
    }


    /**
     * Sets the ocpNre value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param ocpNre
     */
    public void setOcpNre(java.lang.Integer ocpNre) {
        this.ocpNre = ocpNre;
    }


    /**
     * Gets the retRat value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return retRat
     */
    public java.lang.String getRetRat() {
        return retRat;
    }


    /**
     * Sets the retRat value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param retRat
     */
    public void setRetRat(java.lang.String retRat) {
        this.retRat = retRat;
    }


    /**
     * Gets the seqImo value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return seqImo
     */
    public java.lang.Integer getSeqImo() {
        return seqImo;
    }


    /**
     * Sets the seqImo value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param seqImo
     */
    public void setSeqImo(java.lang.Integer seqImo) {
        this.seqImo = seqImo;
    }


    /**
     * Gets the snfNfc value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return snfNfc
     */
    public java.lang.String getSnfNfc() {
        return snfNfc;
    }


    /**
     * Sets the snfNfc value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param snfNfc
     */
    public void setSnfNfc(java.lang.String snfNfc) {
        this.snfNfc = snfNfc;
    }


    /**
     * Gets the snfNfv value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return snfNfv
     */
    public java.lang.String getSnfNfv() {
        return snfNfv;
    }


    /**
     * Sets the snfNfv value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param snfNfv
     */
    public void setSnfNfv(java.lang.String snfNfv) {
        this.snfNfv = snfNfv;
    }


    /**
     * Gets the vctFim value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return vctFim
     */
    public java.lang.String getVctFim() {
        return vctFim;
    }


    /**
     * Sets the vctFim value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param vctFim
     */
    public void setVctFim(java.lang.String vctFim) {
        this.vctFim = vctFim;
    }


    /**
     * Gets the vctIni value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @return vctIni
     */
    public java.lang.String getVctIni() {
        return vctIni;
    }


    /**
     * Sets the vctIni value for this TitulosConsultarTitulosAbertosCP2In.
     * 
     * @param vctIni
     */
    public void setVctIni(java.lang.String vctIni) {
        this.vctIni = vctIni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosConsultarTitulosAbertosCP2In)) return false;
        TitulosConsultarTitulosAbertosCP2In other = (TitulosConsultarTitulosAbertosCP2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.filCtr==null && other.getFilCtr()==null) || 
             (this.filCtr!=null &&
              this.filCtr.equals(other.getFilCtr()))) &&
            ((this.filNfc==null && other.getFilNfc()==null) || 
             (this.filNfc!=null &&
              this.filNfc.equals(other.getFilNfc()))) &&
            ((this.filNff==null && other.getFilNff()==null) || 
             (this.filNff!=null &&
              this.filNff.equals(other.getFilNff()))) &&
            ((this.filNfv==null && other.getFilNfv()==null) || 
             (this.filNfv!=null &&
              this.filNfv.equals(other.getFilNfv()))) &&
            ((this.filOcp==null && other.getFilOcp()==null) || 
             (this.filOcp!=null &&
              this.filOcp.equals(other.getFilOcp()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.forNfc==null && other.getForNfc()==null) || 
             (this.forNfc!=null &&
              this.forNfc.equals(other.getForNfc()))) &&
            ((this.forNff==null && other.getForNff()==null) || 
             (this.forNff!=null &&
              this.forNff.equals(other.getForNff()))) &&
            ((this.numCtr==null && other.getNumCtr()==null) || 
             (this.numCtr!=null &&
              this.numCtr.equals(other.getNumCtr()))) &&
            ((this.numNfc==null && other.getNumNfc()==null) || 
             (this.numNfc!=null &&
              this.numNfc.equals(other.getNumNfc()))) &&
            ((this.numNff==null && other.getNumNff()==null) || 
             (this.numNff!=null &&
              this.numNff.equals(other.getNumNff()))) &&
            ((this.numNfv==null && other.getNumNfv()==null) || 
             (this.numNfv!=null &&
              this.numNfv.equals(other.getNumNfv()))) &&
            ((this.numOcp==null && other.getNumOcp()==null) || 
             (this.numOcp!=null &&
              this.numOcp.equals(other.getNumOcp()))) &&
            ((this.ocpFre==null && other.getOcpFre()==null) || 
             (this.ocpFre!=null &&
              this.ocpFre.equals(other.getOcpFre()))) &&
            ((this.ocpNre==null && other.getOcpNre()==null) || 
             (this.ocpNre!=null &&
              this.ocpNre.equals(other.getOcpNre()))) &&
            ((this.retRat==null && other.getRetRat()==null) || 
             (this.retRat!=null &&
              this.retRat.equals(other.getRetRat()))) &&
            ((this.seqImo==null && other.getSeqImo()==null) || 
             (this.seqImo!=null &&
              this.seqImo.equals(other.getSeqImo()))) &&
            ((this.snfNfc==null && other.getSnfNfc()==null) || 
             (this.snfNfc!=null &&
              this.snfNfc.equals(other.getSnfNfc()))) &&
            ((this.snfNfv==null && other.getSnfNfv()==null) || 
             (this.snfNfv!=null &&
              this.snfNfv.equals(other.getSnfNfv()))) &&
            ((this.vctFim==null && other.getVctFim()==null) || 
             (this.vctFim!=null &&
              this.vctFim.equals(other.getVctFim()))) &&
            ((this.vctIni==null && other.getVctIni()==null) || 
             (this.vctIni!=null &&
              this.vctIni.equals(other.getVctIni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getFilCtr() != null) {
            _hashCode += getFilCtr().hashCode();
        }
        if (getFilNfc() != null) {
            _hashCode += getFilNfc().hashCode();
        }
        if (getFilNff() != null) {
            _hashCode += getFilNff().hashCode();
        }
        if (getFilNfv() != null) {
            _hashCode += getFilNfv().hashCode();
        }
        if (getFilOcp() != null) {
            _hashCode += getFilOcp().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getForNfc() != null) {
            _hashCode += getForNfc().hashCode();
        }
        if (getForNff() != null) {
            _hashCode += getForNff().hashCode();
        }
        if (getNumCtr() != null) {
            _hashCode += getNumCtr().hashCode();
        }
        if (getNumNfc() != null) {
            _hashCode += getNumNfc().hashCode();
        }
        if (getNumNff() != null) {
            _hashCode += getNumNff().hashCode();
        }
        if (getNumNfv() != null) {
            _hashCode += getNumNfv().hashCode();
        }
        if (getNumOcp() != null) {
            _hashCode += getNumOcp().hashCode();
        }
        if (getOcpFre() != null) {
            _hashCode += getOcpFre().hashCode();
        }
        if (getOcpNre() != null) {
            _hashCode += getOcpNre().hashCode();
        }
        if (getRetRat() != null) {
            _hashCode += getRetRat().hashCode();
        }
        if (getSeqImo() != null) {
            _hashCode += getSeqImo().hashCode();
        }
        if (getSnfNfc() != null) {
            _hashCode += getSnfNfc().hashCode();
        }
        if (getSnfNfv() != null) {
            _hashCode += getSnfNfv().hashCode();
        }
        if (getVctFim() != null) {
            _hashCode += getVctFim().hashCode();
        }
        if (getVctIni() != null) {
            _hashCode += getVctIni().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosConsultarTitulosAbertosCP2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosAbertosCP2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filCtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filCtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filNff");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filNff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filOcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filOcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forNff");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forNff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numNff");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numNff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numOcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numOcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ocpFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ocpFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ocpNre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ocpNre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqImo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqImo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
