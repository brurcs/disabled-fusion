/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getrubi_Synccom_senior_g5_rh_fp_historicosPortAddress();

    public br.com.senior.services.vetorh.historico.Rubi_Synccom_senior_g5_rh_fp_historicos getrubi_Synccom_senior_g5_rh_fp_historicosPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.vetorh.historico.Rubi_Synccom_senior_g5_rh_fp_historicos getrubi_Synccom_senior_g5_rh_fp_historicosPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
