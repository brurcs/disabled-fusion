/**
 * HistoricosFuncao2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosFuncao2In  implements java.io.Serializable {
    private java.lang.Integer cadSbs;

    private java.lang.String claSal;

    private java.lang.String codCar;

    private java.lang.Integer codEst;

    private java.lang.String datFim;

    private java.lang.String datIni;

    private java.lang.String desObs;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String funEso;

    private java.lang.Integer motAlt;

    private java.lang.String nivSal;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.String numLoc;

    private java.lang.Double perSbs;

    private java.lang.Integer seqAlt;

    private java.lang.Integer tclSbs;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    private java.lang.String tipSbs;

    public HistoricosFuncao2In() {
    }

    public HistoricosFuncao2In(
           java.lang.Integer cadSbs,
           java.lang.String claSal,
           java.lang.String codCar,
           java.lang.Integer codEst,
           java.lang.String datFim,
           java.lang.String datIni,
           java.lang.String desObs,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String funEso,
           java.lang.Integer motAlt,
           java.lang.String nivSal,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.String numLoc,
           java.lang.Double perSbs,
           java.lang.Integer seqAlt,
           java.lang.Integer tclSbs,
           java.lang.Integer tipCol,
           java.lang.String tipOpe,
           java.lang.String tipSbs) {
           this.cadSbs = cadSbs;
           this.claSal = claSal;
           this.codCar = codCar;
           this.codEst = codEst;
           this.datFim = datFim;
           this.datIni = datIni;
           this.desObs = desObs;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.funEso = funEso;
           this.motAlt = motAlt;
           this.nivSal = nivSal;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.numLoc = numLoc;
           this.perSbs = perSbs;
           this.seqAlt = seqAlt;
           this.tclSbs = tclSbs;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
           this.tipSbs = tipSbs;
    }


    /**
     * Gets the cadSbs value for this HistoricosFuncao2In.
     * 
     * @return cadSbs
     */
    public java.lang.Integer getCadSbs() {
        return cadSbs;
    }


    /**
     * Sets the cadSbs value for this HistoricosFuncao2In.
     * 
     * @param cadSbs
     */
    public void setCadSbs(java.lang.Integer cadSbs) {
        this.cadSbs = cadSbs;
    }


    /**
     * Gets the claSal value for this HistoricosFuncao2In.
     * 
     * @return claSal
     */
    public java.lang.String getClaSal() {
        return claSal;
    }


    /**
     * Sets the claSal value for this HistoricosFuncao2In.
     * 
     * @param claSal
     */
    public void setClaSal(java.lang.String claSal) {
        this.claSal = claSal;
    }


    /**
     * Gets the codCar value for this HistoricosFuncao2In.
     * 
     * @return codCar
     */
    public java.lang.String getCodCar() {
        return codCar;
    }


    /**
     * Sets the codCar value for this HistoricosFuncao2In.
     * 
     * @param codCar
     */
    public void setCodCar(java.lang.String codCar) {
        this.codCar = codCar;
    }


    /**
     * Gets the codEst value for this HistoricosFuncao2In.
     * 
     * @return codEst
     */
    public java.lang.Integer getCodEst() {
        return codEst;
    }


    /**
     * Sets the codEst value for this HistoricosFuncao2In.
     * 
     * @param codEst
     */
    public void setCodEst(java.lang.Integer codEst) {
        this.codEst = codEst;
    }


    /**
     * Gets the datFim value for this HistoricosFuncao2In.
     * 
     * @return datFim
     */
    public java.lang.String getDatFim() {
        return datFim;
    }


    /**
     * Sets the datFim value for this HistoricosFuncao2In.
     * 
     * @param datFim
     */
    public void setDatFim(java.lang.String datFim) {
        this.datFim = datFim;
    }


    /**
     * Gets the datIni value for this HistoricosFuncao2In.
     * 
     * @return datIni
     */
    public java.lang.String getDatIni() {
        return datIni;
    }


    /**
     * Sets the datIni value for this HistoricosFuncao2In.
     * 
     * @param datIni
     */
    public void setDatIni(java.lang.String datIni) {
        this.datIni = datIni;
    }


    /**
     * Gets the desObs value for this HistoricosFuncao2In.
     * 
     * @return desObs
     */
    public java.lang.String getDesObs() {
        return desObs;
    }


    /**
     * Sets the desObs value for this HistoricosFuncao2In.
     * 
     * @param desObs
     */
    public void setDesObs(java.lang.String desObs) {
        this.desObs = desObs;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosFuncao2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosFuncao2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosFuncao2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosFuncao2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the funEso value for this HistoricosFuncao2In.
     * 
     * @return funEso
     */
    public java.lang.String getFunEso() {
        return funEso;
    }


    /**
     * Sets the funEso value for this HistoricosFuncao2In.
     * 
     * @param funEso
     */
    public void setFunEso(java.lang.String funEso) {
        this.funEso = funEso;
    }


    /**
     * Gets the motAlt value for this HistoricosFuncao2In.
     * 
     * @return motAlt
     */
    public java.lang.Integer getMotAlt() {
        return motAlt;
    }


    /**
     * Sets the motAlt value for this HistoricosFuncao2In.
     * 
     * @param motAlt
     */
    public void setMotAlt(java.lang.Integer motAlt) {
        this.motAlt = motAlt;
    }


    /**
     * Gets the nivSal value for this HistoricosFuncao2In.
     * 
     * @return nivSal
     */
    public java.lang.String getNivSal() {
        return nivSal;
    }


    /**
     * Sets the nivSal value for this HistoricosFuncao2In.
     * 
     * @param nivSal
     */
    public void setNivSal(java.lang.String nivSal) {
        this.nivSal = nivSal;
    }


    /**
     * Gets the numCad value for this HistoricosFuncao2In.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosFuncao2In.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosFuncao2In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosFuncao2In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the numLoc value for this HistoricosFuncao2In.
     * 
     * @return numLoc
     */
    public java.lang.String getNumLoc() {
        return numLoc;
    }


    /**
     * Sets the numLoc value for this HistoricosFuncao2In.
     * 
     * @param numLoc
     */
    public void setNumLoc(java.lang.String numLoc) {
        this.numLoc = numLoc;
    }


    /**
     * Gets the perSbs value for this HistoricosFuncao2In.
     * 
     * @return perSbs
     */
    public java.lang.Double getPerSbs() {
        return perSbs;
    }


    /**
     * Sets the perSbs value for this HistoricosFuncao2In.
     * 
     * @param perSbs
     */
    public void setPerSbs(java.lang.Double perSbs) {
        this.perSbs = perSbs;
    }


    /**
     * Gets the seqAlt value for this HistoricosFuncao2In.
     * 
     * @return seqAlt
     */
    public java.lang.Integer getSeqAlt() {
        return seqAlt;
    }


    /**
     * Sets the seqAlt value for this HistoricosFuncao2In.
     * 
     * @param seqAlt
     */
    public void setSeqAlt(java.lang.Integer seqAlt) {
        this.seqAlt = seqAlt;
    }


    /**
     * Gets the tclSbs value for this HistoricosFuncao2In.
     * 
     * @return tclSbs
     */
    public java.lang.Integer getTclSbs() {
        return tclSbs;
    }


    /**
     * Sets the tclSbs value for this HistoricosFuncao2In.
     * 
     * @param tclSbs
     */
    public void setTclSbs(java.lang.Integer tclSbs) {
        this.tclSbs = tclSbs;
    }


    /**
     * Gets the tipCol value for this HistoricosFuncao2In.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosFuncao2In.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosFuncao2In.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosFuncao2In.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the tipSbs value for this HistoricosFuncao2In.
     * 
     * @return tipSbs
     */
    public java.lang.String getTipSbs() {
        return tipSbs;
    }


    /**
     * Sets the tipSbs value for this HistoricosFuncao2In.
     * 
     * @param tipSbs
     */
    public void setTipSbs(java.lang.String tipSbs) {
        this.tipSbs = tipSbs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosFuncao2In)) return false;
        HistoricosFuncao2In other = (HistoricosFuncao2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cadSbs==null && other.getCadSbs()==null) || 
             (this.cadSbs!=null &&
              this.cadSbs.equals(other.getCadSbs()))) &&
            ((this.claSal==null && other.getClaSal()==null) || 
             (this.claSal!=null &&
              this.claSal.equals(other.getClaSal()))) &&
            ((this.codCar==null && other.getCodCar()==null) || 
             (this.codCar!=null &&
              this.codCar.equals(other.getCodCar()))) &&
            ((this.codEst==null && other.getCodEst()==null) || 
             (this.codEst!=null &&
              this.codEst.equals(other.getCodEst()))) &&
            ((this.datFim==null && other.getDatFim()==null) || 
             (this.datFim!=null &&
              this.datFim.equals(other.getDatFim()))) &&
            ((this.datIni==null && other.getDatIni()==null) || 
             (this.datIni!=null &&
              this.datIni.equals(other.getDatIni()))) &&
            ((this.desObs==null && other.getDesObs()==null) || 
             (this.desObs!=null &&
              this.desObs.equals(other.getDesObs()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.funEso==null && other.getFunEso()==null) || 
             (this.funEso!=null &&
              this.funEso.equals(other.getFunEso()))) &&
            ((this.motAlt==null && other.getMotAlt()==null) || 
             (this.motAlt!=null &&
              this.motAlt.equals(other.getMotAlt()))) &&
            ((this.nivSal==null && other.getNivSal()==null) || 
             (this.nivSal!=null &&
              this.nivSal.equals(other.getNivSal()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.numLoc==null && other.getNumLoc()==null) || 
             (this.numLoc!=null &&
              this.numLoc.equals(other.getNumLoc()))) &&
            ((this.perSbs==null && other.getPerSbs()==null) || 
             (this.perSbs!=null &&
              this.perSbs.equals(other.getPerSbs()))) &&
            ((this.seqAlt==null && other.getSeqAlt()==null) || 
             (this.seqAlt!=null &&
              this.seqAlt.equals(other.getSeqAlt()))) &&
            ((this.tclSbs==null && other.getTclSbs()==null) || 
             (this.tclSbs!=null &&
              this.tclSbs.equals(other.getTclSbs()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.tipSbs==null && other.getTipSbs()==null) || 
             (this.tipSbs!=null &&
              this.tipSbs.equals(other.getTipSbs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCadSbs() != null) {
            _hashCode += getCadSbs().hashCode();
        }
        if (getClaSal() != null) {
            _hashCode += getClaSal().hashCode();
        }
        if (getCodCar() != null) {
            _hashCode += getCodCar().hashCode();
        }
        if (getCodEst() != null) {
            _hashCode += getCodEst().hashCode();
        }
        if (getDatFim() != null) {
            _hashCode += getDatFim().hashCode();
        }
        if (getDatIni() != null) {
            _hashCode += getDatIni().hashCode();
        }
        if (getDesObs() != null) {
            _hashCode += getDesObs().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getFunEso() != null) {
            _hashCode += getFunEso().hashCode();
        }
        if (getMotAlt() != null) {
            _hashCode += getMotAlt().hashCode();
        }
        if (getNivSal() != null) {
            _hashCode += getNivSal().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getNumLoc() != null) {
            _hashCode += getNumLoc().hashCode();
        }
        if (getPerSbs() != null) {
            _hashCode += getPerSbs().hashCode();
        }
        if (getSeqAlt() != null) {
            _hashCode += getSeqAlt().hashCode();
        }
        if (getTclSbs() != null) {
            _hashCode += getTclSbs().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTipSbs() != null) {
            _hashCode += getTipSbs().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosFuncao2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosFuncao2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cadSbs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cadSbs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("funEso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "funEso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nivSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nivSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numLoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numLoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perSbs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perSbs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tclSbs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tclSbs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipSbs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipSbs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
