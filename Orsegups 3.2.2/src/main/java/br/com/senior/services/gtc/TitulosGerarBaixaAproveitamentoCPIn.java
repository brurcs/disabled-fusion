/**
 * TitulosGerarBaixaAproveitamentoCPIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosGerarBaixaAproveitamentoCPIn  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String datBai;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private TitulosGerarBaixaAproveitamentoCPInGridTitulosBAI[] gridTitulosBAI;

    private TitulosGerarBaixaAproveitamentoCPInGridTitulosCRE[] gridTitulosCRE;

    private java.lang.String tnsCre;

    private java.lang.String tnsDup;

    public TitulosGerarBaixaAproveitamentoCPIn() {
    }

    public TitulosGerarBaixaAproveitamentoCPIn(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String datBai,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           TitulosGerarBaixaAproveitamentoCPInGridTitulosBAI[] gridTitulosBAI,
           TitulosGerarBaixaAproveitamentoCPInGridTitulosCRE[] gridTitulosCRE,
           java.lang.String tnsCre,
           java.lang.String tnsDup) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.datBai = datBai;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.gridTitulosBAI = gridTitulosBAI;
           this.gridTitulosCRE = gridTitulosCRE;
           this.tnsCre = tnsCre;
           this.tnsDup = tnsDup;
    }


    /**
     * Gets the codEmp value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the datBai value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return datBai
     */
    public java.lang.String getDatBai() {
        return datBai;
    }


    /**
     * Sets the datBai value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param datBai
     */
    public void setDatBai(java.lang.String datBai) {
        this.datBai = datBai;
    }


    /**
     * Gets the flowInstanceID value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the gridTitulosBAI value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return gridTitulosBAI
     */
    public TitulosGerarBaixaAproveitamentoCPInGridTitulosBAI[] getGridTitulosBAI() {
        return gridTitulosBAI;
    }


    /**
     * Sets the gridTitulosBAI value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param gridTitulosBAI
     */
    public void setGridTitulosBAI(TitulosGerarBaixaAproveitamentoCPInGridTitulosBAI[] gridTitulosBAI) {
        this.gridTitulosBAI = gridTitulosBAI;
    }

    public TitulosGerarBaixaAproveitamentoCPInGridTitulosBAI getGridTitulosBAI(int i) {
        return this.gridTitulosBAI[i];
    }

    public void setGridTitulosBAI(int i, TitulosGerarBaixaAproveitamentoCPInGridTitulosBAI _value) {
        this.gridTitulosBAI[i] = _value;
    }


    /**
     * Gets the gridTitulosCRE value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return gridTitulosCRE
     */
    public TitulosGerarBaixaAproveitamentoCPInGridTitulosCRE[] getGridTitulosCRE() {
        return gridTitulosCRE;
    }


    /**
     * Sets the gridTitulosCRE value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param gridTitulosCRE
     */
    public void setGridTitulosCRE(TitulosGerarBaixaAproveitamentoCPInGridTitulosCRE[] gridTitulosCRE) {
        this.gridTitulosCRE = gridTitulosCRE;
    }

    public TitulosGerarBaixaAproveitamentoCPInGridTitulosCRE getGridTitulosCRE(int i) {
        return this.gridTitulosCRE[i];
    }

    public void setGridTitulosCRE(int i, TitulosGerarBaixaAproveitamentoCPInGridTitulosCRE _value) {
        this.gridTitulosCRE[i] = _value;
    }


    /**
     * Gets the tnsCre value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return tnsCre
     */
    public java.lang.String getTnsCre() {
        return tnsCre;
    }


    /**
     * Sets the tnsCre value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param tnsCre
     */
    public void setTnsCre(java.lang.String tnsCre) {
        this.tnsCre = tnsCre;
    }


    /**
     * Gets the tnsDup value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @return tnsDup
     */
    public java.lang.String getTnsDup() {
        return tnsDup;
    }


    /**
     * Sets the tnsDup value for this TitulosGerarBaixaAproveitamentoCPIn.
     * 
     * @param tnsDup
     */
    public void setTnsDup(java.lang.String tnsDup) {
        this.tnsDup = tnsDup;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosGerarBaixaAproveitamentoCPIn)) return false;
        TitulosGerarBaixaAproveitamentoCPIn other = (TitulosGerarBaixaAproveitamentoCPIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.datBai==null && other.getDatBai()==null) || 
             (this.datBai!=null &&
              this.datBai.equals(other.getDatBai()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.gridTitulosBAI==null && other.getGridTitulosBAI()==null) || 
             (this.gridTitulosBAI!=null &&
              java.util.Arrays.equals(this.gridTitulosBAI, other.getGridTitulosBAI()))) &&
            ((this.gridTitulosCRE==null && other.getGridTitulosCRE()==null) || 
             (this.gridTitulosCRE!=null &&
              java.util.Arrays.equals(this.gridTitulosCRE, other.getGridTitulosCRE()))) &&
            ((this.tnsCre==null && other.getTnsCre()==null) || 
             (this.tnsCre!=null &&
              this.tnsCre.equals(other.getTnsCre()))) &&
            ((this.tnsDup==null && other.getTnsDup()==null) || 
             (this.tnsDup!=null &&
              this.tnsDup.equals(other.getTnsDup())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getDatBai() != null) {
            _hashCode += getDatBai().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getGridTitulosBAI() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGridTitulosBAI());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGridTitulosBAI(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGridTitulosCRE() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGridTitulosCRE());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGridTitulosCRE(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTnsCre() != null) {
            _hashCode += getTnsCre().hashCode();
        }
        if (getTnsDup() != null) {
            _hashCode += getTnsDup().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosGerarBaixaAproveitamentoCPIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGerarBaixaAproveitamentoCPIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datBai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datBai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gridTitulosBAI");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gridTitulosBAI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGerarBaixaAproveitamentoCPInGridTitulosBAI"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gridTitulosCRE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gridTitulosCRE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGerarBaixaAproveitamentoCPInGridTitulosCRE"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDup");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
