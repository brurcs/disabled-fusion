/**
 * ClientesCadastrarContatoClienteIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesCadastrarContatoClienteIn  implements java.io.Serializable {
    private java.lang.String carCto;

    private java.lang.Integer codCli;

    private java.lang.String datNas;

    private java.lang.String faxCto;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String fonCt2;

    private java.lang.String fonCt3;

    private java.lang.String fonCto;

    private java.lang.String hobCon;

    private java.lang.String intNet;

    private java.lang.String nomCto;

    private java.lang.Integer ramCt2;

    private java.lang.Integer ramCt3;

    private java.lang.Integer ramCto;

    private java.lang.Integer seqCto;

    private java.lang.String setCto;

    private java.lang.String sitCto;

    private java.lang.String timCon;

    public ClientesCadastrarContatoClienteIn() {
    }

    public ClientesCadastrarContatoClienteIn(
           java.lang.String carCto,
           java.lang.Integer codCli,
           java.lang.String datNas,
           java.lang.String faxCto,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String fonCt2,
           java.lang.String fonCt3,
           java.lang.String fonCto,
           java.lang.String hobCon,
           java.lang.String intNet,
           java.lang.String nomCto,
           java.lang.Integer ramCt2,
           java.lang.Integer ramCt3,
           java.lang.Integer ramCto,
           java.lang.Integer seqCto,
           java.lang.String setCto,
           java.lang.String sitCto,
           java.lang.String timCon) {
           this.carCto = carCto;
           this.codCli = codCli;
           this.datNas = datNas;
           this.faxCto = faxCto;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.fonCt2 = fonCt2;
           this.fonCt3 = fonCt3;
           this.fonCto = fonCto;
           this.hobCon = hobCon;
           this.intNet = intNet;
           this.nomCto = nomCto;
           this.ramCt2 = ramCt2;
           this.ramCt3 = ramCt3;
           this.ramCto = ramCto;
           this.seqCto = seqCto;
           this.setCto = setCto;
           this.sitCto = sitCto;
           this.timCon = timCon;
    }


    /**
     * Gets the carCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return carCto
     */
    public java.lang.String getCarCto() {
        return carCto;
    }


    /**
     * Sets the carCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param carCto
     */
    public void setCarCto(java.lang.String carCto) {
        this.carCto = carCto;
    }


    /**
     * Gets the codCli value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the datNas value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return datNas
     */
    public java.lang.String getDatNas() {
        return datNas;
    }


    /**
     * Sets the datNas value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param datNas
     */
    public void setDatNas(java.lang.String datNas) {
        this.datNas = datNas;
    }


    /**
     * Gets the faxCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return faxCto
     */
    public java.lang.String getFaxCto() {
        return faxCto;
    }


    /**
     * Sets the faxCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param faxCto
     */
    public void setFaxCto(java.lang.String faxCto) {
        this.faxCto = faxCto;
    }


    /**
     * Gets the flowInstanceID value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the fonCt2 value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return fonCt2
     */
    public java.lang.String getFonCt2() {
        return fonCt2;
    }


    /**
     * Sets the fonCt2 value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param fonCt2
     */
    public void setFonCt2(java.lang.String fonCt2) {
        this.fonCt2 = fonCt2;
    }


    /**
     * Gets the fonCt3 value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return fonCt3
     */
    public java.lang.String getFonCt3() {
        return fonCt3;
    }


    /**
     * Sets the fonCt3 value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param fonCt3
     */
    public void setFonCt3(java.lang.String fonCt3) {
        this.fonCt3 = fonCt3;
    }


    /**
     * Gets the fonCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return fonCto
     */
    public java.lang.String getFonCto() {
        return fonCto;
    }


    /**
     * Sets the fonCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param fonCto
     */
    public void setFonCto(java.lang.String fonCto) {
        this.fonCto = fonCto;
    }


    /**
     * Gets the hobCon value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return hobCon
     */
    public java.lang.String getHobCon() {
        return hobCon;
    }


    /**
     * Sets the hobCon value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param hobCon
     */
    public void setHobCon(java.lang.String hobCon) {
        this.hobCon = hobCon;
    }


    /**
     * Gets the intNet value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the nomCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return nomCto
     */
    public java.lang.String getNomCto() {
        return nomCto;
    }


    /**
     * Sets the nomCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param nomCto
     */
    public void setNomCto(java.lang.String nomCto) {
        this.nomCto = nomCto;
    }


    /**
     * Gets the ramCt2 value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return ramCt2
     */
    public java.lang.Integer getRamCt2() {
        return ramCt2;
    }


    /**
     * Sets the ramCt2 value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param ramCt2
     */
    public void setRamCt2(java.lang.Integer ramCt2) {
        this.ramCt2 = ramCt2;
    }


    /**
     * Gets the ramCt3 value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return ramCt3
     */
    public java.lang.Integer getRamCt3() {
        return ramCt3;
    }


    /**
     * Sets the ramCt3 value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param ramCt3
     */
    public void setRamCt3(java.lang.Integer ramCt3) {
        this.ramCt3 = ramCt3;
    }


    /**
     * Gets the ramCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return ramCto
     */
    public java.lang.Integer getRamCto() {
        return ramCto;
    }


    /**
     * Sets the ramCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param ramCto
     */
    public void setRamCto(java.lang.Integer ramCto) {
        this.ramCto = ramCto;
    }


    /**
     * Gets the seqCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return seqCto
     */
    public java.lang.Integer getSeqCto() {
        return seqCto;
    }


    /**
     * Sets the seqCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param seqCto
     */
    public void setSeqCto(java.lang.Integer seqCto) {
        this.seqCto = seqCto;
    }


    /**
     * Gets the setCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return setCto
     */
    public java.lang.String getSetCto() {
        return setCto;
    }


    /**
     * Sets the setCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param setCto
     */
    public void setSetCto(java.lang.String setCto) {
        this.setCto = setCto;
    }


    /**
     * Gets the sitCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return sitCto
     */
    public java.lang.String getSitCto() {
        return sitCto;
    }


    /**
     * Sets the sitCto value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param sitCto
     */
    public void setSitCto(java.lang.String sitCto) {
        this.sitCto = sitCto;
    }


    /**
     * Gets the timCon value for this ClientesCadastrarContatoClienteIn.
     * 
     * @return timCon
     */
    public java.lang.String getTimCon() {
        return timCon;
    }


    /**
     * Sets the timCon value for this ClientesCadastrarContatoClienteIn.
     * 
     * @param timCon
     */
    public void setTimCon(java.lang.String timCon) {
        this.timCon = timCon;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesCadastrarContatoClienteIn)) return false;
        ClientesCadastrarContatoClienteIn other = (ClientesCadastrarContatoClienteIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.carCto==null && other.getCarCto()==null) || 
             (this.carCto!=null &&
              this.carCto.equals(other.getCarCto()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.datNas==null && other.getDatNas()==null) || 
             (this.datNas!=null &&
              this.datNas.equals(other.getDatNas()))) &&
            ((this.faxCto==null && other.getFaxCto()==null) || 
             (this.faxCto!=null &&
              this.faxCto.equals(other.getFaxCto()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.fonCt2==null && other.getFonCt2()==null) || 
             (this.fonCt2!=null &&
              this.fonCt2.equals(other.getFonCt2()))) &&
            ((this.fonCt3==null && other.getFonCt3()==null) || 
             (this.fonCt3!=null &&
              this.fonCt3.equals(other.getFonCt3()))) &&
            ((this.fonCto==null && other.getFonCto()==null) || 
             (this.fonCto!=null &&
              this.fonCto.equals(other.getFonCto()))) &&
            ((this.hobCon==null && other.getHobCon()==null) || 
             (this.hobCon!=null &&
              this.hobCon.equals(other.getHobCon()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.nomCto==null && other.getNomCto()==null) || 
             (this.nomCto!=null &&
              this.nomCto.equals(other.getNomCto()))) &&
            ((this.ramCt2==null && other.getRamCt2()==null) || 
             (this.ramCt2!=null &&
              this.ramCt2.equals(other.getRamCt2()))) &&
            ((this.ramCt3==null && other.getRamCt3()==null) || 
             (this.ramCt3!=null &&
              this.ramCt3.equals(other.getRamCt3()))) &&
            ((this.ramCto==null && other.getRamCto()==null) || 
             (this.ramCto!=null &&
              this.ramCto.equals(other.getRamCto()))) &&
            ((this.seqCto==null && other.getSeqCto()==null) || 
             (this.seqCto!=null &&
              this.seqCto.equals(other.getSeqCto()))) &&
            ((this.setCto==null && other.getSetCto()==null) || 
             (this.setCto!=null &&
              this.setCto.equals(other.getSetCto()))) &&
            ((this.sitCto==null && other.getSitCto()==null) || 
             (this.sitCto!=null &&
              this.sitCto.equals(other.getSitCto()))) &&
            ((this.timCon==null && other.getTimCon()==null) || 
             (this.timCon!=null &&
              this.timCon.equals(other.getTimCon())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCarCto() != null) {
            _hashCode += getCarCto().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getDatNas() != null) {
            _hashCode += getDatNas().hashCode();
        }
        if (getFaxCto() != null) {
            _hashCode += getFaxCto().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getFonCt2() != null) {
            _hashCode += getFonCt2().hashCode();
        }
        if (getFonCt3() != null) {
            _hashCode += getFonCt3().hashCode();
        }
        if (getFonCto() != null) {
            _hashCode += getFonCto().hashCode();
        }
        if (getHobCon() != null) {
            _hashCode += getHobCon().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getNomCto() != null) {
            _hashCode += getNomCto().hashCode();
        }
        if (getRamCt2() != null) {
            _hashCode += getRamCt2().hashCode();
        }
        if (getRamCt3() != null) {
            _hashCode += getRamCt3().hashCode();
        }
        if (getRamCto() != null) {
            _hashCode += getRamCto().hashCode();
        }
        if (getSeqCto() != null) {
            _hashCode += getSeqCto().hashCode();
        }
        if (getSetCto() != null) {
            _hashCode += getSetCto().hashCode();
        }
        if (getSitCto() != null) {
            _hashCode += getSitCto().hashCode();
        }
        if (getTimCon() != null) {
            _hashCode += getTimCon().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesCadastrarContatoClienteIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesCadastrarContatoClienteIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCt2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCt2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCt3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCt3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hobCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hobCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ramCt2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ramCt2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ramCt3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ramCt3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ramCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ramCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("setCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "setCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
