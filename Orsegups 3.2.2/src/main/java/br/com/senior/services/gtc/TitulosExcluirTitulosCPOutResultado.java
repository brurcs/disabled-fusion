/**
 * TitulosExcluirTitulosCPOutResultado.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosExcluirTitulosCPOutResultado  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.String codFil;

    private java.lang.String codFor;

    private java.lang.String codTpt;

    private java.lang.String numTit;

    private java.lang.String resultado;

    public TitulosExcluirTitulosCPOutResultado() {
    }

    public TitulosExcluirTitulosCPOutResultado(
           java.lang.Integer codEmp,
           java.lang.String codFil,
           java.lang.String codFor,
           java.lang.String codTpt,
           java.lang.String numTit,
           java.lang.String resultado) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codTpt = codTpt;
           this.numTit = numTit;
           this.resultado = resultado;
    }


    /**
     * Gets the codEmp value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @return codFil
     */
    public java.lang.String getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.String codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @return codFor
     */
    public java.lang.String getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.String codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codTpt value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the numTit value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the resultado value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @return resultado
     */
    public java.lang.String getResultado() {
        return resultado;
    }


    /**
     * Sets the resultado value for this TitulosExcluirTitulosCPOutResultado.
     * 
     * @param resultado
     */
    public void setResultado(java.lang.String resultado) {
        this.resultado = resultado;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosExcluirTitulosCPOutResultado)) return false;
        TitulosExcluirTitulosCPOutResultado other = (TitulosExcluirTitulosCPOutResultado) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.resultado==null && other.getResultado()==null) || 
             (this.resultado!=null &&
              this.resultado.equals(other.getResultado())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getResultado() != null) {
            _hashCode += getResultado().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosExcluirTitulosCPOutResultado.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosExcluirTitulosCPOutResultado"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
