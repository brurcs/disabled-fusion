/**
 * ClientesCadastrarCartaoClienteIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesCadastrarCartaoClienteIn  implements java.io.Serializable {
    private java.lang.String codBan;

    private java.lang.Integer codCli;

    private java.lang.String codSeg;

    private java.lang.String datVal;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String indAct;

    private java.lang.String motAlt;

    private java.lang.String nomBan;

    private java.lang.String nomcar;

    private java.lang.String numCar;

    public ClientesCadastrarCartaoClienteIn() {
    }

    public ClientesCadastrarCartaoClienteIn(
           java.lang.String codBan,
           java.lang.Integer codCli,
           java.lang.String codSeg,
           java.lang.String datVal,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String indAct,
           java.lang.String motAlt,
           java.lang.String nomBan,
           java.lang.String nomcar,
           java.lang.String numCar) {
           this.codBan = codBan;
           this.codCli = codCli;
           this.codSeg = codSeg;
           this.datVal = datVal;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.indAct = indAct;
           this.motAlt = motAlt;
           this.nomBan = nomBan;
           this.nomcar = nomcar;
           this.numCar = numCar;
    }


    /**
     * Gets the codBan value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codCli value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codSeg value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return codSeg
     */
    public java.lang.String getCodSeg() {
        return codSeg;
    }


    /**
     * Sets the codSeg value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param codSeg
     */
    public void setCodSeg(java.lang.String codSeg) {
        this.codSeg = codSeg;
    }


    /**
     * Gets the datVal value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return datVal
     */
    public java.lang.String getDatVal() {
        return datVal;
    }


    /**
     * Sets the datVal value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param datVal
     */
    public void setDatVal(java.lang.String datVal) {
        this.datVal = datVal;
    }


    /**
     * Gets the flowInstanceID value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the indAct value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return indAct
     */
    public java.lang.String getIndAct() {
        return indAct;
    }


    /**
     * Sets the indAct value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param indAct
     */
    public void setIndAct(java.lang.String indAct) {
        this.indAct = indAct;
    }


    /**
     * Gets the motAlt value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return motAlt
     */
    public java.lang.String getMotAlt() {
        return motAlt;
    }


    /**
     * Sets the motAlt value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param motAlt
     */
    public void setMotAlt(java.lang.String motAlt) {
        this.motAlt = motAlt;
    }


    /**
     * Gets the nomBan value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return nomBan
     */
    public java.lang.String getNomBan() {
        return nomBan;
    }


    /**
     * Sets the nomBan value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param nomBan
     */
    public void setNomBan(java.lang.String nomBan) {
        this.nomBan = nomBan;
    }


    /**
     * Gets the nomcar value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return nomcar
     */
    public java.lang.String getNomcar() {
        return nomcar;
    }


    /**
     * Sets the nomcar value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param nomcar
     */
    public void setNomcar(java.lang.String nomcar) {
        this.nomcar = nomcar;
    }


    /**
     * Gets the numCar value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @return numCar
     */
    public java.lang.String getNumCar() {
        return numCar;
    }


    /**
     * Sets the numCar value for this ClientesCadastrarCartaoClienteIn.
     * 
     * @param numCar
     */
    public void setNumCar(java.lang.String numCar) {
        this.numCar = numCar;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesCadastrarCartaoClienteIn)) return false;
        ClientesCadastrarCartaoClienteIn other = (ClientesCadastrarCartaoClienteIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codSeg==null && other.getCodSeg()==null) || 
             (this.codSeg!=null &&
              this.codSeg.equals(other.getCodSeg()))) &&
            ((this.datVal==null && other.getDatVal()==null) || 
             (this.datVal!=null &&
              this.datVal.equals(other.getDatVal()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.indAct==null && other.getIndAct()==null) || 
             (this.indAct!=null &&
              this.indAct.equals(other.getIndAct()))) &&
            ((this.motAlt==null && other.getMotAlt()==null) || 
             (this.motAlt!=null &&
              this.motAlt.equals(other.getMotAlt()))) &&
            ((this.nomBan==null && other.getNomBan()==null) || 
             (this.nomBan!=null &&
              this.nomBan.equals(other.getNomBan()))) &&
            ((this.nomcar==null && other.getNomcar()==null) || 
             (this.nomcar!=null &&
              this.nomcar.equals(other.getNomcar()))) &&
            ((this.numCar==null && other.getNumCar()==null) || 
             (this.numCar!=null &&
              this.numCar.equals(other.getNumCar())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodSeg() != null) {
            _hashCode += getCodSeg().hashCode();
        }
        if (getDatVal() != null) {
            _hashCode += getDatVal().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIndAct() != null) {
            _hashCode += getIndAct().hashCode();
        }
        if (getMotAlt() != null) {
            _hashCode += getMotAlt().hashCode();
        }
        if (getNomBan() != null) {
            _hashCode += getNomBan().hashCode();
        }
        if (getNomcar() != null) {
            _hashCode += getNomcar().hashCode();
        }
        if (getNumCar() != null) {
            _hashCode += getNumCar().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesCadastrarCartaoClienteIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesCadastrarCartaoClienteIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datVal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datVal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indAct");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indAct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomcar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomcar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
