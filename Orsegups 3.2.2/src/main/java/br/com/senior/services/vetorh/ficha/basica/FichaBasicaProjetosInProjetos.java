/**
 * FichaBasicaProjetosInProjetos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaProjetosInProjetos  implements java.io.Serializable {
    private java.lang.String conPrj;

    private java.lang.String datHis;

    private java.lang.String tipOpe;

    public FichaBasicaProjetosInProjetos() {
    }

    public FichaBasicaProjetosInProjetos(
           java.lang.String conPrj,
           java.lang.String datHis,
           java.lang.String tipOpe) {
           this.conPrj = conPrj;
           this.datHis = datHis;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the conPrj value for this FichaBasicaProjetosInProjetos.
     * 
     * @return conPrj
     */
    public java.lang.String getConPrj() {
        return conPrj;
    }


    /**
     * Sets the conPrj value for this FichaBasicaProjetosInProjetos.
     * 
     * @param conPrj
     */
    public void setConPrj(java.lang.String conPrj) {
        this.conPrj = conPrj;
    }


    /**
     * Gets the datHis value for this FichaBasicaProjetosInProjetos.
     * 
     * @return datHis
     */
    public java.lang.String getDatHis() {
        return datHis;
    }


    /**
     * Sets the datHis value for this FichaBasicaProjetosInProjetos.
     * 
     * @param datHis
     */
    public void setDatHis(java.lang.String datHis) {
        this.datHis = datHis;
    }


    /**
     * Gets the tipOpe value for this FichaBasicaProjetosInProjetos.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaBasicaProjetosInProjetos.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaProjetosInProjetos)) return false;
        FichaBasicaProjetosInProjetos other = (FichaBasicaProjetosInProjetos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.conPrj==null && other.getConPrj()==null) || 
             (this.conPrj!=null &&
              this.conPrj.equals(other.getConPrj()))) &&
            ((this.datHis==null && other.getDatHis()==null) || 
             (this.datHis!=null &&
              this.datHis.equals(other.getDatHis()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConPrj() != null) {
            _hashCode += getConPrj().hashCode();
        }
        if (getDatHis() != null) {
            _hashCode += getDatHis().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaProjetosInProjetos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaProjetosInProjetos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datHis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datHis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
