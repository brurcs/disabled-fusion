/**
 * RelatoriosRelatoriosIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class RelatoriosRelatsIn  implements java.io.Serializable {
    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String prAnexoBool;

    private java.lang.String prAssunto;

    private java.lang.String prCC;

    private java.lang.String prCCo;

    private java.lang.String prDest;

    private java.lang.String prDir;

    private java.lang.String prEntrada;

    private java.lang.String prEntranceIsXML;

    private java.lang.String prExecFmt;

    private java.lang.String prFileExt;

    private java.lang.String prFileLayout;

    private java.lang.String prFileName;

    private java.lang.String prLOG;

    private java.lang.String prLayoutEXCEL;

    private java.lang.String prLayoutSAGA;

    private java.lang.String prMensagem;

    private java.lang.String prOrder;

    private java.lang.String prPrintDest;

    private java.lang.String prRelatorio;

    private java.lang.String prRemetente;

    private java.lang.String prRetorno;

    private java.lang.String prSaveFormat;

    private java.lang.String prTypeBmp;

    private java.lang.String prUniqueFile;

    public RelatoriosRelatsIn() {
    }

    public RelatoriosRelatsIn(
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String prAnexoBool,
           java.lang.String prAssunto,
           java.lang.String prCC,
           java.lang.String prCCo,
           java.lang.String prDest,
           java.lang.String prDir,
           java.lang.String prEntrada,
           java.lang.String prEntranceIsXML,
           java.lang.String prExecFmt,
           java.lang.String prFileExt,
           java.lang.String prFileLayout,
           java.lang.String prFileName,
           java.lang.String prLOG,
           java.lang.String prLayoutEXCEL,
           java.lang.String prLayoutSAGA,
           java.lang.String prMensagem,
           java.lang.String prOrder,
           java.lang.String prPrintDest,
           java.lang.String prRelatorio,
           java.lang.String prRemetente,
           java.lang.String prRetorno,
           java.lang.String prSaveFormat,
           java.lang.String prTypeBmp,
           java.lang.String prUniqueFile) {
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.prAnexoBool = prAnexoBool;
           this.prAssunto = prAssunto;
           this.prCC = prCC;
           this.prCCo = prCCo;
           this.prDest = prDest;
           this.prDir = prDir;
           this.prEntrada = prEntrada;
           this.prEntranceIsXML = prEntranceIsXML;
           this.prExecFmt = prExecFmt;
           this.prFileExt = prFileExt;
           this.prFileLayout = prFileLayout;
           this.prFileName = prFileName;
           this.prLOG = prLOG;
           this.prLayoutEXCEL = prLayoutEXCEL;
           this.prLayoutSAGA = prLayoutSAGA;
           this.prMensagem = prMensagem;
           this.prOrder = prOrder;
           this.prPrintDest = prPrintDest;
           this.prRelatorio = prRelatorio;
           this.prRemetente = prRemetente;
           this.prRetorno = prRetorno;
           this.prSaveFormat = prSaveFormat;
           this.prTypeBmp = prTypeBmp;
           this.prUniqueFile = prUniqueFile;
    }


    /**
     * Gets the flowInstanceID value for this RelatoriosRelatoriosIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this RelatoriosRelatoriosIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this RelatoriosRelatoriosIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this RelatoriosRelatoriosIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the prAnexoBool value for this RelatoriosRelatoriosIn.
     * 
     * @return prAnexoBool
     */
    public java.lang.String getPrAnexoBool() {
        return prAnexoBool;
    }


    /**
     * Sets the prAnexoBool value for this RelatoriosRelatoriosIn.
     * 
     * @param prAnexoBool
     */
    public void setPrAnexoBool(java.lang.String prAnexoBool) {
        this.prAnexoBool = prAnexoBool;
    }


    /**
     * Gets the prAssunto value for this RelatoriosRelatoriosIn.
     * 
     * @return prAssunto
     */
    public java.lang.String getPrAssunto() {
        return prAssunto;
    }


    /**
     * Sets the prAssunto value for this RelatoriosRelatoriosIn.
     * 
     * @param prAssunto
     */
    public void setPrAssunto(java.lang.String prAssunto) {
        this.prAssunto = prAssunto;
    }


    /**
     * Gets the prCC value for this RelatoriosRelatoriosIn.
     * 
     * @return prCC
     */
    public java.lang.String getPrCC() {
        return prCC;
    }


    /**
     * Sets the prCC value for this RelatoriosRelatoriosIn.
     * 
     * @param prCC
     */
    public void setPrCC(java.lang.String prCC) {
        this.prCC = prCC;
    }


    /**
     * Gets the prCCo value for this RelatoriosRelatoriosIn.
     * 
     * @return prCCo
     */
    public java.lang.String getPrCCo() {
        return prCCo;
    }


    /**
     * Sets the prCCo value for this RelatoriosRelatoriosIn.
     * 
     * @param prCCo
     */
    public void setPrCCo(java.lang.String prCCo) {
        this.prCCo = prCCo;
    }


    /**
     * Gets the prDest value for this RelatoriosRelatoriosIn.
     * 
     * @return prDest
     */
    public java.lang.String getPrDest() {
        return prDest;
    }


    /**
     * Sets the prDest value for this RelatoriosRelatoriosIn.
     * 
     * @param prDest
     */
    public void setPrDest(java.lang.String prDest) {
        this.prDest = prDest;
    }


    /**
     * Gets the prDir value for this RelatoriosRelatoriosIn.
     * 
     * @return prDir
     */
    public java.lang.String getPrDir() {
        return prDir;
    }


    /**
     * Sets the prDir value for this RelatoriosRelatoriosIn.
     * 
     * @param prDir
     */
    public void setPrDir(java.lang.String prDir) {
        this.prDir = prDir;
    }


    /**
     * Gets the prEntrada value for this RelatoriosRelatoriosIn.
     * 
     * @return prEntrada
     */
    public java.lang.String getPrEntrada() {
        return prEntrada;
    }


    /**
     * Sets the prEntrada value for this RelatoriosRelatoriosIn.
     * 
     * @param prEntrada
     */
    public void setPrEntrada(java.lang.String prEntrada) {
        this.prEntrada = prEntrada;
    }


    /**
     * Gets the prEntranceIsXML value for this RelatoriosRelatoriosIn.
     * 
     * @return prEntranceIsXML
     */
    public java.lang.String getPrEntranceIsXML() {
        return prEntranceIsXML;
    }


    /**
     * Sets the prEntranceIsXML value for this RelatoriosRelatoriosIn.
     * 
     * @param prEntranceIsXML
     */
    public void setPrEntranceIsXML(java.lang.String prEntranceIsXML) {
        this.prEntranceIsXML = prEntranceIsXML;
    }


    /**
     * Gets the prExecFmt value for this RelatoriosRelatoriosIn.
     * 
     * @return prExecFmt
     */
    public java.lang.String getPrExecFmt() {
        return prExecFmt;
    }


    /**
     * Sets the prExecFmt value for this RelatoriosRelatoriosIn.
     * 
     * @param prExecFmt
     */
    public void setPrExecFmt(java.lang.String prExecFmt) {
        this.prExecFmt = prExecFmt;
    }


    /**
     * Gets the prFileExt value for this RelatoriosRelatoriosIn.
     * 
     * @return prFileExt
     */
    public java.lang.String getPrFileExt() {
        return prFileExt;
    }


    /**
     * Sets the prFileExt value for this RelatoriosRelatoriosIn.
     * 
     * @param prFileExt
     */
    public void setPrFileExt(java.lang.String prFileExt) {
        this.prFileExt = prFileExt;
    }


    /**
     * Gets the prFileLayout value for this RelatoriosRelatoriosIn.
     * 
     * @return prFileLayout
     */
    public java.lang.String getPrFileLayout() {
        return prFileLayout;
    }


    /**
     * Sets the prFileLayout value for this RelatoriosRelatoriosIn.
     * 
     * @param prFileLayout
     */
    public void setPrFileLayout(java.lang.String prFileLayout) {
        this.prFileLayout = prFileLayout;
    }


    /**
     * Gets the prFileName value for this RelatoriosRelatoriosIn.
     * 
     * @return prFileName
     */
    public java.lang.String getPrFileName() {
        return prFileName;
    }


    /**
     * Sets the prFileName value for this RelatoriosRelatoriosIn.
     * 
     * @param prFileName
     */
    public void setPrFileName(java.lang.String prFileName) {
        this.prFileName = prFileName;
    }


    /**
     * Gets the prLOG value for this RelatoriosRelatoriosIn.
     * 
     * @return prLOG
     */
    public java.lang.String getPrLOG() {
        return prLOG;
    }


    /**
     * Sets the prLOG value for this RelatoriosRelatoriosIn.
     * 
     * @param prLOG
     */
    public void setPrLOG(java.lang.String prLOG) {
        this.prLOG = prLOG;
    }


    /**
     * Gets the prLayoutEXCEL value for this RelatoriosRelatoriosIn.
     * 
     * @return prLayoutEXCEL
     */
    public java.lang.String getPrLayoutEXCEL() {
        return prLayoutEXCEL;
    }


    /**
     * Sets the prLayoutEXCEL value for this RelatoriosRelatoriosIn.
     * 
     * @param prLayoutEXCEL
     */
    public void setPrLayoutEXCEL(java.lang.String prLayoutEXCEL) {
        this.prLayoutEXCEL = prLayoutEXCEL;
    }


    /**
     * Gets the prLayoutSAGA value for this RelatoriosRelatoriosIn.
     * 
     * @return prLayoutSAGA
     */
    public java.lang.String getPrLayoutSAGA() {
        return prLayoutSAGA;
    }


    /**
     * Sets the prLayoutSAGA value for this RelatoriosRelatoriosIn.
     * 
     * @param prLayoutSAGA
     */
    public void setPrLayoutSAGA(java.lang.String prLayoutSAGA) {
        this.prLayoutSAGA = prLayoutSAGA;
    }


    /**
     * Gets the prMensagem value for this RelatoriosRelatoriosIn.
     * 
     * @return prMensagem
     */
    public java.lang.String getPrMensagem() {
        return prMensagem;
    }


    /**
     * Sets the prMensagem value for this RelatoriosRelatoriosIn.
     * 
     * @param prMensagem
     */
    public void setPrMensagem(java.lang.String prMensagem) {
        this.prMensagem = prMensagem;
    }


    /**
     * Gets the prOrder value for this RelatoriosRelatoriosIn.
     * 
     * @return prOrder
     */
    public java.lang.String getPrOrder() {
        return prOrder;
    }


    /**
     * Sets the prOrder value for this RelatoriosRelatoriosIn.
     * 
     * @param prOrder
     */
    public void setPrOrder(java.lang.String prOrder) {
        this.prOrder = prOrder;
    }


    /**
     * Gets the prPrintDest value for this RelatoriosRelatoriosIn.
     * 
     * @return prPrintDest
     */
    public java.lang.String getPrPrintDest() {
        return prPrintDest;
    }


    /**
     * Sets the prPrintDest value for this RelatoriosRelatoriosIn.
     * 
     * @param prPrintDest
     */
    public void setPrPrintDest(java.lang.String prPrintDest) {
        this.prPrintDest = prPrintDest;
    }


    /**
     * Gets the prRelatorio value for this RelatoriosRelatoriosIn.
     * 
     * @return prRelatorio
     */
    public java.lang.String getPrRelatorio() {
        return prRelatorio;
    }


    /**
     * Sets the prRelatorio value for this RelatoriosRelatoriosIn.
     * 
     * @param prRelatorio
     */
    public void setPrRelatorio(java.lang.String prRelatorio) {
        this.prRelatorio = prRelatorio;
    }


    /**
     * Gets the prRemetente value for this RelatoriosRelatoriosIn.
     * 
     * @return prRemetente
     */
    public java.lang.String getPrRemetente() {
        return prRemetente;
    }


    /**
     * Sets the prRemetente value for this RelatoriosRelatoriosIn.
     * 
     * @param prRemetente
     */
    public void setPrRemetente(java.lang.String prRemetente) {
        this.prRemetente = prRemetente;
    }


    /**
     * Gets the prRetorno value for this RelatoriosRelatoriosIn.
     * 
     * @return prRetorno
     */
    public java.lang.String getPrRetorno() {
        return prRetorno;
    }


    /**
     * Sets the prRetorno value for this RelatoriosRelatoriosIn.
     * 
     * @param prRetorno
     */
    public void setPrRetorno(java.lang.String prRetorno) {
        this.prRetorno = prRetorno;
    }


    /**
     * Gets the prSaveFormat value for this RelatoriosRelatoriosIn.
     * 
     * @return prSaveFormat
     */
    public java.lang.String getPrSaveFormat() {
        return prSaveFormat;
    }


    /**
     * Sets the prSaveFormat value for this RelatoriosRelatoriosIn.
     * 
     * @param prSaveFormat
     */
    public void setPrSaveFormat(java.lang.String prSaveFormat) {
        this.prSaveFormat = prSaveFormat;
    }


    /**
     * Gets the prTypeBmp value for this RelatoriosRelatoriosIn.
     * 
     * @return prTypeBmp
     */
    public java.lang.String getPrTypeBmp() {
        return prTypeBmp;
    }


    /**
     * Sets the prTypeBmp value for this RelatoriosRelatoriosIn.
     * 
     * @param prTypeBmp
     */
    public void setPrTypeBmp(java.lang.String prTypeBmp) {
        this.prTypeBmp = prTypeBmp;
    }


    /**
     * Gets the prUniqueFile value for this RelatoriosRelatoriosIn.
     * 
     * @return prUniqueFile
     */
    public java.lang.String getPrUniqueFile() {
        return prUniqueFile;
    }


    /**
     * Sets the prUniqueFile value for this RelatoriosRelatoriosIn.
     * 
     * @param prUniqueFile
     */
    public void setPrUniqueFile(java.lang.String prUniqueFile) {
        this.prUniqueFile = prUniqueFile;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RelatoriosRelatsIn)) return false;
        RelatoriosRelatsIn other = (RelatoriosRelatsIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.prAnexoBool==null && other.getPrAnexoBool()==null) || 
             (this.prAnexoBool!=null &&
              this.prAnexoBool.equals(other.getPrAnexoBool()))) &&
            ((this.prAssunto==null && other.getPrAssunto()==null) || 
             (this.prAssunto!=null &&
              this.prAssunto.equals(other.getPrAssunto()))) &&
            ((this.prCC==null && other.getPrCC()==null) || 
             (this.prCC!=null &&
              this.prCC.equals(other.getPrCC()))) &&
            ((this.prCCo==null && other.getPrCCo()==null) || 
             (this.prCCo!=null &&
              this.prCCo.equals(other.getPrCCo()))) &&
            ((this.prDest==null && other.getPrDest()==null) || 
             (this.prDest!=null &&
              this.prDest.equals(other.getPrDest()))) &&
            ((this.prDir==null && other.getPrDir()==null) || 
             (this.prDir!=null &&
              this.prDir.equals(other.getPrDir()))) &&
            ((this.prEntrada==null && other.getPrEntrada()==null) || 
             (this.prEntrada!=null &&
              this.prEntrada.equals(other.getPrEntrada()))) &&
            ((this.prEntranceIsXML==null && other.getPrEntranceIsXML()==null) || 
             (this.prEntranceIsXML!=null &&
              this.prEntranceIsXML.equals(other.getPrEntranceIsXML()))) &&
            ((this.prExecFmt==null && other.getPrExecFmt()==null) || 
             (this.prExecFmt!=null &&
              this.prExecFmt.equals(other.getPrExecFmt()))) &&
            ((this.prFileExt==null && other.getPrFileExt()==null) || 
             (this.prFileExt!=null &&
              this.prFileExt.equals(other.getPrFileExt()))) &&
            ((this.prFileLayout==null && other.getPrFileLayout()==null) || 
             (this.prFileLayout!=null &&
              this.prFileLayout.equals(other.getPrFileLayout()))) &&
            ((this.prFileName==null && other.getPrFileName()==null) || 
             (this.prFileName!=null &&
              this.prFileName.equals(other.getPrFileName()))) &&
            ((this.prLOG==null && other.getPrLOG()==null) || 
             (this.prLOG!=null &&
              this.prLOG.equals(other.getPrLOG()))) &&
            ((this.prLayoutEXCEL==null && other.getPrLayoutEXCEL()==null) || 
             (this.prLayoutEXCEL!=null &&
              this.prLayoutEXCEL.equals(other.getPrLayoutEXCEL()))) &&
            ((this.prLayoutSAGA==null && other.getPrLayoutSAGA()==null) || 
             (this.prLayoutSAGA!=null &&
              this.prLayoutSAGA.equals(other.getPrLayoutSAGA()))) &&
            ((this.prMensagem==null && other.getPrMensagem()==null) || 
             (this.prMensagem!=null &&
              this.prMensagem.equals(other.getPrMensagem()))) &&
            ((this.prOrder==null && other.getPrOrder()==null) || 
             (this.prOrder!=null &&
              this.prOrder.equals(other.getPrOrder()))) &&
            ((this.prPrintDest==null && other.getPrPrintDest()==null) || 
             (this.prPrintDest!=null &&
              this.prPrintDest.equals(other.getPrPrintDest()))) &&
            ((this.prRelatorio==null && other.getPrRelatorio()==null) || 
             (this.prRelatorio!=null &&
              this.prRelatorio.equals(other.getPrRelatorio()))) &&
            ((this.prRemetente==null && other.getPrRemetente()==null) || 
             (this.prRemetente!=null &&
              this.prRemetente.equals(other.getPrRemetente()))) &&
            ((this.prRetorno==null && other.getPrRetorno()==null) || 
             (this.prRetorno!=null &&
              this.prRetorno.equals(other.getPrRetorno()))) &&
            ((this.prSaveFormat==null && other.getPrSaveFormat()==null) || 
             (this.prSaveFormat!=null &&
              this.prSaveFormat.equals(other.getPrSaveFormat()))) &&
            ((this.prTypeBmp==null && other.getPrTypeBmp()==null) || 
             (this.prTypeBmp!=null &&
              this.prTypeBmp.equals(other.getPrTypeBmp()))) &&
            ((this.prUniqueFile==null && other.getPrUniqueFile()==null) || 
             (this.prUniqueFile!=null &&
              this.prUniqueFile.equals(other.getPrUniqueFile())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getPrAnexoBool() != null) {
            _hashCode += getPrAnexoBool().hashCode();
        }
        if (getPrAssunto() != null) {
            _hashCode += getPrAssunto().hashCode();
        }
        if (getPrCC() != null) {
            _hashCode += getPrCC().hashCode();
        }
        if (getPrCCo() != null) {
            _hashCode += getPrCCo().hashCode();
        }
        if (getPrDest() != null) {
            _hashCode += getPrDest().hashCode();
        }
        if (getPrDir() != null) {
            _hashCode += getPrDir().hashCode();
        }
        if (getPrEntrada() != null) {
            _hashCode += getPrEntrada().hashCode();
        }
        if (getPrEntranceIsXML() != null) {
            _hashCode += getPrEntranceIsXML().hashCode();
        }
        if (getPrExecFmt() != null) {
            _hashCode += getPrExecFmt().hashCode();
        }
        if (getPrFileExt() != null) {
            _hashCode += getPrFileExt().hashCode();
        }
        if (getPrFileLayout() != null) {
            _hashCode += getPrFileLayout().hashCode();
        }
        if (getPrFileName() != null) {
            _hashCode += getPrFileName().hashCode();
        }
        if (getPrLOG() != null) {
            _hashCode += getPrLOG().hashCode();
        }
        if (getPrLayoutEXCEL() != null) {
            _hashCode += getPrLayoutEXCEL().hashCode();
        }
        if (getPrLayoutSAGA() != null) {
            _hashCode += getPrLayoutSAGA().hashCode();
        }
        if (getPrMensagem() != null) {
            _hashCode += getPrMensagem().hashCode();
        }
        if (getPrOrder() != null) {
            _hashCode += getPrOrder().hashCode();
        }
        if (getPrPrintDest() != null) {
            _hashCode += getPrPrintDest().hashCode();
        }
        if (getPrRelatorio() != null) {
            _hashCode += getPrRelatorio().hashCode();
        }
        if (getPrRemetente() != null) {
            _hashCode += getPrRemetente().hashCode();
        }
        if (getPrRetorno() != null) {
            _hashCode += getPrRetorno().hashCode();
        }
        if (getPrSaveFormat() != null) {
            _hashCode += getPrSaveFormat().hashCode();
        }
        if (getPrTypeBmp() != null) {
            _hashCode += getPrTypeBmp().hashCode();
        }
        if (getPrUniqueFile() != null) {
            _hashCode += getPrUniqueFile().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RelatoriosRelatsIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "relatoriosRelatoriosIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prAnexoBool");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prAnexoBool"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prAssunto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prAssunto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prCC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prCC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prCCo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prCCo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prDest");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prDest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prDir");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prDir"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prEntrada");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prEntrada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prEntranceIsXML");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prEntranceIsXML"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prExecFmt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prExecFmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prFileExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prFileExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prFileLayout");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prFileLayout"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prFileName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prFileName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prLOG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prLOG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prLayoutEXCEL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prLayoutEXCEL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prLayoutSAGA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prLayoutSAGA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prMensagem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prMensagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prOrder");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prOrder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prPrintDest");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prPrintDest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prRelatorio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prRelatorio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prRemetente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prRemetente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prSaveFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prSaveFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prTypeBmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prTypeBmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prUniqueFile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prUniqueFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
