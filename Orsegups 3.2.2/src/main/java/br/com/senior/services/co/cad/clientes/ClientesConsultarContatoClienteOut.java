/**
 * ClientesConsultarContatoClienteOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesConsultarContatoClienteOut  implements java.io.Serializable {
    private br.com.senior.services.co.cad.clientes.ClientesConsultarContatoClienteOutContatos[] contatos;

    private java.lang.String erroExecucao;

    private java.lang.String retorno;

    public ClientesConsultarContatoClienteOut() {
    }

    public ClientesConsultarContatoClienteOut(
           br.com.senior.services.co.cad.clientes.ClientesConsultarContatoClienteOutContatos[] contatos,
           java.lang.String erroExecucao,
           java.lang.String retorno) {
           this.contatos = contatos;
           this.erroExecucao = erroExecucao;
           this.retorno = retorno;
    }


    /**
     * Gets the contatos value for this ClientesConsultarContatoClienteOut.
     * 
     * @return contatos
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarContatoClienteOutContatos[] getContatos() {
        return contatos;
    }


    /**
     * Sets the contatos value for this ClientesConsultarContatoClienteOut.
     * 
     * @param contatos
     */
    public void setContatos(br.com.senior.services.co.cad.clientes.ClientesConsultarContatoClienteOutContatos[] contatos) {
        this.contatos = contatos;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarContatoClienteOutContatos getContatos(int i) {
        return this.contatos[i];
    }

    public void setContatos(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarContatoClienteOutContatos _value) {
        this.contatos[i] = _value;
    }


    /**
     * Gets the erroExecucao value for this ClientesConsultarContatoClienteOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this ClientesConsultarContatoClienteOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the retorno value for this ClientesConsultarContatoClienteOut.
     * 
     * @return retorno
     */
    public java.lang.String getRetorno() {
        return retorno;
    }


    /**
     * Sets the retorno value for this ClientesConsultarContatoClienteOut.
     * 
     * @param retorno
     */
    public void setRetorno(java.lang.String retorno) {
        this.retorno = retorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesConsultarContatoClienteOut)) return false;
        ClientesConsultarContatoClienteOut other = (ClientesConsultarContatoClienteOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.contatos==null && other.getContatos()==null) || 
             (this.contatos!=null &&
              java.util.Arrays.equals(this.contatos, other.getContatos()))) &&
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.retorno==null && other.getRetorno()==null) || 
             (this.retorno!=null &&
              this.retorno.equals(other.getRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getContatos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContatos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContatos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getRetorno() != null) {
            _hashCode += getRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesConsultarContatoClienteOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarContatoClienteOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contatos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "contatos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarContatoClienteOutContatos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
