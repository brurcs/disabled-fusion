/**
 * TitulosProcessarAVMIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosProcessarAVMIn  implements java.io.Serializable {
    private java.lang.String dataBaseFinal;

    private java.lang.String dataBaseInicial;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer idPacote;

    private TitulosProcessarAVMInTitulosACalcular[] titulosACalcular;

    public TitulosProcessarAVMIn() {
    }

    public TitulosProcessarAVMIn(
           java.lang.String dataBaseFinal,
           java.lang.String dataBaseInicial,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer idPacote,
           TitulosProcessarAVMInTitulosACalcular[] titulosACalcular) {
           this.dataBaseFinal = dataBaseFinal;
           this.dataBaseInicial = dataBaseInicial;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.idPacote = idPacote;
           this.titulosACalcular = titulosACalcular;
    }


    /**
     * Gets the dataBaseFinal value for this TitulosProcessarAVMIn.
     * 
     * @return dataBaseFinal
     */
    public java.lang.String getDataBaseFinal() {
        return dataBaseFinal;
    }


    /**
     * Sets the dataBaseFinal value for this TitulosProcessarAVMIn.
     * 
     * @param dataBaseFinal
     */
    public void setDataBaseFinal(java.lang.String dataBaseFinal) {
        this.dataBaseFinal = dataBaseFinal;
    }


    /**
     * Gets the dataBaseInicial value for this TitulosProcessarAVMIn.
     * 
     * @return dataBaseInicial
     */
    public java.lang.String getDataBaseInicial() {
        return dataBaseInicial;
    }


    /**
     * Sets the dataBaseInicial value for this TitulosProcessarAVMIn.
     * 
     * @param dataBaseInicial
     */
    public void setDataBaseInicial(java.lang.String dataBaseInicial) {
        this.dataBaseInicial = dataBaseInicial;
    }


    /**
     * Gets the flowInstanceID value for this TitulosProcessarAVMIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosProcessarAVMIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosProcessarAVMIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosProcessarAVMIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the idPacote value for this TitulosProcessarAVMIn.
     * 
     * @return idPacote
     */
    public java.lang.Integer getIdPacote() {
        return idPacote;
    }


    /**
     * Sets the idPacote value for this TitulosProcessarAVMIn.
     * 
     * @param idPacote
     */
    public void setIdPacote(java.lang.Integer idPacote) {
        this.idPacote = idPacote;
    }


    /**
     * Gets the titulosACalcular value for this TitulosProcessarAVMIn.
     * 
     * @return titulosACalcular
     */
    public TitulosProcessarAVMInTitulosACalcular[] getTitulosACalcular() {
        return titulosACalcular;
    }


    /**
     * Sets the titulosACalcular value for this TitulosProcessarAVMIn.
     * 
     * @param titulosACalcular
     */
    public void setTitulosACalcular(TitulosProcessarAVMInTitulosACalcular[] titulosACalcular) {
        this.titulosACalcular = titulosACalcular;
    }

    public TitulosProcessarAVMInTitulosACalcular getTitulosACalcular(int i) {
        return this.titulosACalcular[i];
    }

    public void setTitulosACalcular(int i, TitulosProcessarAVMInTitulosACalcular _value) {
        this.titulosACalcular[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosProcessarAVMIn)) return false;
        TitulosProcessarAVMIn other = (TitulosProcessarAVMIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dataBaseFinal==null && other.getDataBaseFinal()==null) || 
             (this.dataBaseFinal!=null &&
              this.dataBaseFinal.equals(other.getDataBaseFinal()))) &&
            ((this.dataBaseInicial==null && other.getDataBaseInicial()==null) || 
             (this.dataBaseInicial!=null &&
              this.dataBaseInicial.equals(other.getDataBaseInicial()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.idPacote==null && other.getIdPacote()==null) || 
             (this.idPacote!=null &&
              this.idPacote.equals(other.getIdPacote()))) &&
            ((this.titulosACalcular==null && other.getTitulosACalcular()==null) || 
             (this.titulosACalcular!=null &&
              java.util.Arrays.equals(this.titulosACalcular, other.getTitulosACalcular())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDataBaseFinal() != null) {
            _hashCode += getDataBaseFinal().hashCode();
        }
        if (getDataBaseInicial() != null) {
            _hashCode += getDataBaseInicial().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIdPacote() != null) {
            _hashCode += getIdPacote().hashCode();
        }
        if (getTitulosACalcular() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosACalcular());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosACalcular(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosProcessarAVMIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosProcessarAVMIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataBaseFinal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataBaseFinal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataBaseInicial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataBaseInicial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPacote");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idPacote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosACalcular");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosACalcular"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosProcessarAVMInTitulosACalcular"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
