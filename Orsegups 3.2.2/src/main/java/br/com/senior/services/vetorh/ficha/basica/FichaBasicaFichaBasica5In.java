/**
 * FichaBasicaFichaBasica5In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaFichaBasica5In  implements java.io.Serializable {
    
    private java.lang.Integer aciTraAfa;
    
    //Início Customizados
    private java.lang.Integer uSU_TipAdm;
    
    private java.lang.String uSU_ColSde;
    
    private java.lang.String uSU_CsgBrc;
    
    private java.lang.String uSU_ValCom;
    
    private java.lang.Integer uSU_DepEmp;
    
    private java.lang.Integer uSU_FisRes;
    //Fim Customizados

    private java.lang.String admAnt;

    private java.lang.Integer admeSo;

    private java.lang.String apeFun;
    
    private java.lang.Integer tipCol;
    
    private java.lang.Integer apuPon;

    private java.lang.Integer apuPonApu;

    private java.lang.Integer assPpr;

    private java.lang.String benRea;

    private java.lang.Integer busHor;

    private java.lang.String cadFol;

    private java.lang.String carVagHca;

    private java.lang.Integer catAnt;

    private java.lang.Integer catSef;

    private java.lang.Integer cateSo;

    private java.lang.Integer cauDemAfa;

    private java.lang.String claSalHsa;

    private java.lang.Double cnpjAn;

    private java.lang.Integer codAge;

    private java.lang.Integer codAteAfa;

    private java.lang.Integer codBan;

    private java.lang.String codCar;

    private java.lang.Integer codCat;

    private java.lang.String codCcu;

    private java.lang.String codCha;

    private java.lang.Integer codDef;

    private java.lang.String codDoeAfa;

    private java.lang.Integer codEqp;

    private java.lang.Integer codEsc;

    private java.lang.Integer codEstHsa;

    private java.lang.Integer codEtb;

    private java.lang.String codFicFmd;

    private java.lang.Integer codFil;

    private java.lang.Integer codFor;

    private java.lang.String codIdn;

    private java.lang.Integer codMotHca;

    private java.lang.Integer codMotHsa;

    private java.lang.Integer codNac;

    private java.lang.String codPro;
    
    private java.lang.Integer codSin;

    private java.lang.Integer codSinHsi;

    private java.lang.Integer codTap;
    
    private java.lang.Integer codTma;

    private java.lang.Integer codTmaHes;

    private java.lang.Integer codVinHvi;

    private java.lang.Double conBan;

    private java.lang.Double conFgt;

    private java.lang.Integer conFinCcu;

    private java.lang.Integer conRho;

    private java.lang.String conTosHlo;

    private java.lang.String conTovAfa;

    private java.lang.String conTovHfi;

    private java.lang.String conTovHlo;

    private java.lang.String cotDef;

    private java.lang.Double cplEstHsa;

    private java.lang.Double cplSalHsa;

    private java.lang.String datAdm;

    private java.lang.String datAfaAfa;

    private java.lang.String datAltCcu;

    private java.lang.String datAltHca;

    private java.lang.String datAltHcs;

    private java.lang.String datAltHes;

    private java.lang.String datAltHfi;

    private java.lang.String datAltHlo;

    private java.lang.String datAltHor;

    private java.lang.String datApo;

    private java.lang.String datInc;

    private java.lang.String datNas;

    private java.lang.String datOpc;

    private java.lang.String datTerAfa;

    private java.lang.String dcdPis;

    private java.lang.String defFis;

    private java.lang.Integer depIrf;

    private java.lang.Integer depSaf;

    private java.lang.String dexCtp;

    private java.lang.Integer diaJusAfa;

    private java.lang.String digBan;

    private java.lang.String digCar;

    private java.lang.String docEst;

    private java.lang.String emiCar;

    private java.lang.Integer escVtr;

    private java.lang.Integer estCiv;

    private java.lang.String estConAfa;

    private java.lang.String estCtp;

    private java.lang.String exmRetAfa;

    private java.lang.String fimEtbHeb;

    private java.lang.String fimEvt;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer graIns;

    private java.lang.String horAfaAfa;

    private java.lang.Integer horBas;

    private java.lang.String horDsrHor;

    private java.lang.String horInc;

    private java.lang.Integer horSabHor;

    private java.lang.String horSemHor;

    private java.lang.String horTerAfa;

    private java.lang.Integer indAdm;

    private java.lang.String iniAtu;

    private java.lang.String iniEtbHeb;

    private java.lang.String iniEvt;

    private java.lang.Integer insCur;

    private java.lang.String irrIse;

    private java.lang.String lisRai;

    private java.lang.String locTraHlo;

    private java.lang.String matAnt;

    private java.lang.String modPag;

    private java.lang.Integer moeEstHsa;

    private java.lang.Integer motPos;

    private java.lang.String movSef;

    private java.lang.String natDesHna;

    private java.lang.String nivSalHsa;

    private java.lang.String nomAteAfa;

    private java.lang.String nomFun;

    private java.lang.Integer numCad;

    private java.lang.String numCpf;

    private java.lang.Integer numCtp;

    private java.lang.Integer numEmp;

    private java.lang.String numLoc;

    private java.lang.Double numPis;

    private java.lang.String obsAfaAfa;

    private java.lang.String obsFimHeb;

    private java.lang.String obsIniHeb;

    private java.lang.Integer onuSce;

    private java.lang.String opcCes;

    private java.lang.Integer orgClaAfa;

    private java.lang.String pagSin;

    private java.lang.Integer perJur;

    private java.lang.String perPag;

    private java.lang.Integer ponEmb;

    private java.lang.String posObs;

    private java.lang.String posTra;

    private java.lang.String prvTerAfa;

    private java.lang.Integer qhrAfaAfa;

    private java.lang.Integer racCor;

    private java.lang.String ratEve;

    private java.lang.String reaRes;

    private java.lang.String rec13S;

    private java.lang.String recAdi;

    private java.lang.String recGra;

    private java.lang.String regConAfa;

    private java.lang.String resOnu;

    private java.lang.Double salEstHsa;

    private java.lang.String serCtp;

    private java.lang.Integer sisCes;

    private java.lang.Integer sitAfa;

    private java.lang.String socSinHsi;

    private java.lang.Integer tipAdmHfi;
    
    private java.lang.Integer tipAdm;

    private java.lang.Integer tipApo;

    private java.lang.Integer tipCon;

    private java.lang.String tipOpc;

    private java.lang.String tipOpe;

    private java.lang.Integer tipSal;

    private java.lang.Integer tipSalHsa;

    private java.lang.String tipSex;

    private java.lang.Integer tpCtBa;

    private java.lang.Integer turInt;
    
    private java.lang.Double valSal;

    private java.lang.Double valSalHsa;

    private java.lang.String verInt;

    public FichaBasicaFichaBasica5In() {
    }

    public FichaBasicaFichaBasica5In(
           java.lang.Integer aciTraAfa,
           java.lang.Integer uSU_TipAdm,
           java.lang.String uSU_ColSde, 
           java.lang.String uSU_CsgBrc,
           java.lang.String uSU_ValCom,
           java.lang.Integer uSU_DepEmp,
           java.lang.Integer uSU_FisRes,
           java.lang.String admAnt,
           java.lang.Integer admeSo,
           java.lang.String apeFun,
           java.lang.Integer apuPon,
           java.lang.Integer tipCol,
           java.lang.Integer apuPonApu,
           java.lang.Integer assPpr,
           java.lang.String benRea,
           java.lang.Integer busHor,
           java.lang.String cadFol,
           java.lang.String carVagHca,
           java.lang.Integer catAnt,
           java.lang.Integer catSef,
           java.lang.Integer cateSo,
           java.lang.Integer cauDemAfa,
           java.lang.String claSalHsa,
           java.lang.Double cnpjAn,
           java.lang.Integer codAge,
           java.lang.Integer codAteAfa,
           java.lang.Integer codBan,
           java.lang.String codCar,
           java.lang.Integer codCat,
           java.lang.String codCcu,
           java.lang.String codCha,
           java.lang.Integer codDef,
           java.lang.String codDoeAfa,
           java.lang.Integer codEqp,
           java.lang.Integer codEsc,
           java.lang.Integer codEstHsa,
           java.lang.Integer codEtb,
           java.lang.String codFicFmd,
           java.lang.Integer codFil,
           java.lang.Integer codFor,
           java.lang.String codIdn,
           java.lang.Integer codMotHca,
           java.lang.Integer codMotHsa,
           java.lang.Integer codNac,
           java.lang.String codPro,
           java.lang.Integer codSin,
           java.lang.Integer codSinHsi,
           java.lang.Integer codTap,
           java.lang.Integer codTma,
           java.lang.Integer codTmaHes,
           java.lang.Integer codVinHvi,
           java.lang.Double conBan,
           java.lang.Double conFgt,
           java.lang.Integer conFinCcu,
           java.lang.Integer conRho,
           java.lang.String conTosHlo,
           java.lang.String conTovAfa,
           java.lang.String conTovHfi,
           java.lang.String conTovHlo,
           java.lang.String cotDef,
           java.lang.Double cplEstHsa,
           java.lang.Double cplSalHsa,
           java.lang.String datAdm,
           java.lang.String datAfaAfa,
           java.lang.String datAltCcu,
           java.lang.String datAltHca,
           java.lang.String datAltHcs,
           java.lang.String datAltHes,
           java.lang.String datAltHfi,
           java.lang.String datAltHlo,
           java.lang.String datAltHor,
           java.lang.String datApo,
           java.lang.String datInc,
           java.lang.String datNas,
           java.lang.String datOpc,
           java.lang.String datTerAfa,
           java.lang.String dcdPis,
           java.lang.String defFis,
           java.lang.Integer depIrf,
           java.lang.Integer depSaf,
           java.lang.String dexCtp,
           java.lang.Integer diaJusAfa,
           java.lang.String digBan,
           java.lang.String digCar,
           java.lang.String docEst,
           java.lang.String emiCar,
           java.lang.Integer escVtr,
           java.lang.Integer estCiv,
           java.lang.String estConAfa,
           java.lang.String estCtp,
           java.lang.String exmRetAfa,
           java.lang.String fimEtbHeb,
           java.lang.String fimEvt,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer graIns,
           java.lang.String horAfaAfa,
           java.lang.Integer horBas,
           java.lang.String horDsrHor,
           java.lang.String horInc,
           java.lang.Integer horSabHor,
           java.lang.String horSemHor,
           java.lang.String horTerAfa,
           java.lang.Integer indAdm,
           java.lang.String iniAtu,
           java.lang.String iniEtbHeb,
           java.lang.String iniEvt,
           java.lang.Integer insCur,
           java.lang.String irrIse,
           java.lang.String lisRai,
           java.lang.String locTraHlo,
           java.lang.String matAnt,
           java.lang.String modPag,
           java.lang.Integer moeEstHsa,
           java.lang.Integer motPos,
           java.lang.String movSef,
           java.lang.String natDesHna,
           java.lang.String nivSalHsa,
           java.lang.String nomAteAfa,
           java.lang.String nomFun,
           java.lang.Integer numCad,
           java.lang.String numCpf,
           java.lang.Integer numCtp,
           java.lang.Integer numEmp,
           java.lang.String numLoc,
           java.lang.Double numPis,
           java.lang.String obsAfaAfa,
           java.lang.String obsFimHeb,
           java.lang.String obsIniHeb,
           java.lang.Integer onuSce,
           java.lang.String opcCes,
           java.lang.Integer orgClaAfa,
           java.lang.String pagSin,
           java.lang.Integer perJur,
           java.lang.String perPag,
           java.lang.Integer ponEmb,
           java.lang.String posObs,
           java.lang.String posTra,
           java.lang.String prvTerAfa,
           java.lang.Integer qhrAfaAfa,
           java.lang.Integer racCor,
           java.lang.String ratEve,
           java.lang.String reaRes,
           java.lang.String rec13S,
           java.lang.String recAdi,
           java.lang.String recGra,
           java.lang.String regConAfa,
           java.lang.String resOnu,
           java.lang.Double salEstHsa,
           java.lang.String serCtp,
           java.lang.Integer sisCes,
           java.lang.Integer sitAfa,
           java.lang.String socSinHsi,
           java.lang.Integer tipAdmHfi,
           java.lang.Integer tipAdm,
           java.lang.Integer tipApo,
           java.lang.Integer tipCon,
           java.lang.String tipOpc,
           java.lang.String tipOpe,
           java.lang.Integer tipSal,
           java.lang.Integer tipSalHsa,
           java.lang.String tipSex,
           java.lang.Integer tpCtBa,
           java.lang.Integer turInt,
           java.lang.Double valSal,
           java.lang.Double valSalHsa,
           java.lang.String verInt) {
           this.aciTraAfa = aciTraAfa;
           this.uSU_TipAdm = uSU_TipAdm;
           this.uSU_ColSde = uSU_ColSde;
           this.uSU_CsgBrc = uSU_CsgBrc;
           this.uSU_ValCom = uSU_ValCom;
           this.uSU_DepEmp = uSU_DepEmp;
           this.uSU_FisRes = uSU_FisRes;
           this.admAnt = admAnt;
           this.admeSo = admeSo;
           this.apeFun = apeFun;
           this.tipCol = tipCol;
           this.apuPon = apuPon;
           this.apuPonApu = apuPonApu;
           this.assPpr = assPpr;
           this.benRea = benRea;
           this.busHor = busHor;
           this.cadFol = cadFol;
           this.carVagHca = carVagHca;
           this.catAnt = catAnt;
           this.catSef = catSef;
           this.cateSo = cateSo;
           this.cauDemAfa = cauDemAfa;
           this.claSalHsa = claSalHsa;
           this.cnpjAn = cnpjAn;
           this.codAge = codAge;
           this.codAteAfa = codAteAfa;
           this.codBan = codBan;
           this.codCar = codCar;
           this.codCat = codCat;
           this.codCcu = codCcu;
           this.codCha = codCha;
           this.codDef = codDef;
           this.codDoeAfa = codDoeAfa;
           this.codEqp = codEqp;
           this.codEsc = codEsc;
           this.codEstHsa = codEstHsa;
           this.codEtb = codEtb;
           this.codFicFmd = codFicFmd;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codIdn = codIdn;
           this.codMotHca = codMotHca;
           this.codMotHsa = codMotHsa;
           this.codNac = codNac;
           this.codPro = codPro;
           this.codSin = codSin;
           this.codSinHsi = codSinHsi;
           this.codTap = codTap;
           this.codTma = codTma;
           this.codTmaHes = codTmaHes;
           this.codVinHvi = codVinHvi;
           this.conBan = conBan;
           this.conFgt = conFgt;
           this.conFinCcu = conFinCcu;
           this.conRho = conRho;
           this.conTosHlo = conTosHlo;
           this.conTovAfa = conTovAfa;
           this.conTovHfi = conTovHfi;
           this.conTovHlo = conTovHlo;
           this.cotDef = cotDef;
           this.cplEstHsa = cplEstHsa;
           this.cplSalHsa = cplSalHsa;
           this.datAdm = datAdm;
           this.datAfaAfa = datAfaAfa;
           this.datAltCcu = datAltCcu;
           this.datAltHca = datAltHca;
           this.datAltHcs = datAltHcs;
           this.datAltHes = datAltHes;
           this.datAltHfi = datAltHfi;
           this.datAltHlo = datAltHlo;
           this.datAltHor = datAltHor;
           this.datApo = datApo;
           this.datInc = datInc;
           this.datNas = datNas;
           this.datOpc = datOpc;
           this.datTerAfa = datTerAfa;
           this.dcdPis = dcdPis;
           this.defFis = defFis;
           this.depIrf = depIrf;
           this.depSaf = depSaf;
           this.dexCtp = dexCtp;
           this.diaJusAfa = diaJusAfa;
           this.digBan = digBan;
           this.digCar = digCar;
           this.docEst = docEst;
           this.emiCar = emiCar;
           this.escVtr = escVtr;
           this.estCiv = estCiv;
           this.estConAfa = estConAfa;
           this.estCtp = estCtp;
           this.exmRetAfa = exmRetAfa;
           this.fimEtbHeb = fimEtbHeb;
           this.fimEvt = fimEvt;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.graIns = graIns;
           this.horAfaAfa = horAfaAfa;
           this.horBas = horBas;
           this.horDsrHor = horDsrHor;
           this.horInc = horInc;
           this.horSabHor = horSabHor;
           this.horSemHor = horSemHor;
           this.horTerAfa = horTerAfa;
           this.indAdm = indAdm;
           this.iniAtu = iniAtu;
           this.iniEtbHeb = iniEtbHeb;
           this.iniEvt = iniEvt;
           this.insCur = insCur;
           this.irrIse = irrIse;
           this.lisRai = lisRai;
           this.locTraHlo = locTraHlo;
           this.matAnt = matAnt;
           this.modPag = modPag;
           this.moeEstHsa = moeEstHsa;
           this.motPos = motPos;
           this.movSef = movSef;
           this.natDesHna = natDesHna;
           this.nivSalHsa = nivSalHsa;
           this.nomAteAfa = nomAteAfa;
           this.nomFun = nomFun;
           this.numCad = numCad;
           this.numCpf = numCpf;
           this.numCtp = numCtp;
           this.numEmp = numEmp;
           this.numLoc = numLoc;
           this.numPis = numPis;
           this.obsAfaAfa = obsAfaAfa;
           this.obsFimHeb = obsFimHeb;
           this.obsIniHeb = obsIniHeb;
           this.onuSce = onuSce;
           this.opcCes = opcCes;
           this.orgClaAfa = orgClaAfa;
           this.pagSin = pagSin;
           this.perJur = perJur;
           this.perPag = perPag;
           this.ponEmb = ponEmb;
           this.posObs = posObs;
           this.posTra = posTra;
           this.prvTerAfa = prvTerAfa;
           this.qhrAfaAfa = qhrAfaAfa;
           this.racCor = racCor;
           this.ratEve = ratEve;
           this.reaRes = reaRes;
           this.rec13S = rec13S;
           this.recAdi = recAdi;
           this.recGra = recGra;
           this.regConAfa = regConAfa;
           this.resOnu = resOnu;
           this.salEstHsa = salEstHsa;
           this.serCtp = serCtp;
           this.sisCes = sisCes;
           this.sitAfa = sitAfa;
           this.socSinHsi = socSinHsi;
           this.tipAdmHfi = tipAdmHfi;
           this.tipAdm = tipAdm;
           this.tipApo = tipApo;
           this.tipCon = tipCon;
           this.tipOpc = tipOpc;
           this.tipOpe = tipOpe;
           this.tipSal = tipSal;
           this.tipSalHsa = tipSalHsa;
           this.tipSex = tipSex;
           this.tpCtBa = tpCtBa;
           this.turInt = turInt;
           this.valSal = valSal;
           this.valSalHsa = valSalHsa;
           this.verInt = verInt;
    }
    
    /**
     * Gets the uSU_FisRes value for this FichaBasicaFichaBasica5In.
     * 
     * @return uSU_FisRes
     */
    public java.lang.Integer getUSU_FisRes() {
        return uSU_FisRes;
    }


    /**
     * Sets the uSU_FisRes value for this FichaBasicaFichaBasica5In.
     * 
     * @param uSU_FisRes
     */
    public void setUSU_FisRes(java.lang.Integer uSU_FisRes) {
        this.uSU_FisRes = uSU_FisRes;
    }
    
    /**
     * Gets the uSU_DepEmp value for this FichaBasicaFichaBasica5In.
     * 
     * @return uSU_DepEmp
     */
    public java.lang.Integer getUSU_DepEmp() {
        return uSU_DepEmp;
    }


    /**
     * Sets the uSU_DepEmp value for this FichaBasicaFichaBasica5In.
     * 
     * @param uSU_DepEmp
     */
    public void setUSU_DepEmp(java.lang.Integer uSU_DepEmp) {
        this.uSU_DepEmp = uSU_DepEmp;
    }
    
    /**
     * Gets the uSU_ValCom value for this FichaBasicaFichaBasica5In.
     * 
     * @return uSU_ValCom
     */
    public java.lang.String getUSU_ValCom() {
        return uSU_ValCom;
    }


    /**
     * Sets the uSU_ValCom value for this FichaBasicaFichaBasica5In.
     * 
     * @param uSU_ValCom
     */
    public void setUSU_ValCom(java.lang.String uSU_ValCom) {
        this.uSU_ValCom = uSU_ValCom;
    }

    /**
     * Gets the uSU_CsgBrc value for this FichaBasicaFichaBasica5In.
     * 
     * @return uSU_CsgBrc
     */
    public java.lang.String getUSU_CsgBrc() {
        return uSU_CsgBrc;
    }


    /**
     * Sets the uSU_CsgBrc value for this FichaBasicaFichaBasica5In.
     * 
     * @param uSU_CsgBrc
     */
    public void setUSU_CsgBrc(java.lang.String uSU_CsgBrc) {
        this.uSU_CsgBrc = uSU_CsgBrc;
    }
    

    /**
     * Gets the uSU_ColSde value for this FichaBasicaFichaBasica5In.
     * 
     * @return uSU_ColSde
     */
    public java.lang.String getUSU_ColSde() {
        return uSU_ColSde;
    }


    /**
     * Sets the uSU_ColSde value for this FichaBasicaFichaBasica5In.
     * 
     * @param uSU_ColSde
     */
    public void setUSU_ColSde(java.lang.String uSU_ColSde) {
        this.uSU_ColSde = uSU_ColSde;
    }
    
    /**
     * Gets the uSU_TipAdm value for this FichaBasicaFichaBasica5In.
     * 
     * @return uSU_TipAdm
     */
    public java.lang.Integer getUSU_TipAdm() {
        return uSU_TipAdm;
    }


    /**
     * Sets the uSU_TipAdm value for this FichaBasicaFichaBasica5In.
     * 
     * @param uSU_TipAdm
     */
    public void setUSU_TipAdm(java.lang.Integer uSU_TipAdm) {
        this.uSU_TipAdm = uSU_TipAdm;
    }
    
    /**
     * Gets the aciTraAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return aciTraAfa
     */
    public java.lang.Integer getAciTraAfa() {
        return aciTraAfa;
    }


    /**
     * Sets the aciTraAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param aciTraAfa
     */
    public void setAciTraAfa(java.lang.Integer aciTraAfa) {
        this.aciTraAfa = aciTraAfa;
    }


    /**
     * Gets the admAnt value for this FichaBasicaFichaBasica5In.
     * 
     * @return admAnt
     */
    public java.lang.String getAdmAnt() {
        return admAnt;
    }


    /**
     * Sets the admAnt value for this FichaBasicaFichaBasica5In.
     * 
     * @param admAnt
     */
    public void setAdmAnt(java.lang.String admAnt) {
        this.admAnt = admAnt;
    }


    /**
     * Gets the admeSo value for this FichaBasicaFichaBasica5In.
     * 
     * @return admeSo
     */
    public java.lang.Integer getAdmeSo() {
        return admeSo;
    }


    /**
     * Sets the admeSo value for this FichaBasicaFichaBasica5In.
     * 
     * @param admeSo
     */
    public void setAdmeSo(java.lang.Integer admeSo) {
        this.admeSo = admeSo;
    }


    /**
     * Gets the apeFun value for this FichaBasicaFichaBasica5In.
     * 
     * @return apeFun
     */
    public java.lang.String getApeFun() {
        return apeFun;
    }


    /**
     * Sets the apeFun value for this FichaBasicaFichaBasica5In.
     * 
     * @param apeFun
     */
    public void setApeFun(java.lang.String apeFun) {
        this.apeFun = apeFun;
    }
    
    /**
     * Gets the tipCol value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }
    
    /**
     * Gets the apuPon value for this FichaBasicaFichaBasica5In.
     * 
     * @return apuPon
     */
    public java.lang.Integer getApuPon() {
        return apuPon;
    }


    /**
     * Sets the apuPon value for this FichaBasicaFichaBasica5In.
     * 
     * @param apuPon
     */
    public void setApuPon(java.lang.Integer apuPon) {
        this.apuPon = apuPon;
    }


    /**
     * Gets the apuPonApu value for this FichaBasicaFichaBasica5In.
     * 
     * @return apuPonApu
     */
    public java.lang.Integer getApuPonApu() {
        return apuPonApu;
    }


    /**
     * Sets the apuPonApu value for this FichaBasicaFichaBasica5In.
     * 
     * @param apuPonApu
     */
    public void setApuPonApu(java.lang.Integer apuPonApu) {
        this.apuPonApu = apuPonApu;
    }


    /**
     * Gets the assPpr value for this FichaBasicaFichaBasica5In.
     * 
     * @return assPpr
     */
    public java.lang.Integer getAssPpr() {
        return assPpr;
    }


    /**
     * Sets the assPpr value for this FichaBasicaFichaBasica5In.
     * 
     * @param assPpr
     */
    public void setAssPpr(java.lang.Integer assPpr) {
        this.assPpr = assPpr;
    }


    /**
     * Gets the benRea value for this FichaBasicaFichaBasica5In.
     * 
     * @return benRea
     */
    public java.lang.String getBenRea() {
        return benRea;
    }


    /**
     * Sets the benRea value for this FichaBasicaFichaBasica5In.
     * 
     * @param benRea
     */
    public void setBenRea(java.lang.String benRea) {
        this.benRea = benRea;
    }


    /**
     * Gets the busHor value for this FichaBasicaFichaBasica5In.
     * 
     * @return busHor
     */
    public java.lang.Integer getBusHor() {
        return busHor;
    }


    /**
     * Sets the busHor value for this FichaBasicaFichaBasica5In.
     * 
     * @param busHor
     */
    public void setBusHor(java.lang.Integer busHor) {
        this.busHor = busHor;
    }


    /**
     * Gets the cadFol value for this FichaBasicaFichaBasica5In.
     * 
     * @return cadFol
     */
    public java.lang.String getCadFol() {
        return cadFol;
    }


    /**
     * Sets the cadFol value for this FichaBasicaFichaBasica5In.
     * 
     * @param cadFol
     */
    public void setCadFol(java.lang.String cadFol) {
        this.cadFol = cadFol;
    }


    /**
     * Gets the carVagHca value for this FichaBasicaFichaBasica5In.
     * 
     * @return carVagHca
     */
    public java.lang.String getCarVagHca() {
        return carVagHca;
    }


    /**
     * Sets the carVagHca value for this FichaBasicaFichaBasica5In.
     * 
     * @param carVagHca
     */
    public void setCarVagHca(java.lang.String carVagHca) {
        this.carVagHca = carVagHca;
    }


    /**
     * Gets the catAnt value for this FichaBasicaFichaBasica5In.
     * 
     * @return catAnt
     */
    public java.lang.Integer getCatAnt() {
        return catAnt;
    }


    /**
     * Sets the catAnt value for this FichaBasicaFichaBasica5In.
     * 
     * @param catAnt
     */
    public void setCatAnt(java.lang.Integer catAnt) {
        this.catAnt = catAnt;
    }


    /**
     * Gets the catSef value for this FichaBasicaFichaBasica5In.
     * 
     * @return catSef
     */
    public java.lang.Integer getCatSef() {
        return catSef;
    }


    /**
     * Sets the catSef value for this FichaBasicaFichaBasica5In.
     * 
     * @param catSef
     */
    public void setCatSef(java.lang.Integer catSef) {
        this.catSef = catSef;
    }


    /**
     * Gets the cateSo value for this FichaBasicaFichaBasica5In.
     * 
     * @return cateSo
     */
    public java.lang.Integer getCateSo() {
        return cateSo;
    }


    /**
     * Sets the cateSo value for this FichaBasicaFichaBasica5In.
     * 
     * @param cateSo
     */
    public void setCateSo(java.lang.Integer cateSo) {
        this.cateSo = cateSo;
    }


    /**
     * Gets the cauDemAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return cauDemAfa
     */
    public java.lang.Integer getCauDemAfa() {
        return cauDemAfa;
    }


    /**
     * Sets the cauDemAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param cauDemAfa
     */
    public void setCauDemAfa(java.lang.Integer cauDemAfa) {
        this.cauDemAfa = cauDemAfa;
    }


    /**
     * Gets the claSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return claSalHsa
     */
    public java.lang.String getClaSalHsa() {
        return claSalHsa;
    }


    /**
     * Sets the claSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param claSalHsa
     */
    public void setClaSalHsa(java.lang.String claSalHsa) {
        this.claSalHsa = claSalHsa;
    }


    /**
     * Gets the cnpjAn value for this FichaBasicaFichaBasica5In.
     * 
     * @return cnpjAn
     */
    public java.lang.Double getCnpjAn() {
        return cnpjAn;
    }


    /**
     * Sets the cnpjAn value for this FichaBasicaFichaBasica5In.
     * 
     * @param cnpjAn
     */
    public void setCnpjAn(java.lang.Double cnpjAn) {
        this.cnpjAn = cnpjAn;
    }


    /**
     * Gets the codAge value for this FichaBasicaFichaBasica5In.
     * 
     * @return codAge
     */
    public java.lang.Integer getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this FichaBasicaFichaBasica5In.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.Integer codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codAteAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return codAteAfa
     */
    public java.lang.Integer getCodAteAfa() {
        return codAteAfa;
    }


    /**
     * Sets the codAteAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param codAteAfa
     */
    public void setCodAteAfa(java.lang.Integer codAteAfa) {
        this.codAteAfa = codAteAfa;
    }


    /**
     * Gets the codBan value for this FichaBasicaFichaBasica5In.
     * 
     * @return codBan
     */
    public java.lang.Integer getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this FichaBasicaFichaBasica5In.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.Integer codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codCar value for this FichaBasicaFichaBasica5In.
     * 
     * @return codCar
     */
    public java.lang.String getCodCar() {
        return codCar;
    }


    /**
     * Sets the codCar value for this FichaBasicaFichaBasica5In.
     * 
     * @param codCar
     */
    public void setCodCar(java.lang.String codCar) {
        this.codCar = codCar;
    }


    /**
     * Gets the codCat value for this FichaBasicaFichaBasica5In.
     * 
     * @return codCat
     */
    public java.lang.Integer getCodCat() {
        return codCat;
    }


    /**
     * Sets the codCat value for this FichaBasicaFichaBasica5In.
     * 
     * @param codCat
     */
    public void setCodCat(java.lang.Integer codCat) {
        this.codCat = codCat;
    }


    /**
     * Gets the codCcu value for this FichaBasicaFichaBasica5In.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this FichaBasicaFichaBasica5In.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codCha value for this FichaBasicaFichaBasica5In.
     * 
     * @return codCha
     */
    public java.lang.String getCodCha() {
        return codCha;
    }


    /**
     * Sets the codCha value for this FichaBasicaFichaBasica5In.
     * 
     * @param codCha
     */
    public void setCodCha(java.lang.String codCha) {
        this.codCha = codCha;
    }


    /**
     * Gets the codDef value for this FichaBasicaFichaBasica5In.
     * 
     * @return codDef
     */
    public java.lang.Integer getCodDef() {
        return codDef;
    }


    /**
     * Sets the codDef value for this FichaBasicaFichaBasica5In.
     * 
     * @param codDef
     */
    public void setCodDef(java.lang.Integer codDef) {
        this.codDef = codDef;
    }


    /**
     * Gets the codDoeAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return codDoeAfa
     */
    public java.lang.String getCodDoeAfa() {
        return codDoeAfa;
    }


    /**
     * Sets the codDoeAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param codDoeAfa
     */
    public void setCodDoeAfa(java.lang.String codDoeAfa) {
        this.codDoeAfa = codDoeAfa;
    }


    /**
     * Gets the codEqp value for this FichaBasicaFichaBasica5In.
     * 
     * @return codEqp
     */
    public java.lang.Integer getCodEqp() {
        return codEqp;
    }


    /**
     * Sets the codEqp value for this FichaBasicaFichaBasica5In.
     * 
     * @param codEqp
     */
    public void setCodEqp(java.lang.Integer codEqp) {
        this.codEqp = codEqp;
    }


    /**
     * Gets the codEsc value for this FichaBasicaFichaBasica5In.
     * 
     * @return codEsc
     */
    public java.lang.Integer getCodEsc() {
        return codEsc;
    }


    /**
     * Sets the codEsc value for this FichaBasicaFichaBasica5In.
     * 
     * @param codEsc
     */
    public void setCodEsc(java.lang.Integer codEsc) {
        this.codEsc = codEsc;
    }


    /**
     * Gets the codEstHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return codEstHsa
     */
    public java.lang.Integer getCodEstHsa() {
        return codEstHsa;
    }


    /**
     * Sets the codEstHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param codEstHsa
     */
    public void setCodEstHsa(java.lang.Integer codEstHsa) {
        this.codEstHsa = codEstHsa;
    }


    /**
     * Gets the codEtb value for this FichaBasicaFichaBasica5In.
     * 
     * @return codEtb
     */
    public java.lang.Integer getCodEtb() {
        return codEtb;
    }


    /**
     * Sets the codEtb value for this FichaBasicaFichaBasica5In.
     * 
     * @param codEtb
     */
    public void setCodEtb(java.lang.Integer codEtb) {
        this.codEtb = codEtb;
    }


    /**
     * Gets the codFicFmd value for this FichaBasicaFichaBasica5In.
     * 
     * @return codFicFmd
     */
    public java.lang.String getCodFicFmd() {
        return codFicFmd;
    }


    /**
     * Sets the codFicFmd value for this FichaBasicaFichaBasica5In.
     * 
     * @param codFicFmd
     */
    public void setCodFicFmd(java.lang.String codFicFmd) {
        this.codFicFmd = codFicFmd;
    }


    /**
     * Gets the codFil value for this FichaBasicaFichaBasica5In.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this FichaBasicaFichaBasica5In.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this FichaBasicaFichaBasica5In.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FichaBasicaFichaBasica5In.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codIdn value for this FichaBasicaFichaBasica5In.
     * 
     * @return codIdn
     */
    public java.lang.String getCodIdn() {
        return codIdn;
    }


    /**
     * Sets the codIdn value for this FichaBasicaFichaBasica5In.
     * 
     * @param codIdn
     */
    public void setCodIdn(java.lang.String codIdn) {
        this.codIdn = codIdn;
    }


    /**
     * Gets the codMotHca value for this FichaBasicaFichaBasica5In.
     * 
     * @return codMotHca
     */
    public java.lang.Integer getCodMotHca() {
        return codMotHca;
    }


    /**
     * Sets the codMotHca value for this FichaBasicaFichaBasica5In.
     * 
     * @param codMotHca
     */
    public void setCodMotHca(java.lang.Integer codMotHca) {
        this.codMotHca = codMotHca;
    }


    /**
     * Gets the codMotHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return codMotHsa
     */
    public java.lang.Integer getCodMotHsa() {
        return codMotHsa;
    }


    /**
     * Sets the codMotHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param codMotHsa
     */
    public void setCodMotHsa(java.lang.Integer codMotHsa) {
        this.codMotHsa = codMotHsa;
    }


    /**
     * Gets the codNac value for this FichaBasicaFichaBasica5In.
     * 
     * @return codNac
     */
    public java.lang.Integer getCodNac() {
        return codNac;
    }


    /**
     * Sets the codNac value for this FichaBasicaFichaBasica5In.
     * 
     * @param codNac
     */
    public void setCodNac(java.lang.Integer codNac) {
        this.codNac = codNac;
    }


    /**
     * Gets the codPro value for this FichaBasicaFichaBasica5In.
     * 
     * @return codPro
     */
    public java.lang.String getCodPro() {
        return codPro;
    }


    /**
     * Sets the codPro value for this FichaBasicaFichaBasica5In.
     * 
     * @param codPro
     */
    public void setCodPro(java.lang.String codPro) {
        this.codPro = codPro;
    }
    
    /**
     * Gets the codSin value for this FichaBasicaFichaBasica5In.
     * 
     * @return codSin
     */
    public java.lang.Integer getCodSin() {
        return codSin;
    }


    /**
     * Sets the codSin value for this FichaBasicaFichaBasica5In.
     * 
     * @param codSin
     */
    public void setCodSin(java.lang.Integer codSin) {
        this.codSin = codSin;
    }


    /**
     * Gets the codSinHsi value for this FichaBasicaFichaBasica5In.
     * 
     * @return codSinHsi
     */
    public java.lang.Integer getCodSinHsi() {
        return codSinHsi;
    }


    /**
     * Sets the codSinHsi value for this FichaBasicaFichaBasica5In.
     * 
     * @param codSinHsi
     */
    public void setCodSinHsi(java.lang.Integer codSinHsi) {
        this.codSinHsi = codSinHsi;
    }


    /**
     * Gets the codTap value for this FichaBasicaFichaBasica5In.
     * 
     * @return codTap
     */
    public java.lang.Integer getCodTap() {
        return codTap;
    }


    /**
     * Sets the codTap value for this FichaBasicaFichaBasica5In.
     * 
     * @param codTap
     */
    public void setCodTap(java.lang.Integer codTap) {
        this.codTap = codTap;
    }
    
    /**
     * Gets the codTma value for this FichaBasicaFichaBasica5In.
     * 
     * @return codTma
     */
    public java.lang.Integer getCodTma() {
        return codTma;
    }


    /**
     * Sets the codTma value for this FichaBasicaFichaBasica5In.
     * 
     * @param codTma
     */
    public void setCodTma(java.lang.Integer codTma) {
        this.codTma = codTma;
    }


    /**
     * Gets the codTmaHes value for this FichaBasicaFichaBasica5In.
     * 
     * @return codTmaHes
     */
    public java.lang.Integer getCodTmaHes() {
        return codTmaHes;
    }


    /**
     * Sets the codTmaHes value for this FichaBasicaFichaBasica5In.
     * 
     * @param codTmaHes
     */
    public void setCodTmaHes(java.lang.Integer codTmaHes) {
        this.codTmaHes = codTmaHes;
    }


    /**
     * Gets the codVinHvi value for this FichaBasicaFichaBasica5In.
     * 
     * @return codVinHvi
     */
    public java.lang.Integer getCodVinHvi() {
        return codVinHvi;
    }


    /**
     * Sets the codVinHvi value for this FichaBasicaFichaBasica5In.
     * 
     * @param codVinHvi
     */
    public void setCodVinHvi(java.lang.Integer codVinHvi) {
        this.codVinHvi = codVinHvi;
    }


    /**
     * Gets the conBan value for this FichaBasicaFichaBasica5In.
     * 
     * @return conBan
     */
    public java.lang.Double getConBan() {
        return conBan;
    }


    /**
     * Sets the conBan value for this FichaBasicaFichaBasica5In.
     * 
     * @param conBan
     */
    public void setConBan(java.lang.Double conBan) {
        this.conBan = conBan;
    }


    /**
     * Gets the conFgt value for this FichaBasicaFichaBasica5In.
     * 
     * @return conFgt
     */
    public java.lang.Double getConFgt() {
        return conFgt;
    }


    /**
     * Sets the conFgt value for this FichaBasicaFichaBasica5In.
     * 
     * @param conFgt
     */
    public void setConFgt(java.lang.Double conFgt) {
        this.conFgt = conFgt;
    }


    /**
     * Gets the conFinCcu value for this FichaBasicaFichaBasica5In.
     * 
     * @return conFinCcu
     */
    public java.lang.Integer getConFinCcu() {
        return conFinCcu;
    }


    /**
     * Sets the conFinCcu value for this FichaBasicaFichaBasica5In.
     * 
     * @param conFinCcu
     */
    public void setConFinCcu(java.lang.Integer conFinCcu) {
        this.conFinCcu = conFinCcu;
    }


    /**
     * Gets the conRho value for this FichaBasicaFichaBasica5In.
     * 
     * @return conRho
     */
    public java.lang.Integer getConRho() {
        return conRho;
    }


    /**
     * Sets the conRho value for this FichaBasicaFichaBasica5In.
     * 
     * @param conRho
     */
    public void setConRho(java.lang.Integer conRho) {
        this.conRho = conRho;
    }


    /**
     * Gets the conTosHlo value for this FichaBasicaFichaBasica5In.
     * 
     * @return conTosHlo
     */
    public java.lang.String getConTosHlo() {
        return conTosHlo;
    }


    /**
     * Sets the conTosHlo value for this FichaBasicaFichaBasica5In.
     * 
     * @param conTosHlo
     */
    public void setConTosHlo(java.lang.String conTosHlo) {
        this.conTosHlo = conTosHlo;
    }


    /**
     * Gets the conTovAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return conTovAfa
     */
    public java.lang.String getConTovAfa() {
        return conTovAfa;
    }


    /**
     * Sets the conTovAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param conTovAfa
     */
    public void setConTovAfa(java.lang.String conTovAfa) {
        this.conTovAfa = conTovAfa;
    }


    /**
     * Gets the conTovHfi value for this FichaBasicaFichaBasica5In.
     * 
     * @return conTovHfi
     */
    public java.lang.String getConTovHfi() {
        return conTovHfi;
    }


    /**
     * Sets the conTovHfi value for this FichaBasicaFichaBasica5In.
     * 
     * @param conTovHfi
     */
    public void setConTovHfi(java.lang.String conTovHfi) {
        this.conTovHfi = conTovHfi;
    }


    /**
     * Gets the conTovHlo value for this FichaBasicaFichaBasica5In.
     * 
     * @return conTovHlo
     */
    public java.lang.String getConTovHlo() {
        return conTovHlo;
    }


    /**
     * Sets the conTovHlo value for this FichaBasicaFichaBasica5In.
     * 
     * @param conTovHlo
     */
    public void setConTovHlo(java.lang.String conTovHlo) {
        this.conTovHlo = conTovHlo;
    }


    /**
     * Gets the cotDef value for this FichaBasicaFichaBasica5In.
     * 
     * @return cotDef
     */
    public java.lang.String getCotDef() {
        return cotDef;
    }


    /**
     * Sets the cotDef value for this FichaBasicaFichaBasica5In.
     * 
     * @param cotDef
     */
    public void setCotDef(java.lang.String cotDef) {
        this.cotDef = cotDef;
    }


    /**
     * Gets the cplEstHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return cplEstHsa
     */
    public java.lang.Double getCplEstHsa() {
        return cplEstHsa;
    }


    /**
     * Sets the cplEstHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param cplEstHsa
     */
    public void setCplEstHsa(java.lang.Double cplEstHsa) {
        this.cplEstHsa = cplEstHsa;
    }


    /**
     * Gets the cplSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return cplSalHsa
     */
    public java.lang.Double getCplSalHsa() {
        return cplSalHsa;
    }


    /**
     * Sets the cplSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param cplSalHsa
     */
    public void setCplSalHsa(java.lang.Double cplSalHsa) {
        this.cplSalHsa = cplSalHsa;
    }


    /**
     * Gets the datAdm value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAdm
     */
    public java.lang.String getDatAdm() {
        return datAdm;
    }


    /**
     * Sets the datAdm value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAdm
     */
    public void setDatAdm(java.lang.String datAdm) {
        this.datAdm = datAdm;
    }


    /**
     * Gets the datAfaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAfaAfa
     */
    public java.lang.String getDatAfaAfa() {
        return datAfaAfa;
    }


    /**
     * Sets the datAfaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAfaAfa
     */
    public void setDatAfaAfa(java.lang.String datAfaAfa) {
        this.datAfaAfa = datAfaAfa;
    }


    /**
     * Gets the datAltCcu value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAltCcu
     */
    public java.lang.String getDatAltCcu() {
        return datAltCcu;
    }


    /**
     * Sets the datAltCcu value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAltCcu
     */
    public void setDatAltCcu(java.lang.String datAltCcu) {
        this.datAltCcu = datAltCcu;
    }


    /**
     * Gets the datAltHca value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAltHca
     */
    public java.lang.String getDatAltHca() {
        return datAltHca;
    }


    /**
     * Sets the datAltHca value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAltHca
     */
    public void setDatAltHca(java.lang.String datAltHca) {
        this.datAltHca = datAltHca;
    }


    /**
     * Gets the datAltHcs value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAltHcs
     */
    public java.lang.String getDatAltHcs() {
        return datAltHcs;
    }


    /**
     * Sets the datAltHcs value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAltHcs
     */
    public void setDatAltHcs(java.lang.String datAltHcs) {
        this.datAltHcs = datAltHcs;
    }


    /**
     * Gets the datAltHes value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAltHes
     */
    public java.lang.String getDatAltHes() {
        return datAltHes;
    }


    /**
     * Sets the datAltHes value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAltHes
     */
    public void setDatAltHes(java.lang.String datAltHes) {
        this.datAltHes = datAltHes;
    }


    /**
     * Gets the datAltHfi value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAltHfi
     */
    public java.lang.String getDatAltHfi() {
        return datAltHfi;
    }


    /**
     * Sets the datAltHfi value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAltHfi
     */
    public void setDatAltHfi(java.lang.String datAltHfi) {
        this.datAltHfi = datAltHfi;
    }


    /**
     * Gets the datAltHlo value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAltHlo
     */
    public java.lang.String getDatAltHlo() {
        return datAltHlo;
    }


    /**
     * Sets the datAltHlo value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAltHlo
     */
    public void setDatAltHlo(java.lang.String datAltHlo) {
        this.datAltHlo = datAltHlo;
    }


    /**
     * Gets the datAltHor value for this FichaBasicaFichaBasica5In.
     * 
     * @return datAltHor
     */
    public java.lang.String getDatAltHor() {
        return datAltHor;
    }


    /**
     * Sets the datAltHor value for this FichaBasicaFichaBasica5In.
     * 
     * @param datAltHor
     */
    public void setDatAltHor(java.lang.String datAltHor) {
        this.datAltHor = datAltHor;
    }


    /**
     * Gets the datApo value for this FichaBasicaFichaBasica5In.
     * 
     * @return datApo
     */
    public java.lang.String getDatApo() {
        return datApo;
    }


    /**
     * Sets the datApo value for this FichaBasicaFichaBasica5In.
     * 
     * @param datApo
     */
    public void setDatApo(java.lang.String datApo) {
        this.datApo = datApo;
    }


    /**
     * Gets the datInc value for this FichaBasicaFichaBasica5In.
     * 
     * @return datInc
     */
    public java.lang.String getDatInc() {
        return datInc;
    }


    /**
     * Sets the datInc value for this FichaBasicaFichaBasica5In.
     * 
     * @param datInc
     */
    public void setDatInc(java.lang.String datInc) {
        this.datInc = datInc;
    }


    /**
     * Gets the datNas value for this FichaBasicaFichaBasica5In.
     * 
     * @return datNas
     */
    public java.lang.String getDatNas() {
        return datNas;
    }


    /**
     * Sets the datNas value for this FichaBasicaFichaBasica5In.
     * 
     * @param datNas
     */
    public void setDatNas(java.lang.String datNas) {
        this.datNas = datNas;
    }


    /**
     * Gets the datOpc value for this FichaBasicaFichaBasica5In.
     * 
     * @return datOpc
     */
    public java.lang.String getDatOpc() {
        return datOpc;
    }


    /**
     * Sets the datOpc value for this FichaBasicaFichaBasica5In.
     * 
     * @param datOpc
     */
    public void setDatOpc(java.lang.String datOpc) {
        this.datOpc = datOpc;
    }


    /**
     * Gets the datTerAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return datTerAfa
     */
    public java.lang.String getDatTerAfa() {
        return datTerAfa;
    }


    /**
     * Sets the datTerAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param datTerAfa
     */
    public void setDatTerAfa(java.lang.String datTerAfa) {
        this.datTerAfa = datTerAfa;
    }


    /**
     * Gets the dcdPis value for this FichaBasicaFichaBasica5In.
     * 
     * @return dcdPis
     */
    public java.lang.String getDcdPis() {
        return dcdPis;
    }


    /**
     * Sets the dcdPis value for this FichaBasicaFichaBasica5In.
     * 
     * @param dcdPis
     */
    public void setDcdPis(java.lang.String dcdPis) {
        this.dcdPis = dcdPis;
    }


    /**
     * Gets the defFis value for this FichaBasicaFichaBasica5In.
     * 
     * @return defFis
     */
    public java.lang.String getDefFis() {
        return defFis;
    }


    /**
     * Sets the defFis value for this FichaBasicaFichaBasica5In.
     * 
     * @param defFis
     */
    public void setDefFis(java.lang.String defFis) {
        this.defFis = defFis;
    }


    /**
     * Gets the depIrf value for this FichaBasicaFichaBasica5In.
     * 
     * @return depIrf
     */
    public java.lang.Integer getDepIrf() {
        return depIrf;
    }


    /**
     * Sets the depIrf value for this FichaBasicaFichaBasica5In.
     * 
     * @param depIrf
     */
    public void setDepIrf(java.lang.Integer depIrf) {
        this.depIrf = depIrf;
    }


    /**
     * Gets the depSaf value for this FichaBasicaFichaBasica5In.
     * 
     * @return depSaf
     */
    public java.lang.Integer getDepSaf() {
        return depSaf;
    }


    /**
     * Sets the depSaf value for this FichaBasicaFichaBasica5In.
     * 
     * @param depSaf
     */
    public void setDepSaf(java.lang.Integer depSaf) {
        this.depSaf = depSaf;
    }


    /**
     * Gets the dexCtp value for this FichaBasicaFichaBasica5In.
     * 
     * @return dexCtp
     */
    public java.lang.String getDexCtp() {
        return dexCtp;
    }


    /**
     * Sets the dexCtp value for this FichaBasicaFichaBasica5In.
     * 
     * @param dexCtp
     */
    public void setDexCtp(java.lang.String dexCtp) {
        this.dexCtp = dexCtp;
    }


    /**
     * Gets the diaJusAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return diaJusAfa
     */
    public java.lang.Integer getDiaJusAfa() {
        return diaJusAfa;
    }


    /**
     * Sets the diaJusAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param diaJusAfa
     */
    public void setDiaJusAfa(java.lang.Integer diaJusAfa) {
        this.diaJusAfa = diaJusAfa;
    }


    /**
     * Gets the digBan value for this FichaBasicaFichaBasica5In.
     * 
     * @return digBan
     */
    public java.lang.String getDigBan() {
        return digBan;
    }


    /**
     * Sets the digBan value for this FichaBasicaFichaBasica5In.
     * 
     * @param digBan
     */
    public void setDigBan(java.lang.String digBan) {
        this.digBan = digBan;
    }


    /**
     * Gets the digCar value for this FichaBasicaFichaBasica5In.
     * 
     * @return digCar
     */
    public java.lang.String getDigCar() {
        return digCar;
    }


    /**
     * Sets the digCar value for this FichaBasicaFichaBasica5In.
     * 
     * @param digCar
     */
    public void setDigCar(java.lang.String digCar) {
        this.digCar = digCar;
    }


    /**
     * Gets the docEst value for this FichaBasicaFichaBasica5In.
     * 
     * @return docEst
     */
    public java.lang.String getDocEst() {
        return docEst;
    }


    /**
     * Sets the docEst value for this FichaBasicaFichaBasica5In.
     * 
     * @param docEst
     */
    public void setDocEst(java.lang.String docEst) {
        this.docEst = docEst;
    }


    /**
     * Gets the emiCar value for this FichaBasicaFichaBasica5In.
     * 
     * @return emiCar
     */
    public java.lang.String getEmiCar() {
        return emiCar;
    }


    /**
     * Sets the emiCar value for this FichaBasicaFichaBasica5In.
     * 
     * @param emiCar
     */
    public void setEmiCar(java.lang.String emiCar) {
        this.emiCar = emiCar;
    }


    /**
     * Gets the escVtr value for this FichaBasicaFichaBasica5In.
     * 
     * @return escVtr
     */
    public java.lang.Integer getEscVtr() {
        return escVtr;
    }


    /**
     * Sets the escVtr value for this FichaBasicaFichaBasica5In.
     * 
     * @param escVtr
     */
    public void setEscVtr(java.lang.Integer escVtr) {
        this.escVtr = escVtr;
    }


    /**
     * Gets the estCiv value for this FichaBasicaFichaBasica5In.
     * 
     * @return estCiv
     */
    public java.lang.Integer getEstCiv() {
        return estCiv;
    }


    /**
     * Sets the estCiv value for this FichaBasicaFichaBasica5In.
     * 
     * @param estCiv
     */
    public void setEstCiv(java.lang.Integer estCiv) {
        this.estCiv = estCiv;
    }


    /**
     * Gets the estConAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return estConAfa
     */
    public java.lang.String getEstConAfa() {
        return estConAfa;
    }


    /**
     * Sets the estConAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param estConAfa
     */
    public void setEstConAfa(java.lang.String estConAfa) {
        this.estConAfa = estConAfa;
    }


    /**
     * Gets the estCtp value for this FichaBasicaFichaBasica5In.
     * 
     * @return estCtp
     */
    public java.lang.String getEstCtp() {
        return estCtp;
    }


    /**
     * Sets the estCtp value for this FichaBasicaFichaBasica5In.
     * 
     * @param estCtp
     */
    public void setEstCtp(java.lang.String estCtp) {
        this.estCtp = estCtp;
    }


    /**
     * Gets the exmRetAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return exmRetAfa
     */
    public java.lang.String getExmRetAfa() {
        return exmRetAfa;
    }


    /**
     * Sets the exmRetAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param exmRetAfa
     */
    public void setExmRetAfa(java.lang.String exmRetAfa) {
        this.exmRetAfa = exmRetAfa;
    }


    /**
     * Gets the fimEtbHeb value for this FichaBasicaFichaBasica5In.
     * 
     * @return fimEtbHeb
     */
    public java.lang.String getFimEtbHeb() {
        return fimEtbHeb;
    }


    /**
     * Sets the fimEtbHeb value for this FichaBasicaFichaBasica5In.
     * 
     * @param fimEtbHeb
     */
    public void setFimEtbHeb(java.lang.String fimEtbHeb) {
        this.fimEtbHeb = fimEtbHeb;
    }


    /**
     * Gets the fimEvt value for this FichaBasicaFichaBasica5In.
     * 
     * @return fimEvt
     */
    public java.lang.String getFimEvt() {
        return fimEvt;
    }


    /**
     * Sets the fimEvt value for this FichaBasicaFichaBasica5In.
     * 
     * @param fimEvt
     */
    public void setFimEvt(java.lang.String fimEvt) {
        this.fimEvt = fimEvt;
    }


    /**
     * Gets the flowInstanceID value for this FichaBasicaFichaBasica5In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this FichaBasicaFichaBasica5In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this FichaBasicaFichaBasica5In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this FichaBasicaFichaBasica5In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the graIns value for this FichaBasicaFichaBasica5In.
     * 
     * @return graIns
     */
    public java.lang.Integer getGraIns() {
        return graIns;
    }


    /**
     * Sets the graIns value for this FichaBasicaFichaBasica5In.
     * 
     * @param graIns
     */
    public void setGraIns(java.lang.Integer graIns) {
        this.graIns = graIns;
    }


    /**
     * Gets the horAfaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return horAfaAfa
     */
    public java.lang.String getHorAfaAfa() {
        return horAfaAfa;
    }


    /**
     * Sets the horAfaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param horAfaAfa
     */
    public void setHorAfaAfa(java.lang.String horAfaAfa) {
        this.horAfaAfa = horAfaAfa;
    }


    /**
     * Gets the horBas value for this FichaBasicaFichaBasica5In.
     * 
     * @return horBas
     */
    public java.lang.Integer getHorBas() {
        return horBas;
    }


    /**
     * Sets the horBas value for this FichaBasicaFichaBasica5In.
     * 
     * @param horBas
     */
    public void setHorBas(java.lang.Integer horBas) {
        this.horBas = horBas;
    }


    /**
     * Gets the horDsrHor value for this FichaBasicaFichaBasica5In.
     * 
     * @return horDsrHor
     */
    public java.lang.String getHorDsrHor() {
        return horDsrHor;
    }


    /**
     * Sets the horDsrHor value for this FichaBasicaFichaBasica5In.
     * 
     * @param horDsrHor
     */
    public void setHorDsrHor(java.lang.String horDsrHor) {
        this.horDsrHor = horDsrHor;
    }


    /**
     * Gets the horInc value for this FichaBasicaFichaBasica5In.
     * 
     * @return horInc
     */
    public java.lang.String getHorInc() {
        return horInc;
    }


    /**
     * Sets the horInc value for this FichaBasicaFichaBasica5In.
     * 
     * @param horInc
     */
    public void setHorInc(java.lang.String horInc) {
        this.horInc = horInc;
    }


    /**
     * Gets the horSabHor value for this FichaBasicaFichaBasica5In.
     * 
     * @return horSabHor
     */
    public java.lang.Integer getHorSabHor() {
        return horSabHor;
    }


    /**
     * Sets the horSabHor value for this FichaBasicaFichaBasica5In.
     * 
     * @param horSabHor
     */
    public void setHorSabHor(java.lang.Integer horSabHor) {
        this.horSabHor = horSabHor;
    }


    /**
     * Gets the horSemHor value for this FichaBasicaFichaBasica5In.
     * 
     * @return horSemHor
     */
    public java.lang.String getHorSemHor() {
        return horSemHor;
    }


    /**
     * Sets the horSemHor value for this FichaBasicaFichaBasica5In.
     * 
     * @param horSemHor
     */
    public void setHorSemHor(java.lang.String horSemHor) {
        this.horSemHor = horSemHor;
    }


    /**
     * Gets the horTerAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return horTerAfa
     */
    public java.lang.String getHorTerAfa() {
        return horTerAfa;
    }


    /**
     * Sets the horTerAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param horTerAfa
     */
    public void setHorTerAfa(java.lang.String horTerAfa) {
        this.horTerAfa = horTerAfa;
    }


    /**
     * Gets the indAdm value for this FichaBasicaFichaBasica5In.
     * 
     * @return indAdm
     */
    public java.lang.Integer getIndAdm() {
        return indAdm;
    }


    /**
     * Sets the indAdm value for this FichaBasicaFichaBasica5In.
     * 
     * @param indAdm
     */
    public void setIndAdm(java.lang.Integer indAdm) {
        this.indAdm = indAdm;
    }


    /**
     * Gets the iniAtu value for this FichaBasicaFichaBasica5In.
     * 
     * @return iniAtu
     */
    public java.lang.String getIniAtu() {
        return iniAtu;
    }


    /**
     * Sets the iniAtu value for this FichaBasicaFichaBasica5In.
     * 
     * @param iniAtu
     */
    public void setIniAtu(java.lang.String iniAtu) {
        this.iniAtu = iniAtu;
    }


    /**
     * Gets the iniEtbHeb value for this FichaBasicaFichaBasica5In.
     * 
     * @return iniEtbHeb
     */
    public java.lang.String getIniEtbHeb() {
        return iniEtbHeb;
    }


    /**
     * Sets the iniEtbHeb value for this FichaBasicaFichaBasica5In.
     * 
     * @param iniEtbHeb
     */
    public void setIniEtbHeb(java.lang.String iniEtbHeb) {
        this.iniEtbHeb = iniEtbHeb;
    }


    /**
     * Gets the iniEvt value for this FichaBasicaFichaBasica5In.
     * 
     * @return iniEvt
     */
    public java.lang.String getIniEvt() {
        return iniEvt;
    }


    /**
     * Sets the iniEvt value for this FichaBasicaFichaBasica5In.
     * 
     * @param iniEvt
     */
    public void setIniEvt(java.lang.String iniEvt) {
        this.iniEvt = iniEvt;
    }


    /**
     * Gets the insCur value for this FichaBasicaFichaBasica5In.
     * 
     * @return insCur
     */
    public java.lang.Integer getInsCur() {
        return insCur;
    }


    /**
     * Sets the insCur value for this FichaBasicaFichaBasica5In.
     * 
     * @param insCur
     */
    public void setInsCur(java.lang.Integer insCur) {
        this.insCur = insCur;
    }


    /**
     * Gets the irrIse value for this FichaBasicaFichaBasica5In.
     * 
     * @return irrIse
     */
    public java.lang.String getIrrIse() {
        return irrIse;
    }


    /**
     * Sets the irrIse value for this FichaBasicaFichaBasica5In.
     * 
     * @param irrIse
     */
    public void setIrrIse(java.lang.String irrIse) {
        this.irrIse = irrIse;
    }


    /**
     * Gets the lisRai value for this FichaBasicaFichaBasica5In.
     * 
     * @return lisRai
     */
    public java.lang.String getLisRai() {
        return lisRai;
    }


    /**
     * Sets the lisRai value for this FichaBasicaFichaBasica5In.
     * 
     * @param lisRai
     */
    public void setLisRai(java.lang.String lisRai) {
        this.lisRai = lisRai;
    }


    /**
     * Gets the locTraHlo value for this FichaBasicaFichaBasica5In.
     * 
     * @return locTraHlo
     */
    public java.lang.String getLocTraHlo() {
        return locTraHlo;
    }


    /**
     * Sets the locTraHlo value for this FichaBasicaFichaBasica5In.
     * 
     * @param locTraHlo
     */
    public void setLocTraHlo(java.lang.String locTraHlo) {
        this.locTraHlo = locTraHlo;
    }


    /**
     * Gets the matAnt value for this FichaBasicaFichaBasica5In.
     * 
     * @return matAnt
     */
    public java.lang.String getMatAnt() {
        return matAnt;
    }


    /**
     * Sets the matAnt value for this FichaBasicaFichaBasica5In.
     * 
     * @param matAnt
     */
    public void setMatAnt(java.lang.String matAnt) {
        this.matAnt = matAnt;
    }


    /**
     * Gets the modPag value for this FichaBasicaFichaBasica5In.
     * 
     * @return modPag
     */
    public java.lang.String getModPag() {
        return modPag;
    }


    /**
     * Sets the modPag value for this FichaBasicaFichaBasica5In.
     * 
     * @param modPag
     */
    public void setModPag(java.lang.String modPag) {
        this.modPag = modPag;
    }


    /**
     * Gets the moeEstHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return moeEstHsa
     */
    public java.lang.Integer getMoeEstHsa() {
        return moeEstHsa;
    }


    /**
     * Sets the moeEstHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param moeEstHsa
     */
    public void setMoeEstHsa(java.lang.Integer moeEstHsa) {
        this.moeEstHsa = moeEstHsa;
    }


    /**
     * Gets the motPos value for this FichaBasicaFichaBasica5In.
     * 
     * @return motPos
     */
    public java.lang.Integer getMotPos() {
        return motPos;
    }


    /**
     * Sets the motPos value for this FichaBasicaFichaBasica5In.
     * 
     * @param motPos
     */
    public void setMotPos(java.lang.Integer motPos) {
        this.motPos = motPos;
    }


    /**
     * Gets the movSef value for this FichaBasicaFichaBasica5In.
     * 
     * @return movSef
     */
    public java.lang.String getMovSef() {
        return movSef;
    }


    /**
     * Sets the movSef value for this FichaBasicaFichaBasica5In.
     * 
     * @param movSef
     */
    public void setMovSef(java.lang.String movSef) {
        this.movSef = movSef;
    }


    /**
     * Gets the natDesHna value for this FichaBasicaFichaBasica5In.
     * 
     * @return natDesHna
     */
    public java.lang.String getNatDesHna() {
        return natDesHna;
    }


    /**
     * Sets the natDesHna value for this FichaBasicaFichaBasica5In.
     * 
     * @param natDesHna
     */
    public void setNatDesHna(java.lang.String natDesHna) {
        this.natDesHna = natDesHna;
    }


    /**
     * Gets the nivSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return nivSalHsa
     */
    public java.lang.String getNivSalHsa() {
        return nivSalHsa;
    }


    /**
     * Sets the nivSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param nivSalHsa
     */
    public void setNivSalHsa(java.lang.String nivSalHsa) {
        this.nivSalHsa = nivSalHsa;
    }


    /**
     * Gets the nomAteAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return nomAteAfa
     */
    public java.lang.String getNomAteAfa() {
        return nomAteAfa;
    }


    /**
     * Sets the nomAteAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param nomAteAfa
     */
    public void setNomAteAfa(java.lang.String nomAteAfa) {
        this.nomAteAfa = nomAteAfa;
    }


    /**
     * Gets the nomFun value for this FichaBasicaFichaBasica5In.
     * 
     * @return nomFun
     */
    public java.lang.String getNomFun() {
        return nomFun;
    }


    /**
     * Sets the nomFun value for this FichaBasicaFichaBasica5In.
     * 
     * @param nomFun
     */
    public void setNomFun(java.lang.String nomFun) {
        this.nomFun = nomFun;
    }


    /**
     * Gets the numCad value for this FichaBasicaFichaBasica5In.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this FichaBasicaFichaBasica5In.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numCpf value for this FichaBasicaFichaBasica5In.
     * 
     * @return numCpf
     */
    public java.lang.String getNumCpf() {
        return numCpf;
    }


    /**
     * Sets the numCpf value for this FichaBasicaFichaBasica5In.
     * 
     * @param numCpf
     */
    public void setNumCpf(java.lang.String numCpf) {
        this.numCpf = numCpf;
    }


    /**
     * Gets the numCtp value for this FichaBasicaFichaBasica5In.
     * 
     * @return numCtp
     */
    public java.lang.Integer getNumCtp() {
        return numCtp;
    }


    /**
     * Sets the numCtp value for this FichaBasicaFichaBasica5In.
     * 
     * @param numCtp
     */
    public void setNumCtp(java.lang.Integer numCtp) {
        this.numCtp = numCtp;
    }


    /**
     * Gets the numEmp value for this FichaBasicaFichaBasica5In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this FichaBasicaFichaBasica5In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the numLoc value for this FichaBasicaFichaBasica5In.
     * 
     * @return numLoc
     */
    public java.lang.String getNumLoc() {
        return numLoc;
    }


    /**
     * Sets the numLoc value for this FichaBasicaFichaBasica5In.
     * 
     * @param numLoc
     */
    public void setNumLoc(java.lang.String numLoc) {
        this.numLoc = numLoc;
    }


    /**
     * Gets the numPis value for this FichaBasicaFichaBasica5In.
     * 
     * @return numPis
     */
    public java.lang.Double getNumPis() {
        return numPis;
    }


    /**
     * Sets the numPis value for this FichaBasicaFichaBasica5In.
     * 
     * @param numPis
     */
    public void setNumPis(java.lang.Double numPis) {
        this.numPis = numPis;
    }


    /**
     * Gets the obsAfaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return obsAfaAfa
     */
    public java.lang.String getObsAfaAfa() {
        return obsAfaAfa;
    }


    /**
     * Sets the obsAfaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param obsAfaAfa
     */
    public void setObsAfaAfa(java.lang.String obsAfaAfa) {
        this.obsAfaAfa = obsAfaAfa;
    }


    /**
     * Gets the obsFimHeb value for this FichaBasicaFichaBasica5In.
     * 
     * @return obsFimHeb
     */
    public java.lang.String getObsFimHeb() {
        return obsFimHeb;
    }


    /**
     * Sets the obsFimHeb value for this FichaBasicaFichaBasica5In.
     * 
     * @param obsFimHeb
     */
    public void setObsFimHeb(java.lang.String obsFimHeb) {
        this.obsFimHeb = obsFimHeb;
    }


    /**
     * Gets the obsIniHeb value for this FichaBasicaFichaBasica5In.
     * 
     * @return obsIniHeb
     */
    public java.lang.String getObsIniHeb() {
        return obsIniHeb;
    }


    /**
     * Sets the obsIniHeb value for this FichaBasicaFichaBasica5In.
     * 
     * @param obsIniHeb
     */
    public void setObsIniHeb(java.lang.String obsIniHeb) {
        this.obsIniHeb = obsIniHeb;
    }


    /**
     * Gets the onuSce value for this FichaBasicaFichaBasica5In.
     * 
     * @return onuSce
     */
    public java.lang.Integer getOnuSce() {
        return onuSce;
    }


    /**
     * Sets the onuSce value for this FichaBasicaFichaBasica5In.
     * 
     * @param onuSce
     */
    public void setOnuSce(java.lang.Integer onuSce) {
        this.onuSce = onuSce;
    }


    /**
     * Gets the opcCes value for this FichaBasicaFichaBasica5In.
     * 
     * @return opcCes
     */
    public java.lang.String getOpcCes() {
        return opcCes;
    }


    /**
     * Sets the opcCes value for this FichaBasicaFichaBasica5In.
     * 
     * @param opcCes
     */
    public void setOpcCes(java.lang.String opcCes) {
        this.opcCes = opcCes;
    }


    /**
     * Gets the orgClaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return orgClaAfa
     */
    public java.lang.Integer getOrgClaAfa() {
        return orgClaAfa;
    }


    /**
     * Sets the orgClaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param orgClaAfa
     */
    public void setOrgClaAfa(java.lang.Integer orgClaAfa) {
        this.orgClaAfa = orgClaAfa;
    }


    /**
     * Gets the pagSin value for this FichaBasicaFichaBasica5In.
     * 
     * @return pagSin
     */
    public java.lang.String getPagSin() {
        return pagSin;
    }


    /**
     * Sets the pagSin value for this FichaBasicaFichaBasica5In.
     * 
     * @param pagSin
     */
    public void setPagSin(java.lang.String pagSin) {
        this.pagSin = pagSin;
    }


    /**
     * Gets the perJur value for this FichaBasicaFichaBasica5In.
     * 
     * @return perJur
     */
    public java.lang.Integer getPerJur() {
        return perJur;
    }


    /**
     * Sets the perJur value for this FichaBasicaFichaBasica5In.
     * 
     * @param perJur
     */
    public void setPerJur(java.lang.Integer perJur) {
        this.perJur = perJur;
    }


    /**
     * Gets the perPag value for this FichaBasicaFichaBasica5In.
     * 
     * @return perPag
     */
    public java.lang.String getPerPag() {
        return perPag;
    }


    /**
     * Sets the perPag value for this FichaBasicaFichaBasica5In.
     * 
     * @param perPag
     */
    public void setPerPag(java.lang.String perPag) {
        this.perPag = perPag;
    }


    /**
     * Gets the ponEmb value for this FichaBasicaFichaBasica5In.
     * 
     * @return ponEmb
     */
    public java.lang.Integer getPonEmb() {
        return ponEmb;
    }


    /**
     * Sets the ponEmb value for this FichaBasicaFichaBasica5In.
     * 
     * @param ponEmb
     */
    public void setPonEmb(java.lang.Integer ponEmb) {
        this.ponEmb = ponEmb;
    }


    /**
     * Gets the posObs value for this FichaBasicaFichaBasica5In.
     * 
     * @return posObs
     */
    public java.lang.String getPosObs() {
        return posObs;
    }


    /**
     * Sets the posObs value for this FichaBasicaFichaBasica5In.
     * 
     * @param posObs
     */
    public void setPosObs(java.lang.String posObs) {
        this.posObs = posObs;
    }


    /**
     * Gets the posTra value for this FichaBasicaFichaBasica5In.
     * 
     * @return posTra
     */
    public java.lang.String getPosTra() {
        return posTra;
    }


    /**
     * Sets the posTra value for this FichaBasicaFichaBasica5In.
     * 
     * @param posTra
     */
    public void setPosTra(java.lang.String posTra) {
        this.posTra = posTra;
    }


    /**
     * Gets the prvTerAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return prvTerAfa
     */
    public java.lang.String getPrvTerAfa() {
        return prvTerAfa;
    }


    /**
     * Sets the prvTerAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param prvTerAfa
     */
    public void setPrvTerAfa(java.lang.String prvTerAfa) {
        this.prvTerAfa = prvTerAfa;
    }


    /**
     * Gets the qhrAfaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return qhrAfaAfa
     */
    public java.lang.Integer getQhrAfaAfa() {
        return qhrAfaAfa;
    }


    /**
     * Sets the qhrAfaAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param qhrAfaAfa
     */
    public void setQhrAfaAfa(java.lang.Integer qhrAfaAfa) {
        this.qhrAfaAfa = qhrAfaAfa;
    }


    /**
     * Gets the racCor value for this FichaBasicaFichaBasica5In.
     * 
     * @return racCor
     */
    public java.lang.Integer getRacCor() {
        return racCor;
    }


    /**
     * Sets the racCor value for this FichaBasicaFichaBasica5In.
     * 
     * @param racCor
     */
    public void setRacCor(java.lang.Integer racCor) {
        this.racCor = racCor;
    }


    /**
     * Gets the ratEve value for this FichaBasicaFichaBasica5In.
     * 
     * @return ratEve
     */
    public java.lang.String getRatEve() {
        return ratEve;
    }


    /**
     * Sets the ratEve value for this FichaBasicaFichaBasica5In.
     * 
     * @param ratEve
     */
    public void setRatEve(java.lang.String ratEve) {
        this.ratEve = ratEve;
    }


    /**
     * Gets the reaRes value for this FichaBasicaFichaBasica5In.
     * 
     * @return reaRes
     */
    public java.lang.String getReaRes() {
        return reaRes;
    }


    /**
     * Sets the reaRes value for this FichaBasicaFichaBasica5In.
     * 
     * @param reaRes
     */
    public void setReaRes(java.lang.String reaRes) {
        this.reaRes = reaRes;
    }


    /**
     * Gets the rec13S value for this FichaBasicaFichaBasica5In.
     * 
     * @return rec13S
     */
    public java.lang.String getRec13S() {
        return rec13S;
    }


    /**
     * Sets the rec13S value for this FichaBasicaFichaBasica5In.
     * 
     * @param rec13S
     */
    public void setRec13S(java.lang.String rec13S) {
        this.rec13S = rec13S;
    }


    /**
     * Gets the recAdi value for this FichaBasicaFichaBasica5In.
     * 
     * @return recAdi
     */
    public java.lang.String getRecAdi() {
        return recAdi;
    }


    /**
     * Sets the recAdi value for this FichaBasicaFichaBasica5In.
     * 
     * @param recAdi
     */
    public void setRecAdi(java.lang.String recAdi) {
        this.recAdi = recAdi;
    }


    /**
     * Gets the recGra value for this FichaBasicaFichaBasica5In.
     * 
     * @return recGra
     */
    public java.lang.String getRecGra() {
        return recGra;
    }


    /**
     * Sets the recGra value for this FichaBasicaFichaBasica5In.
     * 
     * @param recGra
     */
    public void setRecGra(java.lang.String recGra) {
        this.recGra = recGra;
    }


    /**
     * Gets the regConAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return regConAfa
     */
    public java.lang.String getRegConAfa() {
        return regConAfa;
    }


    /**
     * Sets the regConAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param regConAfa
     */
    public void setRegConAfa(java.lang.String regConAfa) {
        this.regConAfa = regConAfa;
    }


    /**
     * Gets the resOnu value for this FichaBasicaFichaBasica5In.
     * 
     * @return resOnu
     */
    public java.lang.String getResOnu() {
        return resOnu;
    }


    /**
     * Sets the resOnu value for this FichaBasicaFichaBasica5In.
     * 
     * @param resOnu
     */
    public void setResOnu(java.lang.String resOnu) {
        this.resOnu = resOnu;
    }


    /**
     * Gets the salEstHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return salEstHsa
     */
    public java.lang.Double getSalEstHsa() {
        return salEstHsa;
    }


    /**
     * Sets the salEstHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param salEstHsa
     */
    public void setSalEstHsa(java.lang.Double salEstHsa) {
        this.salEstHsa = salEstHsa;
    }


    /**
     * Gets the serCtp value for this FichaBasicaFichaBasica5In.
     * 
     * @return serCtp
     */
    public java.lang.String getSerCtp() {
        return serCtp;
    }


    /**
     * Sets the serCtp value for this FichaBasicaFichaBasica5In.
     * 
     * @param serCtp
     */
    public void setSerCtp(java.lang.String serCtp) {
        this.serCtp = serCtp;
    }


    /**
     * Gets the sisCes value for this FichaBasicaFichaBasica5In.
     * 
     * @return sisCes
     */
    public java.lang.Integer getSisCes() {
        return sisCes;
    }


    /**
     * Sets the sisCes value for this FichaBasicaFichaBasica5In.
     * 
     * @param sisCes
     */
    public void setSisCes(java.lang.Integer sisCes) {
        this.sisCes = sisCes;
    }


    /**
     * Gets the sitAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @return sitAfa
     */
    public java.lang.Integer getSitAfa() {
        return sitAfa;
    }


    /**
     * Sets the sitAfa value for this FichaBasicaFichaBasica5In.
     * 
     * @param sitAfa
     */
    public void setSitAfa(java.lang.Integer sitAfa) {
        this.sitAfa = sitAfa;
    }


    /**
     * Gets the socSinHsi value for this FichaBasicaFichaBasica5In.
     * 
     * @return socSinHsi
     */
    public java.lang.String getSocSinHsi() {
        return socSinHsi;
    }


    /**
     * Sets the socSinHsi value for this FichaBasicaFichaBasica5In.
     * 
     * @param socSinHsi
     */
    public void setSocSinHsi(java.lang.String socSinHsi) {
        this.socSinHsi = socSinHsi;
    }


    /**
     * Gets the tipAdmHfi value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipAdmHfi
     */
    public java.lang.Integer getTipAdmHfi() {
        return tipAdmHfi;
    }


    /**
     * Sets the tipAdmHfi value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipAdmHfi
     */
    public void setTipAdmHfi(java.lang.Integer tipAdmHfi) {
        this.tipAdmHfi = tipAdmHfi;
    }
    
    /**
     * Gets the tipAdm value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipAdm
     */
    public java.lang.Integer getTipAdm() {
        return tipAdm;
    }


    /**
     * Sets the tipAdmHfi value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipAdmHfi
     */
    public void setTipAdm(java.lang.Integer tipAdm) {
        this.tipAdm = tipAdm;
    }


    /**
     * Gets the tipApo value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipApo
     */
    public java.lang.Integer getTipApo() {
        return tipApo;
    }


    /**
     * Sets the tipApo value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipApo
     */
    public void setTipApo(java.lang.Integer tipApo) {
        this.tipApo = tipApo;
    }


    /**
     * Gets the tipCon value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipCon
     */
    public java.lang.Integer getTipCon() {
        return tipCon;
    }


    /**
     * Sets the tipCon value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipCon
     */
    public void setTipCon(java.lang.Integer tipCon) {
        this.tipCon = tipCon;
    }


    /**
     * Gets the tipOpc value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipOpc
     */
    public java.lang.String getTipOpc() {
        return tipOpc;
    }


    /**
     * Sets the tipOpc value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipOpc
     */
    public void setTipOpc(java.lang.String tipOpc) {
        this.tipOpc = tipOpc;
    }


    /**
     * Gets the tipOpe value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the tipSal value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipSal
     */
    public java.lang.Integer getTipSal() {
        return tipSal;
    }


    /**
     * Sets the tipSal value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipSal
     */
    public void setTipSal(java.lang.Integer tipSal) {
        this.tipSal = tipSal;
    }


    /**
     * Gets the tipSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipSalHsa
     */
    public java.lang.Integer getTipSalHsa() {
        return tipSalHsa;
    }


    /**
     * Sets the tipSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipSalHsa
     */
    public void setTipSalHsa(java.lang.Integer tipSalHsa) {
        this.tipSalHsa = tipSalHsa;
    }


    /**
     * Gets the tipSex value for this FichaBasicaFichaBasica5In.
     * 
     * @return tipSex
     */
    public java.lang.String getTipSex() {
        return tipSex;
    }


    /**
     * Sets the tipSex value for this FichaBasicaFichaBasica5In.
     * 
     * @param tipSex
     */
    public void setTipSex(java.lang.String tipSex) {
        this.tipSex = tipSex;
    }


    /**
     * Gets the tpCtBa value for this FichaBasicaFichaBasica5In.
     * 
     * @return tpCtBa
     */
    public java.lang.Integer getTpCtBa() {
        return tpCtBa;
    }


    /**
     * Sets the tpCtBa value for this FichaBasicaFichaBasica5In.
     * 
     * @param tpCtBa
     */
    public void setTpCtBa(java.lang.Integer tpCtBa) {
        this.tpCtBa = tpCtBa;
    }


    /**
     * Gets the turInt value for this FichaBasicaFichaBasica5In.
     * 
     * @return turInt
     */
    public java.lang.Integer getTurInt() {
        return turInt;
    }


    /**
     * Sets the turInt value for this FichaBasicaFichaBasica5In.
     * 
     * @param turInt
     */
    public void setTurInt(java.lang.Integer turInt) {
        this.turInt = turInt;
    }
    
    /**
     * Gets the valSal value for this FichaBasicaFichaBasica5In.
     * 
     * @return valSal
     */
    public java.lang.Double getValSal() {
        return valSal;
    }


    /**
     * Sets the valSal value for this FichaBasicaFichaBasica5In.
     * 
     * @param valSal
     */
    public void setValSal(java.lang.Double valSal) {
        this.valSal = valSal;
    }


    /**
     * Gets the valSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @return valSalHsa
     */
    public java.lang.Double getValSalHsa() {
        return valSalHsa;
    }


    /**
     * Sets the valSalHsa value for this FichaBasicaFichaBasica5In.
     * 
     * @param valSalHsa
     */
    public void setValSalHsa(java.lang.Double valSalHsa) {
        this.valSalHsa = valSalHsa;
    }


    /**
     * Gets the verInt value for this FichaBasicaFichaBasica5In.
     * 
     * @return verInt
     */
    public java.lang.String getVerInt() {
        return verInt;
    }


    /**
     * Sets the verInt value for this FichaBasicaFichaBasica5In.
     * 
     * @param verInt
     */
    public void setVerInt(java.lang.String verInt) {
        this.verInt = verInt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaFichaBasica5In)) return false;
        FichaBasicaFichaBasica5In other = (FichaBasicaFichaBasica5In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.aciTraAfa==null && other.getAciTraAfa()==null) || 
             (this.aciTraAfa!=null &&
              this.aciTraAfa.equals(other.getAciTraAfa()))) &&
            ((this.uSU_TipAdm==null && other.getUSU_TipAdm()==null) || 
             (this.uSU_TipAdm!=null &&
              this.uSU_TipAdm.equals(other.getUSU_TipAdm()))) &&
            ((this.uSU_ColSde==null && other.getUSU_ColSde()==null) || 
             (this.uSU_ColSde!=null &&
              this.uSU_ColSde.equals(other.getUSU_ColSde()))) &&
            ((this.uSU_CsgBrc==null && other.getUSU_CsgBrc()==null) || 
             (this.uSU_CsgBrc!=null &&
              this.uSU_CsgBrc.equals(other.getUSU_CsgBrc()))) &&
            ((this.uSU_ValCom==null && other.getUSU_ValCom()==null) || 
             (this.uSU_ValCom!=null &&
              this.uSU_ValCom.equals(other.getUSU_ValCom()))) &&
            ((this.uSU_DepEmp==null && other.getUSU_DepEmp()==null) || 
             (this.uSU_DepEmp!=null &&
              this.uSU_DepEmp.equals(other.getUSU_DepEmp()))) &&
           ((this.uSU_FisRes==null && other.getUSU_FisRes()==null) || 
            (this.uSU_FisRes!=null &&
             this.uSU_FisRes.equals(other.getUSU_FisRes()))) &&  
            ((this.admAnt==null && other.getAdmAnt()==null) || 
             (this.admAnt!=null &&
              this.admAnt.equals(other.getAdmAnt()))) &&
            ((this.admeSo==null && other.getAdmeSo()==null) || 
             (this.admeSo!=null &&
              this.admeSo.equals(other.getAdmeSo()))) &&
            ((this.apeFun==null && other.getApeFun()==null) || 
             (this.apeFun!=null &&
              this.apeFun.equals(other.getApeFun()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.apuPon==null && other.getApuPon()==null) || 
             (this.apuPon!=null &&
              this.apuPon.equals(other.getApuPon()))) &&
            ((this.apuPonApu==null && other.getApuPonApu()==null) || 
             (this.apuPonApu!=null &&
              this.apuPonApu.equals(other.getApuPonApu()))) &&
            ((this.assPpr==null && other.getAssPpr()==null) || 
             (this.assPpr!=null &&
              this.assPpr.equals(other.getAssPpr()))) &&
            ((this.benRea==null && other.getBenRea()==null) || 
             (this.benRea!=null &&
              this.benRea.equals(other.getBenRea()))) &&
            ((this.busHor==null && other.getBusHor()==null) || 
             (this.busHor!=null &&
              this.busHor.equals(other.getBusHor()))) &&
            ((this.cadFol==null && other.getCadFol()==null) || 
             (this.cadFol!=null &&
              this.cadFol.equals(other.getCadFol()))) &&
            ((this.carVagHca==null && other.getCarVagHca()==null) || 
             (this.carVagHca!=null &&
              this.carVagHca.equals(other.getCarVagHca()))) &&
            ((this.catAnt==null && other.getCatAnt()==null) || 
             (this.catAnt!=null &&
              this.catAnt.equals(other.getCatAnt()))) &&
            ((this.catSef==null && other.getCatSef()==null) || 
             (this.catSef!=null &&
              this.catSef.equals(other.getCatSef()))) &&
            ((this.cateSo==null && other.getCateSo()==null) || 
             (this.cateSo!=null &&
              this.cateSo.equals(other.getCateSo()))) &&
            ((this.cauDemAfa==null && other.getCauDemAfa()==null) || 
             (this.cauDemAfa!=null &&
              this.cauDemAfa.equals(other.getCauDemAfa()))) &&
            ((this.claSalHsa==null && other.getClaSalHsa()==null) || 
             (this.claSalHsa!=null &&
              this.claSalHsa.equals(other.getClaSalHsa()))) &&
            ((this.cnpjAn==null && other.getCnpjAn()==null) || 
             (this.cnpjAn!=null &&
              this.cnpjAn.equals(other.getCnpjAn()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codAteAfa==null && other.getCodAteAfa()==null) || 
             (this.codAteAfa!=null &&
              this.codAteAfa.equals(other.getCodAteAfa()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codCar==null && other.getCodCar()==null) || 
             (this.codCar!=null &&
              this.codCar.equals(other.getCodCar()))) &&
            ((this.codCat==null && other.getCodCat()==null) || 
             (this.codCat!=null &&
              this.codCat.equals(other.getCodCat()))) &&
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codCha==null && other.getCodCha()==null) || 
             (this.codCha!=null &&
              this.codCha.equals(other.getCodCha()))) &&
            ((this.codDef==null && other.getCodDef()==null) || 
             (this.codDef!=null &&
              this.codDef.equals(other.getCodDef()))) &&
            ((this.codDoeAfa==null && other.getCodDoeAfa()==null) || 
             (this.codDoeAfa!=null &&
              this.codDoeAfa.equals(other.getCodDoeAfa()))) &&
            ((this.codEqp==null && other.getCodEqp()==null) || 
             (this.codEqp!=null &&
              this.codEqp.equals(other.getCodEqp()))) &&
            ((this.codEsc==null && other.getCodEsc()==null) || 
             (this.codEsc!=null &&
              this.codEsc.equals(other.getCodEsc()))) &&
            ((this.codEstHsa==null && other.getCodEstHsa()==null) || 
             (this.codEstHsa!=null &&
              this.codEstHsa.equals(other.getCodEstHsa()))) &&
            ((this.codEtb==null && other.getCodEtb()==null) || 
             (this.codEtb!=null &&
              this.codEtb.equals(other.getCodEtb()))) &&
            ((this.codFicFmd==null && other.getCodFicFmd()==null) || 
             (this.codFicFmd!=null &&
              this.codFicFmd.equals(other.getCodFicFmd()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codIdn==null && other.getCodIdn()==null) || 
             (this.codIdn!=null &&
              this.codIdn.equals(other.getCodIdn()))) &&
            ((this.codMotHca==null && other.getCodMotHca()==null) || 
             (this.codMotHca!=null &&
              this.codMotHca.equals(other.getCodMotHca()))) &&
            ((this.codMotHsa==null && other.getCodMotHsa()==null) || 
             (this.codMotHsa!=null &&
              this.codMotHsa.equals(other.getCodMotHsa()))) &&
            ((this.codNac==null && other.getCodNac()==null) || 
             (this.codNac!=null &&
              this.codNac.equals(other.getCodNac()))) &&
            ((this.codPro==null && other.getCodPro()==null) || 
             (this.codPro!=null &&
              this.codPro.equals(other.getCodPro()))) &&
            ((this.codSin==null && other.getCodSin()==null) || 
             (this.codSin!=null &&
              this.codSin.equals(other.getCodSin()))) &&
            ((this.codSinHsi==null && other.getCodSinHsi()==null) || 
             (this.codSinHsi!=null &&
              this.codSinHsi.equals(other.getCodSinHsi()))) &&
            ((this.codTap==null && other.getCodTap()==null) || 
             (this.codTap!=null &&
              this.codTap.equals(other.getCodTap()))) &&
            ((this.codTma==null && other.getCodTma()==null) || 
             (this.codTma!=null &&
              this.codTma.equals(other.getCodTma()))) &&
            ((this.codTmaHes==null && other.getCodTmaHes()==null) || 
             (this.codTmaHes!=null &&
              this.codTmaHes.equals(other.getCodTmaHes()))) &&
            ((this.codVinHvi==null && other.getCodVinHvi()==null) || 
             (this.codVinHvi!=null &&
              this.codVinHvi.equals(other.getCodVinHvi()))) &&
            ((this.conBan==null && other.getConBan()==null) || 
             (this.conBan!=null &&
              this.conBan.equals(other.getConBan()))) &&
            ((this.conFgt==null && other.getConFgt()==null) || 
             (this.conFgt!=null &&
              this.conFgt.equals(other.getConFgt()))) &&
            ((this.conFinCcu==null && other.getConFinCcu()==null) || 
             (this.conFinCcu!=null &&
              this.conFinCcu.equals(other.getConFinCcu()))) &&
            ((this.conRho==null && other.getConRho()==null) || 
             (this.conRho!=null &&
              this.conRho.equals(other.getConRho()))) &&
            ((this.conTosHlo==null && other.getConTosHlo()==null) || 
             (this.conTosHlo!=null &&
              this.conTosHlo.equals(other.getConTosHlo()))) &&
            ((this.conTovAfa==null && other.getConTovAfa()==null) || 
             (this.conTovAfa!=null &&
              this.conTovAfa.equals(other.getConTovAfa()))) &&
            ((this.conTovHfi==null && other.getConTovHfi()==null) || 
             (this.conTovHfi!=null &&
              this.conTovHfi.equals(other.getConTovHfi()))) &&
            ((this.conTovHlo==null && other.getConTovHlo()==null) || 
             (this.conTovHlo!=null &&
              this.conTovHlo.equals(other.getConTovHlo()))) &&
            ((this.cotDef==null && other.getCotDef()==null) || 
             (this.cotDef!=null &&
              this.cotDef.equals(other.getCotDef()))) &&
            ((this.cplEstHsa==null && other.getCplEstHsa()==null) || 
             (this.cplEstHsa!=null &&
              this.cplEstHsa.equals(other.getCplEstHsa()))) &&
            ((this.cplSalHsa==null && other.getCplSalHsa()==null) || 
             (this.cplSalHsa!=null &&
              this.cplSalHsa.equals(other.getCplSalHsa()))) &&
            ((this.datAdm==null && other.getDatAdm()==null) || 
             (this.datAdm!=null &&
              this.datAdm.equals(other.getDatAdm()))) &&
            ((this.datAfaAfa==null && other.getDatAfaAfa()==null) || 
             (this.datAfaAfa!=null &&
              this.datAfaAfa.equals(other.getDatAfaAfa()))) &&
            ((this.datAltCcu==null && other.getDatAltCcu()==null) || 
             (this.datAltCcu!=null &&
              this.datAltCcu.equals(other.getDatAltCcu()))) &&
            ((this.datAltHca==null && other.getDatAltHca()==null) || 
             (this.datAltHca!=null &&
              this.datAltHca.equals(other.getDatAltHca()))) &&
            ((this.datAltHcs==null && other.getDatAltHcs()==null) || 
             (this.datAltHcs!=null &&
              this.datAltHcs.equals(other.getDatAltHcs()))) &&
            ((this.datAltHes==null && other.getDatAltHes()==null) || 
             (this.datAltHes!=null &&
              this.datAltHes.equals(other.getDatAltHes()))) &&
            ((this.datAltHfi==null && other.getDatAltHfi()==null) || 
             (this.datAltHfi!=null &&
              this.datAltHfi.equals(other.getDatAltHfi()))) &&
            ((this.datAltHlo==null && other.getDatAltHlo()==null) || 
             (this.datAltHlo!=null &&
              this.datAltHlo.equals(other.getDatAltHlo()))) &&
            ((this.datAltHor==null && other.getDatAltHor()==null) || 
             (this.datAltHor!=null &&
              this.datAltHor.equals(other.getDatAltHor()))) &&
            ((this.datApo==null && other.getDatApo()==null) || 
             (this.datApo!=null &&
              this.datApo.equals(other.getDatApo()))) &&
            ((this.datInc==null && other.getDatInc()==null) || 
             (this.datInc!=null &&
              this.datInc.equals(other.getDatInc()))) &&
            ((this.datNas==null && other.getDatNas()==null) || 
             (this.datNas!=null &&
              this.datNas.equals(other.getDatNas()))) &&
            ((this.datOpc==null && other.getDatOpc()==null) || 
             (this.datOpc!=null &&
              this.datOpc.equals(other.getDatOpc()))) &&
            ((this.datTerAfa==null && other.getDatTerAfa()==null) || 
             (this.datTerAfa!=null &&
              this.datTerAfa.equals(other.getDatTerAfa()))) &&
            ((this.dcdPis==null && other.getDcdPis()==null) || 
             (this.dcdPis!=null &&
              this.dcdPis.equals(other.getDcdPis()))) &&
            ((this.defFis==null && other.getDefFis()==null) || 
             (this.defFis!=null &&
              this.defFis.equals(other.getDefFis()))) &&
            ((this.depIrf==null && other.getDepIrf()==null) || 
             (this.depIrf!=null &&
              this.depIrf.equals(other.getDepIrf()))) &&
            ((this.depSaf==null && other.getDepSaf()==null) || 
             (this.depSaf!=null &&
              this.depSaf.equals(other.getDepSaf()))) &&
            ((this.dexCtp==null && other.getDexCtp()==null) || 
             (this.dexCtp!=null &&
              this.dexCtp.equals(other.getDexCtp()))) &&
            ((this.diaJusAfa==null && other.getDiaJusAfa()==null) || 
             (this.diaJusAfa!=null &&
              this.diaJusAfa.equals(other.getDiaJusAfa()))) &&
            ((this.digBan==null && other.getDigBan()==null) || 
             (this.digBan!=null &&
              this.digBan.equals(other.getDigBan()))) &&
            ((this.digCar==null && other.getDigCar()==null) || 
             (this.digCar!=null &&
              this.digCar.equals(other.getDigCar()))) &&
            ((this.docEst==null && other.getDocEst()==null) || 
             (this.docEst!=null &&
              this.docEst.equals(other.getDocEst()))) &&
            ((this.emiCar==null && other.getEmiCar()==null) || 
             (this.emiCar!=null &&
              this.emiCar.equals(other.getEmiCar()))) &&
            ((this.escVtr==null && other.getEscVtr()==null) || 
             (this.escVtr!=null &&
              this.escVtr.equals(other.getEscVtr()))) &&
            ((this.estCiv==null && other.getEstCiv()==null) || 
             (this.estCiv!=null &&
              this.estCiv.equals(other.getEstCiv()))) &&
            ((this.estConAfa==null && other.getEstConAfa()==null) || 
             (this.estConAfa!=null &&
              this.estConAfa.equals(other.getEstConAfa()))) &&
            ((this.estCtp==null && other.getEstCtp()==null) || 
             (this.estCtp!=null &&
              this.estCtp.equals(other.getEstCtp()))) &&
            ((this.exmRetAfa==null && other.getExmRetAfa()==null) || 
             (this.exmRetAfa!=null &&
              this.exmRetAfa.equals(other.getExmRetAfa()))) &&
            ((this.fimEtbHeb==null && other.getFimEtbHeb()==null) || 
             (this.fimEtbHeb!=null &&
              this.fimEtbHeb.equals(other.getFimEtbHeb()))) &&
            ((this.fimEvt==null && other.getFimEvt()==null) || 
             (this.fimEvt!=null &&
              this.fimEvt.equals(other.getFimEvt()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.graIns==null && other.getGraIns()==null) || 
             (this.graIns!=null &&
              this.graIns.equals(other.getGraIns()))) &&
            ((this.horAfaAfa==null && other.getHorAfaAfa()==null) || 
             (this.horAfaAfa!=null &&
              this.horAfaAfa.equals(other.getHorAfaAfa()))) &&
            ((this.horBas==null && other.getHorBas()==null) || 
             (this.horBas!=null &&
              this.horBas.equals(other.getHorBas()))) &&
            ((this.horDsrHor==null && other.getHorDsrHor()==null) || 
             (this.horDsrHor!=null &&
              this.horDsrHor.equals(other.getHorDsrHor()))) &&
            ((this.horInc==null && other.getHorInc()==null) || 
             (this.horInc!=null &&
              this.horInc.equals(other.getHorInc()))) &&
            ((this.horSabHor==null && other.getHorSabHor()==null) || 
             (this.horSabHor!=null &&
              this.horSabHor.equals(other.getHorSabHor()))) &&
            ((this.horSemHor==null && other.getHorSemHor()==null) || 
             (this.horSemHor!=null &&
              this.horSemHor.equals(other.getHorSemHor()))) &&
            ((this.horTerAfa==null && other.getHorTerAfa()==null) || 
             (this.horTerAfa!=null &&
              this.horTerAfa.equals(other.getHorTerAfa()))) &&
            ((this.indAdm==null && other.getIndAdm()==null) || 
             (this.indAdm!=null &&
              this.indAdm.equals(other.getIndAdm()))) &&
            ((this.iniAtu==null && other.getIniAtu()==null) || 
             (this.iniAtu!=null &&
              this.iniAtu.equals(other.getIniAtu()))) &&
            ((this.iniEtbHeb==null && other.getIniEtbHeb()==null) || 
             (this.iniEtbHeb!=null &&
              this.iniEtbHeb.equals(other.getIniEtbHeb()))) &&
            ((this.iniEvt==null && other.getIniEvt()==null) || 
             (this.iniEvt!=null &&
              this.iniEvt.equals(other.getIniEvt()))) &&
            ((this.insCur==null && other.getInsCur()==null) || 
             (this.insCur!=null &&
              this.insCur.equals(other.getInsCur()))) &&
            ((this.irrIse==null && other.getIrrIse()==null) || 
             (this.irrIse!=null &&
              this.irrIse.equals(other.getIrrIse()))) &&
            ((this.lisRai==null && other.getLisRai()==null) || 
             (this.lisRai!=null &&
              this.lisRai.equals(other.getLisRai()))) &&
            ((this.locTraHlo==null && other.getLocTraHlo()==null) || 
             (this.locTraHlo!=null &&
              this.locTraHlo.equals(other.getLocTraHlo()))) &&
            ((this.matAnt==null && other.getMatAnt()==null) || 
             (this.matAnt!=null &&
              this.matAnt.equals(other.getMatAnt()))) &&
            ((this.modPag==null && other.getModPag()==null) || 
             (this.modPag!=null &&
              this.modPag.equals(other.getModPag()))) &&
            ((this.moeEstHsa==null && other.getMoeEstHsa()==null) || 
             (this.moeEstHsa!=null &&
              this.moeEstHsa.equals(other.getMoeEstHsa()))) &&
            ((this.motPos==null && other.getMotPos()==null) || 
             (this.motPos!=null &&
              this.motPos.equals(other.getMotPos()))) &&
            ((this.movSef==null && other.getMovSef()==null) || 
             (this.movSef!=null &&
              this.movSef.equals(other.getMovSef()))) &&
            ((this.natDesHna==null && other.getNatDesHna()==null) || 
             (this.natDesHna!=null &&
              this.natDesHna.equals(other.getNatDesHna()))) &&
            ((this.nivSalHsa==null && other.getNivSalHsa()==null) || 
             (this.nivSalHsa!=null &&
              this.nivSalHsa.equals(other.getNivSalHsa()))) &&
            ((this.nomAteAfa==null && other.getNomAteAfa()==null) || 
             (this.nomAteAfa!=null &&
              this.nomAteAfa.equals(other.getNomAteAfa()))) &&
            ((this.nomFun==null && other.getNomFun()==null) || 
             (this.nomFun!=null &&
              this.nomFun.equals(other.getNomFun()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numCpf==null && other.getNumCpf()==null) || 
             (this.numCpf!=null &&
              this.numCpf.equals(other.getNumCpf()))) &&
            ((this.numCtp==null && other.getNumCtp()==null) || 
             (this.numCtp!=null &&
              this.numCtp.equals(other.getNumCtp()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.numLoc==null && other.getNumLoc()==null) || 
             (this.numLoc!=null &&
              this.numLoc.equals(other.getNumLoc()))) &&
            ((this.numPis==null && other.getNumPis()==null) || 
             (this.numPis!=null &&
              this.numPis.equals(other.getNumPis()))) &&
            ((this.obsAfaAfa==null && other.getObsAfaAfa()==null) || 
             (this.obsAfaAfa!=null &&
              this.obsAfaAfa.equals(other.getObsAfaAfa()))) &&
            ((this.obsFimHeb==null && other.getObsFimHeb()==null) || 
             (this.obsFimHeb!=null &&
              this.obsFimHeb.equals(other.getObsFimHeb()))) &&
            ((this.obsIniHeb==null && other.getObsIniHeb()==null) || 
             (this.obsIniHeb!=null &&
              this.obsIniHeb.equals(other.getObsIniHeb()))) &&
            ((this.onuSce==null && other.getOnuSce()==null) || 
             (this.onuSce!=null &&
              this.onuSce.equals(other.getOnuSce()))) &&
            ((this.opcCes==null && other.getOpcCes()==null) || 
             (this.opcCes!=null &&
              this.opcCes.equals(other.getOpcCes()))) &&
            ((this.orgClaAfa==null && other.getOrgClaAfa()==null) || 
             (this.orgClaAfa!=null &&
              this.orgClaAfa.equals(other.getOrgClaAfa()))) &&
            ((this.pagSin==null && other.getPagSin()==null) || 
             (this.pagSin!=null &&
              this.pagSin.equals(other.getPagSin()))) &&
            ((this.perJur==null && other.getPerJur()==null) || 
             (this.perJur!=null &&
              this.perJur.equals(other.getPerJur()))) &&
            ((this.perPag==null && other.getPerPag()==null) || 
             (this.perPag!=null &&
              this.perPag.equals(other.getPerPag()))) &&
            ((this.ponEmb==null && other.getPonEmb()==null) || 
             (this.ponEmb!=null &&
              this.ponEmb.equals(other.getPonEmb()))) &&
            ((this.posObs==null && other.getPosObs()==null) || 
             (this.posObs!=null &&
              this.posObs.equals(other.getPosObs()))) &&
            ((this.posTra==null && other.getPosTra()==null) || 
             (this.posTra!=null &&
              this.posTra.equals(other.getPosTra()))) &&
            ((this.prvTerAfa==null && other.getPrvTerAfa()==null) || 
             (this.prvTerAfa!=null &&
              this.prvTerAfa.equals(other.getPrvTerAfa()))) &&
            ((this.qhrAfaAfa==null && other.getQhrAfaAfa()==null) || 
             (this.qhrAfaAfa!=null &&
              this.qhrAfaAfa.equals(other.getQhrAfaAfa()))) &&
            ((this.racCor==null && other.getRacCor()==null) || 
             (this.racCor!=null &&
              this.racCor.equals(other.getRacCor()))) &&
            ((this.ratEve==null && other.getRatEve()==null) || 
             (this.ratEve!=null &&
              this.ratEve.equals(other.getRatEve()))) &&
            ((this.reaRes==null && other.getReaRes()==null) || 
             (this.reaRes!=null &&
              this.reaRes.equals(other.getReaRes()))) &&
            ((this.rec13S==null && other.getRec13S()==null) || 
             (this.rec13S!=null &&
              this.rec13S.equals(other.getRec13S()))) &&
            ((this.recAdi==null && other.getRecAdi()==null) || 
             (this.recAdi!=null &&
              this.recAdi.equals(other.getRecAdi()))) &&
            ((this.recGra==null && other.getRecGra()==null) || 
             (this.recGra!=null &&
              this.recGra.equals(other.getRecGra()))) &&
            ((this.regConAfa==null && other.getRegConAfa()==null) || 
             (this.regConAfa!=null &&
              this.regConAfa.equals(other.getRegConAfa()))) &&
            ((this.resOnu==null && other.getResOnu()==null) || 
             (this.resOnu!=null &&
              this.resOnu.equals(other.getResOnu()))) &&
            ((this.salEstHsa==null && other.getSalEstHsa()==null) || 
             (this.salEstHsa!=null &&
              this.salEstHsa.equals(other.getSalEstHsa()))) &&
            ((this.serCtp==null && other.getSerCtp()==null) || 
             (this.serCtp!=null &&
              this.serCtp.equals(other.getSerCtp()))) &&
            ((this.sisCes==null && other.getSisCes()==null) || 
             (this.sisCes!=null &&
              this.sisCes.equals(other.getSisCes()))) &&
            ((this.sitAfa==null && other.getSitAfa()==null) || 
             (this.sitAfa!=null &&
              this.sitAfa.equals(other.getSitAfa()))) &&
            ((this.socSinHsi==null && other.getSocSinHsi()==null) || 
             (this.socSinHsi!=null &&
              this.socSinHsi.equals(other.getSocSinHsi()))) &&
            ((this.tipAdmHfi==null && other.getTipAdmHfi()==null) || 
             (this.tipAdmHfi!=null &&
              this.tipAdmHfi.equals(other.getTipAdmHfi()))) &&
            ((this.tipApo==null && other.getTipApo()==null) || 
             (this.tipApo!=null &&
              this.tipApo.equals(other.getTipApo()))) &&
            ((this.tipCon==null && other.getTipCon()==null) || 
             (this.tipCon!=null &&
              this.tipCon.equals(other.getTipCon()))) &&
            ((this.tipOpc==null && other.getTipOpc()==null) || 
             (this.tipOpc!=null &&
              this.tipOpc.equals(other.getTipOpc()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.tipSal==null && other.getTipSal()==null) || 
             (this.tipSal!=null &&
              this.tipSal.equals(other.getTipSal()))) &&
            ((this.tipSalHsa==null && other.getTipSalHsa()==null) || 
             (this.tipSalHsa!=null &&
              this.tipSalHsa.equals(other.getTipSalHsa()))) &&
            ((this.tipSex==null && other.getTipSex()==null) || 
             (this.tipSex!=null &&
              this.tipSex.equals(other.getTipSex()))) &&
            ((this.tpCtBa==null && other.getTpCtBa()==null) || 
             (this.tpCtBa!=null &&
              this.tpCtBa.equals(other.getTpCtBa()))) &&
            ((this.turInt==null && other.getTurInt()==null) || 
             (this.turInt!=null &&
              this.turInt.equals(other.getTurInt()))) &&
            ((this.valSal==null && other.getValSal()==null) || 
             (this.valSal!=null &&
              this.valSal.equals(other.getValSal()))) &&
            ((this.valSalHsa==null && other.getValSalHsa()==null) || 
             (this.valSalHsa!=null &&
              this.valSalHsa.equals(other.getValSalHsa()))) &&
            ((this.verInt==null && other.getVerInt()==null) || 
             (this.verInt!=null &&
              this.verInt.equals(other.getVerInt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAciTraAfa() != null) {
            _hashCode += getAciTraAfa().hashCode();
        }
        if (getUSU_FisRes() != null) {
            _hashCode += getUSU_FisRes().hashCode();
        }
        if (getUSU_DepEmp() != null) {
            _hashCode += getUSU_DepEmp().hashCode();
        }
        if (getUSU_ValCom() != null) {
            _hashCode += getUSU_ValCom().hashCode();
        }
        if (getUSU_CsgBrc() != null) {
            _hashCode += getUSU_CsgBrc().hashCode();
        }
        if (getUSU_ColSde() != null) {
            _hashCode += getUSU_ColSde().hashCode();
        }
        if (getUSU_TipAdm() != null) {
            _hashCode += getUSU_TipAdm().hashCode();
        }
        if (getAdmAnt() != null) {
            _hashCode += getAdmAnt().hashCode();
        }
        if (getAdmeSo() != null) {
            _hashCode += getAdmeSo().hashCode();
        }
        if (getApeFun() != null) {
            _hashCode += getApeFun().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getApuPon() != null) {
            _hashCode += getApuPon().hashCode();
        }
        if (getApuPonApu() != null) {
            _hashCode += getApuPonApu().hashCode();
        }
        if (getAssPpr() != null) {
            _hashCode += getAssPpr().hashCode();
        }
        if (getBenRea() != null) {
            _hashCode += getBenRea().hashCode();
        }
        if (getBusHor() != null) {
            _hashCode += getBusHor().hashCode();
        }
        if (getCadFol() != null) {
            _hashCode += getCadFol().hashCode();
        }
        if (getCarVagHca() != null) {
            _hashCode += getCarVagHca().hashCode();
        }
        if (getCatAnt() != null) {
            _hashCode += getCatAnt().hashCode();
        }
        if (getCatSef() != null) {
            _hashCode += getCatSef().hashCode();
        }
        if (getCateSo() != null) {
            _hashCode += getCateSo().hashCode();
        }
        if (getCauDemAfa() != null) {
            _hashCode += getCauDemAfa().hashCode();
        }
        if (getClaSalHsa() != null) {
            _hashCode += getClaSalHsa().hashCode();
        }
        if (getCnpjAn() != null) {
            _hashCode += getCnpjAn().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodAteAfa() != null) {
            _hashCode += getCodAteAfa().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodCar() != null) {
            _hashCode += getCodCar().hashCode();
        }
        if (getCodCat() != null) {
            _hashCode += getCodCat().hashCode();
        }
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodCha() != null) {
            _hashCode += getCodCha().hashCode();
        }
        if (getCodDef() != null) {
            _hashCode += getCodDef().hashCode();
        }
        if (getCodDoeAfa() != null) {
            _hashCode += getCodDoeAfa().hashCode();
        }
        if (getCodEqp() != null) {
            _hashCode += getCodEqp().hashCode();
        }
        if (getCodEsc() != null) {
            _hashCode += getCodEsc().hashCode();
        }
        if (getCodEstHsa() != null) {
            _hashCode += getCodEstHsa().hashCode();
        }
        if (getCodEtb() != null) {
            _hashCode += getCodEtb().hashCode();
        }
        if (getCodFicFmd() != null) {
            _hashCode += getCodFicFmd().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodIdn() != null) {
            _hashCode += getCodIdn().hashCode();
        }
        if (getCodMotHca() != null) {
            _hashCode += getCodMotHca().hashCode();
        }
        if (getCodMotHsa() != null) {
            _hashCode += getCodMotHsa().hashCode();
        }
        if (getCodNac() != null) {
            _hashCode += getCodNac().hashCode();
        }
        if (getCodPro() != null) {
            _hashCode += getCodPro().hashCode();
        }
        if (getCodSin() != null) {
            _hashCode += getCodSin().hashCode();
        }
        if (getCodSinHsi() != null) {
            _hashCode += getCodSinHsi().hashCode();
        }
        if (getCodTap() != null) {
            _hashCode += getCodTap().hashCode();
        }
        if (getCodTma() != null) {
            _hashCode += getCodTma().hashCode();
        }
        if (getCodTmaHes() != null) {
            _hashCode += getCodTmaHes().hashCode();
        }
        if (getCodVinHvi() != null) {
            _hashCode += getCodVinHvi().hashCode();
        }
        if (getConBan() != null) {
            _hashCode += getConBan().hashCode();
        }
        if (getConFgt() != null) {
            _hashCode += getConFgt().hashCode();
        }
        if (getConFinCcu() != null) {
            _hashCode += getConFinCcu().hashCode();
        }
        if (getConRho() != null) {
            _hashCode += getConRho().hashCode();
        }
        if (getConTosHlo() != null) {
            _hashCode += getConTosHlo().hashCode();
        }
        if (getConTovAfa() != null) {
            _hashCode += getConTovAfa().hashCode();
        }
        if (getConTovHfi() != null) {
            _hashCode += getConTovHfi().hashCode();
        }
        if (getConTovHlo() != null) {
            _hashCode += getConTovHlo().hashCode();
        }
        if (getCotDef() != null) {
            _hashCode += getCotDef().hashCode();
        }
        if (getCplEstHsa() != null) {
            _hashCode += getCplEstHsa().hashCode();
        }
        if (getCplSalHsa() != null) {
            _hashCode += getCplSalHsa().hashCode();
        }
        if (getDatAdm() != null) {
            _hashCode += getDatAdm().hashCode();
        }
        if (getDatAfaAfa() != null) {
            _hashCode += getDatAfaAfa().hashCode();
        }
        if (getDatAltCcu() != null) {
            _hashCode += getDatAltCcu().hashCode();
        }
        if (getDatAltHca() != null) {
            _hashCode += getDatAltHca().hashCode();
        }
        if (getDatAltHcs() != null) {
            _hashCode += getDatAltHcs().hashCode();
        }
        if (getDatAltHes() != null) {
            _hashCode += getDatAltHes().hashCode();
        }
        if (getDatAltHfi() != null) {
            _hashCode += getDatAltHfi().hashCode();
        }
        if (getDatAltHlo() != null) {
            _hashCode += getDatAltHlo().hashCode();
        }
        if (getDatAltHor() != null) {
            _hashCode += getDatAltHor().hashCode();
        }
        if (getDatApo() != null) {
            _hashCode += getDatApo().hashCode();
        }
        if (getDatInc() != null) {
            _hashCode += getDatInc().hashCode();
        }
        if (getDatNas() != null) {
            _hashCode += getDatNas().hashCode();
        }
        if (getDatOpc() != null) {
            _hashCode += getDatOpc().hashCode();
        }
        if (getDatTerAfa() != null) {
            _hashCode += getDatTerAfa().hashCode();
        }
        if (getDcdPis() != null) {
            _hashCode += getDcdPis().hashCode();
        }
        if (getDefFis() != null) {
            _hashCode += getDefFis().hashCode();
        }
        if (getDepIrf() != null) {
            _hashCode += getDepIrf().hashCode();
        }
        if (getDepSaf() != null) {
            _hashCode += getDepSaf().hashCode();
        }
        if (getDexCtp() != null) {
            _hashCode += getDexCtp().hashCode();
        }
        if (getDiaJusAfa() != null) {
            _hashCode += getDiaJusAfa().hashCode();
        }
        if (getDigBan() != null) {
            _hashCode += getDigBan().hashCode();
        }
        if (getDigCar() != null) {
            _hashCode += getDigCar().hashCode();
        }
        if (getDocEst() != null) {
            _hashCode += getDocEst().hashCode();
        }
        if (getEmiCar() != null) {
            _hashCode += getEmiCar().hashCode();
        }
        if (getEscVtr() != null) {
            _hashCode += getEscVtr().hashCode();
        }
        if (getEstCiv() != null) {
            _hashCode += getEstCiv().hashCode();
        }
        if (getEstConAfa() != null) {
            _hashCode += getEstConAfa().hashCode();
        }
        if (getEstCtp() != null) {
            _hashCode += getEstCtp().hashCode();
        }
        if (getExmRetAfa() != null) {
            _hashCode += getExmRetAfa().hashCode();
        }
        if (getFimEtbHeb() != null) {
            _hashCode += getFimEtbHeb().hashCode();
        }
        if (getFimEvt() != null) {
            _hashCode += getFimEvt().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getGraIns() != null) {
            _hashCode += getGraIns().hashCode();
        }
        if (getHorAfaAfa() != null) {
            _hashCode += getHorAfaAfa().hashCode();
        }
        if (getHorBas() != null) {
            _hashCode += getHorBas().hashCode();
        }
        if (getHorDsrHor() != null) {
            _hashCode += getHorDsrHor().hashCode();
        }
        if (getHorInc() != null) {
            _hashCode += getHorInc().hashCode();
        }
        if (getHorSabHor() != null) {
            _hashCode += getHorSabHor().hashCode();
        }
        if (getHorSemHor() != null) {
            _hashCode += getHorSemHor().hashCode();
        }
        if (getHorTerAfa() != null) {
            _hashCode += getHorTerAfa().hashCode();
        }
        if (getIndAdm() != null) {
            _hashCode += getIndAdm().hashCode();
        }
        if (getIniAtu() != null) {
            _hashCode += getIniAtu().hashCode();
        }
        if (getIniEtbHeb() != null) {
            _hashCode += getIniEtbHeb().hashCode();
        }
        if (getIniEvt() != null) {
            _hashCode += getIniEvt().hashCode();
        }
        if (getInsCur() != null) {
            _hashCode += getInsCur().hashCode();
        }
        if (getIrrIse() != null) {
            _hashCode += getIrrIse().hashCode();
        }
        if (getLisRai() != null) {
            _hashCode += getLisRai().hashCode();
        }
        if (getLocTraHlo() != null) {
            _hashCode += getLocTraHlo().hashCode();
        }
        if (getMatAnt() != null) {
            _hashCode += getMatAnt().hashCode();
        }
        if (getModPag() != null) {
            _hashCode += getModPag().hashCode();
        }
        if (getMoeEstHsa() != null) {
            _hashCode += getMoeEstHsa().hashCode();
        }
        if (getMotPos() != null) {
            _hashCode += getMotPos().hashCode();
        }
        if (getMovSef() != null) {
            _hashCode += getMovSef().hashCode();
        }
        if (getNatDesHna() != null) {
            _hashCode += getNatDesHna().hashCode();
        }
        if (getNivSalHsa() != null) {
            _hashCode += getNivSalHsa().hashCode();
        }
        if (getNomAteAfa() != null) {
            _hashCode += getNomAteAfa().hashCode();
        }
        if (getNomFun() != null) {
            _hashCode += getNomFun().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumCpf() != null) {
            _hashCode += getNumCpf().hashCode();
        }
        if (getNumCtp() != null) {
            _hashCode += getNumCtp().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getNumLoc() != null) {
            _hashCode += getNumLoc().hashCode();
        }
        if (getNumPis() != null) {
            _hashCode += getNumPis().hashCode();
        }
        if (getObsAfaAfa() != null) {
            _hashCode += getObsAfaAfa().hashCode();
        }
        if (getObsFimHeb() != null) {
            _hashCode += getObsFimHeb().hashCode();
        }
        if (getObsIniHeb() != null) {
            _hashCode += getObsIniHeb().hashCode();
        }
        if (getOnuSce() != null) {
            _hashCode += getOnuSce().hashCode();
        }
        if (getOpcCes() != null) {
            _hashCode += getOpcCes().hashCode();
        }
        if (getOrgClaAfa() != null) {
            _hashCode += getOrgClaAfa().hashCode();
        }
        if (getPagSin() != null) {
            _hashCode += getPagSin().hashCode();
        }
        if (getPerJur() != null) {
            _hashCode += getPerJur().hashCode();
        }
        if (getPerPag() != null) {
            _hashCode += getPerPag().hashCode();
        }
        if (getPonEmb() != null) {
            _hashCode += getPonEmb().hashCode();
        }
        if (getPosObs() != null) {
            _hashCode += getPosObs().hashCode();
        }
        if (getPosTra() != null) {
            _hashCode += getPosTra().hashCode();
        }
        if (getPrvTerAfa() != null) {
            _hashCode += getPrvTerAfa().hashCode();
        }
        if (getQhrAfaAfa() != null) {
            _hashCode += getQhrAfaAfa().hashCode();
        }
        if (getRacCor() != null) {
            _hashCode += getRacCor().hashCode();
        }
        if (getRatEve() != null) {
            _hashCode += getRatEve().hashCode();
        }
        if (getReaRes() != null) {
            _hashCode += getReaRes().hashCode();
        }
        if (getRec13S() != null) {
            _hashCode += getRec13S().hashCode();
        }
        if (getRecAdi() != null) {
            _hashCode += getRecAdi().hashCode();
        }
        if (getRecGra() != null) {
            _hashCode += getRecGra().hashCode();
        }
        if (getRegConAfa() != null) {
            _hashCode += getRegConAfa().hashCode();
        }
        if (getResOnu() != null) {
            _hashCode += getResOnu().hashCode();
        }
        if (getSalEstHsa() != null) {
            _hashCode += getSalEstHsa().hashCode();
        }
        if (getSerCtp() != null) {
            _hashCode += getSerCtp().hashCode();
        }
        if (getSisCes() != null) {
            _hashCode += getSisCes().hashCode();
        }
        if (getSitAfa() != null) {
            _hashCode += getSitAfa().hashCode();
        }
        if (getSocSinHsi() != null) {
            _hashCode += getSocSinHsi().hashCode();
        }
        if (getTipAdmHfi() != null) {
            _hashCode += getTipAdmHfi().hashCode();
        }
        if (getTipApo() != null) {
            _hashCode += getTipApo().hashCode();
        }
        if (getTipCon() != null) {
            _hashCode += getTipCon().hashCode();
        }
        if (getTipOpc() != null) {
            _hashCode += getTipOpc().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTipSal() != null) {
            _hashCode += getTipSal().hashCode();
        }
        if (getTipSalHsa() != null) {
            _hashCode += getTipSalHsa().hashCode();
        }
        if (getTipSex() != null) {
            _hashCode += getTipSex().hashCode();
        }
        if (getTpCtBa() != null) {
            _hashCode += getTpCtBa().hashCode();
        }
        if (getTurInt() != null) {
            _hashCode += getTurInt().hashCode();
        }
        if (getValSal() != null) {
            _hashCode += getValSal().hashCode();
        }
        if (getValSalHsa() != null) {
            _hashCode += getValSalHsa().hashCode();
        }
        if (getVerInt() != null) {
            _hashCode += getVerInt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaFichaBasica5In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaFichaBasica5In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aciTraAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "aciTraAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uSU_TipAdm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uSU_TipAdm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uSU_ColSde");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uSU_ColSde"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uSU_CsgBrc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uSU_CsgBrc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uSU_ValCom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uSU_ValCom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uSU_DepEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uSU_DepEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("admAnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "admAnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("admeSo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "admeSo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apeFun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apeFun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apuPon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apuPon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apuPonApu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apuPonApu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assPpr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assPpr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("benRea");
        elemField.setXmlName(new javax.xml.namespace.QName("", "benRea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("busHor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "busHor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cadFol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cadFol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carVagHca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carVagHca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catAnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catAnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catSef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catSef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cateSo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cateSo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cauDemAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cauDemAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claSalHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claSalHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnpjAn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cnpjAn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAteAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAteAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCha");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDoeAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDoeAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEqp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEqp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEstHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEstHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEtb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEtb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFicFmd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFicFmd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codIdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codIdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMotHca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMotHca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMotHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMotHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSinHsi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSinHsi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTap");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTap"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTma");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTma"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTmaHes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTmaHes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codVinHvi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codVinHvi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conFgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conFgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conFinCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conFinCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conRho");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conRho"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conTosHlo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conTosHlo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conTovAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conTovAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conTovHfi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conTovHfi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conTovHlo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conTovHlo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cotDef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cotDef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEstHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEstHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplSalHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplSalHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAdm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAdm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAfaAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAfaAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAltCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAltCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAltHca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAltHca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAltHcs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAltHcs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAltHes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAltHes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAltHfi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAltHfi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAltHlo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAltHlo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAltHor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAltHor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datApo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datApo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datInc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datInc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datOpc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datOpc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datTerAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datTerAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dcdPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dcdPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("defFis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "defFis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "depIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depSaf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "depSaf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dexCtp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dexCtp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaJusAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaJusAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("digBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "digBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("digCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "digCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "docEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emiCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emiCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("escVtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "escVtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estConAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estConAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCtp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCtp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exmRetAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "exmRetAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fimEtbHeb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fimEtbHeb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fimEvt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fimEvt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("graIns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "graIns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAfaAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAfaAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horBas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horBas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horDsrHor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horDsrHor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horInc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horInc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horSabHor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horSabHor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horSemHor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horSemHor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horTerAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horTerAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indAdm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indAdm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniEtbHeb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniEtbHeb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniEvt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniEvt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insCur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insCur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("irrIse");
        elemField.setXmlName(new javax.xml.namespace.QName("", "irrIse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lisRai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lisRai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locTraHlo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "locTraHlo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matAnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "matAnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modPag");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modPag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moeEstHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "moeEstHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motPos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motPos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("movSef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "movSef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natDesHna");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natDesHna"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nivSalHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nivSalHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomAteAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomAteAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomFun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomFun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCtp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCtp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numLoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numLoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsAfaAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsAfaAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsFimHeb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsFimHeb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsIniHeb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsIniHeb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("onuSce");
        elemField.setXmlName(new javax.xml.namespace.QName("", "onuSce"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opcCes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "opcCes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orgClaAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orgClaAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perJur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perJur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perPag");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perPag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ponEmb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ponEmb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "posObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "posTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prvTerAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prvTerAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qhrAfaAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qhrAfaAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("racCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "racCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratEve");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ratEve"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reaRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reaRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rec13S");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rec13S"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recAdi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recAdi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recGra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recGra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regConAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regConAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resOnu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resOnu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salEstHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salEstHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serCtp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serCtp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sisCes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sisCes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("socSinHsi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "socSinHsi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipAdmHfi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipAdmHfi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipApo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipApo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipSalHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipSalHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipSex");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipSex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tpCtBa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tpCtBa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("turInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "turInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valSalHsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valSalHsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "verInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
