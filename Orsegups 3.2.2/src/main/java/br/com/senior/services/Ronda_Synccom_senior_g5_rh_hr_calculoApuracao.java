/**
 * Ronda_Synccom_senior_g5_rh_hr_calculoApuracao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public interface Ronda_Synccom_senior_g5_rh_hr_calculoApuracao extends java.rmi.Remote {
    public br.com.senior.services.CalculoApuracaoCalcularOut calcular(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.CalculoApuracaoCalcularIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.CalculoApuracaoGravarOut gravar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.CalculoApuracaoGravarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.CalculoApuracaoEscalonarOut escalonar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.CalculoApuracaoEscalonarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.CalculoApuracaoAcertarOut acertar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.CalculoApuracaoAcertarIn parameters) throws java.rmi.RemoteException;
}
