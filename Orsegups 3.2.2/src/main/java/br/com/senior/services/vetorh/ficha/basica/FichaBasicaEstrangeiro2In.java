/**
 * FichaBasicaEstrangeiro2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaEstrangeiro2In  implements java.io.Serializable {
    private java.lang.String casBra;

    private java.lang.String datChe;

    private java.lang.String datEst;

    private java.lang.String datPor;

    private java.lang.String dvlCtp;

    private java.lang.String dvlEst;

    private java.lang.String emiEst;

    private java.lang.String filCbr;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.String numPor;

    private java.lang.String regEst;

    private java.lang.Integer tipCol;

    private java.lang.Integer visEst;

    public FichaBasicaEstrangeiro2In() {
    }

    public FichaBasicaEstrangeiro2In(
           java.lang.String casBra,
           java.lang.String datChe,
           java.lang.String datEst,
           java.lang.String datPor,
           java.lang.String dvlCtp,
           java.lang.String dvlEst,
           java.lang.String emiEst,
           java.lang.String filCbr,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.String numPor,
           java.lang.String regEst,
           java.lang.Integer tipCol,
           java.lang.Integer visEst) {
           this.casBra = casBra;
           this.datChe = datChe;
           this.datEst = datEst;
           this.datPor = datPor;
           this.dvlCtp = dvlCtp;
           this.dvlEst = dvlEst;
           this.emiEst = emiEst;
           this.filCbr = filCbr;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.numPor = numPor;
           this.regEst = regEst;
           this.tipCol = tipCol;
           this.visEst = visEst;
    }


    /**
     * Gets the casBra value for this FichaBasicaEstrangeiro2In.
     * 
     * @return casBra
     */
    public java.lang.String getCasBra() {
        return casBra;
    }


    /**
     * Sets the casBra value for this FichaBasicaEstrangeiro2In.
     * 
     * @param casBra
     */
    public void setCasBra(java.lang.String casBra) {
        this.casBra = casBra;
    }


    /**
     * Gets the datChe value for this FichaBasicaEstrangeiro2In.
     * 
     * @return datChe
     */
    public java.lang.String getDatChe() {
        return datChe;
    }


    /**
     * Sets the datChe value for this FichaBasicaEstrangeiro2In.
     * 
     * @param datChe
     */
    public void setDatChe(java.lang.String datChe) {
        this.datChe = datChe;
    }


    /**
     * Gets the datEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @return datEst
     */
    public java.lang.String getDatEst() {
        return datEst;
    }


    /**
     * Sets the datEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @param datEst
     */
    public void setDatEst(java.lang.String datEst) {
        this.datEst = datEst;
    }


    /**
     * Gets the datPor value for this FichaBasicaEstrangeiro2In.
     * 
     * @return datPor
     */
    public java.lang.String getDatPor() {
        return datPor;
    }


    /**
     * Sets the datPor value for this FichaBasicaEstrangeiro2In.
     * 
     * @param datPor
     */
    public void setDatPor(java.lang.String datPor) {
        this.datPor = datPor;
    }


    /**
     * Gets the dvlCtp value for this FichaBasicaEstrangeiro2In.
     * 
     * @return dvlCtp
     */
    public java.lang.String getDvlCtp() {
        return dvlCtp;
    }


    /**
     * Sets the dvlCtp value for this FichaBasicaEstrangeiro2In.
     * 
     * @param dvlCtp
     */
    public void setDvlCtp(java.lang.String dvlCtp) {
        this.dvlCtp = dvlCtp;
    }


    /**
     * Gets the dvlEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @return dvlEst
     */
    public java.lang.String getDvlEst() {
        return dvlEst;
    }


    /**
     * Sets the dvlEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @param dvlEst
     */
    public void setDvlEst(java.lang.String dvlEst) {
        this.dvlEst = dvlEst;
    }


    /**
     * Gets the emiEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @return emiEst
     */
    public java.lang.String getEmiEst() {
        return emiEst;
    }


    /**
     * Sets the emiEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @param emiEst
     */
    public void setEmiEst(java.lang.String emiEst) {
        this.emiEst = emiEst;
    }


    /**
     * Gets the filCbr value for this FichaBasicaEstrangeiro2In.
     * 
     * @return filCbr
     */
    public java.lang.String getFilCbr() {
        return filCbr;
    }


    /**
     * Sets the filCbr value for this FichaBasicaEstrangeiro2In.
     * 
     * @param filCbr
     */
    public void setFilCbr(java.lang.String filCbr) {
        this.filCbr = filCbr;
    }


    /**
     * Gets the flowInstanceID value for this FichaBasicaEstrangeiro2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this FichaBasicaEstrangeiro2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this FichaBasicaEstrangeiro2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this FichaBasicaEstrangeiro2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the numCad value for this FichaBasicaEstrangeiro2In.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this FichaBasicaEstrangeiro2In.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this FichaBasicaEstrangeiro2In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this FichaBasicaEstrangeiro2In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the numPor value for this FichaBasicaEstrangeiro2In.
     * 
     * @return numPor
     */
    public java.lang.String getNumPor() {
        return numPor;
    }


    /**
     * Sets the numPor value for this FichaBasicaEstrangeiro2In.
     * 
     * @param numPor
     */
    public void setNumPor(java.lang.String numPor) {
        this.numPor = numPor;
    }


    /**
     * Gets the regEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @return regEst
     */
    public java.lang.String getRegEst() {
        return regEst;
    }


    /**
     * Sets the regEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @param regEst
     */
    public void setRegEst(java.lang.String regEst) {
        this.regEst = regEst;
    }


    /**
     * Gets the tipCol value for this FichaBasicaEstrangeiro2In.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this FichaBasicaEstrangeiro2In.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the visEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @return visEst
     */
    public java.lang.Integer getVisEst() {
        return visEst;
    }


    /**
     * Sets the visEst value for this FichaBasicaEstrangeiro2In.
     * 
     * @param visEst
     */
    public void setVisEst(java.lang.Integer visEst) {
        this.visEst = visEst;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaEstrangeiro2In)) return false;
        FichaBasicaEstrangeiro2In other = (FichaBasicaEstrangeiro2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.casBra==null && other.getCasBra()==null) || 
             (this.casBra!=null &&
              this.casBra.equals(other.getCasBra()))) &&
            ((this.datChe==null && other.getDatChe()==null) || 
             (this.datChe!=null &&
              this.datChe.equals(other.getDatChe()))) &&
            ((this.datEst==null && other.getDatEst()==null) || 
             (this.datEst!=null &&
              this.datEst.equals(other.getDatEst()))) &&
            ((this.datPor==null && other.getDatPor()==null) || 
             (this.datPor!=null &&
              this.datPor.equals(other.getDatPor()))) &&
            ((this.dvlCtp==null && other.getDvlCtp()==null) || 
             (this.dvlCtp!=null &&
              this.dvlCtp.equals(other.getDvlCtp()))) &&
            ((this.dvlEst==null && other.getDvlEst()==null) || 
             (this.dvlEst!=null &&
              this.dvlEst.equals(other.getDvlEst()))) &&
            ((this.emiEst==null && other.getEmiEst()==null) || 
             (this.emiEst!=null &&
              this.emiEst.equals(other.getEmiEst()))) &&
            ((this.filCbr==null && other.getFilCbr()==null) || 
             (this.filCbr!=null &&
              this.filCbr.equals(other.getFilCbr()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.numPor==null && other.getNumPor()==null) || 
             (this.numPor!=null &&
              this.numPor.equals(other.getNumPor()))) &&
            ((this.regEst==null && other.getRegEst()==null) || 
             (this.regEst!=null &&
              this.regEst.equals(other.getRegEst()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.visEst==null && other.getVisEst()==null) || 
             (this.visEst!=null &&
              this.visEst.equals(other.getVisEst())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCasBra() != null) {
            _hashCode += getCasBra().hashCode();
        }
        if (getDatChe() != null) {
            _hashCode += getDatChe().hashCode();
        }
        if (getDatEst() != null) {
            _hashCode += getDatEst().hashCode();
        }
        if (getDatPor() != null) {
            _hashCode += getDatPor().hashCode();
        }
        if (getDvlCtp() != null) {
            _hashCode += getDvlCtp().hashCode();
        }
        if (getDvlEst() != null) {
            _hashCode += getDvlEst().hashCode();
        }
        if (getEmiEst() != null) {
            _hashCode += getEmiEst().hashCode();
        }
        if (getFilCbr() != null) {
            _hashCode += getFilCbr().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getNumPor() != null) {
            _hashCode += getNumPor().hashCode();
        }
        if (getRegEst() != null) {
            _hashCode += getRegEst().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getVisEst() != null) {
            _hashCode += getVisEst().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaEstrangeiro2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaEstrangeiro2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("casBra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "casBra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datChe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datChe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dvlCtp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dvlCtp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dvlEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dvlEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emiEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emiEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filCbr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filCbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("visEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "visEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
