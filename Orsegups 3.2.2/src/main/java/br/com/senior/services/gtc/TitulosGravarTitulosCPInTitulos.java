/**
 * TitulosGravarTitulosCPInTitulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosGravarTitulosCPInTitulos  implements java.io.Serializable {
    private java.lang.String ccbFor;

    private java.lang.String cnpjFilial;

    private java.lang.String codAge;

    private java.lang.String codBan;

    private java.lang.String codBar;

    private java.lang.String codCcu;

    private java.lang.String codCrp;

    private java.lang.String codCrt;

    private java.lang.String codEmp;

    private java.lang.String codFil;

    private java.lang.String codFor;

    private java.lang.String codFpg;

    private java.lang.String codFpj;

    private java.lang.String codMoe;

    private java.lang.String codMpt;

    private java.lang.String codNtg;

    private java.lang.String codPor;

    private java.lang.String codTns;

    private java.lang.String codTpt;

    private java.lang.String codTri;

    private java.lang.String codUsu;

    private java.lang.String corApr;

    private java.lang.String cotApr;

    private java.lang.String cotEmi;

    private java.lang.String ctaApr;

    private java.lang.String ctaFin;

    private java.lang.String ctaRed;

    private java.lang.String datApr;

    private java.lang.String datDsc;

    private java.lang.String datEmi;

    private java.lang.String datEnt;

    private java.lang.String datNeg;

    private java.lang.String datPpt;

    private java.lang.String dscApr;

    private java.lang.String empApr;

    private java.lang.String encApr;

    private java.lang.String fpgApr;

    private java.lang.String horApr;

    private java.lang.Integer ideExt;

    private java.lang.String jrsApr;

    private java.lang.String jrsDia;

    private java.lang.String jrsNeg;

    private java.lang.String libApr;

    private java.lang.String mulApr;

    private java.lang.String mulNeg;

    private java.lang.String numArb;

    private java.lang.String numPrj;

    private java.lang.String numTit;

    private java.lang.String oacApr;

    private java.lang.String obsTcp;

    private java.lang.String odeApr;

    private java.lang.String outNeg;

    private java.lang.String perDsc;

    private java.lang.String perJrs;

    private java.lang.String perMul;

    private java.lang.String pgtApr;

    private java.lang.String porAnt;

    private java.lang.String seqApr;

    private java.lang.String tipJrs;

    private java.lang.Integer tipTcc;

    private java.lang.String titBan;

    private java.lang.String tolDsc;

    private java.lang.String tolJrs;

    private java.lang.String tolMul;

    private java.lang.String usuSit;

    private java.lang.String vctOri;

    private java.lang.String vlrApr;

    private java.lang.String vlrDsc;

    private java.lang.String vlrOri;

    public TitulosGravarTitulosCPInTitulos() {
    }

    public TitulosGravarTitulosCPInTitulos(
           java.lang.String ccbFor,
           java.lang.String cnpjFilial,
           java.lang.String codAge,
           java.lang.String codBan,
           java.lang.String codBar,
           java.lang.String codCcu,
           java.lang.String codCrp,
           java.lang.String codCrt,
           java.lang.String codEmp,
           java.lang.String codFil,
           java.lang.String codFor,
           java.lang.String codFpg,
           java.lang.String codFpj,
           java.lang.String codMoe,
           java.lang.String codMpt,
           java.lang.String codNtg,
           java.lang.String codPor,
           java.lang.String codTns,
           java.lang.String codTpt,
           java.lang.String codTri,
           java.lang.String codUsu,
           java.lang.String corApr,
           java.lang.String cotApr,
           java.lang.String cotEmi,
           java.lang.String ctaApr,
           java.lang.String ctaFin,
           java.lang.String ctaRed,
           java.lang.String datApr,
           java.lang.String datDsc,
           java.lang.String datEmi,
           java.lang.String datEnt,
           java.lang.String datNeg,
           java.lang.String datPpt,
           java.lang.String dscApr,
           java.lang.String empApr,
           java.lang.String encApr,
           java.lang.String fpgApr,
           java.lang.String horApr,
           java.lang.Integer ideExt,
           java.lang.String jrsApr,
           java.lang.String jrsDia,
           java.lang.String jrsNeg,
           java.lang.String libApr,
           java.lang.String mulApr,
           java.lang.String mulNeg,
           java.lang.String numArb,
           java.lang.String numPrj,
           java.lang.String numTit,
           java.lang.String oacApr,
           java.lang.String obsTcp,
           java.lang.String odeApr,
           java.lang.String outNeg,
           java.lang.String perDsc,
           java.lang.String perJrs,
           java.lang.String perMul,
           java.lang.String pgtApr,
           java.lang.String porAnt,
           java.lang.String seqApr,
           java.lang.String tipJrs,
           java.lang.Integer tipTcc,
           java.lang.String titBan,
           java.lang.String tolDsc,
           java.lang.String tolJrs,
           java.lang.String tolMul,
           java.lang.String usuSit,
           java.lang.String vctOri,
           java.lang.String vlrApr,
           java.lang.String vlrDsc,
           java.lang.String vlrOri) {
           this.ccbFor = ccbFor;
           this.cnpjFilial = cnpjFilial;
           this.codAge = codAge;
           this.codBan = codBan;
           this.codBar = codBar;
           this.codCcu = codCcu;
           this.codCrp = codCrp;
           this.codCrt = codCrt;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codFpg = codFpg;
           this.codFpj = codFpj;
           this.codMoe = codMoe;
           this.codMpt = codMpt;
           this.codNtg = codNtg;
           this.codPor = codPor;
           this.codTns = codTns;
           this.codTpt = codTpt;
           this.codTri = codTri;
           this.codUsu = codUsu;
           this.corApr = corApr;
           this.cotApr = cotApr;
           this.cotEmi = cotEmi;
           this.ctaApr = ctaApr;
           this.ctaFin = ctaFin;
           this.ctaRed = ctaRed;
           this.datApr = datApr;
           this.datDsc = datDsc;
           this.datEmi = datEmi;
           this.datEnt = datEnt;
           this.datNeg = datNeg;
           this.datPpt = datPpt;
           this.dscApr = dscApr;
           this.empApr = empApr;
           this.encApr = encApr;
           this.fpgApr = fpgApr;
           this.horApr = horApr;
           this.ideExt = ideExt;
           this.jrsApr = jrsApr;
           this.jrsDia = jrsDia;
           this.jrsNeg = jrsNeg;
           this.libApr = libApr;
           this.mulApr = mulApr;
           this.mulNeg = mulNeg;
           this.numArb = numArb;
           this.numPrj = numPrj;
           this.numTit = numTit;
           this.oacApr = oacApr;
           this.obsTcp = obsTcp;
           this.odeApr = odeApr;
           this.outNeg = outNeg;
           this.perDsc = perDsc;
           this.perJrs = perJrs;
           this.perMul = perMul;
           this.pgtApr = pgtApr;
           this.porAnt = porAnt;
           this.seqApr = seqApr;
           this.tipJrs = tipJrs;
           this.tipTcc = tipTcc;
           this.titBan = titBan;
           this.tolDsc = tolDsc;
           this.tolJrs = tolJrs;
           this.tolMul = tolMul;
           this.usuSit = usuSit;
           this.vctOri = vctOri;
           this.vlrApr = vlrApr;
           this.vlrDsc = vlrDsc;
           this.vlrOri = vlrOri;
    }


    /**
     * Gets the ccbFor value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return ccbFor
     */
    public java.lang.String getCcbFor() {
        return ccbFor;
    }


    /**
     * Sets the ccbFor value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param ccbFor
     */
    public void setCcbFor(java.lang.String ccbFor) {
        this.ccbFor = ccbFor;
    }


    /**
     * Gets the cnpjFilial value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return cnpjFilial
     */
    public java.lang.String getCnpjFilial() {
        return cnpjFilial;
    }


    /**
     * Sets the cnpjFilial value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param cnpjFilial
     */
    public void setCnpjFilial(java.lang.String cnpjFilial) {
        this.cnpjFilial = cnpjFilial;
    }


    /**
     * Gets the codAge value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codAge
     */
    public java.lang.String getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.String codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codBar value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codBar
     */
    public java.lang.String getCodBar() {
        return codBar;
    }


    /**
     * Sets the codBar value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codBar
     */
    public void setCodBar(java.lang.String codBar) {
        this.codBar = codBar;
    }


    /**
     * Gets the codCcu value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codCrp value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codCrp
     */
    public java.lang.String getCodCrp() {
        return codCrp;
    }


    /**
     * Sets the codCrp value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codCrp
     */
    public void setCodCrp(java.lang.String codCrp) {
        this.codCrp = codCrp;
    }


    /**
     * Gets the codCrt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codEmp value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codEmp
     */
    public java.lang.String getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.String codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codFil
     */
    public java.lang.String getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.String codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codFor
     */
    public java.lang.String getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.String codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codFpg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codFpg
     */
    public java.lang.String getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.String codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFpj value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codFpj
     */
    public java.lang.String getCodFpj() {
        return codFpj;
    }


    /**
     * Sets the codFpj value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codFpj
     */
    public void setCodFpj(java.lang.String codFpj) {
        this.codFpj = codFpj;
    }


    /**
     * Gets the codMoe value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codMoe
     */
    public java.lang.String getCodMoe() {
        return codMoe;
    }


    /**
     * Sets the codMoe value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codMoe
     */
    public void setCodMoe(java.lang.String codMoe) {
        this.codMoe = codMoe;
    }


    /**
     * Gets the codMpt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codMpt
     */
    public java.lang.String getCodMpt() {
        return codMpt;
    }


    /**
     * Sets the codMpt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codMpt
     */
    public void setCodMpt(java.lang.String codMpt) {
        this.codMpt = codMpt;
    }


    /**
     * Gets the codNtg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codNtg
     */
    public java.lang.String getCodNtg() {
        return codNtg;
    }


    /**
     * Sets the codNtg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codNtg
     */
    public void setCodNtg(java.lang.String codNtg) {
        this.codNtg = codNtg;
    }


    /**
     * Gets the codPor value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codTns value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codTns
     */
    public java.lang.String getCodTns() {
        return codTns;
    }


    /**
     * Sets the codTns value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codTns
     */
    public void setCodTns(java.lang.String codTns) {
        this.codTns = codTns;
    }


    /**
     * Gets the codTpt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the codTri value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codTri
     */
    public java.lang.String getCodTri() {
        return codTri;
    }


    /**
     * Sets the codTri value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codTri
     */
    public void setCodTri(java.lang.String codTri) {
        this.codTri = codTri;
    }


    /**
     * Gets the codUsu value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return codUsu
     */
    public java.lang.String getCodUsu() {
        return codUsu;
    }


    /**
     * Sets the codUsu value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param codUsu
     */
    public void setCodUsu(java.lang.String codUsu) {
        this.codUsu = codUsu;
    }


    /**
     * Gets the corApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return corApr
     */
    public java.lang.String getCorApr() {
        return corApr;
    }


    /**
     * Sets the corApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param corApr
     */
    public void setCorApr(java.lang.String corApr) {
        this.corApr = corApr;
    }


    /**
     * Gets the cotApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return cotApr
     */
    public java.lang.String getCotApr() {
        return cotApr;
    }


    /**
     * Sets the cotApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param cotApr
     */
    public void setCotApr(java.lang.String cotApr) {
        this.cotApr = cotApr;
    }


    /**
     * Gets the cotEmi value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return cotEmi
     */
    public java.lang.String getCotEmi() {
        return cotEmi;
    }


    /**
     * Sets the cotEmi value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param cotEmi
     */
    public void setCotEmi(java.lang.String cotEmi) {
        this.cotEmi = cotEmi;
    }


    /**
     * Gets the ctaApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return ctaApr
     */
    public java.lang.String getCtaApr() {
        return ctaApr;
    }


    /**
     * Sets the ctaApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param ctaApr
     */
    public void setCtaApr(java.lang.String ctaApr) {
        this.ctaApr = ctaApr;
    }


    /**
     * Gets the ctaFin value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return ctaFin
     */
    public java.lang.String getCtaFin() {
        return ctaFin;
    }


    /**
     * Sets the ctaFin value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param ctaFin
     */
    public void setCtaFin(java.lang.String ctaFin) {
        this.ctaFin = ctaFin;
    }


    /**
     * Gets the ctaRed value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return ctaRed
     */
    public java.lang.String getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.String ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the datApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return datApr
     */
    public java.lang.String getDatApr() {
        return datApr;
    }


    /**
     * Sets the datApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param datApr
     */
    public void setDatApr(java.lang.String datApr) {
        this.datApr = datApr;
    }


    /**
     * Gets the datDsc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return datDsc
     */
    public java.lang.String getDatDsc() {
        return datDsc;
    }


    /**
     * Sets the datDsc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param datDsc
     */
    public void setDatDsc(java.lang.String datDsc) {
        this.datDsc = datDsc;
    }


    /**
     * Gets the datEmi value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return datEmi
     */
    public java.lang.String getDatEmi() {
        return datEmi;
    }


    /**
     * Sets the datEmi value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param datEmi
     */
    public void setDatEmi(java.lang.String datEmi) {
        this.datEmi = datEmi;
    }


    /**
     * Gets the datEnt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return datEnt
     */
    public java.lang.String getDatEnt() {
        return datEnt;
    }


    /**
     * Sets the datEnt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param datEnt
     */
    public void setDatEnt(java.lang.String datEnt) {
        this.datEnt = datEnt;
    }


    /**
     * Gets the datNeg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return datNeg
     */
    public java.lang.String getDatNeg() {
        return datNeg;
    }


    /**
     * Sets the datNeg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param datNeg
     */
    public void setDatNeg(java.lang.String datNeg) {
        this.datNeg = datNeg;
    }


    /**
     * Gets the datPpt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return datPpt
     */
    public java.lang.String getDatPpt() {
        return datPpt;
    }


    /**
     * Sets the datPpt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param datPpt
     */
    public void setDatPpt(java.lang.String datPpt) {
        this.datPpt = datPpt;
    }


    /**
     * Gets the dscApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return dscApr
     */
    public java.lang.String getDscApr() {
        return dscApr;
    }


    /**
     * Sets the dscApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param dscApr
     */
    public void setDscApr(java.lang.String dscApr) {
        this.dscApr = dscApr;
    }


    /**
     * Gets the empApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return empApr
     */
    public java.lang.String getEmpApr() {
        return empApr;
    }


    /**
     * Sets the empApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param empApr
     */
    public void setEmpApr(java.lang.String empApr) {
        this.empApr = empApr;
    }


    /**
     * Gets the encApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return encApr
     */
    public java.lang.String getEncApr() {
        return encApr;
    }


    /**
     * Sets the encApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param encApr
     */
    public void setEncApr(java.lang.String encApr) {
        this.encApr = encApr;
    }


    /**
     * Gets the fpgApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return fpgApr
     */
    public java.lang.String getFpgApr() {
        return fpgApr;
    }


    /**
     * Sets the fpgApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param fpgApr
     */
    public void setFpgApr(java.lang.String fpgApr) {
        this.fpgApr = fpgApr;
    }


    /**
     * Gets the horApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return horApr
     */
    public java.lang.String getHorApr() {
        return horApr;
    }


    /**
     * Sets the horApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param horApr
     */
    public void setHorApr(java.lang.String horApr) {
        this.horApr = horApr;
    }


    /**
     * Gets the ideExt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the jrsApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return jrsApr
     */
    public java.lang.String getJrsApr() {
        return jrsApr;
    }


    /**
     * Sets the jrsApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param jrsApr
     */
    public void setJrsApr(java.lang.String jrsApr) {
        this.jrsApr = jrsApr;
    }


    /**
     * Gets the jrsDia value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return jrsDia
     */
    public java.lang.String getJrsDia() {
        return jrsDia;
    }


    /**
     * Sets the jrsDia value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param jrsDia
     */
    public void setJrsDia(java.lang.String jrsDia) {
        this.jrsDia = jrsDia;
    }


    /**
     * Gets the jrsNeg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return jrsNeg
     */
    public java.lang.String getJrsNeg() {
        return jrsNeg;
    }


    /**
     * Sets the jrsNeg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param jrsNeg
     */
    public void setJrsNeg(java.lang.String jrsNeg) {
        this.jrsNeg = jrsNeg;
    }


    /**
     * Gets the libApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return libApr
     */
    public java.lang.String getLibApr() {
        return libApr;
    }


    /**
     * Sets the libApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param libApr
     */
    public void setLibApr(java.lang.String libApr) {
        this.libApr = libApr;
    }


    /**
     * Gets the mulApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return mulApr
     */
    public java.lang.String getMulApr() {
        return mulApr;
    }


    /**
     * Sets the mulApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param mulApr
     */
    public void setMulApr(java.lang.String mulApr) {
        this.mulApr = mulApr;
    }


    /**
     * Gets the mulNeg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return mulNeg
     */
    public java.lang.String getMulNeg() {
        return mulNeg;
    }


    /**
     * Sets the mulNeg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param mulNeg
     */
    public void setMulNeg(java.lang.String mulNeg) {
        this.mulNeg = mulNeg;
    }


    /**
     * Gets the numArb value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return numArb
     */
    public java.lang.String getNumArb() {
        return numArb;
    }


    /**
     * Sets the numArb value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param numArb
     */
    public void setNumArb(java.lang.String numArb) {
        this.numArb = numArb;
    }


    /**
     * Gets the numPrj value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return numPrj
     */
    public java.lang.String getNumPrj() {
        return numPrj;
    }


    /**
     * Sets the numPrj value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param numPrj
     */
    public void setNumPrj(java.lang.String numPrj) {
        this.numPrj = numPrj;
    }


    /**
     * Gets the numTit value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the oacApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return oacApr
     */
    public java.lang.String getOacApr() {
        return oacApr;
    }


    /**
     * Sets the oacApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param oacApr
     */
    public void setOacApr(java.lang.String oacApr) {
        this.oacApr = oacApr;
    }


    /**
     * Gets the obsTcp value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return obsTcp
     */
    public java.lang.String getObsTcp() {
        return obsTcp;
    }


    /**
     * Sets the obsTcp value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param obsTcp
     */
    public void setObsTcp(java.lang.String obsTcp) {
        this.obsTcp = obsTcp;
    }


    /**
     * Gets the odeApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return odeApr
     */
    public java.lang.String getOdeApr() {
        return odeApr;
    }


    /**
     * Sets the odeApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param odeApr
     */
    public void setOdeApr(java.lang.String odeApr) {
        this.odeApr = odeApr;
    }


    /**
     * Gets the outNeg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return outNeg
     */
    public java.lang.String getOutNeg() {
        return outNeg;
    }


    /**
     * Sets the outNeg value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param outNeg
     */
    public void setOutNeg(java.lang.String outNeg) {
        this.outNeg = outNeg;
    }


    /**
     * Gets the perDsc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return perDsc
     */
    public java.lang.String getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.String perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perJrs value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return perJrs
     */
    public java.lang.String getPerJrs() {
        return perJrs;
    }


    /**
     * Sets the perJrs value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param perJrs
     */
    public void setPerJrs(java.lang.String perJrs) {
        this.perJrs = perJrs;
    }


    /**
     * Gets the perMul value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return perMul
     */
    public java.lang.String getPerMul() {
        return perMul;
    }


    /**
     * Sets the perMul value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param perMul
     */
    public void setPerMul(java.lang.String perMul) {
        this.perMul = perMul;
    }


    /**
     * Gets the pgtApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return pgtApr
     */
    public java.lang.String getPgtApr() {
        return pgtApr;
    }


    /**
     * Sets the pgtApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param pgtApr
     */
    public void setPgtApr(java.lang.String pgtApr) {
        this.pgtApr = pgtApr;
    }


    /**
     * Gets the porAnt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return porAnt
     */
    public java.lang.String getPorAnt() {
        return porAnt;
    }


    /**
     * Sets the porAnt value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param porAnt
     */
    public void setPorAnt(java.lang.String porAnt) {
        this.porAnt = porAnt;
    }


    /**
     * Gets the seqApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return seqApr
     */
    public java.lang.String getSeqApr() {
        return seqApr;
    }


    /**
     * Sets the seqApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param seqApr
     */
    public void setSeqApr(java.lang.String seqApr) {
        this.seqApr = seqApr;
    }


    /**
     * Gets the tipJrs value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return tipJrs
     */
    public java.lang.String getTipJrs() {
        return tipJrs;
    }


    /**
     * Sets the tipJrs value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param tipJrs
     */
    public void setTipJrs(java.lang.String tipJrs) {
        this.tipJrs = tipJrs;
    }


    /**
     * Gets the tipTcc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return tipTcc
     */
    public java.lang.Integer getTipTcc() {
        return tipTcc;
    }


    /**
     * Sets the tipTcc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param tipTcc
     */
    public void setTipTcc(java.lang.Integer tipTcc) {
        this.tipTcc = tipTcc;
    }


    /**
     * Gets the titBan value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return titBan
     */
    public java.lang.String getTitBan() {
        return titBan;
    }


    /**
     * Sets the titBan value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param titBan
     */
    public void setTitBan(java.lang.String titBan) {
        this.titBan = titBan;
    }


    /**
     * Gets the tolDsc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return tolDsc
     */
    public java.lang.String getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.String tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the tolJrs value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return tolJrs
     */
    public java.lang.String getTolJrs() {
        return tolJrs;
    }


    /**
     * Sets the tolJrs value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param tolJrs
     */
    public void setTolJrs(java.lang.String tolJrs) {
        this.tolJrs = tolJrs;
    }


    /**
     * Gets the tolMul value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return tolMul
     */
    public java.lang.String getTolMul() {
        return tolMul;
    }


    /**
     * Sets the tolMul value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param tolMul
     */
    public void setTolMul(java.lang.String tolMul) {
        this.tolMul = tolMul;
    }


    /**
     * Gets the usuSit value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return usuSit
     */
    public java.lang.String getUsuSit() {
        return usuSit;
    }


    /**
     * Sets the usuSit value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param usuSit
     */
    public void setUsuSit(java.lang.String usuSit) {
        this.usuSit = usuSit;
    }


    /**
     * Gets the vctOri value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return vctOri
     */
    public java.lang.String getVctOri() {
        return vctOri;
    }


    /**
     * Sets the vctOri value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param vctOri
     */
    public void setVctOri(java.lang.String vctOri) {
        this.vctOri = vctOri;
    }


    /**
     * Gets the vlrApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return vlrApr
     */
    public java.lang.String getVlrApr() {
        return vlrApr;
    }


    /**
     * Sets the vlrApr value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param vlrApr
     */
    public void setVlrApr(java.lang.String vlrApr) {
        this.vlrApr = vlrApr;
    }


    /**
     * Gets the vlrDsc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return vlrDsc
     */
    public java.lang.String getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.String vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrOri value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @return vlrOri
     */
    public java.lang.String getVlrOri() {
        return vlrOri;
    }


    /**
     * Sets the vlrOri value for this TitulosGravarTitulosCPInTitulos.
     * 
     * @param vlrOri
     */
    public void setVlrOri(java.lang.String vlrOri) {
        this.vlrOri = vlrOri;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosGravarTitulosCPInTitulos)) return false;
        TitulosGravarTitulosCPInTitulos other = (TitulosGravarTitulosCPInTitulos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ccbFor==null && other.getCcbFor()==null) || 
             (this.ccbFor!=null &&
              this.ccbFor.equals(other.getCcbFor()))) &&
            ((this.cnpjFilial==null && other.getCnpjFilial()==null) || 
             (this.cnpjFilial!=null &&
              this.cnpjFilial.equals(other.getCnpjFilial()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codBar==null && other.getCodBar()==null) || 
             (this.codBar!=null &&
              this.codBar.equals(other.getCodBar()))) &&
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codCrp==null && other.getCodCrp()==null) || 
             (this.codCrp!=null &&
              this.codCrp.equals(other.getCodCrp()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFpj==null && other.getCodFpj()==null) || 
             (this.codFpj!=null &&
              this.codFpj.equals(other.getCodFpj()))) &&
            ((this.codMoe==null && other.getCodMoe()==null) || 
             (this.codMoe!=null &&
              this.codMoe.equals(other.getCodMoe()))) &&
            ((this.codMpt==null && other.getCodMpt()==null) || 
             (this.codMpt!=null &&
              this.codMpt.equals(other.getCodMpt()))) &&
            ((this.codNtg==null && other.getCodNtg()==null) || 
             (this.codNtg!=null &&
              this.codNtg.equals(other.getCodNtg()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codTns==null && other.getCodTns()==null) || 
             (this.codTns!=null &&
              this.codTns.equals(other.getCodTns()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.codTri==null && other.getCodTri()==null) || 
             (this.codTri!=null &&
              this.codTri.equals(other.getCodTri()))) &&
            ((this.codUsu==null && other.getCodUsu()==null) || 
             (this.codUsu!=null &&
              this.codUsu.equals(other.getCodUsu()))) &&
            ((this.corApr==null && other.getCorApr()==null) || 
             (this.corApr!=null &&
              this.corApr.equals(other.getCorApr()))) &&
            ((this.cotApr==null && other.getCotApr()==null) || 
             (this.cotApr!=null &&
              this.cotApr.equals(other.getCotApr()))) &&
            ((this.cotEmi==null && other.getCotEmi()==null) || 
             (this.cotEmi!=null &&
              this.cotEmi.equals(other.getCotEmi()))) &&
            ((this.ctaApr==null && other.getCtaApr()==null) || 
             (this.ctaApr!=null &&
              this.ctaApr.equals(other.getCtaApr()))) &&
            ((this.ctaFin==null && other.getCtaFin()==null) || 
             (this.ctaFin!=null &&
              this.ctaFin.equals(other.getCtaFin()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.datApr==null && other.getDatApr()==null) || 
             (this.datApr!=null &&
              this.datApr.equals(other.getDatApr()))) &&
            ((this.datDsc==null && other.getDatDsc()==null) || 
             (this.datDsc!=null &&
              this.datDsc.equals(other.getDatDsc()))) &&
            ((this.datEmi==null && other.getDatEmi()==null) || 
             (this.datEmi!=null &&
              this.datEmi.equals(other.getDatEmi()))) &&
            ((this.datEnt==null && other.getDatEnt()==null) || 
             (this.datEnt!=null &&
              this.datEnt.equals(other.getDatEnt()))) &&
            ((this.datNeg==null && other.getDatNeg()==null) || 
             (this.datNeg!=null &&
              this.datNeg.equals(other.getDatNeg()))) &&
            ((this.datPpt==null && other.getDatPpt()==null) || 
             (this.datPpt!=null &&
              this.datPpt.equals(other.getDatPpt()))) &&
            ((this.dscApr==null && other.getDscApr()==null) || 
             (this.dscApr!=null &&
              this.dscApr.equals(other.getDscApr()))) &&
            ((this.empApr==null && other.getEmpApr()==null) || 
             (this.empApr!=null &&
              this.empApr.equals(other.getEmpApr()))) &&
            ((this.encApr==null && other.getEncApr()==null) || 
             (this.encApr!=null &&
              this.encApr.equals(other.getEncApr()))) &&
            ((this.fpgApr==null && other.getFpgApr()==null) || 
             (this.fpgApr!=null &&
              this.fpgApr.equals(other.getFpgApr()))) &&
            ((this.horApr==null && other.getHorApr()==null) || 
             (this.horApr!=null &&
              this.horApr.equals(other.getHorApr()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.jrsApr==null && other.getJrsApr()==null) || 
             (this.jrsApr!=null &&
              this.jrsApr.equals(other.getJrsApr()))) &&
            ((this.jrsDia==null && other.getJrsDia()==null) || 
             (this.jrsDia!=null &&
              this.jrsDia.equals(other.getJrsDia()))) &&
            ((this.jrsNeg==null && other.getJrsNeg()==null) || 
             (this.jrsNeg!=null &&
              this.jrsNeg.equals(other.getJrsNeg()))) &&
            ((this.libApr==null && other.getLibApr()==null) || 
             (this.libApr!=null &&
              this.libApr.equals(other.getLibApr()))) &&
            ((this.mulApr==null && other.getMulApr()==null) || 
             (this.mulApr!=null &&
              this.mulApr.equals(other.getMulApr()))) &&
            ((this.mulNeg==null && other.getMulNeg()==null) || 
             (this.mulNeg!=null &&
              this.mulNeg.equals(other.getMulNeg()))) &&
            ((this.numArb==null && other.getNumArb()==null) || 
             (this.numArb!=null &&
              this.numArb.equals(other.getNumArb()))) &&
            ((this.numPrj==null && other.getNumPrj()==null) || 
             (this.numPrj!=null &&
              this.numPrj.equals(other.getNumPrj()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.oacApr==null && other.getOacApr()==null) || 
             (this.oacApr!=null &&
              this.oacApr.equals(other.getOacApr()))) &&
            ((this.obsTcp==null && other.getObsTcp()==null) || 
             (this.obsTcp!=null &&
              this.obsTcp.equals(other.getObsTcp()))) &&
            ((this.odeApr==null && other.getOdeApr()==null) || 
             (this.odeApr!=null &&
              this.odeApr.equals(other.getOdeApr()))) &&
            ((this.outNeg==null && other.getOutNeg()==null) || 
             (this.outNeg!=null &&
              this.outNeg.equals(other.getOutNeg()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perJrs==null && other.getPerJrs()==null) || 
             (this.perJrs!=null &&
              this.perJrs.equals(other.getPerJrs()))) &&
            ((this.perMul==null && other.getPerMul()==null) || 
             (this.perMul!=null &&
              this.perMul.equals(other.getPerMul()))) &&
            ((this.pgtApr==null && other.getPgtApr()==null) || 
             (this.pgtApr!=null &&
              this.pgtApr.equals(other.getPgtApr()))) &&
            ((this.porAnt==null && other.getPorAnt()==null) || 
             (this.porAnt!=null &&
              this.porAnt.equals(other.getPorAnt()))) &&
            ((this.seqApr==null && other.getSeqApr()==null) || 
             (this.seqApr!=null &&
              this.seqApr.equals(other.getSeqApr()))) &&
            ((this.tipJrs==null && other.getTipJrs()==null) || 
             (this.tipJrs!=null &&
              this.tipJrs.equals(other.getTipJrs()))) &&
            ((this.tipTcc==null && other.getTipTcc()==null) || 
             (this.tipTcc!=null &&
              this.tipTcc.equals(other.getTipTcc()))) &&
            ((this.titBan==null && other.getTitBan()==null) || 
             (this.titBan!=null &&
              this.titBan.equals(other.getTitBan()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.tolJrs==null && other.getTolJrs()==null) || 
             (this.tolJrs!=null &&
              this.tolJrs.equals(other.getTolJrs()))) &&
            ((this.tolMul==null && other.getTolMul()==null) || 
             (this.tolMul!=null &&
              this.tolMul.equals(other.getTolMul()))) &&
            ((this.usuSit==null && other.getUsuSit()==null) || 
             (this.usuSit!=null &&
              this.usuSit.equals(other.getUsuSit()))) &&
            ((this.vctOri==null && other.getVctOri()==null) || 
             (this.vctOri!=null &&
              this.vctOri.equals(other.getVctOri()))) &&
            ((this.vlrApr==null && other.getVlrApr()==null) || 
             (this.vlrApr!=null &&
              this.vlrApr.equals(other.getVlrApr()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrOri==null && other.getVlrOri()==null) || 
             (this.vlrOri!=null &&
              this.vlrOri.equals(other.getVlrOri())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCcbFor() != null) {
            _hashCode += getCcbFor().hashCode();
        }
        if (getCnpjFilial() != null) {
            _hashCode += getCnpjFilial().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodBar() != null) {
            _hashCode += getCodBar().hashCode();
        }
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodCrp() != null) {
            _hashCode += getCodCrp().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFpj() != null) {
            _hashCode += getCodFpj().hashCode();
        }
        if (getCodMoe() != null) {
            _hashCode += getCodMoe().hashCode();
        }
        if (getCodMpt() != null) {
            _hashCode += getCodMpt().hashCode();
        }
        if (getCodNtg() != null) {
            _hashCode += getCodNtg().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodTns() != null) {
            _hashCode += getCodTns().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getCodTri() != null) {
            _hashCode += getCodTri().hashCode();
        }
        if (getCodUsu() != null) {
            _hashCode += getCodUsu().hashCode();
        }
        if (getCorApr() != null) {
            _hashCode += getCorApr().hashCode();
        }
        if (getCotApr() != null) {
            _hashCode += getCotApr().hashCode();
        }
        if (getCotEmi() != null) {
            _hashCode += getCotEmi().hashCode();
        }
        if (getCtaApr() != null) {
            _hashCode += getCtaApr().hashCode();
        }
        if (getCtaFin() != null) {
            _hashCode += getCtaFin().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getDatApr() != null) {
            _hashCode += getDatApr().hashCode();
        }
        if (getDatDsc() != null) {
            _hashCode += getDatDsc().hashCode();
        }
        if (getDatEmi() != null) {
            _hashCode += getDatEmi().hashCode();
        }
        if (getDatEnt() != null) {
            _hashCode += getDatEnt().hashCode();
        }
        if (getDatNeg() != null) {
            _hashCode += getDatNeg().hashCode();
        }
        if (getDatPpt() != null) {
            _hashCode += getDatPpt().hashCode();
        }
        if (getDscApr() != null) {
            _hashCode += getDscApr().hashCode();
        }
        if (getEmpApr() != null) {
            _hashCode += getEmpApr().hashCode();
        }
        if (getEncApr() != null) {
            _hashCode += getEncApr().hashCode();
        }
        if (getFpgApr() != null) {
            _hashCode += getFpgApr().hashCode();
        }
        if (getHorApr() != null) {
            _hashCode += getHorApr().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getJrsApr() != null) {
            _hashCode += getJrsApr().hashCode();
        }
        if (getJrsDia() != null) {
            _hashCode += getJrsDia().hashCode();
        }
        if (getJrsNeg() != null) {
            _hashCode += getJrsNeg().hashCode();
        }
        if (getLibApr() != null) {
            _hashCode += getLibApr().hashCode();
        }
        if (getMulApr() != null) {
            _hashCode += getMulApr().hashCode();
        }
        if (getMulNeg() != null) {
            _hashCode += getMulNeg().hashCode();
        }
        if (getNumArb() != null) {
            _hashCode += getNumArb().hashCode();
        }
        if (getNumPrj() != null) {
            _hashCode += getNumPrj().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getOacApr() != null) {
            _hashCode += getOacApr().hashCode();
        }
        if (getObsTcp() != null) {
            _hashCode += getObsTcp().hashCode();
        }
        if (getOdeApr() != null) {
            _hashCode += getOdeApr().hashCode();
        }
        if (getOutNeg() != null) {
            _hashCode += getOutNeg().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerJrs() != null) {
            _hashCode += getPerJrs().hashCode();
        }
        if (getPerMul() != null) {
            _hashCode += getPerMul().hashCode();
        }
        if (getPgtApr() != null) {
            _hashCode += getPgtApr().hashCode();
        }
        if (getPorAnt() != null) {
            _hashCode += getPorAnt().hashCode();
        }
        if (getSeqApr() != null) {
            _hashCode += getSeqApr().hashCode();
        }
        if (getTipJrs() != null) {
            _hashCode += getTipJrs().hashCode();
        }
        if (getTipTcc() != null) {
            _hashCode += getTipTcc().hashCode();
        }
        if (getTitBan() != null) {
            _hashCode += getTitBan().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getTolJrs() != null) {
            _hashCode += getTolJrs().hashCode();
        }
        if (getTolMul() != null) {
            _hashCode += getTolMul().hashCode();
        }
        if (getUsuSit() != null) {
            _hashCode += getUsuSit().hashCode();
        }
        if (getVctOri() != null) {
            _hashCode += getVctOri().hashCode();
        }
        if (getVlrApr() != null) {
            _hashCode += getVlrApr().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrOri() != null) {
            _hashCode += getVlrOri().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosGravarTitulosCPInTitulos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGravarTitulosCPInTitulos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccbFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ccbFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnpjFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cnpjFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNtg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNtg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codUsu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codUsu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("corApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "corApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cotApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cotApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cotEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cotEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "empApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "encApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fpgApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fpgApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsDia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsDia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("libApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "libApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mulApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mulApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mulNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mulNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numArb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numArb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oacApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "oacApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsTcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsTcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("odeApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "odeApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pgtApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pgtApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porAnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porAnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipTcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipTcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuSit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuSit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
