/**
 * TitulosConsultarTitulosAbertosCPOutTitulosRateios.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosConsultarTitulosAbertosCPOutTitulosRateios  implements java.io.Serializable {
    private java.lang.String codCcu;

    private java.lang.Integer codFpj;

    private java.lang.String codTns;

    private java.lang.Integer criRat;

    private java.lang.Integer ctaFin;

    private java.lang.Integer ctaRed;

    private java.lang.String datBas;

    private java.lang.String mesAno;

    private java.lang.Integer numPrj;

    private java.lang.String obsRat;

    private java.lang.Double perCta;

    private java.lang.Double perRat;

    private java.lang.Integer seqMov;

    private java.lang.Integer seqRat;

    private java.lang.Integer somSub;

    private java.lang.String tipOri;

    private java.lang.Double vlrCta;

    private java.lang.Double vlrRat;

    public TitulosConsultarTitulosAbertosCPOutTitulosRateios() {
    }

    public TitulosConsultarTitulosAbertosCPOutTitulosRateios(
           java.lang.String codCcu,
           java.lang.Integer codFpj,
           java.lang.String codTns,
           java.lang.Integer criRat,
           java.lang.Integer ctaFin,
           java.lang.Integer ctaRed,
           java.lang.String datBas,
           java.lang.String mesAno,
           java.lang.Integer numPrj,
           java.lang.String obsRat,
           java.lang.Double perCta,
           java.lang.Double perRat,
           java.lang.Integer seqMov,
           java.lang.Integer seqRat,
           java.lang.Integer somSub,
           java.lang.String tipOri,
           java.lang.Double vlrCta,
           java.lang.Double vlrRat) {
           this.codCcu = codCcu;
           this.codFpj = codFpj;
           this.codTns = codTns;
           this.criRat = criRat;
           this.ctaFin = ctaFin;
           this.ctaRed = ctaRed;
           this.datBas = datBas;
           this.mesAno = mesAno;
           this.numPrj = numPrj;
           this.obsRat = obsRat;
           this.perCta = perCta;
           this.perRat = perRat;
           this.seqMov = seqMov;
           this.seqRat = seqRat;
           this.somSub = somSub;
           this.tipOri = tipOri;
           this.vlrCta = vlrCta;
           this.vlrRat = vlrRat;
    }


    /**
     * Gets the codCcu value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codFpj value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return codFpj
     */
    public java.lang.Integer getCodFpj() {
        return codFpj;
    }


    /**
     * Sets the codFpj value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param codFpj
     */
    public void setCodFpj(java.lang.Integer codFpj) {
        this.codFpj = codFpj;
    }


    /**
     * Gets the codTns value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return codTns
     */
    public java.lang.String getCodTns() {
        return codTns;
    }


    /**
     * Sets the codTns value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param codTns
     */
    public void setCodTns(java.lang.String codTns) {
        this.codTns = codTns;
    }


    /**
     * Gets the criRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return criRat
     */
    public java.lang.Integer getCriRat() {
        return criRat;
    }


    /**
     * Sets the criRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param criRat
     */
    public void setCriRat(java.lang.Integer criRat) {
        this.criRat = criRat;
    }


    /**
     * Gets the ctaFin value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return ctaFin
     */
    public java.lang.Integer getCtaFin() {
        return ctaFin;
    }


    /**
     * Sets the ctaFin value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param ctaFin
     */
    public void setCtaFin(java.lang.Integer ctaFin) {
        this.ctaFin = ctaFin;
    }


    /**
     * Gets the ctaRed value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the datBas value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return datBas
     */
    public java.lang.String getDatBas() {
        return datBas;
    }


    /**
     * Sets the datBas value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param datBas
     */
    public void setDatBas(java.lang.String datBas) {
        this.datBas = datBas;
    }


    /**
     * Gets the mesAno value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return mesAno
     */
    public java.lang.String getMesAno() {
        return mesAno;
    }


    /**
     * Sets the mesAno value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param mesAno
     */
    public void setMesAno(java.lang.String mesAno) {
        this.mesAno = mesAno;
    }


    /**
     * Gets the numPrj value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return numPrj
     */
    public java.lang.Integer getNumPrj() {
        return numPrj;
    }


    /**
     * Sets the numPrj value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param numPrj
     */
    public void setNumPrj(java.lang.Integer numPrj) {
        this.numPrj = numPrj;
    }


    /**
     * Gets the obsRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return obsRat
     */
    public java.lang.String getObsRat() {
        return obsRat;
    }


    /**
     * Sets the obsRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param obsRat
     */
    public void setObsRat(java.lang.String obsRat) {
        this.obsRat = obsRat;
    }


    /**
     * Gets the perCta value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return perCta
     */
    public java.lang.Double getPerCta() {
        return perCta;
    }


    /**
     * Sets the perCta value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param perCta
     */
    public void setPerCta(java.lang.Double perCta) {
        this.perCta = perCta;
    }


    /**
     * Gets the perRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return perRat
     */
    public java.lang.Double getPerRat() {
        return perRat;
    }


    /**
     * Sets the perRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param perRat
     */
    public void setPerRat(java.lang.Double perRat) {
        this.perRat = perRat;
    }


    /**
     * Gets the seqMov value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return seqMov
     */
    public java.lang.Integer getSeqMov() {
        return seqMov;
    }


    /**
     * Sets the seqMov value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param seqMov
     */
    public void setSeqMov(java.lang.Integer seqMov) {
        this.seqMov = seqMov;
    }


    /**
     * Gets the seqRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return seqRat
     */
    public java.lang.Integer getSeqRat() {
        return seqRat;
    }


    /**
     * Sets the seqRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param seqRat
     */
    public void setSeqRat(java.lang.Integer seqRat) {
        this.seqRat = seqRat;
    }


    /**
     * Gets the somSub value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return somSub
     */
    public java.lang.Integer getSomSub() {
        return somSub;
    }


    /**
     * Sets the somSub value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param somSub
     */
    public void setSomSub(java.lang.Integer somSub) {
        this.somSub = somSub;
    }


    /**
     * Gets the tipOri value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return tipOri
     */
    public java.lang.String getTipOri() {
        return tipOri;
    }


    /**
     * Sets the tipOri value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param tipOri
     */
    public void setTipOri(java.lang.String tipOri) {
        this.tipOri = tipOri;
    }


    /**
     * Gets the vlrCta value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return vlrCta
     */
    public java.lang.Double getVlrCta() {
        return vlrCta;
    }


    /**
     * Sets the vlrCta value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param vlrCta
     */
    public void setVlrCta(java.lang.Double vlrCta) {
        this.vlrCta = vlrCta;
    }


    /**
     * Gets the vlrRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @return vlrRat
     */
    public java.lang.Double getVlrRat() {
        return vlrRat;
    }


    /**
     * Sets the vlrRat value for this TitulosConsultarTitulosAbertosCPOutTitulosRateios.
     * 
     * @param vlrRat
     */
    public void setVlrRat(java.lang.Double vlrRat) {
        this.vlrRat = vlrRat;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosConsultarTitulosAbertosCPOutTitulosRateios)) return false;
        TitulosConsultarTitulosAbertosCPOutTitulosRateios other = (TitulosConsultarTitulosAbertosCPOutTitulosRateios) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codFpj==null && other.getCodFpj()==null) || 
             (this.codFpj!=null &&
              this.codFpj.equals(other.getCodFpj()))) &&
            ((this.codTns==null && other.getCodTns()==null) || 
             (this.codTns!=null &&
              this.codTns.equals(other.getCodTns()))) &&
            ((this.criRat==null && other.getCriRat()==null) || 
             (this.criRat!=null &&
              this.criRat.equals(other.getCriRat()))) &&
            ((this.ctaFin==null && other.getCtaFin()==null) || 
             (this.ctaFin!=null &&
              this.ctaFin.equals(other.getCtaFin()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.datBas==null && other.getDatBas()==null) || 
             (this.datBas!=null &&
              this.datBas.equals(other.getDatBas()))) &&
            ((this.mesAno==null && other.getMesAno()==null) || 
             (this.mesAno!=null &&
              this.mesAno.equals(other.getMesAno()))) &&
            ((this.numPrj==null && other.getNumPrj()==null) || 
             (this.numPrj!=null &&
              this.numPrj.equals(other.getNumPrj()))) &&
            ((this.obsRat==null && other.getObsRat()==null) || 
             (this.obsRat!=null &&
              this.obsRat.equals(other.getObsRat()))) &&
            ((this.perCta==null && other.getPerCta()==null) || 
             (this.perCta!=null &&
              this.perCta.equals(other.getPerCta()))) &&
            ((this.perRat==null && other.getPerRat()==null) || 
             (this.perRat!=null &&
              this.perRat.equals(other.getPerRat()))) &&
            ((this.seqMov==null && other.getSeqMov()==null) || 
             (this.seqMov!=null &&
              this.seqMov.equals(other.getSeqMov()))) &&
            ((this.seqRat==null && other.getSeqRat()==null) || 
             (this.seqRat!=null &&
              this.seqRat.equals(other.getSeqRat()))) &&
            ((this.somSub==null && other.getSomSub()==null) || 
             (this.somSub!=null &&
              this.somSub.equals(other.getSomSub()))) &&
            ((this.tipOri==null && other.getTipOri()==null) || 
             (this.tipOri!=null &&
              this.tipOri.equals(other.getTipOri()))) &&
            ((this.vlrCta==null && other.getVlrCta()==null) || 
             (this.vlrCta!=null &&
              this.vlrCta.equals(other.getVlrCta()))) &&
            ((this.vlrRat==null && other.getVlrRat()==null) || 
             (this.vlrRat!=null &&
              this.vlrRat.equals(other.getVlrRat())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodFpj() != null) {
            _hashCode += getCodFpj().hashCode();
        }
        if (getCodTns() != null) {
            _hashCode += getCodTns().hashCode();
        }
        if (getCriRat() != null) {
            _hashCode += getCriRat().hashCode();
        }
        if (getCtaFin() != null) {
            _hashCode += getCtaFin().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getDatBas() != null) {
            _hashCode += getDatBas().hashCode();
        }
        if (getMesAno() != null) {
            _hashCode += getMesAno().hashCode();
        }
        if (getNumPrj() != null) {
            _hashCode += getNumPrj().hashCode();
        }
        if (getObsRat() != null) {
            _hashCode += getObsRat().hashCode();
        }
        if (getPerCta() != null) {
            _hashCode += getPerCta().hashCode();
        }
        if (getPerRat() != null) {
            _hashCode += getPerRat().hashCode();
        }
        if (getSeqMov() != null) {
            _hashCode += getSeqMov().hashCode();
        }
        if (getSeqRat() != null) {
            _hashCode += getSeqRat().hashCode();
        }
        if (getSomSub() != null) {
            _hashCode += getSomSub().hashCode();
        }
        if (getTipOri() != null) {
            _hashCode += getTipOri().hashCode();
        }
        if (getVlrCta() != null) {
            _hashCode += getVlrCta().hashCode();
        }
        if (getVlrRat() != null) {
            _hashCode += getVlrRat().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosConsultarTitulosAbertosCPOutTitulosRateios.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosAbertosCPOutTitulosRateios"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "criRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datBas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datBas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mesAno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mesAno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("somSub");
        elemField.setXmlName(new javax.xml.namespace.QName("", "somSub"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
