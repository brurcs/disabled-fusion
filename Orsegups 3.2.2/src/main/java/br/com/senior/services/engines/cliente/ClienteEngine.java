package br.com.senior.services.engines.cliente;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.neomind.fusion.custom.orsegups.contract.ContratoCadastraCartaoDeCredito;

@Path("/SapiensServices")
public class ClienteEngine {

	@GET
	@Path("/consultarCartaoDeCredito/{acao}/{codigoCliente}")
	public Response consultar(@PathParam("codigoCliente") int codigoCliente,
			@PathParam("acao") String acao) {
		
		String retorno = ContratoCadastraCartaoDeCredito.consultarCartao(codigoCliente, acao);			
		return Response.status(200).entity(retorno).header("Access-Control-Allow-Origin", "*").build();	
	}

	@POST
	@Path("/cadastrarCartaoDeCredito/{acao}/{codigoCliente}/{numeroCartao}/{dataVencimento}/{nomeTitular}/{codigoVerificacao}")
	public String cadastrar(@PathParam("acao") String acao,
			@PathParam("codigoCliente") int codigoCliente,
			@PathParam("numeroCartao") String numeroCartao,
			@PathParam("dataVencimento") String dataVencimento,
			@PathParam("nomeTitular") String nomeTitular,
			@PathParam("codigoVerificacao") String codigoVerificacao) {	

		String retorno = ContratoCadastraCartaoDeCredito.cadastrarCartao(acao, codigoCliente, numeroCartao, dataVencimento, nomeTitular, codigoVerificacao);	
		return retorno;	
	}
}
