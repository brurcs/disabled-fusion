/**
 * ResultadosExamesResultados3InWGD108RER.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.sm.resultadosExames;

public class ResultadosExamesResultados3InWGD108RER  implements java.io.Serializable {
    private java.lang.String descricao;

    private java.lang.Double quaRes;

    private java.lang.String resExa;

    public ResultadosExamesResultados3InWGD108RER() {
    }

    public ResultadosExamesResultados3InWGD108RER(
           java.lang.String descricao,
           java.lang.Double quaRes,
           java.lang.String resExa) {
           this.descricao = descricao;
           this.quaRes = quaRes;
           this.resExa = resExa;
    }


    /**
     * Gets the descricao value for this ResultadosExamesResultados3InWGD108RER.
     * 
     * @return descricao
     */
    public java.lang.String getDescricao() {
        return descricao;
    }


    /**
     * Sets the descricao value for this ResultadosExamesResultados3InWGD108RER.
     * 
     * @param descricao
     */
    public void setDescricao(java.lang.String descricao) {
        this.descricao = descricao;
    }


    /**
     * Gets the quaRes value for this ResultadosExamesResultados3InWGD108RER.
     * 
     * @return quaRes
     */
    public java.lang.Double getQuaRes() {
        return quaRes;
    }


    /**
     * Sets the quaRes value for this ResultadosExamesResultados3InWGD108RER.
     * 
     * @param quaRes
     */
    public void setQuaRes(java.lang.Double quaRes) {
        this.quaRes = quaRes;
    }


    /**
     * Gets the resExa value for this ResultadosExamesResultados3InWGD108RER.
     * 
     * @return resExa
     */
    public java.lang.String getResExa() {
        return resExa;
    }


    /**
     * Sets the resExa value for this ResultadosExamesResultados3InWGD108RER.
     * 
     * @param resExa
     */
    public void setResExa(java.lang.String resExa) {
        this.resExa = resExa;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResultadosExamesResultados3InWGD108RER)) return false;
        ResultadosExamesResultados3InWGD108RER other = (ResultadosExamesResultados3InWGD108RER) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.descricao==null && other.getDescricao()==null) || 
             (this.descricao!=null &&
              this.descricao.equals(other.getDescricao()))) &&
            ((this.quaRes==null && other.getQuaRes()==null) || 
             (this.quaRes!=null &&
              this.quaRes.equals(other.getQuaRes()))) &&
            ((this.resExa==null && other.getResExa()==null) || 
             (this.resExa!=null &&
              this.resExa.equals(other.getResExa())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescricao() != null) {
            _hashCode += getDescricao().hashCode();
        }
        if (getQuaRes() != null) {
            _hashCode += getQuaRes().hashCode();
        }
        if (getResExa() != null) {
            _hashCode += getResExa().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResultadosExamesResultados3InWGD108RER.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "resultadosExamesResultados3InWGD108RER"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quaRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quaRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resExa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resExa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
