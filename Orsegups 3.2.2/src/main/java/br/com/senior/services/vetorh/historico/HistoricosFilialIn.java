/**
 * HistoricosFilialIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosFilialIn  implements java.io.Serializable {
    private java.lang.Integer admeSo;

    private java.lang.String carVag;

    private java.lang.String claSal;

    private java.lang.Integer codBHr;

    private java.lang.String codCar;

    private java.lang.String codCcu;

    private java.lang.Integer codEsc;

    private java.lang.Integer codEst;

    private java.lang.Integer codMot;

    private java.lang.Integer codMts;

    private java.lang.Integer codSin;

    private java.lang.Integer codTap;

    private java.lang.Integer codTma;

    private java.lang.Integer codVin;

    private java.lang.Integer conFin;

    private java.lang.String conTov;

    private java.lang.Double cplSal;

    private java.lang.String datAlt;

    private java.lang.Integer ficReg;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer horBas;

    private java.lang.String horDsr;

    private java.lang.Integer horSab;

    private java.lang.String horSem;

    private java.lang.Integer motPos;

    private java.lang.Integer natDes;

    private java.lang.String nivSal;

    private java.lang.Integer novCad;

    private java.lang.Integer novEmp;

    private java.lang.Integer novFil;

    private java.lang.Integer numCad;

    private java.lang.String numCra;

    private java.lang.Integer numEmp;

    private java.lang.String numLoc;

    private java.lang.String posTra;

    private java.lang.String socSin;

    private java.lang.Integer tipAdm;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    private java.lang.Integer tipSal;

    private java.lang.Integer traBHr;

    private java.lang.Integer turInt;

    private java.lang.Double valSal;

    public HistoricosFilialIn() {
    }

    public HistoricosFilialIn(
           java.lang.Integer admeSo,
           java.lang.String carVag,
           java.lang.String claSal,
           java.lang.Integer codBHr,
           java.lang.String codCar,
           java.lang.String codCcu,
           java.lang.Integer codEsc,
           java.lang.Integer codEst,
           java.lang.Integer codMot,
           java.lang.Integer codMts,
           java.lang.Integer codSin,
           java.lang.Integer codTap,
           java.lang.Integer codTma,
           java.lang.Integer codVin,
           java.lang.Integer conFin,
           java.lang.String conTov,
           java.lang.Double cplSal,
           java.lang.String datAlt,
           java.lang.Integer ficReg,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer horBas,
           java.lang.String horDsr,
           java.lang.Integer horSab,
           java.lang.String horSem,
           java.lang.Integer motPos,
           java.lang.Integer natDes,
           java.lang.String nivSal,
           java.lang.Integer novCad,
           java.lang.Integer novEmp,
           java.lang.Integer novFil,
           java.lang.Integer numCad,
           java.lang.String numCra,
           java.lang.Integer numEmp,
           java.lang.String numLoc,
           java.lang.String posTra,
           java.lang.String socSin,
           java.lang.Integer tipAdm,
           java.lang.Integer tipCol,
           java.lang.String tipOpe,
           java.lang.Integer tipSal,
           java.lang.Integer traBHr,
           java.lang.Integer turInt,
           java.lang.Double valSal) {
           this.admeSo = admeSo;
           this.carVag = carVag;
           this.claSal = claSal;
           this.codBHr = codBHr;
           this.codCar = codCar;
           this.codCcu = codCcu;
           this.codEsc = codEsc;
           this.codEst = codEst;
           this.codMot = codMot;
           this.codMts = codMts;
           this.codSin = codSin;
           this.codTap = codTap;
           this.codTma = codTma;
           this.codVin = codVin;
           this.conFin = conFin;
           this.conTov = conTov;
           this.cplSal = cplSal;
           this.datAlt = datAlt;
           this.ficReg = ficReg;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.horBas = horBas;
           this.horDsr = horDsr;
           this.horSab = horSab;
           this.horSem = horSem;
           this.motPos = motPos;
           this.natDes = natDes;
           this.nivSal = nivSal;
           this.novCad = novCad;
           this.novEmp = novEmp;
           this.novFil = novFil;
           this.numCad = numCad;
           this.numCra = numCra;
           this.numEmp = numEmp;
           this.numLoc = numLoc;
           this.posTra = posTra;
           this.socSin = socSin;
           this.tipAdm = tipAdm;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
           this.tipSal = tipSal;
           this.traBHr = traBHr;
           this.turInt = turInt;
           this.valSal = valSal;
    }


    /**
     * Gets the admeSo value for this HistoricosFilialIn.
     * 
     * @return admeSo
     */
    public java.lang.Integer getAdmeSo() {
        return admeSo;
    }


    /**
     * Sets the admeSo value for this HistoricosFilialIn.
     * 
     * @param admeSo
     */
    public void setAdmeSo(java.lang.Integer admeSo) {
        this.admeSo = admeSo;
    }


    /**
     * Gets the carVag value for this HistoricosFilialIn.
     * 
     * @return carVag
     */
    public java.lang.String getCarVag() {
        return carVag;
    }


    /**
     * Sets the carVag value for this HistoricosFilialIn.
     * 
     * @param carVag
     */
    public void setCarVag(java.lang.String carVag) {
        this.carVag = carVag;
    }


    /**
     * Gets the claSal value for this HistoricosFilialIn.
     * 
     * @return claSal
     */
    public java.lang.String getClaSal() {
        return claSal;
    }


    /**
     * Sets the claSal value for this HistoricosFilialIn.
     * 
     * @param claSal
     */
    public void setClaSal(java.lang.String claSal) {
        this.claSal = claSal;
    }


    /**
     * Gets the codBHr value for this HistoricosFilialIn.
     * 
     * @return codBHr
     */
    public java.lang.Integer getCodBHr() {
        return codBHr;
    }


    /**
     * Sets the codBHr value for this HistoricosFilialIn.
     * 
     * @param codBHr
     */
    public void setCodBHr(java.lang.Integer codBHr) {
        this.codBHr = codBHr;
    }


    /**
     * Gets the codCar value for this HistoricosFilialIn.
     * 
     * @return codCar
     */
    public java.lang.String getCodCar() {
        return codCar;
    }


    /**
     * Sets the codCar value for this HistoricosFilialIn.
     * 
     * @param codCar
     */
    public void setCodCar(java.lang.String codCar) {
        this.codCar = codCar;
    }


    /**
     * Gets the codCcu value for this HistoricosFilialIn.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this HistoricosFilialIn.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codEsc value for this HistoricosFilialIn.
     * 
     * @return codEsc
     */
    public java.lang.Integer getCodEsc() {
        return codEsc;
    }


    /**
     * Sets the codEsc value for this HistoricosFilialIn.
     * 
     * @param codEsc
     */
    public void setCodEsc(java.lang.Integer codEsc) {
        this.codEsc = codEsc;
    }


    /**
     * Gets the codEst value for this HistoricosFilialIn.
     * 
     * @return codEst
     */
    public java.lang.Integer getCodEst() {
        return codEst;
    }


    /**
     * Sets the codEst value for this HistoricosFilialIn.
     * 
     * @param codEst
     */
    public void setCodEst(java.lang.Integer codEst) {
        this.codEst = codEst;
    }


    /**
     * Gets the codMot value for this HistoricosFilialIn.
     * 
     * @return codMot
     */
    public java.lang.Integer getCodMot() {
        return codMot;
    }


    /**
     * Sets the codMot value for this HistoricosFilialIn.
     * 
     * @param codMot
     */
    public void setCodMot(java.lang.Integer codMot) {
        this.codMot = codMot;
    }


    /**
     * Gets the codMts value for this HistoricosFilialIn.
     * 
     * @return codMts
     */
    public java.lang.Integer getCodMts() {
        return codMts;
    }


    /**
     * Sets the codMts value for this HistoricosFilialIn.
     * 
     * @param codMts
     */
    public void setCodMts(java.lang.Integer codMts) {
        this.codMts = codMts;
    }


    /**
     * Gets the codSin value for this HistoricosFilialIn.
     * 
     * @return codSin
     */
    public java.lang.Integer getCodSin() {
        return codSin;
    }


    /**
     * Sets the codSin value for this HistoricosFilialIn.
     * 
     * @param codSin
     */
    public void setCodSin(java.lang.Integer codSin) {
        this.codSin = codSin;
    }


    /**
     * Gets the codTap value for this HistoricosFilialIn.
     * 
     * @return codTap
     */
    public java.lang.Integer getCodTap() {
        return codTap;
    }


    /**
     * Sets the codTap value for this HistoricosFilialIn.
     * 
     * @param codTap
     */
    public void setCodTap(java.lang.Integer codTap) {
        this.codTap = codTap;
    }


    /**
     * Gets the codTma value for this HistoricosFilialIn.
     * 
     * @return codTma
     */
    public java.lang.Integer getCodTma() {
        return codTma;
    }


    /**
     * Sets the codTma value for this HistoricosFilialIn.
     * 
     * @param codTma
     */
    public void setCodTma(java.lang.Integer codTma) {
        this.codTma = codTma;
    }


    /**
     * Gets the codVin value for this HistoricosFilialIn.
     * 
     * @return codVin
     */
    public java.lang.Integer getCodVin() {
        return codVin;
    }


    /**
     * Sets the codVin value for this HistoricosFilialIn.
     * 
     * @param codVin
     */
    public void setCodVin(java.lang.Integer codVin) {
        this.codVin = codVin;
    }


    /**
     * Gets the conFin value for this HistoricosFilialIn.
     * 
     * @return conFin
     */
    public java.lang.Integer getConFin() {
        return conFin;
    }


    /**
     * Sets the conFin value for this HistoricosFilialIn.
     * 
     * @param conFin
     */
    public void setConFin(java.lang.Integer conFin) {
        this.conFin = conFin;
    }


    /**
     * Gets the conTov value for this HistoricosFilialIn.
     * 
     * @return conTov
     */
    public java.lang.String getConTov() {
        return conTov;
    }


    /**
     * Sets the conTov value for this HistoricosFilialIn.
     * 
     * @param conTov
     */
    public void setConTov(java.lang.String conTov) {
        this.conTov = conTov;
    }


    /**
     * Gets the cplSal value for this HistoricosFilialIn.
     * 
     * @return cplSal
     */
    public java.lang.Double getCplSal() {
        return cplSal;
    }


    /**
     * Sets the cplSal value for this HistoricosFilialIn.
     * 
     * @param cplSal
     */
    public void setCplSal(java.lang.Double cplSal) {
        this.cplSal = cplSal;
    }


    /**
     * Gets the datAlt value for this HistoricosFilialIn.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosFilialIn.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the ficReg value for this HistoricosFilialIn.
     * 
     * @return ficReg
     */
    public java.lang.Integer getFicReg() {
        return ficReg;
    }


    /**
     * Sets the ficReg value for this HistoricosFilialIn.
     * 
     * @param ficReg
     */
    public void setFicReg(java.lang.Integer ficReg) {
        this.ficReg = ficReg;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosFilialIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosFilialIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosFilialIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosFilialIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the horBas value for this HistoricosFilialIn.
     * 
     * @return horBas
     */
    public java.lang.Integer getHorBas() {
        return horBas;
    }


    /**
     * Sets the horBas value for this HistoricosFilialIn.
     * 
     * @param horBas
     */
    public void setHorBas(java.lang.Integer horBas) {
        this.horBas = horBas;
    }


    /**
     * Gets the horDsr value for this HistoricosFilialIn.
     * 
     * @return horDsr
     */
    public java.lang.String getHorDsr() {
        return horDsr;
    }


    /**
     * Sets the horDsr value for this HistoricosFilialIn.
     * 
     * @param horDsr
     */
    public void setHorDsr(java.lang.String horDsr) {
        this.horDsr = horDsr;
    }


    /**
     * Gets the horSab value for this HistoricosFilialIn.
     * 
     * @return horSab
     */
    public java.lang.Integer getHorSab() {
        return horSab;
    }


    /**
     * Sets the horSab value for this HistoricosFilialIn.
     * 
     * @param horSab
     */
    public void setHorSab(java.lang.Integer horSab) {
        this.horSab = horSab;
    }


    /**
     * Gets the horSem value for this HistoricosFilialIn.
     * 
     * @return horSem
     */
    public java.lang.String getHorSem() {
        return horSem;
    }


    /**
     * Sets the horSem value for this HistoricosFilialIn.
     * 
     * @param horSem
     */
    public void setHorSem(java.lang.String horSem) {
        this.horSem = horSem;
    }


    /**
     * Gets the motPos value for this HistoricosFilialIn.
     * 
     * @return motPos
     */
    public java.lang.Integer getMotPos() {
        return motPos;
    }


    /**
     * Sets the motPos value for this HistoricosFilialIn.
     * 
     * @param motPos
     */
    public void setMotPos(java.lang.Integer motPos) {
        this.motPos = motPos;
    }


    /**
     * Gets the natDes value for this HistoricosFilialIn.
     * 
     * @return natDes
     */
    public java.lang.Integer getNatDes() {
        return natDes;
    }


    /**
     * Sets the natDes value for this HistoricosFilialIn.
     * 
     * @param natDes
     */
    public void setNatDes(java.lang.Integer natDes) {
        this.natDes = natDes;
    }


    /**
     * Gets the nivSal value for this HistoricosFilialIn.
     * 
     * @return nivSal
     */
    public java.lang.String getNivSal() {
        return nivSal;
    }


    /**
     * Sets the nivSal value for this HistoricosFilialIn.
     * 
     * @param nivSal
     */
    public void setNivSal(java.lang.String nivSal) {
        this.nivSal = nivSal;
    }


    /**
     * Gets the novCad value for this HistoricosFilialIn.
     * 
     * @return novCad
     */
    public java.lang.Integer getNovCad() {
        return novCad;
    }


    /**
     * Sets the novCad value for this HistoricosFilialIn.
     * 
     * @param novCad
     */
    public void setNovCad(java.lang.Integer novCad) {
        this.novCad = novCad;
    }


    /**
     * Gets the novEmp value for this HistoricosFilialIn.
     * 
     * @return novEmp
     */
    public java.lang.Integer getNovEmp() {
        return novEmp;
    }


    /**
     * Sets the novEmp value for this HistoricosFilialIn.
     * 
     * @param novEmp
     */
    public void setNovEmp(java.lang.Integer novEmp) {
        this.novEmp = novEmp;
    }


    /**
     * Gets the novFil value for this HistoricosFilialIn.
     * 
     * @return novFil
     */
    public java.lang.Integer getNovFil() {
        return novFil;
    }


    /**
     * Sets the novFil value for this HistoricosFilialIn.
     * 
     * @param novFil
     */
    public void setNovFil(java.lang.Integer novFil) {
        this.novFil = novFil;
    }


    /**
     * Gets the numCad value for this HistoricosFilialIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosFilialIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numCra value for this HistoricosFilialIn.
     * 
     * @return numCra
     */
    public java.lang.String getNumCra() {
        return numCra;
    }


    /**
     * Sets the numCra value for this HistoricosFilialIn.
     * 
     * @param numCra
     */
    public void setNumCra(java.lang.String numCra) {
        this.numCra = numCra;
    }


    /**
     * Gets the numEmp value for this HistoricosFilialIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosFilialIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the numLoc value for this HistoricosFilialIn.
     * 
     * @return numLoc
     */
    public java.lang.String getNumLoc() {
        return numLoc;
    }


    /**
     * Sets the numLoc value for this HistoricosFilialIn.
     * 
     * @param numLoc
     */
    public void setNumLoc(java.lang.String numLoc) {
        this.numLoc = numLoc;
    }


    /**
     * Gets the posTra value for this HistoricosFilialIn.
     * 
     * @return posTra
     */
    public java.lang.String getPosTra() {
        return posTra;
    }


    /**
     * Sets the posTra value for this HistoricosFilialIn.
     * 
     * @param posTra
     */
    public void setPosTra(java.lang.String posTra) {
        this.posTra = posTra;
    }


    /**
     * Gets the socSin value for this HistoricosFilialIn.
     * 
     * @return socSin
     */
    public java.lang.String getSocSin() {
        return socSin;
    }


    /**
     * Sets the socSin value for this HistoricosFilialIn.
     * 
     * @param socSin
     */
    public void setSocSin(java.lang.String socSin) {
        this.socSin = socSin;
    }


    /**
     * Gets the tipAdm value for this HistoricosFilialIn.
     * 
     * @return tipAdm
     */
    public java.lang.Integer getTipAdm() {
        return tipAdm;
    }


    /**
     * Sets the tipAdm value for this HistoricosFilialIn.
     * 
     * @param tipAdm
     */
    public void setTipAdm(java.lang.Integer tipAdm) {
        this.tipAdm = tipAdm;
    }


    /**
     * Gets the tipCol value for this HistoricosFilialIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosFilialIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosFilialIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosFilialIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the tipSal value for this HistoricosFilialIn.
     * 
     * @return tipSal
     */
    public java.lang.Integer getTipSal() {
        return tipSal;
    }


    /**
     * Sets the tipSal value for this HistoricosFilialIn.
     * 
     * @param tipSal
     */
    public void setTipSal(java.lang.Integer tipSal) {
        this.tipSal = tipSal;
    }


    /**
     * Gets the traBHr value for this HistoricosFilialIn.
     * 
     * @return traBHr
     */
    public java.lang.Integer getTraBHr() {
        return traBHr;
    }


    /**
     * Sets the traBHr value for this HistoricosFilialIn.
     * 
     * @param traBHr
     */
    public void setTraBHr(java.lang.Integer traBHr) {
        this.traBHr = traBHr;
    }


    /**
     * Gets the turInt value for this HistoricosFilialIn.
     * 
     * @return turInt
     */
    public java.lang.Integer getTurInt() {
        return turInt;
    }


    /**
     * Sets the turInt value for this HistoricosFilialIn.
     * 
     * @param turInt
     */
    public void setTurInt(java.lang.Integer turInt) {
        this.turInt = turInt;
    }


    /**
     * Gets the valSal value for this HistoricosFilialIn.
     * 
     * @return valSal
     */
    public java.lang.Double getValSal() {
        return valSal;
    }


    /**
     * Sets the valSal value for this HistoricosFilialIn.
     * 
     * @param valSal
     */
    public void setValSal(java.lang.Double valSal) {
        this.valSal = valSal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosFilialIn)) return false;
        HistoricosFilialIn other = (HistoricosFilialIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.admeSo==null && other.getAdmeSo()==null) || 
             (this.admeSo!=null &&
              this.admeSo.equals(other.getAdmeSo()))) &&
            ((this.carVag==null && other.getCarVag()==null) || 
             (this.carVag!=null &&
              this.carVag.equals(other.getCarVag()))) &&
            ((this.claSal==null && other.getClaSal()==null) || 
             (this.claSal!=null &&
              this.claSal.equals(other.getClaSal()))) &&
            ((this.codBHr==null && other.getCodBHr()==null) || 
             (this.codBHr!=null &&
              this.codBHr.equals(other.getCodBHr()))) &&
            ((this.codCar==null && other.getCodCar()==null) || 
             (this.codCar!=null &&
              this.codCar.equals(other.getCodCar()))) &&
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codEsc==null && other.getCodEsc()==null) || 
             (this.codEsc!=null &&
              this.codEsc.equals(other.getCodEsc()))) &&
            ((this.codEst==null && other.getCodEst()==null) || 
             (this.codEst!=null &&
              this.codEst.equals(other.getCodEst()))) &&
            ((this.codMot==null && other.getCodMot()==null) || 
             (this.codMot!=null &&
              this.codMot.equals(other.getCodMot()))) &&
            ((this.codMts==null && other.getCodMts()==null) || 
             (this.codMts!=null &&
              this.codMts.equals(other.getCodMts()))) &&
            ((this.codSin==null && other.getCodSin()==null) || 
             (this.codSin!=null &&
              this.codSin.equals(other.getCodSin()))) &&
            ((this.codTap==null && other.getCodTap()==null) || 
             (this.codTap!=null &&
              this.codTap.equals(other.getCodTap()))) &&
            ((this.codTma==null && other.getCodTma()==null) || 
             (this.codTma!=null &&
              this.codTma.equals(other.getCodTma()))) &&
            ((this.codVin==null && other.getCodVin()==null) || 
             (this.codVin!=null &&
              this.codVin.equals(other.getCodVin()))) &&
            ((this.conFin==null && other.getConFin()==null) || 
             (this.conFin!=null &&
              this.conFin.equals(other.getConFin()))) &&
            ((this.conTov==null && other.getConTov()==null) || 
             (this.conTov!=null &&
              this.conTov.equals(other.getConTov()))) &&
            ((this.cplSal==null && other.getCplSal()==null) || 
             (this.cplSal!=null &&
              this.cplSal.equals(other.getCplSal()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.ficReg==null && other.getFicReg()==null) || 
             (this.ficReg!=null &&
              this.ficReg.equals(other.getFicReg()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.horBas==null && other.getHorBas()==null) || 
             (this.horBas!=null &&
              this.horBas.equals(other.getHorBas()))) &&
            ((this.horDsr==null && other.getHorDsr()==null) || 
             (this.horDsr!=null &&
              this.horDsr.equals(other.getHorDsr()))) &&
            ((this.horSab==null && other.getHorSab()==null) || 
             (this.horSab!=null &&
              this.horSab.equals(other.getHorSab()))) &&
            ((this.horSem==null && other.getHorSem()==null) || 
             (this.horSem!=null &&
              this.horSem.equals(other.getHorSem()))) &&
            ((this.motPos==null && other.getMotPos()==null) || 
             (this.motPos!=null &&
              this.motPos.equals(other.getMotPos()))) &&
            ((this.natDes==null && other.getNatDes()==null) || 
             (this.natDes!=null &&
              this.natDes.equals(other.getNatDes()))) &&
            ((this.nivSal==null && other.getNivSal()==null) || 
             (this.nivSal!=null &&
              this.nivSal.equals(other.getNivSal()))) &&
            ((this.novCad==null && other.getNovCad()==null) || 
             (this.novCad!=null &&
              this.novCad.equals(other.getNovCad()))) &&
            ((this.novEmp==null && other.getNovEmp()==null) || 
             (this.novEmp!=null &&
              this.novEmp.equals(other.getNovEmp()))) &&
            ((this.novFil==null && other.getNovFil()==null) || 
             (this.novFil!=null &&
              this.novFil.equals(other.getNovFil()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numCra==null && other.getNumCra()==null) || 
             (this.numCra!=null &&
              this.numCra.equals(other.getNumCra()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.numLoc==null && other.getNumLoc()==null) || 
             (this.numLoc!=null &&
              this.numLoc.equals(other.getNumLoc()))) &&
            ((this.posTra==null && other.getPosTra()==null) || 
             (this.posTra!=null &&
              this.posTra.equals(other.getPosTra()))) &&
            ((this.socSin==null && other.getSocSin()==null) || 
             (this.socSin!=null &&
              this.socSin.equals(other.getSocSin()))) &&
            ((this.tipAdm==null && other.getTipAdm()==null) || 
             (this.tipAdm!=null &&
              this.tipAdm.equals(other.getTipAdm()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.tipSal==null && other.getTipSal()==null) || 
             (this.tipSal!=null &&
              this.tipSal.equals(other.getTipSal()))) &&
            ((this.traBHr==null && other.getTraBHr()==null) || 
             (this.traBHr!=null &&
              this.traBHr.equals(other.getTraBHr()))) &&
            ((this.turInt==null && other.getTurInt()==null) || 
             (this.turInt!=null &&
              this.turInt.equals(other.getTurInt()))) &&
            ((this.valSal==null && other.getValSal()==null) || 
             (this.valSal!=null &&
              this.valSal.equals(other.getValSal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdmeSo() != null) {
            _hashCode += getAdmeSo().hashCode();
        }
        if (getCarVag() != null) {
            _hashCode += getCarVag().hashCode();
        }
        if (getClaSal() != null) {
            _hashCode += getClaSal().hashCode();
        }
        if (getCodBHr() != null) {
            _hashCode += getCodBHr().hashCode();
        }
        if (getCodCar() != null) {
            _hashCode += getCodCar().hashCode();
        }
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodEsc() != null) {
            _hashCode += getCodEsc().hashCode();
        }
        if (getCodEst() != null) {
            _hashCode += getCodEst().hashCode();
        }
        if (getCodMot() != null) {
            _hashCode += getCodMot().hashCode();
        }
        if (getCodMts() != null) {
            _hashCode += getCodMts().hashCode();
        }
        if (getCodSin() != null) {
            _hashCode += getCodSin().hashCode();
        }
        if (getCodTap() != null) {
            _hashCode += getCodTap().hashCode();
        }
        if (getCodTma() != null) {
            _hashCode += getCodTma().hashCode();
        }
        if (getCodVin() != null) {
            _hashCode += getCodVin().hashCode();
        }
        if (getConFin() != null) {
            _hashCode += getConFin().hashCode();
        }
        if (getConTov() != null) {
            _hashCode += getConTov().hashCode();
        }
        if (getCplSal() != null) {
            _hashCode += getCplSal().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getFicReg() != null) {
            _hashCode += getFicReg().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getHorBas() != null) {
            _hashCode += getHorBas().hashCode();
        }
        if (getHorDsr() != null) {
            _hashCode += getHorDsr().hashCode();
        }
        if (getHorSab() != null) {
            _hashCode += getHorSab().hashCode();
        }
        if (getHorSem() != null) {
            _hashCode += getHorSem().hashCode();
        }
        if (getMotPos() != null) {
            _hashCode += getMotPos().hashCode();
        }
        if (getNatDes() != null) {
            _hashCode += getNatDes().hashCode();
        }
        if (getNivSal() != null) {
            _hashCode += getNivSal().hashCode();
        }
        if (getNovCad() != null) {
            _hashCode += getNovCad().hashCode();
        }
        if (getNovEmp() != null) {
            _hashCode += getNovEmp().hashCode();
        }
        if (getNovFil() != null) {
            _hashCode += getNovFil().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumCra() != null) {
            _hashCode += getNumCra().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getNumLoc() != null) {
            _hashCode += getNumLoc().hashCode();
        }
        if (getPosTra() != null) {
            _hashCode += getPosTra().hashCode();
        }
        if (getSocSin() != null) {
            _hashCode += getSocSin().hashCode();
        }
        if (getTipAdm() != null) {
            _hashCode += getTipAdm().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTipSal() != null) {
            _hashCode += getTipSal().hashCode();
        }
        if (getTraBHr() != null) {
            _hashCode += getTraBHr().hashCode();
        }
        if (getTurInt() != null) {
            _hashCode += getTurInt().hashCode();
        }
        if (getValSal() != null) {
            _hashCode += getValSal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosFilialIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosFilialIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("admeSo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "admeSo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carVag");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carVag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBHr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBHr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMts");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTap");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTap"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTma");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTma"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codVin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codVin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conTov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conTov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ficReg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ficReg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horBas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horBas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horDsr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horDsr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horSab");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horSab"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horSem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horSem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motPos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motPos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natDes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natDes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nivSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nivSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("novCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "novCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("novEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "novEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("novFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "novFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numLoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numLoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "posTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("socSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "socSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipAdm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipAdm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("traBHr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "traBHr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("turInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "turInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
