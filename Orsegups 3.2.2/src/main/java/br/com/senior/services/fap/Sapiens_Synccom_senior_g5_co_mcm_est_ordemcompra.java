/**
 * Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.fap;

public interface Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra extends java.rmi.Remote {
    public br.com.senior.services.fap.OrdemcomprareprovarOut reprovar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprareprovarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.fap.OrdemcompraaprovarOut aprovar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcompraaprovarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.fap.OrdemcomprabuscarPendentesOut buscarPendentes(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentesIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.fap.OrdemcomprabuscarPendentes2Out buscarPendentes_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.fap.OrdemcomprabuscarPendentes3Out buscarPendentes_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes3In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out buscarPendentes_4(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes4In parameters) throws java.rmi.RemoteException;
}
