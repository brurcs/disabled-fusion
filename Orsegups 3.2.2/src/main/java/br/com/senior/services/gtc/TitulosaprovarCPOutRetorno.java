/**
 * TitulosaprovarCPOutRetorno.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosaprovarCPOutRetorno  implements java.io.Serializable {
    private java.lang.Integer codigoEmpresa;

    private java.lang.Integer codigoFilial;

    private java.lang.Integer codigoFornecedor;

    private java.lang.String mensagemErro;

    private java.lang.String numero;

    private java.lang.Integer sequencia;

    private java.lang.String tipoTitulo;

    public TitulosaprovarCPOutRetorno() {
    }

    public TitulosaprovarCPOutRetorno(
           java.lang.Integer codigoEmpresa,
           java.lang.Integer codigoFilial,
           java.lang.Integer codigoFornecedor,
           java.lang.String mensagemErro,
           java.lang.String numero,
           java.lang.Integer sequencia,
           java.lang.String tipoTitulo) {
           this.codigoEmpresa = codigoEmpresa;
           this.codigoFilial = codigoFilial;
           this.codigoFornecedor = codigoFornecedor;
           this.mensagemErro = mensagemErro;
           this.numero = numero;
           this.sequencia = sequencia;
           this.tipoTitulo = tipoTitulo;
    }


    /**
     * Gets the codigoEmpresa value for this TitulosaprovarCPOutRetorno.
     * 
     * @return codigoEmpresa
     */
    public java.lang.Integer getCodigoEmpresa() {
        return codigoEmpresa;
    }


    /**
     * Sets the codigoEmpresa value for this TitulosaprovarCPOutRetorno.
     * 
     * @param codigoEmpresa
     */
    public void setCodigoEmpresa(java.lang.Integer codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }


    /**
     * Gets the codigoFilial value for this TitulosaprovarCPOutRetorno.
     * 
     * @return codigoFilial
     */
    public java.lang.Integer getCodigoFilial() {
        return codigoFilial;
    }


    /**
     * Sets the codigoFilial value for this TitulosaprovarCPOutRetorno.
     * 
     * @param codigoFilial
     */
    public void setCodigoFilial(java.lang.Integer codigoFilial) {
        this.codigoFilial = codigoFilial;
    }


    /**
     * Gets the codigoFornecedor value for this TitulosaprovarCPOutRetorno.
     * 
     * @return codigoFornecedor
     */
    public java.lang.Integer getCodigoFornecedor() {
        return codigoFornecedor;
    }


    /**
     * Sets the codigoFornecedor value for this TitulosaprovarCPOutRetorno.
     * 
     * @param codigoFornecedor
     */
    public void setCodigoFornecedor(java.lang.Integer codigoFornecedor) {
        this.codigoFornecedor = codigoFornecedor;
    }


    /**
     * Gets the mensagemErro value for this TitulosaprovarCPOutRetorno.
     * 
     * @return mensagemErro
     */
    public java.lang.String getMensagemErro() {
        return mensagemErro;
    }


    /**
     * Sets the mensagemErro value for this TitulosaprovarCPOutRetorno.
     * 
     * @param mensagemErro
     */
    public void setMensagemErro(java.lang.String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }


    /**
     * Gets the numero value for this TitulosaprovarCPOutRetorno.
     * 
     * @return numero
     */
    public java.lang.String getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this TitulosaprovarCPOutRetorno.
     * 
     * @param numero
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }


    /**
     * Gets the sequencia value for this TitulosaprovarCPOutRetorno.
     * 
     * @return sequencia
     */
    public java.lang.Integer getSequencia() {
        return sequencia;
    }


    /**
     * Sets the sequencia value for this TitulosaprovarCPOutRetorno.
     * 
     * @param sequencia
     */
    public void setSequencia(java.lang.Integer sequencia) {
        this.sequencia = sequencia;
    }


    /**
     * Gets the tipoTitulo value for this TitulosaprovarCPOutRetorno.
     * 
     * @return tipoTitulo
     */
    public java.lang.String getTipoTitulo() {
        return tipoTitulo;
    }


    /**
     * Sets the tipoTitulo value for this TitulosaprovarCPOutRetorno.
     * 
     * @param tipoTitulo
     */
    public void setTipoTitulo(java.lang.String tipoTitulo) {
        this.tipoTitulo = tipoTitulo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosaprovarCPOutRetorno)) return false;
        TitulosaprovarCPOutRetorno other = (TitulosaprovarCPOutRetorno) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoEmpresa==null && other.getCodigoEmpresa()==null) || 
             (this.codigoEmpresa!=null &&
              this.codigoEmpresa.equals(other.getCodigoEmpresa()))) &&
            ((this.codigoFilial==null && other.getCodigoFilial()==null) || 
             (this.codigoFilial!=null &&
              this.codigoFilial.equals(other.getCodigoFilial()))) &&
            ((this.codigoFornecedor==null && other.getCodigoFornecedor()==null) || 
             (this.codigoFornecedor!=null &&
              this.codigoFornecedor.equals(other.getCodigoFornecedor()))) &&
            ((this.mensagemErro==null && other.getMensagemErro()==null) || 
             (this.mensagemErro!=null &&
              this.mensagemErro.equals(other.getMensagemErro()))) &&
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.sequencia==null && other.getSequencia()==null) || 
             (this.sequencia!=null &&
              this.sequencia.equals(other.getSequencia()))) &&
            ((this.tipoTitulo==null && other.getTipoTitulo()==null) || 
             (this.tipoTitulo!=null &&
              this.tipoTitulo.equals(other.getTipoTitulo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoEmpresa() != null) {
            _hashCode += getCodigoEmpresa().hashCode();
        }
        if (getCodigoFilial() != null) {
            _hashCode += getCodigoFilial().hashCode();
        }
        if (getCodigoFornecedor() != null) {
            _hashCode += getCodigoFornecedor().hashCode();
        }
        if (getMensagemErro() != null) {
            _hashCode += getMensagemErro().hashCode();
        }
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getSequencia() != null) {
            _hashCode += getSequencia().hashCode();
        }
        if (getTipoTitulo() != null) {
            _hashCode += getTipoTitulo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosaprovarCPOutRetorno.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosaprovarCPOutRetorno"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoEmpresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoEmpresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoFornecedor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoFornecedor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemErro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemErro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sequencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sequencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoTitulo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoTitulo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
