/**
 * HistoricosValeTransporteIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosValeTransporteIn  implements java.io.Serializable {
    private java.lang.String escVtr;

    private java.lang.String fimEvt;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String iniEvt;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Integer ponEmb;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    private br.com.senior.services.vetorh.historico.HistoricosValeTransporteInWGD038EVN[] WGD038EVN;

    public HistoricosValeTransporteIn() {
    }

    public HistoricosValeTransporteIn(
           java.lang.String escVtr,
           java.lang.String fimEvt,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String iniEvt,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Integer ponEmb,
           java.lang.Integer tipCol,
           java.lang.String tipOpe,
           br.com.senior.services.vetorh.historico.HistoricosValeTransporteInWGD038EVN[] WGD038EVN) {
           this.escVtr = escVtr;
           this.fimEvt = fimEvt;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.iniEvt = iniEvt;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.ponEmb = ponEmb;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
           this.WGD038EVN = WGD038EVN;
    }


    /**
     * Gets the escVtr value for this HistoricosValeTransporteIn.
     * 
     * @return escVtr
     */
    public java.lang.String getEscVtr() {
        return escVtr;
    }


    /**
     * Sets the escVtr value for this HistoricosValeTransporteIn.
     * 
     * @param escVtr
     */
    public void setEscVtr(java.lang.String escVtr) {
        this.escVtr = escVtr;
    }


    /**
     * Gets the fimEvt value for this HistoricosValeTransporteIn.
     * 
     * @return fimEvt
     */
    public java.lang.String getFimEvt() {
        return fimEvt;
    }


    /**
     * Sets the fimEvt value for this HistoricosValeTransporteIn.
     * 
     * @param fimEvt
     */
    public void setFimEvt(java.lang.String fimEvt) {
        this.fimEvt = fimEvt;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosValeTransporteIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosValeTransporteIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosValeTransporteIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosValeTransporteIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the iniEvt value for this HistoricosValeTransporteIn.
     * 
     * @return iniEvt
     */
    public java.lang.String getIniEvt() {
        return iniEvt;
    }


    /**
     * Sets the iniEvt value for this HistoricosValeTransporteIn.
     * 
     * @param iniEvt
     */
    public void setIniEvt(java.lang.String iniEvt) {
        this.iniEvt = iniEvt;
    }


    /**
     * Gets the numCad value for this HistoricosValeTransporteIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosValeTransporteIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosValeTransporteIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosValeTransporteIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the ponEmb value for this HistoricosValeTransporteIn.
     * 
     * @return ponEmb
     */
    public java.lang.Integer getPonEmb() {
        return ponEmb;
    }


    /**
     * Sets the ponEmb value for this HistoricosValeTransporteIn.
     * 
     * @param ponEmb
     */
    public void setPonEmb(java.lang.Integer ponEmb) {
        this.ponEmb = ponEmb;
    }


    /**
     * Gets the tipCol value for this HistoricosValeTransporteIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosValeTransporteIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosValeTransporteIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosValeTransporteIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the WGD038EVN value for this HistoricosValeTransporteIn.
     * 
     * @return WGD038EVN
     */
    public br.com.senior.services.vetorh.historico.HistoricosValeTransporteInWGD038EVN[] getWGD038EVN() {
        return WGD038EVN;
    }


    /**
     * Sets the WGD038EVN value for this HistoricosValeTransporteIn.
     * 
     * @param WGD038EVN
     */
    public void setWGD038EVN(br.com.senior.services.vetorh.historico.HistoricosValeTransporteInWGD038EVN[] WGD038EVN) {
        this.WGD038EVN = WGD038EVN;
    }

    public br.com.senior.services.vetorh.historico.HistoricosValeTransporteInWGD038EVN getWGD038EVN(int i) {
        return this.WGD038EVN[i];
    }

    public void setWGD038EVN(int i, br.com.senior.services.vetorh.historico.HistoricosValeTransporteInWGD038EVN _value) {
        this.WGD038EVN[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosValeTransporteIn)) return false;
        HistoricosValeTransporteIn other = (HistoricosValeTransporteIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.escVtr==null && other.getEscVtr()==null) || 
             (this.escVtr!=null &&
              this.escVtr.equals(other.getEscVtr()))) &&
            ((this.fimEvt==null && other.getFimEvt()==null) || 
             (this.fimEvt!=null &&
              this.fimEvt.equals(other.getFimEvt()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.iniEvt==null && other.getIniEvt()==null) || 
             (this.iniEvt!=null &&
              this.iniEvt.equals(other.getIniEvt()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.ponEmb==null && other.getPonEmb()==null) || 
             (this.ponEmb!=null &&
              this.ponEmb.equals(other.getPonEmb()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.WGD038EVN==null && other.getWGD038EVN()==null) || 
             (this.WGD038EVN!=null &&
              java.util.Arrays.equals(this.WGD038EVN, other.getWGD038EVN())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEscVtr() != null) {
            _hashCode += getEscVtr().hashCode();
        }
        if (getFimEvt() != null) {
            _hashCode += getFimEvt().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIniEvt() != null) {
            _hashCode += getIniEvt().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getPonEmb() != null) {
            _hashCode += getPonEmb().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getWGD038EVN() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWGD038EVN());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWGD038EVN(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosValeTransporteIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosValeTransporteIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("escVtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "escVtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fimEvt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fimEvt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniEvt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniEvt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ponEmb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ponEmb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("WGD038EVN");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WGD038EVN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosValeTransporteInWGD038EVN"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
