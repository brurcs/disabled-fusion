/**
 * ClientesExportarClientesCompletoOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesExportarClientesCompletoOut  implements java.io.Serializable {
    private br.com.senior.services.co.cad.clientes.ClientesExportarClientesCompletoOutClientes[] clientes;

    private java.lang.String erroExecucao;

    private java.lang.String finalizaramRegistros;

    private java.lang.String mensagemRetorno;

    public ClientesExportarClientesCompletoOut() {
    }

    public ClientesExportarClientesCompletoOut(
           br.com.senior.services.co.cad.clientes.ClientesExportarClientesCompletoOutClientes[] clientes,
           java.lang.String erroExecucao,
           java.lang.String finalizaramRegistros,
           java.lang.String mensagemRetorno) {
           this.clientes = clientes;
           this.erroExecucao = erroExecucao;
           this.finalizaramRegistros = finalizaramRegistros;
           this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the clientes value for this ClientesExportarClientesCompletoOut.
     * 
     * @return clientes
     */
    public br.com.senior.services.co.cad.clientes.ClientesExportarClientesCompletoOutClientes[] getClientes() {
        return clientes;
    }


    /**
     * Sets the clientes value for this ClientesExportarClientesCompletoOut.
     * 
     * @param clientes
     */
    public void setClientes(br.com.senior.services.co.cad.clientes.ClientesExportarClientesCompletoOutClientes[] clientes) {
        this.clientes = clientes;
    }

    public br.com.senior.services.co.cad.clientes.ClientesExportarClientesCompletoOutClientes getClientes(int i) {
        return this.clientes[i];
    }

    public void setClientes(int i, br.com.senior.services.co.cad.clientes.ClientesExportarClientesCompletoOutClientes _value) {
        this.clientes[i] = _value;
    }


    /**
     * Gets the erroExecucao value for this ClientesExportarClientesCompletoOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this ClientesExportarClientesCompletoOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the finalizaramRegistros value for this ClientesExportarClientesCompletoOut.
     * 
     * @return finalizaramRegistros
     */
    public java.lang.String getFinalizaramRegistros() {
        return finalizaramRegistros;
    }


    /**
     * Sets the finalizaramRegistros value for this ClientesExportarClientesCompletoOut.
     * 
     * @param finalizaramRegistros
     */
    public void setFinalizaramRegistros(java.lang.String finalizaramRegistros) {
        this.finalizaramRegistros = finalizaramRegistros;
    }


    /**
     * Gets the mensagemRetorno value for this ClientesExportarClientesCompletoOut.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this ClientesExportarClientesCompletoOut.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesExportarClientesCompletoOut)) return false;
        ClientesExportarClientesCompletoOut other = (ClientesExportarClientesCompletoOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clientes==null && other.getClientes()==null) || 
             (this.clientes!=null &&
              java.util.Arrays.equals(this.clientes, other.getClientes()))) &&
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.finalizaramRegistros==null && other.getFinalizaramRegistros()==null) || 
             (this.finalizaramRegistros!=null &&
              this.finalizaramRegistros.equals(other.getFinalizaramRegistros()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClientes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClientes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClientes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getFinalizaramRegistros() != null) {
            _hashCode += getFinalizaramRegistros().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesExportarClientesCompletoOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarClientesCompletoOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clientes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarClientesCompletoOutClientes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("finalizaramRegistros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "finalizaramRegistros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
