package br.com.senior.services.fap;

public class Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy implements br.com.senior.services.fap.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra {
  private String _endpoint = null;
  private br.com.senior.services.fap.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra = null;
  
  public Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy() {
    _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
  }
  
  public Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy(String endpoint) {
    _endpoint = endpoint;
    _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
  }
  
  private void _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy() {
    try {
      sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra = (new br.com.senior.services.fap.G5SeniorServicesLocator()).getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort();
      if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra != null)
      ((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.senior.services.fap.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra getSapiens_Synccom_senior_g5_co_mcm_est_ordemcompra() {
    if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra == null)
      _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
    return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra;
  }
  
  public br.com.senior.services.fap.OrdemcomprareprovarOut reprovar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprareprovarIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra == null)
      _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
    return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.reprovar(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.fap.OrdemcompraaprovarOut aprovar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcompraaprovarIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra == null)
      _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
    return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.aprovar(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.fap.OrdemcomprabuscarPendentesOut buscarPendentes(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentesIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra == null)
      _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
    return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.buscarPendentes(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.fap.OrdemcomprabuscarPendentes2Out buscarPendentes_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes2In parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra == null)
      _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
    return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.buscarPendentes_2(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.fap.OrdemcomprabuscarPendentes3Out buscarPendentes_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes3In parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra == null)
      _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
    return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.buscarPendentes_3(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out buscarPendentes_4(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.fap.OrdemcomprabuscarPendentes4In parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra == null)
      _initSapiens_Synccom_senior_g5_co_mcm_est_ordemcompraProxy();
    return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.buscarPendentes_4(user, password, encryption, parameters);
  }
  
  
}