package com.neomind.fusion.winker.servlet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoDateUtils;

@WebServlet(name = "ServletIntegracaoWinker", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.winker.servlet.ServletIntegracaoWinker" })
public class ServletIntegracaoWinker extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	EntityManager entityManager;
	EntityTransaction transaction;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			response.setContentType("text/plain");
			response.setCharacterEncoding("ISO-8859-1");

			final PrintWriter out = response.getWriter();

			String action = "";

			action = request.getParameter("action");

			if (action.equalsIgnoreCase("listarIntegracaoWinker"))
			{
				Long cgcCpf = 0L;
				if (request.getParameter("cgcCpf") != null && !request.getParameter("cgcCpf").equals(""))
				{
					try
					{
						cgcCpf = Long.parseLong(request.getParameter("cgcCpf"));
						this.listarIntegracaoWinker(response, cgcCpf);
					}
					catch (Exception e)
					{
						JSONObject jsonPortalWinker = new JSONObject();
						jsonPortalWinker.put("msg", "O campo CNPJ/CPF possui caracteres invalidos, por gentileza utilizar somente numeros!");
						jsonPortalWinker.put("status", 99L);
						out.print(jsonPortalWinker.toString());
						out.flush();
						out.close();
					}

				}
			}

			if (action.equalsIgnoreCase("pesquisarClienteSapiens"))
			{
				String nomeCliente = "";
				nomeCliente = request.getParameter("nomeCliente");
				if (nomeCliente.length() >= 3)
				{
					if (request.getParameter("nomeCliente") != null && !request.getParameter("nomeCliente").equals(""))
					{
						this.pesquisarClienteSapiens(response, nomeCliente);
					}
				}
				else
				{
					JSONObject jsonPortalWinker = new JSONObject();
					jsonPortalWinker.put("msg", "Digite um Texto maior para uma pesquisa mais precisa!");
					jsonPortalWinker.put("status", 80L);
					out.print(jsonPortalWinker.toString());
					out.flush();
					out.close();
				}

			}
			if (action.equalsIgnoreCase("listarSindico"))
			{
				Long cgcCpf = 0L;
				if (request.getParameter("cgcCpf") != null && !request.getParameter("cgcCpf").equals(""))
				{
					cgcCpf = Long.parseLong(request.getParameter("cgcCpf"));
					this.listarSindico(response, cgcCpf);
				}
			}
			
			if (action.equalsIgnoreCase("integrarWinker"))
			{
				Long cgcCpf = 0L;
				if (request.getParameter("cgcCpf") != null && !request.getParameter("cgcCpf").equals(""))
				{
					cgcCpf = Long.parseLong(request.getParameter("cgcCpf"));
					this.integrarWinker(cgcCpf);
				}
			}

			if (action.equalsIgnoreCase("listarColaboradores"))
			{
				Long cgcCpf = 0L;
				if (request.getParameter("cgcCpf") != null && !request.getParameter("cgcCpf").equals(""))
				{
					cgcCpf = Long.parseLong(request.getParameter("cgcCpf"));
					this.listarColaboradores(response, cgcCpf);
				}
			}

			if (action.equalsIgnoreCase("integrarPortal"))
			{
				Long cgcCpf = 0L;
				if (request.getParameter("cgcCpf") != null && !request.getParameter("cgcCpf").equals(""))
				{
					cgcCpf = Long.parseLong(request.getParameter("cgcCpf"));
					this.integrarPortal(response, cgcCpf);
				}
			}

			if (action.equalsIgnoreCase("salvarDadosSindico"))
			{
				String nomSin = "";
				String emaSin = "";
				String cpfSin = "";
				String cpfCnpj = "";

				if (request.getParameter("nomSin") != null)
				{
					nomSin = request.getParameter("nomSin");
				}
				if (request.getParameter("emaSin") != null)
				{
					emaSin = request.getParameter("emaSin");
				}
				if (request.getParameter("cpfSin") != null)
				{
					cpfSin = request.getParameter("cpfSin");
				}

				if (request.getParameter("cpfCnpj") != null)
				{
					cpfCnpj = request.getParameter("cpfCnpj");
				}

				this.salvarDadosSindico(response, nomSin, emaSin, cpfSin, cpfCnpj);
			}

			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void salvarDadosSindico(HttpServletResponse response, String nomSin, String emaSin, String cpfSin, String cpfCnpj) throws JSONException, IOException
	{
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");
		PreparedStatement stIntegracao = null;

		StringBuffer sql = new StringBuffer();
		JSONObject jsonPortalWinker = new JSONObject();

		sql.append("UPDATE E085CLI SET USU_EMASIN = ?, USU_CPFSIN = ?, USU_NOMSIN = ? WHERE CGCCPF = ?");
		try
		{
			stIntegracao = connSapiens.prepareStatement(sql.toString());
			stIntegracao.setString(1, emaSin);
			stIntegracao.setString(2, cpfSin);
			stIntegracao.setString(3, nomSin);
			stIntegracao.setString(4, cpfCnpj);
			int re = stIntegracao.executeUpdate();

			if (re > 0)
			{
				jsonPortalWinker.put("msg", "Sindico incluido na fila de integração com sucesso! Em instantes, estará disponivel para consulta.");
				jsonPortalWinker.put("status", 100L);
			}
			else
			{
				jsonPortalWinker.put("msg", "Nenhum dado a ser atualizado.");
				jsonPortalWinker.put("status", 102L);
			}

		}
		catch (SQLException e)
		{
			jsonPortalWinker.put("msg", "Erro ao tentar integrar o cliente " + cpfCnpj);
			jsonPortalWinker.put("status", 99L);
			e.printStackTrace();
		}
		PrintWriter out = response.getWriter();
		out.print(jsonPortalWinker.toString());
		out.flush();
		out.close();
	}

	public void listarIntegracaoWinker(HttpServletResponse response, Long cgcCpf) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();
		Connection conn2 = PersistEngine.getConnection("SAPIENS");
		PreparedStatement stConsultaCliente = null;
		ResultSet rsConsultaCliente = null;
		boolean clienteExiste = false;
		Long codcli = 0L;
		JSONObject jsonPortalWinker = new JSONObject();
		StringBuffer sqlConsultaCliente = new StringBuffer();

		sqlConsultaCliente.append("SELECT codcli FROM E085cli where cgccpf = ?");

		try
		{
			stConsultaCliente = conn2.prepareStatement(sqlConsultaCliente.toString());
			stConsultaCliente.setString(1, String.valueOf(cgcCpf));
			rsConsultaCliente = stConsultaCliente.executeQuery();

			if (rsConsultaCliente.next())
			{
				clienteExiste = true;
				codcli = rsConsultaCliente.getLong("codcli");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (clienteExiste)
		{
			Connection conn = PersistEngine.getConnection("SERVICES_PROD");
			PreparedStatement stIntegracao = null;
			ResultSet rsIntegracao = null;

			StringBuffer sql = new StringBuffer();

			sql.append("SELECT  w.name,address,city,w.dt_cadastro,dt_protocolo,w.manager,w.retorno_integracao,w.tentativas_integracao FROM PORTAL_WINKER w WHERE EXTERNAL_ID = ? order by EXTERNAL_ID ");

			try
			{
				stIntegracao = conn.prepareStatement(sql.toString());
				stIntegracao.setString(1, String.valueOf(cgcCpf));
				rsIntegracao = stIntegracao.executeQuery();

				if (rsIntegracao.next())
				{

					jsonPortalWinker.put("name", rsIntegracao.getString("name"));
					jsonPortalWinker.put("city", rsIntegracao.getString("city"));
					jsonPortalWinker.put("address", rsIntegracao.getString("address"));

					if (rsIntegracao.getTimestamp("dt_cadastro") != null)
					{
						GregorianCalendar dtCadastro = new GregorianCalendar();
						dtCadastro.setTime(rsIntegracao.getTimestamp("dt_cadastro"));
						jsonPortalWinker.put("dt_cadastro", NeoDateUtils.safeDateFormat(dtCadastro, "dd/MM/yyyy HH:mm"));
					}
					else
					{
						jsonPortalWinker.put("dt_cadastro", "");
					}

					if (rsIntegracao.getTimestamp("dt_protocolo") != null)
					{
						GregorianCalendar dtProtocolo = new GregorianCalendar();
						dtProtocolo.setTime(rsIntegracao.getTimestamp("dt_protocolo"));
						jsonPortalWinker.put("dt_protocolo", NeoDateUtils.safeDateFormat(dtProtocolo, "dd/MM/yyyy HH:mm"));
					}
					else
					{
						jsonPortalWinker.put("dt_protocolo", "");
					}

					jsonPortalWinker.put("manager", rsIntegracao.getString("manager"));

					if (rsIntegracao.getString("retorno_integracao") != null)
					{
						jsonPortalWinker.put("retorno_integracao", rsIntegracao.getString("retorno_integracao"));
					}
					else
					{
						jsonPortalWinker.put("retorno_integracao", "");
					}

					jsonPortalWinker.put("tentativas_integracao", rsIntegracao.getLong("tentativas_integracao"));
					jsonPortalWinker.put("msg", "Dados do portal para o cnpf/cpf " + cgcCpf + " carregados com sucesso!");
					jsonPortalWinker.put("status", 100L);

				}
				else
				{
					jsonPortalWinker.put("msg", "Cliente não integrado " + cgcCpf);
					jsonPortalWinker.put("status", 102L);
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			jsonPortalWinker.put("msg", "Não foi encontrado um cliente valido para o CNPJ/CPF " + cgcCpf);
			jsonPortalWinker.put("status", 101L);
		}

		System.out.println(jsonPortalWinker.toString());

		out.print(jsonPortalWinker.toString());
		out.flush();
		out.close();
	}

	public void listarSindico(HttpServletResponse response, Long cgcCpf) throws IOException
	{
		PrintWriter out = response.getWriter();
		Connection conn = PersistEngine.getConnection("SERVICES_PROD");
		PreparedStatement stIntegracao = null;
		ResultSet rsIntegracao = null;

		StringBuffer sql = new StringBuffer();

		sql.append("SELECT (first_name+' '+last_name) as nomeCompleto,email,phone,dt_cadastro,dt_integracao,tentativas_integracao,retorno_integracao FROM SINDICO_WINKER WHERE PORTAL = ? order by PORTAL ");
		JSONArray jsonPortais = new JSONArray();

		try
		{
			stIntegracao = conn.prepareStatement(sql.toString());
			stIntegracao.setString(1, String.valueOf(cgcCpf));
			rsIntegracao = stIntegracao.executeQuery();

			while (rsIntegracao.next())
			{
				if (!rsIntegracao.getString("nomeCompleto").trim().equals(""))
				{
					JSONObject jsonPortalWinker = new JSONObject();
					jsonPortalWinker.put("nomeCompleto", rsIntegracao.getString("nomeCompleto"));
					jsonPortalWinker.put("email", rsIntegracao.getString("email"));
					jsonPortalWinker.put("phone", rsIntegracao.getString("phone"));
					if (rsIntegracao.getTimestamp("dt_cadastro") != null)
					{
						GregorianCalendar dtCadastro = new GregorianCalendar();
						dtCadastro.setTime(rsIntegracao.getTimestamp("dt_cadastro"));
						jsonPortalWinker.put("dt_cadastro", NeoDateUtils.safeDateFormat(dtCadastro, "dd/MM/yyyy HH:mm"));
					}
					else
					{
						jsonPortalWinker.put("dt_cadastro", "");
					}

					if (rsIntegracao.getTimestamp("dt_integracao") != null)
					{
						GregorianCalendar dtIntegracao = new GregorianCalendar();
						dtIntegracao.setTime(rsIntegracao.getTimestamp("dt_integracao"));
						jsonPortalWinker.put("dt_integracao", NeoDateUtils.safeDateFormat(dtIntegracao, "dd/MM/yyyy HH:mm"));
					}
					else
					{
						jsonPortalWinker.put("dt_integracao", "");

					}
					if (rsIntegracao.getString("retorno_integracao") != null)
					{
						jsonPortalWinker.put("retorno_integracao", rsIntegracao.getString("retorno_integracao"));
					}
					else
					{
						jsonPortalWinker.put("retorno_integracao", "");
					}

					jsonPortalWinker.put("tentativas_integracao", rsIntegracao.getLong("tentativas_integracao"));
					jsonPortalWinker.put("msg", "Dados do sindico do portal " + cgcCpf + " carregados com sucesso!");
					jsonPortalWinker.put("status", 100L);

					jsonPortais.put(jsonPortalWinker);
					System.out.println(jsonPortalWinker.toString());
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		out.print(jsonPortais.toString());
		out.flush();
		out.close();
	}

	public void listarColaboradores(HttpServletResponse response, Long cgcCpf) throws IOException
	{
		PrintWriter out = response.getWriter();
		Connection conn = PersistEngine.getConnection("SERVICES_PROD");
		PreparedStatement stIntegracao = null;
		ResultSet rsIntegracao = null;

		StringBuffer sql = new StringBuffer();

		sql.append("SELECT  (first_name+' '+last_name) as nomeCompleto,occupation,doc_cpf,dt_cadastro FROM COLABORADOR_WINKER WHERE PORTAL_ID = ? order by PORTAL");
		JSONArray jsonPortais = new JSONArray();

		try
		{
			stIntegracao = conn.prepareStatement(sql.toString());
			stIntegracao.setString(1, String.valueOf(cgcCpf));
			rsIntegracao = stIntegracao.executeQuery();

			while (rsIntegracao.next())
			{
				JSONObject jsonPortalWinker = new JSONObject();
				jsonPortalWinker.put("nomeCompleto", rsIntegracao.getString("nomeCompleto"));
				jsonPortalWinker.put("occupation", rsIntegracao.getString("occupation"));
				String cpf = rsIntegracao.getString("doc_cpf");
				while (cpf.length() < 11)
				{
					cpf = "0" + cpf;
				}

				jsonPortalWinker.put("doc_cpf", cpf);
				GregorianCalendar dtCadastro = new GregorianCalendar();
				dtCadastro.setTime(rsIntegracao.getTimestamp("dt_cadastro"));
				jsonPortalWinker.put("dt_cadastro", NeoDateUtils.safeDateFormat(dtCadastro, "dd/MM/yyyy HH:mm"));
				jsonPortalWinker.put("msg", "Colaborador(es) do portal " + cgcCpf + " carregado(s) com sucesso!");
				jsonPortalWinker.put("status", 100L);
				jsonPortais.put(jsonPortalWinker);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		out.print(jsonPortais.toString());
		out.flush();
		out.close();
	}

	public void pesquisarClienteSapiens(HttpServletResponse response, String nomeCliente) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();
		JSONObject jsonCliente = null;

		Connection conn = PersistEngine.getConnection("SAPIENS");
		PreparedStatement stConsultaCliente = null;
		ResultSet rsConsultaCliente = null;
		StringBuffer sqlConsultaCliente = new StringBuffer();
		JSONArray jsonClientes = new JSONArray();
		int i = 0;

		sqlConsultaCliente.append("SELECT codcli,nomcli,apecli,cgccpf,tipcli,EndCli,BaiCli,CidCli,CplEnd,NenCli FROM E085cli where nomcli like ?");

		try
		{
			stConsultaCliente = conn.prepareStatement(sqlConsultaCliente.toString());
			stConsultaCliente.setString(1, "%" + nomeCliente + "%");
			rsConsultaCliente = stConsultaCliente.executeQuery();

			while (rsConsultaCliente.next())
			{
				i = 1;
				jsonCliente = new JSONObject();
				jsonCliente.put("codcli", rsConsultaCliente.getLong("codcli"));
				jsonCliente.put("nomcli", rsConsultaCliente.getString("nomcli"));
				jsonCliente.put("apecli", rsConsultaCliente.getString("apecli"));
				jsonCliente.put("cgccpf", rsConsultaCliente.getLong("cgccpf"));
				jsonCliente.put("tipcli", rsConsultaCliente.getString("tipcli"));
				jsonCliente.put("EndCli", OrsegupsUtils.removeAccents(rsConsultaCliente.getString("EndCli")));
				jsonCliente.put("BaiCli", rsConsultaCliente.getString("BaiCli"));
				jsonCliente.put("CidCli", rsConsultaCliente.getString("CidCli"));
				jsonCliente.put("CplEnd", rsConsultaCliente.getString("CplEnd"));
				jsonCliente.put("NenCli", rsConsultaCliente.getString("NenCli"));

				jsonCliente.put("msg", "Consulta realizado com sucesso!");
				jsonCliente.put("status", 100L);
				jsonClientes.put(jsonCliente);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (i == 0)
		{
			jsonCliente = new JSONObject();
			jsonCliente.put("codcli", "");
			jsonCliente.put("nomcli", "");
			jsonCliente.put("apecli", "");
			jsonCliente.put("cgccpf", "");
			jsonCliente.put("tipcli", "");
			jsonCliente.put("msg", "Nenhum resultado encontrato!");
			jsonCliente.put("status", 101L);
			jsonClientes.put(jsonCliente);
		}

		out.print(jsonClientes.toString());
		out.flush();
		out.close();

	}

	public void integrarPortal(HttpServletResponse response, Long cgcCpf) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();
		JSONObject jsonPortalWinker = new JSONObject();

		Connection conn = PersistEngine.getConnection("SAPIENS");
		PreparedStatement stConsultaCliente = null;
		ResultSet rsConsultaCliente = null;
		boolean clienteExiste = false;
		Long codcli = 0L;
		StringBuffer sqlConsultaCliente = new StringBuffer();

		sqlConsultaCliente.append("SELECT codcli FROM E085cli where cgccpf = ?");

		try
		{
			stConsultaCliente = conn.prepareStatement(sqlConsultaCliente.toString());
			stConsultaCliente.setString(1, String.valueOf(cgcCpf));
			rsConsultaCliente = stConsultaCliente.executeQuery();

			if (rsConsultaCliente.next())
			{
				clienteExiste = true;
				codcli = rsConsultaCliente.getLong("codcli");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (clienteExiste)
		{
			Connection connSapiens = PersistEngine.getConnection("SAPIENS");
			PreparedStatement stIntegracao = null;

			StringBuffer sql = new StringBuffer();

			sql.append("UPDATE USU_T160CTR SET USU_INTWIN = 'S' WHERE USU_CODCLI = ? AND USU_SITCTR = 'A' ");
			try
			{
				stIntegracao = connSapiens.prepareStatement(sql.toString());
				stIntegracao.setLong(1, codcli);
				int re = stIntegracao.executeUpdate();

				if (re > 0)
				{
					jsonPortalWinker.put("msg", "Portal incluido na fila de integração com sucesso! Em instantes, estará disponivel para consulta.");
					jsonPortalWinker.put("status", 100L);
				}
				else
				{
					jsonPortalWinker.put("msg", "Nenhum contrato encontrado para atualizar.");
					jsonPortalWinker.put("status", 102L);
				}

			}
			catch (SQLException e)
			{
				jsonPortalWinker.put("msg", "Erro ao tentar integrar o cliente " + cgcCpf);
				jsonPortalWinker.put("status", 99L);
				e.printStackTrace();
			}
		}
		else
		{
			jsonPortalWinker.put("msg", "Não foi encontrado um cliente valido para o CNPJ/CPF " + cgcCpf);
			jsonPortalWinker.put("status", 101L);
		}

		out.print(jsonPortalWinker.toString());
		out.flush();
		out.close();

	}

	public boolean validarExisteColaboradorLotado(Long cgcCpf)
	{
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct(orn.usu_cgccpf), orn.usu_codclisap, case when usu_codclisap = 26358 then 'ORSEGUPS SEDE' else cli.apecli end as apecli,  ");
		sql.append("  cli.EndCli , cli.NenCli as usu_endloc, cli.CepCli as usu_endcep,  cli.BaiCli as bairro, cli.CidCli as nomcid, cli.SigUfs as estcid,   ");
		sql.append("  orn.numloc, cli.usu_nomsin, cli.usu_cpfsin, cli.usu_emasin, cli.usu_fcnsin     ");
		sql.append("  from R016ORN orn ");
		sql.append("  inner join Sapiens.dbo.e085cli cli on orn.usu_codclisap = cli.codcli   ");
		sql.append("  inner join usu_t038cvs cvs on cvs.usu_sitcvs = 'S' and cvs.usu_numloc = orn.numloc and usu_seqalt = (select max(x.usu_seqalt) from usu_t038cvs x where x.usu_numloc = orn.numloc) and orn.usu_sitati = 'S' ");
		sql.append("  left join r074cid cid on cid.CodCid = orn.usu_codcid ");
		sql.append("  where cli.CgcCpf = ? and exists (select 1 from SAPIENS.dbo.usu_t160ctr ctr where ctr.usu_codcli = orn.usu_codclisap and ctr.usu_numctr = orn.usu_numctr and ctr.usu_codemp = orn.usu_numemp and  usu_sitctr = 'A' and usu_intwin = 'S'");
		sql.append("  and usu_codclisap not in (26358) or (usu_codclisap = 26358  and orn.numloc in (61396, 26151,25345,46498,46503,46495)))  ");

		Connection conn = PersistEngine.getConnection("VETORH");
		PreparedStatement stConsultaCliente = null;
		ResultSet rsConsultaCliente = null;

		try
		{
			stConsultaCliente = conn.prepareStatement(sql.toString());
			stConsultaCliente.setString(1, String.valueOf(cgcCpf));
			rsConsultaCliente = stConsultaCliente.executeQuery();

			if (rsConsultaCliente.next())
			{
				return true;
			}
			else
			{
				return false;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

		}
		return false;
	}

	public void integrarWinker(Long cgcCpf) throws IOException, JSONException
	{
		//String url = "http://dsoo46:8081/services/winker/integration/autorization/integrarportal";
		//String url = "http://apps.orsegups.com.br:8280/services/winker-integration-autorization-integrarportal";
		String url = "https://apps.orsegups.com.br:8243/services/winker-integration-autorization-integrarportal";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("X-Auth-Token", "499c7a9a40f3bcda2c54471d03f494dc");

		String urlParameters = "cnpj=" + cgcCpf;
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));

		writer.write(urlParameters);
		writer.flush();
		writer.close();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null)
		{
			response.append(inputLine);
		}
		in.close();
	}

}
