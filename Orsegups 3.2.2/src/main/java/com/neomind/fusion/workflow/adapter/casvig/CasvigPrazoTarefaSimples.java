package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.apache.camel.spi.LifecycleStrategy;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityInfo;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

import sun.util.calendar.Gregorian;

public class CasvigPrazoTarefaSimples implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity) throws ActivityException, TaskException
	{

		try
		{

			NeoUser executor = (NeoUser) processEntity.findGenericValue("Executor");
			String codeExecutor = executor.getCode();
			
			List<NeoObject> listaAgenda = PersistEngine.getObjects(AdapterUtils.getEntityClass("agendaConsultoriaTI"), 
					new QLEqualsFilter("userCode", codeExecutor), "dataAgenda");
			
			if(listaAgenda.size() > 0) {
				
				GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();
				boolean temAgenda = false;
				for(NeoObject eformAgenda : listaAgenda) {
					EntityWrapper ewAgenda = new EntityWrapper(eformAgenda);
					GregorianCalendar diaAgenda = ewAgenda.findGenericValue("dataAgenda");
					if(prazo.equals(diaAgenda)) {
						temAgenda = true;
						break;
					}
				}
				
				if(!temAgenda) {
					for (NeoObject eformAgenda : listaAgenda) {
						EntityWrapper ewAgenda = new EntityWrapper(eformAgenda);
						GregorianCalendar proximaAgenda = ewAgenda.findGenericValue("dataAgenda");
						if(prazo.compareTo(proximaAgenda) < 0) {
							throw new WorkflowException("Data informada não disponível. Por gentileza ajustar prazo da tarefa para o dia "+ NeoDateUtils.safeDateFormat(proximaAgenda, "dd/MM/yyyy"));
						}
					}
					throw new WorkflowException("Consultor sem agenda no prazo definido. Favor consultar a agenda do mesmo.");
				}
			}
			
			String atividadeAnterior = origin != null ? origin.getInstance().getActivity().getActivityName() : "";

			Boolean diretorReenviou = processEntity.findGenericValue("diretorReenviou");

			NeoUser userExec = (NeoUser) (origin != null ? origin.getInstance().getOwner() : processEntity.findGenericValue("Solicitante"));

			Boolean eDiretor = Boolean.FALSE;

			Set<NeoPaper> papeisUserExec = userExec.getPapers();

			for (NeoPaper papelUserExec : papeisUserExec)
			{

				String paperName = papelUserExec.getName();

				if (paperName.contains("Diretor "))
					eDiretor = Boolean.TRUE;

			}

			if (NeoUtils.safeIsNotNull(atividadeAnterior) && atividadeAnterior.contains("Realizar tarefa - Escalada") && eDiretor && !atividadeAnterior.contains("Último Nível"))
			{

				if (diretorReenviou != null && diretorReenviou)
				{

					throw new WorkflowException("NÃO É POSSÍVEL REENVIAR TAREFA POIS JÁ EXISTEM REGISTROS DE REENVIO POR DIRETOR!");

				}
				else
				{

					GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
					GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();

					if (!OrsegupsUtils.isWorkDay(prazo))
					{
						throw new WorkflowException("Prazo informado deve ter um dia útil!");
					}

					if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
					{
						throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
					}

					processEntity.findField("diretorReenviou").setValue(Boolean.TRUE);

				}

			}
			else
			{

				GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
				GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();

				if (!OrsegupsUtils.isWorkDay(prazo))
				{
					throw new WorkflowException("Prazo informado deve ter um dia útil!");
				}

				if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
				{
					throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
				}

			}

			Boolean usuarioPermitido = false;
			
			Boolean ajustarExecutor = processEntity.findGenericValue("ajustarExecutor");
			if ((ajustarExecutor == null || !ajustarExecutor) && !(userExec.getGroup().getName().equals("Supervisão Sustentação de Sistemas") || userExec.getGroup().getName().equals("Supervisão de Desenvolvimento de Sistemas") || userExec.getGroup().getName().equals("Supervisão de Infraestrutura de TI") || userExec.getGroup().getName().equals("Coordenação de Sustentação de Sistemas") || userExec.getGroup().getName().equals("Coordenação de Infraestrutura de TI") || userExec.getGroup().getName().equals("Gerência de Tecnologia da Informação")))
			{
				if (!executor.getCode().equals("suporte.sistemas") && (executor.getGroup().getName().equals("Supervisão Sustentação de Sistemas") || executor.getGroup().getName().equals("Sins") || executor.getGroup().getName().equals("Supervisão de Desenvolvimento de Sistemas")))
				{
					List<NeoObject> listRegistroAtividades = processEntity.findGenericValue("registroAtividades");
					if (listRegistroAtividades != null && !listRegistroAtividades.isEmpty()) 
					{
						for (NeoObject noRegistroAtividade : listRegistroAtividades)
						{
							EntityWrapper wRegistroAtividade = new EntityWrapper(noRegistroAtividade);
							
							NeoUser nuResponsavel = wRegistroAtividade.findGenericValue("responsavel");
							if (userExec.equals(nuResponsavel))
							{
								usuarioPermitido = true;
								break;
							}
						}
					}
					
					if (!usuarioPermitido)
						throw new WorkflowException("Usuário não disponível para abertura de tarefas. Por gentileza ajustar o executor para  Suporte Sistemas");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw (WorkflowException) e;
		}
	}

}