package com.neomind.fusion.workflow.Job.Job;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.mail.EmailException;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class VerificaJobComErro implements CustomJobAdapter

{

	@Override
	public void execute(CustomJobContext ctx)
	{
		System.out.println("[JOB] - Inicia Verificação de Job's com erro");
		processaJob(ctx);
	}

	public static void processaJob(CustomJobContext ctx)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		conn = PersistEngine.getConnection("");

		try
		{
			sql.append(" Select ");
			sql.append(" Case ");
			sql.append(" When sch.schedulerType = 1 Then 'Por Hora' ");
			sql.append(" When sch.schedulerType = 2 Then 'Todos os Dias da Semana' ");
			sql.append(" When sch.schedulerType = 3 Then 'Diário' ");
			sql.append(" When sch.schedulerType = 4 Then 'Semanal' ");
			sql.append(" When sch.schedulerType = 5 Then 'Mensal' ");
			sql.append(" When sch.schedulerType = 6 Then 'Anual' ");
			sql.append(" When sch.schedulerType = 7 Then 'Por Minuto' ");
			sql.append(" End As repeticao,log.firedTime As date, ");
			sql.append(" Case ");
			sql.append("	When log.state = 2 Then 'JOB_EXECUTIONVETOED' ");
			sql.append("	When log.state = 6 Then 'JOB_WASEXECUTED_EXCEPTION' ");
			sql.append("	When log.state = 7 Then 'JOB_EXECUTION_EXCEPTION' ");
			sql.append("	When log.state = 10 Then 'TRIGGER_MISFIRED' ");
			sql.append("End As error, job.name, log.details,log.state,convert(varchar,job.prevFireTime,103) as ultima_execucao,convert(varchar,job.nextFireTime,103) as proxima_execucao ");
			sql.append(" From [cacupe\\sql02].fusion_producao.dbo.Job job With (NOLOCK) ");
			sql.append(" Inner join [cacupe\\sql02].fusion_producao.dbo.JobEventLog log With (NOLOCK) on log.job_neoId = job.neoId ");
			sql.append(" Inner join [cacupe\\sql02].fusion_producao.dbo.NeoTrigger tri With (NOLOCK) on tri.neoId = log.trigger_neoId ");
			sql.append(" Inner join [cacupe\\sql02].fusion_producao.dbo.Scheduler sch With (NOLOCK) on sch.neoid = tri.scheduler_neoId ");
			sql.append(" Where ");
			sql.append(" ( (log.firedTime >= DATEADD(HH,-2,GETDATE()) And log.firedTime < GETDATE() and log.state in (2,6,7,10)) ");
			sql.append("  or ( (job.nextFireTime is null or job.nextFireTime = '1900-01-01 00:00:00.000'))  or (job.nextFireTime < DATEADD(HH,-1,GETDATE()))) and job.name not like 'Abre tarefa simples Job% com problemas' and job.enabled = 1");
			sql.append(" Order By log.firedTime Desc, ");
			sql.append(" job.neoid, ");
			sql.append(" log.state ");
			pstm = conn.prepareStatement(sql.toString());
			String msgEmail = "";
			rs = pstm.executeQuery();
			String tarefa = "";
			boolean enviaEmail = false;

			while (rs.next())
			{
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				NeoUser executor = new NeoUser();
				String solicitante = "graziela.martins";
				String titulo = "";
				String descricao = "";
				String erro = "";
				tarefa = "";
				GregorianCalendar prazo = new GregorianCalendar();

				if (rs.getString("proxima_execucao") == null || rs.getString("proxima_execucao").equals("01/01/1900"))
				{
					titulo = "Verificar o erro no job " + rs.getString("name") + "  pois esta sem data da proxima execução";
				}
				else
				{
					titulo = "Verificar o erro no job " + rs.getString("name");
				}
				titulo = "Verificar o erro no job " + rs.getString("name");
				descricao = "<b>Vefificar o problema que ocorreu no Job a seguir:</b><br><br>";
				descricao += " <b>Nome do Job: </b>" + rs.getString("name") + "<br>";
				descricao += " <b>Ultima execução: </b>" + rs.getString("ultima_execucao") + "<br>";
				descricao += " <b>Próxima execução: </b>" + rs.getString("proxima_execucao") + "<br>";
				descricao += " <b>Periodicidade: </b>" + rs.getString("repeticao") + "<br>";
				descricao += " <b>Erro: " + rs.getString("details");

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				QLEqualsFilter nameJob = new QLEqualsFilter("nameJob", rs.getString("name"));
				groupFilter.addFilter(nameJob);
				//Corrigir nome do e-form antes de colocar em produção
				List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("responsaveiselJob"), groupFilter);
				EntityWrapper Wrapper = null;
				if (objs != null && objs.size() > 0)
				{
					Wrapper = new EntityWrapper(objs.get(0));
					executor = (NeoUser) Wrapper.findField("responsavel").getValue();
				}
				else
				{
					executor = null;
				}

				if (executor != null)
				{
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
					String historicoTarefa = verificaTarefasAbertas(rs.getString("name"));
					if (historicoTarefa == null)
					{
						tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor.getCode(), titulo, descricao, "1", "sim", prazo);
					}
					else
					{
						System.out.println("###TAREFA NÃO ABERTA: O Job " + rs.getString("name") + " existe a tarefa " + historicoTarefa + " aberta para a verificação.");
					}
				}
				else
				{
					System.out.println("##ERRO TAREFA NÃO ABERTA: O job " + rs.getString("name") + " não possui um executor definido no e-form responsaveiselJob");
				}

				if (!tarefa.equals("") && tarefa != null)
				{
					InstantiableEntityInfo tarefaJob = AdapterUtils.getInstantiableEntityInfo("tarefasJob");
					NeoObject objJob = tarefaJob.createNewInstance();
					EntityWrapper WrapperJob = new EntityWrapper(objJob);

					GregorianCalendar date = new GregorianCalendar();

					date.set(GregorianCalendar.HOUR, 0);
					date.set(GregorianCalendar.HOUR_OF_DAY, 0);
					date.set(GregorianCalendar.MINUTE, 0);
					date.set(GregorianCalendar.SECOND, 0);
					date.set(GregorianCalendar.MILLISECOND, 0);

					WrapperJob.findField("startDate").setValue(date);
					WrapperJob.findField("tarefa").setValue(tarefa);
					WrapperJob.findField("executor").setValue(executor);
					WrapperJob.findField("nomeJob").setValue(rs.getString("name"));
					if (rs.getString("details") == null)
					{
						WrapperJob.findField("erro").setValue("nulo");
					}
					else
					{
						WrapperJob.findField("erro").setValue(rs.getString("details"));
					}
					WrapperJob.findField("periodicidade").setValue(rs.getString("repeticao"));
					if (rs.getString("proxima_execucao") == null)
					{
						WrapperJob.findField("proExecucao").setValue(null);

					}
					else
					{
						GregorianCalendar proExe = OrsegupsUtils.stringToGregorianCalendar(rs.getString("proxima_execucao"), "dd/MM/yyyy");
						WrapperJob.findField("proExecucao").setValue(proExe);
					}

					if (rs.getString("ultima_execucao") == null)
					{
						WrapperJob.findField("ultExecucao").setValue(null);
					}
					else
					{
						GregorianCalendar ultExe = OrsegupsUtils.stringToGregorianCalendar(rs.getString("ultima_execucao"), "dd/MM/yyyy");
						WrapperJob.findField("ultExecucao").setValue(ultExe);
					}

					PersistEngine.persist(objJob);
					enviaEmail = true;
				}
			}
			if (enviaEmail)
			{
				encaminharEmail();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static String verificaTarefasAbertas(String nameJob)
	{

		QLEqualsFilter name = new QLEqualsFilter("nomeJob", nameJob);
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(name);
		String tarefas = "";

		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("tarefasJob"), groupFilter);
		Long cont = 0L;
		for (NeoObject neoObject : objs)
		{
			if (cont > 0)
			{
				tarefas += ",";
			}
			EntityWrapper w = new EntityWrapper(neoObject);
			tarefas += (String) w.findField("tarefa").getValue();
			cont++;

		}
		Connection conn = null;
		ResultSet rs = null;
		StringBuilder sqltarefa = new StringBuilder();
		if (!tarefas.equals("") && tarefas != null)
		{
			sqltarefa.append("select processState,saved,code from WFProcess With (NOLOCK) where code in(" + tarefas + ") and processState = 0 and saved = 1");

			conn = PersistEngine.getConnection("");
			PreparedStatement pst = null;
			try
			{
				pst = conn.prepareStatement(sqltarefa.toString());
				rs = pst.executeQuery();
				while (rs.next())
				{
					return rs.getString("code");
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void encaminharEmail() throws EmailException
	{
		GregorianCalendar date = new GregorianCalendar();
		date.set(GregorianCalendar.HOUR, 0);
		date.set(GregorianCalendar.HOUR_OF_DAY, 0);
		date.set(GregorianCalendar.MINUTE, 0);
		date.set(GregorianCalendar.SECOND, 0);
		date.set(GregorianCalendar.MILLISECOND, 0);
		QLOpFilter dataFilter = new QLOpFilter("startDate", "=", (GregorianCalendar) date);
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(dataFilter);
		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("tarefasJob"), groupFilter);
		String nomeJob = "";
		GregorianCalendar ultExecucao = null;
		GregorianCalendar proExecucao = null;
		String periodicidade = "";
		String erro = "";
		String tarefa = "";

		StringBuilder html = new StringBuilder();
		html.append("<html>");
		html.append("<center><h1> Tarefas de Job's com problemas </h1></center>");
		html.append("<br/><br/>");
		html.append(" Segue relação de tarefas abertas no dia de hojé referente aos job's com problemas ");
		html.append("<br/>");
		html.append("<TABLE BORDER=1 style=\"table-layout: fixed; width: 1200px; overflow: hidden;\">");
		html.append("<TR>");
		html.append("<TD WIDTH=15%>NOME DO JOB </TD>");
		html.append("<TD WIDTH=7%>ULTIMA EXECUÇÃO</TD>");
		html.append("<TD WIDTH=7%>PROXIMA EXECUÇÃO </TD>");
		html.append("<TD WIDTH=7%>PERIODICIDADE</TD>");
		html.append("<TD WIDTH=12%>ERRO</TD>");
		html.append("<TD WIDTH=4%>Tarefa</TD>");
		html.append("</TR>");
		for (NeoObject neoObject : objs)
		{
			EntityWrapper wr = new EntityWrapper(neoObject);
			nomeJob = (String) wr.findField("nomeJob").getValue();
			ultExecucao = (GregorianCalendar) wr.findField("ultExecucao").getValue();
			proExecucao = (GregorianCalendar) wr.findField("proExecucao").getValue();
			periodicidade = (String) wr.findField("periodicidade").getValue();
			erro = (String) wr.findField("erro").getValue();
			tarefa = (String) wr.findField("tarefa").getValue();

			html.append("<TR>");
			html.append("<TD>" + nomeJob + "</TD>");
			html.append("<TD>" + NeoUtils.safeDateFormat(ultExecucao, "dd/MM/yyyy") + "</TD>");
			html.append("<TD>" + NeoUtils.safeDateFormat(proExecucao, "dd/MM/yyyy") + "</TD>");
			html.append("<TD>" + periodicidade + "</TD>");
			html.append("<TD>" + erro + "</TD>");
			html.append("<TD>" + tarefa + "</TD>");
			html.append("</TR>");
		}
		html.append("</TABLE>");
		html.append("</html>");
		String emailDst = "graziela.martins@orsegups.com.br";
		String emailRemetente = "fusion@orsegups.com.br";
		List<String> comCopiaOc = new ArrayList<String>();
		OrsegupsUtils.sendEmail2Orsegups(emailDst, "fusion@orsegups.com.br", "Tarefas de Job's com problemas", "", html.toString(), emailRemetente, null, comCopiaOc);
	}
}
