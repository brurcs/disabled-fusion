package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

//com.neomind.fusion.workflow.adapter.casvig.AdapterValidaPrazoAguardarEnvioReinspecao
public class AdapterValidaPrazoAguardarEnvioReinspecao implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		GregorianCalendar prazo = null;

		if (origin.getActivityName().toString().contains("Aprovar Resposta - Inconsistência") || origin.getActivityName().toString().contains("Tratar Reinspeção Escalada"))
		{
			if (processEntity.findValue("prazoEnvioReinspecaoEscalada") != null)
			{
				prazo = (GregorianCalendar) processEntity.findField("prazoEnvioReinspecaoEscalada").getValue();
			}
		}
		else
		{
			if (processEntity.findValue("prazoEficaciaEscalada") != null)
			{
				prazo = (GregorianCalendar) processEntity.findField("prazoEficaciaEscalada").getValue();
			}

		}

		if (prazo == null)
		{
			GregorianCalendar novoPrazo = new GregorianCalendar();
			novoPrazo.add(GregorianCalendar.DATE, 7);
			novoPrazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			novoPrazo.set(GregorianCalendar.MINUTE, 59);
			novoPrazo.set(GregorianCalendar.SECOND, 59);
			if (origin.getActivityName().toString().contains("Aprovar Resposta - Inconsistência") || origin.getActivityName().toString().contains("Tratar Reinspeção Escalada"))
			{
				processEntity.setValue("prazoEnvioReinspecaoEscalada",novoPrazo);
			}
			else
			{
				processEntity.setValue("prazoEficaciaEscalada",novoPrazo);
			}
		}
		else
		{
			GregorianCalendar novoPrazo = new GregorianCalendar();
			novoPrazo.add(GregorianCalendar.DATE, 1);
			novoPrazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			novoPrazo.set(GregorianCalendar.MINUTE, 59);
			novoPrazo.set(GregorianCalendar.SECOND, 59);
			if (origin.getActivityName().toString().contains("Reinspeção"))
			{
				processEntity.findField("prazoEnvioReinspecaoEscalada").setValue(novoPrazo);
			}
			else
			{
				processEntity.findField("prazoEficaciaEscalada").setValue(novoPrazo);
			}
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
