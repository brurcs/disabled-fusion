package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class IniciarTarefaSimplesFluxoReciclagemEscalada implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String solicitante = "";
		String executor = "";
		String titulo = "";
		String descricao = "";
		GregorianCalendar dataPrazo = new GregorianCalendar();
		dataPrazo.add(GregorianCalendar.DATE, 3);
		OrsegupsUtils.getNextWorkDay(dataPrazo);
		Long numcad = (Long) processEntity.findValue("numcad");
		Long numemp = (Long) processEntity.findValue("numemp");
		Long tipcol = (Long) processEntity.findValue("tipcol");
		Long codccu = (Long) processEntity.findValue("codccu");
		String nomFun =  (String) processEntity.findValue("nomfun");
		String titcar =  (String) processEntity.findValue("titcar");
		Long numcpf = (Long) processEntity.findValue("numcpf");
		GregorianCalendar datadm = (GregorianCalendar) processEntity.findValue("datadm");
		GregorianCalendar datven = (GregorianCalendar) processEntity.findValue("datVenCur");
		GregorianCalendar dataCurso = (GregorianCalendar) processEntity.findValue("dataCurso");
		
		String nomBai = NeoUtils.safeOutputString(processEntity.findValue("nomBai"));
		String nomCid = NeoUtils.safeOutputString(processEntity.findValue("nomCid"));
		String estCid = NeoUtils.safeOutputString(processEntity.findValue("estCid"));
		String endereco = NeoUtils.safeOutputString(processEntity.findValue("endereco"));
		String numTel = NeoUtils.safeOutputString(processEntity.findValue("numTel"));
		String emapon = NeoUtils.safeOutputString(processEntity.findValue("emapon"));
		
		String obsCurso = NeoUtils.safeOutputString(processEntity.findValue("obsCurso"));
		
		solicitante = "fernanda.martins"; 
		executor = "beatriz.malmann";
		titulo = "Justificar não convocação "  + numemp + "/" + numcad + " - " + nomFun ;
		descricao = "<strong>Justificar não convocação</strong><br><br>";
		descricao += " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomFun + "<br>";
		descricao += " <strong>Cargo:</strong> " + titcar + "<br>";
		descricao += " <strong>Fone:</strong> " + numTel + "<br>";
		descricao += " <strong>Endereço:</strong> " + endereco + "<br>";
		descricao += " <strong>Bairro:</strong> " + nomBai + "<br>";
		descricao += " <strong>Cidade:</strong> " + nomCid + "<br>";
		descricao += " <strong>Estado:</strong> " + estCid + "<br>";
		descricao += " <strong>E-mail:</strong> " + emapon + "<br>";
		descricao += " <strong>Admissão:</strong> " + NeoUtils.safeDateFormat(datadm, "dd/MM/yyyy") + "<br>";
		descricao += " <strong>Vencimento:</strong> " + NeoUtils.safeDateFormat(datven, "dd/MM/yyyy") + "<br>";
		descricao += " <strong>Centro de Custo:</strong> " + codccu + "<br>";
		if(dataCurso != null){
			descricao += " <strong>Data do curso:</strong> " + NeoUtils.safeDateFormat(dataCurso, "dd/MM/yyyy") + "<br>";
		}
		descricao += "<strong>Observação:</strong> " + obsCurso + "<br>";

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		GregorianCalendar prazo = new GregorianCalendar();
		System.out.println("Prazo da tarefa simples de reciclagem: " + NeoDateUtils.safeDateFormat(dataPrazo));
		if (OrsegupsUtils.isWorkDay(dataPrazo)){
			prazo = (GregorianCalendar) dataPrazo.clone();
		}else{
			prazo = OrsegupsUtils.getNextWorkDay(dataPrazo);
			System.out.println("Não é dia util. Processando próxima data util:" + NeoDateUtils.safeDateFormat(prazo));
		}
		
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "2", "sim", prazo);
		processEntity.setValue("tarefaSimples",tarefa);
		
		InstantiableEntityInfo tarefaReciclagem = AdapterUtils.getInstantiableEntityInfo("TarefaAgendamentoReciclagens");
		NeoObject noAJ = tarefaReciclagem.createNewInstance();
		EntityWrapper ajWrapper = new EntityWrapper(noAJ);
		ajWrapper.findField("numcpf").setValue(numcpf);
		ajWrapper.findField("datven").setValue(datven);
		ajWrapper.findField("tarefa").setValue(tarefa);
		PersistEngine.persist(noAJ);
	}	

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
}
