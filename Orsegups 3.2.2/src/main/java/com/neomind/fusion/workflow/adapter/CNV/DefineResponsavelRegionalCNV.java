package com.neomind.fusion.workflow.adapter.CNV;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class DefineResponsavelRegionalCNV implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Long codReg = (Long) processEntity.findValue("codReg");
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		QLEqualsFilter regional = new QLEqualsFilter("codReg", codReg);
		groupFilter.addFilter(regional);
		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("regionaisCNV"), groupFilter);
		EntityWrapper Wrapper = new EntityWrapper(objs.get(0));
		NeoUser responsavelRegional = (NeoUser) Wrapper.findField("usuResp").getValue();
		processEntity.setValue("respReg",responsavelRegional);
		processEntity.setValue("observacao","Favor providenciar a impressão e entrega da CNV ao Vigilante.");
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
