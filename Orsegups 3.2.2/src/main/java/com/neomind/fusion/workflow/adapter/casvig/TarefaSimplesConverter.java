package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

@SuppressWarnings("unused")
public class TarefaSimplesConverter extends DefaultConverter
{
	@Override
	public String getHTMLView(EFormField field, OriginEnum origin)
	{		
		String codProc = null;
		StringBuffer ret = new StringBuffer();	
		
		try
		{
			EForm efPai = field.getForm();
			if (efPai != null)
			{
				EntityWrapper wrapPai = new EntityWrapper(efPai.getObject());
				if (wrapPai != null && wrapPai.findValue("codigoProcesso") != null)
				{
					codProc = wrapPai.findValue("codigoProcesso").toString();						
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}		
		
		if(codProc != null)
		{	
			ret.append("<script type=\"text/javascript\">");
			ret.append("\nfunction mostraPai()");
			ret.append("\n{");		
			ret.append("\nvar url = '" + PortalUtil.getBaseURL() + "portal/render/Form?id=" + codProc + "&type=WFProcess';");
			ret.append("\nvar newModalId = NEO.neoUtils.dialog().addModal(true, null, null, 900, 750, 'Detalhes Processo Original');");
			ret.append("\nNEO.neoUtils.dialog().createModal(url);");
			ret.append("\n}");
			ret.append("\n</script>");
			ret.append("\n<input type=\"button\" class=\"input_button\" value=\"Consultar\"  onClick=\"javascript:mostraPai();\" />");			
		}
		
		return ret.toString();
	}
}
