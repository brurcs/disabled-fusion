package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CasvigDefineAprovadorNivel500 implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		NeoUser usuario = (NeoUser) processEntity.findField("solicitante").getValue();
		NeoGroup grupoAux = null;
		NeoPaper papelAprovador = null;

		if (NeoUtils.safeIsNull(usuario))
		{
			throw new WorkflowException("Não foi possível obter o usuário solicitante.");
		}
		grupoAux = usuario.getGroup();
		if (NeoUtils.safeIsNull(grupoAux))
		{
			throw new WorkflowException("Não foi possível obter o grupo do usuário solicitante.");
		}
		// Se o solicitante for o responsavel pelo departamento
		if (grupoAux.getGroupLevel() >= 500)
		{
			papelAprovador = usuario.getGroup().getResponsible();
			// Se o solicitante for membro de grupo subordinado ao departamento
			// sobe ateh o nivel de departamento para obter o papel responsavel
		}
		else
		{
			while ((grupoAux.getGroupLevel() < 500))
			{
				grupoAux = grupoAux.getUpperLevel();
				if (NeoUtils.safeIsNull(grupoAux))
				{
					throw new WorkflowException("Não foi possível obter a hierarquia de grupo do usuário solicitante.");
				}
			}
			papelAprovador = grupoAux.getResponsible();
		}
		if (NeoUtils.safeIsNull(papelAprovador))
		{
			throw new WorkflowException("Não foi possível determinar o papel responsável do solicitante. ");
		}

		processEntity.setValue("papelAprovador", papelAprovador);
	}
}