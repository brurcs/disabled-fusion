package com.neomind.fusion.workflow.adapter.reciclagem;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoCalendarUtils;

public class AdapterAtualizaDataCurso implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) 
	{
		GregorianCalendar datCor = (GregorianCalendar) processEntity.findValue("dataFimCurso");
		GregorianCalendar datFor = (GregorianCalendar) processEntity.findValue("datfor"); 
		Long cpf = (Long) processEntity.findValue("numcpf");
		GregorianCalendar datVetorBranco = NeoCalendarUtils.stringToDate("31/12/1900");  
		
				
		if (datCor != null)
		{		    
		    	Connection conn = null; 
			String sql = null; 
			PreparedStatement pstm = null; 
			ResultSet rs = null; 
			String mensagemErro = null;
			
			conn = PersistEngine.getConnection("VETORH");
	        	sql = "SELECT NUMEMP, NUMCAD, TIPCOL FROM R034FUN WHERE SITAFA != 7 AND NUMCPF = ? ";
	        	try {
			    pstm = conn.prepareStatement(sql.toString());
	        	    pstm.setLong(1, cpf);
	        	    rs = pstm.executeQuery(); 
	        	    while (rs.next()) {
	        	    
            	        	    Long numemp = (Long) rs.getLong("NUMEMP");  
            	        	    Long numcad = (Long) rs.getLong("NUMCAD");  
            	        	    Long tipcol = (Long) rs.getLong("TIPCOL"); 
        	        	    
        	        	    String query = "";
        			    Connection conn2 = null;
        	        	    
        	        	    query = "UPDATE USU_TCADCUR SET ";	
        
        				if (datFor.equals(datVetorBranco))
        				{
        					//Colaborador que ainda não possui formação, preencher a data do curso.
        					query = query+" USU_DATFOR = ? ";
        				}
        				else
        				{
        					//Colaborador que já possui formação, deve ser atualizado o campo de reciclagem.
        					query = query+" USU_DATREC = ? ";
        				}
        
        				query = query + " WHERE USU_NUMEMP = ? AND USU_NUMCAD =? AND USU_TIPCOL = ?";
        				PreparedStatement pst = null;
        
        				try
        				{
        					conn2 = PersistEngine.getConnection("VETORH");
        					pst = conn2.prepareStatement(query);
        					pst.setDate(1, new Date(datCor.getTimeInMillis()));
        					pst.setLong(2, numemp);
        					pst.setLong(3, numcad);
        					pst.setLong(4, tipcol);
        					pst.executeUpdate();
        				}
        				catch (Exception e)
        				{
        					try
        					{
        						throw new Exception("Erro ao tentar atualizar data do curso do colaborador, por gentileza entrar em contato com a TI.");
        					}
        					catch (Exception e1)
        					{
        						e1.printStackTrace();
        					}
        				}
	        	    	}    
			} catch (Exception e){
			    e.printStackTrace();
			    if(mensagemErro == null){
				mensagemErro = "Erro ao consultar a matrícula ativa do colaborador.";
			    }
			    try {
				throw new Exception(mensagemErro);
			    } catch (Exception e1) {
				e1.printStackTrace();
			    }
			} finally {
			    OrsegupsUtils.closeConnection(conn, pstm, rs); 
			}
			
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	    	/* NADA A FAZER */
	}

}
