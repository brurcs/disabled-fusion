package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PrazoInicialCNV implements AdapterInterface {
    private static final Log log = LogFactory.getLog(PrazoInicialCNV.class);

    public void back(EntityWrapper processEntity, Activity activity) {
    }

    public void start(Task origin, EntityWrapper wrapper, Activity activity) {
	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

	String erro = "Por favor, contatar o administrador do sistema!";
	try {
	    GregorianCalendar data = new GregorianCalendar();
	    data.set(11, 23);
	    data.set(12, 59);
	    data.set(13, 59);

	    // Calcula prazo com 2 dias para próximo dia útil
	    data.add(GregorianCalendar.DATE, 2);
	    while (!OrsegupsUtils.isWorkDay(data)) {
		data = OrsegupsUtils.getNextWorkDay(data);
	    }
	    
	    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	    System.out.println(formato.format(data.getTime()));
	    
	    wrapper.setValue("prazo", data);
	} catch (Exception e) {
	    log.error(erro);
	    e.printStackTrace();
	    throw new WorkflowException(erro);
	}
    }
}