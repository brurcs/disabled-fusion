package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class AdapterCalcularDatasPool implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar datCor = (GregorianCalendar) processEntity.findValue("dataCurso");
		if (datCor != null)
		{
			GregorianCalendar datPoo = (GregorianCalendar) datCor.clone();
			GregorianCalendar datPooCob = (GregorianCalendar) datCor.clone();
			datPoo.add(GregorianCalendar.DATE, 5); // prazo para a tarefa ir para o juliano confirmar se o curso foi feito
			datPooCob.add(GregorianCalendar.DATE,-8); // prazo para abrir a tarefa simples de aviso de cobertura
			
			/*datPoo = new GregorianCalendar();
			datPooCob = new GregorianCalendar();
			datPoo.add(GregorianCalendar.MINUTE, 1); // prazo para a tarefa ir para o juliano confirmar se o curso foi feito
			datPooCob.add(GregorianCalendar.MINUTE,1); // prazo para abrir a tarefa simples de aviso de cobertura
*/			
			processEntity.setValue("dataRetornaPool",datPoo);
			processEntity.setValue("dataRetornoPoolCobertura",datPooCob);
		}
		
		processEntity.setValue("dataAvancaConvocacao",new GregorianCalendar());
		
		
		/* estava no projeto do fusion core
		  	NeoObject reciclagem = (NeoObject) processEntity.findValue("tarefasDeReciclagem");
			EntityWrapper reciclagemWrapper = new EntityWrapper(reciclagem);
			GregorianCalendar datven =  (GregorianCalendar) reciclagemWrapper.findValue("datven");
			GregorianCalendar datPoo = (GregorianCalendar) datven.clone();
			GregorianCalendar datPooCob = (GregorianCalendar) datven.clone();
			datPoo.add(GregorianCalendar.DATE, +5);
			datPooCob.add(GregorianCalendar.DATE, -7);
			PersistEngine.persist(reciclagem);
		 */
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
