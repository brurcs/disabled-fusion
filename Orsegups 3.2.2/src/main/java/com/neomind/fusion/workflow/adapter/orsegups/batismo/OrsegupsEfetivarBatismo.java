package com.neomind.fusion.workflow.adapter.orsegups.batismo;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.history.HistoryAction;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.webservice.autostore.OrsegoupsAutoStoreWS;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessStateHistory;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class OrsegupsEfetivarBatismo implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Boolean revisar = false;
		List<NeoObject> documentos = (List<NeoObject>) processEntity.findValue("documentos");
		List<NeoObject> documentosRevisao = new ArrayList<NeoObject>();

		Map<String, List<NeoObject>> map = new HashMap<String, List<NeoObject>>();

		for (NeoObject documento : documentos)
		{
			EntityWrapper wDocumento = new EntityWrapper(documento);
			String batismo = (String) wDocumento.findValue("batismo");
			String mesano = (String) wDocumento.findValue("mesano");

			NeoObject colaborador = (NeoObject) wDocumento.findValue("colaborador");

			if (batismo.equalsIgnoreCase("Sem Batismo"))
			{
				throw new WorkflowException("Existem documentos não batizados.");
			}
			else if (batismo.equalsIgnoreCase("Solicitar Nova Digitalização"))
			{
				revisar = true;
				documentosRevisao.add(documento);
			}
			else if (batismo != null && !batismo.equalsIgnoreCase("excluido"))
			{
				if (colaborador == null)
				{
					throw new WorkflowException("Existem documentos sem seleção do colaborador.");
				}
				List<NeoObject> list = map.get(batismo+";"+mesano+";"+colaborador.getNeoId());

				if (list == null)
				{
					list = new ArrayList<NeoObject>();
				}
				list.add(documento);

				map.put(batismo+";"+mesano+";"+colaborador.getNeoId(), list);
			}

		}
//if(true) throw new WorkflowException("Existem documentos sem seleção do colaborador.");
		efetuarBatismo(map);
		processEntity.setValue("documentosRevisar", documentosRevisao);
		processEntity.setValue("revisar", revisar);

		finalizaProcessosRelacionados(processEntity, activity);

	}

	private void finalizaProcessosRelacionados(EntityWrapper processEntity, Activity activity)
	{
		String lista = (String) processEntity.findValue("processosUnificados");
		if (lista != null)
		{
			String[] split = lista.split(";");
			for (String code : split)
			{
				WFProcess processo = (WFProcess) PersistEngine.getNeoObject(WFProcess.class,
						new QLRawFilter("code = '" + code
								+ "' and saved = 1 and processstate = 0 and model.name = 'Batismo' "));

				if (processo != null)
				{
					ProcessStateHistory history = new ProcessStateHistory();
					PersistEngine.persist(history);
					history.setProcess(processo);
					history.setHistoryDate(new GregorianCalendar());
					history.setResponsible(PortalUtil.getCurrentUser());
					history.setAction(HistoryAction.cancel);
					history.setReason("Processo unificado ao fluxo " + activity.getProcess().getCode());
					processo.cancelProcess(history);
				}
			}
		}
	}

	public static void efetuarBatismo(Map<String, List<NeoObject>> map)
	{
		Set<Entry<String, List<NeoObject>>> entrySet = map.entrySet();

		for (Entry<String, List<NeoObject>> entry : entrySet)
		{
			String observacao = null;
			String mesano = null;
			String batismo = entry.getKey().split(";")[0];
			NeoObject colaborador = null;
			NeoFile fileFinal = null;
			List<NeoObject> listaDocumentos = entry.getValue();
			
			Collections.sort (listaDocumentos, new Comparator() {
				public int compare(Object o1, Object o2) {
					NeoObject doc1 = (NeoObject) o1;  
	                NeoObject doc2 = (NeoObject) o2;  
	                return doc1.getNeoId() < doc2.getNeoId() ? -1 : (doc1.getNeoId() > doc2.getNeoId() ? 1 : 0);  
	            }
	        });
			
			if (listaDocumentos.size() == 1)
			{
				if (listaDocumentos.size() == 1)
				{
					NeoObject documento = listaDocumentos.get(0);
					colaborador = (NeoObject) new EntityWrapper(documento).findValue("colaborador");
					fileFinal = (NeoFile) new EntityWrapper(documento).findValue("arquivo");
					mesano = (String) new EntityWrapper(documento).findValue("mesano");
					observacao = (String) new EntityWrapper(documento).findValue("observacao");
				}
			}
			else
			{
				
				NeoFile filen = null;
				List<NeoFile> listaFiles = new ArrayList<NeoFile>();
				for (NeoObject documento : listaDocumentos)
				{
					colaborador = (NeoObject) new EntityWrapper(documento).findValue("colaborador");
					listaFiles.add((NeoFile) new EntityWrapper(documento).findValue("arquivo"));
					filen = (NeoFile) new EntityWrapper(documento).findValue("arquivo");
					mesano = (String) new EntityWrapper(documento).findValue("mesano");
					observacao = (String) new EntityWrapper(documento).findValue("observacao");
				}

				File file = FileManager.getDocumentsAsPDF(listaFiles, "0", batismo, true, null);

				if (file == null)
				{
					throw new WorkflowException(
							"Não foi possível concatenar os documentos para o batismo " + batismo);
				}

				fileFinal = filen.getStorage().copy(file);
			
				fileFinal.setName(file.getName());

				PersistEngine.persist(fileFinal);
			}
			OrsegoupsAutoStoreWS os = new OrsegoupsAutoStoreWS();
			os.insereArquivoColaborador(fileFinal, mesano, batismo, colaborador);
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
