package com.neomind.fusion.workflow.adapter.casvig;

import java.util.Collection;

import javax.persistence.Entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.handler.ActivityHandler;
import com.neomind.fusion.workflow.handler.HandlerFactory;

@Entity
public class CasvigCheckPointValidar extends Activity
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigCheckPointValidar.class);
	
	@Override
	public void start()
	{	
		ActivityHandler handler = HandlerFactory.ACTIVITY.get(this);
        handler.start(this.getIncoming());
		
		boolean ok = true;		
		NeoObject obj = getProcessEntity();
		EntityWrapper wrapperProcessoPlanoAcao = obj == null ? null : new EntityWrapper(obj);
		
		if (wrapperProcessoPlanoAcao != null)
		{
			
			Collection planoList = (Collection) wrapperProcessoPlanoAcao.getValue("PlanoAcao");
		
			/*Lista de todos os subProcessos Plano de Ação*/
			for (Object oPlano : planoList)
			{	
				NeoObject noPlano = (NeoObject) oPlano;				
			 
				QLGroupFilter filter = new QLGroupFilter("AND");				
				/* Nome do modelo do subProcesso */
				filter.addFilter(new QLEqualsFilter("model.name","Validar Tarefa Plano Ação"));				
				/* Eforms do subProcesso*/
				filter.addFilter(new QLEqualsFilter("entity",noPlano)); 
				
				WFProcess wfProcess = (WFProcess)PersistEngine.getNeoObject(WFProcess.class, filter);
				
				/* Verifica se existe algum processo ainda não finalizado */
				if(wfProcess != null && !wfProcess.allActivitiesFinished())
				{
					ok = false;
				}
			}
			
			/* Caso não exista mais nenhum subProcesso aberto */
			if(ok == true)
			{	
				handler.finish();
			}					
		}		
	}
}