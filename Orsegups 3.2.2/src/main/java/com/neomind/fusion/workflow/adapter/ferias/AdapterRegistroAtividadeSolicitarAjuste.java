package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.sun.star.i18n.Calendar;

public class AdapterRegistroAtividadeSolicitarAjuste implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<NeoObject> registroAtividades = (List<NeoObject>) processEntity.getValue("r001RegistroAtividade");
		String observacao = "";
		observacao = String.valueOf(processEntity.getValue("justificativaAjuste"));

		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001RegistroAtividades");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);

		String responsavel = origin.returnResponsible();
		GregorianCalendar dataAcao = new GregorianCalendar();

		wRegAti.setValue("usuario", responsavel);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("descricao", observacao);

		PersistEngine.persist(objRegAti);
		registroAtividades.add(objRegAti);

		processEntity.setValue("r001RegistroAtividade", registroAtividades);
		
		GregorianCalendar novoPrazo = new GregorianCalendar();
		novoPrazo = OrsegupsUtils.getNextWorkDay(novoPrazo);
		novoPrazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		novoPrazo.set(GregorianCalendar.MINUTE, 59);
		novoPrazo.set(GregorianCalendar.SECOND, 59);

		processEntity.setValue("prazoTarefa", novoPrazo);
		processEntity.setValue("flAjuste", true);
		processEntity.setValue("justificativaAjuste","");

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
