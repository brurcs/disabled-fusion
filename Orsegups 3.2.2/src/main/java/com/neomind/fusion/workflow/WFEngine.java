package com.neomind.fusion.workflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldWrapper;
import com.neomind.fusion.entity.form.FieldGroup;
import com.neomind.fusion.history.HistoryAction;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLInFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalKeys;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.portal.portlets.workflow.GroupedTaskItem;
import com.neomind.fusion.portal.taglib.workflow.WorkflowFilteredItemsTag;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoRole;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEngine;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.event.WorkflowEventDispatcher;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ActivityModel;
import com.neomind.fusion.workflow.model.Condition2ActivityModel;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.model.ProcessModelProxy;
import com.neomind.fusion.workflow.service.TaskQueryFilter;
import com.neomind.fusion.workflow.simulation.TaskService;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.fusion.workflow.usermanager.WorkflowSecurityEntityManager;
import com.neomind.util.CustomFlags;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;
import com.neomind.util.ReflectionUtils;

public class WFEngine
{
	private static final Log log = LogFactory.getLog(WFEngine.class);

	private static WFEngine engine;

	private static Map<Long, Collection<Long>> startableModelsByUser;

	private static Map<String, ActivityModel> actModelCache;

	public static boolean modelsLoaded = false;

	public static final WFEngine getInstance()
	{
		if (WFEngine.engine == null)
			WFEngine.engine = new WFEngine();
		return WFEngine.engine;
	}

	private WFEngine()
	{
		startableModelsByUser = new HashMap<>();
		actModelCache = new HashMap<>();
	}

	/**
	 * Cria um novo processo usando o modelo especificado.
	 *
	 * @param model O modelo.
	 * @return O processo.
	 */
	public synchronized WFProcess create(final ProcessModel model)
	{
		return WorkflowService.create(model);
	}

	/**
	 *
	 * @param entity EForm do processo
	 * @param processName Nome do process
	 * @param assign Sinaliza se a primeira tarefa já deverá ser assumida pelo usuário atual.
	 * @param requester Solicitante
	 * @return
	 */
	public WFProcess createProcess(NeoObject entity, String processName, boolean assign,
			NeoUser requester)
	{
		WFProcess process = null;
		ProcessModel modelo = getProcessModel(processName);
		if (modelo != null)
		{
			process = modelo.startProcess(entity, assign, null, null, null, null, requester);
			process.setSaved(true);
			PersistEngine.getEntityManager().flush();
		}
		return process;
	}

	/**
	 * Retorna o modelo corrente de um processo
	 *
	 * @param processName Nome do processo
	 * @return
	 */
	public ProcessModel getProcessModel(String processName)
	{
		ProcessModel modelo = null;

		ProcessModelProxy proxy = PersistEngine.getNeoObject(ProcessModelProxy.class,
				new QLEqualsFilter("name", processName));
		if (proxy != null)
		{
			WorkFlowVersionControl versionControl = proxy.getVersionControl();

			if (versionControl != null)
			{
				modelo = versionControl.getCurrent();
			}
		}
		return modelo;
	}

	public WFProcess createProcess(NeoObject entity, String processName, boolean assign)
	{
		return this.createProcess(entity, processName, assign, null);
	}

	/**
	 * Avança uma tarefa.
	 *
	 * @param task A tarefa a ser avançada.
	 * @return Reetorna true se a tarefa foi avançada com sucesso.
	 */
	public boolean sendTask(Task task)
	{
		return TaskService.sendTask(task);
	}

	public boolean sendTask(Task task, Map parameterMap)
	{
		return TaskService.sendTask(task, parameterMap);
	}

	/**
	 * @see #setEntityValues(NeoObject, Map)
	 */
	public boolean setEntityValues(Task task, Map<String, Object> fields)
	{
		return setEntityValues(task.getProcessEntity(), fields);
	}

	public ActivityModel getActModel(String code, ProcessModel pm)
	{
		String key = pm.getNeoId() + "_" + code;
		ActivityModel ret = actModelCache.get(key);
		if (ret == null)
		{
			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("processModel", pm));
			filter.addFilter(new QLEqualsFilter("code", code));

			ret = PersistEngine.getObject(ActivityModel.class, filter);

			if (ret == null)
			{
				if (log.isWarnEnabled())
					log.warn("ActivityModel " + code + "not found in the model " + pm.getNeoId());
			}
			else
			{
				if (log.isDebugEnabled())
					log.debug("Retirando lazy da activity " + ret.getTitle());

				// necessario tirar o lazy pro simulador
				if (ret.getTerm() != null)
					ret.getTerm().getActivityTimeLimit();
				ret.getDeadLineModel().iterator();

				if (ret.getSplitModel() != null)
				{
					Iterator<Condition2ActivityModel> a = ret.getSplitModel().getCondition2TaskSet()
							.iterator();

					while (a.hasNext())
					{
						Condition2ActivityModel c2acm = a.next();
						c2acm.getActivityModelSet().iterator();
						c2acm.getCondition().getAsString();
					}

					if (ret.getJoinModel() != null)
						ret.getJoinModel().getSourceTaskSet().iterator();
					if (ret.getPermission() != null
							&& ret.getPermission().getAllowPermissionDetail() != null)
						ret.getPermission().getAllowPermissionDetail().iterator();
					if (ret.getPermission() != null
							&& ret.getPermission().getDenyPermissionDetail() != null)
						ret.getPermission().getDenyPermissionDetail().iterator();
				}

				ret.getUsedResources().iterator();

				actModelCache.put(key, ret);
			}
		}
		return ret;
	}

	public ActivityModel returnStartTask(ProcessModel pm)
	{
		return this.getActModel(pm.getVersionControl().getProxy().getFirstActResourceId(), pm);
	}

	/**
	 * Seta valores para a entidade do processo.
	 *
	 * @param o A entidade do processo.
	 * @param fields Mapa com os campos e seus respectivos valores.
	 * @return Retorna true se os valores foram atribuidos com sucesso.
	 */
	public boolean setEntityValues(NeoObject o, Map<String, Object> fields)
	{
		EntityWrapper wrapper = new EntityWrapper(o);
		for (String key : fields.keySet())
		{
			FieldWrapper fw = wrapper.findField(key);
			if (fw != null)
			{
				fw.setValue(fields.get(key), true);
			}
		}
		return true;
	}

	public Task assignActivity(Activity activity, NeoUser user, Long delegatedUserId)
	{
		NeoUser delegatedUser = null;
		if (delegatedUserId != null)
		{
			delegatedUser = PersistEngine.getObject(NeoUser.class, delegatedUserId);
		}
		return assignActivity(activity, user, delegatedUser);
	}

	public Task assignActivity(Activity activity, NeoUser user, NeoUser delegatedUser)
	{
		if (!(activity instanceof UserActivity))
			return null;

//		RoleAvailableActivityData data = PersistEngine.getObject(RoleAvailableActivityData.class,
//				new QLEqualsFilter("activity", activity));
//
//		if (data == null)
//			return null;

		Task task = null;
		if (delegatedUser == null)
			task = activity.getModel().getTaskAssigner().assign((UserActivity) activity, user);
		else
		{
			task = activity.getModel().getTaskAssigner()
					.assignTemporaryUser((UserActivity) activity, user, delegatedUser);
		}
		return task;
	}

	// FIXME - Tratar execução múltipla.
	public NeoUser getResponsible(Activity activity)
	{
		if (activity instanceof UserActivity)
		{
			Task task = ((UserActivity) activity).getTaskList().get(
					((UserActivity) activity).getTaskList().size() - 1);
			return task.getUser();
		}

		return null;
	}

	public static Map<String, List<NeoObject>> getSystemTaskGroups(final NeoUser user)
	{
		return WFEngine.getSystemTaskGroups(user, null);
	}

	/**
	 * Monta um mapa com a quantidade de tarefas/atividades/solicitações para a caixa de entrada
	 * utilizando os mesmos metodos que a listagem da grid, para não ocorrer divergencias de valores
	 *
	 * @param nu - O usuário que esta acessando a caixa de entrada
	 */
	public Map<String, GroupedTaskItem> getInboxCount(final NeoUser nu)
	{
		long l1 = System.currentTimeMillis();
		Long l2 = null;
		Long l3 = null;

		TaskQuery taskQuery;
		WorkFlowQuery workflowQuery;
		QLGroupFilter filter;
		Long total;
		Map<String, GroupedTaskItem> countMap = new HashMap<String, GroupedTaskItem>();
		GroupedTaskItem item;

		// Minhas tarefas
		taskQuery = new PendingTaskQuery();
		taskQuery.setUser(nu);
		filter = taskQuery.buildTaskFilter();
		total = getCount(filter, Task.class);
		item = new GroupedTaskItem();
		item.add(PortalKeys.MY_TASKS, total.intValue());
		countMap.put(PortalKeys.MY_TASKS, item);

		if (log.isDebugEnabled())
		{
			log.debug("my tasks count time: " + (System.currentTimeMillis() - l1) + " ms");
			l2 = System.currentTimeMillis();
		}

		// Minhas solicitações
		workflowQuery = new WorkFlowQueryByRequester();
		workflowQuery.setUser(nu);
		filter = workflowQuery.buildFilter();
		total = getCount(filter, WFProcess.class);
		item = new GroupedTaskItem();
		item.add(PortalKeys.MY_REQUESTS, total.intValue());
		countMap.put(PortalKeys.MY_REQUESTS, item);

		if (log.isDebugEnabled())
		{
			log.debug("my requests count time: " + (System.currentTimeMillis() - l2) + " ms");
			l2 = System.currentTimeMillis();
		}

		// Minhas solicitações finalizadas
		workflowQuery = new WorkFlowFinishedQueryByRequester();
		workflowQuery.setUser(nu);
		filter = workflowQuery.buildFilter();
		total = getCount(filter, WFProcess.class);
		if (total > 0)
		{
			item = new GroupedTaskItem();
			item.add(PortalKeys.MY_FINISHED_REQUESTS, total.intValue());
			countMap.put(PortalKeys.MY_FINISHED_REQUESTS, item);
		}

		if (log.isDebugEnabled())
		{
			log.debug("my finashed requests count time: " + (System.currentTimeMillis() - l2) + " ms");
			l2 = System.currentTimeMillis();
		}

		// Tarefas sob gerência
		taskQuery = new TaskQueryByGestor();
		taskQuery.setUser(nu);

		Long totalManager = 0l;
		item = new GroupedTaskItem();

		Map<ProcessModelProxy, Long> mapCount = ((TaskQueryByGestor) taskQuery).getManagedTaskCount();
		for (ProcessModelProxy model : mapCount.keySet())
		{
			total = mapCount.get(model);
			totalManager += total;
			item.addModelId(model.getAsString(), model.getNeoId());
			item.add(model.getAsString(), total.intValue());
		}
		item.add(PortalKeys.TASKS_BY_GESTOR, totalManager.intValue());
		if (totalManager.intValue() > 0)
		{
			countMap.put(PortalKeys.TASKS_BY_GESTOR, item);
		}

		if (log.isDebugEnabled())
		{
			log.debug("managed tasks count time: " + (System.currentTimeMillis() - l2) + " ms");
			l2 = System.currentTimeMillis();
		}

		// Atividades sob gerência
		taskQuery = new TaskQueryByGestor();
		taskQuery.setUser(nu);
		totalManager = 0l;

		item = new GroupedTaskItem();

		mapCount = ((TaskQueryByGestor) taskQuery).getManagedActivityCount();
		for (ProcessModelProxy model : mapCount.keySet())
		{
			total = mapCount.get(model);
			totalManager += total;
			item.addModelId(model.getAsString(), model.getNeoId());
			item.add(model.getAsString(), total.intValue());
		}
		item.add(PortalKeys.ACTIVITYS_BY_GESTOR, totalManager.intValue());
		if (totalManager.intValue() > 0)
		{
			countMap.put(PortalKeys.ACTIVITYS_BY_GESTOR, item);
		}

		if (log.isDebugEnabled())
		{
			log.debug("managed activities count time: " + (System.currentTimeMillis() - l2) + " ms");
			l2 = System.currentTimeMillis();
		}

		// POOLs
		Set<NeoRole> roles = nu.getRoles();
		// Grupo (*) Todos os Usuários
		roles.add(SecurityEngine.getInstance().getNeoGroupAllUsers());
		for (NeoRole role : roles)
		{
			taskQuery = new PendingTaskQuery();
			((PendingTaskQuery) taskQuery).setRole(role);
			filter = taskQuery.buildActivityFilter();
			total = 0l; //getCount(filter, RoleAvailableActivityData.class);
			if (total > 0)
			{
				item = new GroupedTaskItem();
				// bruno.manzo, 19/07/2012: mesmo padrão do groupedtaskportlet
				item.add(PortalKeys.ROLE_PATH + role.getNeoId(), total.intValue());
				countMap.put(PortalKeys.ROLE_PATH + role.getNeoId(), item);
			}
		}

		if (log.isDebugEnabled())
		{
			log.debug("pools count time: " + (System.currentTimeMillis() - l2) + " ms");
			l2 = System.currentTimeMillis();
		}

		// Tarefas Temporárias
		taskQuery = new TemporaryTaskQuery();
		taskQuery.setUser(nu);
		filter = taskQuery.buildTaskFilter();
		total = getCount(filter, Task.class);
		if (total > 0)
		{
			item = new GroupedTaskItem();
			item.add(PortalKeys.TEMPORARY_TASKS, total.intValue());
			countMap.put(PortalKeys.TEMPORARY_TASKS, item);
		}

		if (log.isDebugEnabled())
		{
			log.debug("temporary tasks count time: " + (System.currentTimeMillis() - l2) + " ms");
			l2 = System.currentTimeMillis();
		}

		// Atividades Temporárias
		taskQuery = new TemporaryTaskQuery();
		taskQuery.setUser(nu);
		filter = taskQuery.buildActivityFilter();
		total = 0l; //getCount(filter, RoleAvailableActivityData.class);
		if (total > 0)
		{
			item = new GroupedTaskItem();
			item.add(PortalKeys.TEMPORARY_ACTIVITIES, total.intValue());
			countMap.put(PortalKeys.TEMPORARY_ACTIVITIES, item);
		}

		if (log.isDebugEnabled())
		{
			log.debug("temporary activities count time: " + (System.currentTimeMillis() - l2) + " ms");
			l2 = System.currentTimeMillis();
		}

		// Tarefas Delegadas
		taskQuery = new PendingTaskQuery();
		taskQuery.setUser(nu);
		filter = ((PendingTaskQuery) taskQuery).buildTaskFilter(true);
		total = getCount(filter, Task.class);
		if (total > 0)
		{
			item = new GroupedTaskItem();
			item.add(PortalKeys.DELEGATED_TASKS, total.intValue());
			countMap.put(PortalKeys.DELEGATED_TASKS, item);
		}

		if (log.isDebugEnabled())
		{
			log.debug("delegate tasks count time: " + (System.currentTimeMillis() - l2) + " ms");
			l2 = System.currentTimeMillis();
		}

		if (log.isDebugEnabled())
			log.debug("inbox total count time: " + (System.currentTimeMillis() - l1) + " ms");

		return countMap;
	}

	/**
	 * Conta quantos registros existem na class passada com o filtro passado
	 */
	public static Long getCount(QLFilter filter, Class<? extends NeoObject> clazz)
	{
		return PersistEngine.countObjects(clazz, "_vo.neoId", "_vo", filter, false);
	}

	/**
	 * Conta quantos registros existem na class passada com o filtro passado com distinct.
	 */
	public static Long getDistinctCount(QLFilter filter, Class<? extends NeoObject> clazz)
	{
		return PersistEngine.countObjects(clazz, "DISTINCT _vo.neoId", "_vo", filter, false);
	}

	/**
	 * Cancela o processo
	 *
	 * @param process - O processo a ser cancelado
	 * @param reason - String contendo a razão do cancelamento
	 */
	public void cancelProcess(WFProcess process, String reason)
	{
		GregorianCalendar finishDate = new GregorianCalendar();
		ProcessStateHistory history = new ProcessStateHistory();
		history.setResponsible(PortalUtil.getCurrentUser());
		history.setHistoryDate(finishDate);
		history.setReason(reason);
		history.setAction(HistoryAction.cancel);
		PersistEngine.persist(history);

		// TODO: algum meio melhor de saber se é subfluxo?
		if (process.getCallBackTask() != null)
			cancelProcess(process.getCallBackTask().getProcess(), reason);

		cancelProcess(process, history);

		TaskInstanceHelper.cancel(process);

		if (CustomFlags.GOLDEN)
		{
			final Object cancelProcess[] = { process, reason };
			final Class<?> cancelParamType[] = { WFProcess.class, String.class };
			ReflectionUtils.invokeMethod(
					"com.neomind.fusion.workflow.adapter.golden.GoldenReflectionMethodUtils",
					"cancelProcessSA", cancelProcess, cancelParamType);
		}
		
		// FBB: Ao cancelar uma solicitação de cadastramento, deve-se desbloquear a entidade do processo.
		if (CustomFlags.FBB && process.getProcessEntity() != null && process.getProcessEntity().getNeoType().equalsIgnoreCase("Dcadastro"))
		{
			NeoObject oCadastro = process.getEntity();
			EntityWrapper wCadastro = new EntityWrapper(oCadastro);
			
			if (NeoUtils.safeIsNotNull(wCadastro.findField("solicitanteCadastroEntidade.bloquearOperacoesEntidade")))
				wCadastro.findField("solicitanteCadastroEntidade.bloquearOperacoesEntidade").setValue(false);
			
			if (NeoUtils.safeIsNotNull(wCadastro.findField("solicitanteCadastroEntidade.eventos1")))
				wCadastro.findField("solicitanteCadastroEntidade.eventos1").setValue(null);
		}
	}

	/**
	 * Cancela o processo
	 *
	 * @param process - O processo
	 * @param history - O ProcessStateHistory
	 */
	public void cancelProcess(WFProcess process, ProcessStateHistory history)
	{
		// cancelar as tasks
		List<Task> tasks = process.getAllPendingTaskList();
		for (Task t : tasks)
		{
			t.cancel();
		}

		// cancelar as activitys
		List<Activity> acts = process.getOpenActivities();
		for (Activity a : acts)
		{
			// bruno.manzo, 10/01/2013: remover as tarefas em pool
			a.cancel();
		}

		// cancelar os fluxos filhos
		Collection<SubProcessActivity> subActivitys = PersistEngine.getObjects(SubProcessActivity.class,
				new QLEqualsFilter("process", process));
		for (SubProcessActivity subAct : subActivitys)
		{
			WFProcess w = subAct.getSubWFProcess();
			if (w != null)
			{
				this.cancelProcess(w, history);
				TaskInstanceHelper.cancel(w);
			}
				
			
		}

		process.cancelProcess(history);
	}

	/**
	 * montava um mapa carregando todos os objetos da caixa de entrada apenas para contagem
	 * substituido pelo getInboxCount
	 */
	@Deprecated
	public static Map<String, List<NeoObject>> getSystemTaskGroups(final NeoUser user,
			final Set<String> groupSet)
	{
		final Map<String, List<NeoObject>> map = new HashMap<String, List<NeoObject>>();

		TaskQuery taskQuery;
		List taskSet;
		long l1, l2;

		// FIXME Revisar a necessidade da opção "Dead-line"
		/*
		 * // Tarefas em dead line //if (groupSet == null ||
		 * groupSet.contains(PortalKeys.MY_REQUESTS)) if (groupSet == null
		 * || groupSet.contains(PortalKeys.DEAD_LINE)) { taskQuery = new
		 * TaskQueryByGestor(); taskQuery.setDeadLineExceded(true);
		 * taskQuery.setUser(user); l1 = System.currentTimeMillis(); taskSet
		 * = taskQuery.getTaskList(); l2 = System.currentTimeMillis();
		 * log.debug(" getDeadline: " + (l2 - l1) + "ms");
		 * map.put(PortalKeys.DEAD_LINE, taskSet); }
		 */

		return map;
	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	/**
	 * Retorna as tarefas pendentes de um determinado usuário
	 *
	 * @param user
	 * @return
	 */
	@Deprecated
	public List<Task> getPendingTasks(final NeoUser user, final String due, final String dueType,
			final String startDate, final String startDateType, final boolean byGestor,
			final boolean deadLineExceded)
	{
		final List<Task> taskSet = new ArrayList<Task>();
		if (byGestor)
		{
			taskSet.addAll(getTaskListByGestor(user, due, dueType, startDate, startDateType,
					deadLineExceded));
		}
		else
		{
			taskSet.addAll(getTaskList(user, due, dueType, startDate, startDateType, deadLineExceded));
		}

		return taskSet;
	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	/**
	 * Retorna as tarefas Disponíveis a um determinado usuário
	 *
	 * @param tag
	 * @return
	 */
	@Deprecated
	public List<Task> getPendingTasks(final WorkflowFilteredItemsTag tag)
	{
		List<Task> taskSet;
		TaskQuery taskQuery = null;
		if (tag.isByGestor())
		{
			taskQuery = new TaskQueryByGestor();
		}
		else
		{
			taskQuery = new PendingTaskQuery();
		}

		taskQuery.setUser(PortalUtil.getCurrentUser());
		taskQuery.setDeadLineExceded(tag.isDeadLineExceded());
		taskQuery.setFinishTermType(tag.getFinishTermType());
		taskQuery.setFinishTermUnits(tag.getFinishTermUnits());
		taskQuery.setShowNoDue(tag.isShowNoDue());
		taskQuery.setStartTermType(tag.getStartTermType());
		taskQuery.setStartTermUnits(tag.getStartTermUnits());
		taskQuery.setProcessId(tag.getWfProcessId());

		taskSet = taskQuery.getTaskList();

		return taskSet;

	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	@Deprecated
	public List<Activity> getAvailableActivities(final WorkflowFilteredItemsTag tag)
	{
		List<Activity> taskSet;

		TaskQuery taskQuery = null;

		if (tag.isTemporary())
		{
			taskQuery = new TemporaryTaskQuery();
		}
		else if (tag.isByGestor())
		{
			taskQuery = new TaskQueryByGestor();
		}
		else
		{
			taskQuery = new PendingTaskQuery();
		}

		taskQuery.setUser(PortalUtil.getCurrentUser());
		taskQuery.setDeadLineExceded(tag.isDeadLineExceded());
		taskQuery.setFinishTermType(tag.getFinishTermType());
		taskQuery.setFinishTermUnits(tag.getFinishTermUnits());
		taskQuery.setShowNoDue(tag.isShowNoDue());
		taskQuery.setStartTermType(tag.getStartTermType());
		taskQuery.setStartTermUnits(tag.getStartTermUnits());
		taskQuery.setProcessId(tag.getWfProcessId());
		taskSet = taskQuery.getActivityList();

		return taskSet;

	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	@Deprecated
	public List<Task> getTaskListByGestor(NeoUser user, String due, String dueType, String startDate,
			String startDateType, boolean deadLineExceded)
	{
		return getTaskGenList(user, due, dueType, startDate, startDateType, true, deadLineExceded);
	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	@Deprecated
	private List<Task> getTaskGenList(NeoUser user, String due, String dueType, String startDate,
			String startDateType, boolean byGestor, boolean deadLineExceded, long wfProcess)

	{
		// TODO Find a more performatic way to do this
		GregorianCalendar dtIni = NeoCalendarUtils.getDueDate(startDateType, startDate);
		GregorianCalendar dtFim = NeoCalendarUtils.getDueDate(dueType, due);

		final QLGroupFilter filter = new QLGroupFilter("AND");

		filter.addFilter(new QLFilterIsNotNull("startDate"));
		filter.addFilter(new QLFilterIsNull("finishDate"));
		filter.addFilter(new QLEqualsFilter("activity.process.processState", ProcessState.running
				.ordinal()));

		if (!byGestor)
			filter.addFilter(new QLEqualsFilter("user", user));

		if (!startDateType.equals(""))
			filter.addFilter(new QLOpFilter("dueDate", ">=", dtIni.getTime()));

		if (!dueType.equals(""))
			filter.addFilter(new QLOpFilter("dueDate", "<=", dtFim.getTime()));

		List<Task> list = PersistEngine.getObjects(Task.class, filter);

		if (log.isDebugEnabled())
		{
			log.debug("Getting task list of user " + user);
			log.debug("  returned " + list.size() + " tasks");
			log.debug("    byGestor " + byGestor);
			log.debug("    startDateType " + startDateType);
			log.debug("    dueType" + dueType);
		}

		SortedMap<String, Task> sortedMap = new TreeMap<String, Task>();
		for (Task o : list)
		{
			try
			{
				if (wfProcess > 0)
				{
					if (o.getActivity().getProcess().getNeoId() != wfProcess)
					{
						continue;
					}
				}
				if (byGestor)
				{
					if (!deadLineExceded)
					{
						if (o.getProcess().isManager(user))
						{
							Task task = o;
							sortedMap.put(task.getFullName(), task);
						}
						else if (o.getActivity().getTaskAssigner() != null)
						{
							if (o.getActivity().getTaskAssigner() instanceof RoleTaskAssigner)
							{
								if (((RoleTaskAssigner) o.getActivity().getTaskAssigner())
										.getParticipants(o.getActivity()) != null)
								{
									for (SecurityEntity nr : ((RoleTaskAssigner) o.getActivity()
											.getTaskAssigner()).getParticipants(o.getActivity()))
									{
										if (user.isInSecurityEntity(nr))
										{
											sortedMap.put(o.getFullName(), o);
											break;
										}
									}
								}
							}
						}
					}
					else
					{
						//						if (o.isDeadLineNotOk())
						//							sortedMap.put(o.getFullName(), o);
					}
				}
				else
					sortedMap.put(o.getFullName(), o);
			}
			catch (Exception e)
			{
				log.trace("Exception loading a task", e);
			}
		}
		List<Task> taskSet = new ArrayList<Task>();
		taskSet.addAll(sortedMap.values());

		return taskSet;
	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	@Deprecated
	public List<Task> getTaskList(NeoUser user, long wfProcess)
	{
		return getTaskGenList(user, "", "", "", "", false, false, wfProcess);
	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	@Deprecated
	public List<Task> getTaskList(NeoUser user)
	{
		return getTaskList(user, 0);
	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	@Deprecated
	public List<Task> getTaskList(NeoUser user, String due, String dueType, String startDate,
			String startDateType, boolean deadLineExceded)
	{
		return getTaskGenList(user, due, dueType, startDate, startDateType, false, deadLineExceded);
	}

	// FIXME - Alterar TaskPortlet para utilizar esse engine.
	@Deprecated
	private List<Task> getTaskGenList(NeoUser user, String due, String dueType, String startDate,
			String startDateType, boolean byGestor, boolean deadLineExceded)
	{
		return getTaskGenList(user, due, dueType, startDate, startDateType, byGestor, deadLineExceded, 0);
	}

	/**
	 * Retorna o tempo tempo gasto de uma tarefa do workflow em milisegundos.
	 *
	 * @return O tempo gasto em milisegundos.
	 */
	public long getTaskTimeSpent(Task task)
	{
		long timespent = 0;
		if (task.getFinishDate() == null)
		{
			timespent = System.currentTimeMillis() - task.getStartDate().getTimeInMillis();
		}
		else
		{
			timespent = task.getFinishDate().getTimeInMillis() - task.getStartDate().getTimeInMillis();
		}
		return timespent;
	}

	/**
	 * Retorna o tempo estimado para uma tarefa do workflow em milisegundos.
	 *
	 * @param task A tarefa do processo.
	 * @return O tempo estimado em milisegundos.
	 */
	public long getTaskBaselineTime(Task task)
	{
		if (task.getDueDate() == null)
			return 0;

		long baselineTime = (task.getDueDate().getTimeInMillis() - task.getStartDate().getTimeInMillis());
		if (baselineTime < 0)
		{
			baselineTime = 0;
		}

		return baselineTime;
	}

	/**
	 * Retorna o tempo restante de uma tarefa do workflow em milisegundos.
	 *
	 * @param task A tarefa do processo.
	 * @return O tempo restante em milisegundos.
	 */
	public long getTaskTimeLeft(Task task)
	{
		long timeLeft = 0;

		timeLeft = this.getTaskBaselineTime(task) - this.getTaskTimeSpent(task);

		return timeLeft;
	}

	/**
	 * Retorna a cor referente ao status da tarefa (vermelho-atraso, amarelo-em aviso, verde-em dia).
	 *
	 * @param task A tarefa do processo.
	 * @return A cor referente ao status da tarefa (vermelho-atraso, amarelo-em aviso, verde-em dia)..
	 */
	public String getTaskStatus(Task task)
	{
		if (task.getDueDate() == null)
			return "green";
		long start = task.getStartDate().getTimeInMillis();
		long due = task.getDueDate().getTimeInMillis() - start;
		boolean finished = task.getFinishDate() != null;
		long finish;
		if (finished)
			finish = task.getFinishDate().getTimeInMillis() - start;
		else
			finish = System.currentTimeMillis() - start;
		double free = (due - finish) / (24.0 * 3600 * 1000);

		String color;
		if (free >= 1.0)
			color = "green";
		else if (free >= 0.0)
			color = "yellow";
		else
			color = "red";
		if (!finished)
			color += "2";

		return color;
	}

	/**
	 * Retorna a chave para o I18N referente a cada um dos status.
	 *
	 * @param color com a cor do status.
	 * @return A chave para o I18N do status.
	 */
	public String getStatusTooltip(String color)
	{
		String result = "";

		if (color != null)
		{
			if (color.equals("green"))
			{
				result = "okClose";
			}
			else if (color.equals("yellow"))
			{
				result = "warningClose";
			}
			else if (color.equals("red"))
			{
				result = "delayedClose";
			}
			else if (color.equals("green2"))
			{
				result = "okOpen";
			}
			else if (color.equals("yellow2"))
			{
				result = "warningOpen";
			}
			else if (color.equals("red2"))
			{
				result = "delayedOpen";
			}
		}

		return result;
	}

	/**
	 * Calcula a proporção de tarefas em dia, em aviso e em atraso e realiza o calculo proporcional para
	 * exibiro resultado no relogioc do BP consolidado.
	 *
	 * @param processModel o processo que se deseja calcular.
	 * @return O valor calculado para ser mostrado no relogio do BPConsolidado.
	 */
	@SuppressWarnings("unchecked")
	public BigDecimal calculeteBPGauge(ProcessModelProxy processModel)
	{
		BigDecimal result = new BigDecimal(0);

		try
		{
			QLGroupFilter filter = new QLGroupFilter("and");
			filter.addFilter(new QLEqualsFilter("model.versionControl.proxy.neoId", processModel
					.getNeoId()));
			filter.addFilter(new QLEqualsFilter("saved", true));

			List<WFProcess> processList = PersistEngine.getObjects(WFProcess.class, filter);

			long totalProcess = processList.size();
			long green = 0;
			long yellow = 0;
			long red = 0;

			for (WFProcess process : processList)
			{
				long total = 0;
				long due = 0;
				for (Task t : process.getAllTasks(FieldGroup.SEARCH_SUB))
				{
					if (t.getDueDate() == null)
						continue;

					if (process.getProcessState() == ProcessState.canceled && t.getFinishDate() == null)
					{
						total += (process.getFinishDate().getTimeInMillis() - t.getStartDate()
								.getTimeInMillis());
					}
					else
					{
						if (t.getFinishDate() == null)
						{
							total += (System.currentTimeMillis() - t.getStartDate().getTimeInMillis());
						}
						else
						{
							total += (t.getFinishDate().getTimeInMillis() - t.getStartDate()
									.getTimeInMillis());
						}
					}

					long ddue = (t.getDueDate().getTimeInMillis() - t.getStartDate().getTimeInMillis());
					if (ddue < 0)
						ddue = 0;
					due += ddue;
				}

				double free = (due - total) / (24.0 * 3600 * 1000);

				if (free >= 1.0)
					green += 1;
				else if (free >= 0.0)
					yellow += 1;
				else
					red += 1;
			}

			BigDecimal greenPercent = (totalProcess == 0 || green == 0) ? new BigDecimal(0)
					: new BigDecimal((green * 100) / totalProcess);
			BigDecimal yellowPercent = (totalProcess == 0 || yellow == 0) ? new BigDecimal(0)
					: new BigDecimal((yellow * 100) / totalProcess);
			BigDecimal redPercent = (totalProcess == 0 || red == 0) ? new BigDecimal(0)
					: new BigDecimal((red * 100) / totalProcess);

			BigDecimal preResult = greenPercent.subtract(redPercent);

			result = new BigDecimal(50).add(preResult.divide(new BigDecimal(2)));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Verifica se o usuário passado é participante do processo
	 *
	 * @param process O processo
	 * @param user O usuário
	 * @return true caso participe, senão false
	 */
	public static boolean isParticipant(Long processId, Long userId)
	{
		boolean isParticipant = false;
		if (processId != null && userId != null)
		{
			WFProcess process = PersistEngine.getObject(WFProcess.class, processId);
			NeoUser user = PersistEngine.getObject(NeoUser.class, userId);
			isParticipant = isParticipant(process, user);
		}
		return isParticipant;
	}

	public static boolean isParticipant(WFProcess process, NeoUser user)
	{
		boolean isParticipant = false;

		if (process != null && user != null)
		{
			// bruno.manzo, 03/10/2012: no caso de um fluxo ser aberto por WS ou algum outro meio customizado
			// a primeira atividade que deveria ser de preenchimento é pulada, logo o solicitante não executa uma atividade
			// do fluxo, porem como solicitante ele deve poder pesquisar
			isParticipant = user.equals(process.getRequester());
			if (!isParticipant)
			{
				List<Task> tasks = getTaksByProcessAndUser(process, user);
				isParticipant = tasks != null && !tasks.isEmpty();
				if (!isParticipant)
				{
					isParticipant = isUserHasPermissionOnProcess(process, user);
					if (!isParticipant)
					{
						List<SubProcessActivity> subProcessActivityList = getSubProcessListFromProcess(process);
						if (subProcessActivityList != null && !subProcessActivityList.isEmpty())
						{
							Iterator<SubProcessActivity> subProcessActivityIterator = subProcessActivityList
									.iterator();
							while (subProcessActivityIterator.hasNext() && !isParticipant)
							{
								SubProcessActivity subProcessActivity = subProcessActivityIterator
										.next();
								WFProcess subProcess = subProcessActivity.getSubWFProcess();
								isParticipant = isParticipant(subProcess, user);
							}
						}
					}
				}
			}
		}

		return isParticipant;
	}

	public static boolean isUserHasPermissionOnProcess(WFProcess process, NeoUser user)
	{
		// @author: bruno.camargo
		// inserido filtro para buscar tambem o campo moreViewers do objeto wfprocess quando FAP Slip		
		if (process.getModel().getName().equals("F002 - FAP Slip"))
		{
			QLGroupFilter orFilter = new QLGroupFilter("OR");
			orFilter.addFilter(new QLInFilter("moreViewers.neoId", TaskQueryFilter
					.getUserRoles(user.getNeoId())));
			orFilter.addFilter(new QLInFilter("model.moreViewers.neoId", TaskQueryFilter
					.getUserRoles(user.getNeoId())));
			
			QLGroupFilter andFilter = new QLGroupFilter("AND");
			andFilter.addFilter(new QLEqualsFilter("neoId", process.getNeoId()));
			andFilter.addFilter(orFilter);
			
			WFProcess wfProcess = PersistEngine.getObject(WFProcess.class,
					andFilter);
			
			return wfProcess != null;
		}
				
		QLGroupFilter processModelGroupFilter = new QLGroupFilter("AND");
		processModelGroupFilter.addFilter(new QLInFilter("model.moreViewers.neoId", TaskQueryFilter
				.getUserRoles(user.getNeoId())));
		processModelGroupFilter.addFilter(new QLEqualsFilter("neoId", process.getNeoId()));
		WFProcess processWithPermission = PersistEngine.getObject(WFProcess.class,
				processModelGroupFilter);
		return processWithPermission != null;
	}

	public static Boolean isManagerOfProcess(Long processId, Long userId)
	{
		boolean isManager = false;
		if (processId != null && userId != null)
		{
			WFProcess process = PersistEngine.getObject(WFProcess.class, processId);
			NeoUser user = PersistEngine.getObject(NeoUser.class, userId);
			isManager = isManagerOfProcess(process, user);
		}
		return isManager;
	}

	public static Boolean isManagerOfProcess(WFProcess process, NeoUser user)
	{
		Boolean isManager = Boolean.FALSE;

		if (process != null && user != null)
		{
			isManager = SecurityEngine.getInstance().isUserInRoles(user, process.getManagers());
		}

		return isManager;
	}

	public static List<SubProcessActivity> getSubProcessListFromProcess(WFProcess process)
	{
		QLEqualsFilter subProcessFilter = new QLEqualsFilter("process", process);
		return PersistEngine.getObjects(SubProcessActivity.class, subProcessFilter);
	}

	public static List<Task> getTaksByProcessAndUser(WFProcess process, NeoUser user)
	{
		QLGroupFilter taskGroupFilter = new QLGroupFilter("AND");
		taskGroupFilter.addFilter(new QLEqualsFilter("user", user));
		taskGroupFilter.addFilter(new QLEqualsFilter("activity.process", process));
		return PersistEngine.getObjects(Task.class, taskGroupFilter);
	}

	/**
	 * Valida se o processo pode ser cancelado
	 */
	public static List<Object> validateCancelProcess(WFProcess process) throws WorkflowException
	{
		List<Object> returnList = new ArrayList<Object>();
		returnList.add(true);

		try
		{
			WorkflowEventDispatcher.dispatchValidateCancelEvent(process);
		}
		catch (WorkflowException wex)
		{
			// FIXME - Quem define senão pode cancelar dever estar no evento e não na exceção.
			//throw wex;
			returnList.clear();
			returnList.add(false);
			if (wex.getErrorList() != null && wex.getErrorList().size() > 0)
			{
				returnList.add(wex.getErrorList().get(0));
			}
			else
			{
				returnList.add("");
			}
			returnList.add(wex.getMessage());
		}

		return returnList;
	}

	/**
	 * Return all managers, in this is included: managers and fieldRef Managers
	 * staic and dynamic managers
	 *
	 * @return list with merged managers
	 */
	@Deprecated
	public static Set<SecurityEntity> getAllManagers(WFProcess process)
	{
		HashSet<SecurityEntity> set = new HashSet<SecurityEntity>();
		set.addAll(process.getModel().getManagers());
		set.addAll(WFEngine.getManagers(process));

		return set;
	}

	/**
	 * Verifica se o {@link NeoUser} passado é gestor por usuário do {@link SecurityEntity} passado
	 *
	 */
	public static boolean isUserManager(NeoUser user, SecurityEntity target)
	{
		WorkflowSecurityEntityManager seManager = PersistEngine.getObject(
				WorkflowSecurityEntityManager.class, new QLEqualsFilter("user", user));
		if (seManager != null)
		{
			for (SecurityEntity se : seManager.getSecurityEntitySet())
			{
				if (SecurityEngine.getInstance().isSecurityEntityInTarget(se, target))
					return true;

				// se for um usuario levar em conta grupo + papeis
				else if (se instanceof NeoUser)
				{
					NeoGroup group = ((NeoUser) se).getGroup();
					if (SecurityEngine.getInstance().isSecurityEntityInTarget(group, target))
						return true;

					for (NeoPaper paper : ((NeoUser) se).getPapers())
					{
						if (SecurityEngine.getInstance().isSecurityEntityInTarget(paper, target))
							return true;
					}
				}
			}
		}
		return false;
	}

	//FIXME - Remover
	/**
	 * Return managers from entity process
	 * staic and dynamic managers
	 *
	 * @return list with merged managers
	 */
	@SuppressWarnings("unchecked")
	@Deprecated
	public static Set<SecurityEntity> getManagers(WFProcess process)
	{
		HashSet<SecurityEntity> set = new HashSet<SecurityEntity>();

		if (process.getModel().getManagersFieldRef() != null
				&& process.getModel().getManagersFieldRef().getFieldInfo() != null
				&& process.getEntity() != null)
		{
			EntityWrapper wrapper = new EntityWrapper(process.getEntity());
			FieldWrapper field = wrapper.findField(process.getModel().getManagersFieldRef()
					.getFieldName());
			if (field != null)
			{
				if (process.getModel().getManagersFieldRef().getFieldInfo().isArray())
				{
					if (field.getValues() != null || field.getValues().size() > 0)
					{
						set.addAll(field.getValues());
					}
				}
				else
				{
					if (field.getValue() != null)
					{
						set.add((SecurityEntity) field.getValue());
					}
				}
			}
		}

		return set;
	}

	@SuppressWarnings("unchecked")
	public static WFProcess getRelatedParent(WFProcess process)
	{
		if (!process.isSimulation())
		{
			List<Task> tasks = PersistEngine.loadRawData(null,
					"select task from Task task join task.relatedProcessesList pr where pr.process.neoId = "
							+ process.getNeoId(), null, 1);
			if (!tasks.isEmpty())
			{
				return tasks.get(0).getProcess();
			}
		}
		return null;
	}

	public static List<WFProcess> getOpenedProcesses(ProcessModel processModel)
	{
		return getOpenedProcesses(processModel, 0, -1, -1);
	}

	public static List<WFProcess> getOpenedProcesses(ProcessModel processModel, long lastNeoId)
	{
		return getOpenedProcesses(processModel, lastNeoId, -1, -1);
	}

	public static List<WFProcess> getOpenedProcesses(ProcessModel processModel, long lastNeoId, int min,
			int max)
	{
		QLRawFilter filter = new QLRawFilter("neoId > " + lastNeoId
				+ " AND saved = true AND (processState = " + ProcessState.running.ordinal()
				+ " OR processState = " + ProcessState.suspended.ordinal() + ") AND model.neoId = "
				+ processModel.getNeoId());
		return PersistEngine.getObjects(WFProcess.class, filter, min, max);
	}

	// Cache de Modelo do Processo para Iniciar =========================================================

	@SuppressWarnings("unchecked")
	public Collection<ProcessModelProxy> getStartableModels(final NeoUser user)
	{
		if (user == null)
		{
			return null;
		}
		if (!modelsLoaded)
		{
			modelsLoaded = true;
		}

		startableModelsByUser.clear();

		if (startableModelsByUser.get(user.getNeoId()) == null)
		{
			Set<Long> modelIds = new LinkedHashSet<Long>();

			// Correção para listar somente os fluxos do tupo Usuário
			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("processType", 0));
			filter.addFilter(new QLEqualsFilter("released", true));
			filter.addFilter(new QLEqualsFilter("executable", true));
			List<ProcessModelProxy> processModels = PersistEngine.getObjects(ProcessModelProxy.class,
					filter, -1, -1, "name");
			SortedSet<ProcessModelProxy> visibleModelList = new TreeSet<ProcessModelProxy>(
					new Comparator<ProcessModelProxy>()
					{
						public int compare(final ProcessModelProxy p1, final ProcessModelProxy p2)
						{
							int x = NeoUtils.safeCompareTo(p1.getName(), p2.getName());
							if (x != 0)
								return x;
							return (int) (p1.getNeoId() - p2.getNeoId());
						}
					});
			for (ProcessModelProxy model : processModels)
			{
				if (model.getVersionControl() != null && model.getVersionControl().getCurrent() != null
						&& model.getVersionControl().getCurrent().returnUserCanStart(user, false))
				{
					modelIds.add(model.getNeoId());
					visibleModelList.add(model);
				}
			}
			startableModelsByUser.put(user.getNeoId(), modelIds);
			return visibleModelList;
		}
		else
		{
			Collection<Long> modelIds = startableModelsByUser.get(user.getNeoId());
			List<ProcessModelProxy> processModels = new ArrayList<ProcessModelProxy>();
			for (Long id : modelIds)
			{
				processModels.add((ProcessModelProxy) PersistEngine.getNeoObject(id));
			}
			return processModels;
		}
	}

	public void clearStartableModels()
	{
		modelsLoaded = false;
	}

	public void clearStartableModelsByUser(final NeoUser user)
	{
		if (user != null && startableModelsByUser != null
				&& startableModelsByUser.containsKey(user.getNeoId()))
		{
			startableModelsByUser.remove(user.getNeoId());
		}
	}

	public static WFProcess getProcessFatherFromProcess(WFProcess process)
	{
		WFProcess processFather = null;

		if (process != null && process.getCallBackTask() != null)
		{
			processFather = process.getCallBackTask().getProcess();
		}

		return processFather;
	}

}
