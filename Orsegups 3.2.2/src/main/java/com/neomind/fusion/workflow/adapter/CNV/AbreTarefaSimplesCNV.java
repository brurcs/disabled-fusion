package com.neomind.fusion.workflow.adapter.CNV;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaSimplesCNV implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Long numEmp = (Long) processEntity.findValue("numEmp");
		Long numCad = (Long) processEntity.findValue("numCad");
		String nomFun = (String) processEntity.findValue("nomFun");
		String numCpf = (String) processEntity.findValue("numCpf");
		Long codreg = (Long) processEntity.findValue("codReg");
		GregorianCalendar datAdm = (GregorianCalendar) processEntity.getValue("datAdm");
		
		NeoFile anexo = (NeoFile) processEntity.getValue("anexoCnv");

		String solicitante = "francini.guedes";
		String executor = "";
		String titulo = "";
		String descricao = "";

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

		try
		{
			if (Boolean.valueOf(String.valueOf(processEntity.getValue("posCnv"))))
			{
				titulo = "Informar CNV do colaborador " + numEmp + "/" + numCad + " - " + nomFun;
				// Alterado mensagem de acordo com solicitação via tarefa 932956 
//				descricao = " Foi constatado através do sistema Gesp que já existe uma CNV válida para o vigilante.<br>";
//				descricao = descricao + "Por gentileza justificar o motivo pelo qual esta CNV não foi apresentada com a documentação da admissão.<br><br>";
				descricao = " Foi constatado através do sistema Gesp que já existe uma CNV válida para o vigilante.<br>";
				descricao = descricao + "Por solicitação da diretoria esta tarefa foi gerada automaticamente pra tentarmos entender o motivo pelo qual o vigilante possui CNV e a mesma não foi apresentada na admissão. Segue em anexo CNV. Por gentileza protocolar a entrega para o vigilante e justifiocar o motivo da não entrega da mesma a empresa no momento da admissão.<br><br>";
				descricao += "<b>Empresa: </b>" + numEmp + "<br>";
				descricao += "<b>Matricula: </b>" + numCad + "<br>";
				descricao += "<b>Nome:</b> " + nomFun + "<br>";
				descricao += "<b>CPF:</b> " + numCpf + "<br>";
				descricao += "<b>Data de Admissão:</b> " + NeoDateUtils.safeDateFormat(datAdm,"dd/MM/yyyy") + " <br>";

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				QLEqualsFilter regional = new QLEqualsFilter("codReg", codreg);
				groupFilter.addFilter(regional);
				List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("regionaisCNV"), groupFilter);
				EntityWrapper Wrapper = new EntityWrapper(objs.get(0));
				NeoUser responsavelRegional = (NeoUser) Wrapper.findField("usuResp").getValue();

				processEntity.findField("respReg").setValue(responsavelRegional);

				NeoUser neoExecutor = responsavelRegional;
				executor = neoExecutor.getCode();
				iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "2", "sim", prazo, anexo);
			}
		}
		catch (Exception e)
		{
			try
			{
				throw new Exception("Erro ao tentar iniciar tarefa simples entrar em contato com a TI.");
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
