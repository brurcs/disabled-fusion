package com.neomind.fusion.workflow.adapter.CNV;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class EscalaTarefaCNV implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String executor ="";
		if (origin.getActivityName().toString().contains("Recebimento/Impressão CNV"))
		{
			NeoUser exe = (NeoUser) processEntity.findField("usuResp").getValue();
			executor = exe.getCode();
		}else{
			executor = "luana.martins";
		}
		String solicitante = "maurelio.pinto";
		 
		Long numEmp = (Long) processEntity.findValue("numEmp");
		Long numCad = (Long) processEntity.findValue("numCad");
		String nomFun = (String) processEntity.findValue("nomFun");
		String numCpf = (String) processEntity.findValue("numCpf");
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String titulo = "Informar motivo não realização da tarefa "+origin.getCode();
		String descricao = "<strong>Por gentileza informar o motivo pela qual a tarefa a baixo não foi atendida:</strong><br><br>";
		descricao += "Tarefa:"+origin.getCode()+"<br>";
		descricao += "Empresa:"+numEmp+"<br>";
		descricao += "Matricula:"+numCad+"<br>";
		descricao += "Nome:"+nomFun+"<br>";
		descricao += "CPF:"+numCpf+"<br>";
		
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "2", "sim", prazo);
		System.out.println(""+tarefa);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
