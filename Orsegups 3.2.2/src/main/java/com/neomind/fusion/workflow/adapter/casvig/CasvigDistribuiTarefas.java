package com.neomind.fusion.workflow.adapter.casvig;

import java.util.Collection;

import javax.persistence.Entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.model.ProcessModel;

public class CasvigDistribuiTarefas implements AdapterInterface
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigDistribuiTarefas.class);

	@Override
	public void start(Task origin, EntityWrapper wrapperProcessoPlanoAcao, Activity activity)
	{
		if (wrapperProcessoPlanoAcao != null)
		{
		    /* Instância um processModel para o subprocesso "Validar Tarefa Plano Ação" */
			QLEqualsFilter equal = new QLEqualsFilter("Name", "Tarefa de Plano de Ação"); 
			ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

			/* planoList coleta cada uma das linhas do eform PlanoAcao que corresponde ao ProcessoPlanoAcao */
			Collection planoList = (Collection) wrapperProcessoPlanoAcao.findValue("PlanoAcao");
					    
			for (Object oPlano : planoList)
			{						

				NeoObject noPlano = (NeoObject) oPlano;
				
				/* TESTA PARA VER SE O PROCESSO JA FOI CRIADO!!! */
				QLGroupFilter filter = new QLGroupFilter("AND");				
				/* Nome do modelo do subProcesso */
				filter.addFilter(new QLEqualsFilter("model.name","Tarefa de Plano de Ação"));				
				/* Eforms do subProcesso*/
				filter.addFilter(new QLEqualsFilter("entity",noPlano)); 				
				WFProcess wfProcess = (WFProcess)PersistEngine.getNeoObject(WFProcess.class, filter);
								
				if (wfProcess == null)
				{
					//FIXME NEOMIND Alterar para o novo padão
					Activity atividade = null;
					
					/* Busca a referência para proxíma atividade */						
//					Activity atividade = (Activity) getSplit().getCondition2TaskSet().iterator().next().getActivitySet().iterator().next();
					
					/* Insere a próxima atividade (referência) no eform do subProcesso que será iniciado */	
					EntityWrapper wrapperPlanoAcao = new EntityWrapper((NeoObject) oPlano);			
					wrapperPlanoAcao.setValue("NextActivityLink", atividade);
					wrapperPlanoAcao.setValue("Solicitante", wrapperProcessoPlanoAcao.findValue("Solicitante")); 
	
					/*Inicia um novo processo */
					OrsegupsWorkflowHelper.iniciaProcesso(processModel, (NeoObject) oPlano, false, PortalUtil.getCurrentUser());					
				}
			}
		}
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}