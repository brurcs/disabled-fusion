package com.neomind.fusion.workflow.adapter.reciclagem;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.workflow.adapter.reciclagem.AlterarAfastamentoReciclagemAdapter
public class AlterarAfastamentoReciclagemAdapter implements AdapterInterface
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(InserirAfastamentoReciclagemAdapter.class);
	EntityManager entityManager;
	EntityTransaction transaction;
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try{
			String numemp = NeoUtils.safeOutputString( processEntity.findValue("numemp"));
			String tipcol = NeoUtils.safeOutputString( processEntity.findValue("tipcol"));
			String numcad = NeoUtils.safeOutputString( processEntity.findValue("numcad"));
			GregorianCalendar datafa =  (GregorianCalendar) processEntity.findValue("dataCurso");
			String horini = "0";
			GregorianCalendar datTermino  = (GregorianCalendar) processEntity.findValue("dataFimCurso");
			String horFim = "0";
			
			EscalaVO escala = QLPresencaUtils.getEscalaColaborador(NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), NeoUtils.safeLong(tipcol));
			
			String codesc = NeoUtils.safeOutputString( escala.getCodigoEscala() );
			String codtma = NeoUtils.safeOutputString( escala.getCodigoTurma() );
		
			alteraSituacaoAfastamento(numemp, tipcol, numcad, datafa, codesc, codtma, horini, datTermino, horFim);
		}
		catch (Exception e)
		{
			throw new WorkflowException("Erro ao inserir afastamento de reciclagem para o colaborador");
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
	
	private void alteraSituacaoAfastamento(String numemp, String tipcol, String numcad, GregorianCalendar datafa, String codesc, String codtma, String horini, GregorianCalendar datTermino, String horFim) throws Exception
	{

		this.entityManager = PersistEngine.getEntityManager("VETORH");
		this.transaction = this.entityManager.getTransaction();

		Integer result = 0;

		try
		{
			GregorianCalendar dataNula = new GregorianCalendar(1900, GregorianCalendar.DECEMBER, 31);
			GregorianCalendar dataInicioAfa = new GregorianCalendar();
			GregorianCalendar datFimAfa = new GregorianCalendar();
			GregorianCalendar dataAtual = new GregorianCalendar();
			
			dataInicioAfa = (GregorianCalendar) datafa.clone();
			datFimAfa = (GregorianCalendar) datTermino.clone();
			
			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}

			NeoUser user = PortalUtil.getCurrentUser();
			String usuario = "sistema.fusion";
			if (user != null){
				usuario = user.getCode();
			}
			StringBuilder sqlUpdateAfastamento = new StringBuilder();

			String dataString = NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm");
			String ObsAfa = "Alterado via tarefa de reciclagem por " + usuario + " em " + dataString;
			
			sqlUpdateAfastamento.append(" UPDATE R038AFA "); 
			sqlUpdateAfastamento.append(" 	SET sitafa = :sitafa, "); 
			sqlUpdateAfastamento.append(" 	ObsAfa = :ObsAfa, "); 
			sqlUpdateAfastamento.append(" 	usu_afainf = 'N', "); 
			sqlUpdateAfastamento.append(" 	sitori= :sitafa, "); 
			sqlUpdateAfastamento.append(" 	datalt = :dataAtual ");
			sqlUpdateAfastamento.append(" WHERE 1=1 "); 
			sqlUpdateAfastamento.append(" AND tipcol = :tipcol ");
			sqlUpdateAfastamento.append(" AND numemp = :numemp ");
			sqlUpdateAfastamento.append(" AND numcad = :numcad ");
			sqlUpdateAfastamento.append(" AND sitafa = 117 ");
			sqlUpdateAfastamento.append(" AND CAST(FLOOR(CAST(datafa AS float)) AS datetime) = :dataInicio ");
			sqlUpdateAfastamento.append(" AND (CAST(FLOOR(CAST(datter AS float)) AS datetime) = :dataTermino OR datter = :dataNula ) "); 

			

			Query queryAlteraAfastamento = this.entityManager.createNativeQuery(sqlUpdateAfastamento.toString());

			queryAlteraAfastamento.setParameter("numemp", numemp);
			queryAlteraAfastamento.setParameter("tipcol", tipcol);
			queryAlteraAfastamento.setParameter("numcad", numcad);
			queryAlteraAfastamento.setParameter("dataInicio", new Timestamp(dataInicioAfa.getTime().getTime()));
			queryAlteraAfastamento.setParameter("dataTermino", new Timestamp(datFimAfa.getTime().getTime()));
			queryAlteraAfastamento.setParameter("ObsAfa", ObsAfa);
			queryAlteraAfastamento.setParameter("dataAtual", new Timestamp(dataAtual.getTime().getTime()));
			queryAlteraAfastamento.setParameter("dataNula", dataNula);
			queryAlteraAfastamento.setParameter("sitafa", 15L);

			result = queryAlteraAfastamento.executeUpdate();
			
			if (result > 0){
				
				//LOG Colaborador
				String txtLog = "Alterado afastamento em " + NeoUtils.safeDateFormat(datafa, "dd/MM/yyyy HH:mm")+" pois colaborador não realizou a reciclagem.";

				StringBuilder buscaCpf = new StringBuilder();
				buscaCpf.append(" SELECT numcpf FROM R034FUN WHERE numemp = :numemp ");
				buscaCpf.append(" AND tipcol = :tipcol ");
				buscaCpf.append(" AND numcad = :numcad ");

				Query queryBuscaCpf = this.entityManager.createNativeQuery(buscaCpf.toString());
				queryBuscaCpf.setParameter("numemp", numemp);
				queryBuscaCpf.setParameter("tipcol", tipcol);
				queryBuscaCpf.setParameter("numcad", numcad);
				queryBuscaCpf.setMaxResults(1);

				BigInteger cpf = (BigInteger) queryBuscaCpf.getSingleResult();
				String cpfString = cpf.toString();

				if (cpfString != null && !cpfString.equals(""))
				{
					this.saveLog(txtLog, null, cpfString, "colaborador", null);
				}

				log.warn("Cadastro Afastamento - NumEmp:" + numemp + " - NumCad: " + numcad + ")");
				
			}
			this.transaction.commit();
		
		}
		catch (Exception ex)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			ex.printStackTrace();
			System.out.println("Erro ao inserir afastamento de reciclagem. ");
			throw new Exception("Erro ao inserir afastamento de reciclagem. ", ex);
		}

	}
	
	private void saveLog(String texto, String idposto, String idColaborador, String tipo, PrintWriter out)
	{
		if (tipo != null && tipo.equalsIgnoreCase("posto"))
		{
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("codloc", idposto));

			/**
		 	* @author orsegups lucas.avila - Classe generica.
		 	* @date 08/07/2015
		 	*/
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8");

			NeoObject postoObject = PersistEngine.getObject(clazz, groupFilter);

			if (postoObject != null)
			{
				NeoObject logPosto = AdapterUtils.createNewEntityInstance("QLLogPresencaPosto");

				if (logPosto != null)
				{
					NeoUser user = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", 578002607L ));
					if (PortalUtil.getCurrentUser() != null){
						user = PortalUtil.getCurrentUser();
					}
					
					
					EntityWrapper postoWrapper = new EntityWrapper(logPosto);
					postoWrapper.setValue("posto", postoObject);
					postoWrapper.setValue("dataLog", new GregorianCalendar());
					postoWrapper.setValue("textoLog", texto);
					postoWrapper.setValue("usuario", PortalUtil.getCurrentUser());

					PersistEngine.persist(logPosto);
					if (out != null)
					{
						out.print("Log salvo com sucesso.");
					}
					return;
				}
			}
		}
		else if (tipo != null && tipo.equalsIgnoreCase("colaborador"))
		{
			NeoObject log = AdapterUtils.createNewEntityInstance("QLLogPresencaColaborador");

			if (log != null && idColaborador != null)
			{
				texto = texto.replaceAll("'", "´");
				texto = texto.replaceAll("\"", "´");

				EntityWrapper logWrapper = new EntityWrapper(log);
				logWrapper.setValue("cpf", Long.valueOf(idColaborador));
				logWrapper.setValue("dataLog", new GregorianCalendar());
				logWrapper.setValue("textoLog", texto);
				logWrapper.setValue("usuario", PortalUtil.getCurrentUser());

				if (idposto != null)
				{
					QLGroupFilter groupFilter = new QLGroupFilter("AND");
					groupFilter.addFilter(new QLEqualsFilter("codloc", idposto));

					/**
		 			* @author orsegups lucas.avila - Classe generica.
		 			* @date 08/07/2015
		 			*/
					Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8");

					NeoObject postoObject = PersistEngine.getObject(clazz, groupFilter);

					if (postoObject != null)
					{
						logWrapper.setValue("posto", postoObject);
					}
				}

				PersistEngine.persist(log);

				if (out != null)
				{
					out.print("Log salvo com sucesso.");
				}
				return;
			}
		}

		out.print("Erro ao salvar log.");
	}

	
	
}


