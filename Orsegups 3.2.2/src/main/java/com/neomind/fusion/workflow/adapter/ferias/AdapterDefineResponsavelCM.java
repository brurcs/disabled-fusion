package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class AdapterDefineResponsavelCM implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		NeoPaper responsavel = null;
		NeoUser usuRes = null;
		
		Long numReg = (Long) processEntity.getValue("codregFer");
		
		if (numReg != null && numReg.equals(9)){
		    responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelFluxoFeriasRegionalNoveCM"));
		}else{
		    boolean isDiurno = (boolean) processEntity.findField("diurno").getValue();
		    if (isDiurno)
		    {
			responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelFluxoFeriasDiurnoCM"));
		    }
		    else
		    {
			responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelFluxoFeriasNoturnoCM"));
		    }
		}
		

		for (NeoUser usr : responsavel.getUsers())
		{
			usuRes = usr;
			break;
		}

		if (usuRes != null)
		{
			processEntity.setValue("responsavelCM", usuRes);
		}
		else
		{
			throw new WorkflowException("Não foi encontrato um usuário de destino para esta tarefa!");
		}

		NeoObject colaborador = (NeoObject) processEntity.findField("primeiraSugestao").getValue();
		EntityWrapper wColaborador = new EntityWrapper(colaborador);

		long numemp = (long) wColaborador.findField("numemp").getValue();
		long numcad = (long) wColaborador.findField("numcad").getValue();
		String nomReg = String.valueOf(wColaborador.getValue("usu_nomreg"));
		String nomFun = String.valueOf(wColaborador.findField("nomfun").getValue());

		String tituloFluxo = numemp + "/" + numcad + " - " + nomFun + " - " + nomReg;
		processEntity.setValue("tituloFluxo", tituloFluxo);

		//Definir prazo
		GregorianCalendar prazoTarefa = new GregorianCalendar();
		prazoTarefa.add(GregorianCalendar.DATE, 2);
		if (!OrsegupsUtils.isWorkDay(prazoTarefa))
		{
			prazoTarefa = OrsegupsUtils.getNextWorkDay(prazoTarefa);
		}

		prazoTarefa.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazoTarefa.set(GregorianCalendar.MINUTE, 59);
		prazoTarefa.set(GregorianCalendar.SECOND, 59);
		System.out.println("Data : "+NeoDateUtils.safeDateFormat(prazoTarefa,"dd/MM/yyyy"));
		processEntity.setValue("prazoTarefa", prazoTarefa);

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
