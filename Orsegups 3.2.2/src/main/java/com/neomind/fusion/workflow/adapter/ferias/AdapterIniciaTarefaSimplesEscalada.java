package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class AdapterIniciaTarefaSimplesEscalada implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar prazo = new GregorianCalendar();

		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);
		prazo.set(GregorianCalendar.MILLISECOND, 0);
		NeoObject colaborador = (NeoObject) processEntity.findField("primeiraSugestao").getValue();

		EntityWrapper wColaborador = new EntityWrapper(colaborador);
		long numemp = (long) wColaborador.findField("numemp").getValue();
		long numcad = (long) wColaborador.findField("numcad").getValue();
		String nomFun = String.valueOf(wColaborador.findField("nomfun").getValue());

		String titulo = "Tarefa escalada - R001 - Fluxo de férias - Código " + activity.getCode() + " - " + numemp + "/" + numcad + " - " + nomFun;

		StringBuilder descricao = new StringBuilder();
		descricao.append("Informar o motivo da não realização da tarefa " + activity.getCode() + " do fluxo de férias no prazo.<br><br>");
		descricao.append("Colaborador de férias:<br>");
		descricao.append("Empresa: " + numemp + " <br>");
		descricao.append("Matricula: " + numcad + " <br>");
		descricao.append("Nome: " + nomFun + ".");

		NeoUser executorNeo = null;
		NeoPaper responsavel = null;

		Boolean isDiurno = (Boolean) processEntity.getValue("diurno");
		if (isDiurno)
		{
			responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelFluxoFeriasDiurnoCM"));
		}
		else
		{
			responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelFluxoFeriasNoturnoCM"));
		}

		for (NeoUser usr : responsavel.getUsers())
		{
			executorNeo = usr;
			break;
		}

		String executor = executorNeo.getCode();
		String solicitante = "fernanda.martins";

		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
		String code = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao.toString(), "1", "Sim", prazo);
		System.out.println("code: " + code);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
