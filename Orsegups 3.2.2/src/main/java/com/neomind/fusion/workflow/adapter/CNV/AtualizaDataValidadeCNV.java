package com.neomind.fusion.workflow.adapter.CNV;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoCalendarUtils;

public class AtualizaDataValidadeCNV implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar datValCNV = (GregorianCalendar) processEntity.findValue("datValCNV");
		GregorianCalendar dataVenCnv = (GregorianCalendar) processEntity.findValue("dataVenCnv");
		Long numEmp = (Long) processEntity.findValue("numEmp");
		Long numCad = (Long) processEntity.findValue("numCad");
		String numCNV = (String) processEntity.findValue("numCNV");
		String query = "";
		Connection conn = null;
		PreparedStatement pst = null;
		
		try
		{
			query = "UPDATE USU_TCADCUR SET USU_DATVALCNV = ? , USU_NUCANAVI = ? WHERE USU_NUMEMP = ? AND USU_NUMCAD = ? AND USU_TIPCOL = 1 ";
			conn = PersistEngine.getConnection("VETORH");
			pst = conn.prepareStatement(query);
			if (Boolean.valueOf(String.valueOf(processEntity.getValue("posCnv")))){
				pst.setDate(1, new Date(dataVenCnv.getTimeInMillis()));
			}else{
				pst.setDate(1, new Date(datValCNV.getTimeInMillis()));
			}
			pst.setString(2, numCNV);
			pst.setLong(3, numEmp);
			pst.setLong(4, numCad);
			pst.executeUpdate();

		}
		catch (Exception e1)
		{
			try
			{
				System.out.println("Erro ao tentar atualizar data do curso do colaborador, por gentileza entrar em contato com a TI.");
				throw new Exception("Erro ao tentar atualizar data do curso do colaborador, por gentileza entrar em contato com a TI.");
			}
			catch (Exception e2)
			{
				e2.printStackTrace();
			}
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
