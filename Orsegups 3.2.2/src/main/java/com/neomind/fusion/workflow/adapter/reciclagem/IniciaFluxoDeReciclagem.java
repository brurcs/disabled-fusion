package com.neomind.fusion.workflow.adapter.reciclagem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;

public class IniciaFluxoDeReciclagem implements CustomJobAdapter

{

	@Override
	public void execute(CustomJobContext ctx)
	{
		if (OrsegupsUtils.isWorkDay(new GregorianCalendar()))
		{
			System.out.println("[RCL] - Inicia fluxo de reciclagem iniciado com sucesso");
			processaJob(ctx);
		}
	}

	public static void processaJob(CustomJobContext ctx)
	{
		Connection conn = PersistEngine.getConnection("VETORH");

		GregorianCalendar datCorte = new GregorianCalendar();
		datCorte.add(Calendar.DATE, 60);
		datCorte.set(Calendar.HOUR_OF_DAY, 0);
		datCorte.set(Calendar.MINUTE, 0);
		datCorte.set(Calendar.SECOND, 0);
		datCorte.set(Calendar.MILLISECOND, 0);

		/*
		 * GregorianCalendar datref = new GregorianCalendar();
		 * datref.set(Calendar.DAY_OF_MONTH, 28);
		 * datref.set(Calendar.MONTH, 2);
		 * datref.set(Calendar.HOUR_OF_DAY, 0);
		 * datref.set(Calendar.MINUTE, 0);
		 * datref.set(Calendar.SECOND, 0);
		 * datref.set(Calendar.MILLISECOND, 0);
		 */
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		try
		{
			System.out.println();
			sql.append(" Select fun.numemp, fun.numcad, fun.tipcol, fun.nomfun, fun.datadm, fun.numcpf, cur.USU_DatFor, ");
			sql.append("		CASE WHEN cur.USU_DatRec = '1900-12-31' THEN DATEADD(yy,2,cur.USU_DatFor) ELSE DATEADD(yy,2,cur.USU_DatRec) END AS datven, ");
			sql.append(" 		usu_codccu, reg.USU_CodReg, reg.USU_NomReg, car.titred, orn.usu_codreg, ");
			sql.append(" 		bai.NomBai, cid.NomCid, cid.EstCid,  (cpl.endrua+ ', N.' + cpl.endnum + ', Complemento:'+cpl.endcpl) as endereco, ");
			sql.append(" 		cpl.numres as numtel1, cpl.numele as numtel2, ( cast( cpl.dddtel as varchar) + ' ' + cast(cpl.numtel as varchar)) as numtel3 ");
			sql.append(" 		, ( cast( cpl.nmddd2 as varchar) + ' ' + cast(cpl.nmtel2 as varchar)) as numtel4, orn.usu_telloc, fun.usu_emapon  ");
			sql.append("		,orn.numloc, orn.taborg	");
			sql.append(" From USU_TCADCUR cur with(nolock)");
			sql.append(" Inner Join R034FUN fun with(nolock) On fun.numemp = cur.USU_NumEmp And fun.tipcol = cur.USU_TipCol And fun.numcad = cur.USU_NumCad ");

			sql.append(" LEFT JOIN R034CPL cpl with(nolock) ON cpl.NumEmp = fun.NumEmp AND cpl.TipCol = fun.TipCol AND cpl.NumCad = fun.NumCad ");
			sql.append(" left join R074CID cid with(nolock) on cpl.codcid = cid.CodCid ");
			sql.append(" left join R074BAI bai with(nolock) on cpl.codbai = bai.CodBai and bai.CodCid = cid.codcid ");

			sql.append(" Inner Join R016ORN orn with(nolock) On orn.numloc = fun.numloc And orn.taborg = fun.taborg ");
			sql.append(" Inner Join R024CAR car with(nolock) On car.CodCar = fun.CodCar And car.EstCar = fun.estcar ");
			sql.append(" Inner Join USU_T200REG reg with(nolock) On reg.USU_CodReg = orn.usu_codreg ");
			sql.append(" Where   ");
			sql.append(" 		(((cur.USU_DatRec = '1900-12-31' And DATEADD(yy,2,cur.USU_DatFor) < ?) Or ");
			sql.append(" (cur.USU_DatRec <> '1900-12-31' And DATEADD(yy,2,cur.USU_DatRec)  < ?)))  ");
//			sql.append(" 		(((cur.USU_DatRec = '1900-12-31' And DATEADD(yy,2,cur.USU_DatFor) < '2001-06-01') Or ");
//			sql.append(" (cur.USU_DatRec <> '1900-12-31' And DATEADD(yy,2,cur.USU_DatRec)  < '2019-05-01')))  ");
//			sql.append(" fun.numemp = 15 and fun.numcad = 7742 ");
			sql.append(" AND fun.sitafa in (1,2,11,12,14,15,20,21,23,24,25,26,27,28,29,30,49,51,52,55,61,62,63,64,65,66,78,81,82,83,84,85,86,117,119,120,121) ");
			sql.append("  And ((car.titred Like 'VIGIL%' or car.codcar = '0001026' or car.TitCar like '%Fscal%') or (car.TitCar like '%INSPETOR DE SEGURAN%') ");
			sql.append("  or (car.TitCar like 'vigil%(3.3%)') or (car.TitCar like '%Supervisor Patrimonial PL%') or (car.TitCar like '%chefe de equipe socorrista%') "); 
			sql.append("  or (car.TitCar like 'fiscal%') or (car.TitCar like '%TECNICO SEGURANCA DO TRABALHO%') or (car.TitCar like '%Chefe de equipe%')  ");
			sql.append("  or (car.TitCar like '%Supervisor de vigilante%') or (car.TitCar like '%Horista HS%'))  ");
			sql.append(" And NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaAgendamentoReciclagens ar WITH (NOLOCK) ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.WFPROCESS WF with(nolock)ON WF.code = ar.tarefa ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.PROCESSMODEL PM with(nolock) ON PM.NEOID = WF.MODEL_NEOID AND WF.PROCESSSTATE = 0 and wf.saved = 1 and PM.name like '%G001%' ");
			sql.append(" 			  	 WHERE ar.numcpf = fun.numcpf AND WF.startDate > '2017-09-12' AND CAST(floor(CAST(ar.datven AS FLOAT)) AS datetime) = CAST(floor(CAST(CASE WHEN cur.USU_DatRec = '1900-12-31' THEN DATEADD(yy,2,cur.USU_DatFor) ELSE DATEADD(yy,2,cur.USU_DatRec) END AS FLOAT)) AS datetime)) ");
			sql.append(" AND NOT EXISTS ");
			sql.append(" (SELECT 1 FROM [CACUPE\\SQL02].fusion_producao.DBO.D_TAREFASDERECICLAGEM TR with(nolock) INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.WFPROCESS WF with(nolock) ON WF.NEOID = TR.WFPROCESS_NEOID");
			sql.append(" AND WF.PROCESSSTATE = 0 and wf.saved = 1 ");
			sql.append(" AND FUN.NUMCPF = TR.NUMCPF");
			sql.append(" )");
			sql.append(" AND NOT EXISTS ");
			sql.append(" (select 1 from  [CACUPE\\SQL02].fusion_producao.DBO.D_Tarefa d with(nolock) inner join [CACUPE\\SQL02].fusion_producao.DBO.WFPROCESS w with(nolock) on d.wfprocess_neoId = w.neoId where d.titulo like 'Justificar não convocação %'+fun.nomfun+'%' and w.processState = 0)");
//			sql.append(" Order By reg.USU_CodReg, fun.numemp, datven ");
//			sql.append("  and fun.numcad = 2735 and fun.numemp = 1");
			sql.append(" AND fun.tipcol = 1");
			sql.append(" Order By fun.nomfun desc ");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setTimestamp(1, new Timestamp(datCorte.getTimeInMillis()));
			pstm.setTimestamp(2, new Timestamp(datCorte.getTimeInMillis()));
			rs = pstm.executeQuery();
			Long cont = 0L;
			while (rs.next())
			{
				InstantiableEntityInfo reciclagem = AdapterUtils.getInstantiableEntityInfo("tarefasDeReciclagem");
				NeoObject objReciclagem = reciclagem.createNewInstance();
				EntityWrapper wReciclagem = new EntityWrapper(objReciclagem);
				wReciclagem.findField("numemp").setValue(rs.getLong("numemp"));
				wReciclagem.findField("numcad").setValue(rs.getLong("numcad"));
				wReciclagem.findField("tipcol").setValue(rs.getLong("tipcol"));
				wReciclagem.findField("nomfun").setValue(rs.getString("nomfun"));
				wReciclagem.findField("numcpf").setValue(rs.getLong("numcpf"));

				wReciclagem.findField("endereco").setValue(rs.getString("endereco"));
				wReciclagem.findField("nomBai").setValue(rs.getString("nomBai"));
				wReciclagem.findField("nomCid").setValue(rs.getString("nomCid"));
				wReciclagem.findField("estCid").setValue(rs.getString("estCid"));
				wReciclagem.findField("emapon").setValue(rs.getString("usu_emapon"));

				wReciclagem.findField("codreg").setValue(rs.getLong("USU_CodReg"));
				wReciclagem.findField("nomreg").setValue(rs.getString("USU_NomReg"));
				wReciclagem.findField("codccu").setValue(rs.getLong("usu_codccu"));
				wReciclagem.findField("titcar").setValue(rs.getString("titred"));
				wReciclagem.findField("jaSolicitouAjuste").setValue(false);
				GregorianCalendar datfor = new GregorianCalendar();
				datfor.setTime(rs.getDate("USU_DatFor"));
				wReciclagem.findField("datfor").setValue(datfor);
				GregorianCalendar datAdm = new GregorianCalendar();
				datAdm.setTime(rs.getDate("datadm"));
				wReciclagem.findField("datadm").setValue(datAdm);
				GregorianCalendar datven = new GregorianCalendar();
				datven.setTime(rs.getDate("datven"));
				wReciclagem.findField("datVenCur").setValue(datven);

				PersistEngine.persist(objReciclagem);

				NeoPaper responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "RCLresponsavel"));

				NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "usuario_reciclagem"));
				//NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "jorge.filho"));
				NeoUser neoExecutor = new NeoUser();

				if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
				{
					for (NeoUser user : responsavel.getUsers())
					{

						neoExecutor = user;
						break;
					}
				}
				else
				{
					continue;
				}
				QLEqualsFilter equal = new QLEqualsFilter("Name", "RCL - informar reciclagem");
				ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

				//final WFProcess processo = WorkflowService.startProcess(processModel, objReciclagem, false, solicitante);
				final WFProcess processo = processModel.startProcess(objReciclagem, false, null, null, null, null, solicitante);
				processo.setRequester(solicitante);
				processo.setSaved(true);
				PersistEngine.persist(processo);
				PersistEngine.commit(true);

				try
				{
					new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(neoExecutor, false);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				InstantiableEntityInfo tarefaReciclagem = AdapterUtils.getInstantiableEntityInfo("TarefaAgendamentoReciclagens");
				NeoObject noAJ = tarefaReciclagem.createNewInstance();
				EntityWrapper ajWrapper = new EntityWrapper(noAJ);
				ajWrapper.findField("numcpf").setValue(rs.getLong("numcpf"));
				ajWrapper.findField("datven").setValue(datven);
				ajWrapper.findField("tarefa").setValue(processo.getCode());
				PersistEngine.persist(noAJ);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			System.out.println("[RCL] - Inicia fluxo de reciclagem terminado com sucesso.");
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}

}

