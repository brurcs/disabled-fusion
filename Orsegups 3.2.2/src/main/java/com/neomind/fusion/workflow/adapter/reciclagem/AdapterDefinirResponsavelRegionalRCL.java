package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class AdapterDefinirResponsavelRegionalRCL implements AdapterInterface
{
    private static final Log log = LogFactory.getLog(AdapterDefinirResponsavelRegionalRCL.class);
    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity){
	Long key = (new GregorianCalendar()).getTimeInMillis();
	try{
	    Long regional = (Long) processEntity.findValue("codreg");
	    NeoUser responsavelReg = null;
	    InstantiableEntityInfo infoRegional = AdapterUtils.getInstantiableEntityInfo("RCLResponsavelRegional");
	    NeoObject eRegional = (NeoObject) PersistEngine.getObject(infoRegional.getEntityClass(), new QLEqualsFilter("codigoRegional.codigo", regional));
	    if (eRegional != null){
		EntityWrapper reg = new EntityWrapper(eRegional);
		NeoPaper papel = (NeoPaper) reg.findField("responsavel").getValue();
		if (papel != null){
		    for(NeoUser user: papel.getUsers()){
			responsavelReg = user;
			break;
		    }
		}
	    }
	    processEntity.findField("respReg").setValue(responsavelReg);
	}catch (Exception e){
	    log.error("["+key+"] Erro ao executar a classe AdapterDefinirResponsavelRegionalRCL "+e.getMessage());
	    e.printStackTrace();
	    throw new WorkflowException(e.getMessage());
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity){
	
    }

}
