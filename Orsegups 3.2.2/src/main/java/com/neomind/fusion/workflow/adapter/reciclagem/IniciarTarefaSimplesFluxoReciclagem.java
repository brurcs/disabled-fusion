package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaSupervisaoUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class IniciarTarefaSimplesFluxoReciclagem implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String solicitante = OrsegupsUtils.getUserNeoPaper("RCLSolicitanteCobertura");
		String executor = "";
		String titulo = "";
		String descricao = "";
		
		GregorianCalendar dataPrazo = new GregorianCalendar();
		dataPrazo.add(GregorianCalendar.DATE, 3);
		//OrsegupsUtils.getNextWorkDay(dataPrazo);
		Long numcad = (Long) processEntity.findValue("numcad");
		Long numemp = (Long) processEntity.findValue("numemp");
		Long tipcol = (Long) processEntity.findValue("tipcol");
		Long codccu = (Long) processEntity.findValue("codccu");
		String nomFun = (String) processEntity.findValue("nomfun");
		String titcar = (String) processEntity.findValue("titcar");
		Long codReg = (Long) processEntity.findValue("codreg");
		GregorianCalendar datadm = (GregorianCalendar) processEntity.findValue("datadm");
		GregorianCalendar datven = (GregorianCalendar) processEntity.findValue("datVenCur");
		GregorianCalendar dataCurso = (GregorianCalendar) processEntity.findValue("dataCurso");
		GregorianCalendar dataFimCurso = (GregorianCalendar) processEntity.findValue("dataFimCurso");
		
		
		String nomBai = NeoUtils.safeOutputString(processEntity.findValue("nomBai"));
		String nomCid = NeoUtils.safeOutputString(processEntity.findValue("nomCid"));
		String estCid = NeoUtils.safeOutputString(processEntity.findValue("estCid"));
		String endereco = NeoUtils.safeOutputString(processEntity.findValue("endereco"));
		String numTel = NeoUtils.safeOutputString(processEntity.findValue("numTel"));
		String emapon = NeoUtils.safeOutputString(processEntity.findValue("emapon"));
		
		
		String obsConvocacao = NeoUtils.safeOutputString(processEntity.findValue("obs"));
		String obsCurso= NeoUtils.safeOutputString(processEntity.findValue("obsCurso"));
		
		
		
		ColaboradorVO colab = QLPresencaSupervisaoUtils.retornaSupervisorPosto(numemp, numcad, tipcol, codccu);
		NeoUser executorNeo = null;
		if (colab != null)
		{
			System.out.println("Colaborador: " + colab);
			if (codReg == 2){// Solicitado via tarefa simples 1056831 / alterado tarefa 1172719
			    executor = "andreia.figueredo";
			}else{
			    executor = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_USUARIO_FUSION, codReg, QLPresencaSupervisaoUtils.recuperaSupervisorExecao(colab.getCpf()), null).getCode(); // aqui a regional não pode ser 0, então passo a lotação nula
			}
			
			if (executor.equals("")){
			    executor = OrsegupsUtils.getUserNeoPaper("RCLExecutorCobertura");
			}
			executorNeo = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
			processEntity.setValue("executorResponsavel",executorNeo);
		}
		else
		{
		    	if (codReg == 2){// Solicitado via tarefa simples 1056831 / alterado tarefa 1172719
			    executor = "andreia.figueredo";
		    	}else{
		    	    executor = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_COORDENADOR_REGIONAL, codReg, null, null).getCode(); // aqui a regional não pode ser 0, então passo a lotação nula
		    	}
		    	if (executor.equals("")){
		    	    executor = OrsegupsUtils.getUserNeoPaper("RCLExecutorCobertura");
			}
			executorNeo = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
			processEntity.setValue("executorResponsavel",executorNeo);
		}
		
		boolean adicionarObsConvocacao = false;
		boolean adicionarObsCurso = false;
		
		if (origin.getActivityName().toString().contains("Convocar Vigilante Reciclagem"))
		{
			titulo = "Justificar não convocação "  + numemp + "/" + numcad + " - " + nomFun ;
			descricao = "<strong>Justificar não convocação</strong><br><br>";
			adicionarObsConvocacao = true;
			if (codReg == 2){// Solicitado via tarefa simples 1056831 / alterado tarefa 1172719
			    executor = "andreia.figueredo";
    			}else{
        			NeoPaper coordenador = OrsegupsUtils.getPapelCoordenadorRegional(codReg);
        			executorNeo = null;
        			executor = "";
        			
        			for(NeoUser u : coordenador.getUsers()){
        				executor = u.getCode();
        				executorNeo = u;
        				break;
        			}
    			}
			if(executorNeo == null){
			    executor = OrsegupsUtils.getUserNeoPaper("RCLExecutorCobertura");
			}			
			executorNeo = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
			processEntity.setValue("executorResponsavel",executorNeo);
		}
		else if (origin.getActivityName().toString().contains("Confirmar realização do curso"))
		{
			titulo = "Justificar não realização do curso de reciclagem "  + numemp + "/" + numcad + " - " + nomFun ;
			descricao = "<strong>Justificar não comparecimento ao curso de reciclagem</strong><br><br>";
			adicionarObsCurso = true;
		}
		else
		{
			titulo = "Informar cobertura referente ao período de reciclagem do colaborador abaixo "  + numemp + "/" + numcad + " - " + nomFun ;
			descricao = "<strong>informar cobertura referente ao período de reciclagem do colaborador abaixo:</strong><br><br>";
			executor = OrsegupsUtils.getUserNeoPaper("RCLExecutorCobertura");
			executorNeo = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
			processEntity.setValue("executorResponsavel",executorNeo);
		}
		
		descricao += " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomFun + "<br>";
		descricao += " <strong>Cargo:</strong> " + titcar + "<br>";
		descricao += " <strong>Fone:</strong> " + numTel + "<br>";
		descricao += " <strong>Endereço:</strong> " + endereco + "<br>";
		descricao += " <strong>Bairro:</strong> " + nomBai + "<br>";
		descricao += " <strong>Cidade:</strong> " + nomCid + "<br>";
		descricao += " <strong>Estado:</strong> " + estCid + "<br>";
		descricao += " <strong>E-mail:</strong> " + emapon + "<br>";
		descricao += " <strong>Admissão:</strong> " + NeoUtils.safeDateFormat(datadm, "dd/MM/yyyy") + "<br>";
		descricao += " <strong>Vencimento:</strong> " + NeoUtils.safeDateFormat(datven, "dd/MM/yyyy") + "<br>";
		descricao += " <strong>Centro de Custo:</strong> " + codccu + "<br>";
		if (dataCurso != null)
		{
			descricao += " <strong>Data do curso:</strong> " + NeoUtils.safeDateFormat(dataCurso, "dd/MM/yyyy") +  " à " + NeoUtils.safeDateFormat(dataFimCurso, "dd/MM/yyyy") + "<br>";
		}
		
		if (adicionarObsConvocacao){
			descricao += " <strong>Obs. não Convocação:</strong> " + obsConvocacao + "<br>";
		}
		
		if (adicionarObsCurso){
			descricao += " <strong>Obs. não Realização do Curso :</strong> " + obsCurso + "<br>";
		}

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		GregorianCalendar prazo = new GregorianCalendar();
		System.out.println("Prazo da tarefa simples de reciclagem: " + NeoDateUtils.safeDateFormat(dataPrazo));
		if (OrsegupsUtils.isWorkDay(dataPrazo)){
			prazo = (GregorianCalendar) dataPrazo.clone();
		}else{
			prazo = OrsegupsUtils.getNextWorkDay(dataPrazo);
			System.out.println("Não é dia util. Processando próxima data util:" + NeoDateUtils.safeDateFormat(prazo));
		}
				
				
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "2", "sim", prazo);
		processEntity.setValue("tarefaSimples",tarefa);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
