package com.neomind.fusion.workflow.adapter.ferias;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class AdapterValidaColaborador implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Boolean iniViaProAut = (Boolean) processEntity.getValue("iniViaProAut");
		if (processEntity.getValue("iniViaProAut") != null && !iniViaProAut)
		{
			NeoObject colaboradorFerias = (NeoObject) processEntity.findField("colaboradorFerias").getValue();
			EntityWrapper wColaborador = new EntityWrapper(colaboradorFerias);
			processEntity.setValue("numempFer", Long.parseLong(String.valueOf(wColaborador.getValue("numemp"))));
			processEntity.setValue("numcadFer", Long.parseLong(String.valueOf(wColaborador.getValue("numcad"))));
			processEntity.setValue("nomfunFer", String.valueOf(wColaborador.getValue("nomfun")));
			processEntity.setValue("codccuFer", String.valueOf(wColaborador.getValue("codccu")));
			processEntity.setValue("titcarFer", String.valueOf(wColaborador.getValue("titred")));

			Connection conn = PersistEngine.getConnection("VETORH");
			StringBuilder sql = new StringBuilder();
			PreparedStatement pstm = null;
			ResultSet rs = null;

			try
			{
				sql.append(" Select top 1 fun.numemp, fun.numcad, fun.nomfun, fun.numcpf, reg.USU_CodReg, reg.USU_NomReg, car.TitRed, ");
				sql.append(" fis.USU_NomFis,cpl.endrua,cpl.endcpl,cpl.endnum,bai.NomBai,cid.NomCid, orn.usu_codccu, orn.usu_lotorn From  R034FUN fun ");
				sql.append("Inner Join R038HLO hlo On hlo.NumEmp = fun.numemp And hlo.tipcol = fun.tipcol And hlo.numcad = fun.numcad And hlo.DatAlt = (Select Max(hlo2.datalt) From R038HLO hlo2 Where hlo2.numemp = hlo.numemp And hlo2.tipcol = hlo.tipcol And hlo2.numcad = hlo.numcad And hlo2.DatAlt <= getdate()) ");
				sql.append("Inner Join R016ORN orn On orn.taborg = hlo.TabOrg And orn.NumLoc = hlo.NumLoc  ");
				sql.append("Inner join USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg  ");
				sql.append("Inner Join R038HCA hca On hca.NumEmp = fun.numemp And hca.tipcol = fun.tipcol And hca.numcad = fun.numcad And hca.DatAlt = (Select Max(hca2.datalt) From R038HCA hca2 Where hca2.numemp = hca.numemp And hca2.tipcol = hca.tipcol And hca2.numcad = hca.numcad And hca2.DatAlt <= getdate()) ");
				sql.append("Inner Join R024CAR car On car.CodCar = fun.codcar And car.EstCar = fun.estcar  ");
				sql.append("Inner JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
				sql.append("Inner join r034cpl cpl on cpl.numcad = fun.numcad and cpl.numemp = fun.numemp and cpl.tipcol = fun.tipcol ");
				sql.append("Inner join R074BAI bai on bai.CodBai = cpl.codbai and bai.CodCid = cpl.codcid ");
				sql.append("Inner join R074CID cid on cid.CodCid = cpl.codcid ");
				sql.append("Where fun.numcad = ? and fun.numemp = ?");
				//			sql.append(" And not exists (select * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaFeriasProgramadas fp WITH (NOLOCK) ");
				//			sql.append("  			  	 where fp.numcpf = fun.numcpf and cast(floor(cast(fp.datalt as float)) as datetime) = cast(floor(cast(cob.usu_datalt as float)) as datetime) and fp.horini = cob.usu_horini) ");
				pstm = conn.prepareStatement(sql.toString());
				pstm.setLong(1, Long.parseLong(String.valueOf(processEntity.getValue("numcadFer"))));
				pstm.setLong(2, Long.parseLong(String.valueOf(processEntity.getValue("numempFer"))));
				rs = pstm.executeQuery();

				while (rs.next())
				{
					processEntity.setValue("lotacaoFer", rs.getString("usu_lotorn"));
					processEntity.setValue("codregFer", rs.getLong("USU_CodReg"));
					processEntity.setValue("nomregFer", rs.getString("USU_NomReg"));

					GregorianCalendar dataAtual = new GregorianCalendar();
					NeoFile anexo = null;

					if (rs.getLong("numemp") == 1 || rs.getLong("numemp") == 15 || rs.getLong("numemp") == 17 || rs.getLong("numemp") == 18 || rs.getLong("numemp") == 19 || rs.getLong("numemp") == 21 || rs.getLong("numemp") == 27 || rs.getLong("numemp") == 28 || rs.getLong("numemp") == 29)
					{
						anexo = OrsegupsUtils.criaNeoFile(new File("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Vigilancia_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf"));
					}
					else if (rs.getLong("numemp") == 2 || rs.getLong("numemp") == 6 || rs.getLong("numemp") == 7 || rs.getLong("numemp") == 8 || rs.getLong("numemp") == 22)
					{
						anexo = OrsegupsUtils.criaNeoFile(new File("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Asseio_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf"));
					}

					if (anexo != null)
					{
						processEntity.setValue("relatorioAvosDeFerias", anexo);
					}

					ColaboradorVO colaborador = new ColaboradorVO();
					colaborador.setEscala(new EscalaVO());
					colaborador.setEscala(QLPresencaUtils.getEscalaColaborador(rs.getLong("numcad"), rs.getLong("numemp"), 1L));
					processEntity.setValue("escalaFer", colaborador.getEscala().getDescricao() + "/ T: " + colaborador.getEscala().getCodigoTurma());
					processEntity.setValue("endFer", rs.getString("endrua") + ", Complemento: " + rs.getString("endcpl") + ", Nº: " + rs.getString("endnum"));
					processEntity.setValue("endFer", processEntity.findField("endFer").getValue() + " Bairro: " + rs.getString("NomBai") + " - Cidade: " + rs.getString("NomCid"));

					GregorianCalendar dataFim = (GregorianCalendar) processEntity.getValue("dataFimCob");
					String dataCor = NeoUtils.safeDateFormat(dataFim, "yyyy-MM-dd");
					HistoricoLocalVO local = new HistoricoLocalVO();
					local.setPosto(new PostoVO());
					local = QLPresencaUtils.listaHistoricoLocal(rs.getLong("numemp"), 1L, rs.getLong("numcad"), dataCor);
					List<EntradaAutorizadaVO> coberturas = new ArrayList<EntradaAutorizadaVO>();
					coberturas = QLPresencaUtils.listaCoberturasTarefasProgramacaoDeFerias(rs.getLong("numemp"), 1L, rs.getLong("numcad"));
					SimpleDateFormat sfinc = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					processEntity.setValue("ultimasCobFer", "");
					if (coberturas != null && coberturas.size() > 0)
					{
						processEntity.setValue("ultimasCobFer", processEntity.findField("ultimasCobFer").getValue() + " Ultimas Coberturas<br>");
						for (EntradaAutorizadaVO entradaAutorizadaVO : coberturas)
						{
							processEntity.setValue("ultimasCobFer", processEntity.findField("ultimasCobFer").getValue() + "Periodo: " + sfinc.format(entradaAutorizadaVO.getDataInicial().getTime()) + " à " + sfinc.format(entradaAutorizadaVO.getDataFinal().getTime()));
							processEntity.setValue("ultimasCobFer", processEntity.findField("ultimasCobFer").getValue() + " - Operação: " + entradaAutorizadaVO.getDescricaoCobertura());
							if (entradaAutorizadaVO.getColaboradorSubstituido().getNomeColaborador() != null)
							{
								processEntity.setValue("ultimasCobFer", processEntity.findField("ultimasCobFer").getValue() + " - Substituído: " + entradaAutorizadaVO.getColaboradorSubstituido().getNomeColaborador());
							}
							processEntity.setValue("ultimasCobFer", processEntity.findField("ultimasCobFer").getValue() + " - Local: " + entradaAutorizadaVO.getPosto().getNomePosto());
							processEntity.setValue("ultimasCobFer", processEntity.findField("ultimasCobFer").getValue() + " - Obs: " + entradaAutorizadaVO.getObservacao() + "<br>");

						}
					}

					GregorianCalendar datAtu = new GregorianCalendar();
					datAtu.add(GregorianCalendar.DATE, 3);
					datAtu.set(GregorianCalendar.HOUR_OF_DAY, 23);
					datAtu.set(GregorianCalendar.MINUTE, 59);
					datAtu.set(GregorianCalendar.SECOND, 59);
					processEntity.setValue("prazoTarefa", datAtu);

					//INICIO - Criar objeto do colaborador cobertura para o R030
					QLOpFilter qlNumCad = new QLOpFilter("numcad", "=", rs.getLong("numcad"));
					QLOpFilter qlNumEmp = new QLOpFilter("numemp", "=", rs.getLong("numemp"));
					QLGroupFilter groupFilterColaborador = new QLGroupFilter("AND");
					groupFilterColaborador.addFilter(qlNumCad);
					groupFilterColaborador.addFilter(qlNumEmp);
					List<NeoObject> objsColaborador = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), groupFilterColaborador);
					for (NeoObject objCol : objsColaborador)
					{
						processEntity.findField("colCobFer").setValue(objCol);
						break;
					}
					//FIM - Criar objeto do colaborador cobertura para o R030

					//INICIO - Definir as operações para Fluxo R030.
					QLOpFilter codigoFerias = new QLOpFilter("codigo", "=", "0003");
					QLOpFilter codigoTrocaEscala = new QLOpFilter("codigo", "=", "0008");
					QLOpFilter codigoTreinamento = new QLOpFilter("codigo", "=", "0007");
					QLGroupFilter groupFilter = new QLGroupFilter("OR");
					groupFilter.addFilter(codigoFerias);
					groupFilter.addFilter(codigoTreinamento);
					groupFilter.addFilter(codigoTrocaEscala);
					List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("QLOperacoes"), groupFilter);
					for (NeoObject objOp : objs)
					{
						EntityWrapper wOp = new EntityWrapper(objOp);
						String codOpe = String.valueOf(wOp.findField("codigo").getValue());
						if (codOpe.equals("0003"))
						{
							processEntity.findField("operacaoFerias").setValue(objOp);

						}
						else if (codOpe.equals("0008"))
						{
							processEntity.findField("operacaoTrocaEscala").setValue(objOp);
						}
						else if (codOpe.equals("0007"))
						{
							processEntity.findField("operacaoTreinamento").setValue(objOp);
						}
					}
					processEntity.setValue("iniViaProAut", false);

				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}

		}
		Long numReg = (Long) processEntity.getValue("codregFer");
		NeoPaper responsavel = null;
		if (numReg != null && numReg.equals(9L)){
		    responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelFluxoFeriasRegionalNoveCM"));
		}else{
		    responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "r001IniciarFluxo"));
		}
		
		processEntity.setValue("papelResponsavelRegional", responsavel);

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
