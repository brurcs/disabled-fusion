package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;

public class ValidarPrazoEmissaoRenovacaoCNV implements AdapterInterface {
    private static final Log log = LogFactory.getLog(ValidarPrazoEmissaoRenovacaoCNV.class);

    public void back(EntityWrapper processEntity, Activity activity) {
    }

    public void start(Task origin, EntityWrapper wrapper, Activity activity) {
	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

	String erro = "Por favor, contatar o administrador do sistema!";
	try {
	    InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("CNVRegistroAtividades");
	    NeoObject registro = registroAtividade.createNewInstance();
	    EntityWrapper wRegistro = new EntityWrapper(registro);

	    wRegistro.findField("responsavel").setValue(origin.getUser());
	    wRegistro.findField("dataInicial").setValue(origin.getStartDate());
	    wRegistro.findField("dataFim").setValue(origin.getFinishDate());

	    if (origin.getActivityName().equalsIgnoreCase("Emitir/Renovar CNV")) {
		wRegistro.findField("descricao").setValue(wrapper.findValue("obsPrazo"));
		wRegistro.findField("atividade").setValue("Solicitou ajuste");
		wRegistro.findField("prazo").setValue(wrapper.findValue("prazo"));
	    } else if (origin.getActivityName().equalsIgnoreCase("Validar Prazo Emissão/Renovação CNV")) {
		wRegistro.findField("descricao").setValue(wrapper.findValue("obsPrazo"));
		wRegistro.findField("atividade").setValue("Ajustou Tarefa");

		GregorianCalendar dtPrazo = (GregorianCalendar) wrapper.getValue("prazo");
		GregorianCalendar data = new GregorianCalendar();
		
		
		// Calcula prazo com 2 dias para próximo dia útil
		data.add(GregorianCalendar.DATE, 1);
		while (!OrsegupsUtils.isWorkDay(data)) {
		    data = OrsegupsUtils.getNextWorkDay(data);
		}		
		
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		System.out.println(formato.format(data.getTime()));	

		if ((dtPrazo != null) && (dtPrazo.before(data))) {
		    throw new WorkflowException("Data deve ser maior que o dia atual.");
		}

		wRegistro.findField("prazo").setValue(wrapper.findValue("prazo"));
	    }

	    PersistEngine.persist(registro);
	    wrapper.findField("registroAtividades").addValue(registro);
	    wrapper.setValue("obsPrazo", null);
	    System.out.println("  ");
	} catch (Exception e) {
	    log.error(erro);
	    e.printStackTrace();
	    throw new WorkflowException(erro + e.getMessage());
	}
    }
   
}