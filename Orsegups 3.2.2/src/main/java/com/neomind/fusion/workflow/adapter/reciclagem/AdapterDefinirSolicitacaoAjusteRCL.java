package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class AdapterDefinirSolicitacaoAjusteRCL implements AdapterInterface
{
    private static final Log log = LogFactory.getLog(AdapterDefinirSolicitacaoAjusteRCL.class);
    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity){
	Long key = (new GregorianCalendar()).getTimeInMillis();
	try{
	    processEntity.findField("jaSolicitouAjuste").setValue(true);
	}catch (Exception e){
	    log.error("["+key+"] Erro ao executar a classe AdapterDefinirSolicitacaoAjusteRCL "+e.getMessage());
	    e.printStackTrace();
	    throw new WorkflowException(e.getMessage());
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity){
	
    }

}
