package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CasvigHEXInsertHEX implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(CasvigHEXInsertHEX.class);

	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Connection conn = PersistEngine.getConnection("VETORH");
		PreparedStatement st = null;
		try
		{
			Collection<NeoObject> hex = processEntity.findField("importarHex").getValues();
			Long codEmp;
			for (NeoObject no : hex)
			{
				codEmp = (Long) processEntity.findField("empresa.codigo").getValue();
				Long codCal = (Long) processEntity.findField("calculo.codcal").getValue();
				GregorianCalendar datCal = (GregorianCalendar) processEntity.findField("calculo.inicmp").getValue();
				SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
				Boolean impHex = (Boolean) processEntity.findField("impHex").getValue();

				EntityWrapper wrapperLista = new EntityWrapper(no);
				Long tipCol = (Long) wrapperLista.findField("colaborador.tipcol").getValue();
				Long numCad = (Long) wrapperLista.findField("colaborador.numcad").getValue();
				Long tipImp = (Long) wrapperLista.findField("tipImp").getValue();
				String hex50 = (String) wrapperLista.findField("he50").getValue();
				String he100 = (String) wrapperLista.findField("he100").getValue();

				if ((Boolean.valueOf(impHex) == true) && (tipImp.longValue() == 1L))
				{
					int[] Array = (int[]) null;
					Array = new int[2];

					int valor = 0;

					int count = 0;
					StringTokenizer aux = new StringTokenizer(hex50, ":");
					while (aux.hasMoreTokens())
					{
						valor = Integer.parseInt(aux.nextToken());
						Array[count] = valor;
						++count;
					}
					int totHex50 = Array[0] * 60 + Array[1];

					count = 0;
					aux = new StringTokenizer(he100, ":");
					while (aux.hasMoreTokens())
					{
						valor = Integer.parseInt(aux.nextToken());
						Array[count] = valor;
						++count;
					}
					int tothe100 = Array[0] * 60 + Array[1];

					StringBuffer sql1 = new StringBuffer();
					sql1.append("SELECT RefEve FROM R044MOV WHERE ");
					sql1.append("NUMEMP = ? AND TIPCOL = ? AND NUMCAD = ? AND CODCAL = ? AND TABEVE = ? AND CODEVE = ?");

					st = conn.prepareStatement(sql1.toString());
					st.setLong(1, codEmp.longValue());
					st.setLong(2, tipCol.longValue());
					st.setLong(3, numCad.longValue());
					st.setLong(4, codCal.longValue());
					st.setInt(5, 941);
					st.setInt(6, 34);

					ResultSet rs = st.executeQuery();
					if (rs.next())
					{
						sql1 = new StringBuffer();
						sql1.append("UPDATE R044MOV SET RefEve = ? WHERE ");
						sql1.append("NUMEMP = ? AND TIPCOL = ? AND NUMCAD = ? AND CODCAL = ? AND TABEVE = ? AND CODEVE = ?");

						st = conn.prepareStatement(sql1.toString());
						st.setLong(1, totHex50);
						st.setLong(2, codEmp.longValue());
						st.setLong(3, tipCol.longValue());
						st.setLong(4, numCad.longValue());
						st.setLong(5, codCal.longValue());
						st.setInt(6, 941);
						st.setInt(7, 34);
						st.executeUpdate();
					}
					else if (totHex50 > 0)
					{
						StringBuffer sql11 = new StringBuffer();
						sql11.append("INSERT INTO R044MOV ");
						sql11.append("(NumEmp,TipCol,NumCad,CodCal,TabEve,CodEve,CodRat,SeqEve,OriMov,RefEve,ValEve) ");
						sql11.append("VALUES (?,?,?,?,?,?,?,?,?,?,?)");

						st = conn.prepareStatement(sql11.toString());
						st.setLong(1, codEmp.longValue());
						st.setLong(2, tipCol.longValue());
						st.setLong(3, numCad.longValue());
						st.setLong(4, codCal.longValue());
						st.setInt(5, 941);
						st.setInt(6, 34);
						st.setInt(7, 0);
						st.setInt(8, 1);
						st.setString(9, "I");
						st.setInt(10, totHex50);
						st.setInt(11, 0);
						st.executeUpdate();
					}

					StringBuffer sql11 = new StringBuffer();
					sql11.append("SELECT RefEve FROM R044MOV WHERE ");
					sql11.append("NUMEMP = ? AND TIPCOL = ? AND NUMCAD = ? AND CODCAL = ? AND TABEVE = ? AND CODEVE = ?");

					st = conn.prepareStatement(sql11.toString());
					st.setLong(1, codEmp.longValue());
					st.setLong(2, tipCol.longValue());
					st.setLong(3, numCad.longValue());
					st.setLong(4, codCal.longValue());
					st.setInt(5, 941);
					st.setInt(6, 36);

					ResultSet rs1 = st.executeQuery();
					if (rs1.next())
					{
						sql11 = new StringBuffer();
						sql11.append("UPDATE R044MOV SET RefEve = ? WHERE ");
						sql11.append("NUMEMP = ? AND TIPCOL = ? AND NUMCAD = ? AND CODCAL = ? AND TABEVE = ? AND CODEVE = ?");

						st = conn.prepareStatement(sql11.toString());
						st.setLong(1, tothe100);
						st.setLong(2, codEmp.longValue());
						st.setLong(3, tipCol.longValue());
						st.setLong(4, numCad.longValue());
						st.setLong(5, codCal.longValue());
						st.setInt(6, 941);
						st.setInt(7, 36);
						st.executeUpdate();
					}
					else if (tothe100 > 0)
					{
						sql11 = new StringBuffer();
						sql11.append("INSERT INTO R044MOV ");
						sql11.append("(NumEmp,TipCol,NumCad,CodCal,TabEve,CodEve,CodRat,SeqEve,OriMov,RefEve,ValEve) ");
						sql11.append("VALUES (?,?,?,?,?,?,?,?,?,?,?)");

						st = conn.prepareStatement(sql11.toString());
						st.setLong(1, codEmp.longValue());
						st.setLong(2, tipCol.longValue());
						st.setLong(3, numCad.longValue());
						st.setLong(4, codCal.longValue());
						st.setInt(5, 941);
						st.setInt(6, 36);
						st.setInt(7, 0);
						st.setInt(8, 1);
						st.setString(9, "I");
						st.setInt(10, tothe100);
						st.setInt(11, 0);
						st.executeUpdate();
					}

				}

			}

			Collection<NeoObject> va = processEntity.findField("listaRegistroC").getValues();
			for (NeoObject no : va)
			{
				codEmp = (Long) processEntity.findField("empresa.codigo").getValue();
				Long codCal = (Long) processEntity.findField("calculo.codcal").getValue();
				GregorianCalendar datCal = (GregorianCalendar) processEntity.findField("calculo.inicmp").getValue();
				SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
				Boolean impVa = (Boolean) processEntity.findField("impVa").getValue();

				EntityWrapper wrapperLista = new EntityWrapper(no);
				Long tipCol = (Long) wrapperLista.findField("colaborador.tipcol").getValue();
				Long numCad = (Long) wrapperLista.findField("colaborador.numcad").getValue();
				Long tipImp = (Long) wrapperLista.findField("tipImp").getValue();
				Long vaExt = (Long) wrapperLista.findField("vaExtra").getValue();

				if ((Boolean.valueOf(impVa) == true) && (tipImp.longValue() == 2L))
				{
					StringBuffer sql1 = new StringBuffer();
					sql1.append("SELECT USU_VlrEnt,USU_QtdEnt,USU_QtdExt FROM USU_T999CMVA WHERE ");
					sql1.append("USU_NUMEMP = ? AND USU_TIPCOL = ? AND USU_NUMCAD = ? AND USU_MESANO = ? AND USU_CODGER = 1");

					st = conn.prepareStatement(sql1.toString());
					st.setLong(1, codEmp.longValue());
					st.setLong(2, tipCol.longValue());
					st.setLong(3, numCad.longValue());
					st.setString(4, date.format(datCal.getTime()));

					ResultSet rs = st.executeQuery();
					if (rs.next())
					{
						Float vlrExt = 0F;
						Float vlrEnt = 0F;
						int qtdEnt = 0;

						vlrEnt = rs.getFloat("USU_VlrEnt");
						qtdEnt = rs.getInt("USU_QtdEnt");

						vlrExt = ((vlrEnt / qtdEnt) * vaExt);

						sql1 = new StringBuffer();
						sql1.append("UPDATE USU_T999CMVA SET USU_QtdExt=?,USU_VlrExt=? WHERE ");
						sql1.append("USU_NUMEMP = ? AND USU_TIPCOL = ? AND USU_NUMCAD = ? AND USU_MESANO = ? AND USU_CODGER = 1 AND USU_SEQVA = 1");

						st = conn.prepareStatement(sql1.toString());
						st.setLong(1, vaExt.longValue());
						st.setFloat(2, vlrExt.floatValue());
						st.setLong(3, codEmp.longValue());
						st.setLong(4, tipCol.longValue());
						st.setLong(5, numCad.longValue());
						st.setString(6, date.format(datCal.getTime()));
						st.executeUpdate();
					}
					else
					{
						log.error("Não foi possível importar o VA, cálculo ainda não foi executado.");
						throw new WorkflowException("Não foi possível importar o VA, cálculo ainda não foi executado.");
					}
				}
			}
		}
		catch (WorkflowException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			log.error(e.getMessage());
			e.printStackTrace();
			throw new WorkflowException("Não foi possível importar os dados, Entre em contato com a TI.");
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
}