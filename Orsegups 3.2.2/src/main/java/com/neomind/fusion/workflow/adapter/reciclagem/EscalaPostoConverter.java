package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoEscalaPostoVO;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.util.NeoUtils;
//com.neomind.fusion.workflow.adapter.reciclagem.EscalaPostoConverter
public class EscalaPostoConverter extends DefaultConverter {

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin){
		
		String retorno = "";
				
		try{
			String strTaborg =  (String) field.getForm().getField("taborg").getValue();
			String strNumloc = (String) field.getForm().getField("numloc").getValue();
			
			Long numloc = Long.parseLong(strNumloc);
			Long taborg = Long.parseLong(strTaborg);
				
			EscalaPostoVO escalaPosto = QLPresencaUtils.getEscalaPosto(numloc, taborg, new GregorianCalendar());
			
			List<HistoricoEscalaPostoVO> listaHistoricoEscalaPosto = QLPresencaUtils.listaHistoricoEscalaPosto(String.valueOf(numloc), String.valueOf(taborg));
			
			String textoLog = "";
			
			if (escalaPosto != null) {
				textoLog = "";//getLogEscalaPosto(escalaPosto);
			} else {
				textoLog = "Posto sem escala cadastrada";
			}
			
			textoLog +=  escalaPosto;
			retorno = textoLog;
			
			return retorno;
		}catch(Exception e){
			System.out.println("Erro ao converter escala do posto na tarefa de reciclagem");
			e.printStackTrace();
			return retorno;
		}
	}	

	public Object getValueFromHTMLString(EFormField field, OriginEnum origin){
		return getHTMLInput(field, origin);
	}

	
	
	
	
	
	
	private static String getLogEscalaPosto(EscalaPostoVO escalaPosto)
	{
		Long diaSem = escalaPosto.getDiaSemana();
		String resultLog = "";
		String permanenciaPosto = "";
		
		if (escalaPosto.getPermanenciaPosto() != null)
		{
			switch (escalaPosto.getPermanenciaPosto().intValue())
			{
				case 0:
					permanenciaPosto = "Nenhum";
					break;
				case 10:
					permanenciaPosto = "Inicio";
					break;
				case 20:
					permanenciaPosto = "Fim";
					break;
				case 30:
					permanenciaPosto = "Intervalo";
					break;
				case 40:
					permanenciaPosto = "Periodo Integral";
					break;
				case 50:
					permanenciaPosto = "Inicio e Intervalo";
					break;
				case 60:
					permanenciaPosto = "Inicio e Fim";
					break;
				case 70:
					permanenciaPosto = "Intervalo e Fim";
					break;
				case 80:
					permanenciaPosto = "Nenhum";
					break;
			}
		}
		
		if (!permanenciaPosto.equals(""))
		{
			resultLog += escalaPosto.getHorariosHoje() + "<b>Ajuste de Escala: </b>" + permanenciaPosto + "</b><br/>";
		}
		
		
		resultLog += escalaPosto;
		/*escalaPosto.setDiaSemana(1L);
		resultLog += "<br/><b>Segunda-feira - </b>" + escalaPosto;
		escalaPosto.setDiaSemana(2L);
		resultLog += "<br/><b>Terça-feira - </b>" + escalaPosto;
		escalaPosto.setDiaSemana(3L);
		resultLog += "<br/><b>Quarta-feira - </b>" + escalaPosto;	
		escalaPosto.setDiaSemana(4L);
		resultLog += "<br/><b>Quinta-feira - </b>" + escalaPosto;
		escalaPosto.setDiaSemana(5L);
		resultLog += "<br/><b>Sexta-feira - </b>" + escalaPosto;
		escalaPosto.setDiaSemana(6L);
		resultLog += "<br/><b>Sabado - </b>" + escalaPosto;
		escalaPosto.setDiaSemana(7L);
		resultLog += "<br/><b>Domingo - </b>" + escalaPosto;
		escalaPosto.setDiaSemana(8L);
		resultLog += "<br/><b>Feriado - </b>" + escalaPosto;	
		escalaPosto.setDiaSemana(diaSem);*/
		
		return resultLog.replaceAll("\"", "'");
	}


	private static String getDiaSemana(Long dia){
		String retorno = "";
		switch(dia.intValue()){
			case	1:
				retorno = "<b>Segunda-feira - </b>";
				break;
			case	2:
				retorno = "<b>Terça-feira - </b>";
				break;
			case	3:
				retorno = "<b>Quarta-feira - </b>";
				break;
			case	4:
				retorno = "<b>Quinta-feira - </b>";
				break;
			case	5:
				retorno = "<b>Sexta-feira - </b>";
				break;
			case	6:
				retorno = "<b>Sábado-feira - </b>";
				break;
			case	7:
				retorno = "<b>Domingo-feira - </b>";
				break;
			case	8:
				retorno = "<b>Feriado - </b>";
				break;
		}
		return retorno;
	}
	
	private static String getLogHistoricoEscalaPosto(List<HistoricoEscalaPostoVO> escalaPosto)
	{
		String resultLog = "";
		
		String horariosSemana[] = {"","","","","","","",""};
		
		int firstPass = 0;
		String lastDatAlt = "";
		if (escalaPosto != null && !escalaPosto.isEmpty()){
			resultLog += "<br><br> <b>Historico de Escalas</b> <br>";
			
			for (HistoricoEscalaPostoVO historicoEscala: escalaPosto){
				
				Long diaSem = NeoUtils.safeLong(historicoEscala.getUsuDiasem());
				
				if (lastDatAlt.equals("")){
					lastDatAlt = historicoEscala.getUsuDatalt();
				}
				
				if (!historicoEscala.getUsuDatalt().equals(lastDatAlt)){
					resultLog += "<br><br> <b>Alterado em: " + lastDatAlt + "</b>";
					for (String h: horariosSemana){
						resultLog += h;
					}
					horariosSemana = new String[]{"","","","","","","",""};
					lastDatAlt = historicoEscala.getUsuDatalt();
				}
				
				
				if (horariosSemana[diaSem.intValue()-1].length() == 0){
					horariosSemana[diaSem.intValue()-1] += "<br>" + getDiaSemana(diaSem) + historicoEscala;
				}else{
					if (firstPass == 1){
						horariosSemana[diaSem.intValue()-1] += " / " +  historicoEscala;
					}	
				}
				
				if (firstPass == 0) firstPass++;
				
			}
			
			resultLog += "<br><br> <b>Alterado em: " + lastDatAlt + "</b>";
			for (String h: horariosSemana){
				resultLog += h;
			}
		
		}
		
		return resultLog.replaceAll("\"", "'");
	} 
}
