package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CasvigAtt implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String codReg = processEntity.findField("contratoSapiens3.usu_codreg").getValue().toString();

		if (NeoUtils.safeIsNull(codReg))
		{
			throw new WorkflowException("Não foi possível determinar a regional do contrato");
		}
		QLFilter filter = new QLEqualsFilter("codigo", Long.valueOf(codReg));

		NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("FCCresponsabilidade"), filter);

		if (NeoUtils.safeIsNull(obj))
		{
			throw new WorkflowException("Não foi possível determinar a regional do contrato (Objeto não localizado no eform FCCresponsabilidade). ");
		}
		EntityWrapper wrapperObj = new EntityWrapper(obj);

		NeoPaper gerente = (NeoPaper) wrapperObj.findValue("respGerente");
		NeoPaper coodernador = (NeoPaper) wrapperObj.findValue("respCoordenador");
		NeoPaper analista = (NeoPaper) wrapperObj.findValue("respAnalista");
		NeoPaper comercial = (NeoPaper) wrapperObj.findValue("respComercial");
		NeoPaper superintendencia = (NeoPaper) wrapperObj.findValue("respSuperintendencia");
		NeoPaper diretor = (NeoPaper) wrapperObj.findValue("respDiretor");
		NeoPaper presidencia = (NeoPaper) wrapperObj.findValue("respPresidencia");

		processEntity.setValue("gerenteRegional", gerente);
		processEntity.setValue("coordenadorRegional", coodernador);
		processEntity.setValue("analistaRegional", analista);
		processEntity.setValue("gerenteComercial", comercial);
		processEntity.setValue("superintendencia", superintendencia);
		processEntity.setValue("diretor", diretor);
		processEntity.setValue("presidente", presidencia);
	}
}