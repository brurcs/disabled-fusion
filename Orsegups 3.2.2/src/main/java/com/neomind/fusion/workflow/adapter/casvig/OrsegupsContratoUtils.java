package com.neomind.fusion.workflow.adapter.casvig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.contract.ContratoLogUtils;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.NeoDataSource;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/**
 * MÉTODOS ÚTEIS PARA O FLUXO DE CONTRATOS
 *
 */
public class OrsegupsContratoUtils
{
	private static final Log log = LogFactory.getLog(OrsegupsContratoUtils.class);
	
	/**
	 * Chamado pelo getResultSet
	 * @param consulta
	 * @param con
	 * @return
	 */
	public static ResultSet selectTable(String consulta, Connection con)
	{
		try
		{
			Statement stm = (Statement) con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery(consulta);
			if (rs.first())
			{
				return rs;
			}
		}
		catch (SQLException e)
		{
			System.out.println("ERRO selectTable ->"+consulta);
			log.error("Select table error", e);
		}
		return null;
	}
	
	/**
	 * Informe a query e o nome da fonte de dados
	 * @param query
	 * @param dataSourceName
	 * @return resultado da query
	 */
	public static ResultSet getResultSet(String query, String dataSourceName)
	{
		NeoDataSource data = (NeoDataSource) PersistEngine.getObject(NeoDataSource.class,
				new QLEqualsFilter("name", dataSourceName));

		if (data != null)
		{
			Connection con = OrsegupsUtils.openConnection(data);
			return selectTable(query, con);
		}
		return null;
	}
	
	
	public static Long getNextChave(String nomeFonteDados, String query)
	{
		Long retorno = 0L;
		ResultSet rs = OrsegupsContratoUtils.getResultSet(query, nomeFonteDados);
		try{
			if(rs!= null){
				retorno = rs.getLong(1);
			}
			rs.close();
			rs = null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return retorno;
	}
	
	/**
	 * Faz inserção evitando problema de "Violation of PRIMARY KEY constraint", 
	 * <br>se tentar inserir um numero que já existe, tenta denovo com outro numero até o numero maximo de tentativas.
	 * <br>
	 * @param nomeFonteDados - Nome da fonte de dados do fusion	
	 * @param sqlNextKey - Sql para consultar proxima chave. Ex.: select max(usu_numCtr)+1 from usu_t160ctr
	 * @param sqlToInsert - Sql de insert a ser executado. Obs.: a chave deve ser passada como :chave 
	 * @param maxTrys - Máximo de tentativas, assim evita de ficar em looping infinito
	 * @param intervalTry - Intervalo de tempo entre as tentativas (em milisegundos).
	 * @return o codigo da cave inserida ou 0, caso não tenha inserido.
	 * @throws Exception 
	 */
	public static int safeInsert(String nomeFonteDados, String sqlNextKey, String sqlToInsert, int maxTrys, int intervalTry) throws Exception
	{
		int retorno = 0;
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		String retInsercao = "";
		int cont = 0;
		do{
			try {
				ResultSet rs = OrsegupsContratoUtils.getResultSet(sqlNextKey, nomeFonteDados);
				if(rs!= null){
					//"SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203"
					long chave = rs.getInt(1);
					
					String query = sqlToInsert;
					if(sqlToInsert.contains(":chave"))
						query = sqlToInsert.replaceAll(":chave", NeoUtils.safeOutputString(chave));
					
					System.out.println("[FLUXO CONTRATOS]- safeInsert-> proxima chave: "+rs.getInt(1) + " ");
					PreparedStatement pst = connection.prepareStatement(query);
					//pst.setQueryTimeout(20);
					
					
					if (pst.executeUpdate() > 0){
						retorno = rs.getInt(1);
						//System.out.println("[FLUXO CONTRATOS]- safeInsert -> "+chave + ", Inserido com sucesso");
					}else{
						retorno =0;
						System.err.println("[FLUXO CONTRATOS]- safeInsert-> Registro não inserido");
					}
				}
				rs.close();
				retInsercao = "";
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("[FLUXO CONTRATOS]- safeInsert->"+e.getMessage());
				if (e.getMessage().contains("Violation of PRIMARY KEY constraint")){
					retInsercao = "tentardenovo";
				}
				if (e.getMessage().contains("java.sql.DataTruncation")){
					System.out.println("[FLUXO CONTRATOS]- Erro - Coluna execedeu o tamanho, SQL=> " + sqlToInsert.toString());
					throw new Exception("Coluna execedeu o tamanho, SQL=> " + sqlToInsert.toString().substring(0, (sqlToInsert.toString().length() > 25 ? 25 : sqlToInsert.toString().length() )  ));
				} 
				//e.printStackTrace();
			}
			cont++;
			try {
				if(retInsercao.equals("tentardenovo")){
					Thread.sleep(intervalTry);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}while(retInsercao.equals("tentardenovo") && cont < maxTrys);
		
		if (connection!= null){
			try {
				connection.close();
			} catch (SQLException e) {
				connection = null;
			}
		}
		
		return retorno;
	}
	
	
	/**
	 * Faz inserção passando uma chave, 
	 * <br>
	 * @param nomeFonteDados - Nome da fonte de dados do fusion	
	 * @param chave - chave gerada externamento para inserção.
	 * @param sqlToInsert - Sql de insert a ser executado. Obs.: a chave deve ser passada como :chave 
	 * @return O código da cave inserida ou 0, caso não tenha inserido.
	 * @throws Exception 
	 */
	public static int safeInsert(String nomeFonteDados, Long chave, String sqlToInsert) throws Exception
	{
		int retorno = 0;
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		try {
				String query = sqlToInsert;
				if(sqlToInsert.contains(":chave"))
					query = sqlToInsert.replaceAll(":chave", NeoUtils.safeOutputString(chave));
				
				PreparedStatement pst = connection.prepareStatement(query);
				
				if (pst.executeUpdate() > 0){
					retorno = chave.intValue();
					//System.out.println("[FLUXO CONTRATOS]- safeInsert -> "+chave + ", Inserido com sucesso");
				}else{
					retorno =0;
					ContratoLogUtils.logInfo("safeInsert -> "+chave+", Registro não inserido");
				}
		} catch (SQLException e) {
			ContratoLogUtils.logInfo("Erro ao persistir informações de Centro de Custo na base do Sapiens. Se o problema persistir, comunique a TI. " + e.getMessage()+ " -- " + chave + " -- " + sqlToInsert);
			e.printStackTrace();
			throw new Exception("Erro ao persistir informações de Centro de Custo na base do Sapiens. Se o problema persistir, comunique a TI.");
		}
		
		if (connection!= null){
			try {
				connection.close();
			} catch (SQLException e) {
				connection = null;
			}
		}
		
		return retorno;
	}
	
	
	/**
	 * Verifica se já existe uma classificação igual no banco com o fim de mitigar duplicação de classificação de centro de custo. 
	 * @param nomeFonteDados
	 * @param aclaCta
	 * @return
	 */
	public static boolean existeClassificacaoPlanoCentroCusto(String nomeFonteDados,String aclaCta)
	{
		boolean retorno = false;
		try {
			System.out.println("[FLUXO CONTRATOS]- " + "select * from e043pcm where clacta= '"+aclaCta+"'  and codmpc=203 ");
			ResultSet rs = OrsegupsContratoUtils.getResultSet("select * from e043pcm where clacta= '"+aclaCta+"'  and codmpc=203 " , nomeFonteDados);
			ResultSet rs2 = OrsegupsContratoUtils.getResultSet("select * from e044ccu where claccu= '"+aclaCta+"'" , nomeFonteDados);
			if(rs!= null && rs2!= null){
				retorno = true;
			}else if(rs2 == null && rs != null){
				ContratoUtils.deleteCentroCusto(aclaCta);
				retorno = false;
			}
		} catch (Exception e) {
			System.out.println("[FLUXO CONTRATOS]- existeClassificacaoPlanoCentroCusto('"+aclaCta+"') -> " +e.getMessage());
			e.printStackTrace();
			return retorno;
		}
			return retorno;
	}
	
	
	/**
	 * Método para buscar centro de custo a partir da classificação de nivel 6. Faço isso pois quando ocorre erro no fusion na inserção da conta sigma, preciso reutilizar a arvore de centro de custo criada.
	 * @param claCta6 - classificação de 6º nivel da conta.
	 * @param nivelConta - nivel da conta, pode ser : 6,7 ou 8
	 * @return ctaRed do respectivel nivel
	 */
	public static String getCentroCusto(String claCta6, int nivelConta){
		String retorno = null;
		String  query = "select ctared from e043pcm where clacta like '"+claCta6+"%' and nivCta = "+nivelConta;
		ResultSet rs = getResultSet(query, "SAPIENS");
		if (rs != null){
			try {
				retorno = rs.getString(1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return retorno;
		
	}

	/**
	 * Verifica pelo cep se o endereço de cobranca do cliente já existe na tabela e085cob
	 * @param codCli
	 * @param cepCli
	 * @return
	 */
	public static boolean existeEnderecoCobranca(String codCli, String cepCli){
		boolean retorno = true;
		String  query = "select SeqCob from e085cob where codcli = "+codCli+" and cepCob = "+ cepCli;
		ResultSet rs = getResultSet(query, "SAPIENS");
		if (rs != null){
			retorno = true;
		}else{
			retorno = false;
		}
		return retorno;
		
	}
	
	
	/**
	 * Verifica se já existe uma classificação igual no banco com o fim de mitigar duplicação de classificação de centro de custo. 
	 * @param nomeFonteDados
	 * @param aclaCta
	 * @return
	 */
	public static boolean existeEnderecoCobranca(String seqCob)
	{
		boolean retorno = false;
		Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
		int cont = 0;
		try {
			ResultSet rs = OrsegupsContratoUtils.getResultSet("select * from e085cob where codcli =  '"+seqCob+"' " , "SAPIENS");
			if(rs!= null){
				retorno = true;
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("[FLUXO CONTRATOS]- existeEnderecoCobranca('"+seqCob+"') -> " +e.getMessage());
			//e.printStackTrace();
		}finally{
			return retorno;
		}
	}
	
	
	public static String exiteInstalacaoCompetenciaFutura(String numctr, String numPos){
		String retorno = "";
		if (numctr == null || "".equals(numctr)){
			numctr = "0";
		}
		
		if (numPos == null || "".equals(numPos)){
			numPos = "0";
		}
		
		String sql =" select RIGHT( CONVERT(VARCHAR(10), cm.usu_datcpt , 103), 7) from usu_t160cms cm "+
				" join usu_t160cvs cv on (cm.usu_numctr = cv.usu_numctr and cm.usu_numpos = cv.usu_numpos and cv.usu_sitcvs = 'A') "+
				//" where cm.usu_adcsub = '+' "+ 
				" and cm.usu_numctr = "+ numctr + " and cm.usu_numpos = " + numPos +  
				" and cast (left(CONVERT(varchar(12), cm.usu_datcpt, 12),4)  as numeric  ) > cast (left(CONVERT(varchar(12), getDate(), 12),4)  as numeric  ) ";
				//" and (upper(cm.usu_cplcvs) like '%INSTALACAO%' "+ 
				//" OR upper(cm.usu_cplcvs) like '%INSTALAÇAO%' "+
				//" OR upper(cm.usu_cplcvs) like '%INSTALAÇÃO%') ";
		
		try{
			Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
			ResultSet rs = OrsegupsContratoUtils.getResultSet(sql , "SAPIENS");
			if(rs!= null){
				do{
					retorno += rs.getString(1)+",";
				}while(rs.next());
				
			}
			if (retorno.indexOf(",") != -1) retorno = retorno.substring(0,retorno.length()-1);
			return retorno;
		}catch(Exception e){
			ContratoLogUtils.logInfo("Erro exiteInstalacaoCompetenciaFutura ->" +sql);
			e.printStackTrace();
			return retorno;
		}
	}
	
	public static boolean atualizaObsDoContrato(String usu_cidctr, Long usu_codemp, Long usu_codfil, Long numContrato) throws Exception{

	String nomeFonteDados = "SAPIENS";
	
	Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

	// valores utilizados na condicao where
	Long empresa = usu_codemp;
	Long filial = usu_codfil;
	Long contrato = numContrato;
	String usu_obsctrAtu = "Serviços Prestados em suas instalações em "+usu_cidctr;
	StringBuffer sql = new StringBuffer();
	sql.append("UPDATE USU_T160CTR ");
	sql.append("SET USU_OBSCTR = ? ");
	sql.append("WHERE USU_CODEMP = ? ");
	sql.append("AND USU_CODFIL = ? ");
	sql.append("AND USU_NUMCTR = ? ");
	PreparedStatement st = null;
	try
	{
		st = connection.prepareStatement(sql.toString());
		st.setString(1, usu_obsctrAtu);
		st.setLong(2, empresa);
		st.setLong(3, filial);
		st.setLong(4, contrato);

		st.executeUpdate();
		return true;
	}
	catch (SQLException e)
	{
		e.printStackTrace();
		long chave = GregorianCalendar.getInstance().getTimeInMillis();
		ContratoLogUtils.logInfo("["+chave+"] Erro ao Atualizar Obs do contrato: "+sql+" - "+ e.getMessage());
		throw new Exception("["+chave+"] Erro ao Atualizar Obs do contrato: "+sql+" - "+e.getMessage());
	}
	finally{
		OrsegupsUtils.closeConnection(connection, st, null);
	}
  }
}

