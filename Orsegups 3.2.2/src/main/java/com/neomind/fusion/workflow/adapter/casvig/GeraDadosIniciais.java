package com.neomind.fusion.workflow.adapter.casvig;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.ContratoLogUtils;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.contract.vo.DadosDebitoSapiensVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class GeraDadosIniciais implements AdapterInterface
{

	private static Long ALTERACAO_TERMO_ADITIVO = (long) 5;
	private static Long CONTRATO_NOVO_CLIENTE_NOVO = (long) 3;
	private static Long CONTRATO_NOVO_CLIENTE_NOVO_HUMANA = (long) 4;
	private static final Log log = LogFactory.getLog(GeraDadosIniciais.class);

	@SuppressWarnings("unchecked")
	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{

		NeoUser usuarioGestorVendas = getSuperior(PortalUtil.getCurrentUser());
		if (usuarioGestorVendas != null)
		{
			wrapper.setValue("usuarioGestorVendas", usuarioGestorVendas);
		}
		else
		{
			throw new WorkflowException("Não foi possível encontrar o usuário gestor para o usuário: " + PortalUtil.getCurrentUser());
		}

		NeoPaper papelAnalista = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "FGCGestorAnalise"));
		wrapper.setValue("papelAnalista", papelAnalista);

		Long codEmpresa = null;
		Long movimentoContrato = null;
		Long movimentoAlteracaoContrato = null;
		String modeloContrato = null;
		try
		{
			codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
			movimentoContrato = (Long) wrapper.findField("movimentoContratoNovo.codTipo").getValue();
			movimentoAlteracaoContrato = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");
			modeloContrato = (String) wrapper.findValue("modelo.codigo");

			// flag utilizada na regra de interface para deixar somente
			// valorMOntanteB para contratos de eletronica.
			if (movimentoAlteracaoContrato == null)
			{
				wrapper.setValue("esconderPrecos", false);
			}
			else
			{
				if (movimentoContrato == 5 && (movimentoAlteracaoContrato == 1 || movimentoAlteracaoContrato == 2 || movimentoAlteracaoContrato == 3 || movimentoAlteracaoContrato == 4 || movimentoAlteracaoContrato == 6 || movimentoAlteracaoContrato == 9 || movimentoAlteracaoContrato == 10))
				{
					wrapper.setValue("esconderPrecos", true);
				}
				else
				{
					wrapper.setValue("esconderPrecos", false);
				}
			}

			if (movimentoContrato == 5 || movimentoContrato == 1 || movimentoContrato == 2)
			{
				if (wrapper.findValue("buscaNomeCliente") == null)
				{
					throw new WorkflowException("Cliente não selecionado, favor preencher.");
				}
			}

			if (movimentoContrato.equals(ALTERACAO_TERMO_ADITIVO))
			{
				Long tipoAlteracao = (Long) wrapper.findField("movimentoAlteracaoContrato.codTipo").getValue();
				Long cliente = (Long) wrapper.findField("buscaNomeCliente.codcli").getValue();
				Long numContrato = (Long) wrapper.findField("numContrato.usu_numctr").getValue();
				Long periodoVigencia = (Long) wrapper.findField("numContrato.usu_pervig").getValue();

				geraDadosIniciais(codEmpresa, cliente, numContrato, wrapper, periodoVigencia);

				// limpa o eform pai
				wrapper.findField("postosContrato").setValue(null);
				PersistEngine.getEntityManager().flush();

				// se for servico extra nao deve popular o posto do sapiens pois
				// será gerado um novo contrato e novos postos
				if (tipoAlteracao != 7 || tipoAlteracao != 8)
				{
					List<NeoObject> postos = geraPostos(codEmpresa, cliente, numContrato, wrapper);

					if (movimentoContrato != null && movimentoContrato == 5)
					{
						for (NeoObject oPosto : postos)
						{
							EntityWrapper wPosto = new EntityWrapper(oPosto);

							GregorianCalendar dtIniPosNov = (GregorianCalendar) wPosto.findValue("periodoFaturamento.de");
							Long numpos = NeoUtils.safeLong(NeoUtils.safeOutputString(wPosto.findValue("numPosto")));

							/*
							 * Verifica se há mão de obra de instalação para
							 * compentecias futuras no termo aditivo
							 */
							if (numContrato != null && numContrato > 0L)
							{
								String competenciasFuturas = OrsegupsContratoUtils.exiteInstalacaoCompetenciaFutura(NeoUtils.safeOutputString(numContrato), NeoUtils.safeOutputString(numpos));
								if (competenciasFuturas.length() > 0)
								{
									ContratoLogUtils.logInfo("Este contrato possui um posto com lançamento de mão de obra de instalação para competências futuras(" + competenciasFuturas + ").");
									wPosto.setValue("BonificacoesCptFutura", "Este posto possui um posto com lançamentos para competências futuras(" + competenciasFuturas + "). Os mesmos serão transferidos ao posto novo ao final da etapa de validação.");
								}
							}
						}

					}

					//Seta no eform pai o posto
					wrapper.findField("postosContrato").setValue(postos);
				}

				if (movimentoContrato == 1 || movimentoContrato == 2 || movimentoContrato == 5)
				{
					NeoObject dadosGeraisdoCliente = AdapterUtils.createNewEntityInstance("FGCClientesCadastroSapiens");
					dadosGeraisdoCliente = buscaDadosGeraisCliente(wrapper, dadosGeraisdoCliente);

					/*
					 * String cpfCnpj = wrapper.findGenericValue("cgccpfConsulta");
					 * if (cpfCnpj != null)
					 * {
					 * String tipo = "";
					 * if (cpfCnpj.length() == 11)
					 * tipo = "F";
					 * else if (cpfCnpj.length() == 14)
					 * tipo = "J";
					 * if (!tipo.equals(""))
					 * {
					 * NeoObject noTipoCliente =
					 * PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FGCClientesTipo"), new
					 * QLEqualsFilter("tipo", tipo));
					 * NeoObject noTipoClienteConsistencia =
					 * PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUT402"), new
					 * QLEqualsFilter("usu_tipcli", tipo.equals("F") ? 1l : 2l));
					 * EntityWrapper wNovoCliente = new EntityWrapper(dadosGeraisdoCliente);
					 * wNovoCliente.setValue("tipoClienteConsistencia", noTipoClienteConsistencia);
					 * wNovoCliente.setValue("tipocliente", noTipoCliente);
					 * }
					 * }
					 */

					PersistEngine.persist(dadosGeraisdoCliente);

					wrapper.setValue("novoCliente", dadosGeraisdoCliente);

					NeoObject clienteBackup = EntityCloner.cloneNeoObject(dadosGeraisdoCliente);
					PersistEngine.persist(clienteBackup);
					wrapper.setValue("novoClienteBackup", clienteBackup);
				}

				ContratoLogUtils.logInfo("Procurando pelo Agrupador-> AGR" + numContrato);
				/*
				 * NeoObject oAgrupador = ContratoUtils.getFirstNeoObjectFromList(
				 * PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUT160AGR"),
				 * new QLGroupFilter("AND",new
				 * QLRawFilter("usu_ctragr ='"+"AGR"+numContrato+"'"))
				 * //QLEqualsFilter("usu_ctragr","AGR"+numContrato ))
				 * );
				 */

				Collection<NeoObject> oListaAgrupador = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUT160AGR"), new QLGroupFilter("AND", new QLRawFilter("usu_ctragr ='" + "AGR" + numContrato + "'")));

				if (oListaAgrupador != null && oListaAgrupador.size() > 0)
				{
					NeoObject oAgrupador = null;
					for (NeoObject agr : oListaAgrupador)
					{
						oAgrupador = agr;
					}
					if (oAgrupador != null)
					{
						EntityWrapper wAgrupador = new EntityWrapper(oAgrupador);
						ContratoLogUtils.logInfo("Agrupador encontrado: " + wAgrupador.findValue("usu_ctragr"));
						NeoObject oDadosGeraisContrato = (NeoObject) wrapper.findValue("dadosGeraisContrato");
						//NeoObject dadosGeraisdoProcesso = AdapterUtils.createNewEntityInstance("FCGContratoDadosGeraisSapiens");
						EntityWrapper wDadosGerais = new EntityWrapper(oDadosGeraisContrato);
						wDadosGerais.findField("codigoAgrupador").setValue(oAgrupador);

						String placas = wrapper.findGenericValue("placaVeiculos");
						if (placas != null)
						{
							placas = placas.replaceAll("/[^,\\w]*/g", "");
							Long nrPlacas = new Long(placas.split(",").length);
							if (!placas.equals(""))
								wDadosGerais.setValue("numVeiculos", nrPlacas);
						}

						NeoObject noTipoContrato = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("CETipoContrato"), new QLEqualsFilter("codigoProduto", 2l));
						wDadosGerais.setValue("tipoDoContrato", noTipoContrato);

						NeoObject noTipoServico = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENST160SCT"), new QLEqualsFilter("usu_serctr", 7l));
						wDadosGerais.setValue("tipoServicoContrato", noTipoServico);

						NeoObject debitoAutomatico = AdapterUtils.createNewEntityInstance("FGCDebitoAutomatico");
						EntityWrapper wDebitoAutomatico = new EntityWrapper(debitoAutomatico);
						Long usu_codemp = (Long) wrapper.findField("empresa.codemp").getValue();
						Long usu_codfil = (Long) wrapper.findField("empresa.codfil").getValue();

						DadosDebitoSapiensVO dsVO = ContratoUtils.retornaDadosDebitoSapiens(String.valueOf(cliente), String.valueOf(usu_codemp), String.valueOf(usu_codfil));

						if (dsVO != null && dsVO.getBanco() != null && !dsVO.getBanco().equals(""))
						{

							QLGroupFilter gpBancos = new QLGroupFilter("AND");
							gpBancos.addFilter(new QLEqualsFilter("codigo", dsVO.getBanco()));
							gpBancos.addFilter(new QLEqualsFilter("empresa", usu_codemp));
							gpBancos.addFilter(new QLEqualsFilter("filial", usu_codfil));

							List<NeoObject> listaBancos = PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCBancos"), gpBancos);

							NeoObject oBanco = null;
							if (listaBancos != null && !listaBancos.isEmpty())
							{
								oBanco = listaBancos.get(0);
							}

							wDebitoAutomatico.setValue("banco", oBanco);
							wDebitoAutomatico.setValue("agecia", dsVO.getAgencia());
							wDebitoAutomatico.setValue("contacorrente", dsVO.getContacorrente());
						}

						PersistEngine.persist(debitoAutomatico);
						wDadosGerais.setValue("debitoDados", debitoAutomatico);

						PersistEngine.persist(oDadosGeraisContrato);
					}
					else
					{
						ContratoLogUtils.logInfo("Agrupador não encontrato: AGR" + numContrato);
					}
				}
				else
				{
					ContratoLogUtils.logInfo("Agrupador não encontrato: AGR" + numContrato);
				}

			}
			
			if (new Long(modeloContrato) == 5l)
			{
				NeoObject dadosGeraisdoCliente = AdapterUtils.createNewEntityInstance("FGCClientesCadastroSapiens");
				EntityWrapper wDadosGeraisCliente = new EntityWrapper(dadosGeraisdoCliente);

				String tipo = "";
				String cpfCnpj = wrapper.findGenericValue("cgccpfConsulta");
				if (cpfCnpj != null)
				{
					if (cpfCnpj.length() == 11)
					{
						tipo = "F";
						wDadosGeraisCliente.setValue("cpf", cpfCnpj);
					}
					else if (cpfCnpj.length() == 14)
					{
						tipo = "J";
						wDadosGeraisCliente.setValue("cnpj", cpfCnpj);
					}

					if (!tipo.equals(""))
					{
						NeoObject noTipoCliente = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FGCClientesTipo"), new QLEqualsFilter("tipo", tipo));
						NeoObject noTipoClienteConsistencia = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUT402"), new QLEqualsFilter("usu_tipcli", tipo.equals("F") ? 1l : 2l));

						wDadosGeraisCliente.setValue("tipoClienteConsistencia", noTipoClienteConsistencia);
						wDadosGeraisCliente.setValue("tipocliente", noTipoCliente);
					}
				}

				/*
				 * Collection<NeoObject> listaEndCob = new ArrayList<NeoObject>();
				 * NeoObject enderecoCob =
				 * AdapterUtils.createNewEntityInstance("FGCListaEndCobClienteSapiens");
				 * NeoObject end = AdapterUtils.createNewEntityInstance("FGCEndereco");
				 * //EntityWrapper wEnderecoCob = new EntityWrapper(enderecoCob);
				 * //wEnderecoCob.setValue("endereco", end);
				 * listaEndCob.add(enderecoCob);
				 * wDadosGeraisCliente.setValue("endsCob",listaEndCob);
				 * PersistEngine.persist(enderecoCob);
				 */

				NeoObject dadosGeraisdoProcesso = (NeoObject) wrapper.findGenericValue("dadosGeraisContrato");
				if (dadosGeraisdoProcesso == null)
					dadosGeraisdoProcesso = AdapterUtils.createNewEntityInstance("FCGContratoDadosGeraisSapiens");
				
				EntityWrapper wDadosGerais = new EntityWrapper(dadosGeraisdoProcesso);

				// tipo de serviço do contrato
				
				EntityWrapper wDadosCtr = new EntityWrapper(dadosGeraisdoProcesso);
				//System.out.println("[FLUXO CONTRATOS] - modelo de contrato: " + modeloContrato);
				NeoObject tipoServico = null;
				if (modeloContrato != null && modeloContrato.equals("01"))
				{ // eletronica - alarmes
					tipoServico = retornaServicoContrato(2L);
				}
				else if (modeloContrato != null && modeloContrato.equals("02"))
				{ // eletronica - cftv
					tipoServico = retornaServicoContrato(3L);
				}
				if (tipoServico != null)
				{
					EntityWrapper wTipoServico = new EntityWrapper(tipoServico);
					if (wTipoServico.findValue("usu_desser") != null)
						wDadosCtr.setValue("tipoServicoContrato", tipoServico);
				}
				;

				// inicio de vigencia deve vir com a data atual para contratos novos
				wDadosCtr.setValue("inivig", new GregorianCalendar());
				wDadosCtr.setValue("inicioFaturamentoContrato", new GregorianCalendar());

				String placas = wrapper.findGenericValue("placaVeiculos");
				if (placas != null)
				{
					placas = placas.replaceAll("/[^,\\w]*/g", "");
					if (!placas.equals(""))
						wDadosGerais.setValue("numVeiculos", new Long(placas.split(",").length));
				}

				NeoObject noTipoContrato = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("CETipoContrato"), new QLEqualsFilter("codigoProduto", 2l));
				wDadosGerais.setValue("tipoDoContrato", noTipoContrato);

				NeoObject noTipoServico = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENST160SCT"), new QLEqualsFilter("usu_serctr", 7l));
				wDadosGerais.setValue("tipoServicoContrato", noTipoServico);

				NeoObject noFormaEnvio = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSET160FEN"), new QLEqualsFilter("usu_codfen", 1l));
				wDadosGerais.setValue("usucodfen", noFormaEnvio);

				QLGroupFilter qlGroupTransacao = new QLGroupFilter("AND", (tipo.equals("F") ? new QLEqualsFilter("codtns", "5949f") : new QLEqualsFilter("codtns", "5949a")), new QLEqualsFilter("codemp", codEmpresa));
				NeoObject noTransacao = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE001TNS"), qlGroupTransacao);
				wDadosGerais.setValue("transacao", noTransacao);

				NeoObject noDiaFixo = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FGCDiaFixoVencimento"), new QLEqualsFilter("diaVencimento", 3l));
				wDadosGerais.setValue("diaFixoVencimento", noDiaFixo);

				NeoObject noIndiceINPC = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE031MOE"), new QLEqualsFilter("codmoe", "08"));
				wDadosGerais.setValue("indice", noIndiceINPC);

				NeoObject noInstalador = retornaInstalador(201050l);
				wDadosGerais.setValue("instalador", noInstalador);

				wDadosGerais.findField("diaBaseFatura").setValue(12l);
				
				if (tipo.equals("F"))
					wDadosGerais.setValue("cpfResponsavelContrato", wrapper.findGenericValue("cgccpfConsulta"));

				PersistEngine.persist(dadosGeraisdoProcesso);

				ContratoLogUtils.logInfo("Preparando para preencher o representante " + PortalUtil.getCurrentUser().getCode() + " nos dados gerais do contrato.");
				NeoObject oRepresentante = ContratoUtils.retornaRepresentantePeloLogin(PortalUtil.getCurrentUser().getCode());
				if (oRepresentante != null)
				{ // preenche com o representante logado
					wDadosCtr.setValue("representante", oRepresentante);
				}

				try
				{
					QLGroupFilter gpFpg = new QLGroupFilter("AND");
					gpFpg.addFilter(new QLEqualsFilter("codemp", codEmpresa));
					gpFpg.addFilter(new QLEqualsFilter("codfpg", NeoUtils.safeLong("14")));

					List<NeoObject> formasPagamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE066FPG"), gpFpg);
					NeoObject formaPagamento = null;
					if (formasPagamento != null && !formasPagamento.isEmpty())
					{
						formaPagamento = formasPagamento.get(0);
					}

					wDadosGerais.setValue("formaPagamento", formaPagamento);

					NeoObject debitoAutomatico = AdapterUtils.createNewEntityInstance("FGCDebitoAutomatico");

					PersistEngine.persist(debitoAutomatico);
					wDadosGerais.setValue("debitoDados", debitoAutomatico);

					QLGroupFilter gp = new QLGroupFilter("AND");
					gp.addFilter(new QLEqualsFilter("codemp", codEmpresa));
					gp.addFilter(new QLEqualsFilter("codcpg", "PRXMES"));
					List<NeoObject> condsPagamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("CondicaoPagamento"), gp);
					NeoObject condPagamento = null;
					if (condsPagamento != null && !condsPagamento.isEmpty())
					{
						condPagamento = condsPagamento.get(0);
					}
					wDadosGerais.setValue("condicaoPagamento", condPagamento);
					wDadosGerais.setValue("numPostosPrevistos", (long) 1);

					if (movimentoContrato.equals(CONTRATO_NOVO_CLIENTE_NOVO) || movimentoContrato.equals(CONTRATO_NOVO_CLIENTE_NOVO_HUMANA) || movimentoContrato.equals(6l))
					{
						QLGroupFilter gpTipoMercado = new QLGroupFilter("AND");
						gpTipoMercado.addFilter(new QLEqualsFilter("tipo", "I"));
						NeoObject tipoMercado = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCClientesTipoMecado"), gpTipoMercado));
						wDadosGeraisCliente.setValue("tipoMercado", tipoMercado);

						QLGroupFilter gpRamoAtividade = new QLGroupFilter("AND");
						gpRamoAtividade.addFilter(new QLEqualsFilter("cd_ramo_atividade", (long) 10017));
						NeoObject ramoAtividade = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMARAMOATIVIDADE"), gpRamoAtividade));
						wDadosGeraisCliente.setValue("sigmaRamoAtividade", ramoAtividade);

						wDadosGerais.setValue("contratoLiberado", (boolean) true);

						QLGroupFilter gpDiaFixoVencimento = new QLGroupFilter("AND");
						gpDiaFixoVencimento.addFilter(new QLEqualsFilter("diaVencimento", (long) 3));
						NeoObject diaVencimento = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCDiaFixoVencimento"), gpDiaFixoVencimento));
						wDadosGerais.setValue("diaFixoVencimento", diaVencimento);

					}
					/*
					 * Está gerando classcast exception com.neomind.fusion.entity.ext.SAPIENSUSUARIO x
					 * com.neomind.fusion.entity.ext.SAPIENSEREP.
					 * Além disto esse código não tem efeito.
					 * String code = PortalUtil.getCurrentUser().getCode();
					 * QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", code);
					 * @SuppressWarnings("unchecked")
					 * ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>)
					 * PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUARIO"),
					 * loginFilter);
					 * if(sapiensUsers.size() > 0)
					 * wDadosGerais.setValue("representante", sapiensUsers.get(0));
					 */

					//CCT
					QLEqualsFilter qlIndice = new QLEqualsFilter("codmoe", "08");
					ArrayList<NeoObject> indices = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE031MOE"), qlIndice);
					if (indices.size() > 0)
						wDadosGerais.setValue("indice", indices.get(0));

					PersistEngine.persist(dadosGeraisdoProcesso);

					if (movimentoContrato == 1 || movimentoContrato == 2 || movimentoContrato == 5)
					{
						dadosGeraisdoCliente = buscaDadosGeraisCliente(wrapper, dadosGeraisdoCliente);
					}

					if (movimentoContrato.equals(CONTRATO_NOVO_CLIENTE_NOVO) || movimentoContrato.equals(CONTRATO_NOVO_CLIENTE_NOVO_HUMANA) || movimentoContrato.equals(6l))
					{
						PersistEngine.persist(dadosGeraisdoCliente);
					}

				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				wrapper.setValue("dadosGeraisContrato", dadosGeraisdoProcesso);
				wrapper.setValue("novoCliente", dadosGeraisdoCliente);

				NeoObject clienteBackup = EntityCloner.cloneNeoObject(dadosGeraisdoCliente);
				PersistEngine.persist(clienteBackup);
				wrapper.setValue("novoClienteBackup", clienteBackup);

				NeoObject dadosGeraisClone = EntityCloner.cloneNeoObject(dadosGeraisdoProcesso);
				wrapper.setValue("dadosGeraisContratoBackup", dadosGeraisClone);
			}
			else
			{
				/*
				 * valores padrao para o eform
				 */
				NeoObject dadosGeraisdoCliente = AdapterUtils.createNewEntityInstance("FGCClientesCadastroSapiens");
				EntityWrapper wDadosGeraisCliente = new EntityWrapper(dadosGeraisdoCliente);

				/*
				 * Collection<NeoObject> listaEndCob = new
				 * ArrayList<NeoObject>(); NeoObject enderecoCob =
				 * AdapterUtils.createNewEntityInstance
				 * ("FGCListaEndCobClienteSapiens"); NeoObject end =
				 * AdapterUtils.createNewEntityInstance("FGCEndereco");
				 * //EntityWrapper wEnderecoCob = new
				 * EntityWrapper(enderecoCob);
				 * //wEnderecoCob.setValue("endereco", end);
				 * listaEndCob.add(enderecoCob);
				 * wDadosGeraisCliente.setValue("endsCob",listaEndCob);
				 * PersistEngine.persist(enderecoCob);
				 */

				NeoObject dadosGeraisdoProcesso = AdapterUtils.createNewEntityInstance("FCGContratoDadosGeraisSapiens");
				EntityWrapper wDadosGerais = new EntityWrapper(dadosGeraisdoProcesso);

				// tipo de serviço do contrato
				EntityWrapper wDadosCtr = new EntityWrapper(dadosGeraisdoProcesso);
				// System.out.println("[FLUXO CONTRATOS] - modelo de contrato: "
				// + modeloContrato);
				NeoObject tipoServico = null;
				if (modeloContrato != null && modeloContrato.equals("01"))
				{ // eletronica
					// -
					// alarmes
					tipoServico = retornaServicoContrato(2L);
				}
				else if (modeloContrato != null && modeloContrato.equals("02"))
				{ // eletronica
					// -
					// cftv
					tipoServico = retornaServicoContrato(3L);
				}
				if (tipoServico != null)
				{
					EntityWrapper wTipoServico = new EntityWrapper(tipoServico);
					if (wTipoServico.findValue("usu_desser") != null)
						wDadosCtr.setValue("tipoServicoContrato", tipoServico);
				}
				;

				// inicio de vigencia deve vir com a data atual para contratos
				// novos
				wDadosCtr.setValue("inivig", new GregorianCalendar());
				wDadosCtr.setValue("inicioFaturamentoContrato", new GregorianCalendar());
				PersistEngine.persist(dadosGeraisdoProcesso);

				ContratoLogUtils.logInfo("Preparando para preencher o representante " + PortalUtil.getCurrentUser().getCode() + " nos dados gerais do contrato.");
				NeoObject oRepresentante = ContratoUtils.retornaRepresentantePeloLogin(PortalUtil.getCurrentUser().getCode());
				if (oRepresentante != null)
				{ // preenche com o representante
					// logado
					wDadosCtr.setValue("representante", oRepresentante);
				}

				try
				{
					QLGroupFilter gpFpg = new QLGroupFilter("AND");
					gpFpg.addFilter(new QLEqualsFilter("codemp", codEmpresa));
					gpFpg.addFilter(new QLEqualsFilter("codfpg", NeoUtils.safeLong("14")));

					List<NeoObject> formasPagamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE066FPG"), gpFpg);
					NeoObject formaPagamento = null;
					if (formasPagamento != null && !formasPagamento.isEmpty())
					{
						formaPagamento = formasPagamento.get(0);
					}

					wDadosGerais.setValue("formaPagamento", formaPagamento);

					NeoObject debitoAutomatico = AdapterUtils.createNewEntityInstance("FGCDebitoAutomatico");

					PersistEngine.persist(debitoAutomatico);
					wDadosGerais.setValue("debitoDados", debitoAutomatico);

					QLGroupFilter gp = new QLGroupFilter("AND");
					gp.addFilter(new QLEqualsFilter("codemp", codEmpresa));
					gp.addFilter(new QLEqualsFilter("codcpg", "PRXMES"));
					List<NeoObject> condsPagamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("CondicaoPagamento"), gp);
					NeoObject condPagamento = null;
					if (condsPagamento != null && !condsPagamento.isEmpty())
					{
						condPagamento = condsPagamento.get(0);
					}
					wDadosGerais.setValue("condicaoPagamento", condPagamento);
					wDadosGerais.setValue("numPostosPrevistos", (long) 1);

					if (movimentoContrato.equals(CONTRATO_NOVO_CLIENTE_NOVO) || movimentoContrato.equals(CONTRATO_NOVO_CLIENTE_NOVO_HUMANA))
					{
						QLGroupFilter gpTipoMercado = new QLGroupFilter("AND");
						gpTipoMercado.addFilter(new QLEqualsFilter("tipo", "I"));
						NeoObject tipoMercado = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCClientesTipoMecado"), gpTipoMercado));
						wDadosGeraisCliente.setValue("tipoMercado", tipoMercado);

						QLGroupFilter gpRamoAtividade = new QLGroupFilter("AND");
						gpRamoAtividade.addFilter(new QLEqualsFilter("cd_ramo_atividade", (long) 10017));
						NeoObject ramoAtividade = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMARAMOATIVIDADE"), gpRamoAtividade));
						wDadosGeraisCliente.setValue("sigmaRamoAtividade", ramoAtividade);

						wDadosGerais.setValue("contratoLiberado", (boolean) true);

						QLGroupFilter gpDiaFixoVencimento = new QLGroupFilter("AND");
						gpDiaFixoVencimento.addFilter(new QLEqualsFilter("diaVencimento", (long) 3));
						NeoObject diaVencimento = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCDiaFixoVencimento"), gpDiaFixoVencimento));
						wDadosGerais.setValue("diaFixoVencimento", diaVencimento);

					}
					/*
					 * Está gerando classcast exception
					 * com.neomind.fusion.entity.ext.SAPIENSUSUARIO x
					 * com.neomind.fusion.entity.ext.SAPIENSEREP. Além disto
					 * esse código não tem efeito. String code =
					 * PortalUtil.getCurrentUser().getCode(); QLEqualsFilter
					 * loginFilter = new QLEqualsFilter("nomusu", code);
					 * @SuppressWarnings("unchecked") ArrayList<NeoObject>
					 * sapiensUsers = (ArrayList<NeoObject>)
					 * PersistEngine.getObjects
					 * (AdapterUtils.getEntityClass("SAPIENSUSUARIO"),
					 * loginFilter); if(sapiensUsers.size() > 0)
					 * wDadosGerais.setValue("representante",
					 * sapiensUsers.get(0));
					 */

					// CCT
					QLEqualsFilter qlIndice = new QLEqualsFilter("codmoe", "08");
					ArrayList<NeoObject> indices = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE031MOE"), qlIndice);
					if (indices.size() > 0)
						wDadosGerais.setValue("indice", indices.get(0));

					PersistEngine.persist(dadosGeraisdoProcesso);

					if (movimentoContrato == 1 || movimentoContrato == 2 || movimentoContrato == 5)
					{
						dadosGeraisdoCliente = buscaDadosGeraisCliente(wrapper, dadosGeraisdoCliente);
					}

					if (movimentoContrato.equals(CONTRATO_NOVO_CLIENTE_NOVO) || movimentoContrato.equals(CONTRATO_NOVO_CLIENTE_NOVO_HUMANA))
					{
						PersistEngine.persist(dadosGeraisdoCliente);
					}

				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				wrapper.setValue("dadosGeraisContrato", dadosGeraisdoProcesso);
				wrapper.setValue("novoCliente", dadosGeraisdoCliente);

				NeoObject clienteBackup = EntityCloner.cloneNeoObject(dadosGeraisdoCliente);
				PersistEngine.persist(clienteBackup);
				wrapper.setValue("novoClienteBackup", clienteBackup);
			}
		}
		catch (Exception e)
		{
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				log.error("[FLUXO CONTRATOS] - Erro ao gerar dados iniciais no Fluxo de contratos.", e);
				throw new WorkflowException("Erro ao gerar dados iniciais no Fluxo de contratos." + e);
			}
		}

		/*
		 * aqui setamos o papel da analise de valores e o papel de análise de contrato
		 * estes papéis serão utilizados para escalar a atividade
		 * papelValores
		 * papelAnalista
		 */

		//NeoPaper papelValores = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "FGCGestorVendas"));
		//wrapper.setValue("papelValores", papelValores);

	}

	public NeoUser getSuperior(NeoUser usuarioAtual)
	{

		NeoGroup grupoAtual = usuarioAtual.getGroup();
		NeoGroup grupoSuperior = grupoAtual.getUpperLevel();
		NeoPaper responsavelGrupoAtual = grupoAtual.getResponsible();
		NeoPaper responsavelGrupoSuperior = grupoSuperior.getResponsible();
		NeoUser usuarioResponsavelGrupoAtual = responsavelGrupoAtual.getUsers().iterator().next();
		NeoUser superior = null;

		if (usuarioAtual.getCode().equals(usuarioResponsavelGrupoAtual.getCode()))
		{
			superior = responsavelGrupoSuperior.getUsers().iterator().next();
		}
		else
		{
			superior = usuarioResponsavelGrupoAtual;
		}

		return superior;

	}

	public NeoObject buscaDadosGeraisCliente(EntityWrapper wrapper, NeoObject dadosGeraisdoCliente)
	{
		EntityWrapper wDadosGeraisCliente = new EntityWrapper(dadosGeraisdoCliente);
		Long movimentoContrato = (Long) wrapper.findField("movimentoContratoNovo.codTipo").getValue();
		Long tipoAlteracao = 0L;
		if (movimentoContrato == 5)
		{
			tipoAlteracao = (Long) wrapper.findField("movimentoAlteracaoContrato.codTipo").getValue();
		}
		
		NeoObject cliente = (NeoObject) wrapper.findValue("buscaNomeCliente");
		EntityWrapper wCliente = new EntityWrapper(cliente);

		// copia a lista de enderecos de cobranca do cliente (botao do sapiens)
		Long codcli = NeoUtils.safeLong(NeoUtils.safeOutputString(wCliente.findValue("codcli")));

		Collection<NeoObject> endsCob = new ArrayList<NeoObject>();

		/**
		 * @author orsegups lucas.avila - case sensitive.
		 * @date 08/07/2015
		 */
		if (codcli != null)
		{
			List<NeoObject> listaEnderecosCobranca = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE085COB"), new QLEqualsFilter("codcli", codcli));
			if (listaEnderecosCobranca != null && listaEnderecosCobranca.size() > 0)
			{
				for (NeoObject objSapiens : listaEnderecosCobranca)
				{
					NeoObject enderecoCob = AdapterUtils.createNewEntityInstance("FGCListaEndCobClienteSapiens");
					NeoObject end = AdapterUtils.createNewEntityInstance("FGCEndereco");
					EntityWrapper wObjSapiens = new EntityWrapper(objSapiens);
					EntityWrapper wEnderecoCob = new EntityWrapper(enderecoCob);

					// EntityWrapper wEnd = new EntityWrapper(end);

					// usarComoDoContrato

					// wEnderecoCob.findValue("usarComoDoContrato")

					wEnderecoCob.setValue("cep", NeoUtils.safeOutputString(wObjSapiens.findValue("cepcob")));
					wEnderecoCob.setValue("endereco", wObjSapiens.findValue("endcob"));
					wEnderecoCob.setValue("numero", wObjSapiens.findValue("nencob"));
					wEnderecoCob.setValue("complemento", wObjSapiens.findValue("cplcob"));
					wEnderecoCob.setValue("bairro", wObjSapiens.findValue("baicob"));
					// wEnderecoCob.setValue("estadoCidade",wObjSapiens.findValue());

					String seqCob = NeoUtils.safeOutputString(wObjSapiens.findValue("seqcob"));
					String cidCob = NeoUtils.safeOutputString(wObjSapiens.findValue("cidcob"));
					String estCob = NeoUtils.safeOutputString(wObjSapiens.findValue("estcob"));
					String cepCob = NeoUtils.safeOutputString(wObjSapiens.findValue("cepcob"));

					List<NeoObject> estado2 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE007UFS"), new QLEqualsFilter("sigufs", estCob));
					QLGroupFilter gp = new QLGroupFilter("AND");
					gp.addFilter(new QLEqualsFilter("sigufs", estCob));
					// gp.addFilter(new
					// QLRawFilter("nomcid ='"+ContratoUtils.retiraCaracteresAcentuados(cidadeCliente)
					// +"'" ));
					gp.addFilter(new QLRawFilter("cepini <= " + cepCob));
					gp.addFilter(new QLRawFilter("cepfim >= " + cepCob));
					List<NeoObject> cidade2 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE008CEP"), gp);
					NeoObject estadoCidade = AdapterUtils.createNewEntityInstance("FGCEstadoCidade");
					EntityWrapper wEstadoCidade = new EntityWrapper(estadoCidade);
					wEstadoCidade.setValue("estado", estado2.get(0));
					wEstadoCidade.setValue("cidade", cidade2.get(0));
					PersistEngine.persist(estadoCidade);
					wEnderecoCob.setValue("estadoCidade", estadoCidade);

					wEnderecoCob.setValue("seqCob", NeoUtils.safeLong(seqCob));
					// wEnderecoCob.setValue("endereco", end);

					endsCob.add(enderecoCob);

				}

				EntityWrapper wNovoCliente = new EntityWrapper(dadosGeraisdoCliente);

				wNovoCliente.setValue("endsCob", endsCob);
			}

		}

		String cep = NeoUtils.safeOutputString(wCliente.findValue("cepcli"));
		String endereco = NeoUtils.safeOutputString(wCliente.findValue("endcli"));
		String numero = NeoUtils.safeOutputString(wCliente.findValue("nencli"));
		String complemento = NeoUtils.safeOutputString(wCliente.findValue("cplend"));
		String bairro = NeoUtils.safeOutputString(wCliente.findValue("baicli"));
		String cidade = NeoUtils.safeOutputString(wCliente.findValue("cidcli"));
		String estado = NeoUtils.safeOutputString(wCliente.findValue("sigufs"));
		NeoObject enderecoCliente = getEndereco(cep, endereco, numero, complemento, bairro, cidade, estado);

		String cepcob = NeoUtils.safeOutputString(wCliente.findValue("cepcob"));
		String endcob = NeoUtils.safeOutputString(wCliente.findValue("endcob"));
		String nencob = NeoUtils.safeOutputString(wCliente.findValue("nencob"));
		String cplcob = NeoUtils.safeOutputString(wCliente.findValue("cplcob"));
		String baicob = NeoUtils.safeOutputString(wCliente.findValue("baicob"));
		String cidcob = NeoUtils.safeOutputString(wCliente.findValue("cidcob"));
		String estcob = NeoUtils.safeOutputString(wCliente.findValue("estcob"));
		NeoObject enderecoCobranca = getEndereco(cepcob, endcob, nencob, cplcob, baicob, cidcob, estcob);

		String cepent = NeoUtils.safeOutputString(wCliente.findValue("cepent"));
		String endent = NeoUtils.safeOutputString(wCliente.findValue("endent"));
		String nenent = NeoUtils.safeOutputString(wCliente.findValue("nenent"));
		String cplent = NeoUtils.safeOutputString(wCliente.findValue("cplent"));
		String baient = NeoUtils.safeOutputString(wCliente.findValue("baient"));
		String cident = NeoUtils.safeOutputString(wCliente.findValue("cident"));
		String estent = NeoUtils.safeOutputString(wCliente.findValue("estent"));
		NeoObject enderecoEntrega = getEndereco(cepent, endent, nenent, cplent, baient, cident, estent);

		String telefone1 = NeoUtils.safeOutputString(wCliente.findValue("foncli"));
		String telefone2 = NeoUtils.safeOutputString(wCliente.findValue("foncl2"));
		String email = NeoUtils.safeOutputString(wCliente.findValue("intnet"));
		String fax = NeoUtils.safeOutputString(wCliente.findValue("faxcli"));

		String tipcli = NeoUtils.safeOutputString(wCliente.findValue("tipcli"));
		String usuTipcli = NeoUtils.safeOutputString(wCliente.findValue("usu_tipcli"));
		String cgccpf = NeoUtils.safeOutputString(wCliente.findValue("cgccpf"));

		wDadosGeraisCliente.setValue("enderecoCliente", enderecoCliente);
		wDadosGeraisCliente.setValue("endCobrancaIgualCliente", false);
		// endereço de entrega quase 100% das vezes vem vazio do sapiens
		wDadosGeraisCliente.setValue("endEntregaIgualCliente", true);
		wDadosGeraisCliente.setValue("endCobranca", enderecoCobranca);
		wDadosGeraisCliente.setValue("endEntrega", enderecoEntrega);
		wDadosGeraisCliente.setValue("telefone1", telefone1);
		wDadosGeraisCliente.setValue("telefone2", telefone2);
		wDadosGeraisCliente.setValue("email", email);
		wDadosGeraisCliente.setValue("fax", fax);

		// FGCClientesTipo
		if (tipcli != null)
		{
			List<NeoObject> tiposCliente = PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCClientesTipo"), new QLEqualsFilter("tipo", tipcli));
			if (tiposCliente != null && tiposCliente.size() > 0)
			{
				NeoObject obj = tiposCliente.get(0);
				wDadosGeraisCliente.setValue("tipocliente", obj);

				if (movimentoContrato == 5 && tipoAlteracao == 1)
				{
					cgccpf = null;
				} // se for alteração de cnpj, não preencher o campo de cpf/cnpj

				if (tipcli.equals("F"))
				{
					wDadosGeraisCliente.setValue("cpf", cgccpf);
				}
				else
				{
					wDadosGeraisCliente.setValue("cnpj", cgccpf);
				}
			}
		}
		// Tipo de cliente para consistencia
		if (usuTipcli != null)
		{
			List<NeoObject> tiposCliente = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUT402"), new QLEqualsFilter("usu_tipcli", NeoUtils.safeLong(usuTipcli)));
			if (tiposCliente != null && tiposCliente.size() > 0)
			{
				NeoObject obj = tiposCliente.get(0);
				wDadosGeraisCliente.setValue("tipoClienteConsistencia", obj);
			}
		}

		// consulta código do regime triburario do cliente
		Long codRtr = ContratoUtils.retornaCodRegimeTributarioCliente(codcli);
		if (codRtr != null)
		{
			List<NeoObject> listaCodRtr = PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCRegimeTributario"), new QLEqualsFilter("codigo", codRtr));
			if (listaCodRtr != null && listaCodRtr.size() > 0)
			{
				NeoObject obj = listaCodRtr.get(0);
				wDadosGeraisCliente.setValue("regimeTributario", obj);
			}

		}

		PersistEngine.persist(dadosGeraisdoCliente);

		return dadosGeraisdoCliente;
	}

	public static NeoObject getEndereco(String cep, String endereco, String numero, String complemento, String bairro, String cidade, String estado)
	{
		if (cep == null || cep.equals("") || cep.equals("0"))
			return null;

		NeoObject enderecoCliente = AdapterUtils.createNewEntityInstance("FGCEndereco");
		EntityWrapper wEnderecoCliente = new EntityWrapper(enderecoCliente);
		wEnderecoCliente.setValue("cep", cep);
		wEnderecoCliente.setValue("endereco", endereco);
		wEnderecoCliente.setValue("complemento", complemento);
		wEnderecoCliente.setValue("bairro", bairro);
		wEnderecoCliente.setValue("numero", numero);
		String cidadeCliente = NeoUtils.safeOutputString(cidade);
		String estadoCliente = NeoUtils.safeOutputString(estado);
		List<NeoObject> estado2 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE007UFS"), new QLEqualsFilter("sigufs", estadoCliente));
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("sigufs", estadoCliente));
		// gp.addFilter(new
		// QLRawFilter("nomcid ='"+ContratoUtils.retiraCaracteresAcentuados(cidadeCliente)
		// +"'" ));
		gp.addFilter(new QLRawFilter("cepini <= " + cep));
		gp.addFilter(new QLRawFilter("cepfim >= " + cep));
		List<NeoObject> cidade2 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE008CEP"), gp);
		NeoObject estadoCidade = AdapterUtils.createNewEntityInstance("FGCEstadoCidade");
		EntityWrapper wEstadoCidade = new EntityWrapper(estadoCidade);
		wEstadoCidade.setValue("estado", estado2.get(0));
		wEstadoCidade.setValue("cidade", cidade2.get(0));
		PersistEngine.persist(estadoCidade);

		wEnderecoCliente.setValue("estadoCidade", estadoCidade);
		PersistEngine.persist(enderecoCliente);

		return enderecoCliente;
	}

	@Override
	public void back(EntityWrapper wrapper, Activity activity)
	{
		try
		{
			ContratoLogUtils.logInfo("Back Dados gerais! limpando objetos já preenchidos");
			wrapper.setValue("novoCliente", (NeoObject) null);
			wrapper.setValue("novoClienteBackup", (NeoObject) null);
			wrapper.setValue("numContrato", (NeoObject) null);
			wrapper.setValue("numContratoBackup", (NeoObject) null);
			wrapper.setValue("dadosGeraisContrato", (NeoObject) null);
			wrapper.setValue("dadosGeraisContratoBackup", (NeoObject) null);
			wrapper.setValue("postosContrato", (NeoObject) null);
			wrapper.setValue("postosContratoBackup", (NeoObject) null);
			wrapper.setValue("listaAlteracoesContrato", (NeoObject) null);
			wrapper.setValue("listaAlteracoesCliente", (NeoObject) null);
			wrapper.setValue("motivoInativacao", (NeoObject) null);
			wrapper.setValue("novoCliente", (NeoObject) null);
			wrapper.setValue("buscaNomeCliente", (NeoObject) null);
		}
		catch (Exception e)
		{
			ContratoLogUtils.logInfo("Erro ao recusar tarefas Dados Gerais.");
			e.printStackTrace();
		}

	}

	/**
	 * Gera Dados Iniciais - Popula o eform do processo com os dados do sapiens
	 * 
	 * @param codEmpresa
	 * @param cliente
	 * @param contrato
	 * @param wrapper
	 * 
	 */
	public static void geraDadosIniciais(Long codEmpresa, Long cliente, Long contrato, EntityWrapper wrapper, Long periodoVigencia)
	{
		System.out.println("[FLUXO CONTRATOS]- Gera Dados Iniciais");

		NeoObject oDadosGeraisContrato = null;

		if (wrapper.findValue("dadosGeraisContrato") == null)
		{
			oDadosGeraisContrato = AdapterUtils.createNewEntityInstance("FCGContratoDadosGeraisSapiens");
		}
		else
		{
			oDadosGeraisContrato = (NeoObject) wrapper.findField("dadosGeraisContrato").getValue();
		}

		// SAPIENSUSUVCTRSAPFUS
		List<NeoObject> sapiensDadosGeraisContrato = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUVCTRSAPFUS"), new QLGroupFilter("AND", new QLEqualsFilter("situacao_contrato", "A"), new QLEqualsFilter("numero_contrato", contrato), new QLEqualsFilter("codigo_cliente", cliente)));

		if (sapiensDadosGeraisContrato != null && !sapiensDadosGeraisContrato.isEmpty())
		{
			if (oDadosGeraisContrato != null)
			{
				for (NeoObject dados : sapiensDadosGeraisContrato)
				{
					EntityWrapper wSapiensDadosGeraisContrato = new EntityWrapper(dados);
					EntityWrapper wDadosContrato = new EntityWrapper(oDadosGeraisContrato);

					String email_do_contrato = (String) wSapiensDadosGeraisContrato.findField("email_do_contrato").getValue();
					String nome_contato = (String) wSapiensDadosGeraisContrato.findField("nome_contato").getValue();
					String fone_contato = (String) wSapiensDadosGeraisContrato.findField("fone_contato").getValue();
					GregorianCalendar data_emisso_contrato = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("data_emisso_contrato").getValue();

					wDadosContrato.findField("emailContato").setValue(email_do_contrato);
					wDadosContrato.findField("foneCOntato").setValue(fone_contato);
					wDadosContrato.findField("pessoaContatoComercial").setValue(nome_contato);
					wDadosContrato.findField("dataEmissaoContrato").setValue(data_emisso_contrato);

					GregorianCalendar iniVig = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("inicio_vigencia_contrato").getValue();
					GregorianCalendar fimVig = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("fim_vigencia").getValue();
					GregorianCalendar iniFat = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("inicio_faturamento_contrato").getValue();
					GregorianCalendar fimFat = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("fim_faturamento_contrato").getValue();
					String diaFixoVencimento = wSapiensDadosGeraisContrato.findField("dia_fixo_vencimento").getValueAsString();

					// Solicitado pela pricila via tarefa simples que esta data
					// não fosse copiada do contrato antigo. Tarefa Simples N.
					// 483249 27/01/2015
					// wDadosContrato.findField("inivig").setValue(iniVig);
					// wDadosContrato.findField("fimvig").setValue(fimVig);
					// wDadosContrato.findField("inicioFaturamentoContrato").setValue(iniFat);
					// wDadosContrato.findField("fimFaturamentoContrato").setValue(fimFat);

					QLGroupFilter filter = new QLGroupFilter("AND");
					filter.addFilter(new QLEqualsFilter("diaVencimento", Long.parseLong(diaFixoVencimento, 10)));
					NeoObject diaFix = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCDiaFixoVencimento"), filter));

					wDadosContrato.findField("diaFixoVencimento").setValue(diaFix);

					/*
					 * Inicio_vigencia_contrato Fim_vigencia
					 * Inicio_faturamento_contrato Fim_faturamento_contrato
					 * Dia_fixo_vencimento
					 */

					String numero_oficial_contrato = (String) wSapiensDadosGeraisContrato.findField("numero_oficial_contrato").getValue();

					wDadosContrato.findField("numeroOficialContrato").setValue(numero_oficial_contrato);

					Long codRegional = (Long) wSapiensDadosGeraisContrato.findField("regional_contrato").getValue();
					if (codRegional != null)
						wDadosContrato.findField("regional").setValue(retornaRegional(codRegional));

					String objeto_contrato = (String) wSapiensDadosGeraisContrato.findField("objeto_contrato").getValue();
					String observacoes = (String) wSapiensDadosGeraisContrato.findField("observacoes").getValue();

					wDadosContrato.findField("ObjetoDoContrato").setValue(objeto_contrato);
					wDadosContrato.findField("observacaoComerciais").setValue(observacoes);

					ContratoLogUtils.logInfo("Preparando para preencher o representante " + PortalUtil.getCurrentUser().getCode() + " nos dados gerais do contrato.");
					NeoObject oRepresentante = ContratoUtils.retornaRepresentantePeloLogin(PortalUtil.getCurrentUser().getCode());
					if (oRepresentante != null)
					{ // preenche com o
						// representante logado
						wDadosContrato.setValue("representante", oRepresentante);
					}
					else
					{ // preenche com o representante do contrato, se
						// houver
						String nome_representante = (String) wSapiensDadosGeraisContrato.findField("nome_representante").getValue();
						Long codRepresentante = (Long) wSapiensDadosGeraisContrato.findField("codigo_representante").getValue();
						if (codRepresentante != null)
							wDadosContrato.findField("representante").setValue(retornaRepresentante(codRepresentante));
					}

					String descricao_transacao = (String) wSapiensDadosGeraisContrato.findField("descricao_transacao").getValue();
					String codTransacao = (String) wSapiensDadosGeraisContrato.findField("codigo_transacao").getValue();

					if (codTransacao != null)
						wDadosContrato.findField("transacao").setValue(retornaTransacao(codEmpresa, codTransacao));

					GregorianCalendar inicio_vigencia_contrato = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("inicio_vigencia_contrato").getValue();
					GregorianCalendar fim_vigencia = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("fim_vigencia").getValue();
					// usu_pervig o cálculo que deve ser feito é simples (Regra
					// = USU_T160CTR.USU_IniVig + ( ( USU_T160CTR.USU_PerVig /12
					// ) * 365.25 ))
					// Long peridoMaximoVigencia = (long) ((long)
					// inicio_vigencia_contrato.get(GregorianCalendar.MONTH) +
					// ((periodoVigencia / 12) * 365.25));

					Long peridoMaximoVigencia2 = retornarPeriodoVigenciaMaximoContrato(contrato);
					if (peridoMaximoVigencia2 != null)
					{
						ContratoLogUtils.logInfo("Periodo Maximo Vigencia: " + peridoMaximoVigencia2);
						NeoObject periodoMaxVig = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCPeriodoMaximoVigencia"), new QLEqualsFilter("meses", peridoMaximoVigencia2)));
						wDadosContrato.setValue("peridoMaximoVigencia2", periodoMaxVig);
					}

					wDadosContrato.findField("peridoVigencia").setValue(inicio_vigencia_contrato);
					GregorianCalendar inicio_faturamento_contrato = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("inicio_faturamento_contrato").getValue();
					GregorianCalendar fim_faturamento_contrato = (GregorianCalendar) wSapiensDadosGeraisContrato.findField("fim_faturamento_contrato").getValue();

					// Solicitado pela pricila via tarefa simples que esta data
					// não fosse copiada do contrato antigo. Tarefa Simples N.
					// 483249 27/01/2015
					// wDadosContrato.findField("inicioFaturamentoContrato").setValue(inicio_faturamento_contrato);
					// wDadosContrato.findField("fimFaturamentoContrato").setValue(fim_faturamento_contrato);

					Long dia_base = (Long) wSapiensDadosGeraisContrato.findField("dia_base").getValue();
					NeoObject oDiaVencimento = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCDiaFixoVencimento"), new QLEqualsFilter("diaVencimento", Long.parseLong(diaFixoVencimento))));

					wDadosContrato.findField("diaBaseFatura").setValue(dia_base);
					wDadosContrato.findField("diaFixoVencimento").setValue(oDiaVencimento);

					String codIndice = (String) wSapiensDadosGeraisContrato.findField("indice_reajuste").getValue();
					if (codIndice != null)
						wDadosContrato.findField("indice").setValue(retornaIndice(codIndice));

					String codCondicaoPagamento = (String) wSapiensDadosGeraisContrato.findField("codigo_condicao_pagamento").getValue();
					if (codCondicaoPagamento != null)
						wDadosContrato.findField("condicaoPagamento").setValue(retornaCondicaoPagamento(codEmpresa, codCondicaoPagamento));

					Long codFormaPagamento = (Long) wSapiensDadosGeraisContrato.findField("codigo_forma_pagamento").getValue();
					if (codFormaPagamento != null)
						wDadosContrato.findField("formaPagamento").setValue(retornaFormaPagamento(codEmpresa, codFormaPagamento));

					String campo = (String) wSapiensDadosGeraisContrato.findField("contrato_composto").getValue();
					Boolean contratoComposto = campo.equals("S") ? true : false;
					wDadosContrato.findField("contratoComposto").setValue(contratoComposto);

					String campo2 = (String) wSapiensDadosGeraisContrato.findField("exige_medicao").getValue();
					Boolean exigeMedicao = campo2.equals("S") ? true : false;
					wDadosContrato.findField("exigeMedicao").setValue(exigeMedicao);

					Long numero_postos_previstos = (Long) wSapiensDadosGeraisContrato.findField("numero_postos_previstos").getValue();
					Long numero_postos_efetivos = (Long) wSapiensDadosGeraisContrato.findField("numero_postos_efetivos").getValue();

					wDadosContrato.findField("numPostosPrevistos").setValue(numero_postos_previstos);
					wDadosContrato.findField("numPostosEfetivos").setValue(numero_postos_efetivos);

					// String campo3 = (String)
					// wSapiensDadosGeraisContrato.findField("contratoLiberado").getValue();

					Boolean contratoLiberado = true;
					wDadosContrato.setValue("contratoLiberado", contratoLiberado);

					Long codTipoDoContrato = (Long) wSapiensDadosGeraisContrato.findField("tipo_contrato").getValue();
					if (codTipoDoContrato != null)
						wDadosContrato.findField("tipoDoContrato").setValue(retornaTipoContrato(codTipoDoContrato));

					Long codTipoServicoContrato = (Long) wSapiensDadosGeraisContrato.findField("tipo_servico_contrato").getValue();
					if (codTipoServicoContrato != null)
						wDadosContrato.findField("tipoServicoContrato").setValue(retornaServicoContrato(codTipoServicoContrato));

					String campo4 = (String) wSapiensDadosGeraisContrato.findValue("suspender_faturamento");
					boolean suspenderFaturamento = campo4.equals("S") ? true : false;

					// removido pois neste ponto, esse campo é read-only e lança
					// exceção
					wDadosContrato.setValue("suspenderFaturamento", suspenderFaturamento);

					// seta os dados gerais no pai
					PersistEngine.persist(oDadosGeraisContrato);
					wrapper.findField("dadosGeraisContrato").setValue(oDadosGeraisContrato);

					NeoObject dadosGeraisClone = EntityCloner.cloneNeoObject(oDadosGeraisContrato);
					wrapper.setValue("dadosGeraisContratoBackup", dadosGeraisClone);
				}

			}
		}
		else
		{
			throw new WorkflowException("Contrato Inativo ou dados gerais não encontrados. Verifique a situação do contrato no sapiens");
		}

	}

	private static long retornarPeriodoVigenciaMaximoContrato(Long contrato)
	{
		Long retorno = 0L;
		String sql = "select usu_pervig from usu_t160ctr where usu_numctr = " + contrato;
		ResultSet rs = null;
		Long codRep = 0L;
		try
		{
			rs = OrsegupsContratoUtils.selectTable(sql, PersistEngine.getConnection("SAPIENS"));
			if (rs != null)
			{
				retorno = rs.getLong(1);
			}
			return retorno;
		}
		catch (Exception e)
		{
			ContratoLogUtils.logInfo("Erro ao buscar Periodo de vigencia maxima Contrato=" + contrato);
			e.printStackTrace();
			return (Long) null;
		}

	}

	/**
	 * Busca todos os postos do contrato do sapiens e popula os campos do fusion
	 * 
	 * @param codEmpresa
	 * @param cliente
	 * @param numContrato
	 * @param wrapper
	 * @return lista de neobject do FGCCadastroPostos
	 */
	public static List<NeoObject> geraPostos(Long codEmpresa, Long cliente, Long numContrato, EntityWrapper wrapper)
	{
		Long movimentoContrato = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("movimentoContratoNovo.codTipo")));
		Long movimentoAlteracaoContrato = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("movimentoAlteracaoContrato.codTipo")));

		System.out.println("[FLUXO CONTRATOS]- Gera Postos");
		List<NeoObject> postos = new ArrayList<NeoObject>();

		NeoObject contratoExt = (NeoObject) wrapper.findField("numContrato").getValue();
		EntityWrapper wContrato = new EntityWrapper(contratoExt);

		Long codFil = (Long) wContrato.findField("usu_codfil").getValue();

		// SAPIENSUSUTCVS
		List<NeoObject> listaPostosExt = retornaListaPostos(codEmpresa, codFil, numContrato);

		// NeoObject regional = (NeoObject)
		// wrapper.findField("dadosGeraisContrato.regional").getValue();

		if (!listaPostosExt.isEmpty())
		{
			for (NeoObject postoExt : listaPostosExt)
			{
				EntityWrapper wPostoExt = new EntityWrapper(postoExt);

				NeoObject oNovoPosto = AdapterUtils.createNewEntityInstance("FGCCadastroPostos");
				EntityWrapper wNovoPosto = new EntityWrapper(oNovoPosto);

				// setando posto por padrao para protecao garantida
				// asdasdasd

				String usu_codserExt = NeoUtils.safeOutputString(wPostoExt.findValue("usu_codser"));

				// contrat
				NeoObject tipoPosto = null;
				if ("9002037".equals(usu_codserExt))
				{
					tipoPosto = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCTipoPosto"), new QLEqualsFilter("codigo", 2L))); // Proteção
					// garantida
				}
				else
				{
					tipoPosto = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCTipoPosto"), new QLEqualsFilter("codigo", 1L))); // normal
				}
				wNovoPosto.setValue("tipoPosto", tipoPosto);

				Long acao = (Long) wrapper.findField("movimentoContratoNovo.codTipo").getValue();
				Long tipoAlteracao = (Long) wrapper.findField("movimentoAlteracaoContrato.codTipo").getValue();

				if (wPostoExt != null && acao.equals(ALTERACAO_TERMO_ADITIVO) && (tipoAlteracao != null && (tipoAlteracao == 3 || tipoAlteracao == 4 || tipoAlteracao == 6 || tipoAlteracao == 9 || tipoAlteracao == 10)))
				{
					// duplicarDesativa
					wNovoPosto.findField("duplicarDesativar").setValue(true);

					// de/para definido com a fernanda martins
					Long codMotivo = 0L;
					if (acao == 5)
					{

						switch (tipoAlteracao.intValue())
						{
							case 1:
								codMotivo = 136L;
								break;
							case 2:
								codMotivo = 111L;
								break;
							case 3:
								codMotivo = 105L;
								break;
							case 4:
								codMotivo = 106L;
								break;
							case 6:
								codMotivo = 108L;
								break;
							case 9:
								codMotivo = 101L; // (quando houver alteração e
								// endereço o código é 97)
								break;
							case 10:
								codMotivo = 105L;
								break;
							default:
								codMotivo = 0L;
								break;
						}

					}

					QLGroupFilter keyMot = new QLGroupFilter("AND");
					keyMot.addFilter(new QLEqualsFilter("codmot", codMotivo));
					NeoObject oMotivo = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEMOT"), keyMot));

					wNovoPosto.findField("motivo").setValue(oMotivo);

				}

				String codTns = (String) wPostoExt.findValue("usu_tnsser");
				String suspenderFaturamento = (String) wPostoExt.findValue("usu_susfat");
				String postoLiberadoFaturamento = (String) wPostoExt.findValue("usu_libfat");
				String geraComissaoVendas = (String) wPostoExt.findValue("usu_gercom");

				QLGroupFilter keyTran = new QLGroupFilter("AND");
				keyTran.addFilter(new QLEqualsFilter("codtns", codTns));
				NeoObject oTransacao = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE001TNS"), keyTran));

				wNovoPosto.setValue("transacao", oTransacao);
				wNovoPosto.setValue("suspenderFaturamento", (suspenderFaturamento != null && suspenderFaturamento.equals("S") ? true : false));
				wNovoPosto.setValue("postoLiberadoFaturamento", (postoLiberadoFaturamento != null && postoLiberadoFaturamento.equals("S") ? true : false));
				wNovoPosto.setValue("geraComissaoVendas", (geraComissaoVendas != null && geraComissaoVendas.equals("S") ? true : false));

				Long usu_codccuPostoExistente = NeoUtils.safeLong(NeoUtils.safeOutputString(wPostoExt.findValue("usu_codccu")));
				Long codEmp = NeoUtils.safeLong(NeoUtils.safeOutputString(wPostoExt.findValue("usu_codemp")));

				QLGroupFilter gp = new QLGroupFilter("AND");
				gp.addFilter(new QLEqualsFilter("codccu", NeoUtils.safeOutputString(usu_codccuPostoExistente)));
				gp.addFilter(new QLEqualsFilter("nivccu", 8L));
				gp.addFilter(new QLEqualsFilter("codemp", codEmp));
				Collection<NeoObject> contas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), gp);

				String ccuPostoAntigo = null;
				if (movimentoContrato == 5 && movimentoAlteracaoContrato != 1)
				{ // se
					// for
					// alteração
					// de
					// cnpj
					// não
					// preencher
					// pois
					// vai
					// ter
					// que
					// gerar
					// uma
					// arvore
					// nova
					// de
					// CC.
					for (NeoObject conta : contas)
					{
						EntityWrapper w = new EntityWrapper(conta);
						ccuPostoAntigo = NeoUtils.safeOutputString(w.findValue("codccu"));
						System.out.println("[FLUXO CONTRATOS]- codCcu: " + ccuPostoAntigo + " do posto antigo");
					}
				}
				else
				{
					System.out.println("[FLUXO CONTRATOS]- codCcu: " + usu_codccuPostoExistente + " do posto antigo não será setado pois é uma alteração de CNPJ");
				}
				wNovoPosto.setValue("codccu", ccuPostoAntigo);

				NeoObject oPeriodoFaturamento = AdapterUtils.createNewEntityInstance("FGCPeriodoFaturamento");
				EntityWrapper wPeriodoFaturamento = new EntityWrapper(oPeriodoFaturamento);
				wPeriodoFaturamento.setValue("de", wPostoExt.findValue("usu_datIni"));
				wPeriodoFaturamento.setValue("ate", wPostoExt.findValue("usu_datFim"));
				wNovoPosto.setValue("periodoFaturamento", oPeriodoFaturamento);

				Long numeroPostoExt = (Long) wPostoExt.findField("usu_numpos").getValue();
				System.out.println("[FLUXO CONTRATOS]- grdi - numPostoExt " + numeroPostoExt);

				String codServicoExt = (String) wPostoExt.findField("usu_codser").getValue();
				System.out.println("[FLUXO CONTRATOS]- grdi - codServicoExt " + codServicoExt);

				wNovoPosto.findField("numPosto").setValue(numeroPostoExt);
				wNovoPosto.findField("numContrato").setValue(numContrato);

				// ABA MOVIMENTACAO DE POSTOS
				BigDecimal usu_qtdfun = (BigDecimal) wPostoExt.findField("usu_qtdfun").getValue(); // quantidade
				// funcionario
				System.out.println("[FLUXO CONTRATOS]- grdi - usu_qtdfun " + usu_qtdfun);
				Long usu_prvpos = (Long) wPostoExt.findField("usu_prvpos").getValue(); // quantidade
				// funcionario
				// previsto
				System.out.println("[FLUXO CONTRATOS]- grdi - usu_prvpos " + usu_prvpos);
				BigDecimal usu_qtdcvs = (BigDecimal) wPostoExt.findField("usu_qtdcvs").getValue(); // quantidade
				// faturamento
				System.out.println("[FLUXO CONTRATOS]- grdi - usu_qtdcvs " + usu_qtdcvs);
				BigDecimal usu_preuni = (BigDecimal) wPostoExt.findField("usu_preuni").getValue(); // preco
				// unitario
				// posto
				System.out.println("[FLUXO CONTRATOS]- grdi - usu_preuni " + usu_preuni);
				String usu_periss = wPostoExt.findField("usu_periss").getValueAsString(); // ISS
				// do
				// posto
				System.out.println("[FLUXO CONTRATOS]- grdi - usu_periss " + usu_periss);

				NeoObject complementoServico = retornaComplementoServicoCodSer(codEmpresa, codServicoExt);
				if (complementoServico != null)
					wNovoPosto.findField("complementoServico").setValue(complementoServico);

				Long usu_ctared = 33625L;
				if (codEmpresa.compareTo(19L) > 0)
				{
					usu_ctared = (Long) wPostoExt.findValue("usu_regcvs");
				}
				NeoObject oContaContabil = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), new QLEqualsFilter("ctared", usu_ctared)));
				if (oContaContabil != null)
					wNovoPosto.setValue("contaContabil", oContaContabil);

				Long usu_ctafin = (Long) wPostoExt.findValue("usu_ctafin");
				NeoObject oContaFinanceira = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), new QLEqualsFilter("ctafin", usu_ctafin)));
				if (oContaFinanceira != null)
					wNovoPosto.setValue("contaFinanceira", oContaFinanceira);

				Long usu_regcvs = (Long) wPostoExt.findField("usu_regcvs").getValue();
				QLGroupFilter keyReg = new QLGroupFilter("AND");
				keyReg.addFilter(new QLEqualsFilter("usu_codreg", usu_regcvs));
				NeoObject oRegional = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUREG"), keyReg));

				if (oRegional != null)
				{
					wNovoPosto.findField("regionalPosto").setValue(oRegional);
				}

				wNovoPosto.findField("qtdeDeFuncionarios").setValue(usu_qtdfun == null ? 0L : usu_qtdfun.longValue());

				wNovoPosto.findField("qtdeDeFuncionariosPrevistos").setValue(usu_prvpos == null ? 0L : usu_prvpos);

				wNovoPosto.findField("qtdeParaFaturamento").setValue(usu_qtdcvs == null ? 1L : usu_qtdcvs);

				if (usu_preuni != null)
				{
					wNovoPosto.findField("precoUnitarioPosto").setValue(usu_preuni);
					wNovoPosto.findField("precoUnitarioPostoConferencia").setValue(usu_preuni);

					if (usu_qtdcvs.equals(0))
					{
						usu_qtdcvs = new BigDecimal(1);
					}
					BigDecimal totalPosto = usu_qtdcvs.multiply(usu_preuni);

					wNovoPosto.findField("valorTotalPosto").setValue(totalPosto);
				}

				if (usu_periss != null)
				{
					wNovoPosto.findField("valorISS").setValue(usu_periss);
				}

				if (wrapper.findField("dadosGeraisContrato.representante") != null)
					wNovoPosto.findField("representante").setValue(wrapper.findField("dadosGeraisContrato.representante").getValue());

				String usu_codccu = (String) wPostoExt.findField("usu_codccu").getValue();

				// SAPIENSE044CCU
				NeoObject oCentroCusto = retornaCentroCusto(codEmpresa, usu_codccu);
				if (oCentroCusto != null)
					wNovoPosto.findField("centroCusto").setValue(oCentroCusto);

				// ENDEREÇO
				geraEndereco(wPostoExt, wNovoPosto, "enderecoPosto", false);
				geraEndereco(wPostoExt, wNovoPosto, "enderecoEfetivo", true);

				// Formacao Preco
				BigDecimal usu_vlrmta = (BigDecimal) wPostoExt.findField("usu_vlrmta").getValue(); // valor
				// montante
				BigDecimal usu_vlrmtb = (BigDecimal) wPostoExt.findField("usu_vlrmtb").getValue(); // valor
				// montante
				System.out.println("[FLUXO CONTRATOS]- grdi - usu_vlrmtb " + usu_vlrmtb);
				BigDecimal usu_vlrva4 = (BigDecimal) wPostoExt.findField("usu_vlrva4").getValue();
				BigDecimal usu_vlrvtr = (BigDecimal) wPostoExt.findField("usu_vlrvtr").getValue(); // vale
				// transporte
				BigDecimal usu_vlrtri = (BigDecimal) wPostoExt.findField("usu_vlrtri").getValue(); // valor
				// tributos
				BigDecimal usu_vlrva6 = (BigDecimal) wPostoExt.findField("usu_vlrva6").getValue();
				BigDecimal usu_vlrva8 = (BigDecimal) wPostoExt.findField("usu_vlrva8").getValue();
				BigDecimal usu_vlrvre = (BigDecimal) wPostoExt.findField("usu_vlrvre").getValue(); // vale
				// refeicao
				BigDecimal usu_vlrvav = (BigDecimal) wPostoExt.findField("usu_vlrvav").getValue(); // vigilante

				NeoObject oFormacaoPrecoPosto = AdapterUtils.createNewEntityInstance("FGCFormacaoPreco");

				if (oFormacaoPrecoPosto != null)
				{
					EntityWrapper wFormacaoPrecoPosto = new EntityWrapper(oFormacaoPrecoPosto);

					wFormacaoPrecoPosto.setValue("valorMontanteA", usu_vlrmta == null ? 0L : usu_vlrmta);
					wFormacaoPrecoPosto.setValue("valorMontanteB", usu_vlrmtb == null ? 0L : usu_vlrmtb);
					wFormacaoPrecoPosto.setValue("valeAlimentacao4", usu_vlrva4 == null ? 0L : usu_vlrva4);
					wFormacaoPrecoPosto.setValue("valeTransporte", usu_vlrvtr == null ? 0L : usu_vlrvtr);
					wFormacaoPrecoPosto.setValue("valorTributos", usu_vlrtri == null ? 0L : usu_vlrtri);
					wFormacaoPrecoPosto.setValue("valeAlimentacao6", usu_vlrva6 == null ? 0L : usu_vlrva6);
					wFormacaoPrecoPosto.setValue("valeAlimentacao8", usu_vlrva8 == null ? 0L : usu_vlrva8);
					wFormacaoPrecoPosto.setValue("valeRefeicao", usu_vlrvre == null ? 0L : usu_vlrvre);
					wFormacaoPrecoPosto.setValue("vigilante", usu_vlrvav == null ? 0L : usu_vlrvav);

					// Seta no eform formacao preco no posto
					PersistEngine.persist(oFormacaoPrecoPosto);
					wNovoPosto.findField("formacaoPreco").setValue(oFormacaoPrecoPosto);
				}

				// SITUAÇÃO Posto
				String usu_sitcvs = (String) wPostoExt.findField("usu_sitcvs").getValue(); // situação
				// posto

				if (usu_sitcvs != null)
				{
					wNovoPosto.findField("situacaoPosto").setValue(retornaSituacao(usu_sitcvs));
				}

				// ABA EQUIPAMENTOS E ITENS - NÃO PRECISA MAIS TRAZER O
				// HISTÓRICO DOS EQUIPAMENTOS/KITS/CHECKLISTS
				NeoObject oNovoEquipamentoItens = AdapterUtils.createNewEntityInstance("FGCEquipamentosItens");
				EntityWrapper wNovoEquipamentoItens = new EntityWrapper(oNovoEquipamentoItens);

				// kits
				/*
				 * List<NeoObject> listaNovoKitPosto =
				 * geraKitsPosto(wNovoEquipamentoItens, codEmpresa, codFil,
				 * numContrato, numeroPostoExt, codServicoExt);
				 * if(!listaNovoKitPosto.isEmpty()) { //Seta no eform pai o
				 * posto wNovoEquipamentoItens.findField("kitsPosto").setValue(
				 * listaNovoKitPosto); }
				 * //Equipamentos List<NeoObject> listaNovoEquip = null;
				 * listaNovoEquip = geraEquipamentosPosto(wNovoEquipamentoItens,
				 * codEmpresa, codFil, numContrato, numeroPostoExt,
				 * codServicoExt);
				 * if(!listaNovoEquip.isEmpty()) { //Seta no eform pai o posto
				 * wNovoEquipamentoItens
				 * .findField("equipamentosPosto").setValue(listaNovoEquip); }
				 */

				// Checklist
				List<NeoObject> listaNovoCheckl = geraChecklistPosto(wNovoEquipamentoItens, codEmpresa, codFil, numContrato, codServicoExt, numeroPostoExt);
				if (!listaNovoCheckl.isEmpty())
				{
					// Seta no eform pai o posto
					wNovoEquipamentoItens.findField("itensChecklist").setValue(listaNovoCheckl);
				}

				// Seta no eform posto o equipamento
				PersistEngine.persist(oNovoEquipamentoItens);
				wNovoPosto.findField("equipamentosItens").setValue(oNovoEquipamentoItens);

				// Vigilancia eletronica
				NeoObject oVigilanciaEletronica = AdapterUtils.createNewEntityInstance("FGCVigilanciaEletronica");
				if (oVigilanciaEletronica != null)
				{
					EntityWrapper wVigilanciaEletronica = new EntityWrapper(oVigilanciaEletronica);

					// USU_T160CVS – CAMPO usu_vlrseg
					BigDecimal usu_vlrseg = (BigDecimal) wPostoExt.findField("usu_vlrseg").getValue();
					if (usu_vlrseg != null)
					{
						wVigilanciaEletronica.findField("valorSeguro").setValue(usu_vlrseg);
					}

					//
					/*
					 * usu_preuni = (BigDecimal)
					 * wPostoExt.findField("usu_preuni").getValue();
					 * solicitado que o valor da instalação para postos vindos
					 * do sapiens venha zerado
					 */
					usu_preuni = new BigDecimal(0);
					if (usu_preuni != null)
					{
						wVigilanciaEletronica.findField("valorInstalacao").setValue(usu_preuni);
					}

					/*
					 * VERIFICAR ONDE GRAVAR AS INFORMACOES ABAIXO E SE
					 * REALMENTE SERA UTILIZADO. VEM DA ABA
					 * MONITORAMENTO(vigilancia eletronica)
					 * referencia(java.lang.String) =
					 * cepReferencia(java.lang.String) =
					 * telefoneRef(java.lang.String) =
					 * parcelas(com.neomind.fusion.entity.dyn.FGCMaxParcelas) =
					 * observacao(java.lang.String) =
					 * responsavel(java.lang.String) =
					 * cpfResponsavel(java.lang.String) =
					 */

					// lista contato posto
					// SAPIENST160CON [SAPIENS] T160CON - Contato postos
					List<NeoObject> novaListaContatoVigilante = geraListaContatoVigilantePosto(codEmpresa, codFil, numContrato, numeroPostoExt, codServicoExt);

					wVigilanciaEletronica.findField("listaContatosPosto").setValue(novaListaContatoVigilante);

					PersistEngine.persist(oVigilanciaEletronica);
					wNovoPosto.findField("vigilanciaEletronica").setValue(oVigilanciaEletronica);
				}

				wNovoPosto.findField("origemSapiens").setValue(true);

				PersistEngine.persist(oNovoPosto);
				postos.add(oNovoPosto);
			}

		}
		return postos;
	}

	private static NeoObject retornaComplementoServicoCodSer(Long codEmpresa, String codServicoExt)
	{
		NeoObject centrocusto = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSESER"), new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmpresa), new QLEqualsFilter("codser", codServicoExt))));

		return centrocusto;
	}

	private static NeoObject retornaCentroCusto(Long codEmpresa, String codccu)
	{
		List<NeoObject> centrocusto = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmpresa), new QLEqualsFilter("codccu", codccu)));

		if (centrocusto.size() > 0)
			return centrocusto.get(0);
		else
			return null;
	}

	private static List<NeoObject> geraListaContatoVigilantePosto(Long codEmpresa, Long codFil, Long numContrato, Long numeroPostoExt, String codServicoExt)
	{
		Long key = new GregorianCalendar().getTimeInMillis();
		ContratoLogUtils.logInfo("[" + key + "] - Buscando Contatos do contrato: " + numContrato + ", posto:" + numeroPostoExt);

		List<NeoObject> listaContatosPosto = new ArrayList<NeoObject>();

		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("usu_codemp", codEmpresa));
		gp.addFilter(new QLEqualsFilter("usu_codfil", codFil));
		gp.addFilter(new QLEqualsFilter("usu_numctr", numContrato));
		// gp.addFilter(new QLEqualsFilter("usu_codser", codServicoExt));
		gp.addFilter(new QLEqualsFilter("usu_numpos", numeroPostoExt));

		List<NeoObject> contatosVigilanteExt = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENST160CON"), gp);

		if (contatosVigilanteExt != null && !contatosVigilanteExt.isEmpty())
		{
			ContratoLogUtils.logInfo("[" + key + "] - Encontrados " + contatosVigilanteExt.size() + " contatos no contrato:" + numContrato + ", posto:" + numeroPostoExt);
			for (NeoObject contatoVigiaExt : contatosVigilanteExt)
			{
				EntityWrapper wContatoExt = new EntityWrapper(contatoVigiaExt);

				String usu_nomcon = (String) wContatoExt.getField("usu_nomcon").getValue();
				String usu_foncon1 = (String) wContatoExt.getField("usu_foncon1").getValue();
				String usu_foncon2 = (String) wContatoExt.getField("usu_foncon2").getValue();
				String usu_funcon = (String) wContatoExt.getField("usu_funcon").getValue();
				Long usu_seqcon = (Long) wContatoExt.getField("usu_seqcon").getValue();

				NeoObject oContato = AdapterUtils.createNewEntityInstance("FGCContato");
				EntityWrapper wContato = new EntityWrapper(oContato);

				wContato.findField("nomecontato").setValue(usu_nomcon);
				wContato.findField("telefone1").setValue(usu_foncon1);
				wContato.findField("telefone2").setValue(usu_foncon2);
				wContato.findField("funcao").setValue(usu_funcon);
				wContato.findField("seqcon").setValue(usu_seqcon);

				listaContatosPosto.add(oContato);
			}
		}
		else
		{
			ContratoLogUtils.logInfo("[" + key + "] - Não foram encontrados contatos no contrato:" + numContrato + ", posto:" + numeroPostoExt);
		}

		return listaContatosPosto;
	}

	private static void geraEndereco(EntityWrapper wPostoExt, EntityWrapper wNovoPosto, String eformEndereco, boolean efetivo)
	{
		Long usu_cepctr = (Long) wPostoExt.findField("usu_cepctr").getValue();
		String usu_endctr = (String) wPostoExt.findField("usu_endctr").getValue();
		String numero = ContratoUtils.getNumeroFromEndereco(usu_endctr);
		String usu_cplctr = (String) wPostoExt.findField("usu_cplctr").getValue(); // complemento
		if (numero.equals(""))
			numero = usu_cplctr;

		String usu_baictr = (String) wPostoExt.findField("usu_baictr").getValue();
		String usu_ufsctr = (String) wPostoExt.findField("usu_ufsctr").getValue();
		String usu_cidctr = (String) wPostoExt.findField("usu_cidctr").getValue();

		if (efetivo)
		{
			usu_cepctr = (Long) wPostoExt.findField("usu_cepeft").getValue();
			usu_endctr = (String) wPostoExt.findField("usu_endeft").getValue();
			usu_cplctr = (String) wPostoExt.findField("usu_cpleft").getValue(); // complemento
			usu_baictr = (String) wPostoExt.findField("usu_baieft").getValue();
			usu_ufsctr = (String) wPostoExt.findField("usu_ufseft").getValue();
			usu_cidctr = (String) wPostoExt.findField("usu_cideft").getValue();
		}

		NeoObject oEnderecoPosto = AdapterUtils.createNewEntityInstance("FGCEndereco");
		EntityWrapper wEnderecoPosto = new EntityWrapper(oEnderecoPosto);

		if (usu_cepctr != null && usu_cepctr != 0)
			wEnderecoPosto.findField("cep").setValue(NeoUtils.safeOutputString(usu_cepctr));
		if (usu_endctr != null && usu_endctr != "")
			wEnderecoPosto.findField("endereco").setValue(NeoUtils.safeOutputString(usu_endctr));
		if (numero != null && numero != "")
			wEnderecoPosto.findField("numero").setValue(NeoUtils.safeOutputString(numero));
		if (usu_cplctr != null && usu_cplctr != "")
			wEnderecoPosto.findField("complemento").setValue(NeoUtils.safeOutputString(usu_cplctr));
		if (usu_baictr != null && usu_baictr != "")
			wEnderecoPosto.findField("bairro").setValue(NeoUtils.safeOutputString(usu_baictr));

		// ESTADO CIDADE
		NeoObject oEstadoCidade = AdapterUtils.createNewEntityInstance("FGCEstadoCidade");
		if (oEstadoCidade != null)
		{
			EntityWrapper wEstadoCidade = new EntityWrapper(oEstadoCidade);

			// uf
			NeoObject estado = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE007UFS"), new QLEqualsFilter("sigufs", usu_ufsctr)));
			if (estado != null)
				wEstadoCidade.findField("estado").setValue(estado);

			// cidade
			NeoObject cidade = retornaCidadeSapiens8Cep(usu_ufsctr, usu_cidctr, usu_cepctr);
			if (cidade != null)
				wEstadoCidade.findField("cidade").setValue(cidade);

			// Seta no eform estadocidade no endereco
			PersistEngine.persist(oEstadoCidade);
			wEnderecoPosto.findField("estadoCidade").setValue(oEstadoCidade);
		}

		// Seta no eform endereco no posto
		PersistEngine.persist(oEnderecoPosto);
		wNovoPosto.findField(eformEndereco).setValue(oEnderecoPosto); // enderecoPosto
		// ou
		// enderecoEfetivo

	}

	/**
	 * SAPIENSE008CEP
	 * 
	 * @param uf
	 * @param cidade
	 * @return NeoObject SAPIENSE008CEP
	 */
	private static NeoObject retornaCidadeSapiens8Cep(String uf, String cidade, Long cep)
	{
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("sigufs", uf));
		gp.addFilter(new QLEqualsFilter("nomcid", cidade));
		Collection<NeoObject> objCidade = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE008CEP"), gp);
		NeoObject cidadeAlvo = null;
		for (NeoObject obj : objCidade)
		{
			EntityWrapper w = new EntityWrapper(obj);
			String cepini = NeoUtils.safeOutputString(w.findValue("cepini"));
			String cepfim = NeoUtils.safeOutputString(w.findValue("cepfim"));

			if ((cep >= NeoUtils.safeLong(cepini)) && cep <= NeoUtils.safeLong(cepfim))
			{
				cidadeAlvo = obj;
			}
		}
		return cidadeAlvo;
	}

	private static List<NeoObject> geraChecklistPosto(EntityWrapper wNovoEquipamentoItens, Long codEmpresa, Long codFil, Long numContrato, String codServicoExt, Long numeroPostoExt)
	{
		List<NeoObject> listaNovoCheckl = new ArrayList<NeoObject>();
		// SAPIENSUSUCHK
		List<NeoObject> listaCheckExt = retornaListaChecklist(codEmpresa, codFil, numContrato, numeroPostoExt, codServicoExt);

		for (NeoObject oChecklistExt : listaCheckExt)
		{
			EntityWrapper wCheckExt = new EntityWrapper(oChecklistExt);

			Long usu_qtdchk = (Long) wCheckExt.findField("usu_qtdchk").getValue();
			GregorianCalendar usu_datchk = (GregorianCalendar) wCheckExt.findField("usu_datchk").getValue();
			Long usu_seqchk = (Long) wCheckExt.findField("usu_seqchk").getValue();
			Long usu_coditm = null;
			if (wCheckExt.findValue("usu_coditm") != null)
				usu_coditm = (Long) wCheckExt.findValue("usu_coditm");

			String usu_rmcchk = "";
			if (NeoUtils.safeOutputString(wCheckExt.findValue("usu_rmcchk")) != null && !NeoUtils.safeOutputString(wCheckExt.findValue("usu_rmcchk")).equals(""))
				usu_rmcchk = NeoUtils.safeOutputString(wCheckExt.findValue("usu_rmcchk"));

			if (usu_qtdchk != null)
			{
				NeoObject oNovoChecklistPosto = AdapterUtils.createNewEntityInstance("FGCListaChecklist");
				EntityWrapper wNovoChecklistPosto = new EntityWrapper(oNovoChecklistPosto);

				// wNovoChecklistPosto.findField("itemChecklist").setValue(oChecklistExt);

				// wNovoChecklistPosto.findField("quantidade").setValue(usu_qtdchk);

				if (usu_coditm != null)
				{
					List<NeoObject> objItm = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUITM"), new QLEqualsFilter("usu_coditm", usu_coditm));
					if (objItm.size() > 0)
					{
						NeoObject itemCheckList = objItm.get(0);
						if (itemCheckList != null)
						{
							EntityWrapper wItemCheckList = new EntityWrapper(itemCheckList);
							wNovoChecklistPosto.findField("itemChecklist").setValue(itemCheckList);
							wNovoChecklistPosto.findField("descricao").setValue(NeoUtils.safeOutputString(wItemCheckList.findValue("usu_desitm")));
						}
					}
				}
				else
				{
					wNovoChecklistPosto.setValue("descricao", NeoUtils.safeOutputString(wCheckExt.findValue("usu_itmchk")));
				}

				if (usu_rmcchk != null && !usu_rmcchk.equals(""))
				{
					wNovoChecklistPosto.setValue("possuiRMC", true);
				}

				if (usu_datchk != null)
					wNovoChecklistPosto.findField("prazo").setValue(usu_datchk);

				wNovoChecklistPosto.findField("seqchk").setValue(usu_seqchk);

				PersistEngine.persist(oNovoChecklistPosto);
				listaNovoCheckl.add(oNovoChecklistPosto);
			}
		}
		return listaNovoCheckl;
	}

	private static List<NeoObject> retornaListaChecklist(Long codEmpresa, Long codFil, Long numContrato, Long numeroPostoExt, String codServicoExt)
	{
		List<NeoObject> checklistExt = new ArrayList<NeoObject>();
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("usu_codemp", codEmpresa));
		gp.addFilter(new QLEqualsFilter("usu_codfil", codFil));
		gp.addFilter(new QLEqualsFilter("usu_numctr", numContrato));
		gp.addFilter(new QLEqualsFilter("usu_codser", codServicoExt));
		gp.addFilter(new QLEqualsFilter("usu_numpos", numeroPostoExt));
		checklistExt = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUT160CHkNovo"), gp);

		return checklistExt;
	}

	private static List<NeoObject> geraEquipamentosPosto(EntityWrapper wNovoEquipamentoItens, Long codEmpresa, Long codFil, Long numContrato, Long numeroPostoExt, String codServicoExt)
	{
		// SAPIENSUSUTEQP
		List<NeoObject> novaListaEquip = new ArrayList<NeoObject>();
		List<NeoObject> listaEquipExt = retornaListaEquipamento(codEmpresa, codFil, numContrato, numeroPostoExt, codServicoExt);
		int i = 1;
		for (NeoObject oEquiExt : listaEquipExt)
		{
			EntityWrapper wEquipExt = new EntityWrapper(oEquiExt);
			String usu_codpro = (String) wEquipExt.findField("usu_codpro").getValue();
			Long usu_qtdeqp = (Long) wEquipExt.findField("usu_qtdeqp").getValue();
			Long usu_codins = (Long) wEquipExt.findField("usu_codins").getValue();
			String usu_tipins = (String) wEquipExt.findField("usu_tipins").getValue();
			Long usu_emppro = (Long) wEquipExt.findField("usu_emppro").getValue();

			if (usu_codpro != null)
			{
				NeoObject oNovoEquipamentoPosto = AdapterUtils.createNewEntityInstance("FGCListaEquipamentosPosto");
				EntityWrapper wNovoEquipPosto = new EntityWrapper(oNovoEquipamentoPosto);
				wNovoEquipPosto.setValue("sequencia", new Long(i++));
				// wNovoEquipPosto.setValue("equipamento",oEquiExt);
				// wNovoEquipPosto.setValue("equipamento",oEquiExt);

				// SAPIENSEPRO
				// se tiver produto no sapiens, mas nao tiver na empresa do
				// cliente o produto deve ser cadastrado
				NeoObject oProdutoExt = retornaProduto(codEmpresa, usu_codpro);

				if (oProdutoExt != null)
					wNovoEquipPosto.findField("produto").setValue(oProdutoExt);

				if (usu_qtdeqp != null)
					wNovoEquipPosto.findField("quantidade").setValue(usu_qtdeqp);

				if (usu_codins != null)
				{
					/*
					 * wNovoEquipPosto.findField("codigoInstalador").setValue(
					 * NeoUtils.safeOutputString(usu_codins));
					 * wNovoEquipPosto.findField
					 * ("instalador").setValue(NeoUtils.
					 * safeOutputString(usu_codins));
					 */
					// SAPIENSUSUT190UNS
					NeoObject oInstalador = retornaInstalador(usu_codins);
					if (oInstalador != null)
						wNovoEquipPosto.findField("instaladores").setValue(oInstalador);
				}

				// TODO criar flag origemSapiens true
				wNovoEquipPosto.findField("origemSapiens").setValue(true);

				PersistEngine.persist(oNovoEquipamentoPosto);
				novaListaEquip.add(oNovoEquipamentoPosto);
			}
		}
		return novaListaEquip;
	}

	private static NeoObject retornaInstalador(Long usu_codins)
	{
		NeoObject instalador = null;
		try
		{
			instalador = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTINS"), new QLEqualsFilter("usu_codins", usu_codins)));
		}
		catch (Exception e)
		{
			instalador = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUT190UNS"), new QLEqualsFilter("usu_codins", usu_codins)));
		}

		return instalador;
	}

	private static NeoObject retornaSituacao(String situacao)
	{
		NeoObject oSituacao = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCSituacaoPosto"), new QLEqualsFilter("situacao", situacao)));

		return oSituacao;
	}

	private static NeoObject retornaProduto(Long codEmpresa, String usu_codpro)
	{
		NeoObject produtoSapiens = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEPRO"), new QLGroupFilter("AND", new QLEqualsFilter("codemp", String.valueOf(codEmpresa)), new QLEqualsFilter("codpro", usu_codpro))));

		return produtoSapiens;
	}

	private static NeoObject retornaProdutoEletronica(Long codEmpresa, String usu_codpro)
	{
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLRawFilter("codemp = " + codEmpresa + " and codpro = " + usu_codpro + " and codfam like 'ELE%'"));

		NeoObject produtoSapiens = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEPRO"), gp));

		return produtoSapiens;
	}

	private static List<NeoObject> retornaListaEquipamento(Long codEmpresa, Long codFil, Long numContrato, Long numeroPostoExt, String codServicoExt)
	{
		@SuppressWarnings("unchecked")
		List<NeoObject> equipamentoExt = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTEQP"), new QLGroupFilter("AND", new QLEqualsFilter("usu_codemp", codEmpresa), new QLEqualsFilter("usu_codfil", codFil), new QLEqualsFilter("usu_numctr", numContrato), new QLEqualsFilter("usu_codser", codServicoExt), new QLEqualsFilter("usu_numpos", numeroPostoExt)));

		return equipamentoExt;
	}

	private static List<NeoObject> geraKitsPosto(EntityWrapper wNovoEquipamentoItens, Long codEmpresa, Long codFil, Long numContrato, Long numeroPostoExt, String codServicoExt)
	{
		List<NeoObject> listaNovoKitPosto = new ArrayList<NeoObject>();
		// SAPIENSUSUT160KPO
		List<NeoObject> listaKitsExt = retornaListaKits(codEmpresa, codFil, numContrato, codServicoExt, numeroPostoExt);

		for (NeoObject kitExt : listaKitsExt)
		{
			EntityWrapper wKitEx = new EntityWrapper(kitExt);

			Long usu_codkit = (Long) wKitEx.findField("usu_codkit").getValue();
			Long usu_qtdkit = (Long) wKitEx.findField("usu_qtdkit").getValue();
			Long usu_codins = (Long) wKitEx.findField("usu_codins").getValue();
			String usu_tipins = (String) wKitEx.findField("usu_tipins").getValue();
			Long seqKit = (Long) wKitEx.findField("usu_seqkit").getValue();

			if (usu_codkit != null)
			{
				NeoObject oNovoKitPosto = AdapterUtils.createNewEntityInstance("FGCListaCombos");
				EntityWrapper wNovoKitPosto = new EntityWrapper(oNovoKitPosto);

				// SAPIENSUSUTKCA
				NeoObject oItemKit = retornaItemKitPosto(usu_codkit);

				if (oItemKit != null)
					wNovoKitPosto.findField("kit").setValue(oItemKit);

				if (usu_qtdkit != null)
					wNovoKitPosto.findField("quantidade").setValue(usu_qtdkit);

				if (usu_codins != null)
				{
					NeoObject oInstalador = retornaInstalador(usu_codins);
					if (oInstalador != null)
						wNovoKitPosto.findField("instaladores").setValue(oInstalador);
				}

				wNovoKitPosto.findField("seqKit").setValue(seqKit);

				// TODO criar flag origemSapiens true
				wNovoKitPosto.findField("origemSapiens").setValue(true);

				PersistEngine.persist(oNovoKitPosto);
				listaNovoKitPosto.add(oNovoKitPosto);
			}
		}
		return listaNovoKitPosto;
	}

	private static List<NeoObject> retornaListaKits(Long codEmpresa, Long codFil, Long numContrato, String codServicoExt, Long numeroPostoExt)
	{
		@SuppressWarnings("unchecked")
		List<NeoObject> kitsExt = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUT160KPO"), new QLGroupFilter("AND", new QLEqualsFilter("usu_codemp", codEmpresa), new QLEqualsFilter("usu_codfil", codFil), new QLEqualsFilter("usu_numctr", numContrato), new QLEqualsFilter("usu_codser", codServicoExt), new QLEqualsFilter("usu_numpos", numeroPostoExt)));

		return kitsExt;
	}

	private static NeoObject retornaItemKitPosto(Long codKit)
	{
		NeoObject itemKit = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTKCA"), new QLEqualsFilter("usu_codkit", codKit)));

		return itemKit;
	}

	public static List<NeoObject> retornaListaPostos(Long codEmpresa, Long codFil, Long numContrato)
	{
		@SuppressWarnings("unchecked")
		List<NeoObject> postoExt = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTCVS"), new QLGroupFilter("AND", new QLEqualsFilter("usu_sitcvs", "A"), new QLEqualsFilter("usu_codemp", codEmpresa), new QLEqualsFilter("usu_codfil", codFil), new QLEqualsFilter("usu_numctr", numContrato)));

		return postoExt;
	}

	private static NeoObject retornaServicoContrato(Long codTipoServicoContrato)
	{
		NeoObject tipoServicoContrato = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENST160SCT"), new QLEqualsFilter("usu_serctr", codTipoServicoContrato)));

		return tipoServicoContrato;
	}

	private static NeoObject retornaTipoContrato(Long codTipoDoContrato)
	{
		NeoObject tipoContrato = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("CETipoContrato"), new QLEqualsFilter("codigoProduto", codTipoDoContrato)));

		return tipoContrato;
	}

	private static NeoObject retornaFormaPagamento(Long codEmpresa, Long codFormaPagamento)
	{
		NeoObject indiceReajuste = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE066FPG"), new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmpresa), new QLEqualsFilter("codfpg", codFormaPagamento))));

		return indiceReajuste;
	}

	private static NeoObject retornaCondicaoPagamento(Long codEmpresa, String codCondicaoPagamento)
	{
		NeoObject indiceReajuste = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("CondicaoPagamento"), new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmpresa), new QLEqualsFilter("codcpg", codCondicaoPagamento))));

		return indiceReajuste;
	}

	private static NeoObject retornaIndice(String codIndice)
	{
		NeoObject indiceReajuste = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE031MOE"), new QLEqualsFilter("codmoe", codIndice)));

		return indiceReajuste;
	}

	private static NeoObject retornaTransacao(Long codEmpresa, String codTransacao)
	{
		NeoObject transacao = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE001TNS"), new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmpresa), new QLEqualsFilter("codtns", codTransacao))));

		return transacao;
	}

	private static NeoObject retornaRepresentante(Long codRepresentante)
	{
		NeoObject representante = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEREP"), new QLEqualsFilter("codrep", codRepresentante)));

		return representante;
	}

	private static NeoObject retornaRegional(Long codRegional)
	{
		NeoObject regional = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUREG"), new QLEqualsFilter("usu_codreg", codRegional)));

		return regional;
	}

}