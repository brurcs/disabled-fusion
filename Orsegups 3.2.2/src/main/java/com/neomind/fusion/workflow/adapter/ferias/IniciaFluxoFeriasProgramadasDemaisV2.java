package com.neomind.fusion.workflow.adapter.ferias;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class IniciaFluxoFeriasProgramadasDemaisV2 implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection(""); 
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		try
		{
			
			sql.append("     Select fun.numemp, ");
			sql.append("            fun.numcad, ");
			sql.append("            fun.nomfun, ");
			sql.append("            fun.numcpf, ");
			sql.append("            reg.USU_CodReg, ");
			sql.append("            cid2.estcid, ");
			sql.append("            reg.USU_NomReg, ");
			sql.append("            cob.usu_datalt, ");
			sql.append("            cob.usu_horini, ");
			sql.append("            cob.usu_datfim, ");
			sql.append("            car.TitRed, ");
			sql.append("            cob.usu_numempcob, ");
			sql.append("            cob.usu_numcadcob, ");
			sql.append("            fis.USU_NomFis, ");
			sql.append("            cpl.endrua, ");
			sql.append("            cpl.endcpl, ");
			sql.append("            cpl.endnum, ");
			sql.append("            bai.NomBai, ");
			sql.append("            cid.NomCid, ");
			sql.append("            orn.usu_codccu, ");
			sql.append("            orn.usu_lotorn, ");
			sql.append("            orn.numLoc ");
			sql.append("       From [FSOODB04\\SQL02].Vetorh.dbo.USU_T038COBFUN cob ");
			sql.append(" Inner Join [FSOODB04\\SQL02].Vetorh.dbo.R034FUN fun On fun.numemp = cob.usu_numemp ");
			sql.append("												   And fun.tipcol = cob.usu_tipcol ");
			sql.append("												   And fun.numcad = cob.usu_numcad ");
			sql.append(" Inner Join [FSOODB04\\SQL02].Vetorh.dbo.R038HLO hlo On hlo.NumEmp = fun.numemp ");
			sql.append("												   And hlo.tipcol = fun.tipcol ");
			sql.append("												   And hlo.numcad = fun.numcad ");
			sql.append("												   And hlo.DatAlt = (Select Max(hlo2.datalt) ");
			sql.append("																	   From [FSOODB04\\SQL02].Vetorh.dbo.R038HLO hlo2 ");
			sql.append("																	  Where hlo2.numemp = hlo.numemp ");
			sql.append("																		And hlo2.tipcol = hlo.tipcol ");
			sql.append("																		And hlo2.numcad = hlo.numcad ");
			sql.append("																		And hlo2.DatAlt <= cob.usu_datfim) ");
			sql.append(" Inner Join [FSOODB04\\SQL02].Vetorh.dbo.R016ORN orn On orn.taborg = hlo.TabOrg ");
			sql.append("					   							   And orn.NumLoc = hlo.NumLoc ");
			sql.append(" Inner join [FSOODB04\\SQL02].Vetorh.dbo.USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg ");
			sql.append(" Inner Join [FSOODB04\\SQL02].Vetorh.dbo.R038HCA hca On hca.NumEmp = fun.numemp ");
			sql.append("          			                               And hca.tipcol = fun.tipcol ");
			sql.append("                                                   And hca.numcad = fun.numcad ");
			sql.append("												   And hca.DatAlt = (Select Max(hca2.datalt) ");
			sql.append("																	   From [FSOODB04\\SQL02].Vetorh.dbo.R038HCA hca2 ");
			sql.append("                          											  Where hca2.numemp = hca.numemp ");
			sql.append("																		And hca2.tipcol = hca.tipcol ");
			sql.append("																		And hca2.numcad = hca.numcad ");
			sql.append("																		And hca2.DatAlt <= cob.usu_datfim) ");
			sql.append(" Inner Join [FSOODB04\\SQL02].Vetorh.dbo.R024CAR car On car.CodCar = fun.codcar ");
			sql.append("												   And car.EstCar = fun.estcar ");
			sql.append(" Inner JOIN [FSOODB04\\SQL02].Vetorh.dbo.USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
			sql.append(" Inner join [FSOODB04\\SQL02].Vetorh.dbo.r034cpl cpl on cpl.numcad = fun.numcad ");
			sql.append("												   and cpl.numemp = fun.numemp ");
			sql.append("												   and cpl.tipcol = fun.tipcol ");
			sql.append(" Inner join [FSOODB04\\SQL02].Vetorh.dbo.R074BAI bai on bai.CodBai = cpl.codbai ");
			sql.append("												   and bai.CodCid = cpl.codcid ");
			sql.append(" Inner join [FSOODB04\\SQL02].Vetorh.dbo.R074CID cid on cid.CodCid = cpl.codcid ");
			sql.append(" Inner join [FSOODB04\\SQL02].Vetorh.dbo.R074CID cid2 on cid2.CodCid = orn.usu_codcid ");
			sql.append("      Where cob.usu_codmot = 3 ");
			sql.append("        AND cob.usu_nextid is not null ");
			sql.append("		And cob.usu_datfim >= '2014-10-19' ");
			sql.append("		And reg.USU_CodReg <> 0 ");
			sql.append("		AND fun.sitafa <> 7 ");
			sql.append("		And cob.usu_datfim Between GetDate()-4 And GetDate() + 20 ");
			sql.append("		And (fun.numemp <> cob.usu_numempcob ");
			sql.append("		      Or fun.tipcol <> cob.usu_tipcolcob ");
			sql.append("			  Or fun.numcad <> cob.usu_numcadcob) ");
			sql.append("	    And cid2.estcid <> 'PR' ");
			sql.append("		And not exists (select * ");
			sql.append("					      FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaFeriasProgramadas fp WITH (NOLOCK) ");
			sql.append("						 where fp.numcpf = fun.numcpf ");
			sql.append("						   and cast(floor(cast(fp.datalt as float)) as datetime) = cast(floor(cast(cob.usu_datalt as float)) as datetime) ");
			sql.append("						   and fp.horini = cob.usu_horini)");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{

				InstantiableEntityInfo insFerias = AdapterUtils.getInstantiableEntityInfo("r001V2ProgramacaoFerias");
				NeoObject objFerias = insFerias.createNewInstance();
				EntityWrapper wFerias = new EntityWrapper(objFerias);

				wFerias.setValue("numempFer", rs.getLong("numemp"));
				wFerias.setValue("numcadFer", rs.getLong("numcad"));
				wFerias.setValue("codccuFer", String.valueOf(rs.getLong("usu_codccu")));
				wFerias.setValue("nomfunFer", rs.getString("nomfun"));
				wFerias.setValue("titcarFer", rs.getString("titred"));
				wFerias.setValue("lotacaoFer", rs.getString("usu_lotorn"));
				wFerias.setValue("codregFer", rs.getLong("USU_CodReg"));
				wFerias.setValue("nomregFer", rs.getString("USU_NomReg"));
				Long horini = rs.getLong("usu_horini");

				GregorianCalendar dataInicio = new GregorianCalendar();
				dataInicio.setTime(rs.getDate("usu_datalt"));

				GregorianCalendar dataFim = new GregorianCalendar();
				dataFim.setTime(rs.getDate("usu_datfim"));
				dataFim.add(Calendar.DAY_OF_MONTH, +1);
				GregorianCalendar dataIni = new GregorianCalendar();
				dataIni.setTime(rs.getDate("usu_datalt"));
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				GregorianCalendar dataAtual = new GregorianCalendar();
				
				/* 26/11/2019 */
				GregorianCalendar dataCorteIni = new GregorianCalendar();
				dataCorteIni.set(GregorianCalendar.HOUR_OF_DAY, 23);
				dataCorteIni.set(GregorianCalendar.MINUTE, 59);
				dataCorteIni.set(GregorianCalendar.SECOND, 59);
				dataCorteIni.set(GregorianCalendar.MILLISECOND, 59);
				dataCorteIni.set(GregorianCalendar.DAY_OF_MONTH, 25);
				dataCorteIni.set(GregorianCalendar.MONTH, 10);
				dataCorteIni.set(GregorianCalendar.YEAR, 2019);
				

				GregorianCalendar datFix = new GregorianCalendar();
				datFix.set(GregorianCalendar.HOUR_OF_DAY, 0);
				datFix.set(GregorianCalendar.MINUTE, 0);
				datFix.set(GregorianCalendar.SECOND, 0);
				datFix.set(GregorianCalendar.MILLISECOND, 0);

				/* 16/12/2019 */
				GregorianCalendar dataCorte = new GregorianCalendar();
				dataCorte.set(GregorianCalendar.HOUR_OF_DAY, 23);
				dataCorte.set(GregorianCalendar.MINUTE, 59);
				dataCorte.set(GregorianCalendar.SECOND, 59);
				dataCorte.set(GregorianCalendar.MILLISECOND, 59);
				dataCorte.set(GregorianCalendar.DAY_OF_MONTH, 16);
				dataCorte.set(GregorianCalendar.MONTH, 11);
				dataCorte.set(GregorianCalendar.YEAR, 2019);

				if(datFix.after(dataCorteIni) && datFix.before(dataCorte)){
				    datFix.set(GregorianCalendar.DAY_OF_MONTH, 06);
				    datFix.set(GregorianCalendar.MONTH, 00);
				    datFix.set(GregorianCalendar.YEAR, 2020);
				    wFerias.setValue("dataFimCob", datFix);
				} else {
				    wFerias.setValue("dataFimCob", dataFim);
				}
				
				NeoFile anexo = null;

				System.out.println("R001V2 Demais: emp"+rs.getLong("numemp"));
				if (rs.getLong("numemp") == 1 || rs.getLong("numemp") == 15 || rs.getLong("numemp") == 17 || rs.getLong("numemp") == 18 || rs.getLong("numemp") == 19 || rs.getLong("numemp") == 21 || rs.getLong("numemp") == 27 || rs.getLong("numemp") == 28 || rs.getLong("numemp") == 29)
				{
					System.out.println("R001V2: \\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Vigilancia_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf");
					anexo = OrsegupsUtils.criaNeoFile(new File("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Vigilancia_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf"));
				}
				else if (rs.getLong("numemp") == 2 || rs.getLong("numemp") == 6 || rs.getLong("numemp") == 7 || rs.getLong("numemp") == 8 || rs.getLong("numemp") == 22)
				{
					System.out.println("R001V2: \\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Asseio_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf");
					anexo = OrsegupsUtils.criaNeoFile(new File("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Asseio_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf"));
				}

				if (anexo != null)
				{
					wFerias.setValue("relatorioAvosDeFerias", anexo);
				}
				
				ColaboradorVO colaborador = new ColaboradorVO();
				colaborador.setEscala(new EscalaVO());
				colaborador.setEscala(QLPresencaUtils.getEscalaColaborador(rs.getLong("numcad"), rs.getLong("numemp"), 1L));
				wFerias.setValue("escalaFer", colaborador.getEscala().getDescricao() + "/ T: " + colaborador.getEscala().getCodigoTurma());
				wFerias.setValue("endFer", rs.getString("endrua") + ", Complemento: " + rs.getString("endcpl") + ", Nº: " + rs.getString("endnum"));
				wFerias.setValue("endFer", wFerias.findField("endFer").getValue() + " Bairro: " + rs.getString("NomBai") + " - Cidade: " + rs.getString("NomCid"));
				
				System.out.println("Data: "+NeoDateUtils.safeDateFormat(datFix,"dd/MM/yyyy"));				
				NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "beatriz.malmann"));
				//NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "adm"));
				
				QLOpFilter qlNumCad = new QLOpFilter("numcad", "=", rs.getLong("numcad"));
				QLOpFilter qlNumEmp = new QLOpFilter("numemp", "=", rs.getLong("numemp"));
				QLGroupFilter groupFilterColaborador = new QLGroupFilter("AND");
				groupFilterColaborador.addFilter(qlNumCad);
				groupFilterColaborador.addFilter(qlNumEmp);
				List<NeoObject> objsColaborador = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), groupFilterColaborador);
				for (NeoObject objCol : objsColaborador)
				{
					wFerias.findField("colaboradorCoberturaFerias").setValue(objCol);
					break;
				}
				
				wFerias.setValue("iniciadoProcessoAutomatico", true);
				
				GregorianCalendar datAtu = new GregorianCalendar();
				datAtu.add(GregorianCalendar.DATE, 5);
				datAtu.set(GregorianCalendar.HOUR_OF_DAY, 23);
				datAtu.set(GregorianCalendar.MINUTE, 59);
				datAtu.set(GregorianCalendar.SECOND, 59);
				
				final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "R001V2ProgramacaoDeFerias"));
				String tarefa = null;
				try
				{
					tarefa = OrsegupsWorkflowHelper.iniciaProcesso(pm, objFerias, false, solicitante, true, null );
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}		
				InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaFeriasProgramadas");
				NeoObject noAJ = tarefaAJ.createNewInstance();
				EntityWrapper ajWrapper = new EntityWrapper(noAJ);

				ajWrapper.findField("numcpf").setValue(rs.getLong("NumCpf"));
				ajWrapper.findField("datalt").setValue(dataInicio);
				ajWrapper.findField("horini").setValue(horini);
				ajWrapper.findField("tarefa").setValue(tarefa);
				PersistEngine.persist(noAJ);
				Thread.sleep(1000);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}
}
