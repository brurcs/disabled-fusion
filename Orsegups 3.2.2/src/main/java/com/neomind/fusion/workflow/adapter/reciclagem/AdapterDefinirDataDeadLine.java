package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class AdapterDefinirDataDeadLine implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity){
	    
	    try{
		
		if (processEntity.findValue("deadLineDtAnexarDoc") != null){
//		    processEntity.setValue("deadLineDtAnexarDoc", processEntity.findValue("prazoAnexarDocumentos"));
//		    processEntity.setValue("prazoAnexarDocumentos", null);
		    Boolean aceitaProrrogacao = (Boolean) processEntity.findValue("aprovarProrrogacao");
		    if (aceitaProrrogacao != null && aceitaProrrogacao){
			GregorianCalendar deadLineDtAud = new GregorianCalendar();
			deadLineDtAud.add(GregorianCalendar.DATE, 30);
			deadLineDtAud.add(GregorianCalendar.HOUR_OF_DAY, 23);
			deadLineDtAud.add(GregorianCalendar.MINUTE, 59);
			deadLineDtAud.add(GregorianCalendar.SECOND, 59);
			System.out.println("data deadLineDtAud : " + NeoDateUtils.safeDateFormat(deadLineDtAud, "dd/MM/yyyy"));
			
			processEntity.setValue("deadLineDtAnexarDoc", deadLineDtAud);
			processEntity.setValue("jaSolicitouAjuste", true);
		    }
		}else{
		    GregorianCalendar deadLineDtAud = new GregorianCalendar();
		    deadLineDtAud.add(GregorianCalendar.DATE, 45);
		    deadLineDtAud.add(GregorianCalendar.HOUR_OF_DAY, 23);
		    deadLineDtAud.add(GregorianCalendar.MINUTE, 59);
		    deadLineDtAud.add(GregorianCalendar.SECOND, 59);
		    
		    System.out.println("data deadLineDtAud : " + NeoDateUtils.safeDateFormat(deadLineDtAud, "dd/MM/yyyy"));
		    processEntity.setValue("deadLineDtAnexarDoc", deadLineDtAud);
		}
	    }catch(Exception e){
		System.out.println("Erro na classe AdapterDefinirDataDeadLine do fluxo RCL.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	    
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity){

	}

}
