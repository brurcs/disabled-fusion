package com.neomind.fusion.workflow.adapter.orsegups;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class OrsegupsRecrutamentoDefinePapeis implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		NeoUser solicitante = PortalUtil.getCurrentUser();

		EntityWrapper solicitanteWrapper = new EntityWrapper(solicitante);
		
		Boolean isDiretor;
		Boolean isGerente;

		NeoPaper papelDiretor;
		NeoPaper papelGerente;
		NeoPaper papelEncaminhamento;
		NeoPaper palelRecrutador;
		NeoPaper papelAdmissao;

		
		
		try
		{
			isDiretor = OrsegupsUtils.checkGroupLevel(solicitante, OrsegupsUtils.NIVEL_DIRETORIA);
			isGerente = OrsegupsUtils.checkGroupLevel(solicitante, OrsegupsUtils.NIVEL_GERENCIA);

			if (isDiretor)
			{
				papelDiretor = solicitante.getGroup().getResponsible();
			}
			else
			{
				papelDiretor = OrsegupsUtils.getSuperior(solicitante, OrsegupsUtils.NIVEL_DIRETORIA);
			}

			if (isGerente)
			{
				papelGerente = solicitante.getGroup().getResponsible();
			}
			else
			{
				papelGerente = OrsegupsUtils.getSuperior(solicitante, OrsegupsUtils.NIVEL_GERENCIA);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
