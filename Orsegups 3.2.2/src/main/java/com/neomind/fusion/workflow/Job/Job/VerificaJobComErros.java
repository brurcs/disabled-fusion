package com.neomind.fusion.workflow.Job.Job;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.mail.EmailException;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.JobComErroVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class VerificaJobComErros implements CustomJobAdapter

{

	@Override
	public void execute(CustomJobContext ctx)
	{
		System.out.println("[JOB] - Inicia Verificação de Job's com erro");
		
		try {
			processaJob(ctx);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void processaJob(CustomJobContext ctx) throws SQLException
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		conn = PersistEngine.getConnection("");

		try
		{
			sql.append(" Select ");
			sql.append(" Case ");
			sql.append(" When sch.schedulerType = 1 Then 'Por Hora' ");
			sql.append(" When sch.schedulerType = 2 Then 'Todos os Dias da Semana' ");
			sql.append(" When sch.schedulerType = 3 Then 'Diário' ");
			sql.append(" When sch.schedulerType = 4 Then 'Semanal' ");
			sql.append(" When sch.schedulerType = 5 Then 'Mensal' ");
			sql.append(" When sch.schedulerType = 6 Then 'Anual' ");
			sql.append(" When sch.schedulerType = 7 Then 'Por Minuto' ");
			sql.append(" End As repeticao,log.firedTime As date, ");
			sql.append(" Case ");
			sql.append("	When log.state = 2 Then 'JOB_TOBEEXECUTED' ");
			sql.append("	When log.state = 3 Then 'JOB_EXECUTIONVETOED' ");
			sql.append("	When log.state = 6 Then 'JOB_WASEXECUTED_EXCEPTION' ");
			sql.append("	When log.state = 7 Then 'JOB_EXECUTION_EXCEPTION' ");
			sql.append("	When log.state = 10 Then 'TRIGGER_MISFIRED' ");
			sql.append("End As error, job.name, log.details,log.state,convert(varchar,job.prevFireTime,103) as ultima_execucao,convert(varchar,job.nextFireTime,103) as proxima_execucao ");
			sql.append(" From [cacupe\\sql02].fusion_producao.dbo.Job job With (NOLOCK) ");
			sql.append(" Inner join [cacupe\\sql02].fusion_producao.dbo.JobEventLog log With (NOLOCK) on log.job_neoId = job.neoId ");
			sql.append(" Inner join [cacupe\\sql02].fusion_producao.dbo.NeoTrigger tri With (NOLOCK) on tri.neoId = log.trigger_neoId ");
			sql.append(" Inner join [cacupe\\sql02].fusion_producao.dbo.Scheduler sch With (NOLOCK) on sch.neoid = tri.scheduler_neoId ");
			sql.append(" Where ");
			sql.append(" ( (log.firedTime >= DATEADD(HH,-2,GETDATE()) And log.firedTime < GETDATE() and log.state in (2,6,7,10)) ");
			sql.append("  or ( (job.nextFireTime is null or job.nextFireTime = '1900-01-01 00:00:00.000'))  or (job.nextFireTime < DATEADD(HH,-1,GETDATE()) and tri.nextFireTime < DATEADD(HH,-1,GETDATE())))  and job.name not like 'Abre tarefa simples Job% com problemas' and job.enabled = 1");
			sql.append(" and exists (select 1 from QRTZ_TRIGGERS q where q.JOB_NAME like '%'+cast(job.neoId as varchar) and ( (q.NEXT_FIRE_TIME is null or q.NEXT_FIRE_TIME = -2208981600000) ");
			sql.append(" or (q.NEXT_FIRE_TIME < cast(Datediff(s, '1970-01-01', GETUTCDATE()) AS bigint)*1000 ) )) ");
			sql.append(" Order By log.firedTime Desc, ");
			sql.append(" job.neoid, ");
			sql.append(" log.state ");
			pstm = conn.prepareStatement(sql.toString());
			String msgEmail = "";
			rs = pstm.executeQuery();
			String tarefa = "";
			boolean enviaEmail = false;

			List<JobComErroVO> listaJobComErro = new ArrayList<>();
			
			while (rs.next())
			{
				JobComErroVO jobComErro = new JobComErroVO();
				
				jobComErro.setProximaExecucao(rs.getString("proxima_execucao"));
				jobComErro.setUltimaExecucao(rs.getString("ultima_execucao"));
				jobComErro.setNomeRotina(rs.getString("name"));
				jobComErro.setPeriodicidade(rs.getString("repeticao"));
				jobComErro.setDetalhesDoErro(rs.getString("details"));
				jobComErro.setErro(rs.getString("error"));
				
				listaJobComErro.add(jobComErro);
			}
			
			for(JobComErroVO jobComErroAux : listaJobComErro) {
				
				String codigoTarefa = null;
				String historicoTarefa = verificaTarefasAbertas(jobComErroAux.getNomeRotina());
	
				if(NeoUtils.safeIsNull(historicoTarefa)) {
					codigoTarefa = abreTarefaJob(jobComErroAux.getUltimaExecucao(), jobComErroAux.getProximaExecucao(), jobComErroAux.getNomeRotina(), jobComErroAux.getPeriodicidade(), jobComErroAux.getDetalhesDoErro());				
				}else {
					System.out.println("##### TAREFA NÃO ABERTA: O Job " + rs.getString("name") + " existe a tarefa " + historicoTarefa + " aberta para a verificação.");
				}
	
				if(NeoUtils.safeIsNotNull(codigoTarefa)) {
					enviaEmail = true;				
				}
				
				if (enviaEmail)
				{
					//encaminharEmail();
				}
			}
		
			
		}
		catch (JobException e)
		{
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}
	
	public static void insereTarefaJobNoFormulario(String codigoTarefa, NeoUser executor, String nomeRotina, String detalhes, String proximaExecucao, String ultimaExecucao, String periodicidade) throws Exception {
		
		if (NeoUtils.safeIsNotNull(codigoTarefa))
		{
			InstantiableEntityInfo tarefaJob = AdapterUtils.getInstantiableEntityInfo("tarefasJob");
			NeoObject objJob = tarefaJob.createNewInstance();
			EntityWrapper WrapperJob = new EntityWrapper(objJob);

			GregorianCalendar date = new GregorianCalendar();

			date.set(GregorianCalendar.HOUR, 0);
			date.set(GregorianCalendar.HOUR_OF_DAY, 0);
			date.set(GregorianCalendar.MINUTE, 0);
			date.set(GregorianCalendar.SECOND, 0);
			date.set(GregorianCalendar.MILLISECOND, 0);

			WrapperJob.findField("startDate").setValue(date);
			WrapperJob.findField("tarefa").setValue(codigoTarefa);
			WrapperJob.findField("executor").setValue(executor);
			WrapperJob.findField("nomeJob").setValue(nomeRotina);
			
			if (NeoUtils.safeIsNull(detalhes.isEmpty()))
			{
				WrapperJob.findField("erro").setValue("nulo");
			}
			else
			{
				WrapperJob.findField("erro").setValue(detalhes);
			}
			
			WrapperJob.findField("periodicidade").setValue(periodicidade);
			
			if (NeoUtils.safeIsNull(proximaExecucao))
			{
				WrapperJob.findField("proExecucao").setValue(null);

			}
			else
			{
				GregorianCalendar proExe = OrsegupsUtils.stringToGregorianCalendar(proximaExecucao, "dd/MM/yyyy");
				WrapperJob.findField("proExecucao").setValue(proExe);
			}

			if (NeoUtils.safeIsNull(proximaExecucao))
			{
				WrapperJob.findField("ultExecucao").setValue(null);
			}
			else
			{
				GregorianCalendar ultExe = OrsegupsUtils.stringToGregorianCalendar(ultimaExecucao, "dd/MM/yyyy");
				WrapperJob.findField("ultExecucao").setValue(ultExe);
			}
			
			PersistEngine.persist(objJob);
		}
	}
	
	public static String abreTarefaJob(String ultimaExecucao, String proximaExecucao, String nomeRotina, String periodicidade, String detalhesErro) {
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		StringBuilder descricao = new StringBuilder();
		
		String titulo = null;
		
		// Prazo
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);
		prazo.set(GregorianCalendar.MILLISECOND, 59);
		
		// Usuários
		String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteTarefaJobComErros");
		NeoUser executor = retornaUsuarioResponsavel(nomeRotina);
		
		// Dados da Tarefa
		
		if(!NeoUtils.safeIsNotNull(proximaExecucao) || proximaExecucao.equals("01/01/1900")) {
			titulo = "Verificar o erro no job "+ nomeRotina +"  pois esta sem data da proxima execução";
		}else{
			titulo = "Verificar o erro no job " + nomeRotina;
		}
		
		descricao.append("<b>Vefificar o problema que ocorreu no Job a seguir:</b><br><br>");
		descricao.append(" <b>Nome do Job: </b>" + nomeRotina + "<br>");
		descricao.append(" <b>Ultima execução: </b>" + ultimaExecucao + "<br>");
		descricao.append(" <b>Próxima execução: </b>" + proximaExecucao + "<br>");
		descricao.append(" <b>Periodicidade: </b>" + periodicidade + "<br>");
		descricao.append(" <b>Erro: " + detalhesErro);
		
		String codigoTarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor.getCode(), titulo, descricao.toString(), "1", "sim", prazo);
		
		if(NeoUtils.safeIsNotNull(codigoTarefa)) {
			try {
				insereTarefaJobNoFormulario(codigoTarefa, executor, nomeRotina, detalhesErro, proximaExecucao, ultimaExecucao, periodicidade);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return codigoTarefa;
		
	}
	
	public static NeoUser retornaUsuarioResponsavel(String nomeRotina) {
		EntityWrapper ewResponsaveisJob = null;
		NeoUser usuarioResponsavel= null;
		
		
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("nameJob", nomeRotina));
		
		NeoObject objetoResponsaveis = PersistEngine.getObject(AdapterUtils.getEntityClass("responsaveiselJob"), groupFilter);
		
		if(NeoUtils.safeIsNotNull(objetoResponsaveis)) {
			ewResponsaveisJob = new EntityWrapper(objetoResponsaveis);
			usuarioResponsavel = (NeoUser) ewResponsaveisJob.findGenericValue("responsavel");
		}
		
		return usuarioResponsavel;
	}
	
	

	public static String verificaTarefasAbertas(String nameJob)
	{

		QLEqualsFilter name = new QLEqualsFilter("nomeJob", nameJob);
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(name);
		String tarefas = "";

		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("tarefasJob"), groupFilter);
		Long cont = 0L;
		for (NeoObject neoObject : objs)
		{
			if (cont > 0)
			{
				tarefas += ",";
			}
			EntityWrapper w = new EntityWrapper(neoObject);
			
			String tarefa = w.findGenericValue("tarefa");
			if (tarefa != null)
				tarefa = tarefa.replaceAll("[^0-9,]", "");
			
			tarefas += tarefa;
			cont++;

		}
		Connection conn = null;
		ResultSet rs = null;
		StringBuilder sqltarefa = new StringBuilder();
		if (!tarefas.equals("") && tarefas != null)
		{
			sqltarefa.append("select processState,saved,code from WFProcess With (NOLOCK) where code in(" + tarefas + ") and processState = 0 and saved = 1");

			conn = PersistEngine.getConnection("");
			PreparedStatement pst = null;
			try
			{
				pst = conn.prepareStatement(sqltarefa.toString());
				rs = pst.executeQuery();
				while (rs.next())
				{
					return rs.getString("code");
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void encaminharEmail() throws EmailException
	{
		GregorianCalendar date = new GregorianCalendar();
		date.set(GregorianCalendar.HOUR, 0);
		date.set(GregorianCalendar.HOUR_OF_DAY, 0);
		date.set(GregorianCalendar.MINUTE, 0);
		date.set(GregorianCalendar.SECOND, 0);
		date.set(GregorianCalendar.MILLISECOND, 0);
		QLOpFilter dataFilter = new QLOpFilter("startDate", "=", (GregorianCalendar) date);
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(dataFilter);
		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("tarefasJob"), groupFilter);
		String nomeJob = "";
		GregorianCalendar ultExecucao = null;
		GregorianCalendar proExecucao = null;
		String periodicidade = "";
		String erro = "";
		String tarefa = "";

		StringBuilder html = new StringBuilder();
		html.append("<html>");
		html.append("<center><h1> Tarefas de Job's com problemas </h1></center>");
		html.append("<br/><br/>");
		html.append(" Segue relação de tarefas abertas no dia de hojé referente aos job's com problemas ");
		html.append("<br/>");
		html.append("<TABLE BORDER=1 style=\"table-layout: fixed; width: 1200px; overflow: hidden;\">");
		html.append("<TR>");
		html.append("<TD WIDTH=15%>NOME DO JOB </TD>");
		html.append("<TD WIDTH=7%>ULTIMA EXECUÇÃO</TD>");
		html.append("<TD WIDTH=7%>PROXIMA EXECUÇÃO </TD>");
		html.append("<TD WIDTH=7%>PERIODICIDADE</TD>");
		html.append("<TD WIDTH=12%>ERRO</TD>");
		html.append("<TD WIDTH=4%>Tarefa</TD>");
		html.append("</TR>");
		for (NeoObject neoObject : objs)
		{
			EntityWrapper wr = new EntityWrapper(neoObject);
			nomeJob = (String) wr.findField("nomeJob").getValue();
			ultExecucao = (GregorianCalendar) wr.findField("ultExecucao").getValue();
			proExecucao = (GregorianCalendar) wr.findField("proExecucao").getValue();
			periodicidade = (String) wr.findField("periodicidade").getValue();
			erro = (String) wr.findField("erro").getValue();
			tarefa = (String) wr.findField("tarefa").getValue();

			html.append("<TR>");
			html.append("<TD>" + nomeJob + "</TD>");
			html.append("<TD>" + NeoUtils.safeDateFormat(ultExecucao, "dd/MM/yyyy") + "</TD>");
			html.append("<TD>" + NeoUtils.safeDateFormat(proExecucao, "dd/MM/yyyy") + "</TD>");
			html.append("<TD>" + periodicidade + "</TD>");
			html.append("<TD>" + erro + "</TD>");
			html.append("<TD>" + tarefa + "</TD>");
			html.append("</TR>");
		}
		html.append("</TABLE>");
		html.append("</html>");
		String emailDst = "thiago.coelho@orsegups.com.br";
		String emailRemetente = "fusion@orsegups.com.br";
		List<String> comCopiaOc = new ArrayList<String>();
		OrsegupsUtils.sendEmail2Orsegups(emailDst, "fusion@orsegups.com.br", "Tarefas de Job's com problemas", "", html.toString(), emailRemetente, null, comCopiaOc);
	}
}
