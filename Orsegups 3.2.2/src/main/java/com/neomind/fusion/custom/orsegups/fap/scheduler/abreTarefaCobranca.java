package com.neomind.fusion.custom.orsegups.fap.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;

public class abreTarefaCobranca implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext arg0)
    {	
	Connection conn = PersistEngine.getConnection("SIGMA90"); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	System.out.println("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Cobrança FAP - abreTarefaCobranca - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
	GregorianCalendar gcHoje = new GregorianCalendar();
	String sHoje = sdf.format(gcHoje.getTime());

	try
	{
	    StringBuffer  varname1 = new StringBuffer();
	    varname1.append(" SELECT ROTA.CD_ROTA AS 'CDROTA', ROTA.NM_ROTA AS 'NMROTA', VTR.CD_VIATURA AS 'CDVIATURA', VTR.NM_VIATURA AS 'NMVIATURA',  ");
	    varname1.append(" VH.CD_EVENTO AS 'EVENTO', VH.DT_FECHAMENTO AS 'DTFECHAMENTO', VH.DT_RECEBIDO, CTRL.ID_CENTRAL, CTRL.ID_EMPRESA, 		");
	    varname1.append(" CTRL.ID_CENTRAL AS 'CTRL', CTRL.CD_CLIENTE AS 'CDCLIENTE', CTRL.RAZAO  							");
	    varname1.append(" FROM ROTA 														");
	    varname1.append(" INNER JOIN ASSOC_ROTA_VIATURA ASSOC ON ROTA.CD_ROTA = ASSOC.CD_ROTA 							");
	    varname1.append(" INNER JOIN VIATURA VTR ON ASSOC.CD_VIATURA = VTR.CD_VIATURA 								");
	    varname1.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.X_SIGMAROTA  AS XROTA ON XROTA.CD_ROTA = ROTA.CD_ROTA 			");
	    varname1.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPAPLICACAOTATICO AS TATICO ON TATICO.ROTASIGMA_NEOID = XROTA.NEOID 	");
	    varname1.append(" INNER JOIN VIEW_HISTORICO VH ON VH.CD_VIATURA = VTR.CD_VIATURA 								");
	    varname1.append(" INNER JOIN dbCENTRAL AS CTRL ON CTRL.CD_CLIENTE = VH.CD_CLIENTE 								");
	    varname1.append(" WHERE ROTA.NM_ROTA LIKE '%PARC%' 												");
	    varname1.append(" AND (VH.DT_ESPERA_DESLOCAMENTO IS NULL 											");
	    varname1.append(" OR VH.DT_VIATURA_NO_LOCAL IS NULL)  											");
	    varname1.append(" AND TATICO.MODALIDADE_NEOID = 908025798 											");
	    varname1.append(" AND VH.DT_RECEBIDO BETWEEN '" + sHoje + " 00:00:01' AND '" + sHoje + " 23:59:59' 									");
	    varname1.append(" AND EXISTS (  														");
	    varname1.append("         SELECT ID_EMPRESA, ID_CENTRAL, CD_CLIENTE FROM dbCENTRAL 								");
	    varname1.append("         WHERE ID_EMPRESA = CTRL.ID_EMPRESA  										");
	    varname1.append("         AND ID_CENTRAL =  CTRL.ID_CENTRAL 										");
	    varname1.append("         AND EXISTS (  													");
	    varname1.append("               SELECT * FROM [FSOODB04\\SQL02].SAPIENS.DBO.usu_t160sig SIG 						");
	    varname1.append("               INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.usu_t160cvs CVS 							");
	    varname1.append("               ON SIG.usu_codemp = CVS.usu_codemp AND SIG.usu_codfil = CVS.usu_codfil  					");
	    varname1.append("               AND SIG.usu_numctr = CVS.usu_numctr AND SIG.usu_numpos = CVS.usu_numpos  					");
	    varname1.append("               WHERE SIG.USU_CODCLI = CD_CLIENTE 										");
	    varname1.append("               AND ((CVS.usu_sitcvs = 'A') OR (CVS.usu_sitcvs = 'I' AND CVS.usu_datfim >= GETDATE()))  			");
	    varname1.append("               ) 														");
	    varname1.append("         ) 														");
	    varname1.append(" ORDER BY ASSOC.CD_ROTA, ASSOC.CD_VIATURA 											");

	    pstm = conn.prepareStatement(varname1.toString());

	    rs = pstm.executeQuery();

	    Long rotaAnterior = null;

	    String texto = null;

	    while (rs.next())
	    {
		Long cdRota = rs.getLong("CDROTA");
		String nmRota = rs.getString("NMROTA");
		String evento = rs.getString("EVENTO");
		
		String dtRecebido = rs.getString("DT_RECEBIDO");
		String anoData = dtRecebido.substring(0, 4);
		String mesData = dtRecebido.substring(5, 7);
		String diaData = dtRecebido.substring(8, 10);
		String hraData = dtRecebido.substring(11, 19); 
		dtRecebido = diaData + "/" + mesData + "/" + anoData + " " + hraData;
		
		String idCentral = rs.getString("ID_CENTRAL");
		Long cdCliente = rs.getLong("CDCLIENTE");

		if(rotaAnterior == null || ((rotaAnterior - cdRota) == 0l)){

		    if(rotaAnterior == null){

			texto = "Favor avaliar os eventos abaixo pois a data de chegada da viatura e/ou de espera deslocamento não foram preenchidas:";
			texto = texto + "<br><br>ROTA: " + cdRota.toString() + " - " + nmRota;
			texto = texto + "<br><br>CLIENTE: " + cdCliente.toString();
			texto = texto + "<br>CENTRAL: " + idCentral;
			texto = texto + "<br>EVENTO: " + evento;
			texto = texto + "<br>DATA RECEBIMENTO: " + dtRecebido;

			rotaAnterior = cdRota;

		    }else{

			texto = texto + "<br><br>CLIENTE: " + cdCliente.toString();
			texto = texto + "<br>CENTRAL: " + idCentral;
			texto = texto + "<br>EVENTO: " + evento;
			texto = texto + "<br>DATA RECEBIMENTO: " + dtRecebido;

		    }

		}else{

		    NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", 11566071)); //Laércio
		    NeoUser executor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", 4679189)); //Edson Heinz
		    String titulo = "Fap Tático - Eventos sem data preenchida";
		    Integer prazoHoras = 48;

		    abreTarefaValidacao(solicitante, executor, titulo, texto, prazoHoras);

		    texto = null;

		    texto = "Favor avaliar os eventos abaixo pois a data de chegada da viatura e/ou de espera deslocamento não foram preenchidas:";
		    texto = texto + "<br><br>ROTA: " + cdRota.toString() + " - " + nmRota;
		    texto = texto + "<br><br>CLIENTE: " + cdCliente.toString();
		    texto = texto + "<br>CENTRAL: " + idCentral;
		    texto = texto + "<br>EVENTO: " + evento;
		    texto = texto + "<br>DATA RECEBIMENTO: " + dtRecebido;

		    rotaAnterior = cdRota;


		}

	    }

	    NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", 125954)); //Rosileny Martins
	    NeoUser executor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", 4679189)); //Edson Heinz
	    String titulo = "Fap Tático - Eventos sem data preenchida";
	    Integer prazoHoras = 48;

	    abreTarefaValidacao(solicitante, executor, titulo, texto, prazoHoras);

	}catch (Exception e) {
	    System.out.println("##### AGENDADOR DE TAREFA: Cobrança FAP - abreTarefaCobranca");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally {
	    try {
		rs.close();
		pstm.close();
		conn.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }

	    System.out.println("##### FIM AGENDADOR DE TAREFA: Cobrança FAP - abreTarefaCobranca - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }

    public void abreTarefaValidacao(NeoUser solicitante, NeoUser executor, String titulo, String descricao, Integer prazoHoras) {

	NeoObject oTS = AdapterUtils.createNewEntityInstance("Tarefa");
	final EntityWrapper wOTS = new EntityWrapper(oTS);

	wOTS.findField("Solicitante").setValue(solicitante); 
	wOTS.findField("Executor").setValue(executor);
	wOTS.findField("Titulo").setValue(titulo);
	wOTS.findField("DescricaoSolicitacao").setValue(descricao);

	GregorianCalendar prazoTarefaSimples = new GregorianCalendar();
	prazoTarefaSimples.add(Calendar.HOUR, prazoHoras);
	Boolean diautil = OrsegupsUtils.isWorkDay(prazoTarefaSimples);
	if(!diautil)
	    prazoTarefaSimples = OrsegupsUtils.getNextWorkDay(prazoTarefaSimples);

	wOTS.findField("Prazo").setValue(prazoTarefaSimples);

	PersistEngine.persist(oTS);
	ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));		
	OrsegupsWorkflowHelper.iniciaProcesso(pm, oTS, true, solicitante, true, null);
    }


}

