package com.neomind.fusion.custom.orsegups.utils;

public class NumberWordsVarsOrsegups {

	private Boolean isNeg = false;

	private int intNumPart;

	private String intNumPartInString;

	private int floatInInt;

	private double floatInDouble;

	private String floatInString;

	private Boolean nIsMoney = false;

	public NumberWordsVarsOrsegups()
	{

	}

	public Boolean getIsNeg()
	{
		return isNeg;
	}

	public void setIsNeg(Boolean isNeg)
	{
		this.isNeg = isNeg;
	}

	public int getIntNumPart()
	{
		return intNumPart;
	}

	public void setIntNumPart(int intNumPart)
	{
		this.intNumPart = intNumPart;
	}

	public String getIntNumPartInString()
	{
		return intNumPartInString;
	}

	public void setIntNumPartInString(String intNumPartInString)
	{
		this.intNumPartInString = intNumPartInString;
	}

	public int getFloatInInt()
	{
		return floatInInt;
	}

	public void setFloatInInt(int floatInInt)
	{
		this.floatInInt = floatInInt;
	}

	public double getFloatInDouble()
	{
		return floatInDouble;
	}

	public void setFloatInDouble(double floatInDouble)
	{
		this.floatInDouble = floatInDouble;
	}

	public String getFloatInString()
	{
		return floatInString;
	}

	public void setFloatInString(String floatInString)
	{
		this.floatInString = floatInString;
	}

	public Boolean isMoney()
	{
		return nIsMoney;
	}

	public void setMoney(Boolean nIsMoney)
	{
		this.nIsMoney = nIsMoney;
	}


}
