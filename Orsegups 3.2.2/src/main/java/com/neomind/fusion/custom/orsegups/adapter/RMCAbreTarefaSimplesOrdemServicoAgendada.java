package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class RMCAbreTarefaSimplesOrdemServicoAgendada implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RMCAbreTarefaSimplesOrdemServicoAgendada.class);
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	/* Regra comentada devido a solicitação da tarefa simples - 1079371 
	    try
		{
			String code = "";
			String avanca = "";

			String solicitante = "bruno.oliveira";
			String executor = (String) processEntity.findValue("executivoRMC");
			String contrato = (String) processEntity.findValue("numeroContrato");

			log.warn("RMC - Chegou no IF");
			if (executor != null && !executor.trim().isEmpty() && !executor.equalsIgnoreCase("leonardo.berka") && !executor.equalsIgnoreCase("leandro.paulo"))
			{
				log.warn("RMC - Verificar usuário Fusion do executor da tarefa simples");
				QLEqualsFilter ftrUsuario = new QLEqualsFilter("code", executor);
				NeoObject oUsuario = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), ftrUsuario);
				if (oUsuario != null)
				{
					avanca = "sim";
				}
				else
				{
					avanca = "nao";
				}
				log.warn("RMC - Avança a Tarefa Simples " + avanca);

				String titulo = "Acompanhar Instalação/Habilitação do Contrato " + contrato + " - " + processEntity.findValue("nomeCliente");//razão do cliente;
				String descricao = "";
				descricao = "<strong><u>Acompanhar instalação da venda abaixo:</u></strong><br><br>";
				descricao = descricao + " <strong>Contrato :</strong> " + contrato + "<br>";
				descricao = descricao + " <strong>Cliente :</strong> " + processEntity.findValue("nomeCliente") + "<br>";//razão do cliente;;
				descricao = descricao + " <strong>Data Agendamento :</strong> " + NeoDateUtils.safeDateFormat(((GregorianCalendar) processEntity.findValue("dataAgendamento")), "dd/MM/yyyy") + "<br>";
				if(((Time) processEntity.findValue("horaAgendamento")) == null)
				{
					descricao = descricao + " <strong>Hora do Agendamento:</strong> 00:00 <br>";
				}
				else
				{
					descricao = descricao + " <strong>Hora do Agendamento:</strong> " + NeoDateUtils.safeDateFormat(((Time) processEntity.findValue("horaAgendamento")), "HH:mm") + "<br>";
				}				
				descricao = descricao + " <strong>Instalador :</strong> " + processEntity.findValue("tecnicoResponsavel.nm_colaborador") + "<br>";

				String origem = "1";

				GregorianCalendar datAtu = new GregorianCalendar();
				datAtu.set(Calendar.HOUR_OF_DAY, 0);
				datAtu.set(Calendar.MINUTE, 0);
				datAtu.set(Calendar.SECOND, 0);
				datAtu.set(Calendar.MILLISECOND, 0);
						
				GregorianCalendar prazo = (GregorianCalendar) processEntity.findValue("dataAgendamento");
				//Data do agendamento igual a data atual ou - Data do agendamento igual a Domingo 1 dia de Prazo
				if(prazo.equals(datAtu) || prazo.get(GregorianCalendar.DAY_OF_WEEK) == 1 || prazo.get(GregorianCalendar.DAY_OF_WEEK) == 7)
				{
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
				}
				else
				{
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 0L);
				}
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);
				log.warn("RMC - Inicia Abertura Tarefa Simples");
				
				IniciarTarefaSimples iniciaTarefaSimples = new IniciarTarefaSimples();
				code = iniciaTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo);
				
				log.warn("RMC - Abriu a Tarefa Simples " + code);				
				
				String obsAlmoxarife = (String) processEntity.findValue("obsAlmoxarife");
				obsAlmoxarife = obsAlmoxarife + "<br> Aberto a Tarefa Simples " + code + " para Acompanhamento da Instalação.";
				processEntity.setValue("obsAlmoxarife", obsAlmoxarife);
			}
			log.warn("RMC - Saiu do IF");
		}
		catch(Exception e)
		{
			log.error("Error RMCAbreTarefaSimplesOrdemServicoAgendada " + e.getMessage());
			e.printStackTrace();
			throw new WorkflowException("Por favor, contatar o administrador do sistema!");
		}
	    */
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}

}
