package com.neomind.fusion.custom.orsegups.integracao.dto;

public class UrlIntegracaoDTO
{

	private String url;
	private String token;

	public UrlIntegracaoDTO(String url, String token)
	{
		this.url = url;
		this.token = token;
	}

	public String getUrl()
	{
		return url;
	}

	public String getToken()
	{
		return token;
	}

	
}
