package com.neomind.fusion.custom.orsegups.fap.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.fap.utils.FapUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;


/**
 * Classe utilizada para validar a competência de pagamento
 * das aplicações FAP Tático e FAP Técnicos Terceiros
 * @author diogo.silva
 * */
public class FAPControlaCompetenciaPagamento implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {

	/* Variáveis usadas para parametrização, tanto locais quanto em produção*/
	boolean controlaCompetencia = (FAPParametrizacao.findParameter("controlaCompetencia") != null ? FAPParametrizacao.findParameter("controlaCompetencia").equals("1") ? true : false : false);
	Long codigoAplicacaoPagamento = processEntity.findGenericValue("aplicacaoPagamento.codigo");
	String codigoTarefaAtual = processEntity.findGenericValue("wfprocess.code");

	if(controlaCompetencia && codigoAplicacaoPagamento == 11L) { // FAP Tático
	    try {
		
		GregorianCalendar competencia = processEntity.findGenericValue("competenciaPagamento");
		competencia.set(Calendar.HOUR, 0);
		competencia.set(Calendar.MINUTE, 0);
		competencia.set(Calendar.SECOND, 0);
		competencia.set(Calendar.MILLISECOND, 0);
		
		processEntity.findField("competenciaPagamento").setValue(competencia);
		
		Long codigoRota = processEntity.findGenericValue("rotaSigma.rotaSigma.cd_rota");

		// Regra de negócio: a competência sendo paga não pode anteceder o início dos pagamentos da rota
		boolean competenciaAntecedeInicioRota = FapUtils.competenciaAntecedeInicioRota(competencia, codigoRota);	
		if(competenciaAntecedeInicioRota){
		    throw new WorkflowException("Data de competência (pagamento) antecede a data de início da rota.");
		}

		// Regra de negócio: só pode existir 1 FAP em andamento ou finalizada FAP por competência.
		boolean competenciaValida = FapUtils.validarCompetencia(competencia, codigoRota, codigoTarefaAtual);

		if(!competenciaValida){
		    throw new WorkflowException("Já existe uma tarefa em andamento ou finalizada para esta competência.");
		}

	    } catch (WorkflowException e) {
		e.printStackTrace();
		throw new WorkflowException(e.getErrorList().get(0).getI18nMessage());
	    }
	    catch (Exception e) {
		e.printStackTrace();
		throw new WorkflowException("Erro inesperado ao validar a competencia de pagamento.");
	    }
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// DO NOTHING
    }




}
