package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class SaldoHorasVO
{
	String codLoc;
	String nomLoc;
	String nomFun;
	Long numcad;
	Long numemp;
	Long tipcol;
	Long saldo;
	Long saldoAnterior;
	Long saldoVencer;
	
	GregorianCalendar perini;
	GregorianCalendar perfim;
	
	
	public String getCodLoc()
	{
		return codLoc;
	}
	public void setCodLoc(String codLoc)
	{
		this.codLoc = codLoc;
	}
	public String getNomLoc()
	{
		return nomLoc;
	}
	public void setNomLoc(String nomLoc)
	{
		this.nomLoc = nomLoc;
	}
	public String getNomFun()
	{
		return nomFun;
	}
	public void setNomFun(String nomFun)
	{
		this.nomFun = nomFun;
	}
	public Long getNumcad()
	{
		return numcad;
	}
	public void setNumcad(Long numcad)
	{
		this.numcad = numcad;
	}
	public Long getNumemp()
	{
		return numemp;
	}
	public void setNumemp(Long numemp)
	{
		this.numemp = numemp;
	}
	public Long getTipcol()
	{
		return tipcol;
	}
	public void setTipcol(Long tipcol)
	{
		this.tipcol = tipcol;
	}
	public Long getSaldo()
	{
		return saldo;
	}
	public void setSaldo(Long saldo)
	{
		this.saldo = saldo;
	}
	public GregorianCalendar getPerini()
	{
		return perini;
	}
	public void setPerini(GregorianCalendar perini)
	{
		this.perini = perini;
	}
	public GregorianCalendar getPerfim()
	{
		return perfim;
	}
	public void setPerfim(GregorianCalendar perfim)
	{
		this.perfim = perfim;
	}
	
	public Long getSaldoAnterior()
	{
		return saldoAnterior;
	}
	public void setSaldoAnterior(Long saldoAnterior)
	{
		this.saldoAnterior = saldoAnterior;
	}
	public Long getSaldoVencer()
	{
		return saldoVencer;
	}
	public void setSaldoVencer(Long saldoVencer)
	{
		this.saldoVencer = saldoVencer;
	}
	@Override
	public String toString()
	{
		return "SaldoHorasVO [codLoc=" + codLoc + ", nomLoc=" + nomLoc + ", nomFun=" + nomFun + ", numcad=" + numcad + ", numemp=" + numemp + ", tipcol=" + tipcol + ", saldo=" + saldo + ", saldoAnterior=" + saldoAnterior + ", saldoVencer=" + saldoVencer + ", perini=" + perini + ", perfim=" + perfim + "]";
	}
	
	
	

}
