package com.neomind.fusion.custom.orsegups.vetorh;

import java.util.GregorianCalendar;

public class HoraExtraVO
{
	private Long numemp;
	private Long numcad;
	private String nomfun;
	private Long codfil;
	private Long codeve;
	private Long refeve;
	private String descricaoEvento;
	private String dataI;
	private String dataF;
	private Long escala;
	private Long turma;
	private Long horario;
	private String descricaoHorario;
	private String descricaoEscala;
	private Long grupoEscala;
	private String grupoEscalaDescricao;
	private Long horasSemana;
	private Long sindicato;
	private GregorianCalendar gDataI;
	private GregorianCalendar gDataF;
	private String horasExtras;
	private Long calculo;
	
	public Long getNumemp()
	{
		return numemp;
	}
	public void setNumemp(Long numemp)
	{
		this.numemp = numemp;
	}
	public Long getNumcad()
	{
		return numcad;
	}
	public void setNumcad(Long numcad)
	{
		this.numcad = numcad;
	}
	public String getNomfun()
	{
		return nomfun;
	}
	public void setNomfun(String nomfun)
	{
		this.nomfun = nomfun;
	}
	public Long getCodeve()
	{
		return codeve;
	}
	public void setCodeve(Long codeve)
	{
		this.codeve = codeve;
	}
	public Long getRefeve()
	{
		return refeve;
	}
	public void setRefeve(Long refeve)
	{
		this.refeve = refeve;
	}
	public String getDataI()
	{
		return dataI;
	}
	public void setDataI(String dataI)
	{
		this.dataI = dataI;
	}
	public String getDataF()
	{
		return dataF;
	}
	public void setDataF(String dataF)
	{
		this.dataF = dataF;
	}
	public Long getEscala()
	{
		return escala;
	}
	public void setEscala(Long escala)
	{
		this.escala = escala;
	}
	public Long getHorario()
	{
		return horario;
	}
	public void setHorario(Long horario)
	{
		this.horario = horario;
	}
	public Long getTurma()
	{
		return turma;
	}
	public void setTurma(Long turma)
	{
		this.turma = turma;
	}
	public String getDescricaoEscala()
	{
		return descricaoEscala;
	}
	public void setDescricaoEscala(String descricaoEscala)
	{
		this.descricaoEscala = descricaoEscala;
	}
	@Override
	public String toString()
	{
		return numemp + ";" + numcad + ";" + nomfun + ";" + dataI + ";" + dataF +";" + descricaoEvento + ";" + refeve + ";" + horasExtras + ";" + escala + ";" + turma + ";" + descricaoEscala + ";" + grupoEscalaDescricao +  ";" + descricaoHorario + ";" + codfil + ";" + sindicato;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codeve == null) ? 0 : codeve.hashCode());
		result = prime * result + ((dataF == null) ? 0 : dataF.hashCode());
		result = prime * result + ((dataI == null) ? 0 : dataI.hashCode());
		result = prime * result + ((descricaoEscala == null) ? 0 : descricaoEscala.hashCode());
		result = prime * result + ((escala == null) ? 0 : escala.hashCode());
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		result = prime * result + ((numcad == null) ? 0 : numcad.hashCode());
		result = prime * result + ((numemp == null) ? 0 : numemp.hashCode());
		result = prime * result + ((refeve == null) ? 0 : refeve.hashCode());
		result = prime * result + ((turma == null) ? 0 : turma.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HoraExtraVO other = (HoraExtraVO) obj;
		if (codeve == null)
		{
			if (other.codeve != null)
				return false;
		}
		else if (!codeve.equals(other.codeve))
			return false;
		if (dataF == null)
		{
			if (other.dataF != null)
				return false;
		}
		else if (!dataF.equals(other.dataF))
			return false;
		if (dataI == null)
		{
			if (other.dataI != null)
				return false;
		}
		else if (!dataI.equals(other.dataI))
			return false;
		if (descricaoEscala == null)
		{
			if (other.descricaoEscala != null)
				return false;
		}
		else if (!descricaoEscala.equals(other.descricaoEscala))
			return false;
		if (escala == null)
		{
			if (other.escala != null)
				return false;
		}
		else if (!escala.equals(other.escala))
			return false;
		if (horario == null)
		{
			if (other.horario != null)
				return false;
		}
		else if (!horario.equals(other.horario))
			return false;
		if (numcad == null)
		{
			if (other.numcad != null)
				return false;
		}
		else if (!numcad.equals(other.numcad))
			return false;
		if (numemp == null)
		{
			if (other.numemp != null)
				return false;
		}
		else if (!numemp.equals(other.numemp))
			return false;
		if (refeve == null)
		{
			if (other.refeve != null)
				return false;
		}
		else if (!refeve.equals(other.refeve))
			return false;
		if (turma == null)
		{
			if (other.turma != null)
				return false;
		}
		else if (!turma.equals(other.turma))
			return false;
		return true;
	}
	public Long getGrupoEscala()
	{
		return grupoEscala;
	}
	public void setGrupoEscala(Long grupoEscala)
	{
		this.grupoEscala = grupoEscala;
	}
	public Long getSindicato()
	{
		return sindicato;
	}
	public void setSindicato(Long sindicato)
	{
		this.sindicato = sindicato;
	}
	public Long getHorasSemana()
	{
		return horasSemana;
	}
	public void setHorasSemana(Long horasSemana)
	{
		this.horasSemana = horasSemana;
	}
	public GregorianCalendar getgDataI()
	{
		return gDataI;
	}
	public void setgDataI(GregorianCalendar gDataI)
	{
		this.gDataI = gDataI;
	}
	public GregorianCalendar getgDataF()
	{
		return gDataF;
	}
	public void setgDataF(GregorianCalendar gDataF)
	{
		this.gDataF = gDataF;
	}
	public String getDescricaoEvento()
	{
		return descricaoEvento;
	}
	public void setDescricaoEvento(String descricaoEvento)
	{
		this.descricaoEvento = descricaoEvento;
	}
	public String getDescricaoHorario()
	{
		return descricaoHorario;
	}
	public void setDescricaoHorario(String descricaoHorario)
	{
		this.descricaoHorario = descricaoHorario;
	}
	public String getGrupoEscalaDescricao()
	{
		return grupoEscalaDescricao;
	}
	public void setGrupoEscalaDescricao(String grupoEscalaDescricao)
	{
		this.grupoEscalaDescricao = grupoEscalaDescricao;
	}
	public String getHorasExtras()
	{
		return horasExtras;
	}
	public void setHorasExtras(String horasExtras)
	{
		this.horasExtras = horasExtras;
	}
	public Long getCodfil()
	{
		return codfil;
	}
	public void setCodfil(Long codfil)
	{
		this.codfil = codfil;
	}
	public Long getCalculo()
	{
		return calculo;
	}
	public void setCalculo(Long calculo)
	{
		this.calculo = calculo;
	}
}
