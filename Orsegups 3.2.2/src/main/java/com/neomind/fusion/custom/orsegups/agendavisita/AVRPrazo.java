package com.neomind.fusion.custom.orsegups.agendavisita;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile.Origin;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.agendavisita.AVRPrazo
public class AVRPrazo implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper wrapper,	Activity activity) 
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		try{
			
			/*NeoUser usuario = (NeoUser) wrapper.findValue("usuarioGerenteComercial");
			NeoUser usuarioEscala = null;
			if (usuario != null){
				
				// execeções de escala de tarefas
				if (usuario.getName().equals("juilson.vieira")){
					usuarioEscala = (NeoUser) PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "gilsoncesar" ) );
				}else if (usuario.getName().equals("cleber.locatelli")){ // romildo
					usuarioEscala = (NeoUser) PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "romildo.diniz" ) );					
				}else if (usuario.getName().equals("roque.alves")){ //romildo
					usuarioEscala = (NeoUser) PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "romildo.diniz" ) );					
				}else if (usuario.getName().equals("geraldo.santos")){ // gilson
					usuarioEscala = (NeoUser) PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "gilsoncesar" ) );					
				}else if (usuario.getName().equals("cleci.pagnussatti")){ // Maurelio
					usuarioEscala = (NeoUser) PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "maurelio.pinto" ) );					
				}else{
					usuarioEscala = getSuperior(usuario); // retorna superior imediato
				}
			}
			
			if (usuarioEscala != null){
				wrapper.setValue("usuarioGerenteComercial", usuarioEscala);
				wrapper.setValue("usuarioEscala1", usuarioEscala);
			}else{
				wrapper.setValue("usuarioEscala1", (NeoUser) null );
				throw new WorkflowException("Não foi possível encotnrar o usuário superior para setar a escala");
			}
			System.out.println("Tarefa "+origin.getCode()+" escalada do usuario " + usuario.getName() + " para o usuário " + usuarioEscala.getName());*/
			
			System.out.println("[Fluxo visitas] - passando pelo dead Line");
			GregorianCalendar prazo = (GregorianCalendar) wrapper.findValue("prazoVisita");
			prazo = new GregorianCalendar();
			prazo.add(GregorianCalendar.DATE, 5);
			wrapper.setValue("prazoVisita", OrsegupsUtils.getNextWorkDay(prazo) );
			System.out.println("[Fluxo visitas] - Tarefa "+origin.getCode()+" escalada com prazo para " + NeoUtils.safeDateFormat(OrsegupsUtils.getNextWorkDay(prazo)) ); 
			System.out.println("[Fluxo visitas] - Novo Prazo:"+ NeoUtils.safeDateFormat((GregorianCalendar)wrapper.findValue("prazoVisita")));
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Não foi possível encotnrar o usuário superior para setar a escala");
		}
		
		
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		System.out.println("voltando pelo dead Line");
		
	}
	
	
	public NeoUser getSuperior(NeoUser usu){
		
		NeoGroup grupo = usu.getGroup();
		//System.out.println("grupo: " + grupo.getName());
		NeoGroup grupoSuperior = grupo.getUpperLevel();
		//System.out.println("grupo superior: " + grupoSuperior.getName());
		NeoPaper responsavel = grupoSuperior.getResponsible();
		Set<NeoUser> usuarios = responsavel.getUsers();
		Iterator<NeoUser> ite = usuarios.iterator();
		NeoUser superior = null;
		if (ite != null && ite.hasNext()){
			superior = ite.next();
			return superior;
		}
		return null;
		//System.out.println("responsável: " + superior.getName() );
	}

}
