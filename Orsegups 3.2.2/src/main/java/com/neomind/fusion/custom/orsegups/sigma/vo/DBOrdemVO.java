package com.neomind.fusion.custom.orsegups.sigma.vo;

public class DBOrdemVO
{
	private Long id;
	private Long idInstalador;
	private Long idDefeito;
	private Long idSolicitacao;
	private String descricao;
	private Long cdCliente;
	private String contaParticao;
	private String razao;
	private String fantasia;
	private String tecnicoForcarOS;
	private String data;
	private String hora;
	private String minuto;
	private String equipamento;
	private String franquia;
	private String franquiaUso;
	private String descricaoDefeito;
	private String nomeColaborador;
	private String dataAbertuda;
	private String dataFechamento;
	private Long status;
	
	public String getDescricaoDefeito()
	{
		return descricaoDefeito;
	}
	public void setDescricaoDefeito(String descricaoDefeito)
	{
		this.descricaoDefeito = descricaoDefeito;
	}
	public String getNomeColaborador()
	{
		return nomeColaborador;
	}
	public void setNomeColaborador(String nomeColaborador)
	{
		this.nomeColaborador = nomeColaborador;
	}
	public String getDataAbertuda()
	{
		return dataAbertuda;
	}
	public void setDataAbertuda(String dataAbertuda)
	{
		this.dataAbertuda = dataAbertuda;
	}
	public String getDataFechamento()
	{
		return dataFechamento;
	}
	public void setDataFechamento(String dataFechamento)
	{
		this.dataFechamento = dataFechamento;
	}
	public Long getStatus()
	{
		return status;
	}
	public void setStatus(Long status)
	{
		this.status = status;
	}
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public Long getIdInstalador()
	{
		return idInstalador;
	}
	public void setIdInstalador(Long idInstalador)
	{
		this.idInstalador = idInstalador;
	}
	public Long getIdDefeito()
	{
		return idDefeito;
	}
	public void setIdDefeito(Long idDefeito)
	{
		this.idDefeito = idDefeito;
	}
	public Long getIdSolicitacao()
	{
		return idSolicitacao;
	}
	public void setIdSolicitacao(Long idSolicitacao)
	{
		this.idSolicitacao = idSolicitacao;
	}
	public String getDescricao()
	{
		return descricao;
	}
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	public Long getCdCliente()
	{
		return cdCliente;
	}
	public void setCdCliente(Long cdCliente)
	{
		this.cdCliente = cdCliente;
	}
	public String getContaParticao()
	{
		return contaParticao;
	}
	public void setContaParticao(String contaParticao)
	{
		this.contaParticao = contaParticao;
	}
	public String getRazao()
	{
		return razao;
	}
	public void setRazao(String razao)
	{
		this.razao = razao;
	}
	public String getFantasia()
	{
		return fantasia;
	}
	public void setFantasia(String fantasia)
	{
		this.fantasia = fantasia;
	}
	public String getTecnicoForcarOS()
	{
		return tecnicoForcarOS;
	}
	public void setTecnicoForcarOS(String tecnicoForcarOS)
	{
		this.tecnicoForcarOS = tecnicoForcarOS;
	}
	public String getData()
	{
		return data;
	}
	public void setData(String data)
	{
		this.data = data;
	}
	public String getHora()
	{
		return hora;
	}
	public void setHora(String hora)
	{
		this.hora = hora;
	}
	public String getMinuto()
	{
		return minuto;
	}
	public void setMinuto(String minuto)
	{
		this.minuto = minuto;
	}
	public String getEquipamento()
	{
		return equipamento;
	}
	public void setEquipamento(String equipamento)
	{
		this.equipamento = equipamento;
	}
	public String getFranquia()
	{
		return franquia;
	}
	public void setFranquia(String franquia)
	{
		this.franquia = franquia;
	}
	public String getFranquiaUso()
	{
		return franquiaUso;
	}
	public void setFranquiaUso(String franquiaUso)
	{
		this.franquiaUso = franquiaUso;
	}
}