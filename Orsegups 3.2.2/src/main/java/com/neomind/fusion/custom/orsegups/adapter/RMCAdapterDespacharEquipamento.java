package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.UserActivity.FinishBy;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.handler.HandlerFactory;
import com.neomind.fusion.workflow.task.central.model.TaskBoxType;
import com.neomind.util.NeoDateUtils;

/**
 * Avançar a tarefa de Despachar Equipamento automaticamente quando o item <b>Todos Compra Direta</b> for <b><u>true</u></b>
 * 
 * @author herisson.ferreira
 *
 */
public class RMCAdapterDespacharEquipamento implements ActivityStartEventListener {
	
	
	@Override
	public void onStart(ActivityEvent event) throws ActivityException {
		
		System.out.println("##### INICIO ADAPTER: "+this.getClass().getSimpleName()+" Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy")+" - Tarefa: "+event.getProcess().getCode());
		
		try {

			EntityWrapper processEntity = event.getWrapper();

			/**
			 *  Inativado em 12/03 devido a reunião na TI.
			 * 
			 */
//			Boolean todosCompraDireta = (Boolean) processEntity.findGenericValue("todosCompraDireta");
			Boolean isRastreamento = (Boolean) processEntity.findGenericValue("isRastreamento");
			Boolean estoqueDisponivel = (Boolean) processEntity.findGenericValue("estoqueDisponivel");
			String tipIns = (String) processEntity.findGenericValue("tipIns");
			
			if(!isRastreamento && (estoqueDisponivel && tipIns.equalsIgnoreCase("M"))) {

				preencheDadosAtividade(processEntity);

				avancaAtividade(processEntity, event);

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("["+this.getClass().getSimpleName()+"] Erro ao avançar a etapa de despachar equipamento automaticamente");
			throw new WorkflowException("Não foi possível avançar a etapa de despachar equipamento automaticamente!");
		} finally {
			System.out.println("##### FIM ADAPTER: "+this.getClass().getSimpleName()+" Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy")+" - Tarefa: "+event.getProcess().getCode());
		}
	}

	private void avancaAtividade(EntityWrapper processEntity, ActivityEvent event) throws Exception{
			
			final Activity act = event.getActivity();
			final Long activityNeoId = act.getNeoId();

			NeoUser nu = PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));
			final Long userNeoId = nu.getNeoId();
			
			Activity act2 = PersistEngine.getNeoObject(Activity.class, activityNeoId);
			NeoUser nuExecutor = PersistEngine.getNeoObject(NeoUser.class, userNeoId);
			Task task = act2.getModel().getTaskAssigner().assign((UserActivity) act2, nuExecutor, true);
			
			task.setFinishByUser(nu);
			task.setFinishBy(FinishBy.USER);
			
			RuntimeEngine.getTaskService().complete(task.getInstance(), nu);
			
	}

	/**
	 * Preenche os dados obrigatórios da atividade
	 * 
	 * @param processEntity
	 */
	private void preencheDadosAtividade(EntityWrapper processEntity) {
		
		try {
			
			GregorianCalendar dataChegada = new GregorianCalendar();
			dataChegada.set(GregorianCalendar.HOUR_OF_DAY, 23);
			dataChegada.set(GregorianCalendar.MINUTE, 59);
			dataChegada.set(GregorianCalendar.MILLISECOND, 59);
			
			processEntity.findField("despachadoRegional").setValue(true);
			processEntity.findField("obsAlmoxarife").setValue("Avançado automaticamente via Sistema pois todos os itens são de compra direta.");
			processEntity.findField("previsaoChegadaEquipamentoRegional").setValue(dataChegada);
			processEntity.findField("equipamentoDespachadoParcialmente").setValue(false);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("["+this.getClass().getSimpleName()+"] Erro ao inserir os dados obrigatórios na etapa de Despachar Equipamento");
			throw new WorkflowException("Não foi possível preencher os dados na tarefa");
		}
		
	}

}
