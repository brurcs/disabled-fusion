package com.neomind.fusion.custom.orsegups.site.vo;

import java.math.BigDecimal;

public class SiteDocumentoVO
{
	
	private String descricao;
	private Boolean mostraDocumento;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getMostraDocumento() {
		return mostraDocumento;
	}

	public void setMostraDocumento(Boolean mostraDocumento) {
		this.mostraDocumento = mostraDocumento;
	}
	
	
}
