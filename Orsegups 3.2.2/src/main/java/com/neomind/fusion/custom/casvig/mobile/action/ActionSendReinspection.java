package com.neomind.fusion.custom.casvig.mobile.action;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.neomind.fusion.custom.casvig.mobile.CasvigMobileServlet;
import com.neomind.fusion.custom.casvig.mobile.MobileUtils;
import com.neomind.fusion.custom.casvig.mobile.entity.CasvigMobileEntityPool;
import com.neomind.fusion.custom.casvig.mobile.xml.XmlMobileBuilder;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Task;
import com.neomind.util.NeoUtils;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

/**
 * Classe que envia o XML que contém as reinspeções a serem realizadas pelo
 * Mobile da Casvig usado nas inspetorias de qualidade
 * 
 * @author Daniel Henrique Joppi
 * 
 */
@WebServlet(name="ActionSendReinspection", urlPatterns={"/servlet/com.neomind.fusion.custom.casvig.mobile.action.ActionSendReinspection"})
public class ActionSendReinspection extends HttpServlet
{
	public static ActionSendReinspection getInstance()
	{
		return new ActionSendReinspection();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String msg = null;
		DataOutputStream out = null;

		boolean error = false;
		try
		{
			out = new DataOutputStream(response.getOutputStream());

			CasvigMobileServlet.log.info("Mobile request Reinspecao.xml");
			boolean have = false;

			List<CasvigMobileEntityPool> mobilePool = (List<CasvigMobileEntityPool>) PersistEngine.getObjects(CasvigMobileEntityPool.class, QLEqualsFilter.buildFromObject("sendToMobile", true));
			if (mobilePool == null || mobilePool.size() <= 0)
			{
				MobileUtils.sendString(ERROR, response);
				CasvigMobileServlet.log.info("No have reinspection - " + ((mobilePool == null) ? "null" : String.valueOf(mobilePool.size())));
				return;
			}

			List<CasvigMobileEntityPool> listPool = new ArrayList<CasvigMobileEntityPool>();

			String strListNeoId = MobileUtils.getOldString(request.getParameter("LNId"));
			if (!NeoUtils.safeIsNull(strListNeoId))
				CasvigMobileServlet.log.info(strListNeoId);
			String[] arrayLNid = strListNeoId.split(";");

			NeoUser user = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("code")));

			for (CasvigMobileEntityPool pool : mobilePool)
			{
				Task task = pool.getTask();

				// se deve enviar a reinspeção para o mobile
				if (pool.getSendToMobile() 
						// verifica se o mobile já não recebeu esta reinspeção
						&& !MobileUtils.haveNeoId(arrayLNid, task.getNeoId())
						// verifica se tem alguma reinspeção para o inspetor solicitante
						&& pool.getUsers().contains(user)) 
				{
					listPool.add(pool);

					// não envia mais para o mobile
					/*
					 * fernando.rebelo
					 * não setar fara false, para que cada vez que sincronizar o 
					 * mobile trazer todas que o usuário não possui no mobile
					 */
					//deixar sempre pra enviar para mobile, assim usuarios podem baixar as reinspecs mesmo se já tiverem baixado.
					//pool.setSendToMobile(false); 

					if (!have)
						have = true;
				}
			}

			if (listPool == null || listPool.size() == 0)
			{
				MobileUtils.sendString(ERROR, response);
				CasvigMobileServlet.log.info("No have new reinspection");
				return;
			}

			if (have)
			{
				Document docXML = XmlMobileBuilder.buildXMLReinspection(listPool);

				if (docXML == null)
				{
					MobileUtils.sendString(ERROR, response);
					CasvigMobileServlet.log.error("XML exception - reinspection");
					return;
				}
				msg = new String("Send to mobile: Reinspecao.xml");

				XMLSerializer serializer = new XMLSerializer(out, new OutputFormat(docXML, "ISO-8859-1", true));
				serializer.serialize(docXML);

				CasvigMobileServlet.log.info("Reinspeção "+msg);
			}
			else
			{
				CasvigMobileServlet.log.error("No have Reinspecao.xml");
				MobileUtils.sendString(ERROR, response);
			}
			return;
		}
		catch (Exception e)
		{
			error = true;
		}
		finally
		{
			if (out != null)
			{
				try
				{
					out.close();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
					error = true;
				}
			}
			if (error)
			{
				if (msg != null)
					CasvigMobileServlet.log.error(msg);
			}
		}
	}

	private static String ERROR = "ERROR";
}
