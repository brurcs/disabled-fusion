package com.neomind.fusion.custom.orsegups.konviva;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.konviva.IntegracaoOrsegupsKonvivaServlet
@WebServlet(name="IntegracaoOrsegupsWinkerServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.konviva.IntegracaoOrsegupsKonvivaServlet"})
public class IntegracaoOrsegupsKonvivaServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(IntegracaoOrsegupsKonvivaServlet.class);
	
	private static final String TOKEN = "f6085876b275a4809a797a0ab111c777";
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter out = null;
		response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
        response.setContentType("application/json");
		
        JSONObject ret = new JSONObject();
        
        try{
        
        	out = response.getWriter();
        	
        	response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "POST");
            response.setHeader("Access-Control-Allow-Headers", "Content-Type");
            response.setHeader("Access-Control-Max-Age", "86400");
            response.setContentType("application/json");
        	
        	
	        if (request.getHeader("X-AUTH-TOKEN").equals(TOKEN)){
	        	String strNumcpf = request.getParameter("numcpf");
	        	if (!"".equals(strNumcpf)){
	        		ret.put("status", 100);
	        		ret.put("message", "Sucesso");
	        		ret.put("working", retornaPresencaColaborador(NeoUtils.safeLong(strNumcpf)));
	        	}else{
	        		ret.put("status", 98);
	        		ret.put("message", "CPF não informado.");
	        		ret.put("working", false);
	        	}
	        	
	        }else{
	        	response.setStatus(401);
	        }
	        
	        out.print(ret.toString());
	        return;
        
        }catch(Exception e){
        	log.error("contexto:",e);
        	System.err.println("Erro ao verificar presenca winker");
        	e.printStackTrace();
        	
        	try
			{
				ret.put("status", 98);
				ret.put("message", "CPF não informado.");
				ret.put("working", false);
				out.print(ret.toString());
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
        	
        }
	}

	
	public static boolean retornaPresencaColaborador(Long numcpf){
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try
		{
			conn = PersistEngine.getConnection("VETORH");
			StringBuffer sql = new StringBuffer();

			sql.append(" DECLARE @DatRef  Datetime  "); 
	        sql.append(" DECLARE @NUMCAD numeric ");
	        sql.append(" DECLARE @NUMEMP numeric ");
	        sql.append(" DECLARE @TIPCOL numeric ");
			sql.append(" SELECT @DatRef = getdate() "); 
			sql.append(" select @NUMCAD = numcad, @NUMEMP = numemp, @TIPCOL = tipcol from r034fun  where numcpf = ? and sitafa <> 7 ");  
			sql.append(" "); 
			sql.append(" SELECT fun.numemp, fun.numcad, fun.nomfun, orn.numloc, orn.usu_telloc "); 
			sql.append(" FROM R034FUN fun "); 
			sql.append(" INNER JOIN R038HCH hch ON  fun.numemp = hch.numemp and fun.numcad = hch.numcad and fun.tipcol = hch.tipcol and hch.datfim = '1900-12-31 00:00:00' ");    
			sql.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp  AND hlo.TipCol = fun.TipCol ");    
			sql.append("                                                                  AND hlo.NumCad = fun.NumCad ");    
			sql.append("                                                                  AND hlo.DatAlt = ( "); 
			sql.append("                                                                                     SELECT MAX (DATALT) ");    
			sql.append("                                                                                     FROM R038HLO TABELA001 ");     
			sql.append("                                                                                     WHERE TABELA001.NUMEMP = hlo.NUMEMP ");    
			sql.append("                                                                                     AND TABELA001.TIPCOL = hlo.TIPCOL ");    
			sql.append("                                                                                     AND TABELA001.NUMCAD = hlo.NUMCAD ");    
			sql.append("                                                                                     AND TABELA001.DATALT <= @DatRef ");    
			sql.append("                                                                                   ) ");   
			sql.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg ");  
			sql.append(" LEFT JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= @DatRef) ");    
			sql.append(" LEFT JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");    
			sql.append(" left JOIN R070ACC ace WITH (NOLOCK) ON ace.TipAcc = 100 AND ace.NumCra = hch.NumCra AND ace.USU_NumLoc = orn.NumLoc AND ace.DirAcc = 'E' ");  
			sql.append("                                 AND DATEADD(SECOND, ace.SeqAcc, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc)) = (SELECT MAX(DATEADD(SECOND, ace2.SeqAcc, DATEADD(MINUTE, ace2.HorAcc, ace2.DatAcc))) FROM R070ACC ace2 WITH (NOLOCK) WHERE ace2.TipAcc = 100 AND ace2.NumCra = ace.NumCra AND ace2.USU_NumLoc = ace.USU_NumLoc AND ace2.DirAcc = ace.DirAcc  ) ");   
			sql.append(" left JOIN R070ACC acs WITH (NOLOCK) ON acs.TipAcc = 100 AND acs.NumCra = hch.NumCra and acs.USU_NumLoc = orn.NumLoc AND acs.DirAcc = 'S' ");  
			sql.append("                                 AND DATEADD(SECOND, acs.SeqAcc, DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) = (SELECT MAX(DATEADD(SECOND, acs2.SeqAcc, DATEADD(MINUTE, acs2.HorAcc, acs2.DatAcc))) FROM R070ACC acs2 WITH (NOLOCK) WHERE acs2.TipAcc = 100 AND acs2.NumCra = acs.NumCra AND acs2.USU_NumLoc = acs.USU_NumLoc AND acs2.DatAcc = acs.DatAcc AND acs2.DirAcc = acs.DirAcc AND DATEADD(MINUTE, acs2.HorAcc, acs2.DatAcc) > DATEADD(MINUTE, ace.HorAcc, ace.DatAcc)) ");   
			sql.append(" WHERE 1=1 ");  
			sql.append(" and @DatRef >= DATEADD(MINUTE, ace.HorAcc, ace.DatAcc) ");  
			sql.append(" and @DatRef <= (case when acs.HorAcc is not null then DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) else getdate() end ) ");  
			sql.append(" and fun.numcad = @NUMCAD and fun.numemp = @NUMEMP and fun.tipcol = @TIPCOL ");
			 			
			pst = conn.prepareStatement(sql.toString());
			
			pst.setLong(1, numcpf);
    		
			
			rs = pst.executeQuery();
			
			if(rs.next()){
				return true;
			}
			
			return false;
		}catch (Exception e){
			e.printStackTrace();
			return false;
		}finally{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}
	}

}
