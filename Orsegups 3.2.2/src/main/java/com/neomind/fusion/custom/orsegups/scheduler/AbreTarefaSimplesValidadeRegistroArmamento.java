package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesValidadeRegistroArmamento implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(AbreTarefaSimplesValidadeRegistroArmamento.class);

	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		final GregorianCalendar dataValidade = new GregorianCalendar();

		
		dataValidade.add(Calendar.DAY_OF_MONTH, +60);
		dataValidade.set(Calendar.HOUR_OF_DAY, 0);
		dataValidade.set(Calendar.MINUTE, 0);
		dataValidade.set(Calendar.SECOND, 0);
		dataValidade.set(Calendar.MILLISECOND, 0);

		//para testes
		/*dataValidade.set(Calendar.DAY_OF_MONTH, 19);
		dataValidade.set(Calendar.MONTH, 02);
		dataValidade.set(Calendar.YEAR, 2016);
		dataValidade.set(Calendar.HOUR_OF_DAY, 0);
		dataValidade.set(Calendar.MINUTE, 0);
		dataValidade.set(Calendar.SECOND, 0);
		dataValidade.set(Calendar.MILLISECOND, 0);
		System.out.println(NeoDateUtils.safeDateFormat(dataValidade, "dd/MM/yyyy"));*/
		
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Validade Registro Armamento - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try
		{
			Collection<NeoObject> empresas = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("EEMP"));

			if (NeoUtils.safeIsNotNull(empresas) && !empresas.isEmpty())
			{
				for (NeoObject objItemEmpresa : empresas)
				{
					EntityWrapper itemEmpresaWrapper = new EntityWrapper(objItemEmpresa);
					final Long empresa = (Long) itemEmpresaWrapper.getValue("codemp");

					Collection<NeoObject> regionais = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCEscritorioRegional"));

					if (NeoUtils.safeIsNotNull(regionais) && !regionais.isEmpty())
					{
						for (NeoObject objItemRegional : regionais)
						{
							EntityWrapper itemRegionalWrapper = new EntityWrapper(objItemRegional);
							final Long regional = (Long) itemRegionalWrapper.getValue("codigo");

							QLGroupFilter filter = new QLGroupFilter("AND");
							QLEqualsFilter filtroData = new QLEqualsFilter("dataValidade", dataValidade);
							QLEqualsFilter filtroRegional = new QLEqualsFilter("regionalResponsavel.usu_codreg", regional);
							QLEqualsFilter filtroEmpresa = new QLEqualsFilter("empresa.codemp", empresa);
							QLGroupFilter statusGroupFilter = new QLGroupFilter("OR");
							QLEqualsFilter filtroSituacaoAtivo = new QLEqualsFilter("situacao.codigoSituacao", 1L);
							QLEqualsFilter filtroSituacaoVencido = new QLEqualsFilter("situacao.codigoSituacao", 4L);
							statusGroupFilter.addFilter(filtroSituacaoAtivo);
							statusGroupFilter.addFilter(filtroSituacaoVencido);
							filter.addFilter(filtroData);
							filter.addFilter(filtroRegional);
							filter.addFilter(filtroEmpresa);
							filter.addFilter(statusGroupFilter);
							
							final Collection<NeoObject> armas = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ARMArmamento"), filter, -1, -1, "numeroRegistro asc");
							PersistEngine.getEntityManager().flush();

							if (NeoUtils.safeIsNotNull(armas) && !armas.isEmpty())
							{
								final NeoRunnable work = new NeoRunnable()
								{
									public void run() throws Exception
									{
										//Código da sub-transação
										abrirTarefa(armas, regional, dataValidade);
									}
								};
								try
								{
									PersistEngine.managedRun(work);
								}
								catch (final Exception e)
								{
									log.error(e.getMessage(), e);
								}
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Validade Registro Armamento");
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Validade Registro Armamento");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Validade Registro Armamento - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	public void abrirTarefa(Collection<NeoObject> armas, Long regional, GregorianCalendar dataValidade)
	{

		String empresa = "";
		StringBuilder html = new StringBuilder();

		html.append("Prezado Gestor,<br>");
		html.append("Por gentileza, providenciar a renovação dos registros das armas em anexo, na qual nosso sistema aponta o vencimento em " + NeoDateUtils.safeDateFormat(dataValidade, "dd/MM/yyyy") + "<br><br>");

		try
		{
			String filename = "\\\\ssoovt09\\f$\\Sistemas\\Fusion\\Temp\\ListaDeArmamentosVencendo.xls";
			FileOutputStream fileOut = new FileOutputStream(filename);
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Armamentos Vencendo");

			HSSFRow rowhead = sheet.createRow((short) 0);
			rowhead.createCell(0).setCellValue("Espécie");
			rowhead.createCell(1).setCellValue("Marca");
			rowhead.createCell(2).setCellValue("Calibre");
			rowhead.createCell(3).setCellValue("Número Arma");
			rowhead.createCell(4).setCellValue("Lotação");
			rowhead.createCell(5).setCellValue("Situação");
			rowhead.createCell(6).setCellValue("Validade");

			int i = 1;
			for (NeoObject objItemArma : armas)
			{
				EntityWrapper itemArmaWrapper = new EntityWrapper(objItemArma);
				empresa = String.valueOf(itemArmaWrapper.findValue("empresa.codemp"));
				String especie = (String) itemArmaWrapper.findValue("especie.descricaoEspecie");
				String marca = (String) itemArmaWrapper.findValue("marca.descricaoMarca");
				String calibre = (String) itemArmaWrapper.findValue("calibre");
				String numeroArma = (String) itemArmaWrapper.findValue("numeroArma");
				String lotacao = (String) itemArmaWrapper.findValue("posto.posto.nomloc");
				String situacao = (String) itemArmaWrapper.findValue("situacao.descricaoSituacao");
				String validade = NeoDateUtils.safeDateFormat((GregorianCalendar) itemArmaWrapper.findValue("dataValidade"), "dd/MM/yyyy"); 

				HSSFRow row = sheet.createRow((short) i);
				row.createCell(0).setCellValue(especie);
				row.createCell(1).setCellValue(marca);
				row.createCell(2).setCellValue(calibre);
				row.createCell(3).setCellValue(numeroArma);
				row.createCell(4).setCellValue(lotacao);
				row.createCell(5).setCellValue(situacao);
				row.createCell(6).setCellValue(validade);

				i++;
			}

			workbook.write(fileOut);
			fileOut.close();
		}
		catch (Exception ex)
		{
			System.out.println(ex);
		}

		NeoFile anexo = OrsegupsUtils.criaNeoFile(new File("\\\\ssoovt09\\f$\\Sistemas\\Fusion\\Temp\\ListaDeArmamentosVencendo.xls"));

		NeoPaper responsavel = new NeoPaper();
		String executor = "";

		if (regional > 0L)
		{
			responsavel = OrsegupsUtils.getPapelGerenteRegional(regional);
			if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
			{
				for (NeoUser user : responsavel.getUsers())
				{
					executor = user.getCode();
					break;
				}
			}
		}
		else
		{
			executor = "francini.guedes";
		}
		String solicitante = "francini.guedes";

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = dataValidade;		
		prazo.add(Calendar.DAY_OF_MONTH, -15);
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);

		String titulo = "Vencimento de Registro de Armamento da Empresa " + empresa + " - " + NeoDateUtils.safeDateFormat(dataValidade, "dd/MM/yyyy");
		String descricao = NeoUtils.safeString(html.toString());

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo, anexo);

		log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Validade Registro Armamento - Tarefa: " + NeoUtils.safeString(tarefa));
	}
}
