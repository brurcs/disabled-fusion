package com.neomind.fusion.custom.orsegups.fap.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.mail.EmailException;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTaticoDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.FornecedorDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.ItemOrcamentoDTO;
import com.neomind.fusion.custom.orsegups.fap.factory.FapFactory;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

/**
 * @author diogo.silva
 *         Classe que monta o e-mail enviado nas atividades de
 *         script "Notificar Fornecedor - Encaminhar NF" e "Enviar E-mail Autorização de Faturamento".
 * */
public class FAPEmailUtils
{

	/**
	 * Este método encapsula todo o e-mail e chama os métodos de construção do html enviado ao fluxo como
	 * corpo do e-mail.
	 * 
	 * @param fapAgrupadora - Objeto NeoObject. É o formulário da aplicação que está disparando o e-mail.
	 * @return email - Objeto String. É o e-mail completo.
	 * @throws Exception Para exceções generalizadas.
	 * */
	public static String autorizacaoFaturamentoFapTatico(NeoObject fapAgrupadora) throws Exception
	{
		String codigoTarefaAgrupadora = null;
		StringBuilder email = new StringBuilder();

		// Lista deve conter todas as FAP's agrupadas
		List<NeoObject> listaFapsAgrupadas = new ArrayList<NeoObject>();
		EntityWrapper ew = new EntityWrapper(fapAgrupadora);
		codigoTarefaAgrupadora = ew.findGenericValue("wfprocess.code");
		QLEqualsFilter codigoTarefaFilter = new QLEqualsFilter("codigoTarefa", codigoTarefaAgrupadora);
		listaFapsAgrupadas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), codigoTarefaFilter);

		/* Se a FAP for individual a agrupadora vai na lista. */
		if (listaFapsAgrupadas.size() == 0)
		{
			listaFapsAgrupadas.add(fapAgrupadora);
		}
		email.append("<html>");
		email.append("<head>");
		email.append(montaCabecalhoEmailFapTatico());
		email.append("</head>");
		email.append("<body>");
		email.append(montaCorpoEmailFapTatico(listaFapsAgrupadas, fapAgrupadora));
		email.append("</body>");
		email.append("</html>");

		return email.toString();

	}

	/**
	 * Este método encapsula todo o e-mail e chama os métodos de construção do html enviado ao fluxo como
	 * corpo do e-mail.
	 * 
	 * @param fapAgrupadora - Objeto NeoObject. É o formulário da aplicação que está disparando o e-mail.
	 * @return email - Objeto String. É o e-mail completo.
	 * @throws Exception Para exceções generalizadas.
	 * */
	public static String autorizacaoFaturamentoFapTecnico(NeoObject fapAgrupadora) throws Exception
	{
		String codigoTarefaAgrupadora = null;
		StringBuilder email = new StringBuilder();

		// Lista deve conter todas as FAP's agrupadas
		List<NeoObject> listaFapsAgrupadas = new ArrayList<NeoObject>();
		EntityWrapper ew = new EntityWrapper(fapAgrupadora);
		codigoTarefaAgrupadora = ew.findGenericValue("wfprocess.code");
		QLEqualsFilter codigoTarefaFilter = new QLEqualsFilter("codigoTarefa", codigoTarefaAgrupadora);
		listaFapsAgrupadas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), codigoTarefaFilter);

		/* Se a FAP for individual a agrupadora vai na lista. */
		if (listaFapsAgrupadas.size() == 0)
		{
			listaFapsAgrupadas.add(fapAgrupadora);
		}
		email.append("<html>");
		email.append("<head>");
		email.append(montaCabecalhoEmailFapTatico());
		email.append("</head>");
		email.append("<body>");
		email.append(montaCorpoEmailFapTecnico(listaFapsAgrupadas, fapAgrupadora));
		email.append("</body>");
		email.append("</html>");

		return email.toString();

	}

	/**
	 * Este método constroi o head do html. Também contém o style da página.
	 * 
	 * @throws Exception Para exceções generalizadas.
	 * */
	public static String montaCabecalhoEmailFapTatico() throws Exception
	{
		StringBuilder cabecalho = new StringBuilder();

		cabecalho.append("<style>");
		cabecalho.append(".divGeral {");
		cabecalho.append("font-family: sans-serif;");
		cabecalho.append("font-size: 16px;");
		cabecalho.append("margin: 0 10%;");
		cabecalho.append("width: 100%;");
		cabecalho.append("}");
		cabecalho.append(".avisos ol {");
		cabecalho.append("padding: 0px 15px;");
		cabecalho.append("}");
		cabecalho.append("body {");
		cabecalho.append("margin: 0px;");
		cabecalho.append("padding: 0px;");
		cabecalho.append("}");
		cabecalho.append("table {");
		cabecalho.append("font-size: 16px;");
		cabecalho.append("font-family: sans-serif;");
		cabecalho.append("border: 1px solid gray;");
		cabecalho.append("border-collapse: collapse;");
		cabecalho.append("width: 69%;");
		cabecalho.append("}");
		cabecalho.append("img {");
		cabecalho.append("width = 69%;");
		cabecalho.append("}");
		cabecalho.append("#rota {");
		cabecalho.append("text-align: left;");
		cabecalho.append("}");
		cabecalho.append(".pagamentos th {");
		cabecalho.append("font-weight: normal;");
		cabecalho.append("border: 1px solid gray;");
		cabecalho.append("padding: 2px;");
		cabecalho.append("text-align: center;");
		cabecalho.append("}");
		cabecalho.append(".pagamentos td {");
		cabecalho.append("font-weight: normal;");
		cabecalho.append("border: 1px solid gray;");
		cabecalho.append("width: 5%;");
		cabecalho.append("padding: 2px;");
		cabecalho.append("text-align: center;");
		cabecalho.append("}");
		cabecalho.append("#subtotal {");
		cabecalho.append("text-align: right;");
		cabecalho.append("}");
		cabecalho.append("#totalGeral {");
		cabecalho.append("text-align: right;");
		cabecalho.append("}");
		cabecalho.append("</style>");

		return cabecalho.toString();

	}

	/**
	 * Este métopdo monta o body do e-mail.
	 * 
	 * @param listaFaps - Objeto List contendo NeoObjects. São as FAP's agrupadas - ou uma única FAP caso
	 *            seja individual - que serão utilizadas
	 *            para preencher as informações no corpo do e-mail.
	 * @param fapAgrupadora - É o formulário da FAP agrupadora para que seja mais fácil identificar a
	 *            mesma na hora de recuperar as informações.
	 * @return corpoEmail - Objeto String. É o body do e-mail.
	 * @throws Exception Para exceções generalizadas.
	 * */
	public static String montaCorpoEmailFapTatico(List<NeoObject> listaFaps, NeoObject fapAgrupadora) throws Exception
	{

		List<NeoObject> faps = listaFaps;
		NeoObject agrupadora = fapAgrupadora;
		StringBuilder corpoEmail = new StringBuilder();
		DecimalFormat formatter = new DecimalFormat("#0.00");
		double totalGeral = 0;
		double subtotal = 0;

		/* Dados gerais que são iguais para todas as FAP's, tanto na agrupadora quanto nas agrupadas */
		EntityWrapper peAgrupadora = new EntityWrapper(agrupadora);
		NeoObject fornecedor = (NeoObject) peAgrupadora.findValue("fornecedor");
		FornecedorDTO fornecedorDTO = FapUtils.retornaDadosFornecedor(fornecedor);
		Long codigoRota = (Long) peAgrupadora.findField("rotaSigma.rotaSigma.cd_rota").getValue();
		NeoObject modalidade = FapUtils.consultaModalidade(codigoRota);
		EntityWrapper ewModalidade = new EntityWrapper(modalidade);
		Long codigoModalidade = (Long) ewModalidade.findField("codigo").getValue();
		String descricaoModalidade = (String) ewModalidade.findField("descricao").getValue();
		Integer colspan = null;

		/* Dependendo da modalidade tem uma coluna a mais */
		if (codigoModalidade == 1)
		{
			colspan = 4;
		}
		else if (codigoModalidade == 2 || codigoModalidade == 3)
		{
			colspan = 5;
		}
		corpoEmail.append("");
		corpoEmail.append(" <div class = divGeral> ");
		corpoEmail.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/header.jpg\" height='180' width='72%'>");
		corpoEmail.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'> ");
		corpoEmail.append(" <div class = header> ");
		corpoEmail.append(" <p><strong>AUTORIZAÇÃO DE FATURAMENTO</strong></p> ");
		corpoEmail.append(" </div> ");
		corpoEmail.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'> ");
		/* Preenche os dados fornecedor no e-mail */
		corpoEmail.append(" <div class=dadosFornecedor> ");
		corpoEmail.append(" <p>Dados do Fornecedor</p>");
		corpoEmail.append(" <p>CNPJ: " + fornecedorDTO.getCgcCpf() + "</p>");
		corpoEmail.append(" <p>Razão: " + fornecedorDTO.getNomeFornecedor() + "</p>");
		corpoEmail.append(" </div>");
		corpoEmail.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'>");
		/* Preenche os dados do servico no e-mail */
		corpoEmail.append(" <div class = dadosServico>");
		corpoEmail.append(" <p>Dados do Serviço Prestado</p>");
		/* Inicio da tabela de pagamentos */
		corpoEmail.append(" <table class ='pagamentos'> ");
		corpoEmail.append(" <thead> ");
		corpoEmail.append(" <tr> ");
		corpoEmail.append(" <th>FAP</th> ");
		corpoEmail.append(" <th>Item</th> ");
		corpoEmail.append(" <th>Modalidade</th> ");
		if (codigoModalidade == 3)
		{
			corpoEmail.append(" <th>Qtd. Atendimentos</th> ");
			corpoEmail.append(" <th>Valor Atendimento</th> ");
		}
		else
		{
			corpoEmail.append(" <th>Qtd. Contas</th> ");
		}
		if (codigoModalidade == 2)
		{
			corpoEmail.append(" <th>Valor Conta</th> ");
		}
		corpoEmail.append(" <th>Total Rota</th> ");
		corpoEmail.append(" </tr> ");
		corpoEmail.append(" </thead> ");
		/*
		 * Inicio do preenchimento dos dados do pagamento - Cada linha é uma FAP e suas respectivas
		 * informações
		 */
		corpoEmail.append(" <tbody> ");
		// Cada FAP deve preencher os campos abaixo, gerando uma linha de registro para cada uma no e-mail
		for (NeoObject fap : faps)
		{

			EntityWrapper ewFap = new EntityWrapper(fap);
			String codigoTarefa = (String) ewFap.findValue("wfprocess.code");
			Long codigoAplicacao = (Long) ewFap.findValue("aplicacaoPagamento.codigo");

			Long rota = (Long) ewFap.findField("rotaSigma.rotaSigma.cd_rota").getValue();
			NeoObject sigmaRota = FapFactory.neoObjectFactory("SIGMAROTA", "cd_rota", rota);
			List<NeoObject> listaEventos = ewFap.findGenericValue("historicoEventos");
			List<NeoObject> listaDeContas = ewFap.findGenericValue("historicoContas");
			Long quantidadeContas = 0L;
			Long quantidadeEventos = 0L;

			NeoObject aplicacaoTatico = FapFactory.neoObjectFactory("FAPAplicacaoTatico", "rotaSigma", sigmaRota);
			EntityWrapper ewAplicacao = new EntityWrapper(aplicacaoTatico);
			BigDecimal valorRota = (BigDecimal) ewAplicacao.findField("valorRota").getValue();
			List<ItemOrcamentoDTO> listaItensOrcamento = AplicacaoTaticoDAO.consultaItemOrcamento(codigoTarefa, codigoAplicacao);
			ItemOrcamentoDTO itemOrcamento = listaItensOrcamento.get(0);

			if (codigoModalidade == 3)
			{
				quantidadeEventos = new Long(listaEventos.size());
				subtotal = valorRota.multiply(new BigDecimal(quantidadeEventos)).floatValue();
			}
			else
			{
				quantidadeContas = new Long(listaDeContas.size());
				subtotal = valorRota.multiply(new BigDecimal(quantidadeContas)).floatValue();
			}

			corpoEmail.append(" <tr> ");
			corpoEmail.append(" <td>" + codigoTarefa + "</td> ");
			corpoEmail.append(" <td>" + itemOrcamento.getDescricao() + "</td> ");
			corpoEmail.append(" <td>" + descricaoModalidade + "</td> ");
			if (codigoModalidade == 2)
			{ // Por Conta
				corpoEmail.append(" <td>" + quantidadeContas + "</td> ");
				corpoEmail.append(" <td>R$ " + formatter.format(valorRota.doubleValue()) + "</td> "); // Valor por conta
				corpoEmail.append(" <td>R$ " + formatter.format(subtotal) + "</td> "); // Total Rota
			}
			else if (codigoModalidade == 1)
			{ // Fixo Mensal
				corpoEmail.append(" <td>" + quantidadeContas + "</td> ");
				corpoEmail.append(" <td>R$ " + formatter.format(valorRota.doubleValue()) + "</td> "); // Total Rota
			}
			else if (codigoModalidade == 3)
			{ // Por Atendimento
				corpoEmail.append(" <td>" + quantidadeEventos + "</td> ");
				corpoEmail.append(" <td>R$ " + formatter.format(valorRota.doubleValue()) + "</td> "); // Valor por conta
				corpoEmail.append(" <td>R$ " + formatter.format(subtotal) + "</td> "); // Total Rota 
			}
			corpoEmail.append(" </tr> ");
			corpoEmail.append("<tr>  ");
			corpoEmail.append("<tbody> ");
			corpoEmail.append("<tr> ");
			corpoEmail.append("<td id=subtotal colspan='" + colspan + "'>Subtotal:</td> ");
			if (codigoModalidade == 2 || codigoModalidade == 3)
			{ // Por Conta
				corpoEmail.append("<td>R$ " + formatter.format(subtotal) + "</td>");
				totalGeral = totalGeral + subtotal;					
			}
			else if (codigoModalidade == 1)
			{ // Fixo Mensal
				corpoEmail.append("<td>R$ " + formatter.format(valorRota.doubleValue()) + "</td>");
				totalGeral = totalGeral + valorRota.doubleValue();
			}
			corpoEmail.append("</tr> ");
			corpoEmail.append(" </tbody>");
			corpoEmail.append("</tr> ");
		}
		// Fim do preenchimento das informações de pagamentos das FAP's
		System.out.println(formatter.format(totalGeral));
		corpoEmail.append("<tr> ");
		corpoEmail.append("<td colspan='" + colspan + "' id=totalGeral>Total Geral:</td> ");
		corpoEmail.append("<td>R$ " + formatter.format(totalGeral) + "</td> ");
		corpoEmail.append("</tr> ");
		corpoEmail.append("</tbody>");
		corpoEmail.append("</table>");
		corpoEmail.append("</div>");
		/* Fim do preenchimento dos dados do pagamento */
		corpoEmail.append("<br/>");
		corpoEmail.append("<img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'>");
		corpoEmail.append(montaInfoComplementaresFapTatico());
		corpoEmail.append("<img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'>");
		corpoEmail.append("<div class= footer>");
		corpoEmail.append("<img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/footer-1.jpg\" width='72%'>");
		corpoEmail.append("</div>");
		corpoEmail.append("</div>");

		return corpoEmail.toString();
	}

	/**
	 * As informações complementares ficam no corpo do e-mail porém estão separadas em caso de
	 * necessidade de alguma mudança.
	 * 
	 * @return infoComplementares. Objeto String. Contém as informações complementares para FAP Tático.
	 * */
	public static String montaInfoComplementaresFapTatico()
	{
		StringBuilder infoComplementares = new StringBuilder();

		infoComplementares.append("<div class='avisos' width='69%'>");
		infoComplementares.append("<br><strong>INFORMAÇÕES COMPLEMENTARES: </strong>");
		infoComplementares.append("<br><br>");
		infoComplementares.append("<strong>1)<font color='FF0000'> DADOS PARA EMISSÃO DA NOTA - ATENÇÃO</font></strong>");
		infoComplementares.append("<br><br>");
		infoComplementares.append("A partir de 01/05/2020, emitir a nota fiscal contra a seguinte empresa abaixo.<br> Empresa registrada no Paraná utilizar o CNPJ da Filial de Curitiba. Já os demais, podem utilizar os dados de São José.");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<table class='pagamentos' border='2px'>");
    	infoComplementares.append("<tr>");
    	infoComplementares.append("<td><strong>Orsegups Monitoramento Eletrônico Ltda</strong></td>	 <td rowspan=2>08.491.597/0001-26</td></tr>");
    	infoComplementares.append("<tr><td>Rua Getúlio Vargas, 2729, Centro, São José/SC - CEP: 88.103-400</td></tr>	");
    	infoComplementares.append("<tr><td><strong>Orsegups Monitoramento Eletrônico Ltda</strong></td> <td rowspan=2>08.491.597/0002-07</td></tr>");
    	infoComplementares.append("<tr><td>Av. Presidente Kennedy, 1622, Rebouças, Curitiba/PR - CEP 80.220-201</td></tr>");
    	infoComplementares.append("</tr>");
    	infoComplementares.append("</table>	");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<strong>2) PRAZOS PARA ENVIO:</strong><br>");
    	infoComplementares.append("A nota fiscal e o boleto devem ser enviados IMEDIATAMENTE para ORSEGUPS, em até 5 dias úteis antes do vencimento.");
    	infoComplementares.append("<br>Atentar que deverá ser um boleto por nota fiscal.");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<strong>3) ENDEREÇO PARA ENVIO:</strong><br>");
    	infoComplementares.append("CORREIO: Rua Getúlio Vargas, Nº: 2729, Centro, São José/SC - CEP: 88.103-400. A/C Depto de Controladoria.");
    	infoComplementares.append("<br>E-MAIL: <a href='mailto:fap@orsegups.com.br'>fap@orsegups.com.br</a>. (*Notas Fiscais Eletrônicas devem ser enviadas apenas por e-mail.)");
    	infoComplementares.append("<br><br><br>");
    	infoComplementares.append("Dúvidas quanto a emissão de sua nota fiscal ou informações de pagamentos podem ser obtidas através do e-mail: <a href='mailto:fap@orsegups.com.br'>fap@orsegups.com.br</a> ou através do telefone (48) 3381-6652.");
    	infoComplementares.append("</div>");
		
		return infoComplementares.toString();

	}

	/**
	 * Este métopdo monta o body do e-mail.
	 * 
	 * @param listaFaps - Objeto List contendo NeoObjects. São as FAP's agrupadas - ou uma única FAP caso
	 *            seja individual - que serão utilizadas
	 *            para preencher as informações no corpo do e-mail.
	 * @param fapAgrupadora - É o formulário da FAP agrupadora para que seja mais fácil identificar a
	 *            mesma na hora de recuperar as informações.
	 * @return corpoEmail - Objeto String. É o body do e-mail.
	 * @throws Exception Para exceções generalizadas.
	 * */
	public static String montaCorpoEmailFapTecnico(List<NeoObject> listaFaps, NeoObject fapAgrupadora) throws Exception
	{

		List<NeoObject> faps = listaFaps;
		NeoObject agrupadora = fapAgrupadora;
		StringBuilder corpoEmail = new StringBuilder();
		DecimalFormat formatter = new DecimalFormat("#0.00");
		double totalGeral = 0;
		double subtotal = 0;

		/* Dados gerais que são iguais para todas as FAP's, tanto na agrupadora quanto nas agrupadas */
		EntityWrapper peAgrupadora = new EntityWrapper(agrupadora);
		NeoObject fornecedor = (NeoObject) peAgrupadora.findValue("fornecedor");
		FornecedorDTO fornecedorDTO = FapUtils.retornaDadosFornecedor(fornecedor);
		Long codTecnico = peAgrupadora.findGenericValue("tecnicoSigma.tecnico.cd_colaborador");
		NeoObject modalidade = FapUtils.consultaModalidadeTecnico(codTecnico);
		EntityWrapper ewModalidade = new EntityWrapper(modalidade);
		Long codigoModalidade = ewModalidade.findGenericValue("codigo");
		String descricaoModalidade = ewModalidade.findGenericValue("descricao");
		Integer colspan = null;

		/* Dependendo da modalidade tem colunas a mais */
		if (codigoModalidade == 1)
		{
			colspan = 3;
		}
		else if (codigoModalidade == 2)
		{
			colspan = 4;
		}
		else if (codigoModalidade == 3)
		{
			colspan = 5;
		}
		corpoEmail.append("");
		corpoEmail.append(" <div class = divGeral> ");
		corpoEmail.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/header.jpg\" height='180' width='72%'>");
		corpoEmail.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'> ");
		corpoEmail.append(" <div class = header> ");
		corpoEmail.append(" <p><strong>AUTORIZAÇÃO DE FATURAMENTO</strong></p> ");
		corpoEmail.append(" </div> ");
		corpoEmail.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'> ");
		/* Preenche os dados fornecedor no e-mail */
		corpoEmail.append(" <div class=dadosFornecedor> ");
		corpoEmail.append(" <p>Dados do Fornecedor</p>");
		corpoEmail.append(" <p>CNPJ: " + fornecedorDTO.getCgcCpf() + "</p>");
		corpoEmail.append(" <p>Razão: " + fornecedorDTO.getNomeFornecedor() + "</p>");
		corpoEmail.append(" </div>");
		corpoEmail.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'>");
		/* Preenche os dados do servico no e-mail */
		corpoEmail.append(" <div class = dadosServico>");
		corpoEmail.append(" <p>Dados do Serviço Prestado</p>");
		/* Inicio da tabela de pagamentos */
		corpoEmail.append(" <table class ='pagamentos'> ");
		corpoEmail.append(" <thead> ");
		corpoEmail.append(" <tr> ");
		corpoEmail.append(" <th>FAP</th> ");
		corpoEmail.append(" <th>Item</th> ");
		corpoEmail.append(" <th>Modalidade</th> ");
		if (codigoModalidade == 3)
		{
			corpoEmail.append(" <th>Qtd. Atendimentos</th> ");
			corpoEmail.append(" <th>Valor Atendimento</th> ");
		}
		if (codigoModalidade == 2)
		{
			//corpoEmail.append(" <th>Qtd. Contas</th> ");
			corpoEmail.append(" <th>Valor Contas</th> ");
		}
		corpoEmail.append(" <th>Total</th> ");
		corpoEmail.append(" </tr> ");
		corpoEmail.append(" </thead> ");
		/*
		 * Inicio do preenchimento dos dados do pagamento - Cada linha é uma FAP e suas respectivas
		 * informações
		 */
		corpoEmail.append(" <tbody> ");
		// Cada FAP deve preencher os campos abaixo, gerando uma linha de registro para cada uma no e-mail
		for (NeoObject fap : faps)
		{

			EntityWrapper ewFap = new EntityWrapper(fap);
			String codigoTarefa = (String) ewFap.findValue("wfprocess.code");
			Long codigoAplicacao = (Long) ewFap.findValue("aplicacaoPagamento.codigo");

			Long tecnico = (Long) ewFap.findGenericValue("tecnicoSigma.tecnico.cd_colaborador");
			NeoObject noTecnico = FapFactory.neoObjectFactory("SIGMACOLABORADOR", "cd_colaborador", tecnico);

			List<NeoObject> listaEventos = ewFap.findGenericValue("historicoEventos");
			List<NeoObject> listaDeContas = ewFap.findGenericValue("historicoContas");
			Long quantidadeContas = 0L;
			Long quantidadeEventos = 0L;

			NeoObject aplicacaoTecnico = FapFactory.neoObjectFactory("FAPAplicacaoTecnico", "tecnico", noTecnico);
			EntityWrapper ewAplicacao = new EntityWrapper(aplicacaoTecnico);

			BigDecimal valorCFTV = (BigDecimal) ewAplicacao.findGenericValue("valorCFTV");
			BigDecimal valorAlarme = (BigDecimal) ewAplicacao.findGenericValue("valorAlarme");

			List<ItemOrcamentoDTO> listaItensOrcamento = AplicacaoTaticoDAO.consultaItemOrcamento(codigoTarefa, codigoAplicacao);
			for (ItemOrcamentoDTO itemOrcamento : listaItensOrcamento)
			{
				Integer qtdContas = 0;
				BigDecimal valor = new BigDecimal(0);
				
				//TecnicoDTO tecDTO = AplicacaoTecnicoDAO.consultaTotalPagamentos(tecnico);

				if (itemOrcamento.getDescricao().contains("CFTV"))
				{
					//qtdContas = tecDTO.getQtdContaCFTV();
					valor = valorCFTV;
				}
				else
				{
					//qtdContas = tecDTO.getQtdContaAlarme();
					valor = valorAlarme;
				}

				if (codigoModalidade == 2)
				{
					//subtotal = valor.multiply(new BigDecimal(qtdContas)).floatValue();
					subtotal = itemOrcamento.getValor().doubleValue();
				} else 
				{
					subtotal = valor.floatValue();
				}

				corpoEmail.append(" <tr> ");
				corpoEmail.append(" <td>" + codigoTarefa + "</td> ");
				corpoEmail.append(" <td>" + itemOrcamento.getDescricao() + "</td> ");
				corpoEmail.append(" <td>" + descricaoModalidade + "</td> ");
				if (codigoModalidade == 2)
				{ // Por Conta
				//	corpoEmail.append(" <td>" + qtdContas + "</td> ");
					corpoEmail.append(" <td>R$ " + formatter.format(valor.doubleValue()) + "</td> "); // Valor por conta
					corpoEmail.append(" <td>R$ " + formatter.format(subtotal) + "</td> "); // Total
				} else
				{
					corpoEmail.append(" <td>R$ " + formatter.format(subtotal) + "</td> "); // Total
				}
				corpoEmail.append(" </tr> ");

				if (codigoModalidade == 2)
				{
					//Por Conta
					corpoEmail.append("<tr>  ");
					corpoEmail.append("    <tbody> ");
					corpoEmail.append("        <tr> ");
					corpoEmail.append("            <td id=subtotal colspan='" + colspan + "'>Subtotal:</td> ");
					corpoEmail.append("            <td>R$ " + formatter.format(subtotal) + "</td>");
					corpoEmail.append("        </tr> ");
					corpoEmail.append("    </tbody>");
					corpoEmail.append("</tr> ");
				}
				
				if(itemOrcamento.getTipo().equals("DESCONTO")) {
					totalGeral = totalGeral - subtotal;										
				} else {
					totalGeral = totalGeral + subtotal;					
				}
			}
		}
		// Fim do preenchimento das informações de pagamentos das FAP's

		System.out.println(formatter.format(totalGeral));
		corpoEmail.append("<tr> ");
		corpoEmail.append("<td colspan='" + colspan + "' id=totalGeral>Total Geral:</td> ");
		corpoEmail.append("<td>R$ " + formatter.format(totalGeral) + "</td> ");
		corpoEmail.append("</tr> ");
		corpoEmail.append("</tbody>");
		corpoEmail.append("</table>");
		corpoEmail.append("</div>");
		/* Fim do preenchimento dos dados do pagamento */
		corpoEmail.append("<br/>");
		corpoEmail.append("<img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'>");
		corpoEmail.append(montaInfoComplementaresFapTatico());
		corpoEmail.append("<img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/line.jpg\" width='72%'>");
		corpoEmail.append("<div class= footer>");
		corpoEmail.append("<img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailfap/footer-1.jpg\" width='72%'>");
		corpoEmail.append("</div>");
		corpoEmail.append("</div>");

		return corpoEmail.toString();
	}

	/**
	 * As informações complementares ficam no corpo do e-mail porém estão separadas em caso de
	 * necessidade de alguma mudança.
	 * 
	 * @return infoComplementares. Objeto String. Contém as informações complementares para FAP Tático.
	 * */
	public static String montaInfoComplementaresFapTecnico()
	{
		StringBuilder infoComplementares = new StringBuilder();

		infoComplementares.append("<div class='avisos' width='69%'>");
		infoComplementares.append("<br><strong>INFORMAÇÕES COMPLEMENTARES: </strong>");
		infoComplementares.append("<br><br>");
		infoComplementares.append("<strong>1)<font color='FF0000'> DADOS PARA EMISSÃO DA NOTA - ATENÇÃO</font></strong>");
		infoComplementares.append("<br><br>");
		infoComplementares.append("A partir de 01/05/2020, emitir a nota fiscal contra a seguinte empresa abaixo.<br> Empresa registrada no Paraná utilizar o CNPJ da Filial de Curitiba. Já os demais, podem utilizar os dados de São José.");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<table class='pagamentos' border='2px'>");
    	infoComplementares.append("<tr>");
    	infoComplementares.append("<td><strong>Orsegups Monitoramento Eletrônico Ltda</strong></td>	 <td rowspan=2>08.491.597/0001-26</td></tr>");
    	infoComplementares.append("<tr><td>Rua Getúlio Vargas, 2729, Centro, São José/SC - CEP: 88.103-400</td></tr>	");
    	infoComplementares.append("<tr><td><strong>Orsegups Monitoramento Eletrônico Ltda</strong></td> <td rowspan=2>08.491.597/0002-07</td></tr>");
    	infoComplementares.append("<tr><td>Av. Presidente Kennedy, 1622, Rebouças, Curitiba/PR - CEP 80.220-201</td></tr>");
    	infoComplementares.append("</tr>");
    	infoComplementares.append("</table>	");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<strong>2) PRAZOS PARA ENVIO:</strong><br>");
    	infoComplementares.append("A nota fiscal e o boleto devem ser enviados IMEDIATAMENTE para ORSEGUPS, em até 5 dias úteis antes do vencimento.");
    	infoComplementares.append("<br>Atentar que deverá ser um boleto por nota fiscal.");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<strong>3) ENDEREÇO PARA ENVIO:</strong><br>");
    	infoComplementares.append("CORREIO: Rua Getúlio Vargas, Nº: 2729, Centro, São José/SC - CEP: 88.103-400. A/C Depto de Controladoria.");
    	infoComplementares.append("<br>E-MAIL: <a href='mailto:fap@orsegups.com.br'>fap@orsegups.com.br</a>. (*Notas Fiscais Eletrônicas devem ser enviadas apenas por e-mail.)");
    	infoComplementares.append("<br><br><br>");
    	infoComplementares.append("Dúvidas quanto a emissão de sua nota fiscal ou informações de pagamentos podem ser obtidas através do e-mail: <a href='mailto:fap@orsegups.com.br'>fap@orsegups.com.br</a> ou através do telefone (48) 3381-6652.");
    	infoComplementares.append("</div>");
		
		return infoComplementares.toString();

	}
	
	public static String montaInfoComplementaresManutencaoInstalacao()
	{
		StringBuilder infoComplementares = new StringBuilder();
		String dataInicio = "";	
		
		infoComplementares.append("<div class='avisos' width='69%'>");
		infoComplementares.append("<br><strong>INFORMAÇÕES COMPLEMENTARES: </strong>");
		infoComplementares.append("<br><br>");
		infoComplementares.append("<strong>1)<font color='FF0000'> DADOS PARA EMISSÃO DA NOTA - ATENÇÃO</font></strong>");
		infoComplementares.append("<br><br>");
		infoComplementares.append("A partir de 01/05/2020, emitir a nota fiscal contra a seguinte empresa abaixo.<br> Empresa registrada no Paraná utilizar o CNPJ da Filial de Curitiba. Já os demais, podem utilizar os dados de São José.");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<table class='pagamentos' border='2px'>");
    	infoComplementares.append("<tr>");
    	infoComplementares.append("<td><strong>Orsegups Monitoramento Eletrônico Ltda</strong></td>	 <td rowspan=2>08.491.597/0001-26</td></tr>");
    	infoComplementares.append("<tr><td>Rua Getúlio Vargas, 2729, Centro, São José/SC - CEP: 88.103-400</td></tr>	");
    	infoComplementares.append("<tr><td><strong>Orsegups Monitoramento Eletrônico Ltda</strong></td> <td rowspan=2>08.491.597/0002-07</td></tr>");
    	infoComplementares.append("<tr><td>Av. Presidente Kennedy, 1622, Rebouças, Curitiba/PR - CEP 80.220-201</td></tr>");
    	infoComplementares.append("</tr>");
    	infoComplementares.append("</table>	");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<strong>2) PRAZOS PARA ENVIO:</strong><br>");
    	infoComplementares.append("A nota fiscal e o boleto devem ser enviados IMEDIATAMENTE para ORSEGUPS, em até 5 dias úteis antes do vencimento, que é dia 30.");
    	infoComplementares.append("<br>Notas fiscais que chegarem fora deste prazo, serão pagas somente no mês seguinte, obedecendo a mesma data de seu vencimento. ");
    	infoComplementares.append("<br>CADA NOTA FISCAL DEVE TER UM BOLETO. Quem recebe via depósito bancário não há alteração.");
    	infoComplementares.append("<br><br>");
    	infoComplementares.append("<strong>3) ENDEREÇO PARA ENVIO:</strong><br>");
    	infoComplementares.append("<br>E-MAIL: <a href='mailto:fap@orsegups.com.br'>fap@orsegups.com.br</a>. (*Notas Fiscais Eletrônicas devem ser enviadas apenas por e-mail.)");
    	infoComplementares.append("<br>CORREIO: Rua Getúlio Vargas, Nº: 2729, Centro, São José/SC - CEP: 88.103-400. A/C Depto de Controladoria.");
    	infoComplementares.append("<br><br><br>");
    	infoComplementares.append("Dúvidas quanto a emissão de sua nota fiscal ou informações de pagamentos podem ser obtidas através do e-mail: <a href='mailto:fap@orsegups.com.br'>fap@orsegups.com.br</a> ou através do telefone (48) 3381-6652.");
    	infoComplementares.append("</div>");
		
		return infoComplementares.toString();

	}

	/**
	 * Este método recebe o corpo do e-mail e envia o mesmo para os devidos destinatários.
	 * 
	 * @param htmlEmail - Objeto String. É o corpo do e-mail com tags html.
	 * @param emailFor - Objeto String. É o e-mail do fornecedor da FAP em questão.
	 * @param emailAprovador - Objeto String. É o e-mail do aprovador da referida FAP.
	 * @throws EmailException Para exceções ao enviar o e- mail.
	 * */
	public static void enviaEmailFap(String htmlEmail, String emailFor, String emailAprovador, Boolean isTecnico) throws EmailException
	{
		String corpoEmail = htmlEmail;
		List<String> listaDestinatarios = new ArrayList<>();

		/* Variáveis usadas em testes, tanto locais quanto em produção */
		boolean debug = (FAPParametrizacao.findParameter("debug", isTecnico).equals("1") ? true : false);
		String usuarioTestes = FAPParametrizacao.findParameter("usuarioTestes", isTecnico);

		if (debug)
		{
			NeoUser user = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioTestes));
			String destinatario1 = user.getEmail();
			listaDestinatarios.add(destinatario1);
		}
		else
		{
			String destinatario1 = emailFor;
			String destinatario2 = emailAprovador;
			String destinatario3 = "rosileny@orsegups.com.br";

			listaDestinatarios.add(destinatario1);
			listaDestinatarios.add(destinatario2);
			listaDestinatarios.add(destinatario3);
		}

		for (String destinatario : listaDestinatarios)
		{
			OrsegupsUtils.sendEmail2Orsegups(destinatario, "fap@orsegups.com.br", "Autorização de Faturamento - Aprovado", "", corpoEmail, "fap@orsegups.com.br", null, "emailautomatico@orsegups.com.br");
		}
	}
}