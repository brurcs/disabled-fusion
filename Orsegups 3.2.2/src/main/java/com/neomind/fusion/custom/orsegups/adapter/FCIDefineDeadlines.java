package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar; 

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class FCIDefineDeadlines implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{		
			GregorianCalendar startDate = (GregorianCalendar) activity.getProcess().getStartDate();
			startDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
			startDate.set(GregorianCalendar.MINUTE, 0);
			startDate.set(GregorianCalendar.SECOND, 0);
			startDate.set(GregorianCalendar.MILLISECOND, 0);
			
			// Deadline 7 dias
			GregorianCalendar deadDate = (GregorianCalendar) startDate.clone();
			deadDate = OrsegupsUtils.addDays(startDate, 7, true);
			processEntity.setValue("dataDeadPrimeiro", deadDate);
			// Deadline 13 dias
			deadDate = OrsegupsUtils.addDays(startDate, 13, true);
			processEntity.setValue("dataDeadSegundo", deadDate);
			// Deadline 19 dias
			deadDate = OrsegupsUtils.addDays(startDate, 19, true);
			processEntity.setValue("dataDeadTerceiro", deadDate);
			// Deadline 27 dias
			deadDate = OrsegupsUtils.addDays(startDate, 27, true);
			processEntity.setValue("dataDeadQuarto", deadDate);
			// Deadline 32 dias
			deadDate = OrsegupsUtils.addDays(startDate, 32, true);
			processEntity.setValue("dataDeadQuinto", deadDate);
			// Deadline 63 dias
			deadDate = OrsegupsUtils.addDays(startDate, 63, true);
			processEntity.setValue("dataDeadSexto", deadDate);
			
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
}
