/*
 * Engine para fazer conexão e tratamento das chamadas SID para o processo de contratos -- ou outros
 * @autor: Edgar Luis
 *   
 */

package com.neomind.fusion.custom.orsegups.contract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.neomind.util.NeoUtils;

public class ContractSID
{
	private Map<String, String> params = new HashMap<String, String>();
	private String content = null;
	private String login = null;
	private String sis = null;
	private String user = null;
	private String acao = null;
	private String proxAcao = null;
	private String codEmp = null;
	private String codFil = null;
	private String nomUsu = null;
	private String senUsu = null;
	private String connection = null;
	private String host = null;

	public ContractSID(String acao, String proxAcao, String connection, String codEmp, String codFil) throws Exception
	{
		super();

		//conexaoSID2
		//NeoObject noConexao = ((List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("conexaoSID"))).get(0);

		//if (noConexao == null)
			//throw new WorkflowException("Problemas na conexão com o SID. Verifique se há conexão cadastrada.");
		
		//EntityWrapper ewConexao = new EntityWrapper(noConexao);
		
		this.login = "SID";
		this.sis = "CO";
		this.user = "";
		this.nomUsu = ParametrizacaoFluxoContratos.findParameter("sid.login"); //ewConexao.findField("login").getValue().toString().trim();
		this.senUsu = ParametrizacaoFluxoContratos.findParameter("sid.senha");//ewConexao.findField("senha").getValue().toString().trim();
		this.acao = acao;
		this.proxAcao = proxAcao;
		this.codEmp = codEmp;
		this.codFil = codFil;
		/*if (connection != null && !connection.equals(""))
			this.connection = connection;
		else
			this.connection = getNumberConnection();*/
		this.host = ParametrizacaoFluxoContratos.findParameter("sid.host");//ewConexao.findField("host").getValue().toString().trim();
		
		if (this.host == null){
			throw new Exception("Problemas na conexão com o SID. Verifique se há conexão cadastrada.");
		}
	}

	public void setParams(Map<String, String> params)
	{
		this.params = params;
	}

	private String getURL()
	{
		String urlstr = this.host + "SIS=" + this.sis + "&LOGIN=" + this.login + "&ACAO=" + this.acao;
		
		if(NeoUtils.safeIsNotNull(this.proxAcao))
			urlstr += "&PROXACAO=" + this.proxAcao;
						
		urlstr += "&CodEmp=" + this.codEmp + "&CodFil=" + this.codFil + "&NOMUSU=" + this.nomUsu + "&SENUSU=" + this.senUsu;
		
		/* MATAR O METODO TBM
		if (!this.connection.equals("OK") && !this.connection.equals(""))
			urlstr += "&CONNECTION=" + this.connection;*/

		urlstr += getStringParams();

		return urlstr;
	}

	public String getContent() throws Exception
	{
		if (this.content != null)
			return this.content;
		return getContent(this.params);
	}

	public String getContent(Map<String, String> params) throws Exception
	{
		
		Long tag = new GregorianCalendar().getTimeInMillis();
		this.params = params;
		StringBuffer content = null;
		try
		{
			URL murl = new URL(this.getURL());
			System.out.println("[FLUXO CONTRATOS]-SID [tag:"+tag+"] : " + murl);
			URLConnection murlc = murl.openConnection();
			murlc.setConnectTimeout(900000);
			System.out.println("[FLUXO CONTRATOS] - SID [tag:"+tag+"] Con: " + murlc + " \n timeout " + murlc.getConnectTimeout());
			BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));

			String line;
			content = new StringBuffer();

			while ((line = in.readLine()) != null)
			{
				content.append(line);
			}
			System.out.println("[FLUXO CONTRATOS]-SID [tag:"+tag+"] return content: " + content.toString());
			
			this.content = content.toString();
			in.close();
		}
		catch(SocketTimeoutException e){
			throw new Exception("O sapiens demorou muito para responder. ");
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
		return this.content;
	}

	private String getNumberConnection() throws Exception
	{
		ContractSID sid = new ContractSID("EXESENHA","", "OK", "1", "1");
		return sid.getContent(null);
	}

	public String getConnection()
	{
		return connection;
	}

	private String getStringParams()
	{
		if (this.params == null)
			return "";
		StringBuilder sb = new StringBuilder();

		for (Iterator it = this.params.keySet().iterator(); it.hasNext();)
		{
			Object key = it.next();
			String conteudo = this.params.get(key);
			if (conteudo != null && !conteudo.equals(""))
			{
				Object item = URLEncoder.encode(this.params.get(key));
				sb.append("&" + key + "=" + item.toString());
			}
		}
		return sb.toString();
	}
}
