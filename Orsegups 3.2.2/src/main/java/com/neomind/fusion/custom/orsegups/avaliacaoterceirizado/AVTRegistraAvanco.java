package com.neomind.fusion.custom.orsegups.avaliacaoterceirizado;

import java.util.Collection;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.FieldMarshaller;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLWhere;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;


//com.neomind.fusion.custom.orsegups.avaliacaoterceirizado.AVTRegistraAvanco
public class AVTRegistraAvanco implements AdapterInterface  {

	@Override
	public void start(Task origin, EntityWrapper wrapper,	Activity activity) {
		
		if (wrapper.findValue("numeroTentativa") != null){
			if (!NeoUtils.safeBoolean(wrapper.findValue("houveContato"))){
				String strTentativas = NeoUtils.safeOutputString(wrapper.findValue("numeroTentativa"));
				Long tentativas = NeoUtils.safeLong(strTentativas.equals("")? "0": strTentativas);
				tentativas++;
				if (wrapper.findValue("numeroTentativa") != null) wrapper.setValue("numeroTentativa", tentativas);
				
				
				NeoObject oConceito = PersistEngine.getObject(AdapterUtils.getEntityClass("AVTConceito"), new QLEqualsFilter("codigo", 0L) ); // sem contato
					if (oConceito != null){
						wrapper.setValue("apresUniforme",oConceito);  
						wrapper.setValue("apresRelacionamento",oConceito);  
						wrapper.setValue("servPrestIns",oConceito);  
						wrapper.setValue("limpeza",oConceito);  
						wrapper.setValue("condGerIns",oConceito); 
						wrapper.setValue("prazoEntregaInstalacao",oConceito);  
					    wrapper.setValue("OrientacoesFuncionamentoSistema",oConceito);
					    regitraHistorico(wrapper, "Sem contato. ");
					}
				
			}else{
				String strTentativas = NeoUtils.safeOutputString(wrapper.findValue("numeroTentativa"));
				Long tentativas = NeoUtils.safeLong(strTentativas.equals("")? "0": strTentativas);
				tentativas++;
				if (wrapper.findValue("numeroTentativa") != null) wrapper.setValue("numeroTentativa", tentativas);
				if(
					wrapper.findValue("apresUniforme") == null 
					|| wrapper.findValue("apresRelacionamento") == null 
					|| wrapper.findValue("servPrestIns") == null 
					|| wrapper.findValue("limpeza") == null 
					|| wrapper.findValue("condGerIns") == null 
					|| wrapper.findValue("prazoEntregaInstalacao") == null 
					//|| wrapper.findValue("OrientacoesFuncionamentoSistema") == null 
					){
					throw new WorkflowException("Preencha todos os conceitos!");
				}else{
					if (wrapper.findValue("apresUniforme.codigo").equals(0L)
							|| wrapper.findValue("apresRelacionamento").equals(0L)              
							|| wrapper.findValue("servPrestIns").equals(0L)                     
							|| wrapper.findValue("limpeza").equals(0L)                          
							|| wrapper.findValue("condGerIns").equals(0L)                       
							|| wrapper.findValue("prazoEntregaInstalacao").equals(0L)           
							//|| wrapper.findValue("OrientacoesFuncionamentoSistema").equals(0L)
					   ){
						throw new WorkflowException("Não é possível avançar tarefa marcada como 'Houve Contato', pois, existem itens de avaliação com conceito 'Sem Contato'. ");
						
					}else{
						regitraHistorico(wrapper, "Contato efetuado com sucesso!");
					}
						
				}
			}
		}else{
			// provavelmente se trata de tarefas anteriores à adição deste adapter
			//nesse caso seto como houve contato para passar direto
			if (wrapper.findValue("numeroTentativa") == null) wrapper.setValue("houveContato", true);
			
		}
		//if(true) throw new WorkflowException("testes");
		
	}
	
	public void regitraHistorico(EntityWrapper wrapper, String descricao){
		NeoObject oRegistroAtividade = AdapterUtils.createNewEntityInstance("AVTHistorico");
		EntityWrapper wRegistroAtividade = new EntityWrapper(oRegistroAtividade);
		
		//preenche os dados aqui
		wRegistroAtividade.setValue("descricao", descricao);
		
		if (wrapper.findValue("historicoTentativas") != null){
			Collection<NeoObject> listaRegistros = wrapper.findField("historicoTentativas").getValues();
			listaRegistros.add(oRegistroAtividade);
		}else{
			wRegistroAtividade.setValue("historicoTentativas", oRegistroAtividade);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}
