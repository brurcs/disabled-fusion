package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.AvaliacaoRatVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.util.NeoDateUtils;

/**
 * Rotina criada para gerar uma tarefa simples informando os dados das avaliações de RAT com Nota 
 * 1 e 2. 
 * 
 * @author herisson.ferreira
 *
 */
public class RotinaAbreTarefaSimplesAvaliacaoRAT implements CustomJobAdapter {
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesAvaliacaoRAT.class);
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		log.error("##### INICIO AGENDADOR DE TAREFA: Rotina Abre Tarefa Simples Avaliação RAT - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		System.out.println("##### INICIO AGENDADOR DE TAREFA: Rotina Abre Tarefa Simples Avaliação RAT - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		List<AvaliacaoRatVO> avaliacoesRAT = new ArrayList<AvaliacaoRatVO>();
		
		try {
			avaliacoesRAT = retornaDadosQuery();
			
			for(AvaliacaoRatVO avaliacoes : avaliacoesRAT) {
				
				StringBuilder descricao = new StringBuilder();
				
				String estrelas = retornaEstrelas(avaliacoes.getNota());
				
				descricao = montaCorpoTarefa(avaliacoes.getRat(), avaliacoes.getEvento(), avaliacoes.getDataAtendimento(), 
						avaliacoes.getAtendente(), avaliacoes.getCpfCnpj(), avaliacoes.getDadosCliente(), avaliacoes.getNota(), 
						avaliacoes.getComentario(), avaliacoes.getAutorizacao(), avaliacoes.getDataAvaliacao(), estrelas);
				
				abrirTarefaAvaliacao(avaliacoes.getNota(), avaliacoes.getDadosCliente(), descricao);
		
			}
			
		} catch (Exception e) {
			log.error("##### ERRO NO AGENDADOR DE TAREFA: Rotina Abre Tarefa Simples Avaliação RAT - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("##### ERRO NO AGENDADOR DE TAREFA: Rotina Abre Tarefa Simples Avaliação RAT - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		
		log.error("##### FIM AGENDADOR DE TAREFA: Rotina Abre Tarefa Simples Avaliação RAT - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		System.out.println("##### FIM AGENDADOR DE TAREFA: Rotina Abre Tarefa Simples Avaliação RAT - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
	}
	
	
	/**
	 * 
	 * @return Objeto contendo os dados da avaliação
	 */
	public List<AvaliacaoRatVO> retornaDadosQuery() {
		
		Connection conn = PersistEngine.getConnection("PORTALCLIENTE_PROD");
		StringBuilder query = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		GregorianCalendar prazoInicial = new GregorianCalendar();
		GregorianCalendar prazoFinal = new GregorianCalendar();
			
		prazoInicial.add(GregorianCalendar.DAY_OF_MONTH, -1);
		prazoInicial.set(GregorianCalendar.HOUR_OF_DAY, 00);
		prazoInicial.set(GregorianCalendar.MINUTE, 00);
		prazoInicial.set(GregorianCalendar.SECOND, 00);
		prazoInicial.set(GregorianCalendar.MILLISECOND, 00);
		
		prazoFinal.add(GregorianCalendar.DAY_OF_MONTH, -1);
		prazoFinal.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazoFinal.set(GregorianCalendar.MINUTE, 59);
		prazoFinal.set(GregorianCalendar.SECOND, 59);
		prazoFinal.set(GregorianCalendar.MILLISECOND, 59);
		
		
		String dataInicio = NeoDateUtils.safeDateFormat(prazoInicial, "yyyy-MM-dd HH:mm:ss");
		String dataFim = NeoDateUtils.safeDateFormat(prazoFinal, "yyyy-MM-dd HH:mm:ss");

		
		List<AvaliacaoRatVO> listaAvaliacoes = new ArrayList<AvaliacaoRatVO>();
		
		/**
		 * 
		 * Retorna todas as avaliações das RATs com Nota entre 1 e 2
		 * 
		 */
		try {
			query.append("      select ");
			query.append("      r.id RAT, ");
			query.append("      r.evento EVENTO, ");
			query.append("      r.data_atendimento DATA_RAT, ");
			query.append("      r.atendido_por ATENDENTE, ");
			query.append("      r.cgc_cpf CPF_CNPJ, ");
			query.append("      r.local DADOS_CLIENTE, ");
			query.append("      r.avaliacao_nota NOTA, ");
			query.append("      r.avaliacao_comentario COMENTARIO, ");
			query.append("      REPLACE(REPLACE(r.avaliacao_autorizacao, '1', 'SIM'), '0', 'NÃO') AUTORIZA, ");
			query.append("      r.avaliacao_datahora DATA_AVALIACAO ");
			query.append("      from ");
			query.append("      PORTALCLIENTE_PROD.rat_publicacao_mobile r ");
			query.append("      where ");
			query.append("      r.avaliacao_nota BETWEEN 1 and 2");
			query.append("      and r.avaliacao_datahora between ? and ?	");	
			query.append("      ORDER BY ");
			query.append("      r.avaliacao_datahora DESC");
			
			pstm = conn.prepareStatement(query.toString());

			pstm.setString(1, dataInicio);
			pstm.setString(2, dataFim);

			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				AvaliacaoRatVO avaliacao = new AvaliacaoRatVO();
				
				avaliacao.setRat(rs.getLong("RAT"));
				avaliacao.setEvento(rs.getString("EVENTO"));
				avaliacao.setDataAtendimento(rs.getString("DATA_RAT"));
				avaliacao.setAtendente(rs.getString("ATENDENTE"));
				avaliacao.setCpfCnpj(rs.getLong("CPF_CNPJ"));
				avaliacao.setDadosCliente(rs.getString("DADOS_CLIENTE"));
				avaliacao.setNota(rs.getLong("NOTA"));
				avaliacao.setComentario(rs.getString("COMENTARIO"));
				avaliacao.setAutorizacao(rs.getString("AUTORIZA"));
				avaliacao.setDataAvaliacao(rs.getTimestamp("DATA_AVALIACAO"));
				
				
				listaAvaliacoes.add(avaliacao);			
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaAvaliacoes;
	}
	
	/**
	 * 
	 * @param rat
	 * @param evento
	 * @param dataAtendimento
	 * @param atendente
	 * @param cpfCnpj
	 * @param dadosCliente
	 * @param nota
	 * @param comentario
	 * @param autoriza
	 * @param dataAvaliacao
	 * @param estrelas
	 * @return Descrição da Tarefa
	 */
	public StringBuilder montaCorpoTarefa(Long rat, String evento, String dataAtendimento, String atendente, Long cpfCnpj, String dadosCliente, Long nota, String comentario, String autoriza, Timestamp dataAvaliacao, String estrelas){
		
		String dtAvaliacao = NeoDateUtils.safeDateFormat(dataAvaliacao, "dd/MM/yyyy HH:mm:ss");
		
		StringBuilder corpoTarefa = new StringBuilder();
		
		corpoTarefa.append(" <html>                                                   ");
        corpoTarefa.append("    <head>                                                ");
        corpoTarefa.append("        <meta charset='utf-8'>                            ");
        corpoTarefa.append("    </head>                                               ");
		corpoTarefa.append("      <body>                                              ");
		corpoTarefa.append("          <table border='2'>                              ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("                  <th>RAT</th>                            ");
		corpoTarefa.append("                  <td>&nbsp; "+rat+" </td>                ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>EVENTO</th>                           ");
		corpoTarefa.append("              	<td>&nbsp; "+evento+" </td>               ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>DATA ATENDIMENTO</th>                 ");
		corpoTarefa.append("                <td>&nbsp; "+dataAtendimento+" </td>      ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>ATENDENTE</th>                        ");
		corpoTarefa.append("                <td>&nbsp; "+atendente+" </td>            ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>CPF/CNPJ</th>                         ");
		corpoTarefa.append("                <td>&nbsp; "+cpfCnpj+" </td>              ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>DADOS CLIENTE</th>                    ");
		corpoTarefa.append("                <td>&nbsp; "+dadosCliente+" </td>         ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>NOTA</th>                             ");
		corpoTarefa.append("                <td>&nbsp; "+nota+" - "+estrelas+"</td>   ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>COMENTARIO</th>                       ");
		corpoTarefa.append("                <td>&nbsp; "+comentario+" </td>           ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>AUTORIZA</th>                         ");
		corpoTarefa.append("                <td>&nbsp; "+autoriza+" </td>             ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>DATA AVALIAÇÃO</th>                   ");
		corpoTarefa.append("                <td>&nbsp; "+dtAvaliacao+" </td>          ");
		corpoTarefa.append("              </tr>                                       ");         
		corpoTarefa.append("          </table>                                        ");
		corpoTarefa.append("      </body>                                             ");
		corpoTarefa.append(" </html>                                                  ");
		
		return corpoTarefa;
	}
	
	/**
	 * 
	 * @param nota
	 * @return Quantidade de estrelas referente a nota
	 */
	public String retornaEstrelas(Long nota) {
		
		String estrelas = "";
		Long notaAuxiliar = 0L;
		
		while(notaAuxiliar < nota) {
			
			estrelas += "<i class='icon-star2'></i>";
			
			notaAuxiliar++;
		}
		
		return estrelas;
	}
	
	/**
	 * Monta os dados necessários para a abertura da tarefa
	 * 
	 * @param nota
	 * @param dadosCliente
	 * @param descricao
	 */
	public void abrirTarefaAvaliacao(Long nota, String dadosCliente, StringBuilder descricao) {
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		GregorianCalendar prazo = new GregorianCalendar();
		
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.add(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.add(GregorianCalendar.MINUTE, 59);
		prazo.add(GregorianCalendar.SECOND, 59);
		
		String titulo = "Nota "+ nota + " - " + dadosCliente;
		
		NeoPaper papelSolicitante = null;
		NeoPaper papelExecutor = null;
		String solicitante = null;
		String executor = null;
		
		papelSolicitante = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteTarefasAvaliacaoCliente"));
		papelExecutor = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "ExecutorTarefasAvaliacaoCliente"));
		
		solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
		executor = OrsegupsUtils.getUserNeoPaper(papelExecutor);
		
		iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao.toString(), "1", "sim", prazo);
		
	}
	


}
