package com.neomind.fusion.custom.orsegups.mainlingcontratos.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;

import com.neomind.fusion.custom.orsegups.mainlingcontratos.vo.ContratosVO;
import com.neomind.util.NeoDateUtils;

public class GeraRelatorioMainlingEmXLS
{

	public static void execute(HttpServletResponse response, java.util.List<ContratosVO> lista) throws IOException
	{

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet abaMainling = workbook.createSheet("Mailing de Contratos");
		FileOutputStream arquivo = null;

		try
		{
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			style.setFont(font);

			CellStyle style2 = workbook.createCellStyle();
			Font font2 = workbook.createFont();
			font2.setBoldweight(Font.BOLDWEIGHT_NORMAL);
			style2.setFont(font2);

			Row row = abaMainling.createRow(0);

			Cell cellRazao = row.createCell(0);
			cellRazao.setCellStyle(style);
			cellRazao.setCellValue("Razão");
			abaMainling.setColumnWidth((short) (0), (short) (10 * 1600));

			Cell cellEmail = row.createCell(1);
			cellEmail.setCellStyle(style);
			cellEmail.setCellValue("E-mail");
			abaMainling.setColumnWidth((short) (1), (short) (10 * 1000));

			Cell cellSituacao = row.createCell(2);
			cellSituacao.setCellStyle(style);
			cellSituacao.setCellValue("Situaçáo");
			abaMainling.setColumnWidth((short) (2), (short) (10 * 300));

			Cell cellRegional = row.createCell(3);
			cellRegional.setCellStyle(style);
			cellRegional.setCellValue("Regional");
			abaMainling.setColumnWidth((short) (3), (short) (10 * 300));

			Cell cellCidadeContrato = row.createCell(4);
			cellCidadeContrato.setCellStyle(style);
			cellCidadeContrato.setCellValue("Cidade Prest. Serviço");
			abaMainling.setColumnWidth((short) (4), (short) (10 * 600));

			Cell cellCidadeCliente = row.createCell(5);
			cellCidadeCliente.setCellStyle(style);
			cellCidadeCliente.setCellValue("Cidade Cliente");
			abaMainling.setColumnWidth((short) (5), (short) (10 * 600));

			Cell cellServico = row.createCell(6);
			cellServico.setCellStyle(style);
			cellServico.setCellValue("Serviço");
			abaMainling.setColumnWidth((short) (6), (short) (10 * 1000));

			Cell cellTipoPessoa = row.createCell(7);
			cellTipoPessoa.setCellStyle(style);
			cellTipoPessoa.setCellValue("Tipo Pessoa");
			abaMainling.setColumnWidth((short) (7), (short) (10 * 400));

			Cell cellTipoCliente = row.createCell(8);
			cellTipoCliente.setCellStyle(style);
			cellTipoCliente.setCellValue("Tipo Cliente");
			abaMainling.setColumnWidth((short) (8), (short) (10 * 400));

			Cell cellDataFaturamento = row.createCell(9);
			cellDataFaturamento.setCellStyle(style);
			cellDataFaturamento.setCellValue("Data Faturamento");
			abaMainling.setColumnWidth((short) (9), (short) (10 * 500));

			Cell cellDataCadastro = row.createCell(10);
			cellDataCadastro.setCellStyle(style);
			cellDataCadastro.setCellValue("Data Faturamento");
			abaMainling.setColumnWidth((short) (10), (short) (10 * 500));
			
			Cell cellCpfCnpj = row.createCell(11);
			cellCpfCnpj.setCellStyle(style);
			cellCpfCnpj.setCellValue("CPF/CNPJ");
			abaMainling.setColumnWidth((short) (11), (short) (10 * 500));

			int rowNum = 2;

			for (ContratosVO contrato : lista)
			{

				Row linha = abaMainling.createRow(rowNum);

				Cell cellRazaoList = linha.createCell(0);
				cellRazaoList.setCellStyle(style2);
				cellRazaoList.setCellValue(contrato.getRazao());

				Cell cellEmailList = linha.createCell(1);
				cellEmailList.setCellStyle(style2);
				cellEmailList.setCellValue(contrato.getEmail());

				Cell cellSituacaoList = linha.createCell(2);
				cellSituacaoList.setCellStyle(style2);
				cellSituacaoList.setCellValue(contrato.getSituacao());

				Cell cellRegionalList = linha.createCell(3);
				cellRegionalList.setCellStyle(style2);
				cellRegionalList.setCellValue(contrato.getRegional().getNomeRegional());

				Cell cellCidadeContratoList = linha.createCell(4);
				cellCidadeContratoList.setCellStyle(style2);
				cellCidadeContratoList.setCellValue(contrato.getCidadeContrato());

				Cell cellCidadeClienteList = linha.createCell(5);
				cellCidadeClienteList.setCellStyle(style2);
				cellCidadeClienteList.setCellValue(contrato.getCidadeCliente());

				Cell cellServicoList = linha.createCell(6);
				cellServicoList.setCellStyle(style2);
				cellServicoList.setCellValue(contrato.getServicosVO().getDescricaoServico());

				Cell cellipoPessoaList = linha.createCell(7);
				cellipoPessoaList.setCellStyle(style2);
				cellipoPessoaList.setCellValue(contrato.getTipoCliente());

				Cell cellipoEmcList = linha.createCell(8);
				cellipoEmcList.setCellStyle(style2);
				cellipoEmcList.setCellValue(contrato.getTipoEmc());

				Cell cellipoDataFaturamentoList = linha.createCell(9);
				cellipoDataFaturamentoList.setCellStyle(style2);
				cellipoDataFaturamentoList.setCellValue(NeoDateUtils.safeDateFormat(contrato.getDataInicio(), "dd/MM/yyyy"));

				Cell cellipoDataCadastroList = linha.createCell(10);
				cellipoDataCadastroList.setCellStyle(style2);
				cellipoDataCadastroList.setCellValue(NeoDateUtils.safeDateFormat(contrato.getDataCadastroInicio(), "dd/MM/yyyy"));
				
				Cell cellCpfCnpjList = linha.createCell(11);
				cellCpfCnpjList.setCellStyle(style2);
				cellCpfCnpjList.setCellValue(contrato.getCpfCnpj());

				rowNum++;

			}

			//		String nomeArquivo = "C:/teste/teste.xls";
			//		arquivo = new FileOutputStream(new File(nomeArquivo));
			//		workbook.write(arquivo);
			//	    arquivo.close();
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"mailingContratos.xls\"");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			workbook.write(baos);
			response.getOutputStream().write(baos.toByteArray());
			baos.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Error : " + e.getMessage());
		}

	}
}
