package com.neomind.fusion.custom.orsegups.e2doc.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docJuridicoEngine;
import com.neomind.fusion.custom.orsegups.e2doc.xml.juridico.Pastas;

@Path(value = "documentosJuridico")
public class E2docJuridicoHandler {
    
	@POST
	@Path("pesquisaJuridico")
	@Produces("application/json")
	public Pastas pesquisaJuridico(
		@MatrixParam("numeroProcesso") String numeroProcesso, @MatrixParam("autor") String autor,
		@MatrixParam("matricula") String matricula, @MatrixParam("reu") String reu) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (numeroProcesso != null && !numeroProcesso.trim().isEmpty()){
		atributosPesquisa.put("NÚMERO DO PROCESSO", numeroProcesso.trim());
	    }
	    
	    if(autor != null && !autor.trim().isEmpty()){
		atributosPesquisa.put("AUTOR", "%"+autor.trim()+"%");
	    }
	    
	    if(matricula != null && !matricula.trim().isEmpty()){
		try {
		    atributosPesquisa.put("MATRICULA", URLEncoder.encode("%"+matricula.trim()+"%","UTF-8"));
		} catch (UnsupportedEncodingException e) {
		    e.printStackTrace();
		}
	    }
	    
	    if(reu != null && !reu.trim().isEmpty()){
		atributosPesquisa.put("RÉU", "%"+reu.trim()+"%");
	    }
	    	    
	    E2docJuridicoEngine e2docEngine = new E2docJuridicoEngine();
	    
	    Pastas pastas = e2docEngine.pesquisaJuridico(atributosPesquisa);
	    	    
	    return pastas;

	}
	
}
