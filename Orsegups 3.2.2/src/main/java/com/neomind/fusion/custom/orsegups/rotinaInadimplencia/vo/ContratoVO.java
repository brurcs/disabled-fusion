package com.neomind.fusion.custom.orsegups.rotinaInadimplencia.vo;

import java.util.List;

public class ContratoVO {
    
    private int numeroContrato;
    
    private List<TituloVO> listaTitulos;

    public int getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(int numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public List<TituloVO> getListaTitulos() {
        return listaTitulos;
    }

    public void setListaTitulos(List<TituloVO> listaTitulos) {
        this.listaTitulos = listaTitulos;
    }
    
    

}
