package com.neomind.fusion.custom.orsegups.camerite.bean;

public class CameriteIntegrationBody {
    
    private String conta;
    private String empresa;
    private String particao;
    private String auxiliar;
    private String evento;
    private CameriteUser usuario;
    
    
    public String getEvento() {
        return evento;
    }
    public void setEvento(String evento) {
        this.evento = evento;
    }
    public String getConta() {
        return conta;
    }
    public void setConta(String conta) {
        this.conta = conta;
    }
    public String getEmpresa() {
        return empresa;
    }
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
    public String getParticao() {
        return particao;
    }
    public void setParticao(String particao) {
        this.particao = particao;
    }
    public String getAuxiliar() {
        return auxiliar;
    }
    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }
    public CameriteUser getUsuario() {
        return usuario;
    }
    public void setUsuario(CameriteUser usuario) {
        this.usuario = usuario;
    }
    
    

}
