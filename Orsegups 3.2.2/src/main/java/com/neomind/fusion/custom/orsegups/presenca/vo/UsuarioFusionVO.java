package com.neomind.fusion.custom.orsegups.presenca.vo;

public class UsuarioFusionVO
{
	
	private String code;
	private String email;
	private Long codReg;
	private Long cpf;
	
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public Long getCodReg()
	{
		return codReg;
	}
	public void setCodReg(Long codReg)
	{
		this.codReg = codReg;
	}
	public Long getCpf()
	{
		return cpf;
	}
	public void setCpf(Long cpf)
	{
		this.cpf = cpf;
	}
	@Override
	public String toString()
	{
		return "UsuarioFusionVO [code=" + code + ", email=" + email + ", codReg=" + codReg + ", cpf=" + cpf + "]";
	}
	
	
	

}
