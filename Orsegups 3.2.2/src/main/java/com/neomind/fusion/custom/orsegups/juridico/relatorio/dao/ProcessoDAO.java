package com.neomind.fusion.custom.orsegups.juridico.relatorio.dao;

import java.util.List;

import com.neomind.fusion.custom.orsegups.juridico.relatorio.dto.EscritorioDTO;
import com.neomind.fusion.custom.orsegups.juridico.relatorio.dto.ProcessoDTO;

public interface ProcessoDAO {
	
	List<ProcessoDTO> listaTudoAberto(String add) throws Exception;
	List<ProcessoDTO> listaTudoFinalizado(String add) throws Exception;
	
}
