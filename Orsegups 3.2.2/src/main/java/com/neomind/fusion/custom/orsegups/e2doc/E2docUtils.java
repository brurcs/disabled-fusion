package com.neomind.fusion.custom.orsegups.e2doc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docEngine;
import com.neomind.fusion.custom.orsegups.e2doc.vo.E2docVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class E2docUtils {

    private static final Log log = LogFactory.getLog(E2docEngine.class);

    /**
     * Método para indexação de documentos, para que seja realizada a indexação
     * todos os atributos do model devem estar preenchidos
     * 
     * @param model
     * @return Boolean
     * @throws Exception
     */
    public static Boolean indexarDocumento(E2docIndexacaoBean model) throws Exception {
	try {
	    return createHTTPPOSTRequest(model);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    public static Boolean indexarDocumentoJuridico(E2docIndexacaoBean model) throws Exception {
	try {
	    return createJuridicoHTTPPOSTRequest(model);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    /**
     * Método para realizar a requisição para indexação do documento
     * 
     * @param model
     * @return Boolean
     * @throws Exception
     */
    private static Boolean createHTTPPOSTRequest(E2docIndexacaoBean model) throws Exception {

	try {

	    E2docVO e2docVO = (E2docVO) getConfigE2DOC();

	    // Cria um objeto HttpURLConnection:
	    HttpURLConnection request = (HttpURLConnection) new URL(e2docVO.getIp() + "/e2web.service2/servicos.asmx/AdicionaDoc").openConnection();
	    try {
		request.setRequestMethod("POST");
		request.setReadTimeout(60 * 1000);
		request.setConnectTimeout(60 * 1000);

		// Define que a conexão pode enviar informações e obtê-las de
		// volta:
		request.setDoOutput(true);

		String key = trataRetorno(autenticarUsuarioWebServiceDocGET("RH"), "string");
		String arrayString = new String(Base64.encodeBase64(model.getDocumento(), true));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String indices = "<indice0>CPF</indice0><valor0>" + model.getCpf().toString() + "</valor0><indice1>Tipo de Documentos</indice1><valor1>" + model.getTipoDocumento() + "</valor1>" + "<indice2>Matricula</indice2><valor2>" + model.getMatricula()
			+ "</valor2><indice3>Colaborador</indice3><valor3>" + model.getColaborador() + "</valor3>" + "<indice4>Empresa</indice4><valor4>" + model.getEmpresa() + "</valor4>" + "<indice5>Data</indice5><valor5>" + sdf.format(model.getData())
			+ "</valor5><indice6>Observação</indice6><valor6>" + model.getObservacao() + "</valor6>" + "<indice7>neoid</indice7><valor7></valor7>";

		String params = "key=" + key + "&tipo=" + model.getTipo() + "&modelo=" + URLEncoder.encode(model.getModelo(), "UTF-8") + "&classe=" + URLEncoder.encode(model.getClasse(), "UTF-8") + "&indices=" + URLEncoder.encode(indices, "UTF-8") + "&formato=" + model.getFormato() + "&buffer="
			+ URLEncoder.encode(arrayString, "UTF-8");

		DataOutputStream wr = new DataOutputStream(request.getOutputStream());
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr));
		writer.write(params);
		writer.close();
		wr.flush();
		wr.close();

		int code = request.getResponseCode();
		String msg = request.getResponseMessage();

		BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
		    response.append(inputLine);
		}
		in.close();

		System.out.println("Mensagem retorno: " + msg);
		// System.out.println(inputLine.toString());
		if (code == 200) {
		    return true;
		} else {
		    log.error("##### ERRO AO indexar documento: " + msg + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}

	    } finally {
		request.disconnect();
	    }
	} catch (IOException ex) {
	    throw new IOException(ex);
	}

	return false;

    }

    /**
     * Método para realizar a requisição para indexação do documento
     * 
     * @param model
     * @return Boolean
     * @throws Exception
     */
    private static Boolean createJuridicoHTTPPOSTRequest(E2docIndexacaoBean model) throws Exception {

	try {

	    E2docVO e2docVO = (E2docVO) getConfigE2DOC();

	    // Cria um objeto HttpURLConnection:
	    HttpURLConnection request = (HttpURLConnection) new URL(e2docVO.getIp() + "/e2web.service2/servicos.asmx/AdicionaDoc").openConnection();
	    try {
		request.setRequestMethod("POST");
		request.setReadTimeout(60 * 1000);
		request.setConnectTimeout(60 * 1000);

		// Define que a conexão pode enviar informações e obtê-las de
		// volta:
		request.setDoOutput(true);

		String key = trataRetorno(autenticarUsuarioWebServiceDocGET("RH"), "string");
		String arrayString = new String(Base64.encodeBase64(model.getDocumento(), true));
		String params = "key=" + key + "&tipo=" + model.getTipo() + "&modelo=" + URLEncoder.encode(model.getModelo(), "UTF-8") + "&classe=" + URLEncoder.encode(model.getClasse(), "UTF-8") + "&indices=" + URLEncoder.encode(model.getIndices(), "UTF-8") + "&formato=" + model.getFormato()
			+ "&buffer=" + URLEncoder.encode(arrayString, "UTF-8");

		DataOutputStream wr = new DataOutputStream(request.getOutputStream());
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr));
		writer.write(params);
		writer.close();
		wr.flush();
		wr.close();

		int code = request.getResponseCode();
		String msg = request.getResponseMessage();

		BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
		    response.append(inputLine);
		}
		in.close();

		System.out.println("Mensagem retorno: " + msg);
		// System.out.println(inputLine.toString());
		if (code == 200) {
		    return true;
		} else {
		    log.error("##### ERRO AO indexar documento: " + msg + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}

	    } finally {
		request.disconnect();
	    }
	} catch (IOException ex) {
	    throw new IOException(ex);
	}

	return false;

    }

    public static E2docVO getConfigE2DOC() {

	E2docVO e2docVO = null;
	try {
	    Collection<NeoObject> configKey = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("usuario", "administrador"));

	    configKey = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("E2DOCConfiguracoes"), filter);

	    if (configKey != null && !configKey.isEmpty()) {

		for (NeoObject neoObject : configKey) {
		    e2docVO = new E2docVO();

		    EntityWrapper keyObj = new EntityWrapper(neoObject);

		    String ipStr = (String) keyObj.getValue("ip");
		    String usuarioStr = (String) keyObj.getValue("usuario");
		    String senhaStr = (String) keyObj.getValue("senha");
		    String keyStr = (String) keyObj.getValue("key");
		    e2docVO.setIp(ipStr);
		    e2docVO.setUsuario(usuarioStr);
		    e2docVO.setSenha(senhaStr);
		    e2docVO.setKey(keyStr);

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO getConfigE2DOC: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
	return e2docVO;

    }

    /**
     * GET
     * /e2web.service/servicos.asmx/AutenticarUsuario?s_usuario=string&s_senha
     * =string&s_base=string HTTP/1.1 Host: 192.168.20.89 HTTP/1.1 200 OK
     * Content-Type: text/xml; charset=utf-8 Content-Length: length <?xml
     * version="1.0" encoding="utf-8"?> <string
     * xmlns="localhost/e2web.service/servicos.asmx">string</string>
     * 
     * @param base
     * @return
     */

    public static String autenticarUsuarioWebServiceDocGET(String base) {
	String retorno = null;
	try {
	    E2docVO e2docVO = (E2docVO) getConfigE2DOC();
	    if (e2docVO != null && base != null && !base.isEmpty()) {
		String urlWebService = e2docVO.getIp() + "/e2web.service2/servicos.asmx/AutenticarUsuario?s_usuario=" + e2docVO.getUsuario() + "&s_senha=" + e2docVO.getSenha() + "&s_base=" + base;

		retorno = requestGET(urlWebService);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO autenticarUsuarioWebServiceDocGET: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

	}
	return retorno;
    }

    public static String requestGET(String urlWebService) throws Exception {
	URL url = null;
	BufferedReader reader = null;
	StringBuilder stringBuilder = null;
	HttpURLConnection connection = null;
	try {

	    url = new URL(urlWebService);
	    connection = (HttpURLConnection) url.openConnection();
	    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	    connection.setRequestMethod("GET");
	    connection.connect();
	    reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
	    stringBuilder = new StringBuilder();
	    String line = null;

	    while ((line = reader.readLine()) != null) {
		stringBuilder.append(line + "\n");
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	    try {
		if (reader != null) {
		    reader.close();
		}

		if (connection != null) {
		    connection.disconnect();
		}
	    } catch (Exception ioe) {
		ioe.printStackTrace();
	    }

	}
	return StringEscapeUtils.unescapeXml(stringBuilder.toString());
    }

    public static String trataRetorno(String retorno, String tag) {
	String retornoTratado = "";
	try {
	    if (retorno != null && !retorno.isEmpty()) {

		Integer firstTagString = retorno.indexOf("<" + tag);
		Integer posXml = retorno.indexOf(">", firstTagString);
		Integer posTagString = retorno.indexOf("</" + tag + ">");
		retornoTratado = retorno.substring(posXml + 1, posTagString + 1);
		retornoTratado = retornoTratado.replaceAll("<", "");

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO trataRetornoAutenticacao: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

	}
	return retornoTratado;
    }
    
    public static boolean isNullOrEmptyOrUndefined(String atributo) {

		return atributo == null || atributo.trim().isEmpty() || atributo.equals("undefined");
	}

}
