package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class TarefaDefinePrazoEscalado implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) 
	{
		GregorianCalendar novoPrazo = new GregorianCalendar(); 
		
		String responsavel = (String) processEntity.findField("ResponsavelExecutor.name").getValue();		
		
		if((responsavel.contains("Diretor")) || (responsavel.contains("Presidente"))){
			novoPrazo  = OrsegupsUtils.getSpecificWorkDay(novoPrazo, 10L);	
		} else {
			novoPrazo  = OrsegupsUtils.getSpecificWorkDay(novoPrazo, 2L);
		}
		
		novoPrazo.set(Calendar.HOUR_OF_DAY, 23);
		novoPrazo.set(Calendar.MINUTE, 59);
		novoPrazo.set(Calendar.SECOND, 59);
	
		processEntity.setValue("Prazo", novoPrazo);		
	}
	
	@Override
	public void back(EntityWrapper arg0, Activity arg1) 
	{
		// TODO Auto-generated method stub
		
	}	

}