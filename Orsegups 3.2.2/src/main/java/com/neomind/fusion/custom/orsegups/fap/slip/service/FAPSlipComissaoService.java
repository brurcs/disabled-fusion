package com.neomind.fusion.custom.orsegups.fap.slip.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.custom.orsegups.fap.slip.job.FAPSlipPagamentosComissaoJob;
import com.neomind.fusion.workflow.exception.WorkflowException;

@Path("/comissao")
public class FAPSlipComissaoService
{	
	@GET
	@Path("/iniciarPagamento/{codRep}/{codCal}")
	@Produces("application/json")
	public Response getPagamentos(@PathParam("codRep") Long codRep, @PathParam("codCal") Long codCal) throws JSONException
	{
		JSONObject jsRetorno = new JSONObject();
		
		try
		{
			String codTarefa = new FAPSlipPagamentosComissaoJob().iniciarPagamento(codRep, codCal);
			jsRetorno.put("codTarefa", codTarefa);
		}
		catch (WorkflowException e) {
			jsRetorno.put("error", ((WorkflowException) e).getErrorList().get(0));
		}
		catch (Exception e)
		{
			jsRetorno.put("error", e.getMessage());
		}
		
		return Response.ok(jsRetorno.toString()).build();
	}
}
