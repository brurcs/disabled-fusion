package com.neomind.fusion.custom.orsegups.avaliacaotecnico;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class AvaliacaoTecnicoJOB implements CustomJobAdapter {

	@Override
	public void execute(CustomJobContext ctx) {
		System.out.println("[C038] - Iniciando JOB de Avaliação de Terceirizado" );
		
		String queryTerceirizados = " select CD_COLABORADOR, NM_COLABORADOR from COLABORADOR where 1=1 and NM_COLABORADOR like '%SOO - TEC -%' and FG_ATIVO_COLABORADOR = 1 and FG_INSTALADOR =1 " +
				" and CD_COLABORADOR not in (33570,11257) " +
				" or CD_COLABORADOR in (105359,107186,108072,108071)	";
		GregorianCalendar dataAtual = new GregorianCalendar();
		Long mesAtual = new Long(dataAtual.get(GregorianCalendar.MONTH)+1);
		
		// Só abre tarefa se estiver no mês final do trimestre
		if ( mesAtual == 3 || mesAtual == 6 || mesAtual == 9 || mesAtual == 12 ){
			
			int cont = 0;
			try{
				
				 				
				//NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
				//NeoUser responsavel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "danilo.silva") ); 
				
				
				NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "joel.medeiros") );
				NeoUser responsavel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "maxlean.oliveira") );
				
				
				
				//Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90") ;
				ResultSet rs = OrsegupsContratoUtils.getResultSet(queryTerceirizados , "SIGMA90");
				
				if(rs!= null){
					do{ 
					
						NeoObject eformProcesso = eformProcesso = AdapterUtils.createNewEntityInstance("ADTPrincipal");
						EntityWrapper wEformProcesso = new EntityWrapper(eformProcesso);
					
						Long id_instalador = rs.getLong(1);
						Long trimestre = 0L;
						
						ArrayList<NeoObject> oInstalador =  (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACOLABORADOR"), new QLEqualsFilter("cd_colaborador", id_instalador) );
						
						if (oInstalador!= null && oInstalador.size() > 0){
							wEformProcesso.setValue("tecnico", oInstalador.get(0));
						}
						System.out.println("mes atual : " + mesAtual);
						NeoObject oTrim = retornaTrimestre(mesAtual);
						if (oTrim != null){
							EntityWrapper wTrim = new EntityWrapper(oTrim);
							trimestre = NeoUtils.safeLong( NeoUtils.safeOutputString( wTrim.findValue("trimestre") ) );
						}
						
						wEformProcesso.setValue("trimestre", oTrim );
						
						
						if (!JaRegistrado(id_instalador, trimestre)){
							String tarefa  = iniciaProcessoTarefaAgendametoVisita(eformProcesso, solicitante, responsavel);
							registraLancamento(id_instalador, trimestre );
							System.out.println("Instalador:" + id_instalador +" Trimestre:" + trimestre  );
						}
						Thread.sleep(50);
						
						cont++;
						
					}while(rs.next());
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			
			//Sem acesso
			
		}
	}
	
	
	/**
	 * Inicia o workflow de Avalizacao de Terceirizado e retorna <b>true</b> se o workflow foi iniciado com suscesso
	 * ou <b>false</b> caso ocorra algum erro.
	 * 
	 * @param NeoObject eformProcesso Eform do processo de tarefa simples ja com seus valores peenchidos
	 * @param NeoUser responsavel Usuario responsãvel pelo workflow que estã sendo iniciado
	 * @return Boolean true se for iniciado corretamente e false caso ocorra algum erro
	 */
	public static String iniciaProcessoTarefaAgendametoVisita(NeoObject eformProcesso, NeoUser solicitante, NeoUser responsavel)
	{
		String result = null;
		try{
			QLEqualsFilter equal = new QLEqualsFilter("Name", "C038 - Avaliacao de Desempenho Trimestral dos Tecnicos");
			ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
			
			WFProcess proc = processModel.startProcess(eformProcesso, true, null, null, null, null, responsavel);
			System.out.println("[ADT] + Tarefa " + proc.getCode() + " Aberta.");
			proc.setRequester(solicitante);
			proc.setSaved(true);
			// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
			PersistEngine.persist(proc);
			PersistEngine.commit(true);
		
			result = proc.getCode();
		}catch(Exception e){
			result = null;
		}
		
		return result;
	}
	 

	public void registraLancamento(Long cd_instalador, long trimestre){
		NeoObject oControle = AdapterUtils.createNewEntityInstance("ADTControleAberturaTarefa"); 
		EntityWrapper wControle = new EntityWrapper(oControle);
		
		wControle.setValue("codigoTecnico", cd_instalador);
		wControle.setValue("trimestre", trimestre);
		
		PersistEngine.persist(oControle);
		
	}


	public boolean JaRegistrado(Long cd_instalador, long trimestre){
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("codigoTecnico",cd_instalador));
		gp.addFilter(new QLEqualsFilter("trimestre",trimestre));
		
		ArrayList<NeoObject> oListaControle = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ADTControleAberturaTarefa"),gp); 
		
		if (oListaControle != null){
			for (NeoObject obj : oListaControle){
				EntityWrapper wControle = new EntityWrapper(obj);
				if (wControle.findValue("codigoTecnico") != null){
					return true;
				}
			}
			return false;
		}else{
			return false;
		}
		
	}

	public NeoObject retornaTrimestre(long mesAtual){
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLRawFilter(mesAtual + " between mesIni and mesFim  "));
		
		ArrayList<NeoObject> oTrimestres = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ADTTrimestre"),gp); 
		
		if (oTrimestres != null){
			for (NeoObject obj : oTrimestres){
				EntityWrapper wTrimstres = new EntityWrapper(obj);
				if (wTrimstres.findValue("trimestre") != null){
					return obj;
				}
			}
			return null;
		}else{
			return null;
		}
		
	}

}
