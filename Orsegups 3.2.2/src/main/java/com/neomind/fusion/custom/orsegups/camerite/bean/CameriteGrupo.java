package com.neomind.fusion.custom.orsegups.camerite.bean;

import java.io.Serializable;
import java.util.List;

public class CameriteGrupo implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private int id;
    private String title;
    private boolean enabled;
    private List<CameriteCamera> GroupCamera;
    private List<CameriteUser> GroupUser;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public boolean isEnabled() {
        return enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    public List<CameriteCamera> getGroupCamera() {
        return GroupCamera;
    }
    public void setGroupCamera(List<CameriteCamera> groupCamera) {
        GroupCamera = groupCamera;
    }
    public List<CameriteUser> getGroupUser() {
        return GroupUser;
    }
    public void setGroupUser(List<CameriteUser> groupUser) {
        GroupUser = groupUser;
    }

    
}
