package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.util.NeoDateUtils;

public class AdapterEventAjustaValorDesconto implements TaskFinishEventListener{
	
	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
			
		System.out.println("#### INICIO DO ADAPTER DE EVENTOS: "+this.getClass().getSimpleName()+" DATA: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		
		BigDecimal valorZerado = new BigDecimal(0.0);
		BigDecimal valorDesconto = new BigDecimal(0.0);
		Boolean temDesconto = false;
		
		try {
			
			InstantiableEntityInfo formularioFAP = AdapterUtils.getInstantiableEntityInfo("FAPAutorizacaoDePagamento");
			NeoObject objetoFap = formularioFAP.createNewInstance();
			EntityWrapper wEvento = new EntityWrapper(event.getProcess().getEntity());
			
			valorDesconto = (BigDecimal) wEvento.findValue("valorDescontoFAP");
			temDesconto = (Boolean) wEvento.findValue("temDesconto");
					
			if(!temDesconto) {
				
				wEvento.findField("valorDescontoFAP").setValue(valorZerado);
				
				PersistEngine.persist(objetoFap);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("#### FIM DO ADAPTER DE EVENTOS: "+this.getClass().getSimpleName()+" DATA: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		
	}

}
