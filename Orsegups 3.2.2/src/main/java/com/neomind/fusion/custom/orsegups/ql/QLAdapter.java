package com.neomind.fusion.custom.orsegups.ql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HorarioVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormError;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class QLAdapter implements AdapterInterface
{
	public static final Log log = LogFactory.getLog(QLAdapter.class);

	public static Boolean validation = true;
	public static Collection<String> erros = new ArrayList<String>();

	public static EntityManager entityManager;
	EntityTransaction transaction;
	Map<String,Object> teste;
	/*
	 * 2 - Posto deve possuir um centro de custo.
	 * 3 - Para ser utilizado o posto deve estar ativo.
	 * 4 - Posto destino deve possuir uma filial ligada.
	 * 5 - Posto deve possuir vaga disponível.
	 * 6 - Posto deve ser da mesma empresa do colaborador.
	 * 7 - Para cobertura de férias o substituído deverá estar com situação férias no período da
	 * operação.
	 * 8 - O colaborador não pode possuir afastamento no período da operação (afastamento que abre vaga).
	 * 9 - O colaborador não pode possuir restrição no cliente.
	 */

	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		this.validation = true;
		this.erros = new ArrayList<String>();
		try	{
			
			if (processEntity.findValue("operacao") != null)
			{
				String observacao = "";
				String tipoOperacao = (String) processEntity.findValue("operacao.tipo");
				String codigoOperacao = (String) processEntity.findValue("operacao.codigo");
				Boolean possuiDataFim = (Boolean) processEntity.findValue("operacao.campos.dataFinal");
				Long codReg = (Long) processEntity.findValue("regional.usu_codreg");
				String nomeMotivoAux = (String) processEntity.findValue("motivo.motivo");
				String idNexti = (String) processEntity.findValue("nextid");
				Integer codOpe = Integer.parseInt(codigoOperacao);

				//Utilizado para a operação de Férias
				NeoObject colaboradorSubstituir = (NeoObject) processEntity.findValue("colaboradorASubstituir");
				System.out.println();

				NeoObject colaborador = (NeoObject) processEntity.findValue("colaborador");
				NeoObject posto = (NeoObject) processEntity.findValue("posto.nivel8");
				NeoObject cliente = (NeoObject) processEntity.findValue("posto.nivel4");
				NeoObject escala = (NeoObject) processEntity.findValue("escala");
				GregorianCalendar dataInicial = (GregorianCalendar) processEntity.findValue("data");
				GregorianCalendar dataFinal = (GregorianCalendar) processEntity.findValue("dataFinal");
				String observacaoAux = (String) processEntity.findValue("observacao");

				if (nomeMotivoAux != null && !nomeMotivoAux.isEmpty())
				{
					observacao += nomeMotivoAux;
				}

				if (observacaoAux != null && !observacaoAux.isEmpty())
				{
					observacao += " " + observacaoAux;
				}

				int horaFim = 0;
				int horaF = 0;
				int minutoF = 0;

				int hora = dataInicial.get(dataInicial.HOUR_OF_DAY);
				int minuto = dataInicial.get(dataInicial.MINUTE);

				if (NeoUtils.safeIsNotNull(dataFinal))
				{
					horaF = dataFinal.get(dataFinal.HOUR_OF_DAY);
					minutoF = dataFinal.get(dataFinal.MINUTE);
				}

				int horaIni = (hora * 60) + minuto;
				horaFim = (horaF * 60) + minutoF;

				Long difMinutos = (dataFinal.getTimeInMillis() - dataInicial.getTimeInMillis()) / 60000L;

				QLAdapter.entityManager = PersistEngine.getEntityManager("VETORH");
				this.transaction = QLAdapter.entityManager.getTransaction();

				if (tipoOperacao != null && codigoOperacao != null && (colaborador != null || colaboradorSubstituir != null))
				{
					EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);

					if (NeoUtils.safeIsNotNull(dataInicial) && NeoUtils.safeIsNotNull(dataFinal) && possuiDataFim && dataInicial.after(dataFinal))
					{
						throw new WorkflowException("A Data Inicial não pode ser maior que a Data Final.");
					}
					if (NeoUtils.safeIsNotNull(dataFinal) && possuiDataFim && horaFim == 0)
					{
					    if (codOpe.equals(2) || codOpe.equals(3) || codOpe.equals(4) || codOpe.equals(5) || codOpe.equals(8) || codOpe.equals(9)
						    || codOpe.equals(14) || codOpe.equals(15) || codOpe.equals(16)){
						horaFim = 24*60;
					    }else{
						throw new WorkflowException("A Data Final não pode conter horário 00:00");
					    }
					}

					if (tipoOperacao.contains("TP"))
					{
						QLAdapterValidacao.validarCentroCustoDoPosto(posto); //2
						QLAdapterValidacao.validarPostoAtivo(posto); //3
						QLAdapterValidacao.validarFilialLigadaPosto(posto); //4
						QLAdapterValidacao.validarVagaNoPosto(posto, codOpe); //5
						QLAdapterValidacao.validarColaboradorEmpresa(colaborador, posto); //6
						QLAdapterValidacao.validarAfastamentoColaborador(colaborador, codOpe, dataInicial, dataFinal); //8
						QLAdapterValidacao.validarColaboradorRestricaoCliente(colaborador, tipoOperacao, posto); //9
					}

					if (tipoOperacao.contains("CC"))
					{
						QLAdapterValidacao.validarCentroCustoDoPosto(colaborador); //2
						QLAdapterValidacao.validarPostoAtivo(colaborador); //3
						QLAdapterValidacao.validarColaboradorRestricaoCliente(colaborador, tipoOperacao, posto); //9
						QLAdapterValidacao.validarDataAdmissao(colaboradorSubstituir, dataInicial);
						QLAdapterValidacao.validarEmpresaDiferenteColaborador(colaborador, colaboradorSubstituir);

						if (this.validation)
						{
							switch (codOpe)
							{

								case 2:

									QLAdapterValidacao.validarAfastamentoColaborador(colaborador, codOpe, dataInicial, dataFinal); //8
									QLAdapterValidacao.validarOPReservaTecnica(colaborador, colaboradorSubstituir, null, codigoOperacao, false);
									break;

								case 3:

									QLAdapterValidacao.validarColaboradorEmFerias(colaboradorSubstituir, dataInicial, dataFinal); //7
									QLAdapterValidacao.validarAfastamentoColaborador(colaborador, codOpe, dataInicial, dataFinal); //8
									break;

								case 11:

									QLAdapterValidacao.validarAfastamentoColaborador(colaborador, codOpe, dataInicial, dataFinal); //8
									break;

								case 14:
								case 15:

									QLAdapterValidacao.validarHoraExtra(colaborador, dataInicial, dataFinal);
									break;

								default:
									break;
							}
						}
					}

					if (tipoOperacao.contains("CP"))
					{
						QLAdapterValidacao.validarCentroCustoDoPosto(posto); //2
						QLAdapterValidacao.validarPostoAtivo(posto); //3
						QLAdapterValidacao.validarAfastamentoColaborador(colaborador, codOpe, dataInicial, dataFinal); //8
						QLAdapterValidacao.validarColaboradorRestricaoCliente(colaborador, tipoOperacao, posto); //9
						QLAdapterValidacao.validarEmpresaDiferente(colaborador, posto);

						//QLAdapterValidacao.validarHorarioCoberturaPosto(dataInicial, dataFinal, posto);

						switch (codOpe)
						{
							case 7:

								QLAdapterValidacao.validarOPReservaTecnica(colaborador, null, posto, codigoOperacao, false);
								break;

							case 12:

								QLAdapterValidacao.validarOPReservaTecnica(colaborador, null, posto, codigoOperacao, true);
								break;

							case 13:
							case 16:

								QLAdapterValidacao.validarHoraExtra(colaborador, dataInicial, dataFinal);
								break;

							default:
								break;
						}
					}
//TODO descomentar depois dos lançamentos
					if (tipoOperacao.contains("TE"))
					{
//					    dataInicial
//					    dataFinal
					    GregorianCalendar dataAtual = new GregorianCalendar();
//					    if (dataInicial.before(dataAtual) && dataInicial.get(Calendar.DAY_OF_MONTH) < dataAtual.get(Calendar.DAY_OF_MONTH)){
//						throw new WorkflowException("Não é permitido avançar com Data retroativa!");
//					    }
					    QLAdapterValidacao.validarTrocaEscala(colaborador, escala);
					}

					if (tipoOperacao.contains("TH"))
					{
					    if (codOpe == 16){
						GregorianCalendar dataAtual = new GregorianCalendar();
//						if ((dataInicial.before(dataAtual) && dataInicial.get(Calendar.DAY_OF_MONTH) < dataAtual.get(Calendar.DAY_OF_MONTH)) 
//							|| (dataFinal.before(dataAtual) && dataFinal.get(Calendar.DAY_OF_MONTH) < dataAtual.get(Calendar.DAY_OF_MONTH))){
//						    throw new WorkflowException("Não permitido avançar com datas retroativas!");
//						}
					    }
					    QLAdapterValidacao.validarCentroCustoDoPosto(colaborador); //2
					    QLAdapterValidacao.validarPostoAtivo(colaborador); //3
					    QLAdapterValidacao.validarColaboradorRestricaoCliente(colaborador, tipoOperacao, posto); //9
					    if (codOpe == 15)
					    {
						QLAdapterValidacao.validarDataAdmissao(colaboradorSubstituir, dataInicial);
						QLAdapterValidacao.validarEmpresaDiferenteColaborador(colaborador, colaboradorSubstituir);
						QLAdapterValidacao.validarDataTrocaEscala(dataInicial, dataFinal);
					    }
					}

					if (this.validation)
					{
						Long numEmp = (Long) colaboradorWrapper.findValue("numemp");
						Long tipCol = (Long) colaboradorWrapper.findValue("tipcol");
						Long numCad = (Long) colaboradorWrapper.findValue("numcad");
						Long sisCar = (Long) colaboradorWrapper.findValue("siscar");
						String nomfun = (String) colaboradorWrapper.findValue("nomfun");
						Long numcpf = (Long) colaboradorWrapper.findValue("numcpf");
						Long numLoc = (Long) colaboradorWrapper.findValue("numloc");
						Long tabOrg = (Long) colaboradorWrapper.findValue("taborg");

						Long numEmpCob = 0L;
						Long tipColCob = 0L;
						Long numCadCob = 0L;
						Long numLocCob = 0L;
						Long tabOrgCob = 0L;

						if (NeoUtils.safeIsNotNull(colaboradorSubstituir))
						{
							EntityWrapper colaboradorSubWrapper = new EntityWrapper(colaboradorSubstituir);
							numEmpCob = (Long) colaboradorSubWrapper.findValue("numemp");
							tipColCob = (Long) colaboradorSubWrapper.findValue("tipcol");
							numCadCob = (Long) colaboradorSubWrapper.findValue("numcad");
							numLocCob = (Long) colaboradorSubWrapper.findValue("numloc");
							tabOrgCob = (Long) colaboradorSubWrapper.findValue("taborg");
						}

						String nomfuncob = "";
						Long numcpfcob = 0L;

						GregorianCalendar datAlt = (GregorianCalendar) processEntity.findValue("data");

						try
						{
							if (!this.transaction.isActive())
							{
								this.transaction.begin();
							}

							// Quando existir uma troca permanente, necessita excluir todos os registros futuros do colaborador.
							// Caso seja TP vai manipular histórico de Filial e histórico de local
							if (tipoOperacao.contains("TP"))
							{

								//Grava Histórico de Centro de Custos
								String codCcu = (String) processEntity.findValue("posto.nivel8.usu_codccu");
								QLAdapterDB.movimentaCentroCusto(numEmp, tipCol, numCad, datAlt, codCcu, true);

								//Buscar o local do cliente (nível para ver se é administrativo).
								EntityWrapper ewCliente = new EntityWrapper(cliente);
								String nivelLocalCliente = (String) ewCliente.findField("codloc").getValue();

								//Delete registros futuros de Centro de Custos
								QLAdapterDB.alteraRegistros(numEmp, tipCol, numCad, datAlt, 3L);
								//Grava Histórico de Filial
								QLAdapterDB.gravaHistoricoFilial(numEmp, tipCol, numCad, datAlt, posto, sisCar, nivelLocalCliente);
								//Grava Histórico de Local
								QLAdapterDB.gravaHistoricoLocal(numEmp, tipCol, numCad, datAlt, posto);

							}

							//Histórico de Troca de Escala
							Long codigoEscala = null;
							Long codigoTurma = null;
							String nomeEscala = "";
							Long codigoHorario = null;

							if (tipoOperacao.contains("TE"))
							{
								codigoEscala = (Long) processEntity.findField("escala.escalaHor.codesc").getValue();
								codigoTurma = (Long) processEntity.findField("escala.turma.codtma").getValue();
								nomeEscala = (String) processEntity.findField("escala.escalaHor.nomesc").getValue();

								//Insere ou atualiza registros de histórico de escala.
								QLAdapterDB.movimentaHistoricoEscala(numEmp, tipCol, numCad, datAlt, codigoEscala, codigoTurma);
							}

							if (tipoOperacao.contains("TH"))
							{
								if (codOpe == 16)
								{
									codigoHorario = (Long) processEntity.findField("horario.codhor").getValue();
									QLAdapterDB.insertTrocaDeHorario(numEmp, tipCol, numCad, dataInicial, codigoHorario, idNexti);
								}
								else
								{
									EscalaVO esc = QLAdapterDB.buscaEscala(numEmp, tipCol, numCad, dataFinal);
									EscalaVO escCob = QLAdapterDB.buscaEscala(numEmpCob, tipColCob, numCadCob, dataInicial);

									if (esc.getCodigoEscala().toString().equals(escCob.getCodigoEscala().toString()) && esc.getCodigoTurma().toString().equals(escCob.getCodigoTurma().toString()))
									{
										throw new WorkflowException("Escala e turma dos colaboradores iguais.");
									}

									HorarioVO horarioInicio = QLAdapterDB.buscaHorario(escCob.getCodigoEscala(), dataInicial);
									HorarioVO horarioFinal = QLAdapterDB.buscaHorario(esc.getCodigoEscala(), dataFinal);

									String diaInicio = NeoUtils.safeDateFormat(dataInicial, "dd/MM/yyyy");
									String diaFinal = NeoUtils.safeDateFormat(dataFinal, "dd/MM/yyyy");

									if (diaInicio.equals(diaFinal))
									{
										QLAdapterDB.insertTrocaDeEscala(numEmp, tipCol, numCad, (GregorianCalendar) dataInicial.clone(), (GregorianCalendar) dataInicial.clone(), escCob.getCodigoEscala(), escCob.getCodigoTurma());
										QLAdapterDB.insertTrocaDeEscala(numEmpCob, tipColCob, numCadCob, (GregorianCalendar) dataInicial.clone(), (GregorianCalendar) dataInicial.clone(), esc.getCodigoEscala(), esc.getCodigoTurma());
									}
									else
									{
										QLAdapterDB.insertTrocaDeEscala(numEmp, tipCol, numCad, (GregorianCalendar) dataInicial.clone(), (GregorianCalendar) dataInicial.clone(), escCob.getCodigoEscala(), escCob.getCodigoTurma());
										QLAdapterDB.insertTrocaDeEscala(numEmp, tipCol, numCad, (GregorianCalendar) dataFinal.clone(), (GregorianCalendar) dataFinal.clone(), escCob.getCodigoEscala(), escCob.getCodigoTurma());
										QLAdapterDB.insertTrocaDeEscala(numEmpCob, tipColCob, numCadCob, (GregorianCalendar) dataInicial.clone(), (GregorianCalendar) dataInicial.clone(), esc.getCodigoEscala(), esc.getCodigoTurma());
										QLAdapterDB.insertTrocaDeEscala(numEmpCob, tipColCob, numCadCob, (GregorianCalendar) dataFinal.clone(), (GregorianCalendar) dataFinal.clone(), esc.getCodigoEscala(), esc.getCodigoTurma());
									}

									QLAdapterDB.insertHistoricoMovimentacao(numEmp, tipCol, numCad, numEmpCob, tipColCob, numCadCob, horarioInicio.getDataInicial(), horarioInicio.getDataFinal(), horarioInicio.getDataInicial(), horarioFinal.getDataInicial(), null, null, null, horaIni, horaFim, codOpe, observacao, colaborador, colaboradorSubstituir, tipoOperacao, idNexti);

									EntityWrapper colabSubstituirWrapper = new EntityWrapper(colaboradorSubstituir);

									Long numEmpSub = (Long) colabSubstituirWrapper.findField("numemp").getValue();
									Long tipColSub = (Long) colabSubstituirWrapper.findField("tipcol").getValue();
									Long numCadSub = (Long) colabSubstituirWrapper.findField("numcad").getValue();

									PostoVO postoFerias = QLAdapterDB.buscaPostoCoberturaDeFerias(numEmp, tipCol, numCad, (GregorianCalendar) dataInicial.clone());

									if (postoFerias != null && postoFerias.getNumeroLocal() != null)
									{
										QLAdapterDB.insertHistoricoMovimentacao(numEmpSub, tipColSub, numCadSub, numEmp, tipCol, numCad, horarioFinal.getDataInicial(), horarioFinal.getDataFinal(), horarioInicio.getDataInicial(), horarioFinal.getDataInicial(), null, null, null, horaIni, horaFim, codOpe, observacao, colaborador, colaboradorSubstituir, tipoOperacao, idNexti);
									}
									else
									{
										QLAdapterDB.insertHistoricoMovimentacao(numEmpSub, tipColSub, numCadSub, numEmp, tipCol, numCad, horarioFinal.getDataInicial(), horarioFinal.getDataFinal(), horarioInicio.getDataInicial(), horarioFinal.getDataInicial(), null, null, null, horaIni, horaFim, codOpe, observacao, colaboradorSubstituir, colaborador, tipoOperacao, idNexti);
									}
								}
							}

							//Inserir dados da operação na tabela USU_T038CobFun
//							if (tipoOperacao.contains("TE") && tipoOperacao.contains("TP"))
//							{
//								QLAdapterDB.insertHistoricoMovimentacao(numEmp, tipCol, numCad, numEmpCob, tipColCob, numCadCob, dataInicial, null, null, null, codigoEscala, codigoTurma, posto, horaIni, horaFim, codOpe, observacao, colaborador, null, tipoOperacao, idNexti);
//							}

							if (tipoOperacao.equals("CC"))
							{
								EntityWrapper colabSubstituirWrapper = new EntityWrapper(colaboradorSubstituir);
								nomfuncob = (String) colabSubstituirWrapper.findField("nomfun").getValue();
								numcpfcob = (Long) colabSubstituirWrapper.findValue("numcpf");
								QLAdapterDB.insertHistoricoMovimentacao(numEmp, tipCol, numCad, numEmpCob, tipColCob, numCadCob, dataInicial, dataFinal, null, null, null, null, null, horaIni, horaFim, codOpe, observacao, colaborador, colaboradorSubstituir, tipoOperacao, idNexti);

								String codCcu = QLAdapterDB.buscaCodCcu(tabOrg, numLoc);
								String codCcuCob = QLAdapterDB.buscaCodCcu(tabOrgCob, numLocCob);

								if (!codCcu.equals(codCcuCob) && !QLAdapterDB.possuiAutorizacaoRateio(numEmp, tipCol, numCad, dataInicial, horaIni))
								{
									QLAdapterDB.insertAutorizacaoRateio(numEmp, tipCol, numCad, dataInicial, horaIni, dataFinal, horaFim, codCcuCob);
								}
							}

							if (tipoOperacao.equals("CP"))
							{
								QLAdapterDB.insertHistoricoMovimentacao(numEmp, tipCol, numCad, numEmpCob, tipColCob, numCadCob, dataInicial, dataFinal, null, null, null, null, posto, horaIni, horaFim, codOpe, observacao, colaborador, colaboradorSubstituir, tipoOperacao, idNexti);

								String codCcu = QLAdapterDB.buscaCodCcu(tabOrg, numLoc);
								String codCcuPos = (String) processEntity.findValue("posto.nivel8.usu_codccu");

								if (!codCcu.equals(codCcuPos) && !QLAdapterDB.possuiAutorizacaoRateio(numEmp, tipCol, numCad, dataInicial, horaIni))
								{
									QLAdapterDB.insertAutorizacaoRateio(numEmp, tipCol, numCad, dataInicial, horaIni, dataFinal, horaFim, codCcuPos);
								}
							}

//							if (tipoOperacao.equals("TE"))
//							{
//								QLAdapterDB.insertHistoricoMovimentacao(numEmp, tipCol, numCad, numEmpCob, tipColCob, numCadCob, dataInicial, null, null, null, codigoEscala, codigoTurma, null, horaIni, horaFim, codOpe, observacao, null, null, tipoOperacao, idNexti);
//							}

//							if (tipoOperacao.equals("TP"))
//							{
//								GregorianCalendar dataFimCob = dataInicial;
//								QLAdapterDB.insertHistoricoMovimentacao(numEmp, tipCol, numCad, numEmpCob, tipColCob, numCadCob, dataInicial, dataFimCob, null, null, null, null, posto, horaIni, horaFim, codOpe, observacao, colaborador, null, tipoOperacao, idNexti);
//							}

							String logColaborador = "";
							String logColaboradorCoberto = "";
							String logPosto = "";

							String stringDataInicial = NeoUtils.safeDateFormat(dataInicial, "dd/MM/yyyy HH:mm");
							String stringDataFinal = NeoUtils.safeDateFormat(dataFinal, "dd/MM/yyyy HH:mm");
							Boolean unicoDia = true;

							//Data diferente da data atual
							if (dataFinal != null && !stringDataInicial.endsWith(stringDataFinal))
							{
								unicoDia = false;
							}

							String lotorn = "";
							String nomloc = "";

							if (posto != null)
							{
								EntityWrapper postoWrapper = new EntityWrapper(posto);
								lotorn = (String) postoWrapper.findValue("usu_lotorn");
								nomloc = (String) postoWrapper.findValue("nomloc");
							}

							//Logs
							switch (codOpe)
							{
								case 2:

									if (unicoDia)
									{
										logColaborador = "Colaborador " + nomfun + " realizando cobertura do colaborador " + nomfuncob + " no dia " + stringDataInicial;
										logPosto = "Colaborador " + nomfun + " realizando cobertura no dia " + stringDataInicial;
									}
									else
									{
										logColaborador = "Colaborador " + nomfun + " realizando cobertura do colaborador " + nomfuncob + " no período de " + stringDataInicial + " a " + stringDataFinal;
										logPosto = "Colaborador " + nomfun + " realizando cobertura no período de " + stringDataInicial + " a " + stringDataFinal;
									}
									//Solicitado pelo Nicolas da CM no dia 31/01/2018
//									QLAdapterDB.insertSituacaoIndefinida(colaboradorSubstituir, dataInicial, dataFinal);
									break;
								case 3:

									logColaborador = "Colaborador " + nomfun + " realizando cobertura de férias do colaborador " + nomfuncob + " no período de " + stringDataInicial + " a " + stringDataFinal;
									logColaboradorCoberto = "Colaborador em férias no período de " + stringDataInicial + " a " + stringDataFinal;
									logPosto = "Colaborador " + nomfun + " realizando cobertura de férias do colaborador " + nomfuncob + " no período de " + stringDataInicial + " a " + stringDataFinal;
									break;

								case 4:

									logColaborador = "Colaborador " + nomfun + " lotado no posto " + lotorn + " - " + nomloc + " no dia " + stringDataInicial;
									logPosto = "Colaborador " + nomfun + " lotado no dia " + stringDataInicial;
									break;

								case 5:

									codigoEscala = (Long) processEntity.findField("escala.escalaHor.codesc").getValue();
									codigoTurma = (Long) processEntity.findField("escala.turma.codtma").getValue();

									logColaborador = "Colaborador " + nomfun + " lotado no posto " + lotorn + " - " + nomloc + " e alterado a escala para " + codigoEscala + " - " + nomeEscala + "/" + codigoTurma + " no dia " + stringDataInicial;
									logPosto = "Colaborador " + nomfun + " lotado no posto e alterado a escala para " + codigoEscala + " - " + nomeEscala + " / " + codigoTurma + " no dia " + stringDataInicial;
									break;

								case 7:
								case 12:

									if (unicoDia)
									{
										logColaborador = "Colaborador " + nomfun + " realizando cobertura no posto " + lotorn + " - " + nomloc + " no dia " + stringDataInicial;
										logPosto = "Colaborador " + nomfun + " realizando cobertura no dia " + stringDataInicial;
									}
									else
									{
										logColaborador = "Colaborador " + nomfun + " realizando cobertura no posto " + lotorn + " - " + nomloc + " no período de " + stringDataInicial + " a " + stringDataFinal;
										logPosto = "Colaborador " + nomfun + " realizando cobertura no período de " + stringDataInicial + " a " + stringDataFinal;
									}
									break;

								case 8:

									codigoEscala = (Long) processEntity.findField("escala.escalaHor.codesc").getValue();
									codigoTurma = (Long) processEntity.findField("escala.turma.codtma").getValue();

									logColaborador = "Colaborador " + nomfun + " alterado a escala para " + codigoEscala + " - " + nomeEscala + "/" + codigoTurma + " no dia " + stringDataInicial;
									break;

								case 9:

									logColaborador = "Colaborador " + nomfun + " retirado do posto " + lotorn + " - " + nomloc + " com restrição no cliente no dia " + stringDataInicial;
									logPosto = "Colaborador " + nomfun + " retirado do posto com restrição no cliente no dia " + stringDataInicial;
									break;

								case 13:

									EntityWrapper postoWrapperHE = new EntityWrapper(posto);
									String lotornHE = (String) postoWrapperHE.findValue("usu_lotorn");
									String nomlocHE = (String) postoWrapperHE.findValue("nomloc");

									if (unicoDia)
									{
										logColaborador = "Colaborador " + nomfun + " realizando cobertura no posto " + lotornHE + " - " + nomlocHE + " com hora extra no dia " + stringDataInicial;
										logPosto = "Colaborador " + nomfun + " realizando cobertura com hora extra no dia " + stringDataInicial;
									}
									else
									{
										logColaborador = "Colaborador " + nomfun + " realizando cobertura no posto " + lotornHE + " - " + nomlocHE + " com hora extra no período de " + stringDataInicial + " a " + stringDataFinal;
										logPosto = "Colaborador " + nomfun + " realizando cobertura com hora extra no período de " + stringDataInicial + " a " + stringDataFinal;
									}

									if (!QLAdapterDB.possuiAutorizacaoHE(numEmp, tipCol, numCad, dataInicial, horaIni))
									{
										QLAdapterDB.insertAutorizacaoHE(numEmp, tipCol, numCad, dataInicial, horaIni, dataFinal, horaFim);
									}

									break;
								case 14:

									if (unicoDia)
									{
										logColaborador = "Colaborador " + nomfun + " realizando cobertura do colaborador " + nomfuncob + " com hora extra no dia " + stringDataInicial;
										log.warn("Colaborador " + numCad + "/" + nomfun + " realizando cobertura do colaborador " + nomfuncob + " com hora extra no dia " + stringDataInicial);
									}
									else
									{
										logColaborador = "Colaborador " + nomfun + " realizando cobertura do colaborador " + nomfuncob + " com hora extra no período de " + stringDataInicial + " a " + stringDataFinal;
										log.warn("Colaborador " + numCad + "/" + nomfun + " realizando cobertura do colaborador " + nomfuncob + " com hora extra no período de " + stringDataInicial + " a " + stringDataFinal);
									}
									//Solicitado pelo Nicolas da CM no dia 31/01/2018
//									QLAdapterDB.insertSituacaoIndefinida(colaboradorSubstituir, dataInicial, dataFinal);
									if (!QLAdapterDB.possuiAutorizacaoHE(numEmp, tipCol, numCad, dataInicial, horaIni))
									{
										QLAdapterDB.insertAutorizacaoHE(numEmp, tipCol, numCad, dataInicial, horaIni, dataFinal, horaFim);
									}
									break;

								case 15:

									EntityWrapper colabSubstituirWrapper = new EntityWrapper(colaboradorSubstituir);
									nomfuncob = (String) colabSubstituirWrapper.findField("nomfun").getValue();
									numcpfcob = (Long) colabSubstituirWrapper.findValue("numcpf");

									logColaborador = "Colaborador " + nomfun + " invertendo escala com colaborador " + nomfuncob + " nos dias " + stringDataInicial + " e " + stringDataFinal;
									logColaboradorCoberto = "Colaborador " + nomfuncob + " invertendo escala com colaborador " + nomfun + " nos dias " + stringDataInicial + " e " + stringDataFinal;
									break;

								case 16:

									String descricaoHorario = (String) processEntity.findField("horario.deshor").getValue();
									logColaborador = "Colaborador " + nomfun + " realizando troca de horário para " + descricaoHorario + " no dia " + stringDataInicial;
									break;

								default:
									break;
							}

							if (!logColaborador.equals(""))
							{
								QLAdapterDB.saveLog(logColaborador, null, numcpf.toString(), "colaborador");
							}

							if (!logColaboradorCoberto.equals(""))
							{
								QLAdapterDB.saveLog(logColaboradorCoberto, null, numcpfcob.toString(), "colaborador");
							}

							if (!logPosto.equals(""))
							{
								QLAdapterDB.saveLog(logPosto, posto, null, "posto");
							}

							NeoPaper coordenadorRegional = new NeoPaper();
							NeoPaper gerenteRegional = new NeoPaper();

							processEntity.setValue("postoServicoExtra", Boolean.FALSE);

							//Regra para Abrir tarefa do fluxo C033 - CSE - Cobertura de Serviço Extra
							if (codOpe == 7 || codOpe == 13)
							{
								if (nomloc.toUpperCase().startsWith("SERVIÇO EXTRA") && difMinutos > 40)
								{
									Long numloc = (Long) colaboradorWrapper.findValue("numloc");
									Long taborg = (Long) colaboradorWrapper.findValue("taborg");

									coordenadorRegional = OrsegupsUtils.getPapelCSEConfirmarCobranca(codReg);
										
									gerenteRegional = getPapelGerenteRegional(codReg);

									processEntity.setValue("gerenteRegional", (NeoPaper) coordenadorRegional);
									processEntity.setValue("gerenteSuperiorRegional", (NeoPaper) gerenteRegional);
									processEntity.setValue("postoServicoExtra", Boolean.TRUE);
								}
							}

							//Regra para Imprimir Relatorio de Cobertura de Ferias
							//Solicitação via tarefa 731836 - if(codOpe == 3 && codReg != 13L && codReg != 15L) {
							if (codOpe == 3)
							{

								if (codReg != 0L && (!numCad.toString().equals(numCadCob.toString()) || !numEmp.toString().equals(numEmpCob.toString()) || !tipCol.toString().equals(tipColCob.toString())))
								{
									processEntity.setValue("enviaGerente", Boolean.TRUE);
									gerenteRegional = OrsegupsUtils.getPapelComunicadoFerias(codReg);
									processEntity.setValue("gerenteRegional", (NeoPaper) gerenteRegional);
								}
							}

							this.transaction.commit();
						}
						catch (WorkflowException e)
						{
							if (e.getErrorList() != null && e.getErrorList().size() > 0)
							{
								QLAdapter.erros.add(e.getErrorList().get(0).toString());
							}
							else
							{
								QLAdapter.erros.add("Erro ao realizar a operação! " + e.getMessage());
							}
							e.printStackTrace();
						}
						catch (Exception e)
						{
							QLAdapter.erros.add("Erro ao realizar a operação! " + e.getMessage());
							e.printStackTrace();
						}
					}
				}

				if (this.erros != null && this.erros.size() > 0)
				{
					
					List<NeoObject> listaErros = new ArrayList<>();

					for (String erro : QLAdapter.erros) {
						
						NeoObject noErro = AdapterUtils.createNewEntityInstance("QLErros");
						EntityWrapper ewErro = new EntityWrapper(noErro);
						
						ewErro.findField("erro").setValue(erro.toString());
						
						listaErros.add(noErro);
					}
					
					processEntity.findField("listaErrosQL").setValue(listaErros);
					
					WorkflowException wfe = null;

					for (String erro : this.erros)
					{
						if (wfe == null)
						{
							wfe = new WorkflowException(erro);
						}
						else
						{
							wfe.addError(new EFormError(erro));
						}
					}

					throw wfe;
				}
			}
			else
			{
				log.error("Operação não informada!");
				throw new WorkflowException("Operação não informada!");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();

			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				throw new WorkflowException("Erro realizar movimentação");
			}
		}
		finally
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			
//		    if(QLAdapter.entityManager != null){
//			QLAdapter.entityManager.close();
//		    }
		}
	}
	
	public static NeoPaper getPapelGerenteRegional(Long codReg) {
		
		NeoPaper papelGerente = null;
		EntityWrapper ewResponsavel = null;
		NeoObject responsavel = null;
		
		try {
			
			responsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("CSEGerentesResponsaveis"), new QLEqualsFilter("regional", codReg));

			if (NeoUtils.safeIsNotNull(responsavel)) {
				ewResponsavel = new EntityWrapper(responsavel);
				papelGerente = (NeoPaper) ewResponsavel.findGenericValue("responsavel");   
			}

		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### QLAdapter ERROR: Não foi possível capturar o Papel Responsável pela Regional "+codReg);
			throw new WorkflowException("#### QLAdapter ERROR: Não foi possível capturar o Papel Responsável pela Regional "+codReg);
		}
		
		return papelGerente;
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}