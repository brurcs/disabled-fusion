package com.neomind.fusion.custom.orsegups.processoJuridico.civel.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ListaAnexoComparator;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoDateUtils;

import edu.emory.mathcs.backport.java.util.Collections;

public class HistoricoAnexosConverter extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuilder outBuilder = new StringBuilder();

		List<NeoObject> listaRegistroArquivos = (List<NeoObject>) field.getValue();

		Collections.sort(listaRegistroArquivos, new ListaAnexoComparator());

		outBuilder.append("<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
		outBuilder.append("    <tr style=\"cursor: auto\">");
		outBuilder.append("        <th style=\"cursor: auto; white-space: normal\">Quem Anexou</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Data do Anexo</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Titulo</th>");
		outBuilder.append("        <th style=\"cursor: auto; white-space: normal\">Anexo</th>");
		outBuilder.append("    </tr>");
		outBuilder.append("    <tbody>");

		for (NeoObject hisArq : listaRegistroArquivos)
		{
			EntityWrapper wHisArq = new EntityWrapper(hisArq);

			outBuilder.append("        <tr>");
			outBuilder.append("            <td>" + wHisArq.getValue("respAnexo") + "</td>");
			outBuilder.append("            <td>" + NeoDateUtils.safeDateFormat((GregorianCalendar) wHisArq.getValue("dtAnexo"), "dd/MM/yyyy HH:mm:ss") + "</td>");
			outBuilder.append("            <td>" + wHisArq.getValue("titulo") + "</td>");

			NeoFile file = (NeoFile) wHisArq.getValue("anexo");
			if (file != null)
			{
				outBuilder.append("            <td><a style=\"cursor:pointer;\" target='_blank' href=\"" + PortalUtil.getBaseURL() + "file/download/" + file.getNeoId() + "\"'; ");
				outBuilder.append("><img src=\"imagens/icones_final/document_search_16x16-trans.png\" align=\"absmiddle\" alt=" + file.getName() + "> ");
				outBuilder.append("			" + file.getName());
				outBuilder.append("</a></td>");
			}
			else
				outBuilder.append("            <td></td>");

			outBuilder.append("        </tr>");
		}

		outBuilder.append("    </tbody>");
		outBuilder.append("</table>");
		
		return outBuilder.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
