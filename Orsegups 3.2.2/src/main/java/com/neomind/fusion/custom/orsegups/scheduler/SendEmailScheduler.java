package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.financeiro.Documento;
import com.neomind.fusion.custom.orsegups.adapter.financeiro.InfoNotaFiscal;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

/**
 * Enviar XML da nota
 * 
 * @author herisson.ferreira
 *
 */
public class SendEmailScheduler implements CustomJobAdapter {
	
	public static String nomeRotina = "SendEmailScheduler";
	public static String enviar = "Enviar";
	public static String saida = "Saida";
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		String diretorio = getDocumentDirectory(nomeRotina, enviar);
		
		if(NeoUtils.safeIsNotNull(diretorio)) {
			processDocuments(diretorio);			
		}
			
	}

	/**
	 * Retorna o diretório cadastrado no formulário UtilsDiretorio
	 * 
	 * @param nomeRotina Nome da Rotina
	 * @param descricao Descrição do diretório
	 * @return Diretório correspondente a pasta
	 */
	private String getDocumentDirectory(String nomeRotina, String descricao) {
		
		String diretorio = null;
		
		try {

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("nome", nomeRotina));
			groupFilter.addFilter(new QLEqualsFilter("descricao", descricao));
			NeoObject noFormulario = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("diretorioUtils"), groupFilter);
			
			if(NeoUtils.safeIsNotNull(noFormulario)) {
				
				EntityWrapper ewFormulario = new EntityWrapper(noFormulario);
				diretorio = ewFormulario.findGenericValue("diretorio");
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return diretorio;
	}
	
	/**
	 * Processa os documentos depositados na pasta enviando os mesmos via e-mail, e enviando uma copia para a pasta de processado
	 * 
	 * @param diretorio Diretório da pasta contendo os xml
	 */
	private void processDocuments(String diretorio) {
		
		try {
			
			File f = new File(diretorio);

			File[] arquivos = f.listFiles();
			
			if(arquivos.length > 0) {
				
				for(File fs : arquivos) {
					
					JAXBContext jaxbContext = JAXBContext.newInstance(Documento.class);
					Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
					
					Documento documento = (Documento) unmarshaller.unmarshal(fs);
					
					for(InfoNotaFiscal e : documento.getNotaFiscal().getInfoNotaFiscal()) {
						
						sendEmail(e, fs);
						
						changeArchiveDirectory(fs);
						
					}
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Altera o diretório do XML atual da pasta processar para a pasta processado
	 * 
	 * @param fs XML da Nota atual
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	private void changeArchiveDirectory(File fs) throws IOException {
		
		FileChannel sourceChannel = null;
		FileChannel destinationChannel = null;
		String diretorio = getDocumentDirectory(nomeRotina, saida);
		
		File f = new File(diretorio, fs.getName());
		
		try {
			
			sourceChannel = new FileInputStream(fs).getChannel();
			sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel = new FileOutputStream(f).getChannel());
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
	        if (sourceChannel != null && sourceChannel.isOpen())
	            sourceChannel.close();
	        if (destinationChannel != null && destinationChannel.isOpen())
	            destinationChannel.close();
		}
			
	}

	/**
	 * Envia o e-mail no mesmo layout do SDE para o cliente, contendo o XML em anexo
	 * 
	 * @param nf XML serializado
	 * @param fs XML da Nota atual
	 * 
	 */
	private void sendEmail(InfoNotaFiscal nf, File fs) {
		
		try {
			
			StringBuilder msg = new StringBuilder();
			
			msg.append("Estabelecimento emissor: "+nf.getEmissor().getRazaoSocial());
			msg.append("<br>");
			msg.append("Série da Nota Fiscal: "+nf.getDocumentoEletronico().getSerie());
			msg.append("<br>");
			msg.append("Número da Nota Fiscal: "+nf.getDocumentoEletronico().getNumeroNotaFiscal());
			msg.append("<br>");
			msg.append("UF: "+nf.getDocumentoEletronico().getCodigoUnidadeFederativa()+" - "+getStateInitials(nf.getDocumentoEletronico().getCodigoUnidadeFederativa()));
			
			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			EmailAttachment attachment = new EmailAttachment();
			attachment.setPath(fs.getAbsolutePath());
			attachment.setDisposition(EmailAttachment.ATTACHMENT);
			attachment.setDescription("XML Nota Fiscal");
			attachment.setName(fs.getName().replaceAll("-procNFe", ""));

			HtmlEmail email = new HtmlEmail();
			email.addTo("nfmaterial@dpsp.com.br");		    
			email.addTo("engenharia.servico@dpsp.com.br");
			email.addBcc("faturamento@orsegups.com.br");
			email.setSubject("Seu XML Orsegups Chegou! Nfe N° "+nf.getDocumentoEletronico().getNumeroNotaFiscal());
			email.setMsg(msg.toString());
			email.attach(attachment);

			email.setFrom("sapiens@orsegups.com.br");
			email.setSmtpPort(settings.getPort());
			email.setHostName(settings.getSmtpServer());
			email.send();
			
		} catch (EmailException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Retorna a UF correspondente ao código preenchido no XML
	 * 
	 * @param codigoEstado Código do Estado preenchido no XML
	 * @return Retorna as inicias do estado
	 */
	private String getStateInitials(Long codigoEstado) {
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String siglaEstado = null;
		
		try {
			
			sql.append(" SELECT U.SIGEST SIGLA FROM SAPIENSNFE.DBO.N100EST U ");
			sql.append(" WHERE U.SEQEST = ? ");
			
			conn = PersistEngine.getConnection("SAPIENSNFE");
			
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, codigoEstado);
			
			rs = pstm.executeQuery();
			
			if(rs.next()) {
				siglaEstado = rs.getString("SIGLA");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return siglaEstado;
	}

}
