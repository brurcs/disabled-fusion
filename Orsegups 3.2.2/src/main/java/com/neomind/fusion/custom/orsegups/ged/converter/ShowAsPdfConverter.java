package com.neomind.fusion.custom.orsegups.ged.converter;

import com.neomind.fusion.doc.NeoDocument;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoTemplate;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.FileConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.dynamic.DocumentEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;

public class ShowAsPdfConverter extends DefaultConverter {


	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin) 
	{
		return this.getLink(field, origin);
	}

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin) 
	{
		Boolean canUpdate = false;
		
		if (field.getForm().getObject() instanceof NeoDocument)
		{
			NeoDocument neoDocument = (NeoDocument) field.getForm().getObject();
			
			DocumentEntityInfo documentEntityInfo = (DocumentEntityInfo) neoDocument.getInfo(); 
			
			documentEntityInfo = (DocumentEntityInfo) PersistEngine.reload(documentEntityInfo);
			
			NeoTemplate neoTemplate = documentEntityInfo.getDefaultTemplate();
			
			if(neoTemplate != null && neoTemplate.canUpdate())
			{
				canUpdate = true;
			}
		}
		
		if(canUpdate)
		{
			FileConverter fileConverter = new FileConverter();
			return fileConverter.getHTMLInput(field, origin);
		}
		else
		{
			return this.getLink(field, origin);
		}
	}
	
	public Object getValueFromHTMLString(EFormField field, Object oValue)
	{
		FileConverter fileConverter = new FileConverter();
		return fileConverter.getValueFromHTMLString(field, oValue);
	}
	
	private String getLink(EFormField field, OriginEnum origin)
	{
		NeoFile file = (NeoFile) field.getValue();
		if(file != null)
		{
		
			String id = "var_" + field.getFieldPath() + "tagA";
	
			String inputId = "var_" + field.getFieldPath() + "old";
			
			StringBuffer html = new StringBuffer();
			
			html.append("<input type=\"hidden\" name=\"" + inputId + "\"  id=\"" + inputId
					+ "\" neoid=\"" + file.getNeoId() + "\"");
			html.append(" errorSpan=\"err_" + inputId + "\" />");
			
			String function = this.getOpenFunction(file.getNeoId());
	
			html.append("<span style='cursor:pointer;' onclick=\""+function+"\">"
					+ "<img src='imagens/icones_final/template_16x16-trans.png' align='absmiddle' id='"
					+ id + "' alt='" + super.getValueAsString(field) + "'> " + file.getName()
					+ "</span>");
			
			return html.toString();
		}
		else
		{
			return "";
		}
	}
	
	private String getOpenFunction(Long fileNeoId)
	{
		StringBuffer html = new StringBuffer();

		html.append("		mask = callSync('"+PortalUtil.getBaseURL()+"servlet/com.neomind.fusion.custom.orsegups.ged.servlet.ShowAsPdfServlet?neoId="+fileNeoId+"');");
		html.append("		if(mask != null && mask != '')");
		html.append("		{");
		html.append("			window.open('"+PortalUtil.getBaseURL()+"ged/pdfViewer.jsp?code='+mask);");
		html.append("			return false;");
		html.append("		}");

		return html.toString();
	}
}
