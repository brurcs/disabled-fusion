package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.workflow.adapter.ferias.ConverterShowHistoricoFerias
public class ConverterRegistroAtividadeFerias extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuilder outBuilder = new StringBuilder();
		NeoObject objPrincipal = field.getForm().getObject();
		EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);
		List<NeoObject> historico = (List<NeoObject>) wPrincipal.getValue("r001V2RegistroAtividade");

		if (historico != null && historico.size() > 0)
		{
			outBuilder.append("	<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			outBuilder.append("			<tr style=\"cursor: auto\">");
			outBuilder.append("				<th style=\"cursor: auto\">Responsável</th>");
			outBuilder.append("				<th style=\"cursor: auto\">Data da Ação</th>");
			outBuilder.append("				<th style=\"cursor: auto; white-space: normal\">Descrição</th>");
			outBuilder.append("				<th style=\"cursor: auto\">Anexo</th>");
			outBuilder.append("			</tr>");
			outBuilder.append("		<tbody>");

			for (NeoObject registro : historico)
			{
				EntityWrapper wRegistro = new EntityWrapper(registro);
				outBuilder.append("		<tr>");
				outBuilder.append("			<td> " + wRegistro.getValue("usuario") + " </td>");
				outBuilder.append("			<td> " + NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.getValue("dataAcao"), "dd/MM/yyyy HH:mm:ss") + " </td>");
				outBuilder.append("			<td style=\"white-space: normal\"> " + wRegistro.getValue("descricao") + " </td>");
				outBuilder.append("			<td>");
				NeoFile file = (NeoFile) wRegistro.getValue("anexo");
				if (file != null)
				{
					outBuilder.append("			<span style=\"cursor:pointer;\" onclick=\"window.location='" + PortalUtil.getBaseURL() + "file/download/" + file.getNeoId() + "'; ");
					outBuilder.append("			return false;\"><img src=\"imagens/icones_final/document_search_16x16-trans.png\" align=\"absmiddle\" alt=" + file.getName() + "> ");
					outBuilder.append("			" + file.getName());
					outBuilder.append("</span><br>");
				}
				outBuilder.append("			</td>");
				outBuilder.append("		</tr>");
			}

			outBuilder.append("		</tbody>");
			outBuilder.append("	</table>");
		}
		return outBuilder.toString();

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
