package com.neomind.fusion.custom.orsegups.maps.vo;

import java.io.Serializable;
import java.util.List;

public class ViaturaVO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String placa;
	
	private Float latitude;
	
	private Float longitude;
	
	private Long velocidade;
	
	private String titulo;
	
	private String dataRecepcao;
	
	private Boolean ignicao;
	
	private Long altitude;
	
	private Long direcao;
	
	private Long satelites;
	
	private Boolean remover = false;
	
	private String nomeImagem = "carro";
	
	private String corLigacao = "#7c9797";

	private String sufixoLigacao = "";
	
	private String nomeMotorista = "";
	
	private String infoWindowContent;
	
	private String corStatus = "#FFFFFF"; //branco
	
	private String corTexto = "#000000"; //preto
	
	private Boolean deslocamentoLento;
	
	private Boolean atrasoDeslocamento;
	
	private String telefone;
	
	private String userField;
	
	private String codigoViatura;
	
	private Boolean alertaNoLocal;
	
	private String statusVtr;
	
	private Long codigoStatus;
	
	private Long codigoCentroCusto;
	
	private String tipoViatura;
	
	private String descricaoViatura;
	
	private boolean controlarRota;
	
	private List<String> rotasDisponiveis;
	
	private String rotaEfetiva;
	
	private String nm_rota;
	
	private long cpf;
	
	private List<RotaVO> listaRotasEfetivas;
	
	public String getNm_rota() {
		return nm_rota;
	}
	
	public void setNm_rota(String nm_rota) {
		this.nm_rota = nm_rota;
	}
	
	public List<RotaVO> getListaRotasEfetivas() {
	    return listaRotasEfetivas;
	}

	public void setListaRotasEfetivas(List<RotaVO> listaRotasEfetivas) {
	    this.listaRotasEfetivas = listaRotasEfetivas;
	}

	public long getCpf() {
	    return cpf;
	}

	public void setCpf(long cpf) {
	    this.cpf = cpf;
	}

	public String getRotaEfetiva() {
	    return rotaEfetiva;
	}

	public void setRotaEfetiva(String rotaEfetiva) {
	    this.rotaEfetiva = rotaEfetiva;
	}

	public List<String> getRotasDisponiveis() {
	    return rotasDisponiveis;
	}

	public void setRotasDisponiveis(List<String> rotasDisponiveis) {
	    this.rotasDisponiveis = rotasDisponiveis;
	}

	public boolean isControlarRota() {
	    return controlarRota;
	}

	public void setControlarRota(boolean controlarRota) {
	    this.controlarRota = controlarRota;
	}

	public String getPlaca()
	{
		return placa;
	}

	public void setPlaca(String placa)
	{
		this.placa = placa;
	}

	public Float getLatitude()
	{
		return latitude;
	}

	public void setLatitude(Float latitude)
	{
		this.latitude = latitude;
	}

	public Float getLongitude()
	{
		return longitude;
	}

	public void setLongitude(Float longitude)
	{
		this.longitude = longitude;
	}

	public Long getVelocidade()
	{
		return velocidade;
	}

	public void setVelocidade(Long velocidade)
	{
		this.velocidade = velocidade;
	}

	public String getTitulo()
	{
		return titulo;
	}

	public void setTitulo(String titulo)
	{
		this.titulo = titulo;
	}

	public String getDataRecepcao()
	{
		return dataRecepcao;
	}

	public void setDataRecepcao(String dataRecepcao)
	{
		this.dataRecepcao = dataRecepcao;
	}

	public Boolean getIgnicao()
	{
		return ignicao;
	}

	public void setIgnicao(Boolean ignicao)
	{
		this.ignicao = ignicao;
	}

	public Long getAltitude()
	{
		return altitude;
	}

	public void setAltitude(Long altitude)
	{
		this.altitude = altitude;
	}

	public Long getDirecao()
	{
		return direcao;
	}

	public void setDirecao(Long direcao)
	{
		this.direcao = direcao;
	}

	public Long getSatelites()
	{
		return satelites;
	}

	public void setSatelites(Long satelites)
	{
		this.satelites = satelites;
	}

	public Boolean getRemover()
	{
		return remover;
	}

	public void setRemover(Boolean remover)
	{
		this.remover = remover;
	}

	@Override
	public String toString()
	{
		return "[ViaturaVO] " + this.placa;
	}

	public String getNomeImagem()
	{
		return nomeImagem;
	}

	public void setNomeImagem(String nomeImagem)
	{
		this.nomeImagem = nomeImagem;
	}

	public String getCorLigacao()
	{
		return corLigacao;
	}

	public void setCorLigacao(String corLigacao)
	{
		this.corLigacao = corLigacao;
	}

	public String getSufixoLigacao()
	{
		return sufixoLigacao;
	}

	public void setSufixoLigacao(String sufixoLigacao)
	{
		this.sufixoLigacao = sufixoLigacao;
	}

	public String getNomeMotorista()
	{
		return nomeMotorista;
	}

	public void setNomeMotorista(String nomeMotorista)
	{
		this.nomeMotorista = nomeMotorista;
	}
	
	public String getInfoWindowContent()
	{
		return infoWindowContent;
	}

	public void setInfoWindowContent(String infoWindowContent)
	{
		this.infoWindowContent = infoWindowContent;
	}

	public String getCorStatus()
	{
		return corStatus;
	}

	public void setCorStatus(String corStatus)
	{
		this.corStatus = corStatus;
	}

	public String getCorTexto()
	{
		return corTexto;
	}

	public void setCorTexto(String corTexto)
	{
		this.corTexto = corTexto;
	}

	public Boolean getDeslocamentoLento() {
		return deslocamentoLento;
	}

	public void setDeslocamentoLento(Boolean deslocamentoLento) {
		this.deslocamentoLento = deslocamentoLento;
	}

	public String getTelefone()
	{
		return telefone;
	}

	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}

	public Boolean getAtrasoDeslocamento()
	{
		return atrasoDeslocamento;
	}

	public void setAtrasoDeslocamento(Boolean atrasoDeslocamento)
	{
		this.atrasoDeslocamento = atrasoDeslocamento;
	}

	public String getUserField()
	{
		return userField;
	}

	public void setUserField(String userField)
	{
		this.userField = userField;
	}

	public String getCodigoViatura()
	{
		return codigoViatura;
	}

	public void setCodigoViatura(String codigoViatura)
	{
		this.codigoViatura = codigoViatura;
	}

	public Boolean getAlertaNoLocal()
	{
		return alertaNoLocal;
	}

	public void setAlertaNoLocal(Boolean alertaNoLocal)
	{
		this.alertaNoLocal = alertaNoLocal;
	}

	public String getStatusVtr()
	{
		return statusVtr;
	}

	public void setStatusVtr(String statusVtr)
	{
		this.statusVtr = statusVtr;
	}

	public Long getCodigoStatus()
	{
		return codigoStatus;
	}

	public void setCodigoStatus(Long codigoStatus)
	{
		this.codigoStatus = codigoStatus;
	}
	
	public Long getCodigoCentroCusto()
	{
		return codigoCentroCusto;
	}

	public void setCodigoCentroCusto(Long codigoCentroCusto)
	{
		this.codigoCentroCusto = codigoCentroCusto;
	}

	public String getTipoViatura()
	{
		return tipoViatura;
	}

	public void setTipoViatura(String tipoViatura)
	{
		this.tipoViatura = tipoViatura;
	}

	public String getDescricaoViatura()
	{
		return descricaoViatura;
	}

	public void setDescricaoViatura(String descricaoViatura)
	{
		this.descricaoViatura = descricaoViatura;
	}
}
