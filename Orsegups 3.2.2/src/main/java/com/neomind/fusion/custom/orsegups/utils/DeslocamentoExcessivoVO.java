package com.neomind.fusion.custom.orsegups.utils;

public class DeslocamentoExcessivoVO {
	
	Long cdCliente;
	Long quantidadeDeslocamento;
	String siglaRegional;
	String idCentral;
	String particao;
	String nomeFantasia;
	String razaoSocial;
	
	public Long getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(Long cdCliente) {
		this.cdCliente = cdCliente;
	}
	public Long getQuantidadeDeslocamento() {
		return quantidadeDeslocamento;
	}
	public void setQuantidadeDeslocamento(Long quantidadeDeslocamento) {
		this.quantidadeDeslocamento = quantidadeDeslocamento;
	}
	public String getSiglaRegional() {
		return siglaRegional;
	}
	public void setSiglaRegional(String siglaRegional) {
		this.siglaRegional = siglaRegional;
	}
	public String getIdCentral() {
		return idCentral;
	}
	public void setIdCentral(String idCentral) {
		this.idCentral = idCentral;
	}
	public String getParticao() {
		return particao;
	}
	public void setParticao(String particao) {
		this.particao = particao;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	
	

}
