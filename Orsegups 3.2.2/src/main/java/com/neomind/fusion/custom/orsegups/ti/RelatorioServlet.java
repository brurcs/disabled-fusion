package com.neomind.fusion.custom.orsegups.ti;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.mobile.vo.NeoUserVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

@WebServlet(name="RelatorioServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.ti.RelatorioServlet"})
public class RelatorioServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RelatorioServlet() {
	super();
    }

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

	try
	{
	    PrintWriter out = null;
	    
	    String action = "";

	    if (request.getParameter("action") != null)
	    {
		action = request.getParameter("action");
		out = response.getWriter();
		System.out.println("Action qlservlet:" + action);
	    }

	    if (action.equalsIgnoreCase("listaUsuarios"))
	    {
		listaUsuarios(out);
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
    }


    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
	return "Short description";
    }// </editor-fold>


    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter out = response.getWriter();

	try{

	    conn = PersistEngine.getConnection("");

	    String dateIni = request.getParameter("dataInicio");
	    String dateEnd = request.getParameter("dataFim");
	    String [] userTask = request.getParameterValues("usuario");
	    String [] solicitanteTask = request.getParameterValues("solicitante");

	    String lista = "";
	    String lista2 = "";

	    if(userTask != null) {
		for(int i =0; i<userTask.length; i++){;
		lista += userTask[i]+",";
		};
	    };
	    
	    if(solicitanteTask != null) {
		for(int i =0; i<solicitanteTask.length; i++){;
		lista2 += solicitanteTask[i]+",";
		};
	    };

	    String usuarios = lista.substring(0, lista.length() -1);
	    String solicitantes = lista2.substring(0, lista2.length() -1);

	    String where = "";

	    if(usuarios.equals("todos") && solicitantes.equals("undefined") || solicitantes.equals("null"))
	    {
		where = "where w.processState = 1 and w.finishDate >= '"+ dateIni +" 00:00:00' and w.finishDate <= '"+ dateEnd +" 23:59:59' and u.group_neoId = 638564840 ";   
	    }
	    else if(usuarios.equals("todos") && (solicitantes != "undefined" || solicitantes != "null"))
	    {
		where = "where w.processState = 1 and w.finishDate >= '"+ dateIni +" 00:00:00' and w.finishDate <= '"+ dateEnd +" 23:59:59' and u.group_neoId = 638564840 and u.group_neoId = 638564840 and solicitante_neoId in("+ solicitantes +")";		
	    }	   
	    else if(usuarios != "todos" && (solicitantes.equals("undefined") || solicitantes.equals("null")))
	    {
		where = "where s.neoId in("+ usuarios +") and w.processState = 1 and  w.finishDate >= '"+ dateIni +" 00:00:00' and w.finishDate <= '"+ dateEnd +" 23:59:59'";
	    }
	    else
	    {
		where = "where s.neoId in("+ usuarios +") and solicitante_neoId in("+ solicitantes +") and w.processState = 1 and  w.finishDate >= '"+ dateIni +" 00:00:00' and w.finishDate <= '"+ dateEnd +" 23:59:59'";
	    }
	    String sql = "select w.code as code, t.titulo as titulo, sol.fullName as solicitante , s.neoId as neoId, u.fullName as fullName, w.processState as processState, w.startDate as startDate, w.finishDate as finishDate from D_Tarefa t WITH (NOLOCK)"
		    + "inner join NeoUser u on u.neoId = t.executor_neoId "
		    + "inner join NeoUser sol on sol.neoId = t.solicitante_neoId "
		    + "inner join WFProcess w on w.entity_neoId = t.neoId "
		    + "inner join SecurityEntity s on s.neoId = u.neoId "
		    + where 
		    + "order by w.finishDate, u.fullName ASC";

	    pstm = conn.prepareStatement(sql);

	    rs = pstm.executeQuery();

	    out.println("<thead>");
	    out.println("<tr>");
	    out.println("<th>Número Tarefa</th>");
	    out.println("<th>Título</th>");
	    out.println("<th>Solicitante</th>");
	    out.println("<th>Executor</th>");
	    out.println("<th>Data Abertura</th>");
	    out.println("<th>Data Finalização</th>");
	    out.println("</tr>");
	    out.println("</thead>");

	    int Contador = 0;	   
	    while (rs.next())
	    {

		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");

		String dataFimEmUmFormato = rs.getString("finishDate");
		Date dataFim = formato.parse(dataFimEmUmFormato);
		formato.applyPattern("dd/MM/yyyy");
		String dataFimFormatada = formato.format(dataFim);

		String dataInicioEmUmFormato = rs.getString("startDate");
		Date dataInicio = formato2.parse(dataInicioEmUmFormato);
		formato2.applyPattern("dd/MM/yyyy");
		String dataInicioFormatada = formato.format(dataInicio);

		out.println("<tbody>");
		out.println("<tr>");
		out.println("<td>"+rs.getString("code")+"</td>");
		out.println("<td>"+rs.getString("titulo")+"</td>");
		out.println("<td>"+rs.getString("solicitante")+"</td>");
		out.println("<td>"+rs.getString("fullName")+"</td>");
		out.println("<td>"+dataInicioFormatada+"</td>");
		out.println("<td>"+dataFimFormatada+"</td>");
		out.println("</tr>");
		out.println("</tbody>");
		Contador++;

	    }
	    //exibindo a quantidade de registros encontrados:
	    out.println("Tarefas Finalizadas: " + Contador);
	}catch (Exception e)
	{
	    out.println(e.getMessage());
	}finally
	{
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
    }

    private Date Date(Date parse) {
	// TODO Auto-generated method stub
	return null;
    }

    
    public void listaUsuarios(PrintWriter out) throws SQLException
    {
		
	List<NeoUserVO> listaUsuarios = this.consultaSolicitante();

	Gson gson = new Gson();
	String retorno = gson.toJson(listaUsuarios);
	System.out.println(retorno);
	out.print(retorno);
	out.flush();
	out.close();
    }


    public List <NeoUserVO> consultaSolicitante() throws SQLException { 

	List <NeoUserVO> listaFuncionario = new ArrayList <NeoUserVO>();

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;


	conn = PersistEngine.getConnection("");

	String sql2 = "select distinct neoId as id, fullName from NeoUser order by fullName ASC";

	pstm = conn.prepareStatement(sql2);

	rs = pstm.executeQuery();

	while(rs.next()){
	    NeoUserVO usuario = new NeoUserVO() ;
	    usuario.setCode(rs.getString("id"));
	    usuario.setFullName(rs.getString("fullname"));
	    listaFuncionario.add(usuario);
	}
	rs.close();
	pstm.close();    
	return listaFuncionario;
    }
}