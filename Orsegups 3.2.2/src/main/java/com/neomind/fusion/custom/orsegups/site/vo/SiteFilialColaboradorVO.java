package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteFilialColaboradorVO
{
	
	private String razsoc;
	private Long numcgc;
	private Long codfil; 
	private String nomFil;
	
	public String getRazsoc()
	{
		return razsoc;
	}
	public void setRazsoc(String razsoc)
	{
		this.razsoc = razsoc;
	}
	public Long getNumcgc()
	{
		return numcgc;
	}
	public void setNumcgc(Long numcgc)
	{
		this.numcgc = numcgc;
	}
	public Long getCodfil()
	{
		return codfil;
	}
	public void setCodfil(Long codfil)
	{
		this.codfil = codfil;
	}
	public String getNomFil()
	{
		return nomFil;
	}
	public void setNomFil(String nomFil)
	{
		this.nomFil = nomFil;
	}
	
	

}
