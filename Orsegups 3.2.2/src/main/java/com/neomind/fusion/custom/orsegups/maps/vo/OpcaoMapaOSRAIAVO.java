package com.neomind.fusion.custom.orsegups.maps.vo;

public class OpcaoMapaOSRAIAVO
{
	private String id;
	
	private String opcao;
	
	private String fechado;
	
	private String cor;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOpcao() {
		return opcao;
	}

	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}

	public String getFechado() {
		return fechado;
	}

	public void setFechado(String fechado) {
		this.fechado = fechado;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	

	
		
	
}
