package com.neomind.fusion.custom.casvig.mobile.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.neomind.fusion.custom.casvig.mobile.MobileUtils;
import com.neomind.fusion.custom.casvig.mobile.xml.XmlMobileConverter;
import com.neomind.fusion.custom.casvig.mobile.xml.XmlMobileParser;
import com.neomind.fusion.custom.casvig.mobile.xml.entity.Inspection;
import com.neomind.fusion.custom.casvig.mobile.xml.entity.Reinspection;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.InstantiableEntityInfo;

/**
 * Classe que recebe os XML gerados no Mobile da Casvig usado nas inspetorias de
 * qualidade
 * 
 * @author Daniel Henrique Joppi
 * 
 */
@WebServlet(name="ActionReceiveInspection", urlPatterns={"/servlet/com.neomind.fusion.custom.casvig.mobile.action.ActionReceiveInspection"})
public class ActionReceiveInspection extends HttpServlet
{
	private static final Log log = LogFactory.getLog(ActionReceiveInspection.class);
	public static ActionReceiveInspection getInstance()
	{
		return new ActionReceiveInspection();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String msg = null;
		DataOutputStream out = null;
		DataInputStream in = null;

		boolean error = false;
		try
		{
			out = new DataOutputStream(response.getOutputStream());
			
			
			System.out.println("********out*************: "+out);
			in = new DataInputStream(request.getInputStream());
			System.out.println("********in*************: "+in);
			log.info("Mobile request Resultado.xml");

			int ch;

			ByteArrayOutputStream bStrm = new ByteArrayOutputStream();

			msg = new String("Receive Resultado.xml");
			while ((ch = in.read()) != -1)
				bStrm.write(ch);

			log.info(msg);

			ByteArrayInputStream bais = new ByteArrayInputStream(bStrm.toByteArray());
			DataInputStream dis = new DataInputStream(bais);
			Document document = XmlMobileConverter.builderDocument(dis);

			bStrm.close();

			InstantiableEntityInfo entityInfo = (InstantiableEntityInfo) EntityRegister.getCacheInstance().getByString("PesquisaResultado");

			XmlMobileParser xmlMobile = null;
			Element elem = document.getDocumentElement(); // Resultados
			if (elem != null && elem.hasChildNodes())
			{
				// Verifica 1 presultado
				// o mobile envia um resultado de cada vez
				Element elemInsp = (Element) elem.getChildNodes().item(0);

				String tipo = String.valueOf(elemInsp.getAttribute("tipo_inspecao"));
				Boolean isInspection = (/*tipo.equals("Inspeção") || tipo.equals("Inspecao") || tipo.equals("Inspe��o") || */ tipo.toLowerCase().startsWith("inspe"));
				if (isInspection)
					xmlMobile = new XmlMobileParser(entityInfo, new Inspection());
				else
					xmlMobile = new XmlMobileParser(entityInfo, new Reinspection());
				log.debug("tipo: " + tipo + " isINSPection: " + isInspection );
				System.out.println("tipo: " + tipo + " isINSPection: " + isInspection );
			}

			if (xmlMobile == null)
				log.error("Resultado.xml is corrupted.");

			boolean isPersist = xmlMobile.persistXml( document);
			if (isPersist)
			{
				MobileUtils.sendString("OK", response);
			}
			else
			{
				error = true;
				msg = new String(" Persist Resultado.xml");
			}
			return;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			error = true;
		}
		finally
		{
			if (out != null)
			{
				try
				{
					out.close();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
					error = true;
				}
			}
			if (in != null)
			{
				try
				{
					in.close();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
					error = true;
				}
			}
			if (error)
			{
				if (msg != null)
					log.error(msg);
			}
		}
	}
}
