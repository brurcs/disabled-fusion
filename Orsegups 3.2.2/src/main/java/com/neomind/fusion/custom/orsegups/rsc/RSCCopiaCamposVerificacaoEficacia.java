package com.neomind.fusion.custom.orsegups.rsc;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RSCCopiaCamposVerificacaoEficacia implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		RSCUtils rscUtils = new RSCUtils();
		Long etapaEficacia = (Long) processEntity.findValue("etapaVerificacaoEficacia");
		
		switch (etapaEficacia.intValue())
		{
			case 1:
				/*Informa a próxima Etapa Da Eficácia, após retornar do fluxo Pai*/
				rscUtils.salvaEtapaVerificacao(processEntity, 2L);
				break;
			case 2:
				/*Informa a próxima Etapa Da Eficácia, após retornar do fluxo Pai*/
				rscUtils.salvaEtapaVerificacao(processEntity, 3L);
				break;
			case 3:
				/*Informa a próxima Etapa Da Eficácia, após retornar do fluxo Pai*/
				rscUtils.salvaEtapaVerificacao(processEntity, 4L);
				break;
			case 4:
				/*Informa a próxima Etapa Da Eficácia, após retornar do fluxo Pai*/
				rscUtils.salvaEtapaVerificacao(processEntity, 5L);
				break;
			case 5:
				/*Informa a próxima Etapa Da Eficácia, após retornar do fluxo Pai*/
				rscUtils.salvaEtapaVerificacao(processEntity, 9L);
				break;
		}		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}

}
