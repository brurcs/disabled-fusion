package com.neomind.fusion.custom.casvig.mobile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.util.NeoUtils;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class MobileUtils
{
	public static String dencodePassword(String senhaCrip)
	{

		String[] arrayCrypt = new String[] { "53", "65", "6e", "a1", "c4", "6f", "72", "23", "48", "61", "ee", "64", "00", "4e", "2e", "28", "54",
				"d9", "6c", "cc", "1d", "02", "57", "11", "73", "88", "69", "9e", "67", "74", "ff", "68", "2c", "20", "34", "43", "dd", "0e", "0d",
				"0a", "44", "6b", "bc", "2f", "55", "51", "66", "83", "79", "8f", "7a", "3a", "90", "01", "ed", "aa", "5c", "c6", "31", "33" };
		String[] array = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
				"x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X",
				"Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		String password = "";

		for (int i = 0; i < senhaCrip.length(); i++)
		{
			boolean notFound = true;
			for (int j = 0; j < 60; j++)
			{
				if ((i + 1) != senhaCrip.length() && senhaCrip.substring(i, i + 2).equals(arrayCrypt[j]))
				{
					password += array[j];
					notFound = false;
					i++;
					break;
				}
				else if ((i + 1) == senhaCrip.length() && senhaCrip.substring(i).equals(arrayCrypt[j]))
				{
					password += array[j];
					notFound = false;
					i++;
					break;
				}
			}
			if (notFound)
				password += (i != senhaCrip.length()) ? senhaCrip.substring(i, i + 1) : senhaCrip.substring(i);
		}

		return password;
	}

	public static String getOldString(String newString)
	{
		String oldString = new String("");
		for (int i = 0; i < newString.length(); i++)
		{
			char newChar = newString.toCharArray()[i];
			oldString += (newChar != ':') ? newChar : ' ';
		}
		return oldString;
	}

	public static boolean haveNeoId(String[] array, long neoId)
	{
		for (int i = 0; i < array.length; i++)
		{
			if (array[i].equals(String.valueOf(neoId)))
				return true;
		}
		return false;
	}

	public static byte[] convertToSendMobile(String text)
	{
		// text = new String(text.replace("\n", ""));
		// text = new String(text.replace("\t", ""));
		text = new String(text.replace("ª", ""));
		text = new String(text.replace("º", ""));
		text = new String(text.replaceAll("[áãàâä]", "a"));
		text = new String(text.replaceAll("[éèêë]", "e"));
		text = new String(text.replaceAll("[íìîï]", "i"));
		text = new String(text.replaceAll("[óõòôö]", "o"));
		text = new String(text.replaceAll("[úùûü]", "u"));
		text = new String(text.replace("ç", "c"));
		text = new String(text.replace("ñ", "n"));

		text = new String(text.replaceAll("[ÁÃÀÂÄ]", "A"));
		text = new String(text.replaceAll("[ÉÈÊË]", "E"));
		text = new String(text.replaceAll("[ÍÌÎÏ]", "I"));
		text = new String(text.replaceAll("[ÓÕÒÔÖ]", "O"));
		text = new String(text.replaceAll("[ÚÙÛÜ]", "U"));
		text = new String(text.replace("Ç", "C"));
		text = new String(text.replace("Ñ", "N"));
		return text.getBytes();
	}

	/**
	 * Verifica se a respota da inspeção passada é conforme ou não gerou
	 * inconsistência.
	 * 
	 * @author Daniel Henrique Joppi 15/12/2008
	 * 
	 * @param logical
	 * "true", "false" ou "null".
	 * @return se "true" retorna true, se "false" retona false, senão retona
	 * null.
	 */
	public static Boolean isConform(String logical)
	{
		if ("false".equals(logical))
		{
			return false;
		}
		if ("true".equals(logical))
		{
			return true;
		}

		return null;
	}

	/**
	 * Cria filtro para tratar "" que o QLEqualsFilter NÃO TRATAAAAAA!!!!! Tem
	 * chamado aberto por Daniel Henrique Joppi sobre isso.
	 * 
	 * @author Daniel Henrique Joppi 15/12/2008
	 * @param attribute
	 * campo do E-Form
	 * @param value
	 * valor a ser adicionado no filtro
	 * @return retorno filtro.
	 */
	public static QLFilter createFilter(String attribute, Object value)
	{
		if (NeoUtils.safeIsNull(value))
			if (value == null)
				return new QLFilterIsNull(attribute);
			else
				return new QLRawFilter(attribute + " LIKE ''");
		else
			return QLEqualsFilter.buildFromObject(attribute, value);
	}

	public static List<NeoObject> cloneList(List<NeoObject> list)
	{
		List<NeoObject> newList = new ArrayList<NeoObject>();
		for (NeoObject neoObj : list)
		{
			newList.add(neoObj);
		}
		return newList;
	}
	
	public static void sendFile(Document docXML, HttpServletResponse response) throws IOException
	{
		OutputStream out = response.getOutputStream();
		ByteArrayOutputStream bos = new ByteArrayOutputStream(0x100000);
		XMLSerializer serializerFile = new XMLSerializer(bos, new OutputFormat(docXML, "ISO-8859-1", true));
		serializerFile.serialize(docXML);
		bos.close();
		int size = bos.size();
		response.setContentLength(size);
		response.setContentType("text/xml");
		CasvigMobileServlet.log.debug((new StringBuilder("Tamanho: ")).append(size).append(" bytes. Enviando...").toString());
		InputStream in = new ByteArrayInputStream(bos.toByteArray());
		byte b[] = new byte[2048];
		for (int l = 0; (l = in.read(b)) != -1;)
			out.write(b, 0, l);

		in.close();
	}
	
	public static void sendString(String xml, HttpServletResponse response) throws IOException
	{
		OutputStream out = response.getOutputStream();
		int size = xml.length();
		response.setContentLength(size);
		response.setContentType("text/xml");
		CasvigMobileServlet.log.debug((new StringBuilder("Tamanho: ")).append(size).append(" bytes. Enviando...").toString());
		InputStream in = IOUtils.toInputStream(xml);
		byte b[] = new byte[2048];
		for (int l = 0; (l = in.read(b)) != -1;)
			out.write(b, 0, l);

		in.close();
	}
}
