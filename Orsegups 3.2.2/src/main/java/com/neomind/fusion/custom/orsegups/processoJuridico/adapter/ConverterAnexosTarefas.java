package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.FaltaEfetivoVO;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.EFormManager;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDocUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterAnexosTarefas
public class ConverterAnexosTarefas extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		NeoObject objPrincipal = field.getForm().getObject();
		EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);
		List<NeoObject> listaPedidos = null;
		String grauProcesso = String.valueOf(wPrincipal.getValue("grauProcesso"));
		BigDecimal totalPedidos = new BigDecimal(0);

		if (grauProcesso.contains("1º"))
		{
			listaPedidos = (List<NeoObject>) wPrincipal.findValue("lisPed");
		}
		else if (grauProcesso.contains("2º"))
		{
			listaPedidos = (List<NeoObject>) wPrincipal.findValue("lisPedSegGrau");
		}
		else
		{
			listaPedidos = (List<NeoObject>) wPrincipal.findValue("lisPedTerGrau");
		}

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder retorno = new StringBuilder();
		if (listaPedidos != null && listaPedidos.size() > 0)
		{
			retorno.append(" <script type=\"text/javascript\"> function viewItemFusion(id)");
			retorno.append(" {");
			retorno.append(" var title =\"vizualizar formulário\";");
			retorno.append(" var uid;");
			retorno.append(" var entityType = '';");
			retorno.append(" var idx = '';");

			retorno.append(" var portlet = new FloatForm(title, uid, entityType, id, 'ellist_', idx, 0, 0, '', null, null, false, false, null, false, '');");
			retorno.append(" portlet.open();");
			retorno.append("}");
			retorno.append(" 	</script>");
			retorno.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			retorno.append("			<tr style=\"cursor: auto\">");
			retorno.append("				<th style=\"cursor: auto\">Pedido</th>");
			retorno.append("				<th style=\"cursor: auto\">Obs</th>");
			retorno.append("				<th style=\"cursor: auto\">Classificação</th>");
			retorno.append("				<th style=\"cursor: auto; white-space: normal\">Parecer Adm/Ope do pedido</th>");
			retorno.append("                 <th style=\"cursor: auto; white-space: normal\">Tarefas</th>");
			retorno.append("				<th style=\"cursor: auto\">Anexos</th>");
			retorno.append("				<th style=\"cursor: auto; white-space: normal\">Valor</th>");
			retorno.append("			</tr>");
			retorno.append("			<tbody>");

			for (NeoObject pedido : listaPedidos)
			{
				EntityWrapper wPedido = new EntityWrapper(pedido);
				List<NeoObject> listaDeTarefas = (List<NeoObject>) wPedido.findValue("lisTar");
				NeoObject tipoPedido = (NeoObject) wPedido.getValue("tipoPedido");
				EntityWrapper wTipoPedido = new EntityWrapper(tipoPedido);
				NeoObject claPed = (NeoObject) wPedido.getValue("j002ClaPed");
				EntityWrapper wClaPed = null;
				if (claPed != null)
				{
					wClaPed = new EntityWrapper(claPed);
				}

				retorno.append("		<tr>");
				retorno.append("<td>" + wTipoPedido.getValue("descricao") + "</td>");

				if (wPedido.getValue("obsGeral") != null)
				{
					retorno.append("<td>" + wPedido.getValue("obsGeral") + "</td>");
				}
				else
				{
					retorno.append("<td></td>");

				}

				if (wClaPed != null && wClaPed.getValue("descricao") != null)
				{
					retorno.append("			<td style=\"white-space: normal\">" + wClaPed.getValue("descricao") + "</td>");
				}
				else
				{
					retorno.append("			<td style=\"white-space: normal\">" + " " + "</td>");
				}

				if (wPedido.getValue("obsAdmOpe") != null)
				{
					retorno.append("			<td style=\"white-space: normal\">" + wPedido.getValue("obsAdmOpe") + "</td>");
				}
				else
				{
					retorno.append("			<td style=\"white-space: normal\">" + " " + "</td>");
				}

				if (listaDeTarefas != null && listaDeTarefas.size() > 0)
				{

					retorno.append("<td>");
					for (NeoObject tarefaShow : listaDeTarefas)
					{

						EntityWrapper wTarefaShow = new EntityWrapper(tarefaShow);
						if (wTarefaShow.getValue("neoIdTarefa") != null)
						{
							Long neoIdTarefa = (long) wTarefaShow.getValue("neoIdTarefa");
							WFProcess processo = (WFProcess) PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", neoIdTarefa));
							retorno.append("<img onclick=\"javascript:viewItemFusion(" + neoIdTarefa + ");\" neoid=\"" + neoIdTarefa + "\" id=\"editItem_49873\" class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a>&nbsp;" + processo.getCode() + "<br>");
						}
					}

					retorno.append("</td>");
					retorno.append("			<td style=\"white-space: normal\">");
					for (NeoObject tarefa : listaDeTarefas)
					{

						EntityWrapper wTarefa = new EntityWrapper(tarefa);
						StringBuilder sql = new StringBuilder();
						if (wTarefa.getValue("neoIdTarefa") != null)
						{
							Long neoIdTarefa = (long) wTarefa.getValue("neoIdTarefa");
							conn = PersistEngine.getConnection("");
							sql.append("select ra.anexo_neoId from D_Tarefa t ");
							sql.append("			inner join WFProcess w on w.neoId = t.wfprocess_neoId ");
							sql.append("inner join D_Tarefa_registroAtividades tg on tg.D_Tarefa_neoId = t.neoId ");
							sql.append("inner join D_TarefaRegistroAtividades ra on ra.neoId = tg.registroAtividades_neoId ");
							sql.append("where w.neoId = ? and ra.anexo_neoId is not null ");

							try
							{

								pstm = conn.prepareStatement(sql.toString());
								pstm.setLong(1, neoIdTarefa);
								rs = pstm.executeQuery();

								while (rs.next())
								{
									long neoIdFile = rs.getLong("anexo_neoId");
									NeoFile file = (NeoFile) PersistEngine.getObject(NeoFile.class, new QLEqualsFilter("neoId", neoIdFile));
									retorno.append("	<a style=\"cursor:pointer;\" target='_blank' href=\"" + PortalUtil.getBaseURL() + "file/download/" + file.getNeoId() + "\"'; ");
									retorno.append("			><img src=\"imagens/icones_final/document_search_16x16-trans.png\" align=\"absmiddle\" alt=" + file.getName() + "> ");
									retorno.append("			" + file.getName());
									retorno.append("</a>");
								}

							}
							catch (SQLException e)
							{
								e.printStackTrace();
							}
						}

					}
					retorno.append("</td>");
				}
				else
				{
					retorno.append("<td></td><td></td>");
				}
				BigDecimal vlrPedTot = new BigDecimal(0);
				if (wPedido.getValue("vlrFinalPedido") != null)
				{
					vlrPedTot = new BigDecimal(String.valueOf(wPedido.getValue("vlrFinalPedido")));
					retorno.append("			<td style=\"white-space: normal\">R$ " + wPedido.getValue("vlrFinalPedido") + "</td>");
					totalPedidos = totalPedidos.add(vlrPedTot);
				}
				else
				{
					retorno.append("			<td style=\"white-space: normal\">R$ 0.00</td>");
				}
				retorno.append("		</tr>");
			}
			retorno.append("		<tr>");
			retorno.append("<td colspan=\"6\" style=\"white-space: normal; text-align: right; \"><b>Total:</b> </td>");
			if (totalPedidos.doubleValue() > 0)
			{
				retorno.append("<td   style=\"white-space: normal; text-align: left; \"><b>R$ " + totalPedidos + "</b></td>");
			}
			else
			{
				retorno.append("<td   style=\"white-space: normal; text-align: left; \"><b>R$ 0.00</b></td>");
			}

			retorno.append("		</tr>");
			retorno.append("		</tbody>");
			retorno.append("		</table>");
		}

		return retorno.toString();

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
