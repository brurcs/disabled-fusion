package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.com.segware.sigmaWebServices.webServices.EventoRecebido;
import br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaRemoveEvento implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaRemoveEvento.class);

	public void execute(CustomJobContext arg0)
	{

		log.warn("##### INICIAR ROTINA REMOÇÃO DE EVENTOS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 

		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			String returnFromAccess = "";
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT h.CD_EVENTO ,h.CD_CLIENTE ,c.ID_EMPRESA ,c.ID_CENTRAL ,c.PARTICAO, ");
			sql.append(" CASE WHEN A.IDOSDEFEITO = 106 THEN 106 ");
			sql.append(" WHEN A1.IDOSDEFEITO = 122 THEN 122 ");
			sql.append(" WHEN A2.IDOSDEFEITO = 123 THEN 123 ");
			sql.append(" WHEN A3.IDOSDEFEITO = 125 THEN 125 ");
			sql.append(" END AS DEFEITO ");
			sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			sql.append(" LEFT JOIN (SELECT OS.ID_ORDEM, CD_CLIENTE, IDOSDEFEITO FROM dbORDEM os WITH (NOLOCK)  ");
			sql.append(" WHERE os.IDOSDEFEITO = 106 AND os.FECHADO = 0) A ON  A.CD_CLIENTE = h.CD_CLIENTE AND h.CD_EVENTO LIKE 'XXXE'  ");
			sql.append(" LEFT JOIN (SELECT OS.ID_ORDEM, CD_CLIENTE, IDOSDEFEITO FROM dbORDEM os WITH (NOLOCK) ");
			sql.append(" WHERE os.IDOSDEFEITO = 122 AND os.FECHADO = 0) A1 ON  A1.CD_CLIENTE = h.CD_CLIENTE AND h.CD_EVENTO LIKE 'XXX1' ");
			sql.append(" LEFT JOIN (SELECT OS.ID_ORDEM, CD_CLIENTE, IDOSDEFEITO FROM dbORDEM os WITH (NOLOCK) ");
			sql.append(" WHERE os.IDOSDEFEITO = 123 AND os.FECHADO = 0) A2 ON  A2.CD_CLIENTE = h.CD_CLIENTE AND h.CD_EVENTO IN ('E302','E309') ");
			sql.append(" LEFT JOIN (SELECT OS.ID_ORDEM, CD_CLIENTE, IDOSDEFEITO FROM dbORDEM os WITH (NOLOCK) ");
			sql.append(" WHERE os.IDOSDEFEITO = 125 AND os.FECHADO = 0) A3 ON  A3.CD_CLIENTE = h.CD_CLIENTE AND h.CD_EVENTO LIKE 'E250' ");
			sql.append(" WHERE  h.FG_STATUS = 5 ");
			sql.append(" AND (A.IDOSDEFEITO IS NOT NULL OR A1.IDOSDEFEITO IS NOT NULL OR A2.IDOSDEFEITO  IS NOT NULL OR A3.IDOSDEFEITO IS NOT NULL) ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			int contador = 0;
			while (rs.next())
			{
				String evento = rs.getString("CD_EVENTO");
				String cliente = rs.getString("CD_CLIENTE");
				String empresa = rs.getString("ID_EMPRESA");
				String idCentral = rs.getString("ID_CENTRAL");
				String particao = rs.getString("PARTICAO");
				String defeito = rs.getString("DEFEITO");
				if (defeito != null && !defeito.isEmpty())
					returnFromAccess = executaServicoSegware(evento, cliente, empresa, idCentral, particao, defeito);
				contador++;

			}
			log.warn("##### EXECUTAR ROTINA REMOÇÃO DE EVENTOS GERANDO UM TOTAL DE  " + contador + "  EVENTOS TRATADOS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{
			log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("["+key+"] ##### ERRO ROTINA REMOÇÃO DE EVENTOS: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	private String executaServicoSegware(String evento, String cliente, String empresa, String idCentral, String particao, String defeito)
	{
		String returnFromAccess = "";
		String eventoAux = null;
		try
		{
			if (NeoUtils.safeIsNotNull(evento) && evento.equals("XXX1") && defeito.equals("122"))
			{
				eventoAux = "X602";
			}
			else if (NeoUtils.safeIsNotNull(evento) && (evento.contains("E302") || evento.contains("E309")) && defeito.equals("123"))
			{
				eventoAux = "X302";
			}
			else if (NeoUtils.safeIsNotNull(evento) && evento.equals("E250") && defeito.equals("125"))
			{
				eventoAux = "X250";
			}
			else if (NeoUtils.safeIsNotNull(evento) && evento.equals("XXXE"))
			{
				eventoAux = "OXXE";
			}

			if (eventoAux != null)
			{
				EventoRecebido eventoRecebido = new EventoRecebido();

				eventoRecebido.setCodigo(eventoAux);
				eventoRecebido.setData(new GregorianCalendar());
				eventoRecebido.setEmpresa(Long.parseLong(empresa));
				eventoRecebido.setIdCentral(idCentral);
				eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
				eventoRecebido.setParticao(particao);
				eventoRecebido.setProtocolo(Byte.parseByte("2"));

				ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();

				returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
		}
		finally
		{
			return returnFromAccess;
		}
	}
}
