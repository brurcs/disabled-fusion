package com.neomind.fusion.custom.orsegups.mobile.vo;

import java.io.Serializable;

public class RegionalVO implements Serializable
{
	private static final long serialVersionUID = 1666936680818171678L;
	public String id;
	public String nome;
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getNome()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	
	@Override
	public String toString()
	{
		return "RegionalVO [id=" + id + ", nome=" + nome + "]";
	}
	
	
}
