package com.neomind.fusion.custom.orsegups.mobile.vo;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "Perguntas")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "http://neomind.com/")
public class PerguntaVO {
	
	 @XmlElement(name = "desc_perg", required = true)
	private String desc_perg;
	 
	 @XmlElement(name = "tip_resposta", required = true)
	private String tip_resposta;
	 
	 @XmlElement(name = "cod_perg", required = true)
	private String cod_perg;
	 
		
	public String getCod_perg() {
		return cod_perg;
	}
	public void setCod_perg(String cod_perg) {
		this.cod_perg = cod_perg;
	}
	public String getDesc_perg() {
		return desc_perg;
	}
	public void setDesc_perg(String desc_perg) {
		this.desc_perg = desc_perg;
	}
	public String getTip_resposta() {
		return tip_resposta;
	}
	public void setTip_resposta(String tip_resposta) {
		this.tip_resposta = tip_resposta;
	}
	
	 @Override
	    public String toString() {
	        return "Perguntas{" +
	                "desc_perg=" + desc_perg +
	                ", tip_resposta='" + tip_resposta + '\'' +
	                ", cod_perg='" + cod_perg + '\'' +
	                ", desc_perg='" + desc_perg + '\'' +
	                             
	                
	                '}';
	    }
	

}
