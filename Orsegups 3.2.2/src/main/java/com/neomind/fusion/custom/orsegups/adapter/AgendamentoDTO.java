package com.neomind.fusion.custom.orsegups.adapter;

public class AgendamentoDTO {
	
	Long numeroOS;
	String defeito;
	String descricaoDefeito;
	String dataAgendamento;
	String tecnicoResponsavel;
	String razaoCliente;
	String contaSigma;	
	String regional;
	public Long getNumeroOS(){return numeroOS;}
	public void setNumeroOS(Long numeroOS){this.numeroOS = numeroOS;}
	public String getDefeito()
	{
		return defeito;
	}
	public void setDefeito(String defeito)
	{
		this.defeito = defeito;
	}
	public String getDescricaoDefeito()
	{
		return descricaoDefeito;
	}
	public void setDescricaoDefeito(String descricaoDefeito)
	{
		this.descricaoDefeito = descricaoDefeito;
	}
	public String getDataAgendamento()
	{
		return dataAgendamento;
	}
	public void setDataAgendamento(String dataAgendamento)
	{
		this.dataAgendamento = dataAgendamento;
	}
	public String getTecnicoResponsavel()
	{
		return tecnicoResponsavel;
	}
	public void setTecnicoResponsavel(String tecnicoResponsavel)
	{
		this.tecnicoResponsavel = tecnicoResponsavel;
	}
	public String getRazaoCliente()
	{
		return razaoCliente;
	}
	public void setRazaoCliente(String razaoCliente)
	{
		this.razaoCliente = razaoCliente;
	}
	public String getContaSigma()
	{
		return contaSigma;
	}
	public void setContaSigma(String contaSigma)
	{
		this.contaSigma = contaSigma;
	}
	public String getRegional()
	{
		return regional;
	}
	public void setRegional(String regional)
	{
		this.regional = regional;
	}
	
	
	
}

