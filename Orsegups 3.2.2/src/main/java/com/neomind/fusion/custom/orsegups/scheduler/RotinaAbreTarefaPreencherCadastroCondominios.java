package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaPreencherCadastroCondominios implements CustomJobAdapter {

    private static final Log log = LogFactory.getLog(RotinaAbreTarefaPreencherCadastroCondominios.class);

    public void execute(CustomJobContext ctx) {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	log.warn("INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Preencher Cadastro Condominios -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	
	QLGroupFilter filter = new QLGroupFilter("AND");
	filter.addFilter(new QLEqualsFilter("abreTarefa", Boolean.TRUE));
	
	List<NeoObject> listaDadosTarefa = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RotinaAbreTarefaPreencherCadastroCondominios"),filter);

	for (NeoObject dadosTarefa : listaDadosTarefa) {
	    
	    int numTarefa = 0;
	    String tarefas = "";

	    EntityWrapper wrapperDadosTarefa = new EntityWrapper(dadosTarefa);

	    Long neoIdSolicitante = (Long) wrapperDadosTarefa.findField("solicitante.neoId").getValue();
	    NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("neoId", neoIdSolicitante));
	    String solicitante = objSolicitante.getCode();

	    Long neoIdExecutor = (Long) wrapperDadosTarefa.findField("executor.neoId").getValue();
	    NeoUser objExecutor = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("neoId", neoIdExecutor));
	    String executor = objExecutor.getCode();

	    String titulo = (String) wrapperDadosTarefa.findField("titulo").getValue();

	    String descricao = (String) wrapperDadosTarefa.findField("descricao").getValue();
	    
	    GregorianCalendar prazo = new GregorianCalendar();

	    Long diasPrazo = (Long) wrapperDadosTarefa.findField("diasPrazo").getValue();
	    prazo = OrsegupsUtils.getSpecificWorkDay(prazo, diasPrazo);
	    prazo.set(Calendar.HOUR_OF_DAY, 23);
	    prazo.set(Calendar.MINUTE, 59);
	    prazo.set(Calendar.SECOND, 59);
	    
	    Long numExecucoes = (Long) wrapperDadosTarefa.findField("numeroDeTarefas").getValue();
	    
	    NeoFile anexo = (NeoFile) wrapperDadosTarefa.findField("anexo").getValue();
	    
	    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

	    for (int i = 0; i < numExecucoes; i++) {
		String tarefa = "";
		if (anexo != null){
		    tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo, anexo);
		}else{
		    tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);
		}
		
		
		if (tarefa != null && !tarefa.isEmpty()) {
		    numTarefa ++;
		    tarefas += tarefa + " ";
		} else {
		    log.error("ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Preencher Cadastro Condominios - ERRO AO GERAR TAREFA");
		    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
	    }
	    
	    log.info("Aberto Tarefa Simples Preencher Cadastro Condominios - Numero de tarefas: " + numTarefa + " ["+tarefas+"]");

	}
    }

}
