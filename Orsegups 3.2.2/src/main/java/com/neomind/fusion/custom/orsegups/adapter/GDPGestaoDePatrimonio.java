package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class GDPGestaoDePatrimonio implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		List<NeoObject> tramites = (List<NeoObject>) processEntity.findValue("intensTramite");

		if (NeoUtils.safeIsNotNull(tramites))
		{
			for (NeoObject tramite : tramites)
			{
				EntityWrapper tramiteWrapper = new EntityWrapper(tramite);
				Long tipoSolicitacao = (Long) tramiteWrapper.findValue("tipoSolicitacaoTramite.codigoTipoSolicitacao");

				NeoObject solicitante = (NeoObject) processEntity.findValue("solicitante");
				
				NeoObject destinoFinal = (NeoObject) tramiteWrapper.findValue("destinoFinal");
				NeoObject regionalDestino = (NeoObject) tramiteWrapper.findValue("regionalDestino");
				NeoObject departamentoDestino = (NeoObject) tramiteWrapper.findValue("departamentoDestino");
				NeoObject setorDestino = (NeoObject) tramiteWrapper.findValue("setor");

				NeoObject tipoSolic = (NeoObject) tramiteWrapper.findValue("tipoSolicitacaoTramite");

				NeoObject patrimonio = (NeoObject) tramiteWrapper.findValue("patrimonio");
				EntityWrapper patrimonioWrapper = new EntityWrapper(patrimonio);

				NeoObject setorOrigem = (NeoObject) patrimonioWrapper.findValue("setor");
				NeoObject historico = AdapterUtils.createNewEntityInstance("GDPHistoricoPatrimonio");

				switch (tipoSolicitacao.intValue())
				{
					//Descarte
					case 1:
						try
						{
							descartarPatrimonio(historico, solicitante, patrimonio, setorOrigem, tipoSolic, patrimonioWrapper, tramite, destinoFinal);
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
						break;

					//Remanejamento
					case 2:

						remanejarPatrimonio(tramiteWrapper, solicitante, setorOrigem, setorDestino, patrimonio, patrimonioWrapper, historico, tipoSolic, regionalDestino, departamentoDestino);
						break;
					
				}
			}
		}
	}

	public void remanejarPatrimonio(EntityWrapper tramiteWrapper, NeoObject solicitante, NeoObject setorOrigem, NeoObject setorDestino, NeoObject patrimonio, EntityWrapper patrimonioWrapper, NeoObject historico, NeoObject tipoSolic, NeoObject  regionalDestino, NeoObject departamentoDestino)
	{
		try
		{
			EntityWrapper solicitanteWrapper = new EntityWrapper(solicitante);

			//Salvar Patrimonio
			patrimonioWrapper.setValue("tipoSituacao", tipoSolic);
			patrimonioWrapper.setValue("regional", regionalDestino);
			patrimonioWrapper.setValue("departamento", departamentoDestino);
			patrimonioWrapper.setValue("setor", setorDestino);
			PersistEngine.persist(patrimonio);

			//Salvar Historico
			EntityWrapper historicoWrapper = new EntityWrapper(historico);
			historicoWrapper.setValue("solicitante", solicitante);
			historicoWrapper.setValue("patrimonio", patrimonio);
			historicoWrapper.setValue("dataHistorico", new GregorianCalendar());
			historicoWrapper.setValue("setorOrigem", setorOrigem);
			historicoWrapper.setValue("regionalDestino", regionalDestino);
			historicoWrapper.setValue("departamentoDestino", departamentoDestino);
			historicoWrapper.setValue("setorDestino", setorDestino);
			historicoWrapper.setValue("tipoSolicitacao", tipoSolic);
			historicoWrapper.setValue("destinoFinal", "");
			PersistEngine.persist(historico);
		}
		catch (Exception ex)
		{
			throw new WorkflowException("Erro ao inserir historico!");
		}
	}

	public void descartarPatrimonio(NeoObject historico, NeoObject solicitante, NeoObject patrimonio, NeoObject setorOrigem, NeoObject tipoSolic, EntityWrapper patrimonioWrapper, NeoObject tramite, NeoObject destinoFinal) throws Exception
	{
		try
		{
			//Salvar Patrimonio
			patrimonioWrapper.setValue("tipoSituacao", tipoSolic);
			patrimonioWrapper.setValue("destinoFinal", destinoFinal);
			patrimonioWrapper.setValue("ativo", Boolean.FALSE);
			PersistEngine.persist(patrimonio);

			//Salvar Historico
			EntityWrapper historicoWrapper = new EntityWrapper(historico);
			historicoWrapper.setValue("solicitante", solicitante);
			historicoWrapper.setValue("patrimonio", patrimonio);
			historicoWrapper.setValue("dataHistorico", new GregorianCalendar());
			historicoWrapper.setValue("setorOrigem", setorOrigem);
			historicoWrapper.setValue("tipoSolicitacao", tipoSolic);
			historicoWrapper.setValue("destinoFinal", destinoFinal);
			PersistEngine.persist(historico);
		}
		catch (Exception ex)
		{
			throw new WorkflowException("Erro ao inserir historico!");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
