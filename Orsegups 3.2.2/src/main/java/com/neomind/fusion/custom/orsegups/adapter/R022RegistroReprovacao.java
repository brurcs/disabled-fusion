package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;


//com.neomind.fusion.custom.orsegups.adapter.R022RegistroReprovacao
public class R022RegistroReprovacao implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		try{
			boolean aprovadoPresidente = NeoUtils.safeBoolean(wrapper.findValue("aprovadoPresidente"));
			boolean finalizarTarefa = NeoUtils.safeBoolean(wrapper.findValue("finalizarTarefa"));
			
			
			
			NeoObject tramite = AdapterUtils.createNewEntityInstance("HEXHistoricoTramiteHE");
			EntityWrapper wTramite = new EntityWrapper(tramite);
			wTramite.setValue("usuario", PortalUtil.getCurrentUser().getCode() );
			wTramite.setValue("obs", NeoUtils.safeOutputString(wrapper.findValue("obsTramite")) );
			
			if (aprovadoPresidente){
				wTramite.setValue("acao", "Aprovou anotações de horas extras." );
			}else if (!aprovadoPresidente && finalizarTarefa){
				wTramite.setValue("acao", "Reprovou e finalizou a tarefa." );
			}else{
				wTramite.setValue("acao", "Reprovou anotações de horas extras" );
			}
			
			PersistEngine.persist(tramite);
			
			wrapper.findField("listaTramites").addValue(tramite);
			wrapper.setValue("obsTramite","");
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Erro ao reprovar anotações de horas extras. msg: "+e.getMessage());
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}



}
