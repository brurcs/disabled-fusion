package com.neomind.fusion.custom.orsegups.maps.rest;

import java.util.Calendar;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.neomind.fusion.custom.orsegups.maps.engine.MapsRESTEngine;
import com.neomind.fusion.custom.orsegups.maps.engine.MapsRESTEngineActions;
import com.neomind.fusion.custom.orsegups.maps.tasks.MapsAutomatizarRotas;
import com.neomind.fusion.custom.orsegups.maps.vo.MapsPainelRotasVO;

@Path(value = "maps")
public class MapsRESTServices {

    //private static final Log log = LogFactory.getLog(MapsRESTServices.class);
    private MapsRESTEngine engine = new MapsRESTEngine();
    private MapsRESTEngineActions actEngine = new MapsRESTEngineActions();
    @GET
    @Path("getViaturasDisponiveis")
    @Produces("application/json")
    public List<MapsPainelRotasVO> getViaturasDisponiveis() {
	
	List<MapsPainelRotasVO> retorno = engine.getViaturasDisponiveis(); 
	
	return retorno;

    }
       
    
    @POST
    @Path("setControleRota/{viatura}/{acao}")
    public Response setControleRota(@PathParam("viatura") long viatura ,@PathParam("acao") int acao) {
	
	boolean retorno = engine.setControleRota(viatura, acao);
	
	if (retorno){
	    return Response.ok("Atualizado com sucesso!", MediaType.TEXT_PLAIN_TYPE).build();
	}else{
	    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Erro ao atualizar controle de rota").type(MediaType.TEXT_PLAIN_TYPE).build();
	}
    }
    
    @POST
    @Path("setRotaEfetiva/{viatura}/{rota}")
    public Response setRotaEfetiva(@PathParam("viatura") long viatura ,@PathParam("rota") String rota) {
	
	String retorno = engine.setRotaEfetiva(viatura, rota);
	
	if (retorno.contains("sucesso")){
	    return Response.ok("Rota alterada com sucesso!", MediaType.TEXT_PLAIN_TYPE).build();
	}else{
	    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(retorno).type(MediaType.TEXT_PLAIN_TYPE).build();
	}
    }
    
    @POST
    @Path("addRotaEfetiva/{viatura}/{rota}")
    public Response addRotaEfetiva(@PathParam("viatura") long viatura ,@PathParam("rota") String rota) {
	
	String retorno = actEngine.addRotaEfetiva(viatura, rota);
	
	if (retorno.contains("sucesso")){
	    return Response.ok("Rota adicionada com sucesso!", MediaType.TEXT_PLAIN_TYPE).build();
	}else{
	    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(retorno).type(MediaType.TEXT_PLAIN_TYPE).build();
	}
    }
    
    @POST
    @Path("removerRotaEfetiva/{viatura}/{rota}")
    public Response removerRotaEfetiva(@PathParam("viatura") long viatura ,@PathParam("rota") String rota) {
	
	String retorno = engine.removerRotaEfetiva(viatura, rota);
	
	if (retorno.contains("sucesso")){
	    return Response.ok("Rota removida com sucesso!", MediaType.TEXT_PLAIN_TYPE).build();
	}else{
	    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(retorno).type(MediaType.TEXT_PLAIN_TYPE).build();
	}
    }
    
    @POST
    @Path("removerRotaEfetivaList/{viatura}/{rota}")
    public Response removerRotaEfetivaLista(@PathParam("viatura") long viatura ,@PathParam("rota") String rota) {
	
	String retorno = actEngine.removerRotaEfetivaLista(viatura, rota);
	
	if (retorno.contains("sucesso")){
	    return Response.ok("Rota removida com sucesso!", MediaType.TEXT_PLAIN_TYPE).build();
	}else{
	    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(retorno).type(MediaType.TEXT_PLAIN_TYPE).build();
	}
    }
    
    @GET
    @Path("automatizarRotas")
    @Produces("application/json")
    public Response automatizarRotas() {
	
	long inicio = Calendar.getInstance().getTimeInMillis();
	
	MapsAutomatizarRotas.getInstance().execute();
	
	long fim = Calendar.getInstance().getTimeInMillis();
	
	long total = (fim - inicio)/1000;

	return Response.ok("{\"status\":\"OK\", \"tempoExecucao\":"+total+" }", MediaType.APPLICATION_JSON).build();
	
    }

    @POST
    @Path("setControleTodasRotas/{acao}")
    public Response setControleTodasRota(@PathParam("acao") int acao) {
	
	boolean retorno = engine.setControleTodasRotas(acao);
	
	if (retorno){
	    return Response.ok("Atualizado com sucesso!", MediaType.TEXT_PLAIN_TYPE).build();
	}else{
	    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Erro ao atualizar controle de rota").type(MediaType.TEXT_PLAIN_TYPE).build();
	}
    }
    
    
}
