package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaFaturamentoOnDemand implements CustomJobAdapter {

    final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaFaturamentoOnDemand");

    private Long codHistorico;
    private Long codEmpresaSapiens;
    private Long codFil;
    private Long numCtr;
    private Long numPos;

    public void execute(CustomJobContext arg0) {

	Connection connSigma = null;
	Connection connSapiens = null;

	PreparedStatement pstm = null;
	PreparedStatement pstmMax = null;

	ResultSet rs = null;
	ResultSet rsMax = null;

	PreparedStatement stRegistro = null;

	StringBuilder sql = new StringBuilder();

	ArrayList<Integer> listaCodCliente = null;

	log.warn("##### INICIO AGENDADOR DE TAREFA: Rotina Faturamento On Demand - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	GregorianCalendar dataInicial = new GregorianCalendar();
	dataInicial.add(Calendar.MONTH, -1);
	dataInicial.set(Calendar.DAY_OF_MONTH, 1);
	dataInicial.set(Calendar.HOUR_OF_DAY, 0);
	dataInicial.set(Calendar.MINUTE, 0);
	dataInicial.set(Calendar.SECOND, 0);
	dataInicial.set(Calendar.MILLISECOND, 0);
	
	System.out.println(NeoDateUtils.safeDateFormat(dataInicial, "dd/MM/yyyy"));

	GregorianCalendar dataFim = new GregorianCalendar();
	dataFim.set(Calendar.DAY_OF_MONTH, 1);
	dataFim.set(Calendar.HOUR_OF_DAY, 0);
	dataFim.set(Calendar.MINUTE, 0);
	dataFim.set(Calendar.SECOND, 0);
	dataFim.set(Calendar.MILLISECOND, 0);

	GregorianCalendar dataCompetencia = new GregorianCalendar();
	dataCompetencia.add(Calendar.MONTH, +1);
	dataCompetencia.set(Calendar.DAY_OF_MONTH, 1);
	dataCompetencia.set(Calendar.HOUR_OF_DAY, 0);
	dataCompetencia.set(Calendar.MINUTE, 0);
	dataCompetencia.set(Calendar.SECOND, 0);
	dataCompetencia.set(Calendar.MILLISECOND, 0);

	GregorianCalendar datSis = new GregorianCalendar();
	GregorianCalendar datSisHorarioZerado = new GregorianCalendar();
	datSisHorarioZerado.set(Calendar.HOUR_OF_DAY, 0);
	datSisHorarioZerado.set(Calendar.MINUTE, 0);
	datSisHorarioZerado.set(Calendar.SECOND, 0);
	datSisHorarioZerado.set(Calendar.MILLISECOND, 0);

	try {

	    connSigma = PersistEngine.getConnection("SIGMA90");
	    connSapiens = PersistEngine.getConnection("SAPIENS"); 
	    
	    sql.append("SELECT cvs.usu_codemp, cvs.usu_codfil, cvs.usu_numctr, cvs.usu_numpos, cvs.usu_codser, cvs.usu_periss, cvs.usu_perirf, cvs.usu_perins, cvs.usu_perpit, cvs.usu_percsl, ");
	    sql.append("cvs.usu_percrt, cvs.usu_perour, cvs.usu_ctafin, cvs.usu_ctared, cvs.usu_codccu, cvs.usu_datini, cvs.usu_tnsser, his.CD_HISTORICO_ALARME, his.dt_recebido, ");
	    sql.append("cen.ID_CENTRAL, cen.PARTICAO, his.DT_FECHAMENTO,cen.CD_CLIENTE,cen.FANTASIA,cen.RAZAO, ctr.usu_SerCtr ");
	    sql.append("FROM dbCENTRAL cen WITH (NOLOCK) ");
	    sql.append("INNER JOIN HISTORICO_ALARME his WITH (NOLOCK) ON his.CD_CLIENTE = cen.CD_CLIENTE ");
	    sql.append("INNER JOIN ROTA rot WITH (NOLOCK) ON  rot.CD_ROTA = cen.ID_ROTA ");
	    sql.append("LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160SIG sig WITH (NOLOCK) ON sig.usu_codcli = cen.CD_CLIENTE "); 
	    sql.append("LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CVS cvs WITH (NOLOCK) ON cvs.usu_codemp = sig.usu_codemp "); 
	    sql.append("AND cvs.usu_codfil = sig.usu_codfil AND cvs.usu_numctr = sig.usu_numctr ");
	    sql.append("AND cvs.usu_numpos = sig.usu_numpos AND  ((cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' AND cvs.usu_datfim >= Getdate())) ");
	    sql.append("LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CTR CTR WITH(NOLOCK) ON cvs.usu_codemp = ctr.usu_codemp "); 
	    sql.append("AND cvs.usu_codfil = ctr.usu_codfil AND cvs.usu_numctr = ctr.usu_numctr ");
	    sql.append("WHERE his.CD_EVENTO = 'EODM' ");
	    sql.append("AND(ctr.usu_SerCtr = 5 OR ctr.usu_SerCtr = 6) ");
	    sql.append("AND (sig.usu_numpos IS NULL OR (sig.usu_numpos IS NOT NULL AND cvs.usu_numpos IS NOT NULL)) ");
	    sql.append("AND his.DT_FECHAMENTO >= ? AND his.DT_FECHAMENTO < ? ");
	    sql.append("AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_RotinaFaturamentoOnDemand rot WITH (NOLOCK) WHERE rot.cdHistorico = his.CD_HISTORICO_ALARME) ");
	    sql.append("AND his.TX_OBSERVACAO_FECHAMENTO NOT LIKE ('%Fechado Automaticamente%') ");
	    sql.append("AND his.TX_OBSERVACAO_FECHAMENTO NOT LIKE ('%Monitoramento desabilitado%') ");
	    sql.append("ORDER BY his.CD_HISTORICO_ALARME ");

	    pstm = connSigma.prepareStatement(sql.toString());
	    pstm.setTimestamp(1, new Timestamp(dataInicial.getTimeInMillis()));
	    pstm.setTimestamp(2, new Timestamp(dataFim.getTimeInMillis()));

	    rs = pstm.executeQuery();

	    listaCodCliente = new ArrayList<Integer>();

	    Long codHistoricoAnt = 0L;

	    while (rs.next()) {

		this.codHistorico = rs.getLong("CD_HISTORICO_ALARME");
		this.codFil = rs.getLong("usu_codfil");
		this.numCtr = rs.getLong("usu_numctr");
		this.numPos = rs.getLong("usu_numpos");
		this.codEmpresaSapiens = rs.getLong("usu_codemp");

		String idConta = rs.getString("ID_CENTRAL");
		String particao = rs.getString("PARTICAO");
		String razaoSocial = rs.getString("RAZAO");
		String nomeFantasia = rs.getString("FANTASIA");
		String dataRecebido = NeoDateUtils.safeDateFormat(rs.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:ss.SSS");
		String dataFechamento = NeoDateUtils.safeDateFormat(rs.getTimestamp("DT_FECHAMENTO"), "dd/MM/yyyy HH:mm:ss.SSS");

		/* Mais de um posto no Sapiens para a conta Sigma não deverá duplicar o faturamento */
		if (codHistoricoAnt != 0L && codHistorico.toString().equals(codHistoricoAnt.toString())) {
		    continue;
		}

		/* Conta Sigma não possui vínculo com Posto Sapiens */
		if (codEmpresaSapiens == null || codEmpresaSapiens == 0L) {
		    int tipoCobranca = 0;
		    this.abreTarefaCobranca(tipoCobranca, idConta, particao, razaoSocial, nomeFantasia, codHistorico, dataRecebido, dataFechamento);
		    codHistoricoAnt = codHistorico;

		} else if (rs.getInt("usu_SerCtr") == 5) { // Contrato On Demand Mensal
		    /* Verifica número de interações sobre uma conta, só deve interar no segundo evento do contrato mensal */
		    if (!listaCodCliente.contains(rs.getInt("CD_CLIENTE"))) {
			listaCodCliente.add(rs.getInt("CD_CLIENTE"));
			continue;
		    } else {

			String codSer = "9002042";
			String cplCvs = "Pronto Atendimento – On Demand";
			Float perIss = rs.getFloat("usu_periss");
			Float perIrf = rs.getFloat("usu_perirf");
			Float perIns = rs.getFloat("usu_perins");
			Float perPit = rs.getFloat("usu_perpit");
			Float perCsl = rs.getFloat("usu_percsl");
			Float perCrt = rs.getFloat("usu_percrt");
			Float perOur = rs.getFloat("usu_perour");
			Long ctaFin = rs.getLong("usu_ctafin");
			Long ctaRed = rs.getLong("usu_ctared");
			String codCcu = rs.getString("usu_codccu");
			Date datInn = rs.getDate("usu_datini");
			String tnsSer = rs.getString("usu_tnsser");
			Timestamp dataEventoRecebido = rs.getTimestamp("DT_RECEBIDO");
			Timestamp dataEventoFechado = rs.getTimestamp("DT_FECHAMENTO");
			String obsCms = "Cobrança Orsegups OnDemand referente ao evento recebido em " + NeoDateUtils.safeDateFormat(dataEventoRecebido, "dd/MM/yyyy HH:mm:ss") + " e fechado em: "
				+ NeoDateUtils.safeDateFormat(dataEventoFechado, "dd/MM/yyyy HH:mm:ss");

			Float preUni = 30.00f;
			GregorianCalendar dat1900 = new GregorianCalendar(1900, 11, 31);

			StringBuilder sqlMax = new StringBuilder();
			sqlMax.append("SELECT MAX(ISNULL(USU_SeqMov,0))+1 AS seqMov FROM USU_T160CMS");

			pstmMax = connSapiens.prepareStatement(sqlMax.toString());
			rsMax = pstmMax.executeQuery();

			Long seqMov = 0L;
			if (rsMax.next()) {
			    seqMov = rsMax.getLong("seqMov");
			} else {
			    log.error("##### ERRO AGENDADOR DE TAREFA: Rotina Faturamento On Demand - Problema ao buscar sequencia CMS no Sapiens ");
			    continue;
			}

			StringBuffer sqlRegistro = new StringBuffer();

			sqlRegistro.append(" INSERT INTO USU_T160CMS VALUES (?,?,?,?,?,?,?,?,'UN',0,1,?,?,?,?,?,?,?,?,?,?,?,?,?,?,100,?,?,0,'',0,?,'+',?,30,'N','S','M',0,?,'N',998) ");

			stRegistro = connSapiens.prepareStatement(sqlRegistro.toString());

			stRegistro.setLong(1, codEmpresaSapiens);
			stRegistro.setLong(2, codFil);
			stRegistro.setLong(3, numCtr);
			stRegistro.setLong(4, numPos);
			stRegistro.setLong(5, seqMov);

			stRegistro.setTimestamp(6, new Timestamp(datSisHorarioZerado.getTimeInMillis()));
			stRegistro.setString(7, codSer);
			stRegistro.setString(8, cplCvs);
			stRegistro.setFloat(9, preUni);
			stRegistro.setFloat(10, perIss);
			stRegistro.setFloat(11, perIrf);
			stRegistro.setFloat(12, perIns);
			stRegistro.setFloat(13, perPit);
			stRegistro.setFloat(14, perCsl);
			stRegistro.setFloat(15, perCrt);
			stRegistro.setFloat(16, perOur);
			stRegistro.setLong(17, ctaFin);
			stRegistro.setLong(18, ctaRed);
			stRegistro.setString(19, codCcu);
			stRegistro.setTimestamp(20, new Timestamp(datInn.getTime()));
			stRegistro.setTimestamp(21, new Timestamp(dat1900.getTimeInMillis()));
			stRegistro.setString(22, tnsSer);
			stRegistro.setTimestamp(23, new Timestamp(datSisHorarioZerado.getTimeInMillis()));
			stRegistro.setLong(24, datSis.get(Calendar.HOUR_OF_DAY) * 60 + datSis.get(Calendar.MINUTE));
			stRegistro.setTimestamp(25, new Timestamp(dataCompetencia.getTimeInMillis()));
			stRegistro.setString(26, obsCms);
			stRegistro.setTimestamp(27, new Timestamp(dat1900.getTimeInMillis()));

			stRegistro.executeUpdate();
			stRegistro.close();

			this.gravarHistorico();

			codHistoricoAnt = codHistorico;
		    }
		} else if (rs.getInt("usu_SerCtr") == 6) { // Contrato On Demand Anual
		    int tipoCobranca = 1;
		    this.abreTarefaCobranca(tipoCobranca, idConta, particao, razaoSocial, nomeFantasia, codHistorico, dataRecebido, dataFechamento);
		    codHistoricoAnt = codHistorico;
		}

	    }

	} catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: Rotina Faturamento On Demand");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(connSapiens, pstmMax, rsMax);
	    OrsegupsUtils.closeConnection(connSigma, pstm, rs);
	}
    }

    private void gravarHistorico() {

	InstantiableEntityInfo tarefaAD = AdapterUtils.getInstantiableEntityInfo("RotinaFaturamentoOnDemand");
	NeoObject noAD = tarefaAD.createNewInstance();
	EntityWrapper ajWrapper = new EntityWrapper(noAD);

	ajWrapper.findField("cdHistorico").setValue(codHistorico);
	ajWrapper.findField("codemp").setValue(codEmpresaSapiens);
	ajWrapper.findField("codfil").setValue(codFil);
	ajWrapper.findField("numctr").setValue(numCtr);
	ajWrapper.findField("numpos").setValue(numPos);
	PersistEngine.persist(noAD);
    }
    private void gravarHistorico(String tarefa) {

	InstantiableEntityInfo tarefaAD = AdapterUtils.getInstantiableEntityInfo("RotinaFaturamentoOnDemand");
	NeoObject noAD = tarefaAD.createNewInstance();
	EntityWrapper ajWrapper = new EntityWrapper(noAD);

	ajWrapper.findField("cdHistorico").setValue(codHistorico);
	ajWrapper.findField("codemp").setValue(codEmpresaSapiens);
	ajWrapper.findField("codfil").setValue(codFil);
	ajWrapper.findField("numctr").setValue(numCtr);
	ajWrapper.findField("numpos").setValue(numPos);
	ajWrapper.findField("tarefa").setValue(tarefa);
	PersistEngine.persist(noAD);
    }

    /**
     * Abre tarefa referente cobrança On Demand para clientes com conta Sigma
     * sem vinculo com Posto no Sapiens e para clientes com Contrato On Demand
     * Anual
     * 
     * @param tipoCobranca
     *            0 = Cobrança Conta Sigma sem Posto. 1 = Cobrança On Demand
     *            contrato Anual
     * @param idConta
     * @param particao
     * @param razaoSocial
     * @param nomeFantasia
     * @param codRegional
     * @param codHistorico
     * @param dataRecebido
     * @param dataFechamento
     */
    private void abreTarefaCobranca(int tipoCobranca, String idConta, String particao, String razaoSocial, String nomeFantasia, Long codHistorico, String dataRecebido, String dataFechamento) {

	String solicitante = "fernanda.maciel";
	String nomePapel = "ResponsavelFaturamentoOnDemand";
	NeoPaper papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
	String executor = "";

	if (papel != null) {
	    for (NeoUser user : papel.getUsers()) {
		executor = user.getCode();
		break;
	    }
	}

	GregorianCalendar prazo = new GregorianCalendar();
	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);

	String titulo = "Efetuar o procedimento de faturamento On Demand para conta Sigma - " + idConta + "[" + particao + "] " + nomeFantasia;

	String descricao = "";

	if (tipoCobranca == 0) {
	    descricao = "Realizar Cobrança Orsegups OnDemand referente ao evento recebido em " + dataRecebido + " e fechado em: " + dataFechamento + ", devido a conta Sigma não possuir vínculo com posto Sapiens.<br>";
	} else {
	    descricao = "Realizar Cobrança Orsegups OnDemand Anual referente ao evento recebido em " + dataRecebido + " e fechado em: " + dataFechamento + ".<br>";
	}

	descricao += "<strong>Conta:</strong> " + idConta + "[" + particao + "] <br>" 
		+ "<strong>Nome Fantasia:</strong> " + nomeFantasia + "<br>" 
		+ "<strong>Razão Social:</strong> " + razaoSocial + "<br>"
		+ "<strong>Evento recebido em:</strong> " + dataRecebido + "<br>" 
		+ "<strong>Evento fechado em:</strong> " + dataFechamento + "<br>";

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);

	if (tarefa != null && !tarefa.isEmpty()) {
	    this.gravarHistorico(tarefa);
	}

	log.warn("##### AGENDADOR DE TAREFA: Rotina Faturamento On Demand - Responsavel " + executor);
    }

}