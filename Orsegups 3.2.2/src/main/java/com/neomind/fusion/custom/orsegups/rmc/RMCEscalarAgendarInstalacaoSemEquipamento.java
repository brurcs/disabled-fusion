package com.neomind.fusion.custom.orsegups.rmc;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RMCEscalarAgendarInstalacaoSemEquipamento implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		try{
			Boolean isRastreamento = (Boolean) processEntity.findValue("isRastreamento");
			NeoPaper neoP = new NeoPaper();
			if (isRastreamento) {
				neoP = OrsegupsUtils.getPaper("supervisorOperacionalRastreamento");
			    processEntity.findField("ResponsavelEscalada").setValue(neoP);
			} else {
				neoP = OrsegupsUtils.getPaper("Gerente de Monitoramento");
			    processEntity.findField("ResponsavelEscalada").setValue(neoP);
			}
		}catch (Exception e){
		    e.printStackTrace();
		    throw new WorkflowException(e.getMessage());
		}
	    }

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		// TODO Auto-generated method stub
		
	}

}

//com.neomind.fusion.custom.orsegups.rmc.RMCEscalarAgendarInstalacaoSemEquipamento