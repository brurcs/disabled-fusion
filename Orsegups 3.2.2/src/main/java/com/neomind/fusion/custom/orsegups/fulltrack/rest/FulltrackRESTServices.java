package com.neomind.fusion.custom.orsegups.fulltrack.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.neomind.fusion.custom.orsegups.fulltrack.engine.CarImport;
import com.neomind.fusion.custom.orsegups.fulltrack.engine.ClientCreate;
import com.neomind.fusion.custom.orsegups.fulltrack.engine.ClientImport;
import com.neomind.fusion.custom.orsegups.fulltrack.engine.TrackerImport;
import com.neomind.fusion.custom.orsegups.fulltrack.engine.VehicleCreate;

@Path(value = "fulltrack")
public class FulltrackRESTServices {
    
    
    private ClientCreate clienteUtils = new ClientCreate();
    private VehicleCreate veiculoUtils = new VehicleCreate();

    @GET
    @Path("runCarImport")
    @Produces(MediaType.TEXT_PLAIN)
    public String runCarImport(){
	
	CarImport.run();
	
	return "Biiirl";
    }
    
    @GET
    @Path("runTrackerImport")
    @Produces(MediaType.TEXT_PLAIN)
    public String runTrackerImport(){
	
	TrackerImport.run();
	
	return "Fim da execução!";
    }
    
    @GET
    @Path("runClientImport")
    @Produces(MediaType.TEXT_PLAIN)
    public String runClientImport(){
	
	ClientImport.run();
	
	return "Fim da execução!";
    }
    
    
    @GET
    @Path("runClientCreate")
    @Produces(MediaType.TEXT_PLAIN)
    public String runClientCreate(){
	
	clienteUtils.createClients();
	
	return "Fim da execução!";
    }
    
    @GET
    @Path("runVehicleCreate")
    @Produces(MediaType.TEXT_PLAIN)
    public String runVehicleCreate(){
	
	veiculoUtils.createVehicles();
	
	return "Fim da execução!";
    }
    
}
