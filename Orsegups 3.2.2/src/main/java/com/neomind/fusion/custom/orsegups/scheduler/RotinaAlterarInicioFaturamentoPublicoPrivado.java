package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.ApontamentoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * Criado rotina para suprir contrato privados e publicos, porém devido a problema apresentado e tarefa 1718268 esta rotina
 * vai rodar a somente para o publico.
 * 
 * @author carlos
 *
 */
public class RotinaAlterarInicioFaturamentoPublicoPrivado implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection("FUSIONPROD"); 
		Connection connSap = PersistEngine.getConnection("SAPIENS");
		StringBuilder sql = new StringBuilder();
		ResultSet rs = null;
 		PreparedStatement ps = null;
 		PreparedStatement psSap = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAlterarInicioFaturamentoPublicoPrivado");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Rotina Alterar Inicio Faturamento - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		StringBuilder htmlFimPublico = new StringBuilder();
		StringBuilder htmlPos = new StringBuilder();
		StringBuilder htmlPosPublico = new StringBuilder();
		
		htmlFimPublico.append("\n <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> ");
		htmlFimPublico.append("\n <html> ");
		htmlFimPublico.append("\n 		<head> ");
		htmlFimPublico.append("\n 			<meta charset=\"ISO-8859-1\"> ");
		htmlFimPublico.append("\n 			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");
		htmlFimPublico.append("\n 		</head> ");
		htmlFimPublico.append("\n 		<body yahoo=\"yahoo\"> ");
		
		htmlFimPublico.append("\n 		Prezado(a), ");
		htmlFimPublico.append("\n 		<br> ");
		htmlFimPublico.append("\n 		Segue abaixo listagem de contratos e postos que foram ajustados a data inicio do faturamento conforme fechamento de OS: ");
		htmlFimPublico.append("\n 		<br> ");
		htmlFimPublico.append("\n 		<br> ");
		
		htmlPos.append("\n 			<table width=\"100%\"cellspacing=\"0\" cellpadding=\"2\" border=\"1\"> ");
		htmlPos.append("\n 					<tr> ");
		htmlPos.append("\n 						<td colspan=\"10\"align=\"center\">");
		htmlPos.append("\n 							<strong>Lista de Postos</strong> ");
		htmlPos.append("\n 						</td> ");
		htmlPos.append("\n 					</tr> ");
		htmlPos.append("\n 					<tr> ");
		htmlPos.append("\n 						<td align=\"center\"> ");
		htmlPos.append("\n 							<strong>OS</strong> ");
		htmlPos.append("\n 						</td> ");
		htmlPos.append("\n 						<td align=\"center\"> ");
		htmlPos.append("\n 							<strong>Conta Sigma</strong> ");
		htmlPos.append("\n 						</td> ");
		htmlPos.append("\n 						<td align=\"center\"> ");
		htmlPos.append("\n 							<strong>Empresa</strong> ");
		htmlPos.append("\n 						</td> ");
		htmlPos.append("\n 						<td align=\"center\"> ");
		htmlPos.append("\n 							<strong>Filial</strong> ");
		htmlPos.append("\n 						</td> ");
		htmlPos.append("\n 						<td align=\"center\"> ");
		htmlPos.append("\n 							<strong>Contrato</strong> ");
		htmlPos.append("\n 						</td> ");
		htmlPos.append("\n 						<td align=\"center\"> ");
		htmlPos.append("\n 							<strong>Data inicio Contrato / (AJUSTADO)</strong> ");
		htmlPos.append("\n 						</td> ");
		htmlPos.append("\n 						<td align=\"center\"> ");
		htmlPos.append("\n 							<strong>Posto</strong> ");
		htmlPos.append("\n 						</td> ");
		htmlPos.append("\n 						<td align=\"center\"> ");
		htmlPos.append("\n 							<strong>Data inicio Posto / (AJUSTADO)</strong> ");
		htmlPos.append("\n 						</td> ");		
		htmlPos.append("\n 					</tr>");
		
		htmlFimPublico.append(htmlPosPublico);
		htmlFimPublico.append(htmlPos);
		
		
		StringBuilder sqlSap = new StringBuilder();
				
		try
		{
			
			sql.append("SELECT ORD.ID_ORDEM, ");
			sql.append("              CEN.CD_CLIENTE, ");
			sql.append("              CEN.ID_CENTRAL, ");
			sql.append("              CEN.PARTICAO, ");
			sql.append("              CTR.USU_CODEMP, ");
			sql.append("              CTR.USU_CODFIL, ");
			sql.append("              CTR.USU_NUMCTR, ");
			sql.append("              POS.USU_NUMPOS, ");
			sql.append("              POS.USU_CODSER, ");
			sql.append("              CTR.USU_DATINI AS DATINICTR, ");
			sql.append("              POS.USU_DATINI AS DATINIPOS, ");
			sql.append("              ORD.ABERTURA AS ABERTURA, ");
			sql.append("              ORD.FECHAMENTO AS FECHAMENTO, ");
			sql.append("              ORD.EXECUTADO, ");
			sql.append("              CLI.TIPEMC, ");
			sql.append("              CTR.USU_SUSFAT, ");
			sql.append("              POS.USU_LIBFAT ");
			sql.append("         FROM [FSOODB03\\sql01].SIGMA90.DBO.DBORDEM ORD ");
			sql.append("   INNER JOIN [FSOODB03\\sql01].SIGMA90.DBO.DBCENTRAL CEN ON CEN.CD_CLIENTE = ORD.CD_CLIENTE ");
			sql.append("   INNER JOIN [FSOODB04\\sql02].SAPIENS.DBO.USU_T160SIG SIG ON SIG.USU_CODCLI = ORD.CD_CLIENTE ");
			sql.append("   INNER JOIN [FSOODB04\\sql02].SAPIENS.DBO.USU_T160CVS POS ON POS.USU_CODEMP = SIG.USU_CODEMP ");
			sql.append("    													  AND POS.USU_CODFIL = SIG.USU_CODFIL ");
			sql.append("    													  AND POS.USU_NUMCTR = SIG.USU_NUMCTR ");
			sql.append("    													  AND POS.USU_NUMPOS = SIG.USU_NUMPOS ");
			sql.append("   INNER JOIN [FSOODB04\\sql02].SAPIENS.DBO.USU_T160CTR CTR ON CTR.USU_CODEMP = POS.USU_CODEMP ");
			sql.append("   														  AND CTR.USU_CODFIL = POS.USU_CODFIL ");
			sql.append("   														  AND CTR.USU_NUMCTR = POS.USU_NUMCTR ");
			sql.append("   INNER JOIN [FSOODB04\\sql02].SAPIENS.DBO.E085CLI CLI ON CLI.CODCLI = CTR.USU_CODCLI ");
			sql.append("        WHERE ORD.IDOSDEFEITO IN (134,178,184) ");// --  134	OS-HABILITAR MONITORAMENTO |  178	OS-REALIZAR REINSTALACAO | 184	OS-REALIZAR INSTALACAO			
			sql.append("          AND ORD.ABERTURA > '2019-04-01' ");
			sql.append("          AND ORD.FECHADO = 1 ");
			sql.append("          AND CEN.FG_ATIVO = 1 ");
			sql.append("          AND CEN.CTRL_CENTRAL = 1 ");
			sql.append("          AND ((CTR.USU_SUSFAT = 'S') OR (POS.USU_SUSFAT = 'S')) ");
			sql.append("		  AND (CTR.USU_SITCTR = 'A' OR (CTR.USU_SITCTR = 'I' AND CTR.USU_DATFIM > GETDATE())) ");
			sql.append("		  AND (POS.USU_SITCVS = 'A' OR (POS.USU_SITCVS = 'I' AND POS.USU_DATFIM > GETDATE())) ");
			sql.append("          AND CLI.TIPEMC = 2 ");
			sql.append("          AND EXISTS (SELECT 1 ");
			sql.append("                        FROM [FSOODB03\\sql01].SIGMA90.DBO.ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA MP ");
			sql.append("                  INNER JOIN [FSOODB03\\sql01].SIGMA90.DBO.MOTIVO_PAUSA P ON MP.CD_MOTIVO_PAUSA = P.CD_MOTIVO_PAUSA ");
			sql.append("                       WHERE MP.CD_ORDEM_SERVICO = ORD.ID_ORDEM ");
			sql.append("                         AND P.NM_DESCRICAO LIKE 'FAP%') ");
			sql.append("     ORDER BY 5,6,7,13");
			
			ps = conn.prepareStatement(sql.toString());
			rs = ps.executeQuery();
			Integer numCtrAnt = 0;
			
			int totalContrato = 0;
			int totalPosto = 0;
			
			LinkedList<ApontamentoVO> apontamentosLista = new LinkedList<>();
			
			while (rs.next()) {
				
				Integer idOS = rs.getInt("ID_ORDEM");
				String idCentral = rs.getString("ID_CENTRAL");
				String particao = rs.getString("PARTICAO");
				Integer codEmp = rs.getInt("usu_codemp");
				Integer codFil = rs.getInt("usu_codfil");
				Integer numCtr = rs.getInt("usu_numctr");
				Integer numPos = rs.getInt("usu_numpos");
				Date datIniCtr = rs.getDate("datIniCtr");
				Date datIniPos = rs.getDate("datIniPos");
				Date datFimOs = rs.getDate("FECHAMENTO");
				String susFat = rs.getString("usu_susfat");
				
				htmlPos = new StringBuilder();
				
				htmlPos.append("\n 					<tr> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+idOS);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+idCentral+"["+particao+"]");
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+codEmp);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+codFil);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+numCtr);
				htmlPos.append("\n 						</td> ");
								
				if (numCtrAnt == 0 || !numCtrAnt.toString().equals(numCtr.toString()))
				{	
					
					apontamentosLista = verificaApontamentosPosto(codEmp, codFil, numCtr, numPos, datFimOs);
					
					if(NeoUtils.safeIsNotNull(apontamentosLista) && !apontamentosLista.isEmpty()) {
						atualizaCompetenciaApontamentos(apontamentosLista);
					}
					
					sqlSap.append(" Update USU_T160CTR Set usu_datini = '" + NeoDateUtils.safeDateFormat(datFimOs, "yyyy-MM-dd") + "', usu_susfat = 'N' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_susfat = 'S'; ");
					sqlSap.append(" Update USU_T160CVS Set usu_datini = '" + NeoDateUtils.safeDateFormat(datFimOs, "yyyy-MM-dd") + "', usu_susfat = 'N' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_numpos = " + numPos + "; ");
					
					String textoLiberado = "";
					if ("S".equals(susFat)) {
						textoLiberado = " liberado";
					}
					
					htmlPos.append("\n 						<td> ");
					htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datIniCtr, "dd/MM/yyyy") + (("S".equals(susFat))?" / ("+NeoDateUtils.safeDateFormat(datFimOs, "dd/MM/yyyy")+")" : "") + textoLiberado);
					htmlPos.append("\n 						</td> ");
					htmlPos.append("\n 						<td> ");
					htmlPos.append("\n 							"+numPos);
					htmlPos.append("\n 						</td> ");
					htmlPos.append("\n 						<td> ");
					htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datIniPos, "dd/MM/yyyy")+" / ("+NeoDateUtils.safeDateFormat(datFimOs, "dd/MM/yyyy")+") liberado");
					htmlPos.append("\n 						</td> ");
					totalContrato ++;
				} else {
					sqlSap.append(" Update USU_T160CVS Set usu_datini = '" + NeoDateUtils.safeDateFormat(datFimOs, "yyyy-MM-dd") + "', usu_susfat = 'N' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_numpos = " + numPos + "; ");
					
					htmlPos.append("\n 						<td> ");
					htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datIniCtr, "dd/MM/yyyy"));
					htmlPos.append("\n 						</td> ");
					htmlPos.append("\n 						<td> ");
					htmlPos.append("\n 							"+numPos);
					htmlPos.append("\n 						</td> ");
					htmlPos.append("\n 						<td> ");
					htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datIniPos, "dd/MM/yyyy")+" / ("+NeoDateUtils.safeDateFormat(datFimOs, "dd/MM/yyyy")+") liberado");
					htmlPos.append("\n 						</td> ");
				}				
				
				htmlPos.append("\n 					</tr>");
				
				htmlPosPublico.append(htmlPos.toString());
				
				numCtrAnt = numCtr;		
				totalPosto++;
			}
			if (!sqlSap.toString().isEmpty()){
				System.out.println(sqlSap);
			    connSap.prepareStatement(sqlSap.toString());
			    psSap = connSap.prepareStatement(sqlSap.toString());
			    psSap.executeUpdate();
			}
			
			htmlFimPublico.append(htmlPosPublico);
			
			htmlFimPublico.append("\n 			</table> ");
			htmlFimPublico.append("\n 			</br> ");
			htmlFimPublico.append("\n <p>Total de Contratos atualizados: "+totalContrato);
			htmlFimPublico.append("\n <p>Total de Postos atualizados: "+totalPosto);
			if(NeoUtils.safeIsNotNull(apontamentosLista) && !apontamentosLista.isEmpty()) {
				htmlFimPublico.append(montaDescricaoApontamentos(apontamentosLista));
			}
			htmlFimPublico.append("\n 		</body> ");
			htmlFimPublico.append("\n </html> ");
			
			System.out.println(htmlFimPublico.toString());
			
			//String emailPublico = "liberacaofaturamentopublico@orsegups.com.br";
			//String emailPublico = "carlos.silva@orsegups.com.br";
			String emailPublico = "mariane.souza@orsegups.com.br";
			
			OrsegupsUtils.sendEmail2Orsegups(emailPublico, "fusion@orsegups.com.br", "[SAPIENS] Alteração da data inicio do contrato e posto (Público)", "", htmlFimPublico.toString());
			
						
		}
		catch (Exception e)
		{
			log.error("##### AGENDADOR DE TAREFA: Rotina Faturamento On Demand");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				if (psSap != null){
				    psSap.close();
				}
				ps.close();
				connSap.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public LinkedList<ApontamentoVO> verificaApontamentosPosto(Integer empresa, Integer filial, Integer contrato, Integer posto, Date dataFinalOs) {
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Connection conn = PersistEngine.getConnection("SAPIENS");
		
		LinkedList<ApontamentoVO> listaApontamentos = new LinkedList<>();
		
		try {
			
			sql.append(" SELECT CMS.USU_DATCPT AS 'COMPETÊNCIA', CMS.USU_CODFIL AS 'FILIAL', CMS.USU_CODEMP AS 'EMPRESA', CMS.USU_NUMCTR AS 'CONTRATO', "
						+ "CMS.USU_NUMPOS AS 'POSTO', CMS.USU_SEQMOV AS 'SEQUÊNCIA', CONVERT(DECIMAL(18,2),CMS.USU_PREUNI) AS 'VALOR UNITÁRIO', CMS.USU_ADCSUB AS 'ADICIONA/SUBTRAI' ");
			sql.append(" FROM USU_T160CVS CVS ");
			sql.append(" INNER JOIN USU_T160CMS CMS ");
			sql.append(" ON CVS.USU_CODEMP = CMS.USU_CODEMP ");
			sql.append(" AND CVS.USU_CODFIL = CMS.USU_CODFIL ");
			sql.append(" AND CVS.USU_NUMCTR = CMS.USU_NUMCTR ");
			sql.append(" AND CVS.USU_NUMPOS = CMS.USU_NUMPOS ");
			sql.append(" WHERE CMS.USU_CODEMP = ? ");
			sql.append(" AND CMS.USU_CODFIL = ? ");
			sql.append(" AND CMS.USU_NUMCTR = ? ");
			sql.append(" AND CMS.USU_NUMPOS = ? ");
			sql.append(" AND ((CMS.USU_CODSER = '9002012') ");
			sql.append(" AND (CMS.USU_ADCSUB = '+') ");
			sql.append(" OR (CMS.USU_CODLAN != 51)) ");			
			sql.append(" AND (MONTH(?) < MONTH(GETDATE())) ");
			sql.append(" AND ((YEAR(USU_DATCPT) = YEAR(GETDATE())) OR ((YEAR(USU_DATCPT) < YEAR(GETDATE())))) ");
			sql.append(" ORDER BY CMS.USU_DATCPT ASC ");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setInt(1, empresa);
			pstm.setInt(2, filial);
			pstm.setInt(3, contrato);
			pstm.setInt(4, posto);
			pstm.setDate(5, dataFinalOs);
			
			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				ApontamentoVO apontamento = new ApontamentoVO();
				
				apontamento.setCompetencia(rs.getTimestamp("COMPETÊNCIA"));
				apontamento.setEmpresa(rs.getLong("EMPRESA"));
				apontamento.setFilial(rs.getLong("FILIAL"));
				apontamento.setContrato(rs.getLong("CONTRATO"));
				apontamento.setPosto(rs.getLong("POSTO"));
				apontamento.setSequenciaApontamento(rs.getLong("SEQUÊNCIA"));
				apontamento.setValorUnitario(rs.getString("VALOR UNITÁRIO"));
				apontamento.setAdicionaOuSubtrai(rs.getString("ADICIONA/SUBTRAI"));
				
				listaApontamentos.add(apontamento);
			}
		   
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("");
		}
		
		
		return listaApontamentos;
	}
	
	public void atualizaCompetenciaApontamentos(LinkedList<ApontamentoVO> listaApontamentos) {
		
		Connection conn = PersistEngine.getConnection("SAPIENS");
		PreparedStatement pstm = null;
		
		GregorianCalendar dataBase = new GregorianCalendar();
		dataBase.set(GregorianCalendar.DAY_OF_MONTH, 1);
		dataBase.set(GregorianCalendar.HOUR_OF_DAY, 0);
		dataBase.set(GregorianCalendar.MINUTE, 0);
		dataBase.set(GregorianCalendar.SECOND, 0);
		dataBase.set(GregorianCalendar.MILLISECOND, 0);
		
		int linhasAfetadas = 0;
		
		for(ApontamentoVO apontamento : listaApontamentos) {

			dataBase.add(GregorianCalendar.MONTH, 1);
			Timestamp tsDataBase = new Timestamp(dataBase.getTimeInMillis());

			StringBuilder sql = new StringBuilder();
			
			try {
				
				sql.append(" UPDATE USU_T160CMS ");
				sql.append(" SET USU_DATCPT = ? ");
				sql.append(" WHERE USU_CODEMP = ? ");
				sql.append(" AND USU_CODFIL = ? ");
				sql.append(" AND USU_NUMCTR = ? ");
				sql.append(" AND USU_NUMPOS = ? ");
				sql.append(" AND USU_SEQMOV = ? ");
				
				pstm = conn.prepareStatement(sql.toString());
				
				pstm.setTimestamp(1, tsDataBase); 
				pstm.setLong(2, apontamento.getEmpresa());
				pstm.setLong(3, apontamento.getFilial());
				pstm.setLong(4, apontamento.getContrato());
				pstm.setLong(5, apontamento.getPosto());
				pstm.setLong(6, apontamento.getSequenciaApontamento());
				
				linhasAfetadas = pstm.executeUpdate();
				
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
	}
	
	public Timestamp retornaCompetenciaAtual(ApontamentoVO apontamento) {
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Connection conn = PersistEngine.getConnection("SAPIENS");
		Timestamp novaCompetencia = null;
		
		try {
			
			sql.append(" SELECT CMS.USU_DATCPT AS 'COMPETÊNCIA' FROM USU_T160CMS CMS  ");
			sql.append(" WHERE CMS.USU_CODEMP = ? AND CMS.USU_CODFIL = ?  ");
			sql.append(" AND CMS.USU_NUMCTR = ? AND CMS.USU_NUMPOS = ?  ");
			sql.append(" AND CMS.USU_SEQMOV = ?  ");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setLong(1, apontamento.getEmpresa());
			pstm.setLong(2, apontamento.getFilial());
			pstm.setLong(3, apontamento.getContrato());
			pstm.setLong(4, apontamento.getPosto());
			pstm.setLong(5, apontamento.getSequenciaApontamento());
			
			rs = pstm.executeQuery();
			
			if(rs.next()) {
				novaCompetencia = rs.getTimestamp("COMPETÊNCIA");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return novaCompetencia;
	}
	
	public String montaDescricaoApontamentos(LinkedList<ApontamentoVO> listaApontamentos) {
		
		StringBuilder apontamentos = new StringBuilder();
		
		apontamentos.append("<table width=\"100%\"cellspacing=\"0\" cellpadding=\"2\" border=\"1\"> ");
		apontamentos.append("\n  <tr> ");
		apontamentos.append("\n   <td> Empresa </td> ");
		apontamentos.append("\n   <td> Filial </td> ");
		apontamentos.append("\n   <td> Contrato </td> ");
		apontamentos.append("\n   <td> Posto </td> ");
		apontamentos.append("\n   <td> Adiciona/Subtrai </td> ");
		apontamentos.append("\n   <td> Valor Unitário </td> ");
		apontamentos.append("\n   <td> Competência Antiga </td> ");
		apontamentos.append("\n   <td> Competência Atual </td> ");
		apontamentos.append("\n  </tr> ");
		
		
		for(ApontamentoVO apontamento : listaApontamentos) {
			
			GregorianCalendar gcCompetenciaAntiga = new GregorianCalendar();
			GregorianCalendar gcCompetenciaNova = new GregorianCalendar();
			gcCompetenciaAntiga.setTimeInMillis(apontamento.getCompetencia().getTime());
			gcCompetenciaNova.setTimeInMillis(retornaCompetenciaAtual(apontamento).getTime());
			
			String dfCompetenciaAntiga = NeoDateUtils.safeDateFormat(gcCompetenciaAntiga, "dd/MM/yyyy");
			String dfCompetenciaNova = NeoDateUtils.safeDateFormat(gcCompetenciaNova, "dd/MM/yyyy");
			
			apontamentos.append("\n <tr> ");
			apontamentos.append("\n  <td> "+apontamento.getEmpresa()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getFilial()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getContrato()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getPosto()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getAdicionaOuSubtrai()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getValorUnitario()+" </td> ");
			apontamentos.append("\n  <td> "+dfCompetenciaAntiga+" </td> ");
			apontamentos.append("\n  <td> "+dfCompetenciaNova+" </td> ");
			apontamentos.append("\n </tr> ");
			
		}
		
		apontamentos.append("\n </table> ");
		
		return apontamentos.toString();
	}
	
}