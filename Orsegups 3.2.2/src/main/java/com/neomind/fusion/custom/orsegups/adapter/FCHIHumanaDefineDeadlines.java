package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class FCHIHumanaDefineDeadlines implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{		
			GregorianCalendar startDate = (GregorianCalendar) activity.getProcess().getStartDate();
			startDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
			startDate.set(GregorianCalendar.MINUTE, 0);
			startDate.set(GregorianCalendar.SECOND, 0);
			startDate.set(GregorianCalendar.MILLISECOND, 0);
			
			// Deadline 8 dias
			GregorianCalendar deadDate = (GregorianCalendar) startDate.clone();
			deadDate = OrsegupsUtils.addDays(startDate, 5, true);
			processEntity.setValue("dataDeadPrimeiro", deadDate);
			// Deadline 14 dias
			deadDate = OrsegupsUtils.addDays(startDate, 10, true);
			processEntity.setValue("dataDeadSegundo", deadDate);
			// Deadline 20 dias
			deadDate = OrsegupsUtils.addDays(startDate, 16, true);
			processEntity.setValue("dataDeadTerceiro", deadDate);
			// Deadline 25 dias
			deadDate = OrsegupsUtils.addDays(startDate, 24, true);
			processEntity.setValue("dataDeadQuarto", deadDate);
			// Deadline 16 dias
			/*deadDate = OrsegupsUtils.addDays(startDate, 16, true);
			processEntity.setValue("dataDeadQuinto", deadDate);
			// Deadline 17 dias
			deadDate = OrsegupsUtils.addDays(startDate, 17, true);
			processEntity.setValue("dataDeadSexto", deadDate);
			// Deadline 26 dias
			deadDate = OrsegupsUtils.addDays(startDate, 26, true);
			processEntity.setValue("dataDeadSetimo", deadDate);
			// Deadline 31 dias
			deadDate = OrsegupsUtils.addDays(startDate, 31, true);
			processEntity.setValue("dataDeadOitavo", deadDate);*/
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
}
