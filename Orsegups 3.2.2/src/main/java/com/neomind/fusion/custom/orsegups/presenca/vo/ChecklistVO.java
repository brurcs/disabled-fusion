package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ChecklistVO
{
	private PostoVO posto;
	private Long sequencia;
	private Long codigoItem;
	private String item;
	private Long quantidade;
	private String prazo;
	private String status;
	private Long rmc;
	private String tarefa;
	
	public PostoVO getPosto()
	{
		return posto;
	}
	public void setPosto(PostoVO posto)
	{
		this.posto = posto;
	}
	public Long getSequencia()
	{
		return sequencia;
	}
	public void setSequencia(Long sequencia)
	{
		this.sequencia = sequencia;
	}
	public Long getCodigoItem()
	{
		return codigoItem;
	}
	public void setCodigoItem(Long codigoItem)
	{
		this.codigoItem = codigoItem;
	}
	public String getItem()
	{
		return item;
	}
	public void setItem(String item)
	{
		this.item = item;
	}
	public Long getQuantidade()
	{
		return quantidade;
	}
	public void setQuantidade(Long quantidade)
	{
		this.quantidade = quantidade;
	}
	public String getPrazo()
	{
		return prazo;
	}
	public void setPrazo(String prazo)
	{
		this.prazo = prazo;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public Long getRmc()
	{
		return rmc;
	}
	public void setRmc(Long rmc)
	{
		this.rmc = rmc;
	}
	public String getTarefa()
	{
		return tarefa;
	}
	public void setTarefa(String tarefa)
	{
		this.tarefa = tarefa;
	}
	
}
