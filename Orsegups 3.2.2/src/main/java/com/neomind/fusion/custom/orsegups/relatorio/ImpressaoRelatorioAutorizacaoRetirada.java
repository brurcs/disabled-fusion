package com.neomind.fusion.custom.orsegups.relatorio;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class ImpressaoRelatorioAutorizacaoRetirada
{

	private static final Log log = LogFactory.getLog(ImpressaoRelatorioAutorizacaoRetirada.class);
	private static final String NOME_RELATORIO = "RelatorioAutorizacaoRetiradaEPI.jasper";
	private static final String NOME_RELATORIO_AGRUPADO = "RelatorioAutorizacaoRetiradaEPI_Agrupado.jasper";
	private static final String NOME_BUSCA_PRODUTO = "SAPATO";

	/**
	 * Preenche o mapa de parâmetros enviados ao relatório.
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> preencheParametros(NeoObject dadosEForm)
	{

		Map<String, Object> paramMap = null;

		try
		{
			paramMap = new HashMap<String, Object>();
			EntityWrapper wrapper = new EntityWrapper(dadosEForm);

			paramMap.put("pathSub1", NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "RelatorioAutorizacaoRetiradaEPI_subreport.jasper");

			paramMap.put("titulo", "AUTORIZAÇÃO PARA RETIRADA");
			paramMap.put("nomeColaborador", NeoUtils.safeOutputString(wrapper.findValue("colaboradorRH.nomfun")));
			paramMap.put("cargoColaborador", NeoUtils.safeOutputString(wrapper.findValue("colaboradorRH.titred")));
			paramMap.put("empresaColaborador", NeoUtils.safeOutputString(wrapper.findValue("colaboradorRH.apeemp")));
			paramMap.put("matriculaColaborador", NeoUtils.safeOutputString(wrapper.findValue("colaboradorRH.numcad")));

			// Subrelatório (grid fichas)
			Collection<FichaDatasource> gridProdutosAutorizadosSapatos = populaGridRelatorioAutorizacao(wrapper, NOME_BUSCA_PRODUTO);
			Collection<FichaDatasource> gridProdutosAutorizados = populaGridRelatorioAutorizacao(wrapper, null);

			if(gridProdutosAutorizadosSapatos != null && gridProdutosAutorizados != null && gridProdutosAutorizadosSapatos.size() > 0 && gridProdutosAutorizados.size() > 0){

				paramMap.put("gridProdutosAutorizadosSapatos", gridProdutosAutorizadosSapatos);
				paramMap.put("gridProdutosAutorizados", gridProdutosAutorizados);
			}else{
				if(gridProdutosAutorizados != null && gridProdutosAutorizados.size() > 0)
				{
					paramMap.put("gridProdutosAutorizadosAgrupados", gridProdutosAutorizados);
				}else{
					paramMap.put("gridProdutosAutorizadosAgrupados", gridProdutosAutorizadosSapatos);
				}
			}
			return paramMap;
		}
		catch (Exception e)
		{
			log.error("Erro ao preencher o mapa de parâmetros da Ficha!", e);
		}
		return paramMap;
	}

	/**
	 * Gera o PDF do relatório, utilizando JasperReports
	 */
	public static File geraPDF(NeoObject dadosEForm)
	{
		InputStream is = null;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		String path = "";

		try
		{
			// ...files/relatorios
			paramMap = preencheParametros(dadosEForm); // obtém os parâmetros

			List<FichaDatasource> produtosAutorizados = (List<FichaDatasource>) paramMap.get("gridProdutosAutorizados");
			List<FichaDatasource> produtosAutorizadosSapatos = (List<FichaDatasource>) paramMap.get("gridProdutosAutorizadosSapatos");
			List<FichaDatasource> produtosAutorizadosAgrupados = (List<FichaDatasource>) paramMap.get("gridProdutosAutorizadosAgrupados");

			if(NeoUtils.safeIsNotNull(produtosAutorizados) && NeoUtils.safeIsNotNull(produtosAutorizadosSapatos) && produtosAutorizados.size() > 0 && produtosAutorizadosSapatos.size() > 0){
				path = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + NOME_RELATORIO;
			}else{
				path = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + NOME_RELATORIO_AGRUPADO;
			}
			is = new BufferedInputStream(new FileInputStream(path));

			if (paramMap != null)
			{

				File file = File.createTempFile("fusexp", ".pdf");
				file.deleteOnExit();

				JasperPrint impressao = JasperFillManager.fillReport(is, paramMap);
				if (impressao != null && file != null)
				{
					JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
					return file;
				}
			}
		}
		catch (Exception e)
		{
			log.error("Erro ao gerar o PDF do relatório de Autorização de retirada!!", e);
		}
		return null;
	}

	private static Collection<FichaDatasource> populaGridRelatorioAutorizacao(EntityWrapper wrapper, String searchProduto)
	{
		return populaGridRelAutorizacaoRetirada(wrapper, searchProduto);

	}

	@SuppressWarnings("unchecked")
	private static Collection<FichaDatasource> populaGridRelAutorizacaoRetirada(EntityWrapper wrapper, String searchProduto)
	{
		Collection<FichaDatasource> lista = new ArrayList();
		try
		{
			lista = new ArrayList<FichaDatasource>();
			Collection<NeoObject> fichas = (Collection<NeoObject>) wrapper.findValue("listaEntregas");
			if (fichas != null && fichas.size() > 0)
			{
				// Percorre a grid
				for (NeoObject ficha : fichas)
				{
					EntityWrapper wrapFicha = new EntityWrapper(ficha);
					if (wrapFicha != null)
					{
						// verifica se existe alguma entrega a ser impressa 
						if (wrapFicha.findValue("exibeRelatorio") != null && (Boolean) wrapFicha.findValue("exibeRelatorio") == true &&
								wrapFicha.findValue("solicitarCompra") != null && (Boolean) wrapFicha.findValue("solicitarCompra") == true )
						{
							Collection<FichaDatasource> listaTmp = createFichaDSItem(ficha, searchProduto);
							if(listaTmp != null){
								lista.addAll(listaTmp);
							}
						}
					}
				}
			}
			return lista;
		}
		catch (Exception e)
		{
			log.error("Erro ao popular a grid de fichas!", e);
		}

		return null;
	}

	private static Collection<FichaDatasource> createFichaDSItem(NeoObject ficha, String searchProduto)
	{
		FichaDatasource dsFicha = null;

		Collection<FichaDatasource> dataSource = new ArrayList<FichaDatasource>();

		if (ficha != null)
		{
			EntityWrapper wrapFicha = new EntityWrapper(ficha);
			if (wrapFicha != null)
			{
				// Busca os dados da entraga
				String produto = (String) wrapFicha.findValue("produto.despro");
				String tamanho = (String) wrapFicha.findValue("derivacao.desder");

				if(NeoUtils.safeIsNotNull(searchProduto) && produto.toUpperCase().contains(searchProduto)){

					if (wrapFicha.findValue("derivacao") != null && produto != null)
					{
						produto += " " + wrapFicha.findValue("derivacao.codder");
					}

					String quantidadeEntregueStr = "";
					Long quantidadeEntregue = (Long) wrapFicha.findValue("quantidadeEntregue");
					if (quantidadeEntregue != null)
					{
						quantidadeEntregueStr = quantidadeEntregue.toString();
					}
					String codigoSapiens = (String) wrapFicha.findValue("codigoSapiens");
					if(NeoUtils.safeIsNull(codigoSapiens)){
						codigoSapiens = "";
					}
					
					
					Boolean devolucaoTotal = (Boolean) wrapFicha.findValue("DevolucaoTotal");
					if (devolucaoTotal != null && !devolucaoTotal)
					{
						dsFicha = new FichaDatasource();
						// seta os dados da entrega no datasource
						dsFicha.setProduto(produto);
						dsFicha.setQtdEntrega(quantidadeEntregueStr);
						dsFicha.setCodigoSapiens(codigoSapiens);
						dsFicha.setTamanho(tamanho);
						// adiciona a lista de intes a serem impressos
						dataSource.add(dsFicha);

					}
					
				}

				if(NeoUtils.safeIsNull(searchProduto) && !produto.toUpperCase().contains(NOME_BUSCA_PRODUTO)){

					if (wrapFicha.findValue("derivacao") != null && produto != null)
					{
						produto += " " + wrapFicha.findValue("derivacao.codder");
					}

					String quantidadeEntregueStr = "";
					Long quantidadeEntregue = (Long) wrapFicha.findValue("quantidadeEntregue");
					if (quantidadeEntregue != null)
					{
						quantidadeEntregueStr = quantidadeEntregue.toString();
					}
					String codigoSapiens = (String) wrapFicha.findValue("codigoSapiens");
					if(NeoUtils.safeIsNull(codigoSapiens)){
						codigoSapiens = "";
					}
					Boolean devolucaoTot = (Boolean) wrapFicha.findValue("DevolucaoTotal");
					if (devolucaoTot != null && !devolucaoTot)
					{
					dsFicha = new FichaDatasource();

					// seta os dados da entrega no datasource
					dsFicha.setProduto(produto);
					dsFicha.setQtdEntrega(quantidadeEntregueStr);
					dsFicha.setCodigoSapiens(codigoSapiens);
					dsFicha.setTamanho(tamanho);

					// adiciona a lista de intes a serem impressos
					dataSource.add(dsFicha);
				}
				}
			}
		}
		return dataSource;
	}
}
