package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityFinishEventListener;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RegistraHistoricoV2 implements TaskFinishEventListener, ActivityStartEventListener, ActivityFinishEventListener
{
	private EntityWrapper processEntity;
	private NeoUser usuario;
	private String observacao;
	private NeoFile anexo;

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
				
		processEntity = event.getWrapper();
		usuario = new NeoUser();
		observacao = ""; 
		anexo = null;
		
		Task task = event.getTask();		
		usuario = event.getTask().getFinishByUser();
		
		if (task.getActivityName().toString().contains("Informar Colaborador de Férias")) {
			 
			informarColaboradorDeFerias();
			
		} else if (task.getActivityName().toString().contains("ESCALADA:")) {
			
			escalada();
			
		} else if (task.getActivityName().toString().contains("Informar Início de Férias")) {	
			
			informarInicioFerias();
			
		} else if (task.getActivityName().toString().contains("Processar Férias")) {
			
			processarFerias();
			
		} else if (task.getActivityName().toString().contains("Aguardando Data do Pagamento")) {
			
			aguardandoDataDoPagamento();
			
		} else if (task.getActivityName().toString().contains("Comunicar Vigilante Cobertura de Férias")) {	
			
			comunicarVigilanteCoberturaDeFerias();
			
		} else if (task.getActivityName().toString().contains("Confirmar Pagamento Férias - Anexar Comprovante Bancário")) {	
			
			confirmarPagamentoFeriasAnexarComprovanteBancario();
			
		} else if (task.getActivityName().toString().contains("Informar Pagamento de Férias em Espécie")) {
			
			informarPagamenteDeFeriasEmEspecie();
			
		} else if (task.getActivityName().toString().contains("Justificar Problema no Pagamento de Férias")) {
			
			justificarProblemaPagamentoDeFerias();
			
		} else if (task.getActivityName().toString().contains("Realizar Pagamento de Férias e anexar Recibo Assinado")) {
			
			realizarPagamentoDeFeriasAnexarReciboAssinado();
			
		} else if (task.getActivityName().toString().contains("Validar Recibo de Férias")) {
			
			validarReciboDeFerias();
			
		} else if (task.getActivityName().toString().contains("Tomar Ciência Cancelamento de Férias - RH (Férias Inativas no RUBI)")) {
			
			tomarCienciaCancelamentoRh();
			
		} else if (task.getActivityName().toString().contains("Tomar Ciência Cancelamento de Férias - Financeiro (Férias Inativas no RUBI)")) {
			
			tomarCienciaCancelamentoFinanceiro();
			
		} else if (task.getActivityName().toString().contains("Aguardando Abertura da Folha")) {
			
			aguardandoAberturaFolha();
			
		}
		
		if (PortalUtil.getCurrentUser() != null)
			atualizaHistoricoAtividade();
	}

	@Override
	public void onStart(ActivityEvent event) throws ActivityException {
		
		String observacao = "";
		
		final Activity act = event.getActivity();
		final Long activityNeoId = act.getNeoId();
		
		EntityWrapper processEntity = event.getWrapper();
		NeoUser usuario = new NeoUser();
		NeoFile anexo = null;
		//Task task =  event.getActivity().getInstance().getTask();
		String _atividade = event.getActivity().getInstance().getTaskName(); 
		SecurityEntity seExecutor = (SecurityEntity) processEntity.findField("executorTarefa").getValue();
		
		//usuario = event.getTask().getFinishByUser();
		
		if (_atividade.contains("ESCALADA:")) {
			
			System.out.println("Entrou em "+_atividade);
											
			NeoPaper papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", seExecutor.getCode()));
			if  (papel != null) {
				for (NeoUser noUser : papel.getAllUsers())
				{
					usuario = noUser;
					//break;
				}
			} else {
				usuario = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code", seExecutor.getCode()));
			}
			
			Activity act2 = PersistEngine.getNeoObject(Activity.class, activityNeoId);
			Task task = act2.getTaskAssigner().assign((UserActivity) act2, usuario, true);

			if (!usuario.getCode().equals("dilmoberger") && !usuario.toString().equals("presidente")) {
				RuntimeEngine.getTaskService().complete(task, usuario);
			} 
			
			InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades"); 
			NeoObject objRegAti = insRegAti.createNewInstance();
			EntityWrapper wRegAti = new EntityWrapper(objRegAti);
			
			
			GregorianCalendar dataAcao = new GregorianCalendar();
	
			wRegAti.setValue("usuario", "SISTEMA FUSION");
			wRegAti.setValue("dataAcao", dataAcao);
			wRegAti.setValue("descricao", "Tarefa ESCALADA para " + usuario.getFullName());
			wRegAti.setValue("anexo", anexo);
	
			processEntity.findField("r001V2RegistroAtividade").addValue(objRegAti);
			
		}
	}

	@Override
	public void onFinish(ActivityEvent event) throws ActivityException {
		
		final Activity act = event.getActivity();
		final Long activityNeoId = act.getNeoId();
		
		EntityWrapper processEntity = event.getWrapper();
		NeoUser usuario = new NeoUser();
		NeoFile anexo = null;
		//Task task =  event.getActivity().getInstance().getTask();
		String _atividade = event.getActivity().getInstance().getTaskName(); 
		SecurityEntity seExecutor = (SecurityEntity) processEntity.findField("executorTarefa").getValue();
		
		if (_atividade.contains("Confirmar Pagamento Férias - Anexar Comprovante Bancário")) {
			
			NeoPaper papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", seExecutor.getCode()));
			if  (papel != null) {
				for (NeoUser noUser : papel.getAllUsers())
				{
					usuario = noUser;
					//break;
				}
			} else {
				usuario = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code", seExecutor.getCode()));
			}
			
			Set<NeoUser> usuarios = usuario.getGroup().getUpperLevel().getResponsible().getAllUsers();
			
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();					
			}
			processEntity.findField("executorTarefa").setValue(usuario);
		}
	}
	
	private void atualizaHistoricoAtividade() {
		
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades"); 
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
		
		
		GregorianCalendar dataAcao = new GregorianCalendar();

		wRegAti.setValue("usuario", usuario.getFullName());
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("descricao", observacao); 
		wRegAti.setValue("anexo", anexo);

		processEntity.findField("r001V2RegistroAtividade").addValue(objRegAti);
	}
	
	private void informarColaboradorDeFerias() {
		
		if (PortalUtil.getCurrentUser() == null) {
			Set<NeoUser> usuarios = usuario.getGroup().getUpperLevel().getResponsible().getAllUsers();				
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();					
			}
			processEntity.findField("executorRHO").setValue(usuario);
			
		} else {
			
			NeoObject colaborador = (NeoObject) processEntity.getValue("primeiraSugestao");
			EntityWrapper wColaborador = new EntityWrapper(colaborador);
			String nomFun = wColaborador.findField("nomfun").getValue().toString();
			String tituloTarefa = wColaborador.findField("numemp").getValue().toString()+"/"+wColaborador.findField("numcad").getValue().toString() + " - " + wColaborador.findField("nomfun").getValue().toString() + " - " + wColaborador.findField("usu_nomreg").getValue().toString();
			processEntity.findField("tituloTarefa").setValue(tituloTarefa);
			
			NeoObject cobertura = (NeoObject) processEntity.getValue("colaboradorCoberturaFerias");
			EntityWrapper wCobertura = new EntityWrapper(cobertura);
			String nomFunCobertura = wCobertura.findField("nomfun").getValue().toString();			
			
			final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			final Calendar cal = (Calendar) processEntity.getValue("inicioFerias");	
			observacao = "Informado a data de início das férias para <b>"+nomFun+"</b> em: <b>" 
			              + df.format(cal.getTime()) + "</b> - Cobertura por: <b>" + nomFunCobertura + "</b>.<br> Anotações: "+processEntity.getValue("anotacao");
			
			if (processEntity.getValue("dataTreinamento1") != null) {
				final Calendar cal1 = (Calendar) processEntity.getValue("dataTreinamento1");	
				observacao += "<br> - Data do 1º Treinamento: " + df.format(cal1.getTime());
			}
			if (processEntity.getValue("dataTreinamento2") != null) {
				final Calendar cal2 = (Calendar) processEntity.getValue("dataTreinamento2");	
				observacao += "<br> - Data do 2º Treinamento: " + df.format(cal2.getTime());
			}
			if (processEntity.getValue("dataTreinamento3") != null) {
				final Calendar cal3 = (Calendar) processEntity.getValue("dataTreinamento3");	
				observacao += "<br> - Data do 3º Treinamento: " + df.format(cal3.getTime());
			}
			
			processEntity.findField("anotacao").setValue("");
			NeoPaper papelRHO = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "papelR001V2RHO"));
			processEntity.findField("executorRHO").setValue(papelRHO);
			
			processEntity.findField("encaminharRH").setValue(null);
		
		}
		
	}
	
	private void escalada() {
		
		observacao = "" + processEntity.getValue("justificativaProblemaFeriasFinanceiro");
		
	}
	
	private void informarInicioFerias() {
		
		GregorianCalendar dataLimiteProcessarFerias = new GregorianCalendar();
		GregorianCalendar dataInicioFerias = new GregorianCalendar();
		GregorianCalendar dataAvancarFolha = new GregorianCalendar();
		
		dataInicioFerias = (GregorianCalendar) processEntity.getValue("inicioFerias");
		
		System.out.println("Data Inicio das Ferias: "+dataInicioFerias.getTime());
		System.out.println("Data Avançar Folha: "+dataAvancarFolha.getTime());
		System.out.println("Data Limite Processar Ferias: "+dataLimiteProcessarFerias.getTime());
		System.out.println(" ");
		
		if ((boolean) processEntity.getValue("encaminharRH")) {
			observacao = "Quantidade de dias informado: " + processEntity.getValue("qtdDias") + "<br>Anotações: " + processEntity.getValue("anotacao").toString();		
			NeoPaper papelRH = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "papelR001V2ResponsavelRH"));
			processEntity.findField("executorRH").setValue(papelRH);
						
			if (diffInDays((Calendar) dataInicioFerias, Calendar.getInstance()) > 30) {
				
				dataAvancarFolha = (GregorianCalendar) dataInicioFerias.clone();
				
				processEntity.findField("dataAvancarAberturaFolha").setValue(dataAvancarFolha);
				processEntity.findField("superior30Dias").setValue(true);
				
				dataAvancarFolha.add(Calendar.DATE, -30);
				
				System.out.println("Data Inicio das Ferias: "+dataInicioFerias.getTime());
				System.out.println("Data Avançar Folha: "+dataAvancarFolha.getTime());
				System.out.println("Data Limite Processar Ferias: "+dataLimiteProcessarFerias.getTime());
				System.out.println(" ");
				
				final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				observacao += "<br>OBS: Este processo ficará aguardando Abertura da Folha até <b>"+ df.format(dataAvancarFolha.getTime())+"</b>. Após esta data será encaminhado automaticamente para o Processamento das Férias.";
			}
			
		} else {
			observacao = "Anotações: " + processEntity.getValue("anotacao").toString();
			NeoPaper papelRHO = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "papelR001V2RHO"));
			processEntity.findField("executorRHO").setValue(papelRHO);
		}			
		processEntity.findField("anotacao").setValue("");
		if ((boolean) processEntity.getValue("solicitouAjusteProcessarFerias") == true) {
			processEntity.findField("solicitarAjuste").setValue(false);
		} else {
			
			dataLimiteProcessarFerias = (GregorianCalendar) dataAvancarFolha.clone();
			int dia = 0;
			while (dia < 2) {	    	
			    if (OrsegupsUtils.isWorkDay(dataLimiteProcessarFerias)) {		
			    	dia++;
			    }
			    dataLimiteProcessarFerias.add(GregorianCalendar.DATE, 1);
			}
			while (!OrsegupsUtils.isWorkDay(dataLimiteProcessarFerias)) 
			{
				dataLimiteProcessarFerias = OrsegupsUtils.getNextWorkDay(dataLimiteProcessarFerias);
			}
			
			dataLimiteProcessarFerias.set(GregorianCalendar.HOUR_OF_DAY, 23);
			dataLimiteProcessarFerias.set(GregorianCalendar.MINUTE, 59);
			dataLimiteProcessarFerias.set(GregorianCalendar.SECOND, 59);
			
			processEntity.findField("dataLimiteProcessarFerias").setValue(dataLimiteProcessarFerias);
			processEntity.findField("dataLimiteProcessarFeriasAux").setValue(dataLimiteProcessarFerias);
		}
		
		System.out.println("Data Inicio das Ferias: "+dataInicioFerias.getTime());
		System.out.println("Data Avançar Folha: "+dataAvancarFolha.getTime());
		System.out.println("Data Limite Processar Ferias: "+dataLimiteProcessarFerias.getTime());
		System.out.println(" ");
	}
	
	private void processarFerias() {
		
		if (PortalUtil.getCurrentUser() == null) {
			Set<NeoUser> usuarios = usuario.getGroup().getUpperLevel().getResponsible().getAllUsers();				
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();					
			}
			processEntity.findField("executorRH").setValue(usuario);
			
		} else {
		
			if (processEntity.getValue("solicitarAjuste") != null && (boolean) processEntity.getValue("solicitarAjuste") == false) {
				
				boolean dataPagamentoVeioDoRubi = true;
			
				Calendar dataPagamento = null;
				dataPagamento = buscarDataPagamentoNoRubi(dataPagamento);
				Calendar dataInicioFerias = (Calendar) processEntity.findField("inicioFerias").getValue();
				
				if (dataPagamento == null) { 
					dataPagamento = (Calendar) dataInicioFerias.clone();
					int dia = 0;
					while (dia > -2) 
					{	    	
					    if (OrsegupsUtils.isWorkDay((GregorianCalendar) dataPagamento)) dia--;		    	
					    	
					    dataPagamento.add(GregorianCalendar.DATE, -1);
					}
					
					while (!OrsegupsUtils.isWorkDay((GregorianCalendar) dataPagamento)) { 
						dataPagamento = OrsegupsUtils.getPrevWorkDay((GregorianCalendar) dataPagamento);
					}
					dataPagamentoVeioDoRubi = false;
				}
				
				/*if (dataPagamento == null) {
					throw new WorkflowException("Férias não estão lançadas no RUBI. É necessário ter a data de pagamento.");
				}*/
				
//				if (dataPagamento.equals(dataInicioFerias) ||  dataPagamento.after(dataInicioFerias)) {
//					throw new WorkflowException("Data do pagamento dever ser menor ou igual à data início das férias.");
//				}
				
				processEntity.getField("dataPagamentoFerias").setValue(dataPagamento);
				final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");	
				observacao = "Processamento efetuado: data do pagamento das Férias: " + df.format(dataPagamento.getTime());
				if (!dataPagamentoVeioDoRubi)
					observacao += ", cálculo da data do pagamento efetuado no FUSION.";
				anexo = (NeoFile) processEntity.getValue("anexoReciboFerias");
				
				Calendar dataComunicacaoVigilante = null;
				dataComunicacaoVigilante = (Calendar) dataInicioFerias.clone();
				
				Calendar dataAtual = new GregorianCalendar();
				
				int cont10Dias = 0;
				while (dataComunicacaoVigilante.after(dataAtual) && cont10Dias < 12) {
					
					dataComunicacaoVigilante.add(GregorianCalendar.DATE, -1);
					cont10Dias++;
				}
				dataComunicacaoVigilante.add(GregorianCalendar.DATE, 2);
				processEntity.findField("dataComVig").setValue(dataComunicacaoVigilante);
							
			} else {
				
				if (processEntity.getValue("solicitarAjuste") == null) {
					observacao = "Ajuste Solicitado: Sem Observação";
				} else {
					observacao = "Ajuste Solicitado: " + processEntity.getValue("justificativaAjuste");
				}
				
				validaAjustesSolicitados(processEntity);
				
				NeoPaper papelRHO = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "papelR001V2RHO"));
				processEntity.findField("executorRHO").setValue(papelRHO);
				
			}
		}
		
		GregorianCalendar dataAuxiliar = (GregorianCalendar) processEntity.findGenericValue("dataLimiteProcessarFeriasAux");
		GregorianCalendar prazo = (GregorianCalendar) dataAuxiliar.clone();
		prazo = OrsegupsUtils.getSpecificWorkDay(dataAuxiliar, 2L);
		
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		processEntity.findField("dataLimiteProcessarFeriasAux").setValue(dataAuxiliar);
	}
	
	private void validaAjustesSolicitados(EntityWrapper processEntity) {
		
		Boolean resultado = (Boolean) processEntity.findGenericValue("solicitarAjuste");
		Boolean ajuste1 = (Boolean) processEntity.findGenericValue("solicitouAjuste1");
		Boolean ajuste2 = (Boolean) processEntity.findGenericValue("solicitouAjuste2");
		
		if (resultado) {
			
			if (!ajuste1) {
				processEntity.findField("solicitouAjuste1").setValue(true);
			} else if (!ajuste2) {
				processEntity.findField("solicitouAjuste2").setValue(true);
				processEntity.findField("solicitouAjusteProcessarFerias").setValue(true);
			}
			
		}
	}

	private void aguardandoDataDoPagamento() {
		
		if (usuario == null) {
			usuario = new NeoUser();
			usuario.setFullName("SISTEMA");
		}		
	}
	
	public void aguardandoAberturaFolha() {
				
		if (usuario == null) {
			usuario = new NeoUser();
			usuario.setFullName("SISTEMA");
		}
	}
	
	private void comunicarVigilanteCoberturaDeFerias() {
		
		if (PortalUtil.getCurrentUser() == null) {
			Set<NeoUser> usuarios = usuario.getGroup().getUpperLevel().getResponsible().getAllUsers();				
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();					
			}
			processEntity.findField("executorRHO").setValue(usuario);
			
		} else {
			observacao = "Comunicado vigilante cobertura: " + processEntity.getValue("obsComVig");
		}
	}
	
	private void confirmarPagamentoFeriasAnexarComprovanteBancario() {
		
		if (PortalUtil.getCurrentUser() == null) {
			Set<NeoUser> usuarios = usuario.getGroup().getUpperLevel().getResponsible().getAllUsers();				
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();					
			}
			processEntity.findField("executorRHComprovante").setValue(usuario);
			
		} else {
			if (processEntity.getValue("anexoComprovanteBancario") != null) {
				observacao = "Anexado Comprovante Bancário";
				anexo = (NeoFile) processEntity.getValue("anexoComprovanteBancario");
			} else {
				observacao = "Não anexado Comprovante Bancário - Pagamento em Espécie";
				anexo = null;
			}
			
			if (processEntity.findField("reciboEstaCorreto").getValue() != null && ((Boolean) processEntity.findField("reciboEstaCorreto").getValue() == false)) {
				
				NeoFile anexo2 = null;
				anexo2 = (NeoFile) processEntity.getValue("anexoReciboFerias");
				 
				NeoObject objRegAti = AdapterUtils.createNewEntityInstance("r001V2RegistroAtividades");
				EntityWrapper wRegAti = new EntityWrapper(objRegAti);
				
				
				GregorianCalendar dataAcao = new GregorianCalendar();

				wRegAti.setValue("usuario", usuario.getFullName());
				wRegAti.setValue("dataAcao", dataAcao);
				wRegAti.setValue("descricao", "Alterado o Recibo de Férias."); 
				wRegAti.setValue("anexo", anexo2);

				processEntity.findField("r001V2RegistroAtividade").addValue(objRegAti);
				
			}
			
		}
	}
	
	private void informarPagamenteDeFeriasEmEspecie() {
		
		if (PortalUtil.getCurrentUser() == null) {
			Set<NeoUser> usuarios = usuario.getGroup().getUpperLevel().getResponsible().getAllUsers();				
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();					
			}
			processEntity.findField("executorControladoria").setValue(usuario);
			
		} else {
			observacao = "Observação do Financeiro: " + processEntity.getValue("justificativaProblemaFeriasFinanceiro");
		}
	}
	
	private void justificarProblemaPagamentoDeFerias() {
		
		observacao = "Justificativa RHO: " + processEntity.getValue("justificativaProblemaFerias");
		
	}
	
	private void realizarPagamentoDeFeriasAnexarReciboAssinado() {
		
		if (PortalUtil.getCurrentUser() == null) {
			Set<NeoUser> usuarios = usuario.getGroup().getUpperLevel().getResponsible().getAllUsers();				
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();					
			}
			processEntity.findField("executorPagtoFerias").setValue(usuario);
			
		} else {
			if (processEntity.getValue("anexoReciboFeriasAssinado") != null) {
				anexo = (NeoFile) processEntity.getValue("anexoReciboFeriasAssinado");
			}
			observacao = "Observação para Financeiro: " + processEntity.getValue("obsExecutorPgtoFerias");
		}
	}
	
	private void validarReciboDeFerias() {
		
		if (PortalUtil.getCurrentUser() == null) {
			Set<NeoUser> usuarios = usuario.getGroup().getUpperLevel().getResponsible().getAllUsers();				
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();					
			}
			processEntity.findField("executorFinanceiro").setValue(usuario);
			
		} else {
			observacao = "Observação do Financeiro: " + processEntity.getValue("obsAprovacaoRecibo");
		}
	}
	
	private void tomarCienciaCancelamentoRh() {
		
		observacao = "Observação - Tomar ciência RH: " + processEntity.getValue("obsFeriasInativasRubi");
		processEntity.findField("obsFeriasInativasRubi").setValue("");
		
	}
	
	private void tomarCienciaCancelamentoFinanceiro() {
		
		observacao = "Observação - Tomar ciência Financeiro: " + processEntity.getValue("obsFeriasInativasRubi");
		processEntity.findField("obsFeriasInativasRubi").setValue("");
		
	}
	
	private Calendar buscarDataPagamentoNoRubi(Calendar datPagamento) {
		
		try {
			String inifer = null;
			Integer numemp = null;
			Integer numcad = null;
			
			Calendar cal = (Calendar) processEntity.findField("inicioFerias").getValue();
			
			final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			inifer = df.format(cal.getTime());
			
			NeoObject colaborador = (NeoObject) processEntity.findField("primeiraSugestao").getValue();
			EntityWrapper wColaborador = new EntityWrapper(colaborador);			
			
			numemp = Integer.parseInt(wColaborador.findField("numemp").getValue().toString());
			numcad = Integer.parseInt(wColaborador.findField("numcad").getValue().toString());
			
			//Colaborador
			QLEqualsFilter filterNumemp = new QLEqualsFilter("numemp", numemp);			
			QLEqualsFilter filterNumcad = new QLEqualsFilter("numcad", numcad);
			QLEqualsFilter filterIniFer = new QLEqualsFilter("inifer", cal.getTime());
			
			QLGroupFilter filtro = new QLGroupFilter("AND");
			filtro.addFilter(filterNumemp);			
			filtro.addFilter(filterNumcad);
			filtro.addFilter(filterIniFer);
			
			List<NeoObject> list = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHR040FEM"), filtro);
			System.out.println("VerificaFeriasAtivaRubi: filtro"+filtro);
			System.out.println("VerificaFeriasAtivaRubi: lista"+list.size());
			if (list.size() != 0) {				
				for (NeoObject ferias : list)
				{
					EntityWrapper wr = new EntityWrapper(ferias);
					datPagamento = (Calendar) wr.findField("datpag").getValue();
				}
			}
		} catch (Exception e) {
			throw new WorkflowException("Data do pagamento dever ser menor ou igual à data início das férias.");
		}
		return datPagamento;
	}
	
	public static int diffInDays(Calendar c1, Calendar c2) {
		int MILLIS_IN_DAY = 86400000;

		c1.set(Calendar.MILLISECOND, 0);
		c1.set(Calendar.SECOND, 0);
		c1.set(Calendar.MINUTE, 0);
		c1.set(Calendar.HOUR_OF_DAY, 0);

		c2.set(Calendar.MILLISECOND, 0);
		c2.set(Calendar.SECOND, 0);
		c2.set(Calendar.MINUTE, 0);
		c2.set(Calendar.HOUR_OF_DAY, 0);
		return (int) ((c1.getTimeInMillis() - c2.getTimeInMillis()) / MILLIS_IN_DAY);
	}
}