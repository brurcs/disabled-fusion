package com.neomind.fusion.custom.orsegups.contract;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;

public class ContratoGeraMinutaConverter extends StringConverter
{
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		String botao = "<input type=\"button\" class=\"input_button\" id=\"bt_import\" value=\"Gerar PDF\" onClick = \"javascript:gerarMinutaContrato();\"/>";
		
		return botao;
	}
}
