package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import java.util.List;

import com.neomind.fusion.eform.actions.EntityActionFactoryParams;
import com.neomind.fusion.eform.actions.EntityActionFactoryParamsHeadList;
import com.neomind.fusion.eform.actions.EntityActionInterface;
import com.neomind.fusion.entity.EntityAction;

public class FAPSlipHistoricoOrcamentoConverter implements EntityActionInterface
{
	@Override
	public List<EntityAction> getExtraActions()
	{
		return null;
	}

	@Override
	public boolean validate(EntityAction action, EntityActionFactoryParams params)
	{
		EntityActionFactoryParamsHeadList paramsFactory = (EntityActionFactoryParamsHeadList) params;
		if (paramsFactory.getId().equals("list_novoContrato__orcamento__"))
		{
			return false;
		}
		
		return true;
	}
}
