package com.neomind.fusion.custom.casvig.mobile;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.casvig.mobile.action.ActionLogin;
import com.neomind.fusion.custom.casvig.mobile.action.ActionReceiveInspection;
import com.neomind.fusion.custom.casvig.mobile.action.ActionSendReinspection;
import com.neomind.fusion.custom.casvig.mobile.action.ActionUpdateClient;
import com.neomind.fusion.custom.casvig.mobile.action.ActionUpdateResearch;

/**
 * Classe que controla a comunicação entre o Fusion e o Mobile da Casvig usado
 * nas inspetorias de qualidade
 * 
 * @author Daniel Henrique Joppi
 * 
 */
@WebServlet(name="CasvigMobileServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.casvig.mobile.CasvigMobileServlet"})
public class CasvigMobileServlet extends HttpServlet
{
	public static final Log log = LogFactory.getLog(CasvigMobileServlet.class);

	public static CasvigMobileServlet getInstance()
	{
		return new CasvigMobileServlet();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String function = request.getParameter("action");

		response.setContentType("text/xml");

		// ações que o servlet do mobile pode tomar

		// checar se login é válido
		if (function.equals("login"))
		{
			ActionLogin.getInstance().doGet(request, response);
		}
		// enviar/atualizar xml de clientes para o mobile
		else if (function.equals("updateClient"))
		{
			ActionUpdateClient.getInstance().doGet(request, response);
		}
		// enviar/atualizar xml de pesquisa para o mobile
		else if (function.equals("updateResearch"))
		{
			ActionUpdateResearch.getInstance().doGet(request, response);
		}
		// enviar reinspeções para mobile
		else if (function.equals("sendReinspection"))
		{
			ActionSendReinspection.getInstance().doGet(request, response);
		}
		// receber as inspeções do mobile
		else if (function.equals("receiveInspection"))
		{
			ActionReceiveInspection.getInstance().doGet(request, response);
		}
	}
}