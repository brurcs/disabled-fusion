package com.neomind.fusion.custom.orsegups.contract;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.vo.FgcPrincipalVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class AtualizaCentroCusto
{
	public NeoObject getEformPrincipal(String codTar)
	{
		NeoObject objPrincipal = null;
		QLGroupFilter groupFilter = new QLGroupFilter("AND");

		QLFilter codeQl = new QLEqualsFilter("wfprocess.code", codTar);
		groupFilter.addFilter(codeQl);
		objPrincipal = PersistEngine.getObject(AdapterUtils.getEntityClass("FGCPrincipal"), groupFilter);
		return objPrincipal;
	}

	public NeoObject getCentroDeCusto(String codCcuNv6, long codEmp)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");

		QLFilter codCcuQl = new QLEqualsFilter("codccu", codCcuNv6);
		QLFilter codEmpQl = new QLEqualsFilter("codemp", codEmp);
		groupFilter.addFilter(codCcuQl);
		groupFilter.addFilter(codEmpQl);
		NeoObject objCcu = PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), groupFilter);
		if (objCcu != null)
		{
			return objCcu;
		}
		else
		{
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public String atualizaCentroDeCusto(String codCcuNv6, String codTar)
	{
		StringBuilder sql = new StringBuilder();
		Connection connSapiens = null;
		ResultSet rsCentroCusto = null;
		PreparedStatement stCentroCusto = null;
		String claCcuNv4 = "";
		String claNiv5 = "";
		String codNiv5 = "";
		String claNiv6 = "";
		String codNiv6 = "";

		//Carregar centro de custo no e-form.
		List<NeoObject> centrocusto = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), new QLGroupFilter("AND", new QLEqualsFilter("codccu", codCcuNv6)));

		sql.append("select ");
		sql.append(" (select top 1(substring(clacta,1,9)) from e043pcm where codccu = ? and nivcta = 6) as claCcuNV4");
		try
		{
			connSapiens = PersistEngine.getConnection("SAPIENS");
			stCentroCusto = connSapiens.prepareStatement(sql.toString());
			stCentroCusto.setString(1, codCcuNv6);
			rsCentroCusto = stCentroCusto.executeQuery();

			while (rsCentroCusto.next())
			{
				claCcuNv4 = rsCentroCusto.getString("claCcuNV4");
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, stCentroCusto, rsCentroCusto);
		}

		if (claCcuNv4 != null && !claCcuNv4.equals(""))
		{

			sql = new StringBuilder();

			sql.append("SELECT	 ");
			sql.append("		(select top 1 clacta from e043pcm where clacta like '" + claCcuNv4 + "%' and nivcta = 5 order by clacta desc) as claNiv5, ");
			sql.append("		(select top 1 codccu from e043pcm where clacta like '" + claCcuNv4 + "%' and nivcta = 5 order by clacta desc) as codNiv5, ");
			sql.append("		(select top 1 clacta from e043pcm where clacta like '" + claCcuNv4 + "%' and nivcta = 6 order by clacta desc) as claNiv6, ");
			sql.append("		(select top 1 codccu from e043pcm where clacta like '" + claCcuNv4 + "%' and nivcta = 6 order by clacta desc) as codNiv6");
			try
			{
				rsCentroCusto = null;
				stCentroCusto = null;
				connSapiens = PersistEngine.getConnection("SAPIENS");
				stCentroCusto = connSapiens.prepareStatement(sql.toString());
				rsCentroCusto = stCentroCusto.executeQuery();

				while (rsCentroCusto.next())
				{
					claNiv5 = rsCentroCusto.getString("claNiv5");
					codNiv5 = rsCentroCusto.getString("codNiv5");
					claNiv6 = rsCentroCusto.getString("claNiv6");
					codNiv6 = rsCentroCusto.getString("codNiv6");
				}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
			finally
			{
				OrsegupsUtils.closeConnection(connSapiens, stCentroCusto, rsCentroCusto);
			}

			NeoObject objPrincipal = this.getEformPrincipal(codTar);
			if (objPrincipal != null)
			{
				EntityWrapper wObjPrincipal = new EntityWrapper(objPrincipal);
				wObjPrincipal.setValue("classificacaoCentroCusto4Nivel", claCcuNv4);
				wObjPrincipal.setValue("aCtaRed", codNiv5);
				wObjPrincipal.setValue("centroCustoContrato", codNiv5);
				wObjPrincipal.setValue("centroCusto6", codNiv6);
				wObjPrincipal.setValue("aClaCta6", claNiv6);
				wObjPrincipal.setValue("aClaCta", claNiv5);

				List<NeoObject> postos = (List<NeoObject>) wObjPrincipal.getValue("postosContrato");
				if (postos != null && postos.size() > 0)
				{
					EntityWrapper wObjPosto = new EntityWrapper(postos.get(0));

					NeoObject objEmp = (NeoObject) wObjPrincipal.getValue("empresa");
					EntityWrapper wEmp = new EntityWrapper(objEmp);
					Long codEmp = Long.parseLong(String.valueOf(wEmp.getValue("codemp")));
					NeoObject ccuNv6 = this.getCentroDeCusto(codCcuNv6, codEmp);

					if (ccuNv6 != null)
					{
						wObjPosto.setValue("centroCusto", ccuNv6);
						PersistEngine.persist(wObjPosto.getObject());
						PersistEngine.persist(wObjPrincipal.getObject());
						return "Registros atualizados com Sucess!";
					}
					else
					{
						return "Nao foi encontrado centro de custo no Fusion de nivel 6 com o codigo " + codCcuNv6 + "!";
					}
				}else{
					return "Nenhum posto encontrado!";
				}
			}
			else
			{
				return "Nao foi encontrado a tarefa " + codTar + " no fluxo de contratos!";
			}
		}
		else
		{
			return "Nao foi encontrado centro de custo no sapiens de nivel 6 com o codigo " + codCcuNv6 + "!";
		}
	}

	public FgcPrincipalVO visualizarEformPrincipal(String codTar)
	{
		FgcPrincipalVO fgcPrincipalVO = null;
		NeoObject objPrincipal = this.getEformPrincipal(codTar);

		if (objPrincipal != null)
		{
			EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);
			fgcPrincipalVO = new FgcPrincipalVO();
			fgcPrincipalVO.setaClaCta(String.valueOf(wPrincipal.getValue("aClaCta")));
			fgcPrincipalVO.setaClaCta6(String.valueOf(wPrincipal.getValue("aClaCta6")));
			fgcPrincipalVO.setaCtaRed(String.valueOf(wPrincipal.getValue("aCtaRed")));
			fgcPrincipalVO.setCentroCusto6(String.valueOf(wPrincipal.getValue("centroCusto6")));
			fgcPrincipalVO.setCentroCustoContrato(String.valueOf(wPrincipal.getValue("centroCustoContrato")));
		}
		return fgcPrincipalVO;
	}
}
