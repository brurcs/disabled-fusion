package com.neomind.fusion.custom.orsegups.rsc.vo;

public class ServicosVO
{

	private int codigoServico;
	private String descricaoServico;
	private String abreviatura;
	private String codigoFamilia;

	public String getAbreviatura()
	{
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura)
	{
		this.abreviatura = abreviatura;
	}

	public int getCodigoServico()
	{
		return codigoServico;
	}

	public void setCodigoServico(int codigoServico)
	{
		this.codigoServico = codigoServico;
	}

	public String getDescricaoServico()
	{
		return descricaoServico;
	}

	public void setDescricaoServico(String descricaoServico)
	{
		this.descricaoServico = descricaoServico;
	}

	public String getCodigoFamilia()
	{
		return codigoFamilia;
	}

	public void setCodigoFamilia(String codigoFamilia)
	{
		this.codigoFamilia = codigoFamilia;
	}

}
