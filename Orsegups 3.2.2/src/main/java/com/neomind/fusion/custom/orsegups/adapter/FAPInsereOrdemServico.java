package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dto.OrdemServicoDTO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPInsereOrdemServico implements AdapterInterface {
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		
		List<OrdemServicoDTO> listaOrdemServico = new ArrayList<OrdemServicoDTO>();
		
		Long codigoCliente = (Long) processEntity.findValue("contaSigma.cd_cliente");
		Long aplicacaoPagamento = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
		
		String codigoTarefa = activity.getCode();
		
		try {
		
			if(NeoUtils.safeIsNotNull(aplicacaoPagamento) && NeoUtils.safeIsNotNull(codigoCliente)) {
				if(aplicacaoPagamento == 3L) {
					
					listaOrdemServico = retornaOrdensDeServico(codigoCliente);
					
					if (NeoUtils.safeIsNotNull(listaOrdemServico)) {
						
						for (OrdemServicoDTO ordemServico : listaOrdemServico) {

							try {
								NeoObject noOrdemServico = AdapterUtils.createNewEntityInstance("FAPListaOrdemServico");
								EntityWrapper ewOrdemServico = new EntityWrapper(noOrdemServico);
								ewOrdemServico.setValue("numeroDaOs", ordemServico.getNumeroDaOs());
								ewOrdemServico.setValue("dataDeAbertura", ordemServico.getDataDeAbertura());
								ewOrdemServico.setValue("dataDeFechamento", ordemServico.getDataDeFechamento());

								PersistEngine.persist(noOrdemServico);
								
								processEntity.findField("listaOrdemDeServico").addValue(noOrdemServico);
							} catch (Exception e) {
								e.printStackTrace();
								System.out.println("### "+this.getClass().getSimpleName()+": ERRO ao Inserir a OS "+ ordemServico.getNumeroDaOs() +" na FAP "+ codigoTarefa);
							}
						} 
						
					}
					
				}
			}
				
		} catch (Exception e) {
			String mensagem = "###"+this.getClass().getSimpleName()+": ERRO ao verificar as ordens de serviço associadas ao cliente. ";
			e.printStackTrace();
			System.out.println(mensagem);
			throw new WorkflowException(mensagem);
		}
		
		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

	public List<OrdemServicoDTO> retornaOrdensDeServico(Long codigoCliente){
		
		List<OrdemServicoDTO> listaOrdensDeServico = new ArrayList<OrdemServicoDTO>();
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		try {
			
			sql.append(" SELECT OS.ID_ORDEM AS 'ORDEM', OS.ABERTURA AS 'DATA DE ABERTURA', OS.FECHAMENTO AS 'DATA DE FECHAMENTO' FROM DBORDEM OS ");
			sql.append(" INNER JOIN OSHISTORICO osh  ");
			sql.append(" WITH (NOLOCK) ON osh.ID_ORDEM = os.ID_ORDEM ");
			sql.append(" WHERE OS.CD_CLIENTE = ? ");
			sql.append(" AND os.FECHADO = 2 AND GETDATE() >= DATEADD(HOUR, +72, osh.DATAINICIOEXECUCAO) ");
			
			conn = PersistEngine.getConnection("SIGMA90");
			
			pstm = conn.prepareStatement(sql.toString());

			pstm.setLong(1, codigoCliente);

			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				OrdemServicoDTO ordemServico = new OrdemServicoDTO();
				
				GregorianCalendar dataAbertura = new GregorianCalendar();
				GregorianCalendar dataFechamento = new GregorianCalendar();
				
				dataAbertura = stringParaGregorianCalendar(rs.getString("DATA DE ABERTURA"));
				dataFechamento = stringParaGregorianCalendar(rs.getString("DATA DE FECHAMENTO"));
								
				ordemServico.setNumeroDaOs(rs.getLong("ORDEM"));
				ordemServico.setDataDeAbertura(dataAbertura);
				ordemServico.setDataDeFechamento(dataFechamento);
				
				listaOrdensDeServico.add(ordemServico);
			}
			
		} catch(Exception e) {
			String mensagem = "###"+this.getClass().getSimpleName()+": ERRO ao verificar as ordens de serviço associadas ao cliente. ";
			e.printStackTrace();
			System.out.println(mensagem);
			throw new WorkflowException(mensagem);
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaOrdensDeServico;
		
	}
	
	public GregorianCalendar stringParaGregorianCalendar(String data) throws ParseException {
		
		GregorianCalendar dataRetorno = new GregorianCalendar();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date dataAuxiliar = null;
		
		if(NeoUtils.safeIsNotNull(data)) {
			
			dataAuxiliar = dateFormat.parse(data);
			
			dataRetorno.setTime(dataAuxiliar);

		}else {
			return null;
		}
		
		return dataRetorno;
	}
}
