package com.neomind.fusion.custom.orsegups.servlets.seniorCommands.fluxoG001;

import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;

import com.neomind.fusion.custom.orsegups.servlets.seniorCommands.Command;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class StartG001 implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		String resultado = null;

		String executor = null;

		try {
			
			 String solicitante = request.getParameter("solicitante");
			 executor = getExecutor(request);
			 String titulo = request.getParameter("titulo");
			 String descricao = request.getParameter("descricao");
			 String origem = "2";
			 String avanca = "hadouken";
			 GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), Long.parseLong(request.getParameter("prazo")));
			 
			 IniciarTarefaSimples tarefa = new IniciarTarefaSimples();
			 
			 resultado = tarefa.abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo);

		} catch (WorkflowException e) {
			e.printStackTrace();
			resultado = "#ERRO!" + e.getErrorList().get(0).getI18nMessage();
		} catch (Exception e) {
			e.printStackTrace();
			resultado = "#ERRO!" + e.getMessage();
		}

		return resultado;
	}

	private String getExecutor(HttpServletRequest req){
		String executor = null;

		if(("devolucaoEquipamentos").equals(req.getParameter("motivoAbertura"))){
			NeoPaper paper = OrsegupsUtils.getPapelRetornaEquipamento(Long.parseLong(req.getParameter("codreg")));
			executor = OrsegupsUtils.getUserNeoPaper(paper);
		} else if(("geral").equals(req.getParameter("motivoAbertura"))){
			executor = req.getParameter("executor");
		}

		return executor;
	}
}
