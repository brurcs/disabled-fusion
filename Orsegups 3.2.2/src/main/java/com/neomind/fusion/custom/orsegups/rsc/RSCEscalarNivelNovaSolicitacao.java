package com.neomind.fusion.custom.orsegups.rsc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCEscalarNivelNovaSolicitacao implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCEscalarNivelCategoriaDiversos.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		String erro = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			if (origin != null)
			{
				OrsegupsUtils paper = new OrsegupsUtils();
				NeoPaper papelSuperior = new NeoPaper();
				NeoUser responsavelRegistrarRSC = origin.getUser();

				if (origin.getActivityName().equalsIgnoreCase("Registrar RSC") || origin.getActivityName().equalsIgnoreCase("Gestor de RSC"))
				{
					if (origin.getFinishByUser() == null)
					{
						if (responsavelRegistrarRSC.getPapers().contains(responsavelRegistrarRSC.getGroup().getResponsible()))
						{
							papelSuperior = responsavelRegistrarRSC.getGroup().getUpperLevel().getResponsible();
							wrapper.setValue("responsavelExecutor", papelSuperior);
						}
						else
						{
							papelSuperior = paper.getPaper(responsavelRegistrarRSC.getGroup().getResponsible().getName());
							wrapper.setValue("responsavelExecutor", papelSuperior);
						}
					}
				}
				else
				{
					NeoPaper responsavelExecutor = (NeoPaper) wrapper.findValue("responsavelExecutor");
					for (NeoUser user : responsavelExecutor.getAllUsers())
					{
						if (origin.getFinishByUser() == null)
						{
							if(user.getPapers().contains(user.getGroup().getResponsible()))
							{
								papelSuperior = user.getGroup().getUpperLevel().getResponsible();
								wrapper.setValue("responsavelExecutor", papelSuperior);
								//papelSuperior = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","Supervisor CEREC"));
							    //wrapper.setValue("responsavelExecutor", papelSuperior);
							}
							else
							{
								papelSuperior = paper.getPaper(user.getGroup().getResponsible().getName());
								wrapper.setValue("responsavelExecutor", papelSuperior);
							}							
						}
					}
				}
			}

			/*
			 * OrsegupsUtils paper = new OrsegupsUtils();
			 * NeoPaper papelSuperior = new NeoPaper();
			 * if(origin.getFinishByUser() == null)
			 * {
			 * papelSuperior = (NeoPaper)
			 * PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new
			 * QLEqualsFilter("code","Supervisor CEREC"));
			 * wrapper.setValue("responsavelExecutor", papelSuperior);
			 * System.out.print(origin.getCode().toString());
			 * }
			 * else
			 * {
			 * NeoUser responsavelExecutor = (NeoUser) wrapper.findValue("responsavelExecutor");
			 * if(responsavelExecutor.getPapers().contains(responsavelExecutor.getGroup().getResponsible()
			 * ))
			 * {
			 * papelSuperior = responsavelExecutor.getGroup().getUpperLevel().getResponsible();
			 * wrapper.setValue("responsavelExecutor", papelSuperior);
			 * }
			 * else
			 * {
			 * papelSuperior = paper.getPaper(responsavelExecutor.getGroup().getResponsible().getName());
			 * wrapper.setValue("responsavelExecutor", papelSuperior);
			 * }
			 * }
			 */

			/*
			 * if(origin.getCode().equals("000102"))
			 * {
			 * System.out.println(origin.getCode());
			 * }
			 * System.out.print(origin.getCode().toString());
			 * origin.getUser();
			 * NeoUser responsavelExecutor = (NeoUser) wrapper.findValue("responsavelExecutor");
			 * Long etapa = (Long) wrapper.findValue("etapaVerificacaoEficacia");
			 * if((etapa != 1L) && (etapa != 2L))
			 * {
			 * if(origin.getUser().getGroup().getUpperLevel().getResponsible() != null)
			 * {
			 * wrapper.setValue("superiorResponsavelExecutor",
			 * origin.getUser().getGroup().getUpperLevel().getResponsible());
			 * }
			 * else
			 * {
			 * erro = "Não foi possível definir o Responsável Superior do Executor (" + origin.getUser()
			 * + ") da tarefa: " + origin.getCode();
			 * }
			 * }
			 * else
			 * {
			 * if(responsavelExecutor.getPapers().contains(responsavelExecutor.getGroup().getResponsible()
			 * ))
			 * {
			 * papelSuperior = responsavelExecutor.getGroup().getUpperLevel().getResponsible();
			 * wrapper.setValue("superiorResponsavelExecutor", papelSuperior);
			 * }
			 * else
			 * {
			 * papelSuperior = paper.getPaper(responsavelExecutor.getGroup().getResponsible().getName());
			 * wrapper.setValue("superiorResponsavelExecutor", papelSuperior);
			 * }
			 * }
			 * NeoUser uSolic = (NeoUser) PersistEngine.getObject(SecurityEntity.class, new
			 * QLEqualsFilter("code", "fernanda.maciel"));
			 * wrapper.setValue("responsavelPrazoRSC",uSolic);
			 */
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
	}

	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}
