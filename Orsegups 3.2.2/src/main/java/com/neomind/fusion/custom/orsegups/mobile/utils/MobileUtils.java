package com.neomind.fusion.custom.orsegups.mobile.utils;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class MobileUtils
{

	public static NeoUser login(String code, String pass)
	{
		NeoUser user = null;

		if (code != null && !code.isEmpty() && pass != null && !pass.isEmpty())
		{
			QLEqualsFilter codeFilter = new QLEqualsFilter("code", code);
			QLEqualsFilter passFilter = new QLEqualsFilter("password", pass);
			QLEqualsFilter activeFilter = new QLEqualsFilter("active", true);

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(codeFilter);
			groupFilter.addFilter(passFilter);
			groupFilter.addFilter(activeFilter);

			user = (NeoUser) PersistEngine.getNeoObject(NeoUser.class, groupFilter);
		}

		return user;
	}

	public static String generateToken(NeoUser neoUser)
	{
		String token = null;

		if (neoUser != null)
		{
			token = createToken(neoUser);

			NeoObject autenticacaoMobile = getAutenticacaoMobileConf(neoUser);
			EntityWrapper wrapper = null;
			if (autenticacaoMobile == null)
			{
				autenticacaoMobile = AdapterUtils.createNewEntityInstance("autenticacaoMobile");
				wrapper = new EntityWrapper(autenticacaoMobile);
				wrapper.setValue("colaborador", neoUser);
				wrapper.setValue("dataCriacao", new GregorianCalendar());
				PersistEngine.persist(autenticacaoMobile);
			}
			else
			{
				wrapper = new EntityWrapper(autenticacaoMobile);
			}
			wrapper.setValue("token", token);
		}

		return token;
	}

	public static NeoUser getUserFromToken(String token)
	{
		NeoUser user = null;

		QLEqualsFilter tokenFilter = new QLEqualsFilter("token", token);
		
		/**
		 * @author orsegups lucas.avila - class generica.
		 * @date 08/07/2015
		 */
		Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("autenticacaoMobile");

		NeoObject autenticacaoMobile = PersistEngine.getNeoObject(clazz, tokenFilter);

		if (autenticacaoMobile != null)
		{
			EntityWrapper wrapper = new EntityWrapper(autenticacaoMobile);

			//user = wrapper.findGenericValue("colaborador");
			user = (NeoUser)wrapper.findValue("colaborador");
			if (user != null && !user.isActive()){
			    user = null;
			}
		}

		return user;
	}

	public static Boolean validateToken(String token)
	{
		Boolean ok = false;

		NeoUser neoUser = getUserFromToken(token);

		if (neoUser != null)
		{
			String newToken = createToken(neoUser);

			if (token.equalsIgnoreCase(newToken))
			{
				ok = true;
			}
		}

		return ok;
	}

	private static NeoObject getAutenticacaoMobileConf(NeoUser neoUser)
	{
		QLEqualsFilter tokenFilter = new QLEqualsFilter("colaborador", neoUser);

		/**
		 * @author orsegups lucas.avila - Classe generica.
		 * @date 08/07/2015
		 */
		Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("autenticacaoMobile");

		NeoObject autenticacaoMobile = PersistEngine.getNeoObject(clazz, tokenFilter);

		return autenticacaoMobile;
	}

	private static String createToken(NeoUser neoUser)
	{
		String token = null;

		if (neoUser != null)
		{
			token = neoUser.getCode() + neoUser.getPassword();
			token = NeoUtils.encodePassword(token);
		}

		return token;
	}
	
	
	/**
	 * Pela sigla da regional seta no fluxo o papel(pool) que a raia qualidade vai ter.<br/> 
	 * Isso foi feito para separa as reinspeções à enviar para o dispositivo por regional.<br/>
	 * Se nenhuma regional for encontrada, será retornardo o papel inspetoriaSEMREG
	 * @param siglaRegional
	 * @return
	 */
	public static NeoPaper getPapelBySiglaRegional(String siglaRegional){
		NeoPaper retorno = null;
		
		if ("SOOSEG".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaSOOSEG"));
		}
		else if ("IAI".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaIAI"));
		}
		else if ("BQE".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaBQE"));
		}
		else if ("BNU".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaBNU"));
		}
		else if ("JLE".equals(siglaRegional)){
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaJLE"));
		}
		else if ("LGS".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaLGS"));
		}
		else if ("CUA".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaCUA"));
		}
		else if ("GPR".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaGPR"));
		}
		else if ("SOOACL".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaSOOACL"));
		}
		else if ("CCO".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaCCO"));
		}
		else if ("RSL".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaRSL"));
		}
		else if ("JGS".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaJGS"));
		}
		else if ("CTAN".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaCTAN"));
		}
		else if ("CSC".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaCSC"));
		}
		else if ("CTAS".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaCTAS"));
		}
		else if ("TRO".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaTRO"));
		}
		else if ("SEMREG".equals(siglaRegional)){ 
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaSEMREG"));
		}else{
			retorno = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","inspetoriaSEMREG"));
		}
		
		return retorno;
		
	}
	
}
