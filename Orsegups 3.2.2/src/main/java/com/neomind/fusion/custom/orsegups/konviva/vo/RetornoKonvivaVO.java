package com.neomind.fusion.custom.orsegups.konviva.vo;

public class RetornoKonvivaVO {
    private String message;
    
    public String getMessage() {
	return message;
    }
    public void setMessage(String message) {
	this.message = message;
    }
}
