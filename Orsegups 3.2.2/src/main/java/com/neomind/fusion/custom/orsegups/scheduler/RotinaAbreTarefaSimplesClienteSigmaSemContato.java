package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesClienteSigmaSemContato implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesClienteSigmaSemContato.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		Connection conn = null;
		StringBuilder sqlBuilder = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cliente Sigma Sem Contato - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

		try
		{

			conn = PersistEngine.getConnection("SIGMA90");
			sqlBuilder = new StringBuilder();
			sqlBuilder.append(" SELECT SUBSTRING(r.NM_ROTA, 1, 3) AS sigla_regional, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, ");
			sqlBuilder.append(" CASE WHEN c.TP_PESSOA = 2 THEN 'Público' ELSE 'Privado' END AS TIPOPESSOA  ");
			sqlBuilder.append(" FROM  DBCENTRAL c WITH (NOLOCK)  ");
			sqlBuilder.append(" INNER JOIN ROTA r WITH (NOLOCK) ON  r.CD_ROTA = c.ID_ROTA  ");
			sqlBuilder.append(" WHERE C.CTRL_CENTRAL = 1   ");
			sqlBuilder.append(" AND C.FONE1 is  null and C.FONE2 is  null    ");
			sqlBuilder.append(" AND NOT EXISTS(SELECT PROV.FONE1, PROV.FONE2 FROM dbPROVIDENCIA AS PROV WITH (NOLOCK)    ");
			sqlBuilder.append(" WHERE PROV.CD_CLIENTE =  c.CD_CLIENTE AND (PROV.FONE1 is not null or PROV.FONE2 is not null))   ");
			sqlBuilder.append(" AND (ID_CENTRAL NOT LIKE '%AAA%' AND ID_CENTRAL NOT LIKE 'F%' )   ");
			sqlBuilder.append(" AND NOT EXISTS(  ");
			sqlBuilder.append(" SELECT code    ");
			sqlBuilder.append(" FROM [CACUPE\\SQL02].fusion_producao.DBO.WFProcess WITH (NOLOCK)     ");
			sqlBuilder.append(" WHERE saved = 1 and     ");
			sqlBuilder.append(" processState = 0 and     ");
			sqlBuilder.append(" model_neoId in (SELECT pmaux.neoId FROM [CACUPE\\SQL02].fusion_producao.DBO.ProcessModel pmaux WITH (NOLOCK) WHERE pmaux.name like '%G001 - Tarefa Simples%') and    ");
			sqlBuilder.append(" entity_neoid in (SELECT neoid     ");
			sqlBuilder.append(" FROM [CACUPE\\SQL02].fusion_producao.DBO.D_tarefa WITH (NOLOCK)     ");
			sqlBuilder.append(" WHERE titulo like '%Favor atualizar o contato telefônico do cliente no sistema sigma: '+c.ID_CENTRAL+'%'+c.PARTICAO+'%' COLLATE Latin1_General_CI_AS ))   ");

			pstm = conn.prepareStatement(sqlBuilder.toString());
			rs = pstm.executeQuery();
			String executor = "";

			while (rs.next())
			{
				String solicitante = "edson.heinz";
				String titulo = "Favor atualizar o contato telefônico do cliente no sistema sigma: " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] ";

				GregorianCalendar prazo = new GregorianCalendar();
				//prazo = NeoCalendarUtils.stringToDate("31/03/2016");
				
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);
				
				NeoPaper papel = new NeoPaper();

				String tipoPessoa = rs.getString("TIPOPESSOA");

				if (tipoPessoa != null && !tipoPessoa.isEmpty() && tipoPessoa.contains("Privado"))
					papel = OrsegupsUtils.getPaper("ResponsavelTarefaClienteSigmaSemContato");
				else if (tipoPessoa != null && !tipoPessoa.isEmpty() && tipoPessoa.contains("Público"))
					papel = OrsegupsUtils.getPaper("ResponsavelTarefaClienteSigmaSemContatoPublico");

				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{

					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						break;
					}
					
					String descricao = "";
					descricao = " <strong>Conta :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
					descricao += " <strong>Razão Social :</strong> " + rs.getString("RAZAO") + " <br>";
					descricao += " <strong>Nome Fantasia :</strong> " + rs.getString("FANTASIA") + " <br>";

					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

					log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cliente Sigma Sem Contato - Responsavel : " + executor + "  Tarefa: " + tarefa);
					System.out.println("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cliente Sigma Sem Contato - Responsavel : " + executor + "  Tarefa: " + tarefa);
				}
				else
				{
					log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cliente Sigma Sem Contato - ERRO AO GERAR TAREFA - Regra: responsável não encontrado - Papel " + papel.getName());
					continue;
				}

			}

		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cliente Sigma Sem Contato - ERRO AO GERAR TAREFA - Regional: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cliente Sigma Sem Contato - ERRO AO GERAR TAREFA - Regional: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

			log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Cliente Sigma Sem Contato e Ordens de Serviço em Excesso Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
