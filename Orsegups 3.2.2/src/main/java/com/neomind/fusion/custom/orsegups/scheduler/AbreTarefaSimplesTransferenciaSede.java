package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.SchedulerUtils;
import com.neomind.fusion.custom.orsegups.utils.TransferenciaSedeAdministracaoVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesTransferenciaSede implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesTransferenciaSede.class);

	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		Connection conn = null;
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Transferência Sede - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		SchedulerUtils schedulerUtils = new SchedulerUtils();
		
		try
		{
			conn = PersistEngine.getConnection("VETORH");
			List<TransferenciaSedeAdministracaoVO> transferenciaSedeAdministracaoVOs = new ArrayList<TransferenciaSedeAdministracaoVO>();

			sql.append(" SELECT fun.numemp, fun.tipcol, fun.numcad, fun.numcpf, fun.nomfun, car.titcar FROM R034FUN FUN                                                                                            ");
			sql.append(" INNER JOIN R038HLO HLO ON HLO.NUMEMP = FUN.NUMEMP AND                                                                                                                                     ");
			sql.append(" 				          HLO.TIPCOL = FUN.TIPCOL AND                                                                                                                                      ");
			sql.append(" 				          HLO.NUMCAD = FUN.NUMCAD AND                                                                                                                                      ");
			sql.append(" 				          HLO.DATALT = (SELECT MAX(DATALT) FROM R038HLO HLO1 WHERE HLO1.NUMEMP = HLO.NUMEMP AND HLO1.TIPCOL = HLO.TIPCOL AND HLO1.NUMCAD = HLO.NUMCAD)                     ");
			sql.append(" LEFT JOIN R038AFA AFA ON AFA.NUMEMP = FUN.NUMEMP AND                                                                                                                                      ");
			sql.append(" 						  AFA.TIPCOL = FUN.TIPCOL AND                                                                                                                                      ");
			sql.append(" 						  AFA.NUMCAD = FUN.NUMCAD AND                                                                                                                                      ");
			sql.append(" 						  AFA.DATAFA = (SELECT MAX(DATAFA) FROM R038AFA AFA1 WHERE AFA1.NUMEMP = AFA.NUMEMP AND AFA1.TIPCOL = AFA.TIPCOL AND AFA1.NUMCAD = AFA.NUMCAD)                     ");
			sql.append(" INNER JOIN R038HCA HCA ON HCA.NUMEMP = FUN.NUMEMP AND                                                                                                                                     ");
			sql.append(" 				          HCA.TIPCOL = FUN.TIPCOL AND                                                                                                                                      ");
			sql.append(" 				          HCA.NUMCAD = FUN.NUMCAD AND                                                                                                                                      ");
			sql.append(" 				          HCA.DATALT = (SELECT MAX(DATALT) FROM R038HCA HCA1 WHERE HCA1.NUMEMP = HCA.NUMEMP AND HCA1.TIPCOL = HCA.TIPCOL AND HCA1.NUMCAD = HCA.NUMCAD AND HCA1.ESTCAR = 1) ");
			sql.append(" INNER JOIN R024CAR CAR ON HCA.EstCar = CAR.ESTCAR AND HCA.CODCAR = CAR.CODCAR                                                                                                             ");
			sql.append(" WHERE FUN.TIPCOL = 1                                                                                                                                                                      ");
			sql.append("   AND HLO.TABORG = 203	                                                                                                                                                                   ");
			sql.append("   AND HLO.NUMLOC = 27124                                                                                                                                                                  ");
			sql.append("   AND (AFA.SITAFA <> 7 OR AFA.SITAFA IS NULL)                                                                                                                                             ");
			sql.append("   AND FUN.NUMEMP <> 4         				                                                                                                                                               ");
			sql.append("   AND FUN.DATADM = CAST(CAST(GETDATE() AS DATE) AS DATETIME) 																															   ");
			sql.append(" ORDER BY FUN.NUMEMP																																									   ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			TransferenciaSedeAdministracaoVO transferenciaSedeAdministracaoVO = null;
			while (rs.next())
			{
				transferenciaSedeAdministracaoVO = new TransferenciaSedeAdministracaoVO();
				Long numemp = rs.getLong("numemp");
				Long tipcol = rs.getLong("tipcol");
				Long numcad = rs.getLong("numcad");
				Long numcpf = rs.getLong("numcpf");

				String nomfun = rs.getString("nomfun");
				String titcar = rs.getString("titcar");

				transferenciaSedeAdministracaoVO.setNumemp(numemp);
				transferenciaSedeAdministracaoVO.setTipcol(tipcol);
				transferenciaSedeAdministracaoVO.setNumcad(numcad);
				transferenciaSedeAdministracaoVO.setNumcpf(numcpf);
				transferenciaSedeAdministracaoVO.setNomfun(nomfun);
				transferenciaSedeAdministracaoVO.setTitcar(titcar);

				transferenciaSedeAdministracaoVOs.add(transferenciaSedeAdministracaoVO);
			}
			OrsegupsUtils.closeConnection(conn, pstm, rs);

			for (TransferenciaSedeAdministracaoVO transferenciaSedeAdministracaoVO2 : transferenciaSedeAdministracaoVOs)
			{
				QLGroupFilter filtroColaborador = new QLGroupFilter("AND");
				filtroColaborador.addFilter(new QLEqualsFilter("numEmp", transferenciaSedeAdministracaoVO2.getNumemp()));
				filtroColaborador.addFilter(new QLEqualsFilter("tipCol", transferenciaSedeAdministracaoVO2.getTipcol()));
				filtroColaborador.addFilter(new QLEqualsFilter("numCad", transferenciaSedeAdministracaoVO2.getNumcad()));
				List<NeoObject> transferenciaColaborador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaTransferenciaColaborador"), filtroColaborador, -1, -1, "dataGeracao DESC");

				if (transferenciaColaborador != null && transferenciaColaborador.size() > 0)
				{
					EntityWrapper transferenciaColaboradorWrapper = new EntityWrapper(transferenciaColaborador.get(0));

					//situacao = 0 - Ativo ou 1 - Inativo
					int situacao = retornaStatusTarefaSimplesTransferenciaSede(transferenciaColaboradorWrapper.findValue("tarefa").toString());
					if (situacao != 0L)
					{
						abreTarefaSimplesTransferenciaSede(transferenciaSedeAdministracaoVO2.getNumemp(), transferenciaSedeAdministracaoVO2.getTipcol(), transferenciaSedeAdministracaoVO2.getNumcad(), transferenciaSedeAdministracaoVO2.getNomfun(), transferenciaSedeAdministracaoVO2.getTitcar(), transferenciaSedeAdministracaoVO2.getNumcpf());
					}
				}
				else
				{
					abreTarefaSimplesTransferenciaSede(transferenciaSedeAdministracaoVO2.getNumemp(), transferenciaSedeAdministracaoVO2.getTipcol(), transferenciaSedeAdministracaoVO2.getNumcad(), transferenciaSedeAdministracaoVO2.getNomfun(), transferenciaSedeAdministracaoVO2.getTitcar(), transferenciaSedeAdministracaoVO2.getNumcpf());
				}
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Rotina Abrir Tarefa Simples - Lotacao Transferencia Sede");
			System.out.println("["+key+"] ##### ERRO AGENDADOR DE TAREFA: Rotina Abrir Tarefa Simples - Lotacao Transferencia Sede");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Transferência Sede - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	@SuppressWarnings("finally")
	public int retornaStatusTarefaSimplesTransferenciaSede(String code)
	{
		//0 - Ativo ou 1 - Inativo  
		int situacao = 1;
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try
		{
			conn = PersistEngine.getConnection("");

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT WF.PROCESSSTATE                                                       ");
			sql.append(" FROM WFPROCESS AS WF WITH (NOLOCK)                                           ");
			sql.append(" INNER JOIN D_TAREFA TA WITH (NOLOCK) ON TA.WFPROCESS_NEOID = WF.NEOID        ");
			sql.append(" INNER JOIN PROCESSMODEL AS PM WITH (NOLOCK)  ON PM.NEOID = WF.MODEL_NEOID    ");
			sql.append(" WHERE PM.NEOID = 18888 AND WF.SAVED = 1 AND WF.CODE = '" + code + "'         ");

			//BUSCA-SE REGISTROS NA BASE
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String status = rs.getString("PROCESSSTATE");
				if (status != null && status.equals("0"))
				{
					situacao = 0;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}
			return situacao;
		}
	}

	public static void abreTarefaSimplesTransferenciaSede(Long numemp, Long tipcol, Long numcad, String nomfun, String titcar, Long numcpf)
	{
		String solicitante = "dilmoberger"; 
		String executor = OrsegupsUtils.getUserNeoPaper("G001ExecutorTransferenciaColaboradorSede");
		String titulo = "Lotar Colaborador " + numemp + "/" + numcad + " - " + nomfun; 
		
		

		String descricao = "";
		descricao = " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomfun + "<br>";
		descricao = descricao + " <strong>Cargo:</strong> " + titcar + "<br>";

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

		InstantiableEntityInfo tarefaTransferenciaColaborador = AdapterUtils.getInstantiableEntityInfo("TarefaTransferenciaColaborador");
		NeoObject no = tarefaTransferenciaColaborador.createNewInstance();
		EntityWrapper wrapper = new EntityWrapper(no);

		GregorianCalendar dataGeracao = new GregorianCalendar();

		wrapper.findField("numEmp").setValue(numemp);
		wrapper.findField("tipCol").setValue(tipcol);
		wrapper.findField("numCad").setValue(numcad);
		wrapper.findField("numCpf").setValue(numcpf);
		wrapper.findField("dataGeracao").setValue(dataGeracao);
		wrapper.findField("tarefa").setValue(tarefa);
		PersistEngine.persist(no);

		log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Transferência Sede - Responsavel " + executor + " Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
}
