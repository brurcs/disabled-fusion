package com.neomind.fusion.custom.orsegups.e2doc.engine;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.custom.orsegups.e2doc.vo.E2docVO;
import com.neomind.fusion.custom.orsegups.e2doc.xml.frota.Pastas;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoDateUtils;

public class E2docFrotaEngine {
    
    private static final Log log = LogFactory.getLog(E2docFrotaEngine.class);
    
    public synchronized Pastas pesquisaFrota(HashMap<String, String> atributosPesquisa) {

	String pesquisa = "<modelo>frota</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	E2docEngine.verificaValidadeKey("RH");

	String retorno = E2docEngine.pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

	retorno = retorno.replaceAll("&", " ");

	Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosFrota(retorno);
	}
	
	E2docEngine.removeDocumentosTemporarios();
	
	return pastas;
    }
    
    private Pastas tratarRetornoDocumentosFrota(String retorno) {

	Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, Pastas.class);

	    pastas = (Pastas) je.getValue();

	} catch (JAXBException e) {
	    e.printStackTrace();
	}

	return pastas;

    }
    
    
    public List<String> getTiposDocumentos(){
	
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append("SELECT DISTINCT s_indice82 FROM PASTA WHERE LEN(s_indice82)>0");
	
	List<String> retorno = new ArrayList<String>();
	
	try {
	    conn = PersistEngine.getConnection("E2DOC");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {
		retorno.add(rs.getString(1));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}
	
	
	return retorno;
    }
    
    public HashMap<String, Object> retornaLinkImagemDocumento2(String id) {
	String link = E2docUtils.trataRetorno(pesquisaDocumentoWebServiceDocGET2(id), "link");
	String[] linkBroke = link.split("[.]");
	String ext = linkBroke[linkBroke.length-1];
	URL url = null;
	File file = null;
	try {
	    url = new URL("http://" + link);

	    System.out.println("URL is: " + url.toString());

	    file = new File("doc");

	    FileUtils.copyURLToFile(url, file);

	} catch (MalformedURLException e) {

	    e.printStackTrace();
	} catch (IOException e) {

	    e.printStackTrace();
	}
	HashMap<String, Object> retorno = new HashMap<String, Object>();
	retorno.put("ext", ext);
	retorno.put("file", file);
	return retorno;
    }

    
    private String pesquisaDocumentoWebServiceDocGET2(String id) {
	String retorno = null;
	try {
	    E2docVO e2docVO = (E2docVO) E2docUtils.getConfigE2DOC();
	    if (e2docVO != null && id != null && !id.isEmpty()) {
		String urlWebService = e2docVO.getIp() + "/e2web.service2/servicos.asmx/LinkImgSeguranca?key=" + e2docVO.getKey() + "&id=" + URLEncoder.encode(id, "UTF-8") + "&seguranca=false";

		System.out.println("URL ARQUIVO: " + urlWebService);

		retorno = E2docUtils.requestGET(urlWebService);

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO pesquisarUsuarioWebServiceDocPost: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
	return retorno;
    }

}
