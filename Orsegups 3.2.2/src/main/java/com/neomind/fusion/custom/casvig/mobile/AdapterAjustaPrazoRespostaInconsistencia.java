package com.neomind.fusion.custom.casvig.mobile;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class AdapterAjustaPrazoRespostaInconsistencia implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		try{
			GregorianCalendar gcPrazo = new GregorianCalendar();
					
			gcPrazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			gcPrazo.set(GregorianCalendar.MINUTE, 59);
			gcPrazo.set(GregorianCalendar.SECOND, 59);
			gcPrazo.add(GregorianCalendar.DATE, 2);
			gcPrazo = OrsegupsUtils.getNextWorkDay(gcPrazo);
			System.out.print("Prazo Aprovar resposta Inconsistencia: " + NeoUtils.safeDateFormat(gcPrazo));
			processEntity.setValue("prazoAprovacaoRespostaInconsistencia", gcPrazo);
			System.out.println("...OK");
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Erro ao setar deadline");
		}
				
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}

