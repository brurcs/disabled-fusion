package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.List;

/**
 * Representa uma escala de turma com os horarios. Classe para agilizar o processamento do presença
 * @author danilo.silva
 *
 */
public class HorariosEscalaVO
{
	//CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, 0 AS Dia
	private Long codEsc;
	private String nomEsc;
	private Long codTma;
	private List<HorariosEscalaHorarioVO> horarios;
	public Long getCodEsc()
	{
		return codEsc;
	}
	public void setCodEsc(Long codEsc)
	{
		this.codEsc = codEsc;
	}
	public String getNomEsc()
	{
		return nomEsc;
	}
	public void setNomEsc(String nomEsc)
	{
		this.nomEsc = nomEsc;
	}
	public Long getCodTma()
	{
		return codTma;
	}
	public void setCodTma(Long codTma)
	{
		this.codTma = codTma;
	}
	public List<HorariosEscalaHorarioVO> getHorarios()
	{
		return horarios;
	}
	public void setHorarios(List<HorariosEscalaHorarioVO> horarios)
	{
		this.horarios = horarios;
	}
	
	@Override
	public String toString()
	{
		return "HorariosEscalaVO [codEsc=" + codEsc + ", nomEsc=" + nomEsc + ", codTma=" + codTma + ", horarios=" + horarios + "]";
	}
	
	
	
	
	
	
	

}
