package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import java.math.BigDecimal;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.FormulaConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;

public class FAPSlipTotalOrcamento extends FormulaConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		NeoObject noOrcamento = field.getForm().getObject();
		BigDecimal saldo = new BigDecimal(0l);
		
		List<NeoObject> listOrcamento = new EntityWrapper(noOrcamento).findGenericValue("orcamento");
		for (NeoObject noObjecto : listOrcamento)
		{
			EntityWrapper wOrcamento = new EntityWrapper(noObjecto);
			
			BigDecimal valor = wOrcamento.findGenericValue("valor");
			
			if (valor != null)
				saldo = saldo.add(valor);
		}
		
		//saldo = (BigDecimal) field.getValue();
		
		return FAPSlipUtils.parseMonetario(saldo);

	}
	
	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
