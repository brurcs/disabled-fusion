package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class ImportaChp1paraEForm2 implements CustomJobAdapter{

    @Override
    public void execute(CustomJobContext ctx) {

	Connection conn = PersistEngine.getConnection(""); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
	
	try {
	    StringBuffer  varname1 = new StringBuffer();
	    varname1.append("select iccId, ");
	    varname1.append("       linha, ");
	    varname1.append("       ativo, ");
	    varname1.append("       valor, ");
	    varname1.append("       empresa, ");
	    varname1.append("       operadora, ");
	    varname1.append("       operadora2 ");
	    varname1.append("  from [FSOODB04\\SQL02].TIDB.dbo.Chp1 ");
	    varname1.append(" where dataImportacao = '2018-04-19' ");
	    
	    pstm = conn.prepareStatement(varname1.toString());
	    rs = pstm.executeQuery();

	    while (rs.next())
	    {
		String _iccId = rs.getString("iccId");
		String _linha = rs.getString("linha");
		String _ativo = rs.getString("ativo");
		//String _valor = rs.getString("valor");
		String _empresa = rs.getString("empresa");
		String _operadora = rs.getString("operadora");
		String _operadora2 = rs.getString("operadora2");
		
		
		NeoObject wkfOCP = AdapterUtils.createNewEntityInstance("Chp1");
		final EntityWrapper ewWkfOCP= new EntityWrapper(wkfOCP);
		ewWkfOCP.findField("iccId").setValue(_iccId); // TODO trocar no producao
		ewWkfOCP.findField("ativo").setValue((Integer.parseInt(_ativo) == 1) ? Boolean.TRUE : Boolean.FALSE);
		ewWkfOCP.findField("linha").setValue(_linha);
		ewWkfOCP.findField("valor").setValue(0f);
		
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("id", Integer.parseInt(_empresa)));
		NeoObject _fEmpresa = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Empresa"), groupFilter);		
		ewWkfOCP.findField("fEmpresa").setValue(_fEmpresa);
		
		QLGroupFilter gFO1 = new QLGroupFilter("AND");
		gFO1.addFilter(new QLEqualsFilter("id", Integer.parseInt(_operadora)));
		NeoObject _fOperadora = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Operadora"), gFO1);		
		ewWkfOCP.findField("fOperadora").setValue(_fOperadora);
		
		if (_operadora2 != null && !_operadora2.equals("null")) {
		    QLGroupFilter gFO2 = new QLGroupFilter("AND");
		    gFO2.addFilter(new QLEqualsFilter("id", Integer.parseInt(_operadora2)));
		    NeoObject _fOperadora2 = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Operadora"), gFO2);		
		    ewWkfOCP.findField("fOperadora2").setValue(_fOperadora2); 
		}
		
		
		PersistEngine.persist(wkfOCP);
				
	    }
	    
	} catch(Exception e) {
	    System.out.println("##### ImportaChp1paraEForm2: Erro");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	
    }

}
