package com.neomind.fusion.custom.orsegups.emailmonitor.vo;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.WFProcess;

public class ResultVO extends NeoObject {

	public Boolean result;
	
	public String processCode;
	
	public String message;
	
	public Integer processNeoId;
	
	public SecurityEntity destinatario;
	
	private WFProcess wfProcess;

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getProcessNeoId() {
		return processNeoId;
	}

	public void setProcessNeoId(Integer processNeoId) {
		this.processNeoId = processNeoId;
	}

	public SecurityEntity getDestinatario()
	{
		return destinatario;
	}

	public void setDestinatario(SecurityEntity destinatario)
	{
		this.destinatario = destinatario;
	}
	
	public void appendMessage(String message)
	{
		if(this.message == null)
		{
			this.message = "";
		}
		
		this.message += message + "\n";
	}

	public WFProcess getWfProcess()
	{
		return wfProcess;
	}

	public void setWfProcess(WFProcess wfProcess)
	{
		this.wfProcess = wfProcess;
	}
}
