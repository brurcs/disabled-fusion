package com.neomind.fusion.custom.orsegups.presenca.vo;

public class FinancasTituloVO {
	private String numtit;
	private String sitTit;
	private String vlrBco;
	private String vlrPag;
	private String vlrOri;
	private String vlrAbe;
	private String datEmi;
	private String vctPro;
	private String datPag;
	
	
	public String getNumtit() {
		return numtit;
	}
	public void setNumtit(String numtit) {
		this.numtit = numtit;
	}
	public String getSitTit() {
		return sitTit;
	}
	public void setSitTit(String sitTit) {
		this.sitTit = sitTit;
	}
	public String getVlrBco() {
		return vlrBco;
	}
	public void setVlrBco(String vlrBco) {
		this.vlrBco = vlrBco;
	}
	public String getVlrPag() {
		return vlrPag;
	}
	public void setVlrPag(String vlrPag) {
		this.vlrPag = vlrPag;
	}
	public String getVlrOri() {
		return vlrOri;
	}
	public void setVlrOri(String vlrOri) {
		this.vlrOri = vlrOri;
	}
	public String getVlrAbe() {
		return vlrAbe;
	}
	public void setVlrAbe(String vlrAbe) {
		this.vlrAbe = vlrAbe;
	}
	public String getDatEmi() {
		return datEmi;
	}
	public void setDatEmi(String datEmi) {
		this.datEmi = datEmi;
	}
	public String getVctPro() {
		return vctPro;
	}
	public void setVctPro(String vctPro) {
		this.vctPro = vctPro;
	}
	public String getDatPag() {
		return datPag;
	}
	public void setDatPag(String datPag) {
		this.datPag = datPag;
	}
	
	@Override
	public String toString() {
		return "FinancasTituloVO [numtit=" + numtit + ", sitTit=" + sitTit
				+ ", vlrBco=" + vlrBco + ", vlrPag=" + vlrPag + ", vlrOri="
				+ vlrOri + ", vlrAbe=" + vlrAbe + ", datEmi=" + datEmi
				+ ", vctPro=" + vctPro + ", datPag=" + datPag + "]";
	}
	
	
	
	
}
