package com.neomind.fusion.custom.orsegups.presenca.horistas.beans;

public class Horista {

    private String nome;
    private int matricula;
    private int empresa;
    private String escala;
    private double valorHoras;
    private String textoHoras;
    private int tipoColaborador;
    private int centroCusto;
    
    

    public int getTipoColaborador() {
        return tipoColaborador;
    }

    public void setTipoColaborador(int tipoColaborador) {
        this.tipoColaborador = tipoColaborador;
    }

    public int getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(int centroCusto) {
        this.centroCusto = centroCusto;
    }

    public String getNome() {
	return nome;
    }

    public void setNome(String nome) {
	this.nome = nome;
    }

    public int getMatricula() {
	return matricula;
    }

    public void setMatricula(int matricula) {
	this.matricula = matricula;
    }

    public int getEmpresa() {
	return empresa;
    }

    public void setEmpresa(int empresa) {
	this.empresa = empresa;
    }

    public String getEscala() {
	return escala;
    }

    public void setEscala(String escala) {
	this.escala = escala;
    }

    public double getValorHoras() {
	return valorHoras;
    }

    public void setValorHoras(double valorHoras) {
	this.valorHoras = valorHoras;
    }

    public String getTextoHoras() {
	return textoHoras;
    }

    public void setTextoHoras(String textoHoras) {
	this.textoHoras = textoHoras;
    }


    
}
