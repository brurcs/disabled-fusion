package com.neomind.fusion.custom.orsegups.eform.vo;

import java.util.Date;
import java.util.GregorianCalendar;

public class Chp1ClienteVO extends EformVO {
    
    public Chp1ClienteVO() {
	super();
    }

    private Integer neoId;
    private ClienteVO cliente;
    private Chp1VO chp1;
    private GregorianCalendar dataInstalacao;
    private String dataInstalacaoFormatada;
    private GregorianCalendar dataRemocao;
    private String dataRemocaoFormatada;
    private String motivoRemocao;
    private String idOrdem; 
    
    public Integer getNeoId() {
        return neoId;
    }
    public void setNeoId(Integer neoId) {
        this.neoId = neoId;
    }
    public ClienteVO getCliente() {
        return cliente;
    }
    public void setCliente(ClienteVO cliente) {
        this.cliente = cliente;
    }
    public Chp1VO getChp1() {
        return chp1;
    }
    public void setChp1(Chp1VO chp1) {
        this.chp1 = chp1;
    }
    public GregorianCalendar getDataInstalacao() {
        return dataInstalacao;
    }
    public void setDataInstalacao(GregorianCalendar dataInstalacao) {
        this.dataInstalacao = dataInstalacao;
    }
    public GregorianCalendar getDataRemocao() {
        return dataRemocao;
    }
    public void setDataRemocao(GregorianCalendar dataRemocao) {
        this.dataRemocao = dataRemocao;
    }
    public String getMotivoRemocao() {
        return motivoRemocao;
    }
    public void setMotivoRemocao(String motivoRemocao) {
        this.motivoRemocao = motivoRemocao;
    }
    public String getDataInstalacaoFormatada() {
        return dataInstalacaoFormatada;
    }
    public void setDataInstalacaoFormatada(String dataInstalacaoFormatada) {
        this.dataInstalacaoFormatada = dataInstalacaoFormatada;
    }
    public String getDataRemocaoFormatada() {
        return dataRemocaoFormatada;
    }
    public void setDataRemocaoFormatada(String dataRemocaoFormatada) {
        this.dataRemocaoFormatada = dataRemocaoFormatada;
    }
    public String getIdOrdem() {
        return idOrdem;
    }
    public void setIdOrdem(String idOrdem) {
        this.idOrdem = idOrdem;
    }

}
