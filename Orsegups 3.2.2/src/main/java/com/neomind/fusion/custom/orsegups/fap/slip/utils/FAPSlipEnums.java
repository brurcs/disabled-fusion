package com.neomind.fusion.custom.orsegups.fap.slip.utils;

public class FAPSlipEnums
{
	public static <E extends Enum<E>> E getEnum(Long value, EnumInterface[] enumTypes)
	{
		if (value == null)
			return null;
		
		for (EnumInterface enumVal : enumTypes)
		{
			if (value == enumVal.value())
				return (E) enumVal;
		}
		
		return null;		
	}
	
	public interface EnumInterface { long value();}
	public enum Operacao implements EnumInterface
	{
		EnviarPagamento(1l),
		EditarContrato(2l),
		CriarContrato(3l);
		
		private long codigo;
		
		Operacao(long codigo)
		{
			this.codigo = codigo;
		}
		
		public Operacao get(long value)
		{
			for (Operacao operacao : Operacao.values())
			{
				if (operacao.value() == value)
					return operacao;
			}
			return null;
		}
		
		public long value()
		{
			return codigo;
		}
	}
	
	public enum TipoPagamento implements EnumInterface
	{
		ContratoOrcamento(1l),
		Avulso(2l),
		SolicitacaoCompra(3l);
		
		private long codigo;
		
		TipoPagamento(long codigo)
		{
			this.codigo = codigo;
		}
		
		public long value()
		{
			return codigo;
		}
	}
	
	public enum Modalidade implements EnumInterface
	{
		Contrato(1l),
		Orcamento(2l);
		
		private long codigo;
		
		Modalidade(long codigo)
		{
			this.codigo = codigo;
		}
		
		public long value()
		{
			return codigo;
		}
	}
	
	public enum Criterio implements EnumInterface
	{
		SemPreAprovacao(1l),
		Media(2l),
		Fixo(3l),
		Variavel(4l),
		OrcamentoIndividual(5l),
		OrcamentoCompartilhado(6l);
		
		private long codigo;
		
		Criterio(long codigo)
		{
			this.codigo = codigo;
		}
		
		public long value()
		{
			return codigo;
		}
	}
	
	public enum TipoOrcamento implements EnumInterface
	{
		Nenhum(0l),
		Individual(1l),
		Compartilhado(2l);
		
		private long codigo;
		
		TipoOrcamento(long codigo)
		{
			this.codigo = codigo;
		}
		
		public long value()
		{
			return codigo;
		}
	}
	
	public enum TipoRateio implements EnumInterface
	{
		Porcentagem(1l),
		Valor(2l);
		
		private long codigo;
		
		TipoRateio(long codigo)
		{
			this.codigo = codigo;
		}
		
		public long value()
		{
			return codigo;
		}
	}
	
	public enum ProdutoServico implements EnumInterface
	{
		Servico(1l),
		Produto(2l);
		
		private long codigo;
		
		ProdutoServico(long codigo)
		{
			this.codigo = codigo;
		}
		
		public long value()
		{
			return codigo;
		}
	}
}
