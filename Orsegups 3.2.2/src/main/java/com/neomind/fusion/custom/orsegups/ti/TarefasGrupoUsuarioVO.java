package com.neomind.fusion.custom.orsegups.ti;

import java.util.ArrayList;
import java.util.Collection;

public class TarefasGrupoUsuarioVO
{
	private String id;
	private String grupo;
	private int total;
	private Long totalExecMes;
	
	private Collection<TarefasUsuarioVO> tarefas = new ArrayList<TarefasUsuarioVO>();
	
	public String getGrupo()
	{
		return grupo;
	}
	public void setGrupo(String grupo)
	{
		this.grupo = grupo;
	}
	
	public int getTotal()
	{
		return total;
	}
	public void setTotal(int total)
	{
		this.total = total;
	}
	public Collection<TarefasUsuarioVO> getTarefas()
	{
		return tarefas;
	}
	public void setTarefas(Collection<TarefasUsuarioVO> tarefas)
	{
		this.tarefas = tarefas;
	}
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public Long getTotalExecMes() {
	    return totalExecMes;
	}
	public void setTotalExecMes(Long totalExecMes) {
	    this.totalExecMes = totalExecMes;
	}
	
}
