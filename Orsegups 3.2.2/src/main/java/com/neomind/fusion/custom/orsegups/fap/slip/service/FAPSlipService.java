package com.neomind.fusion.custom.orsegups.fap.slip.service;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsGroupLevels;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.TaskStatus;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

@Path("/slip")
public class FAPSlipService
{
	String valor = "";
	Long iteration = 1l;
	Long startActivityNeoId = 0l;
	
	@GET
	@Path("/getPagamentos/{page}")
	@Produces("application/json")
	public Response getPagamentos(@PathParam("page") Integer page) throws JSONException
	{
		NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
		JSONArray jsonArr = new JSONArray();
		
		NeoObject noParametrizacao = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipParametrizacao"), new QLEqualsFilter("parametro", "usuarioVisualizador"));
		EntityWrapper wParametrizacao = new EntityWrapper(noParametrizacao);
		valor = wParametrizacao.findGenericValue("valor");
		
		Integer pageSize = 20;
		Integer start  = (page - 1) * pageSize;

		Long total = 0l;
		
		try
		{
			QLGroupFilter qlGroup = new QLGroupFilter("AND");
			qlGroup.addFilter(new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal()));
			qlGroup.addFilter(new QLEqualsFilter("operacao.codigo", 1l));
			qlGroup.addFilter(new QLFilterIsNotNull("dataVencimento"));
			
			total = PersistEngine.countObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup);
			
			List<NeoObject> listPagamentos = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup, start, pageSize, "dataVencimento");
			for (NeoObject noFAP : listPagamentos)
			{
				Boolean pagamentoValido = false;
				try
				{
					pagamentoValido = verificarUsuarioPagamento(noFAP);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
				if (pagamentoValido)
				{
					EntityWrapper wFAP = new EntityWrapper(noFAP);
					
					GregorianCalendar gcDataVencimento = wFAP.findGenericValue("dataVencimento");
					
					String codTarefa = wFAP.findGenericValue("wfprocess.code");
					String dataVencimento = NeoDateUtils.safeDateFormat(gcDataVencimento, "yyyy-MM-dd");
					String dataVencimentoStr = NeoDateUtils.safeDateFormat(gcDataVencimento, "dd/MM/yyyy");
					String filial = wFAP.findGenericValue("filial.codemp") + " - " + wFAP.findGenericValue("filial.codfil");
					String fornecedor = wFAP.findGenericValue("fornecedor.apefor");
					String solicitante = wFAP.findGenericValue("solicitante.fullName");
					String nrDocumento = wFAP.findGenericValue("nrDocumento");
					String competencia = NeoDateUtils.safeDateFormat((GregorianCalendar) wFAP.findGenericValue("competencia"), "MM/yyyy");
					String tipoPagamento = wFAP.findGenericValue("tipoPagamento.descricao");
					
					Long codContrato = wFAP.findGenericValue("dadosContrato.codContrato");
					BigDecimal valorPagamento = wFAP.findGenericValue("valorPagamento");
					
					String atividade = findAtividadeAtual(noFAP);
					if (atividade == null)
						continue;
					
					List<SecurityEntity> listPossiveisAprovadores = new ArrayList<>();
					
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("codTarefa", codTarefa);
					jsonObj.put("dataVencimento", dataVencimento);
					jsonObj.put("dataVencimentoStr", dataVencimentoStr);
					jsonObj.put("valorPagamento", valorPagamento != null ? monetaryFormat.format(valorPagamento) : "R$ 0,00");
					jsonObj.put("fornecedor", fornecedor);
					jsonObj.put("filial", filial);
					jsonObj.put("solicitante", solicitante);
					jsonObj.put("nrDocumento", nrDocumento);
					jsonObj.put("competencia", competencia);
					jsonObj.put("codContrato", codContrato);
					jsonObj.put("atividade", atividade);
					jsonObj.put("tipoPagamento", tipoPagamento);
					jsonObj.put("atividades", buildTimeline(noFAP));
					
					jsonArr.put(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return Response.serverError().build();
		}
		
		JSONObject jsonReturn = new JSONObject();
		jsonReturn.put("pagamentos", jsonArr);
		jsonReturn.put("total", total);
		
		return Response.ok(jsonReturn.toString()).build();
	}
	
	@GET
	@Path("/getPagamentos")
	@Produces("application/json")
	public Response getPagamentos() throws JSONException
	{
		NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
		JSONArray jsonArr = new JSONArray();
		
		NeoObject noParametrizacao = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipParametrizacao"), new QLEqualsFilter("parametro", "usuarioVisualizador"));
		EntityWrapper wParametrizacao = new EntityWrapper(noParametrizacao);
		valor = wParametrizacao.findGenericValue("valor");
		
		Long total = 0l;
		
		try
		{
			QLGroupFilter qlGroup = new QLGroupFilter("AND");
			qlGroup.addFilter(new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal()));
			qlGroup.addFilter(new QLEqualsFilter("operacao.codigo", 1l));
			qlGroup.addFilter(new QLFilterIsNotNull("dataVencimento"));
			qlGroup.addFilter(new QLFilterIsNull("enviadoGTCGLN"));
			
			//total = PersistEngine.countObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup);
			
			List<NeoObject> listPagamentos = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup, "dataVencimento");
			for (NeoObject noFAP : listPagamentos)
			{
				Boolean pagamentoValido = false;
				try
				{
					pagamentoValido = verificarUsuarioPagamento(noFAP);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
				if (pagamentoValido)
				{
					EntityWrapper wFAP = new EntityWrapper(noFAP);
					
					GregorianCalendar gcDataVencimento = wFAP.findGenericValue("dataVencimento");
					
					String codTarefa = wFAP.findGenericValue("wfprocess.code");
					String dataVencimento = NeoDateUtils.safeDateFormat(gcDataVencimento, "yyyy-MM-dd");
					String dataVencimentoStr = NeoDateUtils.safeDateFormat(gcDataVencimento, "dd/MM/yyyy");
					String filial = wFAP.findGenericValue("filial.codemp") + " - " + wFAP.findGenericValue("filial.codfil");
					String fornecedor = wFAP.findGenericValue("fornecedor.apefor");
					String solicitante = wFAP.findGenericValue("solicitante.fullName");
					String nrDocumento = wFAP.findGenericValue("nrDocumento");
					String competencia = NeoDateUtils.safeDateFormat((GregorianCalendar) wFAP.findGenericValue("competencia"), "MM/yyyy");
					String tipoPagamento = wFAP.findGenericValue("tipoPagamento.descricao");
					
					Long codContrato = wFAP.findGenericValue("dadosContrato.codContrato");
					BigDecimal valorPagamento = wFAP.findGenericValue("valorPagamento");
					
					String atividade = findAtividadeAtual(noFAP);
					if (atividade == null)
						continue;
					
					List<SecurityEntity> listPossiveisAprovadores = new ArrayList<>();
					
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("codTarefa", codTarefa);
					jsonObj.put("dataVencimento", dataVencimento);
					jsonObj.put("dataVencimentoStr", dataVencimentoStr);
					jsonObj.put("valorPagamento", valorPagamento != null ? monetaryFormat.format(valorPagamento) : "R$ 0,00");
					jsonObj.put("fornecedor", fornecedor);
					jsonObj.put("filial", filial);
					jsonObj.put("solicitante", solicitante);
					jsonObj.put("nrDocumento", nrDocumento);
					jsonObj.put("competencia", competencia);
					jsonObj.put("codContrato", codContrato);
					jsonObj.put("atividade", atividade);
					jsonObj.put("tipoPagamento", tipoPagamento);
					jsonObj.put("atividades", buildTimeline(noFAP));
					
					jsonArr.put(jsonObj);
					
					total++;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return Response.serverError().build();
		}
		
		JSONObject jsonReturn = new JSONObject();
		jsonReturn.put("pagamentos", jsonArr);
		jsonReturn.put("total", total);
		
		return Response.ok(jsonReturn.toString()).build();
	}
	
	public Boolean verificarUsuarioPagamento(NeoObject noPagamento)
	{
		if (noPagamento == null)
			return false;
		
		NeoUser nuUsuarioLogado = PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", valor));
		if (nuUsuarioLogado == null)
			nuUsuarioLogado = PortalUtil.getCurrentUser();
		
		NeoPaper npVisualizarTodos = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "FAPSlipVisualizarPagamentos"));
		if (npVisualizarTodos != null && npVisualizarTodos.contains(nuUsuarioLogado)) return true;
		
		EntityWrapper wPagamento =  new EntityWrapper(noPagamento);
		
		Long codTipoPagamento = wPagamento.findGenericValue("tipoPagamento.codigo");
		
		NeoUser nuSolicitante = wPagamento.findGenericValue("solicitante");
		if (nuSolicitante != null && nuSolicitante.equals(nuUsuarioLogado)) return true;
		
		NeoObject noContrato = wPagamento.findGenericValue("dadosContrato");
		if (noContrato != null && codTipoPagamento == 1l)
		{
			EntityWrapper wContrato =  new EntityWrapper(noContrato);
			
			NeoUser nuResponsavelContrato = wContrato.findGenericValue("responsavelContrato");
			if (nuResponsavelContrato != null && nuResponsavelContrato.equals(nuUsuarioLogado)) return true;
			
			List<NeoObject> listPermissoes = wContrato.findGenericValue("listaPermissao");
			for (NeoObject noUsuarioPermitido : listPermissoes)
			{
				if (noUsuarioPermitido instanceof NeoUser && noUsuarioPermitido.equals(nuUsuarioLogado))
				{
					return true;
				}
				else if (noUsuarioPermitido instanceof NeoPaper)
				{
					NeoPaper npUsuarios = (NeoPaper) noUsuarioPermitido;
					if (npUsuarios.contains(nuUsuarioLogado)) return true;
				}
				else if (noUsuarioPermitido instanceof NeoGroup)
				{
					NeoGroup ngUsuarios = (NeoGroup) noUsuarioPermitido;
					if (ngUsuarios.contains(nuUsuarioLogado)) return true;
				}
			}
			
			if (nuUsuarioLogado.getGroup().getGroupLevel() >= OrsegupsGroupLevels.GERENCIA)
			{
	 			Set<NeoGroup> lowerGroups = nuUsuarioLogado.getGroup().getLowerLevelGroups();
	 			for (NeoGroup lowerGroup : lowerGroups)
				{
					if (lowerGroup.contains(nuSolicitante)) return true;
					
					if (lowerGroup.contains(nuResponsavelContrato)) return true;		
					
					if (listPermissoes.contains(lowerGroup)) return true;
					
					for (NeoObject noUsuarioPermitido : listPermissoes)
					{
						if (noUsuarioPermitido instanceof NeoUser && lowerGroup.contains((NeoUser) noUsuarioPermitido))
						{
							return true;
						}
						else if (noUsuarioPermitido instanceof NeoPaper)
						{
							for (NeoUser nuPapel : ((NeoPaper) noUsuarioPermitido).getAllUsers())
							{
								if (lowerGroup.contains(nuPapel)) return true;
							}
						}
					}
				}
			}
		}
		else if (codTipoPagamento == 2l)
		{
			if (nuUsuarioLogado.getGroup().getGroupLevel() >= OrsegupsGroupLevels.GERENCIA)
			{
	 			Set<NeoGroup> lowerGroups = nuUsuarioLogado.getGroup().getLowerLevelGroups();
	 			for (NeoGroup lowerGroup : lowerGroups)
				{
					if (lowerGroup.contains(nuSolicitante)) return true;
				}
			}
		}
		
		return false;
	}
	
	private JSONObject buildTimeline(NeoObject noFAP)
	{
		JSONObject jsonAtividades = new JSONObject();
		
		EntityWrapper wFAP = new EntityWrapper(noFAP);
		
		WFProcess process = wFAP.findGenericValue("wfprocess");
		Activity aInserirDados = getActivityByName("Inserir Dados Pagamento", process, null);
		Activity aValidacao = aInserirDados != null ? getActivityByName("Validar Pagamento", process, aInserirDados.getNeoId()) : null;
		Activity aValidacaoGerencia = aValidacao != null ? getActivityByName("Validar Pagamento - Gerência", process, aValidacao.getNeoId()) : null;
		Activity aAprovacaoDiretoria = aValidacaoGerencia != null ? getActivityByName("Aprovar Pagamento - Diretoria Administrativa", process, aValidacaoGerencia.getNeoId()) : null;
		
		try
		{
			jsonAtividades.put("criado", getActivityInfo(aInserirDados));
			jsonAtividades.put("aprovacao", findAprovadores(noFAP, aInserirDados.getNeoId()));
			jsonAtividades.put("validacao", getActivityInfo(aValidacao));
			jsonAtividades.put("validacaoGerencia", getActivityInfo(aValidacaoGerencia));
			jsonAtividades.put("aprovacaoDiretoria", getActivityInfo(aAprovacaoDiretoria));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}		
		
		return jsonAtividades;
	}	
	
	private JSONArray findAprovadores(NeoObject noFAP, Long referenceActivityNeoId) throws JSONException
	{
		JSONArray jsonArr = new JSONArray();
		
		EntityWrapper wFAP = new EntityWrapper(noFAP);
		WFProcess process = wFAP.findGenericValue("wfprocess");
		
		Boolean demandaAprovacao = wFAP.findGenericValue("demandaAprovacao");
		Boolean extraPrazo = wFAP.findGenericValue("extraPrazo");
		
		Boolean finished = false;
		String executor = "Não executado";
		String data = "Não executado";
		Long nivel = 1l;
		
		String tipoPagamento = wFAP.findGenericValue("tipoPagamento.descricao");
		if (tipoPagamento.equalsIgnoreCase("Contrato"))
		{
			LinkedList<Long> listNiveis = new LinkedList<>();
			
			List<SecurityEntity> listAprovadores = wFAP.findGenericValue("dadosContrato.aprovadores");
			ordernarAprovadoresPorNivel(listAprovadores);
			for (NeoObject noAprovador : listAprovadores)
			{
				EntityWrapper wAprovador = new EntityWrapper(noAprovador);
				
				executor = "Não executado";
				data = "Não executado";
				nivel = wAprovador.findGenericValue("nivel");
				finished = false;
				
				if (listNiveis.contains(nivel))
					continue;
				
				listNiveis.add(nivel);
				
				SecurityEntity seAprovador = wAprovador.findGenericValue("aprovador");
				if (seAprovador instanceof NeoUser)
					executor = ((NeoUser) seAprovador).getFullName();
				else if (seAprovador instanceof NeoPaper || seAprovador instanceof NeoGroup)
					executor = ((NeoPaper) seAprovador).getName();
				
				List<Task> listCompletedTasks = process.getAllCompletedTasks(2);
				for (Task task : listCompletedTasks)
				{
					if (task.getActivityName().equals("Aprovar Pagamento") && task.getActivity().getNeoId() > referenceActivityNeoId && ((task.getDelegatedUser() != null && task.getDelegatedUser().equals(seAprovador)) || (task.getUser() != null && task.getUser().equals(seAprovador))))
					{
						executor = task.getFinishByUser().getFullName();
						data = NeoDateUtils.safeDateFormat(task.getFinishDate(), "dd/MM/yyyy");
						finished =  true;
						
						break;
					}
				}
				
				if (!finished && ((demandaAprovacao != null && !demandaAprovacao) || (extraPrazo != null && extraPrazo)))
				{
					finished = true;
					if ((demandaAprovacao != null && !demandaAprovacao))
						data = "Aprovado Automaticamente";
					else if ((extraPrazo != null && extraPrazo))
						data = "Pagamento Extra-Prazo";
				}					
				
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("executor", executor);
				jsonObj.put("nivel", nivel++);
				jsonObj.put("data", data);
				jsonObj.put("finished", finished);
				jsonArr.put(jsonObj);
			}
		}
		else if(tipoPagamento.equalsIgnoreCase("Avulso"))
		{		
			NeoUser nuSolicitante = wFAP.findGenericValue("solicitante");
			if (nuSolicitante.getGroup().getGroupLevel() >= OrsegupsGroupLevels.DIRETORIA)
			{
				executor = nuSolicitante != null ?  nuSolicitante.getFullName() : "Não encontrado";
				data = "Aprovado Diretor";
				nivel = 1l;
				
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("data", data);
				jsonObj.put("finished", true);
				jsonObj.put("executor", executor);
				jsonObj.put("nivel", nivel);
				jsonArr.put(jsonObj);
			}
			else
			{
				NeoUser nuAprovador = findAprovadorAvulso(nuSolicitante);
				
				executor = nuAprovador != null ?  nuAprovador.getFullName() : "Não encontrado";
				data = "Não executado";
				nivel = 1l;
				
				List<Task> listCompletedTasks = process.getAllCompletedTasks(2);
				for (Task task : listCompletedTasks)
				{
					if (task.getActivityName().equals("Aprovar Pagamento") && task.getActivity().getNeoId() > referenceActivityNeoId && ((task.getDelegatedUser() != null && task.getDelegatedUser().equals(nuAprovador)) || (task.getUser() != null && task.getUser().equals(nuAprovador))))
					{
						executor = task.getFinishByUser().getFullName();
						data = NeoDateUtils.safeDateFormat(task.getFinishDate(), "dd/MM/yyyy");
						finished =  true;
						
						break;
					}
				}
				
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("data", data);
				jsonObj.put("finished", finished);
				jsonObj.put("executor", executor);
				jsonObj.put("nivel", nivel++);
				jsonArr.put(jsonObj);
				
				Integer count = 0;
				
				while (nuAprovador != null && count < 10)
				{
					nuAprovador = findAprovadorAvulso(nuAprovador);				
					if (nuAprovador != null)
					{
						jsonObj = new JSONObject();
						executor = nuAprovador != null ?  nuAprovador.getFullName() : "Não encontrado";
						data = "Não executado";
						
						if (finished)
						{
							finished = false;
							
							for (Task task : listCompletedTasks)
							{
								if (task.getActivityName().equals("Aprovar Pagamento") && task.getActivity().getNeoId() > referenceActivityNeoId && ((task.getDelegatedUser() != null && task.getDelegatedUser().equals(nuAprovador)) || (task.getUser() != null && task.getUser().equals(nuAprovador))))
								{
									executor = task.getFinishByUser().getFullName();
									data = NeoDateUtils.safeDateFormat(task.getFinishDate(), "dd/MM/yyyy");
									finished =  true;
									
									break;
								}
							}
						}
						
						jsonObj.put("data", data);
						jsonObj.put("finished", finished);
						jsonObj.put("executor", executor);
						jsonObj.put("nivel", nivel++);
						jsonArr.put(jsonObj);
					}
					
					count++;
				}
			}
		}
		
		return jsonArr;
	}
	
	public NeoUser findAprovadorAvulso(NeoUser nuSolicitante)
	{
		NeoUser nuAprovador = nuSolicitante;
		
		Integer nivelMax = OrsegupsGroupLevels.DIRETORIA;
		
		if (nuAprovador.getGroup().getUpperLevel().getGroupLevel() <= nivelMax)
		{
			for (NeoUser nuUser : nuAprovador.getGroup().getUpperLevel().getResponsible().getUsers())
			{
				if (nuUser.isActive())
					return nuUser;
			}
		}
		else {
			if (nuAprovador.getGroup().getGroupLevel() < nivelMax)
			{
				for (NeoUser nuUser : nuAprovador.getGroup().getUpperLevel().getResponsible().getUsers())
				{
					if (nuUser.isActive())
						return nuUser;
				}
			}
			
			return null;
		}
		
		return nuAprovador;
	}
	
	public String findAtividadeAtual(NeoObject noFAP)
	{
		EntityWrapper wFAP = new EntityWrapper(noFAP);
		
		WFProcess process = wFAP.findGenericValue("wfprocess");
		if (process != null)
		{
			List<TaskInstance> listTaskInstance = TaskInstanceHelper.findTaskInstances(process);
			for (TaskInstance taskInstance : listTaskInstance)
			{
				if (taskInstance.getProcessName().equals("F002 - FAP Slip") && (!taskInstance.getStatus().equals(TaskStatus.SUSPENDED) && !taskInstance.getStatus().equals(TaskStatus.COMPLETED)))
				{
					String taskName = taskInstance.getTaskName();
					String owner = taskInstance.getOwner() != null ? taskInstance.getOwner().getName() : taskInstance.getActivity().getTaskAssigner().getName();
					
					return taskName + " (" + (owner != null ? owner : "") + ")";
				}
			}
		}
		
		return null;
	}
	
	private JSONObject getActivityInfo(Activity activity)
	{
		QLGroupFilter qlGroup = new QLGroupFilter("AND");
		qlGroup.addFilter(new QLEqualsFilter("activity", activity));
		
		Task task = PersistEngine.getNeoObject(Task.class, qlGroup);
		
		String executor = (task != null && task.getFinishByUser() != null) ? task.getFinishByUser().getFullName() : "Não executado";
				
		JSONObject jsonObj = new JSONObject();
		try
		{
			jsonObj.put("executor", executor);
			jsonObj.put("data", (task != null && task.getFinishDate() != null) ? NeoDateUtils.safeDateFormat(task.getFinishDate(), "dd/MM/yyyy") : "Não executado");
			jsonObj.put("finished", task != null && task.getFinishDate() != null);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		
		return jsonObj;
	}
	
	private Activity getActivityByName(String activityName, WFProcess process, Long referenceActivityNeoId)
	{
		QLGroupFilter filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("process", process));
		filter.addFilter(new QLEqualsFilter("model.name", activityName));
		if (referenceActivityNeoId != null)
			filter.addFilter(new QLRawFilter("_vo.neoId >= " + referenceActivityNeoId));
		
		List<Activity> listActivities = PersistEngine.getObjects(Activity.class, filter, "neoId desc");
		
		if (!listActivities.isEmpty())
			return (Activity) PersistEngine.getObjects(Activity.class, filter, "neoId desc").get(0);
		else
			return null;
	}
	
	private String limitarCaracter(String text, Integer limit)
	{
		if (text != null && text.length() >= (limit+1))
			return text.substring(0, limit);
		
		return text;
	}
	
	public void ordernarAprovadoresPorNivel(Collection<? extends NeoObject> list)
	{
		Collections.sort((List<? extends NeoObject>) list, new Comparator<NeoObject>()
		{

			public int compare(NeoObject obj1, NeoObject obj2)
			{

				return ((Long) new EntityWrapper(obj1).findGenericValue("nivel") < (Long) new EntityWrapper(obj2).findGenericValue("nivel")) ? -1 : 1;
			}
		});
	}
}
