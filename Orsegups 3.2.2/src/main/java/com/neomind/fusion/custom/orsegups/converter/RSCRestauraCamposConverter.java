package com.neomind.fusion.custom.orsegups.converter;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;

public class RSCRestauraCamposConverter extends StringConverter
{
	
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		/*StringBuilder button = new StringBuilder();
		button.append("<script>");
		button.append("		$('label[for*=_restaurarCliente__]').hide(); ");
		button.append("</script>");
		button.append("<div align='center'>");
		button.append("<input type='button' class='input_button' id='restauraClienteSapiensButton' onClick='javascript:atualizaCamposClienteSapiens()' value='Restaurar Valores Originais' title='Restaurar Valores Originais'>");
		button.append("</div>");*/
		
		String botao = "<input type='button' class='input_button' id='restauraClienteSapiensButton' onClick='javascript:atualizaCamposClienteSapiens()' value='Restaurar Valores Originais' title='Restaurar Valores Originais'>";
		return botao;
	}	
}
