package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoUtils;

public class RotinaAbreTarefaEnvDocDigPresenca implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaEnvDocDigPresenca.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Providenciar envio de movimento do Mês para digitalização. Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		
		/**
		 * @author orsegups lucas.avila - Troca de JSP para Servlet.
		 * @date 08/07/2015
		 */
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
			String executor = "";
			String solicitante = "jairo";
			String titulo = "Providenciar envio de movimento do Mês para digitalização";
			GregorianCalendar prazo = new GregorianCalendar();
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 5L);
			prazo.set(Calendar.HOUR_OF_DAY, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);
			
			NeoPaper papel = new NeoPaper();
			papel = OrsegupsUtils.getPaper("Responsável envio de movimento");
			
			if(papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers() )
				{
					executor = user.getCode();
					break;
				}
			}
			else
			{
				log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Providenciar envio de movimento do Mês para digitalização não encontrado - Papel " + papel.getName());
			}
			
			String descricao = "";
			descricao = "<strong>Providenciar o envio dos documentos gerados no mês anterior para digitalização e disponibilização no Presença.</strong>";
			descricao = descricao.replaceAll("'", "");
			descricao = descricao.replaceAll("\"", "");
			
			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
	}
}
