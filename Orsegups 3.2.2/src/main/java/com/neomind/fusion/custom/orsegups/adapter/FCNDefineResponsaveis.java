package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Collection;
import java.util.List;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FCNDefineResponsaveis implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			System.out.println("Começou a Inserir o Responsável");
			String codReg = processEntity.findField("contratoSapiens.usu_codreg").getValue().toString();

			if (NeoUtils.safeIsNull(codReg))
			{
				throw new WorkflowException("Não foi possível determinar a regional do contrato");
			}
			
			QLFilter filter = null;
			
			if(verificaContasRastreamento(processEntity)) {
				filter = new QLEqualsFilter("codigo", 99L);
			}else {
				filter = new QLEqualsFilter("codigo", Long.valueOf(codReg));				
			}

			NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("FCNresponsabilidade"), filter);

			if (NeoUtils.safeIsNull(obj))
			{
				throw new WorkflowException("Não foi possível determinar a regional do contrato"
					+ " (Objeto não localizado no eform FCNresponsabilidade). Regional: " + codReg.toString());
			}
			EntityWrapper wrapperObj = new EntityWrapper(obj);

			NeoPaper justEquipamento = (NeoPaper) wrapperObj.findValue("responsavelEquipamento");
			NeoPaper gerente = (NeoPaper) wrapperObj.findValue("respGerente");
			NeoPaper coodernador = (NeoPaper) wrapperObj.findValue("respCoordenador");
			NeoPaper analista = (NeoPaper) wrapperObj.findValue("respAnalista");
			NeoPaper comercial = (NeoPaper) wrapperObj.findValue("respComercial");
			NeoPaper superintendencia = (NeoPaper) wrapperObj.findValue("respSuperintendencia");
			NeoPaper diretor = (NeoPaper) wrapperObj.findValue("respDiretor");
			NeoPaper presidencia = (NeoPaper) wrapperObj.findValue("respPresidencia");

			System.out.println("Passou pelos insertes");

			processEntity.setValue("responsavelJustificativaEquipamento", justEquipamento);
			processEntity.setValue("gerenteRegional", gerente);
			processEntity.setValue("coordenadorRegional", coodernador);
			processEntity.setValue("analistaRegional", analista);
			processEntity.setValue("gerenteComercial", comercial);
			processEntity.setValue("superintendencia", superintendencia);
			processEntity.setValue("diretor", diretor);
			processEntity.setValue("presidente", presidencia);

			System.out.println("Terrminou de Inserir o Responsável");
		}
		catch (Exception e)
		{
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				System.out.println("workflow C023 - FCN - Cancelamento de Contrato Iniciativa eNEW / C025 - FCN - Cancelamento de Contrato Inadimplência eNEW - Metodo start ");
				throw new WorkflowException("Erro ao Inserir o Responsável" + e);
			}
		}
	}

	public Boolean verificaContasRastreamento(EntityWrapper processEntity) {
		
		Boolean isRastreamento = false; 
		
		Collection<NeoObject> contasSigma = (Collection<NeoObject>) processEntity.findValue("listaContasSigma");
		
		if ((contasSigma != null) && (!contasSigma.isEmpty())) {
			for(NeoObject noContasSigma : contasSigma) {
				
				EntityWrapper wpContasSigma = new EntityWrapper(noContasSigma);
				String conta = wpContasSigma.findField("id_central").getValueAsString();
				
				if(conta.startsWith("R")) {
					return true;
				}
				
			}
		}
		
		Long empresa = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
		Long filial = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
		Long contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

		QLGroupFilter filterCtr = new QLGroupFilter("AND");
		filterCtr.addFilter(new QLEqualsFilter("usu_codemp", empresa));
		filterCtr.addFilter(new QLEqualsFilter("usu_codfil", filial));
		filterCtr.addFilter(new QLEqualsFilter("usu_numctr", contrato));
		
		InstantiableEntityInfo infoContaSigma = AdapterUtils.getInstantiableEntityInfo("SIGMAVCLIENTES");

		StringBuilder sql = new StringBuilder();
		sql.append(" select sig.usu_codcli from usu_t160sig sig  ");
		sql.append(" where sig.usu_codemp = " + empresa);
		sql.append(" and sig.usu_codfil = " + filial);
		sql.append(" and usu_numctr = " + contrato);
		sql.append(" order by usu_codcli ");
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();

		if ((resultList != null) && (resultList.size() > 0)) {
			for (Object result : resultList) {
				String cdCli = NeoUtils.safeOutputString(result);
				Long cd_cli = Long.valueOf(Long.parseLong(cdCli));
				QLEqualsFilter filterConta = new QLEqualsFilter("cd_cliente", cd_cli);
				List<NeoObject> listaObjectConta = PersistEngine.getObjects(infoContaSigma.getEntityClass(),filterConta);
				if ((listaObjectConta != null) && (!listaObjectConta.isEmpty())) {
					NeoObject noConta = (NeoObject) listaObjectConta.get(0);
					EntityWrapper wpConta = new EntityWrapper(noConta);
					if (noConta != null) {
						String conta = (String) wpConta.findField("id_central").getValue();
						
						if(conta.startsWith("R")) {
							return true;
						}else {
							isRastreamento = false;
						}
					}
				}
			}
		}
		
		return isRastreamento;
	}
	
}
