package com.neomind.fusion.custom.orsegups.contract;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.SortedSet;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;

public class ContratoGerarTabelaComparativa implements AdapterInterface
{
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		NeoObject cliente = (NeoObject) wrapper.findValue("novoCliente");
		NeoObject clienteBackup = (NeoObject) wrapper.findValue("novoClienteBackup");
		wrapper.findField("listaAlteracoesCliente").getValues().clear();
		if(compareFieldCliente(wrapper, cliente, clienteBackup))
		{
			wrapper.setValue("houveAlteracaoDadosCliente", true);
		}
		
		/*
		 * para contrato novo, apenas esse campo poderá conter alterações
		 */
		long movimentoContrato = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		if(movimentoContrato <= 4 || movimentoContrato == 6)
			return;
		
		NeoObject dadosGerais = (NeoObject) wrapper.findValue("dadosGeraisContrato");
		NeoObject dadosGeraisBackup = (NeoObject) wrapper.findValue("dadosGeraisContratoBackup");
		wrapper.findField("listaAlteracoesContrato").getValues().clear();
		if(!compareFieldContrato(wrapper, dadosGerais, dadosGeraisBackup))
		{
			wrapper.setValue("houveAlteracaoDadosGerais", true);
		}
		
		Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
		Collection<NeoObject> postosBackup = wrapper.findField("postosContratoBackup").getValues();
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			
			wPosto.findField("listaAlteracoes").getValues().clear();
			
			long numeroPosto = 0;
			if(wPosto.findValue("numPosto") != null)
				numeroPosto = (Long) wPosto.findValue("numPosto");
			NeoObject postoBackupAlvo = null;
			for(NeoObject postoBackup : postosBackup)
			{
				EntityWrapper wPostoBackup = new EntityWrapper(postoBackup);
				long numeroPostoBackup = (Long) wPostoBackup.findValue("numPosto");
				if(numeroPostoBackup == numeroPosto)
				{
					postoBackupAlvo = postoBackup;
				}
			}
			
			if(postoBackupAlvo == null)
			{
				//fluxo de alteração, um novo posto criado
				continue;
			}
			EntityWrapper wPostoBackup = new EntityWrapper(postoBackupAlvo);
			
			SortedSet<FieldInfo> fields = posto.getInfo().getFieldSet();
			/*
			 * é necessário ter certeza que este campo ficará true, somente se achar algum campo alterado
			 * se por algum motivo o fluxo der algum loop, isso irá refletir alterações anteriores
			 */
			wPosto.setValue("houveAlteracaoPosto", false);
			for(FieldInfo fi : fields)
			{
				if(!compareField(wPostoBackup, wPosto, fi.getName(), fi.getTitle()))
				{
					wPosto.setValue("houveAlteracaoPosto", true);
				}
			}
			
		}
	}
	
	public void gerarRegistroAlteracao(EntityWrapper a, String field, String acao, String valorAntigo, String valorNovo, String obs)
	{
		NeoObject alteracao = AdapterUtils.createNewEntityInstance("FGCTabelaAlteracoes");
		EntityWrapper wAlteracao = new EntityWrapper(alteracao);
		
		if(NeoUtils.safeIsNull(valorAntigo) || valorAntigo.equals(""))
			valorAntigo = "Vazio";
		
		if(NeoUtils.safeIsNull(valorNovo) || valorNovo.equals(""))
			valorNovo = "Vazio";
		
		wAlteracao.setValue("campo", field);
		wAlteracao.setValue("acao", acao);
		wAlteracao.setValue("valorAntes", valorAntigo);
		wAlteracao.setValue("valorAtual", valorNovo);
		wAlteracao.setValue("obs", obs);
		PersistEngine.persist(alteracao);
		
		try
		{
			a.findField("listaAlteracoes").getValues().add(alteracao);
		}catch(NullPointerException e)
		{
			a.findField("listaAlteracoesContrato").getValues().add(alteracao);
		}
	}
	
	public void gerarRegistroAlteracaoCliente(EntityWrapper a, String field, String acao, String valorAntigo, String valorNovo, String obs)
	{
		NeoObject alteracao = AdapterUtils.createNewEntityInstance("FGCTabelaAlteracoes");
		EntityWrapper wAlteracao = new EntityWrapper(alteracao);
		
		if(NeoUtils.safeIsNull(valorAntigo) || valorAntigo.equals(""))
			valorAntigo = "Vazio";
		
		if(NeoUtils.safeIsNull(valorNovo) || valorNovo.equals(""))
			valorNovo = "Vazio";
		
		wAlteracao.setValue("campo", field);
		wAlteracao.setValue("acao", acao);
		wAlteracao.setValue("valorAntes", valorAntigo);
		wAlteracao.setValue("valorAtual", valorNovo);
		wAlteracao.setValue("obs", obs);
		PersistEngine.persist(alteracao);
		
		try
		{
			a.findField("listaAlteracoesCliente").getValues().add(alteracao);
		}catch(NullPointerException e)
		{
			System.out.println("[FLUXO CONTRATOS] - gerarRegistroAlteracaoCliente");
		}
	}
	
	public boolean compareFieldCliente(EntityWrapper wrapper, NeoObject cliente, NeoObject clienteBackup)
	{
		boolean result = false;
		EntityWrapper wCliente = new EntityWrapper(cliente);
		EntityWrapper wClienteBackup = new EntityWrapper(clienteBackup);
		
		//inserção ou exclusão
		if((wCliente.findValue("enderecoCliente") == null && wClienteBackup.findValue("enderecoCliente") != null) ||
		   (wCliente.findValue("enderecoCliente") != null && wClienteBackup.findValue("enderecoCliente") == null))
	    {
			result = true;
			if(wClienteBackup.findValue("enderecoCliente") == null)
			{
				String endereco = "CEP = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.cep")) + "<br>";
				endereco += "Endereço = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.endereco")) + "<br>";
				endereco += "Número = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.numero")) + "<br>";
				endereco += "Complemento = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.complemento")) + "<br>";
				endereco += "Bairro = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.bairro")) + "<br>";
				endereco += "Cidade = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.nomcid")) + "<br>";
				endereco += "Estado = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.sigufs")) + "<br>";
				gerarRegistroAlteracaoCliente(wrapper, "Endereço do Cliente", "Inclusão", "Vazio", endereco, "Valor Adicionado no Cadastro do Cliente.");
			}else
			{
				String endereco = "CEP = " + NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.cep")) + "<br>";
				endereco += "Endereço = " + NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.endereco")) + "<br>";
				endereco += "Número = " + NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.numero")) + "<br>";
				endereco += "Complemento = " + NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.complemento")) + "<br>";
				endereco += "Bairro = " + NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.bairro")) + "<br>";
				endereco += "Cidade = " + NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.estadoCidade.cidade.nomcid")) + "<br>";
				endereco += "Estado = " + NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.estadoCidade.cidade.sigufs")) + "<br>";
				gerarRegistroAlteracaoCliente(wrapper, "Endereço do Cliente", "Exclusão", endereco, "Vazio", "Valor Removido no Cadastro do Cliente.");
			}
			
	    }
		
		if((wCliente.findValue("endEntrega") == null && wClienteBackup.findValue("endEntrega") != null) ||
		   (wCliente.findValue("endEntrega") != null && wClienteBackup.findValue("endEntrega") == null))
	    {
			result = true;
			if(wClienteBackup.findValue("endEntrega") == null)
			{
				String endereco = "CEP = " + NeoUtils.safeOutputString(wCliente.findValue("endEntrega.cep")) + "<br>";
				endereco += "Endereço = " + NeoUtils.safeOutputString(wCliente.findValue("endEntrega.endereco")) + "<br>";
				endereco += "Número = " + NeoUtils.safeOutputString(wCliente.findValue("endEntrega.numero")) + "<br>";
				endereco += "Complemento = " + NeoUtils.safeOutputString(wCliente.findValue("endEntrega.complemento")) + "<br>";
				endereco += "Bairro = " + NeoUtils.safeOutputString(wCliente.findValue("endEntrega.bairro")) + "<br>";
				endereco += "Cidade = " + NeoUtils.safeOutputString(wCliente.findValue("endEntrega.estadoCidade.cidade.nomcid")) + "<br>";
				endereco += "Estado = " + NeoUtils.safeOutputString(wCliente.findValue("endEntrega.estadoCidade.cidade.sigufs")) + "<br>";
				if(NeoUtils.safeBoolean(wrapper.findValue("novoCliente.endEntregaIgualCliente")))
				{
					endereco = "CEP = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.cep")) + "<br>";
					endereco += "Endereço = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.endereco")) + "<br>";
					endereco += "Número = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.numero")) + "<br>";
					endereco += "Complemento = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.complemento")) + "<br>";
					endereco += "Bairro = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.bairro")) + "<br>";
					endereco += "Cidade = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.nomcid")) + "<br>";
					endereco += "Estado = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.sigufs")) + "<br>";
				}
				
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega", "Inclusão", "Vazio", endereco, "Valor Adicionado no Cadastro do Cliente.");
			}else
			{
				String endereco = "CEP = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.cep")) + "<br>";
				endereco += "Endereço = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.endereco")) + "<br>";
				endereco += "Número = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.numero")) + "<br>";
				endereco += "Complemento = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.complemento")) + "<br>";
				endereco += "Bairro = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.bairro")) + "<br>";
				endereco += "Cidade = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.estadoCidade.cidade.nomcid")) + "<br>";
				endereco += "Estado = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.estadoCidade.cidade.sigufs")) + "<br>";
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega", "Exclusão", endereco, "Vazio", "Valor Removido no Cadastro do Cliente.");
			}
	    }
		
		
		
		if((wCliente.findValue("endCobranca") == null && wClienteBackup.findValue("endCobranca") != null) ||
		   (wCliente.findValue("endCobranca") != null && wClienteBackup.findValue("endCobranca") == null))
	    {
			result = true;
			if(wClienteBackup.findValue("endCobranca") == null)
			{
				String endereco = "CEP = " + NeoUtils.safeOutputString(wCliente.findValue("endCobranca.cep")) + "<br>";
				endereco += "Endereço = " + NeoUtils.safeOutputString(wCliente.findValue("endCobranca.endereco")) + "<br>";
				endereco += "Número = " + NeoUtils.safeOutputString(wCliente.findValue("endCobranca.numero")) + "<br>";
				endereco += "Complemento = " + NeoUtils.safeOutputString(wCliente.findValue("endCobranca.complemento")) + "<br>";
				endereco += "Bairro = " + NeoUtils.safeOutputString(wCliente.findValue("endCobranca.bairro")) + "<br>";
				endereco += "Cidade = " + NeoUtils.safeOutputString(wCliente.findValue("endCobranca.estadoCidade.cidade.nomcid")) + "<br>";
				endereco += "Estado = " + NeoUtils.safeOutputString(wCliente.findValue("endCobranca.estadoCidade.cidade.sigufs")) + "<br>";
				if(NeoUtils.safeBoolean(wrapper.findValue("novoCliente.endCobrancaIgualCliente")))
				{
					endereco = "CEP = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.cep")) + "<br>";
					endereco += "Endereço = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.endereco")) + "<br>";
					endereco += "Número = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.numero")) + "<br>";
					endereco += "Complemento = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.complemento")) + "<br>";
					endereco += "Bairro = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.bairro")) + "<br>";
					endereco += "Cidade = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.nomcid")) + "<br>";
					endereco += "Estado = " + NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.sigufs")) + "<br>";
				}
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança", "Inclusão", "Vazio", endereco, "Valor Adicionado no Cadastro do Cliente.");
			}else
			{
				String endereco = "CEP = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.cep")) + "<br>";
				endereco += "Endereço = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.endereco")) + "<br>";
				endereco += "Número = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.numero")) + "<br>";
				endereco += "Complemento = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.complemento")) + "<br>";
				endereco += "Bairro = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.bairro")) + "<br>";
				endereco += "Cidade = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.estadoCidade.cidade.nomcid")) + "<br>";
				endereco += "Estado = " + NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.estadoCidade.cidade.sigufs")) + "<br>";
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança", "Exclusão", endereco, "Vazio", "Valor Removido no Cadastro do Cliente.");
			}
	    }
		
		//alteração
		if(wCliente.findValue("endEntrega") != null && wClienteBackup.findValue("endEntrega") != null)
		{
			if(!NeoUtils.safeOutputString(wCliente.findValue("endEntrega.cep")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.cep"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.cep"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endEntrega.cep"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega - cep", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endEntrega.endereco")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.endereco"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.endereco"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endEntrega.endereco"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega - endereco", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endEntrega.numero")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.numero"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.numero"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endEntrega.numero"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega - numero", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endEntrega.complemento")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.complemento"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.complemento"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endEntrega.complemento"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega - complemento", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endEntrega.bairro")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.bairro"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.bairro"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endEntrega.bairro"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega - bairro", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endEntrega.estadoCidade.cidade.nomcid")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.estadoCidade.cidade.nomcid"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.estadoCidade.cidade.nomcid"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endEntrega.estadoCidade.cidade.nomcid"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega - cidade", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endEntrega.estadoCidade.cidade.sigufs")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.estadoCidade.cidade.sigufs"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endEntrega.estadoCidade.cidade.sigufs"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endEntrega.estadoCidade.cidade.sigufs"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega - estado", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
		}
		
		if(wCliente.findValue("endCobranca") != null && wClienteBackup.findValue("endCobranca") != null)
		{
			if(!NeoUtils.safeOutputString(wCliente.findValue("endCobranca.cep")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.cep"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.cep"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endCobranca.cep"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança - cep", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endCobranca.endereco")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.endereco"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.endereco"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endCobranca.endereco"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança - endereco", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endCobranca.numero")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.numero"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.numero"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endCobranca.numero"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança - numero", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endCobranca.complemento")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.complemento"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.complemento"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endCobranca.complemento"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança - complemento", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endCobranca.bairro")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.bairro"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.bairro"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endCobranca.bairro"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança - bairro", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endCobranca.estadoCidade.cidade.nomcid")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.estadoCidade.cidade.nomcid"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.estadoCidade.cidade.nomcid"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endCobranca.estadoCidade.cidade.nomcid"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança - cidade", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("endCobranca.estadoCidade.cidade.sigufs")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.estadoCidade.cidade.sigufs"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("endCobranca.estadoCidade.cidade.sigufs"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("endCobranca.estadoCidade.cidade.sigufs"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Cobrança - estado", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
		}
		
		if(wCliente.findValue("enderecoCliente") != null && wClienteBackup.findValue("enderecoCliente") != null)
		{
			if(!NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.cep")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.cep"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.cep"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.cep"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço de Entrega - cep", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.endereco")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.endereco"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.endereco"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.endereco"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço do Cliente - endereco", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.numero")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.numero"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.numero"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.numero"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço do Cliente - numero", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.complemento")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.complemento"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.complemento"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.complemento"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço do Cliente - complemento", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.bairro")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.bairro"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.bairro"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.bairro"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço do Cliente - bairro", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.nomcid")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.estadoCidade.cidade.nomcid"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.estadoCidade.cidade.nomcid"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.nomcid"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço do Cliente - cidade", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
			
			if(!NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.sigufs")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.estadoCidade.cidade.sigufs"))))
			{
				result = true;
				String antigo = NeoUtils.safeOutputString(wClienteBackup.findValue("enderecoCliente.estadoCidade.cidade.sigufs"));
				String novo   = NeoUtils.safeOutputString(wCliente.findValue("enderecoCliente.estadoCidade.cidade.sigufs"));
				gerarRegistroAlteracaoCliente(wrapper, "Endereço do Cliente - estado", "Alteração", antigo, novo, "Valor Alterado no Cadastro do Cliente.");
			}
		}
		
		if(!NeoUtils.safeOutputString(wCliente.findValue("telefone1")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("telefone1"))))
		{
			result = true;
			gerarRegistroAlteracaoCliente(wrapper, "Telefone 1", "Alteração", NeoUtils.safeOutputString(wClienteBackup.findValue("telefone1")), NeoUtils.safeOutputString(wCliente.findValue("telefone1")), "Valor Alterado no Cadastro do Cliente.");
		}
		if(!NeoUtils.safeOutputString(wCliente.findValue("telefone2")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("telefone2"))))
		{
			result = true;
			gerarRegistroAlteracaoCliente(wrapper, "Telefone 2", "Alteração", NeoUtils.safeOutputString(wClienteBackup.findValue("telefone2")), NeoUtils.safeOutputString(wCliente.findValue("telefone2")), "Valor Alterado no Cadastro do Cliente.");
		}
		if(!NeoUtils.safeOutputString(wCliente.findValue("fax")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("fax"))))
		{
			result = true;
			gerarRegistroAlteracaoCliente(wrapper, "Fax", "Alteração", NeoUtils.safeOutputString(wClienteBackup.findValue("fax")), NeoUtils.safeOutputString(wCliente.findValue("fax")), "Valor Alterado no Cadastro do Cliente.");
		}
		if(!NeoUtils.safeOutputString(wCliente.findValue("email")).equals(NeoUtils.safeOutputString(wClienteBackup.findValue("email"))))
		{
			result = true;
			gerarRegistroAlteracaoCliente(wrapper, "E-mail", "Alteração", NeoUtils.safeOutputString(wClienteBackup.findValue("email")), NeoUtils.safeOutputString(wCliente.findValue("email")), "Valor Alterado no Cadastro do Cliente.");
		}
		return result;
	}
	
	public boolean compareFieldContrato(EntityWrapper wrapper, NeoObject dadosGerais, NeoObject dadosGeraisBackup)
	{
		boolean result = true;
		
		EntityWrapper wA = new EntityWrapper(dadosGerais);
		EntityWrapper wB = new EntityWrapper(dadosGeraisBackup);
		
		if(!NeoUtils.safeOutputString(wA.findValue("ObjetoDoContrato")).equals(NeoUtils.safeOutputString(wB.findValue("ObjetoDoContrato"))))
		{
			gerarRegistroAlteracao(wrapper, "Objeto do Contrato", "Alteração", NeoUtils.safeOutputString(wB.findValue("ObjetoDoContrato")), NeoUtils.safeOutputString(wA.findValue("ObjetoDoContrato")), "Valor Alterado nos Dados Gerais do Contrato.");
			result = false;
		}
		
		if(!NeoUtils.safeOutputString(wA.findValue("peridoMaximoVigencia2")).equals(NeoUtils.safeOutputString(wB.findValue("peridoMaximoVigencia2"))))
		{
			gerarRegistroAlteracao(wrapper, "Período Máximo Vigência(meses)", "Alteração", NeoUtils.safeOutputString(wB.findValue("peridoMaximoVigencia2")), NeoUtils.safeOutputString(wA.findValue("peridoMaximoVigencia2")), "Valor Alterado nos Dados Gerais do Contrato.");
			result = false;
		}
		
		if((NeoUtils.safeBoolean(wA.findValue("contratoComposto")) && NeoUtils.safeBoolean(wB.findValue("contratoComposto")))
		|| (!NeoUtils.safeBoolean(wA.findValue("contratoComposto")) && !NeoUtils.safeBoolean(wB.findValue("contratoComposto"))))
		{
			//iguais
		}else
		{
			String valorA = NeoUtils.safeBoolean(wA.findValue("contratoComposto")) ? "Sim" : "Não";
			String valorB = NeoUtils.safeBoolean(wB.findValue("contratoComposto")) ? "Sim" : "Não";
			gerarRegistroAlteracao(wrapper, "Contrato Composto(S/N)", "Alteração", valorB, valorA, "Valor Alterado nos Dados Gerais do Contrato.");
			result = false;
		}
		
		if((NeoUtils.safeBoolean(wA.findValue("exigeMedicao")) && NeoUtils.safeBoolean(wB.findValue("exigeMedicao")))
		|| (!NeoUtils.safeBoolean(wA.findValue("exigeMedicao")) && !NeoUtils.safeBoolean(wB.findValue("exigeMedicao"))))
		{
			//iguais
		}else
		{
			String valorA = NeoUtils.safeBoolean(wA.findValue("exigeMedicao")) ? "Sim" : "Não";
			String valorB = NeoUtils.safeBoolean(wB.findValue("exigeMedicao")) ? "Sim" : "Não";
			gerarRegistroAlteracao(wrapper, "Exige Medição(S/N)", "Alteração", valorB, valorA, "Valor Alterado nos Dados Gerais do Contrato.");
			result = false;
		}
		
		if(!NeoUtils.safeOutputString(wA.findValue("numPostosEfetivos")).equals(NeoUtils.safeOutputString(wB.findValue("numPostosEfetivos"))))
		{
			gerarRegistroAlteracao(wrapper, "N. Postos Efetivos", "Alteração", NeoUtils.safeOutputString(wA.findValue("numPostosEfetivos")), NeoUtils.safeOutputString(wB.findValue("numPostosEfetivos")), "Valor Alterado nos Dados Gerais do Contrato.");
			result = false;
		}
		
		//tipoDoContrato 
		if(!NeoUtils.safeOutputString(wA.findValue("tipoDoContrato.neoid")).equals(NeoUtils.safeOutputString(wB.findValue("tipoDoContrato.neoid"))))
		{
			gerarRegistroAlteracao(wrapper, "Tipo do Contrato", "Alteração", NeoUtils.safeOutputString(wA.findValue("tipoDoContrato.descricaoProduto")), NeoUtils.safeOutputString(wB.findValue("tipoDoContrato.descricaoProduto")), "Valor Alterado nos Dados Gerais do Contrato.");
			result = false;
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @param field
	 * @return
	 */
	
	public boolean compareField(EntityWrapper a, EntityWrapper b, String field, String tituloCampo)
	{
		boolean result = false;
		
		if(field.equals("escala") || field.equals("complementoServico"))
		{
			//System.out.println("field.equals(\"escala\") || field.equals(\"complementoServico\")");
		}
		
		if(a.findValue(field) instanceof Long)
		{
			if(NeoUtils.safeOutputString(a.findValue(field)).equals(NeoUtils.safeOutputString(b.findValue(field)))
			&& NeoUtils.safeOutputString(b.findValue(field)).equals(NeoUtils.safeOutputString(a.findValue(field))))
			{
				result = true;
			}else
			{
				gerarRegistroAlteracao(a, tituloCampo, "Alteracao", NeoUtils.safeOutputString(a.findValue(field)), NeoUtils.safeOutputString(b.findValue(field)), "Valor alterado no atual posto.");
			}
		}else if(a.findValue(field) instanceof String)
		{
			if((NeoUtils.safeOutputString(a.findValue(field)).equals(NeoUtils.safeOutputString(b.findValue(field))))
			&& (NeoUtils.safeOutputString(b.findValue(field)).equals(NeoUtils.safeOutputString(a.findValue(field)))))
			{
				result = true;
			}else
			{
				gerarRegistroAlteracao(a, tituloCampo, "Alteracao", NeoUtils.safeOutputString(a.findValue(field)), NeoUtils.safeOutputString(b.findValue(field)), "Valor alterado no atual posto.");
			}
		}else if(a.findValue(field) instanceof BigDecimal)
		{
			BigDecimal var_a = (BigDecimal) a.findValue(field);
			BigDecimal var_b = (BigDecimal) b.findValue(field);
			if(var_a.compareTo(var_b) == 0 && var_b.compareTo(var_a) == 0)
			{
				result = true;
			}else
			{
				gerarRegistroAlteracao(a, tituloCampo, "Alteracao", NeoUtils.safeOutputString(a.findValue(field)), NeoUtils.safeOutputString(b.findValue(field)), "Valor alterado no atual posto.");
			}
		}else if(a.findValue(field) instanceof Boolean)
		{
			if(field.equals("houveAlteracaoPosto"))
			{
				/*
				 * para este campo, se algum campo do posto ja tiver sido validado
				 * e constatado alteração, será alterado para tru
				 * 
				 * logo esse campo deve ser ignorado na validação de alteração
				 */
				return true;
			}
			if((NeoUtils.safeBoolean(a.findValue(field)) && NeoUtils.safeBoolean(b.findValue(field)))
			|| (!NeoUtils.safeBoolean(a.findValue(field)) && !NeoUtils.safeBoolean(b.findValue(field))))
			{
				result = true;
			}else
			{
				String booA = NeoUtils.safeBoolean(a.findValue(field)) ? "Sim" : "Não";
				String booB = NeoUtils.safeBoolean(b.findValue(field)) ? "Sim" : "Não";
				gerarRegistroAlteracao(a, tituloCampo, "Alteracao", booA, booB, "Valor alterado no atual posto.");
			}
					
		}else if(a.findValue(field) instanceof Collection)
		{
			Collection<NeoObject> listaA = a.findField(field).getValues();
			Collection<NeoObject> listaB = a.findField(field).getValues();
			if(listaA.size() == 0 && listaB.size() == 0)
				return true;
			else
				System.out.println("");
			
		}else if(a.findValue(field) instanceof NeoObject || b.findValue(field) instanceof NeoObject)
		{
			NeoObject obj_a = null;
			NeoObject obj_b = null;
			if(a.findValue(field) != null)
			{
				obj_a = (NeoObject) a.findValue(field);
			}
			if(b.findValue(field) != null)
			{
				obj_b = (NeoObject) b.findValue(field);
			}
			/*if(field.equals("questionarioCentroCusto"))
			{
				
			}*/
			if(obj_a == null && obj_b == null)
				return true;
			
			if((obj_a == null && obj_b != null)
			|| (obj_a != null && obj_b == null))
			{
				if(obj_a != null)
				{
					EntityWrapper wA = new EntityWrapper(obj_a);
					Set<FieldInfo> camposTituloA = obj_a.getInfo().getTitleFields();
					String valorA = "";
					for(FieldInfo fiA : camposTituloA)
					{
						if(valorA.equals(""))
							valorA = NeoUtils.safeOutputString(wA.findValue(fiA.getName()));
						else
							valorA += " - " +NeoUtils.safeOutputString(wA.findValue(fiA.getName()));
					}
					gerarRegistroAlteracao(a, tituloCampo, "Inserção", "Vazio", valorA, "Valor inserido no atual posto - era vazio.");
					return false;
				}else if(obj_b != null)
				{
					EntityWrapper wB = new EntityWrapper(obj_b);
					Set<FieldInfo> camposTituloB = obj_b.getInfo().getTitleFields();
					String valorB = "";
					for(FieldInfo fiB : camposTituloB)
					{
						if(valorB.equals(""))
							valorB = NeoUtils.safeOutputString(wB.findValue(fiB.getName()));
						else
							valorB += " - " +NeoUtils.safeOutputString(wB.findValue(fiB.getName()));
					}
					gerarRegistroAlteracao(a, tituloCampo, "Remoção", valorB, "Vazio", "Valor removido no atual posto.");
					return false;
				}
			}	
			
			EntityWrapper wObjA = new EntityWrapper(obj_a);
			EntityWrapper wObjB = new EntityWrapper(obj_b);
			
			if(field.equals("enderecoPosto") || field.equals("enderecoEfetivo"))
			{
				String enderecoA = "";
				String enderecoB = "";
				if(!NeoUtils.safeOutputString(wObjA.findValue("cep")).equals(NeoUtils.safeOutputString(wObjB.findValue("cep"))))
				{
					enderecoA += "Cep = " + NeoUtils.safeOutputString(wObjA.findValue("cep")) + "<br>";
					enderecoB += "Cep = " + NeoUtils.safeOutputString(wObjB.findValue("cep")) + "<br>";
				}

				if(!NeoUtils.safeOutputString(wObjA.findValue("endereco")).equals(NeoUtils.safeOutputString(wObjB.findValue("endereco"))))
				{
					enderecoA += "Endereço = " + NeoUtils.safeOutputString(wObjA.findValue("endereco")) + "<br>";
					enderecoB += "Endereço = " + NeoUtils.safeOutputString(wObjB.findValue("endereco")) + "<br>";
				}
				
				if(!NeoUtils.safeOutputString(wObjA.findValue("numero")).equals(NeoUtils.safeOutputString(wObjB.findValue("numero"))))
				{
					enderecoA += "Número = " + NeoUtils.safeOutputString(wObjA.findValue("numero")) + "<br>";
					enderecoB += "Número = " + NeoUtils.safeOutputString(wObjB.findValue("numero")) + "<br>";
				}
				
				if(!NeoUtils.safeOutputString(wObjA.findValue("complemento")).equals(NeoUtils.safeOutputString(wObjB.findValue("complemento"))))
				{
					enderecoA += "Complemento = " + NeoUtils.safeOutputString(wObjA.findValue("complemento")) + "<br>";
					enderecoB += "Complemento = " + NeoUtils.safeOutputString(wObjB.findValue("complemento")) + "<br>";
				}
				
				if(!NeoUtils.safeOutputString(wObjA.findValue("bairro")).equals(NeoUtils.safeOutputString(wObjB.findValue("bairro"))))
				{
					enderecoA += "Bairro = " + NeoUtils.safeOutputString(wObjA.findValue("bairro")) + "<br>";
					enderecoB += "Bairro = " + NeoUtils.safeOutputString(wObjB.findValue("bairro")) + "<br>";
				}
				
				if(!NeoUtils.safeOutputString(wObjA.findValue("estadoCidade.estado.sigufs")).equals(NeoUtils.safeOutputString(wObjB.findValue("estadoCidade.estado.sigufs"))))
				{
					enderecoA += "Estado = " + NeoUtils.safeOutputString(wObjA.findValue("estadoCidade.estado.sigufs")) + "<br>";
					enderecoB += "Estado = " + NeoUtils.safeOutputString(wObjB.findValue("estadoCidade.estado.sigufs")) + "<br>";
				}
				
				if(!NeoUtils.safeOutputString(wObjA.findValue("estadoCidade.cidade.nomcid")).equals(NeoUtils.safeOutputString(wObjB.findValue("estadoCidade.cidade.nomcid"))))
				{
					enderecoA += "Cidade = " + NeoUtils.safeOutputString(wObjA.findValue("estadoCidade.cidade.nomcid")) + "<br>";
					enderecoB += "Cidade = " + NeoUtils.safeOutputString(wObjB.findValue("estadoCidade.cidade.nomcid")) + "<br>";
				}
				
				if(!enderecoA.equals(""))
				{
					gerarRegistroAlteracao(a, tituloCampo, "Alteração", enderecoA, enderecoB, "Valor alterado no atual posto.");
					return false;
				}else
					return true;
				
			}else if(field.equals("periodoFaturamento"))
			{
				String dataA = "";
				String dataB = "";
				if(wObjA.findValue("de") != null && wObjB.findValue("de") != null)
				{
					//de, ate e obs
					GregorianCalendar deA = (GregorianCalendar) wObjA.findValue("de");
					deA.set(Calendar.HOUR_OF_DAY, 00);
					deA.set(Calendar.MINUTE, 00);
					deA.set(Calendar.SECOND, 00);
					
					GregorianCalendar deB = (GregorianCalendar) wObjB.findValue("de");
					deB.set(Calendar.HOUR_OF_DAY, 00);
					deB.set(Calendar.MINUTE, 00);
					deB.set(Calendar.SECOND, 00);
					
					if(deA.compareTo(deB) != 0)
					{
						dataA += "Data Inicial = " + NeoCalendarUtils.dateToString(deA) + "<br>";
						dataB += "Data Inicial = " + NeoCalendarUtils.dateToString(deB) + "<br>";
					}
				}
				
				if(wObjA.findValue("ate") != null && wObjB.findValue("ate") != null)
				{
					GregorianCalendar ateA = (GregorianCalendar) wObjA.findValue("ate");
					ateA.set(Calendar.HOUR_OF_DAY, 00);
					ateA.set(Calendar.MINUTE, 00);
					ateA.set(Calendar.SECOND, 00);
					
					GregorianCalendar ateB = (GregorianCalendar) wObjB.findValue("ate");
					ateB.set(Calendar.HOUR_OF_DAY, 00);
					ateB.set(Calendar.MINUTE, 00);
					ateB.set(Calendar.SECOND, 00);
					
					if(ateA.compareTo(ateB) != 0)
					{
						dataA += "Data Final = " + NeoCalendarUtils.dateToString(ateA) + "<br>";
						dataB += "Data Final = " + NeoCalendarUtils.dateToString(ateB) + "<br>";
					}
				}
				
				if(!NeoUtils.safeOutputString(wObjA.findValue("obs")).equals(NeoUtils.safeOutputString(wObjB.findValue("obs"))))
				{
					dataA += "Observação = " + NeoUtils.safeOutputString(wObjA.findValue("obs")) + "<br>";
					dataB += "Observação = " + NeoUtils.safeOutputString(wObjB.findValue("obs")) + "<br>";
				}
				
				if(!dataA.equals(""))
				{
					gerarRegistroAlteracao(a, tituloCampo, "Alteração", dataA, dataB, "Valor alterado no atual posto.");
					return false;
				}else
					return true;
				
			}else if(field.equals("formacaoPreco"))
			{
				String valoresA = "";
				String valoresB = "";
				
				
				if( NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("valorMontanteA"), (BigDecimal)wObjB.findValue("valorMontanteA")) != 0 )
				{
					valoresA += "Montante A = " + NeoUtils.safeOutputString(wObjA.findValue("valorMontanteA")) + "<br>";
					valoresB += "Montante A = " + NeoUtils.safeOutputString(wObjB.findValue("valorMontanteA")) + "<br>";
				}
				if(NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("valorMontanteB"), (BigDecimal)wObjB.findValue("valorMontanteB")) != 0 )
				{
					valoresA += "Montante B = " + NeoUtils.safeOutputString(wObjA.findValue("valorMontanteB")) + "<br>";
					valoresB += "Montante B = " + NeoUtils.safeOutputString(wObjB.findValue("valorMontanteB")) + "<br>";
				}
				if(NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("valeAlimentacao4"), (BigDecimal)wObjB.findValue("valeAlimentacao4")) != 0 )
				{
					valoresA += "Vale Alimentação 4 hrs = " + NeoUtils.safeOutputString(wObjA.findValue("valeAlimentacao4")) + "<br>";
					valoresB += "Vale Alimentação 4 hrs = " + NeoUtils.safeOutputString(wObjB.findValue("valeAlimentacao4")) + "<br>";
				}
				if(NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("valeAlimentacao6"), (BigDecimal)wObjB.findValue("valeAlimentacao6")) != 0 )
				{
					valoresA += "Vale Alimentação 6 hrs = " + NeoUtils.safeOutputString(wObjA.findValue("valeAlimentacao6")) + "<br>";
					valoresB += "Vale Alimentação 6 hrs = " + NeoUtils.safeOutputString(wObjB.findValue("valeAlimentacao6")) + "<br>";
				}
				if(NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("valeAlimentacao8"), (BigDecimal)wObjB.findValue("valeAlimentacao8")) != 0 )
				{
					valoresA += "Vale Alimentação 8 hrs = " + NeoUtils.safeOutputString(wObjA.findValue("valeAlimentacao8")) + "<br>";
					valoresB += "Vale Alimentação 8 hrs = " + NeoUtils.safeOutputString(wObjB.findValue("valeAlimentacao8")) + "<br>";
				}
				if(NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("valeRefeicao"), (BigDecimal)wObjB.findValue("valeRefeicao")) != 0 )
				{
					valoresA += "Vale Refeição = " + NeoUtils.safeOutputString(wObjA.findValue("valeRefeicao")) + "<br>";
					valoresB += "Vale Refeição = " + NeoUtils.safeOutputString(wObjB.findValue("valeRefeicao")) + "<br>";
				}
				if(NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("valeTransporte"), (BigDecimal)wObjB.findValue("valeTransporte")) != 0 )
				{
					valoresA += "Vale Transporte = " + NeoUtils.safeOutputString(wObjA.findValue("valeTransporte")) + "<br>";
					valoresB += "Vale Transporte = " + NeoUtils.safeOutputString(wObjB.findValue("valeTransporte")) + "<br>";
				}
				if(NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("valorTributos"), (BigDecimal)wObjB.findValue("valorTributos")) != 0 )
				{
					valoresA += "Tributos = " + NeoUtils.safeOutputString(wObjA.findValue("valorTributos")) + "<br>";
					valoresB += "Tributos = " + NeoUtils.safeOutputString(wObjB.findValue("valorTributos")) + "<br>";
				}
				if(NeoUtils.safeCompareTo((BigDecimal)wObjA.findValue("vigilante"), (BigDecimal)wObjB.findValue("vigilante")) != 0 )
				{
					valoresA += "Vigilante = " + NeoUtils.safeOutputString(wObjA.findValue("vigilante")) + "<br>";
					valoresB += "Vigilante = " + NeoUtils.safeOutputString(wObjB.findValue("vigilante")) + "<br>";
				}
				
				if(!valoresA.equals(""))
				{
					gerarRegistroAlteracao(a, tituloCampo, "Alteração", valoresA, valoresB, "Valor alterado no atual posto.");
					return false;
				}else
					return true;
			}else if(field.equals("vigilanciaEletronica"))
			{
				BigDecimal oba =  (wObjA.findValue("valorSeguro") != null ? (BigDecimal)wObjA.findValue("valorSeguro") : new BigDecimal(0))  ;
				BigDecimal obb = (wObjB.findValue("valorSeguro") != null ? (BigDecimal)wObjB.findValue("valorSeguro") : new BigDecimal(0))  ;
				//if(((BigDecimal)wObjA.findValue("valorSeguro")).compareTo((BigDecimal)wObjB.findValue("valorSeguro")) != 0)
				if((oba).compareTo(obb) != 0)
				{
					gerarRegistroAlteracao(a, tituloCampo, "Alteração", NeoUtils.safeOutputString(wObjA.findValue("valorSeguro")), NeoUtils.safeOutputString(wObjA.findValue("valorSeguro")), "Valor alterado no atual posto.");
				}
				
				Collection<NeoObject> listaContatosA = wObjA.findField("listaContatosPosto").getValues();
				Collection<NeoObject> listaContatosB = wObjB.findField("listaContatosPosto").getValues();
				if(listaContatosA.size() != listaContatosB.size())
				{
					if(listaContatosA.size() > listaContatosB.size())
					{
						String qtdA = "Quantidade = " + listaContatosA.size();
						String qtdB = "Quantidade = " + listaContatosB.size();
						gerarRegistroAlteracao(a, tituloCampo, "Inclusão", qtdB, qtdA, "Contato inserido no atual posto.");
					}else
					{
						String qtdA = "Quantidade = " + listaContatosA.size();
						String qtdB = "Quantidade = " + listaContatosB.size();
						gerarRegistroAlteracao(a, tituloCampo, "Exclusão", qtdA, qtdB, "Contato removido no atual posto.");
					}
				}
				
				boolean achou = false;
				for(NeoObject contatoA : listaContatosA)
				{
					EntityWrapper wA = new EntityWrapper(contatoA);
					achou = false;
					for(NeoObject contatoB : listaContatosB)
					{
						EntityWrapper wB = new EntityWrapper(contatoB);
						if(NeoUtils.safeOutputString(wA.findValue("nomecontato")).equals(NeoUtils.safeOutputString(wB.findValue("nomecontato"))))
						{
							achou = true;
							String valorContatoA = "";
							String valorContatoB = "";
							if(!NeoUtils.safeOutputString(wA.findValue("telefone1")).equals(NeoUtils.safeOutputString(wB.findValue("telefone1"))))
							{
								valorContatoA += "Telefone 1 = " + NeoUtils.safeOutputString(wA.findValue("telefone1")) + "<br>";
								valorContatoB += "Telefone 1 = " + NeoUtils.safeOutputString(wB.findValue("telefone1")) + "<br>";
							}
							if(!NeoUtils.safeOutputString(wA.findValue("telefone2")).equals(NeoUtils.safeOutputString(wB.findValue("telefone2"))))
							{
								valorContatoA += "Telefone 2 = " + NeoUtils.safeOutputString(wA.findValue("telefone2")) + "<br>";
								valorContatoB += "Telefone 2 = " + NeoUtils.safeOutputString(wB.findValue("telefone2")) + "<br>";
							}
							if(!NeoUtils.safeOutputString(wA.findValue("funcao")).equals(NeoUtils.safeOutputString(wB.findValue("funcao"))))
							{
								valorContatoA += "Função = " + NeoUtils.safeOutputString(wA.findValue("funcao")) + "<br>";
								valorContatoB += "Função = " + NeoUtils.safeOutputString(wB.findValue("funcao")) + "<br>";
							}
							gerarRegistroAlteracao(a, tituloCampo, "Alteração", valorContatoB, valorContatoA, "Contato alterado no atual posto.");
							
						}
						if(achou)
							break;
					}
					//a referencia de A não foi encontrada em B
					if(!achou)
					{
						achou = true;
						String valorContatoA = NeoUtils.safeOutputString(wA.findValue("nomecontato")) + "<br>";
						valorContatoA += NeoUtils.safeOutputString(wA.findValue("telefone1")) + "<br>";
						valorContatoA += NeoUtils.safeOutputString(wA.findValue("telefone2")) + "<br>";
						valorContatoA += NeoUtils.safeOutputString(wA.findValue("funcao")) + "<br>";
						gerarRegistroAlteracao(a, tituloCampo, "Alteração", "", valorContatoA, "O contato foi inserido no novo posto, não foi encontrado referência no posto antigo (utilizando o nome).");
					}
				}
				
				if(achou)
					return false;
				else
					return true;
				
			}else if(field.equals("escala"))
			{
				//validar futuramente
			}else if(field.equals("equipamentosItens"))
			{
				Collection<NeoObject> listaCheckListA = wObjA.findField("equipamentosItens.itensChecklist").getValues();
				Collection<NeoObject> listaCheckListB = wObjB.findField("equipamentosItens.itensChecklist").getValues();
				//validar apenas o check list
				if(listaCheckListA.size() != listaCheckListB.size())
				{
					if(listaCheckListA.size() > listaCheckListB.size())
					{
						String qtdA = "Quantidade = " + listaCheckListA.size();
						String qtdB = "Quantidade = " + listaCheckListB.size();
						gerarRegistroAlteracao(a, tituloCampo, "Inclusão", qtdA, qtdB, "CheckList inserido no atual posto.");
					}else
					{
						String qtdA = "Quantidade = " + listaCheckListA.size();
						String qtdB = "Quantidade = " + listaCheckListB.size();
						gerarRegistroAlteracao(a, tituloCampo, "Exclusão", qtdA, qtdB, "CheckList removido no atual posto.");
					}
				}
				
				boolean achou = false;
				boolean erro = false;
				for(NeoObject checkListA : listaCheckListA)
				{
					EntityWrapper wA = new EntityWrapper(checkListA);
					achou = false;
					for(NeoObject checkListB : listaCheckListB)
					{
						EntityWrapper wB = new EntityWrapper(checkListB);
						if(NeoUtils.safeOutputString(wA.findValue("descricao")).equals(NeoUtils.safeOutputString(wB.findValue("descricao"))))
						{
							achou = true;
							String valorContatoA = "";
							String valorContatoB = "";
							if(!NeoUtils.safeOutputString(wA.findValue("seqchk")).equals(NeoUtils.safeOutputString(wB.findValue("seqchk"))))
							{
								valorContatoA += "Sequencial = " + NeoUtils.safeOutputString(wA.findValue("seqchk")) + "<br>";
								valorContatoB += "Sequencial 1 = " + NeoUtils.safeOutputString(wB.findValue("seqchk")) + "<br>";
							}
							if(!NeoUtils.safeOutputString(wA.findValue("itemChecklist.usu_coditm")).equals(NeoUtils.safeOutputString(wB.findValue("itemChecklist.usu_coditm"))))
							{
								valorContatoA += "Código do Item do Check List = " + NeoUtils.safeOutputString(wA.findValue("itemChecklist.usu_coditm")) + "<br>";
								valorContatoB += "Código do Item do Check List = " + NeoUtils.safeOutputString(wB.findValue("itemChecklist.usu_coditm")) + "<br>";
							}
							if(!NeoUtils.safeOutputString(wA.findValue("descricao")).equals(NeoUtils.safeOutputString(wB.findValue("descricao"))))
							{
								valorContatoA += "Descrição do Item = " + NeoUtils.safeOutputString(wA.findValue("descricao")) + "<br>";
								valorContatoB += "Descrição do Item = " + NeoUtils.safeOutputString(wB.findValue("descricao")) + "<br>";
							}
							if(!NeoUtils.safeOutputString(wA.findValue("quantidade")).equals(NeoUtils.safeOutputString(wB.findValue("quantidade"))))
							{
								valorContatoA += "Quantidade = " + NeoUtils.safeOutputString(wA.findValue("quantidade")) + "<br>";
								valorContatoB += "Quantidade = " + NeoUtils.safeOutputString(wB.findValue("quantidade")) + "<br>";
							}
							if(!NeoCalendarUtils.dateToString((GregorianCalendar)wA.findValue("prazo")).equals(NeoCalendarUtils.dateToString((GregorianCalendar)wB.findValue("prazo"))))
							{
								valorContatoA += "Prazo = " + NeoUtils.safeOutputString(wA.findValue("descricao")) + "<br>";
								valorContatoB += "Prazo = " + NeoUtils.safeOutputString(wB.findValue("descricao")) + "<br>";
							}
							if(!valorContatoA.equals(""))
							{
								erro = true;
								gerarRegistroAlteracao(a, tituloCampo, "Alteração", valorContatoA, valorContatoB, "Lista de Itens do Check List alterado no atual posto.");
							}
							
							
						}
						if(achou)
							break;
					}
					//a referencia de A não foi encontrada em B
					if(!achou)
					{
						erro = true;
						achou = true;
						String valorContatoA = NeoUtils.safeOutputString(wA.findValue("seqchk")) + "<br>";
						valorContatoA += NeoUtils.safeOutputString(wA.findValue("itemChecklist.usu_coditm")) + "<br>";
						valorContatoA += NeoUtils.safeOutputString(wA.findValue("descricao")) + "<br>";
						valorContatoA += NeoUtils.safeOutputString(wA.findValue("quantidade")) + "<br>";
						valorContatoA += NeoCalendarUtils.dateToString((GregorianCalendar)wA.findValue("prazo")) + "<br>";
						gerarRegistroAlteracao(a, tituloCampo, "Alteração", valorContatoA, "", "O check list foi inserido no novo posto, não foi encontrado referência no posto antigo (utilizando a descrição do item).");
					}
				}
				
				if(erro)
					return false;
				else
					return true;
			}/*else if(field.equals("arquivos"))
			{
				
			}*/
			else if(obj_b.getNeoId() == obj_a.getNeoId())
			{
				result = true;
			}else
				return false;
		}else
		{
			if(a.findValue(field) == null && b.findValue(field) == null)
			{
				return true;
			}else 
				System.out.println("");
		}
		
		return result;
	}
	
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
