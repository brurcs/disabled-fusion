package com.neomind.fusion.custom.orsegups.presenca.vo;

public class UsuT160JorVO {
	
	Long usu_horini;
	Long usu_horfim;
	
	Long usu_codemp;
	Long usu_numctr;
	Long usu_numpos;
	Long usu_codfil;
	Long usu_diasem;
	public Long getUsu_horini() {
		return usu_horini;
	}
	public void setUsu_horini(Long usu_horini) {
		this.usu_horini = usu_horini;
	}
	public Long getUsu_horfim() {
		return usu_horfim;
	}
	public void setUsu_horfim(Long usu_horfim) {
		this.usu_horfim = usu_horfim;
	}
	public Long getUsu_codemp() {
		return usu_codemp;
	}
	public void setUsu_codemp(Long usu_codemp) {
		this.usu_codemp = usu_codemp;
	}
	public Long getUsu_numctr() {
		return usu_numctr;
	}
	public void setUsu_numctr(Long usu_numctr) {
		this.usu_numctr = usu_numctr;
	}
	public Long getUsu_numpos() {
		return usu_numpos;
	}
	public void setUsu_numpos(Long usu_numpos) {
		this.usu_numpos = usu_numpos;
	}
	public Long getUsu_codfil() {
		return usu_codfil;
	}
	public void setUsu_codfil(Long usu_codfil) {
		this.usu_codfil = usu_codfil;
	}
	public Long getUsu_diasem() {
		return usu_diasem;
	}
	public void setUsu_diasem(Long usu_diasem) {
		this.usu_diasem = usu_diasem;
	}
	
	
	
	

}
