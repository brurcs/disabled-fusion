package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RMCIniciaTarefaSimples implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		NeoUser usuario = PortalUtil.getCurrentUser();
		NeoObject tarefa = (NeoObject) processEntity.findValue("tarefaSimples");
		
		OrsegupsUtils.iniciaProcessoTarefaSimples(tarefa, usuario);
				
		registrarAberturaTarefaSimples(tarefa);
				
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
	
	public void registrarAberturaTarefaSimples(NeoObject tarefa) {
	
		WFProcess wf = PersistEngine.getNeoObject(WFProcess.class, new QLEqualsFilter("entity", tarefa));
		
		EntityWrapper ew = new EntityWrapper(wf);
		
		Long codigoTarefa = Long.parseLong(ew.findGenericValue("code").toString());
		
		EntityWrapper ew2 = new EntityWrapper(tarefa);
		
		ew2.findField("codigoProcesso").setValue(codigoTarefa);
		PersistEngine.persist(tarefa);
		
	}
}