package com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.beans;

import java.util.List;

public class ACVehiclesImport {
    
    private List<ACVehicleImport> vehicles;

    public List<ACVehicleImport> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<ACVehicleImport> vehicles) {
        this.vehicles = vehicles;
    }
    
    
    
}
