package com.neomind.fusion.custom.orsegups.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.List;




import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.vo.ContratosVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceActionType;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.TaskLog;
import com.neomind.fusion.workflow.TaskLog.TaskLogType;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.AssignmentException;
import com.neomind.fusion.workflow.handler.HandlerFactory;
import com.neomind.fusion.workflow.handler.TaskHandler;
import com.neomind.fusion.workflow.task.central.search.TaskCentralIndex;
import com.neomind.fusion.workflow.task.rule.TaskRuleEngine;
import com.neomind.util.NeoUtils;

@WebServlet(name = "ContratrosScriptsServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.servlets.ContratrosScriptsServlet" })
public class ContratrosScriptsServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	public ContratrosScriptsServlet()
	{
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		String action = request.getParameter("action");


		
		if (action.equalsIgnoreCase("getListaContratos"))
		{
			PrintWriter out = response.getWriter();
			String contrato = request.getParameter("contrato");
			List<ContratosVO> lista = getListaContratos(contrato);
			Gson gson = new Gson();
			String retorno = gson.toJson(lista);
			out.print(retorno);
			out.flush();
			out.close();
			
			request.setAttribute("resposta", retorno);
		}
		
		if (action.equalsIgnoreCase("getStatusRMC"))
		{
			PrintWriter out = response.getWriter();
			String empresa = request.getParameter("empresa");
			String filial = request.getParameter("filial");
			String contrato = request.getParameter("contrato");
			ContratosVO contratosVO = new ContratosVO();
			contratosVO.setEmpresa(empresa);
			contratosVO.setFilial(filial);
			contratosVO.setContrato(contrato);
			List<ContratosVO> lista = getListaRMC(contratosVO);
			Gson gson = new Gson();
			String retorno = gson.toJson(lista);
			out.print(retorno);
			out.flush();
			out.close();
			
			request.setAttribute("resposta", retorno);
		}
		
		if (action.equalsIgnoreCase("getAlterarStatusRMC"))
		{
			PrintWriter out = response.getWriter();
			String empresa = request.getParameter("empresa");
			String filial = request.getParameter("filial");
			String contrato = request.getParameter("contrato");
			String numRmc = request.getParameter("numRmc");
			ContratosVO contratosVO = new ContratosVO();
			contratosVO.setEmpresa(empresa);
			contratosVO.setFilial(filial);
			contratosVO.setContrato(contrato);
			contratosVO.setNumRmc(numRmc);
			Gson gson = new Gson();
			String retorno = gson.toJson(getAlterarStatusRMC(contratosVO));
			out.print(retorno);
			out.flush();
			out.close();
			
			request.setAttribute("resposta", retorno);
		}


	}

	private List<ContratosVO> getListaContratos(String contrato)
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<ContratosVO> lista = new ArrayList();

		try
		{

			conn = PersistEngine.getConnection("SAPIENS");
			StringBuilder sql = new StringBuilder();
			sql.append("select usu_codemp,usu_codfil,usu_numctr from usu_t160ctr ");
			sql.append("where ((usu_sitctr= 'A') or (usu_sitctr = 'I' and usu_datfim >= getdate())) ");
			sql.append("and usu_numctr = ? ");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, contrato);
			rs = pstm.executeQuery();

			while (rs.next())
			{
				ContratosVO vo = new ContratosVO();
				vo.setEmpresa(rs.getString("usu_codEmp"));
				vo.setFilial(rs.getString("usu_codFil"));
				vo.setContrato(rs.getString("usu_numCtr"));
				lista.add(vo);
			   
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return lista;
	}
	
	private List<ContratosVO> getListaRMC(ContratosVO contratosVO)
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<ContratosVO> lista = new ArrayList();

		try
		{

			conn = PersistEngine.getConnection("SAPIENS");
			StringBuilder sql = new StringBuilder();
			sql.append("select usu_numrmc from usu_t160rmc ");
			sql.append("where usu_codemp = ? ");
			sql.append("and usu_codfil = ? ");
			sql.append("and usu_numctr = ?");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, contratosVO.getEmpresa());
			pstm.setString(2, contratosVO.getFilial());
			pstm.setString(3, contratosVO.getContrato());

			rs = pstm.executeQuery();

			while (rs.next())
			{
				ContratosVO vo = new ContratosVO();
				vo.setNumRmc(rs.getString("usu_numrmc"));
				
				lista.add(vo);
			   
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return lista;
	}
	
	private String getAlterarStatusRMC(ContratosVO contratosVO)
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String retorno = "";

		try
		{

			conn = PersistEngine.getConnection("SAPIENS");
			StringBuilder sql = new StringBuilder();
			sql.append("Update usu_t160rmc set usu_sitRmc = 'G' ");
			sql.append("where usu_codemp = ? ");
			sql.append("and usu_codfil = ? ");
			sql.append("and usu_numctr = ? ");
			sql.append("and usu_numRmc = ?");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, contratosVO.getEmpresa());
			pstm.setString(2, contratosVO.getFilial());
			pstm.setString(3, contratosVO.getContrato());
			pstm.setString(4, contratosVO.getNumRmc());

			pstm.executeUpdate();
			retorno = "ok";

		}
		catch (Exception e)
		{
			retorno = "erro";
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return retorno;
	}
}
