package com.neomind.fusion.custom.orsegups.juridico.relatorio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;

import com.neomind.fusion.custom.orsegups.juridico.relatorio.db.ConexaoUtils;
import com.neomind.fusion.custom.orsegups.juridico.relatorio.dto.EscritorioDTO;
import com.neomind.fusion.persist.PersistEngine;

public class EscritorioDAOImpl implements EscritorioDAO{
	
	@Override
	public List<EscritorioDTO> listaTodosEscResp() throws Exception {
		List<EscritorioDTO> retorno = new ArrayList<EscritorioDTO>();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder("select * from D_j002EscAdvPro");

		try {
			conn = PersistEngine.getConnection("");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next()) {
				EscritorioDTO escRec = new EscritorioDTO();
				escRec.setNome(WordUtils.capitalizeFully(rs.getString("nome")));
				escRec.setNeoId(rs.getLong("neoId"));

				retorno.add(escRec);

			}
			return retorno;
		} catch (Exception e) {
			return null;
		} finally {
			ConexaoUtils.closeConnection(conn, pstm, rs);
		}

	}
	
	
}
