package com.neomind.fusion.custom.orsegups.callcenter;

import java.util.ArrayList;
import java.util.List;

public class SigmaVO
{
	private String id;
	
	//DADOS CLIENTE
	private Integer codigoCliente;
	private Integer codigoEmpresa;
	private String razaoSocial;
	private String cnpjCpf;
	
	//DADOS CONTA/PARTICAO
	private String codigoCentral;
	private String particao;
	private String rota;
	private String logradouro;
	private String bairro;
	private String cidade;
	private String uf;
	private String cep;
	
	//Equipamento
	private String equipamento;
	
	//PALAVRA CHAVE
	private String pergunta;
	private String resposta;
	
	private boolean possuiOSAbertas;
	private boolean possuiOSPausada;
	
	//CONTATOS - TELEFONES/E-MAIL
	private String telefoneCentral1;
	private String telefoneCentral2;
	private String emailCentral;
	private String responsavelCentral;
	
	private List<ProvidenciaVO> providenciaVOs;
	private String senhaSiteSigma;
	
	//COMPLEMENTO EMPRESA
	
	private String complementoEmpresa;
	
	public String getComplementoEmpresa() {
	    return complementoEmpresa;
	}

	public void setComplementoEmpresa(String complementoEmpresa) {
	    this.complementoEmpresa = complementoEmpresa;
	}

	public Integer getCodigoCliente()
	{
		return codigoCliente;
	}

	public void setCodigoCliente(Integer codigoCliente)
	{
		this.codigoCliente = codigoCliente;
	}

	public Integer getCodigoEmpresa()
	{
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Integer codigoEmpresa)
	{
		this.codigoEmpresa = codigoEmpresa;
	}

	public String getCodigoCentral()
	{
		return codigoCentral;
	}

	public void setCodigoCentral(String codigoCentral)
	{
		this.codigoCentral = codigoCentral;
	}

	public String getParticao()
	{
		return particao;
	}

	public void setParticao(String particao)
	{
		this.particao = particao;
	}

	public String getRota()
	{
		return rota;
	}

	public void setRota(String rota)
	{
		this.rota = rota;
	}

	public String getRazaoSocial()
	{
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial)
	{
		this.razaoSocial = razaoSocial;
	}

	public String getCnpjCpf()
	{
		return cnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf)
	{
		this.cnpjCpf = cnpjCpf;
	}

	public String getLogradouro()
	{
		return logradouro;
	}

	public void setLogradouro(String logradouro)
	{
		this.logradouro = logradouro;
	}

	public String getBairro()
	{
		return bairro;
	}

	public void setBairro(String bairro)
	{
		this.bairro = bairro;
	}

	public String getCidade()
	{
		return cidade;
	}

	public void setCidade(String cidade)
	{
		this.cidade = cidade;
	}

	public String getUf()
	{
		return uf;
	}

	public void setUf(String uf)
	{
		this.uf = uf;
	}

	public String getCep()
	{
		return cep;
	}

	public void setCep(String cep)
	{
		this.cep = cep;
	}

	public String getTelefoneCentral1()
	{
		return telefoneCentral1;
	}

	public void setTelefoneCentral1(String telefoneCentral1)
	{
		this.telefoneCentral1 = telefoneCentral1;
	}

	public String getTelefoneCentral2()
	{
		return telefoneCentral2;
	}

	public void setTelefoneCentral2(String telefoneCentral2)
	{
		this.telefoneCentral2 = telefoneCentral2;
	}

	public String getEmail()
	{
		return emailCentral;
	}

	public void setEmail(String email)
	{
		this.emailCentral = email;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getPergunta()
	{
		return pergunta;
	}

	public void setPergunta(String pergunta)
	{
		this.pergunta = pergunta;
	}

	public String getResposta()
	{
		return resposta;
	}

	public void setResposta(String resposta)
	{
		this.resposta = resposta;
	}

	public String getEmailCentral()
	{
		return emailCentral;
	}

	public void setEmailCentral(String emailCentral)
	{
		this.emailCentral = emailCentral;
	}

	public String getResponsavelCentral()
	{
		return responsavelCentral;
	}

	public void setResponsavelCentral(String responsavelCentral)
	{
		this.responsavelCentral = responsavelCentral;
	}

	public List<ProvidenciaVO> getProvidenciaVOs()
	{
		return providenciaVOs;
	}

	public void setProvidenciaVOs(List<ProvidenciaVO> providenciaVOs)
	{
		this.providenciaVOs = providenciaVOs;
	}
	
	public void addProvidencia(ProvidenciaVO providenciaVO)
	{
		if(this.providenciaVOs == null)
		{
			this.providenciaVOs = new ArrayList<ProvidenciaVO>();
		}
		
		this.providenciaVOs.add(providenciaVO);
	}

	public boolean isPossuiOSAbertas()
	{
		return possuiOSAbertas;
	}

	public void setPossuiOSAbertas(boolean possuiOSAbertas)
	{
		this.possuiOSAbertas = possuiOSAbertas;
	}

	public String getEquipamento()
	{
		return equipamento;
	}

	public void setEquipamento(String equipamento)
	{
		this.equipamento = equipamento;
	}

	public boolean getPossuiOSPausada()
	{
		return possuiOSPausada;
	}

	public void setPossuiOSPausada(boolean possuiOSPausada)
	{
		this.possuiOSPausada = possuiOSPausada;
	}

	public String getSenhaSiteSigma() {
		return senhaSiteSigma;
	}

	public void setSenhaSiteSigma(String senhaSiteSigma) {
		this.senhaSiteSigma = senhaSiteSigma;
	}
}
