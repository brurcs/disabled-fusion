/**
 * ClientesGravarClientesInDadosGeraisClienteCadastroCEP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

public class ClientesGravarClientesInDadosGeraisClienteCadastroCEP  implements java.io.Serializable {
    private java.lang.String baiCid;

    private java.lang.String cepFim;

    private java.lang.String cepIni;

    private java.lang.String cepPol;

    private java.lang.Integer codRai;

    private java.lang.String endCid;

    private java.lang.String nomCid;

    public ClientesGravarClientesInDadosGeraisClienteCadastroCEP() {
    }

    public ClientesGravarClientesInDadosGeraisClienteCadastroCEP(
           java.lang.String baiCid,
           java.lang.String cepFim,
           java.lang.String cepIni,
           java.lang.String cepPol,
           java.lang.Integer codRai,
           java.lang.String endCid,
           java.lang.String nomCid) {
           this.baiCid = baiCid;
           this.cepFim = cepFim;
           this.cepIni = cepIni;
           this.cepPol = cepPol;
           this.codRai = codRai;
           this.endCid = endCid;
           this.nomCid = nomCid;
    }


    /**
     * Gets the baiCid value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @return baiCid
     */
    public java.lang.String getBaiCid() {
        return baiCid;
    }


    /**
     * Sets the baiCid value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @param baiCid
     */
    public void setBaiCid(java.lang.String baiCid) {
        this.baiCid = baiCid;
    }


    /**
     * Gets the cepFim value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @return cepFim
     */
    public java.lang.String getCepFim() {
        return cepFim;
    }


    /**
     * Sets the cepFim value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @param cepFim
     */
    public void setCepFim(java.lang.String cepFim) {
        this.cepFim = cepFim;
    }


    /**
     * Gets the cepIni value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @return cepIni
     */
    public java.lang.String getCepIni() {
        return cepIni;
    }


    /**
     * Sets the cepIni value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @param cepIni
     */
    public void setCepIni(java.lang.String cepIni) {
        this.cepIni = cepIni;
    }


    /**
     * Gets the cepPol value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @return cepPol
     */
    public java.lang.String getCepPol() {
        return cepPol;
    }


    /**
     * Sets the cepPol value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @param cepPol
     */
    public void setCepPol(java.lang.String cepPol) {
        this.cepPol = cepPol;
    }


    /**
     * Gets the codRai value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @return codRai
     */
    public java.lang.Integer getCodRai() {
        return codRai;
    }


    /**
     * Sets the codRai value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @param codRai
     */
    public void setCodRai(java.lang.Integer codRai) {
        this.codRai = codRai;
    }


    /**
     * Gets the endCid value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @return endCid
     */
    public java.lang.String getEndCid() {
        return endCid;
    }


    /**
     * Sets the endCid value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @param endCid
     */
    public void setEndCid(java.lang.String endCid) {
        this.endCid = endCid;
    }


    /**
     * Gets the nomCid value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @return nomCid
     */
    public java.lang.String getNomCid() {
        return nomCid;
    }


    /**
     * Sets the nomCid value for this ClientesGravarClientesInDadosGeraisClienteCadastroCEP.
     * 
     * @param nomCid
     */
    public void setNomCid(java.lang.String nomCid) {
        this.nomCid = nomCid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesGravarClientesInDadosGeraisClienteCadastroCEP)) return false;
        ClientesGravarClientesInDadosGeraisClienteCadastroCEP other = (ClientesGravarClientesInDadosGeraisClienteCadastroCEP) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baiCid==null && other.getBaiCid()==null) || 
             (this.baiCid!=null &&
              this.baiCid.equals(other.getBaiCid()))) &&
            ((this.cepFim==null && other.getCepFim()==null) || 
             (this.cepFim!=null &&
              this.cepFim.equals(other.getCepFim()))) &&
            ((this.cepIni==null && other.getCepIni()==null) || 
             (this.cepIni!=null &&
              this.cepIni.equals(other.getCepIni()))) &&
            ((this.cepPol==null && other.getCepPol()==null) || 
             (this.cepPol!=null &&
              this.cepPol.equals(other.getCepPol()))) &&
            ((this.codRai==null && other.getCodRai()==null) || 
             (this.codRai!=null &&
              this.codRai.equals(other.getCodRai()))) &&
            ((this.endCid==null && other.getEndCid()==null) || 
             (this.endCid!=null &&
              this.endCid.equals(other.getEndCid()))) &&
            ((this.nomCid==null && other.getNomCid()==null) || 
             (this.nomCid!=null &&
              this.nomCid.equals(other.getNomCid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaiCid() != null) {
            _hashCode += getBaiCid().hashCode();
        }
        if (getCepFim() != null) {
            _hashCode += getCepFim().hashCode();
        }
        if (getCepIni() != null) {
            _hashCode += getCepIni().hashCode();
        }
        if (getCepPol() != null) {
            _hashCode += getCepPol().hashCode();
        }
        if (getCodRai() != null) {
            _hashCode += getCodRai().hashCode();
        }
        if (getEndCid() != null) {
            _hashCode += getEndCid().hashCode();
        }
        if (getNomCid() != null) {
            _hashCode += getNomCid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesGravarClientesInDadosGeraisClienteCadastroCEP.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesInDadosGeraisClienteCadastroCEP"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiCid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiCid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepPol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepPol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
