package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.factory.FapFactory;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaRenovacaoCertificado implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {
	// TODO Auto-generated method stub

	List<NeoObject> certificados = PersistEngine.getObjects(AdapterUtils.getEntityClass("certificadosDigitaisOrsegups"));

	for(NeoObject certificado : certificados) {
	    try {
		
		EntityWrapper ew = new EntityWrapper(certificado);
		boolean ativo = ew.findGenericValue("ativo");
		String code = null;
		
		if(ativo){
		    Long empresa = ew.findGenericValue("empresa");
		    Long filial = ew.findGenericValue("filial");
		    String nome = ew.findGenericValue("nome");
		    String tipoCertificado = ew.findGenericValue("tipoCertificado");
		    String cpfCnpj = ew.findGenericValue("cpfCnpj");

		    GregorianCalendar vencimento = (GregorianCalendar) ew.findField("vencimento").getValue();
		    GregorianCalendar dataAtual = new GregorianCalendar();	
		    GregorianCalendar dataAuxiliar = (GregorianCalendar) dataAtual.clone();		  
		    dataAuxiliar.add(Calendar.DAY_OF_MONTH, 45);

		    boolean temNeogrid = ew.findGenericValue("neogrid");
		    boolean temSDE = ew.findGenericValue("sde");

		    boolean vencimentoProximo = vencimentoProximo(vencimento, dataAuxiliar);

		    if(vencimentoProximo){

			IniciarTarefaSimples tarefa = new IniciarTarefaSimples();
			String dataVencimentoDescricao = NeoDateUtils.safeDateFormat(vencimento,"dd/MM/yyyy");
			String solicitante = null;
			String executor = null;
			String titulo = null;
			String descricao = null;

			NeoPaper papelCoordenador = FapFactory.neoPaperFactory("code", "Coordenador de Sistemas"); 
			
			Set<NeoUser> usuarioCoordenador = papelCoordenador.getUsers();
			for(NeoUser coordenador : usuarioCoordenador){
			    solicitante = coordenador.getCode();
			    break;
			}
			
			GregorianCalendar prazo = new GregorianCalendar();
			prazo.add(Calendar.DAY_OF_MONTH, 10);
			while(!OrsegupsUtils.isWorkDay(prazo)){
			    prazo.add(Calendar.DAY_OF_MONTH, 1);
			}
			prazo.set(Calendar.HOUR, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);

			titulo = "Renovar Certificado Digital - " + empresa.toString() + "/" + filial.toString() + " - " + nome;

			descricao = "O certificado digital abaixo irá vencer no dia " + dataVencimentoDescricao + ". Favor providenciar a compra do mesmo.</br>";
			descricao += "Empresa: " + empresa.toString() + "/" + filial.toString() + "</br>";
			descricao += "CNPJ/CPF: " + cpfCnpj + "</br>";
			descricao += "Tipo do certificao: " + tipoCertificado + "</br>"; 			
			if(temNeogrid){
			    descricao += "Esta empresa envia notas utilizando o sistema Neogrid, é necessário efetuar o processo de recadastro " +
				    "junto ao fornecedor. Veja a documentação.</br>";
			}
			if(temSDE) {
			    descricao += "Esta empresa envia notas utilizando o sistema SDE, é necessário efetuar o processo de recadastro do "
				    + " certificado. Veja a documentação.</br>";
			}

			if(!possuiTarefaAberta(titulo)) {
				code = tarefa.abrirTarefa(solicitante, executor, titulo, descricao, "1", "nao", prazo);					
			}
			
			System.out.println(code);
		    }
		}
	    } catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		continue;
	    }
	}
    }

    private boolean possuiTarefaAberta(String titulo) {
		
    	StringBuilder sql = new StringBuilder();
    	Connection conn = null;
    	PreparedStatement pstm = null;
    	ResultSet rs = null;
    	
    	String tituloAux = titulo.replaceAll("Renovar Certificado Digital -", "");
    	
    	try {
    		
    		sql.append(" SELECT T.NEOID FROM D_TAREFA T ");
    		sql.append(" INNER JOIN WFPROCESS W ");
    		sql.append(" ON T.WFPROCESS_NEOID = W.NEOID ");
    		sql.append(" WHERE W.PROCESSSTATE = 0 ");
    		sql.append(" AND T.TITULO LIKE ?");

    		conn = PersistEngine.getConnection("");
    		
    		pstm = conn.prepareStatement(sql.toString());
    		
    		pstm.setString(1, titulo);
    		
    		rs = pstm.executeQuery();

    		if(rs.next()) {
    			return true;
    		}

    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.err.println(this.getClass().getSimpleName() + " - Erro ao verificar se existem tarefas abertas para o Certificado "+tituloAux);
    	}
    	
		return false;
	}

	private static boolean vencimentoProximo(Calendar dataVencimento, Calendar dataAuxiliar){
	boolean vencimentoProximo = false;

	System.out.println(dataVencimento.getTime().toString());
	System.out.println(dataAuxiliar.getTime().toString());

	if(dataVencimento.before(dataAuxiliar)){
	    vencimentoProximo = true;
	}

	return vencimentoProximo;
    }
}
