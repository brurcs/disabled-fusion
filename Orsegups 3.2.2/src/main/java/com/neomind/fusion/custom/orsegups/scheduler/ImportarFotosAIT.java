package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class ImportarFotosAIT implements CustomJobAdapter
{
	 List<String> listcolaboradorAIT = null;
	 List<String> listCaminho = null;
	@Override
	public void execute(CustomJobContext arg0)
	{
		try
		{
			buscaImagens();
			for (String ait : listcolaboradorAIT) {
				String temp = ait.replace(" (2)", "").replace(" (1)", "").replace(" (3)", "").replace(" - LGS", "").replace(" LGS", "").replace("AIT ", "")
							     .replace("Supervisor ", "").replace("Fiscal ", ""); 
				StringBuilder sql = new StringBuilder();
				sql.append(" select vi.NM_VIATURA, vi.cd_viatura from viatura vi where vi.NM_VIATURA like ('%"+temp+"%') "); 
				Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
				Collection<Object> resultList = query.getResultList();
				
				boolean passou = false;
				for(Object colaboradorAit : resultList) {
					Object[] result = (Object[]) colaboradorAit;
					String nome = (String)result[0];
					int matricula = (Integer)result[1];
					String mat = matricula+"";
					String filename = "C:/Users/francisco.junior/Documents/fotos/"+ait+".jpg";
					InstantiableEntityInfo colAit = AdapterUtils.getInstantiableEntityInfo("AITEmailAutomaticoAIT");
					NeoObject noInatUsu = colAit.createNewInstance();
					EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);
					File file = new File(filename);
					NeoFile newFile = new NeoFile();
					newFile.setName(file.getName());
					newFile.setStorage(NeoStorage.getDefault());
					PersistEngine.persist(newFile);
					PersistEngine.evict(newFile);
					OutputStream outi = newFile.getOutputStream();
					InputStream in = null;
					
					try
					{
						in = new BufferedInputStream(new FileInputStream(file));
						NeoStorage.copy(in, outi);
						newFile.setOrigin(NeoFile.Origin.TEMPLATE);
					}
					catch (FileNotFoundException e)
					{
						e.printStackTrace();
					}
					tmeWrapper.findField("matricula").setValue(mat);
					tmeWrapper.findField("nome").setValue(nome);
					tmeWrapper.findField("fotoTec").setValue(newFile);
					PersistEngine.persist(noInatUsu);
					//file.delete();
					passou = true;
					//break;
				}
				if (!passou) {
					System.out.println(ait.replace(" (2)", "").replace(" (1)", "").replace(" (3)", "").replace(" - LGS", "").replace(" LGS", "").replace("AIT ", ""));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void buscaImagens () {
		 File f = null;
	      File[] paths;
	      
	      try{      
	         // create new file
	         f = new File("C:/Users/francisco.junior/Documents/fotos");
	         
	         // create new filename filter
	         FilenameFilter fileNameFilter = new FilenameFilter() {
	   
	            @Override
	            public boolean accept(File dir, String name) {
	               if(name.lastIndexOf('.')>0)
	               {
	                  // get last index for '.' char
	                  int lastIndex = name.lastIndexOf('.');
	                  
	                  // get extension
	                  String str = name.substring(lastIndex);
	                  
	                  // match path name extension
	                  if(str.equals(".JPG"))
	                  {
	                     return true;
	                  }
	               }
	               return false;
	            }
	         };
	         // returns pathnames for files and directory
	         paths = f.listFiles(fileNameFilter);
	         
	         listCaminho = new ArrayList<String>();
	         listcolaboradorAIT = new ArrayList<String>();
	         for(File path:paths)
	         {
	        	String colaboradorAIT = path.toString();
	        	listCaminho.add(colaboradorAIT);
	        	
	            colaboradorAIT = colaboradorAIT.substring(42, colaboradorAIT.length() - 4);
	            listcolaboradorAIT.add(colaboradorAIT);
	         }
	      }catch(Exception e){
	         // if any error occurs
	         e.printStackTrace();
	      }
	}

}

