package com.neomind.fusion.custom.orsegups.rsc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.rsc.vo.ServicosVO;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RSCUtils
{
	public static GregorianCalendar addMinutosPrazoRSC(GregorianCalendar date, int qtdPrazo)
	{
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.setTime(date.getTime());
		prazo.add(GregorianCalendar.MINUTE, qtdPrazo);

		return prazo;
	}

	public static GregorianCalendar addHorasPrazoRSC(GregorianCalendar date, int qtdPrazo)
	{
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.setTime(date.getTime());
		prazo.add(GregorianCalendar.HOUR_OF_DAY, qtdPrazo);

		return prazo;
	}

	public static GregorianCalendar addDiasPrazoRSC(GregorianCalendar date, int qtdPrazo, boolean diaUtil)
	{
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.setTime(date.getTime());

		if (diaUtil)
		{
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, Long.parseLong(String.valueOf(qtdPrazo)));
		}
		else
		{
			prazo.add(GregorianCalendar.DATE, qtdPrazo);
			prazo = OrsegupsUtils.getNextWorkDay(prazo);
		}

		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		return prazo;
	}

	public static GregorianCalendar atualizaHoraParaFinalDoDia(GregorianCalendar prazo)
	{
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		return prazo;
	}

	public static GregorianCalendar retornaPrazoDeadLine(String atividade, GregorianCalendar date)
	{
		GregorianCalendar prazoDeadLine = new GregorianCalendar();

		Long prazo = 0L;
		Long tipoPrazo = 0L;
		Boolean consideraDiaUtil = false;

		QLEqualsFilter ftrAtividade = new QLEqualsFilter("atividade", atividade);
		List<NeoObject> parametrizacoesDeadLine = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCParametrizacoesPrazoAtividade"), ftrAtividade);
		if (parametrizacoesDeadLine != null && !parametrizacoesDeadLine.isEmpty())
		{
			EntityWrapper wrpParametrizacoesDeadLine = new EntityWrapper(parametrizacoesDeadLine.get(0));
			prazo = (Long) wrpParametrizacoesDeadLine.findValue("prazo");
			tipoPrazo = (Long) wrpParametrizacoesDeadLine.findValue("prazoParametrizacoes.tipoPrazo.codigoTipo");
			consideraDiaUtil = (Boolean) wrpParametrizacoesDeadLine.findValue("consideraDiaUtil");
		}

		switch (Integer.parseInt(tipoPrazo.toString()))
		{
			case 1:
				prazoDeadLine = addMinutosPrazoRSC(date, Integer.parseInt(prazo.toString()));
				break;
			case 2:
				prazoDeadLine = addHorasPrazoRSC(date, Integer.parseInt(prazo.toString()));
				break;
			case 3:
					if (atividade.contains("Diretor") || atividade.contains("Presidente"))
				{
					prazo = prazo + 1L;
				}
				prazoDeadLine = addDiasPrazoRSC(date, Integer.parseInt(prazo.toString()), consideraDiaUtil);
				break;
		}

		return prazoDeadLine;
	}

	public static NeoObject getUltimoData(List<NeoObject> objHistoricoOcorrencia){
		NeoObject noLast = objHistoricoOcorrencia.get(0);
		EntityWrapper ewUltimaBase = new EntityWrapper(noLast);
		GregorianCalendar dataBase = (GregorianCalendar) ewUltimaBase.findValue("data");
		java.sql.Time horaBase = (java.sql.Time) ewUltimaBase.findValue("hora");
		for(NeoObject aux : objHistoricoOcorrencia) {
			EntityWrapper ew = new EntityWrapper(aux);
			GregorianCalendar data = (GregorianCalendar) ew.findValue("data");
			java.sql.Time hora = (java.sql.Time) ew.findValue("hora");
			int comparison;
			comparison = dataBase.compareTo(data);
			if (comparison == 1 ) {
				//dataBase é maior que data
			}else if(comparison == -1) {
				//data é maior que dataBase
				noLast = aux;
				dataBase = data;
			}else{
				if(horaBase.getTime() < hora.getTime()) {
					noLast = aux;
					dataBase = data;
					horaBase = hora; 
				}
			}
			
		}
		return noLast;
	}
	
	public static String retornaDescricaoVerificacaoEficacia(List<NeoObject> objHistoricoOcorrencia)
	{
		String descricao = "";

		if (NeoUtils.safeIsNotNull(objHistoricoOcorrencia))
		{
			NeoObject ultima = getUltimoData(objHistoricoOcorrencia);
			EntityWrapper ewUltima = new EntityWrapper(ultima);
			descricao = "<b>Resultado da Verificação:</b> " + ewUltima.findValue("grauSatisfacao.descricao") + "<br>";
			descricao = descricao + "<b>Justificativa:</b> " + ewUltima.findValue("ocorrencia");
		}
		return descricao;
	}

	public static Long retornaStatusEficacia(List<NeoObject> objHistoricoOcorrencia)
	{
		Long status = 0L;

		if (NeoUtils.safeIsNotNull(objHistoricoOcorrencia))
		{
			NeoObject ultima = getUltimoData(objHistoricoOcorrencia);
			EntityWrapper ewUltima = new EntityWrapper(ultima);

			status = (Long) ewUltima.findValue("grauSatisfacao.codigo");
		}

		return status;
	}

	public static NeoPaper retornaSuperiorResponsavelExecutor(NeoUser executorAtual)
	{
		OrsegupsUtils paper = new OrsegupsUtils();
		NeoPaper papelSuperior = new NeoPaper();

		if (executorAtual.getPapers().contains(executorAtual.getGroup().getResponsible()))
		{
			papelSuperior = executorAtual.getGroup().getUpperLevel().getResponsible();
		}
		else
		{
			papelSuperior = paper.getPaper(executorAtual.getGroup().getResponsible().getCode());
		}

		return papelSuperior;
	}

	public static void salvaEtapaVerificacao(EntityWrapper wrpEficacia, Long proximaEtapa)
	{
		String nomeCampo = "";
		String nomeCampoEtapa = "";

		if (NeoUtils.safeIsNotNull(wrpEficacia.findValue("RscCategoriaAlteracaoBoletoNF")))
		{
			nomeCampo = "RscCategoriaAlteracaoBoletoNF";
			nomeCampoEtapa = "etapaVerificacaoEficacia";
		}
		if (NeoUtils.safeIsNotNull(wrpEficacia.findValue("RscCategoriaAtualizacaoCadastral")))
		{
			nomeCampo = "RscCategoriaAtualizacaoCadastral";
			nomeCampoEtapa = "etapaVerificacaoEficacia";
		}
		if (NeoUtils.safeIsNotNull(wrpEficacia.findValue("RscCategoriaCancelamento")))
		{
			nomeCampo = "RscCategoriaCancelamento";
			nomeCampoEtapa = "etapaVerificacaoEficacia";
		}
		if (NeoUtils.safeIsNotNull(wrpEficacia.findValue("RscCategoriaDiversos")))
		{
			nomeCampo = "RscCategoriaDiversos";
			nomeCampoEtapa = "etapaVerificacaoEficacia";
		}
		if (NeoUtils.safeIsNotNull(wrpEficacia.findValue("RscCategoriaOrcamento")))
		{
			nomeCampo = "RscCategoriaOrcamento";
			nomeCampoEtapa = "etapaVerificacaoEficacia";
		}
		if (NeoUtils.safeIsNotNull(wrpEficacia.findValue("RscCategoriaOrcamentoVisita")))
		{
			nomeCampo = "RscCategoriaOrcamentoVisita";
			nomeCampoEtapa = "etapaVerificacaoEficaciaVisita";
		}
		
		if (NeoUtils.safeIsNotNull(wrpEficacia.findValue("RscCategoriaCancelamentoNovo"))) 
		{
			nomeCampo = "RscCategoriaCancelamentoNovo";
			nomeCampoEtapa = "etapaVerificacaoEficacia";
		}

		NeoObject rscVerificaoEficacia = (NeoObject) wrpEficacia.findValue(nomeCampo);
		EntityWrapper verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
		verEfiWrapper.setValue(nomeCampoEtapa, proximaEtapa);
	}

	public GregorianCalendar retornaPrazoTarefaEficacia(EntityWrapper entityWrapper)
	{
		GregorianCalendar prazoDeadLine = new GregorianCalendar();

		if (((Long) entityWrapper.findValue("etapaVerificacaoEficacia")) == 1L)
		{
			prazoDeadLine = retornaPrazoDeadLine("1ª Verificação Eficácia - Sol. Cliente", (GregorianCalendar) prazoDeadLine);
		}
		else if (((Long) entityWrapper.findValue("etapaVerificacaoEficacia")) == 2L)
		{
			prazoDeadLine = retornaPrazoDeadLine("2ª Verificação Eficácia - Sol. Cliente", (GregorianCalendar) prazoDeadLine);
		}
		else if (((Long) entityWrapper.findValue("etapaVerificacaoEficacia")) == 3L)
		{
			prazoDeadLine = retornaPrazoDeadLine("3ª Verificação Eficácia - Sol. Cliente", (GregorianCalendar) prazoDeadLine);
		}
		else if (((Long) entityWrapper.findValue("etapaVerificacaoEficacia")) == 4L)
		{
			prazoDeadLine = retornaPrazoDeadLine("4ª Verificação Eficácia - Sol. Cliente", (GregorianCalendar) prazoDeadLine);
		}

		return prazoDeadLine;
	}

	public static void gravarApontamento(Long codEmp, Long codFil, Long numCtr, Long numPos, Long seqMov, GregorianCalendar datSisHorarioZerado, String codSer, String cplCvs, BigDecimal preUni, BigDecimal perIss, BigDecimal perIrf, BigDecimal perIns, BigDecimal perPit, BigDecimal perCsl, BigDecimal perCrt, BigDecimal perOur, Long ctaFin, Long ctaRed, String codCcu, GregorianCalendar datInn, GregorianCalendar dat1900, String tnsSer, GregorianCalendar datSis, GregorianCalendar dataCompetencia,
			String obsCms, String descAcreBonificacao)
	{
		Connection conn = null;
		PreparedStatement pstm = null;

		try
		{
		    datSisHorarioZerado.set(Calendar.HOUR_OF_DAY, 0);
		    datSisHorarioZerado.set(Calendar.MINUTE, 0);
		    datSisHorarioZerado.set(Calendar.SECOND, 0);
		    datSisHorarioZerado.set(Calendar.MILLISECOND, 0);
			
			conn = PersistEngine.getConnection("SAPIENS");
			StringBuffer sqlApontamento = new StringBuffer();

			sqlApontamento.append(" INSERT INTO USU_T160CMS VALUES (?,?,?,?,?,?,?,?,'UN',0,1,?,?,?,?,?,?,?,?,?,?,?,?,?,?,100,?,?,0,'',0,?,?,?,30,'N','S','M',0,?,'N') ");

			pstm = conn.prepareStatement(sqlApontamento.toString());

			pstm.setLong(1, codEmp);
			pstm.setLong(2, codFil);
			pstm.setLong(3, numCtr);
			pstm.setLong(4, numPos);
			pstm.setLong(5, seqMov);
			pstm.setTimestamp(6, new Timestamp(datSisHorarioZerado.getTimeInMillis()));
			pstm.setString(7, codSer);
			pstm.setString(8, cplCvs);
			pstm.setBigDecimal(9, preUni);
			pstm.setBigDecimal(10, perIss);
			pstm.setBigDecimal(11, perIrf);
			pstm.setBigDecimal(12, perIns);
			pstm.setBigDecimal(13, perPit);
			pstm.setBigDecimal(14, perCsl);
			pstm.setBigDecimal(15, perCrt);
			pstm.setBigDecimal(16, perOur);
			pstm.setLong(17, ctaFin);
			pstm.setLong(18, ctaRed);
			pstm.setString(19, codCcu);
			pstm.setTimestamp(20, new Timestamp(datInn.getTimeInMillis()));
			pstm.setTimestamp(21, new Timestamp(dat1900.getTimeInMillis()));
			pstm.setString(22, tnsSer);
			pstm.setTimestamp(23, new Timestamp(datSisHorarioZerado.getTimeInMillis()));
			pstm.setLong(24, datSis.get(Calendar.HOUR_OF_DAY) * 60 + datSis.get(Calendar.MINUTE));
			pstm.setTimestamp(25, new Timestamp(dataCompetencia.getTimeInMillis()));
			pstm.setString(26, descAcreBonificacao);
			pstm.setString(27, obsCms);
			pstm.setTimestamp(28, new Timestamp(dat1900.getTimeInMillis()));

			pstm.execute();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}
	}

	public static NeoPaper getPapelGerenteRegional(Long codreg, String codFam)
	{

		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				nomePapel = "Gerente Comercial Mercado Privado";
				break;
			case 1:

				if (codFam.equalsIgnoreCase("SER101"))
				{

					nomePapel = "Gerente de Segurança";
				}
				else
				{
					nomePapel = "Gerente de Vigilância Eletrônica";
				}
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				nomePapel = "Gerente Escritório Regional Curitiba";
				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba Sul";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;	

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}

	public String retornaNomeAtividadePrazo(Long codigo)
	{
		String nomeAtividade = "";

		switch (codigo.intValue())
		{
			case 1:
				nomeAtividade = "Atender Solicitação";
				break;
			case 2:
				nomeAtividade = "Atender Solicitação";
				break;
			case 3:
				nomeAtividade = "Agendar Visita - Executivo";
				break;
			case 5:
				nomeAtividade = "Tentar Reversão";
				break;
			case 6:
				nomeAtividade = "Atender Solicitação";
				break;
		}

		return nomeAtividade;

	}

	public static List<ServicosVO> contratoServico(Long codEmp, Long codFil, Long codCtr)
	{

		Integer empresa = Integer.valueOf(codEmp.toString());
		Integer filial = Integer.valueOf(codFil.toString());
		Integer contrato = Integer.valueOf(codCtr.toString());

		ArrayList<ServicosVO> listaServico = new ArrayList<ServicosVO>();

		PreparedStatement st = null;
		ResultSet rs = null;
		Connection conn = null;
		String nomeFonteDados = "SAPIENS";
		StringBuffer sql = new StringBuffer();

		sql.append("SELECT s.CodFam, P.Usu_CodSer ");
		sql.append("	FROM USU_T160CVS P ");
		sql.append("	JOIN USU_T160CTR C ON P.USU_NUMCTR = C.USU_NUMCTR AND P.USU_CODEMP = C.USU_CODEMP AND P.USU_CODFIL = C.USU_CODFIL ");
		sql.append("	JOIN E080SER S ON P.USU_CODSER=S.CODSER AND S.CODEMP=P.USU_CODEMP ");
		sql.append("	WHERE ((P.USU_SITCVS = 'A') OR (P.USU_SITCVS = 'I' AND P.USU_DATFIM >= GETDATE())) AND ");
		sql.append("	((C.USU_SITCTR = 'A') OR (C.USU_SITCTR = 'I' AND C.USU_DATFIM >= GETDATE())) AND ");
		sql.append("	c.usu_codemp = ? and ");
		sql.append("	c.usu_codfil = ? and ");
		sql.append("	C.USU_NUMCTR = ?  ");
		sql.append("	order by s.CodFam ");

		try
		{
			conn = PersistEngine.getConnection(nomeFonteDados);
			st = conn.prepareStatement(sql.toString());
			st.setLong(1, empresa);
			st.setLong(2, filial);
			st.setLong(3, contrato);
			rs = st.executeQuery();

			while (rs.next())
			{
				ServicosVO servicosVO = new ServicosVO();
				servicosVO.setCodigoFamilia(rs.getString("CodFam"));
				servicosVO.setDescricaoServico(rs.getString("Usu_CodSer"));
				listaServico.add(servicosVO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

		return listaServico;
	}
	
	public static NeoPaper getPapelRespAnexarCartaCancelamento(Long codreg, String codFam, String codSer)
	{

		String nomePapel = "";
		//Solicitado via tarefa simples 1046549 para remover a tratativa da tarefa simples 982428 para fixar o executor das tarefas de rastreamento
//		if ((codSer.equalsIgnoreCase("9002046")) || (codSer.equalsIgnoreCase("9002003")) || (codSer.equalsIgnoreCase("9002039")))
//		{
//
//			nomePapel = "RSCAnexarCartaCancelamentoRastreamento";
//		}
//		else
//		{

			switch (codreg.intValue())
			{
				case 0:
					nomePapel = "RSCAnexarCartaCancelamentoSDE";
					break;
				case 1:

					if (codFam.equalsIgnoreCase("SER101")){ 

						nomePapel = "RSCAnexarCartaCancelamentoSOOHUM";
					}
					else if(codFam.equalsIgnoreCase("SER103")){
					    
						nomePapel = "RSCAnexarCartaCancelamentoSOOELE";
					}
					else {
					    	nomePapel = "RSCAnexarCartaCancelamentoSOO";
					}
					break;
				case 2:
					nomePapel = "RSCAnexarCartaCancelamentoIAIELE";
					break;
				case 3:
					nomePapel = "RSCAnexarCartaCancelamentoBQEELE";
					break;
				case 4:
					nomePapel = "RSCAnexarCartaCancelamentoBNUELE";
					break;
				case 5:
					nomePapel = "RSCAnexarCartaCancelamentoJLEELE";
					break;
				case 6:
					nomePapel = "RSCAnexarCartaCancelamentoLGSELE";
					break;
				case 7:
					nomePapel = "RSCAnexarCartaCancelamentoCUAELE";
					break;
				case 8:
					nomePapel = "RSCAnexarCartaCancelamentoGPRELE";
					break;
				case 9:
					nomePapel = "RSCAnexarCartaCancelamentoACL";
					break;
				case 10:
					nomePapel = "RSCAnexarCartaCancelamentoCCOELE";
					break;
				case 11:
					nomePapel = "RSCAnexarCartaCancelamentoRSLELE";
					break;
				case 12:
					nomePapel = "RSCAnexarCartaCancelamentoJGSELE";
					break;
				case 13:
					if ((codFam.equalsIgnoreCase("SER101")) || (codFam.equalsIgnoreCase("SER103")))
					{
						nomePapel = "RSCAnexarCartaCancelamentoCTAHUM";
					}
					else
					{
						nomePapel = "RSCAnexarCartaCancelamentoCTAELE";
					}

					break;
				case 14:
					nomePapel = "RSCAnexarCartaCancelamentoCSCELE";
					break;
				case 15:
					if ((codFam.equalsIgnoreCase("SER101")) || (codFam.equalsIgnoreCase("SER103")))
					{
						nomePapel = "RSCAnexarCartaCancelamentoCTAHUM";
					}
					else
					{
						nomePapel = "RSCAnexarCartaCancelamentoCTAELE";
					}
					break;
				case 16:
					nomePapel = "RSCAnexarCartaCancelamentoTROELE";
					break;
					
				case 17:
					nomePapel = "RSCAnexarCartaCancelamentoNHO";
					break;
				case 18:
					nomePapel = "RSCAnexarCartaCancelamentoTRI";
					break;
				case 19:
					nomePapel = "RSCAnexarCartaCancelamentoCAS";
					break;
				case 20:
					nomePapel = "RSCAnexarCartaCancelamentoGNO";
					break;
				case 21:
					nomePapel = "RSCAnexarCartaCancelamentoPMJ";
					break;
				case 22:
					nomePapel = "RSCAnexarCartaCancelamentoSRR";
					break;
				case 23:
					nomePapel = "RSCAnexarCartaCancelamentoXLN";
					break;	

			}
//		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}
	
	public void enviaEmailRegistroRSC(RSCVO rsc)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
				
	    paramMap.put("tarefa", rsc.getTarefa());
		paramMap.put("nomePes", rsc.getNome());
		paramMap.put("email", rsc.getEmail());
		paramMap.put("mensagem", rsc.getMensagem());
				
		FusionRuntime.getInstance().getMailEngine().sendEMail(rsc.getEmail(), "/portal_orsegups/emailAberturaRSC.jsp", paramMap);
	}
	
	public void enviarEmailRSCOrcamentoExecutivo(RSCVO rsc, String nomCliSap, String nomCliFus)
	{
		String cliente = "";
		if(NeoUtils.safeIsNotNull(nomCliSap)) {
			cliente = nomCliSap;
		} else {
			cliente = nomCliFus;
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		StringBuilder noUserMsg = new StringBuilder();
			
		noUserMsg.append("<strong>Cliente:</strong> " + cliente + "<br>");
		noUserMsg.append("<strong>Contato:</strong> " + rsc.getNome() + "<br>");
		noUserMsg.append("<strong>Telefone:</strong> " + rsc.getTelefone() + "<br>");
		noUserMsg.append("<strong>Email:</strong> " + rsc.getEmail() + "<br>");
		noUserMsg.append("<strong>Mensagem:</strong> " + rsc.getMensagem() + "<br>");
		
		paramMap.put("tarefa", rsc.getTarefa());
		paramMap.put("mensagem", noUserMsg.toString());
			
		FusionRuntime.getInstance().getMailEngine().sendEMail(rsc.getResponsavelAtendimento().getEmail(), "/portal_orsegups/emailExecutivoRSC.jsp", paramMap);
	}
	
	public void registrarTarefaSimples(EntityWrapper wrpRSC, Activity activity)
	{ 
		NeoObject oTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper wrpTarefaSimples = new EntityWrapper(oTarefaSimples);
		
		String titulo = activity.getCode() + " - " + wrpRSC.findValue("processoRscMae");
		if(((String) wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.nomcli")) != null && !((String) wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.nomcli")).isEmpty())
		{
			titulo = titulo + " - Cliente: " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.codcli") + " - " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.nomcli");
			titulo = titulo + " - " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.cgccpf");
		}
		else if(((String) wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteOrcamento")) != null && !((String) wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteOrcamento")).isEmpty())
		{
			titulo = titulo + " - Cliente: " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteOrcamento");
		}
		
		String descricao = "<strong>Categoria da RSC:</strong> " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.tipoSolicitacao.descricao") + "<br>";
		       descricao = descricao + "<strong>Processo Principal:</strong> " + wrpRSC.findValue("processoRscMae") + "<br>";
		       descricao = descricao + "<strong>Processo Filho:</strong> " + activity.getCode() + "<br>";
		       descricao = descricao + "<strong>Solicitação Inicial:</strong> " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.descricao") + "<br>";
			
		wrpTarefaSimples.setValue("Titulo", titulo);		
		wrpTarefaSimples.setValue("DescricaoSolicitacao", NeoUtils.encodeHTMLValue(descricao));
		
		wrpRSC.setValue("tarefaSimples", oTarefaSimples);	
	}
	
	public void registrarTarefaSimplesRSCPrincipal(EntityWrapper wrpRSC, Activity activity)
	{ 
		NeoObject oTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper wrpTarefaSimples = new EntityWrapper(oTarefaSimples);
		
		String titulo = activity.getCode();
		if(((String) wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.nomcli")) != null && !((String) wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.nomcli")).isEmpty())
		{
			titulo = titulo + " - Cliente: " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.codcli") + " - " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.nomcli");
			titulo = titulo + " - " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.cgccpf");
		}
		else if(((String) wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteOrcamento")) != null && !((String) wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteOrcamento")).isEmpty())
		{
			titulo = titulo + " - Cliente: " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.clienteOrcamento");
		}
		
		String descricao = "<strong>Categoria da RSC:</strong> " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.tipoSolicitacao.descricao") + "<br>";
		       descricao = descricao + "<strong>Processo:</strong> " + activity.getCode() + "<br>";
		       descricao = descricao + "<strong>Solicitação Inicial:</strong> " + wrpRSC.findValue("RscRelatorioSolicitacaoCliente.descricao") + "<br>";
			
		wrpTarefaSimples.setValue("Titulo", titulo);		
		wrpTarefaSimples.setValue("DescricaoSolicitacao", NeoUtils.encodeHTMLValue(descricao));
		
		wrpRSC.setValue("tarefaSimples", oTarefaSimples);	
	}
	
	public String recuperaFamilia(List<ServicosVO> lista)
	{

		String codFam = "";
		for (ServicosVO servicosVO : lista)
		{
			codFam = servicosVO.getCodigoFamilia();
			if ((codFam.equals("SER101")) || (codFam.equals("SER103")))
			{

				codFam = servicosVO.getCodigoFamilia();
				break;
			}
		}

		return codFam;
	}

	public String recuperaServico(List<ServicosVO> lista)
	{

		String codSer = "";
		for (ServicosVO servicosVO : lista)
		{

			codSer = servicosVO.getDescricaoServico();

		}

		return codSer;
	}
}
