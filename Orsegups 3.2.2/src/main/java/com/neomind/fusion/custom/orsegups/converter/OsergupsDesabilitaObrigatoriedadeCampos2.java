package com.neomind.fusion.custom.orsegups.converter;

import java.util.HashMap;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.portal.FreeMarkerUtils;

public class OsergupsDesabilitaObrigatoriedadeCampos2 extends StringConverter {

	@Override
	public String getHTMLInput(final EFormField field, final OriginEnum origin)
	{
		HashMap<String, Object> map = new HashMap<String, Object>();
		String customHTML = FreeMarkerUtils.parseTemplate(map, "custom/orsegups/searchListaObrigatoriedade.html.ftl");
		return super.getHTMLInput(field, origin) + customHTML;
	}
}

