package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaAtualizaOSparaCaronaChp1_4 implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaEventosComDeslocamento.class);

    @Override
    public void execute(CustomJobContext arg0) {
	log.warn("##### INICIAR ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	Connection connSIGMA = null;
	
	PreparedStatement pstm = null;
	PreparedStatement pstmSIGMA = null;
	
	ResultSet rs = null;
	ResultSet rsSIGMA = null;
	
	List<String> listaCmd = null;
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	
	try {
	    
	    conn = PersistEngine.getConnection("");
	    
	    StringBuilder varname1 = new StringBuilder();
	    varname1.append("  select db_ordem.ID_ORDEM, 													");
	    varname1.append("         db_ordem.DEFEITO 														");
	    varname1.append("    from ( 															");
	    varname1.append("         select distinct a.cd_cliente 												");
	    varname1.append("           from [FSOODB03\\SQL01].SIGMA90.dbo.dbOrdem a WITH(NOLOCK) 								");
	    varname1.append("     inner join [FSOODB03\\SQL01].SIGMA90.dbo.COLABORADOR b WITH(NOLOCK) on a.id_instalador = b.CD_COLABORADOR 			");
	    varname1.append("     inner join [FSOODB03\\SQL01].SIGMA90.dbo.OSDEFEITO c WITH(NOLOCK) on a.idosdefeito = c.IDOSDEFEITO 				");
	    varname1.append("          where a.fechado = 0 													");
	    varname1.append("            and c.FG_ATIVO = 1 													");
	    varname1.append("            and b.NM_COLABORADOR not like '%CFTV%' 										");
	    varname1.append("    ) as cliente_os_aberta 													");
	    varname1.append("   inner join 															");
	    varname1.append("    ( 																");
	    varname1.append("	     select MAX(id_ordem) as id_ordem, 												");
	    varname1.append("	            cd_cliente 														");
	    varname1.append("	       from [FSOODB03\\SQL01].SIGMA90.dbo.dbOrdem aa WITH(NOLOCK) 								");
	    varname1.append("	   group by cd_cliente 														");
	    varname1.append("    ) as max_ordem on cliente_os_aberta.cd_cliente = max_ordem.cd_cliente 								");
	    varname1.append("   inner join 															");
	    varname1.append("    ( 																");
	    varname1.append("		select * 														");
	    varname1.append("		  from [FSOODB03\\SQL01].SIGMA90.dbo.dbOrdem aaa WITH(NOLOCK) 								");
	    varname1.append("		 where aaa.fechado = 0 													"); 
	    varname1.append("		   and aaa.defeito not like '%#FOTOCHIP#%' 										");
	    varname1.append("    ) as db_ordem on max_ordem.id_ordem = db_ordem.id_ordem 									");
	    varname1.append("   inner join 															");
	    varname1.append("    ( 																");
	    varname1.append("		select cd_cliente 													");
	    varname1.append("		  from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) 								");
	    varname1.append("		 where ce.ctrl_central = 1 												");
	    varname1.append("		   and ce.STLIBERACAOIMEI = 1 												");
	    varname1.append("		   and ce.id_central not like 'DDD%' 											");
	    varname1.append("	           and ce.id_central not like 'FFF%' 											");
	    varname1.append("	           and ce.id_central not like 'AA%' 											");
	    varname1.append("	           and ce.id_central not like 'R%' 											");
	    varname1.append("	 union   															");
	    varname1.append("	    select cd_cliente 														");
	    varname1.append("	      from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) 								");
	    varname1.append("	     where celular = 1 														");
	    varname1.append("	       and ncelular = 'GPRS' 													");
	    varname1.append("	       and id_central not like 'DDD%' 												");
	    varname1.append("	       and id_central not like 'FFF%' 												");
	    varname1.append("	       and id_central not like 'AA%' 												");
	    varname1.append("	       and id_central not like 'R%' 												");
	    varname1.append("    ) as cliente_gprs on cliente_os_aberta.cd_cliente = cliente_gprs.cd_cliente 							");
	    varname1.append("    WHERE NOT EXISTS(SELECT 1 													");
	    varname1.append("                       FROM [FSOODB03\\SQL01].SIGMA90.dbo.VIEW_HISTORICO VH WITH(NOLOCK) 						");
	    varname1.append("                      WHERE VH.CD_CLIENTE = cliente_gprs.cd_cliente 								");
	    varname1.append("                        AND VH.CD_EVENTO = 'CHP1') 										");
	    varname1.append("      AND NOT EXISTS(SELECT 1 													");
	    varname1.append("                       FROM [FSOODB03\\SQL01].SIGMA90.dbo.RECEPCAO R WITH(NOLOCK) 							");
	    varname1.append("                      WHERE R.CD_CLIENTE = cliente_gprs.cd_cliente 								");
	    varname1.append("                        AND R.NM_EVENTO = 'CHP1') 											");
	    varname1.append("      AND NOT EXISTS(SELECT 1 													");
	    varname1.append("                       FROM D_Chp1Cliente dc WITH(NOLOCK) 										");
	    varname1.append("                      WHERE dc.cdCliente = cliente_gprs.cd_cliente  								");
	    varname1.append("                        AND (dc.visitaValidada is null or dc.visitaValidada = 1)							");
	    varname1.append("                        AND dc.dataRemocao is null)										");
	    varname1.append("   order by db_ordem.abertura desc													");

	    pstm = conn.prepareStatement(varname1.toString());
	    rs = pstm.executeQuery();
	    conn.setAutoCommit(false);

	    listaCmd = new ArrayList<String>();
	    while (rs.next()) {
		String idOrdem = rs.getString("ID_ORDEM");
		String defeito = rs.getString("DEFEITO");
		
		listaCmd.add(" UPDATE dbo.dbOrdem SET DEFEITO = '"+defeito+". ATENÇÃO: Registrar a foto do Chip GPRS da central. Havendo a necessidade de troca, retirar fotos dos 2 Chips(Novo e Antigo), marcar um X no Chip antigo para diferenciar.#FOTOCHIP#' WHERE ID_ORDEM = " + idOrdem);
	    }
	    
	    connSIGMA = PersistEngine.getConnection("SIGMA90");

	    for (String cmd : listaCmd) {

		pstmSIGMA = connSIGMA.prepareStatement(cmd.toString());
		pstmSIGMA.executeUpdate();
	    }

	    conn.commit();
	    conn.setAutoCommit(true);
	    
	    connSIGMA.commit();
	    
	    log.warn("##### EXECUTAR ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    try {
		conn.rollback();
		connSIGMA.rollback();
	    } catch (SQLException e1) {
		e1.printStackTrace();
	    }
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
		OrsegupsUtils.closeConnection(connSIGMA, pstmSIGMA, rsSIGMA);
	    } catch (Exception e) {
		e.printStackTrace();
		log.error("##### ERRO ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    }
	    log.warn("##### FINALIZAR ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
    }
}
