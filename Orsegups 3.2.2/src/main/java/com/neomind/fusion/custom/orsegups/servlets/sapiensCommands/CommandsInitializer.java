package com.neomind.fusion.custom.orsegups.servlets.sapiensCommands;

import java.util.HashMap;
import java.util.Map;

import com.neomind.fusion.custom.orsegups.servlets.sapiensCommands.fluxoC029.StartC029ProcessCommand;

public class CommandsInitializer {
    
    public static Map<String, Command> retornaComandos(){
	
	Map<String, Command> comandos = new HashMap<String, Command>();
	
	comandos.put("startC029Process", new StartC029ProcessCommand());
		
	return comandos;    
    }
    

}
