package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Timestamp;

public class AvaliacaoRatVO {

	private Long rat;
	private Long cpfCnpj;
	private Long nota;
	private String evento;
	private String dataAtendimento;
	private String atendente;
	private String dadosCliente;
	private String comentario;
	private String autorizacao;
	private Timestamp dataAvaliacao;
	
	
	public Long getRat() {
		return rat;
	}
	public void setRat(Long rat) {
		this.rat = rat;
	}
	public Long getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(Long cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public Long getNota() {
		return nota;
	}
	public void setNota(Long nota) {
		this.nota = nota;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public String getDataAtendimento() {
		return dataAtendimento;
	}
	public void setDataAtendimento(String dataAtendimento) {
		this.dataAtendimento = dataAtendimento;
	}
	public String getAtendente() {
		return atendente;
	}
	public void setAtendente(String atendente) {
		this.atendente = atendente;
	}
	public String getDadosCliente() {
		return dadosCliente;
	}
	public void setDadosCliente(String dadosCliente) {
		this.dadosCliente = dadosCliente;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getAutorizacao() {
		return autorizacao;
	}
	public void setAutorizacao(String autorizacao) {
		this.autorizacao = autorizacao;
	}
	public Timestamp getDataAvaliacao() {
		return dataAvaliacao;
	}
	public void setDataAvaliacao(Timestamp dataAvaliacao) {
		this.dataAvaliacao = dataAvaliacao;
	}
	
	
}
