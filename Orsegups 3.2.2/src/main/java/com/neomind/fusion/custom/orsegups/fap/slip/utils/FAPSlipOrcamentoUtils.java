package com.neomind.fusion.custom.orsegups.fap.slip.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.Months;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.dto.HistoricoOrcamentoDTO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLDateBetweenFilter;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPSlipOrcamentoUtils
{	
	public static void validarPermissaoOrcamento(NeoObject noContrato)
	{
		EntityWrapper wContrato = new EntityWrapper(noContrato);
		
		NeoObject noEmpresaContrato = wContrato.findGenericValue("empresa");
		List<NeoObject> empresasPermitidas = wContrato.findGenericValue("orcamentoCadastrado.empresasPermitidas");
		if (empresasPermitidas == null)
			empresasPermitidas = wContrato.findGenericValue("orcamento.empresasPermitidas");
		
		boolean contemEmpresa = validarPermissoesOrcamento(empresasPermitidas, noEmpresaContrato, "codemp");
		if (!contemEmpresa)
			throw new WorkflowException("A empresa deste contrato não é permitida para esse orçamento.");
		
		NeoObject noFornecedorContrato = wContrato.findGenericValue("fornecedor");
		List<NeoObject> fornecedoresPermitidos = wContrato.findGenericValue("orcamentoCadastrado.fornecedoresPermitidos");
		if (fornecedoresPermitidos == null)
			fornecedoresPermitidos = wContrato.findGenericValue("orcamento.fornecedoresPermitidos");
		
		boolean contemFornecedor = validarPermissoesOrcamento(fornecedoresPermitidos, noFornecedorContrato, "codfor");
		if (!contemFornecedor)
			throw new WorkflowException("O fornecedor deste contrato não é permitido para esse orçamento.");
		
		List<NeoObject> listPermissao = wContrato.findGenericValue("orcamentoCadastrado.listaPermissao");
		if (listPermissao == null)
			listPermissao = wContrato.findGenericValue("orcamento.listaPermissao");
			
		boolean usuarioPermitido = FAPSlipUtils.validarPermissaoUsuario(listPermissao, PortalUtil.getCurrentUser());
		
		if (!usuarioPermitido)
			throw new WorkflowException("Você não ter permissão para iniciar esse contrato.");
	}
	
	public static boolean validarPermissoesOrcamento(List<NeoObject> listPermitidos, NeoObject noValidar, String campo)
	{
		boolean contem = true;
		if (listPermitidos != null && !listPermitidos.isEmpty())
		{
			contem = false;
			for (NeoObject noPermitido : listPermitidos)
			{
				long codForPermitido = new EntityWrapper(noPermitido).findGenericValue(campo);
				long codForContrato = new EntityWrapper(noValidar).findGenericValue(campo);
				if (codForContrato == codForPermitido)
					contem = true;
			}
		}
		
		return contem;
	}
	
	public static BigDecimal consultarSaldoDisponivel(NeoObject noContratoOrcamento)
	{
		return consultarSaldoDisponivel(noContratoOrcamento, null, null);
	}
	
	public static BigDecimal consultarSaldoDisponivel(NeoObject noContratoOrcamento, Long codContratoIgnorar)
	{
		return consultarSaldoDisponivel(noContratoOrcamento, codContratoIgnorar, null);
	}
	
	public static BigDecimal consultarSaldoDisponivel(NeoObject noContratoOrcamento, GregorianCalendar competencia)
	{
		return consultarSaldoDisponivel(noContratoOrcamento, null, competencia);
	}
	
	public static BigDecimal consultarSaldoDisponivel(NeoObject noContratoOrcamento, Long codContratoIgnorar, GregorianCalendar competencia)
	{
		List<HistoricoOrcamentoDTO> listHistorico = new ArrayList<>();
		
		listHistorico.addAll(gerarHistoricoCreditosOrcamento(noContratoOrcamento, competencia));
		listHistorico.addAll(gerarHistoricoDebitosOrcamento(noContratoOrcamento, competencia));
		ordernarData(listHistorico);
		
		listHistorico.addAll(gerarHistoricoConsumoPrevistoAtualContratos(noContratoOrcamento, codContratoIgnorar, listHistorico));
		
		BigDecimal saldo = new BigDecimal(0l);
		
		GregorianCalendar ultimaVigencia = null;
		for (HistoricoOrcamentoDTO historico : listHistorico)
		{
			if (!historico.getTransacao().equals("Consumo Previsto"))
			{
				if (ultimaVigencia != null && (ultimaVigencia.before(historico.getData()) && ultimaVigencia.before(new GregorianCalendar())))
				{
					saldo = new BigDecimal(0);
				}
				
				if (historico.getTransacao().equals("Crédito"))
				{
					saldo = saldo.add(historico.getValor());
					ultimaVigencia = historico.getFimVigencia();
				}
				else
				{
					saldo = saldo.subtract(historico.getValor());
				}
			}
			else
			{
				if (historico.getValor().signum() == 1)
					saldo = saldo.subtract(historico.getValor());
				else 
					saldo = saldo.add(historico.getValor());
			}
		}
		
		return saldo;
	}
	
	public static List<HistoricoOrcamentoDTO> gerarHistoricoCreditosOrcamento(NeoObject noContratoOrcamento)
	{
		return gerarHistoricoCreditosOrcamento(noContratoOrcamento, null);
	}
	
	public static List<HistoricoOrcamentoDTO> gerarHistoricoCreditosOrcamento(NeoObject noContratoOrcamento, GregorianCalendar competencia)
	{
		List<HistoricoOrcamentoDTO> listCreditosOrcamento = new ArrayList<>();
		
		List<NeoObject> listOrcamento = new EntityWrapper(noContratoOrcamento).findGenericValue("orcamento");
		for (NeoObject noOrcamento : listOrcamento)
		{
			EntityWrapper wOrcamento = new EntityWrapper(noOrcamento);
			
			GregorianCalendar inicioVigenciaOrcamento = wOrcamento.findGenericValue("inicioVigencia");
			GregorianCalendar fimVigenciaOrcamento = wOrcamento.findGenericValue("fimVigencia");
			
			if (competencia != null)
			{
				if (competencia.before(inicioVigenciaOrcamento) || competencia.after(fimVigenciaOrcamento))
					continue;
			}
			
			WFProcess processoCadastroOrcamento = null;
			
			List<NeoObject> listCadastroOrcamento = buscarCadastroOrcamento(noContratoOrcamento);
			for (NeoObject noCadastroOrcamento : listCadastroOrcamento)
			{
				processoCadastroOrcamento = new EntityWrapper(noCadastroOrcamento).findGenericValue("wfprocess");
				break;
			}
			
			String descricao = wOrcamento.findGenericValue("descricao");
			BigDecimal valor = wOrcamento.findGenericValue("valor");
			
			HistoricoOrcamentoDTO historico = new HistoricoOrcamentoDTO();
			historico.setData(inicioVigenciaOrcamento);
			historico.setFimVigencia(fimVigenciaOrcamento);
			historico.setTransacao("Crédito");
			historico.setDescricao(descricao);
			historico.setTarefa(processoCadastroOrcamento != null ? processoCadastroOrcamento.getCode() : "Não encontrado");
			historico.setValor(valor);
			
			listCreditosOrcamento.add(historico);
		}
		
		return listCreditosOrcamento;
	}
	
	public static List<HistoricoOrcamentoDTO> gerarHistoricoDebitosOrcamento(NeoObject noContratoOrcamento)
	{
		return gerarHistoricoDebitosOrcamento(noContratoOrcamento, null);
	}
	
	public static List<HistoricoOrcamentoDTO> gerarHistoricoDebitosOrcamento(NeoObject noContratoOrcamento, GregorianCalendar competencia)
	{
		List<HistoricoOrcamentoDTO> listHistoricoDebitos = new ArrayList<>();
		
		List<NeoObject> listSlipsOrcamento = FAPSlipOrcamentoUtils.buscarPgtosOrcamento(noContratoOrcamento);
		loop1: for (NeoObject noPagamento : listSlipsOrcamento)
		{
			EntityWrapper wPagamento = new EntityWrapper(noPagamento);
			
			GregorianCalendar competenciaPagto = wPagamento.findGenericValue("competencia");
			
			if (competencia != null)
			{
				Boolean mesmoOrcamento = false;
				
				List<NeoObject> listOrcamentosNaCompetenciaPagto = findOrcamentoPorCompetencia(noContratoOrcamento, competenciaPagto);
				List<NeoObject> listOrcamentosNaCompetenciaPagtoAtual = findOrcamentoPorCompetencia(noContratoOrcamento, competencia);
				for (NeoObject noOrcamentoPagto : listOrcamentosNaCompetenciaPagto)
				{
					for (NeoObject noOrcamentoPagtoAtual : listOrcamentosNaCompetenciaPagtoAtual)
					{
						if (noOrcamentoPagtoAtual.equals(noOrcamentoPagto))
							mesmoOrcamento = true;
					}
				}
				
				if (!mesmoOrcamento)
					continue loop1;
			}
			
			String fornecedor = wPagamento.findGenericValue("fornecedor.apefor");
			fornecedor = fornecedor == null ? "" : " - " + fornecedor;
			String nrDocumento = wPagamento.findGenericValue("nrDocumento");
			nrDocumento = nrDocumento == null ? "" : nrDocumento;
			
			Long codContrato = wPagamento.findGenericValue("dadosContrato.codContrato");
			
			GregorianCalendar vencimento = wPagamento.findGenericValue("dataVencimento");
			String descricao = nrDocumento + fornecedor + (codContrato != null ? " (Contrato: "+codContrato+")" : "");
			WFProcess processo = wPagamento.findGenericValue("wfprocess");
			BigDecimal valorPagto = wPagamento.findGenericValue("valorPagamento");
			
			HistoricoOrcamentoDTO historico = new HistoricoOrcamentoDTO();			
			historico.setCodContrato(codContrato);
			historico.setData(competenciaPagto);
			historico.setVencimento(vencimento);
			historico.setTransacao("Débito");
			historico.setDescricao(descricao);
			historico.setTarefa(processo.getCode());
			historico.setValor(valorPagto);
			
			listHistoricoDebitos.add(historico);
		}
		
		return listHistoricoDebitos;
	}
	
	public static List<HistoricoOrcamentoDTO> gerarHistoricoConsumoPrevistoTotalContratos(NeoObject noContratoOrcamento)
	{
		return gerarHistoricoConsumoPrevistoAtualContratos(noContratoOrcamento, null, null, null);
	}
	
	public static List<HistoricoOrcamentoDTO> gerarHistoricoConsumoPrevistoTotalContratos(NeoObject noContratoOrcamento, Long codContratoIgnorar)
	{
		return gerarHistoricoConsumoPrevistoAtualContratos(noContratoOrcamento, codContratoIgnorar, null, null);
	}
	
	public static List<HistoricoOrcamentoDTO> gerarHistoricoConsumoPrevistoAtualContratos(NeoObject noContratoOrcamento, Long codContratoIgnorar, List<HistoricoOrcamentoDTO> listHistorico)
	{
		return gerarHistoricoConsumoPrevistoAtualContratos(noContratoOrcamento, codContratoIgnorar, listHistorico, null);
	}
	
	public static List<HistoricoOrcamentoDTO> gerarHistoricoConsumoPrevistoAtualContratos(NeoObject noContratoOrcamento, Long codContratoIgnorar, List<HistoricoOrcamentoDTO> listHistorico, GregorianCalendar competencia)
	{
		List<HistoricoOrcamentoDTO> listHistoricoConsumoPrevisto = new ArrayList<>();
		
		List<NeoObject> listContratosUsandoOrcamento = FAPSlipOrcamentoUtils.buscarContratosUsandoOrcamento(noContratoOrcamento);
		for (NeoObject noContrato : listContratosUsandoOrcamento)
		{
			EntityWrapper wContrato = new EntityWrapper(noContrato);
			
			Long codContrato = wContrato.findGenericValue("codContrato");
			if (codContratoIgnorar != null && codContratoIgnorar.equals(codContrato))
				continue;
			
			BigDecimal consumoPrevistoContrato = consultarConsumoPrevistoContrato(noContrato, codContratoIgnorar);
			
			if (listHistorico != null && consumoPrevistoContrato != null)
			{
				for (HistoricoOrcamentoDTO historicoDTO : listHistorico)
				{
					if (historicoDTO.getCodContrato() == codContrato)
					{
						consumoPrevistoContrato = consumoPrevistoContrato.subtract(historicoDTO.getValor());
					}
				}
			}
			
			if (consumoPrevistoContrato != null && (consumoPrevistoContrato.signum() == -1 || consumoPrevistoContrato.compareTo(new BigDecimal(0)) == 0))
				continue;
			
			GregorianCalendar inicioVigenciaContrato = wContrato.findGenericValue("inicioVigencia");
			GregorianCalendar fimVigenciaContrato = wContrato.findGenericValue("fimVigencia");
			
			HistoricoOrcamentoDTO historico = new HistoricoOrcamentoDTO();			
			historico.setCodContrato(codContrato);
			historico.setData(inicioVigenciaContrato);
			historico.setFimVigencia(fimVigenciaContrato);
			historico.setTransacao("Consumo Previsto");
			historico.setDescricao("Consumo Previsto para o contrato " + codContrato);
			historico.setValor(consumoPrevistoContrato == null ?  new BigDecimal(0) : consumoPrevistoContrato);
			
			listHistoricoConsumoPrevisto.add(historico);
		}
		
		return listHistoricoConsumoPrevisto;
	}
	
	public static BigDecimal consultarConsumoPrevistoContrato(NeoObject noContrato)
	{
		return consultarConsumoPrevistoContrato(noContrato, null);
	}
	
	public static BigDecimal consultarConsumoPrevistoContrato(NeoObject noContrato, Long codContratoIgnorar)
	{
		EntityWrapper wContrato = new EntityWrapper(noContrato);
		
		BigDecimal valorMensal = wContrato.findGenericValue("valorBase");
		
		if (valorMensal != null)
		{
			GregorianCalendar inicioVigencia = wContrato.findGenericValue("inicioVigencia");
			GregorianCalendar fimVigencia = wContrato.findGenericValue("fimVigencia");
			
			if (codContratoIgnorar != null)
				inicioVigencia = new GregorianCalendar();
			
			Integer nrMeses = Months.monthsBetween(LocalDate.fromCalendarFields(inicioVigencia), LocalDate.fromCalendarFields(fimVigencia)).getMonths();
			nrMeses++;
			
			return valorMensal.multiply(new BigDecimal(nrMeses));
		}
		
		return null;
	}
	
	public static List<NeoObject> findOrcamentoPorCompetencia(NeoObject noContratoOrcamento, GregorianCalendar competencia)
	{
		List<NeoObject> listOrcamentoContrato = new EntityWrapper(noContratoOrcamento).findGenericValue("orcamento");
		 
		return findOrcamentoPorCompetencia(listOrcamentoContrato, competencia);
	}
	
	public static List<NeoObject> findOrcamentoPorCompetencia(List<NeoObject> listOrcamento, GregorianCalendar competencia)
	{
		List<NeoObject> listOrcamentoNaCompetencia = new ArrayList<>();
		if (listOrcamento != null)
		{
			for (NeoObject noOrcamento : listOrcamento)
			{
				EntityWrapper wOrcamento = new EntityWrapper(noOrcamento);
				
				GregorianCalendar inicioVigenciaOrcamento = wOrcamento.findGenericValue("inicioVigencia");
				inicioVigenciaOrcamento.set(Calendar.DAY_OF_MONTH, 1);
				inicioVigenciaOrcamento.set(Calendar.HOUR_OF_DAY, 0);
				inicioVigenciaOrcamento.set(Calendar.MINUTE, 0);
				inicioVigenciaOrcamento.set(Calendar.SECOND, 0);
				
				GregorianCalendar fimVigenciaOrcamento = wOrcamento.findGenericValue("fimVigencia");
				
				if ((competencia.after(inicioVigenciaOrcamento) || competencia.equals(inicioVigenciaOrcamento)) && (competencia.before(fimVigenciaOrcamento) || competencia.equals(fimVigenciaOrcamento)))
					listOrcamentoNaCompetencia.add(noOrcamento);
			}
		}
		
		return listOrcamentoNaCompetencia;
	}
	
	public static BigDecimal consultarOrcamentoPorCompetencia(NeoObject noContratoOrcamento, GregorianCalendar competencia)
	{
		List<NeoObject> listOrcamentoContrato = new EntityWrapper(noContratoOrcamento).findGenericValue("orcamento");
		
		return consultarOrcamentoPorCompetencia(listOrcamentoContrato, competencia);
	}
	
	public static BigDecimal consultarOrcamentoPorCompetencia(List<NeoObject> listOrcamento, GregorianCalendar competencia)
	{
		List<NeoObject> listOrcamentosNaCompetencia = findOrcamentoPorCompetencia(listOrcamento, competencia);
		
		BigDecimal totalOrcamento = new BigDecimal(0);
		
		for (NeoObject noOrcamentoCompetencia : listOrcamentosNaCompetencia)
		{
			EntityWrapper wOrcamentoCompetencia = new EntityWrapper(noOrcamentoCompetencia);
			
			BigDecimal valorOrcamento = wOrcamentoCompetencia.findGenericValue("valor");
			
			totalOrcamento = totalOrcamento.add(valorOrcamento);
		}
		
		return totalOrcamento;
	}
	
	public static List<NeoObject> buscarPgtosOrcamento(NeoObject noOrcamento)
	{
		return buscarPgtosOrcamento(noOrcamento, null, null);
	}
	
	public static List<NeoObject> buscarPgtosOrcamento(NeoObject noOrcamento, GregorianCalendar inicioCompetencia, GregorianCalendar fimCompetencia)
	{
		if (noOrcamento == null)
			return new ArrayList<>();
		
		Long codContrato = new EntityWrapper(noOrcamento).findGenericValue("codContrato");
		
		
		QLGroupFilter filterCriterios = new QLGroupFilter("OR", new QLEqualsFilter("dadosContrato.tipoOrcamento.codigo", 1L), new QLEqualsFilter("dadosContrato.criterioAprovacao.codigo", 5L));
		QLGroupFilter filterOrcamentoIndividual = new QLGroupFilter("AND", new QLEqualsFilter("dadosContrato.codContrato", codContrato), filterCriterios);
		
		QLGroupFilter orFilter = new QLGroupFilter("OR", new QLEqualsFilter("orcamento.codContrato", codContrato), new QLEqualsFilter("dadosContrato.orcamentoCadastrado.codContrato", codContrato), filterOrcamentoIndividual);
		QLGroupFilter andFilter = new QLGroupFilter("AND", new QLRawFilter("_vo.wfprocess.processState <> 2"), orFilter, new QLFilterIsNotNull("valorPagamento"), new QLEqualsFilter("operacao.codigo", 1l));
		
		if (inicioCompetencia != null && fimCompetencia != null)
			andFilter.addFilter(new QLDateBetweenFilter("competencia", inicioCompetencia, fimCompetencia));
		
		return PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), andFilter);
	}
	
	public static List<NeoObject> buscarContratosUsandoOrcamento(NeoObject noOrcamento)
	{
		QLGroupFilter orFilter = new QLGroupFilter("OR", new QLEqualsFilter("tipoOrcamento.codigo", 2l), new QLEqualsFilter("criterioAprovacao.codigo", 6l));		
		QLGroupFilter andFilter = new QLGroupFilter("AND", new QLEqualsFilter("ativo", true), new QLEqualsFilter("orcamentoCadastrado", noOrcamento), orFilter);
		
		return PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlipCadastroContrato"), andFilter);
	}
	
	public static List<NeoObject> buscarCadastroOrcamento(NeoObject noOrcamento)
	{
		QLGroupFilter andFilter = new QLGroupFilter("AND", new QLRawFilter("_vo.wfprocess.processState <> 2"), new QLEqualsFilter("novoContrato", noOrcamento), new QLEqualsFilter("operacao.codigo", 3l));
		return PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), andFilter);
	}
	
	private static void ordernarData(Collection<? extends HistoricoOrcamentoDTO> list)
	{
		Collections.sort((List<? extends HistoricoOrcamentoDTO>) list, new Comparator<HistoricoOrcamentoDTO>()		
		{
			@Override
			public int compare(HistoricoOrcamentoDTO o1, HistoricoOrcamentoDTO o2)
			{
				if (o1.getData() == null || o2.getData() == null) 
				      return 0;
				else
					return o1.getData().compareTo(o2.getData());
			}
		});
	}
}
