package com.neomind.fusion.custom.orsegups.mobile.vo2;

import java.util.ArrayList;;

public class AssuntoVO {
	
	private String neoId;
	private String txtAssunto;
	private ArrayList<PerguntaVO> perguntas;
	
	public String getTxtAssunto() {
		return txtAssunto;
	}
	public void setTxtAssunto(String txtAssunto) {
		this.txtAssunto = txtAssunto;
	}
	public ArrayList<PerguntaVO> getPerguntas() {
		return perguntas;
	}
	public void setPerguntas(ArrayList<PerguntaVO> perguntas) {
		this.perguntas = perguntas;
	}
	public String getNeoId() {
		return neoId;
	}
	public void setNeoId(String neoId) {
		this.neoId = neoId;
	}
	
	

}
