package com.neomind.fusion.custom.orsegups.ait.vo;

public class AtendimentoValorVO
{
	private Double subTotalDeslocamento;
	private Double subTotalAtendimento;
	private int count;
	private Double totalValorDSL;
	private Double totalValorATD;
	private Double totalDeslocamento;
	private Double totalAtendimento;
	private Double totalValorGeral;
	
	public Double getSubTotalDeslocamento()
	{
		return subTotalDeslocamento;
	}
	public void setSubTotalDeslocamento(Double subTotalDeslocamento)
	{
		this.subTotalDeslocamento = subTotalDeslocamento;
	}
	public Double getSubTotalAtendimento()
	{
		return subTotalAtendimento;
	}
	public void setSubTotalAtendimento(Double subTotalAtendimento)
	{
		this.subTotalAtendimento = subTotalAtendimento;
	}
	public int getCount()
	{
		return count;
	}
	public void setCount(int count)
	{
		this.count = count;
	}
	public Double getTotalValorDSL()
	{
		return totalValorDSL;
	}
	public void setTotalValorDSL(Double totalValorDSL)
	{
		this.totalValorDSL = totalValorDSL;
	}
	public Double getTotalValorATD()
	{
		return totalValorATD;
	}
	public void setTotalValorATD(Double totalValorATD)
	{
		this.totalValorATD = totalValorATD;
	}
	public Double getTotalDeslocamento()
	{
		return totalDeslocamento;
	}
	public void setTotalDeslocamento(Double totalDeslocamento)
	{
		this.totalDeslocamento = totalDeslocamento;
	}
	public Double getTotalAtendimento()
	{
		return totalAtendimento;
	}
	public void setTotalAtendimento(Double totalAtendimento)
	{
		this.totalAtendimento = totalAtendimento;
	}
	public Double getTotalValorGeral() {
	    return totalValorGeral;
	}
	public void setTotalValorGeral(Double totalValorGeral) {
	    this.totalValorGeral = totalValorGeral;
	}
	
	
}
