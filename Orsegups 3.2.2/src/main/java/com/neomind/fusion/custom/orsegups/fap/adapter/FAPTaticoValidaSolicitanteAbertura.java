package com.neomind.fusion.custom.orsegups.fap.adapter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;


public class FAPTaticoValidaSolicitanteAbertura implements TaskFinishEventListener {

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {

		EntityWrapper processEntity = new EntityWrapper(event.getProcess().getEntity());
		
		Long codigoAplicacao = Long.parseLong(processEntity.findField("aplicacaoPagamento.codigo").getValueAsString());
		NeoUser solicitante = (NeoUser) processEntity.findField("solicitanteOrcamento").getValue();
		String nomeSolicitante = solicitante.getCode();
		boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);

		if(codigoAplicacao == 11 && !nomeSolicitante.equalsIgnoreCase("rosileny") && debug == false) {
			throw new WorkflowException("Usuário sem permissão para abrir FAP Tático.");
		}		

		getPostoSapiensViaContaSigma(event);
	}



	public void getPostoSapiensViaContaSigma(TaskFinishEvent event) {


		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		try	{

			EntityWrapper processEntity = new EntityWrapper(event.getProcess().getEntity());
			Long cdCliente = processEntity.findGenericValue("contaSigma.cd_cliente");			
			conn = PersistEngine.getConnection("SIGMA90");

			sql.append(" SELECT CVS.USU_DATINI, CVS.USU_DATFIM,  												");
			sql.append(" CAST(SUM(CVS.USU_QTDCVS * CVS.USU_PREUNI) AS DECIMAL(6,2)) AS VALORPOSTO  				");
			sql.append(" FROM dbCENTRAL C																		");
			sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160SIG SIG 								");
			sql.append("							ON C.CD_CLIENTE = SIG.USU_CODCLI							");
			sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160CVS CVS 								");
			sql.append("							ON SIG.USU_CODEMP = CVS.USU_CODEMP 							");
			sql.append("							AND SIG.USU_CODFIL = CVS.USU_CODFIL 						");
			sql.append("							AND SIG.USU_NUMCTR = CVS.USU_NUMCTR 						");
			sql.append("							AND SIG.USU_NUMPOS = CVS.USU_NUMPOS							");
			sql.append(" WHERE C.CD_CLIENTE = ?																	");
			sql.append(" AND ((CVS.USU_SITCVS = 'A') OR (CVS.USU_SITCVS = 'I' AND CVS.USU_DATFIM >= GETDATE()))	");
			sql.append(" GROUP BY CVS.USU_DATINI, CVS.USU_DATFIM										     	");
			sql.append(" ORDER BY CVS.USU_DATINI ASC 														    ");
			


			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, cdCliente);
			rs = pstm.executeQuery();

			if(rs.next()) {
				GregorianCalendar dataInicioVigencia = new GregorianCalendar();
				GregorianCalendar dataFimVigencia = new GregorianCalendar();				
				dataInicioVigencia.setTime(rs.getDate("USU_DATINI"));
				dataFimVigencia.setTime(rs.getDate("USU_DATFIM"));
				BigDecimal valorPosto = new BigDecimal(rs.getLong("VALORPOSTO"));
				
				NeoObject postoSapiens = AdapterUtils.getInstantiableEntityInfo("FAPPostoSapiens").createNewInstance();
				EntityWrapper ewPosto = new EntityWrapper(postoSapiens);
				ewPosto.findField("inicioVigencia").setValue(dataInicioVigencia);
				ewPosto.findField("fimDeVigencia").setValue(dataFimVigencia);
				ewPosto.findField("valorTotalPosto").setValue(valorPosto);
				
				PersistEngine.persist(postoSapiens);
				
				List<NeoObject> listaDePostos = new ArrayList<>();
				listaDePostos.add(postoSapiens);			
				processEntity.findField("postoSapiens").setValue(listaDePostos);	
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

	}
}







