package com.neomind.fusion.custom.orsegups.maps.call.vo;

import java.util.LinkedHashSet;

import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;

public class CallAlertEventVO
{
	private final String type = "CallAlertEventVO";

	private ViaturaVO callViaturaVO;

	private EventoVO callEventoVO;

	private LinkedHashSet<ProvidenciaVO> numerosChamar;

	private String ultimoNumeroChamado;
	
	private Integer ultimoNumero;

	private LinkedHashSet<String> numerosJaChamados = new LinkedHashSet<String>();

	private String ramalOperador;

	private String infoWindow;

	public ViaturaVO getCallViaturaVO()
	{
		return callViaturaVO;
	}

	public void setCallViaturaVO(ViaturaVO callViaturaVO)
	{
		this.callViaturaVO = callViaturaVO;
	}

	public EventoVO getCallEventoVO()
	{
		return callEventoVO;
	}

	public void setCallEventoVO(EventoVO callEventoVO)
	{
		this.callEventoVO = callEventoVO;
	}

	public LinkedHashSet<ProvidenciaVO> getNumerosChamar()
	{
		return numerosChamar;
	}

	public void setNumerosChamar(LinkedHashSet<ProvidenciaVO> numerosChamar)
	{
		this.numerosChamar = numerosChamar;
	}

	public LinkedHashSet<String> getNumerosJaChamados()
	{
		return numerosJaChamados;
	}

	public void setNumerosJaChamados(LinkedHashSet<String> numerosJaChamados)
	{
		this.numerosJaChamados = numerosJaChamados;
	}

	public String getRamalOperador()
	{
		return ramalOperador;
	}

	public void setRamalOperador(String ramalOperador)
	{
		this.ramalOperador = ramalOperador;
	}

	public String getUltimoNumeroChamado()
	{
		return ultimoNumeroChamado;
	}

	public void setUltimoNumeroChamado(String ultimoNumeroChamado)
	{
		this.ultimoNumeroChamado = ultimoNumeroChamado;
	}

	public String getType()
	{
		return type;
	}

	public String getInfoWindow()
	{
		return infoWindow;
	}

	public void setInfoWindow(String infoWindow)
	{
		this.infoWindow = infoWindow;
	}

	public void updateInfoWindow()
	{
		StringBuilder contentString = new StringBuilder();
		contentString.append("<div id='content'  >");
		contentString.append("	<div id='bodyContent'  >");
		contentString.append("		<span><b>Contatos:</b></span>");
		contentString.append("		<ul>");
		for (ProvidenciaVO providenciaVO : this.numerosChamar)
		{
			if ((providenciaVO.getTelefone1() != null && !providenciaVO.getTelefone1().isEmpty())
					|| (providenciaVO.getTelefone2() != null && !providenciaVO.getTelefone2().isEmpty())
					|| (providenciaVO.getTelefone3() != null && !providenciaVO.getTelefone3().isEmpty())
					|| (providenciaVO.getTelefone4() != null && !providenciaVO.getTelefone4().isEmpty())
					|| (providenciaVO.getTelefone5() != null && !providenciaVO.getTelefone5().isEmpty())
					|| (providenciaVO.getTelefone6() != null && !providenciaVO.getTelefone6().isEmpty()))
			{
				String nameClass = "";
				String phone1Class = "";
				String phone2Class = "";
				String phone3Class = "";
				String phone4Class = "";
				String phone5Class = "";
				String phone6Class = "";

				contentString.append("<li>");
				//adiciona o nome do caontato e trata para saber se esta ligando para este contato
				if (this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone1())
						|| this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone2())
						|| this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone3())
						|| this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone4())
						|| this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone5())
						|| this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone6()))
				{
					nameClass = "class='calling'";
				}
				contentString.append("		<span " + nameClass + ">" + providenciaVO.getNome() + "</span>");
				
				//adiciona o telefone 1 do caontato e trata para saber se esta ligando para este contato
				if (providenciaVO.getTelefone1() != null && !providenciaVO.getTelefone1().isEmpty())
				{
					if (this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone1()))
					{
						phone1Class = "class='calling'";
					}
					contentString.append("		<span " + phone1Class + "> - "
							+"<a href='javascript:dial(\"0" +  providenciaVO.getTelefone1()  + "\");'><img style='border:none' src='images/phone.png'></a>"+  providenciaVO.getTelefone1()  + "</span>");
				}
				
				//adiciona o telefone 2 do caontato e trata para saber se esta ligando para este contato
				if (providenciaVO.getTelefone2() != null && !providenciaVO.getTelefone2().isEmpty())
				{
					if (this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone2()))
					{
						phone2Class = "class='calling'";
					}
					contentString.append("		<span " + phone2Class + "> - "
							+"<a href='javascript:dial(\"0" +  providenciaVO.getTelefone2()  + "\");'><img  style='border:none' src='images/phone.png'></a>"+   providenciaVO.getTelefone2()  +  "</span>");
				}
				//adiciona o telefone 3 do caontato e trata para saber se esta ligando para este contato
				if (providenciaVO.getTelefone3() != null && !providenciaVO.getTelefone3().isEmpty())
				{
					if (this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone3()))
					{
						phone3Class = "class='calling'";
					}
					contentString.append("		<span " + phone3Class + "> - "
							+"<a href='javascript:dial(\"0" +  providenciaVO.getTelefone3()  + "\");'><img  style='border:none' src='images/phone.png'></a>"+providenciaVO.getTelefone3()  +"</span>");
				}
				
				//adiciona o telefone 4 do caontato e trata para saber se esta ligando para este contato
				if (providenciaVO.getTelefone4() != null && !providenciaVO.getTelefone4().isEmpty())
				{
					if (this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone4()))
					{
						phone4Class = "class='calling'";
					}
					contentString.append("		<span " + phone4Class + "> - "
							+ "<a href='javascript:dial(\"0" +  providenciaVO.getTelefone4()  + "\");'><img  style='border:none' src='images/phone.png'></a>"+ providenciaVO.getTelefone4()  +  "</span>");
				}
				
				//adiciona o telefone 5 do caontato e trata para saber se esta ligando para este contato
				if (providenciaVO.getTelefone5() != null && !providenciaVO.getTelefone5().isEmpty())
				{
					if (this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone5()))
					{
						phone5Class = "class='calling'";
					}
					contentString.append("		<span " + phone5Class + "> - "
							+ "<a href='javascript:dial(\"0" +  providenciaVO.getTelefone5()  + "\");'><img  style='border:none' src='images/phone.png'></a>"+providenciaVO.getTelefone5()  + "</span>");
				}
				
				//adiciona o telefone 6 do caontato e trata para saber se esta ligando para este contato
				if (providenciaVO.getTelefone6() != null && !providenciaVO.getTelefone6().isEmpty())
				{
					if (this.ultimoNumeroChamado.equalsIgnoreCase(providenciaVO.getTelefone6()))
					{
						phone6Class = "class='calling'";
					}
					contentString.append("		<span " + phone6Class + "> - "
							+"<a href='javascript:dial(\"0" +  providenciaVO.getTelefone6()  + "\");'><img  style='border:none' src='images/phone.png'></a>"+  providenciaVO.getTelefone6()  +  "</span>");
				}
				

				contentString.append(" <a href=\"#\" class=\"easyui-linkbutton\" data-options=\"iconCls:'icon-remove',disabled:true\"><span id=\"clock\" class='calling'></span></a>  ");
				
                    		if (callEventoVO != null) {
                    
                    		    if (callEventoVO.getObservacaoEmpresa() != null) {
                    
                    			if (callEventoVO.getObservacaoEmpresa().equalsIgnoreCase("autocargo")) {
                    			    contentString.append(" <span class=\"info-empresa-purple\"><b>" + callEventoVO.getObservacaoEmpresa().toUpperCase() + "</b></span> ");
                    			} else if (callEventoVO.getObservacaoEmpresa().equalsIgnoreCase("vigzul")) {
                    			    contentString.append(" <span class=\"info-empresa-blue\"><b>" + callEventoVO.getObservacaoEmpresa().toUpperCase() + "</b></span> ");
                    			} else if (callEventoVO.getObservacaoEmpresa().equalsIgnoreCase("seyprol")) {
                    			    contentString.append(" <span class=\"info-empresa-red\"><b>" + callEventoVO.getObservacaoEmpresa().toUpperCase() + "</b></span> ");
                    			}  else if (callEventoVO.getObservacaoEmpresa().equalsIgnoreCase("securisystem")) {
                    			    contentString.append(" <span class=\"info-empresa-darkblue\"><b>" + callEventoVO.getObservacaoEmpresa().toUpperCase() + "</b></span> ");
                    			} 
                    
                    		    }
                    
                    		}

				contentString.append("</li>");
			}
		}

		contentString.append("		</ul>");
		contentString.append("		<p>");
		contentString.append(" <div id=\"contaSigmaLayout\" class=\"easyui-layout\" style=\"width:100%;height:300px;\"> ");
		contentString.append(" <div id=\"detalhesContrato\" collapsible=\"true\" data-options=\"region:'center',title:'Detalhes Conta',iconCls:'icon-search'\"> ");
		contentString.append(" <ul> ");
		contentString.append(" <fieldset style=\" border:1px solid #95B8E7\"> ");
		contentString.append(" <legend>Dados Cliente</legend> ");
		contentString.append("  <li>Código Cliente: <span id=\"codigoCliente\"></span> - Empresa: <span id=\"codigoEmpresa\"></span> </li> ");
		//contentString.append(" <li>Empresa: <span id=\"codigoEmpresa\"></span></li> ");
		contentString.append(" <li>Razão Social: <span id=\"razaoSocialSigma\"></span></li> ");
		contentString.append(" <li>CPF/CNPJ: <span id=\"cnpjCpfSigma\"></span></li> ");
		contentString.append(" <li>Fantasia: <span id=\"fantasiaSigma\"></span></li> ");
		contentString.append(" </fieldset> ");
		contentString.append(" <fieldset style=\" border:1px solid #95B8E7 \"> ");
		contentString.append(" <legend>Dados Conta/Partição</legend> ");
		contentString.append(" <li>Código Central: <span class=\"destaque\" id=\"codigoCentral\"></span></li> ");
		contentString.append(" <li>Partição: <span class=\"destaque\" id=\"particao\"></span></li> ");
		contentString.append(" <li>Rota: <span id=\"rota\"></span></li> ");
		contentString.append(" <li>Endereço: <span id=\"logradouro\"></span></li> ");
		contentString.append(" <li>Bairro: <span id=\"bairroSigma\"></span></li> ");
		contentString.append(" <li>Cidade: <span id=\"cidadeSigma\"></span></li> ");
		contentString.append(" <li>Estado: <span id=\"ufSigma\"></span></li> ");
		contentString.append(" <li>CEP: <span id=\"cepSigma\"></span></li> ");
		contentString.append(" </fieldset> ");
		contentString.append(" <fieldset style=\" border:1px solid #95B8E7 \"> ");
		contentString.append(" <legend>Contato</legend> ");
		contentString.append(" <li>Responsável: <span id=\"responsavelCentral\"></span></li> ");
		contentString.append(" <li>Email: <span id=\"emailCentral\"></span></li> ");
		contentString.append(" <li>Telefone Central 1: <span id=\"telefoneCentral1\"></span></li> ");
		contentString.append(" <li>Telefone Central 2: <span id=\"telefoneCentral2\"></span></li> ");
		contentString.append(" </fieldset> ");
		contentString.append(" <fieldset style=\" border:1px solid #95B8E7 \"> ");
		contentString.append(" <legend>Palavra Chave</legend> ");
		contentString.append(" <li>Pergunta: <span class=\"destaque\" id=\"pergunta\"></span></li> ");
		contentString.append(" <li>Resposta / Senha Verbal: <span class=\"destaque\" id=\"resposta\"></span></li> ");
		contentString.append(" </fieldset> ");
	      
		contentString.append(" </div> ");
		contentString.append(" </div> ");
		contentString.append(" </li> ");
		contentString.append(" </ul> ");
		contentString.append(" </div> ");
		contentString.append(" </div> ");
		contentString.append(" <div id=\"contaSigmaLayout\" collapsible=\"true\" data-options=\"region:'center'\"  class=\"easyui-layout\" style=\"width:100%;height:300px;\" > ");
		contentString.append(" <fieldset style=\" border:1px solid #95B8E7 \"> ");
		contentString.append(" <legend><b>Informações do Cliente</b></legend> ");
		contentString.append(" <div class=\"easyui-tabs\" id=\"tabsId\" style=\"width:1030px;height:270px;\">  ");
		contentString.append(" <div title=\"Contatos\" style=\"padding:5px;width:99%;height:99%\"> ");
//		contentString.append(" <div class=\"easyui-layout\" style=\"width:auto;height:auto\"> ");
		contentString.append(" <table id=\"dgProvidencia\" class=\"easyui-datagrid\"  singleSelect=\"true\" style=\"width:1020px;height:220px\" ");  
		contentString.append(" data-options=\"fitColumns:true,singleSelect:true\"> ");  
		contentString.append(" <thead> ");  
		contentString.append(" <tr> ");  
		contentString.append(" <th data-options=\"field:'codigoProvidencia',width:50,align:'center'\">Providência</th>  "); 
		contentString.append(" <th data-options=\"field:'nome',width:150\">Nome</th>  "); 
		contentString.append(" <th data-options=\"field:'telefone1',width:60,formatter:function(row){ return row;}\">Telefone 1</th>   ");
		contentString.append(" <th data-options=\"field:'telefone2',width:60\">Telefone 2</th>  "); 
		contentString.append(" <th data-options=\"field:'email',width:90\">Email</th>   ");
		contentString.append(" <th data-options=\"field:'prioridade',width:50,align:'center'\">Prioridade</th>   ");
		contentString.append(" </tr>   ");
		contentString.append(" </thead> ");  
		contentString.append(" </table>  "); 
//		contentString.append(" </div>"); 
		contentString.append(" </div>"); 	
		contentString.append(" <div title=\"Eventos\"   style=\"padding:5px;width:99%;height:99%\"> ");
//		contentString.append(" <div class=\"easyui-layout\" style=\"width:auto;height:auto\"> ");
		contentString.append(" <table id=\"dgHistorico\" class=\"easyui-datagrid\"  singleSelect=\"true\" style=\"width:1020px;height:220px;color:#fff;\" ");  
		contentString.append(" data-options=\"fitColumns:true,singleSelect:true,toolbar:'#tb'\"> ");  
		contentString.append(" <thead> ");  
		contentString.append(" <tr> ");  
		contentString.append(" <th data-options=\"field:'prioridade',width:15,align:'center',styler:cellStyler\"></th>  "); 
		contentString.append(" <th data-options=\"field:'dtRecebido',width:85,align:'center'\">Recebido</th>  "); 
		contentString.append(" <th data-options=\"field:'cdEvento',width:25\">Evento</th>  "); 
		contentString.append(" <th data-options=\"field:'nuFechamento',width:20\">Aux</th>  "); 
		contentString.append(" <th data-options=\"field:'tipoEvento',width:200\">Descrição</th>   ");
		contentString.append(" <th data-options=\"field:'dtEspera',width:85\">Em Espera</th>  "); 
		contentString.append(" <th data-options=\"field:'dtVtrDeslocamento',width:85\">Deslocamento</th>   ");
		contentString.append(" <th data-options=\"field:'dtVtrLocal',width:85,align:'center'\">No Local</th>   ");
		contentString.append(" <th data-options=\"field:'dtFechamento',width:85,align:'center'\">Fechamento</th>   ");
		contentString.append(" <th data-options=\"field:'obsFechamento',width:15,align:'center',formatter:formatA\">Obs</th>   ");
		contentString.append(" </tr>   ");
		contentString.append(" </thead> ");  
		contentString.append(" </table>  "); 
	
		contentString.append(" <div id=\"tb\" style=\"padding:5px;height:auto\"> ");   
		contentString.append(" <div>  Mostrar últimos: ");
	    contentString.append(" <select id='cbEvento'  class=\"easyui-combobox\" panelHeight=\"auto\" editable=\"false\" style=\"width:100px\"> ");
	    contentString.append(" <option value=\"1\">1</option> ");
	    contentString.append(" <option value=\"2\">2</option> ");
	    contentString.append(" <option value=\"3\">3</option> ");
	    contentString.append(" <option value=\"5\">5</option> ");
	    contentString.append(" <option value=\"10\">10</option> ");
	    contentString.append(" </select> ");
	    contentString.append("     dia(s)  <a href='javascript:void(0)' class=\"easyui-linkbutton\" iconCls=\"icon-search\" onclick='getAtualizaEventos("+this.callEventoVO.getCodigoHistorico()+")'>Pesquisar</a>" );
	    contentString.append(" </div> ");
//		contentString.append(" </div>"); 
		contentString.append(" </div>"); 
		contentString.append(" </div>"); 
		contentString.append(" <div title=\"Log\" style=\"padding:5px;width:99%;height:99%\"> ");
//		contentString.append(" <div class=\"easyui-layout\" style=\"width:auto;height:auto\"> ");
		contentString.append(" <div class=\"easyui-tabs\" id=\"tabLogs\"  style=\"width:1020px;height:220px\"> ");
		contentString.append(" <div title=\"Log Evento\" data-options=\"tools:'#p-tools'\" style=\"padding:5px\"> ");
		contentString.append("  <div id=\"p-tools\"> ");
		contentString.append("     <a href=\"javascript:void(0)\" class=\"icon-mini-refresh\" onclick=\"limpaLog()\"></a> ");
		contentString.append("  </div> ");
		contentString.append(" <textarea  id='taLog' class=\"easyui-validatebox textbox\" style=\"overflow:auto;resize:none;width:720px;height:120px;font-family: Arial;font-size: 9pt\" rows=\"4\" cols=\"100\"> </textarea> ");
		contentString.append("		</p>");
		
		String frase1 = "Deslocado Atendente";
		String frase2 = "Atendente no local realizando a verificação.";
		String frase3 = "A DESATIVAÇÃO DO SISTEMA DE ALARME FORA DO HORÁRIO PROGRAMADO FOI ATENDIDO NO EVENTO DE DISPARO SEGUIDO DE DESARME.";
		String frase4 = "O DISPARO SEGUIDO DE DESARME FOI ATENDIDO NO EVENTO DE DESATIVAÇÃO DO SISTEMA DE ALARME FORA DO HORÁRIO PROGRAMADO.";
		String frase5 = "Local com ordem de serviço aberta, aguardando conclusão.";
		String frase6 = "Log Gerência.";
		String frase7 = "EVENTO FINALIZADO APÓS SISTEMA DE ALARME ATIVADO";
		String frase8 = "Feito contato com o(a) Sr.(a) , o(a) qual informou estar tudo bem no local. Confirmada palavra-chave.";
		String frase9 = "Contato Telefônico do local sem exito.";
		String frase10 = "sistema Ativado () sim () não - Houve Disparos () sim () não - O AA ou SUP identificou o arrombamento () sim () não - Houve Danos da Central de Alarme () sim () não. -PGO ()sim ()não";
		String frase11 = "A NÃO ATIVAÇÃO DO SISTEMA DE ALARME FOI ATENDIDO NO EVENTO DE DESATIVAÇÃO DO SISTEMA DE ALARME FORA DO HORÁRIO PROGRAMADO.";
		String frase12 = "Restaurou comunicação.";
		String frase13 = "Disparo provocado pelo cliente, (condominios)";
		String frase14 = "Qual o motivo de não ter ocorrido o disparo? Relato completo Abaixo:";
		String frase15 = "Vistoria feita no local sem alteração.";
		
		
		contentString.append("			<a href='javascript:void(0)' title=\""+frase1+"\"   class='easyui-linkbutton' onclick='adicionaLog(\""+frase1+"\")'>1</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase2+"\"   class='easyui-linkbutton' onclick='adicionaLog(\""+frase2+"\")'>2</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase3+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase3+"\")'>3</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase4+"\" class='easyui-linkbutton' onclick='adicionaLog(\""+frase4+"\")'>4</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase5+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase5+"\")'>5</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase6+"\" class='easyui-linkbutton' onclick='adicionaLog(\""+frase6+"\")'>6</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase7+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase7+"\")'>7</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase8+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase8+"\")'>8</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase9+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase9+"\")'>9</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase10+"\" class='easyui-linkbutton' onclick='adicionaLog(\""+frase10+"\")'>10</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase11+"\" class='easyui-linkbutton' onclick='adicionaLog(\""+frase11+"\")'>11</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase12+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase12+"\")'>12</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase13+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase13+"\")'>13</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase14+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase14+"\")'>14</a>");
		contentString.append("			<a href='javascript:void(0)' title=\""+frase15+"\"  class='easyui-linkbutton' onclick='adicionaLog(\""+frase15+"\")'>15</a>");
		contentString.append("		</p>");
		contentString.append("			<a href='javascript:void(0)' title=\"Sucesso\"  class='easyui-linkbutton' onclick='adicionaLog(\"Ligação realizada com sucesso"+"\")'>Sucesso!</a>");
		contentString.append("			<a href='javascript:void(0)' title=\"Falha\"  class='easyui-linkbutton' onclick='adicionaLog(\"Ligação falha!"+"\")'>Falha</a>");
		contentString.append("			<a href='javascript:void(0)' title=\"Não atende\"  class='easyui-linkbutton' onclick='adicionaLog(\"Tentativa realizada mas não atende!"+"\")'>Não atende</a>");
		contentString.append("			<a href='javascript:void(0)' title=\"Ocupado\"  class='easyui-linkbutton' onclick='adicionaLog(\"Tentativa realizada mas ocupado!"+"\")'>Ocupado</a>");
		contentString.append("			<a href='javascript:void(0)' title=\"Caixa postal\"  class='easyui-linkbutton' onclick='adicionaLog(\"Tentativa realizada mas caixa postal!"+"\")'>Caixa postal</a>");
		contentString.append("			<a href='javascript:void(0)' title=\"Não Existe\"  class='easyui-linkbutton' onclick='adicionaLog(\"Tentativa realizada mas numero não Existe"+"\")'>Não Existe</a>");

		contentString.append("		</p>");

		contentString.append(" </div> ");
		contentString.append(" <div title=\"Log da Gerência\" style=\"padding:10px\"> ");
		contentString.append(" <p>Não Utilizar.</p> ");
		contentString.append(" </div> ");
		
        contentString.append(" </div> ");
		contentString.append(" </div>"); 
		contentString.append(" <div title=\"<span ><img  style='border:none' id='imgAnotacao' />Anotações</span>\" id=\"tabAnotacao\" style=\"padding:5px;width:99%;height:99%\"> ");
		contentString.append("  <table  style=\"width:1020px;height:220px\" ");
		contentString.append(" <tr> ");
		contentString.append(" <td> <span><legend><b>Anotações temporárias.</b></legend></span> <textarea readonly id='taObsTemp' class=\"textAreaCur\"  style=\"overflow:auto;resize:none;width:430px;height:180px;font-family: Arial;font-size: 9pt\" rows=\"4\" cols=\"50\"> </textarea> <td> ");
		contentString.append(" <td> <span><legend><b>Providências a serem tomadas.</b></legend></span><textarea readonly id='taObsProvidencia' class=\"textAreaCur\" style=\"overflow:auto;resize:none;width:430px;height:180px;font-family: Arial;font-size: 9pt\" rows=\"4\" cols=\"50\"> </textarea> </td>");
		contentString.append(" </tr> ");
		contentString.append(" </table> ");
		contentString.append(" </div>"); 
		
		contentString.append(" <div title=\"Usuários\" style=\"padding:5px;width:99%;height:99%\"> ");
		contentString.append(" <table id=\"dgUsuarios\" class=\"easyui-datagrid\"  singleSelect=\"true\" style=\"width:1020px;height:220px\" ");  
		contentString.append(" data-options=\"fitColumns:true,singleSelect:true\"> ");  
		contentString.append(" <thead> ");  
		contentString.append(" <tr> ");  
		contentString.append(" <th data-options=\"field:'idAcesso',width:50,align:'center'\">Código</th>  "); 
		contentString.append(" <th data-options=\"field:'nome',width:150\">Nome</th>  "); 
		contentString.append(" <th data-options=\"field:'nuRegistroGeral',width:60\">Rg</th>   ");
		contentString.append(" <th data-options=\"field:'nuCpf',width:60\">Cpf</th>  "); 
		contentString.append(" <th data-options=\"field:'senha',width:90\">Senha</th>   ");
		contentString.append(" <th data-options=\"field:'telefone',width:50,align:'center'\">Telefone</th>   ");
		contentString.append(" <th data-options=\"field:'obs',width:50,align:'center',formatter:formatA\">Obs</th>   ");
		contentString.append(" </tr>   ");
		contentString.append(" </thead> ");  
		contentString.append(" </table>  "); 

		contentString.append(" </div>"); 
		
		contentString.append(" </div>");
		
	
		contentString.append(" </fieldset> ");
        contentString.append(" </div>"); 
        contentString.append(" </p>");
        contentString.append(" <div align='right'>"); 
        

        if (callEventoVO.getCodigoEvento().equals("XVID") || callEventoVO.getCodigoEvento().equals("XVD2")){
            contentString.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-save'\" style='display:none' id='eventCapAguardeImgBtn' class='easyui-linkbutton'>Salvando...</a>");
            contentString.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-save'\" id='eventCapImgBtn' class='easyui-linkbutton' onclick='capturarImagens("+callEventoVO.getCodigoHistorico()+")'>Capturar Imagens</a>");
        }
        
        contentString.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-ok'\" id='eventOkBtn' class='easyui-linkbutton' onclick='popUpFecharEvento()'>Fechar Evento</a>");

//		contentString
//				.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-ok'\" id='eventOkBtn' class='easyui-linkbutton' onclick='eventCallHandler(\""+this.callEventoVO.getCodigoHistorico()+"\", true,true)'>Fechar Evento</a>");
//		
        contentString.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-redo'\" id='eventAltBtn' class='easyui-linkbutton' onclick='eventCallHandler(\""+this.callEventoVO.getCodigoHistorico()+"\", true, false, true)'>Em Espera com Alteração</a>");
        
        contentString.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-redo'\" id='eventNOPBtn' class='easyui-linkbutton' onclick='eventCallHandler(\""+this.callEventoVO.getCodigoHistorico()+"\", true, false, false)'>Em Espera</a>");
		contentString
				.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-cancel'\" id='eventNOkBtn' class='easyui-linkbutton' onclick='eventCallHandler(\""+this.callEventoVO.getCodigoHistorico()+"\", false, true, false)'>Tratar Sigma</a>");
		if(callEventoVO.getCodigoEvento().equals("XXX1"))
		{
		contentString
				.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-add'\" id='osBtn' class='easyui-linkbutton' onclick='editOS()'>Abrir OS</a>");
		}
		contentString
				.append("			<a href='javascript:void(0)' data-options=\"iconCls:'icon-reload'\" id='eventProBtn' class='easyui-linkbutton' onclick='eventCallHandler(\""+this.callEventoVO.getCodigoHistorico()+"\", false, false, false)'>Próximo Número</a>");
			
		  contentString.append(" </div>"); 
		contentString.append("		</p>");
		contentString.append("	</div>");
		contentString.append("</div>");
			

		this.infoWindow = contentString.toString();
	}

	public Integer getUltimoNumero()
	{
		return ultimoNumero;
	}

	public void setUltimoNumero(Integer ultimoNumero)
	{
		this.ultimoNumero = ultimoNumero;
	}
	
	
}
