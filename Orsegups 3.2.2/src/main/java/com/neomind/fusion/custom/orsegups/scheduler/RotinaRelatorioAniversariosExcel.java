package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class RotinaRelatorioAniversariosExcel implements CustomJobAdapter{
    
    public void execute(CustomJobContext ctx) {
	
	HSSFWorkbook workbook = new HSSFWorkbook(); 
	HSSFSheet abaAniversarios = workbook.createSheet("Aniversarios");
	
	CellStyle style = workbook.createCellStyle();
	Font font = workbook.createFont();
	font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	style.setFont(font);
	
	Row row = abaAniversarios.createRow(0);
	
	Cell cellDataNasc = row.createCell(0);
	cellDataNasc.setCellStyle(style);
	cellDataNasc.setCellValue("Data Nascimento");

	Cell cellEmpresa = row.createCell(1);
	cellEmpresa.setCellStyle(style);
	cellEmpresa.setCellValue("Empresa");
	
	Cell cellMatricula = row.createCell(2);
	cellMatricula.setCellStyle(style);
	cellMatricula.setCellValue("Matrícula");
	
	Cell cellNome = row.createCell(3);
	cellNome.setCellStyle(style);
	cellNome.setCellValue("Nome");
	
	Cell cellOrganograma = row.createCell(4);
	cellOrganograma.setCellStyle(style);
	cellOrganograma.setCellValue("Organograma");
	
	Cell cellRegional = row.createCell(5);
	cellRegional.setCellStyle(style);
	cellRegional.setCellValue("Regional");
	
	Cell cellNomeRegional = row.createCell(6);
	cellNomeRegional.setCellStyle(style);
	cellNomeRegional.setCellValue("Nome Regional");
	
	Cell cellCargo = row.createCell(7);
	cellCargo.setCellStyle(style);
	cellCargo.setCellValue("Cargo");
	
	FileOutputStream arquivo  = null;
	
	Connection conn = null;
	
	StringBuilder sql = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	GregorianCalendar data = new GregorianCalendar();
	
	int ano = data.get(Calendar.YEAR);
	int mes = data.get(Calendar.MONTH)+1;
	
	String nomeArquivo = "C:/teste/aniversarios-"+mes+"-"+ano+".xls";
	
	try{
	    sql.append("DECLARE @DATACONSULTA INT ");
	    sql.append("SET @DATACONSULTA ="+ano+" ");
	    sql.append("DECLARE @MES INT ");
	    sql.append("SET @MES ="+mes);
	    sql.append("SELECT convert(varchar,R034FUN.datnas,103) DATNAS, R030EMP.ApeEmp, R034FUN.NumCad, R034FUN.NomFun, R016ORN.USU_GerOrn, ");
	    sql.append("USU_T200REG.usu_codreg, USU_T200REG.USU_NOMREG,R024CAR.TitCar ");
	    sql.append("FROM R034FUN, R016ORN, R016HIE, R030EMP, USU_T200REG, R024CAR ");
	    sql.append("WHERE R016ORN.TabOrg = R034FUN.TabOrg ");
	    sql.append("AND R016ORN.NumLoc = R034FUN.NumLoc ");
	    sql.append("AND R016HIE.TabOrg = R034FUN.TabOrg ");
	    sql.append("AND R016HIE.NumLoc = R034FUN.NumLoc ");
	    sql.append("AND R030EMP.NumEmp = R034FUN.NumEmp ");
	    sql.append("AND USU_T200REG.USU_CODREG = R016ORN.USU_CODREG ");
	    sql.append("AND R024CAR.CODCAR = R034FUN.CODCAR ");
	    sql.append("and MONTH(R034FUN.datnas) = @MES ");
	    sql.append("AND R034FUN.SitAfa <> 7  AND R034FUN.SitAfa <> 3 AND R034FUN.SitAfa <> 4 AND R034FUN.SitAfa <> 13 AND R034FUN.SitAfa <> 123 ");
	    sql.append("AND R034FUN.TipCol in(1,2) ");
	    sql.append("AND R024CAR.EstCar = 1 ");
	    sql.append("GROUP BY  R034FUN.DatAdm ,R030EMP.ApeEmp, R034FUN.NumCad, R034FUN.NomFun, R016ORN.USU_GerOrn, R034FUN.datnas, ");
	    sql.append("USU_T200REG.usu_codreg, USU_T200REG.USU_NOMREG, R024CAR.TitCar ");
	    sql.append("ORDER BY R034FUN.DatAdm,  R034FUN.nomfun ");
	    
	    conn = PersistEngine.getConnection("VETORH");
	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();
	    
	    int rowNum = 1;
	    while (rs.next()){
		
		Row linha = abaAniversarios.createRow(rowNum);
		
		Cell dataNas = linha.createCell(0);
		String dataNascimento = rs.getString("DATNAS");
		dataNas.setCellValue(dataNascimento);
		
		Cell empresa = linha.createCell(1);
		empresa.setCellValue(rs.getString("ApeEmp") == null ? "Vazio" : rs.getString("ApeEmp"));
		
		Cell matricula = linha.createCell(2);
		matricula.setCellValue(rs.getLong("NumCad"));
		
		Cell nome = linha.createCell(3);
		nome.setCellValue(rs.getString("NomFun") == null ? "Vazio" : rs.getString("NomFun"));
		
		Cell organograma = linha.createCell(4);
		organograma.setCellValue(rs.getString("USU_GerOrn") == null ? "Vazio" : rs.getString("USU_GerOrn"));
		
		Cell regional = linha.createCell(5);
		regional.setCellValue(rs.getLong("usu_codreg"));
		
		Cell nomeRegional = linha.createCell(6);
		nomeRegional.setCellValue(rs.getString("USU_NOMREG") == null ? "Vazio" : rs.getString("USU_NOMREG"));
		
		Cell cargo = linha.createCell(7);
		cargo.setCellValue(rs.getString("TitCar") == null ? "Vazio" : rs.getString("TitCar"));
		
		rowNum++;
	    }
	    for (int i= 0; i<8; i++){
		abaAniversarios.autoSizeColumn(i);
	    }
	    
	    arquivo = new FileOutputStream(new File(nomeArquivo));
	    workbook.write(arquivo);
	    arquivo.close();
	    
	}catch (SQLException e){
	    e.printStackTrace();    
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    System.out.println("Arquivo não encontrado!");
	} catch (IOException e) {
	    e.printStackTrace();
	    System.out.println("Erro na edição do arquivo!");    
	}finally{
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}	
    }
}
