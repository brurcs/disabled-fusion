package com.neomind.fusion.custom.orsegups.converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class FAPHistoricoTarefasAgrupadasConverter extends StringConverter
{
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		List<NeoObject> listaProcessos = new ArrayList<NeoObject>();
		EntityWrapper processEntityWrapper = new EntityWrapper(field.getForm().getObject());
		Long codigoAplicacao = (Long) processEntityWrapper.findValue("aplicacaoPagamento.codigo");
		StringBuilder textoTable = new StringBuilder();

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		QLEqualsFilter aprovadoFilter = new QLEqualsFilter("aprovado", true);
		QLGroupFilter statusGroupFilter = new QLGroupFilter("OR");
		QLOpFilter eliminateItSelf = new QLOpFilter("codigoTarefa", "=", (String) processEntityWrapper.findValue("wfprocess.code"));
		QLEqualsFilter runningFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal());
		QLEqualsFilter finishedFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.finished.ordinal());
		statusGroupFilter.addFilter(runningFilter);
		statusGroupFilter.addFilter(finishedFilter);
		groupFilter.addFilter(statusGroupFilter);
		groupFilter.addFilter(eliminateItSelf);
		groupFilter.addFilter(aprovadoFilter);

		listaProcessos = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), groupFilter, -1, -1, "wfprocess.code ASC");

		textoTable.append("<script>");
		textoTable.append("				function showTarefa(neoid) { ");
		textoTable.append("			    	var url = '" + PortalUtil.getBaseURL() + "bpm/workflow_search.jsp?id='+neoid; ");
		textoTable.append("			    	var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,''); ");
		textoTable.append("			    	var win = window.name;  ");
		textoTable.append("			        NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win); ");
		textoTable.append("			    } ");
		textoTable.append("</script>");
		textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
		textoTable.append("			<tr style=\"cursor: auto\">");
		textoTable.append("				<th style=\"cursor: auto\">Tarefa</th>");
		textoTable.append("				<th style=\"cursor: auto\">Data</th>");
		if (codigoAplicacao != null)
		{
			if (codigoAplicacao == 1L)
			{
				textoTable.append("				<th style=\"cursor: auto\">Placa</th>");
				textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Tipo Veículo</th>");
			}
			if ((codigoAplicacao == 2L) || (codigoAplicacao == 3L) || (codigoAplicacao == 4L) || 
				(codigoAplicacao == 5L) || (codigoAplicacao == 6L) || (codigoAplicacao == 7L) || 
				(codigoAplicacao == 8L) || (codigoAplicacao == 9L))
			{
				textoTable.append("				<th style=\"cursor: auto\">Conta do Cliente</th>");
				textoTable.append("				<th style=\"cursor: auto\">Razão Social</th>");
			}
		}
		textoTable.append("				<th style=\"cursor: auto\">Valor Total</th>");
		textoTable.append("				<th style=\"cursor: auto\">Status</th>");
		textoTable.append("			</tr>");
		textoTable.append("			<tbody>");

		BigDecimal valorTotalHistoricoFap = new BigDecimal(0.00);
		BigDecimal valorTotalHistoricoProduto = new BigDecimal(0.00);
		BigDecimal valorTotalHistoricoServico = new BigDecimal(0.00);

		if (NeoUtils.safeIsNotNull(listaProcessos) && !listaProcessos.isEmpty())
		{
			for (NeoObject obj : listaProcessos)
			{
				EntityWrapper registroWrapper = new EntityWrapper(obj);
				Long neoId = (Long) registroWrapper.findValue("wfprocess.neoId");
				String code = (String) registroWrapper.findValue("wfprocess.code");
				GregorianCalendar dataRaw = (GregorianCalendar) registroWrapper.findValue("dataSolicitacao");
				String placa = "";
				String tipoVeiculo = "";
				String contaCliente = "";
				String razaoSocial = "";
				BigDecimal valorTotalFap = new BigDecimal (0.00);
				
				List<NeoObject> itens = (List<NeoObject>) registroWrapper.findValue("itemOrcamento");
				if (itens != null && !itens.isEmpty()) 	{
					for (NeoObject item : itens) {
						
						EntityWrapper wrpItem = new EntityWrapper(item);
						
						if(wrpItem.findValue("tipoItem.descricao") != null && ((String)wrpItem.findValue("tipoItem.descricao")).equals("PRODUTO"))
						{						
							valorTotalHistoricoProduto = valorTotalHistoricoProduto.add((BigDecimal)wrpItem.findValue("valor"));	
						}					
						if(wrpItem.findValue("tipoItem.descricao") != null && ((String)wrpItem.findValue("tipoItem.descricao")).equals("SERVIÇO")){
							valorTotalHistoricoServico = valorTotalHistoricoServico.add((BigDecimal)wrpItem.findValue("valor"));	
						}
					}
				}
				
				if (codigoAplicacao == 1L)
				{
					placa = (String) registroWrapper.findValue("viatura.placa");
					tipoVeiculo = (String) registroWrapper.findValue("tipoViatura");
				}
				if ((codigoAplicacao == 2L) || (codigoAplicacao == 3L) || (codigoAplicacao == 4L) || 
					(codigoAplicacao == 5L) || (codigoAplicacao == 6L) || (codigoAplicacao == 7L) ||
					(codigoAplicacao == 8L) || (codigoAplicacao == 9L))
				{
					contaCliente = (String) registroWrapper.findValue("codigoConta");
					razaoSocial = (String) registroWrapper.findValue("razaoSocialConta");
				}
				valorTotalFap = (BigDecimal) registroWrapper.findValue("valorTotal");

				if (placa == null)
					placa = "";
				if (tipoVeiculo == null)
					tipoVeiculo = "";
				if (contaCliente == null)
					contaCliente = "";
				
				valorTotalHistoricoFap = valorTotalHistoricoFap.add((BigDecimal) valorTotalFap);				

				String status = "";
				ProcessState processState = (ProcessState) registroWrapper.findValue("wfprocess.processState");
				if (processState == ProcessState.running)
				{
					status = "Em andamento";
				}
				else if (processState == ProcessState.finished)
				{
					status = "Encerrado";
				}

				textoTable.append("		<tr>");
				textoTable.append("			<td><a href=\"javascript:showTarefa('" + neoId + "');\"><img class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a>&nbsp;" + code + "</td>");
				textoTable.append("			<td>" + NeoUtils.safeDateFormat(dataRaw, "dd/MM/yyyy") + "</td>");
				if (codigoAplicacao == 1L)
				{
					textoTable.append("			<td>" + placa + "</td>");
					textoTable.append("			<td style=\"white-space: normal\">" + tipoVeiculo + "</td>");
				}
				if ((codigoAplicacao == 2L) || (codigoAplicacao == 3L) || (codigoAplicacao == 4L) || 
					(codigoAplicacao == 5L) || (codigoAplicacao == 6L) || (codigoAplicacao == 7L) || 
					(codigoAplicacao == 8L) || (codigoAplicacao == 9L))
				{
					textoTable.append("			<td style=\"white-space: normal\">" + contaCliente + "</td>");
					textoTable.append("			<td style=\"white-space: normal\">" + razaoSocial + "</td>");
				}				
				textoTable.append("			<td>" + NeoUtils.safeOutputString((BigDecimal) valorTotalFap) + "</td>");
				textoTable.append("			<td>" + status + "</td>");
				textoTable.append("		</tr>");
			}
			
		}
		else
		{
			textoTable.append("		<tr>");
			textoTable.append("			<td colspan='6'>Nenhum registro</td>");
			textoTable.append("		</tr>");
		}

		textoTable.append("		<tr>");
		textoTable.append("				<td colspan=5 style=\"text-align: right\">" + "Total Geral Produto:" + "</td>");
		textoTable.append("				<td colspan=1 style=\"text-align: left\">" + NeoUtils.safeOutputString((BigDecimal) valorTotalHistoricoProduto) + "</td>");
		textoTable.append("		</tr>");
		textoTable.append("		<tr>");
		textoTable.append("				<td colspan=5 style=\"text-align: right\">" + "Total Geral Serviço:" + "</td>");
		textoTable.append("				<td colspan=1 style=\"text-align: left\">" + NeoUtils.safeOutputString((BigDecimal) valorTotalHistoricoServico) + "</td>");
		textoTable.append("		</tr>");
		textoTable.append("		<tr>");
		textoTable.append("				<td colspan=5 style=\"text-align: right\">" + "Total Geral:" + "</td>");
		textoTable.append("				<td colspan=1 style=\"text-align: left\">" + NeoUtils.safeOutputString((BigDecimal) valorTotalHistoricoFap) + "</td>");
		textoTable.append("		</tr>");
		textoTable.append("			</tbody>");
		textoTable.append("		</table>");

		return textoTable.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
