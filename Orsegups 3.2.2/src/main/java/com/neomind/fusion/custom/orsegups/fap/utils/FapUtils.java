package com.neomind.fusion.custom.orsegups.fap.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.mail.MultiPartEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTaticoDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.FornecedorDTO;
import com.neomind.fusion.custom.orsegups.fap.exception.NaoEncontradoException;
import com.neomind.fusion.custom.orsegups.fap.factory.FapFactory;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.eform.EFormError;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/**
 * Utilidades para o fluxo de FAP
 * 
 * @author diogo.silva
 *
 */
public class FapUtils
{
	/**
	 * Recupera o usuário solicitante da FAP de acordo com a regional do técnico/tático
	 * 
	 * @param siglaRegional (String)
	 * @return NeoUser
	 */
	public static NeoUser getSolicitante(String siglaRegional) throws NaoEncontradoException, Exception
	{
		String sigla = siglaRegional;
		String userCode = null;
		NeoPaper papel = null;
		NeoUser solicitante = null;
		String paperCode = null;

		/* Variáveis usadas em testes, tanto locais quanto em produção */
		boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);
		String usuarioTestes = FAPParametrizacao.findParameter("usuarioTestes");

		try
		{

			paperCode = "FAPSolicitante" + sigla;
			papel = FapFactory.neoPaperFactory("code", paperCode);

			if (papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers())
				{
					userCode = user.getCode();
					break;
				}

				solicitante = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userCode));

				if (solicitante.isActive())
				{
					if (debug)
					{ // Para testes em produção
						solicitante = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioTestes));
						return solicitante;
					}
					else
					{
						return solicitante;
					}
				}
				else
				{
					throw new NaoEncontradoException("Nenhum usuário ativo no papel " + papel.getName());
				}
			}
			else
			{
				throw new NaoEncontradoException("O papel " + paperCode + " não existe ou não possui usuários vinculados.");
			}
		}
		catch (NaoEncontradoException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao recuperar o solicitante para esta FAP. Favor contatar a TI.");
		}
	}

	public static NeoUser getAprovador(String siglaRegional) throws NaoEncontradoException, Exception
	{
		String sigla = siglaRegional;
		String userCode = null;
		NeoPaper papel = null;
		NeoUser aprovador = null;
		String paperCode = null;

		/* Variáveis usadas em testes, tanto locais quanto em produção */
		boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);
		String usuarioTestes = FAPParametrizacao.findParameter("usuarioTestes");

		try
		{
			paperCode = "FAPAprovador" + sigla;
			papel = FapFactory.neoPaperFactory("code", paperCode);

			if (papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers())
				{
					userCode = user.getCode();
					break;
				}

				aprovador = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userCode));

				if (aprovador.isActive())
				{
					if (debug)
					{ // Para testes em produção
						aprovador = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioTestes));
						return aprovador;
					}
					else
					{
						return aprovador;
					}
				}
				else
				{
					throw new NaoEncontradoException("Nenhum usuário ativo no papel " + papel.getName());
				}
			}
			else
			{
				throw new NaoEncontradoException("O papel " + paperCode + " não existe ou não possui usuários vinculados.");
			}
		}
		catch (NaoEncontradoException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao recuperar o aprovador para esta FAP. Favor contatar a TI.");
		}
	}

	public static NeoUser getSolicitanteTecnico(String siglaRegional) throws NaoEncontradoException, Exception
	{
		/* Variáveis usadas em testes, tanto locais quanto em produção */
		boolean debug = (FAPParametrizacao.findParameter("debug", true).equals("1") ? true : false);
		String usuarioTestes = FAPParametrizacao.findParameter("usuarioTestes", true);

		if (debug)
			return PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioTestes));

		try
		{
			NeoObject noRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), new QLEqualsFilter("siglaRegional", siglaRegional));
			if (noRegional == null)
				throw new NaoEncontradoException("Não foi encontrado nenhuma regional para a sigla informada");

			NeoObject noUsuariosRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPTecnicoUsuarios"), new QLEqualsFilter("regional", noRegional));
			if (noUsuariosRegional == null)
				throw new NaoEncontradoException("Não foi encontrado um solicitante para a regional informada");

			return new EntityWrapper(noUsuariosRegional).findGenericValue("solicitante");
		}
		catch (NaoEncontradoException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao recuperar o solicitante para esta FAP. Favor contatar a TI.");
		}
	}

	public static NeoUser getAprovadorTecnico(String siglaRegional) throws NaoEncontradoException, Exception
	{
		/* Variáveis usadas em testes, tanto locais quanto em produção */
		boolean debug = (FAPParametrizacao.findParameter("debug", true).equals("1") ? true : false);
		String usuarioTestes = FAPParametrizacao.findParameter("usuarioTestes", true);

		if (debug)
			return PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioTestes));

		try
		{
			NeoObject noRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), new QLEqualsFilter("siglaRegional", siglaRegional));
			if (noRegional == null)
				throw new NaoEncontradoException("Não foi encontrado nenhuma regional para a sigla informada");

			NeoObject noUsuariosRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPTecnicoUsuarios"), new QLEqualsFilter("regional", noRegional));
			if (noUsuariosRegional == null)
				throw new NaoEncontradoException("Não foi encontrado um aprovador para a regional informada");

			return new EntityWrapper(noUsuariosRegional).findGenericValue("aprovador");
		}
		catch (NaoEncontradoException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao recuperar o solicitante para esta FAP. Favor contatar a TI.");
		}
	}

	/**
	 * Compara o dia atual com o dia corte do fornecedor para avaliar se é dia
	 * de gerar a FAP para o mesmo
	 * 
	 * @param diaAtual (Integer)
	 * @param diaCorte (Integer)
	 * @return boolean
	 */
	public static boolean compararDias(Integer diaAtual, Integer diaCorte)
	{
		Integer atual = diaAtual;
		Integer corte = diaCorte;

		if (corte.equals(atual))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Retorna a sigla da regional de acordo com o cadastro do técnico.
	 * A sigla está nos 3 (três) primeiros caracteres do nome.
	 * 
	 * @param nomeRota (String)
	 * @return siglaRegional (String)
	 */
	public static String getSiglaRegional(String name) throws NaoEncontradoException, Exception
	{
		String nome = name;
		String sigla = null;

		try
		{
			if (NeoUtils.safeIsNotNull(nome))
			{
				sigla = nome.substring(0, 3);
				return sigla;
			}
			else
			{
				throw new NaoEncontradoException("O nome do técnico para esta FAP não foi localizado. Rota: " + nome);
			}
		}
		catch (NaoEncontradoException e)
		{
			e.printStackTrace();
			throw new NaoEncontradoException("Erro ao pesquisar a sigla da regional " + sigla + ". Verifique o cadastro da rota. Rota: " + nome + ". Erro: " + e.getMessage());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new NaoEncontradoException("Erro ao pesquisar a sigla da regional " + sigla + ". Verifique o cadastro da rota. Rota: " + nome + ". Erro: " + e.getMessage());
		}
	}

	/**
	 * Avança a atividade "Finalizar Orçamento" do fluxo de FAP.
	 * 
	 * @param code (String)
	 * @param user (NeoUser)
	 * @return boolean
	 * @throws Exception
	 */
	public static boolean avancaAtividade(String code) throws WorkflowException
	{
		WFProcess processo = null;
		String codigo = code;
		Activity atividade = null;
		String mensagemErro = null;

		try
		{
			processo = PersistEngine.getNeoObject(WFProcess.class, new QLRawFilter("code = '" + codigo + "' and saved = 1 and processstate = 0 and model.name = 'F001 - FAP - AutorizacaoDePagamento'"));

			final List<Activity> activities = processo.getOpenActivities();
			// Existirá somente uma atividade em aberto nesta etapa do processo
			atividade = activities.get(0);

			if (NeoUtils.safeIsNotNull(atividade))
			{
				if (atividade.getActivityName().equals("Finalizar Orçamento") && atividade instanceof UserActivity)
				{
					((UserActivity) atividade).getTaskList().get(0);
					OrsegupsWorkflowHelper.finishTaskByActivity(atividade);
					return true;
				}
				return false;
			}
			else
			{
				return false;
			}
		}
		catch (NaoEncontradoException e)
		{
			e.printStackTrace();
			mensagemErro = e.getMessage();
			throw new WorkflowException(mensagemErro);
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			mensagemErro = e.getErrorList().get(0).getI18nMessage();
			throw new WorkflowException(mensagemErro);
		}
	}

	/**
	 * FAP aplicação técnico.
	 * Envia tarefa para avisar ao responsável que não foi gerada FAP para algum fornecedor
	 * por conta de algum problema ou erro de cadastro.
	 * 
	 * @param lista (List<FornecedorDTO>)
	 */
	public static void naoGerouTarefaTecnicos(List<FornecedorDTO> lista)
	{
		List<FornecedorDTO> listaFornecedores = lista;

		if (listaFornecedores.size() > 0)
		{

			for (FornecedorDTO fornecedor : listaFornecedores)
			{

				try
				{
					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

					GregorianCalendar prazo = new GregorianCalendar();
					prazo.add(Calendar.DAY_OF_MONTH, 2);
					prazo.add(Calendar.HOUR_OF_DAY, 23);
					prazo.add(Calendar.MINUTE, 59);
					prazo.add(Calendar.SECOND, 59);

					String nomeFornecedor = fornecedor.getNomeFornecedor();
					String codigoFornecedor = fornecedor.getCodigoFornecedor();
					Integer dataCorte = fornecedor.getDataCorte();

					Long codigoTecnico = fornecedor.getCodigoTecnico();
					String nomeTecnico = fornecedor.getNomeTecnico();

					String regional = FapUtils.getSiglaRegional(nomeTecnico);
					String executor = FapUtils.getSolicitanteTecnico(regional).getAsString();

					List<String> listaDeErros = fornecedor.getListaErros();

					String titulo = "FAP Técnico - Iniciar tarefa";

					StringBuilder sb = new StringBuilder();
					sb.append("A tarefa para o técnico abaixo não foi aberta: </br>");
					sb.append("<strong> Fornecedor: </strong>" + codigoFornecedor + " - " + nomeFornecedor + "</br>");
					sb.append("<strong> Data Corte: </strong>" + dataCorte + "</br>");
					sb.append("<strong> Técnico: </strong>" + codigoTecnico + " - " + nomeTecnico + "</br>");
					sb.append("<strong> Motivo(s) da não abertura: </strong></br>");
					sb.append("<ul>");
					for (String erro : listaDeErros)
					{
						sb.append("<li style='list-style-type:disc;list-style-position: inside'>" + erro + "</li>");
					}
					sb.append("</ul>");
					sb.append("Favor verificar o(s) motivo(s) informados e tomar as devidas providências.");
					iniciarTarefaSimples.abrirTarefa("rosileny", executor, titulo, sb.toString(), "1", "", prazo);
				}
				catch (Exception e)
				{
					// DO NOTHING
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * FAP aplicação técnico.
	 * Envia e-mail avisando o responsável que não foi possível
	 * avançar a atividade de Finalizar Orçamento da FAP por algum problema ou erro de cadastro.
	 * 
	 * @param lista (List<FornecedorDTO>)
	 */
	public static void naoAvancouAtividadeTecnicos(List<FornecedorDTO> lista)
	{
		List<FornecedorDTO> listaFornecedores = lista;
		MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

		if (listaFornecedores.size() > 0)
		{

			for (FornecedorDTO fornecedor : listaFornecedores)
			{

				try
				{
					MultiPartEmail email = new MultiPartEmail();
					StringBuilder mensagem = new StringBuilder();
					String nomeFornecedor = fornecedor.getNomeFornecedor();
					String codigoFornecedor = fornecedor.getCodigoFornecedor();
					Integer dataCorte = fornecedor.getDataCorte();
					Long codigoTecnico = fornecedor.getCodigoTecnico();
					String nomeTecnico = fornecedor.getNomeTecnico();
					List<String> listaDeErros = fornecedor.getListaErros();
					String regional = fornecedor.getNomeTecnico();

					NeoObject noAprovadorRegional = FapUtils.getAprovadorTecnico(regional);;
					NeoObject noSolicitanteRegional = FapUtils.getSolicitanteTecnico(regional);
					NeoUser aprovador = new EntityWrapper(noAprovadorRegional).findGenericValue("aprovador");
					NeoUser solicitante = new EntityWrapper(noSolicitanteRegional).findGenericValue("solicitante");

					String emailSolicitante = null;
					String emailAprovador = null;
					String bcc = "emailautomatico@orsegups.com.br";

					/* Variáveis usadas em testes, tanto locais quanto em produção */
					boolean debug = (FAPParametrizacao.findParameter("debug", true).equals("1") ? true : false);
					String usuarioTestes = FAPParametrizacao.findParameter("usuarioTestes", true);

					if (debug)
					{
						NeoUser user = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioTestes));
						emailSolicitante = user.getEmail();
						emailAprovador = user.getEmail();
					}
					else
					{
						emailSolicitante = solicitante.getEmail();
						emailAprovador = aprovador.getEmail();
					}

					mensagem.append("A FAP do fornecedor abaixo foi aberta mas a atividade Finalizar Orçamento não foi avançada:</br>");
					mensagem.append("Fornecedor: " + codigoFornecedor + " - " + nomeFornecedor + "</br>");
					mensagem.append("Data Corte: " + dataCorte + "</br>");
					mensagem.append("Técnico: " + codigoTecnico + " - " + nomeTecnico + "</br>");
					mensagem.append("Motivo(s): " + "</br>");

					for (String erro : listaDeErros)
					{
						mensagem.append(erro + "</br>");
					}

					mensagem.append("Favor verificar o(s) motivo(s) informados e tomar as devidas providências.");

					// Preenche as informações do e-mail
					email.addTo(emailSolicitante);
					email.addTo(emailAprovador);
					email.addCc("diogo.silva@orsegups.com.br");
					email.addBcc(bcc);
					email.setSubject("FAP - Atividade não pode ser avançada");
					email.setMsg(mensagem.toString());
					email.setFrom(settings.getFromEMail(), settings.getFromName());
					email.setSmtpPort(settings.getPort());
					email.setHostName(settings.getSmtpServer());
					email.send();

				}
				catch (Exception e)
				{
					// DO NOTHING
				}
			}
		}
	}

	/**
	 * FAP aplicação tático.
	 * Envia tarefa para avisar ao responsável que não foi gerada FAP para algum fornecedor
	 * por conta de algum problema ou erro de cadastro.
	 * 
	 * @param lista (List<FornecedorDTO>)
	 */
	public static void naoGerouTarefaTaticos(List<FornecedorDTO> lista)
	{
		List<FornecedorDTO> listaFornecedores = lista;
		MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

		if (listaFornecedores.size() > 0)
		{

			for (FornecedorDTO fornecedor : listaFornecedores)
			{

				try
				{

					MultiPartEmail email = new MultiPartEmail();
					String nomeFornecedor = fornecedor.getNomeFornecedor();
					String codigoFornecedor = fornecedor.getCodigoFornecedor();

					Long codigoRota = fornecedor.getCodigoRota();
					String nomeRota = fornecedor.getNomeRota();
					String regional = nomeRota.substring(0, 3);
					NeoUser solicitante = FapUtils.getSolicitante(regional);
					NeoUser aprovador = FapUtils.getAprovador(regional);

					String emailSolicitante = null;
					String emailAprovador = null;
					String bcc = "emailautomatico@orsegups.com.br";

					/* Variáveis usadas em testes, tanto locais quanto em produção */
					boolean debug = (FAPParametrizacao.findParameter("debug", true).equals("1") ? true : false);
					String usuarioTestes = FAPParametrizacao.findParameter("usuarioTestes", true);

					if (debug)
					{
						NeoUser user = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioTestes));
						emailSolicitante = user.getEmail();
						emailAprovador = user.getEmail();
					}
					else
					{
						emailSolicitante = solicitante.getEmail();
						emailAprovador = aprovador.getEmail();
					}

					List<String> listaDeErros = fornecedor.getListaErros();

					StringBuilder mensagem = new StringBuilder();
					mensagem.append("A tarefa para o fornecedor abaixo não foi aberta: \n");
					mensagem.append("Fornecedor: " + codigoFornecedor + " - " + nomeFornecedor + "\n");
					mensagem.append("Rota: " + codigoRota + " - " + nomeRota + "\n");
					mensagem.append("Motivo(s) da não abertura: \n");
					if (NeoUtils.safeIsNotNull(listaDeErros))
					{
						for (String erro : listaDeErros)
						{
							mensagem.append(erro + "\n");
						}
					}
					mensagem.append("Favor verificar o(s) motivo(s) informados e abrir a tarefa manualmente.");

					// Preenche as informações do e-mail
					email.addTo(emailSolicitante);
					email.addTo(emailAprovador);
					email.addCc("diogo.silva@orsegups.com.br");
					email.addBcc(bcc);
					email.setSubject("FAP Tático - Iniciar tarefa");
					email.setMsg(mensagem.toString());
					email.setFrom(settings.getFromEMail(), settings.getFromName());
					email.setSmtpPort(settings.getPort());
					email.setHostName(settings.getSmtpServer());
					email.send();

				}
				catch (Exception e)
				{
					//DO NOTHING
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * FAP aplicação tático.
	 * Envia e-mail avisando o responsável que não foi possível
	 * avançar a atividade de Finalizar Orçamento da FAP por algum problema ou erro de cadastro.
	 * 
	 * @param lista (List<FornecedorDTO>)
	 */
	public static void naoAvancouAtividadeTaticos(List<FornecedorDTO> lista)
	{
		List<FornecedorDTO> listaFornecedores = lista;
		MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

		if (listaFornecedores.size() > 0)
		{

			for (FornecedorDTO fornecedor : listaFornecedores)
			{
				try
				{
					MultiPartEmail email = new MultiPartEmail();
					String nomeFornecedor = fornecedor.getNomeFornecedor();
					String codigoFornecedor = fornecedor.getCodigoFornecedor();

					Long codigoRota = fornecedor.getCodigoRota();
					String nomeRota = fornecedor.getNomeRota();
					String codigoTarefa = fornecedor.getCodigoTarefa();
					String regional = FapUtils.getSiglaRegional(nomeRota);
					NeoUser solicitante = FapUtils.getSolicitante(regional);
					NeoUser aprovador = FapUtils.getAprovador(regional);

					String emailSolicitante = null;
					String emailAprovador = null;
					String bcc = "emailautomatico@orsegups.com.br";

					/* Variáveis usadas em testes, tanto locais quanto em produção */
					boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);
					String usuarioTestes = FAPParametrizacao.findParameter("usuarioTestes");

					if (debug)
					{
						NeoUser user = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioTestes));
						emailSolicitante = user.getEmail();
						emailAprovador = user.getEmail();
					}
					else
					{
						emailSolicitante = solicitante.getEmail();
						emailAprovador = aprovador.getEmail();
					}

					List<String> listaDeErros = fornecedor.getListaErros();

					StringBuilder mensagem = new StringBuilder();
					mensagem.append("A FAP " + codigoTarefa + " do fornecedor abaixo foi aberta mas a atividade Finalizar Orçamento não foi avançada:\n");
					mensagem.append("Fornecedor: " + codigoFornecedor + " - " + nomeFornecedor + "\n");
					mensagem.append("Rota: " + codigoRota + " - " + nomeRota + "\n");
					mensagem.append("Motivo(s): " + "\n");
					if (NeoUtils.safeIsNotNull(listaDeErros))
					{
						for (String erro : listaDeErros)
						{
							mensagem.append(erro + "\n");
						}
					}
					mensagem.append("Favor verificar o(s) motivo(s) informados e avançar a tarefa. " + "Caso não seja possível avançar será necessário cancelar a mesma e abrir uma nova FAP.");

					// Preenche as informações do e-mail
					email.addTo(emailSolicitante);
					email.addTo(emailAprovador);
					email.addCc("diogo.silva@orsegups.com.br");
					email.addBcc(bcc);
					email.setSubject("FAP - Atividade não pôde ser avançada");
					email.setMsg(mensagem.toString());
					email.setFrom(settings.getFromEMail(), settings.getFromName());
					email.setSmtpPort(settings.getPort());
					email.setHostName(settings.getSmtpServer());
					email.send();
				}
				catch (Exception e)
				{
					// DO NOTHING
				}
			}
		}
	}

	public static List<String> getErrosEform(WorkflowException e)
	{
		List<EFormError> listaErrosEform = e.getErrorList();

		if (listaErrosEform.size() > 0)
		{
			List<String> listaErros = new ArrayList<>();
			for (EFormError erro : listaErrosEform)
			{
				listaErros.add(erro.getI18nMessage());
			}
			return listaErros;
		}
		else
		{
			return null;
		}
	}

	public static FornecedorDTO retornaDadosFornecedor(NeoObject fornecedor)
	{
		EntityWrapper fornecedorWrapper = new EntityWrapper(fornecedor);
		FornecedorDTO dto = new FornecedorDTO();
		String email = (String) fornecedorWrapper.findValue("intNet");
		Long cgcCpf = (Long) fornecedorWrapper.findValue("cgccpf");
		String nomeFornecedor = (String) fornecedorWrapper.findValue("nomfor");

		dto.setEmail(email);
		dto.setCgcCpf(cgcCpf);
		dto.setNomeFornecedor(nomeFornecedor);

		return dto;

	}

	public static NeoObject consultaModalidade(Long rota) throws Exception
	{
		try
		{
			Long codigoRota = rota;
			NeoObject efRota = FapFactory.neoObjectFactory("SIGMAROTA", "cd_rota", codigoRota);
			EntityWrapper ewRota = new EntityWrapper(FapFactory.neoObjectFactory("FAPAplicacaoTatico", "rotaSigma", efRota));
			NeoObject modalidade = (NeoObject) ewRota.findField("modalidade").getValue();
			return modalidade;
		}
		catch (Exception e)
		{
			throw e;
		}
	}

	public static NeoObject consultaModalidadeTecnico(Long codTecnico) throws Exception
	{
		try
		{
			NeoObject noTecnico = FapFactory.neoObjectFactory("SIGMACOLABORADOR", "cd_colaborador", codTecnico);
			EntityWrapper wTecnico = new EntityWrapper(FapFactory.neoObjectFactory("FAPAplicacaoTecnico", "tecnico", noTecnico));
			NeoObject modalidade = (NeoObject) wTecnico.findGenericValue("modalidadePag");
			return modalidade;
		}
		catch (Exception e)
		{
			throw e;
		}
	}

	public static boolean verificaConciliacao(Long empresa, String central) throws Exception
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		Long empSigma = empresa;
		String idCentral = central;
		boolean conciliado = false;

		sql.append("SELECT ID_EMPRESA, ID_CENTRAL, CD_CLIENTE FROM dbCENTRAL                                 ");
		sql.append("WHERE ID_EMPRESA = ?                                                                     ");
		sql.append("AND ID_CENTRAL = ?                                                                       ");
		sql.append("AND EXISTS                                                                               ");
		sql.append("(SELECT * FROM [FSOODB04\\SQL02].SAPIENS.DBO.usu_t160sig SIG                             ");
		sql.append("INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.usu_t160cvs CVS                                 ");
		sql.append("ON SIG.usu_codemp = CVS.usu_codemp AND SIG.usu_codfil = CVS.usu_codfil                   ");
		sql.append("AND SIG.usu_numctr = CVS.usu_numctr AND SIG.usu_numpos = CVS.usu_numpos                  ");
		sql.append("WHERE SIG.USU_CODCLI = CD_CLIENTE                                                        ");
		sql.append("AND ((CVS.usu_sitcvs = 'A') OR (CVS.usu_sitcvs = 'I' AND CVS.usu_datfim >= GETDATE())) ) ");

		try
		{
			conn = PersistEngine.getConnection("SIGMA90");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, empSigma);
			pstm.setString(2, idCentral);
			rs = pstm.executeQuery();

			if (rs.next())
			{
				conciliado = true;
				return conciliado;
			}
			else
			{
				return conciliado;
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e2)
			{
				// TODO: handle exception
			}
		}
	}

	public static void enviaResumoJobTatico(int total, int abertas, int naoAbertas, int naoAvancadas)
	{

		try
		{
			MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
			MultiPartEmail email = new MultiPartEmail();
			StringBuilder mensagem = new StringBuilder();
			int totalTarefas = total;
			int totalAbertas = abertas;
			int totalNaoAbertas = naoAbertas;
			int totalNaoAvancadas = naoAvancadas;

			mensagem.append("Resumo das tarefas a serem abertas no JOB FAP Tático\n");
			mensagem.append("Total de tarefas a serem abertas: " + totalTarefas + "\n");
			mensagem.append("Total de tarefas abertas: " + totalAbertas + "\n");
			mensagem.append("Tarefas não abertas: " + totalNaoAbertas + "\n");
			mensagem.append("Tarefas não avançadas: " + totalNaoAvancadas + "\n");

			email.addTo("diogo.silva@orsegups.com.br");
			email.setSubject("FAP Tático - Resumo do JOB");
			email.setMsg(mensagem.toString());
			email.setFrom(settings.getFromEMail(), settings.getFromName());
			email.setSmtpPort(settings.getPort());
			email.setHostName(settings.getSmtpServer());
			email.send();
		}
		catch (Exception e)
		{
			// DO NOTHING
		}

	}

	public static void enviaResumoJobTecnico(int total, int abertas, int naoAbertas, int naoAvancadas)
	{

		try
		{
			MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
			MultiPartEmail email = new MultiPartEmail();
			StringBuilder mensagem = new StringBuilder();
			int totalTarefas = total;
			int totalAbertas = abertas;
			int totalNaoAbertas = naoAbertas;
			int totalNaoAvancadas = naoAvancadas;

			mensagem.append("Resumo das tarefas a serem abertas no JOB FAP Técnico\n");
			mensagem.append("Total de tarefas a serem abertas: " + totalTarefas + "\n");
			mensagem.append("Total de tarefas abertas: " + totalAbertas + "\n");
			mensagem.append("Tarefas não abertas: " + totalNaoAbertas + "\n");
			mensagem.append("Tarefas não avançadas: " + totalNaoAvancadas + "\n");

			email.addTo("diogo.silva@orsegups.com.br");
			email.setSubject("FAP Técnico - Resumo do JOB");
			email.setMsg(mensagem.toString());
			email.setFrom(settings.getFromEMail(), settings.getFromName());
			email.setSmtpPort(settings.getPort());
			email.setHostName(settings.getSmtpServer());
			email.send();
		}
		catch (Exception e)
		{
			// DO NOTHING
		}

	}

	public static void rotasParceirasSemCadastro(List<NeoObject> rotas)
	{
		List<NeoObject> rotasParc = rotas;

		for (NeoObject rota : rotasParc)
		{
			Connection conn = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;
			StringBuilder sql = new StringBuilder();
			EntityWrapper ew = new EntityWrapper(rota);
			Long codigo = ew.findGenericValue("cd_rota");
			String nome = ew.findGenericValue("nm_rota");

			try
			{
				conn = PersistEngine.getConnection("SIGMA90");

				sql.append(" SELECT * FROM dbCENTRAL WHERE CTRL_CENTRAL = 1 AND FG_ATIVO = 1 AND ID_ROTA = ? ");
				sql.append(" AND ID_CENTRAL NOT LIKE 'AAA%' AND ID_CENTRAL NOT LIKE 'DDDD'                   ");
				sql.append(" AND ID_CENTRAL NOT LIKE 'FFFF' AND ID_CENTRAL NOT LIKE 'R%'                     ");

				pstm = conn.prepareStatement(sql.toString());
				pstm.setLong(1, codigo);

				rs = pstm.executeQuery();

				if (rs.next())
				{
					String executor = "rosileny";
					String solicitante = "rosileny";
					String titulo = "FAP Tático - Rota PARC sem cadastro";
					String descricao = "Rota " + codigo.toString() + " " + nome + " está sem cadastro de valores para pagamento. Tela: Departamentos - Financeiro - FAP Táticos. Favor providenciar o cadastro.";
					GregorianCalendar prazo = new GregorianCalendar();
					prazo.add(Calendar.DAY_OF_MONTH, 2);
					while (!OrsegupsUtils.isWorkDay(prazo))
					{
						prazo.add(Calendar.DAY_OF_MONTH, 1);
					}
					prazo.set(Calendar.HOUR, 23);
					prazo.set(Calendar.MINUTE, 59);
					prazo.set(Calendar.SECOND, 59);

					IniciarTarefaSimples tarefa = new IniciarTarefaSimples();

					tarefa.abrirTarefa(solicitante, executor, titulo, descricao, "1", "", prazo);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				continue;
			}
		}
	}

	public static void preencheNovaCompetencia()
	{

		NeoObject parametros = PersistEngine.getObject(AdapterUtils.getEntityClass("FAPParametrizacao"), new QLEqualsFilter("parametro", "competencia"));
		EntityWrapper ew = new EntityWrapper(parametros);
		String competencia = ew.findGenericValue("parametro");
		String data = ew.findGenericValue("valor");
		String[] dataArray = data.split("/");
		String mes = dataArray[1];
		Integer ano = Integer.parseInt(dataArray[2]);
		GregorianCalendar calendar = new GregorianCalendar();
		Integer mesAtual = calendar.get(Calendar.MONTH);
		Integer novoMes = mesAtual;
		System.out.println("Competencia atual: " + competencia);
		System.out.println("Mês atual: " + mes);
		if (novoMes == 13)
		{
			novoMes = 1;
			ano = ano + 1;
		}
		String novaCompetencia = "01" + "/" + "0" + novoMes.toString() + "/" + ano.toString();

		System.out.println("Nova competencia: " + novaCompetencia);

		ew.setValue("valor", novaCompetencia);
		PersistEngine.persist(parametros);

	}

	public static boolean validarCompetencia(GregorianCalendar cpt, Long codRota, String codTarefaAtual) throws Exception
	{
		boolean competenciaValida = false;
		int quantidadeFapNestaCompetencia = 0;
		Long codigoRota = codRota;
		GregorianCalendar competencia = cpt;
		String codigoTarefaAtual = codTarefaAtual;

		try
		{

			quantidadeFapNestaCompetencia = AplicacaoTaticoDAO.quantidadeFapPorCompetencia(competencia, codigoRota, codigoTarefaAtual);

			if (quantidadeFapNestaCompetencia == 0)
			{
				competenciaValida = true;
			}
			return competenciaValida;

		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public static boolean competenciaAntecedeInicioRota(GregorianCalendar cpt, Long codRota)
	{
		boolean competenciaAntecedeInicio = false;
		Long codigoRota = codRota;
		GregorianCalendar competencia = cpt;

		NeoObject rotaSigma = PersistEngine.getObject(AdapterUtils.getEntityClass("SIGMAROTA"), new QLEqualsFilter("cd_rota", codigoRota));
		NeoObject tatico = PersistEngine.getObject(AdapterUtils.getEntityClass("FAPAplicacaoTatico"), new QLEqualsFilter("rotaSigma", rotaSigma));
		EntityWrapper ewTatico = new EntityWrapper(tatico);
		GregorianCalendar dataInicioRota = ewTatico.findGenericValue("inicioPagamento");

		// Se a competencia for anterior a data de inicio
		if (competencia.before(dataInicioRota))
		{
			competenciaAntecedeInicio = true;
		}

		return competenciaAntecedeInicio;
	}

}
