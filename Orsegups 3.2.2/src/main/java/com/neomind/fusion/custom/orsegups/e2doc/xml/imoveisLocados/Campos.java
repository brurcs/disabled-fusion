package com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados;

public class Campos {
    
    private String i24;
    private String i38;
    private String i49;
    private String i50;
    private String i51;
    private String i52;
    private String i53;
    private String i58;
    private String i59;
    private String i60;
    private String i61;
    
    public String getI24() {
        return i24;
    }
    public void setI24(String i24) {
        this.i24 = i24;
    }
    public String getI38() {
        return i38;
    }
    public void setI38(String i38) {
        this.i38 = i38;
    }
    public String getI49() {
        return i49;
    }
    public void setI49(String i49) {
        this.i49 = i49;
    }
    public String getI50() {
        return i50;
    }
    public void setI50(String i50) {
        this.i50 = i50;
    }
    public String getI51() {
        return i51;
    }
    public void setI51(String i51) {
        this.i51 = i51;
    }
    public String getI52() {
        return i52;
    }
    public void setI52(String i52) {
        this.i52 = i52;
    }
    public String getI53() {
        return i53;
    }
    public void setI53(String i53) {
        this.i53 = i53;
    }
    public String getI58() {
        return i58;
    }
    public void setI58(String i58) {
        this.i58 = i58;
    }
    public String getI59() {
        return i59;
    }
    public void setI59(String i59) {
        this.i59 = i59;
    }
    public String getI60() {
        return i60;
    }
    public void setI60(String i60) {
        this.i60 = i60;
    }
    public String getI61() {
        return i61;
    }
    public void setI61(String i61) {
        this.i61 = i61;
    }
    
    
    
}
