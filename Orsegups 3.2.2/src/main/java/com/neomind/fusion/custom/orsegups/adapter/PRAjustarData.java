package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class PRAjustarData implements AdapterInterface
{
	@Override
	public void back(EntityWrapper arg0, Activity arg1)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar dataNegociacao = (GregorianCalendar) processEntity.findField("data01").getValue();
		
		dataNegociacao = OrsegupsUtils.getSpecificWorkDay(dataNegociacao, 2L);

		dataNegociacao.set(GregorianCalendar.HOUR_OF_DAY, 23);
		dataNegociacao.set(GregorianCalendar.MINUTE, 59);
		dataNegociacao.set(GregorianCalendar.SECOND, 59);

		processEntity.findField("prazoUm").setValue(dataNegociacao);
	}
}
