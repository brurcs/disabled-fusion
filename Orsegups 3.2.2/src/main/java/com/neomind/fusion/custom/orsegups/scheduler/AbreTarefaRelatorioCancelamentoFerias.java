package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.docx4j.model.datastorage.XPathEnhancerParser.main_return;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
/**
 * 
 * @author herisson.ferreira
 *
 */
public class AbreTarefaRelatorioCancelamentoFerias implements CustomJobAdapter {

	private static final Log log = LogFactory.getLog(AbreTarefaRelatorioCancelamentoFerias.class);
	
	@Override
	public void execute(CustomJobContext ctx) {	
		
		// Log
		
		log.error("##### INICIO AGENDADOR DE TAREFA: Abre Tarefa Simples Relatório de Cancelamento de Férias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		System.out.println("##### INICIO AGENDADOR DE TAREFA: Abre Tarefa Simples Relatório de Cancelamento de Férias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long logKey = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		// Conexão

		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;

		// Datas

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		GregorianCalendar dataBase = new GregorianCalendar();
		dataBase.add(GregorianCalendar.MONTH, -1);
		dataBase.set(GregorianCalendar.DAY_OF_MONTH, 1);
		dataBase.set(GregorianCalendar.HOUR_OF_DAY, 00);
		dataBase.set(GregorianCalendar.MINUTE, 00);
		dataBase.set(GregorianCalendar.SECOND, 00);
		dataBase.set(GregorianCalendar.MILLISECOND, 00);

		String dataInicio = NeoDateUtils.safeDateFormat(dataBase, "yyyy-MM-dd HH:mm:ss");

		int ultimoDia = dataBase.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		dataBase.set(GregorianCalendar.DAY_OF_MONTH, ultimoDia);
		dataBase.set(GregorianCalendar.HOUR_OF_DAY, 23);
		dataBase.set(GregorianCalendar.MINUTE, 59);
		dataBase.set(GregorianCalendar.SECOND, 59);
		dataBase.set(GregorianCalendar.MILLISECOND, 59);

		String dataFim = NeoDateUtils.safeDateFormat(dataBase, "yyyy-MM-dd HH:mm:ss");
		
		String competencia = NeoDateUtils.safeDateFormat(dataBase, "MM-yyyy");
		
		// Executor - Solicitante - Título - Descrição

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

		String papelSolicitante = null;
		String papelExecutor = null;
		String usuarioSolicitante = null;
		String usuarioExecutor = null;

		String titulo = null;
		String descricao = null;
		
		papelSolicitante = "SolicitanteTarefaCancelamentoFerias";
		papelExecutor = "ExecutorTarefaCancelamentoFerias";

		usuarioSolicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
		usuarioExecutor = OrsegupsUtils.getUserNeoPaper(papelExecutor);

		titulo = "Relatório de Cancelamento de Férias";
		descricao = "Relatório de Cancelamento de férias referente a competência "+competencia+"."; 
		
		try {
			
			sql.append(" SELECT NEXTI.usu_numemp, NEXTI.usu_tipcol, NEXTI.usu_numcad, FUN.nomfun, NEXTI.usu_datafa, NEXTI.usu_datter, NEXTI.usu_sitafa FROM USU_T038NEXTI NEXTI ");
			sql.append(" INNER JOIN R034FUN FUN ON FUN.numemp = NEXTI.usu_numemp");
			sql.append(" AND FUN.tipcol = NEXTI.usu_tipcol");
			sql.append(" AND FUN.numcad = NEXTI.usu_numcad");
			sql.append(" AND NEXTI.USU_DATAFA BETWEEN ? AND ?");
			sql.append(" WHERE NEXTI.USU_SITAFA = 2 AND NEXTI.USU_NEXTID LIKE '%EXCLUIDO%'");
			sql.append(" ORDER BY NEXTI.USU_NUMEMP, NEXTI.USU_TIPCOL, NEXTI.USU_NUMCAD");

			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, dataInicio);
			pstm.setString(2, dataFim);

			rs = pstm.executeQuery();

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Cancelamentos");
			int rownum = 0;
			Cell cell;
			Row row;
			
			Map<Integer, String> mapa = new HashMap<>();

			row = sheet.createRow(rownum++);
			row.createCell(0).setCellValue("Empresa");
			row.createCell(1).setCellValue("Tipo Colaborador");
			row.createCell(2).setCellValue("Matrícula");
			row.createCell(3).setCellValue("Colaborador");
			row.createCell(4).setCellValue("Data Início");
			row.createCell(5).setCellValue("Data Fim");
			row.createCell(6).setCellValue("Situação");
			
			while(rs.next()){
				
				dataInicio = NeoDateUtils.safeDateFormat(rs.getDate("usu_datafa"), "dd/MM/yyyy HH:mm:ss");
				dataFim = NeoDateUtils.safeDateFormat(rs.getDate("usu_datter"), "dd/MM/yyyy HH:mm:ss");

				mapa.put(0, rs.getString("usu_numemp"));
				mapa.put(1, rs.getString("usu_tipcol"));
				mapa.put(2, rs.getString("usu_numcad"));
				mapa.put(3, rs.getString("nomfun"));
				mapa.put(4, dataInicio);
				mapa.put(5, dataFim);
				mapa.put(6, rs.getString("usu_sitafa"));
				
				row = sheet.createRow(rownum++);
				
				for(Integer key: mapa.keySet()){
					cell = row.createCell(key);
					cell.setCellValue(mapa.get(key));
				}
				
			}

			File file = File.createTempFile("CancelamentoNexti_"+competencia+"_",".xls", null);
			FileOutputStream out = new FileOutputStream(file);
			workbook.write(out);
			out.close();
			
			NeoFile anexo = OrsegupsUtils.criaNeoFile(file);
			
			iniciarTarefaSimples.abrirTarefa(usuarioSolicitante, usuarioExecutor, titulo, descricao, "1", "sim", prazo, anexo);

		} catch (Exception e) {
			e.printStackTrace();
			throw new JobException("Erro na geração da tarefa de Relatório de Cancelamento de Férias [ "+logKey+"]");
		}finally{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			log.error("##### FIM AGENDADOR DE TAREFA: Abre Tarefa Simples Relatório de Cancelamento de Férias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("##### FIM AGENDADOR DE TAREFA: Abre Tarefa Simples Relatório de Cancelamento de Férias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}

	}

}
