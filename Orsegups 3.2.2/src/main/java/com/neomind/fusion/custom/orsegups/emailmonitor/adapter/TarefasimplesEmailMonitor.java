package com.neomind.fusion.custom.orsegups.emailmonitor.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.emailmonitor.EmailFormAdapter;
import com.neomind.fusion.custom.orsegups.emailmonitor.vo.EmailVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;

public class TarefasimplesEmailMonitor implements EmailFormAdapter
{

	@Override
	public NeoObject getProcessForm(EmailVO emailVO) throws Exception
	{
		NeoObject tarefaObject = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper tarefaWrapper = new EntityWrapper(tarefaObject);
		
		String assunto = emailVO.getAssunto();
		
		
		String titulo = assunto;
		String prazoStr = "";
		GregorianCalendar prazo = new GregorianCalendar();
		if(assunto.indexOf("##") > 0)
		{
			titulo = assunto.substring(0, assunto.indexOf("##"));
			
			if(assunto.indexOf("##") < assunto.lastIndexOf("##"))
			{
				prazoStr = assunto.substring(assunto.indexOf("##")+2, assunto.lastIndexOf("##"));
				prazo = NeoCalendarUtils.fullCanlendar(prazoStr, "00:00:00");
			}
		}
		
		if(prazo != null)
		{
			if(!OrsegupsUtils.isWorkDay(prazo))
			{
				prazo = OrsegupsUtils.getNextWorkDay(prazo);
			}
		}
		
		String descricao = emailVO.getTextoEmail();
		NeoUser solicitante = emailVO.getRemetente();
		NeoUser executor = emailVO.getDestinatarios().get(0);
		
		tarefaWrapper.setValue("Solicitante", solicitante);
		tarefaWrapper.setValue("Executor", executor);
		tarefaWrapper.setValue("Titulo", titulo);
		tarefaWrapper.setValue("DescricaoSolicitacao", descricao);
		tarefaWrapper.setValue("Prazo", prazo);
		
		return tarefaObject;
	}

}
