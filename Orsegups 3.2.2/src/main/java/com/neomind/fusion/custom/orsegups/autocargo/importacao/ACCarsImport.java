package com.neomind.fusion.custom.orsegups.autocargo.importacao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class ACCarsImport {

    public static String execute() {

	String retorno = "Resultado da execução: ";
	
	Connection conn = null;

	PreparedStatement pstm = null;
	
	Pattern pattern = Pattern.compile("[A-Z]{3}[0-9]{4}");
	
	//						 1 	2 	3	 4	5 	6		 7 	8 	 9 	10	 11		12	13
	String sql = "INSERT INTO CELTEC_CARRO (ID_LEGADO ,PLACA ,RENAVAM ,DESCRICAO ,COR ,CHASSI ,ANO_FABRICACAO ,ANO_MODELO ,MARCA ,MODELO ,CATEGORIA ,COMBUSTIVEL,POSSUI_INCONSISTENCIA ) "
		+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

	FileInputStream file = null;
	
	try {
	    file = new FileInputStream(new File("c:\\Importacao\\ClientesAutoCargo.xlsx"));
	    // Create Workbook instance holding reference to .xlsx file
	    XSSFWorkbook workbook = new XSSFWorkbook(file);

	    // Get first/desired sheet from the workbook
	    XSSFSheet sheet = workbook.getSheetAt(2);

	    // Iterate through each rows one by one
	    Iterator<Row> rowIterator = sheet.iterator();
	    
	    //PULAR PRIMERA LINHA
	    rowIterator.next();

	    conn = PersistEngine.getConnection("IMPORTACOES");

	    pstm = conn.prepareStatement(sql);

	    while (rowIterator.hasNext()) {
		
		boolean possuiInconsistencia = false;
		
		Matcher matcher = null;

		Row row = rowIterator.next();

		Cell cellIdLegado = row.getCell(0);
		Cell cellPlaca = row.getCell(1);
		Cell cellRenavam = row.getCell(2);
		Cell cellDescricao = row.getCell(3);
		Cell cellCor = row.getCell(4);
		Cell cellChassi = row.getCell(5);
		Cell cellAnoFabricacao = row.getCell(6);
		Cell cellAnoModelo = row.getCell(7);
		Cell cellMarca = row.getCell(8);
		Cell cellModelo = row.getCell(9);
		Cell cellCategoria = row.getCell(10);
		Cell cellCombustivel = row.getCell(11);

		Double idLegado = cellIdLegado.getNumericCellValue();

		pstm.setInt(1, idLegado.intValue());
		
		String placa = "";
		
		if (cellPlaca != null){
		    switch (cellPlaca.getCellType()) {
		    case Cell.CELL_TYPE_NUMERIC:
			possuiInconsistencia = true;
			Double preCep = cellPlaca.getNumericCellValue();
			int intCep = preCep.intValue();
			placa = String.valueOf(intCep);		
			break;
		    case Cell.CELL_TYPE_STRING:
			placa = cellPlaca.getStringCellValue();
			matcher = pattern.matcher(placa);
			if (!matcher.matches()){
			    possuiInconsistencia = true;
			}		
			break;
		    }	
		}
		
		pstm.setString(2, placa);
		
		String renavam = "";
		if (cellRenavam != null){
		    switch (cellRenavam.getCellType()) {
		    case Cell.CELL_TYPE_NUMERIC:
			Double dRenavam = cellRenavam.getNumericCellValue();
			renavam += dRenavam.intValue();
			break;
		    case Cell.CELL_TYPE_STRING:
			renavam = cellRenavam.getStringCellValue();
			break;
		    }
		}
		
		pstm.setString(3, renavam);

		
		String descricao = "";
		
		if (cellDescricao != null){
		    switch (cellDescricao.getCellType()) {
		    case Cell.CELL_TYPE_NUMERIC:
			descricao += cellDescricao.getNumericCellValue();
			break;
		    case Cell.CELL_TYPE_STRING:
			descricao = cellDescricao.getStringCellValue();
			break;
		    }
		}
	
		pstm.setString(4, descricao.toUpperCase());
		
		String cor = "";
		if (cellCor != null){
		    switch (cellCor.getCellType()) {
		    case Cell.CELL_TYPE_NUMERIC:
			cor += cellCor.getNumericCellValue();
			break;
		    case Cell.CELL_TYPE_STRING:
			cor = cellCor.getStringCellValue();
			break;
		    }
		}
		
		pstm.setString(5, cor.toUpperCase());		    
		
		String chassi = "";
		
		if (cellChassi != null){
		    switch (cellChassi.getCellType()) {
		    case Cell.CELL_TYPE_NUMERIC:
			chassi += cellChassi.getNumericCellValue();
			break;
		    case Cell.CELL_TYPE_STRING:
			chassi = cellChassi.getStringCellValue();
			break;
		    }
		}
		
		pstm.setString(6, chassi.toUpperCase());


		Double anoFabricao = 0.0;
		if (cellAnoFabricacao != null){
		    anoFabricao = cellAnoFabricacao.getNumericCellValue();
		}
		
		pstm.setInt(7, anoFabricao.intValue());
		
		Double anoModelo = 0.0;
		if (cellAnoModelo != null){
		    anoModelo = cellAnoModelo.getNumericCellValue();
		}
		
		pstm.setInt(8, anoModelo.intValue());
		
		pstm.setString(9, cellMarca != null ? cellMarca.getStringCellValue().toUpperCase() : "");
		
		String modelo = "";
		
		if (cellModelo != null){
		    switch (cellModelo.getCellType()) {
		    case Cell.CELL_TYPE_NUMERIC:
			modelo += cellModelo.getNumericCellValue();
			break;
		    case Cell.CELL_TYPE_STRING:
			modelo = cellModelo.getStringCellValue();
			break;
		    }
		}
		
		pstm.setString(10, modelo.toUpperCase());
		
		pstm.setString(11, cellCategoria != null ? cellCategoria.getStringCellValue().toUpperCase() : "");
		
		pstm.setString(12, cellCombustivel != null ? cellCombustivel.getStringCellValue().toUpperCase() : "");
		
		pstm.setBoolean(13, possuiInconsistencia);
		

		pstm.addBatch();
		pstm.clearParameters();

		// For each row, iterate through all the columns
		// Iterator<Cell> cellIterator = row.cellIterator();

		// while (cellIterator.hasNext())
		// {
		// Cell cell = cellIterator.next();
		// //Check the cell type and format accordingly
		// switch (cell.getCellType())
		// {
		// case Cell.CELL_TYPE_NUMERIC:
		// System.out.print(cell.getNumericCellValue() + "t");
		// break;
		// case Cell.CELL_TYPE_STRING:
		// System.out.print(cell.getStringCellValue() + "t");
		// break;
		// }
		// }

	    }

	    int[] results = pstm.executeBatch();
	    	   
	    ArrayList<Integer> posicaoDoProblema = new ArrayList<Integer>();
	    for (int i=0; i<results.length; i++){
		if (results[i] < 1 ){
		    posicaoDoProblema.add(i);
		}
	    }
	    
	    String complemento = "";
	    
	    if (posicaoDoProblema != null && !posicaoDoProblema.isEmpty()){
		complemento = ". Houve problema na inserção dos registros na(s) posição(ões): "+posicaoDoProblema.toString();
	    }else{
		complemento = ". Nenhum problema nas inserções.";
	    }

	    retorno += results.length+complemento;

 
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    try {
		file.close();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	return retorno;
    }

}
