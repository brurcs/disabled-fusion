package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.AdapterAlteraParaUltimoNivelEResponsavel
public class AdapterAlteraParaUltimoNivelEResponsavel implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		NeoPaper papelContador = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "j002contador"));
		NeoPaper papelContadorEJurifico = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "j002ContadorEJuridicoOrse"));

		processEntity.setValue("papelRaiaEscRes", papelContador);
		processEntity.setValue("papelRaiaPoolJurEsc", papelContadorEJurifico);
		if (origin.toString().contains("Movimentar Processo") && !String.valueOf(processEntity.getValue("grauProcesso")).equals("4º Grau"))
		{
			processEntity.setValue("grauProcesso", "4º Grau");
		}
		if (origin.toString().contains("Aprovação Pagamento") && !String.valueOf(processEntity.getValue("grauProcesso")).equals("4º Grau"))
		{
			processEntity.setValue("grauProcesso", "4º Grau");
		}
	    }catch(Exception e){
		System.out.println("Erro na classe AdapterAlteraParaUltimoNivelEResponsavel do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}
}
