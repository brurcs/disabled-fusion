package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class JARegistroAtividade implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

	// String erro = "Por favor, contatar o administrador do sistema!";
	
	boolean prazoVerificar = false;

	InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("jaRegistroAtividade");
	NeoObject registro = registroAtividade.createNewInstance();
	EntityWrapper wRegistro = new EntityWrapper(registro);

	wRegistro.findField("responsavel").setValue(origin.getUser());

	wRegistro.findField("dataInicial").setValue(origin.getStartDate());

	wRegistro.findField("dataFinal").setValue(origin.getFinishDate());

	if (NeoUtils.safeIsNull(origin)) {
	    NeoPaper papel = new NeoPaper();
	    papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
	    NeoUser usuarioResponsavel = new NeoUser();
	    if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
		for (NeoUser user : papel.getUsers()) {
		    usuarioResponsavel = user;
		    break;
		}
	    }
	    wRegistro.findField("responsavel").setValue(usuarioResponsavel);
	    wRegistro.findField("dataInicial").setValue(new GregorianCalendar());
	    wRegistro.findField("dataFinal").setValue(new GregorianCalendar());
	    wRegistro.findField("atividade").setValue("Tarefa escalada em pool");
	    wRegistro.findField("prazo").setValue(processEntity.findValue("Prazo"));

	} else if (origin.getActivityName().equalsIgnoreCase("Justificar ausência - Anexar documento")) {
	    long acaoJustifica = 0L;
	    
	    processEntity.findField("executor").setValue(origin.getUser());
	    
	    NeoObject rAcao = (NeoObject) processEntity.findValue("acao");
	    
	    NeoObject eAcao = (NeoObject) processEntity.findValue("acaoExecutorSubstituto");

	    if (NeoUtils.safeIsNotNull(rAcao)) {
		EntityWrapper wAcao = new EntityWrapper(rAcao);

		acaoJustifica = (Long) wAcao.findValue("idAcao");
	    }else if (NeoUtils.safeIsNotNull(eAcao)) {
		EntityWrapper wAcao = new EntityWrapper(eAcao);

		acaoJustifica = (Long) wAcao.findValue("idAcao");
	    }
	    

	    if (acaoJustifica == 1L) {
		wRegistro.findField("atividade").setValue("Substitiu executor");
		/**
		 * Fazendo alteração aqui devido a bug no cópia de campos		
		 */
		processEntity.findField("executor").setValue(processEntity.findValue("executorSubstituto"));
		
 		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazo").getValue();
 
 		prazo.add(Calendar.DAY_OF_MONTH, +1);

		while (!OrsegupsUtils.isWorkDay(prazo)) {
		    prazo.add(Calendar.DAY_OF_MONTH, +1);
		}

		processEntity.findField("prazo").setValue(prazo);
		processEntity.setValue("acao", null);
		processEntity.setValue("acaoExecutorSubstituto", null);
	    } else if (acaoJustifica == 2L) {
		wRegistro.findField("atividade").setValue("Solicitou ajuste");
		processEntity.setValue("acao", null);//
		processEntity.setValue("acaoExecutorSubstituto", null);
	    } else if (acaoJustifica == 3L) {
		wRegistro.findField("atividade").setValue("Encaminhou para auditoria");
		
 		GregorianCalendar prazo = new GregorianCalendar();
 		
 		prazo.set(Calendar.HOUR_OF_DAY, 23);
 		prazo.set(Calendar.MINUTE, 59);
 		prazo.set(Calendar.SECOND, 59);
 		 
		
		prazo.add(Calendar.DAY_OF_MONTH, +1);

		while (!OrsegupsUtils.isWorkDay(prazo)) {
		    prazo.add(Calendar.DAY_OF_MONTH, +1);
		}

		processEntity.findField("prazo").setValue(prazo);
		processEntity.setValue("acao", null);
		processEntity.setValue("acaoExecutorSubstituto", null);
		
		if (processEntity.findField("auditor").getValue() == null){
		    NeoPaper papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "jaAuditor"));

		    processEntity.findField("auditor").setValue(papel);
		}
		
	    }else if(acaoJustifica == 99L){
		wRegistro.findField("atividade").setValue("Processo cancelado pelo usuário");
	    }else if(acaoJustifica == 4L){
		wRegistro.findField("atividade").setValue("Encaminhou para regional");
		
		GregorianCalendar prazoRegional = (GregorianCalendar) processEntity.findField("prazoRegional").getValue();
		
		if (prazoRegional != null) {
		    GregorianCalendar hoje = new GregorianCalendar();
		    hoje.set(Calendar.HOUR_OF_DAY, 23);
		    hoje.set(Calendar.MINUTE, 59);
		    hoje.set(Calendar.SECOND, 59);
		    hoje.set(Calendar.MILLISECOND, 0);

		    if (!prazoRegional.after(hoje)) {
			throw new WorkflowException("Prazo deve ser de pelo menos um dia útil!");
		    } else {
			while (!OrsegupsUtils.isWorkDay(prazoRegional)) {
			    prazoRegional.add(Calendar.DAY_OF_MONTH, +1);
			}
		    }
		    processEntity.setValue("veioDaAuditoria", false);
		    processEntity.findField("prazo").setValue(prazoRegional);
		} else {
		    GregorianCalendar prazo = new GregorianCalendar();

		    prazo.set(Calendar.HOUR_OF_DAY, 23);
		    prazo.set(Calendar.MINUTE, 59);
		    prazo.set(Calendar.SECOND, 59);

		    prazo.add(Calendar.DAY_OF_MONTH, +1);

		    while (!OrsegupsUtils.isWorkDay(prazo)) {
			prazo.add(Calendar.DAY_OF_MONTH, +1);
		    }

		    processEntity.findField("prazo").setValue(prazo);
		}
		

		processEntity.setValue("acao", null);
		processEntity.setValue("acaoRegional", null);
		processEntity.setValue("acaoExecutorSubstituto", null);
	    }else {
		wRegistro.findField("atividade").setValue("Tarefa escalada");
	    }

	} else if (origin.getActivityName().equalsIgnoreCase("Justificar ausência - Escalada") || origin.getActivityName().equalsIgnoreCase("Justificar ausência - Escalada Diretoria")
		|| origin.getActivityName().equalsIgnoreCase("Justificar ausência regional - Escalada") || origin.getActivityName().equalsIgnoreCase("Justificar ausência regional - Escalada Diretoria") ) {
	    boolean isEscalada = (boolean) processEntity.findValue("isEscalada");

	    if (isEscalada) {
		wRegistro.findField("atividade").setValue("Tarefa escalada");
	    } else {
		wRegistro.findField("atividade").setValue("Encaminhou justificativa");
		
 		GregorianCalendar prazo = new GregorianCalendar();
 		
 		prazo.set(Calendar.HOUR_OF_DAY, 23);
 		prazo.set(Calendar.MINUTE, 59);
 		prazo.set(Calendar.SECOND, 59);
 		 
 		prazo.add(Calendar.DAY_OF_MONTH, +1);

		while (!OrsegupsUtils.isWorkDay(prazo)) {
		    prazo.add(Calendar.DAY_OF_MONTH, +1);
		}
		processEntity.findField("prazo").setValue(prazo);
	    }

	    //processEntity.findField("isEscalada").setValue(false);

	} else if (origin.getActivityName().equalsIgnoreCase("Justificar ausência - Escalada Presidência") || origin.getActivityName().equalsIgnoreCase("Justificar ausência regional - Escalada Presidência")) {
	    Boolean devolverParaExecutor = (Boolean) processEntity.findValue("devolverParaExecutor");

	    if (devolverParaExecutor == null || devolverParaExecutor == false) {
		wRegistro.findField("atividade").setValue("Encaminhou justificativa");
	    } else {
		wRegistro.findField("atividade").setValue("Presidência encaminhou para os cuidados da auditoria");
	    }
	    processEntity.findField("isEscalada").setValue(false);
	} else if (origin.getActivityName().equalsIgnoreCase("Auditar Documento")) {
	    long acao = 0L;
	    NeoObject rAcao = (NeoObject) processEntity.findValue("acaoAuditoria");

	    if (NeoUtils.safeIsNotNull(rAcao)) {
		EntityWrapper wAcao = new EntityWrapper(rAcao);

		acao = (Long) wAcao.findValue("idAcao");
	    }

	    if (acao == 3L) {
		wRegistro.findField("atividade").setValue("Substitiu executor");
		
		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazo").getValue();
		GregorianCalendar hoje = new GregorianCalendar();
		hoje.set(Calendar.HOUR_OF_DAY, 23);
		hoje.set(Calendar.MINUTE, 59);
		hoje.set(Calendar.SECOND, 59);
		hoje.set(Calendar.MILLISECOND, 0);

		if (!prazo.after(hoje)) {
		    throw new WorkflowException("Prazo deve ser de pelo menos um dia útil!");
		} else {
		    while (!OrsegupsUtils.isWorkDay(prazo)) {
			prazo.add(Calendar.DAY_OF_MONTH, +1);
		    }
		}
		processEntity.findField("prazo").setValue(prazo);
	    } else if (acao == 2L) {
		wRegistro.findField("atividade").setValue("Negou a documentação");
		
		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazo").getValue();
		GregorianCalendar hoje = new GregorianCalendar();
		hoje.set(Calendar.HOUR_OF_DAY, 23);
		hoje.set(Calendar.MINUTE, 59);
		hoje.set(Calendar.SECOND, 59);
		hoje.set(Calendar.MILLISECOND, 0);

		if (!prazo.after(hoje)) {
		    throw new WorkflowException("Prazo deve ser de pelo menos um dia útil!");
		} else {
		    while (!OrsegupsUtils.isWorkDay(prazo)) {
			prazo.add(Calendar.DAY_OF_MONTH, +1);
		    }
		}
		processEntity.findField("prazo").setValue(prazo);
		
	    } else if (acao == 1L) {
		wRegistro.findField("atividade").setValue("Aprovou a documentação");
	    } else if (acao == 4L){
		wRegistro.findField("atividade").setValue("Encaminhou para regional");
		
		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazo").getValue();
		GregorianCalendar hoje = new GregorianCalendar();
		hoje.set(Calendar.HOUR_OF_DAY, 23);
		hoje.set(Calendar.MINUTE, 59);
		hoje.set(Calendar.SECOND, 59);
		hoje.set(Calendar.MILLISECOND, 0);

		if (!prazo.after(hoje)) {
		    throw new WorkflowException("Prazo deve ser de pelo menos um dia útil!");
		} else {
		    while (!OrsegupsUtils.isWorkDay(prazo)) {
			prazo.add(Calendar.DAY_OF_MONTH, +1);
		    }
		}
		processEntity.findField("prazo").setValue(prazo);
		processEntity.setValue("veioDaAuditoria", Boolean.TRUE);                                                                                                                                                                                       
	    } else {
		wRegistro.findField("atividade").setValue("Tarefa escalada");
		
 		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazo").getValue();
 		 
 		prazo.add(Calendar.DAY_OF_MONTH, +1);

		while (!OrsegupsUtils.isWorkDay(prazo)) {
		    prazo.add(Calendar.DAY_OF_MONTH, +1);
		}

		processEntity.findField("prazo").setValue(prazo);
	    }
	        	    
	} else if (origin.getActivityName().equalsIgnoreCase("Ajuste prazo - Justificar Ausência") || origin.getActivityName().equalsIgnoreCase("Ajuste prazo -ValidarDocumento") || origin.getActivityName().equalsIgnoreCase(""
		+ " - Justificar ausência - Regional") ) {
	    boolean aceitaProrrogacao = (boolean) processEntity.findValue("aceitaProrrogacaoPrazo");

	    if (aceitaProrrogacao) {
		wRegistro.findField("atividade").setValue("Ajustou o prazo");
	
		    GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazo").getValue();
		    GregorianCalendar hoje = new GregorianCalendar();
		    hoje.set(Calendar.HOUR_OF_DAY, 23);
		    hoje.set(Calendar.MINUTE, 59);
		    hoje.set(Calendar.SECOND, 59);
		    hoje.set(Calendar.MILLISECOND, 0);

		    if (!prazo.after(hoje)) {
			throw new WorkflowException("Prazo deve ser de pelo menos um dia útil!");
		    }else{
			while (!OrsegupsUtils.isWorkDay(prazo)) {
			    prazo.add(Calendar.DAY_OF_MONTH, +1);
			}
		    }
		    processEntity.findField("prazo").setValue(prazo);
		
		
	    } else {
		wRegistro.findField("atividade").setValue("Negou ajuste de prazo");
	    }
	    
	    processEntity.setValue("acao", null);
	    processEntity.setValue("acaoExecutorSubstituto", null);
	    
	    
	} else if (origin.getActivityName().equalsIgnoreCase("Validar atestado médico")) {
	    long acao = 0L;
	    NeoObject rAcao = (NeoObject) processEntity.findValue("acaoGestor");

	    if (NeoUtils.safeIsNotNull(rAcao)) {
		EntityWrapper wAcao = new EntityWrapper(rAcao);

		acao = (Long) wAcao.findValue("idAcao");
	    }

	    if (acao == 1L) {
		wRegistro.findField("atividade").setValue("Solicitou ajuste");
	    } else if (acao == 2L) {
		
		boolean abrirTarefa = (boolean) processEntity.findValue("abrirTarefa");
		
		if (abrirTarefa){
		    wRegistro.findField("atividade").setValue("Validou atestado e abriu tarefa simples");
		}else{
		    wRegistro.findField("atividade").setValue("Validou atestado");
		}
		
	    }else if (acao == 3L){
		wRegistro.findField("atividade").setValue("Verificou não conformidade no atestado");
		
 		GregorianCalendar prazo = new GregorianCalendar();
 		
 		prazo.set(Calendar.HOUR_OF_DAY, 23);
 		prazo.set(Calendar.MINUTE, 59);
 		prazo.set(Calendar.SECOND, 59);
 		 
		
		prazo.add(Calendar.DAY_OF_MONTH, +1);

		while (!OrsegupsUtils.isWorkDay(prazo)) {
		    prazo.add(Calendar.DAY_OF_MONTH, +1);
		}

		processEntity.findField("prazo").setValue(prazo);
	    }else if (acao == 4L){
				
		wRegistro.findField("atividade").setValue("Encaminhou para verificação");
		
		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazoVerificar").getValue();
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		prazo.set(Calendar.MILLISECOND, 0);
		
		GregorianCalendar hoje = new GregorianCalendar();
		hoje.set(Calendar.HOUR_OF_DAY, 23);
		hoje.set(Calendar.MINUTE, 59);
		hoje.set(Calendar.SECOND, 59);
		hoje.set(Calendar.MILLISECOND, 0);

		if (!prazo.after(hoje)) {
		    throw new WorkflowException("Prazo deve ser de pelo menos um dia útil!");
		} else {
		    while (!OrsegupsUtils.isWorkDay(prazo)) {
			prazo.add(Calendar.DAY_OF_MONTH, +1);
		    }
		}
		processEntity.findField("prazoVerificar").setValue(prazo);
		prazoVerificar = true;
		
	    }else {
		wRegistro.findField("atividade").setValue("Tarefa escalada");
	    }
	} else if (origin.getActivityName().equalsIgnoreCase("Validar atestado médico - Escalada") || origin.getActivityName().equalsIgnoreCase("Validar atestado médico  - Escalada Diretoria")) {
	    boolean isEscalada = (boolean) processEntity.findValue("isEscalada");

	    if (isEscalada) {
		wRegistro.findField("atividade").setValue("Tarefa escalada");
	    } else {
		long acao = 0L;
		NeoObject rAcao = (NeoObject) processEntity.findValue("acaoGEDEscalada");

		if (NeoUtils.safeIsNotNull(rAcao)) {
		    EntityWrapper wAcao = new EntityWrapper(rAcao);
		    acao = (Long) wAcao.findValue("idAcao");
		}

		if (acao == 1L) {
		    wRegistro.findField("atividade").setValue("Encaminhou para indexação");
		} else {
		    wRegistro.findField("atividade").setValue("Desaprovou a documentação");
		}

	    }

	    processEntity.findField("isEscalada").setValue(false);
	}else if (origin.getActivityName().equalsIgnoreCase("Justificar Ausência - Regional")){
	    
	    Long acao = 0L;
	    
	    NeoObject rAcao = (NeoObject) processEntity.findValue("acaoRegional");
	    
	    if (NeoUtils.safeIsNotNull(rAcao)) {
		EntityWrapper wAcao = new EntityWrapper(rAcao);
		acao = (Long) wAcao.findValue("idAcao");
	    }
	    
	    if (acao == 1L){
		wRegistro.findField("atividade").setValue("Solicitou ajuste");
	    }else if (acao == 2L){
		wRegistro.findField("atividade").setValue("Finalizou tarefa");
				
		GregorianCalendar hoje = new GregorianCalendar();
		hoje.set(Calendar.HOUR_OF_DAY, 23);
		hoje.set(Calendar.MINUTE, 59);
		hoje.set(Calendar.SECOND, 59);
		hoje.set(Calendar.MILLISECOND, 0);

	
		hoje.add(Calendar.DAY_OF_MONTH, +2);

		while (!OrsegupsUtils.isWorkDay(hoje)) {
		    hoje.add(Calendar.DAY_OF_MONTH, +1);
		}

		processEntity.findField("prazo").setValue(hoje);
		processEntity.setValue("executorRegional", null);
		processEntity.setValue("acaoAuditoria", null);		
	    }else{
		wRegistro.findField("atividade").setValue("Tarefa escalada");
	    }	    
	}else if (origin.getActivityName().equalsIgnoreCase("Verificar atestado médico")){
	    
	    Long acao = 0L;
	    
	    NeoObject rAcao = (NeoObject) processEntity.findValue("acaoRegional");
	    
	    if (NeoUtils.safeIsNotNull(rAcao)) {
		EntityWrapper wAcao = new EntityWrapper(rAcao);
		acao = (Long) wAcao.findValue("idAcao");
	    }
	    
	    if (acao == 1L){
		wRegistro.findField("atividade").setValue("Solicitou ajuste");
	    } else if (acao == 2L) {
		wRegistro.findField("atividade").setValue("Finalizou tarefa");

		GregorianCalendar hoje = new GregorianCalendar();
		hoje.add(Calendar.DAY_OF_MONTH, +2);
		hoje.set(Calendar.HOUR_OF_DAY, 23);
		hoje.set(Calendar.MINUTE, 59);
		hoje.set(Calendar.SECOND, 59);
		hoje.set(Calendar.MILLISECOND, 0);

		while (!OrsegupsUtils.isWorkDay(hoje)) {
		    hoje.add(Calendar.DAY_OF_MONTH, +1);
		}

		processEntity.findField("prazo").setValue(hoje);
		processEntity.findField("acaoGestor").setValue(null);
		
	    }else{
		wRegistro.findField("atividade").setValue("Tarefa escalada");
	    }
	} else if (origin.getActivityName().equalsIgnoreCase("Ajustar prazo verificação atestado") ) {
	    boolean aceitaProrrogacao = (boolean) processEntity.findValue("aceitaProrrogacaoPrazo");

	    if (aceitaProrrogacao) {
		wRegistro.findField("atividade").setValue("Ajustou o prazo");
	
		    GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazoVerificar").getValue();
		    GregorianCalendar hoje = new GregorianCalendar();
		    hoje.set(Calendar.HOUR_OF_DAY, 23);
		    hoje.set(Calendar.MINUTE, 59);
		    hoje.set(Calendar.SECOND, 59);
		    hoje.set(Calendar.MILLISECOND, 0);

		    if (!prazo.after(hoje)) {
			throw new WorkflowException("Prazo deve ser de pelo menos um dia útil!");
		    }else{
			while (!OrsegupsUtils.isWorkDay(prazo)) {
			    prazo.add(Calendar.DAY_OF_MONTH, +1);
			}
		    }
		    processEntity.findField("prazoVerificar").setValue(prazo);
		    prazoVerificar = true;
		
		
	    } else {
		wRegistro.findField("atividade").setValue("Negou ajuste de prazo");
	    }
	    
	}else if (origin.getActivityName().equalsIgnoreCase("Verificar atestado médico Escalada") || origin.getActivityName().equalsIgnoreCase("Verificar atestado médico - Escalada Presidência")
		|| origin.getActivityName().equalsIgnoreCase("Verificar atestado médico - Escalada Diretoria") ) {
	    boolean isEscalada = (boolean) processEntity.findValue("isEscalada");

	    if (isEscalada) {
		wRegistro.findField("atividade").setValue("Tarefa escalada");
	    } else {
		wRegistro.findField("atividade").setValue("Encaminhou justificativa");
		
 		GregorianCalendar prazo = new GregorianCalendar();
 		
 		prazo.set(Calendar.HOUR_OF_DAY, 23);
 		prazo.set(Calendar.MINUTE, 59);
 		prazo.set(Calendar.SECOND, 59);
 		 
 		prazo.add(Calendar.DAY_OF_MONTH, +2);

		while (!OrsegupsUtils.isWorkDay(prazo)) {
		    prazo.add(Calendar.DAY_OF_MONTH, +1);
		}
		processEntity.findField("prazo").setValue(prazo);
	    }

	    processEntity.findField("isEscalada").setValue(false);

	}

	String descricao = (String) processEntity.findValue("descricao");
	wRegistro.findField("descricao").setValue(descricao);
	if(prazoVerificar){
	    wRegistro.findField("prazo").setValue(processEntity.findValue("prazoVerificar"));
	}else{
	    wRegistro.findField("prazo").setValue(processEntity.findValue("prazo"));
	}
	
	NeoFile anexo = (NeoFile) processEntity.findField("anexoJustificativa").getValue();
	
	if (anexo != null){
	    wRegistro.findField("anexo").setValue(anexo);
	}
	
	
	@SuppressWarnings("unchecked")
	List<NeoObject> listaAnexos = (List<NeoObject>) processEntity.findField("anexos").getValue();
	
	if (listaAnexos != null && !listaAnexos.isEmpty()) {
	    wRegistro.findField("anexos").setValue(listaAnexos);	   
	}
	
	processEntity.setValue("descricao", null);
	
	processEntity.setValue("anexos", null);

	PersistEngine.persist(registro);

	processEntity.findField("registroAtividades").addValue(registro);

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

    }

}
