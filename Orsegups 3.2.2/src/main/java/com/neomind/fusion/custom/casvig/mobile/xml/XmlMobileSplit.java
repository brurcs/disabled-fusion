package com.neomind.fusion.custom.casvig.mobile.xml;

import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.neomind.fusion.custom.casvig.mobile.entity.CasvigMobileXmlSplit;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.util.NeoXMLUtils;

/**
 * Classe que divide um XML em varias partes e o armazena numa fila para
 * posteriormente os arquivos serem enviados para o mobile.
 * 
 * @author Daniel Henrique Joppi 18/11/2009
 * 
 */
public abstract class XmlMobileSplit {

	private static final Log log = LogFactory.getLog(XmlMobileSplit.class);

	/**
	 * Método que divide o XML de Clientes em varias partes e retorna um
	 * identificador para que o mobile possa baixar cada parte separadamente.
	 * 
	 * @param doc XML de cliente
	 * @return identificador da fila de arquivos de clientes.
	 * 
	 * @throws Exception
	 * 
	 * @author Daniel Henrique Joppi 18/11/2009
	 */
	public static String splitClient(Document doc) throws Exception 
	{
		String idSplit = XmlMobileSplit.generateIdSplit();
		String clientPart = new String("Cliente-Part");
		
		Element node = (Element) doc.getFirstChild();
		// lista de empresas
		NodeList nodeList = node.getChildNodes();
		
		log.info("Generated split id: " + idSplit + " - parts: " + (nodeList.getLength()));
				
		// cria o aquivo de index
		Document index = NeoXMLUtils.newDocument();
		index.appendChild(NeoXMLUtils.cloneElement(index, node, false));
		for (int i = 0; i < nodeList.getLength(); i++) 
		{
			Element empIndex = NeoXMLUtils.cloneElement(index, (Element) (nodeList.item(i)), false);
			empIndex.removeAttribute("num_cliente");
			empIndex.setAttribute("file_to_client", clientPart+(i+1)+".xml");
			empIndex.setAttribute("id_split", idSplit);
			
			index.getFirstChild().appendChild(empIndex);
		}
		String xmlIndex = NeoXMLUtils.DocumentToString(XmlMobileConverter.setEnconding(index));
		xmlIndex = xmlIndex.replace("encoding=\"UTF-8\"", "encoding=\"ISO-8859-1\"");
		PersistEngine.persist(new CasvigMobileXmlSplit("index", idSplit, 0, xmlIndex));
		
		// cria as demias partes
		for (int i = 0; i < nodeList.getLength(); i++) 
		{
			Document newDoc = NeoXMLUtils.newDocument();
			Element empFull = NeoXMLUtils.cloneElement(newDoc, (Element) (nodeList.item(i)), true);
			newDoc.appendChild(empFull);
			
			String xmlPart = NeoXMLUtils.DocumentToString(XmlMobileConverter.setEnconding(newDoc));
			xmlPart = xmlPart.replace("encoding=\"UTF-8\"", "encoding=\"ISO-8859-1\"");
			PersistEngine.persist(new CasvigMobileXmlSplit(clientPart+(i+1)+".xml", idSplit, (i+1), xmlPart));
		}
				
		return idSplit;
	}
	
	public static Document getXMLClientPart(String idSplit, Integer part, String fileSplit)
	{
		QLGroupFilter group = new QLGroupFilter("AND");
//		group.addFilter(new QLEqualsFilter("mobileEntity", CasvigMobileXmlSplit.ME_CLIENT));
		group.addFilter(new QLEqualsFilter("mobileEntity", fileSplit));
		group.addFilter(new QLEqualsFilter("idSplit", idSplit));
		group.addFilter(new QLEqualsFilter("part", part));
		
		CasvigMobileXmlSplit mobileSpli = (CasvigMobileXmlSplit) PersistEngine.getObject(CasvigMobileXmlSplit.class, group);
		InputStream in = IOUtils.toInputStream(mobileSpli.getXml());
		
		try 
		{
			return NeoXMLUtils.newDocument(in);
		} catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	public static void removeXMLClient(String idSplit) 
	{
		QLGroupFilter group = new QLGroupFilter("AND");
//		group.addFilter(new QLEqualsFilter("mobileEntity", CasvigMobileXmlSplit.ME_CLIENT));
		group.addFilter(new QLEqualsFilter("idSplit", idSplit));
		
		List<CasvigMobileXmlSplit> mobileSplits = (List<CasvigMobileXmlSplit>) PersistEngine.getObjects(CasvigMobileXmlSplit.class, group);
		for (CasvigMobileXmlSplit mobileSplit : mobileSplits) 
		{
			PersistEngine.removeById(mobileSplit.getNeoId());
		}
	}
	
	/**
	 * Gera número identificador.
	 * 
	 * @return identificador único
	 * 
	 * @author Daniel Henrique Joppi 18/11/2009
	 */
	private static String generateIdSplit() 
	{
		return String.valueOf(System.currentTimeMillis());
	}
	
	/**
	 * Método que retorna o primeiro idSplit que estiver na base.
	 * 
	 * @return idSplit
	 */
	public static String giveMeIdSplit() 
	{
		CasvigMobileXmlSplit mobileSplit = (CasvigMobileXmlSplit) PersistEngine.getObject(CasvigMobileXmlSplit.class, new QLEqualsFilter("mobileEntity", "index"));
		return mobileSplit.getIdSplit();
	}
}
