package com.neomind.fusion.custom.orsegups.e2doc.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docComercialPublicoEngine;
import com.neomind.fusion.custom.orsegups.e2doc.xml.ComercialPublico.Pastas;

@Path(value = "documentosComercialPublico")
public class E2docComercialPublicoHandler {
    
	@POST
	@Path("pesquisaComercialPublico")
	@Produces("application/json")
	public Pastas pesquisaComercialPublico(
		@MatrixParam("status") String status, @MatrixParam("empresa") String empresa,
		@MatrixParam("cgcCpf") String cgcCpf, @MatrixParam("nomeCliente") String nomeCliente,
		@MatrixParam("contrato") String contrato, @MatrixParam("ano") String ano,
		@MatrixParam("tipoDocumento") String tipoDocumento) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (status != null && !status.trim().isEmpty()){
		atributosPesquisa.put("STATUS", status.trim());
	    }
	    
	    if(empresa != null && !empresa.trim().isEmpty()){
		atributosPesquisa.put("EMPRESA", empresa.trim());
	    }
	    
	    if(cgcCpf != null && !cgcCpf.trim().isEmpty()){
		atributosPesquisa.put("CNPJ", "%"+cgcCpf.trim()+"%");
	    }
	    
	    if(ano != null && !ano.trim().isEmpty()){
		atributosPesquisa.put("ANO", "%"+ano.trim()+"%");
	    }
	    
	    if(nomeCliente != null && !nomeCliente.trim().isEmpty()){
		atributosPesquisa.put("NOME DO CLIENTE / FORNECEDOR", "%"+nomeCliente.trim()+"%");
	    }
	    
	    if(contrato != null && !contrato.trim().isEmpty()){
		atributosPesquisa.put("NÚMERO DO CONTRATO", "%"+contrato.trim()+"%");
	    }
	    
	    if(tipoDocumento != null && !tipoDocumento.trim().isEmpty()){
		atributosPesquisa.put("TIPO DE DOCUMENTO", tipoDocumento.trim());
	    }

	    E2docComercialPublicoEngine e2docEngine = new E2docComercialPublicoEngine();
	    
	    Pastas pastas = e2docEngine.pesquisaComercialPublico(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("getStatus")
	@Produces("application/json")
	public List<String> getStatus() {
	    
	    E2docComercialPublicoEngine e2docEngine = new E2docComercialPublicoEngine();
	    
	    return e2docEngine.getStatus();

	}
	
	@POST
	@Path("getEmpresas")
	@Produces("application/json")
	public List<String> getEmpresas() {
	    
	    E2docComercialPublicoEngine e2docEngine = new E2docComercialPublicoEngine();
	    
	    return e2docEngine.getEmpresas();
	    

	}
	
	@POST
	@Path("getTipoDocumento")
	@Produces("application/json")
	public List<String> getTipoDocumento() {
	    
	    E2docComercialPublicoEngine e2docEngine = new E2docComercialPublicoEngine();
	    
	    return e2docEngine.getTipoDocumento();
	    

	}
	
	@GET
	@Path("downloadImagem2/{id}")
	@Produces("application/pdf")
	public Response getFile2(@PathParam("id") String id) throws MalformedURLException, IOException
	{
	    E2docComercialPublicoEngine e2docEngine = new E2docComercialPublicoEngine();

	    HashMap<String,Object> map = e2docEngine.retornaLinkImagemDocumento2(id);
	    String ext = (String) map.get("ext");
	    File file = (File) map.get("file");
	    ResponseBuilder response = Response.ok((Object)file);
	    response.header("Content-Disposition", "attachment;filename=temp-"+new GregorianCalendar().getTimeInMillis()+"."+ext);
	    return response.build();

	}
}
