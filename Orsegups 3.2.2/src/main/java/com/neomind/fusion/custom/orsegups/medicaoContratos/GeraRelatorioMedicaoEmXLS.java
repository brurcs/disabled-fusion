package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.ReportUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class GeraRelatorioMedicaoEmXLS
{

	public static void execute(HttpServletResponse response,String sql)
	{

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet abaMedicao = workbook.createSheet("Medição de contratos");

		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);

		Row row = abaMedicao.createRow(0);

		Cell cellCliente = row.createCell(0);
		cellCliente.setCellStyle(style);
		cellCliente.setCellValue("Cliente");

		Cell cellContrato = row.createCell(1);
		cellContrato.setCellStyle(style);
		cellContrato.setCellValue("Contrato");

		Cell cellEmpresa = row.createCell(2);
		cellEmpresa.setCellStyle(style);
		cellEmpresa.setCellValue("Empresa Contratada");

		Cell cellResponsavel = row.createCell(3);
		cellResponsavel.setCellStyle(style);
		cellResponsavel.setCellValue("Responsavel");

		Cell cellMes = row.createCell(4);
		cellMes.setCellStyle(style);
		cellMes.setCellValue("Mês");

		Cell cellRegional = row.createCell(5);
		cellRegional.setCellStyle(style);
		cellRegional.setCellValue("Regional");

		Row row2 = abaMedicao.createRow(2);

		Cell cellMatriNome = row2.createCell(0);
		cellMatriNome.setCellStyle(style);
		cellMatriNome.setCellValue("Matricula/Nome");

		Cell cellCHPosto = row2.createCell(1);
		cellCHPosto.setCellStyle(style);
		cellCHPosto.setCellValue("C.H Posto");

		Cell cellFuncao = row2.createCell(2);
		cellFuncao.setCellStyle(style);
		cellFuncao.setCellValue("Função");

		Cell cellLocal = row2.createCell(3);
		cellLocal.setCellStyle(style);
		cellLocal.setCellValue("Local");

		Cell cellDatAdm = row2.createCell(4);
		cellDatAdm.setCellStyle(style);
		cellDatAdm.setCellValue("Data Admissão");

		Cell cellSit = row2.createCell(5);
		cellSit.setCellStyle(style);
		cellSit.setCellValue("Situação");

		Cell cellDesMed = row2.createCell(6);
		cellDesMed.setCellStyle(style);
		cellDesMed.setCellValue("Desc. Medição");

		Connection conn = null;

		PreparedStatement pstm = null;
		ResultSet rs = null;
		GregorianCalendar data = new GregorianCalendar();
		String nomeArquivo = "medicaoDeContratos.xls";
		try
		{
			conn = PersistEngine.getConnection("");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			int rowNum = 1;
			while (rs.next())
			{

				Row linha = abaMedicao.createRow(rowNum);

				Cell cellClienteSql = linha.createCell(0);
				cellClienteSql.setCellStyle(style);
				cellClienteSql.setCellValue(rs.getString("cliente"));

				Cell cellContratoSql = linha.createCell(1);
				cellContratoSql.setCellStyle(style);
				cellContratoSql.setCellValue(rs.getLong("contrato"));

				Cell cellMatriculaSql = linha.createCell(2);
				cellMatriculaSql.setCellStyle(style);
				cellMatriculaSql.setCellValue(rs.getString("empresacontratada"));

				Cell cellResponsavelSql = linha.createCell(3);
				cellResponsavelSql.setCellStyle(style);
				cellResponsavelSql.setCellValue(rs.getString("responsavel"));

				Cell cellMesSql = linha.createCell(4);
				cellMesSql.setCellStyle(style);
				cellMesSql.setCellValue(rs.getString("periodo"));

				Cell cellRegionalSql = linha.createCell(5);
				cellRegionalSql.setCellStyle(style);
				cellRegionalSql.setCellValue(rs.getString("regional"));

				rowNum = rowNum + 2;
				linha = abaMedicao.createRow(rowNum);

				Cell cellMatriNomeSql = linha.createCell(0);
				cellMatriNomeSql.setCellStyle(style);
				cellMatriNomeSql.setCellValue(rs.getLong("numCadColaborador") + "/" + rs.getString("nomeColaborador"));

				Cell cellCHPostoSql = linha.createCell(1);
				cellCHPostoSql.setCellStyle(style);
				cellCHPostoSql.setCellValue(rs.getString("cargaHorPos"));

				Cell cellFuncaoSql = linha.createCell(2);
				cellFuncaoSql.setCellStyle(style);
				cellFuncaoSql.setCellValue(rs.getString("funcao"));

				Cell cellLocalSql = linha.createCell(3);
				cellLocalSql.setCellStyle(style);
				cellLocalSql.setCellValue(rs.getString("local"));

				Cell cellDatAdmSql = linha.createCell(4);
				cellDatAdmSql.setCellStyle(style);
				cellDatAdmSql.setCellValue(rs.getString("datAdm"));

				Cell cellSitSql = linha.createCell(5);
				cellSitSql.setCellStyle(style);
				cellSitSql.setCellValue(rs.getString("situacao"));

				Cell cellDesMedSql = linha.createCell(6);
				cellDesMedSql.setCellStyle(style);
				cellDesMedSql.setCellValue(rs.getString("descMed"));
				rowNum++;
				
				linha = abaMedicao.createRow(rowNum);
				Cell cellConsideracao = linha.createCell(2);
				cellConsideracao.setCellStyle(style);
				if(rs.getString("descricao") != null){
					cellConsideracao.setCellValue("Consideração: "+rs.getString("descricao"));
				}				

				rowNum++;

			}

			//arquivo = new FileOutputStream(new File(nomeArquivo));
			//workbook.write(arquivo);
			//arquivo.close();
//			response.setContentType("application/x-download");
//			response.setHeader("Content-Disposition", "attachment; filename=\"medicaoDeContratos.xls\"");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			workbook.write(baos);
			baos.close();
			response.setHeader("Content-Disposition", "attachment; filename=medicaoDeContratos.xls");
			response.setContentType("application/octet-stream");
			response.getOutputStream().write(baos.toByteArray());
			
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			System.out.println("Arquivo não encontrado!");
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.out.println("Erro na edição do arquivo!");
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}

}
