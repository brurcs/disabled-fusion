package com.neomind.fusion.custom.orsegups.presenca.bioPresenca.Handler;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.neomind.fusion.custom.orsegups.endpoint.annotation.Secured;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.BioPresencaDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.ErroDTOPai;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.RespostaDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.Engine.BioPresencaEngine;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.Validate.ValidacaoBean;

/**
 * 
 * @author lucas.alison
 *
 */

@Path(value = "BioPresenca")
public class BioPresencaHandler {
    
	@POST
	@Secured	
	@Path("novaMarcacao")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response novaMarcacao(@Valid BioPresencaDTO b) {
	    List<ErroDTOPai> erros = ValidacaoBean.validar(b);
	    RespostaDTO resposta = new RespostaDTO();
	    BioPresencaEngine bioPresencaEngine = new BioPresencaEngine();
	    if (erros.isEmpty()){
		resposta = bioPresencaEngine.novaMarcacao(b);
		if (resposta.getStatus().equals(412)){
		    resposta.setMsg(Response.Status.PRECONDITION_FAILED.getReasonPhrase());
		    bioPresencaEngine.gravarLog(b, resposta);
		    return Response.status(Response.Status.PRECONDITION_FAILED).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		}else if (resposta.getStatus().equals(500)){
		    resposta.setMsg(Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase());
		    bioPresencaEngine.gravarLog(b, resposta);
		    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		}else{
		    bioPresencaEngine.gravarLog(b, resposta);
		    return Response.ok(resposta, MediaType.APPLICATION_JSON_TYPE).build();
		}
	    }else{
		resposta.setStatus(Response.Status.BAD_REQUEST.getStatusCode());
		resposta.setMsg(Response.Status.BAD_REQUEST.getReasonPhrase());
		resposta.setErros(erros);
		bioPresencaEngine.gravarLog(b, resposta);
		return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
	    }
	}
	
}
