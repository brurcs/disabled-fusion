package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ValeTransporteVO
{
	private String linha;
	private String quantidade;
	private String data;
	
	
	public ValeTransporteVO()
	{
		super();
	}
	public ValeTransporteVO(String linha, String quantidade, String data)
	{
		super();
		this.linha = linha;
		this.quantidade = quantidade;
		this.data = data;
	}
	public String getLinha()
	{
		return linha;
	}
	public void setLinha(String linha)
	{
		this.linha = linha;
	}
	public String getQuantidade()
	{
		return quantidade;
	}
	public void setQuantidade(String quantidade)
	{
		this.quantidade = quantidade;
	}
	public String getData()
	{
		return data;
	}
	public void setData(String data)
	{
		this.data = data;
	}
	
	@Override
	public String toString()
	{
		return "ValeTransporteVO [linha=" + linha + ", quantidade=" + quantidade + ", data=" + data + "]";
	}
	
	
	

}
