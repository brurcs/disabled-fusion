package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.itextpdf.text.log.Logger;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.util.JAUtils;
import com.neomind.fusion.custom.orsegups.presenca.PresencaUtil;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class JAInserirAfastamentoAdapter implements AdapterInterface {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(JAInserirAfastamentoAdapter.class);

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	try {

	    long sitAfa = 0L;

	    NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");

	    EntityWrapper wColaborador = new EntityWrapper(colaborador);

	    String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));

	    NeoObject tipoDoc = (NeoObject) processEntity.getValue("tipoDocumento");

	    EntityWrapper wTipoDoc = new EntityWrapper(tipoDoc);

	    /**
	     * 1 - Atestado; 2 - Punição; 3 - Declaração Comp. médico; 4 -
	     * Outros
	     */
	    long idTipoDoc = (long) wTipoDoc.getValue("idDocumento");

	    if (idTipoDoc == 2L) {
		this.inserirPunicao(activity, processEntity, sitAfa);

	    } else {
		if (idTipoDoc == 1L) {
		    sitAfa = 14L;
		} else if (idTipoDoc == 3L) {
		    sitAfa = 15L;
		} else if (idTipoDoc == 4L) {
		    NeoObject tipoAfa = (NeoObject) processEntity.getValue("tipoAfastamento");
		    EntityWrapper wTipoAfa = new EntityWrapper(tipoAfa);
		    sitAfa = (long) wTipoAfa.getValue("codsit");
		}

		System.out.println("Antes do inserirAfastamento: " + numcad);

		this.inserirAfastamento(activity, processEntity, sitAfa);

		System.out.println("Depois do inserirAfastamento: " + numcad);

	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    throw new WorkflowException(e.getMessage());
	}

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

    }

    private void inserirAfastamento(Activity activity, EntityWrapper processEntity, long sitAfa) throws Exception {

	boolean isAtestadoMesmaDataAfa = (boolean) processEntity.getValue("isDataAtestadoIgualAfa");

	NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");

	EntityWrapper wColaborador = new EntityWrapper(colaborador);

	String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));

	System.out.println("Entrou inserirAfastamento: " + numcad);

	if (!isAtestadoMesmaDataAfa) {
	    if (!this.checkExistenciaAfastamento(processEntity, 1)) {

		System.out.println("Entrou !isAtestadoMesmaDataAfa: " + numcad);

		String retorno = this.updateDataSitAfa(activity, processEntity, sitAfa);

		if (!retorno.equals("SUCESSO")) {
		    throw new Exception("Existem afastamentos para o colaborador que conflitam com o período informado. Favor lançar afastamento manualmente.");
		}else{

		}
	    }else {
		throw new Exception("Existem afastamentos para o colaborador que conflitam com o período informado. Favor lançar afastamento manualmente.");
	    }

	} else {

	    System.out.println("Entrou isAtestadoMesmaDataAfa: " + numcad);
	    String retorno = this.updateSitAfa(activity, processEntity, sitAfa);
	    if (!retorno.equals("SUCESSO")) {
		throw new Exception("Existem afastamentos para o colaborador que conflitam com o período informado. Favor lançar afastamento manualmente.");
	    }else{

	    }
	}
    }

    private void inserirPunicao(Activity activity, EntityWrapper processEntity, long sitAfa) throws Exception {

	NeoObject punicoes = (NeoObject) processEntity.getValue("listaPunicoes");
	EntityWrapper wPunicoes = new EntityWrapper(punicoes);

	long idPunicao = (long) wPunicoes.findField("id").getValue();

	if (idPunicao == 2L) { // SUSPENSAO
	    sitAfa = 24L;

	    boolean isSuspensaoMesmaDataAfa = (boolean) processEntity.getValue("isMesmaDataAfa");

	    if (!isSuspensaoMesmaDataAfa) {
		if (!this.checkExistenciaNovoAfastamento(processEntity, 2)) {
		    String retorno = this.inserirNovoAfastamento(activity, processEntity, sitAfa);
		    if (retorno.equals("SUCESSO")) {
			this.inserirAnotacao(processEntity, sitAfa);
		    }
		} else {
		    throw new Exception("Existem afastamentos para o colaborador que conflitam com o período informado. Favor lançar afastamento manualmente.");
		}

	    } else {
		String retorno = this.updateSitAfa(activity, processEntity, sitAfa);
		if (retorno.equals("SUCESSO")) {
		    this.inserirAnotacao(processEntity, sitAfa);
		} else {
		    throw new Exception("Existem afastamentos para o colaborador que conflitam com o período informado. Favor lançar afastamento manualmente.");
		}
	    }

	} else {// ADVERTENCIA
	    this.inserirAnotacao(processEntity, sitAfa);
	}

    }

    private String updateSitAfa(Activity activity, EntityWrapper processEntity, long sitAfa) {


	try {

	    System.out.println("Entrou updateSitAfa");

	    NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
	    EntityWrapper wColaborador = new EntityWrapper(colaborador);

	    String numemp = NeoUtils.safeOutputString(wColaborador.getValue("numemp"));
	    String tipcol = NeoUtils.safeOutputString(wColaborador.getValue("tipcol"));
	    String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));
	    String numcpf = NeoUtils.safeOutputString(wColaborador.getValue("numcpf"));

	    GregorianCalendar datInicio = (GregorianCalendar) processEntity.getValue("dataIniAfa");
	    GregorianCalendar datTermino = (GregorianCalendar) processEntity.getValue("dataFimAfa");

	    GregorianCalendar datMod = new GregorianCalendar();

	    java.sql.Date inicioSql = new java.sql.Date(datInicio.getTime().getTime());

	    java.sql.Date terminoSql = new java.sql.Date(datTermino.getTime().getTime());

	    GregorianCalendar novoInicio = null;
	    GregorianCalendar novoTermino = null;

	    if (sitAfa == 24L) {
		novoInicio = (GregorianCalendar) processEntity.getValue("perIniSuspensao");
		novoTermino = (GregorianCalendar) processEntity.getValue("perFimSuspensao");
	    } else {
		novoInicio = (GregorianCalendar) processEntity.getValue("datIniAtestado");
		novoTermino = (GregorianCalendar) processEntity.getValue("dataFimAtestado");
	    }

	    NeoObject tipoDoc = (NeoObject) processEntity.getValue("tipoDocumento");
	    EntityWrapper wTipoDoc = new EntityWrapper(tipoDoc);

	    long idTipoDoc = (long) wTipoDoc.getValue("idDocumento");

	    Boolean novoMedico = processEntity.findGenericValue("medico.novoMedico");

	    String codCID = processEntity.findGenericValue("ciddoe.coddoe");

	    String regcon = null;

	    Long codprf = null;

	    Integer codatee = null;

	    String codate = null;

	    Integer orgcla = 0;

	    String nomate = null;

	    Integer rs = null;

	    if(idTipoDoc == 1L){

		if(novoMedico){

		    regcon = processEntity.findGenericValue("medico.crm");

		    regcon = regcon.trim();

		    System.out.println("regcon: " + regcon);

		    codprf = processEntity.findGenericValue("medico.conselho.codigo");

		    System.out.println("codprf: " + codprf);

		    codatee = Integer.parseInt(numCODATE()) + 1;

		    System.out.println("codatee: " + codatee);

		    codate = codatee.toString();

		    System.out.println("codate: " + codate);

		    nomate = processEntity.findGenericValue("medico.nome");

		    System.out.println("nomate: " + nomate);

		    if(codprf == 4){

			orgcla = 1;

		    }else if(codprf == 8){

			orgcla = 2;

		    }else{

			orgcla = 3; 

		    }

		    NeoObject oExecutor = processEntity.findGenericValue("executor");

		    EntityWrapper wExecutor = new EntityWrapper(oExecutor);

		    String numcadAte = wExecutor.findGenericValue("matricula");

		    String nomateAte = wExecutor.findGenericValue("fullName");

		    NeoObject oMedico = processEntity.findGenericValue("medico");

		    EntityWrapper wMedico = new EntityWrapper(oMedico);

		    Long lNumempAte = wMedico.findGenericValue("empresa.numemp");

		    String numempAte = lNumempAte.toString();

		    codatee = Integer.parseInt(numCODATE()) + 1;

		    codate = codatee.toString();

		    System.out.println(numcadAte + " - linha: 257");

		    String sql1 = " INSERT INTO R108TAT (CODATE, ORIATE, NUMEMP, TIPCOL, NUMCAD, NOMATE, CODPRF, SITREG, REGCON) VALUES ('" + codate + "', 'E', '" + numempAte + "', "
			    + "'" + tipcol + "', '" + numcadAte + "', '" + nomate + "', " + codprf + ", 'D', " + regcon + ")";

		    Integer result1 = JAUtils.safeInsert("VETORH", sql1, 3, 3000);

		    System.out.println(numcadAte + " - linha: 264");

		    if(result1 == 0){

			new WorkflowException("Erro na inserção do atendente no Rubi!");

		    }

		    System.out.println(numcad + " - linha: 272");

		    String sql2 = " UPDATE R038AFA SET SITAFA=" + sitAfa + ",usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, '" + new Timestamp(datMod.getTimeInMillis()) + "')), "
			    + "usu_hormod=(DATEPART(HOUR, '" + new Timestamp(datMod.getTimeInMillis()) + "') * 60) + DATEPART(MINUTE, '" + new Timestamp(datMod.getTimeInMillis()) + "'), CODATE='" + codate + "', "
			    + "CODDOE='" + codCID + "', EXMRET = 'N', NOMATE = '" + nomate + "', REGCON = '" + regcon + "', ESTCON = 'SC', ORGCLA = " + orgcla + " WHERE NUMCAD=" + numcad + " AND NUMEMP=" + numemp + " "
			    + "AND TIPCOL=" + tipcol + " AND DATAFA='" + inicioSql + "' AND DATTER='" + terminoSql + "'";

		    rs = JAUtils.safeInsert("VETORH", sql2, 3, 3000);

		    System.out.println(numcad + " - linha: 281");

		    System.out.println(numcad + " - linha: 283 - " + sql2);

		    if (rs > 0) {
			this.salvarHistorico(datInicio, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(numcpf), 0L, activity.getCode() , sitAfa, 0L);

			System.out.println(numcad + " - linha: 288");

			return "SUCESSO";
		    } else {

			System.out.println(numcad + " - linha: 293 - ERRO");
			return "ERRO";
		    }

		}else{

		    regcon = processEntity.findGenericValue("medico.registro.regcon");

		    regcon = regcon.trim();

		    System.out.println("regcon: " + regcon);

		    codate = retornaCODATE(regcon);

		    System.out.println("codate: " + codate);

		    nomate = processEntity.findGenericValue("medico.registro.nomate");

		    System.out.println("nomate: " + nomate);

		    codprf = processEntity.findGenericValue("medico.registro.codprf");

		    System.out.println("codprf: " + codprf);

		    if(codprf == 4){

			orgcla = 1;

		    }else if(codprf == 8){

			orgcla = 2;

		    }else{

			orgcla = 3; 

		    }

		    System.out.println(numcad + " - linha: 329");

		    String sql = " UPDATE R038AFA SET SITAFA=" + sitAfa + ",usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, '" + new Timestamp(datMod.getTimeInMillis()) + "')), "
			    + "usu_hormod=(DATEPART(HOUR, '" + new Timestamp(datMod.getTimeInMillis()) + "') * 60) + DATEPART(MINUTE, '" + new Timestamp(datMod.getTimeInMillis()) + "'), CODATE='" + codate + "', "
			    + "CODDOE='" + codCID + "', EXMRET = 'N', NOMATE = '" + nomate + "', REGCON = '" + regcon + "', ESTCON = 'SC', ORGCLA = " + orgcla + " WHERE NUMCAD=" + numcad + " AND NUMEMP=" + numemp + " "
			    + "AND TIPCOL=" + tipcol + " AND DATAFA='" + inicioSql + "' AND DATTER='" + terminoSql + "'";

		    rs = JAUtils.safeInsert("VETORH", sql, 3, 3000);

		    System.out.println(numcad + " - linha: 338");

		    System.out.println(numcad + " - linha: 340 - " + sql);

		    if (rs > 0) {
			this.salvarHistorico(datInicio, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(numcpf), 0L, activity.getCode() , sitAfa, 0L);

			System.out.println(numcad + " - linha: 345");

			return "SUCESSO";
		    } else {
			return "ERRO";
		    }

		}

	    }else{

		System.out.println(numcad + " - linha: 356");

		String query = "UPDATE R038AFA SET SITAFA=" + sitAfa + ", usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, '" 
			+ new Timestamp(datMod.getTimeInMillis()) + "')), usu_hormod=(DATEPART(HOUR, '" + new Timestamp(datMod.getTimeInMillis()) + "') "
			+ "* 60) + DATEPART(MINUTE, '" + new Timestamp(datMod.getTimeInMillis()) + "')"
			+ " WHERE NUMCAD=" + numcad + " AND NUMEMP= " + numemp + " AND TIPCOL=" + tipcol + " AND DATAFA='" + inicioSql + "' "
			+ "AND DATTER='" + terminoSql + "'";

		rs = JAUtils.safeInsert("VETORH", query, 3, 3000);

		System.out.println(numcad + " - linha: 364");

		System.out.println(numcad + " - linha: 366 - " + query);

		if (rs > 0) {
		    this.salvarHistorico(datInicio, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(numcpf), 0L, activity.getCode() , sitAfa, 0L);

		    System.out.println(numcad + " - linha: 373");

		    return "SUCESSO";
		} else {
		    return "ERRO";
		}


	    } 

	}catch (Exception e) {
	    StringWriter erros = new StringWriter();
	    e.printStackTrace(new PrintWriter(erros));
	    String out = null;

	    try {
		out = erros.toString();
		System.out.println("Primeiro: " + out);
		erros.close();

	    } catch (IOException e1) {
		System.out.println("Segundo: " + out);

	    }

	    return "ERRO";

	}
    }


    private String updateDataSitAfa(Activity activity ,EntityWrapper processEntity, long sitAfa) {

	try {

	    System.out.println("Entrou updateDataSitAfa");

	    NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
	    EntityWrapper wColaborador = new EntityWrapper(colaborador);

	    String numemp = NeoUtils.safeOutputString(wColaborador.getValue("numemp"));
	    String tipcol = NeoUtils.safeOutputString(wColaborador.getValue("tipcol"));
	    String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));
	    String numcpf = NeoUtils.safeOutputString(wColaborador.getValue("numcpf"));

	    GregorianCalendar datInicio = (GregorianCalendar) processEntity.getValue("dataIniAfa");
	    GregorianCalendar datTermino = (GregorianCalendar) processEntity.getValue("dataFimAfa");

	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	    String datainicioLog = sdf.format(datInicio.getTime());

	    String datafimLog = sdf.format(datTermino.getTime());

	    System.out.println("JAInserirAfastamentoAdapter - DataInicio: " + datainicioLog);

	    System.out.println("JAInserirAfastamentoAdapter - DataFim: " + datafimLog);

	    GregorianCalendar datMod = new GregorianCalendar();

	    Timestamp dataAtual = new Timestamp(datMod.getTimeInMillis());

	    java.sql.Date inicioSql = new java.sql.Date(datInicio.getTime().getTime());	
	    java.sql.Date terminoSql = new java.sql.Date(datTermino.getTime().getTime());

	    GregorianCalendar novoInicio = null;
	    GregorianCalendar novoTermino = null;

	    if (sitAfa == 24L) {
		novoInicio = (GregorianCalendar) processEntity.getValue("perIniSuspensao");
		novoTermino = (GregorianCalendar) processEntity.getValue("perFimSuspensao");
	    } else {
		novoInicio = (GregorianCalendar) processEntity.getValue("datIniAtestado");
		novoTermino = (GregorianCalendar) processEntity.getValue("dataFimAtestado");
	    }

	    java.sql.Date novoInicioSql = new java.sql.Date(novoInicio.getTime().getTime());	
	    java.sql.Date novoTerminoSql = new java.sql.Date(novoTermino.getTime().getTime());

	    NeoObject tipoDoc = (NeoObject) processEntity.getValue("tipoDocumento");
	    EntityWrapper wTipoDoc = new EntityWrapper(tipoDoc);

	    long idTipoDoc = (long) wTipoDoc.getValue("idDocumento");

	    Boolean novoMedico = processEntity.findGenericValue("medico.novoMedico");

	    String codCID = processEntity.findGenericValue("ciddoe.coddoe");

	    String regcon = null;

	    Long codprf = null;

	    Integer codatee = null;

	    String codate = null;

	    Integer orgcla = 0;

	    String nomate = null;

	    Integer rs = null;

	    if(idTipoDoc == 1L){

		if(novoMedico){

		    regcon = processEntity.findGenericValue("medico.crm");

		    regcon = regcon.trim();

		    System.out.println("regcon: " + regcon);

		    codprf = processEntity.findGenericValue("medico.conselho.codigo");

		    System.out.println("codprf: " + codprf);

		    codatee = Integer.parseInt(numCODATE()) + 1;

		    System.out.println("codatee: " + codatee);

		    codate = codatee.toString();

		    System.out.println("codate: " + codate);

		    nomate = processEntity.findGenericValue("medico.nome");

		    System.out.println("nomate: " + nomate);

		    if(codprf == 4){

			orgcla = 1;

		    }else if(codprf == 8){

			orgcla = 2;

		    }else{

			orgcla = 3; 

		    }

		    NeoObject oExecutor = processEntity.findGenericValue("executor");

		    EntityWrapper wExecutor = new EntityWrapper(oExecutor);

		    String numcadAte = wExecutor.findGenericValue("matricula");

		    String nomateAte = wExecutor.findGenericValue("fullName");

		    NeoObject oMedico = processEntity.findGenericValue("medico");

		    EntityWrapper wMedico = new EntityWrapper(oMedico);

		    Long lNumempAte = wMedico.findGenericValue("empresa.numemp");
		    
		    String numempAte = lNumempAte.toString();

		    codatee = Integer.parseInt(numCODATE()) + 1;

		    codate = codatee.toString();

		    System.out.println(numcad + " - linha: 515");

		    String sql1 = " INSERT INTO R108TAT (CODATE, ORIATE, NUMEMP, TIPCOL, NUMCAD, NOMATE, CODPRF, SITREG, REGCON) VALUES ('" + codate + "', 'E', '" + numempAte + "', "
			    + "'" + tipcol + "', '" + numcadAte + "', '" + nomate + "', " + codprf + ", 'D', " + regcon + ")";

		    rs = JAUtils.safeInsert("VETORH", sql1, 3, 3000);

		    System.out.println(numcad + " - linha: 522");

		    System.out.println(numcad + " - linha: 524 - " + sql1);

		    if(rs == 0){

			new WorkflowException("Erro na inserção do atendente no Rubi!");

		    }

		    System.out.println(numcad + " - linha: 532");

		    String sql2 = "SET DATEFORMAT ymd;"
			    + " DECLARE @datetime datetime = '" + dataAtual + "';"
			    + "UPDATE R038AFA SET SITAFA=" + sitAfa + ", CODATE='" + codate + "', CODDOE='" + codCID + "', EXMRET = 'N', NOMATE = '" + nomate + "', REGCON = '" + regcon + "'"
			    + ", ESTCON = 'SC', ORGCLA = " + orgcla + ", DATAFA='" + novoInicioSql + "',DATTER='" + novoTerminoSql + "', usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, "
			    + "@datetime)), usu_hormod=(DATEPART(HOUR, @datetime) * 60) + DATEPART(MINUTE, @datetime) WHERE NUMCAD=" + numcad + " AND NUMEMP="
			    + "" + numemp + " AND TIPCOL=" + tipcol + " AND DATAFA='" + inicioSql + "' AND DATTER='" + terminoSql + "'";

		    rs = JAUtils.safeInsert("VETORH", sql2, 3, 3000);

		    System.out.println(numcad + " - linha: 543");

		    System.out.println(numcad + " - linha: 545 - " + sql2);

		    if(rs == 0){

			new WorkflowException("Erro na atualização do afastamento!");

		    }else{

			this.salvarHistorico(datInicio, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(numcpf), 0L, activity.getCode() , sitAfa, 0L);

			System.out.println(numcad + " - linha: 555");

		    }

		}else{

		    regcon = processEntity.findGenericValue("medico.registro.regcon");

		    regcon = regcon.trim();

		    System.out.println("regcon: " + regcon);

		    codate = retornaCODATE(regcon);

		    System.out.println("codate: " + codate);

		    nomate = processEntity.findGenericValue("medico.registro.nomate");

		    System.out.println("nomate: " + nomate);

		    codprf = processEntity.findGenericValue("medico.registro.codprf");

		    System.out.println("codprf: " + codprf);

		    if(codprf == 4){

			orgcla = 1;

		    }else if(codprf == 8){

			orgcla = 2;

		    }else{

			orgcla = 3; 

		    }

		    String sql = "SET DATEFORMAT ymd;"
			    + "DECLARE @datetime datetime = '" + dataAtual + "';"
			    + " UPDATE R038AFA SET SITAFA=" + sitAfa + ",DATAFA='" + novoInicioSql + "',DATTER='" + novoTerminoSql + "',CODATE='"  
			    + codate + "', CODDOE='" + codCID + "', EXMRET = 'N', NOMATE = '" + nomate + "', REGCON = '" + regcon + "', ESTCON = 'SC', "
			    + "ORGCLA = " + orgcla + ", usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, @datetime)), usu_hormod=(DATEPART(HOUR, @datetime) * 60) "
			    + "+ DATEPART(MINUTE, @datetime) WHERE NUMCAD=" + numcad + " AND NUMEMP=" + numemp + " AND TIPCOL=" + tipcol + ""
			    + " AND DATAFA='" + inicioSql + "' AND DATTER='" + terminoSql + "'";

		    rs = JAUtils.safeInsert("VETORH", sql, 3, 3000);

		    System.out.println(numcad + " - linha: 596 - " + sql);

		    if (rs > 0){

			System.out.println(numcad + " - linha: 600");

			this.salvarHistorico(datInicio, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(numcpf), 0L, activity.getCode() , sitAfa, 0L);

		    }

		}

	    }else{

		String query = "SET DATEFORMAT ymd;"
			+ "DECLARE @datetime datetime = '" + dataAtual + "';"
			+ "UPDATE R038AFA SET SITAFA=" + sitAfa + ",DATAFA='" + novoInicioSql + "',DATTER=" + novoTerminoSql + ", usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, "
			+ "@datetime)), usu_hormod=(DATEPART(HOUR, @datetime) * 60) + DATEPART(MINUTE, @datetime) "
			+ "WHERE NUMCAD=" + numcad + " AND NUMEMP=" + numemp + " AND TIPCOL=" + tipcol + " AND DATAFA=" + inicioSql + " AND DATTER=" + terminoSql;

		rs = JAUtils.safeInsert("VETORH", query, 3, 3000);

		System.out.println(numcad + " - linha: 618 - " + query);

		if (rs > 0){

		    System.out.println(numcad + " - linha: 622");

		    this.salvarHistorico(datInicio, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(numcpf), 0L, activity.getCode() , sitAfa, 0L);

		}

	    }

	    if (rs > 0) {
		processEntity.findField("dataIniAfa").setValue(novoInicio);
		processEntity.findField("dataFimAfa").setValue(novoTermino);

		this.salvarHistorico(datInicio, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(numcpf), 0L, activity.getCode() , sitAfa, 0L);

		return "SUCESSO";
	    } else {
		return "ERRO";
	    }

	} catch (Exception e) {
	    StringWriter erros = new StringWriter();
	    e.printStackTrace(new PrintWriter(erros));
	    String out = null;

	    try {
		out = erros.toString();
		System.out.println("Primeiro: " + out);
		erros.close();

	    } catch (IOException e1) {
		System.out.println("Segundo: " + out);

	    }

	    return "ERRO";
	}
    }



    private String inserirNovoAfastamento(Activity activity, EntityWrapper processEntity, long sitAfa) {

	Connection conn = null;
	PreparedStatement pstm = null;	

	try {

	    StringBuilder sql = new StringBuilder();

	    NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
	    EntityWrapper wColaborador = new EntityWrapper(colaborador);

	    String numemp = NeoUtils.safeOutputString(wColaborador.getValue("numemp"));
	    String tipcol = NeoUtils.safeOutputString(wColaborador.getValue("tipcol"));
	    String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));
	    String numcpf = NeoUtils.safeOutputString(wColaborador.getValue("numcpf"));

	    GregorianCalendar novoInicio = null;
	    GregorianCalendar novoTermino = null;

	    String afastamentoInformado = "S";

	    if (sitAfa == 24L) {
		novoInicio = (GregorianCalendar) processEntity.getValue("perIniSuspensao");
		novoTermino = (GregorianCalendar) processEntity.getValue("perFimSuspensao");
	    } else {
		novoInicio = (GregorianCalendar) processEntity.getValue("datIniAtestado");
		novoTermino = (GregorianCalendar) processEntity.getValue("dataFimAtestado");
	    }

	    java.sql.Date novoInicioSql = new java.sql.Date(novoInicio.getTime().getTime());	
	    java.sql.Date novoTerminoSql = new java.sql.Date(novoTermino.getTime().getTime());

	    java.sql.Date dataAtualSql = new java.sql.Date(Calendar.getInstance().getTime().getTime());

	    NeoUser user = PortalUtil.getCurrentUser();

	    String dataString = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm");

	    String obsAfa = "Inserido via tarefa " + activity.getCode() + " do processo J003 - Afastamento - pelo usuário " + user.getFullName() + " em " + dataString;

	    sql.append(" Insert Into R038AFA (NumEmp, TipCol, NumCad, DatAfa, HorAfa, DatTer, SitAfa, OriAfa, ObsAfa, StaAtu, usu_codusu, usu_datger, usu_afainf,horTer, usu_datmod, usu_hormod) ");
	    sql.append(" Values (?,?,?,?,0,?,?,0,?,0,389,?,?,0,DATEADD(dd, 0, DATEDIFF(dd, 0, ?)), (DATEPART(HOUR, ?) * 60) + DATEPART(MINUTE, ?)) ");

	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, Integer.parseInt(numemp));
	    pstm.setInt(2, Integer.parseInt(tipcol));
	    pstm.setInt(3, Integer.parseInt(numcad));
	    pstm.setDate(4, novoInicioSql);
	    pstm.setDate(5, novoTerminoSql);
	    pstm.setLong(6, sitAfa);
	    pstm.setString(7, obsAfa);
	    pstm.setDate(8, dataAtualSql);
	    pstm.setString(9, afastamentoInformado);
	    GregorianCalendar datMod = new GregorianCalendar();
	    pstm.setTimestamp(10, new Timestamp(datMod.getTimeInMillis()), datMod);
	    pstm.setTimestamp(11, new Timestamp(datMod.getTimeInMillis()), datMod);
	    pstm.setTimestamp(12, new Timestamp(datMod.getTimeInMillis()), datMod);

	    int rs = pstm.executeUpdate();

	    if (rs > 0) {
		processEntity.findField("dataIniAfa").setValue(novoInicio);
		processEntity.findField("dataFimAfa").setValue(novoTermino);

		this.salvarHistorico(novoInicio, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(numcpf), 0L, activity.getCode() , sitAfa, 0L);

		return "SUCESSO";
	    } else {
		return "ERRO";
	    }

	} catch (Exception e) {
	    e.printStackTrace();

	    return "ERRO";

	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

    }

    /**
     * Verifica se é possivel alterar a data do afastamento existente para o
     * novo periodo informado.
     * 
     * @param processEntity
     * @param tipoAfastamento
     *            - 1 Atestado - 2 Punição
     * @return boolean se existe ou não afastamento conflitante
     */
    private boolean checkExistenciaAfastamento(EntityWrapper processEntity, int tipoAfastamento) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    StringBuilder sql = new StringBuilder();

	    NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
	    EntityWrapper wColaborador = new EntityWrapper(colaborador);

	    String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));
	    String numemp = NeoUtils.safeOutputString(wColaborador.getValue("numemp"));
	    String tipcol = NeoUtils.safeOutputString(wColaborador.getValue("tipcol"));

	    System.out.println("Entrou checkExistenciaAfastamento: " + numcad);

	    GregorianCalendar datInicio = (GregorianCalendar) processEntity.getValue("dataIniAfa");
	    GregorianCalendar datTermino = (GregorianCalendar) processEntity.getValue("dataFimAfa");

	    java.sql.Date inicioSql = new java.sql.Date(datInicio.getTime().getTime());	
	    java.sql.Date terminoSql = new java.sql.Date(datTermino.getTime().getTime());

	    GregorianCalendar novoInicio = null;
	    GregorianCalendar novoTermino = null;

	    if (tipoAfastamento == 1) {
		novoInicio = (GregorianCalendar) processEntity.getValue("datIniAtestado");
		novoTermino = (GregorianCalendar) processEntity.getValue("dataFimAtestado");
	    } else {
		novoInicio = (GregorianCalendar) processEntity.getValue("perIniSuspensao");
		novoTermino = (GregorianCalendar) processEntity.getValue("perFimSuspensao");
	    }

	    java.sql.Date novoInicioSql = new java.sql.Date(novoInicio.getTime().getTime());	
	    java.sql.Date novoTerminoSql = new java.sql.Date(novoTermino.getTime().getTime());

	    sql.append(" SELECT COUNT(*) AS CONTAGEM FROM R038AFA WITH(NOLOCK) ");
	    sql.append(" WHERE NumEmp=? AND NumCad=? AND TipCol=? ");
	    sql.append(" AND ");
	    sql.append(" ( ");
	    sql.append(" (DatAfa != ? AND DatTer != ?) ");
	    sql.append(" AND ");
	    sql.append(" ( ");
	    sql.append(" (	(DatAfa BETWEEN ? AND ?) OR (DatTer BETWEEN ? AND ?)  ) ");
	    sql.append(" OR ");
	    sql.append(" (	( ? BETWEEN DatAfa AND DatTer) OR ( ? BETWEEN DatAfa AND DatTer)  )  ");
	    sql.append(" ) ");
	    sql.append(" ) ");

	    System.out.println("checkExistenciaAfastamento - Consulta: " + sql.toString());

	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, Integer.parseInt(numemp));
	    pstm.setInt(2, Integer.parseInt(numcad));
	    pstm.setInt(3, Integer.parseInt(tipcol));
	    pstm.setDate(4, inicioSql);
	    pstm.setDate(5, terminoSql);
	    pstm.setDate(6, novoInicioSql);
	    pstm.setDate(7, novoTerminoSql);
	    pstm.setDate(8, novoInicioSql);
	    pstm.setDate(9, novoTerminoSql);
	    pstm.setDate(10, novoInicioSql);
	    pstm.setDate(11, novoTerminoSql);

	    rs = pstm.executeQuery();

	    if (rs.next()) {

		System.out.println("checkExistenciaAfastamento - Entrou no rs: " + numcad);
		int count = rs.getInt(1);

		if (count > 0) {
		    return true;
		} else {
		    return false;
		}

	    } else {
		System.out.println("checkExistenciaAfastamento - Não entrou no rs: " + numcad);
		return false;
	    }

	} catch (SQLException e) {

	    e.printStackTrace();

	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return false;
    }

    private boolean checkExistenciaNovoAfastamento(EntityWrapper processEntity, int tipoAfastamento) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    StringBuilder sql = new StringBuilder();

	    NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
	    EntityWrapper wColaborador = new EntityWrapper(colaborador);

	    String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));
	    String numemp = NeoUtils.safeOutputString(wColaborador.getValue("numemp"));
	    String tipcol = NeoUtils.safeOutputString(wColaborador.getValue("tipcol"));

	    GregorianCalendar novoInicio = null;
	    GregorianCalendar novoTermino = null;

	    if (tipoAfastamento == 1) {
		novoInicio = (GregorianCalendar) processEntity.getValue("datIniAtestado");
		novoTermino = (GregorianCalendar) processEntity.getValue("dataFimAtestado");
	    } else {
		novoInicio = (GregorianCalendar) processEntity.getValue("perIniSuspensao");
		novoTermino = (GregorianCalendar) processEntity.getValue("perFimSuspensao");
	    }

	    java.sql.Date novoInicioSql = new java.sql.Date(novoInicio.getTime().getTime());	
	    java.sql.Date novoTerminoSql = new java.sql.Date(novoTermino.getTime().getTime());

	    sql.append(" SELECT COUNT(*) AS CONTAGEM FROM R038AFA ");
	    sql.append(" WHERE NumEmp=? AND NumCad=? AND TipCol=? ");
	    sql.append(" AND  ");
	    sql.append(" ( ");
	    sql.append(" ( ");
	    sql.append(" (	(DatAfa BETWEEN ? AND ?) OR (DatTer BETWEEN ? AND ?)  ) ");
	    sql.append(" OR ");
	    sql.append(" (	( ? BETWEEN DatAfa AND DatTer) OR ( ? BETWEEN DatAfa AND DatTer)  )  ");
	    sql.append(" ) ");
	    sql.append(" ) ");

	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, Integer.parseInt(numemp));
	    pstm.setInt(2, Integer.parseInt(numcad));
	    pstm.setInt(3, Integer.parseInt(tipcol));
	    pstm.setDate(4, novoInicioSql);
	    pstm.setDate(5, novoTerminoSql);
	    pstm.setDate(6, novoInicioSql);
	    pstm.setDate(7, novoTerminoSql);
	    pstm.setDate(8, novoInicioSql);
	    pstm.setDate(9, novoTerminoSql);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		int count = rs.getInt(1);

		if (count > 0) {
		    return true;
		} else {
		    return false;
		}

	    } else {
		return false;
	    }

	} catch (SQLException e) {

	    e.printStackTrace();

	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return false;
    }


    private void inserirAnotacao(EntityWrapper processEntity, long sitAfa) {

	Connection conn = null;
	PreparedStatement pstm = null;

	try {

	    int tipNot = 0;

	    if (sitAfa == 24L) {
		tipNot = 2;
	    } else {
		tipNot = 3;
	    }

	    NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
	    EntityWrapper wColaborador = new EntityWrapper(colaborador);

	    long numCad = (long) wColaborador.findField("numcad").getValue();
	    long numEmp = (long) wColaborador.findField("numemp").getValue();
	    long tipCol = (long) wColaborador.findField("tipcol").getValue();

	    String notFic = processEntity.findField("obsAdvertencia").getValueAsString();
	    GregorianCalendar datNot = new GregorianCalendar();
	    datNot.set(GregorianCalendar.HOUR_OF_DAY, 0);
	    datNot.set(GregorianCalendar.MINUTE, 0);
	    datNot.set(GregorianCalendar.SECOND, 0);
	    datNot.set(GregorianCalendar.MILLISECOND, 0);

	    int seqNot = this.getSeqAnotacao(numCad, numEmp, tipCol);


	    String sql = "INSERT INTO R038NOT (NumEmp,TipCol,NumCad,DatNot,seqNot,tipNot,NotFic,EmpDig,cadDig,OriInf,USU_Codusu,NroDoc,TipDig) " + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ";


	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql);

	    pstm.setLong(1, numEmp);
	    pstm.setLong(2, tipCol);
	    pstm.setLong(3, numCad);
	    pstm.setTimestamp(4, new Timestamp(datNot.getTimeInMillis()));
	    pstm.setInt(5, seqNot);
	    pstm.setInt(6, tipNot);
	    pstm.setString(7, notFic);
	    pstm.setInt(8, 0);
	    pstm.setInt(9, 0);
	    pstm.setString(10, "D");
	    pstm.setInt(11, 0);
	    pstm.setString(12, "");
	    pstm.setInt(13, 0);

	    pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

    }

    private int getSeqAnotacao(long numCad, long numEmp, long tipCol) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    GregorianCalendar datNot = new GregorianCalendar();
	    datNot.set(GregorianCalendar.HOUR_OF_DAY, 0);
	    datNot.set(GregorianCalendar.MINUTE, 0);
	    datNot.set(GregorianCalendar.SECOND, 0);
	    datNot.set(GregorianCalendar.MILLISECOND, 0);

	    String sql = "SELECT MAX(SeqNot) FROM R038NOT WITH(NOLOCK) WHERE numcad = ? AND numemp = ? AND tipcol = ? AND datNot = ?";


	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql);

	    pstm.setLong(1, numCad);
	    pstm.setLong(2, numEmp);
	    pstm.setLong(3, tipCol);
	    pstm.setTimestamp(4, new Timestamp(datNot.getTimeInMillis()));

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		return rs.getInt(1) + 1;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return 1;

    }


    private boolean salvarHistorico(GregorianCalendar datafa, String numemp, Long tipcol, String numcad, Long numcpf, Long horafa, String tarefa, Long sitafa, Long caudem){

	try {

	    QLEqualsFilter filterCpf = new QLEqualsFilter("numcpf", numcpf);
	    QLEqualsFilter filterData = new QLEqualsFilter("datafa", datafa);
	    QLEqualsFilter filterHora = new QLEqualsFilter("horafa", horafa);

	    QLGroupFilter filterGroup = new QLGroupFilter("AND");
	    filterGroup.addFilter(filterCpf);
	    filterGroup.addFilter(filterData);
	    filterGroup.addFilter(filterHora);


	    GregorianCalendar data = (GregorianCalendar) datafa.clone();

	    List<NeoObject> historicos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaAfastamentoJustificativa"), filterGroup);


	    if (historicos != null && !historicos.isEmpty()){

		NeoObject historico = historicos.get(0);

		EntityWrapper wHistorico = new EntityWrapper(historico);

		String tarefas = wHistorico.findField("tarefa").getValueAsString();

		if (!tarefas.contains(tarefa)){
		    tarefas += ","+tarefa;
		}

		wHistorico.setValue("SitAfa", sitafa);
		wHistorico.setValue("tarefa", tarefas);

		PersistEngine.persist(historico);

		return true;

	    }else{
		InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
		NeoObject noAJ = tarefaAJ.createNewInstance();
		EntityWrapper ajWrapper = new EntityWrapper(noAJ);

		ajWrapper.setValue("numemp", NeoUtils.safeLong(numemp));
		ajWrapper.setValue("tipcol", tipcol);
		ajWrapper.setValue("numcad", NeoUtils.safeLong(numcad));
		ajWrapper.setValue("numcpf", numcpf);		
		ajWrapper.setValue("datafa", data);
		ajWrapper.setValue("horafa", horafa);
		ajWrapper.setValue("tarefa", tarefa);
		ajWrapper.setValue("SitAfa", sitafa);
		ajWrapper.setValue("CauDem", caudem);

		PersistEngine.persist(noAJ);

		return true;
	    }
	}catch (Exception e) {
	    System.out.println("Regra: Falta não justificada - erro ao registrar no eform ");
	    e.printStackTrace();

	}

	return false;
    }

    public static String numCODATE()
    {

	Connection connSapiens = null; 

	PreparedStatement pstData = null;


	try{

	    connSapiens = PersistEngine.getConnection("VETORH");

	    String query = " select top 1 codate from r108tat order by codate DESC  ";

	    pstData = connSapiens.prepareStatement(query);

	    java.sql.ResultSet rs = pstData.executeQuery();

	    if(rs.next()){

		return rs.getString("codate");

	    }else{

		log.warn("NÃO EXISTE REGISTRO");

		return null;

	    }

	}
	catch(Exception e){

	    log.warn("");
	    e.printStackTrace();
	    return null;

	}
	finally{

	    OrsegupsUtils.closeConnection(connSapiens, pstData, null);
	}

    }

    public static String retornaCODATE(String regcon)
    {

	Connection connSapiens = null; 

	PreparedStatement pstData = null;


	try{

	    connSapiens = PersistEngine.getConnection("VETORH");

	    String query = " select codate from r108tat where regcon like '" + regcon + "'";

	    pstData = connSapiens.prepareStatement(query);

	    java.sql.ResultSet rs = pstData.executeQuery();

	    if(rs.next()){

		return rs.getString("codate");

	    }else{

		log.warn("NÃO EXISTE REGISTRO");

		return null;

	    }

	}
	catch(Exception e){

	    log.warn("");
	    e.printStackTrace();
	    return null;

	}
	finally{

	    OrsegupsUtils.closeConnection(connSapiens, pstData, null);
	}

    }

    private void insereRegistroECID (EntityWrapper processEntity){

	try {

	    System.out.println("Insere Registro ECID");

	    String codCID = processEntity.findGenericValue("ciddoe.coddoe");

	    System.out.println("codCID: " + codCID);

	    Boolean novoMedico = processEntity.findGenericValue("medico.novoMedico");

	    System.out.println("novoMedico: " + novoMedico);

	    Long numcad = processEntity.findGenericValue("colaborador.numcad");

	    System.out.println("numcad: " + numcad);

	    Long numemp = processEntity.findGenericValue("colaborador.numemp");

	    System.out.println("numemp: " + numemp);

	    Long tipcol = processEntity.findGenericValue("colaborador.tipcol");

	    System.out.println("tipcol: " + tipcol);

	    String nomate = "";

	    GregorianCalendar dataIniAfa = processEntity.findGenericValue("dataIniAfa");

	    System.out.println("dataIniAfa: " + dataIniAfa);

	    GregorianCalendar dataFimAfa = processEntity.findGenericValue("dataFimAfa");

	    System.out.println("dataFimAfa: " + dataFimAfa);

	    String regcon = null;

	    Long codprf = null;

	    Integer codatee = null;

	    String codate = null;

	    Integer orgcla = 0;

	    GregorianCalendar datMod = new GregorianCalendar();

	    Timestamp dataAtual = new Timestamp(datMod.getTimeInMillis());

	    java.sql.Date inicioSql = new java.sql.Date(dataIniAfa.getTime().getTime());	
	    java.sql.Date terminoSql = new java.sql.Date(dataFimAfa.getTime().getTime());

	    if(novoMedico){

		regcon = processEntity.findGenericValue("medico.crm");

		regcon = regcon.trim();

		System.out.println("regcon: " + regcon);

		codprf = processEntity.findGenericValue("medico.conselho.codigo");

		System.out.println("codprf: " + codprf);

		codatee = Integer.parseInt(numCODATE()) + 1;

		System.out.println("codatee: " + codatee);

		codate = codatee.toString();

		System.out.println("codate: " + codate);

		nomate = processEntity.findGenericValue("medico.nome");

		System.out.println("nomate: " + nomate);

		if(codprf == 4){

		    orgcla = 1;

		}else if(codprf == 8){

		    orgcla = 2;

		}else{

		    orgcla = 3; 

		}

		NeoObject oExecutor = processEntity.findGenericValue("executor");

		EntityWrapper wExecutor = new EntityWrapper(oExecutor);

		String numcadAte = wExecutor.findGenericValue("matricula");

		String nomateAte = wExecutor.findGenericValue("fullName");

		NeoObject oMedico = processEntity.findGenericValue("medico");

		EntityWrapper wMedico = new EntityWrapper(oMedico);

		Long lNumempAte = wMedico.findGenericValue("empresa.numemp");
		   
		String numempAte = lNumempAte.toString();

		String sql1 = " INSERT INTO R108TAT (CODATE, ORIATE, NUMEMP, TIPCOL, NUMCAD, NOMATE, CODPRF, SITREG, REGCON) VALUES ('" + codate + "', 'E', '" + numempAte + "', "
			+ "'" + tipcol + "', '" + numcadAte + "', '" + nomate + "', " + codprf + ", 'D', " + regcon + ")";

		Integer result1 = JAUtils.safeInsert("VETORH", sql1, 3, 3000);

		if(result1 == 0){

		    new WorkflowException("Erro na inserção do atendente no Rubi!");

		}

		codatee = Integer.parseInt(numCODATE()) + 1;

		codate = codatee.toString();

		String sql2 = "SET DATEFORMAT ymd;"
			+ "DECLARE @datetime datetime = '" + dataAtual + "';"
			+ " UPDATE R038AFA SET CODATE='" + codate + "', CODDOE='" + codCID + "', EXMRET = 'N', NOMATE = '" + nomate + "', REGCON = "
			+ "'" + regcon + "', ESTCON = 'SC', ORGCLA = " + orgcla + ", usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, @datetime)), "
			+ "usu_hormod=(DATEPART(HOUR, @datetime) * 60) + DATEPART(MINUTE, @datetime) WHERE NUMCAD='" + numcad + "' AND "
			+ "NUMEMP='" + numemp + "' AND TIPCOL='" + tipcol + "' AND DATAFA='" + inicioSql + "' AND DATTER='" + terminoSql + "'";

		Integer result2 = JAUtils.safeInsert("VETORH", sql2, 3, 3000);

		if(result2 == 0){

		    new WorkflowException("Erro na atualização do afastamento!");

		}


	    }else{

		regcon = processEntity.findGenericValue("medico.registro.regcon");

		regcon = regcon.trim();

		System.out.println("regcon: " + regcon);

		codate = retornaCODATE(regcon);

		System.out.println("codate: " + codate);

		nomate = processEntity.findGenericValue("medico.registro.nomate");

		System.out.println("nomate: " + nomate);

		codprf = processEntity.findGenericValue("medico.registro.codprf");

		System.out.println("codprf: " + codprf);

		if(codprf == 4){

		    orgcla = 1;

		}else if(codprf == 8){

		    orgcla = 2;

		}else{

		    orgcla = 3; 

		}

		String sql = " UPDATE R038AFA SET CODATE='" + codate + "', CODDOE='" + codCID + "', EXMRET = 'N', NOMATE = '" + nomate + "', "
			+ "REGCON = '" + regcon + "', ESTCON = 'SC', ORGCLA = " + orgcla + "usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, '"
			+ new Timestamp(datMod.getTimeInMillis()) + "')), usu_hormod=(DATEPART(HOUR, '" + new Timestamp(datMod.getTimeInMillis()) + "') "
			+ "* 60) + DATEPART(MINUTE, '" + new Timestamp(datMod.getTimeInMillis()) + "') WHERE NUMCAD=" + numcad + " AND NUMEMP=" 
			+ numemp + " AND TIPCOL=" + tipcol + " AND DATAFA='" + inicioSql + "' AND DATTER='" + terminoSql + "'";

		Integer result = JAUtils.safeInsert("VETORH", sql, 3, 3000);

		if(result == 0){

		    new WorkflowException("Erro na atualização do afastamento!");

		}

	    }


	}catch (Exception e) {
	    e.printStackTrace();
	}

    }

}
