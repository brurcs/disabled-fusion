package com.neomind.fusion.custom.orsegups.utils;

public class EventosAitAlertaVO
{
	private double total;
	private String cdViatura;
	private String nmViatura;
	private String idCentral;
	private String particao;
	private String cdHistorico;
	private boolean existeLog;
	public double getTotal()
	{
		return total;
	}
	public void setTotal(double total)
	{
		this.total = total;
	}
	public String getCdViatura()
	{
		return cdViatura;
	}
	public void setCdViatura(String cdViatura)
	{
		this.cdViatura = cdViatura;
	}
	public String getNmViatura()
	{
		return nmViatura;
	}
	public void setNmViatura(String nmViatura)
	{
		this.nmViatura = nmViatura;
	}
	public String getIdCentral()
	{
		return idCentral;
	}
	public void setIdCentral(String idCentral)
	{
		this.idCentral = idCentral;
	}
	public String getParticao()
	{
		return particao;
	}
	public void setParticao(String particao)
	{
		this.particao = particao;
	}
	public String getCdHistorico()
	{
		return cdHistorico;
	}
	public void setCdHistorico(String cdHistorico)
	{
		this.cdHistorico = cdHistorico;
	}
	public boolean isExisteLog()
	{
		return existeLog;
	}
	public void setExisteLog(boolean existeLog)
	{
		this.existeLog = existeLog;
	}
	
}
