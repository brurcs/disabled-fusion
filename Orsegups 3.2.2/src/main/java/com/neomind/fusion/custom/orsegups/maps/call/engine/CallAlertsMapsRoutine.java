package com.neomind.fusion.custom.orsegups.maps.call.engine;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.maps.tasks.MapsAutomatizarRotas;

public class CallAlertsMapsRoutine
{
	private static final String URL_FUSION = "http://intranet.orsegups.com.br/";
	private static final Log log = LogFactory.getLog(CallAlertsMapsRoutine.class);
	private static CallAlertsMapsRoutine instancia;
	
	
	public static synchronized CallAlertsMapsRoutine getInstancia()
	{
		if(instancia == null)
		{
			instancia = new CallAlertsMapsRoutine();
		}
		return instancia;
	}
	
	private CallAlertsMapsRoutine()
	{
		executaRotinaAtendimentoDeEventosCorporativos();
		executaRotinaAtendimentoDeEventos();
		
		/**
		 * Nova Thread
		 */
		
		executaAutomatizarRotas();
		
		/**
		 * 
		 */
		
		executaRotinaDeslocamentoEventoViatura();
		executaRotinaDeslocarEventoViaturaLivre();
		executaRotinaAtualizacaoStatusViatura();
		
		
	}
	
	public static void executaRotinaAtendimentoDeEventos()
	{

		try
		{

			Thread rotinaEventosMonitoramento = new Thread()
			{
				public void run()
				{
					while (true)
					{
						executaURL("fusion/services/eventos/verificaEventos/1");
						try
						{
							Thread.sleep(3000);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
			};
			rotinaEventosMonitoramento.start();

			log.debug(" Executando rotina de monitoramento de eventos! ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar rotina de monitoramento de eventos!");
		}
		finally
		{

		}

	}
	
	
	public static void executaRotinaAtendimentoDeEventosCorporativos()
	{

		try
		{

			Thread rotinaEventosMonitoramentoCorporativo = new Thread()
			{
				public void run()
				{
					while (true)
					{
						executaURL("fusion/services/eventos/verificaEventos/2");
						try
						{
							Thread.sleep(2000);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
			};
			rotinaEventosMonitoramentoCorporativo.start();

			log.debug(" Executando rotina de monitoramento de eventos! ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar rotina de monitoramento de eventos!");
		}
		finally
		{

		}

	}
	
	public static void executaRotinaAtualizacaoStatusViatura()
	{

		try
		{

			Thread rotinaAtualizacaoStatusViatura = new Thread()
			{
				public void run()
				{
					while (true)
					{
						executaURL("fusion/custom/jsp/orsegups/maps/atualizacaoStatusViaturas.jsp");
						try
						{
							Thread.sleep(8000);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
			};
			rotinaAtualizacaoStatusViatura.start();

			log.debug(" Executando rotina de atualização status viatura! ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar rotina de monitoramento de eventos!");
		}
		finally
		{

		}

	}
	
	public static void executaRotinaDeslocamentoEventoViatura()
	{

		try
		{
			Thread rotinaDeslocarEvento = new Thread()
			{
				public void run()
				{
					while (true)
					{
						executaURL("fusion/custom/jsp/orsegups/maps/rotinaDeslocarEvento.jsp");
						try
						{
							Thread.sleep(15000);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
			};
			rotinaDeslocarEvento.start();

			log.debug(" Executando rotina de deslocamento de eventos! ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar rotina de deslocamento de eventos!!");
		}
		finally
		{

		}
	}

	public static void executaRotinaDeslocarEventoViaturaLivre()
	{
		try
		{

			Thread rotinaDeslocarEventoViaturaLivre = new Thread()
			{
				public void run()
				{
					while (true)
					{
						executaURL("fusion/custom/jsp/orsegups/maps/rotinaDeslocarEventoViaturaLivre.jsp");
						try
						{
							Thread.sleep(20000);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
			};
			rotinaDeslocarEventoViaturaLivre.start();

			log.debug(" Executando rotina de deslocamento de eventos para viatura livre! ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar rotina de deslocamento de eventos para viatura livre!");
		}
		finally
		{

		}

	}
	
	public static void executaAutomatizarRotas()
	{
		try
		{

			Thread rotinaAutomatizarRotas = new Thread()
			{
				public void run()
				{
					while (true)
					{
					    MapsAutomatizarRotas.getInstance().execute();
						try
						{
							Thread.sleep(20*1000);
						}
						catch (InterruptedException e)
						{
						    System.out.println("MapsAutomatizarRotas ERRO AO EXECUTAR ROTINA VIA CallAlertsMapsRoutine :");
						    e.printStackTrace();
						}
						
					}
				}
			};
			rotinaAutomatizarRotas.start();

			log.debug(" Executando rotina Automatizar Rotas! ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar rotina Automatizar Rotas!");
		}

	}

	public static void executaURL(String URL)
	{
		int code = 0;
		InputStream buffer = null;
		InputStream in = null;
		Reader r = null;
		try
		{
			log.debug(" Executando URL: " + URL_FUSION + URL);
			StringBuffer urlTarefa = new StringBuffer();
			urlTarefa.append(URL_FUSION + URL);
			URL url = new URL(urlTarefa.toString());
	        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
	        uc.setDoOutput(true);
	        uc.setRequestMethod("GET");
	        uc.setRequestProperty("Accept", "application/json");
			in = uc.getInputStream();
		    buffer = new BufferedInputStream(in);
		    // chain the InputStream to a Reader
		    r = new InputStreamReader(buffer);
		    int c;
		    while ((c = r.read()) > 0) {
		    	log.debug((char) c);
				
		    }
		    
			log.debug(" Executando URL: " + URL_FUSION + URL+" Code : "+code);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar regra! "+e+" - "+code);

		}
		finally
		{
			
				try
				{
					if(r != null)
						r.close();
					if(buffer != null)
						buffer.close();
					if(in != null)
						in.close();
					
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		}
	}
	
	
}
