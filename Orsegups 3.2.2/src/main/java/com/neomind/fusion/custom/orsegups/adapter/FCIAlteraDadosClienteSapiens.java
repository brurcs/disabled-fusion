package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCIAlteraDadosClienteSapiens implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(FCIAlteraDadosClienteSapiens.class);

	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		Long codigoCliente = (Long) processEntity.findValue("contratoSapiens.codcli");
		String descricao = (String) processEntity.findValue("razaoSocialCliente");

		try
		{
			this.alteraDadosClienteSapiens(codigoCliente, descricao);
		}
		catch (WorkflowException we)
		{
			throw we;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public void alteraDadosClienteSapiens(Long codigoCliente, String razaoCliente) throws Exception
	{
		String nomeFonteDados = "SAPIENS";
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

		String sql = "UPDATE E085CLI SET E085CLI.NOMCLI = ? WHERE E085CLI.CODCLI = ? ";
		Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql);

		PreparedStatement st = null;

		try
		{
			st = connection.prepareStatement(sql);
			st.setString(1, razaoCliente);
			st.setLong(2, codigoCliente);

			st.executeUpdate();
		}
		catch (SQLException e)
		{
			throw e;
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					log.error("Erro ao fechar o statement");
					e.printStackTrace();
					throw e;
				}
			}
		}
	}
}
