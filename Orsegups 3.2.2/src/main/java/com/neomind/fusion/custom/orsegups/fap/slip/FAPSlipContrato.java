package com.neomind.fusion.custom.orsegups.fap.slip;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jsoup.Jsoup;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipEnums;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipEnums.Criterio;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipEnums.Modalidade;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipEnums.TipoOrcamento;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipOrcamentoUtils;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsGroupLevels;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class FAPSlipContrato
{
	private EntityWrapper wrapper = null;
	private Task task = null;
	
	public FAPSlipContrato(EntityWrapper wrapper, Task task)
	{
		this.wrapper = wrapper;
		this.task = task;
	}
	
	public void cadastrarContrato(Long codTarefa)
	{
		Long codModalidadeContrato = wrapper.findGenericValue("novoContrato.modalidadeContrato.codigo");
		Modalidade modalidadeContrato = FAPSlipEnums.getEnum(codModalidadeContrato, FAPSlipEnums.Modalidade.values());
		
		NeoUser nuResponsavelContrato = (NeoUser) wrapper.findGenericValue("novoContrato.responsavelContrato");
	
		if (modalidadeContrato == null || modalidadeContrato == FAPSlipEnums.Modalidade.CONTRATO)
		{
			validarDadosContrato();
			
			NeoObject noContrato = wrapper.findGenericValue("novoContrato");
		
			Long codTipoOrcamento = wrapper.findGenericValue("novoContrato.tipoOrcamento.codigo");
			TipoOrcamento tipoOrcamento = FAPSlipEnums.getEnum(codTipoOrcamento, FAPSlipEnums.TipoOrcamento.values());
			
			Long codCriterioAprovacao = wrapper.findGenericValue("novoContrato.criterioAprovacao.codigo");
			Criterio criterioAprovacao = FAPSlipEnums.getEnum(codCriterioAprovacao, FAPSlipEnums.Criterio.values());
			
			if ((criterioAprovacao != FAPSlipEnums.Criterio.ORCAMENTOINDIVIDUAL
					&& criterioAprovacao != FAPSlipEnums.Criterio.ORCAMENTOCOMPARTILHADO 
					&& tipoOrcamento != null && tipoOrcamento == FAPSlipEnums.TipoOrcamento.COMPARTILHADO) 
				|| (criterioAprovacao == FAPSlipEnums.Criterio.ORCAMENTOCOMPARTILHADO))
			{		
				FAPSlipOrcamentoUtils.validarPermissaoOrcamento(noContrato);
			}
			
			if (criterioAprovacao == FAPSlipEnums.Criterio.ORCAMENTOCOMPARTILHADO
				|| (criterioAprovacao != FAPSlipEnums.Criterio.ORCAMENTOINDIVIDUAL && criterioAprovacao != FAPSlipEnums.Criterio.ORCAMENTOCOMPARTILHADO && tipoOrcamento == FAPSlipEnums.TipoOrcamento.COMPARTILHADO))
			{
				NeoObject noContratoOrcamento = wrapper.findGenericValue("novoContrato.orcamentoCadastrado");
				validarSaldoOrcamento(noContratoOrcamento);
			}
		} else {
			List<NeoObject> listOrcamento = wrapper.findGenericValue("novoContrato.orcamento");
			List<NeoObject> listNovoOrcamento = wrapper.findGenericValue("novoContrato.novoOrcamento");
			if (!validarVigenciaOrcamento(listOrcamento, listNovoOrcamento))
				throw new WorkflowException("Não é possível cadastrar orçamentos com vigências sobrepostas.");
			
			validarPreenchimentoOrcamento(listOrcamento);
		}
		
		wrapper.findField("novoContrato.codContrato").setValue(codTarefa);
		wrapper.findField("codContrato").setValue(codTarefa);
		
		
		wrapper.findField("novoContrato.listaPermissao").addValue(nuResponsavelContrato);
		
		List<SecurityEntity> listPermissao = wrapper.findGenericValue("novoContrato.listaPermissao");				
		WFProcess processo = wrapper.findGenericValue("wfprocess");							
		FAPSlipUtils.adicionarVisualizadoresProcesso(processo, listPermissao);
		
		NeoUser aprovadorContrato = FAPSlipUtils.findAprovadorPorNivelGrupo(nuResponsavelContrato, OrsegupsGroupLevels.GERENCIA);
		List<NeoObject> listaAprovadores = new ArrayList<>();
		listaAprovadores.add(aprovadorContrato);
		
		wrapper.findField("listaAprovadores").setValue(listaAprovadores);
		
		String descricaoHistorico = wrapper.findGenericValue("observacoes");
		if (descricaoHistorico.equals(""))
			descricaoHistorico = "Mensagem Automática: Novo Contrato Cadastrado";
		
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	public void iniciarEdicaoContrato()
	{
		Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
		List<NeoObject> listContratosEdicao = buscarContratosEdicao(codContrato);
		if (listContratosEdicao != null && !listContratosEdicao.isEmpty())
		{
			for (NeoObject noContratoEdicao : listContratosEdicao)
			{
				WFProcess processoEdicao = new EntityWrapper(noContratoEdicao).findGenericValue("wfprocess");
				if (processoEdicao == null)
					continue;
				
				throw new WorkflowException("Este contrato já está em edição na tarefa " + processoEdicao.getCode());
			}
		}		
		
		NeoObject noNovoContratoAtual = wrapper.findGenericValue("novoContrato");
		if (noNovoContratoAtual == null)
		{
			NeoObject noNovoContrato = EntityCloner.cloneNeoObject((NeoObject) wrapper.findGenericValue("dadosContrato"));
			EntityWrapper wNovoContrato = new EntityWrapper(noNovoContrato);
			wNovoContrato.setValue("ativo", false);
			
			wrapper.setValue("novoContrato", noNovoContrato);
		}
			
		
		String tituloContrato = wrapper.findGenericValue("dadosContrato.titulo");
		Long codModalidadeContrato = wrapper.findGenericValue("dadosContrato.modalidadeContrato.codigo");
		if (codModalidadeContrato != null && codModalidadeContrato == 2l)
			tituloContrato = "Orçamento - " + tituloContrato;
		
		wrapper.setValue("tituloTarefa", tituloContrato);
		
		List<SecurityEntity> listPermissao = wrapper.findGenericValue("dadosContrato.listaPermissao");				
		WFProcess processo = wrapper.findGenericValue("wfprocess");							
		FAPSlipUtils.adicionarVisualizadoresProcesso(processo, listPermissao);
		
		String descricaoHistorico = "Processo Iniciado. Operação: Editar Contrato";
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	public void editarContrato()
	{
		NeoUser nuResponsavelContrato = (NeoUser) wrapper.findGenericValue("novoContrato.responsavelContrato");
		NeoUser aprovadorContrato = FAPSlipUtils.findAprovadorPorNivelGrupo(nuResponsavelContrato, OrsegupsGroupLevels.GERENCIA);
		List<NeoObject> listaAprovadores = new ArrayList<>();
		listaAprovadores.add(aprovadorContrato);
		
		Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
		
		wrapper.findField("listaAprovadores").setValue(listaAprovadores);
		
		String camposAlterados = mapearAlteracoes();
		
		String descricaoHistorico = wrapper.findGenericValue("observacoes");
		if (descricaoHistorico.equals(""))
			descricaoHistorico = camposAlterados;
		else 
			descricaoHistorico = descricaoHistorico + "<br>" + camposAlterados;	
		
		Long codCriterioAprovacao = wrapper.findGenericValue("novoContrato.criterioAprovacao.codigo");
		Long codTipoOrcamento = wrapper.findGenericValue("novoContrato.tipoOrcamento.codigo");
		if ((codCriterioAprovacao != null && codCriterioAprovacao != 5 && codCriterioAprovacao != 6 && codTipoOrcamento != null && codTipoOrcamento == 2) || (codCriterioAprovacao != null && codCriterioAprovacao == 6))
		{
			NeoObject noContrato = wrapper.findGenericValue("novoContrato");
			FAPSlipOrcamentoUtils.validarPermissaoOrcamento(noContrato);
		}
		
		Long codModalidadeContratoCadastrado = wrapper.findGenericValue("novoContrato.modalidadeContrato.codigo");		
		if (codModalidadeContratoCadastrado == null || codModalidadeContratoCadastrado != 2l)
		{
			validarDadosContrato();
			
			if (codCriterioAprovacao == 6l || (codCriterioAprovacao != 5 && codCriterioAprovacao != 6 && codTipoOrcamento == 2l))
			{
				NeoObject noContratoOrcamento = wrapper.findGenericValue("novoContrato.orcamentoCadastrado");
				validarSaldoOrcamento(noContratoOrcamento, codContrato);
			}
		} else {
			List<NeoObject> listOrcamento = wrapper.findGenericValue("novoContrato.orcamento");
			List<NeoObject> listNovoOrcamento = wrapper.findGenericValue("novoContrato.novoOrcamento");
			if (!validarVigenciaOrcamento(listOrcamento, listNovoOrcamento))
				throw new WorkflowException("Não é possível cadastrar orçamentos com vigências sobrepostas.");
			
			validarPreenchimentoOrcamento(listOrcamento);
		}
		
		if ((codModalidadeContratoCadastrado != null && codModalidadeContratoCadastrado == 2l) || ((codCriterioAprovacao != null && codCriterioAprovacao == 5l) || (codTipoOrcamento != null && codTipoOrcamento == 1l)))
		{
			List<NeoObject> listOrcamento = wrapper.findGenericValue("novoContrato.orcamento");
			List<NeoObject> listNovoOrcamento = wrapper.findGenericValue("novoContrato.novoOrcamento");
			if ((listOrcamento != null && listOrcamento.isEmpty()) && (listNovoOrcamento != null && listNovoOrcamento.isEmpty()))
			{
				throw new WorkflowException("É necessário cadastrar um orçamento.");
			}
		}
		
		String tituloContrato = wrapper.findGenericValue("dadosContrato.titulo");
		Long codModalidadeContrato = wrapper.findGenericValue("dadosContrato.modalidadeContrato.codigo");
		if (codModalidadeContrato != null && codModalidadeContrato == 2l)
			tituloContrato = "Orçamento - " + tituloContrato;
		
		wrapper.findField("novoContrato.listaPermissao").addValue(nuResponsavelContrato);
		
		List<SecurityEntity> listPermissao = wrapper.findGenericValue("novoContrato.listaPermissao");				
		WFProcess processo = wrapper.findGenericValue("wfprocess");							
		FAPSlipUtils.adicionarVisualizadoresProcesso(processo, listPermissao);
		
		wrapper.setValue("tituloTarefa", tituloContrato);
		
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	public void aprovarContrato(String hierarquia)
	{
		String descricaoHistorico = wrapper.findGenericValue("observacoes");
		
		Boolean aprovado = false;
		if (hierarquia.equals("Gerência") || hierarquia.equals("Diretoria"))
			aprovado = wrapper.findGenericValue("aprovarPagamento");
		else if (hierarquia.equals("Presidência"))
		{
			aprovado = wrapper.findGenericValue("aprovacaoPresidente");
			
			if (aprovado)
				finalizarCadastroContrato();
		}
		
		if (aprovado)
		{
			if (descricaoHistorico.equals(""))
				descricaoHistorico = "Mensagem Automática: Contrato Aprovado pela " + hierarquia;
			
			registrarExecutorAprovacao(PortalUtil.getCurrentUser(), task.getActivityName());
		}
		else
		{
			if (descricaoHistorico.equals(""))
				descricaoHistorico = "Mensagem Automática: Contrato Reprovado pela Gerência";
		}
		
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	public void validarContrato()
	{
		String descricaoHistorico = wrapper.findGenericValue("observacoes");
		NeoUser nuResponsavelContrato = (NeoUser) wrapper.findGenericValue("novoContrato.responsavelContrato");
		
		Boolean aprovacaoControladoria = wrapper.findGenericValue("aprovacaoControladoria");
		if (aprovacaoControladoria)
		{
			if (descricaoHistorico.equals(""))
				descricaoHistorico = "Mensagem Automática: Novo Contrato Validado pela Controladoria";
			
			registrarExecutorAprovacao(PortalUtil.getCurrentUser(), task.getActivityName());
			
			NeoUser aprovadorContrato = FAPSlipUtils.findAprovadorPorNivelGrupo(nuResponsavelContrato, OrsegupsGroupLevels.DIRETORIA);
			List<NeoObject> listaAprovadores = new ArrayList<>();
			listaAprovadores.add(aprovadorContrato);
			
			wrapper.findField("listaAprovadores").setValue(listaAprovadores);
		}
		else
		{
			if (descricaoHistorico.equals(""))
				descricaoHistorico = "Mensagem Automática: Contrato Reprovado pela Controladoria";
		}
		
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	private void validarDadosContrato()
	{
		NeoObject noContrato = wrapper.findGenericValue("novoContrato");
		EntityWrapper wForm = new EntityWrapper(noContrato);
		
		Long codCriterioAprovacao = wForm.findGenericValue("criterioAprovacao.codigo");
		if (codCriterioAprovacao == null)
		{
			throw new WorkflowException("É necessário escolher um critério de aprovação.");
		}
		
		if (codCriterioAprovacao == 2l)
		{
			Long periodoMedia = wForm.findGenericValue("periodoMedia");
			if (periodoMedia > 12)
				throw new WorkflowException("O valor máximo para cálculo da média é de 12 meses");
		}
		
		//importarRateiosExcel(wForm);
		
		Long codTipoOrcamento = wForm.findGenericValue("tipoOrcamento.codigo");
		List<NeoObject> listOrcamento = wForm.findGenericValue("orcamento");
		List<NeoObject> listNovoOrcamento = wForm.findGenericValue("novoOrcamento");
		
		if ((codTipoOrcamento != null && codTipoOrcamento == 1l && codCriterioAprovacao != 6) && ((listOrcamento == null || listOrcamento.isEmpty()) && (listNovoOrcamento == null || listNovoOrcamento.isEmpty())))
		{
			throw new WorkflowException("É necessário inserir o orçamento.");
		}
			
		if (!validarVigenciaOrcamento(listOrcamento, listNovoOrcamento))
			throw new WorkflowException("A data de vigência do novo orçamento deve ser maior que as vigências anteriores.");
		
		List<NeoObject> listAnexos = wForm.findGenericValue("anexos");
		if (listAnexos.isEmpty())
			throw new WorkflowException("É necessário inserir um anexo.");
		
		BigDecimal totalRateio = wForm.findGenericValue("centroCusto.total");
		
		Long codTipoRateio = wForm.findGenericValue("centroCusto.tipoRateio.codigo");
		if (codTipoRateio == null)
		{
			throw new WorkflowException("Selecione o tipo do rateio.");
		}
		
		if (codTipoRateio == 1l)
		{
			if (totalRateio.compareTo(new BigDecimal(100)) != 0)
				throw new WorkflowException("A porcentagem total de rateios deve chegar à 100.");
		} else if (codTipoRateio == 2l)
		{
			//if (codCriterioAprovacao != 3l)
				//throw new WorkflowException("O tipo de rateio selecionado só é permitido se o valor for fixo.");
			
			BigDecimal valorContrato = wForm.findGenericValue("valorBase");
			if (totalRateio != null && valorContrato != null && totalRateio.compareTo(valorContrato) != 0 && codCriterioAprovacao == 3l)
				throw new WorkflowException("O valor total do rateio deve ser o mesmo que o valor do contrato");
		}
		
		List<String> listCentroCustoInvalidos = new ArrayList<String>();
		
		Long codEmpresa = wForm.findGenericValue("empresa.codemp");		
		List<NeoObject> listCCU = wForm.findGenericValue("centroCusto.rateio");		
		for (NeoObject noCCU : listCCU)
		{
			EntityWrapper wCCU = new EntityWrapper(noCCU);
			
			NeoObject noExtCCU = wCCU.findGenericValue("centroCusto");
			String codCCU = wCCU.findGenericValue("codigoCentroCusto");
			if (codCCU == null || codCCU.length() == 0)
			{
				if (noExtCCU != null)
				{
					String aceRat = new EntityWrapper(noExtCCU).findGenericValue("acerat");
					if (aceRat == null || !aceRat.trim().equals("S"))
						listCentroCustoInvalidos.add(codCCU);
					
					continue;
				}
			}
			
			NeoObject externoCentroCusto = FAPSlipUtils.buscarCCU(codEmpresa, codCCU);			
			if (externoCentroCusto == null)
				listCentroCustoInvalidos.add(codCCU);
			
			wCCU.setValue("centroCusto", externoCentroCusto);
		}
		
		if (!listCentroCustoInvalidos.isEmpty())
		{
			String erroCCU = "Centro de Custo(s) não localizado(s) ou não aceitam rateio! Por favor, verifique se o(s) Centro de Custo " + listCentroCustoInvalidos + " existem na Empresa de lançamento ou não aceitam rateio!";
			erroCCU = erroCCU.replaceAll("\\[|\\]|", "");
			throw new WorkflowException(erroCCU);
		}
		
		String fornecedorFantasia = wForm.findGenericValue("fornecedor.apefor");
		if (fornecedorFantasia == null || fornecedorFantasia.trim().length() == 0 || fornecedorFantasia.contains("#I"))
			fornecedorFantasia = wForm.findGenericValue("fornecedor.nomfor");
		
		Long fornecedorCNPJ = wForm.findGenericValue("fornecedor.cgccpf");
		Long codFornecedor = wForm.findGenericValue("fornecedor.codfor");
		
		wForm.setValue("fornecedorStr", fornecedorFantasia);
		wForm.setValue("fornecedorCNPJ", fornecedorCNPJ != null ? fornecedorCNPJ.toString() : "Não informado");
		wForm.setValue("codFornecedor", codFornecedor != null ? codFornecedor.toString() : "");			
	}
	
	private boolean validarSaldoOrcamento(NeoObject noContratoOrcamento)
	{
		return validarSaldoOrcamento(noContratoOrcamento, null);
	}
	
	private boolean validarSaldoOrcamento(NeoObject noContratoOrcamento, Long codContratoIgnorar)
	{
		BigDecimal saldo = FAPSlipOrcamentoUtils.consultarSaldoDisponivel(noContratoOrcamento, codContratoIgnorar);
		
		NeoObject noContrato = wrapper.findGenericValue("novoContrato");		
		BigDecimal consumoPrevistoContrato = FAPSlipOrcamentoUtils.consultarConsumoPrevistoContrato(noContrato, codContratoIgnorar);
		
		if (consumoPrevistoContrato != null)
		{
			saldo = saldo.subtract(consumoPrevistoContrato);
			if (saldo.compareTo(new BigDecimal(0)) < 0)
			{
				throw new WorkflowException("O valor disponível do Orçamento Compartilhado não é suficiente para comportar este contrato");
			}
		}
		
		return true;
	}
	
	private boolean validarVigenciaOrcamento(List<NeoObject> listOrcamento, List<NeoObject> listNovoOrcamento)
	{
		for (NeoObject noNovoOrcamento : listNovoOrcamento)
		{
			GregorianCalendar iniVigNovoOrcamento = new EntityWrapper(noNovoOrcamento).findGenericValue("inicioVigencia");
			GregorianCalendar fimVigNovoOrcamento = new EntityWrapper(noNovoOrcamento).findGenericValue("fimVigencia");
			for (NeoObject noNovoOrcamento2 : listNovoOrcamento)
			{
				GregorianCalendar iniVigNovoOrcamento2 = new EntityWrapper(noNovoOrcamento2).findGenericValue("inicioVigencia");
				GregorianCalendar fimVigNovoOrcamento2 = new EntityWrapper(noNovoOrcamento2).findGenericValue("fimVigencia");
				
				if (noNovoOrcamento.getNeoId() != noNovoOrcamento2.getNeoId() && 
						(fimVigNovoOrcamento.after(iniVigNovoOrcamento2) && iniVigNovoOrcamento.before(iniVigNovoOrcamento2) ||
						 iniVigNovoOrcamento.before(fimVigNovoOrcamento2) && fimVigNovoOrcamento.after(fimVigNovoOrcamento2) ||
						 iniVigNovoOrcamento.after(iniVigNovoOrcamento2) && fimVigNovoOrcamento.before(fimVigNovoOrcamento2) ||
						 iniVigNovoOrcamento.before(iniVigNovoOrcamento2) && fimVigNovoOrcamento.after(fimVigNovoOrcamento2)))
				{
					return false;
				}
			}
		}
		
		for (NeoObject noNovoOrcamento : listNovoOrcamento)
		{
			GregorianCalendar iniVigNovoOrcamento = new EntityWrapper(noNovoOrcamento).findGenericValue("inicioVigencia");
			GregorianCalendar fimVigNovoOrcamento = new EntityWrapper(noNovoOrcamento).findGenericValue("fimVigencia");
			for (NeoObject noOrcamento : listOrcamento)
			{
				GregorianCalendar iniVigOrcamento = new EntityWrapper(noOrcamento).findGenericValue("inicioVigencia");
				GregorianCalendar fimVigOrcamento = new EntityWrapper(noOrcamento).findGenericValue("fimVigencia");
				
				
				if (fimVigNovoOrcamento.after(iniVigOrcamento) && iniVigNovoOrcamento.before(iniVigOrcamento) ||
					iniVigNovoOrcamento.before(fimVigOrcamento) && fimVigNovoOrcamento.after(fimVigOrcamento) ||
					iniVigNovoOrcamento.after(iniVigOrcamento) && fimVigNovoOrcamento.before(fimVigOrcamento) ||
					iniVigNovoOrcamento.before(iniVigOrcamento) && fimVigNovoOrcamento.after(fimVigOrcamento))
					return false;
			}
		}
		
		return true;
	}
	
	private void validarPreenchimentoOrcamento(List<NeoObject> listOrcamento)
	{
		for (NeoObject noOrcamento : listOrcamento)
		{
			EntityWrapper wOrcamento = new EntityWrapper(noOrcamento);
			
			BigDecimal valor = wOrcamento.findGenericValue("valor");
			GregorianCalendar inicioVigencia = wOrcamento.findGenericValue("inicioVigencia");
			
			if (valor == null || inicioVigencia == null)
				throw new WorkflowException("Os dados do orçamento não foram preenchidos corretamente. Revise-os e tente novamente.");
		}
	}
	
	private void finalizarCadastroContrato()
	{
		Long codOperacao = wrapper.findGenericValue("operacao.codigo");
		Long codTipoOrcamento = wrapper.findGenericValue("novoContrato.tipoOrcamento.codigo");
		Long codCriterioAprovacao = wrapper.findGenericValue("novoContrato.criterioAprovacao.codigo");
		Long codModalidadeContrato = wrapper.findGenericValue("novoContrato.criterioAprovacao.codigo");
		if (codModalidadeContrato != null && codModalidadeContrato != 2 && codCriterioAprovacao != null && (codCriterioAprovacao == 6l || (codCriterioAprovacao != 5 && codCriterioAprovacao != 6 && (codTipoOrcamento != null && codTipoOrcamento == 2l))))
		{
			NeoObject noContratoOrcamento = wrapper.findGenericValue("novoContrato.orcamentoCadastrado");
			
			if (codOperacao == 2l)
			{
				validarSaldoOrcamento(noContratoOrcamento);
			}
			else if (codOperacao == 3l)
			{
				Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
				validarSaldoOrcamento(noContratoOrcamento, codContrato);
			}
		}
		
		List<NeoObject> listNovoOrcamento = wrapper.findGenericValue("novoContrato.novoOrcamento");
		if (listNovoOrcamento != null && !listNovoOrcamento.isEmpty())
		{
			for (NeoObject noNovoOrcamento : listNovoOrcamento)
			{						
				wrapper.findField("novoContrato.orcamento").addValue(noNovoOrcamento);
			}
		}
		
		Long codModalidadeContratoCadastrado = wrapper.findGenericValue("novoContrato.modalidadeContrato.codigo");
		if ((codModalidadeContratoCadastrado != null && codModalidadeContratoCadastrado == 2l) || (codCriterioAprovacao == 5 || codTipoOrcamento == 1))
		{
			NeoObject noNovoContrato = wrapper.findGenericValue("novoContrato");
			Long codNovoContrato = new EntityWrapper(noNovoContrato).findGenericValue("codContrato");
			
			QLGroupFilter qlGroup = new QLGroupFilter("OR", new QLEqualsFilter("orcamento.codContrato", codNovoContrato));
			List<NeoObject> listHistoricoOrcamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlipHistoricoOrcamento"), qlGroup);
			if (listHistoricoOrcamento == null || listHistoricoOrcamento.isEmpty())
			{
				NeoObject noHistoricoOrcamento = AdapterUtils.createNewEntityInstance("FAPSlipHistoricoOrcamento");
				EntityWrapper wHistoricoOrcamento = new EntityWrapper(noHistoricoOrcamento);
			
				wHistoricoOrcamento.setValue("orcamento", noNovoContrato);
				PersistEngine.persist(noHistoricoOrcamento);
			}
		}
		
		if (codOperacao == 2l)
		{
			if (codModalidadeContratoCadastrado == null || codModalidadeContratoCadastrado == 1l)
			{
				wrapper.findField("dadosContrato.ativo").setValue(false);
				wrapper.findField("novoContrato.ativo").setValue(true);
				
				NeoObject noHistoricoContrato = AdapterUtils.createNewEntityInstance("FAPSlipHistoricoContrato");
				EntityWrapper wHistoricoContrato = new EntityWrapper(noHistoricoContrato);
				
				NeoObject noAntigoContrato = wrapper.findGenericValue("dadosContrato");
				
				wHistoricoContrato.setValue("data", new GregorianCalendar());
				wHistoricoContrato.setValue("responsavel", PortalUtil.getCurrentUser());
				wHistoricoContrato.setValue("contrato", noAntigoContrato);
				PersistEngine.persist(noHistoricoContrato);
				
				wrapper.findField("novoContrato.historicoEdicao").addValue(noHistoricoContrato);
			}
			else if (codModalidadeContratoCadastrado == 2l)
			{
				NeoObject noContratoAntigo = EntityCloner.cloneNeoObject((NeoObject) wrapper.findGenericValue("dadosContrato"));
				EntityWrapper wContratoAntigo = new EntityWrapper(noContratoAntigo);
				wContratoAntigo.setValue("ativo", false);
				PersistEngine.persist(noContratoAntigo);
				
				NeoObject noHistoricoContrato = AdapterUtils.createNewEntityInstance("FAPSlipHistoricoContrato");
				EntityWrapper wHistoricoContrato = new EntityWrapper(noHistoricoContrato);
				
				wHistoricoContrato.setValue("data", new GregorianCalendar());
				wHistoricoContrato.setValue("responsavel", PortalUtil.getCurrentUser());
				wHistoricoContrato.setValue("contrato", noContratoAntigo);
				PersistEngine.persist(noHistoricoContrato);
				
				wrapper.findField("dadosContrato.historicoEdicao").addValue(noHistoricoContrato);
				
				finalizarContratoOrcamento();
			}
		}
		else if (codOperacao == 3l) {							
			wrapper.findField("novoContrato.ativo").setValue(true);
		}
		
		wrapper.findField("novoContrato.novoOrcamento").setValue(null);
	}
	
	private void finalizarContratoOrcamento()
	{
		NeoObject noEmpresa = wrapper.findGenericValue("novoContrato.empresa");
		
		NeoUser noResponsavelContrato = wrapper.findGenericValue("novoContrato.responsavelContrato");
		NeoGroup noDepartamentoContrato = wrapper.findGenericValue("novoContrato.dptoResponsavel");
		
		String descricao = wrapper.findGenericValue("novoContrato.descricao");
		String ondeSeraAplicado = wrapper.findGenericValue("novoContrato.ondeSeraAplicado");
		String beneficiosEsperadosAlcancados = wrapper.findGenericValue("novoContrato.beneficiosEsperadosAlcancados");
		Boolean aprovarExtraPrazo = wrapper.findGenericValue("novoContrato.aprovarExtraPrazo");
		Boolean definirAprovadores = wrapper.findGenericValue("novoContrato.definirAprovadores");
		
		List<NeoObject> listFornecedoresPermitidos = wrapper.findGenericValue("novoContrato.fornecedoresPermitidos");
		List<NeoObject> listEmpresasPermitidas = wrapper.findGenericValue("novoContrato.empresasPermitidas");
		
		List<NeoObject> listAnexos = wrapper.findGenericValue("novoContrato.anexos");
		List<NeoObject> listPermissao = wrapper.findGenericValue("novoContrato.listaPermissao");
		List<NeoObject> listAprovadores = wrapper.findGenericValue("novoContrato.aprovadores");
		
		List<NeoObject> listNovoOrcamento = wrapper.findGenericValue("novoContrato.novoOrcamento");
		if (listNovoOrcamento != null && !listNovoOrcamento.isEmpty())
		{
			for (NeoObject noNovoOrcamento : listNovoOrcamento)
			{						
				wrapper.findField("dadosContrato.orcamento").addValue(noNovoOrcamento);
			}
		}
		
		if (listFornecedoresPermitidos != null && !listFornecedoresPermitidos.isEmpty())
		{
			for (NeoObject noFornecedorPermitido : listFornecedoresPermitidos)
			{
				wrapper.findField("dadosContrato.fornecedoresPermitidos").addValue(noFornecedorPermitido);
			}
		}
		
		if (listEmpresasPermitidas != null && !listEmpresasPermitidas.isEmpty())
		{
			for (NeoObject noEmpresaPermitida : listEmpresasPermitidas)
			{
				wrapper.findField("dadosContrato.empresasPermitidas").addValue(noEmpresaPermitida);
			}
		}
		
		if (listPermissao != null && !listPermissao.isEmpty())
		{
			for (NeoObject noPermissao : listPermissao)
			{
				wrapper.findField("dadosContrato.listaPermissao").addValue(noPermissao);
			}
		}
		
		if (listAprovadores != null && !listAprovadores.isEmpty())
		{
			List<NeoObject> novosAprovadores = new ArrayList<>();
			for (NeoObject noAprovador : listAprovadores)
			{
				novosAprovadores.add(EntityCloner.cloneNeoObject(noAprovador));
			}
			wrapper.findField("dadosContrato.aprovadores").setValue(novosAprovadores);
		}
		
		if (listAnexos != null && !listAnexos.isEmpty())
		{
			List<NeoObject> novosAnexos = new ArrayList<>();
			for (NeoObject noAnexo : listAnexos)
			{
				novosAnexos.add(EntityCloner.cloneNeoObject(noAnexo));
			}
			wrapper.findField("dadosContrato.anexos").setValue(novosAnexos);
		}
		
		wrapper.findField("dadosContrato.empresa").setValue(noEmpresa);
		wrapper.findField("dadosContrato.responsavelContrato").setValue(noResponsavelContrato);
		wrapper.findField("dadosContrato.dptoResponsavel").setValue(noDepartamentoContrato);
		wrapper.findField("dadosContrato.descricao").setValue(descricao);
		wrapper.findField("dadosContrato.ondeSeraAplicado").setValue(ondeSeraAplicado);
		wrapper.findField("dadosContrato.beneficiosEsperadosAlcancados").setValue(beneficiosEsperadosAlcancados);
		wrapper.findField("dadosContrato.aprovarExtraPrazo").setValue(aprovarExtraPrazo);
		wrapper.findField("dadosContrato.definirAprovadores").setValue(definirAprovadores);
	}
	
	private String mapearAlteracoes() 
	{
		NeoObject noContrato = wrapper.findGenericValue("dadosContrato");
		NeoObject noNovoContrato = wrapper.findGenericValue("novoContrato");
		
		EntityWrapper wContrato = new EntityWrapper(noContrato);
		EntityWrapper wNovoContrato = new EntityWrapper(noNovoContrato);
		
		Map<String, String> map = new HashMap<>();
		map.put("responsavelContrato", "    - Responsável pelo Contrato");
		map.put("dptoResponsavel", "    - Departamento Responsável");
		map.put("empresa", "    - Empresa");
		map.put("descricao", "    - Descrição (O que é)");
		map.put("inicioVigencia", "    - Início Vigência");
		map.put("fimVigencia", "    - Fim Vigência");
		map.put("competencia", "    - Aceita Pagamento a Partir da Competência");
		map.put("diaProvavelPagto", "    - Dia Provável Vencimento");
		map.put("formaPagamento", "    - Forma Pagamento");
		map.put("contaFinanceira", "    - Conta Financeira");
		map.put("contaContabil", "    - Conta Contábil");
		map.put("tipoDoContrato", "    - Tipo do Contrato");
		map.put("criterioAprovacao", "    - Critério para Aprovação");
		map.put("validarOrcamento", "    - Validar Orçamento do Contrato");
		map.put("tipoTolerancia", "    - Tipo Tolerância");
		map.put("toleranciaAcima", "    - Tolerância Acima");
		map.put("toleranciaAbaixo", "    - Tolerância Abaixo");
		map.put("banco", "    - Banco");
		map.put("agencia", "    - Agência");
		map.put("conta", "    - Conta");
		map.put("ondeSeraAplicado", "    - Onde será aplicado (para que serve)");
		map.put("beneficiosEsperadosAlcancados", "    - Benefícios esperados/alcançados (evidências)");
		map.put("diasAvisoPrevio", "    - Dias Aviso-Prévio	");
		map.put("historicoSlip", "    - Histórico Slip");
		map.put("definirAprovadores", "    - Definir Aprovadores");
		map.put("aprovarExtraPrazo", "    - Aprovar Extra-Prazo");
		
		// setando valores corretos no horario da data de competencia do contrato
		GregorianCalendar competenciaContratoAtual = wContrato.findGenericValue("competencia");
		competenciaContratoAtual.set(Calendar.HOUR, 0);
		competenciaContratoAtual.set(Calendar.MINUTE, 0);
		
		GregorianCalendar competenciaContratoNovo = wNovoContrato.findGenericValue("competencia");
		competenciaContratoNovo.set(Calendar.HOUR, 0);
		competenciaContratoNovo.set(Calendar.MINUTE, 0);
		
		List<String> camposAlterados = new ArrayList<>();
		for (Entry<String, String> entry : map.entrySet())
		{
			Object atual = wContrato.findGenericValue(entry.getKey());
			Object novo = wNovoContrato.findGenericValue(entry.getKey());
			
			if (!(atual == null && novo == null) && ((atual == null && novo != null) || (atual != null && novo == null) || !atual.equals(novo)))
			{
				if (atual != null && novo != null)
				{
					if ((entry.getKey().equals("inicioVigencia") || entry.getKey().equals("fimVigencia") || entry.getKey().equals("competencia")))
					{
						camposAlterados.add(entry.getValue() + " de " + NeoDateUtils.safeDateFormat((GregorianCalendar) atual) + " para " + NeoDateUtils.safeDateFormat((GregorianCalendar) novo));
					}
					else if (entry.getKey().equals("criterioAprovacao"))
					{
						String criterioAtual = wContrato.findGenericValue("criterioAprovacao.descricao");
						String criterioNovo = wNovoContrato.findGenericValue("criterioAprovacao.descricao");
						camposAlterados.add(entry.getValue() + " de " + criterioAtual + " para " + criterioNovo);
					}
					else if (entry.getKey().equals("toleranciaAcima") || entry.getKey().equals("toleranciaAbaixo"))
					{
						BigDecimal toleranciaAtual = wContrato.findGenericValue(entry.getKey());
						BigDecimal toleranciaNova = wNovoContrato.findGenericValue(entry.getKey());
						if (toleranciaNova != null && toleranciaNova.compareTo(toleranciaAtual) != 0)
							camposAlterados.add(entry.getValue());
					}
					else if (entry.getKey().equals("ondeSeraAplicado") || entry.getKey().equals("beneficiosEsperadosAlcancados"))
					{
						if (!Jsoup.parse((String) atual).text().equals(Jsoup.parse((String) novo).text()))
							camposAlterados.add(entry.getValue());
					}
					else
					{
						camposAlterados.add(entry.getValue());	
					}
				}
				else {
					camposAlterados.add(entry.getValue());	
				}
			}	
		}
		
		BigDecimal valorBaseAtual = wContrato.findGenericValue("valorBase");
		BigDecimal valorBaseNovo = wNovoContrato.findGenericValue("valorBase");
		if (valorBaseAtual != null && valorBaseNovo != null && valorBaseAtual.compareTo(valorBaseNovo) != 0)
			camposAlterados.add("    - Valor Base de " + FAPSlipUtils.parseMonetario(valorBaseAtual) + " para " + FAPSlipUtils.parseMonetario(valorBaseNovo));
		
		StringBuilder retorno = new StringBuilder();
		retorno.append("Dados alterados:<ul>");
		
		for (String campo : camposAlterados)
			retorno.append("<li>" + campo + "</li>");
		
		List<NeoObject> listOrcamento = wNovoContrato.findGenericValue("novoOrcamento");
		if (!listOrcamento.isEmpty())
			retorno.append("<li>    - Orçamento</li>");
		
		List<NeoObject> listAprovadoresAtual = wContrato.findGenericValue("aprovadores");
		List<NeoObject> listAprovadoresNovo = wNovoContrato.findGenericValue("aprovadores");
		if (listAprovadoresAtual.size() != listAprovadoresNovo.size())
			retorno.append("<li>    - Aprovadores</li>");
		
		List<NeoObject> listAprovadoresOrcamentoAtual = wContrato.findGenericValue("aprovadoresOrcamento");
		List<NeoObject> listAprovadoresOrcamentoNovo = wNovoContrato.findGenericValue("aprovadoresOrcamento");
		if (listAprovadoresOrcamentoAtual.size() != listAprovadoresOrcamentoNovo.size())
			retorno.append("<li>    - Aprovadores Orçamento</li>");
		
		List<NeoObject> listPermissaoAtual = wContrato.findGenericValue("listaPermissao");
		List<NeoObject> listPermissaoNovo = wNovoContrato.findGenericValue("listaPermissao");
		if (listPermissaoAtual.size() != listPermissaoNovo.size())
			retorno.append("<li>    - Lista Permissão</li>");
		
		NeoObject noTipoRateioAtual = wContrato.findGenericValue("centroCusto.tipoRateio");
		NeoObject noTipoRateioNovo = wNovoContrato.findGenericValue("centroCusto.tipoRateio");
		if (noTipoRateioAtual != null && !noTipoRateioAtual.equals(noTipoRateioNovo))
			retorno.append("<li>    - Tipo de Rateio do Centro de Custo</li>");
		
		List<NeoObject> listCCUAtual = wContrato.findGenericValue("centroCusto.rateio");
		List<NeoObject> listCCUNovo = wNovoContrato.findGenericValue("centroCusto.rateio");
		if (listCCUAtual.size() != listCCUNovo.size())
			retorno.append("<li>    - Centros de Custo</li>");
		
		retorno.append("</ul>");
		
		return retorno.toString();
	}
	
	private List<NeoObject> buscarContratosEdicao(Long codContrato)
	{
		QLGroupFilter qlAndFilter = new QLGroupFilter("AND");
		qlAndFilter.addFilter(new QLEqualsFilter("operacao.codigo", 2l));
		qlAndFilter.addFilter(new QLEqualsFilter("dadosContrato.codContrato", codContrato));
		qlAndFilter.addFilter(new QLRawFilter("_vo.wfprocess.processState = " + ProcessState.running.ordinal()));
		
		return PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlAndFilter);
	}
	
	private void registrarExecutorAprovacao(NeoUser executor, String atividade)
	{
		List<NeoObject> listExecutoresAprovacao = wrapper.findGenericValue("novoContrato.executoresAprovacao");
		for (NeoObject noExecutorAprovacaoRegistrado : listExecutoresAprovacao)
		{
			EntityWrapper wExecutorAprovacaoRegistrado = new EntityWrapper(noExecutorAprovacaoRegistrado);
			NeoUser executorRegistrado = wExecutorAprovacaoRegistrado.findGenericValue("executor");
			String atividadeRegistrada = wExecutorAprovacaoRegistrado.findGenericValue("atividade");
			
			if (executorRegistrado.equals(executor) && atividadeRegistrada.equals(atividade))
				return;
		}
		
		NeoObject noExecutorAprovacao = AdapterUtils.createNewEntityInstance("FAPSlipExecutorAprovacao");
		EntityWrapper wExecutorAprovacao = new EntityWrapper(noExecutorAprovacao);
		wExecutorAprovacao.setValue("executor", executor);
		wExecutorAprovacao.setValue("atividade", atividade);
		
		PersistEngine.persist(noExecutorAprovacao);
		
		wrapper.findField("novoContrato.executoresAprovacao").addValue(noExecutorAprovacao);
	}
}
