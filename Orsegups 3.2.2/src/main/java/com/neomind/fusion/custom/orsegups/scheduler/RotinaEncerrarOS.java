package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaEncerrarOS implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaEncerrarOS.class);

	public void execute(CustomJobContext arg0)
	{
		log.warn("##### INICIAR ROTINA ENCERRAR OS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		OutputStreamWriter writer = null;
		BufferedReader reader = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		try
		{

			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT os.ID_ORDEM FROM dbORDEM os WITH (NOLOCK) ");
			sql.append("WHERE os.FECHADO = 0 ");
			sql.append("AND ( ");
			sql.append("(os.IDOSDEFEITO = 122  AND EXISTS (SELECT * FROM hISTORICO_TESTE_AUTOMATICO ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO = 'E602' AND ht.DT_PROCESSADO > os.ABERTURA) ) ");
			sql.append("  OR (os.IDOSDEFEITO IN (122,1034)  AND EXISTS (SELECT * FROM hISTORICO_TESTE_AUTOMATICO ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO IN ('RP','E602') AND ht.DT_PROCESSADO > os.ABERTURA) ) ");
			sql.append("  OR (os.IDOSDEFEITO = 123  AND EXISTS (SELECT * FROM HISTORICO_SEM_CONTROLE ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO IN ('R302','R309') AND ht.DT_PROCESSADO > os.ABERTURA)) ");
			sql.append("  OR (os.IDOSDEFEITO = 125  AND EXISTS (SELECT * FROM HISTORICO_SEM_CONTROLE ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO IN ('R250','R900') AND ht.DT_PROCESSADO > os.ABERTURA) ) ");
			sql.append("  OR (os.IDOSDEFEITO IN (1069,1151) AND EXISTS (SELECT * FROM HISTORICO_SEM_CONTROLE ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO in ('RRED','D004','D033') AND ht.DT_PROCESSADO > os.ABERTURA) ) ");
			sql.append("  OR (os.IDOSDEFEITO IN(122,1148) AND EXISTS (SELECT 1 FROM HISTORICO_SEM_CONTROLE ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO = 'R002' AND ht.DT_PROCESSADO > os.ABERTURA) )   ");
			sql.append("  OR (os.IDOSDEFEITO = 1072 AND EXISTS (SELECT * FROM HISTORICO_SEM_CONTROLE ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO IN ('R900','RAA6','R250') AND ht.DT_PROCESSADO > os.ABERTURA )) ");
			sql.append("  OR (os.IDOSDEFEITO IN (926,1146) AND EXISTS (SELECT * FROM HISTORICO_SEM_CONTROLE ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO IN ('XX13','EX13') AND ht.DT_PROCESSADO > os.ABERTURA )) ");
			sql.append("  OR (os.IDOSDEFEITO = 1073 AND EXISTS (SELECT * FROM HISTORICO_SEM_CONTROLE ht WITH (NOLOCK) WHERE ht.CD_CLIENTE = os.CD_CLIENTE AND ht.CD_EVENTO IN ('E384','R384') AND ht.DT_PROCESSADO > os.ABERTURA )) ");
			sql.append(" ) ");
			
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			int contador = 0;
			int contadorSucesso = 0;
			int contadorErro = 0;
			while (rs.next())
			{
				int idOrdem = rs.getInt("ID_ORDEM");
				int idSolucao = 3;
				int idCausaDefeito = 1;
				String urlParameters = "orderId=" + idOrdem + "&solution=" + idSolucao + "&defectCause=" + idCausaDefeito;
				URL url = new URL("http://ssooap01:8080/SigmaWebServices/CloseOSWebService?");
				URLConnection connUrl = url.openConnection();
				connUrl.setDoOutput(true);
				writer = new OutputStreamWriter(connUrl.getOutputStream());

				writer.write(urlParameters);
				writer.flush();
				String line;
				reader = new BufferedReader(new InputStreamReader(connUrl.getInputStream()));
				while ((line = reader.readLine()) != null)
				{
				    
					if (NeoUtils.safeIsNotNull(line) && line.contains("success"))
					{
						contadorSucesso++;
						
					}
					else if (NeoUtils.safeIsNotNull(line) && line.contains("erro"))
					{						    	
						contadorErro++;
					}

				}
				writer.close();
				reader.close();
				contador++;
			}

			log.warn("##### EXECUTAR ROTINA ENCERRAR OS GERANDO UM TOTAL DE  " + contador + "  EVENTOS. FORAM EXECUTADOS " + contadorSucesso + "  COM SUCESSO E " + contadorErro + " EVENTOS COM ERROS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{

			log.error("##### ERRO ROTINA ENCERRAR OS: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("[" + key + "] ##### ERRO ROTINA ENCERRAR OS: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");

		}
		finally
		{
			try
			{
				if (writer != null)
					writer.close();
				if (reader != null)
					reader.close();
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA ENCERRAR OS: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			log.warn("##### FINALIZAR ROTINA ENCERRAR OS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

}
