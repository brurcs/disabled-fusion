package com.neomind.fusion.custom.orsegups.processoJuridico.civel.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.poi.xssf.usermodel.TextHorizontalOverflow;
import org.docx4j.model.datastorage.XPathEnhancerParser.originalAbsoluteLocationPathNoroot_return;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.MensagemTIVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class EscalaTarefaJuridico implements AdapterInterface {

	public static String mensagem = "Erro ao efetuar o escalonamento da tarefa";
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		

		NeoUser executorAtual = origin.getUser();
		NeoPaper responsavelGrupoExecutorAtual = executorAtual.getGroup().getResponsible();
		
		NeoUser novoExecutor = null;
		NeoPaper papelNovoExecutor = null;
		
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		try {
			
			if(NeoUtils.safeIsNotNull(executorAtual)) {
				
				if (executorAtual.getPapers().contains(responsavelGrupoExecutorAtual)){ // Caso o usuário seja o responsável do seu grupo, pega o responsável do nível superior		    
					papelNovoExecutor = executorAtual.getGroup().getUpperLevel().getResponsible();
					novoExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelNovoExecutor);	
				} else {
					papelNovoExecutor = executorAtual.getGroup().getResponsible();
					novoExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelNovoExecutor);
				}
				
				if (novoExecutor == null){
					novoExecutor = executorAtual;
				}
				
				if(origin.getActivityName().contains("Aprovar Justificativa") || origin.getActivityName().contains("Aprovar Pagamento")) {
					processEntity.setValue("responsavelAprovacao", novoExecutor);
				}else {				
					processEntity.setValue("responsavelEscritorio", novoExecutor);				
				}
			}
			
			if (origin.getActivityName().contains("Enviar Comprovante de Pagamento") && NeoUtils.safeIsNull(executorAtual)) {	
				papelNovoExecutor = OrsegupsUtils.getPaper("Coordenador Financeiro");
				novoExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelNovoExecutor);
				
				ajustaPrazoFinanceiro(processEntity);
				
			} else if (origin.getActivityName().contains("Elaborar Petição") || origin.getActivityName().contains("Inserir Dados do Processo")) {
				
				ajustaPrazoAtividade(processEntity);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(mensagem+" "+origin.getCode()+" - "+this.getClass().getSimpleName()+" "+NeoDateUtils.safeDateFormat(new GregorianCalendar()));
			throw new WorkflowException(mensagem+" "+origin.getCode());			
		}
		
	}
	
	private void ajustaPrazoAtividade(EntityWrapper processEntity) {
		
		try {
			
			GregorianCalendar prazoAtividade = (GregorianCalendar) processEntity.findGenericValue("prazo");
			GregorianCalendar prazo = (GregorianCalendar) prazoAtividade.clone();
			
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
			prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			prazo.set(GregorianCalendar.MINUTE, 59);
			prazo.set(GregorianCalendar.SECOND, 59);
			
			processEntity.findField("prazo").setValue(prazo);
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagem = "Erro ao ajustar o novo prazo para a atividade.";
			throw new WorkflowException(mensagem);
		}
		
	}

	/**
	 * Ajusta o prazo da atividade "Enviar Comprovante de Pagamento", 
	 * 
	 * @param processEntity
	 */
	private void ajustaPrazoFinanceiro(EntityWrapper processEntity) {
		
		try {
			
			GregorianCalendar prazoFinanceiro = (GregorianCalendar) processEntity.findGenericValue("prazoFinanceiro");
			GregorianCalendar prazo = (GregorianCalendar) prazoFinanceiro.clone();
			
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
			prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			prazo.set(GregorianCalendar.MINUTE, 59);
			prazo.set(GregorianCalendar.SECOND, 59);
			
			processEntity.findField("prazoFinanceiro").setValue(prazo);
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagem = "Erro ao ajustar o novo prazo para atividade de Enviar Comprovante de Pagamento!";
			throw new WorkflowException(mensagem);
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}
	
}
