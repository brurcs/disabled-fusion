package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ContaSapiensVO {
    private int codEmp;
    private int codFil;
    private int numCtr;
    private int numPos;
    private String nomCli;
    private String cgcCpf;
    private String tipoCliente;
    public int getCodEmp() {
        return codEmp;
    }
    public void setCodEmp(int codEmp) {
        this.codEmp = codEmp;
    }
    public int getCodFil() {
        return codFil;
    }
    public void setCodFil(int codFil) {
        this.codFil = codFil;
    }
    public int getNumCtr() {
        return numCtr;
    }
    public void setNumCtr(int numCtr) {
        this.numCtr = numCtr;
    }
    public int getNumPos() {
        return numPos;
    }
    public void setNumPos(int numPos) {
        this.numPos = numPos;
    }
    public String getNomCli() {
        return nomCli;
    }
    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }
    public String getCgcCpf() {
        return cgcCpf;
    }
    public void setCgcCpf(String cgcCpf) {
        this.cgcCpf = cgcCpf;
    }
    public String getTipoCliente() {
        return tipoCliente;
    }
    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }
    
    
}
