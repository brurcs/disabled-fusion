package com.neomind.fusion.custom.orsegups.fap.slip.adapter;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;

public class FAPSlipAvancaOCAutomatico implements ActivityStartEventListener
{

	private static final Log log = LogFactory.getLog(FAPSlipAvancaOCAutomatico.class);

	@Override
	public void onStart(final ActivityEvent event) throws ActivityException
	{
		System.out.println("Inicio avanço automático");
		final Activity act = event.getActivity();
		final Long activityNeoId = act.getNeoId();
		NeoUser nu = PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));

		System.out.println("NeoId Atividade: " + activityNeoId);
		System.out.println("Usuário: " + nu);

		final Long userNeoId = nu.getNeoId();

		NeoRunnable newEntityManager = new NeoRunnable()
		{
			@Override
			public void run() throws Exception
			{
				Thread.sleep(10000);

				System.out.println("Avançando atividade " + act);

				Activity act2 = PersistEngine.getNeoObject(Activity.class, activityNeoId);
				NeoUser nuExecutor = PersistEngine.getNeoObject(NeoUser.class, userNeoId);

				List<Task> taskList = ((UserActivity)act2).getTaskList();
				for (Task task : taskList)
				{
					RuntimeEngine.getTaskService().complete(task, nuExecutor);

					System.out.println("Tarefa " + event.getActivity().getInstance().getTitle() + " avançada com sucesso!");
				}
				
				//para assumir a atividade em pool
				//Task task = act2.getTaskAssigner().assign((UserActivity) act2, nuExecutor, true);
			}
		};

		try
		{
			PersistEngine.pooledBackgroundManagedRun("poolBruno", newEntityManager);
		}
		catch (Exception e)
		{
			System.out.println("Erro FAPSlipAvancaOC: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
