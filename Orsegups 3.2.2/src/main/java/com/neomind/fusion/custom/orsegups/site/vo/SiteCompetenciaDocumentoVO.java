package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.ArrayList;
import java.util.Collection;

import com.neomind.fusion.custom.orsegups.presenca.vo.ClienteDocumentosSapiensVO;

public class SiteCompetenciaDocumentoVO
{
	  private String competencia;
	  private SiteEmpresaDocumentoVO empresa;
	  private String codcli;
	  private String tipo;
	  private Collection<SiteDocumentoVO> documentos = new ArrayList<SiteDocumentoVO>();
	  private String intnet;
	  private Boolean chkDeacordoRecnf; // aceita receber fatura por email?
	  private Boolean chkDeacordoRecdoc; // aceita consultar documentos só pelo site.
	  private Boolean mostraFichaPonto;
	  private Collection<ClienteDocumentosSapiensVO> documentosCadastradosSite = new ArrayList<ClienteDocumentosSapiensVO>(); // contém a lista dos documentos cadastrados na tabela usu_t160doc
	
	public String getCompetencia()
	{
		return competencia;
	}
	public void setCompetencia(String competencia)
	{
		this.competencia = competencia;
	}
	
	
	
	public Collection<SiteDocumentoVO> getDocumentos() {
		return documentos;
	}
	public void setDocumentos(Collection<SiteDocumentoVO> documentos) {
		this.documentos = documentos;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getCodcli() {
		return codcli;
	}
	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}
	public SiteEmpresaDocumentoVO getEmpresa() {
		return empresa;
	}
	public void setEmpresa(SiteEmpresaDocumentoVO empresa) {
		this.empresa = empresa;
	}
	
	
	public String getIntnet()
	{
		return intnet;
	}
	public void setIntnet(String intnet)
	{
		this.intnet = intnet;
	}
	public Boolean getChkDeacordoRecnf()
	{
		return chkDeacordoRecnf;
	}
	public void setChkDeacordoRecnf(Boolean chkDeacordoRecnf)
	{
		this.chkDeacordoRecnf = chkDeacordoRecnf;
	}
	public Boolean getChkDeacordoRecdoc()
	{
		return chkDeacordoRecdoc;
	}
	public void setChkDeacordoRecdoc(Boolean chkDeacordoRecdoc)
	{
		this.chkDeacordoRecdoc = chkDeacordoRecdoc;
	}
	
	public Boolean getMostraFichaPonto() {
		return mostraFichaPonto;
	}
	public void setMostraFichaPonto(Boolean mostraFichaPonto) {
		this.mostraFichaPonto = mostraFichaPonto;
	}
	public Collection<ClienteDocumentosSapiensVO> getDocumentosCadastradosSite() {
		return documentosCadastradosSite;
	}
	public void setDocumentosCadastradosSite(
			Collection<ClienteDocumentosSapiensVO> documentosCadastradosSite) {
		this.documentosCadastradosSite = documentosCadastradosSite;
	}
	
	
	
	  
	  
}
