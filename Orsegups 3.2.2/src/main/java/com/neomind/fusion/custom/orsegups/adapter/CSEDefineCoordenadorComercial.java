package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CSEDefineCoordenadorComercial implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.adapter.CSEDefineCoordenadorComercial");
		log.warn("##### CSE - INICIO - DEFINE COORDENADOR COMERCIAL - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		Long tipemc = (Long) processEntity.findValue("tipoMercao.tipo");
		
		System.out.println("TIPO MERCADO CSE - " + tipemc.toString());
		NeoPaper papel = new NeoPaper();
		NeoUser usuario = new NeoUser();
		
		try
		{
			//Público
			if (NeoUtils.safeIsNotNull(tipemc) && tipemc == 2L)
			{
				papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","Coordenador Comercial"));
			}
			else //Privado
			{
				papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","CSECadastrarServicoExtraEscalada"));
			}
			
			if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers())
				{
					usuario = user;
					break;
				}
			}
			else
			{
				throw new WorkflowException("Papel de coordenador sem usuário definido, entrar em contato com a TI!");
			}
			processEntity.setValue("responsavelSuperior", usuario);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("CSE - Erro na regra de escalonamento.");
		}
		log.warn("##### CSE - INICIO - DEFINE COORDENADOR COMERCIAL - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
