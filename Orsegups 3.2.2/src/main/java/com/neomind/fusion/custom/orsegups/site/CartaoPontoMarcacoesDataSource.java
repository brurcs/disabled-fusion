package com.neomind.fusion.custom.orsegups.site;

public class CartaoPontoMarcacoesDataSource {
	String data;
	String diaSemana;
	String marcacoes;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDiaSemana() {
		return diaSemana;
	}
	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}
	public String getMarcacoes() {
		return marcacoes;
	}
	public void setMarcacoes(String marcacoes) {
		this.marcacoes = marcacoes;
	}
	@Override
	public String toString() {
		return "CartaoPontoMarcacoesDataSource [data=" + data + ", diaSemana="
				+ diaSemana + ", marcacoes=" + marcacoes + "]";
	}
	
	
	
	
	
	

}
