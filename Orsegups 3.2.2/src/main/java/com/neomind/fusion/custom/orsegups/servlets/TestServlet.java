package com.neomind.fusion.custom.orsegups.servlets;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.util.NeoUtils;

@WebServlet(name="TestServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.TestServlet"})
public class TestServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String barcode = "000151000013754000241425";
		String emp = barcode.substring(0,5);
		String tip = barcode.substring(5,6);
		String mat = barcode.substring(6,15);
		Integer comp = Integer.parseInt(barcode.substring(19));
		System.out.println(comp);
		GregorianCalendar data = new GregorianCalendar(1901, 0, 1);
		data.add(GregorianCalendar.DAY_OF_MONTH, comp);
		System.out.println(NeoUtils.safeDateFormat(data, "MM/yyyy"));
	}
}
