package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RSCPrazoEficacia implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		
		RSCUtils oRSCUtil = new RSCUtils();
		GregorianCalendar prazoDeadLine = new GregorianCalendar();
		
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		/*INICIO 
			Define o Prazo para Execução das Atividades de Verificação de Eficácia 
		*/
		if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 1L)
		{
			prazoDeadLine = oRSCUtil.retornaPrazoDeadLine("1ª Verificação Eficácia - Sol. Cliente", new GregorianCalendar());
		}
		else if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 2L)
		{
			prazoDeadLine = oRSCUtil.retornaPrazoDeadLine("2ª Verificação Eficácia - Sol. Cliente", new GregorianCalendar());
		}
		else if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 3L)
		{
			prazoDeadLine = oRSCUtil.retornaPrazoDeadLine("3ª Verificação Eficácia - Sol. Cliente", new GregorianCalendar());
		}
		else if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 4L)
		{
			prazoDeadLine = oRSCUtil.retornaPrazoDeadLine("4ª Verificação Eficácia - Sol. Cliente", new GregorianCalendar());
		}
		else if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 5L)
		{
			prazoDeadLine = oRSCUtil.retornaPrazoDeadLine("5ª Verificação Eficácia - Sol. Clientes", new GregorianCalendar());
		}
						
		processEntity.findField("prazoVerificacaoEficacia").setValue(prazoDeadLine);	
		/*FIM*/
		
		EntityWrapper verEfiWrapper = null;				
		/*Alteração Boleto NF*/
		if(NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaAlteracaoBoletoNF")))
		{
			NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaAlteracaoBoletoNF");
			verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
		}
		/*Atualização Cadastral*/
		if(NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaAtualizacaoCadastral")))
		{
			NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaAtualizacaoCadastral");
			verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
		}
		/*Cancelamento*/
		if(NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaCancelamento")))
		{
			NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaCancelamento");
			verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
		}
		/*Diversos*/
		if(NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaDiversos")))
		{
			NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaDiversos");
			verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
		}
		/*Orçamento*/
		if(NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaOrcamento")))
		{
			NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaOrcamento");
			verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
		}
		if(NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaOrcamentoVisita")))
		{
			NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaOrcamentoVisita");
			verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
		}
		/*Cancelamento Novo*/
		if(NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaCancelamentoNovo")))
		{
			NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaCancelamentoNovo");
			verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
		}
	
		
		/*Criado esse bloco, pois no workflow pai não criamos o registro de atividade*/
		if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 1L && 
		   ((origin.getActivityName().equalsIgnoreCase("Agendar Visita - Executivo")) ||
		    (origin.getActivityName().equalsIgnoreCase("Agendar Visita Escalada - Superior")) ||
		    (origin.getActivityName().equalsIgnoreCase("Agendar Visita Escalada - Diretoria")) ||
		    (origin.getActivityName().equalsIgnoreCase("Agendar Visita Escalada - Presidência"))))
		{
			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("RSCRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);
		
			wRegistro.findField("responsavel").setValue(origin.getUser());
			wRegistro.findField("dataInicialRSC").setValue(origin.getStartDate());
			wRegistro.findField("dataFinalRSC").setValue(origin.getFinishDate());
			wRegistro.findField("descricao").setValue("Registro inserido automaticamente. Prazo para Verificação de Eficácia referente ao Agendamento da Visita ao Cliente.");
			wRegistro.findField("atividade").setValue(origin.getActivityName());
			wRegistro.findField("prazo").setValue(prazoDeadLine);
						
			NeoObject rscRegistroSolicitacaoClienteNovo = (NeoObject) verEfiWrapper.findValue("RscRelatorioSolicitacaoCliente");
			EntityWrapper rscWrapper = new EntityWrapper(rscRegistroSolicitacaoClienteNovo);

			PersistEngine.persist(registro);
			rscWrapper.findField("registroAtividades").addValue(registro);
		}
			
		NeoObject rscRegistroSolicitacaoCliente = (NeoObject) verEfiWrapper.findValue("RscRelatorioSolicitacaoCliente");
		if(rscRegistroSolicitacaoCliente != null) {
			EntityWrapper rscWrapper = new EntityWrapper(rscRegistroSolicitacaoCliente);			
		
			/*Pego meu último valor da minha lista de Registro de Atividades e altero o valor do campo prazo*/
			List<NeoObject> objLstRegistroAtividades = (List<NeoObject>) rscWrapper.findValue("RscRelatorioSolicitacaoCliente.registroAtividades");
			if (NeoUtils.safeIsNotNull(objLstRegistroAtividades))
			{
				NeoObject ultima = objLstRegistroAtividades.get(objLstRegistroAtividades.size() - 1);
				EntityWrapper ewUltima = new EntityWrapper(ultima);
				
				ewUltima.findField("prazo").setValue(prazoDeadLine);
			}
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
	}
}

