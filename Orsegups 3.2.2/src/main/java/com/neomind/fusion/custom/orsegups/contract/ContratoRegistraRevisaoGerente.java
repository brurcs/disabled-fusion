package com.neomind.fusion.custom.orsegups.contract;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryXXX2;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoRegistraRevisaoGerente implements AdapterInterface {
    	private static final Log log = LogFactory.getLog(ContratoRegistraRevisaoGerente.class);
	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
	    Long time = new Date().getTime();
		try{
			boolean aprovaValores = NeoUtils.safeBoolean(wrapper.findValue("aprovaValores"));
			
			
			NeoObject tramite = AdapterUtils.createNewEntityInstance("FGChistoricoTramite");
			EntityWrapper wTramite = new EntityWrapper(tramite);
			wTramite.setValue("usuario", PortalUtil.getCurrentUser().getCode() );
			
			
			if (!aprovaValores ){
				wTramite.setValue("acao", "Recusou Aprovação de Valores." );
				wTramite.setValue("obs", NeoUtils.safeOutputString(wrapper.findValue("motivoValores")) );
			}else{
				wTramite.setValue("acao", "Aprovou Valores" );
				
				String obsTramite = "Valores Aprovados.";
				if (wrapper.findValue("obsTramite") != null && !NeoUtils.safeOutputString(wrapper.findValue("obsTramite")).isEmpty()){
				    obsTramite += "\n Observação: \n"+NeoUtils.safeOutputString(wrapper.findValue("obsTramite"));
				}
				wTramite.setValue("obs", obsTramite);
			}
			
			PersistEngine.persist(tramite);
			
			wrapper.findField("listaTramites").addValue(tramite);
			wrapper.setValue("motivoValores","");
			wrapper.setValue("obsTramite","");
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("[" + time + "] ContratoRegistraRevisaoGerente erro no processamento: " + e.getMessage());
			throw new WorkflowException("Procurar no log por["+time+"] Erro revisar aprovação de Valores. msg: "+e.getMessage());
			
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		// TODO Auto-generated method stub
		
	}

}
