package com.neomind.fusion.custom.orsegups.fap.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dto.HistoricoFapDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.ItemOrcamentoDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.TaticoDTO;
import com.neomind.fusion.custom.orsegups.fap.exception.NaoEncontradoException;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.fap.utils.FapUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * @author diogo.silva
 * */
public class AplicacaoTaticoDAO {

    /**
     * Método para consular a lista de centro de custos
     * de acordo com uma rota específica. Consulta as contas associadas a rota e verifica no Sapiens em quais
     * postos essas contas estão vinculadas.
     * @param codRota - Objeto Long, sendo o código da rota que se deseja usar como parâmetro
     * @return listaCcu - Objeto List de NeoObjects, contendo a lista de centro de custos para a rota em questão
     * @throws Exception Para exceções generalizadas
     * @throws WorkflowException
     */
    public static List<NeoObject> consultaListaCcu(Long codRota) throws Exception, WorkflowException {

	StringBuilder sql = new StringBuilder();
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	List<NeoObject> listaCcu = new ArrayList<>();
	Long codigoRota = codRota;

	conn = PersistEngine.getConnection("SIGMA90");

	sql.append(" SELECT CVS.USU_CODEMP, CVS.USU_CODCCU FROM dbCENTRAL CTRL                                                                       ");
	sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160SIG SIG ON CTRL.CD_CLIENTE = SIG.USU_CODCLI                                    ");
	sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160CVS CVS ON CVS.USU_CODEMP = SIG.USU_CODEMP AND CVS.USU_CODFIL = SIG.USU_CODFIL ");
	sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.E044CCU CCU ON CCU.CODCCU = CVS.USU_CODCCU AND CVS.USU_CODEMP = CCU.CODEMP			 ");
	sql.append(" AND SIG.USU_NUMCTR = CVS.USU_NUMCTR AND SIG.USU_NUMPOS = CVS.USU_NUMPOS                                                         ");
	sql.append(" WHERE ID_ROTA = ?  AND CTRL_CENTRAL = 1 AND FG_ATIVO = 1                                                                        ");	
	sql.append(" AND ((CVS.usu_sitcvs = 'A') OR (CVS.usu_sitcvs = 'I' AND CVS.usu_datfim >= GETDATE()))                                          ");  
	sql.append(" AND CTRL.ID_CENTRAL NOT LIKE 'AAA%'                                                                          					 ");
	sql.append(" AND CTRL.ID_CENTRAL NOT LIKE 'DDDD'                                                          									 ");
    sql.append(" AND CTRL.ID_CENTRAL NOT LIKE 'FFFF'                                                             								 ");
	sql.append(" AND CCU.CLACCU NOT LIKE '11%'                                                            										 ");
	try {
	    pstm = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    pstm.setLong(1, codigoRota);
	    rs = pstm.executeQuery();

	    while(rs.next()){
		NeoObject efCentroDeCusto = AdapterUtils.getInstantiableEntityInfo("FAPListaCentroDeCusto").createNewInstance();
		EntityWrapper ewCentroDeCusto = new EntityWrapper(efCentroDeCusto);
		Long empresa = rs.getLong(1);
		String codCcu = rs.getString(2);
		ewCentroDeCusto.setValue("codigoEmpresa", empresa);
		ewCentroDeCusto.setValue("codigoCentroCusto", codCcu);

		PersistEngine.persist(efCentroDeCusto);		
		listaCcu.add(efCentroDeCusto);	
	    }
	    return listaCcu;   

	} catch (WorkflowException e) {
	    throw e;	    
	} catch (Exception e) {
	    throw new Exception(e.getMessage());
	} finally {
	    try {				
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    
    /**
     * Este método é utilizado para a modalidade de pagamento "por atendimento". Realiza uma consulta no formulário
     * onde os eventos foram gravados e analisa em quais postos as contas desses eventos estão vinculadas.
     * @param codTarefa - Código tarefa que será utilizado como filtro no formulário onde a lista de eventos
     * foi salva.
     * @return listaCcu - Objeto List contendo NeoObjects, sendo esses os centros de custos encontrados na consulta.
     * @throws Exception Para exceções generalizadas
     * @throws WorkflowException
     * */
    public static List<NeoObject> consultaListaCcuViaEventos(String codTarefa) throws Exception, WorkflowException {

	StringBuilder sql = new StringBuilder();
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	List<NeoObject> listaCcu = new ArrayList<>();
	String codigoTarefa = codTarefa;

	conn = PersistEngine.getConnection("");

	sql.append(" SELECT CVS.USU_CODEMP, CVS.USU_CODCCU FROM [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL CTRL                                                                      ");
	sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160SIG SIG ON CTRL.CD_CLIENTE = SIG.USU_CODCLI                                   ");
	sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160CVS CVS ON CVS.USU_CODEMP = SIG.USU_CODEMP AND CVS.USU_CODFIL = SIG.USU_CODFIL");
	sql.append(" AND SIG.USU_NUMCTR = CVS.USU_NUMCTR AND SIG.USU_NUMPOS = CVS.USU_NUMPOS                                                        ");
	sql.append(" INNER JOIN D_FAPHISTORICOEVENTOS HIST ON HIST.CDCLIENTE = SIG.USU_CODCLI                                                       ");                                
	sql.append(" WHERE HIST.CODIGOTAREFA = (?)  AND CTRL_CENTRAL = 1 AND FG_ATIVO = 1                                                           ");
	sql.append(" AND ((CVS.usu_sitcvs = 'A') OR (CVS.usu_sitcvs = 'I' AND CVS.usu_datfim >= GETDATE()))                                         ");

	try {

	    pstm = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    pstm.setString(1, codigoTarefa);
	    rs = pstm.executeQuery();

	    while(rs.next()){
		NeoObject efCentroDeCusto = AdapterUtils.getInstantiableEntityInfo("FAPListaCentroDeCusto").createNewInstance();
		EntityWrapper ewCentroDeCusto = new EntityWrapper(efCentroDeCusto);
		Long empresa = rs.getLong(1);
		String codCcu = rs.getString(2);
		ewCentroDeCusto.setValue("codigoEmpresa", empresa);
		ewCentroDeCusto.setValue("codigoCentroCusto", codCcu);

		PersistEngine.persist(efCentroDeCusto);		
		listaCcu.add(efCentroDeCusto);	
	    }
	    return listaCcu;   

	} catch (WorkflowException e) {
	    throw e;	    
	} catch (Exception e) {
	    throw new Exception(e.getMessage());
	} finally {
	    try {				
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }


    /**
     * Retorna as tarefas de FAP já abertas para uma rota em específico. Utilizada em um converter
     * para mostrar na tarefa o histórico dos pagamentos (FAP's).
     * @param codigo - Objeto Long, sendo este o código da Rota.
     * @param aplicacao - Objeto Long, representa o código da aplicação de pagamento.
     * @param neoId - Objeto Long, é o neoid da tarefa atual que é inserida na cláusula where da consulta para não ser
     * contabilizada.
     * @return taticoDTO - Objeto TaticoDTO contendo os valores encontrados na consulta
     * @throws Exception Para exceções generalizadas.
     */
    public static TaticoDTO recuperaHistoricoTatico(Long codigo, Long aplicacao, Long neoId) throws Exception {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();

	List<HistoricoFapDTO> listaHistoricoFap = new ArrayList<>();
	List<ItemOrcamentoDTO> listaItemOrcamento = null;
	TaticoDTO taticoDTO = new TaticoDTO();

	Long codigoRota = codigo;
	Long wfNeoId = neoId;
	Long codigoAplicacao = aplicacao;

	sql.append(" SELECT ROTA.CD_ROTA, FAPAUT.rotaSigma_neoId,FAPAUT.tecnicoSigma_neoId, FAPAUT.NEOID AS fapNeoId, ");
	sql.append(" WF.code, FAPTAT.valorRota, WF.neoid as wfNeoId, WF.ProcessState,FAPAUT.dataSolicitacao           ");
	sql.append(" FROM WFPROCESS WF                                                                                ");
	sql.append(" INNER JOIN D_FAPAutorizacaoDePagamento FAPAUT ON FAPAUT.wfprocess_neoId = WF.neoId               ");
	sql.append(" INNER JOIN D_FAPAplicacaoTatico FAPTAT ON FAPAUT.rotaSigma_neoId = FAPTAT.neoId                  ");
	sql.append(" INNER JOIN X_SIGMAROTA ROTA ON FAPTAT.rotaSigma_neoId = ROTA.neoId                               ");
	sql.append(" WHERE WF.saved = 1 AND (WF.processState = 0 OR WF.processState = 1)                              ");
	sql.append(" AND WF.neoId <> " + wfNeoId                                                                       );
	sql.append(" AND ROTA.cd_rota =  " + codigoRota                                                                );
	sql.append(" AND FAPAUT.aprovado = 1                                                                          ");
	try {	
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    rs = pstm.executeQuery();

	    while(rs.next()){
		HistoricoFapDTO historicoFap = new HistoricoFapDTO();
		String codigoTarefa = rs.getString("code");
		Date dataSolicitacao = rs.getDate("dataSolicitacao");
		int status = rs.getInt("processState");
		Long wfId = rs.getLong("wfNeoId");
		//historicoFap.setLaudo(laudo);
		//historicoFap.setRelato(relato);

		historicoFap.setCodigoTarefa(codigoTarefa);
		historicoFap.setDataSolicitacao(dataSolicitacao);
		historicoFap.setNeoIdTarefa(wfId);
		switch (status) {
		case 0:
		    historicoFap.setStatus("Em andamento");
		    break;
		case 1:
		    historicoFap.setStatus("Encerrado");
		    break;
		}

		//recupera os itens de orcamento da FAP atual
		listaItemOrcamento = AplicacaoTaticoDAO.consultaItemOrcamento(codigoTarefa, codigoAplicacao);
		BigDecimal valorTotal = new BigDecimal(0.00);

		if(NeoUtils.safeIsNotNull(listaItemOrcamento)){
		    for(ItemOrcamentoDTO item : listaItemOrcamento){
			valorTotal = valorTotal.add(item.getValor());
		    }

		    // Objeto que representa a FAP atual recebe os itens de orçamento e o valor total dos itens
		    historicoFap.setItemOrcamento(listaItemOrcamento);
		    historicoFap.setValorTotal(valorTotal);
		    // Objeto da aplicação recebe a lista de suas respectivas FAP's
		    listaHistoricoFap.add(historicoFap);
		    taticoDTO.setHistorico(listaHistoricoFap);	
		}
	    }
	    return taticoDTO;
	} catch (Exception e) {
	    throw new Exception(e.getMessage());
	} finally {
	    try {				
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}  
    }

    /**
     * Consulta os itens de orçamento para o histórico de pagamentos.
     * @param code - Objeto String, sendo este o código da tarefa que será utilizada como filtro na cláusula where da consulta. 
     * @param aplicacao - Objeto Long, representa o código da aplicação de pagamento.
     * @return listaItens - List contendo objectos ItemOrcamentoDTO. Estes são os itens de orçamento encontrados na consulta.
     * @throws Exception Para exceções generalizadas.
     */
    public static List<ItemOrcamentoDTO> consultaItemOrcamento(String code, Long aplicacao) throws Exception {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();

	List<ItemOrcamentoDTO> listaItens = new ArrayList<>();
	Long codigoAplicacao = aplicacao;
	String codigo = code;

	sql.append(" SELECT FAP.NEOID, ITEM.DESCRICAO, ITEM.VALOR,TIPORC.descricao FROM D_FAPAutorizacaoDePagamento FAP                ");
	sql.append(" INNER JOIN D_FAPAUTORIZACAODEPAGAMENTO_ITEMORCAMENTO ASSOC ON FAP.neoId = ASSOC.D_FAPAutorizacaoDePagamento_neoId ");
	sql.append(" INNER JOIN D_FAPItemDeOrcamento ITEM ON ASSOC.itemOrcamento_neoId = ITEM.neoId                                    ");
	sql.append(" INNER JOIN D_FAPTIPOITEMDEORCAMENTO TIPORC ON ITEM.TIPOITEM_NEOID = TIPORC.NEOID                                  ");
	sql.append(" INNER JOIN D_FAPAplicacaoDoPagamento APL ON FAP.aplicacaoPagamento_neoId = APL.NEOID                              ");
	sql.append(" INNER JOIN WFProcess WF ON WF.neoId = FAP.wfprocess_neoId                                                         ");
	sql.append(" WHERE APL.codigo = " + codigoAplicacao                                                                             );
	sql.append(" AND WF.code = " + codigo                                                                                  );


	try {
	    conn = PersistEngine.getConnection("");

	    pstm = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    rs = pstm.executeQuery();

	    while(rs.next()){
		ItemOrcamentoDTO item = new ItemOrcamentoDTO();
		String descricaoItem = rs.getString(2);
		BigDecimal valorItem = rs.getBigDecimal(3);
		String tipoOrc = rs.getString(4);

		item.setDescricao(descricaoItem);
		item.setTipo(tipoOrc);
		item.setValor(valorItem);

		listaItens.add(item);  
	    }
	    return listaItens;   
	} catch (Exception e) {
	    throw new Exception(e.getMessage());
	} finally {
	    try {				
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    
    /**
     * Método que insere e retorna as contas que estão associadas a rota passada como parâmetro. 
     * A inserção é feita no formulário dinâmico FAPHistoricoContas.
     * P.S.: Apenas contas conciliadas são tidas como válidas para serem inseridas. 
     * @param codigoRota - Objeto Long, sendo este o código da rota que será utilizado na cláusula where da consulta.
     * @param code - Objeto String. Este será inserido no formulário de histórico de contas para a tarefa em questão.
     * @return listaContas - Objeto List contendo NeoObjects. São objetos contendo os dados das contas associadas a rota em questão.
     * @throws Exception Para exceções generalizadas.
     * */
    public static List<NeoObject> retornaContas(Long codigoRota, String code) throws Exception {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();

	Long codigo = codigoRota;
	String codigoTarefa = code;
	List<NeoObject> listaContas = new ArrayList<>();

	sql.append("SELECT CTRL.ID_ROTA, RT.NM_ROTA, CTRL.ID_EMPRESA, EMP.NM_FANTASIA, CTRL.ID_CENTRAL, 								");
	sql.append("(SELECT TOP 1 RAZAO FROM DBCENTRAL WHERE ID_ROTA = CTRL.ID_ROTA AND ID_EMPRESA = CTRL.ID_EMPRESA AND ID_CENTRAL = CTRL.ID_CENTRAL) AS RAZAOCLIENTE	");
	sql.append("FROM DBCENTRAL CTRL																	");
	sql.append("INNER JOIN EMPRESA EMP ON EMP.CD_EMPRESA = CTRL.ID_EMPRESA												");
	sql.append("INNER JOIN ROTA RT ON RT.CD_ROTA = CTRL.ID_ROTA													");
	sql.append("WHERE CTRL.FG_ATIVO = 1 																");									  								  				      
	sql.append("AND CTRL.CTRL_CENTRAL = 1 																"); 							  								  				      
	sql.append("AND CTRL.ID_CENTRAL NOT LIKE 'AAA%' 														");							  						  				      
	sql.append("AND CTRL.ID_CENTRAL NOT LIKE 'DDDD' 														");							 						  				      
	sql.append("AND CTRL.ID_CENTRAL NOT LIKE 'FFFF'															");						  						  				      
	sql.append("AND CTRL.ID_CENTRAL NOT LIKE 'R%'															");
	sql.append("AND CTRL.ID_ROTA = ?   																");
	sql.append("GROUP BY CTRL.ID_ROTA, RT.NM_ROTA, CTRL.ID_EMPRESA, EMP.NM_FANTASIA, CTRL.ID_CENTRAL								");
	sql.append("ORDER BY CTRL.ID_CENTRAL                                                                                                                            ");


	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    pstm.setLong(1, codigo);
	    rs = pstm.executeQuery();

	    if(rs.next()){
		rs.beforeFirst();

		while(rs.next()){
		    String razao = rs.getString("RAZAOCLIENTE");
		    String central = rs.getString("ID_CENTRAL");
		    Long empresa = rs.getLong("ID_EMPRESA");		    
		    Boolean conciliado = FapUtils.verificaConciliacao(empresa, central);

		    if(conciliado){
			NeoObject wfHistorico = AdapterUtils.getInstantiableEntityInfo("FapHistoricoContas").createNewInstance();
			EntityWrapper ewHistorico = new EntityWrapper(wfHistorico); 		   

			if(NeoUtils.safeIsNotNull(razao,central)){
			    ewHistorico.setValue("cliente", razao);
			    ewHistorico.setValue("central", central);
			    ewHistorico.setValue("tipoConta", "--");
			    ewHistorico.setValue("code", codigoTarefa);
			}

			PersistEngine.persist(wfHistorico);

			listaContas.add(wfHistorico);
		    }
		}
		return listaContas; 
	    } else {
		return null;
	    }     	    
	} catch (Exception e) {
	    throw new Exception(e.getMessage());
	} finally {
	    try {				
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}	 	
    }


    /**
     * Calcula o valor que será pago de acordo com a modalidade e quantidade de contas/eventos.
     * @param codigoModalidade - Objeto Long, representa a modalidade de pagamento.
     * @param valorPgto - Objeto Long, sendo este o valor do pagamento que será utilizado conforme a modalidade do mesmo.
     * @param quantidade - Objeto Long, representa a quantidade de contas ou eventos que serão multiplicados pelo valor do pagamento.
     * @return valorPagamento - Objeto BigDecimal. É o valor total a ser pago ao fornecedor.
     * @throws NaoEncontradoException Quando o método não encontrar a modalidade de pagamento através do código passado como parâmetro.
     * @throws Exception Para exceções generalizadas.
     * */
    public static BigDecimal valorPagamento(Long codigoModalidade, BigDecimal valorPgto, Long quantidade) throws Exception, NaoEncontradoException{
	BigDecimal valorPagamento = valorPgto;
	Long modalidade = codigoModalidade;
	BigDecimal valorTotal = new BigDecimal(0.00);

	try {
	    switch (Integer.parseInt(modalidade.toString())) {
	    case 1: // Fixo Mensal - Já é o valor cadastrado
		return valorPagamento;
	    case 2: // Por Conta - Quantidade de contas x Valor do Pagamento		
		Long quantidadeContas = quantidade;
		valorTotal = valorPagamento.multiply(new BigDecimal(quantidadeContas));		
		return valorTotal;
	    case 3: // Por Atendimento  - Valor x Quantidade de atendimentos
		Long quantidadeDeEventos = quantidade;
		valorTotal = valorPagamento.multiply(new BigDecimal(quantidadeDeEventos));		
		return valorTotal;
	    default:
		throw new NaoEncontradoException("Modalidade de pagamento não configurada. Favor contatar a TI.");
	    }
	} catch (NaoEncontradoException e) {
	    e.printStackTrace();
	    throw new NaoEncontradoException(e.getMessage());
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new NaoEncontradoException("Não foi possível calcular o valor do pagamento para esta Rota. Favor contatar a TI.");
	}
    }


    /**
     * Este método consulta e retona a quantidade de contas conciliadas que estão associadas a rota passada como parâmetro.
     * @param codigoRota - Objeto Long, sendo este o código da rota que será utilizada na cláusula where da consulta.
     * @return totalDeContas - Objeto Long, representa a quantidade de contas que atenderam as restrições da consulta.
     * @throws NaoEncontradoException Caso o método não encontre nenhuma conta conciliada associada a rota em questão.
     * @throws Exception Para exceções generalizadas. 
     * */
    public static Long calculaQuantidadeContas(Long codigoRota) throws Exception, NaoEncontradoException {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();
	Long totalDeContas = 0L;

	Long codigo = codigoRota;

	sql.append(" SELECT DISTINCT(R.CD_ROTA), C.ID_EMPRESA, C.ID_CENTRAL FROM DBCENTRAL C ");
	sql.append(" INNER JOIN ROTA R ON C.ID_ROTA = R.CD_ROTA ");
	sql.append(" WHERE FG_ATIVO = 1 AND C.CTRL_CENTRAL = 1 ");
	sql.append(" AND C.ID_CENTRAL NOT LIKE 'AAA%' AND C.ID_CENTRAL NOT LIKE 'DDDD' ");
	sql.append(" AND C.ID_CENTRAL NOT LIKE 'FFFF' AND C.ID_CENTRAL NOT LIKE 'R%' ");
	sql.append(" AND R.CD_ROTA = ? ");
	sql.append(" AND EXISTS ( ");
	sql.append(" 	 SELECT ID_EMPRESA, ID_CENTRAL, CD_CLIENTE FROM dbCENTRAL  ");                               
	sql.append(" 	 WHERE ID_EMPRESA = C.ID_EMPRESA  ");                                                                  
	sql.append(" 	 AND ID_CENTRAL =  C.ID_CENTRAL   ");                                                                    
	sql.append(" 	 AND EXISTS ( ");
	sql.append(" 	            SELECT * FROM [FSOODB04\\SQL02].SAPIENS.DBO.usu_t160sig SIG  ");                            
	sql.append(" 		    INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.usu_t160cvs CVS ");                               
	sql.append(" 		    ON SIG.usu_codemp = CVS.usu_codemp AND SIG.usu_codfil = CVS.usu_codfil ");                  
	sql.append(" 		    AND SIG.usu_numctr = CVS.usu_numctr AND SIG.usu_numpos = CVS.usu_numpos ");                  
	sql.append(" 		    WHERE SIG.USU_CODCLI = CD_CLIENTE      ");                                                  
	sql.append(" 		    AND ((CVS.usu_sitcvs = 'A') OR (CVS.usu_sitcvs = 'I' AND CVS.usu_datfim >= GETDATE()-1)) ");
	sql.append(" 		    ) ");	  
	sql.append("    ) ");
	sql.append(" GROUP BY R.CD_ROTA, C.ID_EMPRESA, C.ID_CENTRAL ");

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    pstm.setLong(1, codigo);
	    rs = pstm.executeQuery();

	    if(rs.next()){
		rs.beforeFirst();
		while(rs.next()){
		    totalDeContas ++;
		}
		return totalDeContas;
	    } else {
		throw new NaoEncontradoException("A rota " + codigoRota + " não possui contas conciliadas associadas.");
	    } 
	}  catch(NaoEncontradoException e){
	    e.printStackTrace();
	    throw new NaoEncontradoException(e.getMessage());	    
	}
	catch(Exception e){
	    e.printStackTrace();
	    throw new Exception("Erro inesperado ao recuperar a quantidade de contas para esta rota. Favor contatar a TI.");	    
	} finally {
	    try {				
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    } 


    /**
     * Este método insere e retorna os eventos atendidos pelo tático dentro do período especificado. Para os pagamentos que não são feitos
     * por atentimento, este método serve apenas para preenchimento de histórico.
     * @param codRota - Objeto Long, representa a rota que será utilizada para consultar os eventos.
     * @param codigoTarefa - Objeto String. Será utilizado no formulário de histórico de eventos para servir de filtro quando necessário.
     * @param pagaEvento - boolean. Especifica se o pagamento do tático em questão é ou não efetuado por esta consulta.
     * @param cpt - Objeto GregorianCalendar. Representa a competência do pagamento e serve como filtro na consulta.
     * @return listaEventos - Objeto List contendo NeoObjects. É uma lista com os objetos que contém as informações dos eventos.
     * @throws NaoEncontradoException Caso a consulta não retorne nenhum evento no período especificado. P.S.: Apenas quando pagaEvento é true.
     * @throws Exception Para exceções generalizadas.
     * */
    public static List<NeoObject> preencheDadosEventos(Long codRota, String codigoTarefa, boolean pagaEvento, GregorianCalendar cpt) throws Exception, NaoEncontradoException {
	List<NeoObject> listaEventos = new ArrayList<>();
	Long codigoRota = codRota;
	String dataInicio = null;
	String dataFim = null;
	String tarefa = codigoTarefa;
	boolean pagaPorEvento = pagaEvento;

	// Controla o período de pagamento que será sempre referente ao mês anterior
	GregorianCalendar competencia  = (GregorianCalendar) cpt.clone();
	// Primeiro dia do mês
	dataInicio = NeoDateUtils.safeDateFormat(competencia, "yyyy-MM-dd HH:mm:ss");	    	    
	// Último dia do mês
	int ultimodia = competencia.getActualMaximum(Calendar.DATE);
	competencia.set(Calendar.DAY_OF_MONTH, ultimodia);
	competencia.set(Calendar.HOUR_OF_DAY, 23);
	competencia.set(Calendar.MINUTE, 59);
	competencia.set(Calendar.SECOND, 59);
	dataFim = NeoDateUtils.safeDateFormat(competencia, "yyyy-MM-dd HH:mm:ss");	    

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT ROTA.CD_ROTA AS 'CDROTA', ROTA.NM_ROTA AS 'NMROTA', VTR.CD_VIATURA AS 'CDVIATURA', VTR.NM_VIATURA AS 'NMVIATURA', ");
	sql.append(" VH.CD_EVENTO AS 'EVENTO', VH.DT_FECHAMENTO AS 'DTFECHAMENTO', VH.DT_RECEBIDO, CTRL.ID_CENTRAL, CTRL.ID_EMPRESA, ");
	sql.append(" CTRL.ID_CENTRAL AS 'CTRL', CTRL.CD_CLIENTE AS 'CDCLIENTE', CTRL.RAZAO ");
	sql.append(" FROM ROTA");
	sql.append(" INNER JOIN ASSOC_ROTA_VIATURA ASSOC ON ROTA.CD_ROTA = ASSOC.CD_ROTA ");
	sql.append(" INNER JOIN VIATURA VTR ON ASSOC.CD_VIATURA = VTR.CD_VIATURA ");
	sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.X_SIGMAROTA  AS XROTA ON XROTA.CD_ROTA = ROTA.CD_ROTA ");
	sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPAPLICACAOTATICO AS TATICO ON TATICO.ROTASIGMA_NEOID = XROTA.NEOID ");
	sql.append(" INNER JOIN VIEW_HISTORICO VH ON VH.CD_VIATURA = VTR.CD_VIATURA ");
	sql.append(" INNER JOIN dbCENTRAL AS CTRL ON CTRL.CD_CLIENTE = VH.CD_CLIENTE ");
	sql.append(" WHERE ROTA.NM_ROTA LIKE '%PARC%' ");
	sql.append(" AND VH.DT_ESPERA_DESLOCAMENTO IS NOT NULL  ");
	sql.append(" AND (VH.DT_VIATURA_NO_LOCAL IS NOT NULL OR VH.TX_OBSERVACAO_FECHAMENTO LIKE '##PagarFAP%')"); 
	sql.append(" AND TATICO.MODALIDADE_NEOID = 908025798 "); // NEOID DA MODALIDADE POR ATENDIMENTO
	sql.append(" AND VH.DT_FECHAMENTO BETWEEN ? AND ? ");
	sql.append(" AND ROTA.CD_ROTA = ? ");
	sql.append("  AND EXISTS ( ");
	sql.append(" 	         SELECT ID_EMPRESA, ID_CENTRAL, CD_CLIENTE FROM dbCENTRAL   ");                               
	sql.append(" 	         WHERE ID_EMPRESA = CTRL.ID_EMPRESA  ");                                                                   
	sql.append(" 	         AND ID_CENTRAL =  CTRL.ID_CENTRAL ");                                                                      
	sql.append(" 	         AND EXISTS ( ");
	sql.append(" 			    SELECT * FROM [FSOODB04\\SQL02].SAPIENS.DBO.usu_t160sig SIG  ");                           
	sql.append(" 			    INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.usu_t160cvs CVS  ");                               
	sql.append(" 			    ON SIG.usu_codemp = CVS.usu_codemp AND SIG.usu_codfil = CVS.usu_codfil   ");            
	sql.append(" 			    AND SIG.usu_numctr = CVS.usu_numctr AND SIG.usu_numpos = CVS.usu_numpos  ");                 
	sql.append(" 			    WHERE SIG.USU_CODCLI = CD_CLIENTE                ");                                        
	sql.append(" 			    AND ((CVS.usu_sitcvs = 'A') OR (CVS.usu_sitcvs = 'I' AND CVS.usu_datfim >= GETDATE())) ");
	sql.append(" 			    ) 	 "); 
	sql.append(" 	         ) ");
	sql.append(" ORDER BY ASSOC.CD_ROTA, ASSOC.CD_VIATURA ");

	try {	   
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

	    pstm.setString(1, dataInicio);
	    pstm.setString(2, dataFim);
	    pstm.setLong(3, codigoRota);
	    rs = pstm.executeQuery();

	    if(rs.next()){
		rs.beforeFirst();
		while(rs.next()){

		    NeoObject efEvento = AdapterUtils.getInstantiableEntityInfo("FAPHistoricoEventos").createNewInstance();
		    EntityWrapper ewEvento = new EntityWrapper(efEvento);

		    Long cdViatura = rs.getLong("CDVIATURA");
		    String nmViatura = rs.getString("NMVIATURA");
		    String evento = rs.getString("EVENTO");
		    GregorianCalendar dataRecebimento = new GregorianCalendar();
		    dataRecebimento.setTime(rs.getDate("DT_RECEBIDO"));
		    String idCentral = rs.getString("CTRL");
		    String razaoCliente = rs.getString("RAZAO");
		    String cdCliente = rs.getString("CDCLIENTE");
		    tarefa = codigoTarefa;

		    ewEvento.setValue("cdViatura", cdViatura);
		    ewEvento.setValue("nmViatura", nmViatura);
		    ewEvento.setValue("evento", evento);
		    ewEvento.setValue("dataRecebimento",dataRecebimento);
		    ewEvento.setValue("idCentral", idCentral);
		    ewEvento.setValue("cdCliente", cdCliente);
		    ewEvento.setValue("nomeCliente", razaoCliente);
		    ewEvento.setValue("codigoTarefa", tarefa);

		    PersistEngine.persist(efEvento);	
		    listaEventos.add(efEvento);			
		}		    
		return listaEventos;
	    } else {
		if(pagaPorEvento){
		    throw new NaoEncontradoException("Nenhum evento para a rota " + codigoRota + " no período informado.");
		} else {
		    return listaEventos;
		}
	    }	    	 	    
	} catch(NaoEncontradoException e) {
	    e.printStackTrace();
	    throw new NaoEncontradoException(e.getMessage());	    
	} catch(Exception e) {
	    e.printStackTrace();
	    throw new Exception("Erro inesperado ao preencher os dados obrigatórios. Favor contatar a TI.");	    
	} finally {
	    try {				
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}		
    }
   
    /**
     * Consulta utilizada para controlar os pagamentos por competência. Não é permitido abrir mais de um
     * processo de pagamento (FAP) para uma mesma rota em uma mesma competência.
     * @param cpt - GregorianCalendar. É a competência onde será avaliada a quantidade de FAP's abertas.
     * @param codRota - Long. Código da rota, também utilizado para avaliar a quantidade de FAP's abertas.
     * @param codTarefaAtual - String. Código da tarefa de onde o método foi chamado, a consulta deve ignorar a FAP atual.
     * @return qtdFapCompetencia - int. Retorna 1 caso encontre outra FAP além da atual. Retorna 0 caso não tenha nenhuma outra FAP.
     * @throws Exeception Para exceções generalizadas.
     * */
    public static int quantidadeFapPorCompetencia(GregorianCalendar cpt, Long codRota, String codTarefaAtual) throws Exception{
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();

	int qtdFapCompetencia = 0;
	String competenciaString = null;
	Long codigoRota = codRota;
	GregorianCalendar competencia = cpt;
	String codigoTarefaAtual = codTarefaAtual;

	boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);
	if(debug){	//Local	    
	    competenciaString = NeoDateUtils.safeDateFormat(competencia, "dd-MM-yyyy HH:mm:ss");
	} else { // Produção
	    competenciaString = NeoDateUtils.safeDateFormat(competencia, "yyyy-MM-dd HH:mm:ss");
	}

	try {
	    sql.append(" SELECT ROTA.CD_ROTA, FAPAUT.ROTASIGMA_NEOID,FAPAUT.TECNICOSIGMA_NEOID, FAPAUT.NEOID AS FAPNEOID, ");             
	    sql.append(" WF.CODE, FAPTAT.VALORROTA, WF.NEOID AS WFNEOID, WF.PROCESSSTATE,FAPAUT.DATASOLICITACAO           ");      
	    sql.append(" FROM WFPROCESS WF                                                                                ");        
	    sql.append(" INNER JOIN D_FAPAUTORIZACAODEPAGAMENTO FAPAUT ON FAPAUT.WFPROCESS_NEOID = WF.NEOID               ");         
	    sql.append(" INNER JOIN D_FAPAPLICACAOTATICO FAPTAT ON FAPAUT.ROTASIGMA_NEOID = FAPTAT.NEOID                  ");             
	    sql.append(" INNER JOIN X_SIGMAROTA ROTA ON FAPTAT.ROTASIGMA_NEOID = ROTA.NEOID                               ");                          
	    sql.append(" WHERE (WF.PROCESSSTATE = 0 OR WF.PROCESSSTATE = 1)                                               ");                   
	    sql.append(" AND ROTA.CD_ROTA = ?                                                                             ");                                           
	    sql.append(" AND FAPAUT.COMPETENCIAPAGAMENTO = ?                                                              ");
	    sql.append(" AND WF.CODE != ?                                                                                 "); 

	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setLong(1, codigoRota);
	    pstm.setString(2, competenciaString);
	    pstm.setString(3, codigoTarefaAtual);
	    rs = pstm.executeQuery();

	    if(rs.next()){		
		qtdFapCompetencia = 1;
	    }

	    return qtdFapCompetencia;
	} catch (Exception e){
	    e.printStackTrace();
	    throw e;
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		//DO NOTHING
	    }
	}
    }

    /**
     * Este método é utilizado em uma rotina que avalia se existem contas associadas em mais de uma rota. Como isto não é correto, inicia-se
     * uma tarefa simples para cada duplicidade encontrada.
     * @return contasEmMaisDeUmaRota - List contendo Strings com as informações de ID_EMPRESA + ID_CENTRAL das duplicidades.
     * @throws Exception Para exceções generalizadas.
     * */
    public static List<String> verificarContasEmMaisDeUmaRota() throws Exception {
	List<String> contasEmMaisDeUmaRota = new ArrayList<>();

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();
	
	sql.append(" SELECT CONVERT(VARCHAR(20),ID_EMPRESA)+'-'+CONVERT(VARCHAR(20),ID_CENTRAL) AS CONTA, COUNT(1) AS QTDE                   ");
	sql.append(" FROM                                                                                                                    ");
	sql.append(" (                                                                                                                       ");
	sql.append("   SELECT DISTINCT R.CD_ROTA,                                                                                            ");
	sql.append("                ID_EMPRESA,                                                                                              ");
	sql.append("                ID_CENTRAL                                                                                               ");
	sql.append("     FROM DBCENTRAL C                                                                                                    ");
	sql.append("   INNER JOIN ROTA R ON C.ID_ROTA = R.CD_ROTA                                                                            ");
	sql.append("   INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.X_SIGMAROTA  AS XROTA ON XROTA.CD_ROTA = R.CD_ROTA                     ");
	sql.append("   INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPAPLICACAOTATICO AS TATICO ON TATICO.ROTASIGMA_NEOID = XROTA.NEOID ");
	sql.append("     WHERE FG_ATIVO = 1 AND C.CTRL_CENTRAL = 1                                                                           ");
	sql.append("       AND C.ID_CENTRAL NOT LIKE 'AAA%' AND C.ID_CENTRAL NOT LIKE 'DDDD'                                                 ");
	sql.append("       AND C.ID_CENTRAL NOT LIKE 'FFFF' AND C.ID_CENTRAL NOT LIKE 'R%'                                                   ");
	sql.append(" ) AS TABELA1                                                                                                            ");
	sql.append("   GROUP BY CONVERT(VARCHAR(20),ID_EMPRESA)+'-'+CONVERT(VARCHAR(20),ID_CENTRAL)                                          ");
	sql.append("    HAVING COUNT(1) > 1                                                                                                  ");

	try {	    
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    rs = pstm.executeQuery();

	    if(rs.next()){
		rs.beforeFirst();
		while(rs.next()){
		   contasEmMaisDeUmaRota.add(rs.getString("CONTA"));
		}

	    } else {
		contasEmMaisDeUmaRota = null;
	    }

	    return contasEmMaisDeUmaRota;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}


    }
}
