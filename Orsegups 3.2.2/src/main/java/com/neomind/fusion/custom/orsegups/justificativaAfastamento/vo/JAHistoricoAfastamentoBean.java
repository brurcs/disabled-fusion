package com.neomind.fusion.custom.orsegups.justificativaAfastamento.vo;

import java.util.Calendar;

/**
 * Modelo da entidade de historico de afastamentos do fluxo de afastamentos - J003
 * @author mateus.batista
 *
 */
public class JAHistoricoAfastamentoBean extends JAHistoricoBean{
    
    private Calendar dataInicio;
    private Calendar dataTermino;
    private String descricao;
    private int situacao;
    
    public Calendar getDataInicio() {
        return dataInicio;
    }
    public void setDataInicio(Calendar dataInicio) {
        this.dataInicio = dataInicio;
    }
    public Calendar getDataTermino() {
        return dataTermino;
    }
    public void setDataTermino(Calendar dataTermino) {
        this.dataTermino = dataTermino;
    }
    public int getSituacao() {
        return situacao;
    }
    public void setSituacao(int situacao) {
        this.situacao = situacao;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public JAHistoricoAfastamentoBean(){
	super();
    }
    
}
