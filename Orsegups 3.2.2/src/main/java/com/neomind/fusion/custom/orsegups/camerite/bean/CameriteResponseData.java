package com.neomind.fusion.custom.orsegups.camerite.bean;

import java.io.Serializable;

public class CameriteResponseData implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private String record;
    private String shot;
    private String errorMessage;
    
    public String getRecord() {
        return record;
    }
    public void setRecord(String record) {
        this.record = record;
    }
    public String getShot() {
        return shot;
    }
    public void setShot(String shot) {
        this.shot = shot;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    

}
