package com.neomind.fusion.custom.orsegups.site;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.UserWebServiceAuthentication;
import com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet;
import com.neomind.fusion.custom.orsegups.presenca.vo.AcessoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.AfastamentoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContraChequeVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EventoSigmaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoEscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ReclamacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.SolicitacaoVO;
import com.neomind.fusion.custom.orsegups.rsc.helper.RSCHelper;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.site.vo.CompetenciaVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteClienteVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteCompetenciaDocumentoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteCompetenciaVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteContaSigmaVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteDocsMostrarVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteDocumentoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteEmpresaDocumentoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteEmpresaVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteGraficoOrdemServicoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteGraficoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteNfvVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteOrdemServicoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteReclamacaoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteSolicitacaoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteTipoDocumentoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaSupervisaoUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

@WebServlet(name = "AreaClienteServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.site.AreaClienteServlet" })
public class AreaClienteServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(OrsegupsMapsServlet.class);
	private static final String LINK_EVENTO = "http://www.webalarme.com.br/acessoControlador.php?";
	private static final String COD_CLIENTE_EVENTO = "054";
	private static final String BASEPATH_BKP01 = "\\\\fsoofs01\\f$\\Site";
	private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	public void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException
	{
		response.setContentType("text/html");
		response.setCharacterEncoding("ISO-8859-1");

		PrintWriter out = null;
		try
		{
			String action = "";
			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
			}

			if (!action.equalsIgnoreCase("carregaFotoColaborador") && !action.equalsIgnoreCase("ImprimirFichasPonto"))
			{ // ao carregar a foto preciso obter o outputStream do servlet que não pode ser obtido se o printwriter já tiver sido obtido 
				out = response.getWriter();
			}

			if (action.equalsIgnoreCase("login"))
			{
				this.login(request, response, out);
			}

			if (action.equalsIgnoreCase("logout"))
			{
				this.logout(request, response, out);
			}

			/*
			 * if (action.equalsIgnoreCase("gerarGraficos"))
			 * {
			 * this.gerarGraficos(request, response, out);
			 * }
			 */

			if (action.equalsIgnoreCase("listarNfs"))
			{
				this.listarNfsBoletos(request, response, out);
			}

			if (action.equalsIgnoreCase("visualizarDocumento"))
			{
				this.visualizarDocumento(request, response, out);
			}

			if (action.equalsIgnoreCase("alterarSenha"))
			{
				this.alterarSenha(request, response, out);
			}

			if (action.equalsIgnoreCase("salvarContato"))
			{
				this.salvarContato(request, response, out);
			}
			
			if (action.equalsIgnoreCase("iniciarSolicitacaoViaMobile"))
			{
				this.iniciarSolicitacaoViaMobile(request, response, out);
			}

			if (action.equalsIgnoreCase("listarContasSigma"))
			{
				this.listarContasSigma(request, response, out);
			}

			if (action.equalsIgnoreCase("listarOrdemServico"))
			{
				this.listarOrdemServico(request, response, out);
			}

			if (action.equalsIgnoreCase("recuperarSenha"))
			{
				this.recuperarSenha(request, response, out);
			}
			if (action.equalsIgnoreCase("listarDocumentos"))
			{
				this.listarDocumentos(request, response, out);
			}
			if (action.equalsIgnoreCase("checkAceiteNfDoc"))
			{
				this.checkAceiteNfDoc(request, response, out);
			}
			if (action.equalsIgnoreCase("visualizarDocumentosCliente"))
			{
				this.visualizarDocumentosCliente(request, response, out);
			}
			if (action.equalsIgnoreCase("solicitacaoCliente"))
			{
				this.listaSolicitacao(request, response, out);
			}
			if (action.equalsIgnoreCase("reclamacaoCliente"))
			{
				this.listaReclamacao(request, response, out);
			}
			if (action.equalsIgnoreCase("listarPresenca"))
			{
				this.listarPresenca(request, response, out);
			}
			if (action.equalsIgnoreCase("listaDadosGeraisPosto"))
			{
				this.listaDadosGeraisPosto(request, response, out);
			}
			if (action.equalsIgnoreCase("listaEventoX8"))
			{ // supervisoes

				String numpos = request.getParameter("numposto");
				String numctr = request.getParameter("numctr");
				this.listaEventoX8(numctr, numpos, response);

			}
			if (action.equalsIgnoreCase("listaCoberturaPosto"))
			{
				String numloc = request.getParameter("numloc");
				String taborg = request.getParameter("taborg");
				this.listaCoberturaPosto(numloc, taborg, response);
			}

			if (action.equalsIgnoreCase("listaSolicitacaoFichaPosto"))
			{
				String numloc = request.getParameter("numloc");
				String taborg = request.getParameter("taborg");
				Long codcli = SiteUtils.retornaCodigoClienteSapiens(NeoUtils.safeLong(numloc), NeoUtils.safeLong(taborg));
				this.listaSolicitacaoFichaPosto(codcli, response);
			}
			if (action.equalsIgnoreCase("listaReclamacaoFichaPosto"))
			{
				String numloc = request.getParameter("numloc");
				String taborg = request.getParameter("taborg");
				Long codcli = SiteUtils.retornaCodigoClienteSapiens(NeoUtils.safeLong(numloc), NeoUtils.safeLong(taborg));
				this.listaReclamacaoFichaPosto(codcli, response);
			}

			if (action.equalsIgnoreCase("listaDadosGeraisColaborador"))
			{
				String numcad = request.getParameter("numcad");
				String tipcol = request.getParameter("tipcol");
				String numemp = request.getParameter("numemp");
				listaDadosGeraisColaborador(numcad, numemp, tipcol, response);
			}
			if (action.equalsIgnoreCase("carregaFotoColaborador"))
			{
				String numcad = request.getParameter("numcad");
				String tipcol = request.getParameter("tipcol");
				String numemp = request.getParameter("numemp");
				carregaFotoColaborador(numcad, numemp, tipcol, response);
			}
			if (action.equalsIgnoreCase("listaAfastamentos"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String numloc = request.getParameter("numloc");
				this.listaAfastamentos(numemp, tipcol, numcad, numloc, response);
			}
			if (action.equalsIgnoreCase("listaCoberturas"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String numloc = request.getParameter("numloc");
				this.listaCoberturas(numemp, tipcol, numcad, numloc, response);
			}
			if (action.equalsIgnoreCase("listaHistoricoEscala"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String numloc = request.getParameter("numloc");
				this.listaHistoricoEscala(numemp, tipcol, numcad, numloc, response);
			}
			if (action.equalsIgnoreCase("listaHistoricoLocal"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String numloc = request.getParameter("numloc");
				this.listaHistoricoLocal(numemp, tipcol, numcad, numloc, response);
			}
			if (action.equalsIgnoreCase("listaAcessos"))
			{
				String numcpf = request.getParameter("numcpf");
				String competencia = request.getParameter("cpt");
				String exibirExcecao = request.getParameter("exibirExcecao");
				try
				{
					this.listaAcessos(numcpf, competencia, exibirExcecao, response);
				}
				catch (ParseException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (action.equalsIgnoreCase("listaCompetencias"))
			{
				this.listaCompetencias(response);
			}
			if (action.equalsIgnoreCase("buscaContraCheque"))
			{
				String numcpf = request.getParameter("numcpf");
				String competencia = request.getParameter("cpt");
				try
				{
					this.buscaContraCheque(numcpf, competencia, response);
				}
				catch (ParseException e)
				{
					e.printStackTrace();
				}
			}
			if (action.equalsIgnoreCase("listaCompetenciasFinancas"))
			{
				String numcpf = request.getParameter("numcpf");
				this.listaCompetenciasFinancas(numcpf, response);
			}
			if (action.equalsIgnoreCase("alterarAceiteNFDOC"))
			{

				String numcpf = request.getParameter("numcpf");
				String codcli = request.getParameter("codcli");
				System.out.println("1- " + (NeoUtils.safeOutputString(request.getParameter("chk_deacordo_recnf")).equals("") ? false : true));
				//System.out.println("2- " + (NeoUtils.safeOutputString(request.getParameter("chk_deacordo_recdoc")).equals("")? false:true) );
				Boolean chk_deacordo_recnf = NeoUtils.safeOutputString(request.getParameter("chk_deacordo_recnf")).equals("") ? false : true;
				//Boolean chk_deacordo_recdoc = NeoUtils.safeOutputString(request.getParameter("chk_deacordo_recdoc")).equals("")? false:true;
				String email = request.getParameter("email");
				this.alterarAceiteNFDOC(codcli, numcpf, email, chk_deacordo_recnf, false, out);
			}
			if (action.equalsIgnoreCase("ImprimirFichasPonto"))
			{

				String codcli = NeoUtils.safeOutputString(request.getParameter("codcli"));
				String numemp = NeoUtils.safeOutputString(request.getParameter("numemp"));
				String tipcol = NeoUtils.safeOutputString(request.getParameter("tipcol"));
				String numcad = NeoUtils.safeOutputString(request.getParameter("numcad"));
				String perref = NeoUtils.safeOutputString(request.getParameter("perref"));

				this.imprimirFichasPonto(NeoUtils.safeLong(codcli), NeoUtils.safeBoolean(numemp), NeoUtils.safeBoolean(numcad), NeoUtils.safeBoolean(tipcol), perref, response);

			}

			if (action.equalsIgnoreCase("listaSupervisorPosto"))
			{

				GregorianCalendar dataAtual = new GregorianCalendar();
				Long numemp = Long.parseLong(NeoUtils.safeOutputString(request.getParameter("numemp")));
				Long tipcol = Long.parseLong(NeoUtils.safeOutputString(request.getParameter("tipcol")));
				Long numcad = Long.parseLong(NeoUtils.safeOutputString(request.getParameter("numcad")));
				Long codccu = Long.parseLong(NeoUtils.safeOutputString(request.getParameter("codccu")));

				ColaboradorVO supervisor = new ColaboradorVO();

				supervisor = QLPresencaSupervisaoUtils.retornaSupervisorPosto(numemp, numcad, tipcol, codccu);
				supervisor = QLPresencaUtils.buscaFichaColaborador(supervisor.getNumeroEmpresa(), 1L, supervisor.getNumeroCadastro(), dataAtual);

				this.listaSupervisor(supervisor, response);
			}

		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void login(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{

		//Validar autentificacao MD5
		String key = request.getParameter("key");

		UserWebServiceAuthentication auth = new UserWebServiceAuthentication();
		if (!auth.authentication(key))
		{
			JSONObject jsonAuth = new JSONObject();

			jsonAuth.append("status", 0L);
			jsonAuth.append("msg", "Erro de autenticação");

			out.print(jsonAuth);
			return;
		}

		String cpf = request.getParameter("user");
		String pass = request.getParameter("pass");

		cpf = cpf.replaceAll("[^0-9]", "");

		if (cpf == null || cpf.trim().equals("") || pass == null || pass.trim().equals(""))
		{
			JSONObject jsonLogin = new JSONObject();

			jsonLogin.append("status", 0L);
			jsonLogin.append("msg", "Login/Senha obrigatório");

			out.print(jsonLogin);
			return;
		}
		else
		{
			Long cpfLong = null;
			try
			{
				cpfLong = Long.parseLong(cpf.trim());

				String senhaMaster = null;
				List<NeoObject> usr = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("siteConfigSenhaMaster"), null);
				if (usr != null && !usr.isEmpty())
				{
					EntityWrapper wrapperSenhaMaster = new EntityWrapper(usr.get(0));
					senhaMaster = (String) wrapperSenhaMaster.getValue("senha");
				}

				QLGroupFilter filterUser = new QLGroupFilter("AND");
				filterUser.addFilter(new QLEqualsFilter("usu_cgccpf", cpfLong));
				if (senhaMaster == null || !senhaMaster.equals(pass))
				{
					filterUser.addFilter(new QLEqualsFilter("usu_senusu", pass));
				}
				filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));
				//View de Contas
				List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);

				if ((users != null && !users.isEmpty()) || senhaMaster.equals(pass))
				{
					EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
					Long codcli = (Long) usuarioCliente.getValue("usu_codcli");
					String noncli = (String) usuarioCliente.getValue("usu_nomusu");

					QLGroupFilter filterCliente = new QLGroupFilter("AND");
					filterCliente.addFilter(new QLEqualsFilter("cd_cliente", codcli));
					filterCliente.addFilter(new QLEqualsFilter("ctrl_central", true));

					String newKey = codcli.toString() + new GregorianCalendar().getTimeInMillis();
					GregorianCalendar dataSession = new GregorianCalendar();

					dataSession.add(Calendar.HOUR_OF_DAY, +1);

					MessageDigest md = MessageDigest.getInstance("MD5");
					md.update(newKey.getBytes());
					byte[] hashMd5 = md.digest();

					String txtMd5 = OrsegupsUtils.stringHexa(hashMd5);

					QLEqualsFilter filtroSession = new QLEqualsFilter("codigo", cpfLong);
					List<NeoObject> sessions = PersistEngine.getObjects(AdapterUtils.getEntityClass("UsuarioWebServiceSession"), filtroSession);

					/*
					 * Excluir Sessao
					 * if (sessions != null && !sessions.isEmpty())
					 * {
					 * PersistEngine.removeObjects(sessions);
					 * }
					 */

					NeoObject session = AdapterUtils.createNewEntityInstance("UsuarioWebServiceSession");

					if (session != null)
					{
						EntityWrapper sessionWrapper = new EntityWrapper(session);
						sessionWrapper.setValue("key", txtMd5);
						sessionWrapper.setValue("time", dataSession);
						sessionWrapper.setValue("codigo", cpfLong);
						sessionWrapper.setValue("ip", request.getRemoteAddr());
						QLEqualsFilter filtroService = new QLEqualsFilter("codigo", 1L);
						NeoObject sessionsServico = PersistEngine.getObject(AdapterUtils.getEntityClass("UsuarioWebServiceServico"), filtroService);
						sessionWrapper.setValue("servico", sessionsServico);

						PersistEngine.persist(session);
					}

					String cpfMask = "";

					QLGroupFilter filterCli = new QLGroupFilter("AND");
					filterCli.addFilter(new QLEqualsFilter("codcli", codcli));

					//Formatar CPF/CNPJ
					List<NeoObject> clientes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCli);
					if (clientes != null && !clientes.isEmpty())
					{
						EntityWrapper clienteWrapper = new EntityWrapper(clientes.get(0));
						String tipcli = (String) clienteWrapper.getValue("tipcli");

						if (tipcli.equals("F"))
						{
							cpfMask = String.format("%011d", cpfLong);
							cpfMask = OrsegupsUtils.formatarString(cpfMask, "###.###.###-##");
						}
						else
						{
							cpfMask = String.format("%014d", cpfLong);
							cpfMask = OrsegupsUtils.formatarString(cpfMask, "##.###.###/####-##");
						}
					}

					JSONObject jsonCliente = new JSONObject();

					jsonCliente.append("status", 1L);
					jsonCliente.append("nomcli", noncli);
					jsonCliente.append("cgccpf", cpfMask);
					jsonCliente.append("codigo", codcli.toString());
					jsonCliente.append("sessionId", txtMd5);

					//Gson gson = new Gson();
					//String userJSON = gson.toJson(user.get(0));

					out.print(jsonCliente);

				}
				else
				{
					JSONObject jsonUser = new JSONObject();

					jsonUser.append("status", 0L);
					jsonUser.append("msg", "Usuário/Senha Inválidos");
					out.print(jsonUser);
					return;
				}
			}
			catch (NumberFormatException e)
			{
				e.printStackTrace();
			}
			catch (NoSuchAlgorithmException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (ParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void logout(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException, ServletException, IOException
	{
		//verifaca id sessao
		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();

		//buscar Sesssao
		NeoObject session = siteUtils.getSession(sessionId);
		JSONObject jsonCliente = new JSONObject();

		if (session != null)
		{
			//remove sessao
			PersistEngine.remove(session);
			jsonCliente.append("status", 1L);
			jsonCliente.append("msg", "Logout efetuado com sucesso.");
		}
		else
		{
			jsonCliente.append("status", 2L);
			jsonCliente.append("msg", "Sessão inexistente.");
		}
		out.print(jsonCliente);
	}

	private void listarNfsBoletos(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		String intnetCliente = null;
		Boolean chkDeacordoRecnf = false;
		Boolean chkDeacordoRecdoc = false;

		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();

		//buscar Sesssao
		NeoObject session = siteUtils.getSession(sessionId);

		if (session == null)
		{
			JSONObject jsonNotas = new JSONObject();

			jsonNotas.append("status", 2L);
			jsonNotas.append("msg", "Sessão inexistente");
			out.print(jsonNotas);
			return;
		}
		EntityWrapper sessionWrapper = new EntityWrapper(session);
		Long cpf = (Long) sessionWrapper.getValue("codigo");

		QLGroupFilter filterUser = new QLGroupFilter("AND");
		filterUser.addFilter(new QLEqualsFilter("usu_cgccpf", cpf));
		filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));

		//Busca Cliente
		List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);

		Long codcli = 0L;

		if (users != null)
		{
			EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
			codcli = (Long) usuarioCliente.getValue("usu_codcli");
		}
		else
		{
			JSONObject jsonUser = new JSONObject();

			jsonUser.append("status", 0L);
			jsonUser.append("msg", "Não foi possível carregar suas NFs e Boletos. Entre em contato com a nossa Central de Relacionamento");
			out.print(jsonUser);
			return;
		}

		if (codcli == 0L)
		{
			JSONObject jsonUser = new JSONObject();

			jsonUser.append("status", 0L);
			jsonUser.append("msg", "Não foi possível carregar suas NFs e Boletos. Entre em contato com a nossa Central de Relacionamento");
			out.print(jsonUser);
			return;
		}

		Connection connSapiens = PersistEngine.getConnection("SAPIENS");
		PreparedStatement stNfvVO = null;
		ResultSet rsNfvVO = null;
		SiteCompetenciaVO competenciaVO = null;
		SiteEmpresaVO empresaVO = null;
		SiteNfvVO nfvVO = null;
		Gson gson = new Gson();
		JSONObject json = new JSONObject();

		try
		{
			/*** Pesquisa Notas / Cliente ultimos 6 meses ***/

			intnetCliente = SiteUtils.recuperaIntnetcliente(codcli);
			Boolean chkNfDoc[] = SiteUtils.recuperaAceiteNfDoc(codcli);
			chkDeacordoRecnf = chkNfDoc[0];
			chkDeacordoRecdoc = chkNfDoc[1];
			System.out.println("[Area Cliente ] NF- intnet cliente[" + codcli + "], chkDeacordoRecnf[" + chkDeacordoRecnf + "], chkDeacordoRecdoc[" + chkDeacordoRecdoc + "]");

			StringBuffer queryNotas = new StringBuffer();
			queryNotas.append("  SELECT N.codemp,N.codfil,N.numcgc,N.numdfs,N.codver,N.numnfv,N.datemi,N.codsnf,N.vlrbse,N.usu_lnknfe,N.cptfat,N.codfpg,N.sittit ");
			queryNotas.append("  FROM [FSOODB04\\sql02].SAPIENS.dbo.USU_V140NFVWEB AS N WHERE CODCLI = ? AND CPTFAT >= DATEADD (Month, -6, GETDATE () )  ORDER BY CODEMP ASC,CPTFAT DESC ");
			stNfvVO = connSapiens.prepareStatement(queryNotas.toString());
			stNfvVO.setLong(1, codcli);

			rsNfvVO = stNfvVO.executeQuery();
			List<SiteNfvVO> listNfvVO = null;
			List<SiteCompetenciaVO> listaCompetencia = new ArrayList<SiteCompetenciaVO>();
			SiteCompetenciaVO competenciaVO2 = new SiteCompetenciaVO();
			GregorianCalendar date = new GregorianCalendar();
			String cptAnt = "";
			Long codEmpAnt = 0L;
			int i = 0;
			listNfvVO = new ArrayList<SiteNfvVO>();

			competenciaVO2.setIntnet(intnetCliente);
			competenciaVO2.setChkDeacordoRecnf(chkDeacordoRecnf);
			competenciaVO2.setChkDeacordoRecdoc(chkDeacordoRecdoc);

			while (rsNfvVO.next())
			{

				competenciaVO = new SiteCompetenciaVO();
				empresaVO = new SiteEmpresaVO();
				nfvVO = new SiteNfvVO();
				nfvVO.setNumDfs(rsNfvVO.getLong("numdfs"));
				nfvVO.setRps(rsNfvVO.getLong("numnfv"));
				nfvVO.setEmissao((String) NeoUtils.safeDateFormat(rsNfvVO.getTimestamp("datemi"), "MM/yyyy"));
				nfvVO.setSerie(rsNfvVO.getString("codsnf"));
				nfvVO.setValor(rsNfvVO.getBigDecimal("vlrbse"));
				nfvVO.setLinkNfse(rsNfvVO.getString("usu_lnknfe"));
				nfvVO.setCodFil(rsNfvVO.getLong("codfil"));
				nfvVO.setCodFpg(rsNfvVO.getLong("codfpg"));
				nfvVO.setSitTit(rsNfvVO.getString("sittit"));
				nfvVO.setCodver(rsNfvVO.getString("codver"));
				nfvVO.setNumCgc(rsNfvVO.getString("numcgc"));
				nfvVO.setCodEmp(rsNfvVO.getLong("codemp"));
				Long codemp = rsNfvVO.getLong("codemp");
				Long codfil = rsNfvVO.getLong("codfil");

				// regra para mostrar ou esconder o tooltip
				if (codemp == 1 || codemp == 2 || codemp == 6 || codemp == 7 || codemp == 8 || codemp == 15 || (codemp == 18 && codfil == 1) || (codemp == 21 ) ||
						codemp == 27 || codemp == 28 ||codemp == 29)
				{
					nfvVO.setMostraToolTip(1L);
				}
				else
				{
					nfvVO.setMostraToolTip(0L);
				}

				boolean mostraBoleto = (nfvVO.getCodFpg() == 1 && (nfvVO.getSitTit().equals("AB"))  /*&& (codemp == 19L)*/);
				String fileDARFPCC = BASEPATH_BKP01 + "\\Matadeiro\\Demonstrativo\\DARF_PCC" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil() + nfvVO.getRps() + nfvVO.getSerie() + ".pdf";
				String fileDARFIRRF = BASEPATH_BKP01 + "\\Matadeiro\\Demonstrativo\\DARF_IRRF" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil() + nfvVO.getRps() + nfvVO.getSerie() + ".pdf";
				String fileGPS = BASEPATH_BKP01 + "\\Matadeiro\\Demonstrativo\\GPS" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil() + nfvVO.getRps() + nfvVO.getSerie() + ".pdf";

				String fileBoleto = "\\\\MATADEIRO\\nfse$\\Boleto\\BOLETO" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil() + nfvVO.getRps() + nfvVO.getSerie() + ".pdf";
				String fileRPS = "";
				String fileRPSNew = "";

				fileRPS = "\\\\MATADEIRO\\nfse$\\Demonstrativo\\DEMONSTRATIVO" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil().toString() + nfvVO.getRps().toString() + nfvVO.getSerie().toString() + ".pdf";
				fileRPSNew = BASEPATH_BKP01 + "\\Matadeiro\\Demonstrativo\\DEMONSTRATIVO" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil().toString() + nfvVO.getRps().toString() + nfvVO.getSerie().toString() + ".pdf";
				//				}
				String fileDonwloaNFSE = BASEPATH_BKP01 + "\\Matadeiro\\NFSe\\NFSe" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil().toString() + nfvVO.getRps().toString() + nfvVO.getSerie().toString() + ".pdf";
				System.out.println("NFSe" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil().toString() + nfvVO.getRps().toString() + nfvVO.getSerie().toString() + ".pdf");
				System.out.println(nfvVO);

				//fileDonwloaNFSE = "X:\\Site\\Matadeiro\\NFSe\\NFSe7111212U01.pdf";

				boolean mostraMensagem = ((nfvVO.getSitTit().equals("LQ")));

				/*
				 * File fb = new File(fileBoleto);
				 * File fr = new File(fileRPS);
				 * File frn = new File(fileRPSNew);
				 * File fdnfse = new File(fileDonwloaNFSE);
				 * File fDarfPcc = new File(fileDARFPCC);
				 * File fDarfIrrf = new File(fileDARFIRRF);
				 * File fGps = new File(fileGPS);
				 * boolean existeArqBol = fb.exists();
				 * boolean existeArqRPS = fr.exists();
				 * boolean existeArqRPSNew = frn.exists();
				 * boolean existeArqDownloadNFSE = fdnfse.exists();
				 * boolean existeArqDARFPCC = fDarfPcc.exists();
				 * boolean existeArqDARFIRRF = fDarfIrrf.exists();
				 * boolean existeGPS = fGps.exists();
				 */

				//if (existeArqDownloadNFSE){
				nfvVO.setLinkNfseDownload("NFSe" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil().toString() + nfvVO.getRps().toString() + nfvVO.getSerie().toString() + ".pdf");
				//nfvVO.setLinkNfseDownload("NFSe7111212U01.pdf");
				//}

				if (codemp != 1 && codemp != 2 && codemp != 6 && codemp != 8)
				{

					//if (existeArqDARFPCC){
					nfvVO.setLinkDarfPcc("DARF_PCC" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil() + nfvVO.getRps() + nfvVO.getSerie() + ".pdf");
					//}

					//if (existeArqDARFIRRF){
					nfvVO.setLinkDarfIRRF("DARF_IRRF" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil() + nfvVO.getRps() + nfvVO.getSerie() + ".pdf");
					//}
					//if(existeGPS){
					nfvVO.setLinkGPS("GPS" + rsNfvVO.getLong("codemp") + nfvVO.getCodFil() + nfvVO.getRps() + nfvVO.getSerie() + ".pdf");
					//}

				}

				if (mostraBoleto)
				{
						nfvVO.setLinkBoleto("rps");
				}		
				else
					nfvVO.setLinkBoleto("Pago");

				empresaVO.setCodigoEmpresa(rsNfvVO.getLong("codemp"));
				empresaVO.setCodigoFilial(rsNfvVO.getLong("codfil"));

				if (cptAnt.equals("") || cptAnt.equals((String) NeoUtils.safeDateFormat(rsNfvVO.getTimestamp("cptfat"), "MM/yyyy")) && codemp == codEmpAnt)
				{
					competenciaVO2.getNfv().add(nfvVO);
					NeoObject empresaObj = (NeoObject) PersistEngine.getObject(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("EEMP")).getEntityClass(), new QLEqualsFilter("codemp", empresaVO.getCodigoEmpresa()));
					EntityWrapper empresaWrapper = new EntityWrapper(empresaObj);
					empresaVO.setNomeEmpresa((String) empresaWrapper.findValue("nomemp"));
					competenciaVO2.setEmpresa(empresaVO);
					competenciaVO2.setCompetencia((String) NeoUtils.safeDateFormat(rsNfvVO.getTimestamp("cptfat"), "MM/yyyy"));

				}
				else
				{

					listaCompetencia.add(competenciaVO2);
					NeoObject empresaObj = (NeoObject) PersistEngine.getObject(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("EEMP")).getEntityClass(), new QLEqualsFilter("codemp", empresaVO.getCodigoEmpresa()));
					EntityWrapper empresaWrapper = new EntityWrapper(empresaObj);
					empresaVO.setNomeEmpresa((String) empresaWrapper.findValue("nomemp"));
					competenciaVO2 = new SiteCompetenciaVO();
					competenciaVO2.getNfv().add(nfvVO);
					competenciaVO2.setEmpresa(empresaVO);
					competenciaVO2.setCompetencia((String) NeoUtils.safeDateFormat(rsNfvVO.getTimestamp("cptfat"), "MM/yyyy"));
				}

				cptAnt = (String) NeoUtils.safeDateFormat(rsNfvVO.getTimestamp("cptfat"), "MM/yyyy");
				codEmpAnt = codemp;
			}
			if (competenciaVO2 != null)
			{
				listaCompetencia.add(competenciaVO2);
			}
			if ((listaCompetencia != null) && (!listaCompetencia.isEmpty()))
			{
				String competenciaJson = gson.toJson(listaCompetencia);
				System.out.println(competenciaJson);
				out.print(competenciaJson);
			}
			else
			{
				json.append("status", 0L);
				json.append("msg", "Não foi possível carregar suas NFs e Boletos. Entre em contato com a nossa Central de Relacionamento");
				System.out.println(json);
				out.print(json);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			json.append("status", 0L);
			json.append("msg", "Não foi possível carregar suas NFs e Boletos. Entre em contato com a nossa Central de Relacionamento");
			System.out.println(json);
			out.print(json);
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connSapiens, stNfvVO, rsNfvVO);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				json.append("status", 0L);
				json.append("msg", "Não foi possível carregar suas NFs e Boletos. Entre em contato com a nossa Central de Relacionamento");
				System.out.println(json);
				out.print(json);

			}
		}
	}

	private void visualizarDocumento(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws ServletException, IOException, JSONException
	{

		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();

		//buscar Sesssao
		NeoObject session = siteUtils.getSession(sessionId);

		if (session == null)
		{
			JSONObject jsonCliente = new JSONObject();

			jsonCliente.append("status", 2L);
			jsonCliente.append("msg", "Sessão inexistente");
			out.print(jsonCliente);
			return;
		}

		EntityWrapper sessionWrapper = new EntityWrapper(session);
		Long codcli = (Long) sessionWrapper.getValue("codigo");

		String action = NeoUtils.safeOutputString(request.getParameter("action")).trim();
		String tipDoc = NeoUtils.safeOutputString(request.getParameter("tipdoc")).trim();
		String codEmp = NeoUtils.safeOutputString(request.getParameter("codemp")).trim();
		String codFil = NeoUtils.safeOutputString(request.getParameter("codfil")).trim();
		String codSnf = NeoUtils.safeOutputString(request.getParameter("codsnf")).trim();
		String numNfv = NeoUtils.safeOutputString(request.getParameter("numnfv")).trim();
		String nomeArq = NeoUtils.safeOutputString(request.getParameter("nomeArq")).trim();

		if (LinkActionsEnum.BOLETO.getDescricao().equals(tipDoc) || LinkActionsEnum.RPS.getDescricao().equals(tipDoc) || LinkActionsEnum.NFSE.getDescricao().equals(tipDoc) || LinkActionsEnum.XML.getDescricao().equals(tipDoc))
		{
			validateParameters(action, tipDoc, codEmp, codFil, numNfv, codSnf);
		}
		else if (LinkActionsEnum.NFSED.getDescricao().equals(tipDoc) || LinkActionsEnum.DARFPCC.getDescricao().equals(tipDoc) || LinkActionsEnum.DARFIRRF.getDescricao().equals(tipDoc) || LinkActionsEnum.GPS.getDescricao().equals(tipDoc))
		{ // download de NFSE
			String file = BASEPATH_BKP01 + "\\Matadeiro\\NFSe\\" + nomeArq;

			if (LinkActionsEnum.DARFPCC.getDescricao().equals(tipDoc) || LinkActionsEnum.DARFIRRF.getDescricao().equals(tipDoc) || LinkActionsEnum.GPS.getDescricao().equals(tipDoc))
			{
				file = BASEPATH_BKP01 + "\\Matadeiro\\Demonstrativo\\" + nomeArq;
			}

			FileInputStream fileInputStream = null;
			try
			{
				response.setContentType("APPLICATION/OCTET-STREAM");

				String disHeader = "Attachment;Filename=\"" + nomeArq + "\"";
				response.setHeader("Content-Disposition", disHeader);
				File fileToDownload = new File(file);

				boolean existefile = fileToDownload.exists();

				if (existefile)
				{
					fileInputStream = new FileInputStream(fileToDownload);
				}
				else
				{
					JSONObject jsonCliente = new JSONObject();
					jsonCliente.append("status", 0L);
					jsonCliente.append("msg", "Arquivo " + nomeArq + " não encontrato.");
					out.print(jsonCliente);
					return;
				}

				int i;
				while ((i = fileInputStream.read()) != -1)
				{
					out.write(i);
				}
				fileInputStream.close();
				out.close();
			}
			catch (Exception e)
			{ // file IO error
				e.printStackTrace();
				JSONObject jsonCliente = new JSONObject();
				jsonCliente.append("status", 0L);
				jsonCliente.append("msg", "Erro ao efetuar download.");
				out.print(jsonCliente);
			}
			return;
		}
		else
		{
			JSONObject jsonCliente = new JSONObject();

			jsonCliente.append("status", 0L);
			jsonCliente.append("msg", "Parâmetro inválido");
			out.print(jsonCliente);
			return;
		}

		FileInputStream fileInputStream = null;
		QLGroupFilter filterCli = new QLGroupFilter("AND");
		filterCli.addFilter(new QLEqualsFilter("codemp", Long.valueOf(codEmp)));
		filterCli.addFilter(new QLEqualsFilter("codfil", Long.valueOf(codFil)));
		filterCli.addFilter(new QLEqualsFilter("codsnf", codSnf));
		filterCli.addFilter(new QLEqualsFilter("numnfv", Long.valueOf(numNfv)));

		//System.out.println(codEmp + "|" + codFil + "|" +codSnf + "|" + numNfv+ "|" +cgcCpf + "|" + tipDoc);
		GregorianCalendar datEmi = null;
		String codSel = "";
		String dirNes = "";
		Long numCgc = 0L;
		Long numDfs = 0L;

		ExternalEntityInfo infoCol = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSUSUVNFVWEB");
		List<NeoObject> listaCli = (List<NeoObject>) PersistEngine.getObjects(infoCol.getEntityClass(), filterCli);
		if (!listaCli.isEmpty())
		{
			EntityWrapper usuarioCliente = new EntityWrapper(listaCli.get(0));
			Long codCli = (Long) usuarioCliente.findValue("codcli");
			codSel = (String) usuarioCliente.getValue("codsel");
			dirNes = (String) usuarioCliente.getValue("dirnes");
			numCgc = (Long) usuarioCliente.findValue("numcgc");
			datEmi = (GregorianCalendar) usuarioCliente.findValue("datemi");
			numDfs = (Long) usuarioCliente.findValue("numdfs");

			String CodSel = codSel;
			String DirNes = dirNes;
			Long NumCgc = Long.valueOf(numCgc);
			Long NumDfs = Long.valueOf(numDfs);

			try
			{

				String path = "";

				//Pesquisa no servidor de bkp, pois o matadeiro ficou lotado - por Lucas 24/04/2013
				String pathNew = "";
				String filename = "";
				if ((tipDoc.equals(LinkActionsEnum.BOLETO.getDescricao())))
				{
					path = "\\\\MATADEIRO\\nfse$\\Boleto\\";
					// BOLETO191515464U01.PDF
					// BOLETO<EMP><FILIAL><RPS><SERIE>.PDF
					filename = "BOLETO" + codEmp + codFil + numNfv + codSnf + ".pdf";
				}
				else if (tipDoc.equals(LinkActionsEnum.RPS.getDescricao()))
				{

					// RPS191515464U01.PDF
					// RPS<EMP><FILIAL><RPS><SERIE>.PDF

					String fileRPSS = "";
					path = "\\\\MATADEIRO\\nfse$\\Demonstrativo\\";
					pathNew = BASEPATH_BKP01 + "\\Matadeiro\\Demonstrativo\\";
					filename = "DEMONSTRATIVO" + codEmp + codFil + numNfv + codSnf + ".pdf";

				}
				else if (tipDoc.equals(LinkActionsEnum.XML.getDescricao()))
				{

					int DiaEmi = datEmi.get(Calendar.DAY_OF_MONTH);
					int MesEmi = datEmi.get(Calendar.MONTH);
					MesEmi = MesEmi + 1;

					String Dia = "";
					if (DiaEmi <= 9)
					{
						Dia = "0" + DiaEmi;
					}
					else
					{
						Dia = DiaEmi + "";
					}

					String Mes = "";
					if (MesEmi <= 9)
					{
						Mes = "0" + MesEmi;
					}
					else
					{
						Mes = MesEmi + "";
					}

					int AnoEmi = datEmi.get(Calendar.YEAR);

					int PosAlf = DirNes.indexOf("Processar");
					PosAlf = PosAlf - 1;

					DirNes = DirNes.substring(0, PosAlf);

					String fileXml = "";
					path = DirNes + "\\XML\\" + NumCgc + "\\Nfse\\" + AnoEmi + "\\" + Mes + "\\" + Dia + "\\";
					filename = NumCgc + "_" + CodSel + "_" + NumDfs + "-procNFSe.xml";
				}

				path = path + filename;
				pathNew = pathNew + filename;

				response.setContentType("APPLICATION/OCTET-STREAM");

				String disHeader = "Attachment;Filename=\"" + filename + "\"";
				response.setHeader("Content-Disposition", disHeader);
				File fileToDownload = new File(path);
				File fileToDownloadNew = new File(pathNew);

				boolean existefile = fileToDownload.exists();

				if (existefile)
				{
					fileInputStream = new FileInputStream(fileToDownload);
				}
				else
				{
					fileInputStream = new FileInputStream(fileToDownloadNew);
				}

				int i;
				while ((i = fileInputStream.read()) != -1)
				{
					out.write(i);
				}
				fileInputStream.close();
				out.close();
			}
			catch (Exception e)
			{ // file IO error
				e.printStackTrace();
				JSONObject jsonCliente = new JSONObject();
				jsonCliente.append("status", 0L);
				jsonCliente.append("msg", "Erro ao efetuar download.");
				out.print(jsonCliente);
			}
		}
	}

	private void alterarSenha(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{

		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();
		JSONObject json = new JSONObject();

		try
		{
			//buscar Sesssao
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{
				json.append("status", 2L);
				json.append("msg", "Sessão inexistente");
				out.print(json);
				return;
			}

			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long codigo = (Long) sessionWrapper.getValue("codigo");

			Connection conn = null;

			String senhaAtual = (String) request.getParameter("inSenhaAtual");
			String novaSenha = (String) request.getParameter("inNovaSenha");
			String novaSenha2 = (String) request.getParameter("inNovaSenha2");

			QLGroupFilter filterCliSen = new QLGroupFilter("AND");
			filterCliSen.addFilter(new QLEqualsFilter("usu_cgccpf", codigo));
			filterCliSen.addFilter(new QLEqualsFilter("usu_senusu", senhaAtual));

			ExternalEntityInfo infoViewLoginCliente = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSUSUTUSU");
			List<NeoObject> user = (List<NeoObject>) PersistEngine.getObjects(infoViewLoginCliente.getEntityClass(), filterCliSen);

			if ((!user.isEmpty()) && (novaSenha != null) && (!novaSenha.equals("")) && (novaSenha.equals(novaSenha2)))
			{
				PreparedStatement st = null;
				try
				{
					conn = PersistEngine.getConnection("SAPIENS");

					StringBuffer sqlUpd = new StringBuffer();
					sqlUpd.append("UPDATE USU_T230USU SET USU_SenUsu = ? WHERE USU_CgcCpf = ?");

					st = conn.prepareStatement(sqlUpd.toString());
					st.setString(1, novaSenha);
					st.setLong(2, codigo);
					st.executeUpdate();
					json.append("status", 1L);
					json.append("msg", "Senha alterada com sucesso.");
					out.print(json);

				}
				catch (Exception e)
				{
					e.printStackTrace();

					json.append("status", 0L);
					json.append("msg", "Não foi possível alterar a senha, erro interno.");
					out.print(json);

				}
				finally
				{
					try
					{
						conn.close();
					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}
			else
			{
				json.append("status", 0L);
				json.append("msg", "Não foi possível alterar a senha, verifique os dados informados. ");
				out.print(json);
			}
		}
		catch (Exception e)
		{
			json.append("status", 0L);
			json.append("msg", "Não foi possível alterar a senha, verifique os dados informados. ");
			out.print(json);
			e.printStackTrace();
		}
	}

	private void validateParameters(String... parameters) throws ServletException
	{
		for (String parameter : parameters)
		{
			if (NeoUtils.safeIsNull(parameter))
			{
				throw new ServletException("Parâmetro inválido");
			}
		}
	}

	private void salvarContato(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		try
		{
			String sessionId = request.getParameter("sessionId");
			SiteUtils siteUtils = new SiteUtils();

			//Busca Sesssao
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{
				JSONObject jsonCliente = new JSONObject();

				jsonCliente.append("status", 2L);
				jsonCliente.append("msg", "Sessão inexistente");
				out.print(jsonCliente);
				return;
			}

			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long cpf = (Long) sessionWrapper.getValue("codigo");

			QLGroupFilter filterUser = new QLGroupFilter("AND");
			filterUser.addFilter(new QLEqualsFilter("usu_cgccpf", cpf));
			filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));

			//Busca Cliente
			List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);

			NeoObject clienteObj = null;

			Long codcli = null;
			if (users != null)
			{
				EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
				codcli = (Long) usuarioCliente.getValue("usu_codcli");

				QLEqualsFilter filterCliente = new QLEqualsFilter("codcli", codcli);
				clienteObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCliente);
			}

			//Busca os valores por Request
			String nome = request.getParameter("nome");
			String email = request.getParameter("email");
			String telefone = request.getParameter("telefone");
			String mensagem = request.getParameter("mensagem");
			String cidade = request.getParameter("cidade"); // atualmente o site nao envia estes dados, nao sei o porque mas ninguem reclamou. Provavelmente alguma solicitacao
			String bairro = request.getParameter("bairro"); // atualmente o site nao envia estes dados, nao sei o porque mas ninguem reclamou. Provavelmente alguma solicitacao
			String tipoContato = request.getParameter("tipoContato");

			//Busca o Tipo de Contato
			QLEqualsFilter filterTipoContato = new QLEqualsFilter("neoId", Long.valueOf(tipoContato));
			NeoObject tipoContatoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("tipoContato"), filterTipoContato);
			EntityWrapper tipoContatoWrapper = new EntityWrapper(tipoContatoObj);
			Long idTipoContato = (Long) tipoContatoWrapper.getValue("neoId");
			String descricaoTipoContato = (String) tipoContatoWrapper.getValue("tipo");

			//Cria Variaveis para diferenciar RSC de RRC
			String eform = "";
			String workflow = "";
			String fieldTelefone = "";
			String fieldContato = "";
			String fieldMensagem = "";
			String valueMensagem = "";
			String valueSolicitante = "";

			GregorianCalendar gcPrazo = new GregorianCalendar();
			gcPrazo.add(GregorianCalendar.DATE, 5);
			gcPrazo.set(GregorianCalendar.HOUR, 23);
			gcPrazo.set(GregorianCalendar.MINUTE, 59);
			gcPrazo.set(GregorianCalendar.SECOND, 59);

			//Reclamacao
			if (idTipoContato == 11715151L)
			{
				eform = "RRCRelatorioReclamacaoCliente";
				workflow = "Q005 - RRC - Relatório de Reclamação de Cliente";
				fieldTelefone = "telefone";
				fieldContato = "pessoaContato";
				fieldMensagem = "textoReclamacao";
				valueSolicitante = "SITERESPONSAVELRRC";
			}
			else
			{
				valueSolicitante = "SITERESPONSAVELRSC";

				RSCVO rsc = new RSCVO();
				rsc.setCodCli(codcli);
				rsc.setOrigemSolicitacao("15");
				rsc.setTipoContato(tipoContato);
				rsc.setNome(nome);
				rsc.setEmail(email);
				rsc.setTelefone(telefone);
				rsc.setMensagem(mensagem);
				rsc.setCidade(cidade);
				rsc.setBairro(bairro);
				rsc.setPrazo(gcPrazo);

				String tarefa = RSCHelper.abrirRSC(rsc, valueSolicitante, true);
				System.out.println("Tarefa RSC Aberta: " + tarefa);
				return;

				/*
				 * eform = "RSCRelatorioSolicitacaoClienteNovo";
				 * workflow = "C027 - RSC - Relatório de Solicitação de Cliente Novo";
				 * fieldTelefone = "telefones";
				 * fieldContato = "contato";
				 * fieldMensagem = "descricao";
				 * valueMensagem = "Tipo do Contato: " + descricaoTipoContato + "\nDescrição: ";
				 * valueSolicitante = "SITERESPONSAVELRSC";
				 */
			}

			//Cria Instancia do Eform Principal
			InstantiableEntityInfo infoSolicitacao = AdapterUtils.getInstantiableEntityInfo(eform);
			NeoObject noSolicitacao = infoSolicitacao.createNewInstance();
			EntityWrapper solicitacaoWrapper = new EntityWrapper(noSolicitacao);

			//Busca Origem da Solicitacao
			QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", "05");
			NeoObject origemObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("RRCOrigem"), filterOrigem);

			//Busca Cidade
			//QLEqualsFilter filterCidade = new QLEqualsFilter("codcid", Long.valueOf(cidade));
			//NeoObject cidadeObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("VETORH_R074CID"), filterCidade);

			/*
			 * solicitacaoWrapper.findField("prazoAtendimento").setValue(OrsegupsUtils.getNextWorkDay(gcPrazo
			 * ));
			 * //Seta os valores
			 * solicitacaoWrapper.findField("origem").setValue(origemObj);
			 * solicitacaoWrapper.findField("cidade").setValue(cidade);
			 * solicitacaoWrapper.findField("bairro").setValue(bairro);
			 * solicitacaoWrapper.findField("email").setValue(email);
			 * solicitacaoWrapper.findField(fieldContato).setValue(nome);
			 * solicitacaoWrapper.findField(fieldTelefone).setValue(telefone);
			 * solicitacaoWrapper.findField(fieldMensagem).setValue(valueMensagem + mensagem);
			 */

			if (idTipoContato == 11715151L)
			{

				solicitacaoWrapper.setValue("prazoAcao", OrsegupsUtils.getNextWorkDay(gcPrazo));
				//Seta os valores
				solicitacaoWrapper.setValue("origem", origemObj);
				solicitacaoWrapper.setValue("cidade", cidade);
				solicitacaoWrapper.setValue("bairro", bairro);
				solicitacaoWrapper.setValue("email", email);
				solicitacaoWrapper.setValue(fieldContato, nome);
				solicitacaoWrapper.setValue(fieldTelefone, telefone);
				solicitacaoWrapper.setValue(fieldMensagem, valueMensagem + mensagem);

			}
			else
			{

				/*
				 * //solicitacaoWrapper.setValue("prazoAtendimento",OrsegupsUtils.getNextWorkDay(gcPrazo))
				 * ; //RSCANTIGA
				 * solicitacaoWrapper.setValue("prazoRegistrarRSC",OrsegupsUtils.getNextWorkDay(gcPrazo));
				 * //RSCANTIGA
				 * //Seta os valores
				 * solicitacaoWrapper.setValue("origem",origemObj);
				 * //solicitacaoWrapper.setValue("cidade",cidade); // removido na nova versao da RSC
				 * //solicitacaoWrapper.setValue("bairro",bairro); // removido na nova versao da RSC
				 * solicitacaoWrapper.setValue("email",email);
				 * solicitacaoWrapper.setValue(fieldContato,nome);
				 * solicitacaoWrapper.setValue(fieldTelefone,telefone);
				 * solicitacaoWrapper.setValue(fieldMensagem,valueMensagem + mensagem + "  Cidade: " +
				 * cidade + ", bairro:"+bairro);
				 */

			}

			//Caso existe o cliente
			if (clienteObj != null)
			{
				solicitacaoWrapper.findField("clienteSapiens").setValue(clienteObj);
			}

			//Salva o Eform
			PersistEngine.persist(noSolicitacao);

			// Cria Instancia do WorkFlow
			QLEqualsFilter equal = new QLEqualsFilter("name", workflow);
			List<ProcessModel> processModelList = (List<ProcessModel>) PersistEngine.getObjects(ProcessModel.class, equal, " neoId DESC ");
			ProcessModel processModel = processModelList.get(0);

			//Busca o solicitante
			NeoPaper papel = new NeoPaper();
			String executor = "";
			NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", valueSolicitante));
			papel = (NeoPaper) obj;

			if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers())
				{
					executor = user.getCode();
					break;
				}

			}
			else
			{
				String gestaoCerec = "Gestão CEREC";
				NeoPaper objCerec = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", gestaoCerec));
				papel = (NeoPaper) objCerec;
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						break;
					}
				}
				else
				{
					JSONObject jsonCliente = new JSONObject();
					jsonCliente.append("status", 0L);
					jsonCliente.append("msg", "Usuário ou Gestor Responsável não encontrado!");
					out.print(jsonCliente);
				}

			}

			NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));

			/**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do
			 *         Fusion
			 * @date 11/03/2015
			 */

			//final WFProcess processo = WorkflowService.startProcess(processModel, noSolicitacao, true, solicitante);
			//processo.setSaved(true);
			/*System.out.println("solicitacao aberta site. " + processo.getCode());
			try
			{
				new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante, false);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				out.print("Erro #4 - Erro ao avançar a primeira tarefa");
				return;
			}*/
			String tarefa = OrsegupsWorkflowHelper.iniciaProcesso(processModel, noSolicitacao, true, solicitante, true, solicitante);

			/**
			 * FIM ALTERAÇÕES - NEOMIND
			 */

			// Dispara email ao cliente
			Map<String, Object> paramMap = new HashMap<String, Object>();

			paramMap.put("tarefa", tarefa);
			paramMap.put("nome", nome);
			paramMap.put("tipo", tipoContato);
			paramMap.put("mensagem", mensagem);

			FusionRuntime.getInstance().getMailEngine().sendEMail(email, "/portal_orsegups/emailAberturaRSC.jsp", paramMap);

			JSONObject jsonCliente = new JSONObject();
			jsonCliente.append("status", 1L);
			jsonCliente.append("msg", "Obrigado pelo contato");
			out.print(jsonCliente);

		}
		catch (Exception e)
		{
			JSONObject jsonCliente = new JSONObject();

			jsonCliente.append("status", 0L);
			jsonCliente.append("msg", "Servidor indisponível, tente mais tarde.");
			out.print(jsonCliente);
			e.printStackTrace();
		}
	}

	private void listarContasSigma(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		Long logKey = new GregorianCalendar().getTimeInMillis();
		Connection conn = PersistEngine.getConnection("SAPIENS");
		JSONObject json = new JSONObject();
		List<SiteContaSigmaVO> listaContaSigmaVONew = null;
		try
		{

			String sessionId = request.getParameter("sessionId");
			String gerarGrafico = request.getParameter("gerarGrafico");
			SiteUtils siteUtils = new SiteUtils();

			//buscar Sesssao
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{

				JSONObject jsonCliente = new JSONObject();

				jsonCliente.append("status", 2L);
				jsonCliente.append("msg", "Sessão inexistente");
				out.print(jsonCliente);
				return;
			}

			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long cpf = (Long) sessionWrapper.getValue("codigo");
			System.out.println("[AREACLIENTE][" + logKey + "][" + logKey + "] - listarContasSigma - CPF:" + cpf);

			List<SiteContaSigmaVO> listaContaSigmaVO = new ArrayList<SiteContaSigmaVO>();
			String cpfStr = String.valueOf(cpf);
			StringBuilder query = new StringBuilder();
			/*
			 * query.append(
			 * " Select cen.cd_cliente, cen.senha_internet, cen.id_central, cen.particao, cen.id_empresa, cen.fantasia, cen.endereco, cid.nome AS cidade, bai.nome AS bairro, cen.id_estado "
			 * );
			 * query.append(" From E085CLI cli WITH (NOLOCK)   ");
			 * query.append(" Inner Join USU_T160CTR ctr WITH (NOLOCK)  ON ctr.usu_codcli = cli.codcli ");
			 * query.append(
			 * " Inner Join USU_T160SIG sig WITH (NOLOCK)  ON sig.usu_numctr = ctr.usu_numctr AND sig.usu_codfil = ctr.usu_codfil "
			 * );
			 * query.append(" Inner Join ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"
			 * +this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+
			 * "].SIGMA90.dbo.dbCentral cen WITH (NOLOCK)  ON cen.cd_cliente = sig.usu_codcli ");
			 * query.append(" Inner Join ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"
			 * +this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+
			 * "].SIGMA90.dbo.dbCidade cid WITH (NOLOCK)  ON cid.id_cidade = cen.id_cidade ");
			 * query.append(" Inner Join ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"
			 * +this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+
			 * "].SIGMA90.dbo.dbBairro bai WITH (NOLOCK)  ON bai.id_bairro = cen.id_bairro ");
			 * query.append(" Where cli.cgccpf = ? And ctr.usu_sitctr = 'A' And cen.ctrl_central = 1 ");
			 */ 
			query.append(" Select cen.cd_cliente, cen.senha_internet, cen.id_central, cen.particao, cen.id_empresa, cen.fantasia, cen.endereco, cid.nome AS cidade, bai.nome AS bairro, cen.id_estado ");
			query.append(" From E085CLI cli WITH (NOLOCK)   ");
			query.append(" Inner Join USU_T160CTR ctr WITH (NOLOCK)  ON ctr.usu_codcli = cli.codcli ");
			query.append(" Inner Join USU_T160SIG sig WITH (NOLOCK)  ON sig.usu_numctr = ctr.usu_numctr AND sig.usu_codfil = ctr.usu_codfil AND sig.usu_codemp = ctr.usu_codemp ");
			query.append(" Inner Join [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral cen WITH (NOLOCK)  ON cen.cd_cliente = sig.usu_codcli ");
			query.append(" Inner Join [FSOODB03\\SQL01].SIGMA90.dbo.dbCidade cid WITH (NOLOCK)  ON cid.id_cidade = cen.id_cidade ");
			query.append(" Inner Join [FSOODB03\\SQL01].SIGMA90.dbo.dbBairro bai WITH (NOLOCK)  ON bai.id_bairro = cen.id_bairro ");
			query.append(" Where cli.cgccpf = ? And (ctr.usu_sitctr = 'A' Or (ctr.usu_sitctr = 'I' And ctr.usu_datini < GetDate() And ctr.usu_datfim > GetDate())) And cen.ctrl_central = 1 ");

			QLEqualsFilter filterCodigo = new QLEqualsFilter("cgccpf", cpfStr);
			NeoObject clienteList = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SITEClientePublico"), filterCodigo);

			if (NeoUtils.safeIsNotNull(clienteList))
			{
				query.append(" And cli.tipemc <> 0 ");
			}
			else
			{
				query.append(" And cli.tipemc <> 2 ");
			}

			query.append(" Group By cen.cd_cliente, cen.senha_internet, cen.id_central, cen.particao, cen.id_empresa, cen.fantasia, cen.endereco, cid.nome, bai.nome, cen.id_estado ");

			PreparedStatement st = conn.prepareStatement(query.toString());
			st.setLong(1, cpf);

			System.out.println("[AREACLIENTE][" + logKey + "][" + logKey + "] - listarContasSigma - CPF:" + query.toString());

			ResultSet rs = st.executeQuery();

			String cdCliente = "";

			int contador = 0;
			while (rs.next())
			{
				SiteContaSigmaVO contaSigma = new SiteContaSigmaVO();
				contaSigma.setCdCliente(rs.getLong("cd_cliente"));
				contaSigma.setCentral(rs.getString("id_central"));
				contaSigma.setParticao(rs.getString("particao"));
				contaSigma.setCentralParticao(contaSigma.getCentral() + "[" + contaSigma.getParticao() + "]");
				contaSigma.setEmpresa(rs.getString("id_empresa"));
				contaSigma.setFantasia(rs.getString("fantasia"));
				contaSigma.setEndereco(rs.getString("endereco"));
				contaSigma.setCidade(rs.getString("cidade"));
				contaSigma.setBairro(rs.getString("bairro"));
				contaSigma.setUf(rs.getString("id_estado"));
				contaSigma.setLinkEventos((String) LINK_EVENTO + "central=" + contaSigma.getCdCliente() + "&senha=" + rs.getString("senha_internet") + "&cliente=" + COD_CLIENTE_EVENTO);

				cdCliente += contaSigma.getCdCliente().toString();
				if (!rs.isLast())
					cdCliente += ",";

				listaContaSigmaVO.add(contaSigma);
			}
			System.out.println("[AREACLIENTE][" + logKey + "] - listarContasSigma - cdsCliente:" + cdCliente);
			List<SiteGraficoOrdemServicoVO> listaTat = siteUtils.graficoAtendimentoTaticoList(cdCliente);
			List<SiteGraficoOrdemServicoVO> listaDes = siteUtils.graficoDesvioHabitoList(cdCliente);
			List<SiteGraficoOrdemServicoVO> listaOs = siteUtils.graficoOrdemServicoList(cdCliente);

			int x = 0;
			listaContaSigmaVONew = new ArrayList<SiteContaSigmaVO>();
			if (NeoUtils.safeIsNotNull(listaContaSigmaVO))
			{
				System.out.println("[AREACLIENTE][" + logKey + "] - Debug 1 -" + cpf);
				SiteGraficoVO graficoTat = null;
				SiteGraficoVO graficoDes = null;
				SiteGraficoVO graficoOs = null;
				for (SiteContaSigmaVO contaSigma : listaContaSigmaVO)
				{
					String txtTat = "";
					String txtDes = "";
					String txtOS = "";
					SiteContaSigmaVO sigmaVO = contaSigma;

					String txtList = "";

					if (gerarGrafico != null && gerarGrafico.equals("Sim"))
					{

						txtList = "";
						x = 0;
						graficoTat = new SiteGraficoVO();
						for (SiteGraficoOrdemServicoVO ordemServicoTatVO : listaTat)
						{
							if (sigmaVO.getCdCliente().toString().equals(ordemServicoTatVO.getCdCliente().toString()))
							{
								GregorianCalendar data = new GregorianCalendar();
								data.setTime(ordemServicoTatVO.getData().getTime());

								String txtLine = "";

								txtLine = txtLine + "[new Date(";
								txtLine = txtLine + data.get(Calendar.YEAR);
								txtLine = txtLine + ",";
								txtLine = txtLine + data.get(Calendar.MONTH);
								txtLine = txtLine + ",";
								txtLine = txtLine + data.get(Calendar.DATE);
								txtLine = txtLine + ",";
								txtLine = txtLine + data.get(Calendar.HOUR_OF_DAY);
								txtLine = txtLine + ",";
								txtLine = txtLine + data.get(Calendar.MINUTE);
								txtLine = txtLine + "),";
								txtLine = txtLine + ordemServicoTatVO.getTempo();
								txtLine = txtLine + ",'" + ordemServicoTatVO.getToolTipHtml() + "'";
								txtLine = txtLine + "],";

								txtList = txtLine + txtList;

								x++;
							}
							if (x == 30)
							{
								break;
							}
						}

						if (!txtList.equals(""))
						{
							txtTat = "[" + txtList + "]";
							graficoTat.setGrafico(GraficosEnum.ATENDIMENTOTATICO.getDescricao());
							graficoTat.setOptions(GraficosEnum.ATENDIMENTOTATICO.getOptions());
							graficoTat.setValor(txtTat);
						}
						else
						{
							graficoTat = null;
						}

					}

					txtList = "";
					x = 0;
					graficoDes = new SiteGraficoVO();
					for (SiteGraficoOrdemServicoVO ordemServicoDesVO : listaDes)
					{
						if (sigmaVO.getCdCliente().toString().equals(ordemServicoDesVO.getCdCliente().toString()))
						{
							GregorianCalendar data = new GregorianCalendar();
							data.setTime(ordemServicoDesVO.getData().getTime());

							String txtLine = "";

							txtLine = txtLine + "[new Date(";
							txtLine = txtLine + data.get(Calendar.YEAR);
							txtLine = txtLine + ",";
							txtLine = txtLine + data.get(Calendar.MONTH);
							txtLine = txtLine + ",";
							txtLine = txtLine + data.get(Calendar.DATE);
							txtLine = txtLine + ",";
							txtLine = txtLine + data.get(Calendar.HOUR_OF_DAY);
							txtLine = txtLine + ",";
							txtLine = txtLine + data.get(Calendar.MINUTE);
							txtLine = txtLine + "),";
							txtLine = txtLine + ordemServicoDesVO.getTempo();
							txtLine = txtLine + ",'" + ordemServicoDesVO.getToolTipHtml() + "'";
							txtLine = txtLine + "],";

							txtList = txtLine + txtList;

							x++;
						}
						if (x == 30)
						{
							break;
						}

					}

					if (!txtList.equals(""))
					{
						txtDes = "[" + txtList + "]";
						graficoDes.setGrafico(GraficosEnum.DESVIOHABITO.getDescricao());
						graficoDes.setOptions(GraficosEnum.DESVIOHABITO.getOptions());
						graficoDes.setValor(txtDes);
					}
					else
					{
						graficoDes = null;
					}

					txtList = "";
					x = 0;
					graficoOs = new SiteGraficoVO();
					for (SiteGraficoOrdemServicoVO ordemServicoVO : listaOs)
					{
						if (sigmaVO.getCdCliente().toString().equals(ordemServicoVO.getCdCliente().toString()))
						{
							GregorianCalendar data = new GregorianCalendar();
							data.setTime(ordemServicoVO.getData().getTime());

							String txtLine = "";

							txtLine = txtLine + "[new Date(";
							txtLine = txtLine + data.get(Calendar.YEAR);
							txtLine = txtLine + ",";
							txtLine = txtLine + data.get(Calendar.MONTH);
							txtLine = txtLine + ",";
							txtLine = txtLine + data.get(Calendar.DATE);
							txtLine = txtLine + ",";
							txtLine = txtLine + data.get(Calendar.HOUR_OF_DAY);
							txtLine = txtLine + ",";
							txtLine = txtLine + data.get(Calendar.MINUTE);
							txtLine = txtLine + "),";
							txtLine = txtLine + ordemServicoVO.getTempo();
							txtLine = txtLine + ",'" + ordemServicoVO.getToolTipHtml() + "'";
							txtLine = txtLine + "],";

							txtList = txtLine + txtList;
							x++;

						}
						if (x == 30)
						{
							break;
						}

					}

					if (!txtList.equals(""))
					{
						txtOS = "[" + txtList + "]";
						graficoOs.setGrafico(GraficosEnum.ORDEMSERVICO.getDescricao());
						graficoOs.setOptions(GraficosEnum.ORDEMSERVICO.getOptions());
						graficoOs.setValor(txtOS);
					}
					else
					{
						graficoOs = null;
					}

					sigmaVO.setGraficos(siteUtils.listarGraficosCliente(sigmaVO.getCdCliente(), graficoOs, graficoTat, graficoDes));
					listaContaSigmaVONew.add(sigmaVO);
				}

			}

			if (listaContaSigmaVONew != null && !listaContaSigmaVONew.isEmpty())
			{
				Gson gson = new Gson();
				String contaJson = gson.toJson(listaContaSigmaVONew);
				System.out.println(contaJson);
				out.print(contaJson);
			}
			else
			{
				json.append("status", 0L);
				json.append("msg", "Conta Insexistente");
				out.print(json);
			}
		}
		catch (Exception e)
		{
			json.append("status", 0L);
			json.append("msg", "Servidor indisponível, tente mais tarde.");
			out.print(json);
			e.printStackTrace();
		}
	}

	private void listarOrdemServico(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{

		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();

		//buscar Sesssao
		NeoObject session = siteUtils.getSession(sessionId);

		if (session == null)
		{
			JSONObject jsonCliente = new JSONObject();

			jsonCliente.append("status", 2L);
			jsonCliente.append("msg", "Sessão inexistente");
			out.print(jsonCliente);
			return;
		}

		String cdCliente = request.getParameter("cdCliente").trim();
		Connection connSigma = PersistEngine.getConnection("SIGMA90");

		SiteOrdemServicoVO ordemServico = null;
		Gson gson = new Gson();
		JSONObject json = new JSONObject();

		try
		{
			/*** Recupera Ordem de Servico ***/
			StringBuffer queryOrdemServico = new StringBuffer();
			queryOrdemServico.append(" SELECT TOP 20 os.ID_ORDEM, os.ABERTURA, os.DEFEITO, os.FECHAMENTO, os.EXECUTADO, os.FECHADO ");
			queryOrdemServico.append(" FROM dbORDEM os  ");
			queryOrdemServico.append(" WHERE os.cd_cliente = ?    ");
			queryOrdemServico.append(" ORDER BY os.ABERTURA DESC");
			PreparedStatement stColaborador = connSigma.prepareStatement(queryOrdemServico.toString());
			stColaborador.setString(1, cdCliente);

			ResultSet rsOrdermServico = stColaborador.executeQuery();
			List<SiteOrdemServicoVO> servicoVOs = new ArrayList<SiteOrdemServicoVO>();

			while (rsOrdermServico.next())
			{
				ordemServico = new SiteOrdemServicoVO();
				ordemServico.setIdOrdem(rsOrdermServico.getLong("ID_ORDEM"));
				ordemServico.setAbertura((String) NeoUtils.safeDateFormat(rsOrdermServico.getTimestamp("ABERTURA"), "dd/MM/yyyy HH:mm:ss"));
				ordemServico.setDefeito(rsOrdermServico.getString("DEFEITO"));
				ordemServico.setFechamento((String) NeoUtils.safeDateFormat(rsOrdermServico.getTimestamp("FECHAMENTO"), "dd/MM/yyyy HH:mm:ss"));
				ordemServico.setExecutado(rsOrdermServico.getString("EXECUTADO"));
				ordemServico.setFechado(rsOrdermServico.getString("FECHADO"));
				servicoVOs.add(ordemServico);

			}
			if ((servicoVOs != null) && (!servicoVOs.isEmpty()))
			{
				String ordemServicoJson = gson.toJson(servicoVOs);
				out.print(ordemServicoJson);
			}
			else
			{
				json.append("status", 0L);
				json.append("msg", "SIGMA - Lista nula ou vazia!");
				out.print(json);
			}
			rsOrdermServico.close();
			stColaborador.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			json.append("status", 0L);
			json.append("msg", "Servidor indisponível, tente mais tarde.");
			out.print(json);
		}
		finally
		{
			try
			{
				connSigma.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				json.append("status", 0L);
				json.append("msg", "Servidor indisponível, tente mais tarde.");
				out.print(json);
			}
		}
	}

	public static String onlyNumbers(String str)
	{
		String sAux = "";
		if (str == null)
		{
			str = "";
		}
		for (int i = 0; i < str.length(); i++)
		{
			if ((str.charAt(i) >= '0') && (str.charAt(i) <= '9'))
			{
				sAux = sAux + str.charAt(i);
			}
		}
		return sAux;
	}

	private void recuperarSenha(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		Long logKey = new GregorianCalendar().getTimeInMillis();
		System.out.println("[AREACLIENTE][" + logKey + "] - recuperarSenha - 1");
		String key = request.getParameter("key");
		JSONObject jsonAuth = null;
		UserWebServiceAuthentication auth = new UserWebServiceAuthentication();
		try
		{
			if (!auth.authentication(key))
			{

				jsonAuth = new JSONObject();

				jsonAuth.append("status", 0L);

				jsonAuth.append("msg", "Erro de autenticação");

				out.print(jsonAuth);
				return;
			}

			String auxCgcCpf = onlyNumbers(request.getParameter("cgccpf"));
			Long cgcCpf = null;

			if (auxCgcCpf == null || auxCgcCpf.trim().equals(""))
			{
				jsonAuth = new JSONObject();

				jsonAuth.append("status", 0L);

				jsonAuth.append("msg", "Erro  CNPJ - CPF Inválido.");

				out.print(jsonAuth);
				return;
			}
			else
			{
				try
				{
					cgcCpf = Long.parseLong(auxCgcCpf.trim());
				}
				catch (NumberFormatException e)
				{
					jsonAuth = new JSONObject();

					jsonAuth.append("status", 0L);

					jsonAuth.append("msg", "Erro ao converter CNPJ - CPF.");

					out.print(jsonAuth);
				}
			}

			ExternalEntityInfo infoCliente = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENS_Clientes");
			List<NeoObject> cliente = new ArrayList<NeoObject>();
			if (infoCliente != null)
			{
				cliente = (List<NeoObject>) PersistEngine.getObjects(infoCliente.getEntityClass(), new QLEqualsFilter("cgccpf", cgcCpf));
			}
			System.out.println("[AREACLIENTE][" + logKey + "] - recuperarSenha - buscando cliente: " + cgcCpf);
			if (!cliente.isEmpty())
			{
				System.out.println("[AREACLIENTE][" + logKey + "] - recuperarSenha - cliente: " + cgcCpf + " encontrado.");
				Map<String, Object> paramMap = new HashMap<String, Object>();

				EntityWrapper wrpCliente = new EntityWrapper(cliente.get(0));
				String fantasia = (String) wrpCliente.getValue("apecli");
				String mailCliente = (String) wrpCliente.getValue("intnet");
				String senha = "";
				ExternalEntityInfo infoUsu = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSUSUTUSU");
				List<NeoObject> listaUsu = (List<NeoObject>) PersistEngine.getObjects(infoUsu.getEntityClass(), new QLEqualsFilter("usu_cgccpf", cgcCpf));
				System.out.println("[AREACLIENTE][" + logKey + "] - recuperarSenha - Debug 2 - email: " + mailCliente);
				if (!listaUsu.isEmpty())
				{
					System.out.println("[AREACLIENTE][" + logKey + "] - recuperarSenha - Debug 2.1");
					EntityWrapper usuarioCliente = new EntityWrapper(listaUsu.get(0));
					senha = (String) usuarioCliente.getValue("usu_senusu");
				}
				System.out.println("[AREACLIENTE][" + logKey + "] - recuperarSenha - Debug 2.3");

				if ((cgcCpf != null) && (!cgcCpf.equals("")) && (senha != null) && (!senha.equals("")) && (mailCliente != null) && (!mailCliente.equals("")) && (fantasia != null) && (!fantasia.equals("")))
				{
					paramMap.put("cgccpf", cgcCpf);
					paramMap.put("senha", senha);
					paramMap.put("fantasia", fantasia);

					FusionRuntime.getInstance().getMailEngine().sendEMail(mailCliente, "/portal_orsegups/areaCliente2TemplateEmailSenhaCliente.jsp", paramMap);
					jsonAuth = new JSONObject();

					jsonAuth.append("status", 1L);

					jsonAuth.append("msg", "E-MAIL enviado com sucesso.");

					out.print(jsonAuth);
				}
				else
				{
					jsonAuth = new JSONObject();

					jsonAuth.append("status", 0L);

					jsonAuth.append("msg", "Erro ao enviar E-MAIL informações inválidas .");

					out.print(jsonAuth);
				}
			}
			else
			{
				jsonAuth = new JSONObject();

				jsonAuth.append("status", 0L);

				jsonAuth.append("msg", "Erro cliente não encontrado .");

				out.print(jsonAuth);
			}

		}
		catch (JSONException e)
		{
			jsonAuth = new JSONObject();

			try
			{
				jsonAuth.append("status", 0L);
				jsonAuth.append("msg", "Erro - " + e.getMessage());
				out.print(jsonAuth);
				e.printStackTrace();
			}
			catch (JSONException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			e.printStackTrace();
		}

	}

	private void checkAceiteNfDoc(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		String intnetCliente = null;
		Boolean chkDeacordoRecnf = false;
		Boolean chkDeacordoRecdoc = false;
		Long nCodcli = null;
		try
		{
			String sessionId = request.getParameter("sessionId");

			SiteUtils siteUtils = new SiteUtils();
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{
				JSONObject json = new JSONObject();

				json.append("status", 2L);
				json.append("msg", "Sessão inexistente");
				out.print(json);
				return;
			}

			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long cpf = (Long) sessionWrapper.getValue("codigo");

			QLGroupFilter filterUser = new QLGroupFilter("AND");
			filterUser.addFilter(new QLEqualsFilter("usu_cgccpf", cpf));
			filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));

			//Busca Cliente
			List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);

			if (users != null)
			{
				EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
				nCodcli = (Long) usuarioCliente.getValue("usu_codcli");
			}

			intnetCliente = SiteUtils.recuperaIntnetcliente(nCodcli);
			Boolean chkNfDoc[] = SiteUtils.recuperaAceiteNfDoc(nCodcli);
			chkDeacordoRecnf = chkNfDoc[0];
			chkDeacordoRecdoc = chkNfDoc[1];
			System.out.println("[Area Cliente ] NF- intnet cliente[" + nCodcli + "], chkDeacordoRecnf[" + chkDeacordoRecnf + "], chkDeacordoRecdoc[" + chkDeacordoRecdoc + "]");

			JsonArray jsonListaRetorno = new JsonArray();
			JsonObject jsonRetorno = new JsonObject();
			jsonRetorno.addProperty("chkDeacordoRecnf", chkDeacordoRecnf);
			jsonRetorno.addProperty("chkDeacordoRecdoc", chkDeacordoRecdoc);
			jsonRetorno.addProperty("txt_email", intnetCliente);
			jsonListaRetorno.add(jsonRetorno);
			out.print(jsonListaRetorno);
			System.out.println(jsonListaRetorno);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			JSONObject json = new JSONObject();
			json.append("status", 0L);
			json.append("msg", "Não foi possível verificar situação do aceite");
			out.print(json);
		}
	}

	private void listarDocumentos(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		HashMap<Long, List<SiteDocsMostrarVO>> tiposDocumentosMostrarSite = new HashMap<Long, List<SiteDocsMostrarVO>>();
		String intnetCliente = null;
		Boolean chkDeacordoRecnf = false;
		Boolean chkDeacordoRecdoc = false;

		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();
		Gson gson = new Gson();
		//buscar Sesssao
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{
				JSONObject json = new JSONObject();

				json.append("status", 2L);
				json.append("msg", "Sessão inexistente");
				out.print(json);
				return;
			}

			List<SiteTipoDocumentoVO> tiposDocumentos = siteUtils.getListaTiposDocumentos();

			DecimalFormat df = new DecimalFormat("00");
			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long cpf = (Long) sessionWrapper.getValue("codigo");

			//			QLGroupFilter filterUser = new QLGroupFilter("AND");
			//			filterUser.addFilter(new QLEqualsFilter("cgccpf", cpf));

			//Busca Cliente
			//List<NeoObject> cliensteListObj = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUVCLIEMP"), filterUser);

			String codcli = "";
			Long codEmpAnt = 0L;
			List<SiteClienteVO> clienteVOs = new ArrayList<SiteClienteVO>();
			conn = PersistEngine.getConnection("SAPIENS");

			StringBuffer sql = new StringBuffer();
			//			sql.append(" SELECT CTR.usu_codemp, CTR.usu_codfil, CLI.CodCli, CLI.NomCli, CLI.ApeCli, CLI.CgcCpf, CLI.TipEmc, CLI.CidCli ");
			//			sql.append(" FROM  dbo.usu_t160ctr AS CTR INNER JOIN ");
			//			sql.append(" dbo.E085CLI AS CLI ON CLI.CodCli = CTR.usu_codcli INNER JOIN ");
			//			sql.append(" dbo.usu_t160cvs AS CVS ON CVS.usu_numctr = CTR.usu_numctr AND CVS.usu_codemp = CTR.usu_codemp AND CVS.usu_codfil = CVS.usu_codfil INNER JOIN ");
			//			sql.append(" dbo.E080SER AS SER ON CVS.usu_codser = SER.CodSer AND SER.CodEmp = CVS.usu_codemp ");
			//			sql.append(" WHERE CLI.cgccpf = ? AND CTR.USU_CODEMP IN (1,6,7,15,18,21,22,27,28,29) AND ( ( (ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate()-(30*6) ) ) AND (SER.CodFam <> 'SER102') OR ");
			//			sql.append(" ((CTR.usu_sitctr = 'I') AND (SER.CodFam <> 'SER102') AND (CTR.usu_datfim >= GETDATE()))) ");
			//			sql.append("  GROUP BY CTR.usu_codemp, CTR.usu_codfil, CLI.CodCli, CLI.NomCli, CLI.ApeCli, CLI.CgcCpf, CLI.TipEmc, CLI.CidCli   ");

			sql.append(" SELECT CTR.usu_codemp, CTR.usu_codfil, CLI.CodCli, CLI.NomCli, CLI.ApeCli, CLI.CgcCpf, CLI.TipEmc, CLI.CidCli ");
			sql.append(" FROM  dbo.usu_t160ctr AS CTR INNER JOIN ");
			sql.append(" dbo.E085CLI AS CLI ON CLI.CodCli = CTR.usu_codcli INNER JOIN ");
			sql.append(" dbo.usu_t160cvs AS CVS ON CVS.usu_numctr = CTR.usu_numctr AND CVS.usu_codemp = CTR.usu_codemp AND CVS.usu_codfil = CVS.usu_codfil INNER JOIN ");
			sql.append(" dbo.E080SER AS SER ON CVS.usu_codser = SER.CodSer AND SER.CodEmp = CVS.usu_codemp ");
			sql.append(" WHERE CLI.cgccpf = ? AND CTR.USU_CODEMP IN (1,6,7,8,15,18,21,22,27,28,29) AND ( ( (ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate()-(30*6) ) ) OR ");
			sql.append(" ((CTR.usu_sitctr = 'I')  AND (CTR.usu_datfim >= GETDATE()))) ");
			sql.append("  GROUP BY CTR.usu_codemp, CTR.usu_codfil, CLI.CodCli, CLI.NomCli, CLI.ApeCli, CLI.CgcCpf, CLI.TipEmc, CLI.CidCli   ");

			st = conn.prepareStatement(sql.toString());
			st.setString(1, String.valueOf(cpf));
			rs = st.executeQuery();
			SiteClienteVO clienteVO = null;

			Long nCodcli = null;
			while (rs.next())
			{
				clienteVO = new SiteClienteVO();
				clienteVO.setEmpresa(df.format(rs.getLong("usu_codemp")));
				clienteVO.setFilial(rs.getString("usu_codfil"));
				String clienteStr = "";
				Long clienteL = Long.parseLong(String.valueOf(rs.getString("cgccpf")));

				clienteStr = String.valueOf(clienteL);

				clienteVO.setCodigoCliente(clienteStr);
				clienteVOs.add(clienteVO);
				nCodcli = rs.getLong("codcli");
			}
			rs.close();

			QLGroupFilter filterUser = new QLGroupFilter("AND");
			filterUser.addFilter(new QLEqualsFilter("usu_cgccpf", cpf));
			filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));

			//Busca Cliente
			List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);

			if (users != null)
			{
				EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
				nCodcli = (Long) usuarioCliente.getValue("usu_codcli");
			}

			intnetCliente = SiteUtils.recuperaIntnetcliente(nCodcli);
			Boolean chkNfDoc[] = SiteUtils.recuperaAceiteNfDoc(nCodcli);
			chkDeacordoRecnf = chkNfDoc[0];
			chkDeacordoRecdoc = chkNfDoc[1];
			System.out.println("[Area Cliente ] NF- intnet cliente[" + nCodcli + "], chkDeacordoRecnf[" + chkDeacordoRecnf + "], chkDeacordoRecdoc[" + chkDeacordoRecdoc + "]");

			if (NeoUtils.safeIsNull(clienteVOs) && !clienteVOs.isEmpty())
			{
				JSONObject jsonUser = new JSONObject();

				jsonUser.append("status", 0L);
				jsonUser.append("msg", "Não há documentos para exibir.");
				out.print(jsonUser);
				return;
			}

			if (!tiposDocumentosMostrarSite.containsKey(nCodcli)){ // se os dados NAO estão no cache
				tiposDocumentosMostrarSite.put(nCodcli, SiteUtils.loadTiposDocumentos(nCodcli));
			}
			/*** Pesquisa documentos / Cliente ultimos 12 meses ***/
			String caminho = BASEPATH_BKP01 + "\\Orsegups\\";
			List<SiteCompetenciaDocumentoVO> source = new ArrayList<SiteCompetenciaDocumentoVO>();
			for (SiteClienteVO clienteObj : clienteVOs)
			{
				
				
				
				System.out.println(">Cliente");
				SiteEmpresaDocumentoVO empresaVO = new SiteEmpresaDocumentoVO();

				List<String> cptList = competencias(new GregorianCalendar());
				Long empNumber = Long.parseLong(clienteObj.getEmpresa());
				String emp = df.format(empNumber);
				String fil = clienteObj.getFilial();
				String cli = clienteObj.getCodigoCliente();

				String dataCptAnt = "";

				for (String dataCpt : cptList)
				{
					System.out.println(">>competencias");

					/*QLOpFilter opFilterTipDoc = new QLOpFilter("codTipoDocumento", "<>", 0);
					QLGroupFilter filterTipDoc = new QLGroupFilter("AND");

					filterTipDoc.addFilter(opFilterTipDoc);

					//View de Contas
					List<NeoObject> tipDocList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SITETipoDocumentoCliente"), filterTipDoc);
					*/
					List<String> filesStr = new ArrayList<String>();

					List<String> filesTipStr = new ArrayList<String>();

					dataCpt = dataCpt.replace("/", "");
					for (SiteTipoDocumentoVO tiposDoc : tiposDocumentos)
					{
						System.out.println(">>>Tipos documentos");
						//EntityWrapper objectTipDocWrapper = new EntityWrapper(tiposDoc);
						//String objectTipDocStr = (String) objectTipDocWrapper.getValue("pasta");
						filesTipStr.add(tiposDoc.getPasta());
						String fileArq = emp + "\\" + fil + "\\" + cli + "\\" + tiposDoc.getPasta() + "\\" + dataCpt;
						filesStr.add(fileArq);
						String fileArqTodas = emp + "\\" + fil + "\\TODAS\\" + tiposDoc.getPasta() + "\\" + dataCpt;
						filesStr.add(fileArqTodas);

					}

					Boolean mostrarSite16 = SiteUtils.mostrarDocumento(nCodcli, empNumber, 16L);
					for (String path : filesStr)
					{
						System.out.println(">>>paths");
						if (path != null)
						{
							SiteCompetenciaDocumentoVO competenciaVO = new SiteCompetenciaDocumentoVO();

							//cartao ponto só pode ser publicado nessas empresas caso tenham o tipo de documento cadastrado no sapiens. Tarefa Simples 552482 11/05/2015
							//Incluido a empresa 22 na regra - Solicitação tarefa simples 841859 - Gileno. Alteração realizado por Jorge L. R. FIlho.
							if (empNumber == 1L || empNumber == 7L || empNumber == 15L || empNumber == 17L || empNumber == 19L || empNumber == 21L || empNumber == 22L || empNumber == 29L)
							{
								competenciaVO.setMostraFichaPonto(mostrarSite16);// cartão ponto
							}
							else
							{
								competenciaVO.setMostraFichaPonto(false);
							}

							//System.out.println("mostrar documento" + competenciaVO.getMostraFichaPonto());

							StringBuilder strCpt = new StringBuilder(dataCpt);
							strCpt.insert(dataCpt.length() - 4, '/');
							competenciaVO.setCompetencia(strCpt.toString());

							competenciaVO.setIntnet(intnetCliente);
							competenciaVO.setChkDeacordoRecnf(chkDeacordoRecnf);
							competenciaVO.setChkDeacordoRecdoc(chkDeacordoRecdoc);

							empresaVO.setCodigoEmpresa(Long.parseLong(emp));
							NeoObject empresaObj0 = (NeoObject) PersistEngine.getObject(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("EEMP")).getEntityClass(), new QLEqualsFilter("codemp", empresaVO.getCodigoEmpresa()));
							EntityWrapper empresaWrapper0 = new EntityWrapper(empresaObj0);
							empresaVO.setNomeEmpresa((String) empresaWrapper0.findValue("nomemp"));
							empresaVO.setCodigoFilial(Long.parseLong(fil));

							//System.out.println(clienteObj);
							competenciaVO.setEmpresa(empresaVO);
							competenciaVO.setCodcli(cli);
							//System.out.println(caminho + path);

							if ((NeoUtils.safeIsNull(empNumber) || empNumber != empresaVO.getCodigoEmpresa()) || (NeoUtils.safeIsNull(fil) || !fil.equals(empresaVO.getCodigoFilial().toString())))
							{
								empresaVO.setCodigoEmpresa(Long.parseLong(emp));
								NeoObject empresaObj = (NeoObject) PersistEngine.getObject(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("EEMP")).getEntityClass(), new QLEqualsFilter("codemp", empresaVO.getCodigoEmpresa()));
								EntityWrapper empresaWrapper = new EntityWrapper(empresaObj);
								empresaVO.setNomeEmpresa((String) empresaWrapper.findValue("nomemp"));
								empresaVO.setCodigoFilial(Long.parseLong(fil));
								competenciaVO.setEmpresa(empresaVO);
								competenciaVO.setCodcli(cli);
								//competenciaVO.setCompetencia(dataCpt);
							}

							for (String fileTip : filesTipStr)
							{
								if (path.contains(fileTip))
								{
									competenciaVO.setTipo(fileTip);
								}
							}
							if (dataCptAnt.equals("") || (!dataCpt.equals(dataCptAnt)))
							{
								StringBuilder stringBuilder = new StringBuilder(dataCpt);
								stringBuilder.insert(dataCpt.length() - 4, '/');
								competenciaVO.setCompetencia(stringBuilder.toString());
								dataCptAnt = dataCpt;
							}
							SiteDocumentoVO docs = new SiteDocumentoVO();
							docs.setDescricao(path.replace("\\", "/"));
							docs.setMostraDocumento(true);

							//System.out.println(path);
							if (!path.contains("\\FOPAG\\") && !path.contains("\\GPS\\") && !path.contains("\\GRF\\") && !path.contains("\\OUTROS\\"))
							{ // os documentos acima devem ser para todos os clientes
								//System.out.print("\n[Docs Site]-"+path.replace("\\", "/") + " [" );
								boolean achou = false;
								for (SiteTipoDocumentoVO tipoDoc : tiposDocumentos)
								{
									
									if (path.contains("\\" + tipoDoc.getPasta() + "\\"))
									{
										//System.out.println(SiteUtils.mostrarDocumento(nCodcli, empNumber , tipoDoc.getCodigoSapiens())+"] codcli ="+nCodcli + ", empNumber="+empNumber+", tipDoc="+ tipoDoc.getCodigoSapiens() );
										Long ini = GregorianCalendar.getInstance().getTimeInMillis();
										try{
											SiteDocsMostrarVO vo = new SiteDocsMostrarVO(tipoDoc.getCodigoSapiens(), empNumber, nCodcli );
											
											if ( tiposDocumentosMostrarSite.get(nCodcli) != null && (tiposDocumentosMostrarSite.get(nCodcli)).contains(vo) ){ // se há documentos para mostrar
												docs.setMostraDocumento(true);
											}else{
												docs.setMostraDocumento(false);
											}
										}catch(Exception e){
											e.printStackTrace();
											docs.setMostraDocumento(true);
										}
										
										//docs.setMostraDocumento(SiteUtils.mostrarDocumento(nCodcli, empNumber, tipoDoc.getCodigoSapiens()));
										Long tempo = GregorianCalendar.getInstance().getTimeInMillis() - ini;
										System.out.println(">>>>Tipos documentos 2 " + tempo + " ms");
										achou = true;
										break;
									}
								}
								if (!achou)
								{
									System.out.println("*false]");
								}
							}

							competenciaVO.getDocumentos().add(docs);
							//System.out.println("---"+empNumber+'+'+competenciaVO.getEmpresa().getCodigoEmpresa()+" data:"+competenciaVO.getCompetencia() + " docs:"+competenciaVO.getDocumentos().size());

							source.add(competenciaVO);
							
						}

					} // for filesStr
				}

			}
			if ((source != null) && (!source.isEmpty()))
			{
				String documentosJson = gson.toJson(source);
				//System.out.println("DocumentosSite:" + documentosJson);
				out.print(documentosJson);
			}
			else
			{
				JSONObject json = new JSONObject();
				json.append("status", 0L);
				json.append("msg", "Não há documentos para exibir.");
				out.print(json);
			}

		}

		catch (Exception e)
		{
			e.printStackTrace();
			JSONObject json = new JSONObject();
			json.append("status", 0L);
			json.append("msg", "Não há documentos para exibir.");
			out.print(json);
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

	}

	private void visualizarDocumentosCliente(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws ServletException, IOException, JSONException
	{

		String sessionId = request.getParameter("sessionId");
		String empresa = request.getParameter("emp");
		String filial = request.getParameter("fil");
		String cliente = request.getParameter("cli");
		String competencia = request.getParameter("cpt");
		String tipoDocumento = request.getParameter("tip");
		String documento = request.getParameter("doc");

		SiteUtils siteUtils = new SiteUtils();

		//buscar Sesssao
		NeoObject session = siteUtils.getSession(sessionId);

		if (session == null)
		{
			JSONObject jsonCliente = new JSONObject();

			jsonCliente.append("status", 2L);
			jsonCliente.append("msg", "Sessão inexistente");
			out.print(jsonCliente);
			return;
		}
		FileInputStream fileInputStream = null;
		competencia = competencia.replace("/", "");
		DecimalFormat df = new DecimalFormat("00");
		empresa = df.format(Long.parseLong(empresa));
		String clienteStr = null;
		Long clienteL = Long.parseLong(cliente);
		//		if(!empresa.equals("21") && !empresa.equals("22")){
		//		if (clienteL <= 11)
		//			clienteStr = String.format("%011d", clienteL);
		//		else if (clienteL > 11)
		//			clienteStr = String.format("%014d", clienteL);
		//		}else{
		clienteStr = String.valueOf(clienteL);
		//		}
		try
		{
			String caminho = BASEPATH_BKP01 + "\\Orsegups\\";

			competencia = competencia.replace("/", "");

			String path = caminho + empresa + File.separator + filial + File.separator + clienteStr + File.separator + tipoDocumento + File.separator + competencia + File.separator + documento;

			String pathStr = caminho + empresa + File.separator + filial + File.separator + "TODAS" + File.separator + tipoDocumento + File.separator + competencia + File.separator + documento;

			String filename = documento;

			// set the http content type to "APPLICATION/OCTET-STREAM"

			response.setContentType("APPLICATION/OCTET-STREAM");

			// initialize the http content-disposition header to
			// indicate a file attachment with the default filename

			String disHeader = "Attachment;Filename=\"" + filename + "\"";
			response.setHeader("Content-Disposition", disHeader);

			// transfer the file byte-by-byte to the response object

			File fileToDownload = new File(path);
			File fileToDownloadAll = new File(pathStr);

			boolean existefile = fileToDownload.exists();
			boolean existefileAll = fileToDownloadAll.exists();

			if (existefile)
			{
				fileInputStream = new FileInputStream(fileToDownload);
			}
			else if (existefileAll)
			{

				fileInputStream = new FileInputStream(fileToDownloadAll);
			}
			else
			{
				JSONObject jsonCliente = new JSONObject();

				jsonCliente.append("status", 0L);
				jsonCliente.append("msg", "Erro o caminho  não existe!");
				out.print(jsonCliente);
				return;
			}

			int i;
			while ((i = fileInputStream.read()) != -1)
			{
				out.write(i);
			}

		}
		catch (Exception e)
		{ // file IO error
			e.printStackTrace();
			JSONObject jsonCliente = new JSONObject();
			jsonCliente.append("status", 0L);
			jsonCliente.append("msg", "Erro ao efetuar download.");
			out.print(jsonCliente);
		}
		finally
		{
			if (NeoUtils.safeIsNotNull(out))
				out.flush();
			if (NeoUtils.safeIsNotNull(fileInputStream))
				fileInputStream.close();
			if (NeoUtils.safeIsNotNull(out))
				out.close();
		}
	}

	public static List<String> competencias(GregorianCalendar calendar)
	{
		List<String> cptList = null;
		if (NeoUtils.safeIsNotNull(calendar))
		{
			cptList = new ArrayList<String>();
			Calendar dia = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
			for (int i = 0; i < 6; i++)
			{
				String diaStr = dateFormat.format(dia.getTime());
				cptList.add(diaStr);
				dia.add(Calendar.MONTH, -1);
			}

		}
		return cptList;
	}

	public void listaSolicitacao(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException, IOException
	{
		String solicitacaoJson = "";
		String sessionId = request.getParameter("sessionId");
		String codCli = request.getParameter("codCli");
		List<SolicitacaoVO> solicitacoes = null;
		List<NeoObject> clientesSapiens = null;
		List<SiteSolicitacaoVO> solicitacaoVOs = null;
		JSONObject json = null;
		try
		{

			SiteUtils siteUtils = new SiteUtils();
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{
				json = new JSONObject();

				json.append("status", 2L);
				json.append("msg", "Sessão inexistente ");
				out.print(json);
				return;
			}
			QLEqualsFilter filter = new QLEqualsFilter("cgccpf", Long.valueOf(codCli));
			clientesSapiens = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filter);
			if (clientesSapiens != null && !clientesSapiens.isEmpty())
			{

				solicitacaoVOs = new ArrayList<SiteSolicitacaoVO>();
				for (NeoObject neoObject : clientesSapiens)
				{
					EntityWrapper entityWrapper = new EntityWrapper(neoObject);
					String codCliStr = String.valueOf(entityWrapper.findValue("nomcli"));
					codCli = String.valueOf(entityWrapper.findValue("codcli"));
					SiteSolicitacaoVO solicitacaoVO = new SiteSolicitacaoVO();
					solicitacoes = new ArrayList<SolicitacaoVO>();
					solicitacoes = SiteUtils.listaSolicitacoes(Long.parseLong(codCli));
					solicitacaoVO.setCodigoCliente(codCliStr);
					solicitacaoVO.setSolicitacoes(solicitacoes);
					solicitacaoVOs.add(solicitacaoVO);
				}

			}

			if (solicitacoes == null || solicitacoes.isEmpty())
			{
				json = new JSONObject();
				json.append("status", 0L);
				json.append("msg", "Não foi possível carregar seus registros de solicitações. Entre em contato com a nossa Central de Relacionamento");
				out.print(json);
			}
			else if (solicitacoes != null && !solicitacoes.isEmpty())
			{
				Gson gson = new Gson();
				solicitacaoJson = gson.toJson(solicitacaoVOs);

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			json = new JSONObject();
			json.append("status", 0L);
			json.append("msg", "Não foi possível carregar suas solicitações. Entre em contato com a nossa Central de Relacionamento");

			out.print(json);
		}
		//System.out.println("Minhas Solicitações: " + solicitacaoJson);
		out.print(solicitacaoJson);
		out.flush();
		out.close();
	}

	public void listaReclamacao(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException, IOException
	{

		String reclamacaoJson = "";
		String sessionId = request.getParameter("sessionId");
		String codCli = request.getParameter("codCli");
		List<NeoObject> clientesSapiens = null;
		List<ReclamacaoVO> reclamacoes = null;
		List<SiteReclamacaoVO> reclamacaoVOs = null;
		JSONObject json = null;
		try
		{
			SiteUtils siteUtils = new SiteUtils();
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{
				json = new JSONObject();

				json.append("status", 2L);
				json.append("msg", "Sessão inexistente");
				out.print(json);

				return;
			}
			QLEqualsFilter filter = new QLEqualsFilter("cgccpf", Long.valueOf(codCli));
			clientesSapiens = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filter);
			if (clientesSapiens != null && !clientesSapiens.isEmpty())
			{
				reclamacaoVOs = new ArrayList<SiteReclamacaoVO>();
				int contador = 0;
				for (NeoObject neoObject : clientesSapiens)
				{
					EntityWrapper entityWrapper = new EntityWrapper(neoObject);
					String codCliStr = String.valueOf(entityWrapper.findValue("codcli"));
					String nomCliStr = String.valueOf(entityWrapper.findValue("nomcli"));
					SiteReclamacaoVO reclamacaoVO = new SiteReclamacaoVO();
					reclamacoes = new ArrayList<ReclamacaoVO>();
					reclamacoes = SiteUtils.listaReclamacao(Long.parseLong(codCliStr));
					reclamacaoVO.setCodigoCliente(nomCliStr);
					reclamacaoVO.setReclamacoes(reclamacoes);
					reclamacaoVOs.add(reclamacaoVO);
					contador++;
				}

			}

			if (reclamacoes == null || reclamacoes.isEmpty())
			{
				json = new JSONObject();
				json.append("status", 0L);
				json.append("msg", "Não foi possível carregar seus registros de reclamações. Entre em contato com a nossa Central de Relacionamento");
				out.print(json);
			}
			else if (reclamacoes != null && !reclamacoes.isEmpty())
			{
				Gson gson = new Gson();
				reclamacaoJson = gson.toJson(reclamacaoVOs);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			json = new JSONObject();
			json.append("status", 0L);
			json.append("msg", "Não foi possível carregar suas reclamações. Entre em contato com a nossa Central de Relacionamento");
			out.print(json);
		}
		out.print(reclamacaoJson);
		out.flush();
		out.close();
	}

	private void listarPresenca(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		try
		{
			System.out.println("filtroHistorico->" + request.getParameter("filtro"));
			GregorianCalendar dataFiltro = new GregorianCalendar();
			String filtroData = NeoUtils.safeOutputString(request.getParameter("filtro"));
			if (!filtroData.equals(""))
			{
				String horaData[] = filtroData.split(" ");

				if (horaData[0] != null)
				{
					String anoMesDia[] = horaData[0].split("-");
					dataFiltro.set(GregorianCalendar.YEAR, NeoUtils.safeInteger(anoMesDia[0]));
					dataFiltro.set(GregorianCalendar.MONTH, NeoUtils.safeInteger(anoMesDia[1]) - 1);
					dataFiltro.set(GregorianCalendar.DATE, NeoUtils.safeInteger(anoMesDia[2]));
				}
				if (horaData[1] != null)
				{
					String horaMinuto[] = horaData[1].split(":");
					dataFiltro.set(GregorianCalendar.HOUR_OF_DAY, NeoUtils.safeInteger(horaMinuto[0]));
					dataFiltro.set(GregorianCalendar.MINUTE, NeoUtils.safeInteger(horaMinuto[1]));
				}
			}

			System.out.println(NeoUtils.safeDateFormat(dataFiltro, "dd/MM/yyyy"));

			String sessionId = request.getParameter("sessionId");
			SiteUtils siteUtils = new SiteUtils();

			//buscar Sesssao
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{
				JSONObject jsonCliente = new JSONObject();
				jsonCliente.append("status", 2L);
				jsonCliente.append("msg", "Sessão inexistente");
				out.print(jsonCliente);
				return;
			}

			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long cpf = (Long) sessionWrapper.getValue("codigo");

			QLGroupFilter filterUser = new QLGroupFilter("AND");
			filterUser.addFilter(new QLEqualsFilter("cgccpf", cpf));
			//filterUser.addFilter(new QLEqualsFilter("sitcli", "A"));

			//View de Contas
			List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterUser);
			Long codcli = 0L;

			if (users != null && !users.isEmpty())
			{
				EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
				codcli = (Long) usuarioCliente.getValue("codcli");
			}

			Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
			//FIXME removido pela neomind pois o referido metodo nao estava mais disponivel no projeto
			List<EscalaVO> escalas = QLPresencaUtils.getEscalasColaboradores(dataFiltro, 0L, 0L, false);

			log.warn("getEscalasColaboradores - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

			List<ColaboradorVO> colaboradores = siteUtils.getListaColaboradores(escalas, dataFiltro, codcli);
			List<EntradaAutorizadaVO> entradasAutorizadas = siteUtils.getListaEntradasAutorizadas(colaboradores, dataFiltro, escalas, codcli);
			System.out.println("PORTAL1:"+entradasAutorizadas);
			QLPresencaUtils.atualizaStatusColaboradores(colaboradores, entradasAutorizadas);
			List<ClienteVO> clientes = siteUtils.getClientes(colaboradores, entradasAutorizadas, dataFiltro, codcli, null);
			System.out.println("PORTAL2:"+clientes);
			for (ClienteVO c : clientes)
			{
				System.out.println("PORTAL3:"+c.getCodigoCliente());
				c.setIsPresencaLiberado((SiteUtils.isPresencaLiberadoSite(c.getCodigoCliente()) ? "1" : "0"));
			}
			Gson gson = new Gson();
			String cliJson = gson.toJson(clientes);
			System.out.println(cliJson);
			out.print(cliJson);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void listaDadosGeraisPosto(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		try
		{
			Long usu_numpos = null;
			if (request.getParameter("numposto") != null && !request.getParameter("numposto").equals(""))
			{
				usu_numpos = Long.parseLong(request.getParameter("numposto"));
			}
			/*
			 * Para as chamadas do site via ajax, usar este cabeçalho.
			 */
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "POST");
			response.setHeader("Access-Control-Allow-Headers", "Content-Type");
			response.setHeader("Access-Control-Max-Age", "86400");
			response.setContentType("application/json");

			String sessionId = request.getParameter("sessionId");
			SiteUtils siteUtils = new SiteUtils();

			//buscar Sesssao
			NeoObject session = siteUtils.getSession(sessionId);

			if (session == null)
			{
				JSONObject jsonCliente = new JSONObject();
				jsonCliente.append("status", 2L);
				jsonCliente.append("msg", "Sessão inexistente");
				out.print(jsonCliente);
				return;
			}

			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long cpf = (Long) sessionWrapper.getValue("codigo");

			QLGroupFilter filterUser = new QLGroupFilter("AND");
			filterUser.addFilter(new QLEqualsFilter("cgccpf", cpf));
			//filterUser.addFilter(new QLEqualsFilter("sitcli", "A"));

			//View de Contas
			List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterUser);
			Long codcli = 0L;
			String fonecli = "";
			String cpfcnpj = "";

			if (users != null && !users.isEmpty())
			{
				EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
				codcli = (Long) usuarioCliente.getValue("codcli");
				fonecli = NeoUtils.safeOutputString(usuarioCliente.getValue("foncli"));
				cpfcnpj = NeoUtils.safeOutputString(usuarioCliente.getValue("cgccpf"));
			}
			GregorianCalendar dataAtual = new GregorianCalendar();

			List<EscalaVO> escalas = new ArrayList<EscalaVO>();
			List<ColaboradorVO> colaboradores = new ArrayList<ColaboradorVO>(); //siteUtils.getListaColaboradores(escalas, dataAtual, codcli);

			List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>(); //siteUtils.getListaEntradasAutorizadas(colaboradores, dataAtual, escalas, codcli);

			QLPresencaUtils.atualizaStatusColaboradores(colaboradores, entradasAutorizadas);
			List<ClienteVO> clientes = siteUtils.getClientes(colaboradores, entradasAutorizadas, dataAtual, codcli, usu_numpos);
			for (ClienteVO v : clientes)
			{
				v.setTelefone(fonecli);
				v.setCpfCnpj(cpfcnpj);
			}
			Gson gson = new Gson();
			String cliJson = gson.toJson(clientes);
			//System.out.println(cliJson);
			out.print(cliJson);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	public void listaEventoX8(String numctr, String numpos, HttpServletResponse response) throws JSONException, IOException
	{

		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		List<EventoSigmaVO> eventos = new ArrayList<EventoSigmaVO>();
		eventos = QLPresencaUtils.listaEventoX8(Long.parseLong(numctr), Long.parseLong(numpos));

		Gson gson = new Gson();
		String eventoJson = gson.toJson(eventos);
		//System.out.println(eventoJson);
		out.print(eventoJson);
		out.flush();
		out.close();
	}

	public void listaCoberturaPosto(String numloc, String taborg, HttpServletResponse response) throws JSONException, IOException
	{

		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		List<EntradaAutorizadaVO> coberturas = new ArrayList<EntradaAutorizadaVO>();
		coberturas = QLPresencaUtils.listaCoberturaPosto(Long.parseLong(numloc), Long.parseLong(taborg));

		JSONArray jsonCoberturas = new JSONArray();

		for (EntradaAutorizadaVO cob : coberturas)
		{
			JSONObject jsonCobertura = new JSONObject();
			jsonCobertura.put("tipcob", cob.getDescricaoCobertura());
			jsonCobertura.put("percob", NeoUtils.safeDateFormat(cob.getDataInicial(), "dd/MM/yyyy") + " à " + NeoUtils.safeDateFormat(cob.getDataFinal(), "dd/MM/yyyy"));
			jsonCobertura.put("nomcob", cob.getColaborador().getNumeroEmpresa() + "/" + cob.getColaborador().getNumeroCadastro() + " - " + cob.getColaborador().getNomeColaborador());
			if (cob.getColaboradorSubstituido().getNomeColaborador() != null)
			{
				jsonCobertura.put("nomsub", cob.getColaboradorSubstituido().getNumeroEmpresa() + "/" + cob.getColaboradorSubstituido().getNumeroCadastro() + " - " + cob.getColaboradorSubstituido().getNomeColaborador());
			}
			jsonCobertura.put("obscob", cob.getObservacao());

			jsonCoberturas.put(jsonCobertura);
		}
		//System.out.println(jsonCoberturas);
		out.print(jsonCoberturas);
		out.flush();
		out.close();
	}

	public void listaDadosGeraisColaborador(String numcad, String numemp, String tipcol, HttpServletResponse response) throws JSONException, IOException
	{

		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();

		//JSONArray jsonDadosGeraisColaborador = new JSONArray();

		ColaboradorVO colaborador = QLPresencaUtils.buscaFichaColaborador(NeoUtils.safeLong(numemp), NeoUtils.safeLong(tipcol), NeoUtils.safeLong(numcad), new GregorianCalendar());
		colaborador.setStrDataNascimento(NeoUtils.safeDateFormat(colaborador.getDataNascimento(), "dd/MM/yyyy"));
		colaborador.setStrDataAdmissao(NeoUtils.safeDateFormat(colaborador.getDataAdmissao(), "dd/MM/yyyy"));

		Gson gson = new Gson();
		String jcolaborador = gson.toJson(colaborador);

		//System.out.println(jcolaborador);
		out.print(jcolaborador);
		out.flush();
		out.close();
	}

	private void carregaFotoColaborador(String numcad, String numemp, String tipcol, HttpServletResponse response)
	{
		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */

		OutputStream o = null;

		PreparedStatement st = null;
		try
		{
			String nomeFonteDados = "VETORH";
			Connection conn = PersistEngine.getConnection(nomeFonteDados);
			byte[] imgData = null;

			StringBuffer sqlFoto = new StringBuffer();
			sqlFoto.append(" SELECT FotEmp FROM R034FOT WHERE NumEmp = " + numemp + " AND TipCol = " + tipcol + " AND NumCad = " + numcad);
			st = conn.prepareStatement(sqlFoto.toString());
			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				imgData = rs.getBytes("FotEmp");

				response.setHeader("Access-Control-Allow-Origin", "*");
				response.setHeader("Access-Control-Allow-Methods", "GET");
				response.setHeader("Access-Control-Allow-Headers", "Content-Type");
				response.setHeader("Access-Control-Max-Age", "86400");
				response.setContentType("image/jpeg");
				response.setContentLength(imgData.length);

				o = response.getOutputStream();
				o.write(imgData);
				o.flush();
				o.close();
			}
			rs.close();
			st.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void listaAfastamentos(String numemp, String tipcol, String numcad, String numLocPosto, HttpServletResponse response) throws JSONException, IOException
	{

		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();

		GregorianCalendar datCorte = SiteUtils.retornaDataCorteColaborador(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), Long.parseLong(numLocPosto));
		if (datCorte == null)
		{
			datCorte = new GregorianCalendar();
		}

		List<AfastamentoVO> afastamentos = new ArrayList<AfastamentoVO>();
		afastamentos = SiteUtils.listaAfastamentos(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), datCorte);

		JSONArray jsonAfastamentos = new JSONArray();

		for (AfastamentoVO afa : afastamentos)
		{
			JSONObject jsonAfastamento = new JSONObject();
			jsonAfastamento.put("numemp", afa.getColaborador().getNumeroEmpresa());
			jsonAfastamento.put("tipcol", afa.getColaborador().getTipoColaborador());
			jsonAfastamento.put("numcad", afa.getColaborador().getNumeroCadastro());
			jsonAfastamento.put("datafa", NeoUtils.safeDateFormat(afa.getDataInicio(), "dd/MM/yyyy"));
			jsonAfastamento.put("datter", NeoUtils.safeDateFormat(afa.getDataFinal(), "dd/MM/yyyy"));
			jsonAfastamento.put("obsafa", afa.getObservacao());
			jsonAfastamento.put("dessit", afa.getSituacao());
			jsonAfastamento.put("link", afa.getLink());
			jsonAfastamento.put("excluir", afa.getExcluir());
			jsonAfastamentos.put(jsonAfastamento);
		}
		//System.out.println(jsonAfastamentos);
		out.print(jsonAfastamentos);
		out.flush();
		out.close();
	}

	public void listaCoberturas(String numemp, String tipcol, String numcad, String numLocPosto, HttpServletResponse response) throws JSONException, IOException//foi este dd/MM/yyyy HH:mm
	{

		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();

		GregorianCalendar datCorte = SiteUtils.retornaDataCorteColaborador(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), Long.parseLong(numLocPosto));
		if (datCorte == null)
		{
			datCorte = new GregorianCalendar();
		}

		List<EntradaAutorizadaVO> coberturas = new ArrayList<EntradaAutorizadaVO>();
		coberturas = SiteUtils.listaCoberturas(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), datCorte);

		JSONArray jsonCoberturas = new JSONArray();

		for (EntradaAutorizadaVO cob : coberturas)
		{
			JSONObject jsonCobertura = new JSONObject();
			jsonCobertura.put("tipcob", cob.getDescricaoCobertura());

			if (cob.getColaboradorSubstituido().getNomeColaborador() != null)
			{
				jsonCobertura.put("nomcob", cob.getColaboradorSubstituido().getNumeroEmpresa() + "/" + cob.getColaboradorSubstituido().getNumeroCadastro() + " - " + cob.getColaboradorSubstituido().getNomeColaborador());
			}

			jsonCobertura.put("percob", NeoUtils.safeDateFormat(cob.getDataInicial(), "dd/MM/yyyy HH:mm") + " à " + NeoUtils.safeDateFormat(cob.getDataFinal(), "dd/MM/yyyy HH:mm"));
			jsonCobertura.put("esccob", cob.getColaboradorSubstituido().getEscala().getDescricao());
			jsonCobertura.put("loccob", cob.getPosto().getLotacao() + " - " + cob.getPosto().getNomePosto() + " - CC: " + cob.getPosto().getCentroCusto());
			jsonCobertura.put("obscob", cob.getObservacao());

			jsonCoberturas.put(jsonCobertura);
		}
		//System.out.println(jsonCoberturas);
		out.print(jsonCoberturas);
		out.flush();
		out.close();
	}

	public void listaHistoricoEscala(String numemp, String tipcol, String numcad, String numLocPosto, HttpServletResponse response) throws JSONException, IOException
	{
		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();

		GregorianCalendar datCorte = SiteUtils.retornaDataCorteColaborador(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), Long.parseLong(numLocPosto));
		if (datCorte == null)
		{
			datCorte = new GregorianCalendar();
		}

		List<HistoricoEscalaVO> escalas = new ArrayList<HistoricoEscalaVO>();
		escalas = SiteUtils.listaHistoricoEscala(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), datCorte);

		JSONArray jsonEscalas = new JSONArray();

		for (HistoricoEscalaVO esc : escalas)
		{
			JSONObject jsonEscala = new JSONObject();
			jsonEscala.put("numemp", esc.getColaborador().getNumeroEmpresa());
			jsonEscala.put("tipcol", esc.getColaborador().getTipoColaborador());
			jsonEscala.put("numcad", esc.getColaborador().getNumeroCadastro());
			jsonEscala.put("codesc", esc.getEscala().getCodigoEscala());
			jsonEscala.put("desesc", esc.getEscala().getDescricao());
			jsonEscala.put("datalt", NeoUtils.safeDateFormat(esc.getData(), "dd/MM/yyyy"));
			jsonEscala.put("excluir", esc.getExcluir());

			jsonEscalas.put(jsonEscala);
		}
		//System.out.println(jsonEscalas);
		out.print(jsonEscalas);
		out.flush();
		out.close();
	}

	public void listaHistoricoLocal(String numemp, String tipcol, String numcad, String numLocPosto, HttpServletResponse response) throws JSONException, IOException
	{

		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		List<HistoricoLocalVO> locais = new ArrayList<HistoricoLocalVO>();

		GregorianCalendar datCorte = SiteUtils.retornaDataCorteColaborador(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), Long.parseLong(numLocPosto));
		if (datCorte == null)
		{
			datCorte = new GregorianCalendar();
		}

		locais = SiteUtils.listaHistoricoLocal(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), datCorte);

		JSONArray jsonLocais = new JSONArray();

		for (HistoricoLocalVO loc : locais)
		{
			JSONObject jsonLocal = new JSONObject();
			jsonLocal.put("numemp", loc.getColaborador().getNumeroEmpresa());
			jsonLocal.put("tipcol", loc.getColaborador().getTipoColaborador());
			jsonLocal.put("numcad", loc.getColaborador().getNumeroCadastro());
			jsonLocal.put("posto", loc.getPosto().getLotacao() + " - " + loc.getPosto().getNomePosto());
			jsonLocal.put("cc", loc.getPosto().getCentroCusto());
			jsonLocal.put("taborg", loc.getPosto().getCodigoOrganograma());
			jsonLocal.put("datalt", NeoUtils.safeDateFormat(loc.getData(), "dd/MM/yyyy"));

			jsonLocais.put(jsonLocal);
		}
		//System.out.println(jsonLocais);
		out.print(jsonLocais);
		out.flush();
		out.close();
	}

	public void listaAcessos(String numcpf, String competencia, String exibirExcecao, HttpServletResponse response) throws JSONException, IOException, ParseException
	{

		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		List<AcessoVO> acessos = new ArrayList<AcessoVO>();
		GregorianCalendar perRef = new GregorianCalendar();
		GregorianCalendar dataInicio = new GregorianCalendar();
		GregorianCalendar dataFim = new GregorianCalendar();

		if (competencia == null || competencia.equals(""))
		{
			competencia = OrsegupsUtils.getCompetencia(dataInicio);
		}

		//competencia = "01/2013";

		CompetenciaVO cpt = QLPresencaUtils.getCompetenciaSiteR044Cal(competencia);
		dataInicio = OrsegupsUtils.getDiaInicioCompetencia(competencia);
		dataFim = OrsegupsUtils.getDiaFimCompetencia(competencia);
		perRef = cpt.getPerRef();

		acessos = QLPresencaUtils.listaAcessos(Long.parseLong(numcpf), perRef, dataInicio, dataFim, exibirExcecao);

		JSONArray jsonAcessos = new JSONArray();

		for (AcessoVO acc : acessos)
		{
			JSONObject jsonAcesso = new JSONObject();
			jsonAcesso.put("dataAnt", NeoUtils.safeDateFormat(acc.getData()));
			jsonAcesso.put("dataNew", NeoUtils.safeDateFormat(acc.getData()));
			jsonAcesso.put("diaSemana", OrsegupsUtils.getDayOfWeek(acc.getData()));
			jsonAcesso.put("sequencia", acc.getSequencia());
			jsonAcesso.put("direcao", acc.getDirecao());
			jsonAcesso.put("tipoAcesso", acc.getTipoAcesso());
			jsonAcesso.put("tipoDirecao", acc.getTipoDirecao());
			jsonAcesso.put("relogio", acc.getRelogio());
			jsonAcesso.put("numeroLocal", acc.getNumeroLocal());
			jsonAcesso.put("codigoOrganograma", acc.getCodigoOrganograma());
			jsonAcesso.put("permanenciaPosto", acc.getPermanenciaPosto());
			jsonAcesso.put("fone", acc.getFone());
			jsonAcesso.put("excluir", acc.getExcluir());
			jsonAcessos.put(jsonAcesso);
		}
		//System.out.println(jsonAcessos);
		out.print(jsonAcessos);
		out.flush();
		out.close();
	}

	public void listaCompetencias(HttpServletResponse response)
	{

		/*
		 * Para as chamadas do site via ajax, usar este cabeçalho.
		 */
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out;
		try
		{
			out = response.getWriter();
			String competenciaAtual = OrsegupsUtils.getCompetencia(new GregorianCalendar());

			JSONArray jsonCPTs = new JSONArray();

			JSONObject jsonCPT = new JSONObject();
			jsonCPT.put("cpt", competenciaAtual);

			jsonCPTs.put(jsonCPT);

			System.out.println(jsonCPTs);
			out.print(jsonCPTs);
			out.flush();
			out.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void listaSolicitacaoFichaPosto(Long codcli, HttpServletResponse response) throws JSONException, IOException
	{
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		List<SolicitacaoVO> solicitacoes = new ArrayList<SolicitacaoVO>();
		solicitacoes = QLPresencaUtils.listaSolicitacoes(codcli);

		Gson gson = new Gson();
		String solicitacaoJson = gson.toJson(solicitacoes);

		//System.out.println(solicitacaoJson);
		out.print(solicitacaoJson);
		out.flush();
		out.close();
	}

	public void listaReclamacaoFichaPosto(Long codcli, HttpServletResponse response) throws JSONException, IOException
	{
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		List<ReclamacaoVO> reclamacoes = new ArrayList<ReclamacaoVO>();
		reclamacoes = QLPresencaUtils.listaReclamacao(codcli);

		Gson gson = new Gson();
		String reclamacaoJson = gson.toJson(reclamacoes);

		System.out.println(reclamacaoJson);
		out.print(reclamacaoJson);
		out.flush();
		out.close();
	}

	public void buscaContraCheque(String numcpf, String competencia, HttpServletResponse response) throws JSONException, IOException, ParseException
	{
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		ContraChequeVO contraCheque = new ContraChequeVO();

		//competencia = "01/2013";

		if (competencia != null && !competencia.equals(""))
		{
			if (NeoUtils.safeOutputString(competencia).equals("") || competencia.equals("0"))
			{
				GregorianCalendar gc = new GregorianCalendar();
				gc.add(GregorianCalendar.MONTH, -1);
				competencia = SiteUtils.getCompetencia(gc, "MM/yyyy");
			}
			System.out.println("CPT contracheque: " + competencia);
			contraCheque = QLPresencaUtils.buscaContraCheque(Long.parseLong(numcpf), OrsegupsUtils.stringToGregorianCalendar("01/" + competencia, "dd/MM/yyyy"));
		}

		Gson gson = new Gson();
		String chqJson = gson.toJson(contraCheque);
		System.out.println(chqJson);

		out.print(chqJson);
		out.flush();
		out.close();
	}

	public void listaCompetenciasFinancas(String numcpf, HttpServletResponse response) throws JSONException, IOException
	{
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		List<String> competencias = new ArrayList<String>();
		competencias = QLPresencaUtils.listaCompetenciasFinacas(Long.parseLong(numcpf));

		JSONArray jsonCompetencias = new JSONArray();

		String selected = "";

		for (String comp : competencias)
		{
			JSONObject jsonCompetencia = new JSONObject();
			jsonCompetencia.put("competencia", comp);

			if (selected.equals(""))
			{
				selected = "selected";
				jsonCompetencia.put(selected, "true");
			}
			jsonCompetencias.put(jsonCompetencia);
		}
		System.out.println(jsonCompetencias);
		out.print(jsonCompetencias);
		out.flush();
		out.close();
	}

	private void alterarAceiteNFDOC(String codcli, String numcpf, String email, Boolean chk_deacordo_recnf, Boolean chk_deacordo_recdoc, PrintWriter out)
	{

		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		System.out.println("[ACEITE NFDOC] [" + key + "] - CPF: " + numcpf.replace(".", "").replace("/", "").replace("-", ""));
		System.out.println("[ACEITE NFDOC] [" + key + "] -Email:" + email);
		System.out.println("[ACEITE NFDOC] [" + key + "] -chk_deacordo_recnf: [" + chk_deacordo_recnf + "]");
		//System.out.println("[ACEITE NFDOC] ["+key+"] -chk_deacordo_recdoc: [" + chk_deacordo_recdoc+"]");

		//recuperar o email original do cliente
		String intnetOriginal = NeoUtils.safeOutputString(SiteUtils.recuperaIntnetcliente(NeoUtils.safeLong(codcli)));
		System.out.println("[ACEITE NFDOC] [" + key + "] - Email Original do cliente" + intnetOriginal);
		String tarefaRSC = null;
		boolean abrirRSC = false;
		boolean abriuRSC = true;
		try
		{

			List<Long> contratos = SiteUtils.getContratosAtivosCliente(NeoUtils.safeLong(codcli));
			if (chk_deacordo_recnf)
			{
				//atualizar o email intnet na tabel e085cli
				if (!NeoUtils.safeOutputString(email).equals(""))
				{

					RSCVO rsc = new RSCVO();
					rsc = SiteUtils.retornaDadosClienteRSC(NeoUtils.safeLong(codcli));

					if (contratos.size() > 1)
					{ // abrir rsc para o NAC efetuar as alterações manualmente.
						abrirRSC = true;

						rsc.setTipoContato("12430375");

						GregorianCalendar gcPrazo = new GregorianCalendar();
						gcPrazo.add(GregorianCalendar.DATE, 1);
						gcPrazo.set(GregorianCalendar.HOUR, 23);
						gcPrazo.set(GregorianCalendar.MINUTE, 59);
						gcPrazo.set(GregorianCalendar.SECOND, 59);

						rsc.setPrazo(OrsegupsUtils.getNextWorkDay(gcPrazo));

						StringBuilder msg = new StringBuilder();

						msg.append("<p>Verificar e adicionar e-mail " + email + " no cadastro do cliente e contratos</p>");

						msg.append("<P>Alterar a forma de envio dos contratos abaixo para 1 : ");

						StringBuilder ctrs = new StringBuilder();
						for (Long num : contratos)
						{
							ctrs.append("," + num);
						}
						msg.append(ctrs.toString().substring(1));
						msg.append("</p>");

						msg.append("<P>Obs.: Tarefa gerada automaticamente pois o cliente aceitou pelo site em receber nota fiscal, boleto e documentos relativos a comprovação dos serviços prestados, no Portal do Cliente </p>");
						rsc.setMensagem(msg.toString());

						try
						{
							rsc.setEmail(email);
							tarefaRSC = RSCHelper.abrirRSC(rsc, "SITERESPONSAVELRSCCADASTRAL", true);
							abriuRSC = true;
						}
						catch (Exception e)
						{
							System.out.println("[ACEITE NFDOC] [" + key + "] -Erro ao abrir rsc de atualização cadastral " + e.getMessage());
							e.printStackTrace();
						}

					}
					else
					{ // senão, atualiza(adiciona ao final do existente) direto no contrato
						Boolean salvou = SiteUtils.atualizaEmailCli(NeoUtils.safeLong(codcli), rsc.getEmail() + ";" + email);
						if (salvou)
							System.out.println("[ACEITE NFDOC] [" + key + "] - Email do cliente salvo com sucesso.");

						//gravar log de alteração
						Boolean gravouLog = SiteUtils.gravaLogAlteracaoCli(NeoUtils.safeLong(codcli), numcpf, intnetOriginal + "-->" + email, new GregorianCalendar());
						if (gravouLog)
							System.out.println("[ACEITE NFDOC] [" + key + "] - Log do cliente salvo com sucesso." + intnetOriginal + "-->" + email);

						Boolean atualisouCodFen = SiteUtils.atualizaCodFenUsut160CTR(NeoUtils.safeLong(codcli));
						if (atualisouCodFen)
							System.out.println("[ACEITE NFDOC] [" + key + "] - Usu_codfen dos contratos deste cliente atualizados para 1.");

						Boolean atualisouEmactr = SiteUtils.atualizaEmailContratos(NeoUtils.safeLong(codcli), email);
						if (atualisouEmactr)
							System.out.println("[ACEITE NFDOC] [" + key + "] - Email do contrato atualizado.");

						SiteUtils.inserirObservacaoContrato(NeoUtils.safeLong(codcli));
					}

				}
				else
				{
					System.out.println("[ACEITE NFDOC] [" + key + "] - Email do cliente vazio, por isso não foi atualizado.");
				}

			}
			//registrar no eform-dinamico o status das escolhas Eform: siteAceite[numcpf, codcli, dataAtualização, chk_deacordo_recnf, chk_deacordo_recdoc ]
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("codcli", NeoUtils.safeLong(codcli)));
			ArrayList<NeoObject> aceitesExistentes = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SiteAceiteNFDOC"), gp);

			if (aceitesExistentes != null && !aceitesExistentes.isEmpty())
			{
				for (NeoObject oAceite : aceitesExistentes)
				{

					EntityWrapper wAceite = new EntityWrapper(oAceite);

					wAceite.setValue("codcli", NeoUtils.safeLong(codcli));
					wAceite.setValue("recnf", chk_deacordo_recnf);
					wAceite.setValue("recdoc", chk_deacordo_recdoc);
					wAceite.setValue("intnet", email);
					wAceite.setValue("dataAceite", new GregorianCalendar());

					if (abrirRSC)
					{
						wAceite.setValue("rsc", tarefaRSC);
					}

					PersistEngine.persist(oAceite);
				}
			}
			else
			{
				NeoObject oAceite = AdapterUtils.createNewEntityInstance("SiteAceiteNFDOC");
				EntityWrapper wAceite = new EntityWrapper(oAceite);

				wAceite.setValue("codcli", NeoUtils.safeLong(codcli));
				wAceite.setValue("recnf", chk_deacordo_recnf);
				wAceite.setValue("recdoc", chk_deacordo_recdoc);
				wAceite.setValue("intnet", email);

				if (abrirRSC)
				{
					wAceite.setValue("rsc", tarefaRSC);
				}

				PersistEngine.persist(oAceite);

			}

			JSONObject json = new JSONObject();
			json.append("status", 1L);
			json.append("msg", "Aceite processado com sucesso.");
			out.print(json);
		}
		catch (Exception e)
		{
			System.out.println("Erro ao processar aceite");
			e.printStackTrace();
			try
			{
				JSONObject json = new JSONObject();
				json.append("status", 0L);
				json.append("msg", "Erro ao processar aceite.");
				out.print(json);
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
			}

		}

	}

	private void imprimirFichasPonto(Long codcli, boolean numemp, boolean numcad, boolean tipcol, String perref, HttpServletResponse response)
	{

		OutputStream out = null;
		try
		{
			System.out.println("codcli=[" + codcli + "], perref=[" + perref + "]");

			String sContentType = "application/pdf";
			response.setContentType(sContentType);
			//response.addHeader("content-disposition", "attachment; filename=" + "FichaPonto");
			//response.setCharacterEncoding("ISO-8859-1" );

			InputStream in = null;

			File fileRelatorio = SiteUtils.geraFichaPontoPDF(codcli, null, null, null, perref);

			in = new BufferedInputStream(new FileInputStream(fileRelatorio));
			if (fileRelatorio.exists())
			{
				response.setContentLength((int) in.available());
				out = response.getOutputStream();
				int l;
				byte b[] = new byte[1024];
				while ((l = in.read(b, 0, b.length)) != -1)
				{
					out.write(b, 0, l);
				}
				out.flush();
				out.close();
				in.close();
			}
			else
			{
				System.out.println("[Area ClientServlet]-Download de Arquivo de ficha ponto invalido");
			}
		}
		catch (Exception e)
		{
			System.out.println("[Area ClientServlet]- Erro Baixando o arquivo de ficha ponto");
			e.printStackTrace();
		}

	}

	public void listaSupervisor(ColaboradorVO supervisor, HttpServletResponse response) throws JSONException, IOException
	{
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		String jcolaborador = gson.toJson(supervisor);
		System.out.println(jcolaborador);
		out.print(jcolaborador);
		out.flush();
		out.close();
	}
	
	/**
	 * Este método é chamado apenas pelo portal orsegups mobile para abrir as RSCs/RRCs via aplicativo.
	 * @param request
	 * @param response
	 * @param out
	 * @throws JSONException
	 */
	private void iniciarSolicitacaoViaMobile(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		try
		{
			String xAuthToken = request.getHeader("X-AUTH-TOKEN");
			if(xAuthToken.equals("499c7a9a40f3bcda2c54471d03f494dc")){
				Long cpf = null;
				
				NeoObject clienteObj = null;
				Long codcli = null;
				
				try{
					cpf = Long.parseLong(request.getParameter("cpfCnpj"));
					
					QLGroupFilter filterUser = new QLGroupFilter("AND");
					filterUser.addFilter(new QLEqualsFilter("usu_cgccpf", cpf));
					filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));
		
					//Busca Cliente
					List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);
		
					
					if (users != null)
					{
						EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
						codcli = (Long) usuarioCliente.getValue("usu_codcli");
		
						QLEqualsFilter filterCliente = new QLEqualsFilter("codcli", codcli);
						clienteObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCliente);
					}
					
				}catch(Exception e){
					cpf = null;
				}
				
	
				
	
				//Busca os valores por Request
				String nome = new String(request.getParameter("nome").getBytes(),"UTF-8");
				String email = request.getParameter("email");
				String telefone = request.getParameter("telefone");
				String mensagem =  new String(request.getParameter("mensagem").getBytes(),"UTF-8");
				String cidade = request.getParameter("cidade"); // atualmente o site nao envia estes dados, nao sei o porque mas ninguem reclamou. Provavelmente alguma solicitacao
				String bairro = request.getParameter("bairro"); // atualmente o site nao envia estes dados, nao sei o porque mas ninguem reclamou. Provavelmente alguma solicitacao
				String tipoContato = request.getParameter("tipoContato");
				String origem = request.getParameter("origem") != null ? request.getParameter("origem") : "";
				String idCtr = request.getParameter("idCtrSapiens");
				boolean isMobile = false;
				if(request.getParameter("isMobile") != null){
					isMobile = (Boolean.parseBoolean(request.getParameter("isMobile")));
				}
				
				//Busca o Tipo de Contato
				QLEqualsFilter filterTipoContato = new QLEqualsFilter("neoId", Long.valueOf(tipoContato));
				NeoObject tipoContatoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("tipoContato"), filterTipoContato);
				EntityWrapper tipoContatoWrapper = new EntityWrapper(tipoContatoObj);
				Long idTipoContato = (Long) tipoContatoWrapper.getValue("neoId");
				
				//Cria Variaveis para diferenciar RSC de RRC
				String eform = "";
				String workflow = "";
				String fieldTelefone = "";
				String fieldContato = "";
				String fieldMensagem = "";
				String valueMensagem = "";
				String valueSolicitante = "";
	
				GregorianCalendar gcPrazo = new GregorianCalendar();
				gcPrazo.add(GregorianCalendar.DATE, 5);
				gcPrazo.set(GregorianCalendar.HOUR, 23);
				gcPrazo.set(GregorianCalendar.MINUTE, 59);
				gcPrazo.set(GregorianCalendar.SECOND, 59);
	
				//Reclamacao
				if (idTipoContato == 11715151L)
				{
					eform = "RRCRelatorioReclamacaoCliente";
					workflow = "Q005 - RRC - Relatório de Reclamação de Cliente";
					fieldTelefone = "telefone";
					fieldContato = "pessoaContato";
					fieldMensagem = "textoReclamacao";
					valueSolicitante = "SITERESPONSAVELRRC";
				}
				else
				{
					valueSolicitante = "SITERESPONSAVELRSC";
										
					RSCVO rsc = new RSCVO();
					rsc.setCodCli(codcli);
					if(isMobile){
						rsc.setOrigemSolicitacao("11");
					}else{
						rsc.setOrigemSolicitacao(origem);
						if(origem.equals("16")) {
							valueSolicitante = "HotsiteResponsavelRSC";
						}
						if(origem.equals("17")) {
							valueSolicitante = "ProspectadorResponsavelRSC";
						}
					}
					
					rsc.setTipoContato(tipoContato);
					rsc.setNome(nome);
					rsc.setEmail(email);
					rsc.setTelefone(telefone);
					rsc.setMensagem(mensagem);
					rsc.setCidade(cidade);
					rsc.setBairro(bairro);
					rsc.setPrazo(gcPrazo);
					rsc.setIdCtrSapiens(idCtr);
	
					String tarefa = RSCHelper.abrirRSC(rsc, valueSolicitante, true);
					System.out.println("Tarefa RSC Aberta: " + tarefa);
					JSONObject jsonCliente = new JSONObject();
					jsonCliente.append("status",100L);
					jsonCliente.append("msg", "Solicitacao gerada com sucesso!");
					jsonCliente.append("ret", tarefa);
					out.print(jsonCliente);
					return;
				}
				InstantiableEntityInfo infoSolicitacao = AdapterUtils.getInstantiableEntityInfo(eform);
				NeoObject noSolicitacao = infoSolicitacao.createNewInstance();
				EntityWrapper solicitacaoWrapper = new EntityWrapper(noSolicitacao);
	
				//Busca Origem da Solicitacao
				QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", "05");
				NeoObject origemObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("RRCOrigem"), filterOrigem);
	
				if (idTipoContato == 11715151L)
				{
	
					solicitacaoWrapper.setValue("prazoAcao", OrsegupsUtils.getNextWorkDay(gcPrazo));
					//Seta os valores
					solicitacaoWrapper.setValue("origem", origemObj);
					solicitacaoWrapper.setValue("cidade", cidade);
					solicitacaoWrapper.setValue("bairro", bairro);
					solicitacaoWrapper.setValue("email", email);
					solicitacaoWrapper.setValue("idCtrSapiens", idCtr);
					solicitacaoWrapper.setValue(fieldContato, nome);
					solicitacaoWrapper.setValue(fieldTelefone, telefone);
					solicitacaoWrapper.setValue(fieldMensagem, valueMensagem + mensagem);
	
				}
	
				if (clienteObj != null)
				{
					solicitacaoWrapper.findField("clienteSapiens").setValue(clienteObj);
				}
				PersistEngine.persist(noSolicitacao);
				QLEqualsFilter equal = new QLEqualsFilter("name", workflow);
				List<ProcessModel> processModelList = (List<ProcessModel>) PersistEngine.getObjects(ProcessModel.class, equal, " neoId DESC ");
				ProcessModel processModel = processModelList.get(0);
				NeoPaper papel = new NeoPaper();
				String executor = "";
				NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", valueSolicitante));
				papel = (NeoPaper) obj;
	
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						break;
					}
				}
				else
				{
					String gestaoCerec = "Gestão CEREC";
					NeoPaper objCerec = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", gestaoCerec));
					papel = (NeoPaper) objCerec;
					if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
					{
						for (NeoUser user : papel.getUsers())
						{
							executor = user.getCode();
							break;
						}
					}
					else
					{
						JSONObject jsonCliente = new JSONObject();
						jsonCliente.append("status", 0L);
						jsonCliente.append("msg", "Usuário ou Gestor Responsável não encontrado!");
						out.print(jsonCliente);
					}
				}
				NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
	
				/**
				 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do
				 *         Fusion
				 * @date 11/03/2015
				 */
				/*final WFProcess processo = WorkflowService.startProcess(processModel, noSolicitacao, true, solicitante);
				processo.setSaved(true);
				System.out.println("solicitacao aberta site. " + processo.getCode());
				try
				{
					new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante, false);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					out.print("Erro #4 - Erro ao avançar a primeira tarefa");
					return;
				}*/
				/**
				 * FIM ALTERAÇÕES - NEOMIND
				 */
				
				String tarefa = OrsegupsWorkflowHelper.iniciaProcesso(processModel, noSolicitacao, true, solicitante, true, solicitante);
	
				// Dispara email ao cliente
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("tarefa", tarefa);
				paramMap.put("nome", nome);
				paramMap.put("tipo", tipoContato);
				paramMap.put("mensagem", mensagem);
				FusionRuntime.getInstance().getMailEngine().sendEMail(email, "/portal_orsegups/emailAberturaRSC.jsp", paramMap);
				JSONObject jsonCliente = new JSONObject();
				jsonCliente.append("status", 100L);
				jsonCliente.append("msg", "Obrigado pelo contato");
				jsonCliente.append("ret", tarefa );
				out.print(jsonCliente);
				}else{
					JSONObject jsonCliente = new JSONObject();
					jsonCliente.append("status", 90L);
					jsonCliente.append("msg", "Token Invalido!");
					jsonCliente.append("ret", "Acesso Nao Autorizado" );
					out.print(jsonCliente);
				}
		}
		catch (Exception e)
		{
			JSONObject jsonCliente = new JSONObject();

			jsonCliente.append("status", 0L);
			jsonCliente.append("msg", "Estamos enfrentando dificudades de processamento. Tente mais tarde ou informe nossa TI.");
			jsonCliente.append("ret", e.getMessage());
			out.print(jsonCliente);
			e.printStackTrace();
		}
	}
	
}