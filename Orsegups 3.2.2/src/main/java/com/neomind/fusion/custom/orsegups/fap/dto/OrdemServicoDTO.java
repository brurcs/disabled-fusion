package com.neomind.fusion.custom.orsegups.fap.dto;

import java.util.GregorianCalendar;

public class OrdemServicoDTO {
	
	Long numeroDaOs;
	GregorianCalendar dataDeAbertura;
	GregorianCalendar dataDeFechamento;
	
	public Long getNumeroDaOs() {
		return numeroDaOs;
	}
	public void setNumeroDaOs(Long numeroDaOs) {
		this.numeroDaOs = numeroDaOs;
	}
	public GregorianCalendar getDataDeAbertura() {
		return dataDeAbertura;
	}
	public void setDataDeAbertura(GregorianCalendar dataDeAbertura) {
		this.dataDeAbertura = dataDeAbertura;
	}
	public GregorianCalendar getDataDeFechamento() {
		return dataDeFechamento;
	}
	public void setDataDeFechamento(GregorianCalendar dataDeFechamento) {
		this.dataDeFechamento = dataDeFechamento;
	}

}