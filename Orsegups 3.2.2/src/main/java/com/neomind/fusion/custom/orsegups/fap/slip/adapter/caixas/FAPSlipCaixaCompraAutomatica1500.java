package com.neomind.fusion.custom.orsegups.fap.slip.adapter.caixas;

import java.math.BigDecimal;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskSummary;
import com.neomind.fusion.workflow.task.rule.TaskRuleAdapter;
import com.neomind.fusion.workflow.task.rule.TaskRuleContext;

public class FAPSlipCaixaCompraAutomatica1500 implements TaskRuleAdapter
{

	@Override
	public boolean isTriggered(TaskRuleContext context)
	{
		boolean retorno = false;
		BigDecimal valorCaixa = new BigDecimal("1500");

		Task t = null;
		TaskSummary ts = null;
		try
		{
			if (context.getTask() instanceof Task)
			{
				t = (Task) context.getTask();
			}
			else
			{
				ts = (TaskSummary) context.getTask();
			}
			
			if (((t != null && t.getProcessName().equals("F002 - FAP Slip")) && t.getTitle().equals("Aprovar Compra")) || 
					((ts != null && ts.getProcessName().equals("F002 - FAP Slip")) && ts.getTaskName().equals("Aprovar Compra")))
			{
				EntityWrapper wrapper = context.getWrapper();
				
				BigDecimal valorPagamento = wrapper.findGenericValue("valorPagamento");
				String tipoOC = wrapper.findGenericValue("ordemCompra.tipoOC");
				
				if (valorPagamento.compareTo(valorCaixa) > 0 && tipoOC.equals("Automatico"))
				{
					retorno = true;
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Erro ao direcionar tarefa de FAP para subpastas. Mensagem: " + e.getMessage());
			e.printStackTrace();
		}

		return retorno;
	}
}