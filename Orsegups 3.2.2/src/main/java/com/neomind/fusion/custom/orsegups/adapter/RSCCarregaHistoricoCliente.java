package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCCarregaHistoricoCliente implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			Long codigoCliente = (Long) processEntity.findValue("clienteSapiens.codcli");
			QLEqualsFilter filtroCliente = new QLEqualsFilter("clienteSapiens.codcli", codigoCliente);

			QLGroupFilter filtroHistorico = new QLGroupFilter("AND");
			filtroHistorico.addFilter(filtroCliente);

			List<NeoObject> listHistoricoNovo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoClienteNovo"), filtroHistorico);
			List<NeoObject> listHistorico = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoCliente"), filtroHistorico);

			if (listHistoricoNovo != null && !listHistoricoNovo.isEmpty())
			{
				for (NeoObject noHistorico : listHistoricoNovo)
				{
					if (noHistorico != null)
					{
						EntityWrapper historicoWrapper = new EntityWrapper(noHistorico);
						GregorianCalendar dataRegistro = (GregorianCalendar) historicoWrapper.findField("dataRegistro").getValue();

						if (dataRegistro != null)
						{
							processEntity.findField("historicoCliente").addValue(noHistorico);
						}
					}
				}
			}
			//TODO RSC - Remover após 01/05/2017
			if (listHistorico != null && !listHistorico.isEmpty())
			{
				for (NeoObject noHistorico : listHistorico)
				{
					if (noHistorico != null)
					{
						EntityWrapper historicoWrapper = new EntityWrapper(noHistorico);
						GregorianCalendar dataRegistro = (GregorianCalendar) historicoWrapper.findField("dataRegistro").getValue();

						if (dataRegistro != null)
						{
							processEntity.findField("historicoCliente").addValue(noHistorico);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}
}