package com.neomind.fusion.custom.orsegups.utils;

import java.util.Comparator;

import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;


public class CoberturaComparator  implements Comparator {  
	public int compare(Object o1, Object o2){  
		EntradaAutorizadaVO e1 = (EntradaAutorizadaVO) o1;  
		EntradaAutorizadaVO e2 = (EntradaAutorizadaVO) o2;  

		return e1.getDataInicial().compareTo(e2.getDataInicial());  
	}  
}