package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.util.JAUtils;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter.JAInserirAfastamentoAdapter
public class JAInserirAfastamentoProcessoAutomatico implements AdapterInterface {
    private static final Log log = LogFactory.getLog(JAInserirAfastamentoProcessoAutomatico.class);
    EntityManager entityManager;
    EntityTransaction transaction;

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	try {
	    NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
	    EntityWrapper wColaborador = new EntityWrapper(colaborador);
	    String numemp = NeoUtils.safeOutputString(wColaborador.getValue("numemp"));
	    String tipcol = NeoUtils.safeOutputString(wColaborador.getValue("tipcol"));
	    String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));
	    GregorianCalendar datafa = (GregorianCalendar) processEntity.getValue("dataIniAfa");
	    String horini = "0";
	    GregorianCalendar datTermino = (GregorianCalendar) processEntity.findValue("dataFimAfa");
	    String horFim = "0";
	    Long sitAfa = 0L;
	    NeoUser user = PortalUtil.getCurrentUser();
	    String dataString = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm");
	    String descricao = "";
	    EscalaVO escala = QLPresencaUtils.getEscalaColaborador(NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), NeoUtils.safeLong(tipcol));
	    String codesc = NeoUtils.safeOutputString(escala.getCodigoEscala());
	    String codtma = NeoUtils.safeOutputString(escala.getCodigoTurma());

	    String tarefa = activity.getCode();

	    sitAfa = 15L;
	    descricao = "Inserido via tarefa " + activity.getCode() + " do processo J003 - Afastamento - pelo usuário " + user.getFullName() + " em " + dataString;
	    String retorno = insertAfastamento(numemp, tipcol, numcad, datafa, codesc, codtma, horini, datTermino, horFim, sitAfa, descricao, tarefa);

	    if (retorno.contains("Problema")){
		throw new Exception(retorno);
	    }

	} catch (Exception e) {
	    e.printStackTrace();

	    String mensagem = "Erro ao inserir afastamento para o colaborador";

	    if (e.getMessage().contains("Problema")){
		mensagem = e.getMessage();
	    }

	    throw new WorkflowException(mensagem);
	}

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

    }

    private String insertAfastamento(String numemp, String tipcol, String numcad, GregorianCalendar datafa, String codesc, String codtma, String horini, GregorianCalendar datTermino, String horFim, Long sitAfa,
	    String descricao, String tarefa) throws Exception {
	String retorno = "";
	this.entityManager = PersistEngine.getEntityManager("VETORH");
	this.transaction = this.entityManager.getTransaction();

	try {

	    GregorianCalendar dataInicioAfa = new GregorianCalendar();
	    GregorianCalendar datFimAfa = new GregorianCalendar();

	    dataInicioAfa = (GregorianCalendar) datafa.clone();
	    datFimAfa = (GregorianCalendar) datTermino.clone();

	    String afastamentoInformado = null;
	    afastamentoInformado = "S";
	    if (!this.transaction.isActive()) {
		this.transaction.begin();
	    }

	    StringBuilder admColaborador = new StringBuilder();
	    admColaborador.append(" SELECT DATADM FROM R034FUN WHERE numemp = :numemp AND tipcol = :tipcol AND numcad = :numcad ");

	    Query queryAdm = this.entityManager.createNativeQuery(admColaborador.toString());
	    queryAdm.setParameter("numemp", numemp);
	    queryAdm.setParameter("tipcol", tipcol);
	    queryAdm.setParameter("numcad", numcad);
	    queryAdm.setMaxResults(1);

	    Timestamp timeAdm = (Timestamp) queryAdm.getSingleResult();
	    GregorianCalendar dataAdm = new GregorianCalendar();
	    dataAdm.setTimeInMillis(timeAdm.getTime());

	    StringBuilder buscaCpf = new StringBuilder();
	    buscaCpf.append(" SELECT numcpf FROM R034FUN WHERE numemp = :numemp ");
	    buscaCpf.append(" AND tipcol = :tipcol ");
	    buscaCpf.append(" AND numcad = :numcad ");

	    Query queryBuscaCpf = this.entityManager.createNativeQuery(buscaCpf.toString());
	    queryBuscaCpf.setParameter("numemp", numemp);
	    queryBuscaCpf.setParameter("tipcol", tipcol);
	    queryBuscaCpf.setParameter("numcad", numcad);
	    queryBuscaCpf.setMaxResults(1);

	    BigInteger cpf = (BigInteger) queryBuscaCpf.getSingleResult();
	    String cpfString = cpf.toString();

	    if (dataAdm != null && !dataInicioAfa.before(dataAdm)) {

		Integer count = this.verificaAfastamento(numcad, numemp, tipcol, dataInicioAfa, datFimAfa);

		if (count != null && count > 0) {
		    count = this.verificaAfastamentoLancado(numcad, numemp, tipcol, dataInicioAfa, datFimAfa, sitAfa);
		    if (count != null && count > 0){
			retorno = "";
		    }else{
			return "<b>Problema!</b> Colaborador possui lançamentos que conflitam com o período informado. Favor efetuar lançamento manual.";
		    }
		} else {
		    retorno = "INSERT Situacao " + sitAfa;
		   // StringBuilder insereAfastamento = new StringBuilder();

		    String ObsAfa = descricao;

		    GregorianCalendar datMod = new GregorianCalendar();

		    String query = " Insert Into R038AFA (NumEmp, TipCol, NumCad, DatAfa, HorAfa, DatTer, SitAfa, OriAfa, ObsAfa, StaAtu, usu_codusu, usu_datger, usu_afainf,horTer, usu_datmod, usu_hormod) "
			    + "Values (" + numemp + "," + tipcol + "," + numcad + ",'" + new Timestamp(dataInicioAfa.getTime().getTime()) + "',0,'" + new Timestamp(datFimAfa.getTime().getTime()) + "'"
			    + "," + sitAfa + ",0,'" + ObsAfa + "',0,389,'" + new Timestamp(datMod.getTime().getTime()) +"', '" + afastamentoInformado + "',0,DATEADD(dd, 0, DATEDIFF(dd, 0, '" + new Timestamp(datMod.getTimeInMillis()) + "')),"
			    + " (DATEPART(HOUR, '" + new Timestamp(datMod.getTime().getTime()) + "') * 60) + DATEPART(MINUTE, '" + new Timestamp(datMod.getTime().getTime()) + "')) ";


		    Integer result = JAUtils.safeInsert("VETORH", query, 3, 3000);

		    if(result == 0){

			new WorkflowException("Erro na inserção do afastamento!");

		    }

		    //queryInsereAfa.executeUpdate();

		    this.salvarHistorico(datafa, numemp, Long.valueOf(tipcol), numcad, Long.valueOf(cpfString), 0L, tarefa, sitAfa, 0L);

		    // LOG Colaborador
		    String txtLog = "Inserido afastamento em " + datafa;

		    if (cpfString != null && !cpfString.equals("")) {
			this.saveLog(txtLog, null, cpfString, "colaborador", null);
		    }

		    log.warn("Cadastro Afastamento -  NumEmp:" + numemp + " - NumCad: " + numcad + ")");
		}
	    }
	    this.transaction.commit();

	} catch (Exception ex) {
	    if (this.transaction.isActive()) {
		this.transaction.rollback();
	    }
	    ex.printStackTrace();
	    throw new Exception("Erro ao inserir afastamento pelo fluxo J003 ", ex);
	}
	return retorno;
    }

    private int verificaAfastamento (String numcad, String numemp, String tipcol, GregorianCalendar inicio, GregorianCalendar termino){

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT COUNT(*) AS CONTAGEM FROM R038AFA ");
	sql.append(" WHERE NumEmp=? AND NumCad=? AND TipCol=? ");
	sql.append(" AND  ");
	sql.append(" ( ");
	sql.append(" ( ");
	sql.append(" (	(DatAfa BETWEEN ? AND ?) OR (DatTer BETWEEN ? AND ?)  ) ");
	sql.append(" OR ");
	sql.append(" (	( ? BETWEEN DatAfa AND DatTer) OR ( ? BETWEEN DatAfa AND DatTer)  )  ");
	sql.append(" ) ");
	sql.append(" ) ");

	java.sql.Date inicioSql = new java.sql.Date(inicio.getTime().getTime());	
	java.sql.Date terminoSql = new java.sql.Date(termino.getTime().getTime());

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try{
	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, Integer.parseInt(numemp));
	    pstm.setInt(2, Integer.parseInt(numcad));
	    pstm.setInt(3, Integer.parseInt(tipcol));
	    pstm.setDate(4, inicioSql);
	    pstm.setDate(5, terminoSql);
	    pstm.setDate(6, inicioSql);
	    pstm.setDate(7, terminoSql);
	    pstm.setDate(8, inicioSql);
	    pstm.setDate(9, terminoSql);

	    rs = pstm.executeQuery();

	    if (rs.next()){
		return rs.getInt(1);
	    }

	}catch (SQLException e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

	return 0;
    }

    private int verificaAfastamentoLancado(String numcad, String numemp, String tipcol, GregorianCalendar inicio, GregorianCalendar termino, Long sitAfa){

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT COUNT(*) AS CONTAGEM FROM R038AFA ");
	sql.append(" WHERE NumEmp=? AND NumCad=? AND TipCol=? AND SitAfa =? and DatAfa =? and DatTer =? ");

	java.sql.Date inicioSql = new java.sql.Date(inicio.getTime().getTime());	
	java.sql.Date terminoSql = new java.sql.Date(termino.getTime().getTime());

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try{
	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, Integer.parseInt(numemp));
	    pstm.setInt(2, Integer.parseInt(numcad));
	    pstm.setInt(3, Integer.parseInt(tipcol));
	    pstm.setInt(4, Integer.parseInt(sitAfa.toString()));
	    pstm.setDate(5, inicioSql);
	    pstm.setDate(6, terminoSql);

	    rs = pstm.executeQuery();

	    if (rs.next()){
		return rs.getInt(1);
	    }

	}catch (SQLException e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

	return 0;
    }

    private void saveLog(String texto, String idposto, String idColaborador, String tipo, PrintWriter out) {
	if (tipo != null && tipo.equalsIgnoreCase("posto")) {
	    QLGroupFilter groupFilter = new QLGroupFilter("AND");
	    groupFilter.addFilter(new QLEqualsFilter("codloc", idposto));

	    Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8");

	    NeoObject postoObject = PersistEngine.getObject(clazz, groupFilter);

	    if (postoObject != null) {
		NeoObject logPosto = AdapterUtils.createNewEntityInstance("QLLogPresencaPosto");

		if (logPosto != null) {
		    EntityWrapper postoWrapper = new EntityWrapper(logPosto);
		    postoWrapper.setValue("posto", postoObject);
		    postoWrapper.setValue("dataLog", new GregorianCalendar());
		    postoWrapper.setValue("textoLog", texto);
		    postoWrapper.setValue("usuario", PortalUtil.getCurrentUser());

		    PersistEngine.persist(logPosto);
		    if (out != null) {
			out.print("Log salvo com sucesso.");
		    }
		    return;
		}
	    }
	} else if (tipo != null && tipo.equalsIgnoreCase("colaborador")) {
	    NeoObject log = AdapterUtils.createNewEntityInstance("QLLogPresencaColaborador");

	    if (log != null && idColaborador != null) {
		texto = texto.replaceAll("'", "Â´");
		texto = texto.replaceAll("\"", "Â´");

		EntityWrapper logWrapper = new EntityWrapper(log);
		logWrapper.setValue("cpf", Long.valueOf(idColaborador));
		logWrapper.setValue("dataLog", new GregorianCalendar());
		logWrapper.setValue("textoLog", texto);
		logWrapper.setValue("usuario", PortalUtil.getCurrentUser());

		if (idposto != null) {
		    QLGroupFilter groupFilter = new QLGroupFilter("AND");
		    groupFilter.addFilter(new QLEqualsFilter("codloc", idposto));

		    /**
		     * @author orsegups lucas.avila - Classe generica.
		     * @date 08/07/2015
		     */
		    Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8");

		    NeoObject postoObject = PersistEngine.getObject(clazz, groupFilter);

		    if (postoObject != null) {
			logWrapper.setValue("posto", postoObject);
		    }
		}

		PersistEngine.persist(log);

		if (out != null) {
		    out.print("Log salvo com sucesso.");
		}
		return;
	    }
	}

	out.print("Erro ao salvar log.");
    }

    public boolean salvarHistorico(GregorianCalendar datafa, String numemp, Long tipcol, String numcad, Long numcpf, Long horafa, String tarefa, Long sitafa, Long caudem) {

	QLEqualsFilter filterCpf = new QLEqualsFilter("numcpf", numcpf);
	QLEqualsFilter filterData = new QLEqualsFilter("datafa", datafa);
	QLEqualsFilter filterHora = new QLEqualsFilter("horafa", horafa);

	QLGroupFilter filterGroup = new QLGroupFilter("AND");
	filterGroup.addFilter(filterCpf);
	filterGroup.addFilter(filterData);
	filterGroup.addFilter(filterHora);

	GregorianCalendar data = (GregorianCalendar) datafa.clone();

	List<NeoObject> historicos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaAfastamentoJustificativa"), filterGroup);

	if (historicos != null && !historicos.isEmpty()) {

	    NeoObject historico = historicos.get(0);

	    EntityWrapper wHistorico = new EntityWrapper(historico);

	    String tarefas = wHistorico.findField("tarefa").getValueAsString();

	    if (!tarefas.contains(tarefa)){
		tarefas += ","+tarefa;
	    }

	    wHistorico.setValue("SitAfa", sitafa);
	    wHistorico.setValue("tarefa", tarefas);

	    PersistEngine.persist(historico);

	    return true;

	} else {
	    InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
	    NeoObject noAJ = tarefaAJ.createNewInstance();
	    EntityWrapper ajWrapper = new EntityWrapper(noAJ);

	    try {

		ajWrapper.setValue("numemp", NeoUtils.safeLong(numemp));
		ajWrapper.setValue("tipcol", tipcol);
		ajWrapper.setValue("numcad", NeoUtils.safeLong(numcad));
		ajWrapper.setValue("numcpf", numcpf);
		ajWrapper.setValue("datafa", data);
		ajWrapper.setValue("horafa", horafa);
		ajWrapper.setValue("tarefa", tarefa);
		ajWrapper.setValue("SitAfa", sitafa);
		ajWrapper.setValue("CauDem", caudem);

		PersistEngine.persist(noAJ);

		return true;

	    } catch (Exception e) {
		System.out.println("Regra: Falta nÃ£o justificada - erro ao registrar no eform ");
		e.printStackTrace();

	    }
	}

	return false;
    }
}
