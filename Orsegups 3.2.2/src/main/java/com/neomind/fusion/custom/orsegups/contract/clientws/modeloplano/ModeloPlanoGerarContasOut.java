/**
 * ModeloPlanoGerarContasOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano;

public class ModeloPlanoGerarContasOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasOutGridCtaResult[] gridCtaResult;

    public ModeloPlanoGerarContasOut() {
    }

    public ModeloPlanoGerarContasOut(
           java.lang.String erroExecucao,
           com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasOutGridCtaResult[] gridCtaResult) {
           this.erroExecucao = erroExecucao;
           this.gridCtaResult = gridCtaResult;
    }


    /**
     * Gets the erroExecucao value for this ModeloPlanoGerarContasOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this ModeloPlanoGerarContasOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the gridCtaResult value for this ModeloPlanoGerarContasOut.
     * 
     * @return gridCtaResult
     */
    public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasOutGridCtaResult[] getGridCtaResult() {
        return gridCtaResult;
    }


    /**
     * Sets the gridCtaResult value for this ModeloPlanoGerarContasOut.
     * 
     * @param gridCtaResult
     */
    public void setGridCtaResult(com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasOutGridCtaResult[] gridCtaResult) {
        this.gridCtaResult = gridCtaResult;
    }

    public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasOutGridCtaResult getGridCtaResult(int i) {
        return this.gridCtaResult[i];
    }

    public void setGridCtaResult(int i, com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasOutGridCtaResult _value) {
        this.gridCtaResult[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModeloPlanoGerarContasOut)) return false;
        ModeloPlanoGerarContasOut other = (ModeloPlanoGerarContasOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.gridCtaResult==null && other.getGridCtaResult()==null) || 
             (this.gridCtaResult!=null &&
              java.util.Arrays.equals(this.gridCtaResult, other.getGridCtaResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getGridCtaResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGridCtaResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGridCtaResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModeloPlanoGerarContasOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "modeloPlanoGerarContasOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gridCtaResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gridCtaResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "modeloPlanoGerarContasOutGridCtaResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
