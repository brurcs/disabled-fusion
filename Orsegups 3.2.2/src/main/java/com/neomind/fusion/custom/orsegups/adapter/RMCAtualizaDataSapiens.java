package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.jfree.util.Log;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RMCAtualizaDataSapiens implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String nomeFonteDados = "SAPIENS";

		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

		// valores a serem atualizados no update
		GregorianCalendar dataRecebimentoContrato = (GregorianCalendar) processEntity.findValue("DataRecebimentoContrato");

		// valores utilizados na condicao where
		Long codigoEmpresa = (Long) processEntity.findValue("empresa.codigo");
		String numeroContrato = (String) processEntity.findValue("numeroContrato");
		Long filial = (Long) processEntity.findValue("filial");
		String numerosPostos = (String) processEntity.findValue("numerosPostos");
		String[] nrPostos = numerosPostos.split(",");

		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE USU_T160CVS ");
		sql.append("SET USU_CTRFIS = ?, ");
		sql.append("USU_DATFIS = ? ");
		sql.append("WHERE USU_CODEMP = ? ");
		sql.append("AND USU_CODFIL = ? ");
		sql.append("AND USU_NUMCTR = ? ");
		sql.append("AND USU_NUMPOS IN (");

		//workarraound para utilizar IN com preparedStatement
		Boolean first = true;
		for (String nrPosto : nrPostos)
		{
			if (first)
			{
				sql.append("?");
				first = false;
			}
			else
			{
				sql.append(",?");
			}
		}
		sql.append(")");

		try
		{
			PreparedStatement st = connection.prepareStatement(sql.toString());
			//UPDATE
			st.setString(1, "S");
			st.setDate(2, new Date(dataRecebimentoContrato.getTimeInMillis()));
			//WHERE
			st.setLong(3, codigoEmpresa);
			st.setLong(4, filial);
			st.setString(5, numeroContrato);

			//workarraound para utilizar IN com preparedStatement
			Integer index = 6;
			for (String nrPosto : nrPostos)
			{
				st.setString(index, nrPosto);
				index++;
			}
			st.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
