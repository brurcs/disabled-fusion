package com.neomind.fusion.custom.orsegups.sigma.vo;

public class ColaboradorVO
{
	private Long id;
	private String nome;
	private Long sel;
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public String getNome()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	public Long getSel()
	{
		return sel;
	}
	public void setSel(Long sel)
	{
		this.sel = sel;
	}
}