package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.rsc.helper.RSCHelper;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.utils.FAPUtils;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaAbreTarefaOS implements CustomJobAdapter {
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaOS.class);
	private static final String LINK = OrsegupsUtils.URL_PRODUCAO + "/fusion/";
	private String numeroTarefaRSC = "";

	private Set<Integer> defeitosInstalacao = new TreeSet<Integer>();
	private Set<Integer> defeitosInstalacaoRastreamento = new TreeSet<Integer>();

	public RotinaAbreTarefaOS() {
		this.defeitosInstalacao.add(134);
		this.defeitosInstalacao.add(184);
		this.defeitosInstalacao.add(1018);
		this.defeitosInstalacao.add(178);
		this.defeitosInstalacao.add(1161);
		this.defeitosInstalacao.add(1168);

		this.defeitosInstalacaoRastreamento.add(98);
		this.defeitosInstalacaoRastreamento.add(1153);
		this.defeitosInstalacaoRastreamento.add(1154); 
		this.defeitosInstalacaoRastreamento.add(1157);
		this.defeitosInstalacaoRastreamento.add(1159);
		this.defeitosInstalacaoRastreamento.add(1160);
		this.defeitosInstalacaoRastreamento.add(1161);
	}

	@Override
	public void execute(CustomJobContext arg0) {

		Connection conn = null;	
		Connection connSapiens = null;
		PreparedStatement st = null;
		PreparedStatement st3 = null;
		ResultSet rs = null;
		ResultSet rsEquip = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		try {

			String executor = "";
			String solicitante = "";

			GregorianCalendar cal2Temp = new GregorianCalendar();
			GregorianCalendar cal4Temp = new GregorianCalendar();

			GregorianCalendar cal2 = OrsegupsUtils.getSpecificWorkDay(cal2Temp, 2L);
			cal2.set(Calendar.HOUR_OF_DAY, 23);
			cal2.set(Calendar.MINUTE, 59);
			cal2.set(Calendar.SECOND, 59);

			GregorianCalendar cal4 = OrsegupsUtils.getSpecificWorkDay(cal4Temp, 4L);
			cal4.set(Calendar.HOUR_OF_DAY, 23);
			cal4.set(Calendar.MINUTE, 59);
			cal4.set(Calendar.SECOND, 59);

			GregorianCalendar prazo = new GregorianCalendar();

			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String isAcanca = "nao";

			log.warn("##### INICIAR ABERTURA DE TAREFA OS - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			InstantiableEntityInfo osInfo = AdapterUtils.getInstantiableEntityInfo("SIGMAControleUltimaOS");

			conn = PersistEngine.getConnection("SIGMA90");
			connSapiens = PersistEngine.getConnection("SAPIENS");

			
			boolean parametroValidaAreaTecnica = (FAPParametrizacao.findParameter("validacaoAreaTecnica").equals("1") ? true : false);
			
			StringBuffer sqlOS = new StringBuffer();
			sqlOS.append(" SELECT NOTAS.NOTA, c.CD_CLIENTE, c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO, os.ID_ORDEM, os.DEFEITO, def.DESCRICAODEFEITO, col.NM_COLABORADOR, c.CGCCPF, c.FANTASIA, c.RAZAO, c.CONTRATO, c.COMPLEMENTO, ");
			sqlOS.append(" p.TX_OBSERVACAO, mp.NM_DESCRICAO, p.DT_PAUSA, c.RESPONSAVEL, os.ABERTURA, def.IDOSDEFEITO, c.FONE1, c.FONE2, p.CD_MOTIVO_PAUSA, SUBSTRING(col.NM_COLABORADOR,1,3) AS REG_ROTA, os.ID_INSTALADOR, os.IDOSDEFEITO ");
			sqlOS.append(", c.EMAILRESP, c.ENDERECO, cid.NOME, c.TP_PESSOA, RES.NM_COLABORADOR AS TEC_RES, C.CD_GRUPO_CLIENTE ");
			sqlOS.append(" FROM dbORdem os WITH (NOLOCK) ");
			sqlOS.append(" INNER JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
			sqlOS.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR 												");
			sqlOS.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO														");
			sqlOS.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE 														 	");
			sqlOS.append(" LEFT JOIN COLABORADOR RES WITH(NOLOCK) ON RES.CD_COLABORADOR = C.CD_TECNICO_RESPONSAVEL ");
			sqlOS.append(" LEFT JOIN OS_NOTAS NOTAS WITH(NOLOCK) ON NOTAS.ID_ORDEM = OS.ID_ORDEM ");
			sqlOS.append(" INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA																			");
			sqlOS.append(" INNER JOIN MOTIVO_PAUSA mp WITH (NOLOCK) ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA             ");
			sqlOS.append(" INNER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE ");
			sqlOS.append(" WHERE 1 = 1 ");
			sqlOS.append(" AND c.cd_cliente != 96674 ");
			sqlOS.append(" AND def.IDOSDEFEITO != 1165 ");
			sqlOS.append(" AND     (  						 ");
			sqlOS.append(" (os.FECHADO = 3 AND p.CD_MOTIVO_PAUSA IN (2, 41, 49) AND (NOTAS.NOTA != '#INTEGRA_RAIA' OR NOTAS.NOTA IS NULL))");
			sqlOS.append("           OR  						   ");
			sqlOS.append(  parametroValidaAreaTecnica 
					? "(os.FECHADO = 3 AND p.CD_MOTIVO_PAUSA IN ( 9, 43, 44, 45, 46, 47, 48, 50, 51) AND (NOTAS.NOTA != '#INTEGRA_RAIA' AND NOTAS.NOTA IS NOT NULL))" 
							:"(os.FECHADO = 3 AND p.CD_MOTIVO_PAUSA IN ( 9,  43, 44, 45, 46, 47, 48, 50, 51) AND (NOTAS.NOTA != '#INTEGRA_RAIA' OR NOTAS.NOTA IS NULL))");

			sqlOS.append("           OR  						   ");
			sqlOS.append("          (os.FECHADO = 1 AND p.CD_MOTIVO_PAUSA IN (2, 9, 41, 43, 44, 45, 46, 47, 48, 49, 50, 51) AND NOTAS.NOTA = '#INTEGRA_RAIA' )");
			sqlOS.append("           OR  						   ");
			sqlOS.append(" 		(os.FECHADO = 3 AND p.CD_MOTIVO_PAUSA = 1 AND DT_PAUSA < (CAST((STR( YEAR( GETDATE()-2 ) ) + '/' + STR( MONTH( GETDATE()-2 ) ) + '/' + STR( DAY( GETDATE()-2 ) ))AS DATETIME))))   ");
			sqlOS.append(" ORDER BY CD_MOTIVO_PAUSA ");

			st = conn.prepareStatement(sqlOS.toString());
			
			rs = st.executeQuery();
			
			String cidade = " ";// cidade cliente
			String emailRes = "";// emai do responsavel
			String fone = "";// fone do responsavel
			String contatoResp = ""; // nome do responsavel
			Long cpfCgc;// CPF do cliente


			while (rs.next()) {

				try	{
					
					Long numeroOS = new Long(rs.getLong("ID_ORDEM"));
					int cd_motivo = rs.getInt("CD_MOTIVO_PAUSA");
					
					
					if(parametroValidaAreaTecnica && cd_motivo != 2L && cd_motivo != 41L && cd_motivo != 49L) {
						if(!validaVerificacaoAreaTecnica(numeroOS)) {
							continue;
						}
					}
					
					cpfCgc = 0L;
					Long cdCliente = new Long(rs.getLong("CD_CLIENTE"));			
					Long cdGrupOCliente = rs.getLong("CD_GRUPO_CLIENTE");
					String notaRaia = rs.getString("NOTA") != null ? rs.getString("NOTA") : "";				
					boolean osRaiaApp = notaRaia.equals("#INTEGRA_RAIA") ? true : false;

					
					int idEmpresa = rs.getInt("ID_EMPRESA");
					Integer idOsDefeito = rs.getInt("IDOSDEFEITO");

					String tecRes = rs.getString("TEC_RES") != null ? rs.getString("TEC_RES") : "";

					String defeito = rs.getString("DESCRICAODEFEITO");
					String descricaoDefeito = rs.getString("DEFEITO");
					String tecnico = rs.getString("NM_COLABORADOR");
					String contrato = rs.getString("CONTRATO");
					String complemento = rs.getString("COMPLEMENTO");
					String conta = rs.getString("ID_CENTRAL");
					String particao = rs.getString("PARTICAO");
					String motivoPausa = rs.getString("TX_OBSERVACAO");
					String tipoPausa = rs.getString("NM_DESCRICAO");
					String regional = rs.getString("REG_ROTA");
					int tipoPessoa = rs.getInt("TP_PESSOA");

					defeito = defeito.replaceAll("\\s\\s+", " ");

					/* INÍCIO - VINCULAR OS A FAP */
					List<NeoObject> listaOs = new ArrayList<>();

					NeoObject ordemServico = AdapterUtils.createNewEntityInstance("FAPListaOrdemServico");

					EntityWrapper ew = new EntityWrapper(ordemServico);

					GregorianCalendar dataAbertura = new GregorianCalendar();
					dataAbertura.setTime(rs.getDate("ABERTURA"));

					GregorianCalendar pausa = new GregorianCalendar();
					pausa.setTime(rs.getDate("DT_PAUSA"));

					ew.findField("dataDeAbertura").setValue(dataAbertura);
					ew.findField("dataDeFechamento").setValue(pausa);
					ew.findField("numeroDaOs").setValue(numeroOS);
					ew.findField("descricaoDefeito").setValue(defeito);
					ew.findField("idOsDefeito").setValue(idOsDefeito.longValue());

					PersistEngine.persist(ordemServico);

					listaOs.add(ordemServico);		

					/* FIM - VINCULAR OS A FAP */
					if(regional.equals("PUB")) {
						regional = tecnico.substring(6,9);
					}

					log.error("#PAUSAOS# INICIOU OS:"+numeroOS);

					/**
					 * Trapaça para não que seja necessário reescrever toda a regra
					 * 
					 * Pausa atual
					 * 
					 * 45 FAP 
					 * 
					 * Passam a ser divididas por dados da ordem de serviço e passam
					 * a assumir uns dos antigos códigos
					 * 
					 * Antigas pausas 
					 * 45 FAP DE INSTALAÇÃO - OK 
					 * 46 FAP ADMINISTRATIVA - DEIXA DE EXISTIR 
					 * 47 FAP DE MANUTENÇÃO - OK
					 * 48 FAP DE RASTREAMENTO - MANUTENCAO - OK 
					 * 100 FAP DE RASTREAMENTO - INSTALACAO (MOTIVO VIRTUAL, NAO EXISTE NA TABELA OSMOTIVO) - OK 
					 * 50 FAP CORP - MANUTENÇÃO 
					 * 51 FAP CORP - INSTALAÇÃO 
					 * 52 FAP CORP E ADM - DEIXA DE EXISTIR
					 */

					boolean instalacao = this.defeitosInstalacao.contains(idOsDefeito);
					boolean instalacaoRastreamento = this.defeitosInstalacaoRastreamento.contains(idOsDefeito);

					if (cd_motivo == 45) {

						if (tecnico.toUpperCase().contains("-TEC-") || tecnico.toUpperCase().contains("- TEC -")){
							continue;			    
						}

						if (idEmpresa == 10075 || idEmpresa == 10129 || idEmpresa == 10127 || idEmpresa == 10144) { // RASTREAMENTO

							if (instalacao) {
								cd_motivo = 100; //FAP DE RASTREAMENTO - INSTALACAO
							} else {
								cd_motivo = 48; //FAP DE RASTREAMENTO - MANUTENCAO 
							}
						} else {
							if (tecRes.toUpperCase().contains("-CORP") || tecRes.toUpperCase().contains("- CORP")) {
								if (instalacao) {
									cd_motivo = 51; // FAP CORP INSTALACAO
								} else {
									cd_motivo = 50; //FAP CORP MANUTENCAO
								}
							} else {
								if (instalacao) {
									cd_motivo = 45; //FAP INSTALACAO
								} else {
									cd_motivo = 47; //FAP MANUTENCAO
								}
							}
						}
					}
					/* novo */
					if (rs.getString("NOME") != null && !rs.getString("NOME").equals("")) {
						cidade = rs.getString("NOME");
					} else {
						cidade = "";
					}

					if (rs.getString("EMAILRESP") != null && !rs.getString("EMAILRESP").equals("")) {
						emailRes = rs.getString("EMAILRESP");
					} else {
						emailRes = "";
					}

					if (rs.getString("FONE1") != null && !rs.getString("FONE1").equals("")) {
						fone = rs.getString("FONE1");
					} else {
						fone = "";
					}

					if (rs.getString("RESPONSAVEL") != null && !rs.getString("RESPONSAVEL").equals("")) {
						contatoResp = rs.getString("RESPONSAVEL");
					} else {
						contatoResp = "";
					}

					if (rs.getString("CGCCPF") != null && !rs.getString("CGCCPF").equals("")) {
						String cpf = OrsegupsUtils.GetOnlyNumbers(rs.getString("CGCCPF"));
						if (cpf != null & !cpf.isEmpty()) {
							cpfCgc = Long.parseLong(cpf);
						}
					} else {
						cpfCgc = 0L;
					}

					/* Fim novo */
					String dataPausa = formatter.format(rs.getDate("DT_PAUSA"));
					String responsavel = (rs.getString("RESPONSAVEL") != null ? rs.getString("RESPONSAVEL") : "");
					String fone1 = (rs.getString("FONE1") != null ? rs.getString("FONE1") : "");
					String fone2 = (rs.getString("FONE2") != null ? rs.getString("FONE2") : "");
					String motivoEspecifico = "";
					Long id_instalador = rs.getLong("ID_INSTALADOR");

					String flag_equip = "<img src='imagens/icones/sphere_red_16x16-trans.png' alt='Equipamento Cliente'/> Cliente";

					if (this.isEquipamentoOrsegups(cdCliente)) {
						// Equipamento Orsegups
						flag_equip = "<img src='imagens/icones/sphere_yellow_16x16-trans.png' alt='Equipamento Orsegups'/> Orsegups";
					}

					if (motivoPausa == null) {
						motivoPausa = "";
					}

					int tipoEmpresa = 0;

					if (cd_motivo == 9) {
						motivoEspecifico = "PENDÊNCIA ORÇAMENTO";
						// Descobrir se é Público ou Privado
						tipoEmpresa = this.getTipoEmpresa(cdCliente);

					} else if (cd_motivo == 44) {
						motivoEspecifico = "PENDÊNCIA ADMINISTRATIVA E DE ORÇAMENTO";
						tipoEmpresa = this.getTipoEmpresa(cdCliente);
					} else if (cd_motivo == 41) {
						motivoEspecifico = "Pendência de agendamento";
					} else if (cd_motivo == 42) {
						motivoEspecifico = "Habilitação do Monitoramento/Faturamento";
					} else if (cd_motivo == 43) {
						solicitante = "sistemaSigma";
						motivoEspecifico = "Pendência de Negociação";
					} else if (cd_motivo == 45 || cd_motivo == 46 || cd_motivo == 47) {
						motivoEspecifico = "Fechamento de FAP";
					} else if (cd_motivo == 50 || cd_motivo == 51){
						motivoEspecifico = "Fechamento de FAP CORP";
					} else if (cd_motivo == 49) {
						motivoEspecifico = "Validação da instalação";
					} else if (cd_motivo == 48) {
						motivoEspecifico = "FAP de Rastreamento";
					} else if (cd_motivo == 52) {
						motivoEspecifico = "Fechamento de FAP + Administrativa";
					}

					String cliente = rs.getString("FANTASIA") + " (" + rs.getString("RAZAO") + ")";
					log.warn(numeroOS + "<br>");
					@SuppressWarnings("rawtypes")
					List listOSTarefa = PersistEngine.getObjects(osInfo.getEntityClass(), new QLEqualsFilter("numeroOS", numeroOS), 0, 1, "");

					if (listOSTarefa.isEmpty()) {
						String titulo = "";
						String descricao = "";
						String descricaoAux = ""; // Utilizada para pausa de código
						// 44 onde são criados ambos RSC e
						// Tarefa Simples

						if (cd_motivo == 9 || cd_motivo == 44) {
							descricao = "";
							descricao = " DETALHES DA OS: \n";
							descricao = descricao + " Conta: " + conta + "[" + particao + "]" + "\n";
							descricao = descricao + " Cliente: " + cliente + "\n";
							descricao = descricao + " Número da OS: " + numeroOS + " - " + rs.getString("FANTASIA") + "\n";
							descricao = descricao + " Contrato/Complemento: " + contrato + "/" + complemento + "\n";
							descricao = descricao + " Equipamento: " + flag_equip + "\n";
							descricao = descricao + " Responsável: " + responsavel + "\n";
							descricao = descricao + " Fone 1: " + fone1 + "\n";
							descricao = descricao + " Fone 2: " + fone2 + "\n";
							descricao = descricao + " Defeito: " + defeito + "\n";
							descricao = descricao + " Descrição: " + descricaoDefeito + "\n";
							descricao = descricao + " Pausada em: " + dataPausa + "\n";
							descricao = descricao + " Tipo da Pausa: " + tipoPausa + "\n";
							descricao = descricao + " Motivo da Pausa: " + motivoPausa + "\n";
							descricao = descricao + " Técnico: " + tecnico + "\n";
							descricao = descricao + " Motivo de abertura: " + motivoEspecifico + " \n";
							descricao = descricao.replaceAll("'", "");
							descricao = descricao.replaceAll("\"", "");

						} else {
							descricao = "";
							descricao = "<strong>DETALHES DA OS:</strong><br>";
							descricao = descricao + "<strong>Conta:</strong> " + conta + "[" + particao + "]" + "<br>";
							descricao = descricao + "<strong>Cliente:</strong> " + cliente + "<br>";
							descricao = descricao + "<strong>Contrato/Complemento:</strong> " + contrato + "/" + complemento + "<br>";
							descricao = descricao + "<strong>Equipamento:</strong> " + flag_equip + "<br>";
							descricao = descricao + "<strong>Responsável:</strong> " + responsavel + "<br>";
							descricao = descricao + "<strong>Fone 1:</strong> " + fone1 + "<br>";
							descricao = descricao + "<strong>Fone 2:</strong> " + fone2 + "<br>";
							descricao = descricao + "<strong>Defeito:</strong> " + defeito + "<br>";
							descricao = descricao + "<strong>Descrição:</strong> " + descricaoDefeito + "<br>";
							descricao = descricao + "<strong>Pausada em:</strong> " + dataPausa + "<br>";
							descricao = descricao + "<strong>Tipo da Pausa:</strong> " + tipoPausa + "<br>";
							descricao = descricao + "<strong>Motivo da Pausa:</strong> " + motivoPausa + "<br>";
							descricao = descricao + "<strong>Técnico:</strong> " + tecnico + "<br>";
							descricao = descricao + "<br><br>";
							descricao = descricao + "<strong>Motivo de abertura: " + motivoEspecifico + "</strong>";
							descricao = descricao.replaceAll("'", "");
							descricao = descricao.replaceAll("\"", "");

							if (cd_motivo == 43) {
								descricao = URLEncoder.encode(descricao, "ISO-8859-1");
								titulo = URLEncoder.encode("OS: " + numeroOS + " - " + rs.getString("FANTASIA"), "ISO-8859-1");
							} else if (cd_motivo == 45 || cd_motivo == 46 || cd_motivo == 47 || cd_motivo == 50 || cd_motivo == 51) {
								titulo = "OS Fechamento de FAP: " + numeroOS + " - " + rs.getString("FANTASIA");
							} else if (cd_motivo == 48) {
								titulo = "OS FAP Rastreamento: " + numeroOS + " - " + rs.getString("FANTASIA");
							} else if (cd_motivo == 52) {
								titulo = "OS FAP Rastreamento + ADM: " + numeroOS + " - " + rs.getString("FANTASIA");
							} else {
								titulo = "OS: " + numeroOS + " - " + rs.getString("FANTASIA");
							}
						}



						if (cd_motivo == 44) {
							descricaoAux = "";
							descricaoAux = "<strong>DETALHES DA OS:</strong><br>";
							descricaoAux = descricaoAux + "<strong>Conta:</strong> " + conta + "[" + particao + "]" + "<br>";
							descricaoAux = descricaoAux + "<strong>Cliente:</strong> " + cliente + "<br>";
							descricaoAux = descricaoAux + "<strong>Contrato/Complemento:</strong> " + contrato + "/" + complemento + "<br>";
							descricaoAux = descricaoAux + "<strong>Equipamento:</strong> " + flag_equip + "<br>";
							descricaoAux = descricaoAux + "<strong>Responsável:</strong> " + responsavel + "<br>";
							descricaoAux = descricaoAux + "<strong>Fone 1:</strong> " + fone1 + "<br>";
							descricaoAux = descricaoAux + "<strong>Fone 2:</strong> " + fone2 + "<br>";
							descricaoAux = descricaoAux + "<strong>Defeito:</strong> " + defeito + "<br>";
							descricaoAux = descricaoAux + "<strong>Descrição:</strong> " + descricaoDefeito + "<br>";
							descricaoAux = descricaoAux + "<strong>Pausada em:</strong> " + dataPausa + "<br>";
							descricaoAux = descricaoAux + "<strong>Tipo da Pausa:</strong> " + tipoPausa + "<br>";
							descricaoAux = descricaoAux + "<strong>Motivo da Pausa:</strong> " + motivoPausa + "<br>";
							descricaoAux = descricaoAux + "<strong>Técnico:</strong> " + tecnico + "<br>";
							descricaoAux = descricaoAux + "<br><br>";
							descricaoAux = descricaoAux + "<strong>Motivo de abertura: " + motivoEspecifico + "</strong>";
							descricaoAux = descricaoAux.replaceAll("'", "");
							descricaoAux = descricaoAux.replaceAll("\"", "");

							titulo = "OS: " + numeroOS + " - " + rs.getString("FANTASIA");

						}

						log.error("#PAUSAOS# MONTOU DESCRICAO OS:"+numeroOS+" DESCRICAO: \n"+descricao );

						NeoPaper papelSolicitantePausaAdm = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteTarefaOSPausada" + regional));
						NeoPaper papelExecutorPausaAdm = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteTarefaOSPausada" + regional));
						NeoPaper papelExecutorPausaAgendamento = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "ExecutorTarefaOSPausaAgendamento" + regional));

						executor = OrsegupsUtils.getUserNeoPaper(papelExecutorPausaAdm); //EXECUTOR PADRÃO

						String urlTarefa = "";
						if (cd_motivo == 1) {

							String papel = "SolicitanteAbreTarefaOS" + regional;
							solicitante = OrsegupsUtils.getUserNeoPaper(papel);
							prazo = cal4;
							isAcanca = "nao";
							executor = "";
						}

						if (cd_motivo == 2 || cd_motivo == 44 || cd_motivo == 52) {
							if (idEmpresa == 10075 || idEmpresa == 10129 || idEmpresa == 10127 || idEmpresa == 10144) {
								solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteAbreTarefaOSRastreamento");
								executor = OrsegupsUtils.getUserNeoPaper("ExecutorTarefaOSRastreamento");
							} else {
								solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitantePausaAdm);				    
							}

							prazo = cal2;
							isAcanca = "nao";
							executor = "";

							if(cd_motivo == 44){
								solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitantePausaAdm);
								executor = OrsegupsUtils.getUserNeoPaper(papelExecutorPausaAdm);			    
								isAcanca = "sim";
							}			    
						}

						if ((cd_motivo == 9 || cd_motivo == 44) && !osRaiaApp) {
							salvarContato(cidade, emailRes, fone, contatoResp, descricao, cpfCgc, tecnico, numeroOS, tipoEmpresa, cd_motivo, regional);
						}

						if (cd_motivo == 41) {
							solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitantePausaAdm);			
							isAcanca = "sim";
							prazo = cal4;

							if (idEmpresa == 10075 || idEmpresa == 10129 || idEmpresa == 10127 || idEmpresa == 10144){
								solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteAbreTarefaOSRastreamento");

								if(instalacaoRastreamento) {
									executor = OrsegupsUtils.getUserNeoPaper("ExecutorTarefaOSRastreamentoInstalacao");
								} else {
									executor = OrsegupsUtils.getUserNeoPaper("ExecutorTarefaOSRastreamento");
								}

							}else if ((id_instalador == 108479) || (id_instalador == 108480) || (id_instalador == 108481) || (id_instalador == 108491) || (id_instalador == 108482) || (id_instalador == 108483) || (id_instalador == 108484) || (id_instalador == 108485) || (id_instalador == 108486)
									|| (id_instalador == 108487) || (id_instalador == 108488) || (id_instalador == 108489) || (id_instalador == 108493) || (id_instalador == 108490)) {
								executor = OrsegupsUtils.getUserNeoPaper("ExecutorTarefaOSInstaladores");
							} else {
								executor = OrsegupsUtils.getUserNeoPaper(papelExecutorPausaAgendamento);
							}
						}

						if (cd_motivo == 42) {

							executor = OrsegupsUtils.getUserNeoGroup("Gerência CEREC");
							isAcanca = "sim";
							prazo = cal2;
						}
						if (cd_motivo == 43) {
							urlTarefa = LINK + "custom/jsp/orsegups/sigma/iniciarTRDC.jsp?avanca=sim&solicitante=" + solicitante + "&origem=3&descricao=" + descricao + "&titulo=" + titulo + "&idOrdem=" + numeroOS;

						}		    

						if ((idEmpresa == 10119 || idEmpresa == 10120) && instalacao && cd_motivo != 2) {//PRO E ACM
							try {
								solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteFAPPRO");
								// TAREFA FUSION 1344497
								
								executor = (idOsDefeito == 1168) ? retornaResponsavelPROInstalacao(regional) : OrsegupsUtils.getUserNeoPaper(OrsegupsUtils.getPapelGerenteRegional(OrsegupsUtils.getCodigoRegional(regional)));

								// TODO TAREFA 1720779 - Renoa receber as FAP de TAG PRO de IAI
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (solicitante == null || solicitante.isEmpty()) {
								solicitante = "edson.heinz"; // GARANTE SOLICITANTE
							}			
						}else if (cd_motivo == 45) {
							solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteFAPInstalacao");
						} else if (cd_motivo == 46) {
							solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteAbreTarefaOSFAP");
							isAcanca = "nao";
							executor = "";
							prazo = cal2;
						} else if (cd_motivo == 47) {
							solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteFAPManutencao");
						} else if (cd_motivo == 48) {
							solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteFAPRastreamento");		
						} else if(cd_motivo == 100) { 
							solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteFAPRastreamentoInstalacao");
						} else if (cd_motivo == 50 || cd_motivo == 51) {
							try {
								solicitante = FAPUtils.getSolicitanteRegional(regional);
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (solicitante == null || solicitante.isEmpty()) {
								solicitante = "edson.heinz"; // GARANTE SOLICITANTE SE PAPEU VAZIO
							}

						}

						if (cd_motivo == 49) {
							solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteAbreTarefaOSValidarInstalacao");// SolicitanteAbreTarefaOSValidarInstalacao
							prazo = cal2;
							isAcanca = "nao";
							executor = "";
						}

						String fapCorpTS = "";

						if (cd_motivo != 9) {
							log.warn("##### EXECUTAR ABERTURA DE TAREFA OS - URL " + urlTarefa.toString() + " \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
							String retorno = "";

							if (urlTarefa != null && !urlTarefa.equals("")) {
								urlTarefa = urlTarefa.trim();
								urlTarefa = urlTarefa.replaceAll(" ", "");
								URL urlAux = new URL(urlTarefa);
								URLConnection uc = urlAux.openConnection();
								BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
								retorno = in.readLine();
							} else {
								IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
								// Adicionado condicional para utilizar descricaoAux
								// quando código 44

								if (descricao.length() > 7998) {
									descricao = descricao.substring(0, 7997);
								}

								if(cdGrupOCliente == 128L) {
									solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteTarefaOSPRO");
								}

								if (cd_motivo == 44) {

									retorno = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricaoAux, "3", isAcanca, prazo);

								} else if (cd_motivo == 52) {

									fapCorpTS = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricaoAux, "3", isAcanca, prazo);

									try {
										solicitante = FAPUtils.getSolicitanteRegional(regional);
									} catch (Exception e) {
										e.printStackTrace();
									}

									if (solicitante == null || solicitante.isEmpty()) {
										solicitante = "edson.heinz";
									}

								} else if (cd_motivo == 45 || cd_motivo == 47 || cd_motivo == 48 || cd_motivo == 50 || cd_motivo == 51 || cd_motivo == 52 || cd_motivo == 100) {

									log.error("#PAUSAOS# ENTROU NA ABERTURA DE FAP OS:"+numeroOS);

									StringBuilder relatoServico = new StringBuilder();

									relatoServico.append("Descrição OS: \r\n");
									relatoServico.append(descricaoDefeito);
									relatoServico.append("\r\n");
									relatoServico.append("Descrição da Pausa: \r\n");
									relatoServico.append(motivoPausa);	

									if (cd_motivo == 48 || cd_motivo == 100) {
										if (cd_motivo == 48) {
											try {
												retorno = FAPUtils.abrirFAPManutencaoRastreamento(solicitante, cdCliente, relatoServico.toString(), listaOs);
												log.error("#PAUSAOS# ABRIU FAP OS:"+numeroOS+" FAP: "+retorno);
											} catch (Exception e) {
												System.out.println("#RotinaAbreTarefaOS# ERRO AO INICIAR FAP");
												e.printStackTrace();
											}
										} else if (cd_motivo == 100) {
											try {
												retorno = FAPUtils.abrirFAPInstalacaoRastreamento(solicitante, cdCliente, relatoServico.toString(), listaOs);
												log.error("#PAUSAOS# ABRIU FAP OS:"+numeroOS+" FAP: "+retorno);
											} catch (Exception e) {
												System.out.println("#RotinaAbreTarefaOS# ERRO AO INICIAR FAP");
												e.printStackTrace();
											}
										}

									} else if (cd_motivo == 45 || cd_motivo == 51) {
										if (tipoPessoa == 2) {
											try {
												retorno = FAPUtils.abrirFAPInstalacaoMercadoPublico(solicitante, cdCliente, relatoServico.toString(), listaOs);
												log.error("#PAUSAOS# ABRIU FAP OS:"+numeroOS+" FAP: "+retorno);
											} catch (Exception e) {
												System.out.println("#RotinaAbreTarefaOS# ERRO AO INICIAR FAP");
												e.printStackTrace();
											}
										} else {
											try {
												retorno = FAPUtils.abrirFAPInstalacaoMercadoPrivado(solicitante, cdCliente, relatoServico.toString(), listaOs);
												log.error("#PAUSAOS# ABRIU FAP OS:"+numeroOS+" FAP: "+retorno);
											} catch (Exception e) {
												System.out.println("#RotinaAbreTarefaOS# ERRO AO INICIAR FAP");
												e.printStackTrace();
											}
										}

									} else {
										if (tipoPessoa == 2) {
											try {
												retorno = FAPUtils.abrirFAPManutencaoMercadoPublico(solicitante, cdCliente, relatoServico.toString(), listaOs);
												log.error("#PAUSAOS# ABRIU FAP OS:"+numeroOS+" FAP: "+retorno);
											} catch (Exception e) {
												System.out.println("#RotinaAbreTarefaOS# ERRO AO INICIAR FAP");
												e.printStackTrace();
											}
										} else {
											try {
												retorno = FAPUtils.abrirFAPManutencaoMercadoPrivado(solicitante, cdCliente, relatoServico.toString(), listaOs);
												log.error("#PAUSAOS# ABRIU FAP OS:"+numeroOS+" FAP: "+retorno);
											} catch (Exception e) {
												System.out.println("#RotinaAbreTarefaOS# ERRO AO INICIAR FAP");
												e.printStackTrace();
											}
										}
									}

									if (retorno == "Erro #3  Erro ao avançar a primeira tarefa") {

										log.error("#PAUSAOS# CAIU EM ERRO DE FAP OS:"+numeroOS+" FAP: "+retorno);

										try {
											InstantiableEntityInfo errosVerificados = AdapterUtils.getInstantiableEntityInfo("gravaOSErroFAP");
											NeoObject objErrosVerificador = errosVerificados.createNewInstance();
											EntityWrapper wrapperErrosVerificador = new EntityWrapper(objErrosVerificador);

											String motivoErroOSFAP = "NÃo foi encontrado vinculo entre o cliente Sapiens e Sigma";

											String equipamento = "Equipamento Cliente";

											if (this.isEquipamentoOrsegups(cdCliente)) {
												// Equipamento Orsegups
												equipamento = "Equipamento Orsegups";
											}

											GregorianCalendar dtPausa = new GregorianCalendar();
											dtPausa.setTime(rs.getDate("DT_PAUSA"));

											wrapperErrosVerificador.findField("cdCliente").setValue(cdCliente);
											wrapperErrosVerificador.findField("conta").setValue(conta);
											wrapperErrosVerificador.findField("contrato").setValue(contrato);
											wrapperErrosVerificador.findField("defeito").setValue(defeito);
											wrapperErrosVerificador.findField("descricaoDefeito").setValue(descricaoDefeito);
											wrapperErrosVerificador.findField("dtPausa").setValue(dtPausa);
											wrapperErrosVerificador.findField("motivoErroOSFAP").setValue(motivoErroOSFAP);
											wrapperErrosVerificador.findField("motivoEspecifico").setValue(motivoEspecifico);
											wrapperErrosVerificador.findField("numeroOS").setValue(numeroOS);
											wrapperErrosVerificador.findField("responsavel").setValue(responsavel);
											wrapperErrosVerificador.findField("tecnico").setValue(tecnico);
											wrapperErrosVerificador.findField("fone1").setValue(fone1);
											wrapperErrosVerificador.findField("fone2").setValue(fone2);
											wrapperErrosVerificador.findField("equipamento").setValue(equipamento);
											wrapperErrosVerificador.findField("motivoPausa").setValue(motivoPausa);

											PersistEngine.persist(objErrosVerificador);
										} catch (Exception e) {
											e.printStackTrace();
										}



									}

								} else {
									retorno = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "3", isAcanca, prazo);
								}

							}

							if (retorno != null && !retorno.isEmpty() && (!retorno.trim().contains("Erro"))) {

								// Executar fechamento da OS no SIGMA e gravar
								// relação OS x Tarefa no EFORM:
								// SIGMAControleUltimaOS

								String textoFechamento = "Fechamento automático via Tarefa: " + retorno;

								if (cd_motivo == 44) {
									textoFechamento = "Fechamento automático via Tarefa: " + retorno + " e RSC: " + numeroTarefaRSC;
								}

								if (cd_motivo == 45 || cd_motivo == 46  || cd_motivo == 47 || cd_motivo == 48 || cd_motivo == 100  || cd_motivo == 50 || cd_motivo == 51) {
									textoFechamento = "Fechamento automático via FAP: " + retorno;
								}

								if (cd_motivo == 52) {
									textoFechamento = "Fechamento automático via FAP: " + retorno + " e Tarefa: " + fapCorpTS;
								}

								if(!osRaiaApp) {
									this.fechamentoOS(numeroOS, retorno, textoFechamento);
								}
							} else {
								log.error("Erro #7 - Erro ao criar tarefa");
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("[" + key + "] ##### ERRO AO EXECUTAR ABERTURA DE TAREFA OS - URL \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
				}


			}
		} catch (Exception e) {

			log.error("##### ERRO AO EXECUTAR ABERTURA DE TAREFA OS - URL \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			System.out.println("[" + key + "] ##### ERRO AO EXECUTAR ABERTURA DE TAREFA OS - URL \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		} finally {
			try {
				OrsegupsUtils.closeConnection(connSapiens, st3, rsEquip);
				OrsegupsUtils.closeConnection(conn, st, rs);
				log.warn("##### FINALIZAR ABERTURA DE TAREFA OS - URL  Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

			} catch (Exception e) {

				log.error("##### ERRO AO FINALIZAR ABERTURA DE TAREFA OS - URL " + e.getMessage().toString() + " \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
				return;
			}
		}

	}

	private String retornaResponsavelPROInstalacao(String regional) {
		
		String usuario = null;
		
		try {
			
			usuario = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(OrsegupsUtils.getPaper("ResponsavelPROInstalacao"+regional)).getCode();
			
			if(NeoUtils.safeIsNull(usuario)) {
				usuario = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(OrsegupsUtils.getPaper("ResponsavelPROInstalacaoPadrao")).getCode();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(this.getClass().getSimpleName() + " - Erro ao retornar o usuário executor da regional "+regional);
		}
		
		return usuario;
	}

	private void salvarContato(String cidadeCli, String emailRes, String fone, String contatoResp, String descricaoCli, Long CpfCgc, String tecnico, Long numeroOS, int tipoEmpresa, int cd_motivo, String regional) {
		{
			try {
				QLGroupFilter filterUser = new QLGroupFilter("AND ");
				filterUser.addFilter(new QLEqualsFilter("usu_cgccpf", CpfCgc));
				filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));

				// Busca Cliente
				List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);

				Long codcli = 0L;
				if (users != null && users.size() > 0) {
					EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
					codcli = (Long) usuarioCliente.getValue("usu_codcli");

				}

				// Busca os valores por Request
				String nome = contatoResp;
				String email = emailRes;
				String telefone = fone;
				String mensagem = descricaoCli;
				String cidade = cidadeCli;

				// String workflow =
				// "C027 - RSC - Relatório de Solicitação de Cliente Novo";
				// String fieldTelefone = "telefones";
				// String fieldContato = "contato";
				// String fieldMensagem = "descricao";
				// String valueMensagem = "Tipo do Contato: " +
				// descricaoTipoContato + "\nDescrição: ";
				// String valueSolicitante = "sistemaFusionPaper";

				// Busca o solicitante
				// NeoPaper papel = new NeoPaper();
				NeoUser responsavelAtendimento = (NeoUser) PersistEngine.getObject(SecurityEntity.class, new QLEqualsFilter("code", "leonardo.berka"));
				
				// NeoPaper obj = (NeoPaper)
				// PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"),
				// new QLEqualsFilter("code", valueSolicitante));
				// papel = (NeoPaper) obj;
				// Privado
				String gestaoCerec = null;
				if (tipoEmpresa == 1) {
					gestaoCerec = "Responsável Pendência Orçamento";

				} else {// Publico
					if (tipoEmpresa == 2) {
						gestaoCerec = "Responsável Pendência Orçamento Publico";
					} else {
						gestaoCerec = "Responsável Pendência Orçamento";
					}
				}

				// Prazo para o atendimento da Solicitação
				GregorianCalendar dataResponderRRCTemp = new GregorianCalendar();
				GregorianCalendar dataResponderRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 3L);
				dataResponderRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataResponderRRC.set(Calendar.MINUTE, 59);
				dataResponderRRC.set(Calendar.SECOND, 59);

				log.warn("##### EXECUTAR ABERTURA DE TAREFA OS RSC - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

				QLEqualsFilter ftrCodigo = new QLEqualsFilter("codigo", 6L);
				List<NeoObject> lstCategoriaRSC = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCCategoriaNovo"), ftrCodigo);
				NeoObject tipoSolicitacao = lstCategoriaRSC.get(0);

				QLEqualsFilter ftrRegional = new QLEqualsFilter("siglaRegional", regional);
				List<NeoObject> lstRegional = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCEscritorioRegional"), ftrRegional);
				NeoObject oRegional = lstRegional.get(0);

				RSCVO rsc = new RSCVO();
				rsc.setCodCli(codcli);
				rsc.setOrigemSolicitacao("13"); // SIGMA OS PAUSADA
				rsc.setTipoContato("11715150"); // orçamento
				rsc.setNome(nome);
				rsc.setEmail(email);
				rsc.setTelefone(telefone);

				if (mensagem.length() >= 3000){
					mensagem = mensagem.substring(0, 2999);
				}

				rsc.setMensagem(mensagem);
				rsc.setCidade(cidade);
				rsc.setPrazo(dataResponderRRC);
				rsc.setResponsavelAtendimento(responsavelAtendimento);
				rsc.setTipoSolicitacao(tipoSolicitacao);
				rsc.setRegional(oRegional);

				String tarefa = "";

				try {
					tarefa = RSCHelper.abrirRSC(rsc, gestaoCerec, false, true, false);
					numeroTarefaRSC = tarefa;
				} catch (Exception e) {
					System.out.println("#RotinaAbreTarefaOS# ERRO AO INICIAR RRC");
					e.printStackTrace();
				}
				log.error("#RotinaAbreTarefaOS# ERRO AO INICIAR RRC");

				String textoFechamento = "Fechamento automático via RSC: " + tarefa;

				if (!tarefa.isEmpty()) {
					if (this.fechamentoOS(numeroOS, tarefa, textoFechamento)) {
						log.warn("##### SUCESSO AO EXECUTAR FECHAMENTO OS SIGMA RSC - Nº OS " + numeroOS.toString() + " E DESCRIÇÃO " + textoFechamento.toString() + " \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Erro ao Abrir a RSC Pela Classe RotinaAbreTarefaOS  e metodo salvarContato!");
			}
		}
	}

	public Calendar addBusinessDays(Calendar cal, int numBusinessDays) {
		int numNonBusinessDays = 0;

		for (int i = 0; i < numBusinessDays; i++) {
			cal.add(Calendar.DATE, 1);

			if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
				numNonBusinessDays++;
			}
		}

		if (numNonBusinessDays > 0) {
			cal.add(Calendar.DATE, numNonBusinessDays);
		}

		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			cal.add(Calendar.DATE, 2);
		}
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			cal.add(Calendar.DATE, 1);
		}

		return cal;
	}

	/**
	 * Grava relação no Eform SIGMAControleUltimaOS e altera status da OS no
	 * SIGMA
	 * 
	 * @param numeroOS
	 *            Long
	 * @param numeroTarefa
	 *            String
	 * @return boolean true se conclui o fechamento da OS sem problemas false
	 *         caso algum problema com a base de dados tenha acontecido
	 * @throws SQLException
	 * @author mateus.batista
	 * @version 1.0
	 * @since 07/03/2016
	 */
	private boolean fechamentoOS(Long numeroOS, String numeroTarefa, String textoFechamento) {

		Connection conn = null;
		PreparedStatement pstm = null;

		InstantiableEntityInfo osInfo = AdapterUtils.getInstantiableEntityInfo("SIGMAControleUltimaOS");
		NeoObject objOS = osInfo.createNewInstance();
		EntityWrapper wrpOS = new EntityWrapper(objOS);
		wrpOS.findField("numeroOS").setValue(numeroOS);
		wrpOS.findField("numeroTarefa").setValue(Long.parseLong(numeroTarefa));
		StringBuffer sqlFechaOS = new StringBuffer();
		sqlFechaOS.append("UPDATE dbORDEM SET FECHADO = 1, OPFECHOU = 11010, FECHAMENTO = GETDATE(), IDOSCAUSADEFEITO = 1, IDOSSOLUCAO = 249, EXECUTADO = ? WHERE ID_ORDEM = ? ");

		try {
			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(sqlFechaOS.toString());
			pstm.setString(1, textoFechamento);
			pstm.setLong(2, numeroOS);
			int resultado = pstm.executeUpdate();
			while (resultado == 0) {
				resultado = pstm.executeUpdate();
			}
			PersistEngine.persist(objOS);

			log.warn("##### SUCESSO AO EXECUTAR FECHAMENTO OS SIGMA - Nº OS " + numeroOS.toString() + " E DESCRIÇÃO " + textoFechamento.toString() + " \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

			return true;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}

		log.warn("##### ERRO AO EXECUTAR FECHAMENTO OS SIGMA - Nº OS " + numeroOS.toString() + " E DESCRIÇÃO " + textoFechamento.toString() + " \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

		return false;
	}

	/**
	 * Retorna Tipo da empresa conforme SQL especifico
	 * 
	 * @param cdCliente
	 *            Long
	 * @return tipoEmpresa int
	 * @throws SQLException
	 * @author mateus.batista
	 * @version 1.0
	 * @since 08/03/2016
	 */

	private int getTipoEmpresa(Long cdCliente) {

		int tipoEmpresa = 0;

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		StringBuffer SQL = new StringBuffer();
		SQL.append(" SELECT cvs.USU_CodSer, CLI.TipEmc as tipemc ");
		SQL.append(" FROM USU_T160SIG sig ");
		SQL.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
		SQL.append(" INNER JOIN USU_T160CTR ctr ON ctr.usu_codemp = cvs.usu_codemp and ctr.usu_codfil = cvs.usu_codfil and ctr.usu_numctr = cvs.usu_numctr ");
		SQL.append(" inner join dbo.E085CLI cli ON CLI.CodCli = CTR.usu_codcli ");
		SQL.append(" WHERE sig.usu_codcli = ? ");
		SQL.append(" GROUP BY cvs.USU_CodSer, CLI.TipEmc ");
		SQL.append(" ORDER BY cvs.USU_CodSer ");

		try {
			conn = PersistEngine.getConnection("SAPIENS");
			pstm = conn.prepareStatement(SQL.toString());
			pstm.setLong(1, cdCliente);

			rs = pstm.executeQuery();

			if (rs.next()) {
				tipoEmpresa = rs.getInt("tipemc");
			}

			return tipoEmpresa;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return tipoEmpresa;
	}

	/**
	 * Retorna se deve ser alterado icone do equipamento
	 * 
	 * @param cdCliente
	 *            Long
	 * @return boolean
	 * @throws SQLException
	 * @author mateus.batista
	 * @version 1.0
	 * @since 08/03/2016
	 */
	private boolean isEquipamentoOrsegups(Long cdCliente) {

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		StringBuffer SQL = new StringBuffer();
		SQL.append(" SELECT cvs.USU_CodSer ");
		SQL.append(" FROM USU_T160SIG sig ");
		SQL.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
		SQL.append(" WHERE sig.usu_codcli = ?");
		SQL.append(" AND cvs.usu_codser IN ('9002035','9002011', '9002004', '9002005','9002014') ");
		SQL.append(" ORDER BY cvs.usu_sitcvs ");

		try {
			conn = PersistEngine.getConnection("SAPIENS");
			pstm = conn.prepareStatement(SQL.toString());
			pstm.setLong(1, cdCliente);

			rs = pstm.executeQuery();

			if (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return false;
	}

	private boolean validaVerificacaoAreaTecnica(Long numeroOS) {
		boolean validada = false;

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();


		try	{
			conn = PersistEngine.getConnection("SIGMA90");

			sql.append("SELECT NOTA FROM OS_NOTAS WHERE ID_ORDEM = ? AND  (NOTA = '#PAGAR' OR NOTA = '#PAGARADM')");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, numeroOS);
			rs = pstm.executeQuery();

			if(rs.next()) {
				validada = true;								
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}


		return validada;
	}

}