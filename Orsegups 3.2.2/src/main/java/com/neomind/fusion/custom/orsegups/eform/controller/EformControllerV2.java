package com.neomind.fusion.custom.orsegups.eform.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.log.SysoLogger;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.eform.vo.Chp1ClienteVO;
import com.neomind.fusion.custom.orsegups.eform.vo.Chp1VO;
import com.neomind.fusion.custom.orsegups.eform.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.eform.vo.EformVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLBPAFilter;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.persist.query.QLTranslator;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class EformControllerV2 {

    public List<EformVO> getEmpresas() {

	List<NeoObject> empresas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1Empresa"), null);
	List<EformVO> empresasVo = new ArrayList<EformVO>();
	for (NeoObject no : empresas) {
	    EformVO e = new EformVO();
	    EntityWrapper wp = new EntityWrapper(no);
	    e.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
	    e.setDescricao(wp.findField("nome").getValue().toString());

	    empresasVo.add(e);
	}

	return empresasVo;
    }

    public List<EformVO> getOperadoras() {

	List<NeoObject> operadoras = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1Operadora"), null);
	List<EformVO> operadorasVo = new ArrayList<EformVO>();
	for (NeoObject no : operadoras) {
	    EformVO e = new EformVO();
	    EntityWrapper wp = new EntityWrapper(no);
	    e.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
	    e.setDescricao(wp.findField("nome").getValue().toString());

	    operadorasVo.add(e);
	}

	return operadorasVo;
    }

    public List<Chp1VO> getChipes(String inicio, String fim) {

	List<Chp1VO> chipesVo = new ArrayList<Chp1VO>();
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {
	    /*
	     * conn = PersistEngine.getConnection("");
	     * 
	     * StringBuffer varname1 = new StringBuffer();
	     * varname1.append("SELECT neoId "); varname1.append(
	     * "  FROM (SELECT ROW_NUMBER() OVER (ORDER BY neoId DESC) AS RowNum, * "
	     * ); varname1.append("          FROM D_Chp1 ");
	     * varname1.append("       ) AS RowConstrainedResult ");
	     * varname1.append(" WHERE RowNum >= ? ");
	     * varname1.append("   AND RowNum < ? ");
	     * varname1.append(" ORDER BY RowNum");
	     * 
	     * pstm = conn.prepareStatement(varname1.toString()); pstm.setInt(1,
	     * new Integer(inicio)); pstm.setInt(2, new Integer(fim));
	     * 
	     * rs = pstm.executeQuery();
	     * 
	     * while (rs.next()) {
	     * 
	     * //NeoObject o =
	     * PersistEngine.getObject(AdapterUtils.getEntityClass("errosJobs"),
	     * groupFilter); NeoObject no =
	     * PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1"),new
	     * QLEqualsFilter("neoId", rs.getLong("neoId"))); Chp1VO e = new
	     * Chp1VO(); EntityWrapper wp = new EntityWrapper(no);
	     * e.setId(Integer
	     * .parseInt(wp.findField("neoId").getValue().toString()));
	     * e.setIccId(wp.findField("IccId").getValue().toString());
	     * e.setLinha(wp.findField("linha").getValue().toString()); if
	     * (wp.findField("fEmpresa.neoId") != null)
	     * e.setEmpresa(getEmpresa(wp
	     * .findField("fEmpresa.neoId").getValue().toString())); if
	     * (wp.findField("fOperadora.neoId") != null)
	     * e.setOperadora(getOperadora
	     * (wp.findField("fOperadora.neoId").getValue().toString())); if
	     * (wp.findField("fOperadora2.neoId") != null)
	     * e.setOperadora2(getOperadora
	     * (wp.findField("fOperadora2.neoId").getValue().toString())); if
	     * (wp.findField("fGCEscritorioRegional.neoId") != null)
	     * e.setRegional
	     * (getRegional(wp.findField("fGCEscritorioRegional.neoId"
	     * ).getValue().toString())); e.setAtivo((Boolean)
	     * wp.findField("ativo").getValue());
	     * 
	     * chipesVo.add(e); }
	     */

	    List<NeoObject> chipes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1"), null, -1, 100000, "neoId DESC");
	    int co = 0;
	    for (NeoObject no : chipes) {
		Chp1VO e = new Chp1VO();
		EntityWrapper wp = new EntityWrapper(no);
		e.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
		if (wp.findField("iccId") != null && wp.findField("iccId").getValue() != null)
		    e.setIccId(wp.findField("iccId").getValue().toString());
		if (wp.findField("linha") != null && wp.findField("linha").getValue() != null)
		    e.setLinha(wp.findField("linha").getValue().toString());
		if (wp.findField("fEmpresa.neoId") != null)
		    e.setEmpresa(getEmpresa(wp.findField("fEmpresa.neoId").getValue().toString()));
		if (wp.findField("fOperadora.neoId") != null)
		    e.setOperadora(getOperadora(wp.findField("fOperadora.neoId").getValue().toString()));
		if (wp.findField("fOperadora2.neoId") != null)
		    e.setOperadora2(getOperadora(wp.findField("fOperadora2.neoId").getValue().toString()));
		if (wp.findField("fGCEscritorioRegional.neoId") != null)
		    e.setRegional(getRegional(wp.findField("fGCEscritorioRegional.neoId").getValue().toString()));
		if (wp.findField("ativo") != null)
		    e.setAtivo((Boolean) wp.findField("ativo").getValue());

		co++;

		chipesVo.add(e);
	    }

	} catch (Exception e) {

	}

	return chipesVo;
    }

    public List<EformVO> getRegionais() {

	List<NeoObject> regionais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCEscritorioRegional"), null);
	List<EformVO> regionaisVo = new ArrayList<EformVO>();
	for (NeoObject no : regionais) {
	    EformVO e = new EformVO();
	    EntityWrapper wp = new EntityWrapper(no);
	    e.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
	    e.setDescricao(wp.findField("siglaRegional").getValue().toString() + " - " + wp.findField("nome").getValue().toString());

	    regionaisVo.add(e);
	}

	return regionaisVo;
    }

    public EformVO getEmpresa(String _neoId) {

	NeoObject no = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("Chp1Empresa"), Long.parseLong(_neoId));
	EntityWrapper ew = new EntityWrapper(no);

	EformVO empresa = new EformVO();
	empresa.setId(Integer.parseInt(_neoId));
	empresa.setDescricao(ew.findField("nome").getValue().toString());

	return empresa;
    }

    public EformVO getOperadora(String _neoId) {

	NeoObject no = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("Chp1Operadora"), Long.parseLong(_neoId));
	EntityWrapper ew = new EntityWrapper(no);

	EformVO operadora = new EformVO();
	operadora.setId(Integer.parseInt(_neoId));
	operadora.setDescricao(ew.findField("nome").getValue().toString());

	return operadora;
    }

    public EformVO getRegional(String _neoId) {

	NeoObject no = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), Long.parseLong(_neoId));
	EntityWrapper ew = new EntityWrapper(no);

	EformVO regional = new EformVO();
	regional.setId(Integer.parseInt(_neoId));
	regional.setDescricao(ew.findField("nome").getValue().toString());

	return regional;
    }

    public ClienteVO getClient(String _cdCliente) {

	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	ClienteVO c = new ClienteVO();

	try {
	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("SELECT ");
	    varname1.append("       CD_CLIENTE, ");
	    varname1.append("       ID_EMPRESA, ");
	    varname1.append("       ID_CENTRAL, ");
	    varname1.append("       PARTICAO, ");
	    varname1.append("       RAZAO, ");
	    varname1.append("       ENDERECO ");
	    varname1.append("    from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("   where CD_CLIENTE = " + _cdCliente);

	    pst = conn.prepareStatement(varname1.toString());

	    rs = pst.executeQuery();

	    while (rs.next()) {
		c.setCdCliente(rs.getInt("CD_CLIENTE"));
		c.setIdEmpresa("" + rs.getInt("ID_EMPRESA"));
		c.setIdCentral(rs.getString("ID_CENTRAL"));
		c.setParticao(rs.getString("PARTICAO"));
		c.setRazaoSocial(rs.getString("RAZAO"));
		c.setEndereco(rs.getString("ENDERECO"));
	    }

	    return c;
	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar cliente: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public boolean verificaIccIdExistente(String _iccId) {

	String sql = "SELECT COUNT(1) as conta FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 WHERE iccId = '" + _iccId + "'";
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer count = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append(sql);
	    pst = conn.prepareStatement(varname1.toString());

	    rs = pst.executeQuery();

	    while (rs.next()) {
		count = rs.getInt("conta");
	    }

	    if (count > 0)
		return true;

	    return false;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao verificar IccId existente: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}

    }

    public Integer insereDataRemocaoPorIccId(String _iccId) {

	String sql = " UPDATE D_Chp1Cliente set dataRemocao = GETDATE(),motivoRemocao = 'Atualizado via SISTEMA' where chp1_neoId like ? and dataRemocao is null";
	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement pst = null;

	try {
	    conn = PersistEngine.getConnection("");
	    pst = conn.prepareStatement(sql);
	    pst.setString(1, _iccId);

	    int contador = pst.executeUpdate();

	    return contador;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao atualizar data de remoção por IccId: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer insereDataRemocaoPorCliente(String _cliente) {

	String sql = " UPDATE D_Chp1Cliente set dataRemocao = GETDATE(),motivoRemocao = 'Atualizado via SISTEMA' where cdCliente = ? and dataRemocao is null";
	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement pst = null;
	try {
	    conn = PersistEngine.getConnection("");
	    pst = conn.prepareStatement(sql);

	    pst.setString(1, _cliente);

	    int contador = pst.executeUpdate();
	    return contador;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao atualizar data de remoção por Cliente: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaChipes() {

	String sql = "SELECT COUNT(1) as conta FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1";
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append(sql);

	    pst = conn.prepareStatement(varname1.toString());

	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;// total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar chip: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public String setChip(String _iccId, String _empresa, String _operadora, String _operadora2, String _regional, String _linha, String _ativo, String _neoId, String _dataCancelamento) {

	if (!_neoId.equals("")) { // atualiza

	    QLGroupFilter groupFilter = new QLGroupFilter("AND");
	    groupFilter.addFilter(new QLEqualsFilter("neoId", Long.parseLong(_neoId)));
	    NeoObject o = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1"), groupFilter);

	    final EntityWrapper ewWkfOCP = new EntityWrapper(o);
	    // ewWkfOCP.findField("iccId").setValue(_iccId);
	    ewWkfOCP.findField("ativo").setValue((Integer.parseInt(_ativo) == 1) ? Boolean.TRUE : Boolean.FALSE);
	    ewWkfOCP.findField("linha").setValue(_linha);
	    ewWkfOCP.findField("valor").setValue(0f);

	    QLGroupFilter groupFilter2 = new QLGroupFilter("AND");
	    groupFilter2.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_empresa)));
	    NeoObject _fEmpresa = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Empresa"), groupFilter2);
	    ewWkfOCP.findField("fEmpresa").setValue(_fEmpresa);

	    QLGroupFilter gFO1 = new QLGroupFilter("AND");
	    gFO1.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_operadora)));
	    NeoObject _fOperadora = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Operadora"), gFO1);
	    ewWkfOCP.findField("fOperadora").setValue(_fOperadora);

	    if (_operadora2 != null && !_operadora2.equals("null") && !_operadora2.equals("")) {
		QLGroupFilter gFO2 = new QLGroupFilter("AND");
		gFO2.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_operadora2)));
		NeoObject _fOperadora2 = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Operadora"), gFO2);
		ewWkfOCP.findField("fOperadora2").setValue(_fOperadora2);
	    }

	    if (_regional != null && !_regional.equals("null") && !_regional.equals("")) {
		QLGroupFilter gFO3 = new QLGroupFilter("AND");
		gFO3.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_regional)));
		NeoObject _fRegional = PersistEngine.getObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), gFO3);
		ewWkfOCP.findField("fGCEscritorioRegional").setValue(_fRegional);
	    }

	    if (_dataCancelamento != null && !_dataCancelamento.equals("null") && !_dataCancelamento.equals("")) {
		String array[] = new String[3];
		array = _dataCancelamento.split("-");

		ewWkfOCP.findField("dataCancelamento").setValue(new GregorianCalendar(Integer.parseInt(array[2]), Integer.parseInt(array[1]) - 1, Integer.parseInt(array[0])));
	    }

	    PersistEngine.persist(o);

	} else {

	    NeoObject wkfOCP = AdapterUtils.createNewEntityInstance("Chp1");
	    final EntityWrapper ewWkfOCP = new EntityWrapper(wkfOCP);
	    ewWkfOCP.findField("iccId").setValue(_iccId);
	    ewWkfOCP.findField("ativo").setValue((Integer.parseInt(_ativo) == 1) ? Boolean.TRUE : Boolean.FALSE);
	    ewWkfOCP.findField("linha").setValue(_linha);
	    ewWkfOCP.findField("valor").setValue(0f);
	    ewWkfOCP.findField("dataCadastro").setValue(new GregorianCalendar());

	    ewWkfOCP.findField("removerChp1").setValue(Boolean.FALSE);

	    QLGroupFilter groupFilter = new QLGroupFilter("AND");
	    groupFilter.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_empresa)));
	    NeoObject _fEmpresa = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Empresa"), groupFilter);
	    ewWkfOCP.findField("fEmpresa").setValue(_fEmpresa);

	    QLGroupFilter gFO1 = new QLGroupFilter("AND");
	    gFO1.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_operadora)));
	    NeoObject _fOperadora = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Operadora"), gFO1);
	    ewWkfOCP.findField("fOperadora").setValue(_fOperadora);

	    if (_operadora2 != null && !_operadora2.equals("null") && !_operadora2.equals("")) {
		QLGroupFilter gFO2 = new QLGroupFilter("AND");
		gFO2.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_operadora2)));
		NeoObject _fOperadora2 = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Operadora"), gFO2);
		ewWkfOCP.findField("fOperadora2").setValue(_fOperadora2);
	    }

	    if (_regional != null && !_regional.equals("null") && !_regional.equals("")) {
		QLGroupFilter gFO3 = new QLGroupFilter("AND");
		gFO3.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_regional)));
		NeoObject _fRegional = PersistEngine.getObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), gFO3);
		ewWkfOCP.findField("fGCEscritorioRegional").setValue(_fRegional);
	    }

	    if (_dataCancelamento != null && !_dataCancelamento.equals("null") && !_dataCancelamento.equals("")) {
		String array[] = new String[3];
		array = _dataCancelamento.split("-");

		ewWkfOCP.findField("dataCancelamento").setValue(new GregorianCalendar(Integer.parseInt(array[2]), Integer.parseInt(array[1]) - 1, Integer.parseInt(array[0])));
	    }

	    PersistEngine.persist(wkfOCP);
	}

	return "";
    }

    public List<ClienteVO> getCliente(String _cliente) {

	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;

	List<ClienteVO> clientes = new ArrayList<ClienteVO>();

	try {
	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select top 10 ");
	    varname1.append("       a.CD_CLIENTE, ");
	    varname1.append("       a.ID_EMPRESA, ");
	    varname1.append("       a.ID_CENTRAL, ");
	    varname1.append("       a.PARTICAO, ");
	    varname1.append("       a.RAZAO, ");
	    varname1.append("       a.ID_CENTRAL+\'/\'+a.PARTICAO+\'/\'+CAST(a.ID_EMPRESA as varchar(200))+\'-\'+a.RAZAO AS D_EMPRESA ");
	    varname1.append("  from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL a ");
	    varname1.append(" where a.ID_CENTRAL+\'/\'+a.PARTICAO+\'/\'+a.RAZAO like \'");
	    varname1.append(_cliente);
	    varname1.append("%\' ");
	    // varname1.append("    and a.PARTICAO = ");
	    // varname1.append("              (select MIN(CAST(PARTICAO as INT)) ");
	    // varname1.append("                 from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    // varname1.append("                where a.ID_CENTRAL = ID_CENTRAL ");
	    // varname1.append("                  and a.ID_EMPRESA = ID_EMPRESA) ");
	    varname1.append("    and a.FG_ATIVO = 1 ");
	    varname1.append("    and a.CTRL_CENTRAL = 1 ");
	    varname1.append("    and a.PARTICAO <> \'AAA\' ");
	    varname1.append("order by a.ID_CENTRAL,a.RAZAO,a.PARTICAO");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		ClienteVO c = new ClienteVO();
		c.setId(rs.getInt("CD_CLIENTE"));
		c.setDescricao(rs.getString("D_EMPRESA"));
		c.setCdCliente(rs.getInt("CD_CLIENTE"));
		c.setIdEmpresa("" + rs.getInt("ID_EMPRESA"));
		c.setIdCentral(rs.getString("ID_CENTRAL"));
		c.setParticao(rs.getString("PARTICAO"));
		c.setRazaoSocial(rs.getString("RAZAO"));
		c.setEmpresaParticaoRazao(rs.getString("D_EMPRESA"));

		clientes.add(c);
	    }

	    return clientes;
	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar cliente: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}

    }

    public List<ClienteVO> getClientePorCdCliente(String _cdCliente) {

	// List<NeoObject> lientes = (List<NeoObject>)
	// PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"),
	// null);

	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	// Integer total = 0;

	List<ClienteVO> clientes = new ArrayList<ClienteVO>();

	try {
	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select a.CD_CLIENTE, ");
	    varname1.append("             a.ID_EMPRESA, ");
	    varname1.append("             a.ID_CENTRAL, ");
	    varname1.append("             a.PARTICAO, ");
	    varname1.append("             a.RAZAO, ");
	    varname1.append("             a.ID_CENTRAL+'/'+a.PARTICAO+'/'+CAST(a.ID_EMPRESA as varchar(200))+'-'+a.RAZAO AS D_EMPRESA ");
	    varname1.append("        from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL a ");
	    varname1.append("       where exists (select d.id_empresa, ");
	    varname1.append("							d.id_central ");
	    varname1.append("					   from dbCENTRAL d ");
	    varname1.append("					  where d.CD_CLIENTE = ");
	    varname1.append(_cdCliente);
	    varname1.append("					    and a.ID_EMPRESA = d.ID_EMPRESA ");
	    varname1.append("					    and a.ID_CENTRAL = d.ID_CENTRAL) ");
	    varname1.append("         and a.FG_ATIVO = 1 ");
	    varname1.append("         and a.CTRL_CENTRAL = 1 ");
	    varname1.append("         and a.PARTICAO <> 'AAA' ");
	    varname1.append("       order by a.ID_CENTRAL, ");
	    varname1.append("                a.RAZAO,a.PARTICAO");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		ClienteVO c = new ClienteVO();
		c.setId(rs.getInt("CD_CLIENTE"));
		c.setDescricao(rs.getString("D_EMPRESA"));
		c.setCdCliente(rs.getInt("CD_CLIENTE"));
		c.setIdEmpresa("" + rs.getInt("ID_EMPRESA"));
		c.setIdCentral(rs.getString("ID_CENTRAL"));
		c.setParticao(rs.getString("PARTICAO"));
		c.setRazaoSocial(rs.getString("RAZAO"));
		c.setEmpresaParticaoRazao(rs.getString("D_EMPRESA"));

		clientes.add(c);
	    }

	    return clientes;
	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar cliente por código: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public List<Chp1ClienteVO> getChp1Clientes() {

	List<Chp1ClienteVO> chp1ClientesVo = new ArrayList<Chp1ClienteVO>();

	QLFilterIsNotNull q = new QLFilterIsNotNull("dataInstalacao");

	List<NeoObject> chp1Clientes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1Cliente"), q, -1, 100000, "motivoRemocao, neoId DESC");
	GregorianCalendar data = new GregorianCalendar();
	GregorianCalendar data2 = new GregorianCalendar();

	for (NeoObject no : chp1Clientes) {

	    ClienteVO cliente = new ClienteVO();
	    Chp1VO chp1 = new Chp1VO();
	    Chp1ClienteVO e = new Chp1ClienteVO();

	    EntityWrapper wp = new EntityWrapper(no);
	    e.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
	    if (wp.findField("cdCliente") != null) {
		if (wp.findField("cdCliente").getValue() != null)
		    cliente.setCdCliente(Integer.parseInt(wp.findField("cdCliente").getValue().toString()));
	    }
	    if (wp.findField("contaSIGMA") != null) {
		if (wp.findField("contaSIGMA").getValue() != null)
		    cliente.setIdCentral(wp.findField("contaSIGMA").getValue().toString());
	    }
	    if (wp.findField("empresaConta") != null) {
		if (wp.findField("empresaConta").getValue() != null)
		    cliente.setIdEmpresa(wp.findField("empresaConta").getValue().toString());
	    }
	    if (wp.findField("endereco") != null) {
		if (wp.findField("endereco").getValue() != null)
		    cliente.setEndereco(wp.findField("endereco").getValue().toString());
	    }
	    if (wp.findField("particao") != null) {
		if (wp.findField("particao").getValue() != null)
		    cliente.setParticao(wp.findField("particao").getValue().toString());
	    }
	    if (wp.findField("razaoSocial") != null) {
		if (wp.findField("razaoSocial").getValue() != null)
		    cliente.setRazaoSocial(wp.findField("razaoSocial").getValue().toString());
	    }
	    if (wp.findField("dataInstalacao") != null) {
		if (wp.findField("dataInstalacao").getValue() != null) {
		    data = (GregorianCalendar) wp.findField("dataInstalacao").getValue();
		    e.setDataInstalacao(data);
		    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		    fmt.setCalendar(data);
		    String Sdata = fmt.format(data.getTime());
		    e.setDataInstalacaoFormatada(Sdata);
		}
	    }
	    if (wp.findField("dataRemocao") != null) {
		if (wp.findField("dataRemocao").getValue() != null) {
		    data2 = (GregorianCalendar) wp.findField("dataRemocao").getValue();
		    e.setDataRemocao(data2);
		    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		    fmt.setCalendar(data2);
		    String Sdata = fmt.format(data2.getTime());
		    e.setDataRemocaoFormatada(Sdata);
		}
	    }
	    // System.out.println(wp.findField("Chp1.IccId").getValue());

	    if (wp.findField("Chp1.iccId") != null)
		if (wp.findField("Chp1.iccId").getValue() != null)
		    chp1.setIccId(wp.findField("Chp1.iccId").getValue().toString());

	    e.setCliente(cliente);
	    e.setChp1(chp1);

	    chp1ClientesVo.add(e);
	}
	return chp1ClientesVo;
    }

    public Integer contaChipClientes() {

	return null;
    }

    public List<Chp1VO> getChip(String _chip) {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	// Integer total = 0;

	List<Chp1VO> chips = new ArrayList<Chp1VO>();

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select top 10 ");
	    varname1.append("       a.neoId, ");
	    varname1.append("       a.iccId, ");
	    varname1.append("       a.linha, ");
	    varname1.append("       a.iccId+\'-\'+upper(c.nome)+\'-\'+UPPER(b.nome) as d_iccid ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Empresa b on a.FEmpresa_neoId = b.neoId ");
	    varname1.append(" inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Operadora c on a.FOperadora_neoId = c.neoId ");
	    varname1.append(" where iccId like \'");
	    varname1.append(_chip);
	    varname1.append("%\' ");
	    varname1.append(" order by 4");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		Chp1VO c = new Chp1VO();
		c.setId(rs.getInt("neoId"));
		c.setDescricao(rs.getString("d_iccid"));
		c.setIccId(rs.getString("iccId"));
		c.setLinha(rs.getString("linha"));

		chips.add(c);
	    }

	    return chips;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar chip: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public String setChp1Cliente(String _cdCliente, String _iccId, String _replicar) {

	if (_replicar.equals("S")) {

	    List<ClienteVO> clientes = new ArrayList<ClienteVO>();
	    clientes = getClientePorCdCliente(_cdCliente);

	    insereDataRemocaoPorIccId(_iccId);
	    insereDataRemocaoPorCliente("" + _cdCliente);

	    for (ClienteVO cliente : clientes) {

		QLGroupFilter gFO1 = new QLGroupFilter("AND");
		gFO1.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_iccId)));
		NeoObject _fChp1 = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1"), gFO1);

		NeoObject wkfOCP = AdapterUtils.createNewEntityInstance("Chp1Cliente");
		final EntityWrapper ewWkfOCP = new EntityWrapper(wkfOCP);
		ewWkfOCP.findField("cdCliente").setValue(Long.parseLong("" + cliente.getCdCliente()));
		ewWkfOCP.findField("contaSIGMA").setValue(cliente.getIdCentral());
		ewWkfOCP.findField("empresaConta").setValue(Long.parseLong(cliente.getIdEmpresa()));
		ewWkfOCP.findField("endereco").setValue(cliente.getEndereco());
		ewWkfOCP.findField("particao").setValue(cliente.getParticao());
		ewWkfOCP.findField("razaoSocial").setValue(cliente.getRazaoSocial());

		ewWkfOCP.findField("dataInstalacao").setValue(new GregorianCalendar());

		ewWkfOCP.findField("Chp1").setValue(_fChp1);

		PersistEngine.persist(wkfOCP);
	    }

	} else {
	    insereDataRemocaoPorIccId(_iccId);
	    insereDataRemocaoPorCliente(_cdCliente);

	    ClienteVO cliente = new ClienteVO();
	    cliente = getClient(_cdCliente);

	    QLGroupFilter gFO1 = new QLGroupFilter("AND");
	    gFO1.addFilter(new QLEqualsFilter("neoId", Integer.parseInt(_iccId)));
	    NeoObject _fChp1 = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1"), gFO1);

	    NeoObject wkfOCP = AdapterUtils.createNewEntityInstance("Chp1Cliente");
	    final EntityWrapper ewWkfOCP = new EntityWrapper(wkfOCP);
	    ewWkfOCP.findField("cdCliente").setValue(Long.parseLong("" + cliente.getCdCliente()));
	    ewWkfOCP.findField("contaSIGMA").setValue(cliente.getIdCentral());
	    ewWkfOCP.findField("empresaConta").setValue(Long.parseLong(cliente.getIdEmpresa()));
	    ewWkfOCP.findField("endereco").setValue(cliente.getEndereco());
	    ewWkfOCP.findField("particao").setValue(cliente.getParticao());
	    ewWkfOCP.findField("razaoSocial").setValue(cliente.getRazaoSocial());

	    ewWkfOCP.findField("dataInstalacao").setValue(new GregorianCalendar());

	    ewWkfOCP.findField("Chp1").setValue(_fChp1);

	    PersistEngine.persist(wkfOCP);
	}

	/*
	 * insereDataRemocaoPorIccId(_iccId);
	 * insereDataRemocaoPorCliente(_cdCliente);
	 * 
	 * ClienteVO cliente = new ClienteVO(); cliente = getClient(_cdCliente);
	 * 
	 * QLGroupFilter gFO1 = new QLGroupFilter("AND"); gFO1.addFilter(new
	 * QLEqualsFilter("neoId", Integer.parseInt(_iccId))); NeoObject _fChp1
	 * = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1"), gFO1);
	 * 
	 * 
	 * NeoObject wkfOCP =
	 * AdapterUtils.createNewEntityInstance("Chp1Cliente"); final
	 * EntityWrapper ewWkfOCP= new EntityWrapper(wkfOCP);
	 * ewWkfOCP.findField(
	 * "cdCliente").setValue(Long.parseLong(""+cliente.getCdCliente()));
	 * ewWkfOCP.findField("contaSIGMA").setValue(cliente.getIdCentral());
	 * ewWkfOCP
	 * .findField("empresaConta").setValue(Long.parseLong(cliente.getIdEmpresa
	 * ())); ewWkfOCP.findField("endereco").setValue(cliente.getEndereco());
	 * ewWkfOCP.findField("particao").setValue(cliente.getParticao());
	 * ewWkfOCP.findField("razaoSocial").setValue(cliente.getRazaoSocial());
	 * 
	 * ewWkfOCP.findField("dataInstalacao").setValue(new
	 * GregorianCalendar());
	 * 
	 * ewWkfOCP.findField("Chp1").setValue(_fChp1);
	 * 
	 * PersistEngine.persist(wkfOCP);
	 */

	return "";
    }

    public List<ClienteVO> getCdCliente(String _cliente) {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	// Integer total = 0;

	List<ClienteVO> clientes = new ArrayList<ClienteVO>();

	try {
	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select top 10 ");
	    varname1.append("       a.CD_CLIENTE, ");
	    varname1.append("       a.ID_EMPRESA, ");
	    varname1.append("       a.ID_CENTRAL, ");
	    varname1.append("       a.PARTICAO, ");
	    varname1.append("       a.RAZAO, ");
	    varname1.append("       a.ID_CENTRAL+\'/\'+a.PARTICAO+\'/\'+CAST(a.ID_EMPRESA as varchar(200))+\'-\'+a.RAZAO AS D_EMPRESA ");
	    varname1.append("  from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL a ");
	    varname1.append(" where CAST(a.CD_CLIENTE as varchar(200))+\'/\'+a.ID_CENTRAL+\'/\'+a.RAZAO like \'");
	    varname1.append(_cliente);
	    varname1.append("%\' ");
	    varname1.append("    and a.PARTICAO = ");
	    varname1.append("              (select MIN(CAST(PARTICAO as INT)) ");
	    varname1.append("                 from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("                where a.ID_CENTRAL = ID_CENTRAL ");
	    varname1.append("                  and a.ID_EMPRESA = ID_EMPRESA) ");
	    varname1.append("    and a.FG_ATIVO = 1 ");
	    varname1.append("    and a.CTRL_CENTRAL = 1 ");
	    varname1.append("    and a.PARTICAO <> \'AAA\' ");
	    varname1.append("order by a.ID_CENTRAL,a.RAZAO,a.PARTICAO");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		ClienteVO c = new ClienteVO();
		c.setId(rs.getInt("CD_CLIENTE"));
		c.setDescricao(rs.getString("D_EMPRESA"));
		c.setCdCliente(rs.getInt("CD_CLIENTE"));
		c.setIdEmpresa("" + rs.getInt("ID_EMPRESA"));
		c.setIdCentral(rs.getString("ID_CENTRAL"));
		c.setParticao(rs.getString("PARTICAO"));
		c.setRazaoSocial(rs.getString("RAZAO"));
		c.setEmpresaParticaoRazao(rs.getString("D_EMPRESA"));

		clientes.add(c);
	    }

	    return clientes;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar clientes: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public List<EformVO> getRegionalRota(String _regionalRota) {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	// Integer total = 0;

	List<EformVO> rotas = new ArrayList<EformVO>();

	try {
	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select DISTINCT 0 as cod, ");
	    varname1.append("       (SUBSTRING(NM_ROTA,0,4))+\' (Toda Regional)\' as nome ");
	    varname1.append("  from ROTA ");
	    varname1.append(" where NM_ROTA like \'");
	    varname1.append(_regionalRota);
	    varname1.append("%\' ");
	    varname1.append("UNION ALL ");
	    varname1.append("select CD_ROTA as cod, ");
	    varname1.append("       NM_ROTA as nome ");
	    varname1.append("  from ROTA ");
	    varname1.append(" where NM_ROTA like \'");
	    varname1.append(_regionalRota);
	    varname1.append("%\' ");
	    varname1.append(" order by 2");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		EformVO c = new EformVO();
		c.setId(rs.getInt("cod"));
		c.setDescricao(rs.getString("nome"));

		rotas.add(c);
	    }

	    return rotas;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar rotas: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public List<EformVO> getTecnicoTercerizado(String _codRegionalRota, String _regionalRota, String _codTecnicoTercerizado, String _tecnicoTercerizado) {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	// Integer total = 0;
	String clausula = "";

	List<EformVO> clientes = new ArrayList<EformVO>();

	try {
	    if (_regionalRota != null && !_regionalRota.equals("")) {
		if (_regionalRota.equals("")) {
		    clausula += "";
		} else if (_regionalRota.equals("0")) {
		    clausula += "%" + _regionalRota.substring(0, 3) + "%" + _tecnicoTercerizado + "%";
		} else {
		    clausula += "%" + _tecnicoTercerizado + "%";
		}
	    } else {
		if (_tecnicoTercerizado != null && !_tecnicoTercerizado.equals("")) {
		    clausula += "%" + _tecnicoTercerizado + "%";
		}
	    }

	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select 0 as CD_COLABORADOR,  ");
	    varname1.append("       \' ");
	    varname1.append(_tecnicoTercerizado);
	    varname1.append(" (Todos)\' as NM_COLABORADOR ");
	    varname1.append("union all ");
	    varname1.append("select CD_COLABORADOR, ");
	    varname1.append("         NM_COLABORADOR ");
	    varname1.append("    from COLABORADOR ");
	    varname1.append("   where NM_COLABORADOR like \'");
	    varname1.append(clausula);
	    varname1.append("\'   order by NM_COLABORADOR");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		ClienteVO c = new ClienteVO();
		c.setId(rs.getInt("CD_COLABORADOR"));
		c.setDescricao(rs.getString("NM_COLABORADOR"));

		clientes.add(c);
	    }

	    return clientes;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar rotas: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTecnicoTercerizado(String _colaboradores, String _textoColaboradores, String _rotas, String _textoRotas) {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;
	String clausulaRotas = "";
	String clausulaColaboradores = "";

	try {

	    if (!clausulaRotas.equals("") && !_rotas.equals("0,")) {
		clausulaRotas = _rotas.substring(0, _rotas.length() - 1);
	    }
	    if (_rotas.equals("0,")) {
		clausulaRotas = _textoRotas.substring(0, 3);
	    } else if (!clausulaRotas.equals("")) {
		clausulaRotas = _rotas.substring(0, _rotas.length() - 1);
	    } else if (clausulaRotas.equals("")) {
		clausulaRotas = _rotas.substring(0, _rotas.length() - 1);
	    }

	    if (!_colaboradores.equals("") && !_colaboradores.equals("0,")) {
		clausulaColaboradores = _colaboradores.substring(0, _colaboradores.length() - 1);
	    }
	    if (_colaboradores.equals("0,")) {
		clausulaColaboradores = _textoColaboradores.substring(0, 3);
	    }

	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append(" select count(1) as conta");
	    varname1.append("  from ");
	    varname1.append("  ( ");
	    varname1.append("   select cd_cliente ");
	    varname1.append("     from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("    where ce.ctrl_central = 1 ");
	    varname1.append("      and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("      and id_central not like 'DDD%' ");
	    varname1.append("      and id_central not like 'FFF%' ");
	    varname1.append("      and id_central not like 'AA%' ");
	    varname1.append("      and id_central not like 'R%' ");
	    varname1.append(" union ");
	    varname1.append("   select cd_cliente ");
	    varname1.append("     from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("    where celular = 1 ");
	    varname1.append("      and ncelular = 'GPRS' ");
	    varname1.append("  ) as cliente_gprs ");
	    varname1.append("inner join ");
	    varname1.append(" ( ");
	    varname1.append("       select CD_CLIENTE, ");
	    varname1.append("              ID_CENTRAL, ");
	    varname1.append("              PARTICAO, ");
	    varname1.append("              ID_EMPRESA, ");
	    varname1.append("              RAZAO, ");
	    varname1.append("	          ID_INSTALADOR, ");
	    varname1.append("	          e.NOME as CIDADE, ");
	    varname1.append("	          d.NOME as BAIRRO, ");
	    varname1.append("	          GETDATE() as ABERTURA, ");
	    varname1.append("	          1 as FG_EMAIL_ENVIADO, ");
	    varname1.append("	          10011 as CD_OS_SOLICITANTE, ");
	    varname1.append("	          0 as FG_TODAS_PARTICOES_EM_MANUTENCAO, ");
	    varname1.append("	          1 as EXECUTADO, ");
	    varname1.append("	          1 as TEMPOEXECUCAOPREVISTO ");
	    varname1.append("	     from [FSOODB03\\SQL01].SIGMA90.dbo.dbCENTRAL c ");
	    varname1.append("   inner join [FSOODB03\\SQL01].SIGMA90.dbo.COLABORADOR b on CD_TECNICO_RESPONSAVEL = CD_COLABORADOR ");
	    varname1.append("   inner join [FSOODB03\\SQL01].SIGMA90.dbo.ROTA cc on c.ID_ROTA = cc.CD_ROTA ");
	    varname1.append("   inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbBAIRRO d on c.ID_BAIRRO = d.ID_BAIRRO ");
	    varname1.append("   inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbCIDADE e on d.ID_CIDADE = e.ID_CIDADE ");
	    varname1.append("   where c.cd_cliente = (select min(cd_cliente) ");
	    varname1.append("                           from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL d ");
	    varname1.append("                          where c.id_central = d.id_central and c.id_empresa = d.id_empresa) ");
	    varname1.append("     and not exists(select 1 ");
	    varname1.append("	                  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente aaa ");
	    varname1.append("	                 where aaa.cdCliente = c.CD_CLIENTE ");
	    varname1.append("	                   and aaa.dataRemocao is null)");
	    if (!clausulaRotas.equals("") && !_rotas.equals("0,")) {
		varname1.append("    and c.ID_ROTA in (											");
		varname1.append(clausulaRotas);
		varname1.append(") 														");
	    }
	    if (_rotas.equals("0,")) {
		varname1.append("    and cc.NM_ROTA like \'%");
		varname1.append(clausulaRotas);
		varname1.append("%\' 													");
	    }
	    if (!clausulaColaboradores.equals("") && !_colaboradores.equals("0,")) {
		varname1.append("    and c.CD_TECNICO_RESPONSAVEL in (									");
		varname1.append(clausulaColaboradores);
		varname1.append(") 														");
	    }
	    if (_colaboradores.equals("0,")) {
		varname1.append("    and b.NM_COLABORADOR like \'%");
		varname1.append(clausulaColaboradores);
		varname1.append("%\' 													");
	    }

	    varname1.append(") as min_cliente_sem_chip_cadastrado on cliente_gprs.cd_cliente = min_cliente_sem_chip_cadastrado.cd_cliente ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar técnicos terceirizados: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer gerarOS(String _colaboradores, String _textoColaboradores, String _rotas, String _textoRotas, String _qtdeGerar) {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;
	String clausulaRotas = "";
	String clausulaColaboradores = "";

	Integer cdCliente = 0;
	Integer idInstalador = 0;
	Timestamp abertura;
	String defeito = "";
	Integer opAbriu = 0;
	Integer idOsDefeito = 0;
	boolean fgEmailEnviado = false;
	Integer cdOsSolicitante = 0;
	boolean fgTodasParticoesEmManutencao = false;
	String executado = "";
	Integer tempoExecucaoPrevisto = 0;

	try {
	    if (!clausulaRotas.equals("") && !_rotas.equals("0,")) {
		clausulaRotas = _rotas.substring(0, _rotas.length() - 1);
	    }
	    if (_rotas.equals("0,")) {
		clausulaRotas = _textoRotas.substring(0, 3);
	    } else if (!clausulaRotas.equals("")) {
		clausulaRotas = _rotas.substring(0, _rotas.length() - 1);
	    } else if (clausulaRotas.equals("")) {
		clausulaRotas = _rotas.substring(0, _rotas.length() - 1);
	    }

	    if (!_colaboradores.equals("") && !_colaboradores.equals("0,")) {
		clausulaColaboradores = _colaboradores.substring(0, _colaboradores.length() - 1);
	    }
	    if (_colaboradores.equals("0,")) {
		clausulaColaboradores = _textoColaboradores.substring(0, 3);
	    }

	    conn = PersistEngine.getConnection("");

	    if (Integer.parseInt(_qtdeGerar) > 20) {
		_qtdeGerar = "20";
	    }

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append(" select TOP 													");
	    varname1.append(_qtdeGerar);
	    varname1.append("       min_cliente_sem_chip_cadastrado.CD_CLIENTE, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.ID_INSTALADOR, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.CIDADE, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.BAIRRO, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.ABERTURA, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.FG_EMAIL_ENVIADO, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.CD_OS_SOLICITANTE, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.FG_TODAS_PARTICOES_EM_MANUTENCAO, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.EXECUTADO, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.TEMPOEXECUCAOPREVISTO, ");
	    varname1.append("	   min_cliente_sem_chip_cadastrado.CD_TECNICO_RESPONSAVEL ");
	    varname1.append("  from ");
	    varname1.append("  ( ");
	    varname1.append("   select cd_cliente ");
	    varname1.append("     from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("    where ce.ctrl_central = 1 ");
	    varname1.append("      and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("      and id_central not like 'DDD%' ");
	    varname1.append("      and id_central not like 'FFF%' ");
	    varname1.append("      and id_central not like 'AA%' ");
	    varname1.append("      and id_central not like 'R%' ");
	    varname1.append(" union ");
	    varname1.append("   select cd_cliente ");
	    varname1.append("     from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("    where celular = 1 ");
	    varname1.append("      and ncelular = 'GPRS' ");
	    varname1.append("  ) as cliente_gprs ");
	    varname1.append("inner join ");
	    varname1.append(" ( ");
	    varname1.append("       select CD_CLIENTE, ");
	    varname1.append("              ID_CENTRAL, ");
	    varname1.append("              PARTICAO, ");
	    varname1.append("              ID_EMPRESA, ");
	    varname1.append("              RAZAO, ");
	    varname1.append("	          ID_INSTALADOR, ");
	    varname1.append("	          CD_TECNICO_RESPONSAVEL, ");
	    varname1.append("	          e.NOME as CIDADE, ");
	    varname1.append("	          d.NOME as BAIRRO, ");
	    varname1.append("	          GETDATE() as ABERTURA, ");
	    varname1.append("	          1 as FG_EMAIL_ENVIADO, ");
	    varname1.append("	          10011 as CD_OS_SOLICITANTE, ");
	    varname1.append("	          0 as FG_TODAS_PARTICOES_EM_MANUTENCAO, ");
	    varname1.append("	          1 as EXECUTADO, ");
	    varname1.append("	          1 as TEMPOEXECUCAOPREVISTO ");
	    varname1.append("	     from [FSOODB03\\SQL01].SIGMA90.dbo.dbCENTRAL c ");
	    varname1.append("   inner join [FSOODB03\\SQL01].SIGMA90.dbo.COLABORADOR b on CD_TECNICO_RESPONSAVEL = CD_COLABORADOR ");
	    varname1.append("   inner join [FSOODB03\\SQL01].SIGMA90.dbo.ROTA cc on c.ID_ROTA = cc.CD_ROTA ");
	    varname1.append("   inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbBAIRRO d on c.ID_BAIRRO = d.ID_BAIRRO ");
	    varname1.append("   inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbCIDADE e on d.ID_CIDADE = e.ID_CIDADE ");
	    varname1.append("   where c.cd_cliente = (select min(cd_cliente) ");
	    varname1.append("                           from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL d ");
	    varname1.append("                          where c.id_central = d.id_central and c.id_empresa = d.id_empresa) ");
	    varname1.append("     and not exists(select 1 ");
	    varname1.append("	                  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente aaa ");
	    varname1.append("	                 where aaa.cdCliente = c.CD_CLIENTE ");
	    varname1.append("	                   and aaa.dataRemocao is null)");
	    if (!clausulaRotas.equals("") && !_rotas.equals("0,")) {
		varname1.append("    and c.ID_ROTA in (											");
		varname1.append(clausulaRotas);
		varname1.append(") 														");
	    }
	    if (_rotas.equals("0,")) {
		varname1.append("    and cc.NM_ROTA like \'%");
		varname1.append(clausulaRotas);
		varname1.append("%\' 													");
	    }
	    if (!clausulaColaboradores.equals("") && !_colaboradores.equals("0,")) {
		varname1.append("    and c.CD_TECNICO_RESPONSAVEL in (									");
		varname1.append(clausulaColaboradores);
		varname1.append(") 														");
	    }
	    if (_colaboradores.equals("0,")) {
		varname1.append("    and b.NM_COLABORADOR like \'%");
		varname1.append(clausulaColaboradores);
		varname1.append("%\' 													");
	    }

	    varname1.append(") as min_cliente_sem_chip_cadastrado on cliente_gprs.cd_cliente = min_cliente_sem_chip_cadastrado.cd_cliente ");
	    varname1.append("   order by min_cliente_sem_chip_cadastrado.CIDADE, ");
	    varname1.append("            min_cliente_sem_chip_cadastrado.BAIRRO");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		cdCliente = rs.getInt("CD_CLIENTE");
		idInstalador = rs.getInt("CD_TECNICO_RESPONSAVEL");
		abertura = rs.getTimestamp("ABERTURA");
		idOsDefeito = 1019; // OS-REGISTRAR FOTO CHIP GPRS
		fgEmailEnviado = rs.getBoolean("FG_EMAIL_ENVIADO");
		cdOsSolicitante = rs.getInt("CD_OS_SOLICITANTE");
		fgTodasParticoesEmManutencao = rs.getBoolean("FG_TODAS_PARTICOES_EM_MANUTENCAO");
		executado = rs.getString("EXECUTADO");
		tempoExecucaoPrevisto = rs.getInt("TEMPOEXECUCAOPREVISTO");

		inserirOs(cdCliente, idInstalador, idOsDefeito, cdOsSolicitante);

	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao gerar OS: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public void inserirOs(Integer cdCliente, Integer idInstalador, Integer idOsDefeito, Integer cdOsSolicitante) {
	StringBuffer sql = new StringBuffer();
	sql.append("INSERT INTO [FSOODB03\\SQL01].SIGMA90.dbo.dbORDEM 	");
	sql.append("	      (CD_CLIENTE, 				"); // 1
	sql.append("	       ID_INSTALADOR, 				"); // 2
	sql.append("	       ABERTURA, 				"); // 3
	sql.append("	       DEFEITO, 				"); // 4
	sql.append("	       OPABRIU, 				"); // 5
	sql.append("	       IDOSDEFEITO, 				"); // 6
	sql.append("	       FG_EMAIL_ENVIADO, 			"); // 7
	sql.append("	       CD_OS_SOLICITANTE, 			"); // 8
	sql.append("	       FG_TODAS_PARTICOES_EM_MANUTENCAO, 	"); // 9
	sql.append("	       EXECUTADO, 				"); // 10
	sql.append("	       TEMPOEXECUCAOPREVISTO)			"); // 11
	sql.append("	VALUES(?, 					"); // 1
	sql.append("	       ?, 					"); // 2
	sql.append("	       GETDATE(), 				"); // 3
	sql.append("	       ?, 					"); // 4
	sql.append("	       ?, 					"); // 5
	sql.append("	       ?, 					"); // 6
	sql.append("	       ?, 					"); // 7
	sql.append("	       ?, 					"); // 8
	sql.append("	       ?, 					"); // 9
	sql.append("	       ?, 					"); // 10
	sql.append("	       1)					"); // 11

	String abertura = "ATENÇÃO: Registrar a foto do Chip GPRS da central. Havendo a necessidade de troca, retirar fotos dos 2 Chips(Novo e Antigo), marcar um X no Chip antigo para diferenciar.#FOTOCHIP# \r\n";
	Long opaAbriu = 11010L; // usuario que esta abrindo a OS - Fusion
	Long fgEmailEnviado = 0L;
	Long fgTodasParticoesEmManutencao = 0L;
	String executado = "";

	Connection conn = OrsegupsUtils.getSqlConnection("SIGMA90");
	ResultSet rs = null;
	PreparedStatement pst = null;

	try {
	    conn.setAutoCommit(false);

	    pst = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
	    // 1 - CD_CLIENTE
	    pst.setLong(1, cdCliente);
	    // 2 - ID_INSTALADOR - Técnico responsável informado no cadastro da
	    // conta
	    pst.setLong(2, idInstalador);
	    // 4 - DEFEITO
	    pst.setString(3, abertura);
	    // 5 - OPABRIU
	    pst.setLong(4, opaAbriu);
	    // 6 - IDOSDEFEITO
	    pst.setLong(5, idOsDefeito);
	    // 7 - FG_EMAIL_ENVIADO = 0
	    pst.setLong(6, fgEmailEnviado);
	    // 8 - CD_OS_SOLICITANTE
	    pst.setLong(7, cdOsSolicitante);
	    // 9 - FG_TODAS_PARTICOES_EM_MANUTENCAO
	    pst.setLong(8, fgTodasParticoesEmManutencao);
	    // 10 - EXECUTADO
	    pst.setString(9, executado);

	    pst.executeUpdate();
	    conn.commit();

	    rs = pst.getGeneratedKeys();

	    if (rs.next()) {
		// processEntity.setValue("numeroOSSigma",
		// generatedKeys.getLong(1));
	    } else {
		throw new SQLException("Não foi possível criar um número de OS");
	    }

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao inserir OS: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public int countChips(String _iccId, String _empresa, String _operadora, String _operadora2, String _regional, String _linha, String _cancelado, String _habilitado, String _dataCancelamento, String _emCliente, String _emEstoque) {

//	StringBuffer sql = new StringBuffer();
//	sql.append("SELECT");
//	sql.append(" count(*) as rows");
//	sql.append(" FROM D_Chp1 c");
//
//	if (!_operadora.equals("")) {
//	    sql.append(" INNER JOIN D_Chp1Operadora o ON o.neoId = c.FOperadora_neoId AND o.nome LIKE '%");
//	    sql.append(_operadora);
//	    sql.append("%'");
//	}
//
//	if (!_regional.equals("")) {
//	    sql.append(" INNER JOIN D_GCEscritorioRegional r ON r.neoId = c.FGCEscritorioRegional_neoId AND r.nome LIKE '%");
//	    sql.append(_regional);
//	    sql.append("%'");
//	}
//
//	if (!_empresa.equals("")) {
//	    sql.append(" INNER JOIN D_Chp1Empresa e ON e.neoId = c.FEmpresa_neoId AND e.nome LIKE '%");
//	    sql.append(_empresa);
//	    sql.append("%'");
//	}
//
//	if (_emCliente.equals("true")) {
//	    sql.append(" JOIN D_Chp1Cliente AS cc ON cc.chp1_neoId = c.neoId");
//	}
//
//	sql.append(" WHERE 1 = 1");
//
//	if (!_iccId.equals("")) {
//	    sql.append(" AND c.iccId LIKE '%");
//	    sql.append(_iccId);
//	    sql.append("%'");
//	}
//
//	if (!_linha.equals("")) {
//	    sql.append(" AND c.linha LIKE '%");
//	    sql.append(_linha);
//	    sql.append("%'");
//	}
//
//	if (_dataCancelamento.length() >= 10) {
//	    String datas[] = _dataCancelamento.split("-");
//	    _dataCancelamento = datas[2] + "-" + datas[1] + "-" + datas[0];
//	    sql.append(" AND c.dataCancelamento = '");
//	    sql.append(_dataCancelamento);
//	    sql.append("'");
//	}
//
//	if ((_cancelado.equals("true") && _habilitado.equals("true")) || (_cancelado.equals("") && _habilitado.equals(""))) {
//	    // Não é necessário filtrar
//	} else {
//	    if (_cancelado.equals("true")) {
//		sql.append(" AND c.dataCancelamento IS NOT NULL");
//	    } else if (_habilitado.equals("true")) {
//		sql.append(" AND c.dataCancelamento IS NULL");
//	    }
//	}
//
//	Connection conn = OrsegupsUtils.getSqlConnection("");
//	ResultSet rs = null;
//	PreparedStatement pst = null;
//
//	try {
//	    conn.setAutoCommit(false);
//
//	    pst = conn.prepareStatement(sql.toString());
//	    rs = pst.executeQuery();
//
//	    rs.next();
//	    return rs.getInt("rows");
//
//	} catch (Exception e) {
//	    System.err.println("sql:" + pst);
//	    e.printStackTrace();
//
//	    throw new WorkflowException("Erro ao contar chips " + e.getMessage());
//	} finally {
//	    OrsegupsUtils.closeConnection(conn, pst, rs);
//	}

	QLGroupFilter gFO1 = new QLGroupFilter("AND");
	if (!_iccId.equals("")) {
	    gFO1.addFilter(new QLOpFilter("iccId", "LIKE", "%" + _iccId + "%"));
	}
	if (!_empresa.equals("")) {
	    gFO1.addFilter(new QLOpFilter("fEmpresa.nome", "LIKE", "%" + _empresa + "%"));
	}
	if (!_operadora.equals("")) {
	    QLGroupFilter gFO2 = new QLGroupFilter("OR");
	    gFO2.addFilter(new QLOpFilter("fOperadora.nome", "LIKE", "%" + _operadora + "%"));
	    gFO2.addFilter(new QLOpFilter("fOperadora2.nome", "LIKE", "%" + _operadora + "%"));
	    gFO1.addFilter(gFO2);
	}
	if (!_regional.equals("")) {
	    gFO1.addFilter(new QLOpFilter("fGCEscritorioRegional.nome", "LIKE", "%" + _regional + "%"));
	}
	if (!_linha.equals("")) {
	    gFO1.addFilter(new QLOpFilter("linha", "LIKE", "%" + _linha + "%"));
	}
	if (_dataCancelamento.length() >= 10) {
	    String datas[] = _dataCancelamento.split("-");
	    _dataCancelamento = datas[2] + "-" + datas[1] + "-" + datas[0];
	    gFO1.addFilter(new QLRawFilter("dataCancelamento = '" + _dataCancelamento + "'"));
	}

	if ((_cancelado.equals("true") && _habilitado.equals("true")) || (_cancelado.equals("") && _habilitado.equals(""))) {
	    // Não é necessário filtrar
	} else {
	    if (_cancelado.equals("true")) {
		gFO1.addFilter(new QLRawFilter("dataCancelamento IS NOT NULL"));
	    } else if (_habilitado.equals("true")) {
		gFO1.addFilter(new QLRawFilter("dataCancelamento IS NULL"));
	    }
	}

	if (_emCliente.equals("true")) {
	    gFO1.addFilter(new QLRawFilter("_vo IN (SELECT D.chp1 FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente D WHERE D.dataRemocao IS NOT NULL)"));
	}

	List<NeoObject> retorno = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1"), gFO1);

	return retorno.size();
    }

    public List<Chp1VO> getChip(String _iccId, String _empresa, String _operadora, String _operadora2, String _regional, String _linha, String _dataCancelamento, String _cancelado, String _habilitado, String _emCliente, String _emEstoque, String _pageNumber, String _pageResults) {

	// StringBuffer sql = new StringBuffer();
	//
	// sql.append("SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY c.neoId) AS RowNum, ");
	// sql.append(" c.iccId, c.linha, c.ativo, c.dataCancelamento, o.nome as nomeOperadora, o.neoId as operadoraId, e.nome as nomeEmpresa, e.neoId as empresaId, r.nome as nomeRegional, r.neoId as regionalId");
	// sql.append(" FROM D_Chp1 c");
	// sql.append(" INNER JOIN D_Chp1Operadora o ON o.neoId = c.FOperadora_neoId AND o.nome LIKE '%");
	// sql.append(_operadora);
	// sql.append("%'");
	// sql.append(" INNER JOIN D_GCEscritorioRegional r ON r.neoId = c.FGCEscritorioRegional_neoId AND r.nome LIKE '%");
	// sql.append(_regional);
	// sql.append("%'");
	// sql.append(" INNER JOIN D_Chp1Empresa e ON e.neoId = c.FEmpresa_neoId AND e.nome LIKE '%");
	// sql.append(_empresa);
	// sql.append("%'");
	//
	// if (_emCliente.equals("true")) {
	// sql.append(" JOIN D_Chp1Cliente AS cc ON cc.chp1_neoId = c.neoId");
	// }
	//
	// sql.append(" WHERE 1 = 1");
	//
	// if (!_iccId.equals("")) {
	// sql.append(" AND c.iccId LIKE '%");
	// sql.append(_iccId);
	// sql.append("%'");
	// }
	//
	// if (!_linha.equals("")) {
	// sql.append(" AND c.linha LIKE '%");
	// sql.append(_linha);
	// sql.append("%'");
	// }
	//
	// if (_dataCancelamento.length() >= 10) {
	// String datas[] = _dataCancelamento.split("-");
	// _dataCancelamento = datas[2] + "-" + datas[1] + "-" + datas[0];
	// sql.append(" AND c.dataCancelamento = '");
	// sql.append(_dataCancelamento);
	// sql.append("'");
	// }
	//
	// if ((_cancelado.equals("true") && _habilitado.equals("true")) ||
	// (_cancelado.equals("") && _habilitado.equals(""))) {
	// // Não é necessário filtrar
	// } else {
	// if (_cancelado.equals("true")) {
	// sql.append(" AND c.dataCancelamento IS NOT NULL");
	// } else if (_habilitado.equals("true")) {
	// sql.append(" AND c.dataCancelamento IS NULL");
	// }
	// }
	//
	// sql.append(") AS RowConstrainedResult WHERE");
	// sql.append(" RowNum >=");
	// sql.append(_pageNumber);
	// sql.append(" AND RowNum <= ");
	// sql.append(_pageResults);
	// sql.append(" ORDER BY RowNum");
	//
	// Connection conn = OrsegupsUtils.getSqlConnection("");
	// ResultSet rs = null;
	// PreparedStatement pst = null;
	//
	// try {
	// conn.setAutoCommit(false);
	//
	// pst = conn.prepareStatement(sql.toString());
	// rs = pst.executeQuery();
	//
	// List<Chp1VO> lista = new ArrayList<Chp1VO>();
	// while (rs.next()) {
	//
	// Chp1VO e = new Chp1VO();
	// e.setIccId(rs.getString("iccId"));
	// e.setLinha(rs.getString("linha"));
	//
	// Chp1VO formOperadora = new Chp1VO();
	// formOperadora.setId(rs.getInt("operadoraId"));
	// e.setOperadora(formOperadora);
	//
	// Chp1VO formEmpresa = new Chp1VO();
	// formEmpresa.setId(rs.getInt("empresaId"));
	// e.setEmpresa(formEmpresa);
	//
	// Chp1VO formRegional = new Chp1VO();
	// formRegional.setId(rs.getInt("regionalId"));
	// e.setRegional(formRegional);
	//
	// // if (_operadora2.equals("")){
	// // e.setOperadora2(null);
	// // }
	// // else{
	// // Chp1VO form = new Chp1VO();
	// // form.setId(rs.getInt("regionalId"));
	// // e.setEmpresa(form);
	// // }
	// e.setAtivo(rs.getBoolean("iccId"));
	//
	// // Converte Data de Cancelamento
	// if (!rs.getString("dataCancelamento").equals(""))
	// {
	// // String array[] = new String[3];
	// // array = rs.getString("dataCancelamento").split("-");
	// //
	// // GregorianCalendar data = new GregorianCalendar();
	// // data.set(Integer.parseInt(array[2].split(" ")[0]),
	// Integer.parseInt(array[1]), Integer.parseInt(array[0]));
	// // SimpleDateFormat fmt = new
	// SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	// e.setDataCancelamentoFormatada(rs.getString("dataCancelamento"));
	// }
	// else
	// {
	// e.setDataCancelamentoFormatada("");
	// }
	//
	// lista.add(e);
	// }
	//
	// return lista;
	//
	// } catch (Exception e) {
	// System.err.println("sql:" + pst);
	// e.printStackTrace();
	//
	// throw new WorkflowException("Erro ao contar chips " +
	// e.getMessage());
	// } finally {
	// OrsegupsUtils.closeConnection(conn, pst, rs);
	// }

	List<Chp1VO> lista = new ArrayList<Chp1VO>();

	QLGroupFilter gFO1 = new QLGroupFilter("AND");
	if (!_iccId.equals("")) {
	    gFO1.addFilter(new QLOpFilter("iccId", "LIKE", "%" + _iccId + "%"));
	}
	if (!_empresa.equals("")) {
	    gFO1.addFilter(new QLOpFilter("fEmpresa.nome", "LIKE", "%" + _empresa + "%"));
	}
	if (!_operadora.equals("")) {
	    gFO1.addFilter(new QLOpFilter("fOperadora.nome", "LIKE", "%" + _operadora + "%"));
	    // gFO1.addFilter(new QLOpFilter("fOperadora2.nome", "LIKE", "%" +
	    // _operadora + "%"));
	}
	if (!_regional.equals("")) {
	    gFO1.addFilter(new QLOpFilter("fGCEscritorioRegional.nome", "LIKE", "%" + _regional + "%"));
	}
	if (!_linha.equals("")) {
	    gFO1.addFilter(new QLOpFilter("linha", "LIKE", "%" + _linha + "%"));
	}
	if (_dataCancelamento.length() >= 10) {
	    String datas[] = _dataCancelamento.split("-");
	    _dataCancelamento = datas[2] + "-" + datas[1] + "-" + datas[0];
	    gFO1.addFilter(new QLRawFilter("dataCancelamento = '" + _dataCancelamento + "'"));
	}

	if ((_cancelado.equals("true") && _habilitado.equals("true")) || (_cancelado.equals("") && _habilitado.equals(""))) {
	    // Não é necessário filtrar
	} else {
	    if (_cancelado.equals("true")) {
		gFO1.addFilter(new QLRawFilter("dataCancelamento IS NOT NULL"));
	    } else if (_habilitado.equals("true")) {
		gFO1.addFilter(new QLRawFilter("dataCancelamento IS NULL"));
	    }
	}

	if (_emCliente.equals("true")) {
	    gFO1.addFilter(new QLRawFilter("_vo IN (SELECT D.chp1 FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente D WHERE D.dataRemocao IS NOT NULL)"));
	}

	// Calcula a página atual a ser mostrada
	int page = (Integer.parseInt(_pageNumber) - 1) * Integer.parseInt(_pageResults);

	@SuppressWarnings("unchecked")
	List<NeoObject> retorno = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1"), gFO1, page, Integer.parseInt(_pageResults), "neoId");

	for (NeoObject no : retorno) {

	    Chp1VO e = new Chp1VO();
	    EntityWrapper wp = new EntityWrapper(no);
	    e.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
	    if (wp.findField("iccId") != null && wp.findField("iccId").getValue() != null)
		e.setIccId(wp.findField("iccId").getValue().toString());
	    if (wp.findField("linha") != null && wp.findField("linha").getValue() != null)
		e.setLinha(wp.findField("linha").getValue().toString());
	    if (wp.findField("fEmpresa.neoId") != null)
		e.setEmpresa(getEmpresa(wp.findField("fEmpresa.neoId").getValue().toString()));
	    if (wp.findField("fOperadora.neoId") != null)
		e.setOperadora(getOperadora(wp.findField("fOperadora.neoId").getValue().toString()));
	    if (wp.findField("fOperadora2.neoId") != null)
		e.setOperadora2(getOperadora(wp.findField("fOperadora2.neoId").getValue().toString()));
	    if (wp.findField("fGCEscritorioRegional.neoId") != null)
		e.setRegional(getRegional(wp.findField("fGCEscritorioRegional.neoId").getValue().toString()));
	    if (wp.findField("ativo") != null)
		e.setAtivo((Boolean) wp.findField("ativo").getValue());
	    if (wp.findField("neoId") != null && wp.findField("neoId").getValue() != null)
		e.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
	    if (wp.findField("dataCancelamento") != null) {
		GregorianCalendar data = (GregorianCalendar) wp.findField("dataCancelamento").getValue();
		e.setDataCancelamento(data);
		if (data != null) {
		    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		    fmt.setCalendar(data);
		    String Sdata = fmt.format(data.getTime());
		    e.setDataCancelamentoFormatada(Sdata);
		}
	    }

	    lista.add(e);
	}

	return lista;
    }

    public List<Chp1ClienteVO> getChipCliente(String _iccId, String _empresa, String _instalacao, String _remocao) {

	List<Chp1ClienteVO> lista = new ArrayList<Chp1ClienteVO>();

	QLGroupFilter gFO1 = new QLGroupFilter("AND");
	if (!_iccId.equals("")) {
	    gFO1.addFilter(new QLOpFilter("Chp1.iccId", "LIKE", "%" + _iccId + "%"));
	}
	if (!_empresa.equals("")) {
	    // gFO1.addFilter(new QLOpFilter("razaoSocial", "LIKE",
	    // "%"+_empresa+"%"));
	    gFO1.addFilter(new QLOpFilter("contaSIGMA", "LIKE", "%" + _empresa + "%"));
	}
	if (!_instalacao.equals("")) {
	    gFO1.addFilter(new QLOpFilter("dataInstalacao", "LIKE", "%" + _instalacao + "%"));
	}
	if (!_remocao.equals("")) {
	    gFO1.addFilter(new QLOpFilter("dataRemocao", "LIKE", "%" + _remocao + "%"));
	}

	List<NeoObject> retorno = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1Cliente"), gFO1, "dataRemocao,contaSIGMA,particao");

	for (NeoObject no : retorno) {

	    Chp1ClienteVO e = new Chp1ClienteVO();
	    EntityWrapper wp = new EntityWrapper(no);
	    e.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
	    if (wp.findField("Chp1") != null && wp.findField("Chp1").getValue() != null) {
		Chp1VO chip = new Chp1VO();
		chip.setId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
		chip.setIccId(wp.findField("Chp1.iccId").getValue().toString());
		e.setChp1(chip);
		if (wp.findField("dataInstalacao") != null) {
		    if (wp.findField("dataInstalacao").getValue() != null) {
			GregorianCalendar data = (GregorianCalendar) wp.findField("dataInstalacao").getValue();
			e.setDataInstalacao(data);
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			fmt.setCalendar(data);
			String Sdata = fmt.format(data.getTime());
			e.setDataInstalacaoFormatada(Sdata);
		    }
		}
		e.setDataInstalacao((GregorianCalendar) wp.findField("dataInstalacao").getValue());
		if (wp.findField("dataRemocao") != null) {
		    if (wp.findField("dataRemocao").getValue() != null) {
			GregorianCalendar data = (GregorianCalendar) wp.findField("dataRemocao").getValue();
			e.setDataInstalacao(data);
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			fmt.setCalendar(data);
			String Sdata = fmt.format(data.getTime());
			e.setDataRemocaoFormatada(Sdata);
		    }
		}
		e.setDataRemocao((GregorianCalendar) wp.findField("dataRemocao").getValue());
		e.setDescricao(wp.findField("razaoSocial").getValue().toString() + " - " + wp.findField("contaSIGMA").getValue().toString() + "[" + wp.findField("particao").getValue().toString() + "]");
		if (wp.findField("idOrdem") != null) {
		    if (wp.findField("idOrdem").getValue() != null) {
			e.setIdOrdem(wp.findField("idOrdem").getValue().toString());
		    }
		}
	    }
	    lista.add(e);
	}

	return lista;
    }

    // Total contratado)
    public Integer contaTotalContratado() {
   	Connection conn = null;
   	PreparedStatement pstm = null;
   	ResultSet rs = null;
   	Integer total = 0;
   			
   	try
   	{   		 
   	       	conn = PersistEngine.getConnection("");
   	    	
   	       	StringBuffer  varname1 = new StringBuffer();
   	       	
   	       	varname1.append("select COUNT(1) as conta ");
   	       	varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
   	       	varname1.append(" where a.dataCancelamento is null");
   	    	   	    	    		    	
           	pstm = conn.prepareStatement(varname1.toString());        	
           	rs = pstm.executeQuery();
           	
           	while (rs.next())
       	    	{
           	    total = rs.getInt("conta");
       	    	}
           	
   	} catch(Exception e) {
   	    
   	}
   	return total;
       }

    // Total cancelado
    public Integer contaTotalCancelado() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" where a.dataCancelamento is not null");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;
	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total cancelado: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    // Total ativo
    public Integer contaTotalAtivo() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" where a.ativo = 1 ");
	    varname1.append("   and not exists ");
	    varname1.append("	 ( ");
	    varname1.append("		select 1 ");
	    varname1.append("		  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("		 where a.iccId = b.iccIdRemovido ");
	    varname1.append("	 )");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;
	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total ativo: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    // Total inativo
    public Integer contaTotalInativo() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" where a.ativo = 0 ");
	    varname1.append("   and not exists ");
	    varname1.append("	 ( ");
	    varname1.append("		select 1 ");
	    varname1.append("		  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("		 where a.iccId = b.iccIdRemovido ");
	    varname1.append("	 )");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;
	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total inativo: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    // Total Nexti
    public Integer contaTotalNexti() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" where a.FGCEscritorioRegional_neoId = 794740692 ");
	    varname1.append("   and a.dataCancelamento is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;
	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total nexti: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    // Total utilizado em Alarme(GPRS)
    public Integer contaTotalGprs() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from ");
	    varname1.append("( ");
	    varname1.append("	select ce.ID_CENTRAL, ");
	    varname1.append("	       ce.ID_EMPRESA, ");
	    varname1.append("	       ce.PARTICAO, ");
	    varname1.append("	       ce.RAZAO, ");
	    varname1.append("	       ce.cd_cliente ");
	    varname1.append("	  from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("	 where ce.ctrl_central = 1 ");
	    varname1.append("	   and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("	   and id_central not like 'DDD%' ");
	    varname1.append("      and id_central not like 'FFF%' ");
	    varname1.append("      and id_central not like 'AA%' ");
	    varname1.append("      and id_central not like 'R%' ");
	    varname1.append(" union ");
	    varname1.append("    select ID_CENTRAL, ");
	    varname1.append("		ID_EMPRESA, ");
	    varname1.append("		PARTICAO, ");
	    varname1.append("		RAZAO, ");
	    varname1.append("           cd_cliente ");
	    varname1.append("      from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("     where celular = 1 ");
	    varname1.append("       and ctrl_central = 1 "); 
	    varname1.append("       and ncelular = 'GPRS' ");
	    varname1.append("	    and id_central not like 'DDD%' ");
	    varname1.append("       and id_central not like 'FFF%' ");
	    varname1.append("       and id_central not like 'AA%' ");
	    varname1.append("       and id_central not like 'R%' ");
	    varname1.append(") as cliente_gprs");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total gprs: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    // Total utilizado em Conta Alarme(GPRS)
    public Integer contaTotalContaGprs() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from ( ");
	    varname1.append("    select ID_EMPRESA, ");
	    varname1.append("           ID_CENTRAL ");
	    varname1.append("    from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("   where ce.ctrl_central = 1 ");
	    varname1.append("     and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("     and ce.id_central not like 'DDD%' ");
	    varname1.append("           and ce.id_central not like 'FFF%' ");
	    varname1.append("           and ce.id_central not like 'AA%' ");
	    varname1.append("           and ce.id_central not like 'R%' ");
	    varname1.append("             ");
	    varname1.append(" union ");
	    varname1.append("  select ID_EMPRESA, ");
	    varname1.append("           ID_CENTRAL ");
	    varname1.append("    from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("   where celular = 1 ");
	    varname1.append("     and ctrl_central = 1 ");  
	    varname1.append("     and ncelular = 'GPRS' ");
	    varname1.append("     and id_central not like 'DDD%' ");
	    varname1.append("     and id_central not like 'FFF%' ");
	    varname1.append("     and id_central not like 'AA%' ");
	    varname1.append("     and id_central not like 'R%' ) as clientes");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total conta gprs: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }
    
    //contaSIGMAcomCHIP
    public Integer contaSIGMAcomCHIP() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer  varname1 = new StringBuffer();
	    varname1.append("SELECT COUNT(1) as conta");
	    varname1.append("     FROM ( ");
	    varname1.append("        select * ");
	    varname1.append("	      from ( ");
	    varname1.append("	        select ID_EMPRESA, ");
	    varname1.append("	               ID_CENTRAL ");
	    varname1.append("	        from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("	       where ce.ctrl_central = 1 ");
	    varname1.append("	         and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("	         and ce.id_central not like 'DDD%' ");
	    varname1.append("	               and ce.id_central not like 'FFF%' ");
	    varname1.append("	               and ce.id_central not like 'AA%' ");
	    varname1.append("	               and ce.id_central not like 'R%' ");
	    varname1.append("	                  ");
	    varname1.append("	     union ");
	    varname1.append("	      select ID_EMPRESA, ");
	    varname1.append("	               ID_CENTRAL ");
	    varname1.append("	        from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("	       where celular = 1 ");
	    varname1.append("	         and ctrl_central = 1 ");
	    varname1.append("	         and ncelular = 'GPRS' ");
	    varname1.append("	         and id_central not like 'DDD%' ");
	    varname1.append("	         and id_central not like 'FFF%' ");
	    varname1.append("	         and id_central not like 'AA%' ");
	    varname1.append("	         and id_central not like 'R%' ) as clientes) as tabela ");
	    varname1.append("	   inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b on tabela.ID_EMPRESA = b.empresaConta ");
	    varname1.append("	                             and tabela.ID_CENTRAL = b.contaSIGMA collate Latin1_General_CI_AS ");
	    varname1.append("	       where b.dataRemocao is null ");
	    varname1.append("	         and b.chp1_neoId is not null ");
	    varname1.append("	         and b.chp1_neoId <> 879480089");
	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar identificados: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }
    
    
    //contaSIGMAcomEthernet
    public Integer contaSIGMAcomEthernet() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer  varname1 = new StringBuffer();
	    varname1.append("SELECT COUNT(1) as conta");
	    varname1.append("     FROM ( ");
	    varname1.append("        select * ");
	    varname1.append("	      from ( ");
	    varname1.append("	        select ID_EMPRESA, ");
	    varname1.append("	               ID_CENTRAL ");
	    varname1.append("	        from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("	       where ce.ctrl_central = 1 ");
	    varname1.append("	         and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("	         and ce.id_central not like 'DDD%' ");
	    varname1.append("	               and ce.id_central not like 'FFF%' ");
	    varname1.append("	               and ce.id_central not like 'AA%' ");
	    varname1.append("	               and ce.id_central not like 'R%' ");
	    varname1.append("	                  ");
	    varname1.append("	     union ");
	    varname1.append("	      select ID_EMPRESA, ");
	    varname1.append("	               ID_CENTRAL ");
	    varname1.append("	        from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("	       where celular = 1 ");
	    varname1.append("	         and ctrl_central = 1 ");
	    varname1.append("	         and ncelular = 'GPRS' ");
	    varname1.append("	         and id_central not like 'DDD%' ");
	    varname1.append("	         and id_central not like 'FFF%' ");
	    varname1.append("	         and id_central not like 'AA%' ");
	    varname1.append("	         and id_central not like 'R%' ) as clientes) as tabela ");
	    varname1.append("	   inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b on tabela.ID_EMPRESA = b.empresaConta ");
	    varname1.append("	                             and tabela.ID_CENTRAL = b.contaSIGMA collate Latin1_General_CI_AS ");
	    varname1.append("	       where b.dataRemocao is null ");
	    varname1.append("	         and b.chp1_neoId is not null ");
	    varname1.append("	         and b.chp1_neoId = 879480089");
	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar identificados: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }
    
    public Integer contaSIGMAsemCHIP() {
	return contaTotalContaGprs() - contaSIGMAcomCHIP() - contaSIGMAcomEthernet();
    }

    public Integer contaIdentificados() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer  varname1 = new StringBuffer();
	    varname1.append("select SUM(tabela.conta) as conta2 ");
	    varname1.append("    from ( ");
	    varname1.append("           select COUNT(1) as conta ");
	    varname1.append("             from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append("            where a.dataCancelamento is null ");
	    varname1.append("              and a.FGCEscritorioRegional_neoId is null ");
	    varname1.append("     union ");
	    varname1.append("           select COUNT(1) as conta ");
	    varname1.append("	         from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("	    left join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a on a.neoId = b.chp1_neoId ");
	    varname1.append("	        where b.dataRemocao is null ");
	    varname1.append("	          and a.dataCancelamento is null ");
	    varname1.append("	          and b.chp1_neoId is not null ");
	    varname1.append("	          and b.chp1_neoId <> 879480089 ");
	    varname1.append("	          and a.FGCEscritorioRegional_neoId not in (870182283,794740692,870183722,931953043,917604768) ) as tabela");
	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta2");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar identificados: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }
    
    public Integer contaAtiva() {
	Connection conn2 = null;
	PreparedStatement pst2 = null;
	ResultSet rs2 = null;
	Integer total = 0;

	try {
	    conn2 = PersistEngine.getConnection("FUSIONPROD");

	    StringBuffer  varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("  where b.dataRemocao is null ");
	    varname1.append("    and b.chp1_neoId is not null ");
	    varname1.append("    and b.chp1_neoId <> 879480089 ");
	    varname1.append("    and exists ( ");
	    varname1.append("    select 1 ");
	    varname1.append("	      from ( ");
	    varname1.append("	        select ID_EMPRESA, ");
	    varname1.append("	               ID_CENTRAL ");
	    varname1.append("	        from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("	       where ce.ctrl_central = 1 ");
	    varname1.append("	         and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("	         and ce.id_central not like 'DDD%' ");
	    varname1.append("	               and ce.id_central not like 'FFF%' ");
	    varname1.append("	               and ce.id_central not like 'AA%' ");
	    varname1.append("	               and ce.id_central not like 'R%' ");
	    varname1.append("	                  ");
	    varname1.append("	     union ");
	    varname1.append("	      select ID_EMPRESA, ");
	    varname1.append("	               ID_CENTRAL ");
	    varname1.append("	        from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("	       where celular = 1 ");
	    varname1.append("	         and ctrl_central = 1 ");
	    varname1.append("	         and ncelular = 'GPRS' ");
	    varname1.append("	         and id_central not like 'DDD%' ");
	    varname1.append("	         and id_central not like 'FFF%' ");
	    varname1.append("	         and id_central not like 'AA%' ");
	    varname1.append("	         and id_central not like 'R%' ) as clientes ");
	    varname1.append("	         where clientes.ID_EMPRESA = b.empresaConta ");
	    varname1.append("	           and clientes.ID_CENTRAL = b.contaSIGMA collate Latin1_General_CI_AS ");
	    varname1.append("    )");
	    pst2 = conn2.prepareStatement(varname1.toString());
	    rs2 = pst2.executeQuery();

	    while (rs2.next()) {
		total = rs2.getInt("conta");
		System.out.println(total);
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst2);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar identificados: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn2, pst2, rs2);
	}
    }
    
    public Integer contaInativa() {
	return contaIdentificados() - contaAtiva();
    }

    public Integer contaAguardandoIdentificacao() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("	  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append("	 where a.FGCEscritorioRegional_neoId is not null ");
	    varname1.append("	   and a.FGCEscritorioRegional_neoId <> 794740692 ");
	    varname1.append("	   and a.FGCEscritorioRegional_neoId <> 870183722 ");
	    varname1.append("	   and not exists(select 1 ");
	    varname1.append("			    from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("			   where a.neoId = b.chp1_neoId ");
	    varname1.append("			     and b.dataRemocao is null ");
	    varname1.append("			     and b.visitaValidada = 1)");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar aguardando identificados: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaNaoIdentificado() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(*) as conta ");
	    varname1.append("	  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append("	 where a.ativo = 1 ");
	    varname1.append("	   and a.FGCEscritorioRegional_neoId is null ");
	    varname1.append("	   and not exists(select 1 ");
	    varname1.append("	                    from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("	                   where a.neoId = b.chp1_neoId ");
	    varname1.append("	                     and b.dataRemocao is null ");
	    varname1.append("	                     and b.visitaValidada = 1)");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar não identificados: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalRastreamento() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    /*
	     * varname1.append("select COUNT(1) as conta "); varname1.append(
	     * "  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	     * varname1.append(
	     * " inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b "
	     * ); varname1.append("         on a.neoId = b.chp1_neoId ");
	     * varname1
	     * .append(" inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral c "
	     * ); varname1.append("         on b.cdCliente = c.CD_CLIENTE ");
	     * varname1.append(" where a.ativo = 1 ");
	     * varname1.append("   and substring(c.ID_CENTRAL,1,1) like 'R'");
	     */

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" where a.FGCEscritorioRegional_neoId = 870183722 ");
	    varname1.append("   and a.dataCancelamento is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total rastreamento: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalDeggy() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" where a.FGCEscritorioRegional_neoId = 917604768 ");
	    varname1.append("   and a.dataCancelamento is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total deggy: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalAC() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" where a.FGCEscritorioRegional_neoId = 870183722 ");
	    varname1.append("   and a.plataforma = 'AUTOCARGO' ");
	    varname1.append("   and a.dataCancelamento is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total AC: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalFT() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append(" where a.FGCEscritorioRegional_neoId = 870183722 ");
	    varname1.append("   and a.plataforma = 'FULLTRACK' ");
	    varname1.append("   and a.dataCancelamento is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total FT: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaValidacao() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("   	  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente a ");
	    varname1.append("   	 where idOrdem is not null");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar validação: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaValidacaoPositiva() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("   	  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente a ");
	    varname1.append("   	 where idOrdem is not null");
	    varname1.append("   	   and a.visitaValidada = 1 ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar validação positiva: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaValidacaoNegativa() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("   	  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente a ");
	    varname1.append("   	 where idOrdem is not null");
	    varname1.append("   	   and a.visitaValidada = 0 ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar validação negativa: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaValidacaoAguardando() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("   	  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente a ");
	    varname1.append("   	 where idOrdem is not null");
	    varname1.append("   	   and a.visitaValidada is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar aguardando validação: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaOSComplementar() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("   	  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1RegistroOS    ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar OS complementar: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalTI() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1)  as conta ");
	    varname1.append("   from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append("  where a.FGCEscritorioRegional_neoId = 870182283 ");
	    varname1.append("    and not exists ( ");
	    varname1.append("		select 1 ");
	    varname1.append("		  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("		 where a.neoId = b.chp1_neoId ");
	    varname1.append("		   and b.dataRemocao is null ");
	    varname1.append("    )");
	    varname1.append("    and a.dataCancelamento is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total TI: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalTIaCancelar() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1)  as conta ");
	    varname1.append("   from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append("  where a.FGCEscritorioRegional_neoId = 931953043 ");
	    //varname1.append("    and not exists ( ");
	    //varname1.append("		select 1 ");
	    //varname1.append("		  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    //varname1.append("		 where a.neoId = b.chp1_neoId ");
	    //varname1.append("		   and b.dataRemocao is null ");
	    //varname1.append("    )");
	    varname1.append("    and a.dataCancelamento is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total TI a cancelar: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalRegionais() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("   from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 a ");
	    varname1.append("  where a.FGCEscritorioRegional_neoId not in (870182283,794740692,870183722,931953043,917604768) ");
	    varname1.append("    and not exists ( ");
	    varname1.append("		select 1 ");
	    varname1.append("		  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("		 where a.neoId = b.chp1_neoId ");
	    varname1.append("		   and b.dataRemocao is null ");
	    varname1.append("    )");
	    varname1.append("    and a.dataCancelamento is null ");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total regionais: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaNaoIdentificados() {

	return contaTotalContaGprs() - contaIdentificados();
    }

    public Integer totalIdentificado() {

	return contaTotalNexti() + contaTotalRastreamento() + contaIdentificados() + contaTotalTI() + contaTotalRegionais() + contaTotalDeggy() + contaTotalTIaCancelar();
    }

    public Integer totalNaoIdentificado() {
	Integer totalContratado = contaTotalContratado();
	Integer totalIdentificado = totalIdentificado();
	Integer total = totalContratado - totalIdentificado;
	return total;
    }

    public Integer contaTotalEncaronada() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta");
	    varname1.append("  from [FSOODB03\\SQL01].SIGMA90.dbo.dbORDEM o ");
	    varname1.append(" where o.DEFEITO like '%FOTOCHIP%'");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total encaronada: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalVisuVTR() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("   from [FSOODB03\\SQL01].SIGMA90.dbo.dbORDEM o ");
	    varname1.append("  where o.DEFEITO like '%FOTOCHIP%' ");
	    varname1.append("    and exists ( ");
	    varname1.append("		select 1 ");
	    varname1.append("		  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente c ");
	    varname1.append("		 inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 b on b.neoId = c.chp1_neoId ");
	    varname1.append("		 where o.ID_ORDEM = c.idOrdem ");
	    varname1.append("		   and c.visitaValidada = 1 ");
	    varname1.append("		   and c.dataRemocao is null ");
	    varname1.append("		   and b.FGCEscritorioRegional_neoId not in (794740692,870183722) ");
	    varname1.append("   )");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total visu VTR: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public Integer contaTotalVisuVTRImagem() {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Integer total = 0;

	try {
	    conn = PersistEngine.getConnection("");

	    StringBuffer varname1 = new StringBuffer();
	    varname1.append("select COUNT(1) as conta ");
	    varname1.append("		  from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente c ");
	    varname1.append("		 inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1 b on b.neoId = c.chp1_neoId ");
	    varname1.append("		 where c.visitaValidada is null ");
	    varname1.append("		   and c.viatura is null ");
	    varname1.append("		   and c.dataRemocao is null ");
	    varname1.append("		   and (b.FGCEscritorioRegional_neoId not in (794740692,870183722) ");
	    varname1.append("		         or b.FGCEscritorioRegional_neoId is null)");

	    pst = conn.prepareStatement(varname1.toString());
	    rs = pst.executeQuery();

	    while (rs.next()) {
		total = rs.getInt("conta");
	    }

	    return total;

	} catch (Exception e) {
	    System.err.println("sql:" + pst);
	    e.printStackTrace();

	    throw new WorkflowException("Erro ao consultar total visu VTR Imagem: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }
}
