package com.neomind.fusion.custom.orsegups.contract.converter;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;

public class PlacasConverter extends StringConverter 
{
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		String html = super.getHTMLInput(field, origin);
		
		String botaoHtml = "<br><input type='input' class='input_button' name='btn_validar' value='Validar' onclick='validarPlacas()'>";
		botaoHtml += "<script>"
				+ ""
				+ ""
				+ ""
				+ ""
				+ "function validarPlacas(){"
				+ "    var regex = /[^,\\w]*/g;"
				+ "    var placasStr = $('#var_placaVeiculos__').val();"
				+ "    placasStr = placasStr.replace(regex, '');"
				+ "    $('#var_placaVeiculos__').val(placasStr);"
				+ ""
				+ "    var placasArr = placasStr.split(',');"
				+ "    for (var x=0; x<placasArr.length; x++){"
				+ "        var placa = placasArr[x];"
				+ "        if (placa.length < 7 || placa.length > 7){"
				+ "            alert('Existem placas inválidas. Por favor, revise-as e valide novamente.');"
				+ "            return;"
				+ "        }"
				+ "    }"
				+ ""
				+ "    alert('Total de Placas: ' + placasArr.length);"
				+ "}"
				+ "</script>";		
		
		return html + botaoHtml;
	}
	
	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		// TODO Auto-generated method stub
		return super.getHTMLView(field, origin);
	}
}
