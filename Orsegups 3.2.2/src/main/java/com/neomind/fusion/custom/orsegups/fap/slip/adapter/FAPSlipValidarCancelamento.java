package com.neomind.fusion.custom.orsegups.fap.slip.adapter;

import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEvent;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEventListener;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPSlipValidarCancelamento implements WorkflowValidateCancelEventListener
{

	@Override
	public void validateCancel(WorkflowValidateCancelEvent paramWorkflowValidateCancelEvent)
			throws WorkflowException
	{
		NeoUser nuSolicitanteProcesso = paramWorkflowValidateCancelEvent.getProcess().getRequester();
		
		if (PortalUtil.getCurrentUser() != null && !nuSolicitanteProcesso.equals(PortalUtil.getCurrentUser()) && !PortalUtil.getCurrentUser().isAdm())
		{
			throw new WorkflowException("Você não tem permissão para cancelar esse processo.");
		}
	}

}
