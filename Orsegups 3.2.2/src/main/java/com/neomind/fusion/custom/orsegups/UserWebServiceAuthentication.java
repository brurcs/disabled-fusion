package com.neomind.fusion.custom.orsegups;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class UserWebServiceAuthentication
{
	public Boolean authentication(String key)
	{
		Boolean valid = false;
		QLEqualsFilter filtroKey = new QLEqualsFilter("key", key);
		NeoObject user = PersistEngine.getObject(AdapterUtils.getEntityClass("UsuarioWebService"), filtroKey);
		
		if (user != null)
		{
			valid = true;
		}
		return valid;
	}
}
