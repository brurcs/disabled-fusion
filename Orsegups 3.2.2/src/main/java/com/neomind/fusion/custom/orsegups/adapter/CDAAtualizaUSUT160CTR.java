package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class CDAAtualizaUSUT160CTR implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String nomeFonteDados = "SAPIENS";
		
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
	
		// valores utilizados na condicao where
		Long empresa = (Long) processEntity.findValue("contrato.usu_codemp");
		Long filial = (Long) processEntity.findValue("contrato.usu_codfil");
		Long contrato = (Long) processEntity.findValue("contrato.usu_numctr");

		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE USU_T160CTR ");
		sql.append("SET USU_CODFPG = 14, ");
		sql.append("USU_DIABAS = 20 ");
		sql.append("WHERE USU_CODEMP = ? ");
		sql.append("AND USU_CODFIL = ? ");
		sql.append("AND USU_NUMCTR = ? ");
		
		try
		{
			PreparedStatement st = connection.prepareStatement(sql.toString());
			//WHERE
			st.setLong(1, empresa);
			st.setLong(2, filial);
			st.setLong(3, contrato);

			st.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}

