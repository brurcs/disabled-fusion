package com.neomind.fusion.custom.orsegups.integraRubi.adapter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskTransferEvent;
import com.neomind.fusion.workflow.event.TaskTransferEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;

public class RHintegracaoRubi implements AdapterInterface, ActivityStartEventListener, TaskAssignerEventListener, TaskTransferEventListener
{
	private static final Log log = LogFactory.getLog(RHintegracaoRubi.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity) //throws RemoteException, ServiceException
	{
		NeoUser executor = origin.getActivity().getInstance().getOwner();
		String idTarefa = activity.getProcess().getCode();
		String codeUsu = origin.getActivity().getInstance().getOwner().getCode();
		String chaveCodigo = activity.getProcess().getCode();

		log.info("Início Integração - Tarefa: " + idTarefa);

		wrapper.setValue("errosIntegracao", "");
		wrapper.setValue("contErros", 0l);

		//String validade = RHIntegracaoAdmissaoUtils.validarNegocio(processEntity);

		/*
		 * if (validade != null)
		 * {
		 * throw new WorkflowException(validade);flor
		 * }
		 */

		String erro = integrar(wrapper, codeUsu, chaveCodigo);
		if (erro == null || erro.trim().length() == 0)
		{
			wrapper.setValue("validaIntegracao", true);
		}
		else
		{
			wrapper.setValue("errosIntegracao", erro);
			wrapper.setValue("contErros", 1l);
			wrapper.setValue("exec2", executor);
			wrapper.setValue("validaIntegracao", false);
		}

		log.info("Fim Integração - Tarefa: " + idTarefa);
	}

	private String integrar(EntityWrapper wrapper, String codUsuario, String chaveCodigo)
	{
		Long nrEmpresa = wrapper.findGenericValue("emp.numemp"); //Número Empresa
		Long tipoColaborador = wrapper.findGenericValue("tpColaborador.codigo"); //Tipo de Colaborador
		wrapper.setValue("apefun", wrapper.findGenericValue("colaborador"));

		//CADASTRO DE DADOS BÁSICOS
		String erroCadastroBasico = RHIntegracaoAdmissaoUtils.efetivaCadastroBase(wrapper, codUsuario, chaveCodigo, nrEmpresa);

		if (RHIntegracaoAdmissaoUtils.ocorreuErro(erroCadastroBasico))
			return erroCadastroBasico;

		Long matricula = RHIntegracaoBanco.resgataNumCad(RHIntegracaoAdmissaoUtils.getApFun(chaveCodigo), nrEmpresa);

		//CADASTRO COMPLEMENTAR
		String erroCadastroComplementar = RHIntegracaoAdmissaoUtils.efetivarCadastroComplementar(wrapper, chaveCodigo, matricula);

		if (RHIntegracaoAdmissaoUtils.ocorreuErro(erroCadastroComplementar))
			return erroCadastroComplementar;

		//CADASTRO ADICIONAL
		String erroCadastroAdicional = RHIntegracaoAdmissaoUtils.efetivarIntegracaoAdicional(wrapper, chaveCodigo, matricula);
		log.info("Retorno cadastro adicional: " + erroCadastroComplementar);

		if (RHIntegracaoAdmissaoUtils.ocorreuErro(erroCadastroAdicional))
			return erroCadastroAdicional;

		//CADASTRO DE DEPENDENTES
		String errocadastrarDependentes = RHIntegracaoAdmissaoUtils.efetivarCadastroDependentes(wrapper, chaveCodigo, nrEmpresa, tipoColaborador, matricula);
		log.info("Retorno cadastro dependentes: " + erroCadastroComplementar);

		if (RHIntegracaoAdmissaoUtils.ocorreuErro(errocadastrarDependentes))
			return errocadastrarDependentes;

		//CADASTRO DE USUARIOS DA SEDE
		if (!RHIntegracaoAdmissaoUtils.isSede(wrapper))
		{
			String StcodFic = wrapper.findGenericValue("documentacao.fichamedica");
			if (StcodFic == null)
				return "ERRO NA INTEGRAÇÃO - Ficha médica não cadastrada";

			String errocadastroASO = RHIntegracaoAdmissaoUtils.cadastrarASO(wrapper, chaveCodigo, nrEmpresa);

			if (RHIntegracaoAdmissaoUtils.ocorreuErro(errocadastroASO))
				return errocadastroASO;

			String erroexame = RHIntegracaoAdmissaoUtils.cadastraExame(wrapper, chaveCodigo, nrEmpresa);

			if (RHIntegracaoAdmissaoUtils.ocorreuErro(erroexame))
				return erroexame;
		}

		return null;

	}

	@Override
	public void onTransfer(TaskTransferEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onAssign(TaskAssignerEvent arg0) throws TaskException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ActivityEvent arg0) throws ActivityException
	{
		// TODO Auto-generated method stub

	}

}