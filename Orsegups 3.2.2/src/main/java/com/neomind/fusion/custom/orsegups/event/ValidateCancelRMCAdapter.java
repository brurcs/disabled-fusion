package com.neomind.fusion.custom.orsegups.event;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEvent;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEventListener;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class ValidateCancelRMCAdapter implements WorkflowValidateCancelEventListener
{

	@Override
	public void validateCancel(WorkflowValidateCancelEvent event) throws WorkflowException
	{
		
		EntityWrapper wrapper = event.getWrapper();
		WFProcess process = event.getProcess();
		/*
		// Busca o valor do campo aprovado do eform da solicitação
		Boolean aprovado = (Boolean) wrapper.findValue("aprovado");
		
		//Caso esteja aprovado não permite que a solicitaçào seja cancelada
		if(aprovado != null && aprovado == true){
			throw new WorkflowException("Solicitação número "+process.getCode()+" não pode ser cancelada pois já foi aprovada.");
		}
		*/
		throw new WorkflowException("Solicitação número "+process.getCode()+" não pode ser cancelada pois já foi aprovada.");
	}
}
