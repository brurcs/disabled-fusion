package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesAtualizaGestoresRegionais implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(AbreTarefaSimplesAtualizaGestoresRegionais.class);

	@Override
	public void execute(CustomJobContext ctx)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		Connection conn = PersistEngine.getConnection("SAPIENS");
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		
		String solicitante = "";
		String papelSol = "SolicitanteAtualizarGestoresRegionais";
		NeoPaper papelSolicitante = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",papelSol));
		if (papelSolicitante != null && papelSolicitante.getAllUsers() != null && !papelSolicitante.getAllUsers().isEmpty())
		{
			for (NeoUser user : papelSolicitante.getUsers())
			{
				if (user.isActive())
				{	
					solicitante = user.getCode();
					break;
				}
			}
		}
		
		String executor = "";
		String papelExec = "Responsavel Atualizar Gestores Regionais";
		NeoPaper papelExecutor = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",papelExec));
		if (papelExecutor != null && papelExecutor.getAllUsers() != null && !papelExecutor.getAllUsers().isEmpty())
		{
			for (NeoUser user : papelExecutor.getUsers())
			{
				if (user.isActive())
				{	
					executor = user.getCode();
					break;
				}
			}
		}
		
		String titulo = "Atualizar Gestores das Regionais";
		String descricao = "";
		String tarefa = "";
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Atualiza Gestores Regionais - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		try
		{			
			sql.append(" select  reg.usu_codreg, reg.usu_nomreg, gerreg.nomcom as gerenteregional, corhum.nomcom as coordenadorhumana, gerele.nomcom as gerenteeletronica, corele.nomcom as coordenadoreletronica ");
			sql.append(" from usu_t200reg reg ");
			sql.append(" inner join r910usu gerreg on gerreg.codent = reg.usu_gerreg ");
			sql.append(" inner join r910usu gerele on gerele.codent = reg.usu_gerrel ");
			sql.append(" inner join r910usu corele on corele.codent = reg.usu_coereg ");
			sql.append(" inner join r910usu corhum on corhum.codent = reg.usu_cohreg ");
			sql.append(" order by reg.usu_codreg ");	
			
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			
			descricao = descricao + "<table class=\"gridbox\" cellpadding=\"0\" cellspacing=\"0\" style=\"empty-cells: show\">";
			descricao = descricao + "<thead>";
			descricao = descricao + "<tr><th colspan=8>Gestores das Regionais</th></tr>";
			descricao = descricao + "<tr>";
			descricao = descricao + "<th>Regional</th>";
			descricao = descricao + "<th>Descrição</th>";
			descricao = descricao + "<th>Gerente Regional</th>";
			descricao = descricao + "<th>Coordenador Humana</th>";
			descricao = descricao + "<th>Gerente Eletrônica</th>";
			descricao = descricao + "<th>Coordenador Eletrônica</th>";
			descricao = descricao + "</tr>";
			descricao = descricao + "</thead>";
			descricao = descricao + "<tbody>";

			while (rs.next())
			{
				descricao = descricao + "<tr>";
				descricao = descricao + "<td>" + rs.getString("usu_codreg") + "</td>";
				descricao = descricao + "<td>" + rs.getString("usu_nomreg") + "</td>";
				descricao = descricao + "<td>" + rs.getString("gerenteregional") + "</td>";
				descricao = descricao + "<td>" + rs.getString("coordenadorhumana") + "</td>";
				descricao = descricao + "<td>" + rs.getString("gerenteeletronica") + "</td>";
				descricao = descricao + "<td>" + rs.getString("coordenadoreletronica") + "</td>";
				descricao = descricao + "</tr>";
			}
			descricao = descricao + "</tbody>";
			descricao = descricao + "</table>";
			
			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Atualiza Gestores Regionais");
			System.out.println("["+key+"] ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Atualiza Gestores Regionais");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Atualiza Gestores Regionais - Tarefa: "+tarefa+" Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}				
	}
}
