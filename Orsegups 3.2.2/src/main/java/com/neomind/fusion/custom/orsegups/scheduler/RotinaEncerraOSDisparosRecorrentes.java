package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class RotinaEncerraOSDisparosRecorrentes implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {
		
		
 	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append(" SELECT ID_ORDEM FROM dbORDEM O WITH(NOLOCK) ");
	sql.append(" INNER JOIN HISTORICO_ARME A WITH(NOLOCK) ON O.CD_CLIENTE = A.CD_CLIENTE  ");
	sql.append(" WHERE O.IDOSDEFEITO=1020 "); // 1020 OS-DISPAROS RECORRENTES
	sql.append(" AND A.CD_HISTORICO_ARME = (SELECT TOP 1 CD_HISTORICO_ARME FROM HISTORICO_ARME A2 WITH(NOLOCK) WHERE A2.DT_RECEBIDO >= O.ABERTURA) ");
	sql.append(" AND NOT EXISTS  ");
	sql.append(" ( ");
	sql.append(" SELECT 1 FROM VIEW_HISTORICO H WITH(NOLOCK) WHERE H.CD_CLIENTE = A.CD_CLIENTE AND H.DT_RECEBIDO BETWEEN A.DT_RECEBIDO AND DATEADD(DAY,1,A.DT_RECEBIDO) ");
	sql.append(" AND H.CD_EVENTO IN ('E120','E121','E122','E123','E124','E125','E130','E131','E132','E133','E134','E135','E136') ");
	sql.append(" ) ");
	sql.append(" ORDER BY ID_ORDEM DESC ");

	List<Integer> resultados = new ArrayList<Integer>();
	
	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		
		resultados.add(rs.getInt(1));
		
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	
	for (Integer r : resultados){
	    this.encerrarOrdemServico(r);	    
	}
	
    }
    
    private void encerrarOrdemServico(int idOrdem){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	sql.append(" UPDATE dbORDEM SET FECHADO = 1, OPFECHOU = 11010, FECHAMENTO = GETDATE(), IDOSCAUSADEFEITO = 1, IDOSSOLUCAO = 249, EXECUTADO = ? WHERE ID_ORDEM = ? ");
	
	String executado = "Ordem de serviço encerrada devido a ausência de disparos nas 24 horas seguintes à ativação do sistema. ";
	
	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setString(1, executado);
	    pstm.setInt(2, idOrdem);

	    pstm.executeUpdate();
	    
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	

    }

}
