package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.ArrayList;
import java.util.Collection;

public class ClienteVO
{
  private String codigoCliente;
  private String nomeCliente;
  private String tipoPessoa;
  private String cpfCnpj;
  private String telefone;
  private String isCliente;
  private String isPresencaLiberado;
  private Collection<PostoVO> postos = new ArrayList();

  public Collection<PostoVO> getPostos()
  {
    return this.postos;
  }

  public void setPostos(Collection<PostoVO> postos)
  {
    this.postos = postos;
  }

  public String getCodigoCliente()
  {
    return this.codigoCliente;
  }

  public void setCodigoCliente(String codigoCliente)
  {
    this.codigoCliente = codigoCliente;
  }

  public String getNomeCliente()
  {
    return this.nomeCliente;
  }

  public void setNomeCliente(String nomeCliente)
  {
    this.nomeCliente = nomeCliente;
  }

public String getTipoPessoa()
{
	return tipoPessoa;
}

public void setTipoPessoa(String tipoPessoa)
{
	this.tipoPessoa = tipoPessoa;
}

public String getCpfCnpj()
{
	return cpfCnpj;
}

public void setCpfCnpj(String cpfCnpj)
{
	this.cpfCnpj = cpfCnpj;
}

public String getTelefone()
{
	return telefone;
}

public void setTelefone(String telefone)
{
	this.telefone = telefone;
}

public String getIsCliente()
{
	return isCliente;
}

public void setIsCliente(String isCliente)
{
	this.isCliente = isCliente;
}

public String getIsPresencaLiberado()
{
	return isPresencaLiberado;
}

public void setIsPresencaLiberado(String isPresencaLiberado)
{
	this.isPresencaLiberado = isPresencaLiberado;
}



}