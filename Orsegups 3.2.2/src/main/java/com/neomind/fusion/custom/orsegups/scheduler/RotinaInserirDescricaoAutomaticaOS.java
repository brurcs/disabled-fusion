package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaInserirDescricaoAutomaticaOS implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaInserirDescricaoAutomaticaOS.class);
    private static final int[] DEFEITOS_REG = {21, 122, 134, 140, 166, 167, 176, 178, 184, 737, 926, 1019, 1067, 1068, 1072, 1073, 1145, 1146, 1149, 1162, 1166, 1187, 1192};
    
    @Override
    public void execute(CustomJobContext arg0) {

	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	try {

	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuffer sqlOS = new StringBuffer();
	    sqlOS.append(" SELECT ID_ORDEM, IDOSDEFEITO, DEFEITO, SUBSTRING(r.NM_ROTA, 1, 3) ROTA FROM dbORDEM os WITH(NOLOCK) ");
	    sqlOS.append(" INNER JOIN dbCENTRAL c WITH(NOLOCK) on c.CD_CLIENTE = os.CD_CLIENTE ");
	    sqlOS.append(" LEFT JOIN ROTA r WITH(NOLOCK) ON c.ID_ROTA = r.CD_ROTA ");
	    sqlOS.append(" where os.ABERTURA between DATEADD(mi,-10,GETDATE()) and GETDATE() ");
	    sqlOS.append(" and (c.PARTICAO in ('000','001','002','003','004','005','006','007','008','009','010') ");
	    sqlOS.append(" AND c.ID_EMPRESA NOT IN (10075, 10119, 10120,10127,10144) ");
	    sqlOS.append(" AND c.STSERVIDORCFTV = 0 ");
	    sqlOS.append(" OR (os.IDOSDEFEITO IN (21,122,134,140,166,167,176,178,184,737,926,1019,1067,1068,1072,1073,1145,1146,1149,1162,1166,1187,1192) ");
	    sqlOS.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ('TRI','XLN') AND os.FECHADO = 0)) ");
	    
	    st = conn.prepareStatement(sqlOS.toString());
	    rs = st.executeQuery();

	    while (rs.next()) {
		Long idOrdem = new Long(rs.getLong("ID_ORDEM"));
		String defeito = rs.getString("DEFEITO");
		Long idDefeito = rs.getLong("IDOSDEFEITO");
		String rota = rs.getString("ROTA");
		
		if (eDefeitoValidoParaDescricaoQuedaEnergia(idDefeito) && (rota.equals("XLN") || rota.equals("TRI"))) {
			defeito += "\n Configurar reporte de queda de energia para 15 minutos.";
		}
		
		defeito += "\n ATENÇÃO:_ Verificar se o cliente possui placas e se todas estão em perfeitas condições, enviar FOTO no anexo. Conferir se a(s) central(is) deste cliente estão com a programação correta de envio de RESTAURO no ato do fechamento da zona. Dúvidas entrar em contato com a Assessoria Técnica."; 
		
		/*
		 * 134 - OS-HABILITAR MONITORAMENTO
		 * 184 - OS-REALIZAR INSTALACAO
		 * 1068 - OS-ALARME-HABILIT MONITORAM
		 */
		if(idDefeito == 134L || idDefeito == 184L || idDefeito == 1068L){
			defeito += "\n Sr. Técnico, orientar o cliente a baixar o aplicativo REMOTO ORSEGUPS para uso na habilitação do sistema de alarme ou para visualização das câmeras. Caso a central de alarme não seja Intelbras modelo 4010 ou 2018, desconsiderar essa orientação.";
		}
		
		Connection connUpd = null;
		PreparedStatement pstmUpd = null;

		StringBuilder sql = new StringBuilder();

		try {
		    connUpd = PersistEngine.getConnection("SIGMA90");
		    
		    sql.append("UPDATE dbORDEM SET DEFEITO = ? WHERE ID_ORDEM = ?");
		    
		    pstmUpd = connUpd.prepareStatement(sql.toString());
		    System.out.println("****** PRINT DEFEITO PARA ANALISE [RotinaInserirDescricaoAutomaticaOS] *****");
		    System.out.println(defeito);
		    pstmUpd.setString(1, defeito.replaceAll("\\s\\s+", " "));
		    pstmUpd.setLong(2, idOrdem);
		    
		    pstmUpd.execute();

		} catch (SQLException e) {
		    e.printStackTrace();
		} finally {
		    OrsegupsUtils.closeConnection(connUpd, pstmUpd, null);
		}
		
	    }
	} catch (Exception e) {

	    log.error("##### ERRO AO EXECUTAR RotinaInserirDescricaoAutomaticaOS - URL " + e.getMessage().toString() + " \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    System.out.println("[" + key + "] ##### ERRO AO EXECUTAR RotinaInserirDescricaoAutomaticaOS - URL " + e.getMessage().toString() + " \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, st, rs);
		log.warn("##### FINALIZAR RotinaInserirDescricaoAutomaticaOS - URL  Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

	    } catch (Exception e) {

		log.error("##### ERRO AO RotinaInserirDescricaoAutomaticaOS - URL " + e.getMessage().toString() + " \n- Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		return;
	    }
	}

    }

	private boolean eDefeitoValidoParaDescricaoQuedaEnergia (Long idDefeito) {
		
		for (int defeito : DEFEITOS_REG) {
			if (idDefeito == defeito) {
				return true;
			}
		}
		
		return false;
	}

}
