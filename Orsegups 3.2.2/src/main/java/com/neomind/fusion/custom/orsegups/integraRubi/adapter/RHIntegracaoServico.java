package com.neomind.fusion.custom.orsegups.integraRubi.adapter;

import java.rmi.RemoteException;
import java.util.GregorianCalendar;

import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.util.NeoDateUtils;

import br.com.senior.services.sm.asoExterno.AsoexternoASOExternoIn;
import br.com.senior.services.sm.asoExterno.AsoexternoASOExternoOut;
import br.com.senior.services.sm.asoExterno.G5SeniorServicesLocatorAso;
import br.com.senior.services.sm.resultadosExames.G5SeniorServicesLocatorResultExam;
import br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados3In;
import br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados3Out;
import br.com.senior.services.vetorh.constants.WSConfigs;
import br.com.senior.services.vetorh.dependentes.DependentesDependente2In;
import br.com.senior.services.vetorh.dependentes.DependentesDependente2Out;
import br.com.senior.services.vetorh.dependentes.DependentesNascimentoIn;
import br.com.senior.services.vetorh.dependentes.DependentesNascimentoOut;
import br.com.senior.services.vetorh.dependentes.G5SeniorServicesLocatorDep;
import br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica5In;
import br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica5Out;
import br.com.senior.services.vetorh.ficha.basica.G5SeniorServicesLocatorFBas;
import br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar7In;
import br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar7Out;
import br.com.senior.services.vetorh.ficha.complementar.G5SeniorServicesLocatorFCom;
import br.com.senior.services.vetorh.historico.G5SeniorServicesLocatorFPHis;
import br.com.senior.services.vetorh.historico.HistoricosAdicional2In;
import br.com.senior.services.vetorh.historico.HistoricosAdicional2Out;

public class RHIntegracaoServico
{
	private static final Log log = LogFactory.getLog(RHIntegracaoServico.class);

	public static String integrarVetorInicio(EntityWrapper processEntity, String chaveCodigo)
	{
		WSConfigs config = new WSConfigs();
		
		//LOCATOR FBas
		G5SeniorServicesLocatorFBas glBas = new G5SeniorServicesLocatorFBas();
		FichaBasicaFichaBasica5Out integrarFBasOut = new FichaBasicaFichaBasica5Out();
		try
		{
			FichaBasicaFichaBasica5In integrarFBasIn = RHIntegracaoAdmissaoUtils.defineFichaBasica(chaveCodigo, processEntity);
			
			log.info("INTEGRACAO Ficha Basica :" + chaveCodigo + " - INICIO: ");
			integrarFBasOut = glBas.getrubi_Synccom_senior_g5_rh_fp_fichaBasicaPort().fichaBasica_5(config.getUserService(), config.getPasswordService(), 0, integrarFBasIn);
			log.info("INTEGRACAO Ficha Basica :" + chaveCodigo + " - FIM: ");
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Erro ao integrar Ficha Básica com Vetor : " + chaveCodigo;
		}

		return integrarFBasOut.getErroExecucao();
	}

	public static String integrarVetorFichaComplementar(EntityWrapper pEntity, String chaveCodigo, Long matricula)
	{
		String erroCom;
		//Criando Entradas e Saídas da Ficha Complementar
		FichaComplementarFichaComplementar7Out integrarFComOut = new FichaComplementarFichaComplementar7Out();

		FichaComplementarFichaComplementar7In integrarFComIn = RHIntegracaoAdmissaoUtils.integrarFichaComplementar(pEntity, matricula);
		//LOCATOR FCom
		G5SeniorServicesLocatorFCom glCom = new G5SeniorServicesLocatorFCom();

		//INTEGRAÇÃO FCom
		try
		{
			WSConfigs config = new WSConfigs();
			log.info("INTEGRACAO FichaComplementar :" + chaveCodigo + " - INICIO: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			integrarFComOut = glCom.getrubi_Synccom_senior_g5_rh_fp_fichaComplementarPort().fichaComplementar_7(new WSConfigs().getUserService(), config.getPasswordService(), 0, integrarFComIn);
			log.info("INTEGRACAO FichaComplementar :" + chaveCodigo + " - FIM: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			erroCom = integrarFComOut.getErroExecucao();
			
			log.info("Mensagem da ficha complementar da matrícula: " + matricula + " - " + erroCom);

			if (erroCom == null || erroCom.contains("O campo \"DENifExt1\" não está disponível na tela \"FR034CPL\""))
			{
				log.warn("FICHA COMPLEMENTAR GERADA COM SUCESSO!");
				log.info("FICHA COMPLEMENTAR GERADA COM SUCESSO!");

				RHIntegracaoAdmissaoUtils.adicionaRegistroIntegracao(RHIntegracaoAdmissaoUtils.getChaveFCom(chaveCodigo));
			}
		}
		catch (RemoteException | ServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Erro ao integrar ficha complementar" + e.getMessage();
		}

		return erroCom;
	}

	public static String integrarDependente(String chaveCodigo, Integer coddep, DependentesDependente2In integrarDepIn)
	{
		//Criando Entradas e Saídas dos Dependentes
		DependentesDependente2Out integrarDepOut = new DependentesDependente2Out();

		//LOCATOR Dep
		G5SeniorServicesLocatorDep glDep = new G5SeniorServicesLocatorDep();

		try
		{
			WSConfigs config = new WSConfigs();
			log.info("INTEGRACAO Dependentes :" + chaveCodigo + " - codDep:" + coddep + " - INICIO: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			integrarDepOut = glDep.getrubi_Synccom_senior_g5_rh_fp_dependentesPort().dependente_2(config.getUserService(), config.getPasswordService(), 0, integrarDepIn);
			log.info("INTEGRACAO Dependentes :" + chaveCodigo + " - codDep:" + coddep + " - FIM: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			return integrarDepOut.getErroExecucao();
		}
		catch (RemoteException | ServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			return "Ocorreu um erro ao integrar o dependente " + coddep;
		}
	}

	public static String integrarVetorFichaAdicional(String chaveCodigo, Long nrControle, EntityWrapper pEntity)
	{
		HistoricosAdicional2Out integrarAdiOut;
		//LOCATOR Adi
		G5SeniorServicesLocatorFPHis glAdi = new G5SeniorServicesLocatorFPHis();

		//INTEGRAÇÃO Adi
		try
		{
			HistoricosAdicional2In integrarAdiIn = RHIntegracaoAdmissaoUtils.getObjetoIntegracaoAdcional(pEntity, nrControle);
			WSConfigs config = new WSConfigs();
			log.info("INTEGRACAO Adicionais :" + chaveCodigo + " - INICIO: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			integrarAdiOut = glAdi.getrubi_Synccom_senior_g5_rh_fp_historicosPort().adicional_2(config.getUserService(), config.getPasswordService(), 0, integrarAdiIn);
			log.info("INTEGRACAO Adicionais :" + chaveCodigo + " - FIM: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));

			if (integrarAdiOut.getErroExecucao() != null && integrarAdiOut.getErroExecucao().contains("erro") && !integrarAdiOut.getErroExecucao().contains("Registro já existe"))
				return integrarAdiOut.getErroExecucao();
			else
				return null;
		}
		catch (RemoteException | ServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Erro ao integrar ficha adicional" + e.getMessage();
		}
	}

	public static String integraDependenteNascimento(String chaveCodigo, Integer coddep, DependentesNascimentoIn integrarDepNasIn)
	{
		String erroDepNas;
		DependentesNascimentoOut integrarDepNasOut = new DependentesNascimentoOut();
		//LOCATOR DepNas
		G5SeniorServicesLocatorDep glDepNas = new G5SeniorServicesLocatorDep();

		//Integrar Entrega da Certidão
		try
		{
			WSConfigs config = new WSConfigs();
			log.info("INTEGRACAO NascDependente :" + chaveCodigo + " - " + coddep + "/" + RHIntegracaoAdmissaoUtils.getchaveDNas(chaveCodigo) + " - INICIO: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			integrarDepNasOut = glDepNas.getrubi_Synccom_senior_g5_rh_fp_dependentesPort().nascimento(config.getUserService(), config.getPasswordService(), 0, integrarDepNasIn);

			log.info("INTEGRACAO NascDependente :" + chaveCodigo + " - " + coddep + "/" + RHIntegracaoAdmissaoUtils.getchaveDNas(chaveCodigo) + " - FIM: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
		}
		catch (RemoteException | ServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			return "Ocorreu um erro ao gravar o nascimento do dependente";
		}

		erroDepNas = integrarDepNasOut.getErroExecucao();
		return erroDepNas;
	}

	public static String integraASO(String chaveCodigo, AsoexternoASOExternoIn integrarASOIn)
	{
		AsoexternoASOExternoOut integrarASOOut = new AsoexternoASOExternoOut();

		// LOCATOR ASO
		G5SeniorServicesLocatorAso glASO = new G5SeniorServicesLocatorAso();

		// INTEGRAÇÃO ASO
		try
		{
			WSConfigs config = new WSConfigs();
			log.info("INTEGRACAO AsoExterna :" + chaveCodigo + " - INICIO: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			integrarASOOut = glASO.getsm_Synccom_senior_g5_rh_sm_asoexternoPort().ASOExterno(config.getUserService(), config.getPasswordService(), 0, integrarASOIn);
			log.info("INTEGRACAO AsoExterna :" + chaveCodigo + " - FIM: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss") + " - " + integrarASOOut.getErroExecucao());
			return integrarASOOut.getErroExecucao();
		}
		catch (RemoteException | ServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			return "Ocorreu um erro na integração ASO";
		}
	}

	public static String integraExames(String chaveCodigo, ResultadosExamesResultados3In integrarResultExamIn)
	{
		// LOCATOR RESULTADO DO EXAME
		G5SeniorServicesLocatorResultExam glResultExam = new G5SeniorServicesLocatorResultExam();

		// INTEGRAÇÃO RESULTADO DO EXAME
		try
		{
			WSConfigs config = new WSConfigs();
			log.info("INTEGRACAO ExameAso :" + chaveCodigo + " - INICIO: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			ResultadosExamesResultados3Out integrarResultExamOut = glResultExam.getsm_Synccom_senior_g5_rh_sm_ResultadosExamesPort().resultados_3(config.getUserService(), config.getPasswordService(), 0, integrarResultExamIn);
			log.info("INTEGRACAO ExameAso :" + chaveCodigo + " - FIM: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss") + " - " + integrarResultExamOut.getErroExecucao());

			return integrarResultExamOut.getErroExecucao();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Ocorreu um erro ao integrar exames " + e.getMessage();
		}

	}

}
