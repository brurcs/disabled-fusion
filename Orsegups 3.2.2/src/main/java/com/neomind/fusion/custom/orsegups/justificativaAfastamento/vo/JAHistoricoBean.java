package com.neomind.fusion.custom.orsegups.justificativaAfastamento.vo;

import java.util.Calendar;

/**
 * Classe pai de JAHistoricoAfastamentoBean e JAHistoricoMedicoBean
 * Utilizada no fluxo [JA] Fluxo de afastamento - J003
 * @author mateus.batista
 *
 */
public class JAHistoricoBean {
    
    private Calendar dataProcesso;
    private int statusProcesso;
    private String link;
    
    public Calendar getDataProcesso() {
        return dataProcesso;
    }
    public void setDataProcesso(Calendar dataProcesso) {
        this.dataProcesso = dataProcesso;
    }

    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    /**
     * 0 - Em Execução
     * 1 - Finalizado
     * 2 - Cancelado
     * @return
     */
    public int getStatusProcesso() {
        return statusProcesso;
    }
    public void setStatusProcesso(int statusProcesso) {
        this.statusProcesso = statusProcesso;
    }
    
    
    
}
