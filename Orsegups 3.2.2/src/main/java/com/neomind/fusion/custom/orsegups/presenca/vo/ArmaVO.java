package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ArmaVO {
	
	String neoId;
	String descricaoEspecie;
	String descricaoMarca;
	String modelo;
	String numeroArma;
	String numeroSinarm;
	String situacao;
	String usuNumctr;
	String usuNumpos;
	String dataValidade;
	String dataLotacao;
	String linkDetalhes;
	
	public String getNeoId() {
		return neoId;
	}
	public void setNeoId(String neoId) {
		this.neoId = neoId;
	}
	public String getDescricaoEspecie() {
		return descricaoEspecie;
	}
	public void setDescricaoEspecie(String descricaoEspecie) {
		this.descricaoEspecie = descricaoEspecie;
	}
	public String getDescricaoMarca() {
		return descricaoMarca;
	}
	public void setDescricaoMarca(String descricaoMarca) {
		this.descricaoMarca = descricaoMarca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNumeroArma() {
		return numeroArma;
	}
	public void setNumeroArma(String numeroArma) {
		this.numeroArma = numeroArma;
	}
	public String getNumeroSinarm() {
		return numeroSinarm;
	}
	public void setNumeroSinarm(String numeroSinarm) {
		this.numeroSinarm = numeroSinarm;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getUsuNumctr() {
		return usuNumctr;
	}
	public void setUsuNumctr(String usuNumctr) {
		this.usuNumctr = usuNumctr;
	}
	public String getUsuNumpos() {
		return usuNumpos;
	}
	public void setUsuNumpos(String usuNumpos) {
		this.usuNumpos = usuNumpos;
	}
	public String getDataValidade() {
		return dataValidade;
	}
	public void setDataValidade(String dataValidade) {
		this.dataValidade = dataValidade;
	}
	public String getDataLotacao()
	{
		return dataLotacao;
	}
	public void setDataLotacao(String dataLotacao)
	{
		this.dataLotacao = dataLotacao;
	}
	public String getLinkDetalhes()
	{
		return linkDetalhes;
	}
	public void setLinkDetalhes(String linkDetalhes)
	{
		this.linkDetalhes = linkDetalhes;
	}
	
	
	
	

}
