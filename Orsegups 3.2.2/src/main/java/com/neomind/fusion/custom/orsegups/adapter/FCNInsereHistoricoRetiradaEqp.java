package com.neomind.fusion.custom.orsegups.adapter;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FCNInsereHistoricoRetiradaEqp implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(FCNInsereHistoricoRetiradaEqp.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		WorkflowException exception = null;
		try
		{
			NeoObject registro = AdapterUtils.createNewEntityInstance("FCNcancelamentoEletHistoricoRetiradaEqp");
			EntityWrapper wRegistro = new EntityWrapper(registro);
			StringBuilder equipReceb = new StringBuilder();
			List<NeoObject> listaEqp = new ArrayList<NeoObject>();

			//dados de quem avançou a tarefa e data
			NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));
			if (origin != null)
			{
				if (origin.getUser() != null)
				{
					System.out.println(origin.getUser().toString());
					wRegistro.findField("responsavel").setValue(origin.getUser());
				}
				else
				{
					System.out.println(objSolicitante.toString());
					wRegistro.findField("responsavel").setValue(objSolicitante);
				}
			}
			else
			{
				System.out.println(objSolicitante.toString());
				wRegistro.findField("responsavel").setValue(objSolicitante);
			}
			wRegistro.findField("dataAcao").setValue(new GregorianCalendar());

			//Fluxo C023
			if (activity.getProcess().getModel().getName().equals("C023 - FCN - Cancelamento de Contrato Iniciativa eNEW"))
			{
				//grava campos no histórico C023
				Boolean equiChegou = processEntity.findGenericValue("equiChegou");
				if (equiChegou != null && equiChegou)
				{
					wRegistro.findField("equiChegou").setValue("Sim");
				}
				else
				{
					wRegistro.findField("equiChegou").setValue("Não");
				}

				if ((Boolean) processEntity.findValue("quantConfer"))
				{
					wRegistro.findField("quantConfer").setValue("Sim");
				}
				else
				{
					wRegistro.findField("quantConfer").setValue("Não");
				}
				wRegistro.findField("dataChegadaEquip").setValue(processEntity.findValue("dataChegadaEquip"));
				listaEqp = (List<NeoObject>) processEntity.findValue("equipReceb");
				if (NeoUtils.safeIsNotNull(listaEqp) && (equiChegou != null && equiChegou))
				{
					for (NeoObject obj : listaEqp)
					{
						EntityWrapper registroWrapper = new EntityWrapper(obj);
						equipReceb.append(registroWrapper.findValue("tipoEquipamento.descricaoTipoEquipamento") + " Qtd: " + registroWrapper.findValue("quantidadeEquipamento").toString() + "\n");
					}
				}
				wRegistro.findField("equipReceb").setValue(equipReceb.toString());
				wRegistro.findField("observacoes").setValue(processEntity.findValue("obs05"));
				wRegistro.findField("dataFinalRecebimento").setValue(processEntity.findValue("dataFinalRecebimento"));
				wRegistro.findField("anexo").setValue(processEntity.findValue("anexo05"));
				if (!(Boolean) processEntity.findValue("quantConfer"))
				{
					processEntity.setValue("quantConfer", null);
				}
				processEntity.setValue("equiChegou", null);
				processEntity.setValue("dataChegadaEquip", null);
				//processEntity.setValue("equipReceb", null);
				processEntity.setValue("obs05", null);
				processEntity.setValue("dataFinalRecebimento", null);
				processEntity.setValue("anexo05", null);
			}

			//Fluxo C025
			if (activity.getProcess().getModel().getName().equals("C025 - FCN - Cancelamento de Contrato Inadimplência eNEW"))
			{
				//grava campos no histórico C025			
				if ((Boolean) processEntity.findValue("equipChegar"))
				{
					wRegistro.findField("equiChegou").setValue("Sim");
				}
				else
				{
					wRegistro.findField("equiChegou").setValue("Não");
				}

				if ((Boolean) processEntity.findValue("quantConfere"))
				{
					wRegistro.findField("quantConfer").setValue("Sim");
				}
				else
				{
					wRegistro.findField("quantConfer").setValue("Não");
				}
				wRegistro.findField("dataChegadaEquip").setValue(processEntity.findValue("dataChegEquip"));
				listaEqp = (List<NeoObject>) processEntity.findValue("equipReceb");
				if (NeoUtils.safeIsNotNull(listaEqp))
				{
					for (NeoObject obj : listaEqp)
					{
						EntityWrapper registroWrapper = new EntityWrapper(obj);
						equipReceb.append(registroWrapper.findValue("tipoEquipamento.descricaoTipoEquipamento") + " Qtd: " + registroWrapper.findValue("quantidadeEquipamento").toString() + "\n ");
					}
				}
				wRegistro.findField("equipReceb").setValue(equipReceb.toString());
				wRegistro.findField("observacoes").setValue(processEntity.findValue("obs07"));
				wRegistro.findField("dataFinalRecebimento").setValue(processEntity.findValue("dataFinalRecebimento"));
				wRegistro.findField("anexo").setValue(processEntity.findValue("anexo07"));

				if (!(Boolean) processEntity.findValue("quantConfere"))
				{
					processEntity.setValue("quantConfere", null);
				}
				processEntity.setValue("equipChegar", null);
				processEntity.setValue("dataChegEquip", null);
				//processEntity.setValue("equipReceb", null);
				processEntity.setValue("obs07", null);
				processEntity.setValue("dataFinalRecebimento", null);
				processEntity.setValue("anexo07", null);
			}

			PersistEngine.persist(registro);
			processEntity.findField("historicoRetiradaEquipamento").addValue(registro);
			System.out.println("  ");
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
