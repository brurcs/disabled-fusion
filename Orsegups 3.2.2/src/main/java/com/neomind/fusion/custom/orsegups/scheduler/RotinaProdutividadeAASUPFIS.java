package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ait.controller.ProdutividadeAitController;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentosAitVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaProdutividadeAASUPFIS implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaProdutividadeAASUPFIS.class);
    private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");

    @Override
    public void execute(CustomJobContext arg0) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	try {

	    GregorianCalendar dataAtual = new GregorianCalendar();
	    dataAtual.set(Calendar.HOUR_OF_DAY, 0);
	    dataAtual.set(Calendar.MINUTE, 0);
	    dataAtual.set(Calendar.SECOND, 0);
	    dataAtual.set(Calendar.MILLISECOND, 0);

	    GregorianCalendar datIni = (GregorianCalendar) dataAtual.clone();
	    datIni.add(Calendar.MONTH, -1); // SEMPRE -1
	    datIni.set(Calendar.DAY_OF_MONTH, 16); // SEMPRE 16

	    System.out.println("Data inicial " + NeoDateUtils.safeDateFormat(datIni, "dd/MM/yyyy"));

	    GregorianCalendar datFim = (GregorianCalendar) dataAtual.clone();
	    datFim.set(Calendar.DAY_OF_MONTH, 16); // SEMPRE 16

	    System.out.println("Data fim " + NeoDateUtils.safeDateFormat(datFim, "dd/MM/yyyy"));

	    String competencia = NeoDateUtils.safeDateFormat(datFim, "MM/yyyy");

	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuilder sql = new StringBuilder();

	    sql.append("    SELECT SUBSTRING(vtr.NM_VIATURA, 1, 3) AS REG, vtr.NM_VIATURA AS AA,   ");
	    sql.append("    vtr.CD_VIATURA  ");
	    sql.append(" 	FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
	    sql.append(" 	INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE    ");
	    sql.append(" 	INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA    ");
	    sql.append(" 	INNER JOIN USUARIO u WITH (NOLOCK) ON u.CD_USUARIO = h.CD_USUARIO_FECHAMENTO   ");
	    sql.append(" 	INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = u.CD_COLABORADOR     ");
	    sql.append(" 	WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL      ");
	    sql.append(" 	AND h.DT_VIATURA_NO_LOCAL IS NOT NULL     ");
	    sql.append(" 	AND h.DT_FECHAMENTO >= ?    ");
	    sql.append(" 	AND h.DT_FECHAMENTO < ?      ");
	    sql.append(" 	AND vtr.NM_VIATURA NOT LIKE '%PLACAS%'    ");
	    sql.append("    AND DATEDIFF (MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0    ");
	    sql.append("    AND h.CD_EVENTO NOT IN ('XAAE', 'XXX8', 'X8', 'XXX7', 'X406', 'XXX2', 'XXX5', 'XDSE','XDSC')   ");
	    sql.append(" 	AND SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ('SOO','CTA','JLE','CSC','CUA','JGS','TRO','LGS','IAI','CCO','BQE','BNU','RSL','NHO','TRI','CAS','GNA','PMJ','SRR','XLN')  ");
	    sql.append(" 	GROUP BY  SUBSTRING(vtr.NM_VIATURA, 1, 3), vtr.NM_VIATURA, vtr.CD_VIATURA   ");
	    sql.append(" 	ORDER BY SUBSTRING(vtr.NM_VIATURA, 1, 3),vtr.NM_VIATURA      ");

	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setTimestamp(1, new Timestamp(datIni.getTimeInMillis()));
	    pstm.setTimestamp(2, new Timestamp(datFim.getTimeInMillis()));
	    rs = pstm.executeQuery();
	    TreeMap<Long, String> treeMap = new TreeMap<Long, String>();

	    while (rs.next()) {
		treeMap.put(rs.getLong("CD_VIATURA"), rs.getString("AA"));

	    }

	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	    efetuaLancamentoDaProdutividade(treeMap, competencia);

	    this.abrirTarefaSimplesProdutividadeAIT();

	} catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: PRODUTIVIDADE [" + key + "]" + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro ao processar job, procurar no log por:" + key);
	} finally {

	    OrsegupsUtils.closeConnection(conn, pstm, rs);

	    log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: PRODUTIVIDADE AA SUP FIS e Ordens de Serviço em Excesso - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}

    }

    private void abrirTarefaSimplesProdutividadeAIT() {

	try {

	    String executor = "";
	    String solicitante = "";
	    String titulo = "Produtividade AIT";
	    GregorianCalendar prazo = new GregorianCalendar();
	    prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 5L);
	    prazo.set(Calendar.HOUR_OF_DAY, 23);
	    prazo.set(Calendar.MINUTE, 59);
	    prazo.set(Calendar.SECOND, 59);

	    NeoPaper papel = new NeoPaper();
	    papel = OrsegupsUtils.getPaper("CPATarefaSimplesExecutor");

	    if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
		for (NeoUser user : papel.getUsers()) {
		    executor = user.getCode();
		    break;
		}
	    } else {
		log.error("##### ERRO AGENDADOR DE TAREFA: Tarefa Simples Produtividade AIT: - Papel " + papel.getName());
	    }

	    papel = OrsegupsUtils.getPaper("CPATarefaSimplesSolicitante");

	    if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
		for (NeoUser user : papel.getUsers()) {
		    solicitante = user.getCode();
		    break;
		}
	    } else {
		log.error("##### ERRO AGENDADOR DE TAREFA: Tarefa Simples Produtividade AIT: - Papel " + papel.getName());
	    }

	    String descricao = "";
	    descricao = " Sr. Edson, favor proceder análise da produtividade dos AITs.";

	    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	    String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
	    System.out.println(" Tarefa Simples Produtividade AIT " + tarefa);

	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    private void efetuaLancamentoDaProdutividade(TreeMap<Long, String> treeMap, String competencia) {

	try {

	    if (treeMap != null && !treeMap.isEmpty()) {
		Set<Entry<Long, String>> set = treeMap.entrySet();
		Iterator<Entry<Long, String>> iterator = set.iterator();
		ViaturaVO viaturaVO = null;
		boolean isActiveEmployee = false;

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 5L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		while (iterator.hasNext()) {
		    try {
				@SuppressWarnings("rawtypes")
				Map.Entry mentry = (Map.Entry) iterator.next();
   
				Long longV = (Long) mentry.getKey();

				int codigoViatura = longV.intValue();

				List<ViaturaVO> aitObjs = this.getViaturaXColaboradorByCDViatura(codigoViatura);		    
				Long cdViatura = (Long) mentry.getKey();
				viaturaVO = aitObjs.get(0);
				isActiveEmployee = this.verificarColaboradorAtivo(viaturaVO);

				// Método de validação para verificar se o colaborador ainda é ativo (!= 7)
				if(isActiveEmployee) {
				
				String totalProdutividade = this.getTotalProdutividadeAIT(cdViatura.toString(), competencia);

				double valorProdutividade = 0;

				if (totalProdutividade != null && !totalProdutividade.isEmpty()) {

				    try {

					if (totalProdutividade.contains(".")) {
					    totalProdutividade = totalProdutividade.replace(",", ".");
					    totalProdutividade = totalProdutividade.replaceFirst("[.]", "");
					    valorProdutividade = Double.parseDouble(totalProdutividade);
					} else {
					    valorProdutividade = Double.parseDouble(totalProdutividade.replace(",", "."));
					}

				    } catch (NumberFormatException e) {
					log.error("RotinaProdutividadeAASUPFIS - Tentou converter o valor: " + totalProdutividade + " - Com replace: " + totalProdutividade.replaceAll(",", ".") + " Para a viatura: " + cdViatura + " " + (String) mentry.getValue());
					e.printStackTrace();
				    }
				}

				String nmViatura = (String) mentry.getValue();

				// VALIDA PRODUTIVIDADE
				if (valorProdutividade > 0) {

				    // VALIDA SE EXISTE VINCULO ENTRE VIATURA X COLABORADOR
				    if (aitObjs == null || aitObjs != null && aitObjs.isEmpty()) {
					abreTarefaViaturaSemVinculoComColaborador(nmViatura, cdViatura.toString(), competencia);
				    }

				    cadastraProdutividade(cdViatura.toString(), nmViatura, competencia, prazo, totalProdutividade);		    

				} else {
				    log.warn("##### AGENDADOR DE TAREFA: PRODUTIVIDADE AA SUP FIS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " AIT Sem Produtividade: " + cdViatura + " " + nmViatura + " Competencia: " + competencia);
				}
				}
			} catch (WorkflowException e) {
				e.printStackTrace();
				throw new WorkflowException("Erro na RotinaProdutividadeAASUPFIS "+e.getErrorList().get(0).getI18nMessage().toString());
			} catch (Exception e) {
				e.printStackTrace();
				throw new WorkflowException("Erro na RotinaProdutividadeAASUPFIS "+e.getMessage() == null ? "" : e.getMessage());
			} 
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private List<ViaturaVO> getViaturaXColaboradorByCDViatura(int cdViatura) {

	List<ViaturaVO> result = new ArrayList<ViaturaVO>();

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    StringBuilder sql = new StringBuilder();

	    sql.append(" SELECT cpfInt, nm_viatura FROM D_RHViaturaColaborador RH WITH(NOLOCK) ");
	    sql.append(" INNER JOIN X_SIGMA90VIATURA X WITH(NOLOCK) ON X.neoId = RH.viatura_neoId ");
	    sql.append(" INNER JOIN [" + this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR) + "\\" + this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA) + "].SIGMA90.dbo.VIATURA V WITH(NOLOCK) ON V.CD_VIATURA = X.cd_viatura ");
	    sql.append(" WHERE V.CD_VIATURA = ? ");

	    conn = PersistEngine.getConnection("");

	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, cdViatura);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		ViaturaVO v = new ViaturaVO();
		v.setCpf(rs.getLong(1));
		v.setNomeMotorista(rs.getString(2));
		result.add(v);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return result;



    }

    private String getTotalProdutividadeAIT(String cdViatura, String competencia) {
	String totalProdutividade = "";

	ProdutividadeAitController produtividadeAitController = new ProdutividadeAitController();

	String array[] = competencia.split("/");

	String mes = array[0];
	String ano = array[1];

	List<AtendimentosAitVO> produtividadeAASUPFISs = produtividadeAitController.listaAtendimentos(cdViatura.toString(), mes, ano);

	if (NeoUtils.safeIsNotNull(produtividadeAASUPFISs) && !produtividadeAASUPFISs.isEmpty()) {

	    Double totalValorDSL = 0.0;
	    Double totalValorATD = 0.0;
	    Double totalDeslocamento = 0.0;
	    Double totalAtendimento = 0.0;

	    String aitAnt = "";

	    DecimalFormat formatoReal = new DecimalFormat("#,##0.00");

	    for (AtendimentosAitVO aasupfisObj : produtividadeAASUPFISs) {
		Double valorDSL = 0.0;
		Double valorATD = 0.0;
		String aitAtual = aasupfisObj.getAit();

		if (aasupfisObj.getDeslocamento() != null && !aasupfisObj.getDeslocamento().isEmpty()) {
		    if (Integer.parseInt(aasupfisObj.getDeslocamento()) >= 0 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 10) {
			valorDSL = 2.0;
		    } else if (Integer.parseInt(aasupfisObj.getDeslocamento()) > 10 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 15) {
			valorDSL = 1.0;
		    } else if (Integer.parseInt(aasupfisObj.getDeslocamento()) > 15 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 20) {
			valorDSL = 0.50;
		    }

		    if (Integer.parseInt(aasupfisObj.getAtendimento()) >= 0 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 15) {
			valorATD = 1.0;
		    } else if (Integer.parseInt(aasupfisObj.getAtendimento()) > 15 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 20) {
			valorATD = 0.50;
		    } else if (Integer.parseInt(aasupfisObj.getAtendimento()) > 20 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 25) {
			valorATD = 0.25;
		    }
		}
		if (aitAtual != null && !aitAtual.isEmpty() && aitAnt != null && !aitAnt.isEmpty() && !aitAtual.equalsIgnoreCase(aitAnt)) {
		    aitAnt = aitAtual;
		}

		totalDeslocamento += Integer.parseInt(aasupfisObj.getDeslocamento());
		totalAtendimento += Integer.parseInt(aasupfisObj.getAtendimento());
		totalValorDSL += valorDSL;
		totalValorATD += valorATD;

	    }

	    totalProdutividade = NeoUtils.safeFormat(formatoReal, totalValorDSL + totalValorATD);

	}

	return totalProdutividade;
    }

    private void cadastraProdutividade(String cdViatura, String nmViatura, String competencia, GregorianCalendar prazo, String totalProdutividade) {
	try {



	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("cdViatura", Long.parseLong(cdViatura)));
	    filter.addFilter(new QLEqualsFilter("competencia", competencia));

	    List<NeoObject> aitObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CPAProdutividadeAIT"), filter);

	    if ((aitObjs != null && aitObjs.isEmpty()) || aitObjs == null) {

		NeoObject noPS = AdapterUtils.createNewEntityInstance("CPAProdutividadeAIT");

		EntityWrapper psWrapper = new EntityWrapper(noPS);

		psWrapper.findField("cdViatura").setValue(Long.parseLong(cdViatura));
		psWrapper.findField("nmViatura").setValue(nmViatura);
		psWrapper.findField("competencia").setValue(competencia);
		psWrapper.findField("prazo").setValue(prazo);
		psWrapper.findField("totalProdutividade").setValue(totalProdutividade);

		QLGroupFilter filterAux = new QLGroupFilter("AND");
		filterAux.addFilter(new QLEqualsFilter("descricao", "GERADA"));
		List<NeoObject> situacao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CPASituacao"), filterAux);
		psWrapper.findField("situacao").setValue(situacao.get(0));
		PersistEngine.persist(noPS);		
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private void abreTarefaViaturaSemVinculoComColaborador(String nmViatura, String cdViatura, String competencia) {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("cdViatura", Long.parseLong(cdViatura)));
	    filter.addFilter(new QLEqualsFilter("competencia", competencia));
	    List<NeoObject> aitObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CPAProdutividadeAIT"), filter);

	    if ((aitObjs != null && aitObjs.isEmpty()) || aitObjs == null) {
		String executor = "";
		String solicitante = "";
		String titulo = "Providenciar cadastro viatura x colaborador";
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		NeoPaper papel = new NeoPaper();
		papel = OrsegupsUtils.getPaper("RHExecutorCadastroViaturaColaborador");

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
		    for (NeoUser user : papel.getUsers()) {
			executor = user.getCode();
			break;
		    }
		} else {
		    log.error("##### ERRO AGENDADOR DE TAREFA:Providenciar cadastro viatura x colaborador: - Papel " + papel.getName());
		}

		papel = OrsegupsUtils.getPaper("RHSolicitanteCadastroViaturaColaborador");

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
		    for (NeoUser user : papel.getUsers()) {
			solicitante = user.getCode();
			break;
		    }
		} else {
		    log.error("##### ERRO AGENDADOR DE TAREFA:Providenciar cadastro viatura x colaborador: - Papel " + papel.getName());
		}

		String descricao = "";
		descricao = "<strong>Providenciar cadastro viatura x colaborador: " + nmViatura + "</strong>";
		descricao = descricao.replaceAll("'", "");
		descricao = descricao.replaceAll("\"", "");

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		System.out.println(" Tarefa Providenciar cadastro viatura x colaborador " + tarefa);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private boolean verificarColaboradorAtivo(ViaturaVO viatura) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();
	boolean isActiveEmployee = false;


	sql.append(" SELECT * FROM ");
	sql.append(" R034FUN FUN ");
	sql.append(" WHERE FUN.NUMCPF = " + viatura.getCpf() + " ");
	sql.append(" AND FUN.SITAFA != 7 ");

	try {
	    conn = PersistEngine.getConnection("VETORH");
	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();

	    if(rs.next()){
		isActiveEmployee = true;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e2) {
		e2.printStackTrace();
	    }
	}	
	return isActiveEmployee;
    }
}
