package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.mail.MultiPartEmail;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class EmailDeliveryInformaOSFechadaAutomaticamente  implements CustomJobAdapter {

	@Override
	public void execute(CustomJobContext ctx) {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
		String ultimaExecucaoRotina = OrsegupsEmailUtils.ultimaExecucaoRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_OS_FECHADA);

		
		try {
			System.out.println("EMAIL_OS_FECHADA INICIADO: ultimaExecucaoRotina="+ultimaExecucaoRotina);
			conn = PersistEngine.getConnection("SIGMA90");

			sql.append("    SELECT DISTINCT RAT.ID_ORDEM as id_ordem ");
			sql.append("      FROM [FSOODB04\\SQL02].TIDB.DBO.HISTORICO_RAT_OS_AGENDADA RAT ");
			sql.append("INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbORDEM OS ON RAT.ID_ORDEM = OS.ID_ORDEM ");
			sql.append("			                                      AND OS.OPFECHOU = 9998 AND RAT.DT_AGENDAMENTO IS NOT NULL ");
			sql.append("			                                      AND OS.FECHAMENTO BETWEEN '" + ultimaExecucaoRotina + "' AND GETDATE() ");
			sql.append("  ORDER BY 1");

			pstm = conn.prepareStatement(sql.toString());

			OrsegupsEmailUtils.inserirFimRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_OS_FECHADA);
			
			rs = pstm.executeQuery();
					
			while(rs.next()) {

				String idOrdem = rs.getString("id_ordem");
				
				MultiPartEmail email = new MultiPartEmail();								
				StringBuilder mensagem = new StringBuilder();

				mensagem.append("Realizar contato com o cliente para informar o cancelamento do agendamento técnico devido restauro do sistema.\n");
				mensagem.append("OS: " + idOrdem);
				

				email.addTo("cm@orsegups.com.br");
				email.addBcc("emailautomatico@orsegups.com.br");
				email.setSubject("Cancelar Agendamento Técnico");
				email.setMsg(mensagem.toString());
				email.setFrom(settings.getFromEMail(),settings.getFromName());
				email.setSmtpPort(settings.getPort());
				email.setHostName(settings.getSmtpServer());
				email.send();
				System.out.println("EMAIL_OS_FECHADA ID_ORDEM:"+idOrdem);

			}
			System.out.println("EMAIL_OS_FECHADA FINALIZADO");

		} catch (Exception e) {		
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

	}

}
