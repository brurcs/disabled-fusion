package com.neomind.fusion.custom.orsegups.maps.call.vo;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;

public class CallAlertVO
{
	private ViaturaVO callViaturaVO;
	
	private EventoVO callEventoVO;
	
	private GregorianCalendar lastCallCalendar;

	public ViaturaVO getCallViaturaVO()
	{
		return callViaturaVO;
	}

	public void setCallViaturaVO(ViaturaVO callViaturaVO)
	{
		this.callViaturaVO = callViaturaVO;
	}

	public EventoVO getCallEventoVO()
	{
		return callEventoVO;
	}

	public void setCallEventoVO(EventoVO callEventoVO)
	{
		this.callEventoVO = callEventoVO;
	}

	public GregorianCalendar getLastCallCalendar()
	{
		return lastCallCalendar;
	}

	public void setLastCallCalendar(GregorianCalendar lastCallCalendar)
	{
		this.lastCallCalendar = lastCallCalendar;
	}
}
