package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesDeslocamentoExcessivo implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesDeslocamentoExcessivo.class);

	@Override
	public void execute(CustomJobContext arg0)	{

		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		String reg = "";
		Connection conn = null;
		StringBuilder sqlDeslocamentoExcessivo = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try	{
			sqlDeslocamentoExcessivo.append(" SELECT XR.CD_ROTA INTO #TEMP_ROTAS_PAGAS FROM [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.X_SIGMAROTA XR                                                       ");
			sqlDeslocamentoExcessivo.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPAplicacaoTatico TATICO ON XR.neoId = TATICO.rotaSigma_neoId                                        ");
			sqlDeslocamentoExcessivo.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPTaticoModalidadePgto MODAL ON MODAL.neoId = TATICO.modalidade_neoId                                ");
			sqlDeslocamentoExcessivo.append(" WHERE MODAL.CODIGO = 3                                                                                                                                 ");
			
			sqlDeslocamentoExcessivo.append("  SELECT  TOP(25) h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3) AS sigla_regional, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, COUNT(*) AS  QtdDsl        ");
			sqlDeslocamentoExcessivo.append("  , GETDATE()-15 as PrimeiraData,GETDATE() as UltimaData                                                                                                ");
			sqlDeslocamentoExcessivo.append("  FROM VIEW_HISTORICO h WITH (NOLOCK)                                                                                                                   ");
			sqlDeslocamentoExcessivo.append("  INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE                                                                                   ");
			sqlDeslocamentoExcessivo.append("  INNER JOIN ROTA r WITH (NOLOCK) ON  r.CD_ROTA = c.ID_ROTA                                                                                             ");
			sqlDeslocamentoExcessivo.append("  WHERE h.DT_VIATURA_NO_LOCAL BETWEEN   GETDATE()-15 AND  GETDATE()                                                                                     ");
			sqlDeslocamentoExcessivo.append("  AND h.CD_CODE <> 'RNP'                                                                                                                                ");
			sqlDeslocamentoExcessivo.append("  AND h.CD_CODE <> 'RNA'                                                                                                                                ");
			sqlDeslocamentoExcessivo.append("  AND c.ID_CENTRAL NOT LIKE 'AAA%'                                                                                                                      ");
			sqlDeslocamentoExcessivo.append("  AND r.NM_ROTA NOT LIKE '%UNIVALI%'                                                                                                                    ");
			sqlDeslocamentoExcessivo.append("  AND SUBSTRING(r.NM_ROTA, 1, 3) IN ('IAI','BQE','BNU','JLE','LGS','CUA','GPR','SOO','CCO','RSL','CTA','TRO','NHO','TRI','CAS','GNA','PMJ','SRR','XLN') "); 
			sqlDeslocamentoExcessivo.append("  AND h.CD_EVENTO IN ('E123','E124')                                                                                               ");
			sqlDeslocamentoExcessivo.append("  AND CD_ROTA NOT IN (SELECT * FROM #TEMP_ROTAS_PAGAS T WHERE T.CD_ROTA = R.CD_ROTA )                                                                   ");
			sqlDeslocamentoExcessivo.append("  GROUP BY h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3), c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO                                                      ");
			sqlDeslocamentoExcessivo.append("  HAVING COUNT(*) > 1                                                                                                                                   ");
			sqlDeslocamentoExcessivo.append("  ORDER BY 7 DESC                                                                                                                                       ");
			
			sqlDeslocamentoExcessivo.append("  DROP TABLE #TEMP_ROTAS_PAGAS                                                                                                                          ");
			
			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(sqlDeslocamentoExcessivo.toString());

			rs = pstm.executeQuery();
			String executor = "";

			while (rs.next()) {
				try	{
					String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteDeslocamentoExcessivo");
					String tituloAux = "Tratar Deslocamentos em Excesso Diversos - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]% ";
					String titulo = "Tratar Deslocamentos em Excesso Diversos - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] "+rs.getString("RAZAO");

					GregorianCalendar prazo = new GregorianCalendar();
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 7L);
					prazo.set(Calendar.HOUR_OF_DAY, 23);
					prazo.set(Calendar.MINUTE, 59);
					prazo.set(Calendar.SECOND, 59);

					NeoPaper papel = new NeoPaper();
					papel = OrsegupsUtils.getPaper("ExecutorDeslocamentoExcessivo");				
					executor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papel).getCode();

					String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
					descricao += " <strong>Conta :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
					descricao = descricao + " <strong>Razão Social :</strong> " + rs.getString("RAZAO") + " <br>";
					descricao = descricao + " <strong>Nome Fantasia :</strong> " + rs.getString("FANTASIA") + " <br>";
					descricao = descricao + " <strong>Período :</strong> " + NeoDateUtils.safeDateFormat(rs.getDate("PrimeiraData"), "dd/MM/yyyy") + "<strong>  à </strong>" + NeoDateUtils.safeDateFormat(rs.getDate("UltimaData"), "dd/MM/yyyy") + "<br>";
					descricao = descricao + " <strong>Deslocamentos :</strong> " + rs.getString("QtdDsl") + "<br>";

					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

					log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos - Responsavel : " + executor + " Regional: " + reg + " Tarefa: " + tarefa);
					System.out.println("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos - Responsavel : " + executor + " Regional: " + reg + " Tarefa: " + tarefa);	
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos Eventos. Cliente : " + rs.getString("ID_CENTRAL"));
				}
			}
		} catch (Exception e) {
			log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos - ERRO AO GERAR TAREFA - Regional: " + reg);
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos - ERRO AO GERAR TAREFA - Regional: " + reg);
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Deslocamentos Excessivos e Ordens de Serviço em Excesso - Regional: " + reg + " Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
