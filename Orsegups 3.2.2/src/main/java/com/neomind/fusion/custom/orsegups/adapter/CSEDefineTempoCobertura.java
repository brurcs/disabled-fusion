package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CSEDefineTempoCobertura implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			GregorianCalendar datini = new GregorianCalendar();
			GregorianCalendar datfim = new GregorianCalendar();
			
			datini = (GregorianCalendar) processEntity.findValue("qlExecucao.data");
			datfim = (GregorianCalendar) processEntity.findValue("qlExecucao.dataFinal");
			
			Long horas = (datfim.getTimeInMillis() - datini.getTimeInMillis()) /3600000L;
			
			processEntity.setValue("tempoCobertura", horas);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("CSE - Erro na regra de escalonamento.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}