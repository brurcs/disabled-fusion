package com.neomind.fusion.custom.orsegups.mobile;

import com.neomind.fusion.custom.orsegups.mobile.utils.MobileUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.mobile.MobileDefinePoolRegional
public class MobileDefinePoolRegional implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String strSiglaRegional = null;
		
		Task origem = OrsegupsWorkflowHelper.getTaskOrigin(activity); 
		

		
		try{
			
			strSiglaRegional = NeoUtils.safeOutputString(processEntity.findValue("relatorioInspecao.siglaRegional"));
			
			
			NeoPaper poolInspecao = MobileUtils.getPapelBySiglaRegional(strSiglaRegional);
			processEntity.setValue("poolQualidadeRegional", poolInspecao);
			
			
		}catch(Exception e){
			throw new WorkflowException("Erro ao definir pool da regional (tarefa:"+origem.getCode()+", Sigla:"+strSiglaRegional);
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}

}
