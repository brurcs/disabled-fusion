package com.neomind.fusion.custom.orsegups.adapter;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.axis.utils.BeanUtils;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class ChipGPRSRecuperaChipsDefeito implements AdapterInterface 
{
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			String nomeFonteDados = "SAPIENS";
	
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			
			Long codEmp = (Long) processEntity.findValue("postoSapiens.usu_codemp");
			Long codFil = (Long) processEntity.findValue("postoSapiens.usu_codfil");
			Long numCtr = (Long) processEntity.findValue("postoSapiens.usu_numctr");
			Long numPos = (Long) processEntity.findValue("postoSapiens.usu_numpos");
			String codSer = (String) processEntity.findValue("postoSapiens.usu_codser");
				
			StringBuffer sqlSelect = new StringBuffer();
			sqlSelect.append(" SELECT usu_iccid FROM USU_T160CHP ");
			sqlSelect.append(" WHERE usu_codemp = ? AND usu_codfil = ? AND usu_numctr = ? AND usu_numpos = ? AND usu_codser = ? AND usu_sitchp = ? ");
			
			PreparedStatement stSelect = connection.prepareStatement(sqlSelect.toString());
			stSelect.setLong(1, codEmp);
			stSelect.setLong(2, codFil);
			stSelect.setLong(3, numCtr);
			stSelect.setLong(4, numPos);
			stSelect.setString(5, codSer);
			stSelect.setString(6, "A");
			System.out.println("Chip: SELECT usu_iccid FROM USU_T160CHP  WHERE usu_codemp = "+codEmp+" AND usu_codfil = "+codFil+" AND usu_numctr = "+numCtr+" AND usu_numpos = "+numPos+" AND usu_codser = "+codSer);
			
			ResultSet rs = stSelect.executeQuery();
			
			processEntity.findField("chipsGPRSSubstituir").removeValues();
			
			while(rs.next()) {							
				String iccid = rs.getString("usu_iccid");
				
				QLEqualsFilter qf = new QLEqualsFilter("iccID",iccid);
				Collection<NeoObject> chipGprs = PersistEngine.getObjects(AdapterUtils.getEntityClass("TELECOMChipsGPRS"), qf);
				List<NeoObject> listSub = new ArrayList<NeoObject>();
				for (NeoObject chip : chipGprs)
				{
					NeoObject oRegistroAtividade = AdapterUtils.createNewEntityInstance("SubstituirChipGPRS");
					EntityWrapper wRegistroAtividade = new EntityWrapper(oRegistroAtividade);
					wRegistroAtividade.setValue("ChipGPRSPosto", org.apache.commons.beanutils.BeanUtils.cloneBean(chip));
					PersistEngine.persist(oRegistroAtividade);
					listSub.add(oRegistroAtividade);
				}
				processEntity.setValue("chipsGPRSSubstituir", listSub);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel recuperar as informação no Sapiens.");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel recuperar as informação no Sapiens.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
