package com.neomind.fusion.custom.orsegups.presenca.vo;

public class RTOciosoVO {
	
	String neoId;
	String tarefa;
	String data;
	String link;
	
	public String getNeoId() {
		return neoId;
	}
	public void setNeoId(String neoId) {
		this.neoId = neoId;
	}
	public String getTarefa() {
		return tarefa;
	}
	public void setTarefa(String tarefa) {
		this.tarefa = tarefa;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	

}
