package com.neomind.fusion.custom.orsegups.e2doc.xml.ComercialPublico;

public class Campos {
    
    private String i83;
    private String i84;
    private String i85;
    private String i86;
    private String i87;
    private String i88;
    
    public String getI83() {
        return i83;
    }
    public void setI83(String i83) {
        this.i83 = i83;
    }
    public String getI84() {
        return i84;
    }
    public void setI84(String i84) {
        this.i84 = i84;
    }
    public String getI85() {
        return i85;
    }
    public void setI85(String i85) {
        this.i85 = i85;
    }
    public String getI86() {
        return i86;
    }
    public void setI86(String i86) {
        this.i86 = i86;
    }
    public String getI87() {
        return i87;
    }
    public void setI87(String i87) {
        this.i87 = i87;
    }
    public String getI88() {
        return i88;
    }
    public void setI88(String i88) {
        this.i88 = i88;
    }
    
}
