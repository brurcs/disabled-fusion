package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

import org.apache.commons.lang.StringUtils;

public class FAPCopiaDeCamposGLNSliparPagamentoAgrupado implements AdapterInterface
{
	public static String mensagem = "";
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		try
		{ 
			List<NeoObject> listaCentroCusto = new ArrayList<>();
			listaCentroCusto.add(retornaCentroDeCustoPadrao(processEntity)); 
			Long limite = Long.parseLong(FAPParametrizacao.findParameter("limiteCentroDeCusto"));
			
			String nomeAplicacao = (String) processEntity.findValue("aplicacaoPagamento.nome");
			Boolean abrirTitulo = (Boolean) processEntity.findValue("abrirTitulo");

			Long tipoLancamento = (Long) processEntity.findValue("lancarNotaOuTitulo.codigoLancamento");
			String usuarioSolicitante = (String) processEntity.findValue("solicitanteOrcamento.code");

			QLGroupFilter filtroCondicaoPagamento = new QLGroupFilter("AND");
			filtroCondicaoPagamento.addFilter(new QLEqualsFilter("codemp", ((Long) processEntity.findValue("empresa.codemp"))));
			filtroCondicaoPagamento.addFilter(new QLEqualsFilter("codcpg", "AV"));
			List<NeoObject> condicaoPagamento = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CondicaoPagamento"), filtroCondicaoPagamento);
			if (condicaoPagamento != null && condicaoPagamento.size() > 0)
			{
				condicaoPagamento.get(0);
			}
			else
			{
				mensagem = "Não encontrado a Condição de Pagamento para lançamento da Nota Fiscal de Entrada!";
				throw new WorkflowException(mensagem);
			}

			QLEqualsFilter fitroUnidadeMedida = new QLEqualsFilter("unimed", "UN");
			List<NeoObject> lstUnidadeMedida = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE015MED"), fitroUnidadeMedida);
			if (lstUnidadeMedida != null && lstUnidadeMedida.size() > 0)
			{
				lstUnidadeMedida.get(0);
			}
			else
			{
				mensagem = "Não encontrado a Unidade de Medida para lançamento da Nota Fiscal de Entrada!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroFormaPagamento = new QLGroupFilter("AND");
			filtroFormaPagamento.addFilter(new QLEqualsFilter("codemp", ((Long) processEntity.findValue("empresa.codemp"))));
			filtroFormaPagamento.addFilter(new QLEqualsFilter("codfpg", ((Long) processEntity.findValue("formaPagamento.codigoFormaPagamento"))));
			List<NeoObject> formaPagamento = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE066FPG"), filtroFormaPagamento);
			if (formaPagamento != null && formaPagamento.size() > 0)
			{
				formaPagamento.get(0);
			}
			else
			{
				mensagem = "Não encontrado o Código da Forma de Pagamento para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroUsuarioSolicitante = new QLGroupFilter("AND");
			filtroUsuarioSolicitante.addFilter(new QLEqualsFilter("nomusu", (usuarioSolicitante)));
			List<NeoObject> solicitante = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), filtroUsuarioSolicitante);
			if (solicitante != null && solicitante.size() > 0)
			{
				solicitante.get(0);
			}
			else
			{
				mensagem = "Não encontrado o Código do usuário Solicitante informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			if (!abrirTitulo && tipoLancamento == 1L)
			{
				InstantiableEntityInfo gestaLancamentoDeNota = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNota = gestaLancamentoDeNota.createNewInstance();
				EntityWrapper wrpGestaLancamentoDeNota = new EntityWrapper(oGestaLancamentoDeNota);

				wrpGestaLancamentoDeNota.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaLancamentoDeNota.setValue("automatico", Boolean.TRUE);
				wrpGestaLancamentoDeNota.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaLancamentoDeNota.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaLancamentoDeNota.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaLancamentoDeNota.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaLancamentoDeNota.setValue("formaPagamento", formaPagamento.get(0));
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaLancamentoDeNota.setValue("usuarioResponsavel", solicitante.get(0));
				}

				List<NeoObject> listaFapsAgrupadas = new ArrayList<NeoObject>();
				List<NeoObject> listaFapAgrupadasProduto = new ArrayList<NeoObject>();
				List<NeoObject> listaFapAgrupadasServico = new ArrayList<NeoObject>();
				String codigoTarefa = (String) processEntity.findValue("wfprocess.code");
				QLEqualsFilter codigoTarefaFilter = new QLEqualsFilter("codigoTarefa", codigoTarefa);
				listaFapsAgrupadas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), codigoTarefaFilter);

				BigDecimal valorTotal = new BigDecimal(0.0);
				BigDecimal valorTotalProduto = new BigDecimal(0.0);
				BigDecimal valorTotalServico = new BigDecimal(0.0);
				BigDecimal valorTotalDesconto = new BigDecimal(0.0);

				//120
				valorTotalProduto = totalizaFapAgrupadaPorTipoDeItem("PRODUTO", listaFapsAgrupadas);
				listaFapAgrupadasProduto = retornaListaFapAgrupadaPorTipoDeOrcamento("PRODUTO", listaFapsAgrupadas);

				List<NeoObject> lstCentroCustoProduto = new ArrayList<NeoObject>();
				List<NeoObject> lstCentroCustoProdutoObject = new ArrayList<NeoObject>();
				List<NeoObject> lstItensDeProduto = new ArrayList<NeoObject>();
				NeoObject oItensDeProduto = AdapterUtils.createNewEntityInstance("GLNItensDeProduto");
				EntityWrapper wrpItensDeProduto = new EntityWrapper(oItensDeProduto);
				InserirContaItensProdutoServico(processEntity, wrpItensDeProduto);

				if (valorTotalProduto.compareTo(new BigDecimal(0.00)) > 0)
				{
					lstCentroCustoProduto = verificaCentroCustoSapiens(((Long) processEntity.findValue("empresa.codemp")), listaFapsAgrupadas);
					
					lstCentroCustoProduto = (lstCentroCustoProduto.size() >= limite) ? listaCentroCusto : lstCentroCustoProduto;
					
					if (lstCentroCustoProduto != null && !lstCentroCustoProduto.isEmpty())
					{
						lstCentroCustoProdutoObject = insereCentroCustoFusionAgrupado(processEntity, ((Long) processEntity.findValue("empresa.codemp")), listaFapAgrupadasProduto, null);
					}

					wrpItensDeProduto.setValue("unidadeMedida", lstUnidadeMedida.get(0));
					wrpItensDeProduto.setValue("quantidadeRecebida", 1L);
					wrpItensDeProduto.setValue("precoUnitarioProduto", valorTotalProduto);
					wrpItensDeProduto.setValue("listaCentroCusto", lstCentroCustoProdutoObject);

					lstItensDeProduto.add(oItensDeProduto);
					PersistEngine.persist(oItensDeProduto);

					wrpGestaLancamentoDeNota.setValue("listaItensDeProduto", lstItensDeProduto);
				}

				List<NeoObject> lstCentroCustoServico = new ArrayList<NeoObject>();
				List<NeoObject> lstCentroCustoServicoObject = new ArrayList<NeoObject>();
				List<NeoObject> lstItensDeServico = new ArrayList<NeoObject>();
				NeoObject oItensDeServico = AdapterUtils.createNewEntityInstance("GLNItensDeServico");
				EntityWrapper wrpItensDeServico = new EntityWrapper(oItensDeServico);
				InserirContaItensProdutoServico(processEntity, wrpItensDeServico);

				//45				
				valorTotalDesconto = totalizaFapAgrupadaPorTipoDeItem("DESCONTO", listaFapsAgrupadas);
				valorTotalServico = totalizaFapAgrupadaPorTipoDeItem("SERVIÇO", listaFapsAgrupadas);
				valorTotalServico = valorTotalServico.subtract(valorTotalDesconto);
				listaFapAgrupadasServico = retornaListaFapAgrupadaPorTipoDeOrcamento("SERVIÇO", listaFapsAgrupadas);
				
				if (valorTotalServico.compareTo(new BigDecimal(0.00)) > 0)
				{
					lstCentroCustoServico = verificaCentroCustoSapiens(((Long) processEntity.findValue("empresa.codemp")), listaFapsAgrupadas);
					
					lstCentroCustoServico = (lstCentroCustoServico.size() >= limite) ? listaCentroCusto : lstCentroCustoServico;
					
					if (lstCentroCustoServico != null && !lstCentroCustoServico.isEmpty())
					{
						lstCentroCustoServicoObject = insereCentroCustoFusionAgrupado(processEntity, ((Long) processEntity.findValue("empresa.codemp")), listaFapAgrupadasServico, null);
					}

					wrpItensDeServico.setValue("unidadeMedida", lstUnidadeMedida.get(0));
					wrpItensDeServico.setValue("quantidadeRecebida", 1L);
					wrpItensDeServico.setValue("precoUnitarioServico", valorTotalServico);
					wrpItensDeServico.setValue("listaCentroCusto", lstCentroCustoServicoObject);

					lstItensDeServico.add(oItensDeServico);
					PersistEngine.persist(oItensDeServico);

					wrpGestaLancamentoDeNota.setValue("listaItensDeServico", lstItensDeServico);
				}

				String code = "";
				String observacao = "";
				code = (String) processEntity.findValue("wfprocess.code");
				observacao = nomeAplicacao + " FAP Agrupadora: " + code;

				List<NeoObject> lstParcelas = new ArrayList<NeoObject>();
				valorTotal = valorTotal.add(valorTotalProduto).add(valorTotalServico);
				lstParcelas = insereParcelasFusion(valorTotal, observacao);
				wrpGestaLancamentoDeNota.setValue("listaParcelas", lstParcelas);

				wrpGestaLancamentoDeNota.setValue("observacao", observacao);

				processEntity.findField("GlnGestaoDeLancamentoDeNotaAgrupado").setValue(oGestaLancamentoDeNota);
				PersistEngine.persist(oGestaLancamentoDeNota);
			}
			else if (abrirTitulo && tipoLancamento == 1L)
			{
				List<NeoObject> listaFapsAgrupadas = new ArrayList<NeoObject>();
				String codigoTarefa = (String) processEntity.findValue("wfprocess.code");
				QLEqualsFilter codigoTarefaFilter = new QLEqualsFilter("codigoTarefa", codigoTarefa);
				listaFapsAgrupadas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), codigoTarefaFilter);

				BigDecimal valorTotal = new BigDecimal(0.0);
				String observacao = "";
				String code = "";

				/* Gera Nota Fiscal via GLN para Produto */
				InstantiableEntityInfo gestaLancamentoDeNotaProduto = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNotaProduto = gestaLancamentoDeNotaProduto.createNewInstance();
				EntityWrapper wrpGestaLancamentoDeNotaProduto = new EntityWrapper(oGestaLancamentoDeNotaProduto);

				wrpGestaLancamentoDeNotaProduto.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaLancamentoDeNotaProduto.setValue("automatico", Boolean.TRUE);
				wrpGestaLancamentoDeNotaProduto.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaLancamentoDeNotaProduto.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaLancamentoDeNotaProduto.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaLancamentoDeNotaProduto.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaLancamentoDeNotaProduto.setValue("formaPagamento", formaPagamento.get(0));
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaLancamentoDeNotaProduto.setValue("usuarioResponsavel", solicitante.get(0));
				}

				List<NeoObject> listaFapAgrupadasProduto = new ArrayList<NeoObject>();
				BigDecimal valorTotalProduto = new BigDecimal(0.0);

				//120
				valorTotalProduto = totalizaFapAgrupadaPorTipoDeItem("PRODUTO", listaFapsAgrupadas);
				listaFapAgrupadasProduto = retornaListaFapAgrupadaPorTipoDeOrcamento("PRODUTO", listaFapsAgrupadas);

				List<NeoObject> lstCentroCustoProduto = new ArrayList<NeoObject>();
				List<NeoObject> lstCentroCustoProdutoObject = new ArrayList<NeoObject>();
				List<NeoObject> lstItensDeProduto = new ArrayList<NeoObject>();
				NeoObject oItensDeProduto = AdapterUtils.createNewEntityInstance("GLNItensDeProduto");
				EntityWrapper wrpItensDeProduto = new EntityWrapper(oItensDeProduto);
				InserirContaItensProdutoServico(processEntity, wrpItensDeProduto);

				if (valorTotalProduto.compareTo(new BigDecimal(0.00)) > 0)
				{
					lstCentroCustoProduto = verificaCentroCustoSapiens(((Long) processEntity.findValue("empresa.codemp")), listaFapsAgrupadas);
					
					lstCentroCustoProduto = (lstCentroCustoProduto.size() >= limite) ? listaCentroCusto : lstCentroCustoProduto;
					
					if (lstCentroCustoProduto != null && !lstCentroCustoProduto.isEmpty())
					{
						lstCentroCustoProdutoObject = insereCentroCustoFusionAgrupado(processEntity, ((Long) processEntity.findValue("empresa.codemp")), listaFapAgrupadasProduto, "P");
					}

					wrpItensDeProduto.setValue("unidadeMedida", lstUnidadeMedida.get(0));
					wrpItensDeProduto.setValue("quantidadeRecebida", 1L);
					wrpItensDeProduto.setValue("precoUnitarioProduto", valorTotalProduto);
					wrpItensDeProduto.setValue("listaCentroCusto", lstCentroCustoProdutoObject);

					lstItensDeProduto.add(oItensDeProduto);
					PersistEngine.persist(oItensDeProduto);

					wrpGestaLancamentoDeNotaProduto.setValue("listaItensDeProduto", lstItensDeProduto);
				}

				observacao = "";
				code = (String) processEntity.findValue("wfprocess.code");
				observacao = nomeAplicacao + " FAP Agrupadora: " + code;

				List<NeoObject> lstParcelasProduto = new ArrayList<NeoObject>();
				valorTotal = valorTotal.add(valorTotalProduto);
				lstParcelasProduto = insereParcelasFusion(valorTotal, observacao);
				wrpGestaLancamentoDeNotaProduto.setValue("valorLiquido", valorTotal);
				wrpGestaLancamentoDeNotaProduto.setValue("listaParcelas", lstParcelasProduto);
				wrpGestaLancamentoDeNotaProduto.setValue("observacao", observacao);

				processEntity.findField("GlnGestaoDeLancamentoDeNotaProdutoAgrupado").setValue(oGestaLancamentoDeNotaProduto);
				PersistEngine.persist(oGestaLancamentoDeNotaProduto);

				/* Gera Nota Fiscal via GLN para Serviço */
				InstantiableEntityInfo gestaLancamentoDeNotaServico = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNotaServico = gestaLancamentoDeNotaServico.createNewInstance();
				EntityWrapper wrpGestaLancamentoDeNotaServico = new EntityWrapper(oGestaLancamentoDeNotaServico);
				valorTotal = new BigDecimal(0.0);

				wrpGestaLancamentoDeNotaServico.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaLancamentoDeNotaServico.setValue("automatico", Boolean.TRUE);
				wrpGestaLancamentoDeNotaServico.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaLancamentoDeNotaServico.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaLancamentoDeNotaServico.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaLancamentoDeNotaServico.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaLancamentoDeNotaServico.setValue("formaPagamento", formaPagamento.get(0));
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaLancamentoDeNotaServico.setValue("usuarioResponsavel", solicitante.get(0));
				}

				List<NeoObject> listaFapAgrupadasServico = new ArrayList<NeoObject>();
				BigDecimal valorTotalServico = new BigDecimal(0.0);
				BigDecimal valorTotalDesconto = new BigDecimal(0.0);
				
				List<NeoObject> lstCentroCustoServico = new ArrayList<NeoObject>();
				List<NeoObject> lstCentroCustoServicoObject = new ArrayList<NeoObject>();
				List<NeoObject> lstItensDeServico = new ArrayList<NeoObject>();
				NeoObject oItensDeServico = AdapterUtils.createNewEntityInstance("GLNItensDeServico");
				EntityWrapper wrpItensDeServico = new EntityWrapper(oItensDeServico);
				InserirContaItensProdutoServico(processEntity, wrpItensDeServico);

				//45
				valorTotalDesconto = totalizaFapAgrupadaPorTipoDeItem("DESCONTO", listaFapsAgrupadas);
				valorTotalServico = totalizaFapAgrupadaPorTipoDeItem("SERVIÇO", listaFapsAgrupadas);
				valorTotalServico = valorTotalServico.subtract(valorTotalDesconto);
				
				listaFapAgrupadasServico = retornaListaFapAgrupadaPorTipoDeOrcamento("SERVIÇO", listaFapsAgrupadas);

				if (valorTotalServico.compareTo(new BigDecimal(0.00)) > 0)
				{
					lstCentroCustoServico = verificaCentroCustoSapiens(((Long) processEntity.findValue("empresa.codemp")), listaFapsAgrupadas);
					
					lstCentroCustoServico = (lstCentroCustoServico.size() >= limite) ? listaCentroCusto : lstCentroCustoServico;
					
					if (lstCentroCustoServico != null && !lstCentroCustoServico.isEmpty())
					{
						lstCentroCustoServicoObject = insereCentroCustoFusionAgrupado(processEntity, ((Long) processEntity.findValue("empresa.codemp")), listaFapAgrupadasServico, "S");
					}

					wrpItensDeServico.setValue("unidadeMedida", lstUnidadeMedida.get(0));
					wrpItensDeServico.setValue("quantidadeRecebida", 1L);
					wrpItensDeServico.setValue("precoUnitarioServico", valorTotalServico);
					wrpItensDeServico.setValue("listaCentroCusto", lstCentroCustoServicoObject);

					lstItensDeServico.add(oItensDeServico);
					PersistEngine.persist(oItensDeServico);

					wrpGestaLancamentoDeNotaServico.setValue("listaItensDeServico", lstItensDeServico);
				}

				observacao = "";
				code = (String) processEntity.findValue("wfprocess.code");
				observacao = nomeAplicacao + " FAP Agrupadora: " + code;

				List<NeoObject> lstParcelasServico = new ArrayList<NeoObject>();
				valorTotal = valorTotal.add(valorTotalServico);
				lstParcelasServico = insereParcelasFusion(valorTotal, observacao);
				wrpGestaLancamentoDeNotaServico.setValue("valorLiquido", valorTotal);
				wrpGestaLancamentoDeNotaServico.setValue("listaParcelas", lstParcelasServico);
				wrpGestaLancamentoDeNotaServico.setValue("observacao", observacao);

				processEntity.findField("GlnGestaoDeLancamentoDeNotaServicoAgrupado").setValue(oGestaLancamentoDeNotaServico);
				PersistEngine.persist(oGestaLancamentoDeNotaServico);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
	}

	/**
	 * Retorna um objeto do centro de custo utilizado para quando a FAP contém mais de 1000 centros de custo
	 * 
	 * @param processEntity
	 * @return Objeto correspondente ao centro de custo padrão para FAP com mais de 1000 ccu
	 */
	private NeoObject retornaCentroDeCustoPadrao(EntityWrapper processEntity) {
		
		NeoObject externoCentroCusto = null;
		
		try {
			
			String ccuPadrao = FAPParametrizacao.findParameter("centroDeCustoPadrao");
			Long empresa = (Long) processEntity.findValue("empresa.codemp");
			
			if(NeoUtils.safeIsNotNull(ccuPadrao) && NeoUtils.safeIsNotNull(empresa)) {
				
				QLGroupFilter filterCcu = new QLGroupFilter("AND");
				filterCcu.addFilter(new QLEqualsFilter("codccu", ccuPadrao));
				filterCcu.addFilter(new QLEqualsFilter("codemp", empresa));
				filterCcu.addFilter(new QLEqualsFilter("nivccu", Long.valueOf(8L)));
				
				externoCentroCusto = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu).get(0);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(this.getClass().getSimpleName() + " - Não foi possível capturar o centro de custo padrão disponível. Contate o Administrador do Sistema. ");
		}
		
		return externoCentroCusto;
	}

	public static BigDecimal verificaValorTotalDesconto(EntityWrapper processEntity) {
		
		List<NeoObject> itens = (List<NeoObject>) processEntity.findValue("itemOrcamento");
		
		BigDecimal valorDesconto = new BigDecimal(0.0);
		
		if (itens != null && !itens.isEmpty()) 	{
			for (NeoObject item : itens) {
				EntityWrapper wrpItem = new EntityWrapper(item);
				String tipoDoItem = (String) wrpItem.findValue("tipoItem.descricao");
				
				if(tipoDoItem.equalsIgnoreCase("DESCONTO")) {
					valorDesconto = valorDesconto.add((BigDecimal)wrpItem.findValue("valor"));
				}
			}
		}
		
		return valorDesconto;
	}
	
	public static List<NeoObject> retornaListaFapAgrupadaPorTipoDeOrcamento(String tipoItem, List<NeoObject> listaFapsAgrupadas)
	{
		List<NeoObject> lstFapAgrupadasPorTipoDeOrcamento = new ArrayList<NeoObject>();
		for (NeoObject oFap : listaFapsAgrupadas)
		{
			EntityWrapper wrpItem = new EntityWrapper(oFap);
			List<NeoObject> listaItens = (List<NeoObject>) wrpItem.findValue("itemOrcamento");

			for (NeoObject itensObject : listaItens)
			{
				EntityWrapper itensWrapper = new EntityWrapper(itensObject);

				if (itensWrapper.findValue("tipoItem.descricao") != null && ((String) itensWrapper.findValue("tipoItem.descricao")).equals(tipoItem))
				{
					lstFapAgrupadasPorTipoDeOrcamento.add(oFap);
					break;
				}
			}
		}
		return lstFapAgrupadasPorTipoDeOrcamento;
	}

	public static BigDecimal totalizaFapAgrupadaPorTipoDeItem(String tipoItem, List<NeoObject> listaFapsAgrupadas)
	{
		BigDecimal valorTotal = new BigDecimal(0.00);
		for (NeoObject oFap : listaFapsAgrupadas)
		{
			EntityWrapper wrpItem = new EntityWrapper(oFap);
			List<NeoObject> listaItens = (List<NeoObject>) wrpItem.findValue("itemOrcamento");

			for (NeoObject itensObject : listaItens)
			{
				EntityWrapper itensWrapper = new EntityWrapper(itensObject);

				if (itensWrapper.findValue("tipoItem.descricao") != null && ((String) itensWrapper.findValue("tipoItem.descricao")).equals(tipoItem))
				{
					valorTotal = valorTotal.add((BigDecimal) itensWrapper.findValue("valor"));
				}
			}
		}
		return valorTotal;
	}

	public static List<NeoObject> verificaCentroCustoSapiens(Long codigoEmpresa, List<NeoObject> listaFapsAgrupadas)
	{
		List<NeoObject> lstCentroCusto = new ArrayList<NeoObject>();
		List<NeoObject> externoCentroCusto = new ArrayList<NeoObject>();
		
		List<String> centrosDeCustoNaoEncontrados = new ArrayList<>();

		for (NeoObject oFap : listaFapsAgrupadas)
		{
			EntityWrapper wrpFap = new EntityWrapper(oFap);
			List<NeoObject> listaCentroCustoFap = (List<NeoObject>) wrpFap.findValue("listaCentroDeCusto");

			for (NeoObject oCentroCusto : listaCentroCustoFap)
			{
				EntityWrapper wrpFapCentroCusto = new EntityWrapper(oCentroCusto);

				if (wrpFapCentroCusto != null)
				{
					String codigoCentroCusto = (String) wrpFapCentroCusto.findValue("codigoCentroCusto");

					QLGroupFilter filterCcu = new QLGroupFilter("AND");
					filterCcu.addFilter(new QLEqualsFilter("codccu", codigoCentroCusto));
					filterCcu.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
					filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));
					externoCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

					if (externoCentroCusto == null || externoCentroCusto.isEmpty())
					{
						centrosDeCustoNaoEncontrados.add(codigoCentroCusto);
						lstCentroCusto.clear();
						continue;
					}
					
					lstCentroCusto.add((NeoObject)externoCentroCusto.get(0));	
				}
			}
		
		}
		
		if (!centrosDeCustoNaoEncontrados.isEmpty()) {
			mensagem = "Centro(s) de Custo(s) não localizado(s)!"
					+ "	Por favor, verificar o(s) Centro(s) de Custo(s) associado(s) ao FAP e se o(s) mesmo(s) existe na Empresa de lançamento da Nota!" + " - Centro(s) de custo(s): "
					+ StringUtils.join(centrosDeCustoNaoEncontrados, ",");
			throw new WorkflowException(mensagem);
		
		}

		return lstCentroCusto;
	}

	public static List<NeoObject> insereCentroCustoFusionAgrupado(EntityWrapper processEntity1, Long codigoEmpresa, List<NeoObject> listaFapsAgrupadas, String produtoServico)
	{
		List<NeoObject> listCentroCusto = new ArrayList<NeoObject>();

		Map<Long, NeoObject> ordenaCentroCusto = new HashMap<Long, NeoObject>();
		Map<Long, BigDecimal> somaValorCentro = new HashMap<Long, BigDecimal>();

		BigDecimal valorTotalAgrupado = new BigDecimal(0.00);
		BigDecimal valorTotalAgrupadoAux = new BigDecimal(0.00);
		BigDecimal valorDesconto = new BigDecimal(0.00);
		BigDecimal valorTotalDesconto = new BigDecimal(0.00);
		BigDecimal valorTotalDescontoAux = new BigDecimal(0.00);

		if (produtoServico == null)
		{
			for (NeoObject neoObject5 : listaFapsAgrupadas)
			{
				EntityWrapper registroWrapper = new EntityWrapper(neoObject5);
				
				valorTotalAgrupado = valorTotalAgrupado.add((BigDecimal) registroWrapper.findValue("valorTotal"));
				valorTotalAgrupadoAux = valorTotalAgrupado;
				
			}

			if ((Boolean) processEntity1.findValue("temDesconto"))
			{
				valorDesconto = (BigDecimal) processEntity1.findValue("valorDescontoFAP");
			}
			valorTotalAgrupado = valorTotalAgrupado.subtract(valorDesconto);
		}
		else
		{
			if (((BigDecimal) processEntity1.findValue("valorTotalProduto")).compareTo(new BigDecimal(0.00)) > 0 && produtoServico.equals("P"))
			{
				valorTotalAgrupado = valorTotalAgrupado.add((BigDecimal) processEntity1.findValue("valorTotalProduto"));
				if (processEntity1.findValue("tipoDesconto.codigoTipoDesconto") != null && (((Long) processEntity1.findValue("tipoDesconto.codigoTipoDesconto")) == 1L || ((Long) processEntity1.findValue("tipoDesconto.codigoTipoDesconto")) == 3L))
				{
					if ((Boolean) processEntity1.findValue("temDesconto"))
					{
						valorDesconto = (BigDecimal) processEntity1.findValue("valorDescontoFAP");
					}
				}
			}
			else
			{
				if (processEntity1.findValue("tipoDesconto.codigoTipoDesconto") != null && (((Long) processEntity1.findValue("tipoDesconto.codigoTipoDesconto")) == 2L || ((Long) processEntity1.findValue("tipoDesconto.codigoTipoDesconto")) == 3L))
				{
					if ((Boolean) processEntity1.findValue("temDesconto"))
					{
						valorDesconto = (BigDecimal) processEntity1.findValue("valorDescontoFAP");
					}
				}
				
				for (NeoObject noFaps : listaFapsAgrupadas) {
					
					EntityWrapper ewFaps = new EntityWrapper(noFaps);
					
					valorTotalDescontoAux = verificaValorTotalDesconto(ewFaps);
					valorTotalDesconto = valorTotalDesconto.add(valorTotalDescontoAux);
				}
				
				valorTotalAgrupado = valorTotalAgrupado.add((BigDecimal) processEntity1.findValue("valorTotalServicos"));
				valorTotalAgrupado = valorTotalAgrupado.subtract(valorTotalDesconto);
			}

			valorTotalAgrupadoAux = valorTotalAgrupado;
			valorTotalAgrupado = valorTotalAgrupado.subtract(valorDesconto);
		}

		BigDecimal valorTotalRateio = new BigDecimal(0.00);
		for (NeoObject neoObject : listaFapsAgrupadas)
		{
			EntityWrapper registroWrapper = new EntityWrapper(neoObject);
			BigDecimal valorTotal = new BigDecimal(0.00);
			valorTotal = (BigDecimal) registroWrapper.findValue("valorTotal");

			/*
			 * if (codigoTarefa != registroWrapper.findValue("wfprocess.code"))
			 * {
			 * if (tarefasAgrupadas != null && !tarefasAgrupadas.isEmpty())
			 * {
			 * tarefasAgrupadas += ", ";
			 * }
			 * tarefasAgrupadas = tarefasAgrupadas + registroWrapper.findValue("wfprocess.code");
			 * }
			 */

			BigDecimal valorItemRateado = new BigDecimal(0.00);
			if (produtoServico != null || valorDesconto.compareTo(new BigDecimal(0.00)) > 0)
			{
				List<NeoObject> listaItens = (List<NeoObject>) registroWrapper.findValue("itemOrcamento");
				for (NeoObject itensObject : listaItens)
				{
					EntityWrapper itensWrapper = new EntityWrapper(itensObject);

					BigDecimal valorItem = (BigDecimal) itensWrapper.findValue("valor");
					String tipoItem = (String) itensWrapper.findValue("tipoItem.descricao");
					String tipoLancamento = produtoServico;

					if (NeoUtils.safeIsNull(tipoLancamento) || tipoLancamento.equals("P"))
					{
						if ((tipoItem != null && tipoItem.equals("PRODUTO")))
						{
							valorItemRateado = valorItemRateado.add((BigDecimal) valorItem);
						}
					}

					if (NeoUtils.safeIsNull(tipoLancamento) || tipoLancamento.equals("S"))
					{
						if ((tipoItem != null && tipoItem.equals("SERVIÇO")))
						{
							valorItemRateado = valorItemRateado.add((BigDecimal) valorItem);
						} else if ((tipoItem != null && tipoItem.equals("DESCONTO"))) {
							valorItemRateado = valorItemRateado.subtract((BigDecimal) valorItem);
						}
					}
				}
			}

			List<NeoObject> listaCentroDeCusto = (List<NeoObject>) registroWrapper.findValue("listaCentroDeCusto");
			int qtdCentroCusto = listaCentroDeCusto.size();
			BigDecimal valorRateio = new BigDecimal(0.00);
			Boolean abrirTitulo = (Boolean) processEntity1.findValue("abrirTitulo");
			if (abrirTitulo)
			{
				if (valorDesconto.compareTo(new BigDecimal(0.00)) == 0)
				{
					valorRateio = valorItemRateado.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
				}
				else
				{
					BigDecimal percentualDesconto = new BigDecimal(0.00);
					BigDecimal percentualDesconto1 = new BigDecimal(0.00);
					BigDecimal percentualDescontoAtu = new BigDecimal(0.00);
					BigDecimal valorDescontoItem = new BigDecimal(0.00);
					percentualDesconto = (valorItemRateado.divide(valorTotalAgrupadoAux, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
					percentualDesconto1 = (valorItemRateado.divide(valorTotalAgrupado, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
					percentualDescontoAtu = percentualDesconto1.subtract(percentualDesconto);
					valorDescontoItem = (valorTotalAgrupado.multiply(percentualDescontoAtu)).divide(new BigDecimal(100.00), 4, RoundingMode.HALF_UP);
					valorRateio = valorItemRateado.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
					valorRateio = valorRateio.subtract(valorDescontoItem);
				}
			}
			else
			{
				if (valorDesconto.compareTo(new BigDecimal(0.00)) == 0)
				{
					valorRateio = valorTotal.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
				}
				else
				{
					BigDecimal percentualDesconto = new BigDecimal(0.00);
					BigDecimal percentualDesconto1 = new BigDecimal(0.00);
					BigDecimal percentualDescontoAtu = new BigDecimal(0.00);
					BigDecimal valorDescontoItem = new BigDecimal(0.00);
					percentualDesconto = (valorItemRateado.divide(valorTotalAgrupadoAux, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
					percentualDesconto1 = (valorItemRateado.divide(valorTotalAgrupado, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
					percentualDescontoAtu = percentualDesconto1.subtract(percentualDesconto);
					valorDescontoItem = (valorTotalAgrupado.multiply(percentualDescontoAtu)).divide(new BigDecimal(100.00), 4, RoundingMode.HALF_UP);
					valorRateio = valorItemRateado.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
					valorRateio = valorRateio.subtract(valorDescontoItem);
				}
			}

			List<NeoObject> lstCentroCusto = new ArrayList<NeoObject>();
			List<NeoObject> externoCentroCusto = new ArrayList<NeoObject>();
			for (NeoObject neoObject2 : listaCentroDeCusto)
			{
				EntityWrapper wrapper = new EntityWrapper(neoObject2);
				if (wrapper != null)
				{
					NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("GLNListaDeCentroDeCusto");
					EntityWrapper centralWrapper = new EntityWrapper(noCentroCusto);

					String codigoCentroCusto = (String) wrapper.findValue("codigoCentroCusto");

					QLGroupFilter filterCcu = new QLGroupFilter("AND");
					filterCcu.addFilter(new QLEqualsFilter("codccu", codigoCentroCusto));
					filterCcu.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
					filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));

					externoCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

					if (externoCentroCusto != null && !externoCentroCusto.isEmpty())
					{
						centralWrapper.setValue("codigoCentroCusto", externoCentroCusto.get(0));
						centralWrapper.setValue("valorRateio", valorRateio);

						valorTotalRateio = valorTotalRateio.add(valorRateio);
						valorTotalRateio.setScale(4, RoundingMode.HALF_UP);

						if (somaValorCentro != null && !somaValorCentro.containsKey(Long.parseLong(codigoCentroCusto)))
						{
							somaValorCentro.put(Long.parseLong(codigoCentroCusto), valorRateio);
						}
						else if (somaValorCentro != null && somaValorCentro.containsKey(Long.parseLong(codigoCentroCusto)))
						{
							BigDecimal valorRateioAux = new BigDecimal(0.00);
							valorRateioAux = somaValorCentro.get(Long.parseLong(codigoCentroCusto));
							valorRateioAux = valorRateioAux.add(valorRateio);
							somaValorCentro.put(Long.parseLong(codigoCentroCusto), valorRateioAux);
						}

						if (ordenaCentroCusto != null && !ordenaCentroCusto.containsKey(codigoCentroCusto))
						{
							ordenaCentroCusto.put(Long.parseLong(codigoCentroCusto), noCentroCusto);
						}
					}

					lstCentroCusto.add(noCentroCusto);
					PersistEngine.persist(noCentroCusto);
				}
			}
		}

		Set ordenaCCU = ordenaCentroCusto.entrySet();
		Iterator ordenaCcuIterator = ordenaCCU.iterator();
		while (ordenaCcuIterator.hasNext())
		{
			Map.Entry me = (Map.Entry) ordenaCcuIterator.next();
			EntityWrapper centroCusto = new EntityWrapper((NeoObject) me.getValue());
			centroCusto.findField("valorRateio").setValue(somaValorCentro.get(me.getKey()));

			listCentroCusto.add((NeoObject) me.getValue());
		}

		int resultado = valorTotalAgrupado.compareTo(valorTotalRateio);
		BigDecimal valorSobra = new BigDecimal(0.00);
		valorTotalAgrupado.plus(new MathContext(2, RoundingMode.HALF_UP));
		valorSobra = valorTotalRateio.subtract(valorTotalAgrupado);

		BigDecimal valorAjustado = new BigDecimal(0.00);
		BigDecimal valorRateio = new BigDecimal(0.00);

		NeoObject oCentroCusto = listCentroCusto.get(listCentroCusto.size() - 1);
		EntityWrapper wrapperCentroCusto = new EntityWrapper(oCentroCusto);
		valorRateio = (BigDecimal) wrapperCentroCusto.findValue("valorRateio");

		if (resultado == 1)
		{
			BigDecimal retorno = valorRateio.subtract(valorSobra);
			valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
			wrapperCentroCusto.setValue("valorRateio", valorAjustado);
		}
		else if (resultado == -1)
		{
			BigDecimal retorno = valorRateio.subtract(valorSobra);
			valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
			wrapperCentroCusto.setValue("valorRateio", valorAjustado);
		}

		return listCentroCusto;
	}

	public static List<NeoObject> insereParcelasFusion(BigDecimal valorTotal, String observacao)
	{
		List<NeoObject> lstParcelas = new ArrayList<NeoObject>();

		NeoObject oParcelas = AdapterUtils.createNewEntityInstance("GLNParcelas");
		EntityWrapper wrpParcelas = new EntityWrapper(oParcelas);

		wrpParcelas.setValue("titulo", "1");
		wrpParcelas.setValue("vencimentoParcela", new GregorianCalendar());
		wrpParcelas.setValue("valorParcela", valorTotal);
		wrpParcelas.setValue("observacaoParcela", observacao);

		lstParcelas.add(oParcelas);
		PersistEngine.persist(oParcelas);

		return lstParcelas;
	}

	public static void InserirContaItensProdutoServico(EntityWrapper processEntity, EntityWrapper wrpItensDeProdutoServico)
	{

		String mensagem;
		Long codigoTipoAplicacao = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
		Long codigoEmpresa = (Long) processEntity.findValue("empresa.codemp");

		if (codigoTipoAplicacao == 1)
		{
			// seta conta contabil  para FAP de Frota
			QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
			filtroContaContabil.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
			filtroContaContabil.addFilter(new QLEqualsFilter("ctared", ((Long) 4770l)));
			List<NeoObject> contaContabil = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
			if (contaContabil != null && contaContabil.size() > 0)
			{
				wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
			}
			else
			{
				mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			// seta conta financeira para FAP de Frota
			QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
			filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
			filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", ((Long) 79l)));
			List<NeoObject> contaFinanceira = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
			if (contaFinanceira != null && contaFinanceira.size() > 0)
			{
				wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
			}
			else
			{
				mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

		}
		else
		{
			if ((codigoTipoAplicacao == 2) || (codigoTipoAplicacao == 3) || (codigoTipoAplicacao == 5) || 
				(codigoTipoAplicacao == 6) || (codigoTipoAplicacao == 8) || (codigoTipoAplicacao == 9))
			{
				// seta conta contabil  para FAP de CFTV
				QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
				filtroContaContabil.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
				filtroContaContabil.addFilter(new QLEqualsFilter("ctared", ((Long) 4690l)));
				List<NeoObject> contaContabil = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
				if (contaContabil != null && contaContabil.size() > 0)
				{
					wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

				// seta conta financeira para FAP de CFTV
				QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
				filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
				filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", ((Long) 377l)));
				List<NeoObject> contaFinanceira = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
				if (contaFinanceira != null && contaFinanceira.size() > 0)
				{
					wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

			}
			else
			{

				// seta conta contabil  para FAP de Rastreamento
				QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
				filtroContaContabil.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
				filtroContaContabil.addFilter(new QLEqualsFilter("ctared", ((Long) 38895l)));
				List<NeoObject> contaContabil = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
				if (contaContabil != null && contaContabil.size() > 0)
				{
					wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

				// seta conta financeira para FAP de Rastreamento
				QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
				filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", ((Long) codigoEmpresa)));
				filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", ((Long) 707l)));
				List<NeoObject> contaFinanceira = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
				if (contaFinanceira != null && contaFinanceira.size() > 0)
				{
					wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
	}
}
