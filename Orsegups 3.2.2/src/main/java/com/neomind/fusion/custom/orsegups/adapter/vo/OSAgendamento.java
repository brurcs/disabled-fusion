package com.neomind.fusion.custom.orsegups.adapter.vo;

public class OSAgendamento {

	private Long numeroOs;
	private String regionalTecnico;
	
	public Long getNumeroOs() {
		return numeroOs;
	}
	
	public void setNumeroOs(Long numeroOs) {
		this.numeroOs = numeroOs;
	}

	public String getRegionalTecnico() {
		return regionalTecnico;
	}

	public void setRegionalTecnico(String regionalTecnico) {
		this.regionalTecnico = regionalTecnico;
	}
	
	
}
