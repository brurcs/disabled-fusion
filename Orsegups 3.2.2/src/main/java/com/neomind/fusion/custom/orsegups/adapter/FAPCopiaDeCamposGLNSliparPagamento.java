package com.neomind.fusion.custom.orsegups.adapter;

import com.mysql.jdbc.log.Log;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTaticoDAO;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTecnicoDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.ItemOrcamentoDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.TecnicoDTO;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class FAPCopiaDeCamposGLNSliparPagamento
implements AdapterInterface
{
	public static String mensagem = "";
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		try
		{
			List<NeoObject> listaCentroCusto = new ArrayList<>();
			listaCentroCusto.add(retornaCentroDeCustoPadrao(processEntity)); 
			Long limite = Long.parseLong(FAPParametrizacao.findParameter("limiteCentroDeCusto"));
			
			Boolean abrirTitulo = (Boolean)processEntity.findValue("abrirTitulo");
			Long tipoLancamento = (Long)processEntity.findValue("lancarNotaOuTitulo.codigoLancamento");
			String usuarioSolicitante = (String)processEntity.findValue("solicitanteOrcamento.code");
			
			BigDecimal valorTotalDesconto = new BigDecimal(0.0);
			valorTotalDesconto = verificaValorTotalDesconto(processEntity);

			QLGroupFilter filtroCondicaoPagamento = new QLGroupFilter("AND");
			filtroCondicaoPagamento.addFilter(new QLEqualsFilter("codemp", (Long)processEntity.findValue("empresa.codemp")));
			filtroCondicaoPagamento.addFilter(new QLEqualsFilter("codcpg", "AV"));
			List condicaoPagamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("CondicaoPagamento"), filtroCondicaoPagamento);
			if ((condicaoPagamento != null) && (condicaoPagamento.size() > 0))
			{
				condicaoPagamento.get(0);
			}
			else
			{
				mensagem = "Não encontrado o Código da Condição de Pagamentos para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLEqualsFilter fitroUnidadeMedida = new QLEqualsFilter("unimed", "UN");
			List lstUnidadeMedida = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE015MED"), fitroUnidadeMedida);
			if ((lstUnidadeMedida != null) && (lstUnidadeMedida.size() > 0))
			{
				lstUnidadeMedida.get(0);
			}
			else
			{
				mensagem = "Não encontrado a Unidade de Medida para lançamento da Nota Fiscal de Entrada!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroFormaPagamento = new QLGroupFilter("AND");
			filtroFormaPagamento.addFilter(new QLEqualsFilter("codemp", (Long)processEntity.findValue("empresa.codemp")));
			filtroFormaPagamento.addFilter(new QLEqualsFilter("codfpg", (Long)processEntity.findValue("formaPagamento.codigoFormaPagamento")));
			List formaPagamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE066FPG"), filtroFormaPagamento);
			if ((formaPagamento != null) && (formaPagamento.size() > 0))
			{
				formaPagamento.get(0);
			}
			else
			{
				mensagem = "Não encontrado o Código da Forma de Pagamento para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroUsuarioSolicitante = new QLGroupFilter("AND");
			filtroUsuarioSolicitante.addFilter(new QLEqualsFilter("nomusu", usuarioSolicitante));
			List solicitante = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), filtroUsuarioSolicitante);
			if ((solicitante != null) && (solicitante.size() > 0))
			{
				solicitante.get(0);
			}
			else
			{
				mensagem = "Não encontrado o Código do usuário Solicitante informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			if ((!abrirTitulo.booleanValue()) && (tipoLancamento.longValue() == 1L))
			{
				InstantiableEntityInfo gestaLancamentoDeNota = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNota = gestaLancamentoDeNota.createNewInstance();
				EntityWrapper wrpGestaLancamentoDeNota = new EntityWrapper(oGestaLancamentoDeNota);

				wrpGestaLancamentoDeNota.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaLancamentoDeNota.setValue("automatico", Boolean.TRUE);
				wrpGestaLancamentoDeNota.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaLancamentoDeNota.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaLancamentoDeNota.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaLancamentoDeNota.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaLancamentoDeNota.setValue("formaPagamento", formaPagamento.get(0));
				wrpGestaLancamentoDeNota.setValue("valorLiquido", processEntity.findValue("valorTotal"));
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaLancamentoDeNota.setValue("usuarioResponsavel", solicitante.get(0));
				}

				BigDecimal valorTotal = new BigDecimal(0.0D);
				BigDecimal valorTotalProduto = new BigDecimal(0.0D);
				BigDecimal valorTotalServico = new BigDecimal(0.0D);

				List lstCentroCustoProduto = new ArrayList();
				List lstCentroCustoProdutoObject = new ArrayList();
				List lstItensDeProduto = new ArrayList();
				NeoObject oItensDeProduto = AdapterUtils.createNewEntityInstance("GLNItensDeProduto");
				EntityWrapper wrpItensDeProduto = new EntityWrapper(oItensDeProduto);
				InserirContaItensProdutoServico(processEntity, wrpItensDeProduto);

				List lstItensFapCentroCusto = (List)processEntity.findValue("listaCentroDeCusto");
				List lstItensFapProduto = (List)processEntity.findValue("itemOrcamento");

				if ((lstItensFapProduto != null) && (!lstItensFapProduto.isEmpty()))
				{
					valorTotalProduto = totalizaItemFapPorTipo("PRODUTO", lstItensFapProduto);

					if (valorTotalProduto.compareTo(new BigDecimal(0.0D)) > 0)
					{
						if ((lstItensFapCentroCusto != null) && (!lstItensFapCentroCusto.isEmpty()))
						{
							
							lstCentroCustoProduto = (lstItensFapCentroCusto.size() >= limite) ? listaCentroCusto : verificaCentroCustoSapiens((Long)processEntity.findValue("empresa.codemp"), lstItensFapCentroCusto);;
							
							if ((lstCentroCustoProduto != null) && (!lstCentroCustoProduto.isEmpty()))
							{
								lstCentroCustoProdutoObject = insereCentroCustoFusion((Long)processEntity.findValue("empresa.codemp"), lstCentroCustoProduto, valorTotalProduto);
							}
						}

						wrpItensDeProduto.setValue("unidadeMedida", lstUnidadeMedida.get(0));
						wrpItensDeProduto.setValue("quantidadeRecebida", Long.valueOf(1L));
						wrpItensDeProduto.setValue("precoUnitarioProduto", valorTotalProduto);
						wrpItensDeProduto.setValue("listaCentroCusto", lstCentroCustoProdutoObject);

						lstItensDeProduto.add(oItensDeProduto);
						PersistEngine.persist(oItensDeProduto);

						wrpGestaLancamentoDeNota.setValue("listaItensDeProduto", lstItensDeProduto);
					}
				}

				List lstCentroCustoServico = new ArrayList();
				List lstCentroCustoServicoObject = new ArrayList();
				List lstItensDeServico = new ArrayList();
				NeoObject oItensDeServico = AdapterUtils.createNewEntityInstance("GLNItensDeServico");
				EntityWrapper wrpItensDeServico = new EntityWrapper(oItensDeServico);
				InserirContaItensProdutoServico(processEntity, wrpItensDeServico);

				List lstItensFapServico = (List)processEntity.findValue("itemOrcamento");
				if ((lstItensFapServico != null) && (!lstItensFapServico.isEmpty()))
				{
					valorTotalServico = totalizaItemFapPorTipo("SERVIÇO", lstItensFapProduto);
					valorTotalServico = valorTotalServico.subtract(valorTotalDesconto);
					if (valorTotalServico.compareTo(new BigDecimal(0.0D)) > 0)
					{
						if ((lstItensFapCentroCusto != null) && (!lstItensFapCentroCusto.isEmpty()))
						{
							
							lstCentroCustoServico = verificaCentroCustoSapiens((Long)processEntity.findValue("empresa.codemp"), lstItensFapCentroCusto);
							
							lstCentroCustoServico = (lstItensFapCentroCusto.size() >= limite) ? listaCentroCusto : verificaCentroCustoSapiens((Long)processEntity.findValue("empresa.codemp"), lstItensFapCentroCusto);

							if ((lstCentroCustoServico != null) && (!lstCentroCustoServico.isEmpty()))
							{
								lstCentroCustoServicoObject = insereCentroCustoFusion((Long)processEntity.findValue("empresa.codemp"), lstCentroCustoServico, valorTotalServico);
							}
						}

						wrpItensDeServico.setValue("unidadeMedida", lstUnidadeMedida.get(0));
						wrpItensDeServico.setValue("quantidadeRecebida", Long.valueOf(1L));
						wrpItensDeServico.setValue("precoUnitarioServico", valorTotalServico);
						wrpItensDeServico.setValue("listaCentroCusto", lstCentroCustoServicoObject);

						lstItensDeServico.add(oItensDeServico);
						PersistEngine.persist(oItensDeServico);

						wrpGestaLancamentoDeNota.setValue("listaItensDeServico", lstItensDeServico);
					}
				}

				String observacao = "";
				Long aplicacaoPagamento = (Long)processEntity.findValue("aplicacaoPagamento.codigo");
				String nomeAplicacao = (String)processEntity.findValue("aplicacaoPagamento.nome");
				String code = (String)processEntity.findValue("wfprocess.code");

				if (aplicacaoPagamento.longValue() == 1L)
				{
					String viaturaPlaca = (String)processEntity.findValue("viatura.placa");
					String tipoViatura = (String)processEntity.findValue("tipoViatura");
					String codigoRegional = (String)processEntity.findValue("codigoRegional");
					String anoViatura = (String)processEntity.findValue("anoViatura");

					if (anoViatura != null)
					{
						anoViatura = (String)processEntity.findValue("anoViatura");
					}
					else
					{
						anoViatura = "Ano não informado";
					}

					observacao = nomeAplicacao + " - " + viaturaPlaca + " - " + tipoViatura + " - " + anoViatura + " - Regional: " + codigoRegional + " - FAP: " + code;
				}
				else if ((aplicacaoPagamento.longValue() == 2L) || (aplicacaoPagamento.longValue() == 3L) || (aplicacaoPagamento.longValue() == 5L) || 
						(aplicacaoPagamento.longValue() == 6L) || (aplicacaoPagamento.longValue() == 8L) || (aplicacaoPagamento.longValue() == 9L))
				{
					String codigoConta = (String)processEntity.findValue("codigoConta");
					String razaoSocialConta = (String)processEntity.findValue("razaoSocialConta");
					observacao = nomeAplicacao + " - " + codigoConta + " - " + razaoSocialConta + " - FAP: " + code;
				} else if (aplicacaoPagamento.longValue() == 4L || aplicacaoPagamento.longValue() == 7L) {
					String codigoConta = (String)processEntity.findValue("codigoConta");
					String razaoSocialConta = (String)processEntity.findValue("razaoSocialConta");
					observacao = nomeAplicacao + " - " + codigoConta + " - " + razaoSocialConta + " - FAP: " + code;
				} else if(aplicacaoPagamento.longValue() == 10L) {
					String codigo = (String)processEntity.findValue("wfprocess.code");
					GregorianCalendar dataCompetencia = processEntity.findGenericValue("competenciaPagamento");
					String competencia = NeoDateUtils.safeDateFormat(dataCompetencia, "MM/yyyy");			
					String nomeTecnico = processEntity.findGenericValue("tecnicoSigma.tecnico.nm_colaborador");
					String descricaoModalidade = (String)processEntity.findValue("tecnicoSigma.modalidadePag.descricao");					

		            BigDecimal valorTotalItens = new BigDecimal(0.00);
		            Long qtdContaCFTV = 0L;
		            Long qtdContaAlarme = 0L;

		            List<ItemOrcamentoDTO> listaItensOrcamento = AplicacaoTaticoDAO.consultaItemOrcamento(codigo, aplicacaoPagamento.longValue());
					for (ItemOrcamentoDTO itemOrcamento : listaItensOrcamento){		
						valorTotalItens = valorTotalItens.add(itemOrcamento.getValor());
					}

					List<NeoObject> historicoContasNeo = processEntity.findGenericValue("historicoContas");
					
					for(NeoObject conta : historicoContasNeo) {
						EntityWrapper ewConta = new EntityWrapper(conta);
						String tipoConta = ewConta.findGenericValue("tipoConta");						
						if(("CFTV").equals(tipoConta)) {
							qtdContaCFTV++;
						} else {
							qtdContaAlarme++;
						}
						
					}
					
					observacao = "Fap Técnico: " + codigo + " Comp.: " + competencia + ". Técnico: " + nomeTecnico + ". Pagamento " + descricaoModalidade
							+ ". Valor: R$" + valorTotalItens + ". Contas CFTV: " + qtdContaCFTV + " Contas Alarme: " + qtdContaAlarme;	
		            					
			    } else if (aplicacaoPagamento.longValue() == 11L) {
					String nomeRota = processEntity.findField("rotaSigma.nm_rota").getValueAsString();
					BigDecimal valor = (BigDecimal)processEntity.findValue("rotaSigma.valorRota");			
					String codigo = (String)processEntity.findValue("wfprocess.code");
					String descricaoModalidade = (String)processEntity.findValue("rotaSigma.modalidade.descricao");	
					GregorianCalendar dataCompetencia = processEntity.findGenericValue("competenciaPagamento");
					String competencia = NeoDateUtils.safeDateFormat(dataCompetencia, "MM/yyyy");
					String descricaoQuantidade = "";					
					Long codigoModalidade = (Long)processEntity.findValue("rotaSigma.modalidade.codigo");
					List<NeoObject> listaDeContas = processEntity.findGenericValue("historicoContas");
					List<NeoObject> listaEventos = processEntity.findGenericValue("historicoEventos");
					
					Long quantidadeContas = 0L;
					Long quantidadeEventos = 0L;

					if(codigoModalidade == 3L){
						quantidadeEventos = new Long(listaEventos.size());
						descricaoQuantidade = "Eventos: " + quantidadeEventos.toString();
					} else {
						quantidadeContas = new Long(listaDeContas.size());
						descricaoQuantidade = "Contas: " + quantidadeContas.toString();
					}

					observacao = "FAP Tático: " + codigo + " Comp.: " + competencia + ". Rota " + nomeRota + ". Pagamento " + descricaoModalidade 
							+ " . Valor: " + valor.toString() + ". " + descricaoQuantidade;


				}
				else if(aplicacaoPagamento == 12L){
					String codigoProduto = processEntity.findGenericValue("patrimonio.codigo");
					String nomeproduto = processEntity.findGenericValue("patrimonio.produto.despro");

					observacao = nomeAplicacao + " " + codigoProduto + " - " + nomeproduto + " - FAP: " + code;
				}

				List lstParcelas = new ArrayList();
				valorTotal = valorTotal.add(valorTotalProduto).add(valorTotalServico);
				lstParcelas = insereParcelasFusion(valorTotal, observacao);
				wrpGestaLancamentoDeNota.setValue("listaParcelas", lstParcelas);
				wrpGestaLancamentoDeNota.setValue("observacao", observacao);

				processEntity.findField("GlnGestaoDeLancamentoDeNota").setValue(oGestaLancamentoDeNota);
				PersistEngine.persist(oGestaLancamentoDeNota);
			}
			else if ((abrirTitulo.booleanValue()) && (tipoLancamento.longValue() == 1L))
			{
				String observacao = "";

				Long aplicacaoPagamento = (Long)processEntity.findValue("aplicacaoPagamento.codigo");
				String nomeAplicacao = (String)processEntity.findValue("aplicacaoPagamento.nome");
				String code = (String)processEntity.findValue("wfprocess.code");

				if (aplicacaoPagamento.longValue() == 1L)
				{
					String viaturaPlaca = (String)processEntity.findValue("viatura.placa");
					String tipoViatura = (String)processEntity.findValue("tipoViatura");
					String codigoRegional = (String)processEntity.findValue("codigoRegional");
					String anoViatura = (String)processEntity.findValue("anoViatura");

					if (anoViatura != null)
					{
						anoViatura = (String)processEntity.findValue("anoViatura");
					}
					else
					{
						anoViatura = "Ano não informado";
					}

					observacao = nomeAplicacao + " - " + viaturaPlaca + " - " + tipoViatura + " - " + anoViatura + " - Regional: " + codigoRegional + " - FAP: " + code;
				}
				else if ((aplicacaoPagamento.longValue() == 2L) || (aplicacaoPagamento.longValue() == 3L) || (aplicacaoPagamento.longValue() == 5L) || 
						(aplicacaoPagamento.longValue() == 6L) || (aplicacaoPagamento.longValue() == 8L) || (aplicacaoPagamento.longValue() == 9L))
				{
					String codigoConta = (String)processEntity.findValue("codigoConta");
					String razaoSocialConta = (String)processEntity.findValue("razaoSocialConta");
					observacao = nomeAplicacao + " - " + codigoConta + " - " + razaoSocialConta + " - FAP: " + code;
				} else if (aplicacaoPagamento.longValue() == 4L) {
					observacao = "Aplicação: " + aplicacaoPagamento + " - " + nomeAplicacao + " - FAP: " + code;
				} else if (aplicacaoPagamento.longValue() == 11L) {
					String nomeRota = processEntity.findField("rotaSigma.nm_rota").getValueAsString();
					BigDecimal valor = (BigDecimal)processEntity.findValue("rotaSigma.valorRota");			
					String codigo = (String)processEntity.findValue("wfprocess.code");
					String descricaoModalidade = (String)processEntity.findValue("rotaSigma.modalidade.descricao");	
					GregorianCalendar dataCompetencia = processEntity.findGenericValue("competenciaPagamento");
					String competencia = NeoDateUtils.safeDateFormat(dataCompetencia, "MM/yyyy");
					String descricaoQuantidade = "";					
					Long codigoModalidade = (Long)processEntity.findValue("rotaSigma.modalidade.codigo");
					List<NeoObject> listaDeContas = processEntity.findGenericValue("historicoContas");
					List<NeoObject> listaEventos = processEntity.findGenericValue("historicoEventos");
					
					Long quantidadeContas = 0L;
					Long quantidadeEventos = 0L;

					if(codigoModalidade == 3L){
						quantidadeEventos = new Long(listaEventos.size());
						descricaoQuantidade = "Eventos: " + quantidadeEventos.toString();
					} else {
						quantidadeContas = new Long(listaDeContas.size());
						descricaoQuantidade = "Contas: " + quantidadeContas.toString();
					}

					observacao = "FAP Tático." + codigo + "Comp.: " + competencia + ". Rota " + nomeRota + ". Pagamento " + descricaoModalidade 
							+ " . Valor: " + valor.toString() + ". " + descricaoQuantidade;
				}

				InstantiableEntityInfo gestaLancamentoDeNotaProduto = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNotaProduto = gestaLancamentoDeNotaProduto.createNewInstance();
				EntityWrapper wrpGestaoLancamentoDeNotaProduto = new EntityWrapper(oGestaLancamentoDeNotaProduto);

				wrpGestaoLancamentoDeNotaProduto.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaoLancamentoDeNotaProduto.setValue("automatico", Boolean.TRUE);
				wrpGestaoLancamentoDeNotaProduto.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaoLancamentoDeNotaProduto.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaoLancamentoDeNotaProduto.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaoLancamentoDeNotaProduto.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaoLancamentoDeNotaProduto.setValue("formaPagamento", formaPagamento.get(0));
				wrpGestaoLancamentoDeNotaProduto.setValue("valorLiquido", processEntity.findValue("valorTotalProduto"));
				wrpGestaoLancamentoDeNotaProduto.setValue("produtoServico", "P");
				wrpGestaoLancamentoDeNotaProduto.setValue("observacao", observacao);
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaoLancamentoDeNotaProduto.setValue("usuarioResponsavel", solicitante.get(0));
				}

				BigDecimal valorTotalProduto = new BigDecimal(0.0D);
				BigDecimal valorTotalServico = new BigDecimal(0.0D);

				List lstCentroCustoProduto = new ArrayList();
				List lstCentroCustoProdutoObject = new ArrayList();
				List lstItensDeProduto = new ArrayList();
				NeoObject oItensDeProduto = AdapterUtils.createNewEntityInstance("GLNItensDeProduto");
				EntityWrapper wrpItensDeProduto = new EntityWrapper(oItensDeProduto);
				InserirContaItensProdutoServico(processEntity, wrpItensDeProduto);

				List lstItensFapCentroCusto = (List)processEntity.findValue("listaCentroDeCusto");
				List lstItensFapProduto = (List)processEntity.findValue("itemOrcamento");

				if ((lstItensFapProduto != null) && (!lstItensFapProduto.isEmpty()))
				{
					valorTotalProduto = (BigDecimal)processEntity.findValue("valorTotalProduto");

					if (valorTotalProduto.compareTo(new BigDecimal(0.0D)) > 0)
					{
						if ((lstItensFapCentroCusto != null) && (!lstItensFapCentroCusto.isEmpty()))
						{
							
							lstCentroCustoProduto = (lstItensFapCentroCusto.size() >= limite) ? listaCentroCusto : verificaCentroCustoSapiens((Long)processEntity.findValue("empresa.codemp"), lstItensFapCentroCusto);
							
							if ((lstCentroCustoProduto != null) && (!lstCentroCustoProduto.isEmpty()))
							{
								lstCentroCustoProdutoObject = insereCentroCustoFusion((Long)processEntity.findValue("empresa.codemp"), lstCentroCustoProduto, valorTotalProduto);
							}
						}

						wrpItensDeProduto.setValue("unidadeMedida", lstUnidadeMedida.get(0));
						wrpItensDeProduto.setValue("quantidadeRecebida", Long.valueOf(1L));
						wrpItensDeProduto.setValue("precoUnitarioProduto", valorTotalProduto);
						wrpItensDeProduto.setValue("listaCentroCusto", lstCentroCustoProdutoObject);

						lstItensDeProduto.add(oItensDeProduto);
						PersistEngine.persist(oItensDeProduto);

						wrpGestaoLancamentoDeNotaProduto.setValue("listaItensDeProduto", lstItensDeProduto);
					}
				}

				List lstParcelasProduto = new ArrayList();
				lstParcelasProduto = insereParcelasFusion(valorTotalProduto, observacao);
				wrpGestaoLancamentoDeNotaProduto.setValue("observacao", observacao);
				wrpGestaoLancamentoDeNotaProduto.setValue("listaParcelas", lstParcelasProduto);

				processEntity.findField("GlnGestaoDeLancamentoDeNotaProduto").setValue(oGestaLancamentoDeNotaProduto);
				PersistEngine.persist(oGestaLancamentoDeNotaProduto);

				InstantiableEntityInfo gestaLancamentoDeNotaServico = AdapterUtils.getInstantiableEntityInfo("GLNGestaoDeLancamentoDeNota");
				NeoObject oGestaLancamentoDeNotaServico = gestaLancamentoDeNotaServico.createNewInstance();
				EntityWrapper wrpGestaoLancamentoDeNotaServico = new EntityWrapper(oGestaLancamentoDeNotaServico);

				wrpGestaoLancamentoDeNotaServico.setValue("FapAutorizacaoDePagamento", processEntity.getObject());
				wrpGestaoLancamentoDeNotaServico.setValue("automatico", Boolean.TRUE);
				wrpGestaoLancamentoDeNotaServico.setValue("empresa", processEntity.findValue("empresa"));
				wrpGestaoLancamentoDeNotaServico.setValue("responsavelNota", PortalUtil.getCurrentUser());
				wrpGestaoLancamentoDeNotaServico.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
				wrpGestaoLancamentoDeNotaServico.setValue("condicaoPagamento", condicaoPagamento.get(0));
				wrpGestaoLancamentoDeNotaServico.setValue("formaPagamento", formaPagamento.get(0));
				wrpGestaoLancamentoDeNotaServico.setValue("valorLiquido", processEntity.findValue("valorTotalServicos"));
				wrpGestaoLancamentoDeNotaServico.setValue("produtoServico", "S");
				if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
				{
					wrpGestaoLancamentoDeNotaServico.setValue("usuarioResponsavel", solicitante.get(0));
				}

				List lstCentroCustoServico = new ArrayList();
				List lstCentroCustoServicoObject = new ArrayList();
				List lstItensDeServico = new ArrayList();
				NeoObject oItensDeServico = AdapterUtils.createNewEntityInstance("GLNItensDeServico");
				EntityWrapper wrpItensDeServico = new EntityWrapper(oItensDeServico);
				InserirContaItensProdutoServico(processEntity, wrpItensDeServico);

				List lstItensFapServico = (List)processEntity.findValue("itemOrcamento");
				if ((lstItensFapServico != null) && (!lstItensFapServico.isEmpty()))
				{
					valorTotalServico = (BigDecimal)processEntity.findValue("valorTotalServicos");
					valorTotalServico = valorTotalServico.subtract(valorTotalDesconto);
					
					if (valorTotalServico.compareTo(new BigDecimal(0.0D)) > 0)
					{
						if ((lstItensFapCentroCusto != null) && (!lstItensFapCentroCusto.isEmpty()))
						{
							
							lstCentroCustoServico = (lstItensFapCentroCusto.size() >= limite) ? listaCentroCusto : verificaCentroCustoSapiens((Long)processEntity.findValue("empresa.codemp"), lstItensFapCentroCusto);
							
							if ((lstCentroCustoServico != null) && (!lstCentroCustoServico.isEmpty()))
							{
								lstCentroCustoServicoObject = insereCentroCustoFusion((Long)processEntity.findValue("empresa.codemp"), lstCentroCustoServico, valorTotalServico);
							}
						}

						wrpItensDeServico.setValue("unidadeMedida", lstUnidadeMedida.get(0));
						wrpItensDeServico.setValue("quantidadeRecebida", Long.valueOf(1L));
						wrpItensDeServico.setValue("precoUnitarioServico", valorTotalServico);
						wrpItensDeServico.setValue("listaCentroCusto", lstCentroCustoServicoObject);

						lstItensDeServico.add(oItensDeServico);
						PersistEngine.persist(oItensDeServico);

						wrpGestaoLancamentoDeNotaServico.setValue("listaItensDeServico", lstItensDeServico);
					}
				}

				List lstParcelasServico = new ArrayList();
				lstParcelasServico = insereParcelasFusion(valorTotalServico, observacao);
				wrpGestaoLancamentoDeNotaServico.setValue("observacao", observacao);
				wrpGestaoLancamentoDeNotaServico.setValue("listaParcelas", lstParcelasServico);

				processEntity.findField("GlnGestaoDeLancamentoDeNotaServico").setValue(oGestaLancamentoDeNotaServico);
				PersistEngine.persist(oGestaLancamentoDeNotaServico);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
	}

	/**
	 * Retorna um objeto do centro de custo utilizado para quando a FAP contém mais de 1000 centros de custo
	 * 
	 * @param processEntity
	 * @return Objeto correspondente ao centro de custo padrão para FAP com mais de 1000 ccu
	 */
	private NeoObject retornaCentroDeCustoPadrao(EntityWrapper processEntity) {
		
		NeoObject externoCentroCusto = null;
		
		try {
			
			String ccuPadrao = FAPParametrizacao.findParameter("centroDeCustoPadrao");
			Long empresa = (Long) processEntity.findValue("empresa.codemp");
			
			if(NeoUtils.safeIsNotNull(ccuPadrao) && NeoUtils.safeIsNotNull(empresa)) {
				
				QLGroupFilter filterCcu = new QLGroupFilter("AND");
				filterCcu.addFilter(new QLEqualsFilter("codccu", ccuPadrao));
				filterCcu.addFilter(new QLEqualsFilter("codemp", empresa));
				filterCcu.addFilter(new QLEqualsFilter("nivccu", Long.valueOf(8L)));
				
				externoCentroCusto = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu).get(0);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(this.getClass().getSimpleName() + " - Não foi possível capturar o centro de custo padrão disponível. Contate o Administrador do Sistema. ");
		}
		
		return externoCentroCusto;
	}

	private BigDecimal verificaValorTotalDesconto(EntityWrapper processEntity) {
		
		List<NeoObject> itens = (List<NeoObject>) processEntity.findValue("itemOrcamento");
		
		BigDecimal valorDesconto = new BigDecimal(0.0);
		
		if (itens != null && !itens.isEmpty()) 	{
			for (NeoObject item : itens) {
				EntityWrapper wrpItem = new EntityWrapper(item);
				String tipoDoItem = (String) wrpItem.findValue("tipoItem.descricao");
				
				if(tipoDoItem.equalsIgnoreCase("DESCONTO")) {
					valorDesconto = valorDesconto.add((BigDecimal)wrpItem.findValue("valor"));
				}
			}
		}
		
		return valorDesconto;
	}

	public static BigDecimal totalizaItemFapPorTipo(String tipoItem, List<NeoObject> lstItensFap)
	{
		BigDecimal valorTotal = new BigDecimal(0.0D);
		for (NeoObject oItemFap : lstItensFap)
		{
			EntityWrapper wrpItem = new EntityWrapper(oItemFap);
			if ((wrpItem.findValue("tipoItem.descricao") != null) && (((String)wrpItem.findValue("tipoItem.descricao")).equals(tipoItem)))
			{
				valorTotal = valorTotal.add((BigDecimal)wrpItem.findValue("valor"));
			}
		}
		return valorTotal;
	}

	public static List<NeoObject> verificaCentroCustoSapiens(Long codigoEmpresa, List<NeoObject> lstItensFapCentroCusto)
	{
		List lstCentroCusto = new ArrayList();
		List externoCentroCusto = new ArrayList();
		
		List<String> centrosDeCustoNaoEncontrados = new ArrayList<>();

		for (NeoObject oItemCentroCusto : lstItensFapCentroCusto)
		{
			EntityWrapper wrapper = new EntityWrapper(oItemCentroCusto);
			if (wrapper != null)
			{
				String codigoCentroCusto = (String)wrapper.findValue("codigoCentroCusto");

				QLGroupFilter filterCcu = new QLGroupFilter("AND");
				filterCcu.addFilter(new QLEqualsFilter("codccu", codigoCentroCusto));
				filterCcu.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
				filterCcu.addFilter(new QLEqualsFilter("nivccu", Long.valueOf(8L)));
				externoCentroCusto = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

				if ((externoCentroCusto == null) || (externoCentroCusto.isEmpty()))
				{
					centrosDeCustoNaoEncontrados.add(codigoCentroCusto);
					lstCentroCusto.clear();
					continue;
				}

				lstCentroCusto.add((NeoObject)externoCentroCusto.get(0));
			}

		}
		
		if (!centrosDeCustoNaoEncontrados.isEmpty()) {			
				mensagem = "Centro(s) de Custo(s) não localizado(s)!"
						+ "	Por favor, verificar o(s) Centro(s) de Custo(s) associado(s) ao FAP e se o(s) mesmo(s) existe na Empresa de lançamento da Nota!" + " - Centro(s) de custo(s): "
						+ StringUtils.join(centrosDeCustoNaoEncontrados, ",");
			throw new WorkflowException(mensagem);
			
		}
		
		return lstCentroCusto;
	
	}

	public static List<NeoObject> insereParcelasFusion(BigDecimal valorTotal, String observacao)
	{
		List lstParcelas = new ArrayList();

		NeoObject oParcelas = AdapterUtils.createNewEntityInstance("GLNParcelas");
		EntityWrapper wrpParcelas = new EntityWrapper(oParcelas);

		wrpParcelas.setValue("titulo", "1");
		wrpParcelas.setValue("vencimentoParcela", new GregorianCalendar());
		wrpParcelas.setValue("valorParcela", valorTotal);
		wrpParcelas.setValue("observacaoParcela", observacao);

		lstParcelas.add(oParcelas);
		PersistEngine.persist(oParcelas);

		return lstParcelas;
	}

	public static List<NeoObject> insereCentroCustoFusion(Long codigoEmpresa, List<NeoObject> lstItensFapCentroCusto, BigDecimal valorTotal)
	{
		BigDecimal valorRateio = new BigDecimal(0.0D);
		BigDecimal valorTotalRateio = new BigDecimal(0.0D);
		List lstCentroCusto = new ArrayList();

		int qtdCentroCusto = lstItensFapCentroCusto.size();
		valorRateio = valorTotal.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);

		for (NeoObject oItemCentroCusto : lstItensFapCentroCusto)
		{
			if (oItemCentroCusto != null)
			{
				NeoObject oItensCentroCusto = AdapterUtils.createNewEntityInstance("GLNListaDeCentroDeCusto");
				EntityWrapper wrpCentroCusto = new EntityWrapper(oItensCentroCusto);

				if ((lstItensFapCentroCusto != null) && (!lstItensFapCentroCusto.isEmpty()))
				{
					wrpCentroCusto.setValue("codigoCentroCusto", oItemCentroCusto);
					wrpCentroCusto.setValue("valorRateio", valorRateio);

					valorTotalRateio = valorTotalRateio.add(valorRateio);
					lstCentroCusto.add(oItensCentroCusto);
					PersistEngine.persist(oItensCentroCusto);
				}
			}
		}

		BigDecimal valorAjustado = new BigDecimal(0.0D);
		int resultado = valorTotal.compareTo(valorTotalRateio);
		BigDecimal valorSobra = new BigDecimal(0.0D);
		valorTotal.plus(new MathContext(2, RoundingMode.HALF_UP));
		valorSobra = valorTotalRateio.subtract(valorTotal);

		NeoObject oCentroCusto = (NeoObject)lstCentroCusto.get(lstCentroCusto.size() - 1);
		EntityWrapper wrapperCentroCusto = new EntityWrapper(oCentroCusto);
		valorRateio = (BigDecimal)wrapperCentroCusto.findValue("valorRateio");

		if (resultado == 1)
		{
			BigDecimal retorno = valorRateio.add(valorSobra);
			valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
			wrapperCentroCusto.setValue("valorRateio", valorAjustado);
		}
		else if (resultado == -1)
		{
			BigDecimal retorno = valorRateio.subtract(valorSobra);
			valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
			wrapperCentroCusto.setValue("valorRateio", valorAjustado);
		}

		return lstCentroCusto;
	}

	public static void InserirContaItensProdutoServico(EntityWrapper processEntity, EntityWrapper wrpItensDeProdutoServico)
	{
		Long codigoTipoAplicacao = (Long)processEntity.findValue("aplicacaoPagamento.codigo");
		Long codigoEmpresa = (Long)processEntity.findValue("empresa.codemp");

		if (codigoTipoAplicacao.longValue() == 1L)
		{
			QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
			filtroContaContabil.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaContabil.addFilter(new QLEqualsFilter("ctared", Long.valueOf(4770L)));
			List contaContabil = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
			if ((contaContabil != null) && (contaContabil.size() > 0))
			{
				wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
			}
			else
			{
				String mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
			filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", Long.valueOf(79L)));
			List contaFinanceira = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
			if ((contaFinanceira != null) && (contaFinanceira.size() > 0))
			{
				wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
			}
			else
			{
				String mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

		}
		else if ((codigoTipoAplicacao.longValue() == 2L) || (codigoTipoAplicacao.longValue() == 3L) || (codigoTipoAplicacao.longValue() == 5L) || 
				(codigoTipoAplicacao.longValue() == 6L) || (codigoTipoAplicacao.longValue() == 8L) || (codigoTipoAplicacao.longValue() == 9L))
		{
			QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
			filtroContaContabil.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaContabil.addFilter(new QLEqualsFilter("ctared", Long.valueOf(4690L)));
			List contaContabil = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
			if ((contaContabil != null) && (contaContabil.size() > 0))
			{
				wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
			}
			else
			{
				String mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
			filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", Long.valueOf(377L)));
			List contaFinanceira = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
			if ((contaFinanceira != null) && (contaFinanceira.size() > 0))
			{
				wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
			}
			else
			{
				String mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}
		}
		else if ((codigoTipoAplicacao.longValue() == 10L) || (codigoTipoAplicacao.longValue() == 11L))
		{
			QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
			filtroContaContabil.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaContabil.addFilter(new QLEqualsFilter("ctared", Long.valueOf(4690L)));
			List contaContabil = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
			if ((contaContabil != null) && (contaContabil.size() > 0))
			{
				wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
			}
			else
			{
				String mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
			filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", Long.valueOf(267L)));
			List contaFinanceira = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
			if ((contaFinanceira != null) && (contaFinanceira.size() > 0))
			{
				wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
			}
			else
			{
				String mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

		}    
		else if (codigoTipoAplicacao.longValue() == 12L) {    	
			QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
			filtroContaContabil.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaContabil.addFilter(new QLEqualsFilter("ctared", Long.valueOf(33505L)));
			List contaContabil = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);

			if ((contaContabil != null) && (contaContabil.size() > 0)) {
				wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
			}
			else {
				String mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
			filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", Long.valueOf(337L)));
			List contaFinanceira = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);

			if ((contaFinanceira != null) && (contaFinanceira.size() > 0)) {
				wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
			}
			else {
				String mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}   	
		}      
		else 
		{    	
			QLGroupFilter filtroContaContabil = new QLGroupFilter("AND");
			filtroContaContabil.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaContabil.addFilter(new QLEqualsFilter("ctared", Long.valueOf(38895L)));
			List contaContabil = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), filtroContaContabil);
			if ((contaContabil != null) && (contaContabil.size() > 0))
			{
				wrpItensDeProdutoServico.setValue("contaContabil", contaContabil.get(0));
			}
			else
			{
				String mensagem = "Não encontrado a Conta Contábil para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroContaFinanceira = new QLGroupFilter("AND");
			filtroContaFinanceira.addFilter(new QLEqualsFilter("codemp", codigoEmpresa));
			filtroContaFinanceira.addFilter(new QLEqualsFilter("ctafin", Long.valueOf(707L)));
			List contaFinanceira = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), filtroContaFinanceira);
			if ((contaFinanceira != null) && (contaFinanceira.size() > 0))
			{
				wrpItensDeProdutoServico.setValue("contaFinanceira", contaFinanceira.get(0));
			}
			else
			{
				String mensagem = "Não encontrado a Conta Financecira para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}
		}
	}

	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}