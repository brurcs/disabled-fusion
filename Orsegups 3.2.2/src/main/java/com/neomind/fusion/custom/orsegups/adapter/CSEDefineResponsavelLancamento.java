package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jdt.core.dom.CatchClause;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CSEDefineResponsavelLancamento implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(CSEDefineResponsavelLancamento.class);

	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.adapter.CSEDefineResponsavelLancamento");
		log.warn("##### INICIAR TAREFA: FLUXO CSE DEFINE RESPONSAVEL LANCAMENTO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		try
		{

			String nomePapel = "";
			NeoObject qlExecucao = null;
			EntityWrapper qlExecucaoWrapper = null;
			NeoObject coberturaServicoExtra = null;
			EntityWrapper coberturaServicoExtraWrapper = null;
			NeoObject tipoMercado = null;
			EntityWrapper tipoMercadoWrapper = null;
			Long tipCli = 0L;
			//EFORM QLEXECUCAO PARA PESQUISAR POSTO
			qlExecucao = (NeoObject) processEntity.findValue("qlExecucao");
			if (NeoUtils.safeIsNotNull(qlExecucao))
				qlExecucaoWrapper = new EntityWrapper(qlExecucao);
			//EFORM CSECOBERTURASERVICOEXTRA
			if (NeoUtils.safeIsNotNull(qlExecucaoWrapper))
				coberturaServicoExtra = (NeoObject) qlExecucaoWrapper.findValue("coberturaServicoExtra");
			if (NeoUtils.safeIsNotNull(coberturaServicoExtra))
				coberturaServicoExtraWrapper = new EntityWrapper(coberturaServicoExtra);
			//EFORM CSECOBERTURASERVICOEXTRA TIPO MERCADO
			if (NeoUtils.safeIsNotNull(coberturaServicoExtraWrapper))
				tipoMercado = (NeoObject) coberturaServicoExtraWrapper.findValue("tipoMercao");
			if (NeoUtils.safeIsNotNull(tipoMercado))
				tipoMercadoWrapper = new EntityWrapper(tipoMercado);
			// TIPO MERCADO  CLIENTE
			if (NeoUtils.safeIsNotNull(tipoMercado) || NeoUtils.safeIsNotNull(tipoMercadoWrapper))
				tipCli = (Long) tipoMercadoWrapper.findValue("tipo");
			//VERIFICA TIPO CLIENTE E ESCOLHE POOL PARA EXECUCAO DA TAREFA
			if (tipCli != 0L && NeoUtils.safeIsNotNull(tipCli))
			{
				if (tipCli == 1L)
					nomePapel = "CSE - Cadastrar Servico Extra";
				else if (tipCli == 2L)
					nomePapel = "CSE - Cadastrar Servico Extra Publico";
			}
			else
			{
				nomePapel = "CSE - Cadastrar Servico Extra";
			}
			log.warn("##### EXECUTAR TAREFA: FLUXO CSE DEFINE RESPONSAVEL LANCAMENTO " + nomePapel + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			//VERIFICA TIPO CLIENTE E ESCOLHE POOL PARA EXECUCAO DA TAREFA	
			NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
			//INSERE RESPONSAVEL	
			processEntity.setValue("responsavelLancamento", (NeoPaper) obj);

		}
		catch (Exception e)
		{
			log.warn("##### ERRO TAREFA: FLUXO CSE DEFINE RESPONSAVEL LANCAMENTO Exception " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{
			log.warn("##### FINALIZAR TAREFA: FLUXO CSE DEFINE RESPONSAVEL LANCAMENTO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
