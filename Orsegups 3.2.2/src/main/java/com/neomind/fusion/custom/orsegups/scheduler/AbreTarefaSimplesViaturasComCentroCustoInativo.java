package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesViaturasComCentroCustoInativo implements CustomJobAdapter
{

	@Override
	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		Connection conn = PersistEngine.getConnection("SAPIENS");
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			List<ViaturaVO> lstviaturas = new ArrayList<ViaturaVO>();

			QLGroupFilter filter = new QLGroupFilter("AND");
			QLEqualsFilter filtroAtivaFrota = new QLEqualsFilter("ativaNaFrota", true);
			filter.addFilter(filtroAtivaFrota);
			String teste = "";

			Collection<NeoObject> viaturas = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filter, -1, -1, "placa asc");

			if (NeoUtils.safeIsNotNull(viaturas) && !viaturas.isEmpty())
			{
				for (NeoObject objViatura : viaturas)
				{

					EntityWrapper viatura = new EntityWrapper(objViatura);
					Long cc = (Long) viatura.findValue("codigoCentroCusto");
					String placa = (String) viatura.findValue("placa");

					//validação inserida pois o esta placa é utilizada para testes pelo Sr. Mateus Batista
					
					
					if((!placa.equalsIgnoreCase("MIH0572")) || (!placa.equalsIgnoreCase("MBZ1321")))
					{

						teste = teste + "," + cc;
						if (((Long) viatura.findValue("codigoCentroCusto")) != null && ((Long) viatura.findValue("codigoCentroCusto")) != 0)
						{
							StringBuilder sql = new StringBuilder();
							sql.append("SELECT * FROM E044CCU WHERE ACERAT = 'N' AND CODCCU = " + ((Long) viatura.findValue("codigoCentroCusto") + " AND CODEMP = 1"));

							pstm = conn.prepareStatement(sql.toString());
							rs = pstm.executeQuery();

							while (rs.next())
							{
								ViaturaVO viaturaVO = new ViaturaVO();

								viaturaVO.setPlaca(((String) viatura.findValue("placa")));
								viaturaVO.setTipoViatura(((String) viatura.findValue("tipoViatura.nome")));
								viaturaVO.setDescricaoViatura(((String) viatura.findValue("descricao")));
								viaturaVO.setCodigoCentroCusto(((Long) viatura.findValue("codigoCentroCusto")));

								lstviaturas.add(viaturaVO);
								//System.out.println("Viatura com problema -->" + ((String) viatura.findValue("placa")) + "/" + ((Long) viatura.findValue("codigoCentroCusto")));
							}
						}
						else
						{
							ViaturaVO viaturaVO = new ViaturaVO();

							viaturaVO.setPlaca(((String) viatura.findValue("placa")));
							viaturaVO.setTipoViatura(((String) viatura.findValue("tipoViatura.nome")));
							viaturaVO.setDescricaoViatura(((String) viatura.findValue("descricao")));
							viaturaVO.setCodigoCentroCusto(((Long) viatura.findValue("codigoCentroCusto")));

							lstviaturas.add(viaturaVO);
							//System.out.println("Houston! We have a problem. Viatura -->" + ((String) viatura.findValue("placa")) + "/" + ((Long) viatura.findValue("codigoCentroCusto")));
						}
					}
				}
				System.out.println(teste);
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}

			if (lstviaturas != null || lstviaturas.isEmpty())
			{

				String solicitante = "rosileny";
				String executor = "marcos.borges";
				String titulo = "Viaturas com Centro de Custo Inativo";
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 3L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
				if (lstviaturas != null && !lstviaturas.isEmpty())
				{
					String descricao = montaDescricaoTarefaSimples(lstviaturas);
					String codigoTarefa = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

					System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Viaturas com Centro Custo Inativo - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " - Aberto a Tarefa Simples: " + codigoTarefa);
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("[Abrir Tarefa Simples Viaturas com Centro Custo Inativo][" + key + "] - Erro ao executar o processo Abrir Tarefa Simples Viaturas com Centro Custo Inativo");
			e.printStackTrace();
			throw new JobException("Erro ao executar o processo, para maiores detalhes procurar no log por [Abrir Tarefa Simples Viaturas com Centro Custo Inativo][" + key + "]");
		}
		finally
		{
			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

	public String montaDescricaoTarefaSimples(List<ViaturaVO> viaturas)
	{
		String descricao = "";

		descricao = "Foi identificado que a(s) placa(s) abaixo relacionada(s) possuem o seu centro de custo Inativo no Sistema Sapiens. Por favor, providenciar o devido ajuste no Cadastro das Viaturas no Fusion.<br/><br/>";

		descricao = descricao + "<table class=\"gridbox\" cellpadding=\"0\" cellspacing=\"0\" style=\"empty-cells: show\">";
		descricao = descricao + "<thead>";
		descricao = descricao + "<tr><th colspan=4>Viaturas com Centro de Custo Inativo</th></tr>";
		descricao = descricao + "<tr>";
		descricao = descricao + "<th>Placa</th>";
		descricao = descricao + "<th>Tipo Viatura</th>";
		descricao = descricao + "<th>Descriçao</th>";
		descricao = descricao + "<th>Centro de Custo Inativo</th>";
		descricao = descricao + "</tr>";
		descricao = descricao + "</thead>";
		descricao = descricao + "<tbody>";

		for (ViaturaVO viatura : viaturas)
		{
			descricao = descricao + "<tr>";
			descricao = descricao + "<td>" + viatura.getPlaca() + "</td>";
			descricao = descricao + "<td>" + viatura.getTipoViatura() + "</td>";
			descricao = descricao + "<td>" + viatura.getDescricaoViatura() + "</td>";
			descricao = descricao + "<td>" + viatura.getCodigoCentroCusto() + "</td>";
			descricao = descricao + "</tr>";
		}

		descricao = descricao + "</tbody>";
		descricao = descricao + "</table>";

		return descricao;
	}
}
