package com.neomind.fusion.custom.orsegups.sigma;

public class TecnicoConta implements Comparable<TecnicoConta>
{
	private String nomeTecnico;
	private String contaAlarme;
	private String contaCFTV;
	private String regional;
	private Double totalAlarme;
	private Double totalCFTV;
	private Double valorAlarme;
	private Double valorCFTV;
	private Double subTotalAlarme;
	private Double subTotalCFTV;
	private Double total;
	private Integer modalidadePag;
	private String situacao;
	
	
	public Double getValorAlarme()
	{
		return valorAlarme;
	}
	public void setValorAlarme(Double valorAlarme)
	{
		this.valorAlarme = valorAlarme;
	}
	public Double getValorCFTV()
	{
		return valorCFTV;
	}
	public void setValorCFTV(Double valorCFTV)
	{
		this.valorCFTV = valorCFTV;
	}
	public Double getSubTotalAlarme()
	{
		return subTotalAlarme;
	}
	public void setSubTotalAlarme(Double subTotalAlarme)
	{
		this.subTotalAlarme = subTotalAlarme;
	}
	public Double getSubTotalCFTV()
	{
		return subTotalCFTV;
	}
	public void setSubTotalCFTV(Double subTotalCFTV)
	{
		this.subTotalCFTV = subTotalCFTV;
	}
	public Double getTotalAlarme()
	{
		return totalAlarme;
	}
	public void setTotalAlarme(Double totalAlarme)
	{
		this.totalAlarme = totalAlarme;
	}
	public Double getTotalCFTV()
	{
		return totalCFTV;
	}
	public void setTotalCFTV(Double totalCFTV)
	{
		this.totalCFTV = totalCFTV;
	}
	public String getNomeTecnico()
	{
		return nomeTecnico;
	}
	public void setNomeTecnico(String nomeTecnico)
	{
		this.nomeTecnico = nomeTecnico;
	}
	public String getContaAlarme()
	{
		return contaAlarme;
	}
	public void setContaAlarme(String contaAlarme)
	{
		this.contaAlarme = contaAlarme;
	}
	public String getContaCFTV()
	{
		return contaCFTV;
	}
	public void setContaCFTV(String contaCFTV)
	{
		this.contaCFTV = contaCFTV;
	}
	public String getRegional()
	{
		return regional;
	}
	public void setRegional(String regional)
	{
		this.regional = regional;
	}
	public Double getTotal()
	{
		return total;
	}
	public void setTotal(Double total)
	{
		this.total = total;
	}
	@Override
	public int compareTo(TecnicoConta o)
	{
		if((this.getSubTotalAlarme() + this.getSubTotalCFTV()) < (o.getSubTotalAlarme() + o.getSubTotalCFTV()))
			return 1;
		// TODO Auto-generated method stub
		return  -1;
	}
	public Integer getModalidadePag() {
	    return modalidadePag;
	}
	public void setModalidadePag(Integer modalidadePag) {
	    this.modalidadePag = modalidadePag;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	
	
}
