package com.neomind.fusion.custom.orsegups.integraRubi.adapter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;

public class AvancaAtividadeAutomatico implements ActivityStartEventListener
{

	private static final Log log = LogFactory.getLog(AvancaAtividadeAutomatico.class);

	@Override
	public void onStart(final ActivityEvent event) throws ActivityException
	{
		log.info("Inicio avanço automático");
		final Activity act = event.getActivity();
		final Long activityNeoId = act.getNeoId();
		final Task origin = OrsegupsWorkflowHelper.getTaskOrigin(act);
		NeoUser nu = null;

		if (origin == null || origin.getUser() == null)
		{
			if (event.getActivity().getInstance().getOwner() == null)
			{
				loop1: for (SecurityEntity se : event.getActivity().getInstance().getPotentialOwners())
				{
					if (se instanceof NeoPaper)
					{
						for (NeoUser nuUser : se.getAllUsers())
						{
							nu = nuUser;
							break loop1;
						}
					}
				}
			}
			else if (event.getActivity().getInstance().getOwner() instanceof NeoUser)
				nu = event.getActivity().getInstance().getOwner();
		}
		else
			nu = origin.getUser();

		log.info("NeoId Atividade: " + activityNeoId);
		log.info("Usuário: " + nu);

		final Long userNeoId = nu.getNeoId();

		NeoRunnable newEntityManager = new NeoRunnable()
		{
			@Override
			public void run() throws Exception
			{
				Thread.sleep(10000);

				log.info("Avançando atividade " + act);

				Activity act2 = PersistEngine.getNeoObject(Activity.class, activityNeoId);
				NeoUser nuExecutor = PersistEngine.getNeoObject(NeoUser.class, userNeoId);

				//para assumir a atividade em pool
				Task task = act2.getTaskAssigner().assign((UserActivity) act2, nuExecutor, true);

				RuntimeEngine.getTaskService().complete(task, nuExecutor);

				log.info("Tarefa " + event.getActivity().getInstance().getTitle() + " avançada com sucesso!");
			}
		};

		try
		{
			PersistEngine.backgroundManagedRun(newEntityManager);
		}
		catch (Exception e)
		{
			log.error("ERRO ", e);
		}
	}

}
