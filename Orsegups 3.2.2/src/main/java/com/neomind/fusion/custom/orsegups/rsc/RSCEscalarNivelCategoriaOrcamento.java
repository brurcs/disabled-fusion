package com.neomind.fusion.custom.orsegups.rsc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RSCEscalarNivelCategoriaOrcamento implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCEscalarNivelCategoriaOrcamento.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String erro = "Por favor, contatar o administrador do sistema!";
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		try
		{
			if (origin != null)
			{
				NeoPaper superiorResponsavelExecutor = null;
				NeoPaper superiorResponsavelExecutorVisitar = null;

				NeoUser executivoResponsavelRSC = (NeoUser) processEntity.findValue("responsavelExecutor");
				QLEqualsFilter ftrGrupoExecutivo = new QLEqualsFilter("grupoExecutivoRegional", executivoResponsavelRSC.getGroup());
				NeoObject grupoExecutivo = PersistEngine.getObject(AdapterUtils.getEntityClass("RSCHierarquisaOrcamento"), ftrGrupoExecutivo);

				if (NeoUtils.safeIsNotNull(grupoExecutivo))
				{
					EntityWrapper wrpResponsavelExecutivo = new EntityWrapper(grupoExecutivo);
					NeoPaper gerenteResponsavelExecutivo = (NeoPaper) wrpResponsavelExecutivo.findValue("gerenciaExecutivoRegional");
					NeoPaper superintendenciaResponsavelExecutivo = (NeoPaper) wrpResponsavelExecutivo.findValue("superintendenciaExecutivaRegional");
					NeoPaper diretoriaResponsavelExecutivo = (NeoPaper) wrpResponsavelExecutivo.findValue("diretoriaExecutivaRegional");
					NeoPaper presidenciaResponsavelExecutivo = (NeoPaper) wrpResponsavelExecutivo.findValue("presidenciaExecutivaRegional");

					if (origin.getActivityName().equalsIgnoreCase("Agendar Visita - Executivo"))
					{
						processEntity.setValue("superiorResponsavelExecutor", gerenteResponsavelExecutivo);
					}
					else if (origin.getActivityName().equalsIgnoreCase("Agendar Visita Escalada - Superior"))
					{
						superiorResponsavelExecutor = (NeoPaper) processEntity.findValue("superiorResponsavelExecutor");
						if (superiorResponsavelExecutor.getName().equals(gerenteResponsavelExecutivo.getName()))
						{
							processEntity.setValue("superiorResponsavelExecutor", superintendenciaResponsavelExecutivo);
						}
						else
						{
							processEntity.setValue("superiorResponsavelExecutor", diretoriaResponsavelExecutivo);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("Agendar Visita Escalada - Diretoria"))
					{
						processEntity.setValue("superiorResponsavelExecutor", presidenciaResponsavelExecutivo);
					}
					else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente - Executivo") || 
							(origin.getActivityName().equalsIgnoreCase("Visitar Cliente Escalada - Superior")) || 
							(origin.getActivityName().equalsIgnoreCase("Visitar Cliente Escalada - Diretoria")) || 
							(origin.getActivityName().equalsIgnoreCase("Visitar Cliente Escalada - Presidência")))
					{
						processEntity.setValue("superiorResponsavelExecutorVisitar", gerenteResponsavelExecutivo);
						Boolean fechouNegocio = (Boolean) processEntity.findValue("fechouNegocio");
						if (origin.getFinishByUser() != null)
						{
							if (!((Boolean) processEntity.findValue("fechouNegocio")) && ((Long) processEntity.findValue("motivoStatusNegocio.codigoMotivo")) == 5L)
							{
								processEntity.setValue("responsavelValidarPrazoRespostaOrcamento", gerenteResponsavelExecutivo);
								processEntity.setValue("responsavelValidarPrazoRespostaOrcamentoDiretoria", diretoriaResponsavelExecutivo);
							}
							else if (!((Boolean) processEntity.findValue("fechouNegocio")))
							{
								processEntity.setValue("responsavelValidarPrazoRespostaOrcamento", gerenteResponsavelExecutivo);
							}
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente Escalada - Superior"))
					{
						superiorResponsavelExecutorVisitar = (NeoPaper) processEntity.findValue("superiorResponsavelExecutorVisitar");
						if (superiorResponsavelExecutorVisitar.getName().equals(gerenteResponsavelExecutivo.getName()))
						{
							processEntity.setValue("superiorResponsavelExecutorVisitar", superintendenciaResponsavelExecutivo);
						}
						else
						{
							processEntity.setValue("superiorResponsavelExecutorVisitar", diretoriaResponsavelExecutivo);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente Escalada - Diretoria"))
					{
						processEntity.setValue("superiorResponsavelExecutorVisitar", presidenciaResponsavelExecutivo);
					}
				} else {
					erro = "Grupo" + executivoResponsavelRSC.getGroup() + "não cadastrado no formulário de hierarquia de RSC de Orçamento";
					throw new WorkflowException(erro);
				}			
			}
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
