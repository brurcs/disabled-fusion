package com.neomind.fusion.custom.orsegups.integracaoSlack;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Classe para integração com webhook de mensagens do Slack
 * 
 * @author danilo.silva
 *
 */
public class SlackNotifcationUtils
{

	public final static String NOTIFICATION_CHANNEL = "https://hooks.slack.com/services/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";

	public final static int ERROR_NOTIFICATIOIN = 0;
	public final static int WARNNING_NOTIFICATIOIN = 1;
	public final static int INFO_NOTIFICATIOIN = 2;

	/**
	 * Envia mensagem de notificação via Slack API.
	 * 
	 * @param notificationType - Tipo de Notificação
	 * @param message - mensagem a ser enviada
	 */
	public static void sendNotification(int notificationType, String message)
	{
		try
		{
			URL object = new URL(NOTIFICATION_CHANNEL);
			HttpURLConnection connection = (HttpURLConnection) object.openConnection();
			connection.setRequestMethod("POST");
			connection.setReadTimeout(60 * 1000);
			connection.setConnectTimeout(60 * 1000);
			//connection.setRequestProperty("X-Auth-Token-User-Integration", getConfig().getToken());

			JsonObject payload = new JsonObject();
			payload.addProperty("text", getMessageTAG(notificationType) + " - " + message);

			Gson gson = new Gson();

			String rawData = gson.toJson(payload);

			connection.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));
			writer.write(rawData);
			//wr.writeBytes(urlParams);
			writer.close();
			wr.flush();
			wr.close();

			//connection.setRequestProperty("Content-Type", "application/json");
			int responseCode = connection.getResponseCode();
			if (responseCode != 200)
			{
				throw new Exception("Erro ao enviar mensagem para o slack. " + responseCode);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static String getMessageTAG(int notificationType)
	{
		switch (notificationType)
		{
			case ERROR_NOTIFICATIOIN:
				return "[ERROR]";
			case WARNNING_NOTIFICATIOIN:
				return "[WARNNING]";
			case INFO_NOTIFICATIOIN:
				return "[INFO]";
			default:
				return "[INFO]";
		}

	}

}
