package com.neomind.fusion.custom.orsegups.rotinaInadimplencia.vo;

import java.util.GregorianCalendar;
import java.util.List;

public class TituloVO {
    
    private int codigoEmpresa;
    private int codigoFilial;
    private String tipoTitulo;
    private String numeroTitulo;
    private GregorianCalendar emissao;
    private GregorianCalendar vencimentoOriginal;
    private GregorianCalendar vencimentoProrrogado;
    private double valorOriginal;
    private double valorAberto;
    
    private List<ObservacaoTituloVO> listaObservacoes;
    
    public int getCodigoEmpresa() {
        return codigoEmpresa;
    }
    public void setCodigoEmpresa(int codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
    public int getCodigoFilial() {
        return codigoFilial;
    }
    public void setCodigoFilial(int codigoFilial) {
        this.codigoFilial = codigoFilial;
    }    
    public List<ObservacaoTituloVO> getListaObservacoes() {
        return listaObservacoes;
    }
    public void setListaObservacoes(List<ObservacaoTituloVO> listaObservacoes) {
        this.listaObservacoes = listaObservacoes;
    }
    public String getTipoTitulo() {
        return tipoTitulo;
    }
    public void setTipoTitulo(String tipoTitulo) {
        this.tipoTitulo = tipoTitulo;
    }
    public String getNumeroTitulo() {
        return numeroTitulo;
    }
    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }
    public GregorianCalendar getEmissao() {
        return emissao;
    }
    public void setEmissao(GregorianCalendar emissao) {
        this.emissao = emissao;
    }
    public GregorianCalendar getVencimentoOriginal() {
        return vencimentoOriginal;
    }
    public void setVencimentoOriginal(GregorianCalendar vencimentoOriginal) {
        this.vencimentoOriginal = vencimentoOriginal;
    }
    public GregorianCalendar getVencimentoProrrogado() {
        return vencimentoProrrogado;
    }
    public void setVencimentoProrrogado(GregorianCalendar vencimentoProrrogado) {
        this.vencimentoProrrogado = vencimentoProrrogado;
    }
    public double getValorOriginal() {
        return valorOriginal;
    }
    public void setValorOriginal(double valorOriginal) {
        this.valorOriginal = valorOriginal;
    }
    public double getValorAberto() {
        return valorAberto;
    }
    public void setValorAberto(double valorAberto) {
        this.valorAberto = valorAberto;
    }
    
    
    
}
