package com.neomind.fusion.custom.orsegups.rotinaInadimplencia.job;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.custom.orsegups.rotinaInadimplencia.vo.ClienteInadimplenteVO;
import com.neomind.fusion.custom.orsegups.rotinaInadimplencia.vo.ContratoVO;
import com.neomind.fusion.custom.orsegups.rotinaInadimplencia.vo.ObservacaoTituloVO;
import com.neomind.fusion.custom.orsegups.rotinaInadimplencia.vo.TituloVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaInadimplencia implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext arg0) {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT E085CLI.CodCli, E085CLI.NOMCLI, USU_T160CTR.USU_NUMCTR, E301TCR.CODEMP, E070EMP.SigEmp, E301TCR.CODFIL,   ");
	sql.append(" E301TCR.CODTPT AS TIPO_TITULO, E301TCR.NUMTIT AS NUMERO_TITULO,  ");
	sql.append(" E301TCR.DatEmi AS EMISSAO, E301TCR.VctOri AS VENCIMENTO_ORIGINAL, E301TCR.VctPro AS VENCIMENTO_PRORROGADO,  ");
	sql.append(" E301TCR.VlrOri AS VALOR_ORIGINAL, E301TCR.VlrAbe AS VALOR_ABERTO   ");
	sql.append(" FROM E301TCR, E085CLI, E001TNS, E140NFV, E140ISV, USU_T160CTR, E070EMP");
	sql.append(" WHERE E301TCR.CODEMP = E001TNS.CODEMP");
	sql.append(" AND E301TCR.CODCLI = E085CLI.CODCLI  ");
	sql.append(" AND E140NFV.CODEMP = E301TCR.CODEMP  ");
	sql.append(" AND E140NFV.CODFIL = E301TCR.CODFIL  ");
	sql.append(" AND E140NFV.CODSNF = E301TCR.CODSNF  ");
	sql.append(" AND E140NFV.NUMNFV = E301TCR.NUMNFV  ");
	sql.append(" AND E140ISV.CODEMP = E140NFV.CODEMP  ");
	sql.append(" AND E140ISV.CODFIL = E140NFV.CODFIL  ");
	sql.append(" AND E140ISV.CODSNF = E140NFV.CODSNF  ");
	sql.append(" AND E140ISV.NUMNFV = E140NFV.NUMNFV  ");
	sql.append(" AND E001TNS.CODEMP = E301TCR.CODEMP  ");
	sql.append(" AND E001TNS.CODTNS = E301TCR.CODTNS  ");
	sql.append(" AND USU_T160CTR.USU_CODEMP = E140ISV.CODEMP  ");
	sql.append(" AND USU_T160CTR.USU_CODFIL = E140ISV.CODFIL  ");
	sql.append(" AND USU_T160CTR.USU_NUMCTR = E140ISV.NUMCTR ");
	sql.append(" AND E301TCR.CODEMP = E070EMP.CodEmp");
	sql.append(" AND E001TNS.LISMOD = 'CRE'   ");
	sql.append(" AND E301TCR.VLRABE > 0  ");
	sql.append(" AND E301TCR.VCTPRO >= GETDATE()-34   ");
	sql.append(" AND E301TCR.CODFPG <> 21  ");
	sql.append(" AND ((E301TCR.CODFPG <> 14 AND E301TCR.VCTPRO <= GETDATE()-4) OR (E301TCR.CODFPG = 14 AND E301TCR.VCTPRO <= GETDATE()-9))  ");
	sql.append(" AND E085CLI.TIPEMC = 1  ");
	sql.append(" AND ((E301TCR.SITTIT >= 'AA' AND E301TCR.SITTIT <= 'AV') OR (E301TCR.SITTIT = 'CE'))  ");
	sql.append(" AND USU_T160CTR.USU_SITCTR = 'A'  ");
	sql.append(" AND USU_T160CTR.USU_SERCTR IN(2,3,5,6,7,8,9,53,52,55)  ");
	sql.append(" AND E301TCR.CODEMP IN (24,26)  ");
	sql.append(" AND E301TCR.CODTPT <> 'IFA'   ");
	sql.append(" GROUP BY E085CLI.CodCli, E301TCR.CODEMP, E301TCR.CODFIL, USU_T160CTR.USU_NUMCTR, E085CLI.NOMCLI, E070EMP.SigEmp,");
	sql.append(" E301TCR.NUMTIT, E301TCR.CODTPT, E301TCR.DatEmi,E301TCR.VctOri,E301TCR.VctPro, E301TCR.VlrOri, E301TCR.VlrAbe  ");
	sql.append(" ORDER BY CodCli   ");

	Map<Integer, List<ClienteInadimplenteVO>> mapaEmpresas = new HashMap<Integer, List<ClienteInadimplenteVO>>();

	try {
	    conn = PersistEngine.getConnection("SAPIENS");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {

		int codigoEmpresa = rs.getInt("CODEMP");

		int codigoCliente = rs.getInt("CodCli");

		List<ClienteInadimplenteVO> clientes = mapaEmpresas.get(codigoEmpresa);

		if (clientes == null) {
		    clientes = new ArrayList<ClienteInadimplenteVO>();
		    mapaEmpresas.put(codigoEmpresa, clientes);

		}

		ClienteInadimplenteVO cliente = null;

		for (ClienteInadimplenteVO c : clientes) {

		    if (c.getCodigoCliente() == codigoCliente) {
			cliente = c;
		    }

		}

		if (cliente == null) {
		    cliente = new ClienteInadimplenteVO();
		    cliente.setCodigoCliente(codigoCliente);
		    cliente.setNomeCliente(rs.getString("NOMCLI"));
		    cliente.setSiglaEmpresa(rs.getString("SigEmp"));
		    clientes.add(cliente);

		}

		List<ContratoVO> contratos = cliente.getListaContratos();

		if (contratos == null) {
		    contratos = new ArrayList<ContratoVO>();
		    cliente.setListaContratos(contratos);
		}

		int numeroContrato = rs.getInt("USU_NUMCTR");

		ContratoVO contrato = null;

		for (ContratoVO c : contratos) {
		    if (c.getNumeroContrato() == numeroContrato) {
			contrato = c;
		    }
		}

		if (contrato == null) {
		    contrato = new ContratoVO();
		    contrato.setListaTitulos(new ArrayList<TituloVO>());
		    contrato.setNumeroContrato(numeroContrato);
		    contratos.add(contrato);
		}

		List<TituloVO> titulos = contrato.getListaTitulos();

		if (titulos == null) {
		    titulos = new ArrayList<TituloVO>();
		    contrato.setListaTitulos(titulos);
		}

		TituloVO titulo = new TituloVO();

		titulo.setCodigoEmpresa(codigoEmpresa);
		titulo.setCodigoFilial(rs.getInt("CODFIL"));
		titulo.setTipoTitulo(rs.getString("TIPO_TITULO"));
		titulo.setNumeroTitulo(rs.getString("NUMERO_TITULO"));

		Timestamp tse = rs.getTimestamp("EMISSAO");

		if (tse != null) {
		    GregorianCalendar emissao = new GregorianCalendar();
		    emissao.setTimeInMillis(tse.getTime());
		    titulo.setEmissao(emissao);
		}

		Timestamp tsvo = rs.getTimestamp("VENCIMENTO_ORIGINAL");

		if (tsvo != null) {
		    GregorianCalendar vctOriginal = new GregorianCalendar();
		    vctOriginal.setTimeInMillis(tsvo.getTime());
		    titulo.setVencimentoOriginal(vctOriginal);
		}

		Timestamp tsvp = rs.getTimestamp("VENCIMENTO_PRORROGADO");

		if (tsvp != null) {
		    GregorianCalendar vctProrrogado = new GregorianCalendar();
		    vctProrrogado.setTimeInMillis(tsvp.getTime());
		    titulo.setVencimentoProrrogado(vctProrrogado);
		}

		titulo.setValorOriginal(rs.getDouble("VALOR_ORIGINAL"));
		titulo.setValorAberto(rs.getDouble("VALOR_ABERTO"));

		this.getObservacoesTitulo(titulo);

		titulos.add(titulo);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	for (Integer key : mapaEmpresas.keySet()) {


	    for (ClienteInadimplenteVO cliente : mapaEmpresas.get(key)) {

		String descricaoTarefa = "<p>Realizar a cobrança dos títulos em aberto, faturados pela empresa "+cliente.getSiglaEmpresa()
			+" para o cliente "+cliente.getNomeCliente()+" conforme relatório abaixo: </p>";
		
		descricaoTarefa += this.construirHtml(cliente);

//		File htmlFile = new File(NeoStorage.getDefault().getPath() + "/report/inadimplencia/relatorio.html");
//
//		try {
//		    FileUtils.writeStringToFile(htmlFile, html);
//		} catch (IOException e) {
//		    e.printStackTrace();
//		}

//		NeoStorage neoStorage = NeoStorage.getDefault();
//		NeoFile anexo = neoStorage.copy(htmlFile);
//		anexo.setName("relatorio.html");

		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 5L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		String titulo = "["+cliente.getSiglaEmpresa()+ "] Cobrar cliente inadimplente - "+cliente.getNomeCliente();

		String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteTarefaInadimplencia");

		String executor = "";

		if (key == 24) {
		    executor = OrsegupsUtils.getUserNeoPaper("executorTarefaInadimplencia4B2G");
		} else if (key == 25) {
		    executor = OrsegupsUtils.getUserNeoPaper("executorTarefaInadimplenciaWinker");
		} else if (key == 26) {
		    executor = OrsegupsUtils.getUserNeoPaper("executorTarefaInadimplenciaNexxus");
		}

		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
		tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricaoTarefa, "3", "sim", prazo);
	    }

	}

    }

    private void getObservacoesTitulo(TituloVO titulo) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	sql.append(" SELECT ObsTit, DatMov  ");
	sql.append(" FROM E301MOR WITH(NOLOCK) ");
	sql.append(" WHERE CodEmp = ?  ");
	sql.append(" AND CodFil= ? ");
	sql.append(" AND CodTpt= ? ");
	sql.append(" AND NumTit= ? ");

	try {
	    conn = PersistEngine.getConnection("SAPIENS");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, titulo.getCodigoEmpresa());
	    pstm.setInt(2, titulo.getCodigoFilial());
	    pstm.setString(3, titulo.getTipoTitulo());
	    pstm.setString(4, titulo.getNumeroTitulo());

	    rs = pstm.executeQuery();

	    List<ObservacaoTituloVO> observacoes = new ArrayList<ObservacaoTituloVO>();

	    while (rs.next()) {

		ObservacaoTituloVO obs = new ObservacaoTituloVO();

		obs.setObservacaoTitulo(rs.getString(1));

		Timestamp ts = rs.getTimestamp(2);

		if (ts != null) {
		    GregorianCalendar data = new GregorianCalendar();
		    data.setTimeInMillis(ts.getTime());
		    obs.setDataObservacao(data);
		}

		observacoes.add(obs);

	    }

	    titulo.setListaObservacoes(observacoes);

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    public String construirHtml(ClienteInadimplenteVO cli) {

	StringBuilder html = new StringBuilder();

//	html.append("<!DOCTYPE html>");
//	html.append("<html>");
//	html.append("<head>");
//	html.append("<title>Relatório de inadimplência</title>");
//	html.append("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"> ");
//	html.append("</head>");
//	html.append("<body>");

//	html.append("<div class=\"container-fluid\"> ");

//	html.append("<div class=\"table-responsive\"> ");

	html.append(" <table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\"> ");

//	html.append(" <tr><th colspan=\"10\"><strong>" + cli.getCodigoCliente() + "-" + cli.getNomeCliente() + "</strong></th></tr> ");
	html.append(" <tr style=\"text-align: left;\"> ");
	html.append(" <th>Contrato</th><th>Empresa</th><th>Filial</th><th>Tipo Título</th><th>Número Título</th><th>Emissão</th><th>Venc Original</th> ");
	html.append(" <th>Venc Prorrogado</th><th>Valor Original</th><th>Valor Aberto</th> ");
	html.append(" </tr> ");

	for (ContratoVO con : cli.getListaContratos()) {

	    for (TituloVO t : con.getListaTitulos()) {
		html.append(" <tr> ");
		html.append(" <td>" + con.getNumeroContrato() + "</td><td>" + t.getCodigoEmpresa() + "</td><td>" + t.getCodigoFilial() + "</td><td>" + t.getTipoTitulo() + "</td><td>" + t.getNumeroTitulo() + "</td>");
		html.append(" <td>" + NeoDateUtils.safeDateFormat(t.getEmissao(), "dd/MM/yyyy") + "</td><td>" + NeoDateUtils.safeDateFormat(t.getVencimentoOriginal(), "dd/MM/yyyy") + "</td><td>" + NeoDateUtils.safeDateFormat(t.getVencimentoProrrogado(), "dd/MM/yyyy") + "</td>");
		html.append(" <td>R$ " + t.getValorOriginal() + "</td><td>R$ " + t.getValorAberto() + "</td> ");

		/**
		 * Omitindo observações do contrato afim de economizar
		 * caracteres
		 *
		 * html.append(" <td>"); for (ObservacaoTituloVO obs :
		 * t.getListaObservacoes()){
		 * html.append(NeoDateUtils.safeDateFormat
		 * (obs.getDataObservacao(),
		 * "dd/MM/yyyy")+" "+obs.getObservacaoTitulo()+"<br>
		 * "); } html.append("</td> ");
		 */

		html.append(" </tr> ");
	    }

	}

	html.append("</table> ");

//	html.append("</div>");
//	html.append("</div>");
//	html.append("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" </script>");
//	html.append("</body> ");
//	html.append("</html> ");

	return html.toString();
    }

}
