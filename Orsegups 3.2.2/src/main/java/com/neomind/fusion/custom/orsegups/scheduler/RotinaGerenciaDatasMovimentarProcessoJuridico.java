package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaGerenciaDatasMovimentarProcessoJuridico implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {

	Connection conn = PersistEngine.getConnection("");

	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaGerenciaDatasMovimentarProcessoJuridico");
	System.out.println("##### INICIO AGENDADOR DE TAREFA: RotinaGerenciaDatasMovimentarProcessoJuridico - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	try {

	    Date datAud = null;

	    Integer neoId = 0;
	    Integer tipoProcesso = 0;

	    StringBuffer varname1 = new StringBuffer();

	    varname1.append("       select j002.neoId, ");
	    varname1.append("	           J002Tp.codigo as tipoProcesso, ");
	    varname1.append("	           datAud, ");
	    varname1.append("	           coalesce(j002Tp.codigo,1) as tipoProcesso, ");
	    varname1.append("	           j002HP.dataRecebido as dataRecebido, ");
	    varname1.append("	           j002HP.dataPrazo as dataPrazo ");
	    varname1.append("	      from D_j002Principal j002 ");
	    varname1.append("	     inner join WFProcess p on j002.wfprocess_neoId = p.neoId ");
	    varname1.append("	      left join d_j002TipoProcesso j002Tp on j002.tipoProcesso_neoId = j002Tp.neoId ");
	    varname1.append("	     inner join d_J002Principal_historicoPenalidades j002php on j002.neoId = j002php.D_j002Principal_neoId ");
	    varname1.append("	     inner join d_J002HistoricoPenalidade j002HP on j002HP.neoId = j002php.historicoPenalidades_neoId ");
	    varname1.append("	     where p.finishDate is null ");
	    varname1.append("	       and (RIGHT('0' +RTRIM(month(GETDATE())),2)+RIGHT('0' +RTRIM(day(GETDATE())),2)+0) < 1220 ");
	    varname1.append("	       and (RIGHT('0' +RTRIM(month(GETDATE())),2)+RIGHT('0' +RTRIM(day(GETDATE())),2)+0) > 0121 ");
	    varname1.append("	       and j002Tp.codigo = 2 ");
	    varname1.append("	       and j002HP.dataVencimento is null ");
	    varname1.append("	  order by j002.neoId ");

	    pstm = conn.prepareStatement(varname1.toString());
	    rs = pstm.executeQuery();

	    while (rs.next()) {

		datAud = rs.getDate("datAud");
		neoId = rs.getInt("neoId");
		tipoProcesso = rs.getInt("tipoProcesso");

		if (tipoProcesso.equals(2)) {

		    if (datAud != null) {
			CivelItem11(datAud);
		    }
		}
	    }

	} catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: RotinaGerenciaDatasMovimentarProcessoJuridico");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    try {
		rs.close();
		pstm.close();
		conn.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }
	    System.out.println("##### FIM AGENDADOR DE TAREFA: RotinaGerenciaDatasMovimentarProcessoJuridico - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }

    private void CivelItem3() {

	// Verificar se no lista de Tipo de Processos deve ou não buscar os
	// processos com data de inicio.

    }

    /**
     * CivelItem11 - No dia seguinte ao agendado, o processo irá para caixa do
     * escritório de advocacia onde terá o prazo de 5 dias úteis para anexar a
     * ata e providências.
     */
    private void CivelItem11(Date dataAudiencia) {

	// Verificar se é processo Civel - OK
	// Veriricar data de Suspensão Fim Do Ano
	// Verificar se processo está em Movimentar Processo
	// Verificar se a data Agendada da Audiencia é 1 dia antes da Atual Data

	// Setar AvancaPorJob para 11
	// Avançar tarefa
	// Setar AvançaPorJob para null
    }

    /**
     * CivelItem12 -
     */
    private void CivelItem12(Date dataAcordo) {

	// Verificar se é processo Civel
	// Veriricar data de Suspensão Fim Do Ano
	// Verificar se processo está em Movimentar Processo
	// Verificar se data Acordo é 1 dia antes da Atual Data

	// Setar AvancaPorJob para 12
	// Avançar tarefa
	// Setar AvançaPorJob para null
    }
}