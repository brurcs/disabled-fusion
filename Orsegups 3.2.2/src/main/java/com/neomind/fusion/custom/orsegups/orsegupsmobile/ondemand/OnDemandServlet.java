package com.neomind.fusion.custom.orsegups.orsegupsmobile.ondemand;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.rsc.helper.RSCHelper;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.site.SiteUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;

/**
 * Servlet para integrações com o appl Orsegups Ondemand mobile que interagem com tarefas do fusion.
 * @author danilo.silva
 *
 */
@WebServlet(name="OnDemandServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.orsegupsmobile.ondemand.OnDemandServlet"})
public class OnDemandServlet extends HttpServlet
{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}
	
	public void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException
	{
		response.setContentType("text/html");
		response.setCharacterEncoding("ISO-8859-1");

		PrintWriter out = null;
		
		try
		{
		
			String action = "";
			if (request.getParameter("action") != null)
			{
				
				action = request.getParameter("action");
				out = response.getWriter();
				
				if (action.equals("rsc.abrir")){
					
					abrirRSC(request, response, out);
					
				}else if (action.equals("abrirTarefaSimplesValidarContaSigmaOnDemand")){
					
					abrirTarefaSimplesValidarContaSigmaOnDemand(request, out);
					
				}
				
				
			}
			
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	private void abrirRSC(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		try
		{
			String sessionId = request.getParameter("sessionId");
			//Busca os valores por Request
			String nome = request.getParameter("nome");
			String email = request.getParameter("email");
			String telefone = request.getParameter("telefone");
			String mensagem = request.getParameter("mensagem");
			String cidade = request.getParameter("cidade");
			String bairro = request.getParameter("bairro");
			String tipoContato = "127514862"; /*por pardao o tipo será solicitação*/ //request.getParameter("tipoContato");
			
			
			String valueSolicitante = "MOBILERESPONSAVELRSC";
			
			GregorianCalendar gcPrazo = new GregorianCalendar();
			gcPrazo.add(GregorianCalendar.DATE, 1);
			gcPrazo.set(GregorianCalendar.HOUR, 23);
			gcPrazo.set(GregorianCalendar.MINUTE, 59);
			gcPrazo.set(GregorianCalendar.SECOND, 59);
			
			RSCVO rsc = new RSCVO();
			//rsc.setCodCli(codcli);
			rsc.setOrigemSolicitacao("14");
			rsc.setTipoContato(tipoContato);
			rsc.setNome(nome);
			rsc.setEmail(email);
			rsc.setTelefone(telefone);
			rsc.setMensagem(mensagem);
			rsc.setCidade(cidade);
			rsc.setBairro(bairro);
			rsc.setPrazo(OrsegupsUtils.getNextWorkDay(gcPrazo));
			
			String tarefa = RSCHelper.abrirRSC(rsc, valueSolicitante,true);
			System.out.println("Tarefa RSC Aberta: "+ tarefa);
			
			JSONObject jsonCliente = new JSONObject();
			jsonCliente.put("status", 100L);
			jsonCliente.put("msg", "Obrigado pelo contato");
			jsonCliente.put("code", tarefa);
			out.print(jsonCliente);

		}
		catch (Exception e)
		{
			JSONObject jsonCliente = new JSONObject();

			jsonCliente.put("status", 99L);
			jsonCliente.put("msg", "Servidor indisponível, tente mais tarde.");
			out.print(jsonCliente);
			e.printStackTrace();
		}
	}
	
	private void abrirTarefaSimplesValidarContaSigmaOnDemand(HttpServletRequest request, PrintWriter out)
	{
		GregorianCalendar prazo = OrsegupsUtils.getNextWorkDay(new GregorianCalendar());
		prazo.set(GregorianCalendar.HOUR,23);
		prazo.set(GregorianCalendar.MINUTE,59);
		prazo.set(GregorianCalendar.SECOND,59);
		
		
		String id_central 	= request.getParameter("id_central");
		String cd_cliente 	= request.getParameter("cd_cliente");
		String nome 		= request.getParameter("nome");
		String email 		= request.getParameter("email");
		String telefone 	= request.getParameter("telefone");
		String cidade 		= request.getParameter("cidade");
		String bairro 		= request.getParameter("bairro");
		String endereco 	= request.getParameter("endereco");
		String cgcCpf 		= request.getParameter("cgcCpf");
		
		
		String solicitante = "giliardi" ;
		String papelExecutor = "MOBILERESPONSAVELTAREFASIMPLES";
		
		NeoPaper papel = new NeoPaper();
		String executor = "";
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", papelExecutor));
		papel = (NeoPaper) obj;

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				executor = user.getCode();
				break;
			}
		}
		
		String origem = "2";
		
		String titulo = " Validar Conta Sigma - Orsegups On Demand " + id_central + ", cd_cliente:"+cd_cliente;
		
		String descricao = " Validar os dados da conta criada automaticamente pelo serviço. ";

		descricao += "\n Conta: " + id_central; 	
		descricao += "\n Cliente Sigma: " + cd_cliente; 	
		descricao += "\n Razao: " + nome; 		
		descricao += "\n E-mail: " + email;
		descricao += "\n Telefone: " + telefone; 	
		descricao += "\n Cidade: " + cidade; 		
		descricao += "\n Bairro: " + bairro; 		
		descricao += "\n Endereco: " + endereco; 	
		descricao += "\n CPF/CNPJ:" + cgcCpf; 		
		
		out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo));
	}

	private static String abrirTarefa (String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo)
	{
		/**
		 * @author neomind willian.mews - Alterado para chamar classe já existente e assim manter a lógica centralizada
		 * @date 11/03/2015
		 */
		
		com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples tarefaSimples = new com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples();
		return tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo);
		
		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
	}
}
