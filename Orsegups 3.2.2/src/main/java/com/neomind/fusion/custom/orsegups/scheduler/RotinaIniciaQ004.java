package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;

public class RotinaIniciaQ004 implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {

	//List<NeoObject> postos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHR016ORN"), new QLRawFilter("numloc = " + c.getWorkplaceExternalId()));
	
	NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "coordenadorTI"));
	
	List<NeoObject> postos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHR016ORN"), new QLRawFilter("numloc = " + 88322));
	NeoObject posto = postos.get(0);
	EntityWrapper postoWrapper = new EntityWrapper(posto);

	final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "Q004 - Checklist Supervisão"));
	InstantiableEntityInfo objFormPrincipal = AdapterUtils.getInstantiableEntityInfo("q004ChecklistSupervisao");
	final NeoObject formularioProcesso = objFormPrincipal.createNewInstance();// processo.getEntity();
	final EntityWrapper formularioWrapper = new EntityWrapper(formularioProcesso);
	
	final NeoObject formularioProcesso1 = AdapterUtils.createNewEntityInstance("tarefa");//processo.getEntity();
	final EntityWrapper formularioWrapper1 = new EntityWrapper(formularioProcesso1);
	
	formularioWrapper1.findField("Solicitante").setValue(objSolicitante);
	formularioWrapper1.findField("Titulo").setValue("Check List Supervisão: MERCURY MARINE DO BRASIL INDUSTRIA E COMERCIO LTDA");
	formularioWrapper1.findField("DescricaoSolicitacao").setValue("");
	formularioWrapper1.findField("dataSolicitacao").setValue(new GregorianCalendar());
	formularioWrapper1.findField("avancaPrimeiraAtividade").setValue(true);
	
	formularioWrapper.findField("tarefaSimples").setValue(formularioProcesso1);
	formularioWrapper.findField("empresa").setValue("CASVIG CATARINENSE DE SEGURANÇA E VIGILÂNCIA LTDA.");
	formularioWrapper.findField("cliente").setValue("MERCURY MARINE DO BRASIL INDUSTRIA E COMERCIO LTDA");
	formularioWrapper.findField("questao").setValue("O registro autenticado da arma está no posto?");
	formularioWrapper.findField("resposta").setValue(false);
	formularioWrapper.findField("responsavel").setValue(objSolicitante);
	formularioWrapper.findField("respostaObservacao").setValue("Verificado que existe apenas a copia do registro do armamento no posto");

	String tarefa = null;
	try {
	    tarefa = OrsegupsWorkflowHelper.iniciaProcesso(pm, formularioProcesso, true, objSolicitante, false, null);
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println(e.getMessage());
	}
    }
}
