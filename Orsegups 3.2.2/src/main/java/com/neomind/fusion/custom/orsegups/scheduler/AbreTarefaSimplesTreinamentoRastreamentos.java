package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesTreinamentoRastreamentos implements CustomJobAdapter
{
    protected String descricao;
    protected String descricao2;
    protected String titulo;
    protected String titulo2;
    @Override
    public void execute(CustomJobContext arg0)
    {
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Connection conn = PersistEngine.getConnection("SIGMA90");
	StringBuilder sql = new StringBuilder();
	StringBuilder sql2 = new StringBuilder();
	PreparedStatement pstm = null;
	PreparedStatement pstm2 = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	final String solicitante = "nicolas.dias";
	final String solicitante2 = "gustavo.freitas";
	final String executor = "thiago.orlandi";
	final String executor2 = "zildo.borcathe";
	final GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 1L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);
	
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesTreinamentoRastreamentos");
	log.warn("##### INICIO AGENDADOR DE TAREFA: Abre Tarefa Simples Treinamento Rastreamentos - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	try
	{
	    // BUSCA CLIENTES COM APENAS 1 ORDEM
	    sql.append("SELECT CD_CLIENTE, COUNT(ID_ORDEM) AS QTD_ORDENS FROM dbORDEM WHERE FECHADO = 1 ");
	    sql.append("GROUP BY CD_CLIENTE ");
	    sql.append("HAVING COUNT(ID_ORDEM) = 1 ");
	    sql.append("ORDER BY CD_CLIENTE ");

	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();
	    String clientes = "";
	    int cont = 0;
	    while (rs.next())
	    {
		if(rs.getLong("QTD_ORDENS") == 1)
		{
		    Long codCliente = rs.getLong("CD_CLIENTE");
		
		    clientes = clientes + codCliente + ",";
		    
		    cont++;    
		}
	    }
	    
	    System.out.println(cont + " ordens encontradas.");
	    
	    StringBuilder b = new StringBuilder(clientes);
	    b.replace(clientes.lastIndexOf(","), clientes.lastIndexOf(",") + 1, "" );
	    clientes = b.toString();
	  
	    pstm.close();
	    rs.close();
	    
	    GregorianCalendar cal = new GregorianCalendar(); 
	    cal.add(GregorianCalendar.DAY_OF_MONTH , - 1);
	    cal.add(GregorianCalendar.MONTH , + 1);
	    int diaAnterior = cal.get(GregorianCalendar.DAY_OF_MONTH);
	    int mes = cal.get(GregorianCalendar.MONTH);
	    
	    
	    System.out.println(mes);
	    int ano = cal.get(GregorianCalendar.YEAR);
	    
	
	    sql2.append("SELECT o.ID_ORDEM, o.CD_CLIENTE, c.RAZAO, c.FANTASIA, c.CGCCPF, c.FONE1, c.FONE2, c.ENDERECO, c.RESPONSAVEL,c.EMAILRESP, o.ABERTURA, o.FECHAMENTO, o.EXECUTADO");
	    sql2.append(" FROM dbORDEM o WITH (NOLOCK) ");
	    sql2.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON o.CD_CLIENTE = c.CD_CLIENTE ");
	    sql2.append(" WHERE o.CD_CLIENTE IN (" + clientes + ")" );
	    sql2.append(" AND o.EXECUTADO LIKE 'INSTALAÇÃO%' ");
	    sql2.append(" AND o.FECHADO = 1 ");
	    if(mes < 10)
	    {
		String numero = "0" + mes;
		sql2.append(" AND CONVERT(VARCHAR(25), o.FECHAMENTO, 126) LIKE '" + ano + "-" + numero + "-" + diaAnterior + "%'");
	    }else
	    {
		sql2.append(" AND CONVERT(VARCHAR(25), o.FECHAMENTO, 126) LIKE '" + ano + "-" + mes + "-" + diaAnterior + "%'");
	    }
	    
	    System.out.println(sql2);
	    
	    pstm2 = conn.prepareStatement(sql2.toString());
	    rs2 = pstm2.executeQuery();
	           
	    while (rs2.next())
	    {
                
		// DADOS TAREFA TREINAMENTO DE RASTREAMENTO
		
		titulo = "Treinamento de Rastreamento - " + rs2.getString("ID_ORDEM") + "  " + rs2.getString("RAZAO");
		
		descricao = "";
		descricao = descricao + "Segue os dados do cliente:" + "<br><br>";
		descricao = descricao + " <strong>Ordem de Serviço:</strong> " + rs2.getString("ID_ORDEM") + "<br>";
		descricao = descricao + " <strong>Cod. do CLiente:</strong> " + rs2.getString("CD_CLIENTE") + "<br>";
		descricao = descricao + " <strong>Razão Social:</strong> " + rs2.getString("RAZAO") + "<br>";
		descricao = descricao + " <strong>Fantasia:</strong> " + rs2.getString("FANTASIA") + "<br>";
		descricao = descricao + " <strong>CNPJ/CPF:</strong> " + rs2.getString("CGCCPF") + "<br>";
		descricao = descricao + " <strong>Telefone 1:</strong> " + rs2.getString("FONE1") + "<br>";
		descricao = descricao + " <strong>Telefone 2:</strong> " + rs2.getString("FONE2") + "<br>";
		descricao = descricao + " <strong>Endereço:</strong> " + rs2.getString("ENDERECO") + "<br>";
		descricao = descricao + " <strong>Responsável:</strong> " + rs2.getString("RESPONSAVEL") + "<br>";
		descricao = descricao + " <strong>Email:</strong> " + rs2.getString("EMAILRESP") + "<br>";
				
		// DADOS TAREFA ACESSO AO RASTREAMENTO
		
		titulo2 = "Acesso ao Rastreamento - " + rs2.getString("ID_ORDEM") + "  " + rs2.getString("RAZAO");
		
		descricao2 = "";
		descricao2 = descricao2 + "Segue os dados do cliente:" + "<br><br>";
		descricao2 = descricao + " <strong>Ordem de Serviço:</strong> " + rs2.getString("ID_ORDEM") + "<br>";
		descricao2 = descricao + " <strong>Cod. do CLiente:</strong> " + rs2.getString("CD_CLIENTE") + "<br>";
		descricao2 = descricao2 + " <strong>Razão Social:</strong> " + rs2.getString("RAZAO") + "<br>";
		descricao2 = descricao2 + " <strong>Fantasia:</strong> " + rs2.getString("FANTASIA") + "<br>";
		descricao2 = descricao2 + " <strong>CNPJ/CPF:</strong> " + rs2.getString("CGCCPF") + "<br>";
		descricao2 = descricao2 + " <strong>Telefone 1:</strong> " + rs2.getString("FONE1") + "<br>";
		descricao2 = descricao2 + " <strong>Telefone 2:</strong> " + rs2.getString("FONE2") + "<br>";
		descricao2 = descricao2 + " <strong>Endereço:</strong> " + rs2.getString("ENDERECO") + "<br>";
		descricao2 = descricao2 + " <strong>Responsável:</strong> " + rs2.getString("RESPONSAVEL") + "<br>";
		descricao2 = descricao2 + " <strong>Email:</strong> " + rs2.getString("EMAILRESP") + "<br>";

			
		final NeoRunnable work = new NeoRunnable(){
		    public void run() throws Exception{
			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
			log.warn("##### EXECUTAR AGENDADOR DE TAREFA:Abre Tarefa Simples Treinamento Rastreamentos - Tarefa: " + NeoUtils.safeString(tarefa));
		    }
		};
		
		PersistEngine.managedRun(work);
		
		final NeoRunnable work2 = new NeoRunnable(){
		    public void run() throws Exception{
			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			String tarefa2 = iniciarTarefaSimples.abrirTarefa(solicitante2, executor2, titulo2, descricao2, "1", "sim", prazo);
			log.warn("##### EXECUTAR AGENDADOR DE TAREFA:Abre Tarefa Simples Treinamento Rastreamentos - Tarefa: " + NeoUtils.safeString(tarefa2));
		    }
		};
		
		PersistEngine.managedRun(work2);
		
	    }
	    
	}
	catch (Exception e)
	{
	    log.error("##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Treinamento Rastreamentos");
	    System.out.println("["+key+"] ##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Treinamento Rastreamentos");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally
	{

	    try
	    {
		rs2.close();
		pstm2.close();
		conn.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }

	    log.warn("##### FIM AGENDADOR DE TAREFA: Abre Tarefa Simples Treinamento Rastreamentos - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
    }
}
