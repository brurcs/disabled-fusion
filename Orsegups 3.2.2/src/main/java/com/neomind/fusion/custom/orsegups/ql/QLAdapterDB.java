package com.neomind.fusion.custom.orsegups.ql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HorarioVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class QLAdapterDB
{
	
	/**
	 * Movimenta registros de centro de Custo
	 * @param numEmp
	 * @param tipCol
	 * @param numCad
	 * @param datAlt
	 * @param codCcu
	 * @param updateExistingValue
	 * @throws Exception 
	 */
	public static void movimentaCentroCusto(Long numEmp, Long tipCol, Long numCad, GregorianCalendar datAlt, String codCcu, Boolean updateExistingValue) throws Exception
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		try{
			System.out.println("[QLP TP] ["+key+"] - numcad: " + numCad + ", numemp: " + numEmp + ", codccu:" + codCcu + ", updateExistingValue?"+updateExistingValue + " - Iniciando movimentaCentroCusto" );
			GregorianCalendar data = (GregorianCalendar) datAlt.clone();
			data.set(GregorianCalendar.HOUR_OF_DAY, 0);
			data.set(GregorianCalendar.MINUTE, 0);
			data.set(GregorianCalendar.SECOND, 0);
			data.set(GregorianCalendar.MILLISECOND, 0);

			String sqlUpdate = "UPDATE R038HCC SET codccu = :codCcu WHERE numemp = :numEmp AND tipcol = :tipCol AND numcad = :numCad AND datalt = :datAlt  AND confin = 0 "; //AND stahis = 1

			String sqlInsert = "INSERT INTO R038HCC ( numemp, tipcol, numcad, datalt, codccu,  stahis,  confin) VALUES  (:numEmp, :tipCol, :numCad, :datAlt, :codCcu, 0, 0)";

			String sqlSelect = "SELECT count(*) FROM R038HCC WITH (NOLOCK) WHERE numemp = :numEmp AND tipcol = :tipCol AND numcad = :numCad AND datalt = :datAlt";

			Query selectQuery = QLAdapter.entityManager.createNativeQuery(sqlSelect);
			selectQuery.setParameter("numEmp", numEmp);
			selectQuery.setParameter("tipCol", tipCol);
			selectQuery.setParameter("numCad", numCad);
			selectQuery.setParameter("datAlt", data);
			selectQuery.setMaxResults(1);

			Integer count = (Integer) selectQuery.getSingleResult();
			System.out.println("[QLP TP] ["+key+"] - numcad: " + numCad + ", numemp: " + numEmp + ", codccu:" + codCcu + ", updateExistingValue?"+updateExistingValue + " - Existe R038HCC? " +count );

			if (count != null && count > 0 && updateExistingValue)
			{
				Query updateQuery = QLAdapter.entityManager.createNativeQuery(sqlUpdate);
				//update value
				updateQuery.setParameter("codCcu", codCcu);
				//where values
				updateQuery.setParameter("numEmp", numEmp);
				updateQuery.setParameter("tipCol", tipCol);
				updateQuery.setParameter("numCad", numCad);
				updateQuery.setParameter("datAlt", data);
				
				Integer result = updateQuery.executeUpdate();
				System.out.println("[QLP TP] ["+key+"] - numcad: " + numCad + ", numemp: " + numEmp + ", codccu:" + codCcu + ", updateExistingValue?"+updateExistingValue + " - Atualizou na R038HCC? " +result );

				QLAdapter.log.warn("Update: " + result);
			}
			else if(count == 0)
			{
				Query insertQuery = QLAdapter.entityManager.createNativeQuery(sqlInsert);
				//insert values
				insertQuery.setParameter("codCcu", codCcu);
				insertQuery.setParameter("numEmp", numEmp);
				insertQuery.setParameter("tipCol", tipCol);
				insertQuery.setParameter("numCad", numCad);
				insertQuery.setParameter("datAlt", data);

				Integer result = insertQuery.executeUpdate();
				System.out.println("[QLP TP] ["+key+"] - numcad: " + numCad + ", numemp: " + numEmp + ", codccu:" + codCcu + ", updateExistingValue?"+updateExistingValue + " - Inseriu na R038HCC? " +result );

				QLAdapter.log.warn("R038HCC - INSERT: " + result);
			}
		}catch(Exception e){
			System.out.println("[QLP TP] ["+key+"] - numcad: " + numCad + ", numemp: " + numEmp + ", codccu:" + codCcu + ", updateExistingValue?"+updateExistingValue + " - Erro " +e.getMessage() );
			throw new Exception(e); 
		}
		System.out.println("[QLP TP] ["+key+"] - numcad: " + numCad + ", numemp: " + numEmp + ", codccu:" + codCcu + ", updateExistingValue?"+updateExistingValue + " - Fim " );
	}
	
	/**
	 * Movimenta histórico de escala
	 * @param numEmp
	 * @param tipCol
	 * @param numCad
	 * @param datAlt
	 * @param codigoEscala
	 * @param codigoTurma
	 * @throws Exception 
	 */
	public static void movimentaHistoricoEscala(Long numEmp, Long tipCol, Long numCad, GregorianCalendar datAlt, Long codigoEscala, Long codigoTurma) throws Exception
	{
		try{
			GregorianCalendar data = (GregorianCalendar) datAlt.clone();
			data.set(GregorianCalendar.HOUR_OF_DAY, 0);
			data.set(GregorianCalendar.MINUTE, 0);
			data.set(GregorianCalendar.SECOND, 0);
			data.set(GregorianCalendar.MILLISECOND, 0);
			
			String sqlHistEscala = "Select count(*) From R038HES hes WITH (NOLOCK)" +
					"   Where hes.NumEmp =:numEmp " +
					"         AND hes.TipCol =:tipCol " +
					"         AND hes.NumCad =:numCad " +
					"         AND hes.DatAlt = (Select Max(Tab1.DatAlt) From R038HES Tab1 WITH (NOLOCK)" +
					"                        Where Tab1.NumEmp = hes.numemp " +
					"                          AND Tab1.TipCol = hes.tipcol " +
					"                          AND Tab1.NumCad = hes.numcad " +
					"                          AND Tab1.DatAlt <=:datAlt) " +
					"		 AND hes.CodEsc =:codigoEscala " +
					"         AND hes.CodTma =:codigoTurma";

			Query selectQuery = QLAdapter.entityManager.createNativeQuery(sqlHistEscala);
			selectQuery.setParameter("numEmp", numEmp);
			selectQuery.setParameter("tipCol", tipCol);
			selectQuery.setParameter("numCad", numCad);
			selectQuery.setParameter("datAlt", data);
			selectQuery.setParameter("codigoEscala", codigoEscala);
			selectQuery.setParameter("codigoTurma", codigoTurma);

			Integer resultSelect = (Integer) selectQuery.getSingleResult();

			//Se não existe histórico de escala igual, é necessário cadastrar ou atualizar.
			if(NeoUtils.safeIsNull(resultSelect) || resultSelect <= 0){
				String selectQueryHes = "Select count(*) From R038HES WITH (NOLOCK) " +
						" Where NumEmp =:numEmp " +
						" AND TipCol =:tipCol " +
						" AND NumCad =:numCad " +
						" AND DatAlt =:datAlt ";

				Query queryHes = QLAdapter.entityManager.createNativeQuery(selectQueryHes);
				queryHes.setParameter("numEmp", numEmp);
				queryHes.setParameter("tipCol", tipCol);
				queryHes.setParameter("numCad", numCad);
				queryHes.setParameter("datAlt", data);

				Integer resultHes = (Integer) queryHes.getSingleResult();

				//Se não encontrou registro, insere histórico de escala
				if(NeoUtils.safeIsNull(resultHes) || resultHes <= 0){
					String insertQueryHes = "INSERT Into R038HES ( NumEmp, TipCol, NumCad, DatAlt, CodEsc, CodTma, StaHis) " +
							"  Values (:numEmp, :tipCol, :numCad, :datAlt, :codigoEscala, :codigoTurma, 0) ";

					Query insertHes = QLAdapter.entityManager.createNativeQuery(insertQueryHes);
					insertHes.setParameter("numEmp", numEmp);
					insertHes.setParameter("tipCol", tipCol);
					insertHes.setParameter("numCad", numCad);
					insertHes.setParameter("datAlt", data);
					insertHes.setParameter("codigoEscala", codigoEscala);
					insertHes.setParameter("codigoTurma", codigoTurma);

					Integer result = insertHes.executeUpdate();

					QLAdapter.log.warn("R038HES - INSERT: " + result);

				}else{
					//Se encontrou registro, atualiza com novos dados
					String updateQueryHes = "UPDATE R038HES Set CodEsc =:codigoEscala, CodTma =:codigoTurma, StaHis = 0 " +
							"  Where NumEmp =:numEmp " +
							"   AND TipCol =:tipCol " +
							"   AND NumCad =:numCad " +
							"   AND DatAlt =:datAlt ";

					Query updateHes = QLAdapter.entityManager.createNativeQuery(updateQueryHes);
					updateHes.setParameter("codigoEscala", codigoEscala);
					updateHes.setParameter("codigoTurma", codigoTurma);
					updateHes.setParameter("numEmp", numEmp);
					updateHes.setParameter("tipCol", tipCol);
					updateHes.setParameter("numCad", numCad);
					updateHes.setParameter("datAlt", data);

					Integer result = updateHes.executeUpdate();

					QLAdapter.log.warn("R038HES - UPDATE: " + result);
				}
			}
		}catch(Exception e)
		{
			throw new Exception(e); 
		}
	}
	/**
	 * Função para manipular os registros da tabela R038HCC ajustando as datas e Centro de Custo antes da inserção dos novos registros.
	 * @param numEmp
	 * @param tipCol
	 * @param numCad
	 * @param datAlt
	 * @param datAltRetorno
	 * @param codCcuRetorno
	 * @param tipoAlteracao 1 - delete 2 - alter
	 * @throws Exception 
	 *
	 */
	public static void alteraRegistros(Long numEmp, Long tipCol, Long numCad, GregorianCalendar datAlt, Long tipoAlteracao) throws Exception
	{
		try 
		{
			String sqlCommand = "";
			//Excluir registros específicos
			if(tipoAlteracao == 1){
				sqlCommand = "DELETE FROM R038HCC WHERE numemp = :numEmp AND tipcol = :tipCol AND numcad = :numCad AND datalt = :datAlt ";
				Query deleteExistsRegisters = QLAdapter.entityManager.createNativeQuery(sqlCommand);
				deleteExistsRegisters.setParameter("numEmp", numEmp);
				deleteExistsRegisters.setParameter("tipCol", tipCol);
				deleteExistsRegisters.setParameter("numCad", numCad);
				deleteExistsRegisters.setParameter("datAlt", datAlt);
	
				Integer result = deleteExistsRegisters.executeUpdate();
				QLAdapter.log.warn("R038HCC - DELETED: " + result);
	
				//Alterar registros	
			}else if(tipoAlteracao == 2){
				sqlCommand = "UPDATE R038HCC SET codccu = :codCcu WHERE numemp = :numEmp AND tipcol = :tipCol AND numcad = :numCad AND datalt = :datAlt";
				Query updateExistsRegisters = QLAdapter.entityManager.createNativeQuery(sqlCommand);
				updateExistsRegisters.setParameter("numEmp", numEmp);
				updateExistsRegisters.setParameter("tipCol", tipCol);
				updateExistsRegisters.setParameter("numCad", numCad);
				updateExistsRegisters.setParameter("datAlt", datAlt);
	
				Integer result = updateExistsRegisters.executeUpdate();
				QLAdapter.log.warn("R038HCC - UPDATE: " + result);
	
				//Excluir registros futuros para operação TP 
			}else if(tipoAlteracao == 3){
				sqlCommand = "DELETE FROM R038HCC WHERE numemp = :numEmp AND tipcol = :tipCol AND numcad = :numCad AND datalt > :datAlt ";
				Query deleteExistsRegisters = QLAdapter.entityManager.createNativeQuery(sqlCommand);
				deleteExistsRegisters.setParameter("numEmp", numEmp);
				deleteExistsRegisters.setParameter("tipCol", tipCol);
				deleteExistsRegisters.setParameter("numCad", numCad);
				deleteExistsRegisters.setParameter("datAlt", datAlt);
	
				Integer result = deleteExistsRegisters.executeUpdate();
				QLAdapter.log.warn("R038HCC - DELETED: " + result);
			}
		} catch (Exception e) {
			throw new Exception(e); 
		}	
	}

	/**
	 * Insere registros de histórico de cobertura do colaborador
	 * @param numEmp
	 * @param tipCol
	 * @param numCad
	 * @param datAlt
	 * @param posto
	 * @throws Exception 
	 */
	public static void gravaHistoricoLocal(Long numEmp, Long tipCol, Long numCad, GregorianCalendar datAlt, NeoObject posto) throws Exception
	{
		try{
			GregorianCalendar data = (GregorianCalendar) datAlt.clone();
			data.set(GregorianCalendar.HOUR_OF_DAY, 0);
			data.set(GregorianCalendar.MINUTE, 0);
			data.set(GregorianCalendar.SECOND, 0);
			data.set(GregorianCalendar.MILLISECOND, 0);
			
			EntityWrapper postoWrapper = new EntityWrapper(posto);
			Long numLocPosto = (Long) postoWrapper.findValue("numloc");
			Long tabOrgPosto = (Long) postoWrapper.findValue("taborg");

			//Busca se existe históricos de local
			String sqlSelectHistoricoLocal = " Select count(*) From R038Hlo WITH (NOLOCK), R016ORN " +
					"   Where R038Hlo.NumEmp =:numEmp " +
					"     AND R038Hlo.TipCol =:tipCol " +
					"     AND R038Hlo.NumCad =:numCad " +
					"     AND R038Hlo.DatAlt =:datAlt " +
					"     AND R016ORN.TabOrg = R038Hlo.TabOrg " +
					"     AND R016ORN.NumLoc = R038Hlo.NumLoc ";

			Query selectExistsRegisters = QLAdapter.entityManager.createNativeQuery(sqlSelectHistoricoLocal);
			selectExistsRegisters.setParameter("numEmp", numEmp);
			selectExistsRegisters.setParameter("tipCol", tipCol);
			selectExistsRegisters.setParameter("numCad", numCad);
			selectExistsRegisters.setParameter("datAlt", data);
			selectExistsRegisters.setMaxResults(1);
			Integer selectExistsRegistersResult = (Integer) selectExistsRegisters.getSingleResult();

			if(selectExistsRegistersResult == 0){

				//Se não existe registro, vai ser inserido um histórico de local.
				String sqlInsertHistoricoLocal = "Insert Into R038Hlo( NumEmp, TipCol, NumCad, DatAlt, TabOrg, " +
						" NumLoc, ConToV, ConTos, StaHis, LocTra, MotAlt,usu_codusu) " +
						"  Values (:numEmp,:tipCol,:numCad,:datAlt,:tabOrgPosto,:numLocPosto, " +
						"  'N', 'N', 0, 0, 0,389)";

				Query selectInsertRegisters = QLAdapter.entityManager.createNativeQuery(sqlInsertHistoricoLocal);
				selectInsertRegisters.setParameter("numEmp", numEmp);
				selectInsertRegisters.setParameter("tipCol", tipCol);
				selectInsertRegisters.setParameter("numCad", numCad);
				selectInsertRegisters.setParameter("datAlt", data);
				selectInsertRegisters.setParameter("numLocPosto", numLocPosto);
				selectInsertRegisters.setParameter("tabOrgPosto", tabOrgPosto);

				Integer result = selectInsertRegisters.executeUpdate();
				QLAdapter.log.warn("R038HLO - INSERT: " + result);

			}else{
				//Se existe registro atualiza o histórico de local
				String sqlUpdateHistoricoLocal = "UPDATE R038HLO SET TabOrg = :tabOrgPosto, NumLoc = :numLocPosto, usu_codusu = 389, StaHis = 0 " +
						"  WHERE NumEmp = :numEmp " +
						"   AND TipCol = :tipCol " +
						"   AND NumCad = :numCad " +
						"   AND DatAlt = :datAlt ";

				Query selectUpdateRegisters = QLAdapter.entityManager.createNativeQuery(sqlUpdateHistoricoLocal);
				selectUpdateRegisters.setParameter("numEmp", numEmp);
				selectUpdateRegisters.setParameter("tipCol", tipCol);
				selectUpdateRegisters.setParameter("numCad", numCad);
				selectUpdateRegisters.setParameter("datAlt", data);
				selectUpdateRegisters.setParameter("numLocPosto", numLocPosto);
				selectUpdateRegisters.setParameter("tabOrgPosto", tabOrgPosto);

				Integer result = selectUpdateRegisters.executeUpdate();
				QLAdapter.log.warn("R038HLO - UPDATE: " + result);
			}
		}catch(Exception e)
		{
			throw new Exception(e); 
		}
	}

	/**
	 * Insere registros de histórico de cobertura do colaborador
	 * @param numEmp
	 * @param tipCol
	 * @param numCad
	 * @param datAlt
	 * @param posto
	 * @param sisCar
	 * @param nivelLocalCliente
	 * @throws Exception
	 */
	public static void gravaHistoricoFilial(Long numEmp, Long tipCol, Long numCad, GregorianCalendar datAlt, NeoObject posto, Long sisCar, String nivelLocalCliente) throws Exception
	{
		try{
			GregorianCalendar data = (GregorianCalendar) datAlt.clone();
			data.set(GregorianCalendar.HOUR_OF_DAY, 0);
			data.set(GregorianCalendar.MINUTE, 0);
			data.set(GregorianCalendar.SECOND, 0);
			data.set(GregorianCalendar.MILLISECOND, 0);			
			
			EntityWrapper postoWrapper = new EntityWrapper(posto);
			Long numLocPosto = (Long) postoWrapper.findValue("numloc");
			Long tabOrgPosto = (Long) postoWrapper.findValue("taborg");
			Long codRegPosto = (Long) postoWrapper.findValue("usu_codreg");

			String locCli = nivelLocalCliente.substring(0, 3);

			//Verifica qual a filial do posto a ser alterado
			String sqlSelect = "Select R016ORN.USU_CodFil, R074CID.EstCid " +
					" From R016ORN, R016HIE, R074CID " +
					" Where R016ORN.TabOrg = ? " +
					" AND R016ORN.NumLoc = ? " +
					" AND R016HIE.TabOrg = R016ORN.TabOrg " +
					" AND R016HIE.NumLoc = R016ORN.NumLoc " +
					" AND R074CID.CodCid = R016ORN.usu_codcid ";

//			Query query = QLAdapter.entityManager.createNativeQuery(sqlSelect);
//			query.setParameter("tabOrg", tabOrgPosto);
//			query.setParameter("numLoc", numLocPosto);
//			query.setMaxResults(1);
//
//			Object organograma = query.getSingleResult();
//			Long codigoFilialAtual = new Long(organograma.toString());
			Long codigoFilialAtual = null;
			String estado = null;
			
			Connection conn = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;

			try {
			    conn = PersistEngine.getConnection("VETORH");
			    pstm = conn.prepareStatement(sqlSelect);

			    pstm.setLong(1, tabOrgPosto);
			    pstm.setLong(2, numLocPosto);

			    rs = pstm.executeQuery();

			    while (rs.next()) {
				codigoFilialAtual = rs.getLong("USU_CodFil");
				estado = rs.getString("EstCid");
			    }

			} catch (SQLException e) {
			    e.printStackTrace();
			} finally {
			    OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			

			//Verifica se o código da Filial esta cadastrado no Grid de Organogramas no Rubi
			if (codigoFilialAtual == null || codigoFilialAtual.equals(0L)) {
				throw new Exception("A filial do posto não está preenchida no Cadastro de Organogramas do Rubi.");
			}
			
			if (estado == null){
			    throw new Exception("O estado do posto não está preenchido no Cadastro de Organogramas do Rubi.");
			}

			//Buscar no cadastro de tranferencia a filial parametrizada.
			if(locCli.equals("1.1"))
			{
				if (NeoUtils.safeIsNull(sisCar) || sisCar.equals(0L))
				{
					throw new Exception("O sistema de cargos do colaborador não está preenchido.");
				}
				
				Long newFilial = QLAdapterValidacao.searchFilialTransferencia(numEmp, sisCar, codRegPosto, estado);
				
				if(newFilial.equals(0L)){
					throw new Exception("Não foi encontrado Filial cadastrada em [QL] - Transferência de Filial.");
				}
				codigoFilialAtual = newFilial;
			}

			//Verifica a última transferência do colaborador

			String sqlSearchLastTransf = "Select empatu,CodFil From R038Hfi WITH (NOLOCK) " +
					"  Where NumEmp =:numEmp " +
					"    And TipCol =:tipCol " +
					"    And NumCad =:numCad " +
					"    And DatAlt = (Select Max(DatAlt) From R038Hfi Tab5 WITH (NOLOCK) " +
					"      Where Tab5.NumEmp =:numEmp " +
					"        And Tab5.TipCol =:tipCol " +
					"        And Tab5.NumCad =:numCad " +
					"        And Tab5.DatAlt <=:datAlt) ";

			Query selectRegisters = QLAdapter.entityManager.createNativeQuery(sqlSearchLastTransf);

			selectRegisters.setParameter("numEmp", numEmp);
			selectRegisters.setParameter("tipCol", tipCol);
			selectRegisters.setParameter("numCad", numCad);
			selectRegisters.setParameter("datAlt", data);

			Short ultEmpColab = 0;
			Integer ultFilColab = 0;

			Object result = selectRegisters.getSingleResult();
			if(NeoUtils.safeIsNotNull(result))
			{
				Object[] resultado = (Object[]) result;
				ultEmpColab = (Short) resultado[0];
				ultFilColab = (Integer) resultado[1];
			}

			//Só vai gravar histórico de filial se for diferente do último histórico de filial existente.
			if((ultEmpColab.longValue() != numEmp) || (ultFilColab.longValue() != codigoFilialAtual))
			{
				//Verifica histórico de colaborador
				String sqlSearchHistoricoFilial = " Select count(*) From R038Hfi WITH (NOLOCK) " +
						"   Where NumEmp =:numEmp " +
						"   AND TipCol =:tipCol " +
						"   AND NumCad =:numCad " +
						"   AND DatAlt =:datAlt ";

				Query selectRegistersHistorico = QLAdapter.entityManager.createNativeQuery(sqlSearchHistoricoFilial);

				selectRegistersHistorico.setParameter("numEmp", numEmp);
				selectRegistersHistorico.setParameter("tipCol", tipCol);
				selectRegistersHistorico.setParameter("numCad", numCad);
				selectRegistersHistorico.setParameter("datAlt", data);

				Integer qtdeRegistrosHistoricoFilial = (Integer) selectRegistersHistorico.getSingleResult();

				//Se não existe histórico insere registro
				if(qtdeRegistrosHistoricoFilial == 0){

					String sqlInsertHistoricoFilial = " Insert Into R038Hfi ( NumEmp, TipCol, NumCad, DatAlt, EmpAtu, " +
							"   CodFil, TabOrg, NumLoc, CadAtu, TipAdm, FicReg,usu_codusu, StaHis) " +
							" Values (:numEmp,:tipCol,:numCad,:datAlt,:numEmp,:codigoFilialAtual, " +
							"  :tabOrgPosto,:numLocPosto,:numCad, 2, :numCad, 389, 0) ";

					Query queryInsertHistoricoFilial = QLAdapter.entityManager.createNativeQuery(sqlInsertHistoricoFilial);

					queryInsertHistoricoFilial.setParameter("numEmp", numEmp);
					queryInsertHistoricoFilial.setParameter("tipCol", tipCol);
					queryInsertHistoricoFilial.setParameter("numCad", numCad);
					queryInsertHistoricoFilial.setParameter("datAlt", data);
					queryInsertHistoricoFilial.setParameter("numLocPosto", numLocPosto);
					queryInsertHistoricoFilial.setParameter("tabOrgPosto", tabOrgPosto);
					queryInsertHistoricoFilial.setParameter("codigoFilialAtual", codigoFilialAtual);

					Integer qdeInsert = queryInsertHistoricoFilial.executeUpdate();
					QLAdapter.log.warn("R038HFI - INSERT: " + qdeInsert);

					//Se existe registro de histórico de filial, atualiza o mesmo
				}else{
					String sqlUpdateHistoricoFilial = " UPDATE R038Hfi SET TabOrg = :tabOrgPosto, " +
							"   NumLoc = :numLocPosto, CodFil = :codigoFilialAtual, " +
							"   usu_codusu = 389, StaHis = 0 " +
							"     WHERE NumEmp = :numEmp " +
							"       AND TipCol = :tipCol " +
							"       AND NumCad = :numCad " +
							"       AND DatAlt = :datAlt ";

					Query queryUpdateHistoricoFilial = QLAdapter.entityManager.createNativeQuery(sqlUpdateHistoricoFilial);

					queryUpdateHistoricoFilial.setParameter("numEmp", numEmp);
					queryUpdateHistoricoFilial.setParameter("tipCol", tipCol);
					queryUpdateHistoricoFilial.setParameter("numCad", numCad);
					queryUpdateHistoricoFilial.setParameter("datAlt", data);
					queryUpdateHistoricoFilial.setParameter("numLocPosto", numLocPosto);
					queryUpdateHistoricoFilial.setParameter("tabOrgPosto", tabOrgPosto);
					queryUpdateHistoricoFilial.setParameter("codigoFilialAtual", codigoFilialAtual);

					Integer qdeUpdate = queryUpdateHistoricoFilial.executeUpdate();
					QLAdapter.log.warn("R038HFI - INSERT: " + qdeUpdate);

				}
			}
		}catch(Exception e)
		{
			throw new Exception(e); 
		}
	}

	/**
	 * Responsável por inserir os dados na tabela USU_T038CobFun - Momvimentação de Histórico
	 * @param numEmp
	 * @param tipCol
	 * @param numCad
	 * @param datAlt
	 * @param datRetorno
	 * @param codigoEscala
	 * @param codigoTurma
	 * @param posto
	 * @param horaIni
	 * @param horaFim
	 * @param codMotivo
	 * @param observacao
	 * @param colaborador
	 * @param colaboradorSubstituirdt
	 * @throws Exception 
	 */
	public static void insertHistoricoMovimentacao(Long numEmp, Long tipCol, Long numCad, Long numEmpCob, Long tipColCob, Long numCadCob, GregorianCalendar datAlt, GregorianCalendar datRetorno,
			GregorianCalendar datIniInv, GregorianCalendar datFimInv, Long codigoEscala, Long codigoTurma, NeoObject posto, int horaIni, int horaFim, Integer codMotivo, String observacao,
			NeoObject colaborador, NeoObject colaboradorSubstituir, String tipoOperacao, String idNexti) throws Exception
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		
		try{

			Long numLocPostoTra = null;
			Long tabOrgPostoTra = null;
			Long numlocColab = null;
			Long tabOrgColab = null;

			if(NeoUtils.safeIsNotNull(posto)){
				EntityWrapper postoWrapper = new EntityWrapper(posto);
				numLocPostoTra = (Long) postoWrapper.findValue("numloc");
				tabOrgPostoTra = (Long) postoWrapper.findValue("taborg");
			}

			if(NeoUtils.safeIsNotNull(colaborador)){
				EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
				numlocColab = (Long) colaboradorWrapper.findValue("numloc");
				tabOrgColab = (Long) colaboradorWrapper.findValue("taborg");
			}

			if(tipoOperacao.contains("CC")){
				if(NeoUtils.safeIsNotNull(colaborador)){
					EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
					numlocColab = (Long) colaboradorWrapper.findValue("numloc");
					tabOrgColab = (Long) colaboradorWrapper.findValue("taborg");
				}
				if(NeoUtils.safeIsNotNull(colaboradorSubstituir)){
					EntityWrapper colaboradorSubWrapper = new EntityWrapper(colaboradorSubstituir);
					numLocPostoTra = (Long) colaboradorSubWrapper.findValue("numloc");
					tabOrgPostoTra = (Long) colaboradorSubWrapper.findValue("taborg");
				}
			}
			if(tipoOperacao.contains("CP") || tipoOperacao.contains("TP")){
				if(NeoUtils.safeIsNotNull(colaborador)){
					EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
					numlocColab = (Long) colaboradorWrapper.findValue("numloc");
					tabOrgColab = (Long) colaboradorWrapper.findValue("taborg");
				}
				if(NeoUtils.safeIsNotNull(posto)){
					EntityWrapper postoWrapper = new EntityWrapper(posto);
					numLocPostoTra = (Long) postoWrapper.findValue("numloc");
					tabOrgPostoTra = (Long) postoWrapper.findValue("taborg");
				}
			}
			if(tipoOperacao.contains("TH")){
				if (codMotivo == 16)
				{
					if(NeoUtils.safeIsNotNull(colaborador)){
						EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
						numLocPostoTra = (Long) colaboradorWrapper.findValue("numloc");
						tabOrgPostoTra = (Long) colaboradorWrapper.findValue("taborg");
					} 
				}
				else
				{
					//Regra invertida para Inversão de escala quando possui cobertura de férias
					if(NeoUtils.safeIsNotNull(colaborador)){
						EntityWrapper colaboradorSubWrapper = new EntityWrapper(colaboradorSubstituir);
						numLocPostoTra = (Long) colaboradorSubWrapper.findValue("numloc");
						tabOrgPostoTra = (Long) colaboradorSubWrapper.findValue("taborg");
					} 
					else
					{
						EntityWrapper colaboradorSubWrapper = new EntityWrapper(colaboradorSubstituir);
						numLocPostoTra = (Long) colaboradorSubWrapper.findValue("numloc");
						tabOrgPostoTra = (Long) colaboradorSubWrapper.findValue("taborg");
					}
				}
			}

			String sqlSelectExists = "SELECT count(*) FROM USU_T038CobFun " +
					" WHERE USU_NumEmp = :numEmp AND USU_TipCol = :tipCol AND USU_NumCad = :numCad AND " +
					" USU_DatAlt = :datAlt AND USU_horIni = :horaIni";

			Query sqlSelect = QLAdapter.entityManager.createNativeQuery(sqlSelectExists);

			sqlSelect.setParameter("numEmp", numEmp);
			sqlSelect.setParameter("tipCol", tipCol);
			sqlSelect.setParameter("numCad", numCad);
			sqlSelect.setParameter("datAlt", datAlt);
			sqlSelect.setParameter("horaIni", horaIni);

			Integer registroExistente = (Integer) sqlSelect.getSingleResult();
			String sqlInsertUpdate = "";
			if(datAlt.after(datRetorno)){
				throw new WorkflowException("Falha no sistema - Tentando inserir Data Inicial maior que a Data Final. Entre em contato com o suporte.");
			}
			if(registroExistente == 0){
				sqlInsertUpdate = "INSERT INTO USU_T038CobFun ( USU_NumEmp, USU_TipCol, USU_NumCad, USU_DatAlt, USU_HorIni, USU_CodMot, USU_tabOrg," +
						" USU_NumLoc, USU_DatFim, USU_NumEmpCob, USU_TipColCob, USU_NumCadCob, USU_ObsCob, USU_TabOrgTra, USU_codUsuInc," +
						" USU_NumLocTra, USU_DatIncCob, USU_HorIncCob, USU_CodEsc, USU_CodTma, USU_ObsCobFun, USU_SeqAlt, USU_RetCob, USU_HorFim," +
						" USU_datIniRet, USU_datFimRet, USU_nextid) " +
						"   VALUES (:numEmp, :tipCol, :numCad, :datAlt, :horaIni, :codMotivo, :tabOrg, :numLoc, :dataFim, :numEmpCob, " +
						"           :tipColCob, :numCadCob, :observacao, :tabOrgTra, 389, :numLocTra, :datIncCob, :horIncCob, :codEscala, :codTurma, " +
						"           :observacao, 0, 'N', :horaFim, :datIniInv, :datFimInv, :nextid ) ";
			}else{
				sqlInsertUpdate = " UPDATE USU_T038CobFun SET USU_NumEmp =:numEmp, USU_TipCol =:tipCol, " +
						"    USU_NumCad =:numCad, USU_DatAlt =:datAlt, USU_HorIni =:horaIni, USU_CodMot =:codMotivo," +
						"    USU_tabOrg =:tabOrg , USU_NumLoc =:numLoc, USU_DatFim =:dataFim, USU_NumEmpCob =:numEmpCob, " +
						"    USU_TipColCob =:tipColCob, USU_NumCadCob =:numCadCob, USU_ObsCob =:observacao, " +
						"    USU_TabOrgTra =:tabOrgTra, USU_codUsuInc = 389, USU_NumLocTra =:numLocTra, " +
						"    USU_DatIncCob =:datIncCob, USU_HorIncCob =:horIncCob, USU_CodEsc =:codEscala, USU_CodTma =:codTurma, " +
						"    USU_ObsCobFun =:observacao, USU_SeqAlt = 0, USU_RetCob = 'N', USU_HorFim =:horaFim," +
						"    USU_datIniRet =:datIniInv, USU_datFimRet =:datFimInv, USU_nextid = :nextid " +
						" WHERE USU_numemp =:numEmp AND USU_tipcol =:tipCol AND USU_numcad =:numCad AND USU_datalt =:datAlt AND USU_horIni =:horaIni ";
			}

			Query insertHistorico = QLAdapter.entityManager.createNativeQuery(sqlInsertUpdate);

			//Fixos
			insertHistorico.setParameter("numEmp", numEmp);
			insertHistorico.setParameter("tipCol", tipCol);
			insertHistorico.setParameter("numCad", numCad);
			insertHistorico.setParameter("datAlt", datAlt);
			insertHistorico.setParameter("horaIni", horaIni);
			insertHistorico.setParameter("codMotivo", codMotivo);
			insertHistorico.setParameter("tabOrgTra", tabOrgPostoTra);
			insertHistorico.setParameter("numLocTra", numLocPostoTra);
			insertHistorico.setParameter("tabOrg", tabOrgColab);
			insertHistorico.setParameter("numLoc", numlocColab);
			insertHistorico.setParameter("observacao", observacao);
			insertHistorico.setParameter("dataFim", datRetorno);
			insertHistorico.setParameter("datIncCob", datAlt);
			insertHistorico.setParameter("horIncCob", horaIni);
			insertHistorico.setParameter("codEscala", codigoEscala);
			insertHistorico.setParameter("codTurma", codigoTurma);
			insertHistorico.setParameter("numEmpCob", numEmpCob != null ? numEmpCob : 0);
			insertHistorico.setParameter("tipColCob", tipColCob != null ? tipColCob : 0);
			insertHistorico.setParameter("numCadCob", numCadCob != null ? numCadCob : 0);
			insertHistorico.setParameter("horaFim", horaFim);
			insertHistorico.setParameter("datIniInv", datIniInv);
			insertHistorico.setParameter("datFimInv", datFimInv);
			insertHistorico.setParameter("nextid", idNexti);

			Integer insertReg = insertHistorico.executeUpdate();
			QLAdapter.log.warn("USU_T038CobFun - INSERT: " + insertReg);
		}
		catch (WorkflowException e)
		{
			throw new WorkflowException(e.getErrorList().get(0).toString());
		}
		catch (Exception e)
		{
			throw new Exception(e); 
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
	}
	/**
	 * Deleta os registros de cobertura
	 * @param numEmp
	 * @param tipCol
	 * @param numCad
	 * @param dataInicial
	 * @param horaInicial
	 * @throws Exception
	 */
	public static void deleteRegistroCobertura(String numEmp, String tipCol, String numCad, String dataInicial, String horaInicial) throws Exception{
		Integer result = 0;
		try{
			
			deleteRegistroCoberturaTIDB(numEmp, tipCol, numCad, dataInicial, horaInicial);

			String deleteCobertura = "DELETE FROM USU_T038CobFun " +
					" WHERE USU_NumEmp =:numEmp " +
					"   AND USU_TipCol =:tipCol " +
					"   AND USU_NumCad =:numCad " +
					"   AND CONVERT(varchar, USU_DatAlt, 103) =:dataInicial " +
					"   AND USU_HorIni =:horaInicial ";

			Query queryDeleteCobertura = QLAdapter.entityManager.createNativeQuery(deleteCobertura);
			queryDeleteCobertura.setParameter("numEmp", numEmp);
			queryDeleteCobertura.setParameter("tipCol", tipCol);
			queryDeleteCobertura.setParameter("numCad", numCad);
			queryDeleteCobertura.setParameter("dataInicial", dataInicial);
			queryDeleteCobertura.setParameter("horaInicial", horaInicial);

			result = queryDeleteCobertura.executeUpdate();
			QLAdapter.log.warn("USU_T038CobFun - DELETED: " + result);
			
			//Excluir Autorizacao de Hora Extra
			String deleteHoraExtra = " DELETE FROM R064EXT " +
					"   WHERE numemp = :numEmp AND " +
					"         tipcol = :tipCol AND " +
					"         numcad = :numCad AND " +
					"         CONVERT(varchar, datIni, 103) = :datIni AND " +
					"         horini = :hora " ;
			Query deleteHERegisters = QLAdapter.entityManager.createNativeQuery(deleteHoraExtra);
			deleteHERegisters.setParameter("numEmp", numEmp);
			deleteHERegisters.setParameter("tipCol", tipCol);
			deleteHERegisters.setParameter("numCad", numCad);
			deleteHERegisters.setParameter("datIni", dataInicial);
			deleteHERegisters.setParameter("horIni", horaInicial);

			Integer result2 = deleteHERegisters.executeUpdate();
			QLAdapter.log.warn("R064EXT - DELETED: " + result2);
			
			//Excluir Afastamento Situacao 500
			String deleteAfastamento = " DELETE FROM R038AFA " +
					"   	  WHERE NumEmp = :numEmp AND " +
					"         TipCol = :tipCol AND " +
					"         NumCad = :numCad AND " +
					"         CONVERT(varchar, DatAfa, 103) = :datAfa AND " +
					"         HorAfa = 0 AND " +
					"		  SitAfa = 500 " ;
			Query deleteAfastamentoRegisters = QLAdapter.entityManager.createNativeQuery(deleteAfastamento);
			deleteAfastamentoRegisters.setParameter("numEmp", numEmp);
			deleteAfastamentoRegisters.setParameter("tipCol", tipCol);
			deleteAfastamentoRegisters.setParameter("numCad", numCad);
			deleteAfastamentoRegisters.setParameter("datAfa", dataInicial);

			Integer result3 = deleteAfastamentoRegisters.executeUpdate();
			QLAdapter.log.warn("R038AFA - DELETED: " + result3);
			
			//Excluir Troca de Escala
			String deleteTroca = " DELETE FROM R064TES " +
					"   	  WHERE NumEmp = :numEmp AND " +
					"         TipCol = :tipCol AND " +
					"         NumCad = :numCad AND " +
					"         DatIni = DATEADD(dd, 0, DATEDIFF(dd, 0, :datIni)) ";
			Query deleteTrocaRegisters = QLAdapter.entityManager.createNativeQuery(deleteTroca);
			deleteTrocaRegisters.setParameter("numEmp", numEmp);
			deleteTrocaRegisters.setParameter("tipCol", tipCol);
			deleteTrocaRegisters.setParameter("numCad", numCad);
			deleteTrocaRegisters.setParameter("datIni", dataInicial);

			Integer result4 = deleteTrocaRegisters.executeUpdate();
			QLAdapter.log.warn("R064TES - DELETED: " + result4);
			
			//Excluir Troca de Rateio
			String deleteRateio = " DELETE FROM R064RAT " +
					"   	  WHERE NumEmp = :numEmp AND " +
					"         TipCol = :tipCol AND " +
					"         NumCad = :numCad AND " +
					"         DatIni = DATEADD(dd, 0, DATEDIFF(dd, 0, :datIni)) ";
			Query deleteRateioRegisters = QLAdapter.entityManager.createNativeQuery(deleteRateio);
			deleteRateioRegisters.setParameter("numEmp", numEmp);
			deleteRateioRegisters.setParameter("tipCol", tipCol);
			deleteRateioRegisters.setParameter("numCad", numCad);
			deleteRateioRegisters.setParameter("datIni", dataInicial);
			deleteRateioRegisters.setParameter("horIni", horaInicial);

			Integer result5 = deleteRateioRegisters.executeUpdate();
			QLAdapter.log.warn("R064RAT - DELETED: " + result5);
		}
		catch (Exception e)
		{
			throw new Exception(e); 
		}
	}
	
	/**
	 * Remove os autorizaçõesde horas extras da TIDB.
	 * @param numEmp
	 * @param tipCol
	 * @param numCad
	 * @param dataInicial
	 * @param horaInicial
	 * @throws Exception
	 */
	public static void deleteRegistroCoberturaTIDB(String numEmp, String tipCol, String numCad, String dataInicial, String horaInicial) throws Exception{
		EntityManager entityManager = PersistEngine.getEntityManager("TIDB");
		try{
			
			//Excluir Autorizacao de Hora Extra TIDB
			String deleteHoraExtra = " DELETE FROM R064EXT " +
					"   WHERE numemp = :numEmp AND " +
					"         tipcol = :tipCol AND " +
					"         numcad = :numCad AND " +
					"         CONVERT(varchar, datIni, 103) = :datIni AND " +
					"         horini = :hora " ;
			Query deleteHERegisters = entityManager.createNativeQuery(deleteHoraExtra);
			deleteHERegisters.setParameter("numEmp", numEmp);
			deleteHERegisters.setParameter("tipCol", tipCol);
			deleteHERegisters.setParameter("numCad", numCad);
			deleteHERegisters.setParameter("datIni", dataInicial);
			deleteHERegisters.setParameter("horIni", horaInicial);

			Integer result = deleteHERegisters.executeUpdate();
			QLAdapter.log.warn("R064EXT[TIDB] - DELETED: " + result);
			
		}catch(Exception e){
			QLAdapter.log.error(" Erro ao remover autorização de horas extras da TIDB");
			throw new Exception(e); 
		}
	}
	
	public static void insertSituacaoIndefinida(NeoObject colaboradorSubstituir, GregorianCalendar datini, GregorianCalendar datfim) throws Exception {
		
		QLAdapter.entityManager = PersistEngine.getEntityManager("VETORH");
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try {
			
			Long numemp = 0L;
			Long tipcol = 0L;
			Long numcad = 0L;
			
			if(NeoUtils.safeIsNotNull(colaboradorSubstituir)){
				EntityWrapper colaboradorSubWrapper = new EntityWrapper(colaboradorSubstituir);
				numemp = (Long) colaboradorSubWrapper.findValue("numemp");
				tipcol = (Long) colaboradorSubWrapper.findValue("tipcol");
				numcad = (Long) colaboradorSubWrapper.findValue("numcad");
			}
			QLAdapter.log.warn("["+key+"] - inserir afastamento para o colaborador  " + numcad);
			
			GregorianCalendar dataAfastamento = (GregorianCalendar) datini.clone();
			GregorianCalendar dataAtual = new GregorianCalendar();
			
			datini.set(Calendar.HOUR_OF_DAY, 0);
			datini.set(Calendar.MINUTE, 0);
			datini.set(Calendar.SECOND, 0);
			
			datfim.set(Calendar.HOUR_OF_DAY, 0);
			datfim.set(Calendar.MINUTE, 0);
			datfim.set(Calendar.SECOND, 0);
			
			String afastamentoInformado = null;
			long horas = (dataAfastamento.getTimeInMillis() - dataAtual.getTimeInMillis()) / 3600000;
			
			if (horas >= 6L)
			{
				afastamentoInformado = "S";
			}
			
			GregorianCalendar dataNula = new GregorianCalendar(1900, GregorianCalendar.DECEMBER, 31);
			
			StringBuilder buscaAfastamento = new StringBuilder();
			
			buscaAfastamento.append(" SELECT COUNT(*) FROM R038AFA WHERE numemp = :numemp ");
			buscaAfastamento.append(" AND tipcol = :tipcol ");
			buscaAfastamento.append(" AND numcad = :numcad ");
			buscaAfastamento.append(" AND datafa <= :datini ");
			buscaAfastamento.append(" AND (datter >= :datini OR datter = :dataNula) ");
			
			Query queryBuscaAfa = QLAdapter.entityManager.createNativeQuery(buscaAfastamento.toString());
			queryBuscaAfa.setParameter("numemp", numemp);
			queryBuscaAfa.setParameter("tipcol", tipcol);
			queryBuscaAfa.setParameter("numcad", numcad);
			queryBuscaAfa.setParameter("datini", datini);
			queryBuscaAfa.setParameter("datini", datini);
			queryBuscaAfa.setParameter("dataNula", dataNula);
			queryBuscaAfa.setMaxResults(1);

			Integer count = (Integer) queryBuscaAfa.getSingleResult();
			
			if (count != null && count > 0) {
				return;
			} 
			else 
			{
				NeoUser user = PortalUtil.getCurrentUser();
				StringBuilder insereAfastamento = new StringBuilder();
				
				String dataString = NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm");
				String ObsAfa = "Inserido na cobertura por " + user.getFullName() + " em " + dataString;
				
				insereAfastamento.append(" Insert Into R038AFA (NumEmp, TipCol, NumCad, DatAfa, HorAfa, DatTer, SitAfa, OriAfa, ObsAfa, StaAtu, usu_codusu, usu_afainf, usu_datmod, usu_hormod) ");
				insereAfastamento.append(" Values (:numemp,:tipcol,:numcad,:datini,0,:datini,500,0,:ObsAfa,1,389,:afainf,");
				insereAfastamento.append("		DATEADD(dd, 0, DATEDIFF(dd, 0, :datMod1)), (DATEPART(HOUR, :datMod2) * 60) + DATEPART(MINUTE, :datMod3)) ");
				
				Query queryInsereAfa = QLAdapter.entityManager.createNativeQuery(insereAfastamento.toString());
				
				queryInsereAfa.setParameter("numemp", numemp);
				queryInsereAfa.setParameter("tipcol", tipcol);
				queryInsereAfa.setParameter("numcad", numcad);
				queryInsereAfa.setParameter("datini", datini);
				queryInsereAfa.setParameter("datini", datini);
				queryInsereAfa.setParameter("ObsAfa", ObsAfa);
				queryInsereAfa.setParameter("afainf", afastamentoInformado);
				GregorianCalendar datMod = new GregorianCalendar();
				queryInsereAfa.setParameter("datMod1", new Timestamp(datMod.getTimeInMillis()));
				queryInsereAfa.setParameter("datMod2", new Timestamp(datMod.getTimeInMillis()));
				queryInsereAfa.setParameter("datMod3", new Timestamp(datMod.getTimeInMillis()));
				
				int qtde = queryInsereAfa.executeUpdate();
				
				String txtLog = "";
				
				if (datfim.after(datini)) 
				{
					txtLog = "Inserido afastamento por cobertura de " + NeoUtils.safeDateFormat(datini, "dd/MM/yyyy") + " até " + NeoUtils.safeDateFormat(datfim, "dd/MM/yyyy");
				} else {
					txtLog = "Inserido afastamento por cobertura em " + NeoUtils.safeDateFormat(datini, "dd/MM/yyyy");
				}
				
				String buscaCpf = " SELECT numcpf FROM R034FUN WHERE numemp = ? AND tipcol = ? AND numcad = ? ";
				
				PreparedStatement stCpf = connVetorh.prepareStatement(buscaCpf);
				stCpf.setLong(1, numemp);
				stCpf.setLong(2, tipcol);
				stCpf.setLong(3, numcad);
				
				ResultSet rsCpf = stCpf.executeQuery();
				
				if (rsCpf.next())
				{
					saveLog(txtLog, null, rsCpf.getString("numcpf"), "colaborador");
					QLAdapter.log.warn("["+key+"] - [inserido? "+qtde+"] Cadastro Afastamento - NumEmp:" + numemp + " - NumCad: " + numcad + ")");
				}
				
				rsCpf.close();
				stCpf.close();
			}
		} 
		catch (Exception e)
		{
			QLAdapter.log.error("["+key+"] - Erro ao inserir afastamento de situacação indefinida");
			throw new Exception(e); 
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
	}
	
	public static void saveLog(String texto, NeoObject posto, String idColaborador, String tipo) throws Exception
	{
		try 
		{
			if (tipo != null && tipo.equalsIgnoreCase("posto"))
			{

				if (posto != null)
				{
					NeoObject logPosto = AdapterUtils.createNewEntityInstance("QLLogPresencaPosto");

					if (logPosto != null)
					{
						EntityWrapper postoWrapper = new EntityWrapper(logPosto);
						postoWrapper.setValue("posto", posto);
						postoWrapper.setValue("dataLog", new GregorianCalendar());
						postoWrapper.setValue("textoLog", texto);
						postoWrapper.setValue("usuario", PortalUtil.getCurrentUser());

						PersistEngine.persist(logPosto);
					}
				}
			}
			else if (tipo != null && tipo.equalsIgnoreCase("colaborador"))
			{
				NeoObject log = AdapterUtils.createNewEntityInstance("QLLogPresencaColaborador");

				if (log != null && idColaborador != null)
				{
					
					EntityWrapper logWrapper = new EntityWrapper(log);
					logWrapper.setValue("cpf", Long.valueOf(idColaborador));
					logWrapper.setValue("dataLog", new GregorianCalendar());
					logWrapper.setValue("textoLog", texto);
					logWrapper.setValue("usuario", PortalUtil.getCurrentUser());

					if (posto != null)
					{
						logWrapper.setValue("posto", posto);
					}

					PersistEngine.persist(log);
				}
			}
			
		} catch (Exception e) {
			throw new Exception(e); 
		}
	}
	
	public static void insertAutorizacaoHE(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, Integer horini, GregorianCalendar dataFim, Integer horfim) throws Exception
	{
		try
		{		
			EntityManager entityManager = PersistEngine.getEntityManager("TIDB");
			
			StringBuffer sqlHE = new StringBuffer();
			
			GregorianCalendar datini = (GregorianCalendar) dataInicio.clone();
			GregorianCalendar datfim = (GregorianCalendar) dataFim.clone();
			
			datini.set(Calendar.HOUR_OF_DAY, 0);
			datini.set(Calendar.MINUTE, 0);
			datini.set(Calendar.SECOND, 0);
			datini.set(Calendar.MILLISECOND, 0);
			
			datfim.set(Calendar.HOUR_OF_DAY, 0);
			datfim.set(Calendar.MINUTE, 0);
			datfim.set(Calendar.SECOND, 0);
			datfim.set(Calendar.MILLISECOND, 0);
			
			sqlHE.append(" INSERT INTO R064EXT (NumEmp, TipCol, NumCad, DatIni, HorIni, DatFim, HorFim, StaAcc) ");
			sqlHE.append(" 				VALUES (:numemp,:tipcol,:numcad, :datini,:horini,:datfim,:horfim, 0) ");
			
			String buffer = "INSERT INTO R064EXT (NumEmp, TipCol, NumCad, DatIni, HorIni, DatFim, HorFim, StaAcc) ";
			buffer += " 				VALUES ("+numemp+","+tipcol+", "+numcad+", " + datini +", " +horini + ", " +datfim+", "+ horfim + ", 0) " ;
			
			QLAdapter.log.warn("sql de insert: "+ buffer);
			
			Query queryInsereTE = entityManager.createNativeQuery(sqlHE.toString());
			
			queryInsereTE.setParameter("numemp", numemp.toString());
			queryInsereTE.setParameter("tipcol", tipcol.toString());
			queryInsereTE.setParameter("numcad", numcad.toString());
			queryInsereTE.setParameter("datini", datini);
			queryInsereTE.setParameter("horini", horini.toString());
			queryInsereTE.setParameter("datfim", datfim);
			queryInsereTE.setParameter("horfim", horfim.toString());
			
			Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
			
			QLAdapter.log.warn("["+timeExec+"]insertAutorizacaoHE - start"); 
			queryInsereTE.executeUpdate();
			QLAdapter.log.warn("["+timeExec+"]insertAutorizacaoHE - numcad:"+numcad+" - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		}
		catch (WorkflowException e)
		{
			throw new WorkflowException(e.getErrorList().get(0).toString());
		}
		catch (Exception e)
		{
			QLAdapter.log.warn("Erro ao inserir autorização de HE, msg:"+e.getMessage());
			throw new Exception(e); 
		}
	}
	
	/**
	 * verifica na r064ext do vetor e TIDB se há autorizações de horas extras.
	 * @param numemp
	 * @param tipcol
	 * @param numcad
	 * @param dataInicio
	 * @param horaIni
	 * @return
	 */
	public static Boolean possuiAutorizacaoHE(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, Integer horaIni)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		Boolean possui = false;
		
		try
		{
			StringBuffer query = new StringBuffer();
			query.append(" SELECT ext.NumCad FROM R064EXT ext ");
			query.append(" WHERE ext.NumEmp = ? AND ext.TipCol = ? AND ext.NumCad = ? AND ext.DatIni = cast(floor(cast(? as float)) as datetime) AND HorIni = ? ");

			Timestamp dataInicioTS = new Timestamp(dataInicio.getTimeInMillis());
			PreparedStatement st = connVetorh.prepareStatement(query.toString());
			
			st.setLong(1, numemp);
			st.setLong(2, tipcol);
			st.setLong(3, numcad);
			st.setTimestamp(4, dataInicioTS);
			st.setLong(5, horaIni.longValue());

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				possui = true;
			}else{
				possui = possuiAutorizacaoHETIDB(numemp, tipcol, numcad, dataInicio, horaIni);
			}
			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

		return possui;
	}
	
	private static Boolean possuiAutorizacaoHETIDB(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, Integer horaIni)
	{
		Connection connVetorh = PersistEngine.getConnection("TIDB");
		Boolean possui = false;
		
		try
		{
			StringBuffer query = new StringBuffer();
			query.append(" SELECT ext.NumCad FROM R064EXT ext ");
			query.append(" WHERE ext.NumEmp = ? AND ext.TipCol = ? AND ext.NumCad = ? AND ext.DatIni = cast(floor(cast(? as float)) as datetime) AND HorIni = ? ");

			Timestamp dataInicioTS = new Timestamp(dataInicio.getTimeInMillis());
			PreparedStatement st = connVetorh.prepareStatement(query.toString());
			
			st.setLong(1, numemp);
			st.setLong(2, tipcol);
			st.setLong(3, numcad);
			st.setTimestamp(4, dataInicioTS);
			st.setLong(5, horaIni.longValue());

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				possui = true;
			}
			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

		return possui;
	}
	
	public static String buscaCodCcu(Long taborg, Long numloc)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		String codccu = "";

		try
		{
			StringBuffer query = new StringBuffer();
			query.append(" SELECT orn.USU_CodCCU, orn.USU_LotOrn, orn.NomLoc FROM R016ORN orn ");
			query.append(" WHERE orn.TabOrg = ? AND orn.NumLoc = ? ");

			PreparedStatement st = connVetorh.prepareStatement(query.toString());
			st.setLong(1, taborg);
			st.setLong(2, numloc);

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				codccu = rs.getString("USU_CodCCU");
			}
			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
		return codccu;
	}
	
	public static Boolean possuiAutorizacaoRateio(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, Integer horaIni)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		Boolean possui = false;
		
		try
		{
			StringBuffer query = new StringBuffer();
			query.append(" SELECT rat.NumCad FROM R064RAT rat ");
			query.append(" WHERE rat.NumEmp = ? AND rat.TipCol = ? AND rat.NumCad = ? AND rat.DatIni = cast(floor(cast(? as float)) as datetime) AND rat.HorIni = ? ");

			Timestamp dataInicioTS = new Timestamp(dataInicio.getTimeInMillis());
			PreparedStatement st = connVetorh.prepareStatement(query.toString());
			
			st.setLong(1, numemp);
			st.setLong(2, tipcol);
			st.setLong(3, numcad);
			st.setTimestamp(4, dataInicioTS);
			st.setLong(5, horaIni.longValue());

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				possui = true;
			}
			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

		return possui;
	}
	
	public static void insertAutorizacaoRateio(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, Integer horini, GregorianCalendar dataFim, Integer horfim, String codrat) throws Exception
	{
		try
		{		
			StringBuffer sql = new StringBuffer();
			
			GregorianCalendar datini = (GregorianCalendar) dataInicio.clone();
			GregorianCalendar datfim = (GregorianCalendar) dataFim.clone();
			
			datini.set(Calendar.HOUR_OF_DAY, 0);
			datini.set(Calendar.MINUTE, 0);
			datini.set(Calendar.SECOND, 0);
			datini.set(Calendar.MILLISECOND, 0);
			
			datfim.set(Calendar.HOUR_OF_DAY, 0);
			datfim.set(Calendar.MINUTE, 0);
			datfim.set(Calendar.SECOND, 0);
			datfim.set(Calendar.MILLISECOND, 0);
			
			sql.append(" INSERT INTO R064RAT (NumEmp, TipCol, NumCad, DatIni, HorIni, DatFim, HorFim, CodRat) ");
			sql.append(" 				VALUES (:numemp, :tipcol, :numcad, :datini, :horini, :datfim, :horfim, :codrat) ");
			
			Query query = QLAdapter.entityManager.createNativeQuery(sql.toString());
			
			query.setParameter("numemp", numemp.toString());
			query.setParameter("tipcol", tipcol.toString());
			query.setParameter("numcad", numcad.toString());
			query.setParameter("datini", datini);
			query.setParameter("horini", horini.toString());
			query.setParameter("datfim", datfim);
			query.setParameter("horfim", horfim.toString());
			query.setParameter("codrat", codrat);
			
			query.executeUpdate();
		}
		catch (WorkflowException e)
		{
			throw new WorkflowException(e.getErrorList().get(0).toString());
		}
		catch (Exception e)
		{
			throw new Exception(e); 
		}
	}
	
	public static void insertTrocaDeEscala(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, GregorianCalendar dataFim, Long codesc, Long codtma) throws Exception
	{
		try
		{		
			//Excluir Troca de Escala
			String selectTroca = " SELECT COUNT(*) FROM R064TES " +
					"   	  WHERE NumEmp = :numEmp AND " +
					"         TipCol = :tipCol AND " +
					"         NumCad = :numCad AND " +
					"         DatIni = DATEADD(dd, 0, DATEDIFF(dd, 0, :datIni)) ";
			Query selectTrocaRegisters = QLAdapter.entityManager.createNativeQuery(selectTroca);
			selectTrocaRegisters.setParameter("numEmp", numemp);
			selectTrocaRegisters.setParameter("tipCol", tipcol);
			selectTrocaRegisters.setParameter("numCad", numcad);
			selectTrocaRegisters.setParameter("datIni", dataInicio);

			Integer resultSelct = (Integer) selectTrocaRegisters.getSingleResult();
			QLAdapter.log.warn("R064TES - SELECT: " + resultSelct);
			
			if (resultSelct == 0)
			{
			
				StringBuffer sqlTE = new StringBuffer();
				
				GregorianCalendar datini = (GregorianCalendar) dataInicio.clone();
				GregorianCalendar datfim = (GregorianCalendar) dataFim.clone();
				
				datini.set(Calendar.HOUR_OF_DAY, 0);
				datini.set(Calendar.MINUTE, 0);
				datini.set(Calendar.SECOND, 0);
				datini.set(Calendar.MILLISECOND, 0);
				
				datfim.set(Calendar.HOUR_OF_DAY, 0);
				datfim.set(Calendar.MINUTE, 0);
				datfim.set(Calendar.SECOND, 0);
				datfim.set(Calendar.MILLISECOND, 0);
				
				sqlTE.append(" INSERT INTO R064TES (NumEmp, TipCol, NumCad, DatIni, DatFim, CodEsc, CodTma, TurInt, StaAcc, codeqp, codcat) ");
				sqlTE.append(" 				VALUES (:numemp,:tipcol,:numcad,:datini,:datfim,:codesc,:codtma,0, 2, 0, 0) ");
				
				Query queryInsereTE = QLAdapter.entityManager.createNativeQuery(sqlTE.toString());
				
				queryInsereTE.setParameter("numemp", numemp.toString());
				queryInsereTE.setParameter("tipcol", tipcol.toString());
				queryInsereTE.setParameter("numcad", numcad.toString());
				queryInsereTE.setParameter("datini", datini);
				queryInsereTE.setParameter("datfim", datfim);
				queryInsereTE.setParameter("codesc", codesc.toString());
				queryInsereTE.setParameter("codtma", codtma.toString());
				
				queryInsereTE.executeUpdate();
			}	
		}
		catch (WorkflowException e)
		{
			throw new WorkflowException(e.getErrorList().get(0).toString());
		}
		catch (Exception e)
		{
			throw new Exception(e); 
		}
	}
	
	public static void insertTrocaDeHorario(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, Long codhor, String idNexti) throws Exception
	{
		try
		{		
			//Excluir Troca de Horario
			String selectTroca = " SELECT COUNT(*) FROM R064THR " +
					"   	  WHERE NumEmp = :numEmp AND " +
					"         TipCol = :tipCol AND " +
					"         NumCad = :numCad AND " +
					"         DatIni = DATEADD(dd, 0, DATEDIFF(dd, 0, :datIni)) ";
			Query selectTrocaRegisters = QLAdapter.entityManager.createNativeQuery(selectTroca);
			selectTrocaRegisters.setParameter("numEmp", numemp);
			selectTrocaRegisters.setParameter("tipCol", tipcol);
			selectTrocaRegisters.setParameter("numCad", numcad);
			selectTrocaRegisters.setParameter("datIni", dataInicio);

			Integer resultSelct = (Integer) selectTrocaRegisters.getSingleResult();
			QLAdapter.log.warn("R064THR - SELECT: " + resultSelct);
			
			if (resultSelct == 0)
			{
			
				StringBuffer sqlTE = new StringBuffer();
				
				GregorianCalendar datini = (GregorianCalendar) dataInicio.clone();
				
				datini.set(Calendar.HOUR_OF_DAY, 0);
				datini.set(Calendar.MINUTE, 0);
				datini.set(Calendar.SECOND, 0);
				datini.set(Calendar.MILLISECOND, 0);
				
				sqlTE.append(" INSERT INTO R064THR (NumEmp, TipCol, NumCad, DatIni, CodHor, CodInt, StaAcc, usu_nextid, usu_datmod, usu_hormod) ");
				sqlTE.append(" 				VALUES (:numemp,:tipcol,:numcad,:datini,:codhor,0, 2,:idNexti, ");
				sqlTE.append(" DATEADD(dd, 0, DATEDIFF(dd, 0, :datMod1)), (DATEPART(HOUR, :datMod2) * 60) + DATEPART(MINUTE, :datMod3)) ");
				
				
				Query queryInsereTE = QLAdapter.entityManager.createNativeQuery(sqlTE.toString());
				
				queryInsereTE.setParameter("numemp", numemp.toString());
				queryInsereTE.setParameter("tipcol", tipcol.toString());
				queryInsereTE.setParameter("numcad", numcad.toString());
				queryInsereTE.setParameter("datini", datini);
				queryInsereTE.setParameter("codhor", codhor.toString());
				queryInsereTE.setParameter("idNexti", idNexti.toString());
				GregorianCalendar datMod = new GregorianCalendar();
				queryInsereTE.setParameter("datMod1", new Timestamp(datMod.getTimeInMillis()));
				queryInsereTE.setParameter("datMod2", new Timestamp(datMod.getTimeInMillis()));
				queryInsereTE.setParameter("datMod3", new Timestamp(datMod.getTimeInMillis()));
				
				queryInsereTE.executeUpdate();
			}	
		}
		catch (WorkflowException e)
		{
			throw new WorkflowException(e.getErrorList().get(0).toString());
		}
		catch (Exception e)
		{
			throw new Exception(e); 
		}
	}
	
	public static EscalaVO buscaEscala(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataAtual)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		EscalaVO escala = new EscalaVO();

		try
		{
			StringBuffer queryColaborador = new StringBuffer();
			queryColaborador.append(" SELECT hes.CodEsc, hes.CodTma FROM R034FUN fun  ");
			queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.numemp AND hes.TipCol = fun.tipcol AND hes.NumCad = fun.numcad AND hes.DatAlt = (SELECT MAX(hes2.DatAlt) FROM R038HES hes2 WHERE hes2.NumEmp = hes.NumEmp AND hes2.TipCol = hes.TipCol AND hes2.NumCad = hes.NumCad AND hes2.DatAlt <= ?) ");
			queryColaborador.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
			queryColaborador.append(" WHERE fun.NumEmp = ?  AND fun.TipCol = ? AND fun.NumCad = ? ");

			Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
			PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
			stColaborador.setTimestamp(1, dataAtualTS);
			stColaborador.setLong(2, numemp);
			stColaborador.setLong(3, tipcol);
			stColaborador.setLong(4, numcad);

			ResultSet rsColaborador = stColaborador.executeQuery();

			if (rsColaborador.next())
			{
				escala.setCodigoEscala(rsColaborador.getLong("CodEsc"));
				escala.setCodigoTurma(rsColaborador.getLong("CodTma"));
			}
			rsColaborador.close();
			stColaborador.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

		return escala;
	}
	
	public static HorarioVO buscaHorario(Long codesc, GregorianCalendar data)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		HorarioVO horario = new HorarioVO();

		try
		{
			StringBuffer queryColaborador = new StringBuffer();
			queryColaborador.append(" SELECT mhr.HorBat FROM R006ESC esc  ");
			queryColaborador.append(" INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc ");
			queryColaborador.append(" INNER JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor ");
			queryColaborador.append(" WHERE hor.CodEsc = ? AND hor.SeqReg in ( ");
			queryColaborador.append(" 						SELECT min(hor.SeqReg) from R006ESC esc ");  
			queryColaborador.append(" 						INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc "); 
			queryColaborador.append(" 						left JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor "); 
			queryColaborador.append(" 						WHERE hor.CodEsc = ? and mhr.HorBat is not null ");
			queryColaborador.append(" ) ");

			PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
			stColaborador.setLong(1, codesc);
			stColaborador.setLong(2, codesc);

			ResultSet rsColaborador = stColaborador.executeQuery();
			List<Long> marcacoes = new ArrayList<Long>();
			
			while (rsColaborador.next())
			{
				marcacoes.add(rsColaborador.getLong("HorBat"));
			}
			if (marcacoes != null && marcacoes.size() > 0)
			{
				int hora = (int) (marcacoes.get(0) / 60);
				int minuto = (int) (marcacoes.get(0) % 60);
				
				GregorianCalendar datini = (GregorianCalendar) data.clone();
				datini.set(Calendar.HOUR_OF_DAY, hora);
				datini.set(Calendar.MINUTE, minuto);
				
				hora = (int) (marcacoes.get(marcacoes.size() - 1) / 60);
				minuto = (int) (marcacoes.get(marcacoes.size() - 1) % 60);
				
				GregorianCalendar datfim = (GregorianCalendar) data.clone();
				if (marcacoes.get(0) > marcacoes.get(marcacoes.size() - 1))
				{
					datfim.add(Calendar.DATE, +1);
				}
				datfim.set(Calendar.HOUR_OF_DAY, hora);
				datfim.set(Calendar.MINUTE, minuto);
				
				horario.setDataInicial(datini);
				horario.setDataFinal(datfim);
			}
			
			rsColaborador.close();
			stColaborador.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("");
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

		return horario;
	}
	
	public static PostoVO buscaPostoCoberturaDeFerias(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataAtual)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		PostoVO posto = new PostoVO();

		try
		{
			StringBuffer query = new StringBuffer();
			query.append(" SELECT cob.usu_taborgtra, cob.usu_numloctra FROM USU_T038COBFUN cob  ");
			query.append(" WHERE cob.USU_CodMot = 3 AND cob.USU_NumEmp = ? AND cob.USU_TipCol = ? AND cob.USU_NumCad = ? ");
			query.append(" AND cob.USU_DatAlt <= ? AND cob.USU_DatFim >= ? ");

			Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
			PreparedStatement st = connVetorh.prepareStatement(query.toString());
			
			st.setLong(1, numemp);
			st.setLong(2, tipcol);
			st.setLong(3, numcad);
			st.setTimestamp(4, dataAtualTS);
			st.setTimestamp(5, dataAtualTS);

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				posto.setCodigoOrganograma(rs.getLong("usu_taborgtra"));
				posto.setNumeroLocal(rs.getLong("usu_numloctra"));
			}
			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

		return posto;
	}
	
	public static String buscaRaizCC(Long numloc, Long taborg)
	{
		String codloc = "";

		try
		{
			String sqlSelect = "Select R016HIE.CodLoc " +
					" From R016ORN, R016HIE " +
					" Where R016ORN.TabOrg =:taborg " +
					" AND R016ORN.NumLoc =:numloc " +
					" AND R016HIE.TabOrg = R016ORN.TabOrg " +
					" AND R016HIE.NumLoc = R016ORN.NumLoc ";

			Query query = QLAdapter.entityManager.createNativeQuery(sqlSelect);
			query.setParameter("taborg", taborg);
			query.setParameter("numloc", numloc);
			query.setMaxResults(1);

			Object organograma = query.getSingleResult();
			codloc = new String(organograma.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("");
		}

		return codloc;
	}
}
