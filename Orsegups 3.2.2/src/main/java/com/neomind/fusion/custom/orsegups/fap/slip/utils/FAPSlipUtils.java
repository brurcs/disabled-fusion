package com.neomind.fusion.custom.orsegups.fap.slip.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.dto.HistoricoOrcamentoDTO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsGroupLevels;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLDateBetweenFilter;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPSlipUtils
{
	public static void preencherGTC(EntityWrapper wrapper)
	{
		Long tipoPagamento = wrapper.findGenericValue("tipoPagamento.codigo");
		Long codModalidade = wrapper.findGenericValue("modalidadeContrato.codigo");
		 
		NeoObject noGTC = wrapper.findGenericValue("GtcGestaoTitulosControladoria") ;
		if (noGTC == null)
			noGTC = AdapterUtils.createNewEntityInstance("GTCGestaoTitulosControladoria");
		EntityWrapper wGTC = new EntityWrapper(noGTC);
		
		NeoObject noEmpresa = wrapper.findGenericValue("empresa");
		NeoObject noFilial = wrapper.findGenericValue("filial");
		NeoObject noUsuarioResponsavel = wrapper.findGenericValue("solicitante");
		NeoObject noTipoLancamentoRateio = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GTCTipoLancamento"), new QLEqualsFilter("codigoLancamento", 2l));
		
		GregorianCalendar competencia = wrapper.findGenericValue("competencia");
		Integer mesCompetencia = competencia.get(Calendar.MONTH)+1;
		Integer anoCompetencia = competencia.get(Calendar.YEAR);
		
		WFProcess process = wrapper.findGenericValue("wfprocess");
		
		List<NeoObject> listAprovadores = wrapper.findGenericValue("listaAprovadores");
		String ultimoAprovador = listAprovadores.size() > 0 ? ((NeoUser)listAprovadores.get(0)).getFullName() : "";
		
		String obs = "";
		String obsSlip = "";
		if (tipoPagamento == 1l && codModalidade == 1l)
		{
			Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
			obs += "FAP SLIP Contrato " + codContrato + " Tarefa " + process.getCode() + ". Competência:" + mesCompetencia + "/" + anoCompetencia + ". ";
			obsSlip = "FAP Slip: " + process.getCode() + " Usuário Aprovador: " + ultimoAprovador + " Contrato: " + codContrato;
			
			NeoUser nuResponsavelContrato = wrapper.findGenericValue("dadosContrato.responsavelContrato");
			noUsuarioResponsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), new QLEqualsFilter("nomusu", nuResponsavelContrato.getCode()));
		}
		else if (tipoPagamento == 2l || codModalidade == 2l)
		{
			obs += "FAP SLIP AVULSO  Tarefa " + process.getCode() + ". " + mesCompetencia + "/" + anoCompetencia + ".";
			obsSlip = "FAP Slip: " + process.getCode() + " Usuário Aprovador: " + ultimoAprovador + " Avulso";
			
			noUsuarioResponsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), new QLEqualsFilter("nomusu", ((NeoUser)noUsuarioResponsavel).getCode()));
		}
		
		obs += wrapper.findGenericValue("obsGTCGLN");
		if (obs.length() >= 249)
		{
			obs = obs.substring(0, 249);
		}

		Long formaPagamento = wrapper.findGenericValue("formaPagamento.codigoFormaPagamento");
		Long codEmpresa = wrapper.findGenericValue("empresa.codemp");
		
		QLGroupFilter filtroformaPagamento = new QLGroupFilter("AND");
		filtroformaPagamento.addFilter(new QLEqualsFilter("codemp", codEmpresa));
		filtroformaPagamento.addFilter(new QLEqualsFilter("codfpg", formaPagamento));
		
		NeoObject noFormaPagto = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE066FPG"), filtroformaPagamento);
		if (noFormaPagto == null)
			throw new WorkflowException("Não encontrado a Forma de Pagamento Boleto ou Depósito para lançamento do Título!");
		
		QLGroupFilter filtroTransacao = new QLGroupFilter("AND");
		filtroTransacao.addFilter(new QLEqualsFilter("codemp", (Long) new EntityWrapper(noEmpresa).findGenericValue("codemp")));
		filtroTransacao.addFilter(new QLEqualsFilter("codtns", "90542"));
		List<NeoObject> transacoes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE001TNS"), filtroTransacao);
		if (transacoes == null || transacoes.isEmpty())
		{
			throw new WorkflowException("Não encontrado a Transação 90542 para lançamento do Título!");
		}
		
		List<NeoObject> listCentroCusto = new ArrayList<>();
		List<NeoObject> listCCU = wrapper.findGenericValue("centrosCusto.rateio");
		for (NeoObject noCCU : listCCU)
		{
			EntityWrapper wCCU = new EntityWrapper(noCCU);
			
			Long codTipoRateio = wrapper.findGenericValue("centrosCusto.tipoRateio.codigo");
			NeoObject noCentroCusto = wCCU.findGenericValue("centroCusto");
			BigDecimal valor = wCCU.findGenericValue("valor");
			
			if (codTipoRateio == 1l && valor != null)
			{
				BigDecimal valorPagamento = wrapper.findGenericValue("valorPagamento");
				valor = valorPagamento.multiply(valor).divide(new BigDecimal(100), BigDecimal.ROUND_UP);
			}
			
			NeoObject noCentroCustoGTC = AdapterUtils.createNewEntityInstance("GTCListaCentroCusto");
			EntityWrapper wCentroCustoGTC = new EntityWrapper(noCentroCustoGTC);
			wCentroCustoGTC.setValue("codigoCentroCusto", noCentroCusto);
			wCentroCustoGTC.setValue("valorRateio", valor);
			
			PersistEngine.persist(noCentroCustoGTC);
			
			listCentroCusto.add(noCentroCustoGTC);
		}
		wGTC.setValue("listaCentroCusto", listCentroCusto);		
		
		/*List<NeoObject> listParcelas = wrapper.findGenericValue("parcelasGTC");
		if (listParcelas != null)
		{
			for (NeoObject noParcela : listParcelas)
			{
				wGTC.findField("parcelas").addValue(EntityCloner.cloneNeoObject(noParcela));
			}
		}*/
		
		wGTC.setValue("obsSlip", obsSlip);
		wGTC.setValue("observacao", obs);
		wGTC.setValue("competenciaBI", competencia);
		wGTC.setValue("dataEmissao", wrapper.findGenericValue("dataEmissao"));
		wGTC.setValue("formaPagamento", noFormaPagto);
		wGTC.setValue("transacao", transacoes.get(0));
		wGTC.setValue("empresa", noEmpresa);
		wGTC.setValue("codigoFilial", noFilial);
		wGTC.setValue("listaLancamento", noTipoLancamentoRateio);	
		wGTC.setValue("codigoBanco", wrapper.findGenericValue("banco.codban"));
		wGTC.setValue("codigoAgencia", wrapper.findGenericValue("agencia"));
		wGTC.setValue("contaBanco", wrapper.findGenericValue("conta"));
		wGTC.setValue("contaFinanceira", wrapper.findGenericValue("contaFinanceira"));
		wGTC.setValue("contaContabil", wrapper.findGenericValue("contaContabil"));
		wGTC.setValue("usuarioResponsavel", noUsuarioResponsavel);
		wrapper.setValue("GtcGestaoTitulosControladoria", noGTC);
	}
	
	public static void adicionarVisualizadoresProcesso(WFProcess processo, List<SecurityEntity> listPermissao)
	{						
		processo.getMoreViewers().addAll(listPermissao);
	}
	
	public static NeoUser findAprovadorPorNivelGrupo(NeoUser nuSolicitante, Integer nivel)
	{
		NeoUser nuAprovador = nuSolicitante;
		Integer count = 0;
		
		while (nuAprovador.getGroup().getGroupLevel() < nivel && count < 20)
		{
			for (NeoUser aprovador : nuAprovador.getGroup().getUpperLevel().getResponsible().getUsers())
			{
				if (aprovador.isActive())
				{
					nuAprovador = aprovador;
				}
			}
			count++;
		}
		
		return nuAprovador;
	}
	
	public static List<NeoObject> findAprovadoresPorNivel(List<NeoObject> listAprovadores, Long nivelAprovador)
	{
		List<NeoObject> listAprovadoresNivelSuperior = new ArrayList<>();
		if (nivelAprovador > 10l)
			return listAprovadoresNivelSuperior;
		
		for (NeoObject noAprovadorContrato : listAprovadores)
		{
			EntityWrapper wAprovadorContrato = new EntityWrapper(noAprovadorContrato);
			NeoObject noAprovador = wAprovadorContrato.findGenericValue("aprovador");
			Long nivel = wAprovadorContrato.findGenericValue("nivel");
			if (nivel.equals(nivelAprovador))
			{
				if (noAprovador instanceof NeoUser)
				{
					if (((NeoUser) noAprovador).isActive())
						listAprovadoresNivelSuperior.add(noAprovador);
				}
				else
				{
					listAprovadoresNivelSuperior.add(noAprovador);
				}
					
			}
		}
		
		if (listAprovadoresNivelSuperior.isEmpty())
			return findAprovadoresPorNivel(listAprovadores, nivelAprovador+1);
		else 
			return listAprovadoresNivelSuperior;
	}
	
	public static List<NeoObject> findAprovadoresPorNivelSuperior(List<NeoObject> listAprovadoresAtuais, List<NeoObject> listAprovadoresContrato)
	{
		Long nivelAtual = 1l;
		loop: for (NeoObject noAprovadorContrato : listAprovadoresContrato)
		{
			EntityWrapper wAprovadorContrato = new EntityWrapper(noAprovadorContrato);
			Long nivel = wAprovadorContrato.findGenericValue("nivel");
			NeoObject noAprovador = wAprovadorContrato.findGenericValue("aprovador");
			
			for (NeoObject noAprovadorAtual : listAprovadoresAtuais)
			{
				if (noAprovadorAtual.equals(noAprovador))
				{
					nivelAtual = nivel + 1;
					break loop;
				}
			}
		}
		
		return FAPSlipUtils.findAprovadoresPorNivel(listAprovadoresContrato, nivelAtual);
	}
	
	public static NeoUser findAprovadorAvulso(EntityWrapper wrapper, NeoUser nuSolicitante)
	{
		return findAprovadorAvulso(wrapper, nuSolicitante, null);
	}
	
	public static NeoUser findAprovadorAvulso(EntityWrapper wrapper, NeoUser nuSolicitante, Integer nivel)
	{
		NeoUser nuAprovador = nuSolicitante;
		
		Integer nivelMax = OrsegupsGroupLevels.DIRETORIA;
		if (nivel != null)
			nivelMax = nivel;
		
		//recupera o superior do aprovador, caso seja abaixo ou igual ao nivel esperado
		if (nuSolicitante.getGroup().getUpperLevel().getGroupLevel() <= nivelMax
				|| nuSolicitante.getGroup().getGroupLevel() < nivelMax)
		{
			nuAprovador = retornarUsuarioPapel(nuSolicitante.getGroup().getUpperLevel().getResponsible());
			if (nuAprovador != null)
				return nuAprovador;
			else
			{
				nuAprovador = retornarUsuarioPapel(nuSolicitante.getGroup().getUpperLevel().getUpperLevel().getResponsible());
				if (nuAprovador != null)
					return nuAprovador;
			}
		}
		else {			
			wrapper.setValue("demandaAprovacao", false);
			return null;
		}
		
		return nuAprovador;
	}
	
	public static NeoUser retornarUsuarioPapel(NeoPaper npPapel)
	{		
		for (NeoUser nuUser : npPapel.getUsers())
		{
			if (nuUser.isActive())
				return nuUser;
		}
		
		return null;
	}
	
	public static Boolean validarPermissaoUsuario(List<NeoObject> listUsuariosPermitidos, NeoUser nuUsuario)
	{
		Boolean usuarioPermitido = false;
		
		if (listUsuariosPermitidos.isEmpty() || nuUsuario.isAdm())
		{
			usuarioPermitido = true;
		}
		else {
			for (NeoObject noUsuarioPermitido : listUsuariosPermitidos)
			{
				if (noUsuarioPermitido instanceof NeoUser)
				{
					if (nuUsuario.equals(noUsuarioPermitido))
					{
						usuarioPermitido = true;
						break;
					}
				}
				else 
				{
					Set<NeoUser> listUsuarios = noUsuarioPermitido instanceof NeoPaper ? ((NeoPaper) noUsuarioPermitido).getAllUsers() : ((NeoGroup) noUsuarioPermitido).getAllUsers();
					for (NeoObject noUsuarioGrupo : listUsuarios)
					{
						if (nuUsuario.equals(noUsuarioGrupo))
						{
							usuarioPermitido = true;
							break;
						}
					}
				}
			}
		}
		
		return usuarioPermitido;
	}
	
	public static BigDecimal getTotalPagamentos(Long codContrato, GregorianCalendar inicioCompetencia, GregorianCalendar fimCompetencia, String operacao)
	{
		BigDecimal totalPagamentos = new BigDecimal(0);
		
		List<NeoObject> listProcessos = buscarProcessos(codContrato, inicioCompetencia, fimCompetencia);
		if (!listProcessos.isEmpty())
		{
			for (NeoObject processo : listProcessos)
			{
				EntityWrapper wForm = new EntityWrapper(processo);
				
				BigDecimal valorPagamento = wForm.findGenericValue("valorPagamento");
				valorPagamento = valorPagamento != null ? valorPagamento : new BigDecimal(0);
				
				totalPagamentos = totalPagamentos.add(valorPagamento);
			}
		}
		
		if (operacao.equals("media"))
		{
			if (listProcessos.isEmpty())
				return new BigDecimal(0);
			else 
				return totalPagamentos.divide(new BigDecimal(listProcessos.size()), BigDecimal.ROUND_UP);
		}
		else 
			return totalPagamentos;
	}
	
	public static List<NeoObject> buscarProcessos(Long codContrato, GregorianCalendar inicioCompetencia, GregorianCalendar fimCompetencia)
	{
		return buscarProcessos(codContrato, inicioCompetencia, fimCompetencia, true);
	}
	
	public static List<NeoObject> buscarProcessos(Long codContrato, GregorianCalendar inicioCompetencia, GregorianCalendar fimCompetencia, Boolean finalizados)
	{
		QLGroupFilter qlGroup = new QLGroupFilter("AND");
		qlGroup.addFilter(new QLEqualsFilter("operacao.codigo", 1l));
		qlGroup.addFilter(new QLEqualsFilter("dadosContrato.codContrato", codContrato));		
		qlGroup.addFilter(new QLRawFilter("_vo.wfprocess.processState <> 2"));
		
		if (finalizados != null && finalizados)
			qlGroup.addFilter(new QLEqualsFilter("enviadoGTCGLN", true));
		
		if (inicioCompetencia != null && fimCompetencia != null)
			qlGroup.addFilter(new QLDateBetweenFilter("competencia", inicioCompetencia, fimCompetencia));
		 
		return PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup, "competencia desc");
	}
	
	public static List<NeoObject> buscarPagamentosFornecedor(Long codFornecedor)
	{		
		QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("codfor", codFornecedor), new QLEqualsFilter("sittit", "LQ"), new QLRawFilter("codemp not in (3, 4, 6)"));
		List<NeoObject> listTitulos = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE501TCP"), qlGroup, 0, 15, "usu_datent desc");
		
		return listTitulos;
	}
	
	public static NeoObject buscarCCU(Long codEmpresa, String codCCU, Long nivel, Boolean aceRat)
	{
		QLGroupFilter filterCcu = new QLGroupFilter("AND");
		filterCcu.addFilter(new QLEqualsFilter("codccu", codCCU));
		filterCcu.addFilter(new QLEqualsFilter("codemp", codEmpresa));
		filterCcu.addFilter(new QLEqualsFilter("nivccu", nivel));
		
		if (aceRat)
			filterCcu.addFilter(new QLEqualsFilter("acerat", "S"));
		
		return PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);
	}
	
	public static NeoObject buscarCCU(Long codEmpresa, String codCCU)
	{
		return buscarCCU(codEmpresa, codCCU, 8L, true);
	}
	
	public static void registrarHistorico(EntityWrapper wrapper, String atividade, String descricao, String responsavel)
	{
		NeoUser nuResponsavel = PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("fullName", responsavel));
		
		NeoObject noRegistroAtividade = AdapterUtils.createNewEntityInstance("FAPSlipRegistroHistorico");
		EntityWrapper wRegistroAtividade = new EntityWrapper(noRegistroAtividade);
		
		wRegistroAtividade.setValue("data", new GregorianCalendar());
		wRegistroAtividade.setValue("atividade", atividade);
		wRegistroAtividade.setValue("responsavel", nuResponsavel);
		wRegistroAtividade.setValue("responsavelStr", responsavel);
		wRegistroAtividade.setValue("descricao", descricao);
		
		wrapper.findField("historicoAtividades").addValue(noRegistroAtividade);
		wrapper.setValue("observacoes", "");
	}
	
	public static String parseMonetario(BigDecimal value)
	{
		NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
		
		if (value != null)
		{
			BigDecimal returnValue = value;
			returnValue = returnValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			return monetaryFormat.format(returnValue);
		}
		
		return "R$ 0,00";
	}
	
	public static void ordernarData(Collection<? extends HistoricoOrcamentoDTO> list)
	{
		Collections.sort((List<? extends HistoricoOrcamentoDTO>) list, new Comparator<HistoricoOrcamentoDTO>()		
		{
			@Override
			public int compare(HistoricoOrcamentoDTO o1, HistoricoOrcamentoDTO o2)
			{
				if (o1.getData() == null || o2.getData() == null) 
				      return 0;
				else
					return o1.getData().compareTo(o2.getData());
			}
		});
	}
}
