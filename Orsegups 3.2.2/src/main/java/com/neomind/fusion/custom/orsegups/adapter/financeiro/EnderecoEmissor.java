package com.neomind.fusion.custom.orsegups.adapter.financeiro;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "enderEmit", namespace = "http://www.portalfiscal.inf.br/nfe")
public class EnderecoEmissor {
	

	private String logradouro;
	private Long numero;
	private String complemento;
	private String bairro;
	private Long codigoMunicipio;
	private String municipio;
	private String unidadeFederativa;
	private Long cep;
	private Long codigoPais;
	private String pais;
	private Long telefone;
	
	@XmlElement(name="xLgr", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getLogradouro() {
		return logradouro;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	@XmlElement(name="nro", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getNumero() {
		return numero;
	}
	
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	
	@XmlElement(name="xCpl", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getComplemento() {
		return complemento;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	@XmlElement(name="xBairro", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getBairro() {
		return bairro;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	@XmlElement(name="cMun", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCodigoMunicipio() {
		return codigoMunicipio;
	}
	
	public void setCodigoMunicipio(Long codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	
	@XmlElement(name="xMun", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getMunicipio() {
		return municipio;
	}
	
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	
	@XmlElement(name="UF", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getUnidadeFederativa() {
		return unidadeFederativa;
	}
	
	public void setUnidadeFederativa(String unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}
	
	@XmlElement(name="CEP", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCep() {
		return cep;
	}
	
	public void setCep(Long cep) {
		this.cep = cep;
	}
	
	@XmlElement(name="cPais", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCodigoPais() {
		return codigoPais;
	}
	
	public void setCodigoPais(Long codigoPais) {
		this.codigoPais = codigoPais;
	}
	
	@XmlElement(name="xPais", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	@XmlElement(name="fone", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getTelefone() {
		return telefone;
	}
	
	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}
	
}
