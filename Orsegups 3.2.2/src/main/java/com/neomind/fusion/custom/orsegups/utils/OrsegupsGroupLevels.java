package com.neomind.fusion.custom.orsegups.utils;

public final class OrsegupsGroupLevels {
        
        public static final int SUPERVISAO = 100;
        public static final int COORDENACAO = 300;
        public static final int GERENCIA = 500;
        public static final int DIRETORIA = 800;
        public static final int PRESIDENCIA = 1000;
        
}   
