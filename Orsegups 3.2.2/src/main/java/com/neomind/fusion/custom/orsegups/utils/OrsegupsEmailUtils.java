package com.neomind.fusion.custom.orsegups.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.validation.ValidationException;

import org.apache.commons.mail.HtmlEmail;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

/**
 * Classe utilitaria para a montagem dos e-mails de RAT e OS
 * 
 * @author mateus.batista
 *
 */
public class OrsegupsEmailUtils {

    //==================================== INICIO CHAVES DO METODO GETCONFIGURACAOEMPRESARAT =======================================
    
    /**
     * Grupo em que a empresa especificada se encaixa <br>
     * Utilizado como chave no retorno do método
     * {@link #getConfiguracaoEmpresaRAT(int)}
     */
    public static final int RAT_GRUPO = 1;

    /**
     * Pasta onde estão as imagens utilizadas para envio de e-mails de RAT e OS<br>
     * Utilizado como chave no retorno do método
     * {@link #getConfiguracaoEmpresaRAT(int)}
     */
    public static final int RAT_PASTA = 2;

    /**
     * Sufixo do e-mail utilizado como remetente dos e-mails de RAT e OS<br>
     * Utilizado como chave no retorno do método
     * {@link #getConfiguracaoEmpresaRAT(int)}
     */
    public static final int RAT_REMETENTE = 3;


    /**
     * NeoId do registro de configuração da rat<br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryOSFechamento
     */  
    public static final int RAT_NEOID = 4;
    
    
    //==================================== FIM CHAVES DO METODO GETCONFIGURACAOEMPRESARAT =======================================

    
    
    //================================= INICIO TIPO RAT =========================================================
    /**
     * E-mail de finalização de ordem de serviço<br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryOSFechamento
     */
    public static final String TIPO_OS = "os";
    
    /**
     * E-mail de finalização de ordem de serviço<br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryOSFechamento
     */
    public static final String TIPO_OS_AGENDADA = "os.agendada";
    
    public static final String TIPO_RAT_ANULACAO_PONTO = "rat.anulacao";

    /**
     * E-mail de acompanhamento de imagens <tt>CUCs ACP e ACV </tt><br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryMonitoramentoDeImagens
     */
    public static final String TIPO_RAT_ACOMPANHAMENTO = "rat.aco";

    /**
     * E-mail de atendimento de desvio de hábito <tt>X406</tt><br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryX406
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryXXX2
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryXXX5
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryDesarmeOutros
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliverySemControleOutros
     */
    public static final String TIPO_RAT_DESVIO_HABITO = "rat.adh";

    /**
     * E-mail de atendimento de tático <tt>DSL</tt> Deslocamento<br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryAIT
     */
    public static final String TIPO_RAT_DESLOCAMENTO = "rat.dsl";

    /**
     * E-mail de acompanhamento de imagens <tt>XVID</tt><br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryXVIDComImagens
     */
    public static final String TIPO_RAT_VERIFICACAO = "rat.ver";

    /**
     * E-mail de desconexão de CFTV <tt>XRED</tt><br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryXRED
     */
    public static final String TIPO_RAT_DESCONEXAO_CFTV = "rat.xred";
    
    
    /**
     * E-mail de acompanhamento de cliente <tt>AC</tt><br>
     * 
     * @see com.neomind.fusion.custom.orsegups.scheduler.EmailDeliveryAcompanhamentoCliente
     */
    public static final String TIPO_RAT_ACOMPANHAMENTO_CLIENTE = "rat.aco.cli";
    
    //================================= FIM TIPO RAT =========================================================
    
    
    //========================== INICIO NOMES ROTINAS EMAIL EM [TI] MONITORA AGENDADOR =======================
    
    public static final String MONITOR_EMAIL_DELIVERY_AIT = "EmailAtendimentoAIT";
    
    public static final String MONITOR_EMAIL_DELIVERY_XXX5 = "EmailAtendimentoXXX5";
    
    public static final String MONITOR_EMAIL_DELIVERY_XXX2 = "EmailAtendimentoXXX2";
    
    public static final String MONITOR_EMAIL_DELIVERY_X406 = "EmailAtendimentoX406";
    
    public static final String MONITOR_EMAIL_DELIVERY_XVID_COM_IMAGENS = "EmailAtendimentoXVID";
    
    public static final String MONITOR_EMAIL_DELIVERY_RONDA_VIRTUAL_VTO = "EmailDeliveryRondaVirtualVTO";
    
    public static final String MONITOR_EMAIL_DELIVERY_ANULACAO_PONTO = "EmailAtendimentoAnulacaoPonto";
    
    public static final String MONITOR_EMAIL_DELIVERY_OS_FECHADA = "EmailDeliveryInformaOSFechadaAutomaticamente";
    
    /**
     * Retorno mapa com os detalhes da configuração da empresa solicitada
     * previamente cadastrados em <tt>[RAT] Configuração Empresa</tt>.
     * </br></br> As chaves e valores do mapa seguem :
     * 
     * <pre>
     * {@link #RAT_GRUPO} Grupo em que a empresa especificada se encaixa 
     * {@link #RAT_PASTA} Pasta onde estão as imagens utilizadas para envio de e-mails de RAT e OS
     * {@link #RAT_REMETENTE} Sufixo do e-mail utilizado como remetente dos e-mails de RAT e OS
     * </pre>
     * 
     * @param empresa
     *            int <tt>ID_EMPRESA</tt>
     * @return <code>HashMap</code> se encontrar os parametros para a empresa
     *         especificada; <code>null</code> caso a empresa são seja
     *         encontrada
     */
    public static Map<Integer, String> getConfiguracaoEmpresa(int empresa) {

	Map<Integer, String> retorno = null;

	QLGroupFilter filtroEmpresa = new QLGroupFilter("AND");
	filtroEmpresa.addFilter(new QLEqualsFilter("listaEmpresas.cd_empresa", empresa));

	List<NeoObject> parametros = PersistEngine.getObjects(AdapterUtils.getEntityClass("RATConfiguracaoEmpresa"), filtroEmpresa);

	if (parametros != null && !parametros.isEmpty()) {

	    NeoObject objParam = parametros.get(0);

	    EntityWrapper wParam = new EntityWrapper(objParam);

	    retorno = new HashMap<Integer, String>();

	    retorno.put(OrsegupsEmailUtils.RAT_GRUPO, wParam.findField("grupo").getValueAsString());
	    retorno.put(OrsegupsEmailUtils.RAT_PASTA, wParam.findField("pasta").getValueAsString());
	    retorno.put(OrsegupsEmailUtils.RAT_REMETENTE, wParam.findField("remetente").getValueAsString());
	    retorno.put(OrsegupsEmailUtils.RAT_NEOID, String.valueOf(wParam.findField("neoId").getValue()));

	    return retorno;
	} else {
	    return retorno;
	}

    }

    public static String getCabecalhoEmail(String tipo, String pasta) {
	return getCabecalhoEmail(tipo, pasta, null);
    }
    
    /**
     * Retorna o <tt>HTML</tt> contendo o cabeçalho do e-mail conforme tipo de
     * e-mail e pasta especificados
     * 
     * @param tipo
     *            <code>String</code> contendo tipo do e-mail conforme as
     *            constantes abaixo : <br>
     * 
     *            <pre>
     *            {@link #TIPO_OS}
     *            {@link #TIPO_RAT_ACOMPANHAMENTO}
     *            {@link #TIPO_RAT_DESLOCAMENTO}
     *            {@link #TIPO_RAT_DESVIO_HABITO}
     *            {@link #TIPO_RAT_VERIFICACAO}
     * 
     * </pre>
     * @param pasta
     *            <code>String</code> contendo a pasta retornada no mapa do
     *            método {@link #getConfiguracaoEmpresa(int)}
     * @return <code>String</code> contendo <code>HTML</code> do e-mail; nunca
     *         <code>null</code>
     */
    public static String getCabecalhoEmail(String tipo, String pasta, String ratingToken) {

	StringBuilder html = new StringBuilder();

	html.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
	html.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
	html.append("<head>");
	html.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
	html.append("<meta name=\"viewport\" content=\"width=device-width\" />");
	html.append("<!--[if !mso]><!-- --><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /><!--<![endif]-->");
	html.append("<title>Email</title>");	
	html.append("</head>");
	html.append("<body>");
	html.append("");
	html.append("<style type=\"text/css\">");
	html.append("");
	html.append("#outlook a{padding:0;}");
	html.append("body{width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0!important;padding:0!important;}");
	html.append(".ExternalClass{width:100%;}");
	html.append(".ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}");
	html.append(".bodytbl{margin:0;padding:0;width:100%!important;-webkit-text-size-adjust:none;}");
	html.append("img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block;max-width:100%;}");
	html.append("a img{border:none;}");
	html.append("p{margin:1em 0;}");
	html.append("");
	html.append("table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}");
	html.append("table td{border-collapse:collapse;}");
	html.append(".o-fix table,.o-fix td{mso-table-lspace:0pt;mso-table-rspace:0pt;}");
	html.append("");
	html.append("body,.bodytbl{background-color:#FAFAFA/*Background Color*/;}");
	html.append("");
	html.append("table{color:#787878/*Text Color*/;}");
	html.append("td,p{color:#787878;}");
	html.append(".h1{color:#353535/*Headings*/;}");
	html.append(".h2{color:#353535;}");
	html.append(".cta .h2{color:#FFFFFF;}");
	html.append(".cta .h3{color:#FFFFFF;}");
	html.append(".quote{color:#AAAAAA;}");
	html.append(".invert,.invert h1,.invert td,.invert p{background-color:#353535;color:#FAFAFA !important;}");
	html.append("");
	html.append(".wrap.header{}");
	html.append(".wrap{background-color:#FAFAFA;}");
	html.append(".wrap.body-i{background-color:#353535;}");
	html.append(".wrap.footer{}");
	html.append(".padd{width:20px;}");
	html.append("");
	html.append("a{color:#00A9E0;}");
	html.append("a:link,a:visited,a:hover{color:#00A9E0/*Contrast*/;}");
	html.append(".btn,.btn div,.btn a{color:#FFFFFF/*Contrast Link Color*/;}");
	html.append(".btn a,.btn a img{background:#1f3b8b/*Button Color*/;}");
	html.append(".invert .btn a,.invert .btn a img{background:none;}");
	html.append("");
	html.append("h1,h2,h3,h4,h5,h6{color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;}");
	html.append("h1{font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px;}");
	html.append("h2{font-size:20px;margin-bottom:12px;margin-top:2px;line-height:24px;}");
	html.append("h3{font-size:15px;margin-bottom:12px;margin-top:2px;line-height:12px;text-align:left;}");
	html.append("h4{font-size:16px;}");
	html.append("h5{font-size:14px;}");
	html.append("h6{font-size:12px;}");
	html.append("h1 a,h2 a,h3 a,h4 a,h5 a,h6 a{color:#00A9E0;}");
	html.append("h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active{color:#00A9E0 !important;}");
	html.append("h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited{color:#00A9E0 !important;}");
	html.append(".h1{font-family:Helvetica,Arial,sans-serif;font-size:55px;font-weight:bold;line-height:40px !important;letter-spacing:-2px;}");
	html.append(".h2{font-family:Helvetica,Arial,sans-serif;font-size:19px;font-weight:bold;letter-spacing:-1px;line-height:24px;}");
	html.append(".h3{font-family:Helvetica,Arial,sans-serif;font-size:17px;line-height:15px;}");
	html.append("");
	html.append(".line{border-bottom:1px solid #AAAAAA/*Separator*/;}");
	html.append("table{font-family:Helvetica,Arial,sans-serif;font-size:14px;}");
	html.append("td,p{line-height:24px;}");
	html.append("ul,ol{margin-top:20px;margin-bottom:20px;}");
	html.append("li{line-height:24px;}");
	html.append("td,tr{padding:0;}");
	html.append(".quote{font-family:Helvetica,Arial,sans-serif;font-size:24px;letter-spacing:0;margin-bottom:6px;margin-top:6px;line-height:24px;font-style:italic;}");
	html.append(".small{font-size:10px;color:#787878;line-height:15px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px;}");
	html.append("table.plan {width:100%;min-width:100%;}");
	html.append("table.plan td{border-right:1px solid #EBEBEB/*Lines*/;border-bottom:1px solid #EBEBEB;text-align:center;}");
	html.append("table.plan td.last{border-right:0;}");
	html.append("table.plan th{text-align:center;border-bottom:1px solid #EBEBEB;}");
	html.append("a{text-decoration:none;padding:2px 0px;}");
	html.append(".btn{display:block;}");
	html.append(".btn a{display:inline-block;padding:0;line-height:0.5em;}");
	html.append(".btn img,.social img{display:inline;margin:0;}");
	html.append(".social .btn a,.social .btn a img{background:none;}");
	html.append(".right{text-align:right;}");
	html.append("");
	html.append("table.textbutton td{background:#1f3b8b;padding:1px 14px 1px 14px;color:#FFF;display:block;height:24px;vertical-align:top;margin-right:4px;margin-bottom:4px;}");
	html.append("table.textbutton a{color:#FFF;font-size:14px;font-weight:100;line-height:20px;width:100%;display:inline-block;}");
	html.append(".label table.textbutton {width:auto !important;}");
	html.append(".label table.textbutton td{height:auto !important;}");
	html.append(".label table.textbutton a{padding:2px 0 !important;font-size:12px !important;}");
	html.append(".cta table.textbutton td{height:32px;padding:4px 16px 7px 16px;}");
	html.append(".cta table.textbutton a{font-size:19px;line-height:32px;}");
	html.append("");
	html.append("div.preheader{line-height:0px;font-size:0px;height:0px;display:none !important;visibility:hidden;text-indent:-9999px;}");
	html.append("");
	html.append("@media only screen and (max-width: 599px) {");
	html.append("body{-webkit-text-size-adjust:120% !important;-ms-text-size-adjust:120% !important;}");
	html.append(".wrap{width:96% !important;}");
	html.append(".wrap .padd{width:10px !important;}");
	html.append(".wrap table{width:100% !important;}");
	html.append(".wrap img {max-width:100% !important;height:auto !important;}");
	html.append(".wrap .m-padd {padding:0 20px !important;}");
	html.append(".wrap .m-w-auto{width:auto !important;}");
	html.append(".wrap .m-l{text-align:left !important;}");
	html.append(".wrap .h div{letter-spacing:-1px !important;font-size:18px !important;}");
	html.append(".wrap .m-0{width:0;display:none;}");
	html.append(".wrap .m-b,{margin-bottom:20px !important;}");
	html.append(".wrap .m-b,.m-b img{display:block;min-width:100% !important;width:100% !important;}");
	html.append("table{font-size:15px !important;}");
	html.append("table.textbutton td{height:44px !important;}");
	html.append(".cta table.textbutton td{height:50px !important;}");
	html.append("table.textbutton a{padding:10px 0 !important;font-size:18px !important;}");
	html.append("}");
	html.append("");
	html.append("@media only screen and (max-width: 479px) {");
	html.append("}");
	html.append("@media only screen and (max-width: 320px) {");
	html.append("}");
	html.append("@media only screen and (min-device-width: 375px) and (max-device-width: 667px) {");
	html.append("body{-webkit-text-size-adjust:170% !important;-ms-text-size-adjust:170% !important;}");
	html.append("}");
	html.append("@media only screen and (min-device-width: 414px) and (max-device-width: 736px) {");
	html.append("body{-webkit-text-size-adjust:170% !important;-ms-text-size-adjust:170% !important;}");
	html.append("}");
	html.append(".texto-rodape { text-align:justify; font-size: 11px; color:#ffffff;padding:5px;}");
	html.append("");
	html.append("</style>");
	html.append("");
	html.append("<table class=\"bodytbl\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
	html.append("");
	html.append("<tr>");
	html.append("<td align=\"center\">");
	html.append("");
	html.append("<!-- ☺ header block ends here -->");
	html.append("<!-- Full Size Image start  -->");
	html.append("");
	html.append("<table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" class=\"wrap\">");
	html.append("<tr>");
	html.append("<td width=\"600\" valign=\"top\" align=\"left\">");
	html.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"o-fix\">");
	html.append("<tr>");
	html.append("<td width=\"600\" valign=\"top\" align=\"left\">");
	html.append("<img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/novo-delivery/" + pasta + "/" + tipo + "/topo.png\" alt=\"Relatório de Atendimeto Técnico\" width=\"600\" height=\"300\" border=\"0\"");
	html.append("style=\"max-width:600px;\"  />");
	html.append("</td>");
	html.append("</tr>");
	html.append("</table>");
	html.append("</td>");
	html.append("</tr>");
	html.append("</table>");
	html.append("");
	html.append("<!-- Full Size Image end   -->");
	if (ratingToken != null)
	    html.append(OrsegupsEmailUtils.getStarsRatingHTML(ratingToken));
	
	return html.toString();
    }

    public static String getRodapeEmail(String tipo, String pasta, String grupo, String emailCliente, String remetente) {
	return getRodapeEmail(tipo, pasta, grupo, emailCliente, remetente, null);
    }
    
    /**
     * Retorna o <tt>HTML</tt> contendo o rodapé do e-mail conforme tipo de
     * e-mail e pasta especificados e sufixo do destinatario especificado
     * 
     * @param tipo
     *            <code>String</code> contendo tipo do e-mail conforme as
     *            constantes abaixo : <br>
     * 
     *            <pre>
     *            {@link #TIPO_OS}
     *            {@link #TIPO_RAT_ACOMPANHAMENTO}
     *            {@link #TIPO_RAT_DESLOCAMENTO}
     *            {@link #TIPO_RAT_DESVIO_HABITO}
     *            {@link #TIPO_RAT_VERIFICACAO}
     * 
     * </pre>
     * @param pasta
     *            <code>String</code> contendo a pasta retornada no mapa do
     *            método {@link #getConfiguracaoEmpresa(int)}
     * @param grupo
     *            <code>String</code> contendo grupo que a empresa faz parte
     *            retornado pelo método {@link #getConfiguracaoEmpresaRAT(int)}
     * @param emailCliente
     *            <code>String</code> contendo e-mail do cliente que receberá o
     *            e-mail
     * @param remetente
     *            <code>String</code> sufixo do e-mail do grupo que a empresa
     *            faz parte retornado pelo método
     *            {@link #getConfiguracaoEmpresaRAT(int)}
     * @return <code>String</code> contendo <code>HTML</code> do e-mail; nunca
     *         <code>null</code>
     */
    public static String getRodapeEmail(String tipo, String pasta, String grupo, String emailCliente, String remetente, String ratingToken) {

	StringBuilder html = new StringBuilder();

	html.append("<!-- Call to Action start  -->");
	if (ratingToken != null)
	    html.append(OrsegupsEmailUtils.getStarsRatingHTML(ratingToken));
	html.append("");
	html.append("<table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" class=\"wrap cta\">");
	html.append("<tr>");
	html.append("<td width=\"600\" valign=\"top\" align=\"left\">");
	html.append("<table cellpadding=\"0\" cellspacing=\"0\">");
	html.append("<tr>");
	html.append("<!-- CONTENT start -->");
	html.append("					<td width=\"10\">&nbsp;</td>");
	html.append("<td width=\"580\" align=\"center\">");
	html.append("<div class=\"btn\" align=\"center\">");
	html.append("<br>");
	html.append("<table><tr><td align=\"center\" width=\"auto\"><a href=\"https://www.orsegups.com.br/contato.html\" label=\"botão solicite contato\" ><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/" + pasta
		+ "/comum/cta.jpg\" alt=\"botão solicite contato\" width=\"120\" height=\"28\" border=\"0\" /></a></td></tr></table>");
	html.append("</div>");
	html.append("</td>");
	html.append("<td width=\"10\">&nbsp;</td>					");
	html.append("<!-- CONTENT end -->");
	html.append("</tr>");
	html.append("</table>");
	html.append("</td>");
	html.append("</tr>");
	html.append("<tr>");
	html.append("<td width=\"10\" style='text-align: center'><a href=\"http://www.orsegups.com.br/portal-do-cliente/\" target=\"_blank\" >Portal do Cliente</a> | <a href=\"http://www.orsegups.com.br/portal-do-cliente/\" target=\"_blank\" >Relatórios de Eventos</a>  | <a href=\"http://www.orsegups.com.br/contato/\" target=\"_blank\">Atualização Cadastral</a> </td>");
	html.append("</tr>");
	html.append("</table>");
	html.append("");
	html.append("<!-- Call to Action end   -->");
	html.append("");
	html.append("");
	html.append("<!-- Full Size Image start  -->");
	html.append("");
	html.append("<table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" class=\"wrap\">");
	html.append("<tr>");
	html.append("<td width=\"600\" valign=\"top\" align=\"left\">");
	html.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"o-fix\">");
	html.append("<tr>");
	html.append("<td width=\"600\" valign=\"top\" align=\"left\">");
	html.append("<a href=\"#\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/novo-delivery/" + pasta + "/" + tipo + "/rodape.png\" alt=\"Imagem do rodapé - Orsegups\" width=\"600\" height=\"400\" border=\"0\" style=\"border:none; max-width:600px;background-color:#204a72 \"  /></a>");
	html.append("</td>");
	html.append("</tr>");
	html.append("</table>");
	html.append("</td>");
	html.append("</tr>");
	html.append("</table>");
	html.append("");
	html.append("<!-- Full Size Image end   -->");
	html.append("");
	html.append("<!-- 1/1 Text start  -->");
	html.append("");
	html.append("<table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" class=\"wrap\" style=\"background: #204a72;\">");
	html.append("<tr>");
	html.append("<td width=\"600\" valign=\"top\" align=\"left\">");
	html.append("<table cellpadding=\"0\" cellspacing=\"0\">");
	html.append("<tr>");
	html.append("<!-- CONTENT start -->");
	html.append("					<td width=\"600\" valign=\"top\" align=\"left\">						");
	html.append("<div class=\"h3 texto-rodape\"><span style=\"font-size: 10px;color: #FFFFFF;\">Este e-mail foi enviado a " + emailCliente.toLowerCase() + " pois você está cadastrado em nosso sistema operacional.<br/>");
	html.append("Este e-mail foi enviado em nome da " + grupo + ". Teremos satisfação em ajudá-lo com quaisquer dúvidas ou preocupações. Para qualquer esclarecimento desejado, visite www." + grupo.toLowerCase() + ".com.br, no link contato ou envie e-mail para sac" + remetente + ".<br />    ");
	html.append("© 2017 " + grupo + ". Todos os direitos reservados<br /> ");
	html.append("Este email e todas as informações ou arquivos contidos nele são exclusivamente para uso confidencial do destinatário. Esta mensagem contém informações confidenciais e pertencentes à "
		+ grupo
		+ " (como dados corporativos, dos clientes e dos funcionários da  "
		+ grupo
		+ ") que não podem ser lidas, pesquisadas, distribuídas ou utilizadas de outra forma por qualquer outro indivíduo que não seja o destinatário pretendido. Caso tenha recebido este email por engano, por favor, notifique o remetente e exclua esta mensagem e seus anexos imediatamente.</span></div>");
	html.append("</td>");
	html.append("<!-- CONTENT end -->");
	html.append("</tr>");
	html.append("</table>");
	html.append("</td>");
	html.append("</tr>");
	html.append("</table>");
	html.append("");
	html.append("<!-- 1/1 Text end   -->");
	html.append("");
	html.append("</td>");
	html.append("</tr>");
	html.append("</table>");
	html.append("</body>");
	html.append("</html>");

	return html.toString();
    }
    
    private static String getStarsRatingHTML(String ratingToken) {
	StringBuilder html = new StringBuilder();
	
	html.append("<div style=\"text-align: center; color: #60666d; font-family: sans-serif;\">");
	html.append("  <span style=\"font-size: 10px; text-transform: uppercase; letter-spacing: .5px;\">");
	html.append("    Avalie este atendimento");
	html.append("  </span>");
	html.append("  <div style=\"width: 100%; float: left; unicode-bidi: bidi-override; direction: rtl;\">");
	html.append("    <table style=\"border-collapse: collapse; width: 275px; margin: 0 auto; font-size: 50px;\">");
	html.append("      <tr>");
	String url = "";
	for (int i = 5; i > 0; i--) {
	    url = OrsegupsUtils.URL_AVALIACAO + "?rating=" + i + "&ratingToken=" + ratingToken;
	    html.append("        <td width=\"55\">");
	    html.append("          <a href=\"" + url + "\" tabindex=\"" + (6-i) + "\" target=\"_blank\" width=\"55\" style=\"text-decoration:none !important; text-decoration:none;\">");
	    html.append("            <span style=\"color: #FFCC00;\">&#9733;</div>");
	    html.append("          </a>");
	    html.append("        </td>");
	}
	html.append("      </tr>");
	html.append("    </table>");
	html.append("  </div>");
	html.append("</div>");
	
	return html.toString();
    }

    /**
     * Envia e-mail para gerente da central de monitoramento e responsáveis da
     * TI para verificar empresa sem configuração para envio de RATs
     * 
     * @param empresa
     */
    public static void enviarNotificacaoEmpresaSemConfig(int empresa) {

	StringBuilder html = new StringBuilder();

	html.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");

	html.append("<html>");

	html.append("<head>");

	html.append("<title>Aviso</title>");

	html.append("</head>");

	html.append("<body>");

	html.append("<p><h2>Não foi encontrado uma configuração de e-mail para a empresa " + empresa + " favor entrar em contato com o setor de tecnologia da informação.</h2></p>");

	html.append("</body>");
	html.append("</html>");

	try {

	    MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

	    MailSettings mailClone = new MailSettings();
	    mailClone.setMinutesInterval(settings.getMinutesInterval());
	    mailClone.setFromEMail(settings.getFromEMail());
	    mailClone.setFromName(settings.getFromName());
	    mailClone.setRenderServer(settings.getRenderServer());
	    mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
	    mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
	    mailClone.setSmtpSettings(settings.getSmtpSettings());
	    mailClone.setPort(settings.getPort());
	    mailClone.setSmtpServer(settings.getSmtpServer());
	    mailClone.setEnabled(settings.isEnabled());

	    mailClone.setFromEMail("fusion@orsegups.com.br");
	    mailClone.setFromName("Fusion");
	    if (mailClone != null) {

		HtmlEmail noUserEmail = new HtmlEmail();

		noUserEmail.setCharset("UTF-8");
		noUserEmail.addTo("edson.heinz@orsegups.com.br");
		noUserEmail.addTo("lucas.alison@orsegups.com.br");
		noUserEmail.addTo("carlos.silva@orsegups.com.br");
		noUserEmail.addTo("jorge.junior@orsegups.com.br");
		noUserEmail.addTo("emailautomatico@orsegups.com.br");		

		noUserEmail.setSubject("Empresa sem configuração para envio de RATs: " + empresa);

		noUserEmail.setHtmlMsg(html.toString());
		noUserEmail.setContent(html.toString(), "text/html");
		mailClone.applyConfig(noUserEmail);

		noUserEmail.send();
	    }
	} catch (Exception e) {
	    e.printStackTrace();

	}

    }

    /**
     * Valida o email(s) recebido(s) e retorna uma lista com o(s) email(s)
     * validado(s) e demais emails do cliente nos sistemas Sigma e Sapiens.
     * 
     * @param emails
     *            <code>String</code> um ou mais emails separados por ponto e
     *            virgula.
     * @param cdCliente
     *            <code>String</code> <tt>CD_CLIENTE</tt>
     * @return <code>ArrayList</code> com os e-mails do cliente; pode ser vazia;
     *         nunca <code>null</code>
     */
    public static List<String> validarEmail(String emails, String cdCliente, boolean flagCadastroAtualizadoNAC) {

	final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	Pattern pattern;
	Matcher matcher;

	List<String> listaDeEmail = new ArrayList<String>();

	try {
	    pattern = Pattern.compile(EMAIL_PATTERN);
	} catch (PatternSyntaxException e) {
	    e.printStackTrace();
	    return listaDeEmail;
	}

	emails = emails.replaceAll(",", ";");

	String emailsSigma[] = emails.split(";");

	for (String mail : emailsSigma) {
	    matcher = pattern.matcher(mail);

	    if (matcher.matches()) {
		if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
		    listaDeEmail.add(mail.toLowerCase().trim());
		}
	    }
	}

	Connection connSigma = null; // PersistEngine.getConnection("SIGMA90");
	PreparedStatement psSigma = null;
	ResultSet rsSigma = null;

	try {

	    StringBuilder sqlSigma = new StringBuilder();

	    sqlSigma.append(" SELECT email FROM DBPROVIDENCIA WITH(NOLOCK) WHERE CD_CLIENTE = " + Long.valueOf(cdCliente) + " AND  LEN(email) > 5 ");
	    sqlSigma.append(" UNION ");
	    sqlSigma.append(" SELECT NM_EMAIL FROM MENSAGEM_CONFIG WITH(NOLOCK) WHERE CD_CLIENTE = " + Long.valueOf(cdCliente) + " AND LEN(NM_EMAIL) > 5 ");

	    connSigma = PersistEngine.getConnection("SIGMA90");

	    psSigma = connSigma.prepareStatement(sqlSigma.toString());

	    rsSigma = psSigma.executeQuery();

	    while (rsSigma.next()) {

		String emailRs = rsSigma.getString("email");

		if (emailRs != null && !emailRs.isEmpty()) {
		    emailRs = emailRs.replaceAll(",", ";");

		    String emailsProv[] = emailRs.split(";");

		    for (String mail : emailsProv) {
			matcher = pattern.matcher(emailRs);

			if (matcher.matches()) {
			    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				listaDeEmail.add(mail.toLowerCase().trim());
			    }
			}
		    }
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(connSigma, psSigma, rsSigma);
	}
	
	if (!flagCadastroAtualizadoNAC){
    	
	    Connection connSapiens = null;
	    PreparedStatement psSapiens = null;
	    ResultSet rsSapiens = null;

	    try {

		connSapiens = PersistEngine.getConnection("SAPIENS");

		StringBuilder sqlSap = new StringBuilder();

		sqlSap.append(" SELECT DISTINCT cli.IntNet, cli.emanfe, ctr.usu_emactr ");
		sqlSap.append(" FROM USU_T160SIG sig WITH(NOLOCK) ");
		sqlSap.append(" INNER JOIN USU_T160CTR ctr WITH(NOLOCK) ON  ctr.usu_codemp = sig.usu_codemp and ctr.usu_codfil = sig.usu_codfil and ctr.usu_numctr = sig.usu_numctr ");
		sqlSap.append(" INNER JOIN dbo.E085CLI cli WITH(NOLOCK) ON CLI.CodCli = CTR.usu_codcli ");
		sqlSap.append(" WHERE sig.usu_codcli =" + cdCliente);

		psSapiens = connSapiens.prepareStatement(sqlSap.toString());

		rsSapiens = psSapiens.executeQuery();

		while (rsSapiens.next()) {

		    String intNet = rsSapiens.getString("IntNet");
		    String emanfe = rsSapiens.getString("emanfe");
		    String usu_emactr = rsSapiens.getString("usu_emactr");

		    if (intNet != null && !intNet.isEmpty()) {
			intNet = intNet.replaceAll(",", ";");
			String rsEmails[] = intNet.split(";");

			for (String mail : rsEmails) {
			    matcher = pattern.matcher(mail);
			    if (matcher.matches()) {
				if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				    listaDeEmail.add(mail.toLowerCase().trim());
				}
			    }
			}
		    }

		    if (emanfe != null && !emanfe.isEmpty()) {
			emanfe = emanfe.replaceAll(",", ";");
			String rsEmails[] = emanfe.split(";");

			for (String mail : rsEmails) {
			    matcher = pattern.matcher(mail);
			    if (matcher.matches()) {
				if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				    listaDeEmail.add(mail.toLowerCase().trim());
				}
			    }
			}
		    }

		    if (usu_emactr != null && !usu_emactr.isEmpty()) {
			usu_emactr = usu_emactr.replaceAll(",", ";");
			String rsEmails[] = usu_emactr.split(";");

			for (String mail : rsEmails) {
			    matcher = pattern.matcher(mail);
			    if (matcher.matches()) {
				if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				    listaDeEmail.add(mail.toLowerCase().trim());
				}
			    }
			}
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(connSapiens, psSapiens, rsSapiens);
	    }
  	    
	    
	}
	
	return listaDeEmail;

    }
    
    
    public static List<String> getEmailsGrupoCliente(int grupoCliente){

	List<String> retorno = new ArrayList<String>();
	
	final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	Pattern pattern;
	Matcher matcher;
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	
	try {
	    pattern = Pattern.compile(EMAIL_PATTERN);
	} catch (PatternSyntaxException e) {
	    e.printStackTrace();
	    return retorno;
	}
	
	StringBuilder sql = new StringBuilder();
	
	sql.append(" SELECT VALUE FROM ( ");
	sql.append(" SELECT DISTINCT CAST(C.EMAILRESP AS VARCHAR(255)) AS EMAILRESP, CAST(P.EMAIL AS VARCHAR(255)) AS EMAIL, CAST(M.NM_EMAIL AS VARCHAR(255))AS NM_EMAIL FROM dbCENTRAL C WITH(NOLOCK) ");
	sql.append(" LEFT JOIN DBPROVIDENCIA P WITH(NOLOCK) ON C.CD_CLIENTE = P.CD_CLIENTE ");
	sql.append(" LEFT JOIN MENSAGEM_CONFIG M WITH(NOLOCK) ON M.CD_CLIENTE = C.CD_CLIENTE ");
	sql.append(" WHERE C.CD_GRUPO_CLIENTE = ? ");
	sql.append(" ) AS D ");
	sql.append(" UNPIVOT ( ");
	sql.append(" VALUE  "); 
	sql.append(" FOR COL IN (EMAILRESP, EMAIL, NM_EMAIL) ");	
	sql.append(" )UN WHERE LEN(VALUE)>0 ");

	
	
	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, grupoCliente);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		
		String mail = rs.getString(1);
		matcher = pattern.matcher(mail);
		if (matcher.matches()) {
		    if (!retorno.contains(mail.toLowerCase().trim())) {
			retorno.add(mail.toLowerCase().trim());
		    }
		}
		
		

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return retorno;
	
    }
    
    public static List<String> getCgcCpfGrupoCliente(int grupoCliente){
    	List<String> retorno = new ArrayList<String>();
    	
    	Connection conn = null;
    	PreparedStatement pstm = null;
    	ResultSet rs = null;

    	StringBuilder sql = new StringBuilder();
    	
    	sql.append(" SELECT CGCCPF from dbCENTRAL where CD_GRUPO_CLIENTE = ? ");
    	
    	try {
    	    conn = PersistEngine.getConnection("SIGMA90");
    	    pstm = conn.prepareStatement(sql.toString());

    	    pstm.setInt(1, grupoCliente);

    	    rs = pstm.executeQuery();

    	    while (rs.next()) {
    	    	retorno.add(rs.getString("CGCCPF"));
    	    }
    	    
    	} catch (SQLException e) {
    	    e.printStackTrace();
    	} finally {
    	    OrsegupsUtils.closeConnection(conn, pstm, rs);
    	}
    	
    	return retorno;
    }
    
    /**
     * Verifica se cliente está cadastrado em <tt>[RAT] Exceções CGC/CPF</tt> para que não seja encaminhado nenhum tipo de RAT
     * @param cdCliente <code>CD_CLIENTE</code>
     * @return <code>boolean</code> <code>true</code> se cliente está vinculado ao cadastro e não deve receber e-mails;<code>false</code> se não estiver no cadastro e pode receber e-mails normalmente
     */
    public static boolean verificarExcecaoCNPJ(int cdCliente) {

    	List<NeoObject> clientes = PersistEngine.getObjects(AdapterUtils.getEntityClass("RATExcecoesCgcCPF"));
    	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT cli.CgcCpf FROM usu_t160sig SIG WITH(NOLOCK) ");
	sql.append(" INNER JOIN USU_T160CVS cvs WITH (NOLOCK)   ");
	sql.append(" ON cvs.usu_codemp = sig.usu_codemp   ");
	sql.append(" AND cvs.usu_codfil = sig.usu_codfil AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos   ");
	sql.append(" INNER JOIN USU_T160CTR CTR WITH(NOLOCK)   ");
	sql.append(" ON cvs.usu_codemp = ctr.usu_codemp   ");
	sql.append(" AND cvs.usu_codfil = ctr.usu_codfil AND cvs.usu_numctr = ctr.usu_numctr   ");
	sql.append(" INNER JOIN E085CLI cli WITH(NOLOCK) ON CTR.usu_codcli = cli.CodCli   ");
	sql.append(" WHERE SIG.usu_codcli = ? ");
	sql.append(" AND SIG.USU_NUMPOS IN (SELECT MAX (sig2.usu_numpos) FROM usu_t160sig AS sig2   ");
	sql.append(" INNER JOIN usu_t160cvs cvs2 on cvs2.usu_codemp = sig2.usu_codemp and cvs2.usu_codfil = sig2.usu_codfil and cvs2.usu_numctr = sig2.usu_numctr and cvs2.usu_numpos = sig2.usu_numpos  ");
	sql.append(" WHERE sig2.usu_codemp = sig.usu_codemp AND sig2.usu_codfil = sig.usu_codfil AND sig2.usu_numctr = sig.usu_numctr   ");
	sql.append(" AND ((cvs2.usu_sitcvs = 'A') OR (cvs2.usu_sitcvs = 'I' and cvs2.usu_datfim >= GETDATE())))    ");

	long cgcCpf = 0L;

	try {
	    conn = PersistEngine.getConnection("SAPIENS");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, cdCliente);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		cgcCpf = rs.getLong(1);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	if (cgcCpf != 0L) {
		
	    if (clientes != null && !clientes.isEmpty()) {

		for (NeoObject o : clientes) {
		    EntityWrapper w = new EntityWrapper(o);

		    NeoObject cliente = (NeoObject) w.getValue("cliente");

		    EntityWrapper wCliente = new EntityWrapper(cliente);

		    if ((long) wCliente.getValue("cgccpf") == cgcCpf) {
			return true;
		    }

		}
		return false;
	    } else {
		return false;
	    }

	} else {
	    return false;
	}

    }
    
    /**
     * Retorna link do mini mapa Google contendo todos os pontos das casas das contas no grupo informado
     * @param grupo
     * @return String contendo o link para o mapa; se grupo não existir o mapa será inválido
     */
    public static String getMapaGrupoClienteSigma(int grupo) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	String sql = "SELECT DISTINCT C.NU_LATITUDE, C.NU_LONGITUDE FROM dbCENTRAL C WHERE C.CD_GRUPO_CLIENTE=?";

	StringBuilder link = new StringBuilder();

	link.append("https://maps.googleapis.com/maps/api/staticmap?");
	link.append("size=300x300");
	link.append("&maptype=hybrid");
	link.append("&format=png");
	link.append("&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png|");
	

	List<String> posicoes = null;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql);
	    pstm.setInt(1, grupo);

	    rs = pstm.executeQuery();

	    posicoes = new ArrayList<String>();

	    while (rs.next()) {
		posicoes.add(rs.getString(1) + "," + rs.getString(2));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	
	link.append(Joiner.on("|").join(posicoes));

	return link.toString();
    }
    
    /**
     * Atualiza em [TI] Monitora agendador o fim da execução da rotina de e-mail informada
     * @param nomeRotina
     * @return true se atualizado com sucesso; false não ocorrer atualização
     */
    public static boolean inserirFimRotinaEmail(String nomeRotina){
	
	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append("UPDATE D_TIMonitoraAgendador SET dataFinalAgendador = GETDATE() WHERE nomeRotina = ?");
	
	try {
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setString(1, nomeRotina);
	    
	    int rs = pstm.executeUpdate();

	    if (rs > 0){
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
	return false;
	
    }
    
    /**
     * Busca a ultima execução da rotina de e-mail informada
     * @param nomeRotina
     * @return data da ultima execução da rotina ou se algum problema na busca ocorrer, retornará a data de 20 minutos da data atual
     */
    public static String ultimaExecucaoRotinaEmail(String nomeRotina){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append("SELECT dataFinalAgendador FROM D_TIMonitoraAgendador WHERE nomeRotina = ?");
	
	GregorianCalendar data = new GregorianCalendar();
	
	GregorianCalendar inicioExecucao = (GregorianCalendar) data.clone();

	try {
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setString(1, nomeRotina);
	    
	    rs = pstm.executeQuery();

	    if (rs.next()) {
		data.setTimeInMillis(rs.getTimestamp(1).getTime());
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	String retorno = null;
	
	try{
	    if (data.equals(inicioExecucao)){
		data.add(Calendar.MINUTE, -20);
	    }
	    retorno = NeoDateUtils.safeDateFormat(data, "yyyy-MM-dd HH:mm:ss");
	}catch(Exception e){
	    e.printStackTrace();
	}
	
	return retorno;
    }
    
    /**
     * Busca as imagens do histórico informado no repositório na Amazon
     * @param historico
     * @return Lista contendo as URLs para as imagens
     */
    public static List<String> getImagensAmazonS3(int historico){
	
	StringBuilder retorno = new StringBuilder();
	
	try {

	    URL url = new URL("https://apps2.orsegups.com.br:8080/AtendimentoEventosServices/amazonS3/getUrlsImagensAtendimento/" + historico);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("x-auth-token", "682ee2548d4a73d8fadf72f32c52df790eb72aff");

	    if (conn.getResponseCode() != 200) {
		inserirLogErro(historico, conn.getResponseMessage());
		return null;
	    }


	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()),"UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {
	    inserirLogErro(historico, e.getMessage());
	    e.printStackTrace();

	} catch (IOException e) {
	    inserirLogErro(historico, e.getMessage());
	    e.printStackTrace();

	}
	
	
	if(!retorno.toString().isEmpty()){
	    Gson gson = new Gson();
	    
	    
	    @SuppressWarnings("unchecked")
	    List<String> resultList = (List<String>) gson.fromJson(retorno.toString(), List.class);
	    
	    
	    return resultList;
	}
	

	
	return null;
    }
    
    /**
     * Retorna uma lista com os erros de gravação de imagens do serviço de atendimento de evento. <br> Consulta efetuada diretamente no banco
     * @param historico do evento
     * @return Lista de strings contendo um ou mais erros; retorna uma lista vazia se nenhum erro for encontrado
     */
    public static List<String> getErroGravacaoImagem(int historico){
	
	List<String> retorno = new ArrayList<String>();
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append("SELECT id FROM log_salvar_imagens_ao_vivo WITH(NOLOCK) WHERE historico = ? ");
	
	try {
	    conn = PersistEngine.getConnection("ATENDIMENTO_SERVICES");
	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setInt(1, historico);

	    rs = pstm.executeQuery();
	    
	    while(rs.next()){
		retorno.add(rs.getString(1));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
	return retorno;
    }
    
    public static void sendTestEmail(StringBuilder msg, String assunto) {
	sendTestEmail(null, msg, assunto);
    }
    
    public static void sendTestEmail(List<String> testEmails, StringBuilder msg, String assunto) {
	try {
	    MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
	    MailSettings mailClone = new MailSettings();
	    mailClone.setMinutesInterval(settings.getMinutesInterval());
	    mailClone.setRenderServer(settings.getRenderServer());
	    mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
	    mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
	    mailClone.setSmtpSettings(settings.getSmtpSettings());
	    mailClone.setPort(settings.getPort());
	    mailClone.setSmtpServer(settings.getSmtpServer());
	    mailClone.setEnabled(settings.isEnabled());
	    mailClone.setFromEMail("cm.rat@orsegups.com.br");
	    mailClone.setFromName("Orsegups Participações S.A.");

	    HtmlEmail noUserEmail = new HtmlEmail();
	    noUserEmail.setCharset("UTF-8");
	    
	    if (testEmails == null || testEmails.isEmpty()) {
		noUserEmail.addTo("herisson.ferreira@orsegups.com.br");
	    } else {
		for (String email : testEmails) {
		    if (validEmail(email))
			noUserEmail.addTo(email);
		}
	    }
	    noUserEmail.setSubject(assunto);
	    noUserEmail.setHtmlMsg(msg.toString());
	    noUserEmail.setContent(msg.toString(), "text/html");
	    
	    mailClone.applyConfig(noUserEmail);

	    noUserEmail.send();

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
    private static boolean validEmail(String email) {
	Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
	Matcher m = p.matcher(email);
	if (m.find()) {
	    return true;
	} else {
	    return false;
	}
    }
    
    private static void inserirLogErro(int historico, String erro){
	
	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append("INSERT INTO Log_OrsegupsEmailUtils (historico, erro) VALUES (?,?)");
	
	try {
	    conn = PersistEngine.getConnection("TIDB");
	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setInt(1, historico);
	    pstm.setString(2, erro);
	    
	    pstm.executeUpdate();


	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
	
    }
    
    public static void sendEmailAutomatic(List<String> mails, String msg, String assunto) throws ValidationException{
    	try {
    	    MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
    	    MailSettings mailClone = new MailSettings();
    	    mailClone.setMinutesInterval(settings.getMinutesInterval());
    	    mailClone.setRenderServer(settings.getRenderServer());
    	    mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
    	    mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
    	    mailClone.setSmtpSettings(settings.getSmtpSettings());
    	    mailClone.setPort(settings.getPort());
    	    mailClone.setSmtpServer(settings.getSmtpServer());
    	    mailClone.setEnabled(settings.isEnabled());
    	    mailClone.setFromEMail("emailautomatico@orsegups.com.br");
    	    mailClone.setFromName(assunto);

    	    HtmlEmail noUserEmail = new HtmlEmail();
    	    noUserEmail.setCharset("UTF-8");
    	    
    
    		for (String email : mails) {
    		    if (validEmail(email)){
    		    	noUserEmail.addTo(email);
    		    }else{
    		    	throw new ValidationException("Email "+ email +" inválido.");
    		    }
    		}
    		
    	    noUserEmail.setSubject(assunto);
    	    noUserEmail.setHtmlMsg(msg);
    	    noUserEmail.setContent(msg, "text/html");
    	    
    	    mailClone.applyConfig(noUserEmail);

    	    noUserEmail.send();

    	} catch (Exception e) {
    	    e.printStackTrace();
    	}
        }
    
    public static void sendEmailRastreamento(List<String> mails, String msg, String assunto) throws ValidationException{
    	try {
    	    MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
    	    MailSettings mailClone = new MailSettings();
    	    mailClone.setMinutesInterval(settings.getMinutesInterval());
    	    mailClone.setRenderServer(settings.getRenderServer());
    	    mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
    	    mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
    	    mailClone.setSmtpSettings(settings.getSmtpSettings());
    	    mailClone.setPort(settings.getPort());
    	    mailClone.setSmtpServer(settings.getSmtpServer());
    	    mailClone.setEnabled(settings.isEnabled());
    	    mailClone.setFromEMail("frotarastreamento@orsegups.com.br");
    	    mailClone.setFromName(assunto);

    	    HtmlEmail noUserEmail = new HtmlEmail();
    	    noUserEmail.setCharset("UTF-8");
    	    
    
    		for (String email : mails) {
    		    if (validEmail(email)){
    		    	noUserEmail.addTo(email);
    		    }else{
    		    	throw new ValidationException("Email "+ email +" inválido.");
    		    }
    		}
    		
    	    noUserEmail.setSubject(assunto);
    	    noUserEmail.setHtmlMsg(msg);
    	    noUserEmail.setContent(msg, "text/html");
    	    
    	    mailClone.applyConfig(noUserEmail);

    	    noUserEmail.send();

    	} catch (Exception e) {
    	    e.printStackTrace();
    	}
        }

}
