package com.neomind.fusion.custom.orsegups.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

@WebServlet(name = "FCIRecuperaIndiceServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.servlets.FCIRecuperaIndiceServlet" })
public class FCIRecuperaIndiceServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/plain");
		response.setCharacterEncoding("ISO-8859-1");

		final PrintWriter out = response.getWriter();

		Connection conn = null;
		StringBuilder sqlTarefas = new StringBuilder();
		PreparedStatement pstm = null;
		String resposta = "erro";
		Integer indice;
		ResultSet rs = null;
		conn = PersistEngine.getConnection("");

		sqlTarefas.append("select n.neoId from dbo.SecurityEntity secPaper ");
		sqlTarefas.append("inner join dbo.NeoPaper_users nps on secPaper.neoId = nps.papers_neoId ");
		sqlTarefas.append("inner join dbo.NeoUser n on n.neoId = nps.users_neoId ");
		sqlTarefas.append("inner join dbo.SecurityEntity secUser on n.neoId = secUser.neoId ");
		sqlTarefas.append("where secPaper.code = 'FCIUsuariosLigacoes' and ");
		sqlTarefas.append("secUser.active = 1 and ");
		sqlTarefas.append("n.neoId <>  1240041 and ");
		sqlTarefas.append("n.fullName not like 'zz%' ");

		try
		{
			pstm = conn.prepareStatement(sqlTarefas.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				indice = rs.getRow();
				resposta = indice.toString();

			}
			

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);
			out.print(resposta);

		}
	}

}
