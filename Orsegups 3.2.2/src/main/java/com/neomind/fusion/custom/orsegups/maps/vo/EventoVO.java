package com.neomind.fusion.custom.orsegups.maps.vo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoAcessoClienteVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoHistoricoVO;


public class EventoVO
{
	private String codigoHistorico;
	
	private String codigoEvento;
	
	private Long prioridade;
	
	private String codigoCliente;
	
	private String nomeViatura;
	
	private Integer status;
	
	private String nomeEvento;
	
	private String codigoViatura;
	
	private String dataRecebimento;
	
	private Timestamp dtRecebido;
	
	private String dataDeslocamentoViatura;

	private String dataViaturaNoLocal;
	
	private String codigoCentral;
	
	private String particao;
	
	private String fantasia;
	
	private String razao;
	
	private String endereco;
	
	private String nomeCidade;
	
	private String Bairro;
	
	private String observacaoFechamento;
	
	private String observavaoGerente;
	
	private String telefone;
	
	private Float latitude;
	
	private Float longitude;
	
	private String placa;
	
	private Boolean remover = false;
	
	private String nomeImagem = "evento";
	
	private String corPrioridade = "#FF24E9";
	
	private String infoWindowContent;

	private String type;
	
	private String corTexto = "#000000";
	
	private String empresa;
	
	private String pergunta;
	
	private String resposta;
	
	private String responsavel;
	
	private String emailResponsavel;
	
	private String telefone1;
	
	private String telefone2;
	
	private String cgccpf;
	
	private String rota;
	
	private String uf;
	
	private String cep;
	
	private List<ProvidenciaVO> providenciaVOs;
	
	private List<EventoHistoricoVO> eventoHistoricoVOs;
	
	private String nomeEmpresa;
	
	private String obsTemp;
	
	private String obsProvidencia;
	
	private String dataNovoX2;
	
	private List<EventoAcessoClienteVO> eventoAcessoClienteVOs;
	
	private Boolean historicoPai;
	
	private String senhaCoacao;
	
	private String dataFechamento;
	
	private String textoObservacao;
	
	private String motivo;
	
	private String usuarioFechamento;
	
	private String textoObservacaoGerente;
	
	private String cdCode;
	
	private String servidorCFTV;
	
	private String portaCFTV;
	
	private String usuarioCFTV;
	
	private String senhaCFTV;
	
	private long peso;
	
	private double distancia;
	
	private long codigo;
	
	private String observacaoEmpresa;
	
	private Long grupo;
	
	private int tipoPessoa;
	
	private Boolean atrasoDeslocamento;
	private Boolean atrasoFechamento;
	
	
	
	public Boolean getAtrasoDeslocamento() {
		return atrasoDeslocamento;
	}

	public void setAtrasoDeslocamento(Boolean atrasoDeslocamento) {
		this.atrasoDeslocamento = atrasoDeslocamento;
	}

	public Boolean getAtrasoFechamento() {
		return atrasoFechamento;
	}

	public void setAtrasoFechamento(Boolean atrasoFechamento) {
		this.atrasoFechamento = atrasoFechamento;
	}

	public int getTipoPessoa() {
	    return tipoPessoa;
	}

	public void setTipoPessoa(int tipoPessoa) {
	    this.tipoPessoa = tipoPessoa;
	}

	public String getObservacaoEmpresa() {
	    return observacaoEmpresa;
	}

	public void setObservacaoEmpresa(String observacaoEmpresa) {
	    this.observacaoEmpresa = observacaoEmpresa;
	}

	/**
	 * Referente ao código do evento na fila de espera (posição na fila)
	 * @return posição do evento na fila
	 */
	public long getCodigo() {
	    return codigo;
	}

	/**
	 * Referente ao código do evento na fila de espera (posição na fila)
	 * @param codigo
	 */
	public void setCodigo(long codigo) {
	    this.codigo = codigo;
	}

	public long getPeso() {
	    return peso;
	}

	public void setPeso(long peso) {
	    this.peso = peso;
	}

	public double getDistancia() {
	    return distancia;
	}

	public void setDistancia(double distancia) {
	    this.distancia = distancia;
	}

	public Timestamp getDtRecebido() {
	    return dtRecebido;
	}

	public void setDtRecebido(Timestamp dtRecebido) {
	    this.dtRecebido = dtRecebido;
	}
	
	
	public String getServidorCFTV() {
	    return servidorCFTV;
	}

	public void setServidorCFTV(String servidorCFTV) {
	    this.servidorCFTV = servidorCFTV;
	}

	public String getPortaCFTV() {
	    return portaCFTV;
	}

	public void setPortaCFTV(String portaCFTV) {
	    this.portaCFTV = portaCFTV;
	}

	public String getUsuarioCFTV() {
	    return usuarioCFTV;
	}

	public void setUsuarioCFTV(String usuarioCFTV) {
	    this.usuarioCFTV = usuarioCFTV;
	}

	public String getSenhaCFTV() {
	    return senhaCFTV;
	}

	public void setSenhaCFTV(String senhaCFTV) {
	    this.senhaCFTV = senhaCFTV;
	}

	public String getCodigoHistorico()
	{
		return codigoHistorico;
	}

	public void setCodigoHistorico(String codigoHistorico)
	{
		this.codigoHistorico = codigoHistorico;
	}

	public String getCodigoEvento()
	{
		return codigoEvento;
	}

	public void setCodigoEvento(String codigoEvento)
	{
		this.codigoEvento = codigoEvento;
	}

	public Long getPrioridade()
	{
		return prioridade;
	}

	public void setPrioridade(Long prioridade)
	{
		this.prioridade = prioridade;
	}

	public String getCodigoCliente()
	{
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente)
	{
		this.codigoCliente = codigoCliente;
	}

	public String getNomeViatura()
	{
		return nomeViatura;
	}

	public void setNomeViatura(String nomeViatura)
	{
		this.nomeViatura = nomeViatura;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public String getNomeEvento()
	{
		return nomeEvento;
	}

	public void setNomeEvento(String nomeEvento)
	{
		this.nomeEvento = nomeEvento;
	}

	public String getCodigoViatura()
	{
		return codigoViatura;
	}

	public void setCodigoViatura(String codigoViatura)
	{
		this.codigoViatura = codigoViatura;
	}

	public String getDataRecebimento()
	{
		return dataRecebimento;
	}

	public void setDataRecebimento(String dataRecebimento)
	{
		this.dataRecebimento = dataRecebimento;
	}

	public String getDataDeslocamentoViatura()
	{
		return dataDeslocamentoViatura;
	}

	public void setDataDeslocamentoViatura(String dataDeslocamentoViatura)
	{
		this.dataDeslocamentoViatura = dataDeslocamentoViatura;
	}

	public String getDataViaturaNoLocal()
	{
		return dataViaturaNoLocal;
	}

	public void setDataViaturaNoLocal(String dataViaturaNoLocal)
	{
		this.dataViaturaNoLocal = dataViaturaNoLocal;
	}

	public String getCodigoCentral()
	{
		return codigoCentral;
	}

	public void setCodigoCentral(String codigoCentral)
	{
		this.codigoCentral = codigoCentral;
	}

	public String getParticao()
	{
		return particao;
	}

	public void setParticao(String particao)
	{
		this.particao = particao;
	}

	public String getFantasia()
	{
		return fantasia;
	}

	public void setFantasia(String fantasia)
	{
		this.fantasia = fantasia;
	}

	public String getRazao()
	{
		return razao;
	}

	public void setRazao(String razao)
	{
		this.razao = razao;
	}

	public String getEndereco()
	{
		return endereco;
	}

	public void setEndereco(String endereco)
	{
		this.endereco = endereco;
	}

	public String getNomeCidade()
	{
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade)
	{
		this.nomeCidade = nomeCidade;
	}

	public String getBairro()
	{
		return Bairro;
	}

	public void setBairro(String bairro)
	{
		Bairro = bairro;
	}

	public String getObservacaoFechamento()
	{
		return observacaoFechamento;
	}

	public void setObservacaoFechamento(String observacaoFechamento)
	{
		this.observacaoFechamento = observacaoFechamento;
	}

	public String getObservavaoGerente()
	{
		return observavaoGerente;
	}

	public void setObservavaoGerente(String observavaoGerente)
	{
		this.observavaoGerente = observavaoGerente;
	}

	public String getTelefone()
	{
		return telefone;
	}

	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}

	public Float getLatitude()
	{
		return latitude;
	}

	public void setLatitude(Float latitude)
	{
		this.latitude = latitude;
	}

	public Float getLongitude()
	{
		return longitude;
	}

	public void setLongitude(Float longitude)
	{
		this.longitude = longitude;
	}

	public String getPlaca()
	{
		return placa;
	}

	public void setPlaca(String placa)
	{
		this.placa = placa;
	}

	public Boolean getRemover()
	{
		return remover;
	}

	public void setRemover(Boolean remover)
	{
		this.remover = remover;
	}

	@Override
	public String toString()
	{
		return "[EventoVO] "+this.codigoHistorico;
	}

	public String getNomeImagem()
	{
		return nomeImagem;
	}

	public void setNomeImagem(String nomeImagem)
	{
		this.nomeImagem = nomeImagem;
	}

	public String getCorPrioridade()
	{
		return corPrioridade;
	}

	public void setCorPrioridade(String corPrioridade)
	{
		this.corPrioridade = corPrioridade;
	}

	public String getInfoWindowContent()
	{
		return infoWindowContent;
	}

	public void setInfoWindowContent(String infoWindowContent)
	{
		this.infoWindowContent = infoWindowContent;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getCorTexto()
	{
		return corTexto;
	}

	public void setCorTexto(String corTexto)
	{
		this.corTexto = corTexto;
	}

	public String getEmpresa()
	{
		return empresa;
	}

	public void setEmpresa(String empresa)
	{
		this.empresa = empresa;
	}

	public String getPergunta()
	{
		return pergunta;
	}

	public void setPergunta(String pergunta)
	{
		this.pergunta = pergunta;
	}

	public String getResposta()
	{
		return resposta;
	}

	public void setResposta(String resposta)
	{
		this.resposta = resposta;
	}

	public String getResponsavel()
	{
		return responsavel;
	}

	public void setResponsavel(String responsavel)
	{
		this.responsavel = responsavel;
	}

	public String getEmailResponsavel()
	{
		return emailResponsavel;
	}

	public void setEmailResponsavel(String emailResponsavel)
	{
		this.emailResponsavel = emailResponsavel;
	}

	public String getTelefone1()
	{
		return telefone1;
	}

	public void setTelefone1(String telefone1)
	{
		this.telefone1 = telefone1;
	}

	public String getTelefone2()
	{
		return telefone2;
	}

	public void setTelefone2(String telefone2)
	{
		this.telefone2 = telefone2;
	}

	public String getRota()
	{
		return rota;
	}

	public void setRota(String rota)
	{
		this.rota = rota;
	}

	public String getUf()
	{
		return uf;
	}

	public void setUf(String uf)
	{
		this.uf = uf;
	}

	public String getCep()
	{
		return cep;
	}

	public void setCep(String cep)
	{
		this.cep = cep;
	}

	public String getCgccpf()
	{
		return cgccpf;
	}

	public void setCgccpf(String cgccpf)
	{
		this.cgccpf = cgccpf;
	}

	public List<ProvidenciaVO> getProvidenciaVOs()
	{
		return providenciaVOs;
	}

	public void setProvidenciaVOs(List<ProvidenciaVO> providenciaVOs)
	{
		this.providenciaVOs = providenciaVOs;
	}
	
	public void addProvidencia(ProvidenciaVO providenciaVO)
	{
		if(this.providenciaVOs == null)
		{
			this.providenciaVOs = new ArrayList<ProvidenciaVO>();
		}
		
		this.providenciaVOs.add(providenciaVO);
	}

	public List<EventoHistoricoVO> getEventoHistoricoVOs()
	{
		return eventoHistoricoVOs;
	}

	public void setEventoHistoricoVOs(List<EventoHistoricoVO> eventoHistoricoVOs)
	{
		this.eventoHistoricoVOs = eventoHistoricoVOs;
	}
	
	public void addEventoHistorico(EventoHistoricoVO eventoHistoricoVO)
	{
		if(this.eventoHistoricoVOs == null)
		{
			this.eventoHistoricoVOs = new ArrayList<EventoHistoricoVO>();
		}
		
		this.eventoHistoricoVOs.add(eventoHistoricoVO);
	}

	public String getNomeEmpresa()
	{
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa)
	{
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getObsTemp()
	{
		return obsTemp;
	}

	public void setObsTemp(String obsTemp)
	{
		this.obsTemp = obsTemp;
	}

	public String getObsProvidencia()
	{
		return obsProvidencia;
	}

	public void setObsProvidencia(String obsProvidencia)
	{
		this.obsProvidencia = obsProvidencia;
	}

	public String getDataNovoX2()
	{
		return dataNovoX2;
	}

	public void setDataNovoX2(String dataNovoX2)
	{
		this.dataNovoX2 = dataNovoX2;
	}

	public List<EventoAcessoClienteVO> getEventoAcessoClienteVOs()
	{
		return eventoAcessoClienteVOs;
	}

	public void setEventoAcessoClienteVOs(List<EventoAcessoClienteVO> eventoAcessoClienteVOs)
	{
		this.eventoAcessoClienteVOs = eventoAcessoClienteVOs;
	}
	
	public void addEventoAcessoCliente(EventoAcessoClienteVO eventoAcessoClienteVO)
	{
		if(this.eventoAcessoClienteVOs == null)
		{
			this.eventoAcessoClienteVOs = new ArrayList<EventoAcessoClienteVO>();
		}
		
		this.eventoAcessoClienteVOs.add(eventoAcessoClienteVO);
	}

	public Boolean getHistoricoPai()
	{
		return historicoPai;
	}

	public void setHistoricoPai(Boolean historicoPai)
	{
		this.historicoPai = historicoPai;
	}

	public String getSenhaCoacao()
	{
		return senhaCoacao;
	}

	public void setSenhaCoacao(String senhaCoacao)
	{
		this.senhaCoacao = senhaCoacao;
	}

	public String getDataFechamento()
	{
		return dataFechamento;
	}

	public void setDataFechamento(String dataFechamento)
	{
		this.dataFechamento = dataFechamento;
	}

	public String getTextoObservacao()
	{
		return textoObservacao;
	}

	public void setTextoObservacao(String textoObservacao)
	{
		this.textoObservacao = textoObservacao;
	}

	public String getMotivo()
	{
		return motivo;
	}

	public void setMotivo(String motivo)
	{
		this.motivo = motivo;
	}

	public String getUsuarioFechamento()
	{
		return usuarioFechamento;
	}

	public void setUsuarioFechamento(String usuarioFechamento)
	{
		this.usuarioFechamento = usuarioFechamento;
	}

	public String getTextoObservacaoGerente()
	{
		return textoObservacaoGerente;
	}

	public void setTextoObservacaoGerente(String textoObservacaoGerente)
	{
		this.textoObservacaoGerente = textoObservacaoGerente;
	}

	public String getCdCode()
	{
		return cdCode;
	}

	public void setCdCode(String cdCode)
	{
		this.cdCode = cdCode;
	}

	public Long getGrupo() {
	    return grupo;
	}
	
	public void setGrupo(Long grupo) {
	    this.grupo = grupo;
	}
}
