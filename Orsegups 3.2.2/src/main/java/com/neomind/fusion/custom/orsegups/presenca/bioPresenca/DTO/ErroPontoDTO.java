package com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author lucas.alison
 *
 */

public class ErroPontoDTO implements ErroDTOPai{
    
    @JsonProperty("id")
    private String id;
    
    @JsonProperty("return")
    private String retorno;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }
    
    @Override
    public String toString() {        
        return "{"+getId()+":"+getRetorno()+"}";
    }
    
}
