package com.neomind.fusion.custom.orsegups.contract;

import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.entity.EntityAdapter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.util.NeoUtils;

public class ContratoControlaSequencialEquipamentos implements EntityAdapter
{
	public void run(Map<String, Object> params)
	{
		try
		{
			if (params.get(EVENT).equals(Events.PreUpdate.toString())) 
			{
				if (NeoUtils.safeIsNotNull(params.get(EntityAdapter.EFORM)))
				{
					EForm form = (EForm) params.get(EntityAdapter.EFORM);
					EForm efPai = (EForm) form.getCaller();
					
					NeoObject equipamento = form.getObject();
					EntityWrapper wEquipamento = new EntityWrapper(equipamento);
					
					//long sequencia = NeoUtils.safeLong(NeoUtils.safeOutputString(efPai.findField("sequencia").getValue()));
					NeoObject equipPosto = efPai.getObject();
					EntityWrapper wEquipPosto = new EntityWrapper(equipPosto);
					
					long sequencia = NeoUtils.safeLong(NeoUtils.safeOutputString(wEquipPosto.findValue("sequencia")));
					sequencia++;
					wEquipamento.setValue("sequencia", sequencia);
					
					wEquipPosto.setValue("sequencia", sequencia);
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
