package com.neomind.fusion.custom.orsegups.fap.utils;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

/**
 * Usa um eform para consultar parametros diversos que hoje estao fixos no código. Tudo que for parametro de FAP deve ser passado para esse eform.
 * Desta maneira, não será necessário reiniciar o fusion se algum parametro precisar ser alterado.
 * @author diogo.silva
 *
 */

public class FAPParametrizacao { 
		
    static String eformParams = "FAPParametrizacao";
	
	/**
	 * Consulta um parametro e retorna null caso o mesmo não seja econtrato ou ocorra algum erro de acesso ao eform
	 * @param parameter
	 * @return
	 */
    
	public static String findParameter(String parameter){
	    return findParameter(parameter, false);
	}
	
	public static String findParameter(String parameter, Boolean isTecnico){
	    	if (isTecnico)
	    	    eformParams = "FAPParametrizacaoTecnico";
	    	else
	    		eformParams = "FAPParametrizacao";
	    
		String retorno = "";
		try {
			QLGroupFilter gp = new QLGroupFilter("AND");	
			gp.addFilter( new QLEqualsFilter("parametro", parameter ) );
			NeoObject oControleParametreizacao = PersistEngine.getObject(AdapterUtils.getEntityClass(eformParams), gp );
			EntityWrapper wControleParametreizacao = new EntityWrapper(oControleParametreizacao);
			retorno = wControleParametreizacao.findGenericValue("valor");
			return retorno;
		} catch(Exception e) {
			e.printStackTrace();
			return retorno = "0";
		}
	}  
    
}
