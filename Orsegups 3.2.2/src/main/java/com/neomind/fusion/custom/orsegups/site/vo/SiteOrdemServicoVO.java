package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteOrdemServicoVO {
	
	private Long idOrdem;
	private String abertura;
	private String defeito;
	private String fechamento;
	private String executado;
	private String fechado;
	private SiteContaSigmaVO contaSigmaVO;
	public Long getIdOrdem() {
		return idOrdem;
	}
	public void setIdOrdem(Long idOrdem) {
		this.idOrdem = idOrdem;
	}
	public String getAbertura() {
		return abertura;
	}
	public void setAbertura(String abertura) {
		this.abertura = abertura;
	}
	public String getDefeito() {
		return defeito;
	}
	public void setDefeito(String defeito) {
		this.defeito = defeito;
	}
	public String getFechamento() {
		return fechamento;
	}
	public void setFechamento(String fechamento) {
		this.fechamento = fechamento;
	}
	public SiteContaSigmaVO getContaSigmaVO() {
		return contaSigmaVO;
	}
	public void setContaSigmaVO(SiteContaSigmaVO contaSigmaVO) {
		this.contaSigmaVO = contaSigmaVO;
	}
	public String getExecutado() {
		return executado;
	}
	public void setExecutado(String executado) {
		this.executado = executado;
	}
	public String getFechado() {
		return fechado;
	}
	public void setFechado(String fechado) {
		this.fechado = fechado;
	}
	
	
}
