package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesMetasExecutivos implements CustomJobAdapter
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesMetasExecutivos.class);
	
	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sqlMetasExecutivos = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesMetasExecutivos");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Metas Executivos - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		GregorianCalendar dataAtual = new GregorianCalendar();
		dataAtual.add(Calendar.MONTH, - 1);
		dataAtual.set(Calendar.DATE, 1);
		
		GregorianCalendar dataCompetencia = new GregorianCalendar( dataAtual.get(GregorianCalendar.YEAR), dataAtual.get(GregorianCalendar.MONTH), dataAtual.get(GregorianCalendar.DAY_OF_MONTH));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		try
		{
			//Faltas não justificadas			
			sqlMetasExecutivos.append(" SELECT fun.NomFun, ver.NumEmp, ver.TipCol, ver.NumCad, fun.NumCpf, orn.USU_CodReg, ISNULL(com.ValEve, 0) AS VlrCom, SUM(ver.ValEve) AS VlrPrv ");
			sqlMetasExecutivos.append(" FROM R046VER ver ");
			sqlMetasExecutivos.append(" INNER JOIN R008EVC evc ON evc.CodTab = ver.TabEve AND evc.CodEve = ver.CodEve ");
			sqlMetasExecutivos.append(" INNER JOIN R044CAL cal ON cal.NumEmp = ver.NumEmp AND cal.CodCal = ver.CodCal ");
			sqlMetasExecutivos.append(" INNER JOIN R034FUN fun ON fun.NumEmp = ver.NumEmp AND fun.TipCol = ver.TipCol AND fun.NumCad = ver.NumCad ");
			sqlMetasExecutivos.append(" INNER JOIN R016ORN orn ON orn.TabOrg = fun.TabOrg AND orn.NumLoc = fun.NumLoc ");
			sqlMetasExecutivos.append(" INNER JOIN R024CAR car ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
			sqlMetasExecutivos.append(" LEFT OUTER JOIN (SELECT verCom.NumEmp, verCom.TipCol, verCom.NumCad, verCom.CodCal, verCom.ValEve ");
			sqlMetasExecutivos.append(" 				 FROM R046VER verCom ");
			sqlMetasExecutivos.append(" 				 INNER JOIN R044CAL calCom ON calCom.NumEmp = verCom.NumEmp AND calCom.CodCal = verCom.CodCal ");
			sqlMetasExecutivos.append(" 				 WHERE calCom.PerRef = ? ");
			sqlMetasExecutivos.append(" 				 AND verCom.TabEve = 941 AND verCom.CodEve = 268 ) AS com ON com.NumEmp = ver.NumEmp AND com.TipCol = ver.TipCol AND com.NumCad = ver.NumCad AND com.CodCal = ver.CodCal ");
			sqlMetasExecutivos.append(" WHERE cal.PerRef = ? AND evc.TipEve IN (1, 2) AND ver.TabEve = 941 AND ver.CodEve <> 268 ");
			sqlMetasExecutivos.append(" AND (car.TitRed LIKE '%executivo%venda%' OR car.TitRed LIKE '%executivo%negocio%') ");
			sqlMetasExecutivos.append(" AND fun.SitAfa IN (1, 2) ");
			sqlMetasExecutivos.append(" AND fun.DatAdm < DATEADD(mm, -3, GETDATE()) ");
			sqlMetasExecutivos.append(" AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].fusion_Producao.DBO.d_TarefaMetasExecutivos tme WITH (NOLOCK) ");
			sqlMetasExecutivos.append(" 				WHERE tme.numcpf = fun.NumCpf and cast(floor(cast(tme.datcpt as float)) as datetime) = cast(floor(cast(? as float)) as datetime)) ");
			sqlMetasExecutivos.append(" GROUP BY fun.NomFun, ver.NumEmp, ver.TipCol, ver.NumCad, fun.NumCpf, com.ValEve, orn.USU_CodReg ");
			sqlMetasExecutivos.append(" HAVING com.ValEve < SUM(ver.ValEve) ");

			pstm = conn.prepareStatement(sqlMetasExecutivos.toString());
			pstm.setTimestamp(1, new Timestamp(dataCompetencia.getTimeInMillis()));
			pstm.setTimestamp(2, new Timestamp(dataCompetencia.getTimeInMillis()));
			pstm.setTimestamp(3, new Timestamp(dataCompetencia.getTimeInMillis()));
			
			rs = pstm.executeQuery();

			while (rs.next())
			{
				String solicitante = "gilsoncesar";
				String titulo = "Realizar desligamento de executivo de vendas - " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun");
				titulo = URLEncoder.encode(titulo, "ISO-8859-1");
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 7L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				Long codReg = rs.getLong("USU_CodReg");

				NeoPaper papel = new NeoPaper();
				String executor = "";

				papel = OrsegupsUtils.getPapelGerenteRegional(codReg);

				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						break;
					}
				}
				else
				{
					log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Metas Executivos - Regra: Falta não justificada - Papel " + papel.getCode() + " sem usuário definido - Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoUtils.safeDateFormat(rs.getDate("DatAfa"), "dd/MM/yyyy"));
					continue;
				}

				String descricao = "";
				descricao = " <strong>Descrição:</strong> Realizar o desligamento do(a) executivo(a) de vendas " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun") + " por baixa produtividade<br>";
				descricao = descricao + " <strong>Competência Folha:</strong> " + NeoUtils.safeDateFormat(dataCompetencia, "MM/yyyy") + "<br>";
				descricao = descricao + " <strong>Proventos:</strong> R$ " + NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("VlrPrv")) + "<br>";
				descricao = descricao + " <strong>Comissão:</strong> R$ " + NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("VlrCom")) + "<br>";

				descricao = URLEncoder.encode(descricao, "ISO-8859-1");

				String urlTarefa = "";
				urlTarefa = OrsegupsUtils.URL_PRODUCAO + "/fusion/custom/jsp/orsegups/iniciarTarefaSimples.jsp?avanca=sim&solicitante=" + solicitante + "&executor=" + executor + "&origem=1&descricao=" + descricao + "&titulo=" + titulo + "&prazo=" + NeoUtils.safeDateFormat(prazo, "dd/MM/yyyy");

				URL url = new URL(urlTarefa);
				URLConnection uc = url.openConnection();
				BufferedReader leitorArquivo = new BufferedReader(new InputStreamReader(uc.getInputStream()));
				String tarefa = "";
				String leitor = "";
				while ((leitor = leitorArquivo.readLine()) != null)
				{
					tarefa = leitor;
				}

				InstantiableEntityInfo tme = AdapterUtils.getInstantiableEntityInfo("TarefaMetasExecutivos");
				NeoObject noTme = tme.createNewInstance();
				EntityWrapper tmeWrapper = new EntityWrapper(noTme);

				tmeWrapper.findField("numemp").setValue(rs.getLong("NumEmp"));
				tmeWrapper.findField("tipcol").setValue(rs.getLong("TipCol"));
				tmeWrapper.findField("numcad").setValue(rs.getLong("NumCad"));
				tmeWrapper.findField("numcpf").setValue(rs.getLong("NumCpf"));
				tmeWrapper.findField("datcpt").setValue(dataCompetencia);
				tmeWrapper.findField("tarefa").setValue(tarefa);
				PersistEngine.persist(noTme);

				log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Metas Executivos - Responsavel " + executor + " Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Competência folha: " + NeoUtils.safeDateFormat(dataCompetencia, "dd/MM/yyyy"));
				
				Thread.sleep(10000);
			}
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Metas Executivos");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Metas Executivos - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
