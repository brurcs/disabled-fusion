package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EnderecoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaSupervisaoUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesFeriasProgramadas implements CustomJobAdapter
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesFeriasProgramadas.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesFeriasProgramadas");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Ferias Programadas - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		try
		{
			sql.append(" Select fun.numemp, fun.numcad, fun.nomfun, fun.numcpf, reg.USU_CodReg, reg.USU_NomReg, cob.usu_datalt, cob.usu_horini, cob.usu_datfim, car.TitRed, ");
			sql.append("		cob.usu_numempcob, cob.usu_numcadcob, fis.USU_NomFis,cpl.endrua,cpl.endcpl,cpl.endnum,bai.NomBai,cid.NomCid, orn.usu_codccu, orn.usu_lotorn From USU_T038COBFUN cob ");
			sql.append(" Inner Join R034FUN fun On fun.numemp = cob.usu_numemp And fun.tipcol = cob.usu_tipcol And fun.numcad = cob.usu_numcad ");
			sql.append(" Inner Join R038HLO hlo On hlo.NumEmp = fun.numemp And hlo.tipcol = fun.tipcol And hlo.numcad = fun.numcad And hlo.DatAlt = (Select Max(hlo2.datalt) From R038HLO hlo2 Where hlo2.numemp = hlo.numemp And hlo2.tipcol = hlo.tipcol And hlo2.numcad = hlo.numcad And hlo2.DatAlt <= cob.usu_datfim) ");
			sql.append(" Inner Join R016ORN orn On orn.taborg = hlo.TabOrg And orn.NumLoc = hlo.NumLoc ");
			sql.append(" Inner join USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg ");
			sql.append(" Inner Join R038HCA hca On hca.NumEmp = fun.numemp And hca.tipcol = fun.tipcol And hca.numcad = fun.numcad And hca.DatAlt = (Select Max(hca2.datalt) From R038HCA hca2 Where hca2.numemp = hca.numemp And hca2.tipcol = hca.tipcol And hca2.numcad = hca.numcad And hca2.DatAlt <= cob.usu_datfim) ");
			sql.append(" Inner Join R024CAR car On car.CodCar = fun.codcar And car.EstCar = fun.estcar ");
			sql.append(" Inner JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
			sql.append(" Inner join r034cpl cpl on cpl.numcad = fun.numcad and cpl.numemp = fun.numemp and cpl.tipcol = fun.tipcol");
			sql.append(" Inner join R074BAI bai on bai.CodBai = cpl.codbai and bai.CodCid = cpl.codcid");
			sql.append(" Inner join R074CID cid on cid.CodCid = cpl.codcid");
			sql.append(" Where cob.usu_codmot = 3 And cob.usu_datfim >= '2014-10-19' And reg.USU_CodReg <> 0 ");
			sql.append(" And cob.usu_datfim = GetDate() + 16 ");
			sql.append(" And (fun.numemp <> cob.usu_numempcob Or fun.tipcol <> cob.usu_tipcolcob Or fun.numcad <> cob.usu_numcadcob)");
			sql.append(" And not exists (select * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaFeriasProgramadas fp WITH (NOLOCK) ");
			sql.append(" 			  	 where fp.numcpf = fun.numcpf and cast(floor(cast(fp.datalt as float)) as datetime) = cast(floor(cast(cob.usu_datalt as float)) as datetime) and fp.horini = cob.usu_horini) ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				
				Long numemp = rs.getLong("numemp");
				Long numcad = rs.getLong("numcad");
				Long usuCodccu = rs.getLong("usu_codccu");
				
				String nomfun = rs.getString("nomfun");
				String titcar = rs.getString("titred");
				String lotacao = rs.getString("usu_lotorn");
				Long codreg = rs.getLong("USU_CodReg");
				Long horini = rs.getLong("usu_horini");
				
				GregorianCalendar dataInicio = new GregorianCalendar();
				dataInicio.setTime(rs.getDate("usu_datalt"));
				
				GregorianCalendar dataFim = new GregorianCalendar();
				dataFim.setTime(rs.getDate("usu_datfim"));
				dataFim.add(Calendar.DAY_OF_MONTH, +1);
				
				GregorianCalendar dataIni = new GregorianCalendar();
				dataIni.setTime(rs.getDate("usu_datalt"));
				
				String sigreg = OrsegupsUtils.getSiglaRegional(codreg);
				
				
				String solicitante = "beatriz.malmann";
				String titulo = "Informar destino de cobertura de férias - " + sigreg + " - " + numemp + " - " + numcad + " - " + nomfun;
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				Long codReg = rs.getLong("USU_CodReg");
				String nomReg = rs.getString("USU_NomReg");
				GregorianCalendar dataAtual = new GregorianCalendar();
				NeoFile anexo = null;
				
				if(numemp == 1 || numemp == 15 || numemp == 17 || numemp == 19 || numemp == 21){
					anexo = OrsegupsUtils.criaNeoFile(new File
							("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Vigilancia_Regional_"+codReg+"_Data_"+NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy")+".pdf"));
				}else if(numemp == 2 || numemp == 6 || numemp == 7 || numemp == 8 || numemp == 22){
					anexo = OrsegupsUtils.criaNeoFile(new File
							("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Asseio_Regional_"+codReg+"_Data_"+NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy")+".pdf"));
				}
				
				
				NeoPaper responsavel = new NeoPaper();
				String executor = "";
				
				ColaboradorVO colab = QLPresencaSupervisaoUtils.retornaSupervisorPosto(numemp, numcad, 1L,usuCodccu);
				
				if (colab != null){
					System.out.println("Colaborador: " + colab);
					executor = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_USUARIO_FUSION, codreg, QLPresencaSupervisaoUtils.recuperaSupervisorExecao(colab.getCpf()), lotacao).getCode();
					System.out.println("responsavel Tarefa: " + colab);

				}else{
					executor = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "juliano.clemente" )).getCode();
				}

				/*responsavel = OrsegupsUtils.getPapelFeriasProgramada(codReg);

				if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
				{
					for (NeoUser user : responsavel.getUsers())
					{
						executor = user.getCode();
						break;
					}
				}
				else
				{
					log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Ferias Programadas - Papel " + responsavel.getCode() + " sem usuário definido - Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoUtils.safeDateFormat(dataInicio, "dd/MM/yyyy"));
					continue;
				}
				*/
				//Tarefa 675653 
				/*if (mes == 11 && (numemp == 1L || numemp == 15L || numemp == 17L || numemp == 18L || numemp == 19L || numemp == 21L || numemp == 22L))
				{
					dataFim = new GregorianCalendar(ano+1, 0, 1);
					dataFim = OrsegupsUtils.getSpecificWorkDay(dataFim, 1L);
				}*/
				
				ColaboradorVO colaborador = new ColaboradorVO();
				colaborador.setEscala(new EscalaVO());
				colaborador.setEscala(QLPresencaUtils.getEscalaColaborador(numcad,numemp,1L));
				colaborador.setEndereco(new EnderecoVO());
				colaborador.getEndereco().setLogradouro(rs.getString("endrua"));
				colaborador.getEndereco().setComplemento(rs.getString("endcpl"));
				colaborador.getEndereco().setNumero(rs.getString("endnum"));
				colaborador.getEndereco().setBairro(rs.getString("NomBai"));
				colaborador.getEndereco().setCidade(rs.getString("NomCid"));
				
				
				String descricao = "";
				descricao = " <strong>Regional:</strong> "+codReg+" - "+nomReg+" <br>";
				descricao = descricao + " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomfun + "<br>";
				descricao = descricao + " <strong>Cargo:</strong> " + titcar + "<br>";
				if((colaborador.getEscala().getDescricao() != null) && (!colaborador.getEscala().getDescricao().equals(""))){
					descricao = descricao + " <strong>Escala:</strong> " + colaborador.getEscala().getDescricao()+"/T "+colaborador.getEscala().getCodigoTurma()+ "<br>";
				}
				String dataCor = NeoUtils.safeDateFormat(dataIni, "yyyy-MM-dd");
				HistoricoLocalVO local = new HistoricoLocalVO();
				local.setPosto(new PostoVO());
				local = QLPresencaUtils.listaHistoricoLocal(numemp, 1L, numcad, dataCor);
				
				descricao = descricao + " <strong>Endereço do colaborador: </strong> "+colaborador.getEndereco().getLogradouro()
						+", Complemento: "+colaborador.getEndereco().getComplemento()
						+" nº"+colaborador.getEndereco().getNumero()
						+" Bairro: "+colaborador.getEndereco().getBairro()+" - "
						+ colaborador.getEndereco().getCidade()+"<br>";
				
				List<EntradaAutorizadaVO> coberturas = new ArrayList<EntradaAutorizadaVO>();
				coberturas = QLPresencaUtils.listaCoberturasTarefasProgramacaoDeFerias(numemp,1L,numcad);
				
				SimpleDateFormat sfinc = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				if(coberturas != null && coberturas.size() > 0){
					descricao = descricao + " <strong>Ultimas Coberturas</strong> <br>";
					for (EntradaAutorizadaVO entradaAutorizadaVO : coberturas)
					{
						descricao = descricao +"Periodo: "+sfinc.format(entradaAutorizadaVO.getDataInicial().getTime())
								+" à "
								+sfinc.format(entradaAutorizadaVO.getDataFinal().getTime());
						descricao = descricao +" - Operação: "+ entradaAutorizadaVO.getDescricaoCobertura();
						if(entradaAutorizadaVO.getColaboradorSubstituido().getNomeColaborador() != null){
							descricao = descricao +" - Substituído: "+ entradaAutorizadaVO.getColaboradorSubstituido().getNomeColaborador();
						}						
						descricao = descricao +" - Local: "+ entradaAutorizadaVO.getPosto().getNomePosto();
						descricao = descricao +" - Obs: "+ entradaAutorizadaVO.getObservacao()+"<br>";
						
					}
				}
				descricao = descricao + " <strong>Disponivel:</strong> " + NeoUtils.safeDateFormat(dataFim, "dd/MM/yyyy") + "<br>";
				descricao = descricao + " <strong>Sugestão 1:</strong> <br>";
				descricao = descricao + " <strong>Sugestão 2:</strong> <br>";
				
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "nao", prazo, anexo);

				InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaFeriasProgramadas");
				NeoObject noAJ = tarefaAJ.createNewInstance();
				EntityWrapper ajWrapper = new EntityWrapper(noAJ);

				ajWrapper.findField("numcpf").setValue(rs.getLong("NumCpf"));
				ajWrapper.findField("datalt").setValue(dataInicio);
				ajWrapper.findField("horini").setValue(horini);
				ajWrapper.findField("tarefa").setValue(tarefa);
				PersistEngine.persist(noAJ);

				log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Ferias Programadas - Regra: Justificar desligamento sem justa causa - Responsavel " + responsavel.getCode() + " Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoUtils.safeDateFormat(dataInicio, "dd/MM/yyyy"));
				
				Thread.sleep(1000);
			}
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Ferias Programadas");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Ferias Programadas -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
	
	public static Map<String, String> prepararInformacoesSid(Long codReg, Long codEmp){
		Map<String, String> params = new HashMap<String, String>();
		GregorianCalendar eDatIni = new GregorianCalendar();
		GregorianCalendar eDatRef = new GregorianCalendar();
		
		params.put("EAbrBasReg", codReg.toString());
		params.put("EDatIni", NeoUtils.safeDateFormat(eDatIni,"MM/yyyy"));
		params.put("EDatRef", NeoUtils.safeDateFormat(eDatRef,"MM/yyyy"));
		params.put("EAbrTcl", "1");
		params.put("ETipPrg", "T");
		params.put("ESomVen", "S");
		params.put("EAvosIni", "12");
		params.put("EAvosFim", "900");
		params.put("ESitCol", "1,2,15,14");
		if(codEmp == 1 || codEmp == 15 || codEmp == 17 || codEmp == 19 || codEmp == 21){
			params.put("EAbrEmp", "1,15,17,19,21");//Empresas de Vigilancia
		}else if(codEmp == 2 || codEmp == 6 || codEmp == 7 || codEmp == 8 || codEmp == 22){
			params.put("EAbrEmp", "2,6,7,8,22");   //Empresas de Asseio
		}
		
		return params;
		
	}
}
