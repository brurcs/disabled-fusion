package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCNDesativaContaSigma implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(FCNDesativaContaSigma.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		PreparedStatement st = null;
		String nomeFonteDados = "SIGMA90";
		
		StringBuffer sql= new StringBuffer();
		sql.append("INSERT INTO TAREFA_CENTRAL (CD_CLIENTE, DT_EXECUCAO_TAREFA, TP_TAREFA, FG_STATUS) VALUES(?, ?, 2, 0)");
		
			Collection<NeoObject> contasSigma = (Collection<NeoObject>) processEntity.findValue("listaContasSigma");
			
			if(contasSigma != null && !contasSigma.isEmpty())
			{
				String nomeProcesso = activity.getProcessName();
				GregorianCalendar dataFinalMonitoramento = null;

				if(nomeProcesso.equalsIgnoreCase(OrsegupsUtils.CANCELAMENTO_CONTRATO_INADIMPLENCIA))
				{
					dataFinalMonitoramento = OrsegupsUtils.getSpecificWorkDay(activity.getProcess().getStartDate(), 22L);
				}
				else
				{
					dataFinalMonitoramento = (GregorianCalendar) processEntity.findValue("dataFinalMonitoramento");
				}

				if(dataFinalMonitoramento != null)
				{
					Timestamp timestamp = new Timestamp(dataFinalMonitoramento.getTimeInMillis());
				
					for(NeoObject contaSigma : contasSigma)
					{
						Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
						
						EntityWrapper contaSigmaWrapper = new EntityWrapper(contaSigma);
						Long cdCliente = (Long) contaSigmaWrapper.findValue("clienteSigma.cd_cliente");
						
						try
						{
							connection.setAutoCommit(false);
 
							st = connection.prepareStatement(sql.toString());
							st.setLong(1, cdCliente);
							st.setTimestamp(2, timestamp);
							st.executeUpdate();

							connection.commit();
							connection.setAutoCommit(true);
						}
						catch (Exception e)
						{
							e.printStackTrace();
							try
							{
								connection.rollback();
							}
							catch (SQLException e1)
							{
								e1.printStackTrace();
								throw new WorkflowException("Erro ao tentar realizar agendamento de desativação de conta.");
							}
							throw new WorkflowException("Erro ao tentar realizar agendamento de desativação de conta.");
						}
						finally
						{
							if (st != null)
							{
								try
								{
									st.close();
									connection.close();
								}
								catch (SQLException e)
								{
									log.error("Erro ao fechar o statement");
									e.printStackTrace();
									throw new WorkflowException("Erro ao tentar realizar agendamento de desativação de conta.");
								}
							}
						}
						
					}
				}
				else
				{
					throw new WorkflowException("O campo 'Data final recebimento Almoxarifado' não está preenchido.");
				}
			}
			else
			{
				throw new WorkflowException("Não foi encontrada nenhuma conta do SIGMA para desativação.");
			}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
