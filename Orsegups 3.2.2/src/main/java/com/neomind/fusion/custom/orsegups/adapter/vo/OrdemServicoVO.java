package com.neomind.fusion.custom.orsegups.adapter.vo;

import com.neomind.fusion.custom.orsegups.sapiens.vo.ContratoClienteVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.DBOrdemVO;

public class OrdemServicoVO {
	
	DBOrdemVO os;
	ContratoClienteVO contrato;
	RepresentanteVO representante;
	
	public DBOrdemVO getOs() {
		return os;
	}
	public void setOs(DBOrdemVO os) {
		this.os = os;
	}
	public ContratoClienteVO getContrato() {
		return contrato;
	}
	public void setContrato(ContratoClienteVO contrato) {
		this.contrato = contrato;
	}
	public RepresentanteVO getRepresentante() {
		return representante;
	}
	public void setRepresentante(RepresentanteVO representante) {
		this.representante = representante;
	}
}
