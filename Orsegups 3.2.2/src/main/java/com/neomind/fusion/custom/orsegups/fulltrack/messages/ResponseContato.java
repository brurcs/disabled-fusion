package com.neomind.fusion.custom.orsegups.fulltrack.messages;

import java.util.List;

import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackContato;

public class ResponseContato {

    
    private List<FulltrackContato> data;

    public List<FulltrackContato> getData() {
        return data;
    }

    public void setData(List<FulltrackContato> data) {
        this.data = data;
    }
    
    
    
}
