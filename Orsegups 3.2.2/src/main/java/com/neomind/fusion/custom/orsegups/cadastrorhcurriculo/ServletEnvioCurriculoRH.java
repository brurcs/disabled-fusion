package com.neomind.fusion.custom.orsegups.cadastrorhcurriculo;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.LoginControl;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;

@WebServlet(name="ServletEnvioCurriculoRH", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.cadastrorhcurriculo.ServletEnvioCurriculoRH"})
public class ServletEnvioCurriculoRH extends HttpServlet
{
	private static final Log log = LogFactory.getLog(ServletEnvioCurriculoRH.class);
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		request.setAttribute("user", "convidadoCurriculo");
		request.setAttribute("pass", "guestCV0rs3gup5");

		// dados do formulário

		String areaInteresse = NeoUtils.safeOutputString((String) request.getParameter("inAreaInteresse"));
		String subAreaInteresse = NeoUtils.safeOutputString((String) request.getParameter("inSubAreaInteresse"));
		String nomeCompleto = NeoUtils.safeOutputString((String) request.getParameter("inNomeCompleto"));
		String foto = (String) request.getParameter("inFoto");
		String cpf = NeoUtils.safeOutputString((String)request.getParameter("inCpf"));
		String rg = NeoUtils.safeOutputString((String) request.getParameter("inRg"));
		String sexo = NeoUtils.safeOutputString((String) request.getParameter("inSexo"));
		String dataNasc = NeoUtils.safeOutputString((String) request.getParameter("inDataNascimento"));
		String email = NeoUtils.safeOutputString((String)request.getParameter("inEmail"));
		String endereco = NeoUtils.safeOutputString((String)request.getParameter("inEndereco"));
		String numero = NeoUtils.safeOutputString((String) request.getParameter("inNumero"));
		String complemento = NeoUtils.safeOutputString((String) request.getParameter("inComplemento"));
		String cep = NeoUtils.safeOutputString((String) request.getParameter("inCep"));
		String cidade = NeoUtils.safeOutputString((String) request.getParameter("id_cidade__"));
		String bairro = NeoUtils.safeOutputString((String) request.getParameter("id_bairro__"));
		String telefoneFixo = NeoUtils.safeOutputString((String) request.getParameter("inTelefoneFixo"));
		String telefoneCelular = NeoUtils.safeOutputString((String) request.getParameter("inTelefoneCelular"));
		String telefoneRecados = NeoUtils.safeOutputString((String) request.getParameter("inTelefoneRecados"));
		String possuiCNH = NeoUtils.safeOutputString((String) request.getParameter("inPossuiCNH"));
		String possuiMoto = NeoUtils.safeOutputString((String) request.getParameter("inPossuiMoto"));
		String estadoCivil = NeoUtils.safeOutputString((String) request.getParameter("inEstadoCivil"));
		String filhos = NeoUtils.safeOutputString((String) request.getParameter("inNumeroFilhos"));
		String idadeFilhos = NeoUtils.safeOutputString((String) request.getParameter("inIdadeFilhos"));
		String altura = NeoUtils.safeOutputString((String) request.getParameter("inAltura"));
		String peso = NeoUtils.safeOutputString((String) request.getParameter("inPeso"));
		String turno = NeoUtils.safeOutputString((String) request.getParameter("inTurno"));
		String grauInstrucao = NeoUtils.safeOutputString((String) request.getParameter("inGrauInstrucao"));
		String estuda = NeoUtils.safeOutputString((String) request.getParameter("inEstudaAtualmente"));
		String curso = NeoUtils.safeOutputString((String) request.getParameter("inCurso"));
		String cursoVigilante = NeoUtils.safeOutputString((String) request.getParameter("inCursoVigilante"));
		String dataCurso = NeoUtils.safeOutputString((String) request.getParameter("inDataCursoVigilante"));
		String escolaFormacao = NeoUtils.safeOutputString((String) request.getParameter("inEscolaFormacao"));
		String ultimaReciclagem = NeoUtils.safeOutputString((String) request.getParameter("inUltimaReciclagem"));
		String escolaReciclagem = NeoUtils.safeOutputString((String) request.getParameter("inEscolaReciclagem"));

		String ultimaEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inUltimaEmpresa"));
		String telefoneUltima = NeoUtils.safeOutputString((String) request.getParameter("inTelefoneUltimaEmpresa"));
		String dataAdmissaoUltima = NeoUtils.safeOutputString((String) request.getParameter("inDataAdmissaoUltimaEmpresa"));
		String dataSaidaUltimaEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inDataSaidaUltimaEmpresa"));
		String cargoUltimaEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inCargoUltimaEmpresa"));
		String ultimoSalario = NeoUtils.safeOutputString(NeoUtils.safeOutputString((String) request.getParameter("inUltimoSalario")));
		String motivoSaidaUltima = NeoUtils.safeOutputString((String) request.getParameter("inMotivoSaidaUltima"));
		String atividadesDesenvolvidasUltima = NeoUtils.safeOutputString((String) request.getParameter("inAtividadesDesenvolvidasUltima"));

		String penultimaEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inPenultimaEmpresa"));
		String telefonePenultimaEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inTelefonePenultimaEmpresa"));
		String dataAdmissaoPenultimaEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inDataAdmissaoPenultimaEmpresa"));
		String dataSaidaPenultimaEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inDataSaidaPenultimaEmpresa"));
		String cargoPenultimaEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inCargoPenultimaEmpresa"));
		String penultimoSalario = NeoUtils.safeOutputString(NeoUtils.safeOutputString((String) request.getParameter("inPenultimoSalario")));
		String motivoSaidaPenultima = NeoUtils.safeOutputString((String) request.getParameter("inMotivoSaidaPenultima"));
		String atividadesDesenvolvidasPenultima = NeoUtils.safeOutputString((String) request.getParameter("inAtividadesDesenvolvidasPenultima"));

		String comoConheceuEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inComoConheceuEmpresa"));
		String trabalhouEmpresaGrupo = NeoUtils.safeOutputString((String) request.getParameter("inTrabalhouEmpresaGrupo"));
		String qualEmpresa = NeoUtils.safeOutputString((String) request.getParameter("inQualEmpresa"));

		Boolean lc = Boolean.valueOf(LoginControl.getInstance().login(request, response));
		String url = PortalUtil.getBaseURL();
		try
		{
			String model = "R021 - BCV - Cadastro de Currículo (Portal)";
			String entity = "CVprocessoBancoDeCurriculos";

			ProcessModel processModel = null;

			QLEqualsFilter filter = new QLEqualsFilter("Name", model);
			processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, filter);

			InstantiableEntityInfo entityInfo = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByType(entity);
			NeoObject processEntity = entityInfo.createNewInstance();

			EntityWrapper wrpProcess = new EntityWrapper(processEntity);

			InstantiableEntityInfo cvTemp = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CVbancoDeCurriculosTemp");
			NeoObject temp = cvTemp.createNewInstance();

			EntityWrapper wrpEformTemp = new EntityWrapper(temp);

			// popula os dados dentro do eform de curriculo
			InstantiableEntityInfo infoAreaInteresse = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CVAreaInteresse");
			NeoObject cvai = (NeoObject) PersistEngine.getObject(infoAreaInteresse.getEntityClass(), new QLEqualsFilter("descricao", areaInteresse));

			wrpEformTemp.findField("areaInteresse").setValue(cvai);

			/*
			if (areaInteresse != "Administrativo")
			{
				subAreaInteresse = null;
			}
			*/
			if (subAreaInteresse != null)
			{
				InstantiableEntityInfo infoSubAreaInteresse = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo(
						"CVSubAreaInteresse");
				NeoObject sai = (NeoObject) PersistEngine.getObject(infoSubAreaInteresse.getEntityClass(), new QLEqualsFilter("neoId", new Long(
						subAreaInteresse)));
				wrpEformTemp.findField("subAreaInteresse").setValue(sai);

			}

			wrpEformTemp.findField("nomeCompleto").setValue(nomeCompleto);

			if (foto != null)
			{
				NeoFile fileFusion = (NeoFile) PersistEngine.getObject(NeoFile.class, new QLEqualsFilter("neoId", new Long(foto)));

				wrpEformTemp.findField("foto").setValue(fileFusion);
			}
			if (cpf != null)
			{
				wrpEformTemp.findField("numeroCPF").setValue(new Long(cpf));
			}
			wrpEformTemp.findField("rg").setValue(rg);

			wrpEformTemp.findField("idade").setValue(idadeFilhos);

			if (cidade != null)
			{
				InstantiableEntityInfo infoCidade = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("VETORH_R074CID");
				NeoObject objCidade = (NeoObject) PersistEngine.getObject(infoCidade.getEntityClass(), new QLEqualsFilter("neoId", new Long(cidade)));

				wrpEformTemp.findField("cidade").setValue(objCidade);
			}
			if (bairro != null)
			{
				InstantiableEntityInfo infoBairro = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("VETORHRBAI");
				NeoObject objBairro = (NeoObject) PersistEngine.getObject(infoBairro.getEntityClass(), new QLEqualsFilter("neoId", new Long(bairro)));
				wrpEformTemp.findField("bairro").setValue(objBairro);
			}
			if (estadoCivil != null)
			{
				InstantiableEntityInfo infoEstadoCivil = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("UTILestadoCivil");
				NeoObject objEstadoCivil = (NeoObject) PersistEngine.getObject(infoEstadoCivil.getEntityClass(), new QLEqualsFilter("neoId",
						new Long(estadoCivil)));
				wrpEformTemp.findField("estadoCivil").setValue(objEstadoCivil);
			}
			if (grauInstrucao != null)
			{
				InstantiableEntityInfo infoGrauInstrucao = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("VETORHRGRA");
				NeoObject objGrauInstrucao = (NeoObject) PersistEngine.getObject(infoGrauInstrucao.getEntityClass(), new QLEqualsFilter("neoId",
						new Long(grauInstrucao)));
				wrpEformTemp.findField("grauInstrucao").setValue(objGrauInstrucao);
			}
			if (sexo != null)
			{
				InstantiableEntityInfo infoSexo = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("GCSexo");
				NeoObject objSexo = (NeoObject) PersistEngine.getObject(infoSexo.getEntityClass(), new QLEqualsFilter("neoId", new Long(sexo)));
				wrpEformTemp.findField("sexo").setValue(objSexo);
			}
			wrpEformTemp.findField("email").setValue(email);
			wrpEformTemp.findField("idade").setValue(idadeFilhos);
			wrpEformTemp.findField("endereco").setValue(endereco);
			wrpEformTemp.findField("numero").setValue(numero);
			wrpEformTemp.findField("complemento").setValue(complemento);
			if (cep != null)
			{
				wrpEformTemp.findField("cep").setValue(new Long(cep));
			}
			wrpEformTemp.findField("telefone1").setValue(telefoneFixo);
			wrpEformTemp.findField("telefone2").setValue(telefoneCelular);
			wrpEformTemp.findField("telefone3").setValue(telefoneRecados);
			
			wrpEformTemp.findField("cnh").setValue(possuiCNH);
			wrpEformTemp.findField("motocicleta").setValue(new Boolean(possuiMoto));
			if (filhos != null)
			{
				wrpEformTemp.findField("numeroFilhos").setValue(new Long(filhos));

				wrpEformTemp.findField("idadeFilhos").setValue(idadeFilhos);
			}
			if (altura != null)
			{
				altura = altura.replace(",", ".");
				wrpEformTemp.findField("altura").setValue(new BigDecimal(altura));

			}

			if (peso != null)
			{
				peso = peso.replace(",", ".");
				wrpEformTemp.findField("peso").setValue(new BigDecimal(peso));
			}
			wrpEformTemp.findField("trabalhouGrupo").setValue(new Boolean(trabalhouEmpresaGrupo));
			wrpEformTemp.findField("empresaTrabalhouGrupo").setValue(qualEmpresa);

			if (comoConheceuEmpresa != null)
			{
				InstantiableEntityInfo infoComoConheceu = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("UTILorigem");
				NeoObject tipos = (NeoObject) PersistEngine.getObject(infoComoConheceu.getEntityClass(), new QLEqualsFilter("neoId", new Long(
						comoConheceuEmpresa)));

				wrpEformTemp.findField("conheceuEmpresa").setValue(tipos);
			}
			if (turno != null)
			{
				InstantiableEntityInfo infoTurnoDesejado = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CVturnoDesejado");
				NeoObject turnodes = (NeoObject) PersistEngine.getObject(infoTurnoDesejado.getEntityClass(), new QLEqualsFilter("neoId", new Long(
						turno)));

				wrpEformTemp.findField("turnoDesejado").setValue(turnodes);
			}
			wrpEformTemp.findField("estudaAtualmente").setValue(new Boolean(estuda));

			wrpEformTemp.findField("curso").setValue(curso);

			wrpEformTemp.findField("cursoVigilante").setValue(new Boolean(cursoVigilante));

			wrpEformTemp.findField("escolaVigilante").setValue(escolaFormacao);

			wrpEformTemp.findField("escolaReciclagem").setValue(escolaReciclagem);

			InstantiableEntityInfo infoEmpresa = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CVempregosAnteriores");

			/* Ultima empresa */
			NeoObject objUltimaEmpresa = infoEmpresa.createNewInstance();

			EntityWrapper wrpUltimaEmpresa = new EntityWrapper(objUltimaEmpresa);

			wrpUltimaEmpresa.findField("empresa").setValue(ultimaEmpresa);

			wrpUltimaEmpresa.findField("telefone").setValue(telefoneUltima);

			wrpUltimaEmpresa.findField("cargoOcupado").setValue(cargoUltimaEmpresa);
			log.warn("valor do salario =   " + ultimoSalario);
			if (ultimoSalario != "")
			{
				ultimoSalario = ultimoSalario.replace(".", "");
				ultimoSalario = ultimoSalario.replace(",", ".");
				wrpUltimaEmpresa.findField("ultimoSalario").setValue(new BigDecimal(ultimoSalario));
			}
			wrpUltimaEmpresa.findField("motivoSaida").setValue(motivoSaidaUltima);
			wrpUltimaEmpresa.findField("atividadesDesenvolvidas").setValue(atividadesDesenvolvidasUltima);

			/* Penultima empresa */
			NeoObject objPenultimaEmpresa = infoEmpresa.createNewInstance();

			EntityWrapper wrpPenultimaEmpresa = new EntityWrapper(objPenultimaEmpresa);

			wrpPenultimaEmpresa.findField("empresa").setValue(penultimaEmpresa);
			wrpPenultimaEmpresa.findField("telefone").setValue(telefonePenultimaEmpresa);

			wrpPenultimaEmpresa.findField("cargoOcupado").setValue(cargoPenultimaEmpresa);

			if (penultimoSalario != "")
			{
				penultimoSalario = penultimoSalario.replace(".", "");
				penultimoSalario = penultimoSalario.replace(",", ".");
				wrpPenultimaEmpresa.findField("ultimoSalario").setValue(new BigDecimal(penultimoSalario));
			}
			wrpPenultimaEmpresa.findField("motivoSaida").setValue(motivoSaidaPenultima);
			wrpPenultimaEmpresa.findField("atividadesDesenvolvidas").setValue(atividadesDesenvolvidasPenultima);

			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yy");
			GregorianCalendar data = null;
			Date d = null;
			try
			{
				if (!dataNasc.equalsIgnoreCase(""))
				{
					d = formato.parse(dataNasc);
					data = new GregorianCalendar();
					data.setTime(d);
					wrpEformTemp.findField("dataNascimento").setValue(data);
				}
				if (!ultimaReciclagem.equalsIgnoreCase(""))
				{
					d = formato.parse(ultimaReciclagem);
					data = new GregorianCalendar();
					data.setTime(d);
					wrpEformTemp.findField("dataReciclagem").setValue(data);
				}
				if (!dataCurso.equalsIgnoreCase(""))
				{
					d = formato.parse(dataCurso);
					data = new GregorianCalendar();
					data.setTime(d);
					wrpEformTemp.findField("dataCursoVigilante").setValue(data);
				}
				if (!dataAdmissaoUltima.equalsIgnoreCase(""))
				{
					d = formato.parse(dataAdmissaoUltima);
					data = new GregorianCalendar();
					data.setTime(d);
					wrpUltimaEmpresa.findField("dataAdmissao").setValue(data);
				}
				if (!dataSaidaUltimaEmpresa.equalsIgnoreCase(""))
				{
					d = formato.parse(dataSaidaUltimaEmpresa);
					data = new GregorianCalendar();
					data.setTime(d);
					wrpUltimaEmpresa.findField("dataSaida").setValue(data);
				}
				if (!dataAdmissaoPenultimaEmpresa.equalsIgnoreCase(""))
				{
					d = formato.parse(dataAdmissaoPenultimaEmpresa);
					data = new GregorianCalendar();
					data.setTime(d);
					wrpPenultimaEmpresa.findField("dataAdmissao").setValue(data);
				}
				if (!dataSaidaPenultimaEmpresa.equalsIgnoreCase(""))
				{
					d = formato.parse(dataSaidaPenultimaEmpresa);
					data = new GregorianCalendar();
					data.setTime(d);
					wrpPenultimaEmpresa.findField("dataSaida").setValue(data);
				}
			}
			catch (ParseException e)
			{
				log.warn("Não foi possivel a conversão de data");
				e.printStackTrace();
			}
			wrpEformTemp.findField("ultimoEmprego").setValue(objUltimaEmpresa);
			wrpEformTemp.findField("penultimoEmprego").setValue(objPenultimaEmpresa);

			wrpProcess.findField("cvTemp").setValue(temp);

			/**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
			 * @date 12/03/2015
			 */
			
			NeoUser currentUser = PortalUtil.getCurrentUser();
			
			WFProcess proc = WorkflowService.startProcess(processModel, processEntity, false, currentUser);

			try
			{
				new OrsegupsWorkflowHelper(proc).avancaPrimeiraAtividade(currentUser);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			/**
			 * FIM ALTERAÇÕES - NEOMIND
			 */

			if (!lc.booleanValue())
			{
				url = PortalUtil.getBaseURL() + "portal_orsegups/curriculoEnviado.jsp?mensagem=erro";
			}
			else
			{
				url = PortalUtil.getBaseURL() + "portal_orsegups/curriculoEnviado.jsp?mensagem=ok";
			}
			response.sendRedirect(url);
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
		}

	}

	public void process(HttpServletRequest request, HttpServletResponse response, String[] params) throws IOException
	{
	}
}