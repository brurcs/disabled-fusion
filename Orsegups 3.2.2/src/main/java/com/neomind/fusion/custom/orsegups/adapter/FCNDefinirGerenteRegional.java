package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCNDefinirGerenteRegional implements AdapterInterface
{
	/*
	 * (non-Javadoc)
	 * Autor Francisco Junior
	 * @see com.neomind.fusion.workflow.adapter.AdapterInterface#start(com.neomind.fusion.workflow.Task, com.neomind.fusion.entity.EntityWrapper, com.neomind.fusion.workflow.Activity)
	 */
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			String nomePapel = "";
			Long codreg = (Long) processEntity.findValue("contratoSapiens.usu_codreg");//contratoSapiens.usu_codreg			
			Long serctr = (Long) processEntity.findValue("contratoSapiens.usu_serctr");
			
			NeoPaper noGerente = (NeoPaper) OrsegupsUtils.getPapelGerenteRegional(codreg, serctr);
			if (noGerente != null && (noGerente.getAllUsers() == null || noGerente.getAllUsers().isEmpty()))
			{
				throw new WorkflowException("Gerente Regional Não Encontrado!");
			}
			
			processEntity.findField("managerRegion").setValue(noGerente);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Problema ao Selecionar um Gerente Regional, Classe FCNDefinirGerenteRegional");
			throw new WorkflowException("Problema ao Selecionar um Gerente Regional");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
