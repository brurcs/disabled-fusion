package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import com.neomind.fusion.custom.orsegups.utils.DeslocamentoExcessivoVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;


public class RotinaDeslocamentoExcessivoXVID implements CustomJobAdapter {

	private static String mensagemErro = "Erro ao executar a Rotina Deslocamento Excessivo XVID.";
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		LinkedList <DeslocamentoExcessivoVO> listaDeslocamento = new LinkedList<>();
	
		try {
			
			listaDeslocamento = retornaDeslocamentosXVID();
			
			for(DeslocamentoExcessivoVO deslocamento : listaDeslocamento) {
				abrirTarefaDeslocamentoExcessivo(deslocamento);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException(mensagemErro);
		}
		
	}

	private void abrirTarefaDeslocamentoExcessivo(DeslocamentoExcessivoVO deslocamento) {
		
		try {
			
			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			
			NeoPaper papelSolicitante = OrsegupsUtils.getPaper("SolicitanteDeslocamentoExcessivoXVID");
			NeoPaper papelExecutor = OrsegupsUtils.getPaper("ExecutorDeslocamentoExcessivoXVID");
			
			NeoUser solicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelSolicitante);
			NeoUser executor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);
			
			String titulo = "Tratar Deslocamentos em Excesso XVID - " + deslocamento.getIdCentral() + "[" + deslocamento.getParticao() + "] "+deslocamento.getRazaoSocial();
			String tituloAux = "Tratar Deslocamentos em Excesso XVID - " + deslocamento.getIdCentral() + "[" + deslocamento.getParticao() + "]% ";

			String descricao = null;
			
			GregorianCalendar prazo = new GregorianCalendar();
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 7L);
			prazo.set(Calendar.HOUR_OF_DAY, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);
			
			descricao = montaDescricaoTarefa(tituloAux, deslocamento);
			
			iniciarTarefaSimples.abrirTarefa(solicitante.getCode(), executor.getCode(), titulo, descricao, "1", "sim", prazo);
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao abrir a tarefa de deslocamento excessivo XVID";
			throw new WorkflowException(mensagemErro);
		}
	}

	private String montaDescricaoTarefa(String tituloAux, DeslocamentoExcessivoVO deslocamento) {
		
		String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
		descricao += " <strong>Conta :</strong> " + deslocamento.getIdCentral() + "[" + deslocamento.getParticao() + "] " + " <br>";
		descricao = descricao + " <strong>Razão Social: </strong> " + deslocamento.getRazaoSocial() + " <br>";
		descricao = descricao + " <strong>Nome Fantasia: </strong> " + deslocamento.getNomeFantasia() + " <br>";
		descricao = descricao + " <strong>Período: </strong> " + retornaDataInicio() + "<strong>  à </strong>" + retornaDataFinal() + "<br>";
		descricao = descricao + " <strong>Deslocamentos: </strong> " + deslocamento.getQuantidadeDeslocamento() + "<br>";
		
		return descricao;
	}

	private String retornaDataFinal() {
		
		String dataInicio = null;
		
		try {
			
			GregorianCalendar dataBase = new GregorianCalendar();
			dataBase.set(GregorianCalendar.HOUR_OF_DAY, 23);
			dataBase.set(GregorianCalendar.MINUTE, 59);
			dataBase.set(GregorianCalendar.SECOND, 59);
			
			dataInicio = NeoDateUtils.safeDateFormat(dataBase, "yyyy-MM-dd HH:mm:ss");
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao retornar a data de inicio";
			throw new WorkflowException(mensagemErro);
		}
		
		return dataInicio;
	}

	private String retornaDataInicio() {

		String dataFim = null;
		
		try {
			
			GregorianCalendar dataBase = new GregorianCalendar();
			dataBase.add(GregorianCalendar.DAY_OF_MONTH, -15);
			dataBase.set(GregorianCalendar.HOUR_OF_DAY, 00);
			dataBase.set(GregorianCalendar.MINUTE, 00);
			dataBase.set(GregorianCalendar.SECOND, 00);
			
			dataFim = NeoDateUtils.safeDateFormat(dataBase, "yyyy-MM-dd HH:mm:ss");
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao retornar a data de fim";
			throw new WorkflowException(mensagemErro);
		}
		
		return dataFim;
	}

	private LinkedList<DeslocamentoExcessivoVO> retornaDeslocamentosXVID() {
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		LinkedList<DeslocamentoExcessivoVO> listaDeslocamento = new LinkedList<>();
		
		try {
			
			 sql.append("  SELECT XR.CD_ROTA INTO #ROTAS_PAGAS_TEMP   ");                                                                                                              
			 sql.append(" FROM  [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.X_SIGMAROTA XR   ");       
			 sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPAplicacaoTatico TATICO ON XR.neoId = TATICO.rotaSigma_neoId ");
			 sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPTaticoModalidadePgto MODAL ON MODAL.neoId = TATICO.modalidade_neoId   "); 
			 sql.append(" WHERE MODAL.CODIGO = 3 ");                                                                                                                           
			 sql.append(" 	                            ");
			 sql.append(" SELECT  TOP(25) h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3) AS sigla_regional, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, COUNT(*) AS QtdDsl,");      
			 sql.append(" GETDATE()-15 as PrimeiraData,GETDATE() as UltimaData ");                                                                                             
			 sql.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)             ");                                                                              
			 sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE  ");                                                         
			 sql.append(" INNER JOIN ROTA r WITH (NOLOCK) ON  r.CD_ROTA = c.ID_ROTA       ");                                                              			
			 sql.append(" WHERE h.DT_VIATURA_NO_LOCAL BETWEEN ? AND ? ");                                                                                       
			 sql.append(" AND h.CD_CODE <> 'RNP'               ");                                                                                                                    
			 sql.append(" AND h.CD_CODE <> 'RNA'               ");                                                                                                                    
			 sql.append(" AND c.ID_CENTRAL NOT LIKE 'AAA%'     ");                                                                                                                    
			 sql.append(" AND r.NM_ROTA NOT LIKE '%UNIVALI%'   ");                                                                                                               
			 sql.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ('IAI','BQE','BNU','JLE','LGS','CUA','GPR','SOO','CCO','RSL','CTA','TRO','NHO','TRI','CAS','GNA','PMJ','SRR','XLN')   "); 			
			 sql.append(" AND H.CD_EVENTO = 'XVID' ");
			 sql.append(" AND NOT EXISTS (SELECT * FROM #ROTAS_PAGAS_TEMP T WHERE T.CD_ROTA = R.CD_ROTA)		");
			 sql.append(" GROUP BY h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3), R.NM_ROTA, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO       ");                                       
			 sql.append(" HAVING COUNT(*) > 1  ");                                                                                                                                    
			 sql.append(" ORDER BY 7 DESC      ");                                                                                                                                     
			 sql.append(" 	                   ");
			 sql.append(" DROP TABLE #ROTAS_PAGAS_TEMP  ");
			 
			 conn = OrsegupsUtils.getConnection("SIGMA90");
			 
			 pstm = conn.prepareStatement(sql.toString());
			 pstm.setString(1, retornaDataInicio());
			 pstm.setString(2, retornaDataFinal());
			 
			 rs = pstm.executeQuery();
			 
			 while(rs.next()) {
				 
				 DeslocamentoExcessivoVO deslocamento = new DeslocamentoExcessivoVO();
				 
				 deslocamento.setCdCliente(rs.getLong("CD_CLIENTE"));
				 deslocamento.setIdCentral(rs.getString("ID_CENTRAL"));
				 deslocamento.setParticao(rs.getString("PARTICAO"));
				 deslocamento.setRazaoSocial(rs.getString("RAZAO"));
				 deslocamento.setNomeFantasia(rs.getString("FANTASIA"));
				 deslocamento.setSiglaRegional(rs.getString("SIGLA_REGIONAL"));
				 deslocamento.setQuantidadeDeslocamento(rs.getLong("QTDDSL"));
				 
				 listaDeslocamento.add(deslocamento);

			 }
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao retornar os deslocamentos excessivos";
			throw new WorkflowException(mensagemErro);
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaDeslocamento;
	}
	
}
