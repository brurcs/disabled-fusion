/**
 * Sapiens_Synccom_senior_g5_co_ger_cad_clientes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

public interface Sapiens_Synccom_senior_g5_co_ger_cad_clientes extends java.rmi.Remote {
    public com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesOut gravarClientes(java.lang.String user, java.lang.String password, int encryption, com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesIn parameters) throws java.rmi.RemoteException;
}
