package com.neomind.fusion.custom.orsegups.adapter;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ValidaMudancaExecutorAprovarTarefa implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		List<NeoObject> historicos = new ArrayList<NeoObject>();
		historicos = (List<NeoObject>) processEntity.findValue("registroAtividades");
		GregorianCalendar prazo = null;
		GregorianCalendar dataAtual = null;

		if (NeoUtils.safeIsNotNull(historicos) && historicos.size() > 0)
		{
			NeoObject hist = historicos.get(historicos.size() - 1);
			NeoUser executorAtual = (NeoUser) processEntity.findValue("Executor");

			EntityWrapper historicoWrapper = new EntityWrapper(hist);
			NeoUser executorHistorico = (NeoUser) historicoWrapper.findValue("responsavel");

			if (NeoUtils.safeIsNotNull(executorAtual) && NeoUtils.safeIsNotNull(executorAtual) && !executorAtual.getCode().equals(executorHistorico.getCode()))
			{
				dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
				prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();

				if (!OrsegupsUtils.isWorkDay(prazo))
				{
					throw new WorkflowException("Prazo informado deve ter um dia útil!");
				}
				else if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
				{
					throw new WorkflowException("Quando alterado o executor, o prazo informado deve ser de pelo menos 1 dia!");
				}
			}
			else if (NeoUtils.safeIsNotNull(executorAtual) && executorAtual.getCode().equals(executorHistorico.getCode()))
			{

				prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();
				GregorianCalendar prazoAux = new GregorianCalendar(prazo.get(1), prazo.get(2), prazo.get(5), 00, 00, 00);
				
				GregorianCalendar prazoHisAux = (GregorianCalendar) historicoWrapper.findValue("prazo");
				GregorianCalendar prazoAux2 = new GregorianCalendar(prazoHisAux.get(1), prazoHisAux.get(2), prazoHisAux.get(5), 00, 00, 00); 
				/*
				 * Alterado segundo solicitação do Sr. Giliardi via tarefa simples 742165. Agora quando o usuário for mantido, poderá ser dada qualquer data após a data atual.
				 * */
				
				GregorianCalendar dtAtual = new GregorianCalendar();
				GregorianCalendar dtAtualAux = new GregorianCalendar(dtAtual.get(1), dtAtual.get(2), dtAtual.get(5), 00, 00, 00);

				if (!OrsegupsUtils.isWorkDay(prazo))
				{
					throw new WorkflowException("Prazo informado deve ter um dia útil!");
				}
				
				if(!prazoAux.equals(prazoAux2)){
					if (prazoAux.before(dtAtualAux) )
					{
						throw new WorkflowException("Quando o executor for mantido, o prazo deve ser maior ou igual a data atual!");
					}
				}
			}
		}
	}
}