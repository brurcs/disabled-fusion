package com.neomind.fusion.custom.orsegups.presenca;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

public class PresencaUtil
{
	public static final String SENDGRID_USERNAME = "orsegups";
	public static final String SENDGRID_PASSWORD = "Ors1234!";
	
	public static String getDayOfWeek(Date data)
	{ 
		String[] diaSemana = new String[]{"Dom","Seg", 
		"Ter","Qua","Qui","Sex","Sáb"};
		
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		
		return diaSemana[dayOfWeek - 1]; 
	}
	
	public static String formatDate(Date date, String mask)
	{
		SimpleDateFormat format = new SimpleDateFormat(mask);
		String txt = format.format(date);
		return txt;
	}
	
	public static void sendEmail2SendGrid(String addTo, String from, String subject, String text, String html, String nameFile, File file) 
	{
		SendGrid sendgrid = new SendGrid(SENDGRID_USERNAME, SENDGRID_PASSWORD);
		try {

		    SendGrid.Email email = new SendGrid.Email();
		    email.addTo(addTo);
		    email.setFrom(from);
		    email.setSubject(subject);
		    email.setText(text);
		    email.setHtml(html);
		    if (nameFile != null && !nameFile.equals("")){
		    	email.addAttachment(nameFile, file);
		    }

			SendGrid.Response response = sendgrid.send(email);
			System.out.println(response.getMessage());
	    }
	    catch (SendGridException e) {
	    	e.printStackTrace();
	    }
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}