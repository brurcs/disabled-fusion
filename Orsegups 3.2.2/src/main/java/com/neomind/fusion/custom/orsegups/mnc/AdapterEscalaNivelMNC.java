package com.neomind.fusion.custom.orsegups.mnc;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class AdapterEscalaNivelMNC implements AdapterInterface
{
    private static final Log log = LogFactory.getLog(AdapterEscalaNivelMNC.class);
    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity){
	Long key = (new GregorianCalendar()).getTimeInMillis();
	try{
	    origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
	    NeoPaper responsavelExecutor = null;
	    NeoUser responsavelReg = null;
	    SecurityEntity executor = (SecurityEntity) processEntity.findValue("solicitante");
	    //CASO SEJA UM NEO USER
	    if(executor instanceof NeoUser){
		String usuario = executor.getCode();
		NeoUser objExecutor = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code",usuario));
		NeoPaper papelGroup = objExecutor.getGroup().getResponsible(); 
		
		if (objExecutor.getPapers().contains(papelGroup)){
		    responsavelExecutor = objExecutor.getGroup().getUpperLevel().getResponsible();
		}else{
		    responsavelExecutor = objExecutor.getGroup().getResponsible();
		}
		if (responsavelExecutor != null){
		    for (NeoUser usr : responsavelExecutor.getUsers()){
			responsavelReg = usr;
			break;
		    }
		}
	    }  
	    //CASO SEJA UM PAPEL
	    if(executor instanceof NeoPaper){
		String papel = executor.getCode();
		NeoPaper objExecutor = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",papel));
		Set<NeoUser> setUsers = objExecutor.getUsers();
		 NeoUser aux = null;
		for (Iterator<NeoUser> it = setUsers.iterator(); it.hasNext(); ) {
		       aux = it.next();
		    }
		NeoPaper papelGroup = aux.getGroup().getResponsible(); 
		
		if (aux.getPapers().contains(papelGroup)){
		    responsavelExecutor = aux.getGroup().getUpperLevel().getResponsible();
		}else{
		    responsavelExecutor = aux.getGroup().getResponsible();
		}
		if (responsavelExecutor != null){
		    for (NeoUser usr : responsavelExecutor.getUsers()){
			responsavelReg = usr;
			break;
		    }
		}
	    }
	    processEntity.findField("responsavelEscalada").setValue(responsavelReg);
	}catch (Exception e){
	    log.error("["+key+"] Erro ao executar a classe AdapterEscalaNivelRCL "+e.getMessage());
	    e.printStackTrace();
	    throw new WorkflowException(e.getMessage());
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity){
	
    }

}
