package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityAdapter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class FAPSlipCentroCustoConverter implements EntityAdapter
{	
	@Override
	public void run(Map<String, Object> map)
	{
		Map<String, Object> dados = (Map<String, Object>) map.get("reqMap");
		
		Long formNeoId = dados.get("id_") != null ? new Long((String) dados.get("id_")) : 0l;
		
		NeoObject noForm = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipCentroCusto"), formNeoId);
		if (noForm == null)
		{
			noForm = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlip"), formNeoId);
			if (noForm == null)
				return;
		}
		EntityWrapper wForm = new EntityWrapper(noForm);
		
		importarRateiosExcel(wForm);
		
		/*Long codTipoRateio = wForm.findGenericValue("centrosCusto.tipoRateio.codigo");
		BigDecimal totalRateio = wForm.findGenericValue("centrosCusto.total");
		BigDecimal valorPagamento = wForm.findGenericValue("valorPagamento");
		
		if (codTipoRateio == 1l && totalRateio.compareTo(new BigDecimal(100)) != 0)
			throw new WorkflowException("A porcentagem total de rateios deve chegar à 100.");
		else if (codTipoRateio == 2l && totalRateio.compareTo(valorPagamento) != 0)
			throw new WorkflowException("O valor total do rateio deve ser igual ao valor do pagamento.");*/
	}
	
	private void importarRateiosExcel(EntityWrapper wrapper)
	{
		NeoFile anexo = wrapper.findGenericValue("centrosCusto.anexoCentroCusto");
		if (anexo == null)
			return;
		
		String mensagem = "";
		/* Obrigatoriamente o arquivo excel para ser importado tem que ser salvo no formato .XLS */
		Workbook wbRateios = null;
		
		try
		{
			Long codEmpresa = wrapper.findGenericValue("empresa.codemp");

			List<NeoObject> listCentroCusto = new ArrayList<NeoObject>();
			List<String> listCentroCustoInvalidos = new ArrayList<String>();			

			wbRateios = Workbook.getWorkbook(anexo.getAsFile());
			Sheet rateios = wbRateios.getSheet(0);

			int liTroca = rateios.getRows();
			for (int i = 0; i < liTroca; i++)
			{
				if (i != 0)
				{
					Cell centroCusto = rateios.getCell(0, i);
					Cell valorRateio = rateios.getCell(1, i);
					Cell obsRateio = rateios.getCell(2, i);
					
					if (centroCusto.getContents().isEmpty())
						continue;

					QLGroupFilter filterCcu = new QLGroupFilter("AND");
					filterCcu.addFilter(new QLEqualsFilter("codccu", centroCusto != null ? centroCusto.getContents().trim() : ""));
					filterCcu.addFilter(new QLEqualsFilter("codemp", codEmpresa));
					filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));
					filterCcu.addFilter(new QLEqualsFilter("acerat", "S"));

					NeoObject externoCentroCusto = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

					if (externoCentroCusto != null)
					{
						String valorRateioStr = valorRateio.getContents();
						BigDecimal valorRateio1 = new BigDecimal(valorRateioStr.replace(",", "."));
						
						NeoObject noRateio = AdapterUtils.createNewEntityInstance("FAPSlipRateio");
						EntityWrapper wRateio = new EntityWrapper(noRateio);

						wRateio.setValue("codigoCentroCusto", centroCusto.getContents());
						wRateio.setValue("centroCusto", externoCentroCusto);
						wRateio.setValue("valor", valorRateio1);
						if (obsRateio != null)
							wRateio.setValue("observacoes", obsRateio.getContents()); 

						/* valorTotalRateio = valorTotalRateio.add(valorRateio); */
						listCentroCusto.add(noRateio);
					}
					else
					{
						listCentroCustoInvalidos.add(centroCusto.getContents());
					}
				}
			}
			if (listCentroCustoInvalidos.isEmpty())
			{
				NeoObject noCentroCusto = wrapper.findGenericValue("centrosCusto");
				if (noCentroCusto == null)
					noCentroCusto = AdapterUtils.createNewEntityInstance("FAPSlipCentroCusto");
				EntityWrapper wCentroCusto = new EntityWrapper(noCentroCusto);
				
				NeoObject noTipoRateio = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipTipoRateio"), new QLEqualsFilter("codigo", 2l));
				
				wCentroCusto.setValue("tipoRateio", noTipoRateio);				
				wCentroCusto.setValue("anexoCentroCusto", null);
				for (NeoObject noCCU : listCentroCusto)
				{
					wCentroCusto.findField("rateio").addValue(noCCU);
				}
				
				wrapper.setValue("centrosCusto", noCentroCusto);
			}
			else
			{
				mensagem = "Centro de Custo(s) não localizado(s) ou não aceitam rateio! Por favor, verifique se o(s) Centro de Custo " + listCentroCustoInvalidos + " existem na Empresa de lançamento do Título ou não aceitam rateio!";
				mensagem = mensagem.replaceAll("\\[|\\]|", "");
				throw new WorkflowException(mensagem);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
		finally {
			wbRateios.close();
		}
	}
}
