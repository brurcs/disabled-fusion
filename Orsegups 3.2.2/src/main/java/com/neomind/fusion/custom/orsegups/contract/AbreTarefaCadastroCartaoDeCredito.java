package com.neomind.fusion.custom.orsegups.contract;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.jfree.util.Log;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

/**
 * Adapter de geração da tarefa simples de cadastro de cartão de crédito
 * 
 * @param Forma de Pagamento = Cartão de Crédito (codfpg = 10) & Cadastrar cartão agora = false [Fluxo de Contratos - C001]  
 * @author herisson.ferreira
 *
 */
public class AbreTarefaCadastroCartaoDeCredito implements AdapterInterface {
	
	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) {
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		Log.warn("[INICIO] ADAPTER ABRE TAREFA CARTÃO DE CRÉDITO ["+key+"]");
		System.out.println("[INICIO] ADAPTER ABRE TAREFA CARTÃO DE CRÉDITO ["+key+"]");
		
		// Usuários
		
		String solicitante = null;
		String executor = null;
		String papelSolicitante = null;
		String papelExecutor = null;

		papelSolicitante = "solicitanteCadastraCartaoDeCredito";
		papelExecutor = "executorCadastraCartaoDeCredito";
		
		solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
		executor = OrsegupsUtils.getUserNeoPaper(papelExecutor);
		
		// Corpo da Tarefa
		
		Long codigoCliente = (Long) processEntity.findValue("buscaNomeCliente.codcli");
		Long contratoCliente = (Long) processEntity.findValue("dadosGeraisContrato.numeroContratoSapiens");
		Long empresaCliente = (Long) processEntity.findValue("empresa.codemp");
		Long filialCliente = (Long) processEntity.findValue("empresa.codfil");
		Long cgccpfCliente = (Long) processEntity.findValue("buscaNomeCliente.cgccpf");
		String razaoSocial = (String) processEntity.findValue("buscaNomeCliente.nomcli");
		String contatoCliente = (String) processEntity.findValue("buscaNomeCliente.foncli");
		
		String titulo = null;
		StringBuilder descricao = new StringBuilder();
		
		titulo = "Cadastrar Cartão de Crédito - "+ razaoSocial+" - "+codigoCliente;
		descricao.append("Realizar o cadastro do cartão de crédito do cliente:");
		descricao.append("<table><tr>");
		descricao.append("<td> Razão Social: "+ razaoSocial +"</td></tr>");
		descricao.append("<tr><td> Código do Cliente: "+ codigoCliente +"</td></tr>");
		descricao.append("<tr><td> Contrato: " +contratoCliente +"</td></tr>");
		descricao.append("<tr><td> Empresa: "+ empresaCliente);
		descricao.append(" - Filial: "+ filialCliente +"</td>");
		descricao.append("</tr>");
		descricao.append("<tr>");
		descricao.append("<td> CNPJ/CPF: "+cgccpfCliente+"</td>");
		descricao.append("</tr>");
		
		if(contatoCliente != null && !contatoCliente.isEmpty()){
			descricao.append("<tr>");
			descricao.append("<td> Contato: "+contatoCliente+"</td>");
			descricao.append("</tr>");
		}
		
		descricao.append("</table>");

		// Prazo
		
		GregorianCalendar prazo = OrsegupsUtils.getNextWorkDay(new GregorianCalendar());
		
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 10L); // 
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
	
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		try {
			iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao.toString(), "1", "sim", prazo);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("[ERRO] Geração da Tarefa para Cadastro de Cartão de Crédito - AbreTarefaCadastroCartaoDeCredito.java:"+e);
			throw new WorkflowException("[ERRO] Geração da Tarefa para Cadastro de Cartão de Crédito - AbreTarefaCadastroCartaoDeCredito.java:"+e);
		} finally {
			Log.warn("[FIM] ADAPTER ABRE TAREFA CARTÃO DE CRÉDITO ["+key+"]");
			System.out.println("[FIM] ADAPTER ABRE TAREFA CARTÃO DE CRÉDITO ["+key+"]");
		}
		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity) {

		
	}

}
