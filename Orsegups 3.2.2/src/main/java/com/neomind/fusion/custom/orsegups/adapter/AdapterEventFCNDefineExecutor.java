package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

/**
 * Adapter responsável por definir os executores de acordo com a atividade, para garantir o escalonamento correto da mesma.
 * 
 * @author herisson.ferreira
 *
 */
public class AdapterEventFCNDefineExecutor implements ActivityStartEventListener {
	
	private static final Log log = LogFactory.getLog(AdapterEventFCNDefineExecutor.class);
	
	@Override
	public void onStart(ActivityEvent event) throws ActivityException {
		
		System.out.println(" ###### INICIO DO ADAPTER DE EVENTOS AdapterEventFCNDefineExecutor "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		
		String atividade = event.getActivity().getActivityName().toString();
		
		InstantiableEntityInfo formularioTarefa = AdapterUtils.getInstantiableEntityInfo("FCNcancelamentoEletInadimplenciaNew");
		NeoObject objetoTarefa = formularioTarefa.createNewInstance();
		EntityWrapper wEvento = new EntityWrapper(event.getProcess().getEntity());
		
		String papelSupervisor = null;
		String papelGerente = null;
		String papelDiretor = null;
		
		NeoPaper supervisor = null;
		NeoPaper gerente = null;
		NeoPaper diretor = null;
		
		try {
								 
			if(atividade.equals("..INCLUIR CONTA SIGMA - INADIMPLÊNCIA - Tarefa Escalada") ||
			   atividade.equals("..Cancelar Contrato Sapiens - Inadimplência - Tarefa Escalada")) {
				
				papelGerente = "FCNGerenteCPO";
				papelDiretor = "FCNDiretorCPO";
				
				gerente = OrsegupsUtils.getPaper(papelGerente);
				diretor = OrsegupsUtils.getPaper(papelDiretor);
				
				wEvento.findField("gerenteFCN").setValue(gerente);
				wEvento.findField("diretorFCN").setValue(diretor);
				
			}else {
				
				papelSupervisor = "FCNSupervisorCEREC";
				papelGerente = "FCNGerenteCEREC";
				papelDiretor = "FCNDiretorCEREC";
				
				supervisor = OrsegupsUtils.getPaper(papelSupervisor);
				gerente = OrsegupsUtils.getPaper(papelGerente);
				diretor = OrsegupsUtils.getPaper(papelDiretor);
				
				wEvento.findField("supervisorFCN").setValue(supervisor);
				wEvento.findField("gerenteFCN").setValue(gerente);
				wEvento.findField("diretorFCN").setValue(diretor);
				
			}

			PersistEngine.persist(objetoTarefa);
			
		} catch (WorkflowException e) {
			throw new WorkflowException("#### [AdapterEventFCNDefineExecutor] ERRO AO DEFINIR OS RESPONSÁVEIS PELO CEREC");
		} finally {
			System.out.println(" ###### FIM DO ADAPTER DE EVENTOS AdapterEventFCNDefineExecutor "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		
	}
}
