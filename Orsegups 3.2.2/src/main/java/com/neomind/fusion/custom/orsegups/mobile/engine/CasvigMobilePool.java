package com.neomind.fusion.custom.orsegups.mobile.engine;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.annotations.FieldShowOrder;
import com.neomind.fusion.entity.annotations.NeoField;
import com.neomind.fusion.entity.annotations.NotAField;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;

/**
 * Entidade que faz o controle das reinspe��es que devem ser enviadas ao mobile.
 * 
 * @author Daniel Henrique Joppi
 * 
 */
@Entity
public class CasvigMobilePool extends NeoObject
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigMobilePool.class);

	private Boolean sendToMobile;

	private Boolean isReinspection;

	private Boolean isEfficacy;

	private Task task;

	private Activity activity;

	private List<NeoUser> users;

	/**
	 * Private empty constructor to be used by the persistence engine
	 * 
	 */
	public CasvigMobilePool()
	{
		this(null, null, null, null, null, null);
	}

	public CasvigMobilePool(Activity activity, Task task)
	{
		this(activity, task, null, null, null, true);
	}

	public CasvigMobilePool(Activity activity, Task task, List<NeoUser> users, Boolean isReinspection, Boolean isEfficacy, Boolean sendToMobile)
	{
		this.sendToMobile = sendToMobile;
		this.isReinspection = isReinspection;
		this.isEfficacy = isEfficacy;
		this.task = task;
		this.activity = activity;
		this.users = users;
	}

	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 60)
	public Boolean getSendToMobile()
	{
		return sendToMobile != null && sendToMobile;
	}

	public void setSendToMobile(Boolean sendToMobile)
	{
		this.sendToMobile = sendToMobile;
	}

	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 40)
	public Boolean getIsReinspection()
	{
		return isReinspection != null && isReinspection;
	}

	/**
	 * Se for uma Reinspe��oa, isReinspection deve receber valor true e
	 * automaticamente isEfficacy recebe false.
	 * 
	 * @author Daniel Henrique Joppi 10/12/2008
	 * @param isEfficacy
	 * true se � uma Reinspe��o.
	 */
	public void setIsReinspection(Boolean isReinspection)
	{
		this.isReinspection = isReinspection;

		// se isReinspection � true ... isEfficacy � false
		if (this.isEfficacy == null)
			this.setIsEfficacy(!isReinspection);
	}

	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 50)
	public Boolean getIsEfficacy()
	{
		return isEfficacy != null && isEfficacy;
	}

	/**
	 * Se for uma Verifica��o de Efic�cia, isEfficacy deve receber valor true e
	 * automaticamente isReinspection recebe false.
	 * 
	 * @author Daniel Henrique Joppi 10/12/2008
	 * @param isEfficacy
	 * true se � uma Verifica��o de Efic�cia.
	 */
	public void setIsEfficacy(Boolean isEfficacy)
	{
		this.isEfficacy = isEfficacy;

		// se isEfficacy � true ... isReinspection � false
		if (this.isReinspection == null)
			this.setIsReinspection(!isEfficacy);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 20)
	public Task getTask()
	{
		return task;
	}

	public void setTask(Task task)
	{
		this.task = task;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 10)
	public Activity getActivity()
	{
		return activity;
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 30)
	public List<NeoUser> getUsers()
	{
		return users;
	}

	public void setUsers(List<NeoUser> users)
	{
		this.users = users;
	}

	/**
	 * Retorna o tipo da inste��o.
	 * 
	 * @author Daniel Henrique Joppi 10/12/2008
	 * @return se isReinspection � true returna "Reinspe��o". Se isEfficacy �
	 * true retorna "Efic�cia".
	 */
	@Transient
	@NotAField
	public String getTypeInspection()
	{
		if (this.isReinspection)
			return "Reinspeção";
		else
			return "Eficácia";
	}
}
