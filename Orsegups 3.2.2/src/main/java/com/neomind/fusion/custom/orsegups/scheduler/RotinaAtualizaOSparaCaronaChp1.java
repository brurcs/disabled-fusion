package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaAtualizaOSparaCaronaChp1 implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaAtualizaOSparaCaronaChp1.class);

    @Override
    public void execute(CustomJobContext arg0) {
	log.warn("##### INICIAR ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	Connection connSIGMA = null;
	
	PreparedStatement pstm = null;
	PreparedStatement pstmSIGMA = null;
	
	ResultSet rs = null;
	ResultSet rsSIGMA = null;
	
	List<String> listaCmd = null;
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	
	try {
	    conn = PersistEngine.getConnection("");
	    
	    StringBuffer  varname1 = new StringBuffer();
	    
	    varname1.append("select ordem.ID_ORDEM, ");
	    varname1.append("       replace(convert(varchar(8000),ordem.DEFEITO), '''', '''''') as DEFEITO ");
	    varname1.append("  from ( ");
	    varname1.append("      select cd_cliente ");
	    varname1.append("        from [FSOODB03\\SQL01].SIGMA90.DBO.dbCentral ");
	    varname1.append("       inner join ( ");
	    varname1.append("            select * ");
	    varname1.append("              from ( ");
	    varname1.append("                  select ID_EMPRESA, ");
	    varname1.append("                           ID_CENTRAL ");
	    varname1.append("                    from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("                   where ce.ctrl_central = 1 ");
	    varname1.append("                     and ce.id_empresa not in (10175) "); // TODO - Inserido esta linha para na Encaronar para clientes TELETOC a pedidos do Fabio
	    varname1.append("                     and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("                     and ce.id_central not like 'DDD%' ");
	    varname1.append("                           and ce.id_central not like 'FFF%' ");
	    varname1.append("                           and ce.id_central not like 'AA%' ");
	    varname1.append("                           and ce.id_central not like 'R%' ");
	    varname1.append("                            ");
	    varname1.append("                                          union ");
	    varname1.append("                  select ID_EMPRESA, ");
	    varname1.append("                           ID_CENTRAL ");
	    varname1.append("                    from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("                   where ctrl_central = 1 ");
	    varname1.append("                     and id_empresa not in (10175) "); // TODO - Inserido esta linha para na Encaronar para clientes TELETOC a pedidos do Fabio
	    varname1.append("                     and celular = 1 "); 
	    varname1.append("                     and ncelular = 'GPRS' ");
	    varname1.append("                     and id_central not like 'DDD%' ");
	    varname1.append("                     and id_central not like 'FFF%' ");
	    varname1.append("                     and id_central not like 'AA%' ");
	    varname1.append("                     and id_central not like 'R%' ");
	    varname1.append("                    ) ");
	    varname1.append("                  as clientesSIGMA ");
	    varname1.append("                  where not exists ( ");
	    varname1.append("                     select 1 ");
	    varname1.append("                        from [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente dc ");
	    varname1.append("                        where ((dc.visitaValidada = 1 and dc.dataRemocao is null) or (dc.visitaValidada is null and dc.dataRemocao is null)) ");
	    varname1.append("                          and dc.empresaConta = clientesSIGMA.ID_EMPRESA ");
	    varname1.append("                          and dc.contaSIGMA = clientesSIGMA.ID_CENTRAL collate Latin1_General_CS_AS ");
	    varname1.append("                  ) ");
	    varname1.append("            ) as clientesSIGMAsemValidacao ");
	    varname1.append("                          on clientesSIGMAsemValidacao.ID_EMPRESA = dbCentral.ID_EMPRESA ");
	    varname1.append("                        and clientesSIGMAsemValidacao.ID_CENTRAL = dbCentral.ID_CENTRAL ");
	    varname1.append("                         where dbCENTRAL.PARTICAO not like ('09%') ");
	    varname1.append("      ) as clientesAEncaronar ");
	    varname1.append("                         ");
	    varname1.append("      inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbOrdem ordem ");
	    varname1.append("            on clientesAEncaronar.cd_cliente = ordem.cd_cliente ");
	    varname1.append("    inner join [FSOODB03\\SQL01].SIGMA90.dbo.COLABORADOR colaborador ");
	    varname1.append("            on ordem.id_instalador = colaborador.CD_COLABORADOR ");
	    varname1.append("                                    ");
	    varname1.append("     where ordem.fechado = 0 ");
	    varname1.append("       and colaborador.NM_COLABORADOR not like '%CFTV%' ");
	    varname1.append("       and ordem.DEFEITO not like '%#FOTOCHIP#%' "); 
	    varname1.append("       and ordem.ABERTURA >= DateAdd(month,-1,GETDATE ())"); 

	    pstm = conn.prepareStatement(varname1.toString());
	    rs = pstm.executeQuery();

	    listaCmd = new ArrayList<String>();
	    while (rs.next()) {
		String idOrdem = rs.getString("ID_ORDEM");
		String defeito = rs.getString("DEFEITO");
		
		listaCmd.add(" UPDATE dbo.dbOrdem SET DEFEITO = '"+defeito+". ATENÇÃO: Registrar a foto do Chip GPRS da central. Havendo a necessidade de troca (Valide no link: http://chip.legal), retirar fotos dos 2 Chips(Novo e Antigo), marcar um X no Chip antigo para diferenciar.#FOTOCHIP#' WHERE ID_ORDEM = " + idOrdem);
	    }
	    
	    connSIGMA = PersistEngine.getConnection("SIGMA90");

	    for (String cmd : listaCmd) {

		pstmSIGMA = connSIGMA.prepareStatement(cmd.toString());
		pstmSIGMA.executeUpdate();
	    }
	    
	    log.warn("##### EXECUTAR ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    try {
		conn.rollback();
		connSIGMA.rollback();
	    } catch (SQLException e1) {
		e1.printStackTrace();
	    }
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
		OrsegupsUtils.closeConnection(connSIGMA, pstmSIGMA, rsSIGMA);
	    } catch (Exception e) {
		e.printStackTrace();
		log.error("##### ERRO ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    }
	    log.warn("##### FINALIZAR ROTINA DE ATUALIZACAO DE OS PARA CARONA CHP1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
    }
}
