package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaValidacaoOSChp1_3 implements CustomJobAdapter {
    
    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(AbreTarefaOperacaoEvento.class);

    @Override
    public void execute(CustomJobContext arg0)
    {	
	Connection conn = PersistEngine.getConnection("");
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaValidacaoOSChp1");
	log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try
	{
	    StringBuffer  varname1 = new StringBuffer();	    
	    varname1.append("          select * 														");
	    varname1.append("            from [FSOODB03\\SQL01].SIGMA90.dbo.dbOrdem a 										");
	    varname1.append("      inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral c on a.cd_cliente = c.cd_cliente 						");
	    varname1.append(" left outer join [FSOODB03\\SQL01].SIGMA90.DBO.dbcidade cid on cid.id_cidade = c.id_cidade 					");
	    varname1.append(" left outer join [FSOODB03\\SQL01].SIGMA90.DBO.dbbairro bai on bai.id_cidade = c.id_cidade and bai.id_bairro = c.id_bairro 	");
	    varname1.append("           where a.fechado = 1 													");
	    varname1.append("             and a.defeito like '%#FOTOCHIP#%' 											");
	    varname1.append("             and not exists (select 1 												");
	    varname1.append("                               from D_Chp1Cliente b  										");
	    varname1.append("                              where a.id_ordem = b.idOrdem)									");
	    pstm = conn.prepareStatement(varname1.toString());
	    rs = pstm.executeQuery();

	    while (rs.next())
	    {
		Long cdHistorico = 0l; 
		Long empresaConta = rs.getLong("ID_EMPRESA");
		Long cdCliente = rs.getLong("CD_CLIENTE");
		String cdEvento = "0";
		String idCentral = rs.getString("ID_CENTRAL");
		String particao = rs.getString("PARTICAO");
		String razaoSocial = rs.getString("RAZAO");
		String endereco = rs.getString("ENDERECO");
		String viatura = "Gerado por OS";
		String logViatura = "";
		Long idOrdem = rs.getLong("ID_ORDEM");

		String userSolicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteChp1Cliente"); 
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userSolicitante));
		NeoPaper papel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteChp1Cliente"));
		NeoObject wkfOCP = AdapterUtils.createNewEntityInstance("Chp1Cliente");
		final EntityWrapper ewWkfOCP= new EntityWrapper(wkfOCP);
		
		ewWkfOCP.findField("cdHistorico").setValue(cdHistorico); 
		ewWkfOCP.findField("contaSIGMA").setValue(idCentral);
		ewWkfOCP.findField("empresaConta").setValue(empresaConta);
		ewWkfOCP.findField("particao").setValue(particao);
		ewWkfOCP.findField("razaoSocial").setValue(razaoSocial);
		ewWkfOCP.findField("endereco").setValue(endereco);
		ewWkfOCP.findField("viatura").setValue(viatura);
		ewWkfOCP.findField("solicitante").setValue(solicitante);
		ewWkfOCP.findField("pool").setValue(papel);
		ewWkfOCP.findField("cdCliente").setValue(cdCliente);
		ewWkfOCP.findField("idOrdem").setValue(idOrdem);
		
		int index = logViatura.indexOf("#");
		String resultadoVisita = "";
		if (index > 0) {
		    resultadoVisita = logViatura.substring(0, index);					
		} else {
		    resultadoVisita = logViatura.toString();					
		}
		Boolean visitaOK = Boolean.TRUE;
		if (resultadoVisita.contains("PENDENTE")) {
		    ewWkfOCP.findField("eventoAGerar").setValue(cdEvento.substring(0,cdEvento.length()-1)+"3");
		} else {
		    ewWkfOCP.findField("eventoAGerar").setValue(cdEvento.substring(0,cdEvento.length()-1)+"2");
		}
		ewWkfOCP.findField("visitaOK").setValue(visitaOK);		
		PersistEngine.persist(wkfOCP);
		ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "O040-OperacaoChp1Cliente"));		
		OrsegupsWorkflowHelper.iniciaProcesso(pm, wkfOCP, true, solicitante, true, null);
	    }
	    
	} catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally {
	    try {
		rs.close();
		pstm.close();
		conn.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }
	    
	    log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Vaidacao OS Chp1 - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    System.out.println("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1 - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }
}
