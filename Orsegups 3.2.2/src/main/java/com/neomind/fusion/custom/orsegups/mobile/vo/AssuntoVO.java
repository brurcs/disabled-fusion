package com.neomind.fusion.custom.orsegups.mobile.vo;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;




//@XmlRootElement(name = "Assuntos")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "http://neomind.com/")

public class AssuntoVO {

	 @XmlElement(name = "cod_assun", required = true)
	private String cod_assun;
	 
	 @XmlElement(name = "desc_assun", required = true)
	private String desc_assun;
	
	 @XmlElementWrapper(name = "perguntas")
	 @XmlElement(name = "Pergunta")
	    private Collection<PerguntaVO>pergunta;
	
	public Collection<PerguntaVO> getPergunta() {
		return pergunta;
	}
	public void setPergunta(Collection<PerguntaVO> pergunta) {
		this.pergunta = pergunta;
	}
	public String getCod_assun() {
		return cod_assun;
	}
	public void setCod_assun(String cod_assun) {
		this.cod_assun = cod_assun;
	}
	public String getDesc_assun() {
		return desc_assun;
	}
	public void setDesc_assun(String desc_assun) {
		this.desc_assun = desc_assun;
	}

	 @Override
	    public String toString() {
	        return "Assuntos{" +
	                "des_pesq=" + desc_assun +
	                ", cod_assun='" + cod_assun + '\'' +
	                ", pergunta='" + pergunta + '\'' +
	                '}';
	    }
	
}
