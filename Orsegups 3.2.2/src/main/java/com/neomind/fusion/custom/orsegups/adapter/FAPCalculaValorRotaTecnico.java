package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTaticoDAO;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTecnicoDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.TaticoDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.TecnicoDTO;
import com.neomind.fusion.custom.orsegups.fap.exception.NaoEncontradoException;
import com.neomind.fusion.custom.orsegups.fap.factory.FapFactory;
import com.neomind.fusion.custom.orsegups.fap.utils.FapUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPCalculaValorRotaTecnico implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) throws WorkflowException {
		Long tipoAplicacao = null;
		List<NeoObject> historicoContas = null;
		tipoAplicacao = (Long) processEntity.findField(
				"aplicacaoPagamento.codigo").getValue();
		NeoObject formaPagamento = null;

		try {
			// Objeto do tipo de item de orçamento - Padrão para as ambas as
			// aplicações (Técnico / Tático);
			NeoObject tipoItem = FapFactory.neoObjectFactory(
					"FAPTipoItemDeOrcamento", "descricao", "SERVIÇO");

			// Verifica se é Tecnico ou Rota
			if (tipoAplicacao == 10) {

				Long codTecnico = null;
				BigDecimal valorDoPagamento = new BigDecimal(0.0);
				List<NeoObject> listaPagamentos = new ArrayList<NeoObject>();
				EntityWrapper wTecnico = new EntityWrapper(
						(NeoObject) processEntity
								.findGenericValue("tecnicoSigma"));

				NeoObject fornecedor = (NeoObject) wTecnico
						.findGenericValue("fornecedor");
				formaPagamento = (NeoObject) wTecnico
						.findGenericValue("formaPagamento");

				// Objeto DTO aplicação técnico
				codTecnico = (Long) processEntity.findField(
						"tecnicoSigma.tecnico.cd_colaborador").getValue();
				TecnicoDTO tecDTO = AplicacaoTecnicoDAO
						.consultaTotalPagamentos(codTecnico);

				// TODO Preencher o fornecedor no e-form principal
				processEntity.setValue("fornecedor", fornecedor); // Fornecedor

				Long codModalidade = wTecnico.findGenericValue("modalidadePag.codigo");

				if (tecDTO.getQtdContaCFTV() > 0) {

					BigDecimal valorCFTV = new BigDecimal(0.0);
					Long qtdCFTV = new Long(tecDTO.getQtdContaCFTV());

					valorCFTV = (BigDecimal) processEntity
							.findGenericValue("tecnicoSigma.valorCFTV");
					valorDoPagamento = AplicacaoTaticoDAO.valorPagamento(codModalidade, valorCFTV, qtdCFTV);

					// Item de orçamento recebe tipo de item de orçamento
					NeoObject wkfFAPItemOrcamentoCFTV = AdapterUtils
							.getInstantiableEntityInfo("FAPItemDeOrcamento")
							.createNewInstance();
					EntityWrapper ewWkfFAPItemOrcamentoCFTV = new EntityWrapper(
							wkfFAPItemOrcamentoCFTV);
					ewWkfFAPItemOrcamentoCFTV.setValue("tipoItem", tipoItem);
					ewWkfFAPItemOrcamentoCFTV.setValue(
							"descricao",
							"Pagamento referente a carteira do técnico "
									+ tecDTO.getNomeTecnico()
									+ " pelo atendimento de "
									+ tecDTO.getQtdContaCFTV()
									+ " contas de CFTV");
					ewWkfFAPItemOrcamentoCFTV.setValue("valor",
							valorDoPagamento);
					PersistEngine.persist(wkfFAPItemOrcamentoCFTV);

					// Cada item é adicionado na lista de pagamentos
					listaPagamentos.add(wkfFAPItemOrcamentoCFTV);

				}
				if (tecDTO.getQtdContaAlarme() > 0) {

					BigDecimal valorAlarme = new BigDecimal(0.0);
					Long qtdAlarme = new Long(tecDTO.getQtdContaAlarme());

					valorAlarme = (BigDecimal) processEntity.findField(
							"tecnicoSigma.valorAlarme").getValue();
					valorDoPagamento = AplicacaoTaticoDAO.valorPagamento(codModalidade, valorAlarme, qtdAlarme);

					// Item de orçamento recebe tipo de item de orçamento
					NeoObject wkfFAPItemOrcamentoAlarme = AdapterUtils
							.getInstantiableEntityInfo("FAPItemDeOrcamento")
							.createNewInstance();
					EntityWrapper ewWkfFAPItemOrcamentoAlarme = new EntityWrapper(
							wkfFAPItemOrcamentoAlarme);
					ewWkfFAPItemOrcamentoAlarme.setValue("tipoItem", tipoItem);
					ewWkfFAPItemOrcamentoAlarme.setValue(
							"descricao",
							"Pagamento referente a carteira do técnico "
									+ tecDTO.getNomeTecnico()
									+ " pelo atendimento de "
									+ tecDTO.getQtdContaAlarme()
									+ " contas de ALARME");
					ewWkfFAPItemOrcamentoAlarme.setValue("valor",
							valorDoPagamento);
					PersistEngine.persist(wkfFAPItemOrcamentoAlarme);

					// Cada item é adicionado na lista de pagamentos
					listaPagamentos.add(wkfFAPItemOrcamentoAlarme);
				}

				// Preenche os itens de orçamento na FAP mediante os pagamentos
				// da lista de pagamentos
				processEntity.findField("itemOrcamento").setValue(
						listaPagamentos);

				// Retorna as contas do técnico em específico
				tecDTO.setHistoricoContasSigma(AplicacaoTecnicoDAO
						.retornaContas(codTecnico));

				// Instancia um e-form com as contas sigma do técnico
				historicoContas = tecDTO.getHistoricoContasSigma();
				
				NeoObject noHistoricoContas = AdapterUtils.createNewEntityInstance("FAPHistoricoContasAgrupadas");
				EntityWrapper wHistoricoContas = new EntityWrapper(noHistoricoContas);
				wHistoricoContas.setValue("historicoContasAgrupadas", historicoContas);
				PersistEngine.persist(noHistoricoContas);

				// Insere o e-form do histórico de contas que foram pagas para
				// esta FAP
				if (historicoContas != null) {
					processEntity.findField("historicoContasAgrupadas").addValue(
							noHistoricoContas);
					processEntity.findField("historicoContas").setValue(
							historicoContas);
				}

				processEntity.setValue("formaPagamento", formaPagamento);

				String nomeTecnico = wTecnico
						.findGenericValue("tecnico.nm_colaborador");
				String codRegional = FapUtils.getSiglaRegional(nomeTecnico);
				
				NeoObject noAprovadorRegional = FapUtils.getAprovadorTecnico(codRegional);
								
				if (noAprovadorRegional != null) {
					NeoUser nuAprovador = new EntityWrapper(noAprovadorRegional)
							.findGenericValue("aprovador");
					processEntity.setValue("aprovador", nuAprovador);
				}
			} else {
				// Tipo de aplicação 11 - Tático (Rota)
				TaticoDTO taticoDTO = new TaticoDTO();
				BigDecimal valorDoPagamento = new BigDecimal(0.0);
				List<NeoObject> listaPagamentos = new ArrayList<NeoObject>();
				Long codRota = null;
				String nomeRota = null;
				String descricao = null;
				boolean pagaPorEvento = false;
				codRota = (Long) processEntity.findField("rotaSigma.rotaSigma.cd_rota").getValue();
				nomeRota = (String) processEntity.findField("rotaSigma.rotaSigma.nm_rota").getValue();
				taticoDTO.setRota(codRota.toString() + " - " + nomeRota);
				Long quantidadeEventos = 0L;
				Long quantidadeContas = 0L;

				/*
				 * Preparação de valores para serem utilizados no preenchimento
				 * das informações obrigatórias da FAP
				 */
				NeoObject efRota = FapFactory.neoObjectFactory("SIGMAROTA","cd_rota", codRota);
				EntityWrapper ewRota = new EntityWrapper(FapFactory.neoObjectFactory("FAPAplicacaoTatico","rotaSigma", efRota));

				NeoObject fornecedor = (NeoObject) ewRota.findField("fornecedor").getValue();

				NeoObject modalidade = (NeoObject) ewRota.findField("modalidade").getValue();
				EntityWrapper ewModalidade = new EntityWrapper(modalidade);
				Long codigoModalidade = (Long) ewModalidade.findField("codigo").getValue();

				formaPagamento = (NeoObject) ewRota.findField("formaPagamento").getValue();
				BigDecimal valorPgto = (BigDecimal) ewRota.findField("valorRota").getValue();

				String codigoTarefa = (String) processEntity.findGenericValue("wfprocess.code");

				if (codigoModalidade == 3L) {
					pagaPorEvento = true;
				}

				// TODO - verificar a data certinho
				GregorianCalendar competencia = (GregorianCalendar) processEntity
						.findGenericValue("competenciaPagamento");
				competencia.set(GregorianCalendar.DAY_OF_MONTH, 1);
				competencia.set(GregorianCalendar.HOUR, 0);
				competencia.set(GregorianCalendar.MINUTE, 0);
				competencia.set(GregorianCalendar.SECOND, 0);
				competencia.set(GregorianCalendar.MILLISECOND, 0);

				List<NeoObject> listaDeEventos = AplicacaoTaticoDAO.preencheDadosEventos(codRota, codigoTarefa,pagaPorEvento, competencia);

				if (NeoUtils.safeIsNotNull(listaDeEventos)) {
					processEntity.findField("historicoEventos").setValue(listaDeEventos);
				}

				if (codigoModalidade.longValue() == 3L) {
					quantidadeEventos = new Long(listaDeEventos.size());
					valorDoPagamento = AplicacaoTaticoDAO.valorPagamento(codigoModalidade, valorPgto, quantidadeEventos);
					descricao = "Serviços prestados na rota "
							+ taticoDTO.getRota() + ". Total de Eventos: "
							+ quantidadeEventos;
				} else {
					quantidadeEventos = new Long(listaDeEventos.size());
					quantidadeContas = AplicacaoTaticoDAO
							.calculaQuantidadeContas(codRota);
					valorDoPagamento = AplicacaoTaticoDAO.valorPagamento(codigoModalidade, valorPgto, quantidadeContas);
					descricao = "Serviços prestados na rota "
							+ taticoDTO.getRota() + ". Total de contas: "
							+ quantidadeContas + ". Total de Eventos: "
							+ quantidadeEventos;
				}

				// Item de orçamento recebe tipo de item de orçamento
				NeoObject wkfFAPItemOrcamento = AdapterUtils
						.getInstantiableEntityInfo("FAPItemDeOrcamento")
						.createNewInstance();
				EntityWrapper ewWkfFAPItemOrcamento = new EntityWrapper(
						wkfFAPItemOrcamento);
				ewWkfFAPItemOrcamento.setValue("tipoItem", tipoItem);
				ewWkfFAPItemOrcamento.setValue("descricao", descricao);

				ewWkfFAPItemOrcamento.setValue("valor", valorDoPagamento);

				// Pagamento é adicionado na lista de pagamentos
				listaPagamentos.add(wkfFAPItemOrcamento);

				// Preenche as contas do tático em específico
				String codigo = (String) processEntity.findValue("wfprocess.code");
				taticoDTO.setHistoricoContasSigma(AplicacaoTaticoDAO.retornaContas(codRota, codigo));

				// Instancia um e-form com as contas sigma do tático
				historicoContas = taticoDTO.getHistoricoContasSigma();
				
				NeoObject noHistoricoContas = AdapterUtils.createNewEntityInstance("FAPHistoricoContasAgrupadas");
				EntityWrapper wHistoricoContas = new EntityWrapper(noHistoricoContas);
				wHistoricoContas.setValue("historicoContasAgrupadas", historicoContas);

				// Insere o e-form do histórico de contas que foram pagas para
				// esta FAP
				if (historicoContas != null) {
					processEntity.findField("historicoContasAgrupadas").addValue(
							noHistoricoContas);
					processEntity.findField("historicoContas").setValue(
							historicoContas);
				}

				// Persiste as informações pertinentes no e-form principal
				processEntity.findField("fornecedor").setValue(fornecedor);
				processEntity.findField("itemOrcamento").setValue(listaPagamentos);
				processEntity.findField("formaPagamento").setValue(formaPagamento);
			}

			// Padrão para ambos os tipos de aplicação (Técnico / Tático)
			NeoObject responsavelDocumento = FapFactory.neoObjectFactory("FAPResponsavelDocumento", "descricao", "REGIONAL/DEPTO");
			processEntity.findField("responsavelDocumento").setValue(responsavelDocumento);
			processEntity.findField("anexarDocumento").setValue(false);

			NeoObject responsavelPagamento = FapFactory.neoObjectFactory("FAPResponsavelPagamento", "nome", "Empresa");
			processEntity.findField("responsavelPagamento").setValue(responsavelPagamento);

		} catch (WorkflowException | NaoEncontradoException e) {
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		} catch (Exception e) {
			throw new WorkflowException("Erro inesperado. Favor contatar a TI.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		// TODO Auto-generated method stub

	}
}
