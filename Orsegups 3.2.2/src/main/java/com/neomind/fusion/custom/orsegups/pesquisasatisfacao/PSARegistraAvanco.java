package com.neomind.fusion.custom.orsegups.pesquisasatisfacao;

import java.util.Collection;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.FieldMarshaller;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLWhere;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;


//com.neomind.fusion.custom.orsegups.avaliacaoterceirizado.PSARegistraAvanco
public class PSARegistraAvanco implements AdapterInterface  {

	@Override
	public void start(Task origin, EntityWrapper wrapper,	Activity activity) {
		
		if (wrapper.findValue("tentativa") != null){
			if (!NeoUtils.safeBoolean(wrapper.findValue("houveContato"))){ // sem contato
				String strTentativas = NeoUtils.safeOutputString(wrapper.findValue("tentativa"));
				Long tentativas = NeoUtils.safeLong(strTentativas.equals("")? "0": strTentativas);
				tentativas++;
				if (wrapper.findValue("tentativa") != null) wrapper.setValue("tentativa", tentativas);
				regitraHistorico(wrapper, "Sem contato. ");
				
			}else{ // com contato
				String strTentativas = NeoUtils.safeOutputString(wrapper.findValue("tentativa"));
				Long tentativas = NeoUtils.safeLong(strTentativas.equals("")? "0": strTentativas);
				tentativas++;
				if (wrapper.findValue("tentativa") != null) wrapper.setValue("tentativa", tentativas);
				if( wrapper.findValue("satisfeito") == null	){
					throw new WorkflowException("Preencha todas as perguntas de satisfação!");
				}else{
					regitraHistorico(wrapper, "Contato efetuado com sucesso!");
				}
			}
		}
		
	}
	
	public void regitraHistorico(EntityWrapper wrapper, String descricao){
		NeoObject oRegistroAtividade = AdapterUtils.createNewEntityInstance("PSAHistorico");
		EntityWrapper wRegistroAtividade = new EntityWrapper(oRegistroAtividade);
		
		//preenche os dados aqui
		wRegistroAtividade.setValue("descricao", descricao);
		
		if (wrapper.findValue("historicoTentativas") != null){
			Collection<NeoObject> listaRegistros = wrapper.findField("historicoTentativas").getValues();
			listaRegistros.add(oRegistroAtividade);
		}else{
			wRegistroAtividade.setValue("historicoTentativas", oRegistroAtividade);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}
