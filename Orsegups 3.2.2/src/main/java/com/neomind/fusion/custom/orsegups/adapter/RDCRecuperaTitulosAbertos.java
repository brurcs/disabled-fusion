package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class RDCRecuperaTitulosAbertos implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		String idOrdem = (String) processEntity.findValue("idordem").toString();

		String nomeFonteDados = "SAPIENS";

		Connection conSapiens = PersistEngine.getConnection("SAPIENS");
		Connection conSigma = PersistEngine.getConnection("SIGMA90");

		PreparedStatement stOS = null;
		PreparedStatement stSig = null;

		try
		{
			//Busca código do cliente do sigma via OS
			int sigCli = 0;
			StringBuffer sqlOS = new StringBuffer();
			sqlOS.append("SELECT CD_CLIENTE FROM dbo.dbORDEM WHERE ID_ORDEM = ?");
			stOS = conSigma.prepareStatement(sqlOS.toString());
			stOS.setString(1, idOrdem);
			ResultSet rsOs = stOS.executeQuery();
			if (rsOs.next())
			{
				sigCli = rsOs.getInt("CD_CLIENTE");
			}

			//Busca código do cliente do sapiens que possui ligação com o sigma
			int codcli = 0;
			StringBuffer sqlSig = new StringBuffer();
			sqlSig.append("select c.usu_codcli from usu_t160sig s ");
			sqlSig.append("inner join usu_t160ctr c on s.usu_codemp = c.usu_codemp and s.usu_codfil = c.usu_codfil and s.usu_numctr = c.usu_numctr ");
			sqlSig.append("where s.usu_codcli = ?");

			stSig = conSapiens.prepareStatement(sqlSig.toString());
			stSig.setLong(1, sigCli);
			ResultSet rsSig = stSig.executeQuery();

			if (rsSig.next())
			{
				codcli = rsSig.getInt("usu_codcli");
			}
			if (codcli > 0)
			{
				//Busca títulos em aberto do cliente
				String sql = "	SELECT E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT, E301TCR.DATEMI, E301TCR.VCTORI, E301TCR.VCTPRO, E301TCR.VLRORI, E301TCR.VLRABE, E140NFV.VLRBSE, E140IDE.NUMDFS, E140ISV.NUMCTR " + "	FROM E301TCR " + "	INNER JOIN E001TNS ON E001TNS.CODEMP = E301TCR.CODEMP AND E001TNS.CODTNS = E301TCR.CODTNS "
						+ "	LEFT JOIN E140NFV ON E140NFV.CODEMP = E301TCR.CODEMP AND E140NFV.CODFIL = E301TCR.CODFIL AND E140NFV.CODSNF = E301TCR.CODSNF AND E140NFV.NUMNFV = E301TCR.NUMNFV " + "	LEFT JOIN E140ISV ON E140ISV.CODEMP = E140NFV.CODEMP AND E140ISV.CODFIL = E140NFV.CODFIL AND E140ISV.CODSNF = E140NFV.CODSNF AND E140ISV.NUMNFV = E140NFV.NUMNFV "
						+ "	LEFT JOIN E140IDE ON E140IDE.CODEMP = E140NFV.CODEMP AND E140IDE.CODFIL = E140NFV.CODFIL AND E140IDE.CODSNF = E140NFV.CODSNF AND E140IDE.NUMNFV = E140NFV.NUMNFV " + "	WHERE E001TNS.LISMOD = 'CRE' " + "	AND E301TCR.VLRABE > 0 AND E301TCR.VCTPRO <= (GETDATE() - 4) AND E301TCR.CODTPT <> 'IFA' " + "	AND ((E301TCR.SITTIT >= 'AA' AND E301TCR.SITTIT <= 'AV') OR (E301TCR.SITTIT = 'CE')) " + "	AND E301TCR.CodCli =   " + codcli
						+ "	GROUP BY E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT, E301TCR.DATEMI, E301TCR.VCTORI, E301TCR.VCTPRO, E301TCR.VLRORI, E301TCR.VLRABE, E140NFV.VLRBSE, E140IDE.NUMDFS, E140ISV.NUMCTR  " + "	ORDER BY E301TCR.DATEMI DESC ";

				Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql);
				@SuppressWarnings("unchecked")
				Collection<Object> resultList = query.getResultList();

				// Lista de Títulos
				processEntity.findField("listaTitulos").removeValues();

				// Auxiliar para contar quantos titus de vencimentos originais diferentes
				GregorianCalendar gcVectoOriginalAnterior = new GregorianCalendar();
				Long qtdTitVctoDiferentes = 0L;

				if (resultList != null)
				{
					for (Object resultSet : resultList)
					{
						if (resultSet != null)
						{
							Object[] result = (Object[]) resultSet;
							QLGroupFilter filterTituloSapiens = new QLGroupFilter("AND");
							filterTituloSapiens.addFilter(new QLEqualsFilter("codemp", (Short) result[0]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("codfil", (Short) result[1]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("codtpt", (String) result[2]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("numtit", (String) result[3]));
							NeoObject noTitulo = AdapterUtils.createNewEntityInstance("GERTituloSapiens");
							EntityWrapper tituloFusionWrapper = new EntityWrapper(noTitulo);
							tituloFusionWrapper.setValue("empresa", new Long((Short) result[0]));
							tituloFusionWrapper.setValue("filial", new Long((Short) result[1]));
							tituloFusionWrapper.setValue("tipoTitulo", (String) result[2]);
							tituloFusionWrapper.setValue("numeroTitulo", (String) result[3]);
							Timestamp dataEmissao = (Timestamp) result[4];
							GregorianCalendar gc = new GregorianCalendar();
							gc.setTimeInMillis(dataEmissao.getTime());
							tituloFusionWrapper.setValue("emissao", gc);
							Timestamp dataOriginal = (Timestamp) result[5];
							gc = new GregorianCalendar();
							gc.setTimeInMillis(dataOriginal.getTime());
							if (!gcVectoOriginalAnterior.equals(gc))
							{
								gcVectoOriginalAnterior = (GregorianCalendar) gc.clone();
								qtdTitVctoDiferentes++;
							}
							tituloFusionWrapper.setValue("vencimentoOriginal", gc);
							Timestamp dataProrrogado = (Timestamp) result[6];
							gc = new GregorianCalendar();
							gc.setTimeInMillis(dataProrrogado.getTime());
							tituloFusionWrapper.setValue("vctoProrrogado", gc);
							tituloFusionWrapper.setValue("valorOriginal", (BigDecimal) result[7]);
							tituloFusionWrapper.setValue("valorAberto", (BigDecimal) result[8]);
							tituloFusionWrapper.setValue("valorBrutoNF", (BigDecimal) result[9]);
							BigInteger sNumero = (BigInteger) result[10];
							if (sNumero != null)
							{
								tituloFusionWrapper.setValue("numeroNFSe", sNumero.toString());
							}
							Integer intCtr = (Integer) result[11];
							if (intCtr != null)
							{
								Long longCtr = new Long(intCtr);
								tituloFusionWrapper.setValue("contrato", longCtr);
							}

							List<NeoObject> listaObs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEMOR"), filterTituloSapiens, -1, -1, " seqmov ASC");
							tituloFusionWrapper.setValue("listaObservacoes", listaObs);
							PersistEngine.persist(noTitulo);

							processEntity.findField("listaTitulos").addValue(noTitulo);
						}
					}
				}
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally 
		{
			try
			{
				conSigma.close();
				conSapiens.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}