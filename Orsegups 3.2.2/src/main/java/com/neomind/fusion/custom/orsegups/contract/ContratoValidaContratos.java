package com.neomind.fusion.custom.orsegups.contract;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.axis.utils.ArrayUtil;
import org.hibernate.mapping.DenormalizedTable;
import org.jfree.util.ArrayUtilities;

import com.mchange.v2.cfg.PropertiesConfigSource.Parse;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.CadastraCentroCusto;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.gd.base.controller.actions.defaultactions.New;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;

public class ContratoValidaContratos implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		long movimentoContrato = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		
		String alteracaoCNPJ = NeoUtils.safeOutputString(wrapper.findValue("movimentoAlteracaoContrato.codTipo"));
		
		//VALIDAÇÕES APÓS CLIENTE CADASTRADO NO SAPIENS
		//-- Passo 1 --
		// Verifica se o Cliente tem Definição para a empresa/filial@
		//SELECT CODCLI FROM E085HCL WHERE CODEMP = aCodEmp AND CODFIL = aCodFil AND CODCLI = aCodCli;
		if(!validaDefinicaoCliente(wrapper))
		{
			throw new WorkflowException("Cliente não possui definições cadastradas. Favor entre em contato com departamento comercial.");
		}
		
		//-- Passo 2
		//-- Verifica se o Representante tem Definição para a empresa/filial@      
		//SELeCT CODREP FROM E090HRP WHERE CODEMP = aCodEmp AND CODREP = aCodRep;
		if(!validaDefinicaoRepresentante(wrapper))
		{
			throw new WorkflowException("Representante não possui definições cadastradas. Favor entre em contato com departamento comercial.");
		}
		
		validaTipoCliente(wrapper);
		validaSituacaoContrato(wrapper);
		validaRetencaoCliente(wrapper);
		
		String inscricaoMunicipal = NeoUtils.safeString((String) wrapper.findGenericValue("novoCliente.inscricaoMunicipal")); 
		String tipoCliente = NeoUtils.safeOutputString("buscaNomeCliente.tipcli");
		
		if(!inscricaoMunicipal.matches("[0-9]+|(?i)ISENTO") && !tipoCliente.equals("F")) {
			throw new WorkflowException("Inscrição Municipal deve ser preenchido com números ou ISENTO.");			
		}

		long diaBaseFatura = (Long) wrapper.findValue("dadosGeraisContrato.diaBaseFatura");
		if(diaBaseFatura > 31 || diaBaseFatura <= 0)
		{
			throw new WorkflowException("O campo Dia Base para Emissão da Fatura não está preenchido corretamente.");
		}
		
		//validaClienteSigmaSapiens(wrapper);
		
		Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
		
		/*
		 * empresa BACK, o ISS deve ser o mesmo para todos os postos
		 */
		if(codemp.equals("19"))
		{
			if(!validaISSIgual(postos))
			{
				throw new WorkflowException("Aliquota do ISS deve ser igual para todos os Postos.");
			}
		}
		
		/*
		 * por enquanto remover esta validacao
		 * 
		if(!validaCidadePostos(postos))
		{
			throw new WorkflowException("Todos os postos de um mesmo contrato, devem pertencer a mesma cidade!");
		}*/
		
		// VALIDAÇÕES DE TRANSAÇÃO PARA PESSOA FÍSICA
		
		String transacao = NeoUtils.safeOutputString("dadosGeraisContrato.transacao.codtns");
		
		if((codemp.equals("21") || codemp.equals("22")) && tipoCliente.equals("F") && !transacao.equals("5933F")) {
			throw new WorkflowException("Clientes físicos só aceitam a transação 5933F.");
		}
		
		if((!codemp.equals("21") && !codemp.equals("22")) && tipoCliente.equals("F") && (!transacao.equals("5949F") && !transacao.equals("6949F"))) {
			throw new WorkflowException("Clientes físicos só aceitam a transação 5949F ou 6949F.");
		}
		
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			NeoObject oTipoPosto = (NeoObject) wPosto.findValue("tipoPosto");
			EntityWrapper wTipoPosto = new EntityWrapper(oTipoPosto);

			if(NeoUtils.safeOutputString(wTipoPosto.findValue("descricao")).contains("Proteção Garantida")) {
				if(validaValorDeRepasse(wPosto)) {
					throw new WorkflowException("O valor de Repasse da PGO deve ser preenchido!!");
				}
			}
			
			if (!NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("2")) { //só valida se for um posto normal
				/*
				 * no fluxo de alteração de CNPJ devemos obrigar que todos os postos do contrato
				 * sejam inativados
				 */
				boolean origemSapiens = NeoUtils.safeBoolean(wPosto.findValue("origemSapiens"));
				boolean duplicarDesativar = NeoUtils.safeBoolean(wPosto.findValue("duplicarDesativar"));
				if(alteracaoCNPJ.equals("1") && origemSapiens && !duplicarDesativar)
				{
					throw new WorkflowException("Na alteração de CNPJ/CPF do cliente, é obrigatório inativar os postos do contrato.");
				}

				
				ContratoLogUtils.logInfo(" Validando Transações pela Regra " + (ParametrizacaoFluxoContratos.findParameter("regraTnsNova").equals("1")? "nova": "antiga") );
				validaTransacaoPosto(wrapper, wPosto, origin.getCode());
				
				// inicia verificacao de mudanca de valor do posto
				Collection<NeoObject> postosBackup = wrapper.findField("postosContratoBackup").getValues();
				long numeroPosto = 0;
				if(wPosto.findValue("numPosto") != null) numeroPosto = (Long) wPosto.findValue("numPosto");
				NeoObject postoBackupAlvo = null;
				for(NeoObject postoBackup : postosBackup)
				{
					EntityWrapper wPostoBackup = new EntityWrapper(postoBackup);
					long numeroPostoBackup = (Long) wPostoBackup.findValue("numPosto");
					if(numeroPostoBackup == numeroPosto)
					{
						postoBackupAlvo = postoBackup;
						BigDecimal formacaoPrecoAtual = null;
						BigDecimal formacaoPrecoBKP = null;
						
						formacaoPrecoAtual = (BigDecimal) wPosto.findValue("valorTotalPosto");
						formacaoPrecoBKP = (BigDecimal) wPostoBackup.findValue("valorTotalPosto");
						
						// se for termo aditivo e não mudar o valor do posto novo em relação ao antigo, não validar Data de faturamento.
						if (movimentoContrato == 5 && !formacaoPrecoAtual.equals(new BigDecimal(0))  && !formacaoPrecoBKP.equals(new BigDecimal(0)) && formacaoPrecoAtual.compareTo(formacaoPrecoBKP) == 0){
							// nao valida nada faz nada
							ContratoLogUtils.logInfo("Validacao Data Faturamento-> tarefa:" + origin.getCode() + ", Preco do posto novo igual ao preco do posto antigo. não será validado");
						}else{
							ContratoLogUtils.logInfo("Validacao Data Faturamento-> tarefa:" + origin.getCode() + ", Preco do posto novo("+formacaoPrecoAtual+") diferente do preco do posto antigo("+formacaoPrecoBKP+"). Será validado");
							validaDataFaturamento(wPosto);
							validaDataFimFaturamento(wPosto);
						}
					}
				}
				
				//se não encontrou posto de backup, validar
				if(postoBackupAlvo == null)
				{
					validaDataFaturamento(wPosto);
					validaDataFimFaturamento(wPosto);
				}
				
				//if (true) throw new WorkflowException("Me remova");
				
				//fim validação de alteração de valor do posto
				
				
				
				validaDataInicioFimFaturamento(wPosto);
				
				validaEquipamentosPosto(wrapper, wPosto);
				
				validaCheckList(wPosto);
				
				/*
				String situacaoPosto = NeoUtils.safeOutputString(wPosto.findValue("situacaoPosto.situacao"));
				if(situacaoPosto.equals("I"))
				{
					throw new WorkflowException("Não é permitido cadastrar o posto como inativo. Cadastre o posto como ativo e após altere para inativo!!!");
				}*/
			}
			//Um contrato Ativo sempre deve ter um posto Ativo
			}
			
			
		
	}
	
	private void validaRetencaoCliente(EntityWrapper wrapper) {
		
		Long regimeTributario = wrapper.findGenericValue("buscaNomeCliente.codrtr");
		String cofins = NeoUtils.safeOutputString(wrapper.findGenericValue("novoCliente.retcofins"));
		String csl = NeoUtils.safeOutputString(wrapper.findGenericValue("novoCliente.retcsl"));
		String ir = NeoUtils.safeOutputString(wrapper.findGenericValue("novoCliente.retir"));
		String pis = NeoUtils.safeOutputString(wrapper.findGenericValue("novoCliente.retpis"));
		String tipoCliente = NeoUtils.safeOutputString(wrapper.findGenericValue("buscaNomeCliente.tipcli"));
		String cpfCnpj = NeoUtils.safeOutputString(wrapper.findGenericValue("buscaNomeCliente.cgccpf"));
		
		if (NeoUtils.safeIsNull(tipoCliente)) {
			
			tipoCliente = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipocliente.tipo"));
			
			if (NeoUtils.safeIsNull(cpfCnpj)) {
				cpfCnpj = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.cpf"));
			}
			
		}
		
		if (NeoUtils.safeIsNotNull(regimeTributario) && regimeTributario == 1L && 
				(!cofins.equals("N") || !csl.equals("N") || !pis.equals("N"))) {
			throw new WorkflowException("Para clientes cadastrados como SIMPLES NACIONAL, as retenções de PIS, COFINS e CSLL devem ser assinalados como N.");
		}
		
		if (tipoCliente.equals("F") && NeoUtils.safeIsNotNull(cpfCnpj) && !cpfCnpj.equals("0") &&
				(!cofins.equals("N") || !csl.equals("N") || !pis.equals("N") || !ir.equals("N"))) {
			throw new WorkflowException("Para cliente Pessoa Física e com CPF preenchido, as retenções de PIS, COFINS, IRRF e CSLL devem ser assinalados como N.");
		}
		
		valorValidoRetencaoCliente(cofins);
		valorValidoRetencaoCliente(csl);
		valorValidoRetencaoCliente(ir);
		valorValidoRetencaoCliente(pis);
		
	}

	private boolean valorValidoRetencaoCliente(String retencao) {
		
		retencao = retencao.toUpperCase();
		
		if (retencao.equals("S") || retencao.equals("N")) {
			return true;
		} else {
			throw new WorkflowException("O campo de retenção de "+retencao+" deve ser preenchido com N ou S.");	
		}
		
	}

	public static void validaClienteSigmaSapiens(EntityWrapper wrapper)
	{
		String nCodEmp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String nCodFil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String nNumCtr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		if(nNumCtr.equals(""))
			nNumCtr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		StringBuilder sql = new StringBuilder();
		sql.append("select e085cli.cgccpf, e085cli.tipcli, e085cli.CodCli "); 
		sql.append("from e085cli, usu_t160ctr ");
		
		sql.append("where ");
		sql.append("e085cli.CodCli = usu_t160ctr.usu_codcli ");
		sql.append(" AND usu_t160ctr.usu_codemp = " + nCodEmp);
		sql.append(" AND usu_t160ctr.usu_codfil = " + nCodFil);
		sql.append(" AND usu_t160ctr.usu_numctr = " + nNumCtr);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] os = (Object[]) result;
					String cpf = NeoUtils.safeOutputString(os[0]);
					String tipCli = NeoUtils.safeOutputString(os[1]);
					String codcli = NeoUtils.safeOutputString(os[2]);
					
					QLEqualsFilter filtroCliente = new QLEqualsFilter("cd_cliente",NeoUtils.safeLong(codcli));	
					List<NeoObject> clientes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACENTRAL"),filtroCliente);
					if(clientes.size() > 0)
					{
						
					}else
					{
						throw new WorkflowException("ERRO - Conta "+codcli+" inexistente no SIGMA!");
					}
				}
			}
		}
	}
	
	public static void validaCheckList(EntityWrapper wPosto)
	{
	    if(wPosto.findField("equipamentosItens").getValue() != null){
		Collection<NeoObject> itens = wPosto.findField("equipamentosItens.itensChecklist").getValues();
		for(NeoObject item : itens)
		{
			EntityWrapper wItem = new EntityWrapper(item);
			GregorianCalendar prazo = (GregorianCalendar) wItem.findValue("prazo");
			if(prazo == null || NeoUtils.safeBoolean(wItem.findValue("prazoPadrao")))
			{
				prazo = new GregorianCalendar();
				prazo.add(prazo.DAY_OF_MONTH, 7);
				
			}
			
			prazo.set(Calendar.HOUR_OF_DAY, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);
			wItem.setValue("prazo", prazo);
			
			GregorianCalendar hoje = new GregorianCalendar();
			
			if(prazo.before(hoje))
			{
				throw new WorkflowException("Prazo do Check List menor ou igual à data de hoje. Favor ajustar!!!");
			}
			
			int dayWeek = prazo.get(prazo.DAY_OF_WEEK);
			if(dayWeek == 1 || dayWeek == 7 )
			{
				throw new WorkflowException("Dia do prazo do Check List é Sábado ou Domingo. Favor ajustar para alguma data útil!!!");
			}
		}
	    }
	}
	
	public static void validaEquipamentosPosto(EntityWrapper wContrato, EntityWrapper wPosto)
	{
		String aCodPro = "";
		//String aCodDer = "";
		String codEmp = NeoUtils.safeOutputString(wContrato.findValue("empresa.codemp"));
		String codFil = NeoUtils.safeOutputString(wContrato.findValue("empresa.codfil"));
		String aTipIns = "";
		/*
		if(wPosto.findField("equipamentosItens").getValue() != null){
			
			Collection<NeoObject> equipamentos = wPosto.findField("equipamentosItens.equipamentosPosto").getValues();
			for(NeoObject equipamento : equipamentos)
			{
				
				EntityWrapper wEquip = new EntityWrapper(equipamento);
				aCodPro = NeoUtils.safeOutputString(wEquip.findValue("produto.codpro"));
				//aCodDer = NeoUtils.safeOutputString(wEquip.findValue("equipamento.usu_codder"));
				aTipIns = NeoUtils.safeOutputString(wEquip.findValue("tipoInstalacao.codigo"));
				
				StringBuilder sql = new StringBuilder();
				sql.append("select "); 
				sql.append(" 1 ");
				sql.append("from E082ITP,E082TPR ");
				
				sql.append("where ");
				sql.append("E082ITP.CODEMP = E082TPR.CODEMP AND E082ITP.CODTPR = E082TPR.CODTPR ");
				sql.append("AND E082ITP.DATINI = E082TPR.DATINI AND E082ITP.CODPRO = '" + aCodPro +"' ");
				if(codEmp.equals("18") && codFil.equals("2"))
				{
					//sql.append("AND E082ITP.CODDER = '" + aCodDer + "' AND E082ITP.CODTPR IN ('12','13') ");
					sql.append("AND E082ITP.CODTPR IN ('12','13') ");
				}else
				{
					//sql.append("AND E082ITP.CODDER = '" + aCodDer + "' AND E082ITP.CODTPR IN ('5','6','7','8') ");
					sql.append("AND E082ITP.CODTPR IN ('5','6','7','8') ");
				}
				
				sql.append("AND E082ITP.CODEMP = 1 AND E082ITP.SITREG = 'A' AND E082TPR.SITREG = 'A'");
				
				Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
				Collection<Object> resultList = query.getResultList();
				if (resultList != null && !resultList.isEmpty())
				{
					//achou, sem problemas
				}else
				{
					if(aTipIns.equals("F") && codEmp.equals("18") && codFil.equals("2"))
					{
						throw new WorkflowException("O produto " + aCodPro + " não existe na Tabela de Preço 12 - SITECH. Favor informe o setor de compras sobre o problema!");
					}else if(!codEmp.equals("18") || !codFil.equals("2"))
					{
						if(aTipIns.equals("F"))
						{
							throw new WorkflowException("O produto " + aCodPro + " não existe na Tabela de Preço 6 - GSN ou 8 - ASP. Favor informe o setor de compras sobre o problema!");
						}else if(aTipIns.equals("D"))
						{
							throw new WorkflowException("O produto " + aCodPro + " não existe na Tabela de Preço 5 - GSN ou 7 - ASP. Favor informe o setor de compras sobre o problema!");
						}
						
					}
				}
			}
			
		}*/
		
	}
	
	public static boolean validaCidadePostos(Collection<NeoObject> postos)
	{
		String cidadeAlvo = "";
		
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			String cidade = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.nomcid"));
			if(cidadeAlvo.equals(""))
				cidadeAlvo = cidade;
			else if(!cidadeAlvo.equals(cidade))	
			{
				return false;
			}
		}
		
		return true;
	}
	
	public static boolean validaISSIgual(Collection<NeoObject> postos)
	{
		String issAlvo = "";
		
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			String iss = NeoUtils.safeOutputString(wPosto.findValue("valorISS"));
			if(issAlvo.equals(""))
				issAlvo = iss;
			else if(!issAlvo.equals(iss))	
			{
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
	
	public static void validaSituacaoContrato(EntityWrapper wrapper)
	{
		long tipoMovimento = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("movimentoContratoNovo.codTipo")));
		
		if(tipoMovimento == 5)
		{
			String sitctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_sitctr"));
			BigDecimal iss = new BigDecimal(0);

			if(wrapper.findValue("postosContrato.valorISS") != null)
			{
				iss = new BigDecimal((String)wrapper.findValue("postosContrato.valorISS"));
			}
			if(sitctr.equals("I"))
			{
				throw new WorkflowException("Não é possivel cadastrar o posto porque o contrato está inativo!!!");
			}
			
			if(iss != null && iss.equals(0) )
			{
				throw new WorkflowException("ISS com o valor zerado!!!");
			}else if (iss == null){
				throw new WorkflowException("ISS nulo!!!");
			}
		}else if(tipoMovimento != 5) {
			
			String valorISS = NeoUtils.safeString((String) wrapper.findValue("postosContrato.valorISS"));
			BigDecimal iss = new BigDecimal((String)wrapper.findValue("postosContrato.valorISS"));
			
			/**
			 * Compare to:
			 * -1 = smaller than the object passed as a parameter
			 * 	0 = equals to the object passed as a parameter
			 * 	1 = greater than the object passed as a parameter
			 */
			if(NeoUtils.safeIsNull(valorISS) || valorISS.equals("0") || iss.compareTo(new BigDecimal(0.0)) < 1) {
				throw new WorkflowException("Valor de ISS nulo ou zerado!");
			}
			
		}
		
	}
	//numContrato.usu_sitctr
	
	public static void validaDataFaturamento(EntityWrapper wPosto)
	{
		//TODO se for 
		GregorianCalendar dataIniFat = (GregorianCalendar) wPosto.findValue("periodoFaturamento.de");
		if (dataIniFat== null){
			throw new WorkflowException("Periodo de Faturamento do posto não preenchido.");
		}
		GregorianCalendar hoje = new GregorianCalendar();
		
		int dias = NeoCalendarUtils.calculateDiff(dataIniFat.getTime(), hoje.getTime());
		
		if(dias > 30)
			throw new WorkflowException("A data de inicio de faturamento do posto não pode ser maior que 30 dias da data atual!!!");
	}
	
	public static String verificaParametroTransacao(String usu_codemp, String usu_codfil, String usu_tipcli, String usu_tipemc, String usu_denfor, String usu_codfam, String usu_codtns){
		String retorno = "";
		
		//if (true)return "5949F"; //TODO DESFIXCONTRATOS
		
		String sql = " SELECT usu_codemp, usu_codfil, usu_tipcli, usu_tipemc, usu_denfor, usu_codtns,  usu_codfam FROM USU_T001TNS "+
		" where 1=1 and usu_codemp=" + usu_codemp + 
		" and usu_codfil =" + usu_codfil +
		" and usu_tipcli ='" + usu_tipcli + "' " +
		" and usu_tipemc =" + usu_tipemc +
		" and usu_denfor ='" + usu_denfor + "' " +
		//" and usu_codtns =" + usu_codtns +
		" and usu_codfam = '" + usu_codfam + "' " ;
		
		ContratoLogUtils.logInfo("verificaParametroTransacao->"+sql);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql);
		Collection<Object[]> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty()){
			for (Object res[] : resultList){
				System.out.println("TNS " + NeoUtils.safeOutputString(res[5]));
				if (NeoUtils.safeOutputString(res[5]).equals( NeoUtils.safeOutputString(usu_codtns) )){
					retorno =  NeoUtils.safeOutputString(res[5]);
					break;
				}
			}
		}
		
		
		
		return retorno;
	}
	
	public static void validaTransacaoPosto(EntityWrapper wrapper, EntityWrapper wPosto, String codeTarefa)
	{
		String codEmp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codFil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String tipCli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipcli"));
		String ufPosto = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.estado.sigufs"));
		String ufCLi = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.sigufs"));
		String cidade = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.nomcid"));
		String tipEmc = "1"; 
		String denFor = "";
		String codFam = NeoUtils.safeOutputString(wPosto.findValue("complementoServico.codfam"));
		String transacaoPosto = NeoUtils.safeOutputString(wPosto.findValue("transacao.codtns"));
		if(tipCli.equals(""))
		{
			tipCli = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipocliente.tipo"));
		}
		
		
		 if ( ( codEmp.equals("18") && codFil.equals("2") && tipCli.equals("F") ) 
				 	|| (codEmp.equals("21") && tipCli.equals("F")) 
					|| (codEmp.equals("22") && tipCli.equals("F")) 
				 )
		{
			
			if(ufCLi.equals("PR") && ufPosto.equals("PR"))	denFor = "D"; else	denFor = "F";
			
		} else if ( ( codEmp.equals("18") && codFil.equals("2") && tipCli.equals("J") )
					|| (codEmp.equals("21") && tipCli.equals("J")) 
					|| (codEmp.equals("22") && tipCli.equals("J")) 
				 )
		{
			
			if(ufCLi.equals("PR") && ufPosto.equals("PR") /*&& cidade.equals("CURITIBA")*/)	denFor = "D"; else denFor = "F";
			
		}else if( !codEmp.equals("21") && !codEmp.equals("22") && ufCLi.equals("SC") && ufPosto.equals("SC") )
		{
			denFor = "D";
			
		}else {
			denFor = "F";
		}
		 
		 if (denFor.equals("")){
			 throw new WorkflowException("Não foi possível verificar se o posto está dentro ou fora do estado selecionado. Verificar junto à TI.");
		 }
		 
		 String debugInfo = "Tarefa["+codeTarefa+"] - Parametros de Validação => \nEmpresa=" + codEmp +
				  " \nFilial=" + codFil +
				  " \nTipo de Cliente=" + tipCli +
				  " \nUF="  + ufPosto +
				  " \nCidade=" + cidade +
				  " \nDentro ou fora do estado=" + denFor +
				  " \nFamilia do serviço=" + codFam +
				  " \ntransacaoPosto=" + transacaoPosto +
				  " \nUF cad. Cliente.="+ufCLi +
				  " \nUF cad. Posto="+ufPosto;
		 
		 ContratoLogUtils.logInfo(debugInfo);
		
		String retornoTransacao = verificaParametroTransacao(codEmp, codFil, tipCli, tipEmc, denFor, codFam, transacaoPosto);
		if (!retornoTransacao.trim().equals("") && !retornoTransacao.equals(transacaoPosto)){
			throw new WorkflowException("A Transação do posto deve ser: " + retornoTransacao + ". " + debugInfo );
		}else if ( retornoTransacao == null || ( retornoTransacao != null && retornoTransacao.trim().equals("") ) ){
			throw new WorkflowException("Transação não se enquadra em nenhuma regra de validação. Verificar junto ao departamento financeiro. " + debugInfo);
		}
	}
	
	
	
	
	private static boolean cotemTransacao(String lista[], String transacao){
		if (lista != null){
			for (String tns: lista){
				if (transacao.equals(tns)) return true;
			}
			return false;
		}else{
			return false;
		}
	}
	
	private static String listaToString(String lista[]){
		StringBuilder sb = new StringBuilder();
		if (lista != null){
			for (String tns: lista){
				sb.append(tns+",");
			}
		}
		return sb.toString().substring(0,sb.toString().length()-1);
	}
	
	
	
	public static boolean validaDefinicaoCliente(EntityWrapper wrapper)
	{
		String codEmp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codFil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String codCli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codcli"));
		if(codCli.equals(""))
			codCli = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.codigoCliente"));
		
		//SELECT CODCLI FROM E085HCL WHERE CODEMP = aCodEmp AND CODFIL = aCodFil AND CODCLI = aCodCli;
		StringBuilder sql = new StringBuilder();
		sql.append("select "); 
		sql.append("hcl.CODCLI ");
		sql.append("from E085HCL hcl ");
		
		sql.append("where ");
		sql.append("hcl.CODEMP = " + codEmp);
		sql.append(" and hcl.CODFIL = " + codFil);
		sql.append(" and hcl.codcli = " + codCli);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			return true;
		}else
			return false;
		
	}
	
	public static boolean validaDefinicaoRepresentante(EntityWrapper wrapper)
	{
		String codEmp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		
		String codRep = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.representante.codrep"));
		
		//SELeCT CODREP FROM E090HRP WHERE CODEMP = aCodEmp AND CODREP = aCodRep;
		StringBuilder sql = new StringBuilder(); 
		sql.append("select "); 
		sql.append("hrp.CODREP ");
		sql.append("from E090HRP hrp ");
		
		sql.append("where ");
		sql.append("hrp.CODEMP = " + codEmp);
		sql.append(" and hrp.CODREP = " + codRep);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			return true;
		}else
			return false;
		
	}
	
	public static void validaTipoCliente(EntityWrapper wrapper)
	{
		//-- Passo 3
		/*
		-- Verifica o tipo do cliente

		SELECT TIPEMC,CEPCLI, TIPCLI FROM E085CLI WHERE CODCLI = vCodCli
		SELECT TIPEMC,CEPCLI, TIPCLI FROM E085CLI WHERE CODCLI = 2

		--TipCli F - FISICO / J - JURIDICO
		--TnsSer COD DA TRANSACAO do contrato
		--TIPEMC - 1 -privado ou 2 - publico

		se((TipCli = F) e (TnsSer <> 5949F) e (TnsSer <> 6949F))
		      Alerta do usuario: (Clientes físicos só aceitam transação 5949F ou 6949F!);
		      
		se ( vTipEmc = 0 ) @----- Não Preenchido -----@
		      Alerta do usuario:(Tipo do cliente não definido. Favor informar o tipo no cadastro de cliente);
		 */
		boolean novocliente = false;
		String tipCli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipcli"));
		if(tipCli.equals(""))
		{
			tipCli = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipocliente.tipo"));
			novocliente = true;
		}
		
		String codTrans = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.transacao.codtns"));
		
		String tipEmc = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipemc"));
		
		if(tipEmc.equals("") && !novocliente)
			throw new WorkflowException("Tipo do cliente não definido. Favor informar o tipo no cadastro de cliente!");
		else if(tipEmc.equals("") && novocliente)
		{
			tipEmc = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipoEmpresa.tipo"));
			if(tipEmc.equals(""))
				throw new WorkflowException("Tipo do cliente não definido. Favor informar o tipo no cadastro de cliente!");
		}
	}
	
	/**
	 * Valida se a data de inicio do posto novo é menor que a data de fim do posto antigo para inativação. 
	 * @param wPosto
	 */
	public static void validaDataFimFaturamento(EntityWrapper wPosto)
	{
		GregorianCalendar dataIniFat = (GregorianCalendar) wPosto.findValue("periodoFaturamento.de");
		GregorianCalendar dataFimFat = (GregorianCalendar) wPosto.findValue("dataFimFaturamento");
		
		if (dataFimFat == null) return;
			
		if (dataIniFat== null){
			throw new WorkflowException("Periodo de Faturamento do posto não preenchido.");
		}
			
		
		GregorianCalendar hoje = new GregorianCalendar();
		
		int dias = NeoCalendarUtils.calculateDiff(dataIniFat.getTime(), dataFimFat.getTime());
		
		if(dias < 0)
			throw new WorkflowException("A data de inicio de faturamento não pode ser menor que a data fim de faturamento do posto antigo!!!");
	}
	
	/**
	 * Valida se a data de fim de faturamento é menor que a data de inicio de faturamento do posto. 
	 * @param wPosto
	 */
	public static void validaDataInicioFimFaturamento(EntityWrapper wPosto)
	{
		GregorianCalendar dataIniFat = (GregorianCalendar) wPosto.findValue("periodoFaturamento.de");
		GregorianCalendar dataFimFat = (GregorianCalendar) wPosto.findValue("periodoFaturamento.ate");
		
		if (dataFimFat == null) return; // se a data fim != null não tem porque validar
		
		if (dataIniFat== null){
			throw new WorkflowException("Periodo de Faturamento do posto não preenchido.");
		}
		
		GregorianCalendar hoje = new GregorianCalendar();
		
		int dias = NeoCalendarUtils.calculateDiff(dataIniFat.getTime(), dataFimFat.getTime());
		
		if( dias < 0)
			throw new WorkflowException("A data de Fim de faturamento não pode ser menor que a data de Início de faturamento!!!");
	}
	
	/**
	 * 
	 * @return <b>False</b> para quando o valor estiver preenchido
	 * 		   <b>True</b> para quando o valor não estiver preenchido
	 */
	public static Boolean validaValorDeRepasse(EntityWrapper posto) {
		
		Boolean resultado = null;
		BigDecimal valorRepasse = new BigDecimal(0.00);
		
		valorRepasse = (BigDecimal) posto.findValue("valorRepasseProtecaoG");
		
		if(NeoUtils.safeIsNull(valorRepasse) || (valorRepasse.compareTo(new BigDecimal(0.00)) < 1)) {
			resultado = true;
		}else {
			resultado = false;
		}
		
		return resultado;
	}
	
}