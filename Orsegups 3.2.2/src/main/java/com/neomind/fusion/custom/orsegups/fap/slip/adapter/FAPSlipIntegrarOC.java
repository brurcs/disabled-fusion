package com.neomind.fusion.custom.orsegups.fap.slip.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

import br.com.senior.services.fap.G5SeniorServicesLocator;
import br.com.senior.services.fap.OrdemcompraaprovarIn;
import br.com.senior.services.fap.OrdemcompraaprovarInOrdemCompra;
import br.com.senior.services.fap.OrdemcompraaprovarOut;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4In;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4OutOrdemCompra;
import br.com.senior.services.vetorh.constants.WSConfigs;

public class FAPSlipIntegrarOC implements AdapterInterface
{
	public void start(Task paramTask, EntityWrapper wrapper, Activity paramActivity)
	{		
		Boolean aprovacaoDiretoria = wrapper.findGenericValue("aprovarPagamento");
		Long codOrdemCompra = wrapper.findGenericValue("ordemCompra.ordemCompra");
		String descricaoErro = "";
		
		Boolean integracaoOK = false;
		
		WSConfigs config = new WSConfigs();
		G5SeniorServicesLocator locator = new G5SeniorServicesLocator();
		
		OrdemcomprabuscarPendentes4In buscaPendentesIn = new OrdemcomprabuscarPendentes4In();
		buscaPendentesIn.setCodUsu(50);
		
		if (aprovacaoDiretoria)
		{
			OrdemcomprabuscarPendentes4Out buscaPendentesOut;
			try
			{
				buscaPendentesOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().buscarPendentes_4(config.getUserService(), config.getPasswordService(), 0, buscaPendentesIn);
				if (buscaPendentesOut.getErroExecucao() == null || buscaPendentesOut.getErroExecucao().trim().equals(""))
				{
					OrdemcomprabuscarPendentes4OutOrdemCompra[] solicitacoes = buscaPendentesOut.getOrdemCompra();
					for (OrdemcomprabuscarPendentes4OutOrdemCompra solicitacao : solicitacoes)
					{
						if (codOrdemCompra.equals(solicitacao.getNumero().longValue()))
						{									
							OrdemcompraaprovarInOrdemCompra aprovacao = new OrdemcompraaprovarInOrdemCompra();
							aprovacao.setCodigoEmpresa(solicitacao.getCodigoEmpresa());
							aprovacao.setCodigoFilial(solicitacao.getCodigoFilial());
							aprovacao.setNumero(solicitacao.getNumero());
							aprovacao.setSequencia(0);
							
							OrdemcompraaprovarInOrdemCompra[] aprovarArray = {aprovacao};
							
							OrdemcompraaprovarIn aprovarIn = new OrdemcompraaprovarIn();
							aprovarIn.setOrdemCompra(aprovarArray);
							
							try
							{
								OrdemcompraaprovarOut aprovarOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().aprovar(config.getUserService(), config.getPasswordService(), 0, aprovarIn);
								if (aprovarOut.getErroExecucao() == null || aprovarOut.getErroExecucao().trim().equals(""))
								{
									integracaoOK = true;
								}
								else {
									descricaoErro = "Houve um erro ao aprovar a Ordem de Compra. Tente novamente em alguns instantes.";
								}
							}
							catch (Exception e)
							{
								e.printStackTrace();
								descricaoErro = "Houve um erro ao aprovar a Ordem de Compra. Por favor, contate a TI.";
							}
						}	
					}
				}
				else {
					descricaoErro = "Houve um erro ao aprovar a Ordem de Compra. Tente novamente em alguns instantes.";
				}
			}
			catch (Exception e1)
			{
				descricaoErro = "Houve um erro ao aprovar a Ordem de Compra. Por favor, contate a TI.";
				e1.printStackTrace();
			}
			
			wrapper.setValue("integracaoOK", integracaoOK);
			
			registrarHistorico(wrapper, "Integração Sapiens", descricaoErro);
		}
	}
	
	private void registrarHistorico(EntityWrapper wrapper, String atividade, String descricao)
	{		
		NeoObject noRegistroAtividade = AdapterUtils.createNewEntityInstance("FAPSlipRegistroHistorico");
		EntityWrapper wRegistroAtividade = new EntityWrapper(noRegistroAtividade);
		wRegistroAtividade.setValue("data", new GregorianCalendar());
		wRegistroAtividade.setValue("atividade", atividade);
		wRegistroAtividade.setValue("responsavelStr", "Sistema");
		wRegistroAtividade.setValue("descricao", descricao);
		
		wrapper.findField("historicoAtividades").addValue(noRegistroAtividade);
	}

	@Override
	public void back(EntityWrapper paramEntityWrapper, Activity paramActivity)
	{
		// TODO Auto-generated method stub
		
	}
}
