package com.neomind.fusion.custom.orsegups.utils;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class ContratoVO {
	
	private Long empresa;
	private Long filial;
	private Long numeroContrato;
	private String numeroOficialContrato;
	private Long codigoIndiceReajuste;
	private String descricaoIndiceReajuste;
	private String nomeCliente;
	private Timestamp dataInicioVigencia;
	private Timestamp dataFimVigencia;
	private Long tempoVigencia;
	private BigDecimal valorPosto;
	private String nomeRepresentante;
	private String usuarioFusionRepresentante;
	private String servico;
	private String observacaoServico;
	private String motivo;
	private String observacaoMotivo;
	private Timestamp dataEncerramento;
	
	public Long getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Long empresa) {
		this.empresa = empresa;
	}
	public Long getFilial() {
		return filial;
	}
	public void setFilial(Long filial) {
		this.filial = filial;
	}
	public Long getNumeroContrato() {
		return numeroContrato;
	}
	public void setNumeroContrato(Long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getNumeroOficialContrato() {
		return numeroOficialContrato;
	}
	public void setNumeroOficialContrato(String numeroOficialContrato) {
		this.numeroOficialContrato = numeroOficialContrato;
	}
	public Long getCodigoIndiceReajuste() {
		return codigoIndiceReajuste;
	}
	public void setCodigoIndiceReajuste(Long codigoIndiceReajuste) {
		this.codigoIndiceReajuste = codigoIndiceReajuste;
	}
	public String getDescricaoIndiceReajuste() {
		return descricaoIndiceReajuste;
	}
	public void setDescricaoIndiceReajuste(String descricaoIndiceReajuste) {
		this.descricaoIndiceReajuste = descricaoIndiceReajuste;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public Timestamp getDataInicioVigencia() {
		return dataInicioVigencia;
	}
	public void setDataInicioVigencia(Timestamp dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}
	public Timestamp getDataFimVigencia() {
		return dataFimVigencia;
	}
	public void setDataFimVigencia(Timestamp dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}
	public Long getTempoVigencia() {
		return tempoVigencia;
	}
	public void setTempoVigencia(Long tempoVigencia) {
		this.tempoVigencia = tempoVigencia;
	}

	public BigDecimal getValorPosto() {
		return valorPosto;
	}

	public void setValorPosto(BigDecimal valorPosto) {
		this.valorPosto = valorPosto;
	}

	public String getNomeRepresentante() {
		return nomeRepresentante;
	}

	public void setNomeRepresentante(String nomeRepresentante) {
		this.nomeRepresentante = nomeRepresentante;
	}

	public String getUsuarioFusionRepresentante() {
		return usuarioFusionRepresentante;
	}

	public void setUsuarioFusionRepresentante(String usuarioFusionRepresentante) {
		this.usuarioFusionRepresentante = usuarioFusionRepresentante;
	}

	public String getServico() {
		return servico;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public String getObservacaoServico() {
		return observacaoServico;
	}

	public void setObservacaoServico(String observacaoServico) {
		this.observacaoServico = observacaoServico;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getObservacaoMotivo() {
		return observacaoMotivo;
	}

	public void setObservacaoMotivo(String observacaoMotivo) {
		this.observacaoMotivo = observacaoMotivo;
	}

	public Timestamp getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(Timestamp dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}
}
