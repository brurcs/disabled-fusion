package com.neomind.fusion.custom.orsegups.adapter.financeiro;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "infNFe", namespace = "http://www.portalfiscal.inf.br/nfe")
public class InfoNotaFiscal {
	
	private String id;
	private String versao;
	private Emissor emissor;
	private DocumentoEletronico documentoEletronico;

	@XmlAttribute(name="Id")
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@XmlAttribute(name="versao")
	public String getVersao() {
		return versao;
	}
	
	public void setVersao(String versao) {
		this.versao = versao;
	}

	@XmlElement(name="emit", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Emissor getEmissor() {
		return emissor;
	}

	public void setEmissor(Emissor emissor) {
		this.emissor = emissor;
	}

	@XmlElement(name="ide", namespace = "http://www.portalfiscal.inf.br/nfe")
	public DocumentoEletronico getDocumentoEletronico() {
		return documentoEletronico;
	}

	public void setDocumentoEletronico(DocumentoEletronico documentoEletronico) {
		this.documentoEletronico = documentoEletronico;
	}

}
