package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class FCNRecuperaListaEquipamentos implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Boolean retiraEquipamento = (Boolean) processEntity.findValue("retiraEquipamento");
		if (retiraEquipamento)
		{
			GregorianCalendar jan2011 = new GregorianCalendar(2011, GregorianCalendar.JANUARY, 1);
			GregorianCalendar inicioVigencia = (GregorianCalendar) processEntity.findValue("contratoSapiens.usu_inivig");
			Boolean isListEmpty = true;

			// Se contrato tem vigência >= a 01/01/2011 (início EQP), considera equipamentos do contrato (EQP) - tipos 'R', 'N', 'D', 'E'.
			if (inicioVigencia != null && inicioVigencia.compareTo(jan2011) >= 0)
			{
				Long empresa = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
				Long filial = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
				Long contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

				QLGroupFilter filterCtr = new QLGroupFilter("AND");
				filterCtr.addFilter(new QLEqualsFilter("usu_codemp", empresa));
				filterCtr.addFilter(new QLEqualsFilter("usu_codfil", filial));
				filterCtr.addFilter(new QLEqualsFilter("usu_numctr", contrato));
				QLGroupFilter filterTipoEqp = new QLGroupFilter("OR");
				//				filterTipoEqp.addFilter(new QLEqualsFilter("usu_tipins", "R"));
				filterTipoEqp.addFilter(new QLEqualsFilter("usu_tipins", "N"));
				filterTipoEqp.addFilter(new QLEqualsFilter("usu_tipins", "D"));
				filterTipoEqp.addFilter(new QLEqualsFilter("usu_tipins", "E"));
				filterCtr.addFilter(filterTipoEqp);
				List<NeoObject> listaEqp = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTEQP"), filterCtr, -1, -1, " usu_seqeqp ASC");
				if (listaEqp != null && !listaEqp.isEmpty())
				{
					for (NeoObject eqp : listaEqp)
					{
						EntityWrapper wrapperEqp = new EntityWrapper(eqp);
						Long empresaProduto = (Long) wrapperEqp.findField("usu_emppro").getValue();
						String codigoProduto = (String) wrapperEqp.findField("usu_codpro").getValue();
						Long quantidade = (Long) wrapperEqp.findField("usu_qtdeqp").getValue();
						QLGroupFilter filterProduto = new QLGroupFilter("AND");
						filterProduto.addFilter(new QLEqualsFilter("codemp", empresaProduto));
						filterProduto.addFilter(new QLEqualsFilter("codpro", codigoProduto));
						List<NeoObject> listaObjectProduto = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SAPIENSEPRO")).getEntityClass(), filterProduto);
						if (listaObjectProduto != null && !listaObjectProduto.isEmpty())
						{
							NeoObject noProduto = listaObjectProduto.get(0);
							if (noProduto != null)
							{
								NeoObject noDescricaoEqp = AdapterUtils.createNewEntityInstance("FCNDescricaoEquipamento");
								EntityWrapper descricaoEqpWrapper = new EntityWrapper(noDescricaoEqp);
								EntityWrapper produtoSapiensWrapper = new EntityWrapper(noProduto);
								descricaoEqpWrapper.setValue("codigoProduto", listaObjectProduto.get(0));
								descricaoEqpWrapper.setValue("descricaoEquipamento", produtoSapiensWrapper.getValue("despro"));
								descricaoEqpWrapper.setValue("quantidadeEquipamento", BigDecimal.valueOf(quantidade));
								PersistEngine.persist(noDescricaoEqp);
								processEntity.findField("listaEquipamento").addValue(noDescricaoEqp);
								
								//efetuando uma copia dos dados para a tabela de equipamentos recebidos								
								addListaEquipamentosReceber(processEntity, (String) produtoSapiensWrapper.getValue("despro"), listaObjectProduto.get(0));
								
								isListEmpty = false;
							}
						}
					}
				}
			}
			if (isListEmpty)
			{
				// Caso lista ainda vazia adiciona o KIT básico de equipamentos

				// 1 CENTRAL
				NeoObject noCentral = AdapterUtils.createNewEntityInstance("FCNDescricaoEquipamento");
				EntityWrapper centralWrapper = new EntityWrapper(noCentral);
				centralWrapper.setValue("descricaoEquipamento", "CENTRAL MICROPROCESSADA");
				centralWrapper.setValue("quantidadeEquipamento", new BigDecimal(1));
				PersistEngine.persist(noCentral);
				processEntity.findField("listaEquipamento").addValue(noCentral);
				
				//efetuando uma copia dos dados para a tabela de equipamentos recebidos
				addListaEquipamentosReceber(processEntity, "CENTRAL MICROPROCESSADA", null);
				
				// 1 BATERIA
				NeoObject noBateria = AdapterUtils.createNewEntityInstance("FCNDescricaoEquipamento");
				EntityWrapper bateriaWrapper = new EntityWrapper(noBateria);
				bateriaWrapper.setValue("descricaoEquipamento", "BATERIA");
				bateriaWrapper.setValue("quantidadeEquipamento", new BigDecimal(1));
				PersistEngine.persist(noBateria);
				processEntity.findField("listaEquipamento").addValue(noBateria);
				
				//efetuando uma copia dos dados para a tabela de equipamentos recebidos
				addListaEquipamentosReceber(processEntity, "BATERIA", null);
				
				// 1 TRAFO
				NeoObject noTrafo = AdapterUtils.createNewEntityInstance("FCNDescricaoEquipamento");
				EntityWrapper trafoWrapper = new EntityWrapper(noTrafo);
				trafoWrapper.setValue("descricaoEquipamento", "TRAFO");
				trafoWrapper.setValue("quantidadeEquipamento", new BigDecimal(1));
				PersistEngine.persist(noTrafo);
				processEntity.findField("listaEquipamento").addValue(noTrafo);
				
				//efetuando uma copia dos dados para a tabela de equipamentos recebidos
				addListaEquipamentosReceber(processEntity, "TRAFO", null);
				
				// 1 CAIXA METÁLICA
				NeoObject noCaixa = AdapterUtils.createNewEntityInstance("FCNDescricaoEquipamento");
				EntityWrapper caixaWrapper = new EntityWrapper(noCaixa);
				caixaWrapper.setValue("descricaoEquipamento", "CAIXA METÁLICA");
				caixaWrapper.setValue("quantidadeEquipamento", new BigDecimal(1));
				PersistEngine.persist(noCaixa);
				processEntity.findField("listaEquipamento").addValue(noCaixa);
				
				//efetuando uma copia dos dados para a tabela de equipamentos recebidos
				addListaEquipamentosReceber(processEntity, "CAIXA METÁLICA", null);
				
				// 1 TECLADO
				NeoObject noTeclado = AdapterUtils.createNewEntityInstance("FCNDescricaoEquipamento");
				EntityWrapper tecladoWrapper = new EntityWrapper(noTeclado);
				tecladoWrapper.setValue("descricaoEquipamento", "TECLADO");
				tecladoWrapper.setValue("quantidadeEquipamento", new BigDecimal(1));
				PersistEngine.persist(noTeclado);
				processEntity.findField("listaEquipamento").addValue(noTeclado);
				
				//efetuando uma copia dos dados para a tabela de equipamentos recebidos
				addListaEquipamentosReceber(processEntity, "TECLADO", null);
				
				// 2 SIRENE
				NeoObject noSirene = AdapterUtils.createNewEntityInstance("FCNDescricaoEquipamento");
				EntityWrapper sireneWrapper = new EntityWrapper(noSirene);
				sireneWrapper.setValue("descricaoEquipamento", "SIRENE");
				sireneWrapper.setValue("quantidadeEquipamento", new BigDecimal(2));
				PersistEngine.persist(noSirene);
				processEntity.findField("listaEquipamento").addValue(noSirene);
				
				//efetuando uma copia dos dados para a tabela de equipamentos recebidos
				addListaEquipamentosReceber(processEntity, "SIRENE", null);
				
				// X SENSOR
				NeoObject noSensor = AdapterUtils.createNewEntityInstance("FCNDescricaoEquipamento");
				EntityWrapper sensorWrapper = new EntityWrapper(noSensor);
				sensorWrapper.setValue("descricaoEquipamento", "SENSOR");
				Collection<NeoObject> contasSigma = (Collection<NeoObject>) processEntity.findValue("listaContasSigma");
				Long qtdSensor = 0L;
				if (contasSigma != null && !contasSigma.isEmpty())
				{
					for (NeoObject contaSigma : contasSigma)
					{
						EntityWrapper contaSigmaWrapper = new EntityWrapper(contaSigma);
						Long cdCliente = (Long) contaSigmaWrapper.findValue("clienteSigma.cd_cliente");
						QLEqualsFilter filterConta = new QLEqualsFilter("cd_cliente", cdCliente);
						qtdSensor = qtdSensor + PersistEngine.countObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SIGMADBEVENTO")).getEntityClass(), filterConta);
						// Correção para tirar a zona da Sirena Pega ladrão
						qtdSensor = qtdSensor - 1;
					}
				}
				if (qtdSensor <= 0L)
				{
					qtdSensor = 5L;
				}
				sensorWrapper.setValue("quantidadeEquipamento", new BigDecimal(qtdSensor));
				PersistEngine.persist(noSensor);
				processEntity.findField("listaEquipamento").addValue(noSensor);
				
				//efetuando uma copia dos dados para a tabela de equipamentos recebidos
				addListaEquipamentosReceber(processEntity, "SENSOR", null);
			}
		}

	}
	
	private void addListaEquipamentosReceber(EntityWrapper processEntity, String descricaoEquipamento, NeoObject noProduto)
	{
		NeoObject noEquipamento = AdapterUtils.createNewEntityInstance("CEListaEquipamento");
		EntityWrapper wEquipamento = new EntityWrapper(noEquipamento);
		wEquipamento.setValue("codigoProduto", noProduto);
		wEquipamento.setValue("descricaoEquipamento", descricaoEquipamento);
		PersistEngine.persist(noEquipamento);
		
		processEntity.findField("equipReceb").addValue(noEquipamento);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
