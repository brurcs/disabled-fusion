package com.neomind.fusion.custom.orsegups.adapter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class GTCInsereTituloPagarSapiens implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(GTCInsereTituloPagarSapiens.class);

	private String urlstr = "";
	private String NOMUSU = "";
	private String SENUSU = "";
	private String PROXACAO = "";
	private String urlSID = "";

	private String errors;

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "";
		String nomeUsuario = "";
		String senhaUsuario = "";
		String nomeUsuarioAux = "";

		String SIS = "SIS=CO";
		String LOGIN = "&LOGIN=SID";
		String ACAO = "&ACAO=EXESENHA";
		StringBuffer inputLine = null;

		try
		{
			GregorianCalendar dataentrada = (GregorianCalendar) processEntity.findValue("dataEntrada");
			GregorianCalendar dataemissao = (GregorianCalendar) processEntity.findValue("dataEmissao");
			if (dataentrada.before(dataemissao))
			{
				mensagem = "Data de Entrada deve ser maior ou igual a Data de Emissão!";
				throw new WorkflowException(mensagem);
			}

			QLEqualsFilter filtroLogin = new QLEqualsFilter("nomeUsuario", PortalUtil.getCurrentUser().getCode().concat(".sid"));
			List<NeoObject> listLogin = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCLoginSapiens"), filtroLogin);
			if (listLogin != null)
			{
				EntityWrapper wrapperLogin = new EntityWrapper(listLogin.get(0));
				nomeUsuario = (String) wrapperLogin.findValue("nomeUsuario");
				nomeUsuarioAux = nomeUsuario.substring(0, nomeUsuario.length() - 4);

				if (nomeUsuarioAux.equals(PortalUtil.getCurrentUser().getCode()))
				{
					senhaUsuario = (String) wrapperLogin.findValue("senhaUsuario");
				}
				else
				{
					mensagem = "Por favor, acesse o Fusion com um Usuário correspondente no Sapiens!";
					throw new WorkflowException(mensagem);
				}
			}
			else
			{
				mensagem = "Não encontrado usuário para lançar o Título no Sapiens!";
				throw new WorkflowException(mensagem);
			}

			NeoObject noConexao = ((List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("conexaoSID"))).get(0);
			EntityWrapper ewConexao = new EntityWrapper(noConexao);
			this.NOMUSU = "&NOMUSU=" + nomeUsuario.trim();
			this.SENUSU = "&SENUSU=" + senhaUsuario.trim();
			this.urlstr = ewConexao.findField("host").getValue().toString().trim();

			Long codigoempresa = (Long) processEntity.findValue("empresa.codemp");
			Long codigofilial = (Long) processEntity.findValue("codigoFilial.codfil");
			String titulo = (String) processEntity.findValue("titulo");
			String tipotitulo = (String) processEntity.findValue("tipoTitulo.codtpt");
			Long codigofornecedor = (Long) processEntity.findValue("fornecedor.codfor");
			String transacao = (String) processEntity.findValue("transacao.codtns");
			GregorianCalendar vencimentooriginal = (GregorianCalendar) processEntity.findValue("vencimentoOriginal");
			GregorianCalendar competenciaBI = (GregorianCalendar) processEntity.findValue("competenciaBI");
			BigDecimal valororiginal = (BigDecimal) processEntity.findValue("valorOriginal");
			processEntity.setValue("vencimentoProrrogado", vencimentooriginal);
			GregorianCalendar vencimentoprorrogado = (GregorianCalendar) processEntity.findValue("vencimentoProrrogado");
			processEntity.setValue("dataProvavelPagamento", vencimentooriginal);
			GregorianCalendar dataprovavelpagamento = (GregorianCalendar) processEntity.findValue("dataProvavelPagamento");
			String codigomoeda = (String) processEntity.findValue("moeda.codmoe");
			String codigoportador = (String) processEntity.findValue("portador.codpor");
			String codigocarteira = (String) processEntity.findValue("carteira.codcrt");
			String observacao = (String) processEntity.findValue("observacao");
			Long formapagamento = (Long) processEntity.findValue("formaPagamento.codfpg");
			Long usuarioresponsavel = (Long) processEntity.findValue("usuarioResponsavel.codusu");
			String codigobanco = (String) processEntity.findValue("codigoBanco");
			String codigoagencia = (String) processEntity.findValue("codigoAgencia");
			String contabanco = (String) processEntity.findValue("contaBanco");
			Long contafinanceira = (Long) processEntity.findValue("contaFinanceira.ctafin");
			Long contacontabil = (Long) processEntity.findValue("contaContabil.ctared");
			Long naturezagasto = (Long) processEntity.findValue("naturezaGasto.codntg");

		

			urlSID = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU;
			PROXACAO = "&PROXACAO=SID.SRV.XML";
			String CodEmp = codigoempresa.toString();
			String CodFil = codigofilial.toString();
			String NumTit = titulo;
			String CodTpt = tipotitulo.toString();
			String CodFor = codigofornecedor.toString();
			String CodTns = transacao;
			String Usu_DatEnt = NeoUtils.safeDateFormat(competenciaBI, "dd/MM/yyyy");
			String DatEnt = NeoUtils.safeDateFormat(dataentrada, "dd/MM/yyyy");
			String DatEmi = NeoUtils.safeDateFormat(dataemissao, "dd/MM/yyyy");
			String VctOri = NeoUtils.safeDateFormat(vencimentooriginal, "dd/MM/yyyy");
			String VlrOri = valororiginal.toString();
			String VctPro = NeoUtils.safeDateFormat(vencimentoprorrogado, "dd/MM/yyyy");
			String ProPgt = NeoUtils.safeDateFormat(dataprovavelpagamento, "dd/MM/yyyy");
			String CodMoe = codigomoeda;
			String CodPor = codigoportador.toString();
			String CodCrt = codigocarteira.toString();
			String ObsTcp = observacao;
			String CodFpg = formapagamento.toString();
			String UsuRes = usuarioresponsavel.toString();
			String CodNtg = "";
			
			
			if (OrsegupsUtils.isWorkDay(vencimentooriginal) == false)
			{
				mensagem = "Data de vencimento do Titulo não é um dia útil!";
				throw new WorkflowException(mensagem);
			}
			
			
			if (naturezagasto != null)
			{
				CodNtg = naturezagasto.toString();
			}

			String CodBan = "";
			String CodAge = "";
			String CcbFor = "";
			if (formapagamento == 2L)
			{
				CodBan = codigobanco.toString();
				CodAge = codigoagencia.toString();
				CcbFor = contabanco.toString();
			}

			String CtaFin = contafinanceira.toString();
			String CtaRed = contacontabil.toString();

			String sidxml = "";
			sidxml = "<sidxml>";
			sidxml = sidxml + "<sid acao='SID.Srv.AltEmpFil'>";
			sidxml = sidxml + "<param nome='codemp' valor='" + CodEmp + "' />";
			sidxml = sidxml + "<param nome='codfil' valor='" + CodFil + "' />";
			sidxml = sidxml + "</sid>";
			sidxml = sidxml + "<sid acao='SID.Tcp.Gravar'>";
			sidxml = sidxml + "<param nome='numtit' valor='" + NumTit + "' />";
			sidxml = sidxml + "<param nome='codtpt' valor='" + CodTpt + "' />";
			sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";
			sidxml = sidxml + "<param nome='codtns' valor='" + CodTns + "' />";
			sidxml = sidxml + "<param nome='datent' valor='" + DatEnt + "' />";
			sidxml = sidxml + "<param nome='datemi' valor='" + DatEmi + "' />";
			sidxml = sidxml + "<param nome='vctori' valor='" + VctOri + "' />";
			sidxml = sidxml + "<param nome='vlrori' valor='" + VlrOri + "' />";
			sidxml = sidxml + "<param nome='vctpro' valor='" + VctPro + "' />";
			sidxml = sidxml + "<param nome='codmoe' valor='" + CodMoe + "' />";
			sidxml = sidxml + "<param nome='codpor' valor='" + CodPor + "' />";
			sidxml = sidxml + "<param nome='codcrt' valor='" + CodCrt + "' />";
			sidxml = sidxml + "<param nome='obstcp' valor='" + ObsTcp + "' />";
			sidxml = sidxml + "<param nome='codfpg' valor='" + CodFpg + "' />";
			sidxml = sidxml + "<param nome='codntg' valor='" + CodNtg + "' />";
			sidxml = sidxml + "<param nome='usu_datent' valor='" + Usu_DatEnt + "' />";
			sidxml = sidxml + "<param nome='usu_usures' valor='" + UsuRes + "' />";

			if (formapagamento == 2L)
			{
				sidxml = sidxml + "<param nome='codban' valor='" + CodBan + "' />";
				sidxml = sidxml + "<param nome='codage' valor='" + CodAge + "' />";
				sidxml = sidxml + "<param nome='ccbfor' valor='" + CcbFor + "' />";
			}

			sidxml = sidxml + "<param nome='ctared' valor='" + CtaRed + "' />";
			sidxml = sidxml + "<param nome='ctafin' valor='" + CtaFin + "' />";

			String codigocentrocusto;
			String CodCcu = "";

			sidxml = sidxml + "</sid>";
			sidxml = sidxml + "<sid acao='SID.Tcp.GravarRateio'>";
			sidxml = sidxml + "<param nome='numtit' valor='" + NumTit + "' />";
			sidxml = sidxml + "<param nome='codtpt' valor='" + CodTpt + "' />";
			sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";

			/* Ler a lista de centros de custos informadas no EFormDinâmico */
			List<NeoObject> listaCentroCustos = (List<NeoObject>) processEntity.findValue("listaCentroCusto");
			if (listaCentroCustos != null && !listaCentroCustos.isEmpty())
			{
				Integer seq = 1;
				for (NeoObject ccuDin : listaCentroCustos)
				{
					EntityWrapper wrpItem = new EntityWrapper(ccuDin);
					BigDecimal valorRateio = (BigDecimal) wrpItem.findValue("valorRateio");
					String valorRateioStr = valorRateio.toString();

					/* Ler o código do Centro de Custo informado no EForm Externo (E044CCU) */
					NeoObject ccuExt = (NeoObject) wrpItem.findValue("codigoCentroCusto");
					EntityWrapper wrpCcu = new EntityWrapper(ccuExt);
					String centroCustoStr = (String) wrpCcu.findValue("codccu");

					if (centroCustoStr != null && valorRateioStr != null)
					{
						sidxml = sidxml + "<param nome='ctafin" + seq.toString() + "' valor='" + CtaFin + "' />";
						sidxml = sidxml + "<param nome='ctared" + seq.toString() + "' valor='" + CtaRed + "' />";
						sidxml = sidxml + "<param nome='codccu" + seq.toString() + "' valor='" + centroCustoStr + "' />";
						sidxml = sidxml + "<param nome='percta" + seq.toString() + "' valor='" + 0 + "' />";
						sidxml = sidxml + "<param nome='vlrcta" + seq.toString() + "' valor='" + VlrOri + "' />";
						sidxml = sidxml + "<param nome='perrat" + seq.toString() + "' valor='" + 0 + "' />";
						sidxml = sidxml + "<param nome='vlrrat" + seq.toString() + "' valor='" + valorRateioStr + "' />";
						sidxml = sidxml + "<param nome='numprj" + seq.toString() + "' valor='" + 0 + "' />";
						sidxml = sidxml + "<param nome='codfpj" + seq.toString() + "' valor='" + 0 + "' />";
						sidxml = sidxml + "<param nome='obsrat" + seq.toString() + "' valor='" + "" + "' />";

						seq++;
					}
				}
			}
			else
			{
				codigocentrocusto = (String) processEntity.findValue("centroCusto.codccu");
				if (codigocentrocusto != null)
				{
					CodCcu = codigocentrocusto.toString();

					sidxml = sidxml + "<param nome='ctafin1' valor='" + CtaFin + "' />";
					sidxml = sidxml + "<param nome='ctared1' valor='" + CtaRed + "' />";
					sidxml = sidxml + "<param nome='codccu1' valor='" + CodCcu + "' />";
					sidxml = sidxml + "<param nome='percta1' valor='" + 0 + "' />";
					sidxml = sidxml + "<param nome='vlrcta1' valor='" + VlrOri + "' />";
					sidxml = sidxml + "<param nome='perrat1' valor='" + 0 + "' />";
					sidxml = sidxml + "<param nome='vlrrat1' valor='" + VlrOri + "' />";
					sidxml = sidxml + "<param nome='numprj1' valor='" + 0 + "' />";
					sidxml = sidxml + "<param nome='codfpj1' valor='" + 0 + "' />";
					sidxml = sidxml + "<param nome='obsrat1' valor='" + "" + "' />";
				}
				else
				{
					mensagem = "Não encontrado o Código do Centro de Custo para lançamento do Título";
					throw new WorkflowException(mensagem);
				}
			}

			sidxml = sidxml + "</sid>";
			sidxml = sidxml + "</sidxml>";

			urlSID = urlSID + PROXACAO;
			inputLine = this.callSapiensSIDViaPOST(urlSID, sidxml);
			mensagem = inputLine.toString();
			System.out.println("##" + this.getClass().getSimpleName() + ": " + mensagem);
			System.out.println("##" + this.getClass().getSimpleName() + " ([ "+sidxml+"])");

			if (!mensagem.equals("OK"))
			{
				throw new WorkflowException(mensagem);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
	}

	private StringBuffer callSapiensSIDViaPOST(String url, String xml)
	{
		StringBuffer inputLine = new StringBuffer();
		url = url.replaceAll(" ", "%20");
		try
		{
			URL murl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) murl.openConnection();

			//add cabeçalho request
			conn.setRequestMethod("POST");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0");
			conn.setRequestProperty("Accept-Language", "pt-BR,en;q=0.5");

			String urlParameters = "xml=" + xml;

			// Send post request
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			char letter;

			while ((letter = (char) in.read()) != '\uFFFF')
			{
				inputLine.append(letter);
			}

			in.close();

			log.warn("Retorno Sapiens: " + inputLine.toString());
		}
		catch (Exception e)
		{
			inputLine.append("ERRO: Problema de conexão com o Serviço Sapiens SID" + e.getStackTrace());
			e.printStackTrace();
			errors = "Problema de conexão com o Serviço Sapiens SID! Erro: " + inputLine + e.getStackTrace() + e.getMessage() + e.getCause();
			log.error(errors);
			throw new WorkflowException(errors);

		}
		return inputLine;
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
