/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;


public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getsapiens_Synccom_senior_g5_co_ger_cad_clientesPortAddress();

    public com.neomind.fusion.custom.orsegups.contract.clientws.cliente.Sapiens_Synccom_senior_g5_co_ger_cad_clientes getsapiens_Synccom_senior_g5_co_ger_cad_clientesPort() throws javax.xml.rpc.ServiceException;

    public com.neomind.fusion.custom.orsegups.contract.clientws.cliente.Sapiens_Synccom_senior_g5_co_ger_cad_clientes getsapiens_Synccom_senior_g5_co_ger_cad_clientesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    
}
