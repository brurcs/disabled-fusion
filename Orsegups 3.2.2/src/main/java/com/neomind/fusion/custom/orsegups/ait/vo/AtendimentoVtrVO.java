package com.neomind.fusion.custom.orsegups.ait.vo;

public class AtendimentoVtrVO
{
	private String cdViatura;
	private String nmViatura;
	private AtendimentoValorVO produtividade;
	private String nomeImagem;
	private String titulo;
	private Long cpf;

	public String getNomeImagem() {
	    return nomeImagem;
	}

	public void setNomeImagem(String nomeImagem) {
	    this.nomeImagem = nomeImagem;
	}

	public String getTitulo() {
	    return titulo;
	}

	public void setTitulo(String titulo) {
	    this.titulo = titulo;
	}

	public String getCdViatura()
	{
		return cdViatura;
	}

	public void setCdViatura(String cdViatura)
	{
		this.cdViatura = cdViatura;
	}

	public String getNmViatura()
	{
		return nmViatura;
	}

	public void setNmViatura(String nmViatura)
	{
		this.nmViatura = nmViatura;
	}

	public AtendimentoValorVO getProdutividade() {
	    return produtividade;
	}

	public void setProdutividade(AtendimentoValorVO produtividade) {
	    this.produtividade = produtividade;
	}

	public Long getCpf() {
	    return cpf;
	}

	public void setCpf(Long cpf) {
	    this.cpf = cpf;
	}

}
