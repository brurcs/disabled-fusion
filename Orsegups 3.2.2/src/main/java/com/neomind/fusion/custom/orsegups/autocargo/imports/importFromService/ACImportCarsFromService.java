package com.neomind.fusion.custom.orsegups.autocargo.imports.importFromService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.autocargo.beans.client.ACClient;
import com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.beans.ACVehicleImport;
import com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.beans.ACVehiclesImport;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class ACImportCarsFromService {

    private int totalPageNumber = 1;

    private List<ACVehicleImport> result = new ArrayList<ACVehicleImport>();

    public void runIntegration() {

	for (int i = 0; i < totalPageNumber; i++) {

  	    this.getResult(i);

	    try {
		Thread.sleep(1000);
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    }
	}

	for (ACVehicleImport ACVehicleImport : result) {
	    if (!this.isCarAccountAlreadyCreated(ACVehicleImport.getRegistration_plate())) {

		if (!this.isClientAlreadyCreated(ACVehicleImport.getClient_id())) {

		    ACClient c = this.getClientFromServer(ACVehicleImport.getClient_id());

		    this.insertClientAccount(c);

		}
		
		this.insertCarAccount(ACVehicleImport);

	    }
	}

	// this.addToDB(result);

    }

    private boolean insertCarAccount(ACVehicleImport v) {
	
	Connection conn = null;
	PreparedStatement pstm = null;
	

	StringBuilder sql = new StringBuilder();
	
	//					 1 	2 	3	 4	    5 	6		 7 	 8 	     9 		10	 11	12		13
	sql.append("INSERT INTO CELTEC_CARRO (ID_LEGADO ,PLACA ,RENAVAM ,DESCRICAO ,COR ,CHASSI ,ANO_FABRICACAO ,ANO_MODELO ,MARCA ,MODELO ,CATEGORIA ,COMBUSTIVEL,POSSUI_INCONSISTENCIA ) " );
	sql.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, v.getClient_id());
	    pstm.setString(2, v.getRegistration_plate() != null ? v.getRegistration_plate().toUpperCase() : "");
	    pstm.setString(3, "");
	    pstm.setString(4, v.getDescription() != null ? v.getDescription().toUpperCase() : "");
	    pstm.setString(5, v.getColor() != null ? v.getColor().toUpperCase() : "");
	    pstm.setString(6, "");
	    
	    String fabricacao = "0";
	    
	    if (v.getManufacturing_year() != null && !v.getManufacturing_year().isEmpty()){
		fabricacao = v.getManufacturing_year();
	    }
	    
	    pstm.setInt(7, Integer.parseInt(fabricacao));
	    
	    String modelo = "0";
	    
	    if (v.getModel_year() != null && !v.getModel_year().isEmpty()){
		modelo = v.getModel_year();
	    }
	    
	    pstm.setInt(8, Integer.parseInt(modelo));
	    pstm.setString(9, v.getBrand() != null ? v.getBrand().toUpperCase() : "");
	    pstm.setString(10, v.getModel() != null ? v.getModel().toUpperCase() : "");
	    pstm.setString(11, v.getCategory() != null ? v.getCategory().toUpperCase() : "");
	    pstm.setString(12, v.getFuel_type() != null ? v.getFuel_type().toUpperCase() : "");
	    pstm.setBoolean(13, false);
	    
	    int rs = pstm.executeUpdate();

	    if (rs > 0) {
		System.out.println("Inseriu carro para: "+v.getClient_name());
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

	return false;
    }

    private boolean insertClientAccount(ACClient c) {

	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();

	// 1 2 3 4 5 6 7 8 9 10 11
	sql.append(" INSERT INTO CELTEC_CLIENTE (ID_LEGADO, OPERADORA, NOME_CLIENTE, TELEFONE_CLIENTE, CEP_CLIENTE, RUA_CLIENTE, NUMERO_CLIENTE, COMPLEMENTO_CLIENTE, BAIRRO_CLIENTE, CIDADE_CLIENTE, ESTADO_CLIENTE, CONTATO) ");
	sql.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ");

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, c.getId());
	    pstm.setString(2, c.getOperator_name());
	    pstm.setString(3, c.getName());

	    // TODO ADICIONAR VALIDACAO PARA TELEFONE DO CLIENTE

	    String telefone = "";

	    if (c.getPhone_number() != null && !c.getPhone_number().isEmpty()) {
		telefone = c.getPhone_number().replace("/", ";");
	    }

	    pstm.setString(4, telefone);

	    String cep = "88103400";

	    if (c.getAddress_code() != null && !c.getAddress_code().isEmpty()) {
		cep = String.valueOf(c.getAddress_code());
	    }

	    pstm.setString(5, cep);

	    String rua = "";

	    if (c.getAddress_street() != null && !c.getAddress_street().isEmpty()) {
		rua = c.getAddress_street().toUpperCase();
	    }

	    pstm.setString(6, rua);

	    String numero = "";

	    if (c.getAddress_number() != null && !c.getAddress_number().isEmpty()) {
		numero = String.valueOf(c.getAddress_number()).toUpperCase();
	    }

	    pstm.setString(7, numero);

	    String complemento = "";

	    if (c.getAddress_complement() != null && !c.getAddress_complement().isEmpty()) {
		complemento = c.getAddress_complement().toUpperCase();
	    }

	    pstm.setString(8, complemento);

	    String bairro = "CENTRO";

	    if (c.getAddress_district() != null && !c.getAddress_district().isEmpty()) {
		bairro = c.getAddress_district().toUpperCase();
	    }

	    pstm.setString(9, bairro);

	    String cidade = "SÃO JOSÉ";

	    if (c.getAddress_city() != null && !c.getAddress_city().isEmpty()) {
		cidade = c.getAddress_city().toUpperCase();
	    }

	    pstm.setString(10, cidade);

	    String estado = "SC";

	    if (c.getAddress_state() != null && !c.getAddress_state().isEmpty()) {
		estado = c.getAddress_state().toUpperCase();
	    }

	    pstm.setString(11, estado);

	    String contato = "";

	    if (c.getResponsible() != null && !c.getResponsible().isEmpty()) {
		contato = c.getResponsible();
	    }

	    pstm.setString(12, contato);

	    int rs = pstm.executeUpdate();

	    if (rs > 0) {
		System.out.println("Inseriu cliente! "+ c.getName());
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

	return false;
    }

    private ACClient getClientFromServer(int clientID) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://www2.autocargo.com.br/api/integration/v1/clients/" + clientID);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Authorization", "Token token=\"115fe7a39c6d1910308c335ba3f845a0f9d6495ee912068b9cf7a90f717f03a5\"");
	    conn.setRequestProperty("Content-Type", "application/json");
	    conn.setRequestProperty("Accept", "application/json+detran");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
	    }

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {

	    e.printStackTrace();

	} catch (IOException e) {

	    e.printStackTrace();

	}

	System.out.println(retorno.toString());
	Gson gson = new Gson();
	ACClient result = gson.fromJson(retorno.toString(), ACClient.class);

	return result;
    }
 
    private boolean isCarAccountAlreadyCreated(String placa) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT 1 FROM dbCENTRAL WITH(NOLOCK) WHERE NM_RASTREADOR=? ");

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setString(1, placa);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return false;
    }

    private boolean isClientAlreadyCreated(int idLegado) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append("SELECT 1 FROM CELTEC_CLIENTE WHERE ID_LEGADO = ?");

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, idLegado);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return false;
    }

    private void getResult(int pageNumber) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://www2.autocargo.com.br/api/integration/v1/vehicles?page=" + pageNumber);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Authorization", "Token token=\"115fe7a39c6d1910308c335ba3f845a0f9d6495ee912068b9cf7a90f717f03a5\"");
	    conn.setRequestProperty("Content-Type", "application/json");
	    conn.setRequestProperty("Accept", "application/json+detran");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
	    }

	    String totalPorPagina = conn.getHeaderField("X-Autocargo-Per-Page");
	    String totalResultados = conn.getHeaderField("X-Autocargo-Total");

	    this.totalPageNumber = Integer.parseInt(totalResultados) / Integer.parseInt(totalPorPagina);

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {

	    e.printStackTrace();

	} catch (IOException e) {

	    e.printStackTrace();

	}

	System.out.println(retorno.toString());
	Gson gson = new Gson();
	ACVehiclesImport resultList = gson.fromJson(retorno.toString(), ACVehiclesImport.class);

	this.result.addAll(resultList.getVehicles());

    }

}
