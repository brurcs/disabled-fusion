package com.neomind.fusion.custom.orsegups.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SapiensUtilsServlet
 */
@WebServlet(name = "/SapiensUtilsServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.servlets.SapiensUtilsServlet" }) 
public class SapiensUtilsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SapiensUtilsServlet() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	this.doRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	this.doRequest(request, response);
    }

    private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/plain");
	response.setCharacterEncoding("ISO-8859-1");

	final PrintWriter out = response.getWriter();

	String action = "";
	
	if (request.getParameter("action") != null) {
	    action = request.getParameter("action");
	}
	
	if (action.equalsIgnoreCase("verificarEmail")) {
	    this.validarEmail(request, out);
	}	

    }

    
    /**
     * O nome escolhido deve respeitar as seguintes regras sintáticas:
     * Tamanho mínimo de 2 e máximo de 26 caracteres, não incluindo a categoria. Por exemplo: no domínio xxxx.com.br, esta limitação se refere ao xxxx; 
     * Caracteres válidos são letras de "a" a "z", números de "0" a "9", o hífen, e os seguintes caracteres acentuados: à, á, â, ã, é, ê, í, ó, ô, õ, ú, ü, ç 
     * Não conter somente números; 
     * Não iniciar ou terminar por hífen. 
     * Para fins de registro, verifica-se uma equivalência na comparação de nomes de domínio. Esta verificação é realizada convertendo-se os caracteres acentuados e o cedilha,
     * respectivamente, para suas versões não acentuadas e o "c", e descartando-se os hífens. O registro de um domínio não é permitido se houver domínio equivalente pertencente
     * a outro titular. 
     * 
     * @param request objeto HttpServletRequest  
     * @param out objeto PrintWriter que dá o retorno para o Sapiens
     * */
    private void validarEmail(HttpServletRequest request, PrintWriter out) {

		
	//String PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	String PATTERN = "[a-zA-Z0-9]+[a-zA-Z0-9_.-]+@{1}[a-zA-Z0-9_.-]*\\.+[a-z]{2,4}";
	
	final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		Pattern.compile(PATTERN, Pattern.CASE_INSENSITIVE);
	String emails = request.getParameter("emails");
	boolean valido = false;
	String retorno = "";
	String arrayEmails[] = emails.split(";");

	for(int i = 0; i < arrayEmails.length; i++){
	    if(!arrayEmails[i].equals(null)) {
		String email = arrayEmails[i];
		System.out.println(email);
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
		valido = matcher.find();
		System.out.println((valido == true ? "Valido" : "Invalido"));
		if(!valido) {
		    break;
		}		
	    }	 	    
	}
	
	retorno = (valido == true ? "Valido" : "Invalido");
	out.print(retorno);

    }

}
