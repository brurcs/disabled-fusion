package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class EmailDeliveryXXX5 implements CustomJobAdapter {

    private static final Log log = LogFactory.getLog(EmailDeliveryXXX5.class);
    public static final String EVT_XXX5 = "XXX5";

    @SuppressWarnings({ "unchecked", "static-access", "deprecation" })
    @Override
    public void execute(CustomJobContext arg0) {

	int adicionados = 0;
	log.warn("E-Mail XXX5 Inicio execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailAutomaticoDesvioDeHabito");
	InstantiableEntityInfo ExcecoesEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CadastroExcecoesEmailsDesvioDeHabito");
	InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailDelivery");

	Connection conn = null;
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	String ultimaExecucaoRotina = OrsegupsEmailUtils.ultimaExecucaoRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_XXX5);

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	try {
	    strSigma.append(" 	 SELECT c.OBSERVACAO, c.CGCCPF,c.FANTASIA, c.RAZAO, c.ID_CENTRAL, c.PARTICAO, c.ENDERECO, cid.NOME AS NM_CIDADE, bai.NOME AS NM_BAIRRO, h.DT_FECHAMENTO, c.EMAILRESP,");
	    strSigma.append(" 	 hfe.NM_FRASE_EVENTO as NM_FRASE_EVENTO, ");
	    strSigma.append(" 	 h.TX_OBSERVACAO_FECHAMENTO, ISNULL(ma.DS_MOTIVO_ALARME, 'N/A - SEM ALTERAÇÃO') as motivo, h.CD_HISTORICO_SEM_CONTROLE AS CD_HISTORICO, ");
	    strSigma.append(" 	 h.CD_EVENTO, c.CD_CLIENTE,  c.NU_LATITUDE, c.NU_LONGITUDE, h.DT_RECEBIDO, c.ID_EMPRESA ");
	    strSigma.append(" 	  FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK)    ");
	    strSigma.append(" 	 INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA   ");
	    strSigma.append(" 	 INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
	    strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
	    strSigma.append(" 	 LEFT JOIN MOTIVO_ALARME ma ON ma.CD_MOTIVO_ALARME = h.CD_MOTIVO_ALARME  ");
	    strSigma.append(" 	 LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE  ");
	    strSigma.append(" 	 LEFT JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO  ");
	    strSigma.append(" 	 WHERE  H.CD_EVENTO = 'XXX5' AND C.TP_PESSOA != 2 ");
	    strSigma.append(" 	 AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_EmailAutomaticoDesvioDeHabito em where em.historico = h.CD_HISTORICO_SEM_CONTROLE)  ");
	    strSigma.append("    AND (h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#AtualizarCadastro%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#NaoEnviaComunicado%')   ");
	    strSigma.append(" 	 AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
	    strSigma.append(" 	 AND CD_USUARIO_FECHAMENTO <> 9999     ");
	    strSigma.append(" 	 AND ( DT_RECEBIDO > '" + ultimaExecucaoRotina + "' ) ");

	    conn = PersistEngine.getConnection("SIGMA90");

	    pstm = conn.prepareStatement(strSigma.toString());

	     OrsegupsEmailUtils.inserirFimRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_XXX5);

	    rs = pstm.executeQuery();

	    String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
	    // String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");

	    while (rs.next()) {

		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));
		String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");
		if (!clienteComExcecao) {
		    String cgcCpf = rs.getString("CGCCPF") == null ? "" : rs.getString("CGCCPF").replaceAll("([.\\-/])", "").trim();
		    String fantasia = (rs.getString("FANTASIA") == null ? "" : rs.getString("FANTASIA"));
		    String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
		    String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
		    String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
		    String cidade = (rs.getString("NM_CIDADE") == null ? "" : rs.getString("NM_CIDADE"));
		    String bairro = (rs.getString("NM_BAIRRO") == null ? "" : rs.getString("NM_BAIRRO"));
		    Timestamp dtViaturaNoLocal = rs.getTimestamp("DT_FECHAMENTO");

		    String nomeFraseEvento = (rs.getString("NM_FRASE_EVENTO") == null ? "" : rs.getString("NM_FRASE_EVENTO"));
		    String txObsevacaoFechamento = (rs.getString("TX_OBSERVACAO_FECHAMENTO") == null ? "" : rs.getString("TX_OBSERVACAO_FECHAMENTO"));
		    String motivo = (rs.getString("motivo") == null ? "" : rs.getString("motivo"));
		    String evento = (rs.getString("CD_EVENTO") == null ? "" : rs.getString("CD_EVENTO"));
		    String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
		    String cdCliente = (rs.getString("CD_CLIENTE") == null ? "" : rs.getString("CD_CLIENTE"));
		    String dataAtendimento = NeoDateUtils.safeDateFormat(dtViaturaNoLocal, "dd/MM/yyyy");
		    String horaAtendimento = rs.getString("DT_FECHAMENTO").substring(11, 16);
		    String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));
		    Timestamp dtRecebido = rs.getTimestamp("DT_RECEBIDO");
		    int empresa = rs.getInt("ID_EMPRESA");
		    log.warn("[XXX5] fantasia:" + fantasia + ", razao:" + razao + ", email" + email);

		    System.out.println();
		    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
			String array[] = txObsevacaoFechamento.split("###");
			String textoFormatado = "";
			for (String string : array) {
			    textoFormatado += string + "<br/>";
			}

			if (!textoFormatado.isEmpty()) {
			    textoFormatado = textoFormatado.replaceAll("#", "");
			    textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			    textoFormatado = textoFormatado.replaceAll(";", "");
			    GregorianCalendar calendar = new GregorianCalendar();
			    calendar.setTime(dtRecebido);
			    List<String> dias = dias(calendar);

			    if (dias != null && !dias.isEmpty()) {
				for (String diaStr : dias) {
				    textoFormatado = textoFormatado.replaceAll(diaStr, "<br/><br/>" + diaStr);
				}

			    } else {
				textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
				// textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);
			    }

			    textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
			    textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
			    textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

			    textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
			    textoFormatado = textoFormatado.replaceAll("Ramal", "a partir do ramal ");

			    textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
			    textoFormatado = textoFormatado.replaceAll("SemContato", " Não houve sucesso em nenhuma tentativa de contato.");
			    textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
			    textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");

			    txObsevacaoFechamento = textoFormatado;
			}
		    }

		    boolean flagAtualizadoNAC = observacao.contains("#AC");

		    List<String> emailClie = OrsegupsEmailUtils.validarEmail(email, cdCliente, flagAtualizadoNAC);
		    if ((emailClie != null) && (!emailClie.isEmpty())) {
			log.warn("[XXX5] email do cliente validado");

			final String tipo = OrsegupsEmailUtils.TIPO_RAT_DESVIO_HABITO;

			Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

			String pasta = null;
			String remetente = null;
			String grupo = null;

			if (params != null) {

			    pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
			    remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
			    grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
			    String neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);
			    Long tipRat = 4L;
			    String ratingToken = DigestUtils.sha256Hex(cgcCpf + Long.toString(tipRat) + Long.toString(new Date().getTime()));
			    
			    for (String emailFor : emailClie) {
				log.warn("[XXX5] addTo: " + emailFor);
				StringBuilder noUserMsg = new StringBuilder();
				List<NeoObject> neoObjects = null;
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("email", emailFor));
				groupFilter.addFilter(new QLEqualsFilter("tratarXXX5", Boolean.TRUE));

				neoObjects = PersistEngine.getObjects(ExcecoesEmail.getEntityClass(), groupFilter);

				if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty())) {

				    noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, ratingToken));

				    noUserMsg.append("\n <table width=\"600\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr>");

				    noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td colspan=\"5\" style=\"padding-left:1%;padding-right:18%;\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Informamos o atendimento ao seguinte evento de seu sistema de segurança:</p>");
				    noUserMsg.append("\n </td> </tr> </br>");
				    noUserMsg.append("\n <tr> ");
				    noUserMsg
					    .append("\n <td colspan=\"5\" style=\"text-align: center;\"> <p style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;text-align:center;color:#007FFF;font-weight:bold;padding:10px;margin-top:0px;\">DESVIO DE HÁBITO: SISTEMA DESARMADO FORA DO HORÁRIO</p>");
				    noUserMsg.append("\n </td> </tr><br/>");

				    // Aviso ao cliente para atualização de contatos
				    if (txObsevacaoFechamento.contains("#SemContato")) {
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"6\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
					noUserMsg.append("\n </tr>");

					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"6\"> <b><div style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;color:red; text-align: center\">ATENÇÃO: Aviso Importante</b></div></td>");
					noUserMsg.append("\n </tr>");

					noUserMsg.append("\n <tr> ");
					noUserMsg.append("\n <td colspan=\"6\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
					noUserMsg.append("\n </tr>");

					noUserMsg.append("\n <tr> ");
					noUserMsg.append("\n <td colspan=\"6\">");
					noUserMsg.append("\n <div style=\"font-family: 'Verdana';font-weight:normal;font-size:16px;color:#787878; text-align: justify\"></br>");
					noUserMsg.append("Informamos que em <strong>" + dataAtendimento + "&nbsp;" + horaAtendimento + " </strong> nossa Central de ");
					noUserMsg.append("Monitoramento 24h identificou que seu sistema de segurança foi <strong> desativado fora do horário programado.</strong></div>");

					noUserMsg.append("\n <div style=\"font-family: 'Verdana';font-weight:normal;font-size:16px;color:#787878; text-align: justify\"></br>");
					noUserMsg.append("Conforme pode-se constatar no relatório abaixo, durante este atendimento, nossos operadores tentaram realizar contato telefônico com as pessoas ");
					noUserMsg.append("indicadas no cadastro para verificar necessidade de auxílio com o sistema, <strong> não houve sucesso em nenhuma tentativa. </strong></div>");

					noUserMsg.append("\n <div style=\"font-family: 'Verdana';font-weight:normal;font-size:16px;color:#787878; text-align: justify\"></br>");
					noUserMsg.append("<strong>Lembre-se:</strong> é imprescindível que seus contatos emergenciais estejam atualizados.</div>");

					noUserMsg.append("\n <div style=\"font-family: 'Verdana';font-weight:normal;font-size:16px;color:#787878; text-align: justify\"></br>");
					noUserMsg.append("Como o sistema estava desativado com a senha autorizada, o evento foi encerrado por nosso operadores.</div></br>");

					noUserMsg.append("\n </td>");
					noUserMsg.append("\n </tr> ");
				    }

				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr> ");
				    noUserMsg
					    .append("\n <td colspan=\"6\" style=\"padding:5px;\"><h2 style=\"border-bottom:1px solid #CCC;margin:0px 15px 0px 5px;padding-bottom:10px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">DETALHES SOBRE O ATENDIMENTO</td>");
				    noUserMsg.append("\n </tr>");
				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"6\">");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + txObsevacaoFechamento + "</p></td>");
					noUserMsg.append("\n </tr><br/>");
				    }
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td colspan=\"4\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:14px;\">");
				    noUserMsg.append("\n <img src=\"https://maps.googleapis.com/maps/api/staticmap?center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
					    + "&zoom=19&size=300x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
					    + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "\" width=\"300\" height=\"300\" alt=\"\"/></p><p style=\"font-family: 'Verdana';font-weight:normal;font-size:8px;width: 300px;\"></p></td>");
				    noUserMsg.append("\n <td width=\"10%\"><p><br>");
				    noUserMsg.append("\n </p></td>");
				    noUserMsg.append("\n <td width=\"51%\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Local </br> <strong>" + razao + "</strong></br>");
				    noUserMsg.append("\n <strong>" + endereco + "</strong></br>");
				    noUserMsg.append("\n <strong>" + bairro + "</strong></br>");
				    noUserMsg.append("\n <strong>" + cidade + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Data <br> <strong>" + dataAtendimento + "</strong></br>");
				    noUserMsg.append("\n Hora  </br> <strong>" + horaAtendimento + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Evento </br> ");
				    noUserMsg.append("\n <strong>" + nomeFraseEvento + "</strong></p> ");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n </tr>");

				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n </table>");
				    noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, email, remetente, ratingToken));

				    NeoObject emailHis = infoHis.createNewInstance();
				    EntityWrapper emailHisWp = new EntityWrapper(emailHis);
				    adicionados = adicionados + 1;
				    emailHisWp.findField("fantasia").setValue(fantasia);
				    emailHisWp.findField("razao").setValue(razao);
				    emailHisWp.findField("particao").setValue(particao);
				    emailHisWp.findField("endereco").setValue(endereco);
				    emailHisWp.findField("cidade").setValue(cidade);
				    emailHisWp.findField("bairro").setValue(bairro);
				    emailHisWp.findField("dataFechamento").setValue(dataAtendimento);
				    emailHisWp.findField("horaFechamento").setValue(horaAtendimento);
				    emailHisWp.findField("motivoAlarme").setValue(motivo);
				    emailHisWp.findField("evento").setValue(evento);
				    emailHisWp.findField("enviadoPara").setValue(emailClie.toString());
				    emailHisWp.findField("nomeFraseEvento").setValue(nomeFraseEvento);
				    emailHisWp.findField("historico").setValue(historico);
				    PersistEngine.persist(emailHis);

				    
				    // TESTE E-MAIL UTILIZANDO RECURSO FUSION
//				    String subject = "Relatório de Atendimento XXX5 - " + fantasia;
//				    OrsegupsEmailUtils.sendTestEmail(noUserMsg, subject);

				    GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
				    NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
				    EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
				    emailEnvioWp.findField("de").setValue("cm.rat" + remetente);
				    emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br;copia@orsegups.com.br");
				    emailEnvioWp.findField("assunto").setValue("Relatório de Atendimento XXX5 - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
				    emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
				    emailEnvioWp.findField("datCad").setValue(dataCad);
				    PersistEngine.persist(emaiEnvio);

				    log.warn("Cliente [XXX5]: " + fantasia + ", E-mail: " + emailFor);
				    // Regra para gerar os pushs
				    ObjRatMobile objRat = new ObjRatMobile();
				    objRat.setTipRat(tipRat);
				    objRat.setRatingToken(ratingToken);
				    objRat.setInformativo("Informamos o atendimento ao seguinte evento de seu sistema de segurança:");
				    objRat.setEvento(nomeFraseEvento);
				    objRat.setHashId("Relatório de Atendimento XXX5 - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
				    objRat.setResultadoDoAtendimento("DESVIO DE HÁBITO: SISTEMA DESARMADO FORA DO HORÁRIO");
				    objRat.setAtendidoPor("");
				    objRat.setLocal(razao + " - " + endereco + " - " + bairro + " - " + cidade);
				    objRat.setDataAtendimento(dataAtendimento);
				    objRat.setHoraAtendimento(horaAtendimento);

				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					objRat.setObservacao(txObsevacaoFechamento);
				    } else {
					objRat.setObservacao("");
				    }
				    objRat.setResultado("");
				    objRat.setEmpRat(grupo);
				    objRat.setNeoId(neoId);
				    objRat.setLnkFotoAit("");
				    objRat.setLnkFotoLocal("https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyDie-lwDjXfRv0diccYyQtC8ZuGt-P4scs&center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
					    + "&zoom=19&size=300x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
					    + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE"));
				    objRat.setCdCliente(cdCliente);

				    // Montando informações para serviço Mobile - Fim

				    if (!cgcCpf.equals("")) {
					objRat.setCgcCpf(Long.parseLong(cgcCpf));
					IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();
					integracao.inserirInformacoesPush(objRat);
				    }
				}
			    }
			    OrsegupsUtils.sendEmailWhatsAppRAT(Long.valueOf(cdCliente), "Relatório de Atendimento XXX5 - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento, endereco + " - " + bairro + " - " + cidade, dataAtendimento + " " + horaAtendimento);
			} else {
			    if (!empresasNotificadas.contains(empresa)) {
				OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
				empresasNotificadas.add(empresa);
			    }
			}

		    }
		}
	    }
	    log.warn("E-Mail XXX5 Fim execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	} catch (Exception e) {
	    log.error("E-Mail XXX5 erro no processamento:");
	    System.out.println("[" + key + "] E-Mail XXX5 erro no processamento: " + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }
    
    public static List<String> dias(GregorianCalendar calendar) {
	List<String> cptList = null;
	if (NeoUtils.safeIsNotNull(calendar)) {
	    long milisecInicial = calendar.getTime().getTime();
	    long milisecFinal = new GregorianCalendar().getTime().getTime();
	    long dif = milisecFinal - milisecInicial;

	    long dias = (((dif / 1000) / 60) / 60) / 24;
	    cptList = new ArrayList<String>();
	    Calendar dia = Calendar.getInstance();
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
	    for (int i = 0; i <= dias; i++) {
		String diaStr = dateFormat.format(dia.getTime());
		cptList.add(diaStr);
		dia.add(Calendar.DAY_OF_MONTH, -1);
	    }

	}
	return cptList;
    }
}