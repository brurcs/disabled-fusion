/**
 * G5SeniorServicesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano;

public class G5SeniorServicesLocator extends org.apache.axis.client.Service implements com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.G5SeniorServices {

    public G5SeniorServicesLocator() {
    }


    public G5SeniorServicesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public G5SeniorServicesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort
    //private java.lang.String sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort_address = "http://jurere:8080/g5-senior-services/sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano";
    private java.lang.String sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort_address = "http://ssooax02:8080/g5-senior-services/sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano";

    public java.lang.String getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortAddress() {
        return sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortWSDDServiceName = "sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort";

    public java.lang.String getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortWSDDServiceName() {
        return sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortWSDDServiceName;
    }

    public void setsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortWSDDServiceName(java.lang.String name) {
        sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortWSDDServiceName = name;
    }

    public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort(endpoint);
    }

    public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortBindingStub _stub = new com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortBindingStub(portAddress, this);
            _stub.setPortName(getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortEndpointAddress(java.lang.String address) {
        sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano.class.isAssignableFrom(serviceEndpointInterface)) {
                com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortBindingStub _stub = new com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortBindingStub(new java.net.URL(sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort_address), this);
                _stub.setPortName(getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort".equals(inputPortName)) {
            return getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.senior.com.br", "g5-senior-services");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort".equals(portName)) {
            setsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
