package com.neomind.fusion.custom.orsegups.justificativaAfastamento.vo;

/**
 * Modelo da entidade de historico médico do fluxo de afastamentos - J003
 * @author mateus.batista
 *
 */
public class JAHistoricoMedicoBean extends JAHistoricoBean{
    
    private String nome;
    private String crm;
    private String codCid;
    private String desCid;
    private String nomFun;
    private long numCad;
    private long numEmp;
    
    public long getNumCad() {
        return numCad;
    }
    public void setNumCad(long numCad) {
        this.numCad = numCad;
    }
    public long getNumEmp() {
        return numEmp;
    }
    public void setNumEmp(long numEmp) {
        this.numEmp = numEmp;
    }
    private String link;
    
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCrm() {
        return crm;
    }
    public void setCrm(String crm) {
        this.crm = crm;
    }
    public String getCodCid() {
        return codCid;
    }
    public void setCodCid(String codCid) {
        this.codCid = codCid;
    }
    public String getDesCid() {
        return desCid;
    }
    public void setDesCid(String desCid) {
        this.desCid = desCid;
    }
    public String getNomFun() {
        return nomFun;
    }
    public void setNomFun(String nomFun) {
        this.nomFun = nomFun;
    }

    
    public JAHistoricoMedicoBean(){
	super();
    }
    
}
