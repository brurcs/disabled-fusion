package com.neomind.fusion.custom.orsegups.maps.comparator;

import java.util.Comparator;

import com.neomind.fusion.custom.orsegups.maps.vo.EventoEsperaVO;

public class EventoFilaEsperaComparator implements Comparator<EventoEsperaVO> {

    @Override
    public int compare(EventoEsperaVO o1, EventoEsperaVO o2) {
	return o1.getCodigo().compareTo(o2.getCodigo());
    }

}
