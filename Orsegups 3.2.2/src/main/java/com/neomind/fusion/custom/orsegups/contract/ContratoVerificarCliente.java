package com.neomind.fusion.custom.orsegups.contract;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.util.NeoUtils;

public class ContratoVerificarCliente implements AdapterInterface {
	
	protected class ClienteDTO{
		private Long codCli;
		private String nomCli;
		private Long cgcCpf;
		private String tipCli;
		
		public ClienteDTO() {
			// TODO Auto-generated constructor stub
		}

		public Long getCodCli() {
			return codCli;
		}

		public void setCodCli(Long codCli) {
			this.codCli = codCli;
		}

		public String getNomCli() {
			return nomCli;
		}

		public void setNomCli(String nomCli) {
			this.nomCli = nomCli;
		}

		public Long getCgcCpf() {
			return cgcCpf;
		}

		public void setCgcCpf(Long cgcCpf) {
			this.cgcCpf = cgcCpf;
		}

		public String getTipCli() {
			return tipCli;
		}

		public void setTipCli(String tipCli) {
			this.tipCli = tipCli;
		}
		
		
	}
	
	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity) {
		//com.neomind.fusion.custom.orsegups.contract.ContratoVerificarCliente
		
		Long codCli = null;
		String nomCli = null;
		String cgccpf = null;
		String tipCli = null;
		
		String resumoClienteConsultado = "";
		
		String cpfcnpj = NeoUtils.safeOutputString(wrapper.findValue("cgccpfConsulta"));
		cpfcnpj = cpfcnpj.replaceAll("[./-]", "");
		System.out.println("[FLUXO CONTRATOS]-Procurando CPF "+cpfcnpj);
		
		/*QLGroupFilter filterUser = new QLGroupFilter("AND");
		filterUser.addFilter(new QLEqualsFilter("cgccpf", Long.parseLong(cpfcnpj) ));
		NeoObject oCliente = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterUser );*/
		
		ClienteDTO cli = null;
		if (!cpfcnpj.equals("")){
			cli = this.findCliente(Long.parseLong(cpfcnpj));
		}
		
		
		if (cli != null ){
			codCli = cli.getCodCli(); //(Long)wCliente.findValue("codcli");
			nomCli = cli.getNomCli(); //(String)wCliente.findValue("nomcli");
			cgccpf = String.valueOf(cli.getCgcCpf()); //NeoUtils.safeOutputString( (Long) wCliente.findValue("cgccpf") );
			tipCli = cli.getTipCli(); //wCliente.findValue("tipcli");
			
			resumoClienteConsultado = "Cliente existente : cpf/cnpj " + cgccpf + " - " + nomCli;
			wrapper.setValue("resumoClienteConsultado", resumoClienteConsultado);
			wrapper.setValue("filtroMovimentoContrato", "1,5"); // tratar no neoql do posto como codTipo in (${parent.filtroMovimentoContrato}) 
			
			// não é possível fazer pois, mesmo estando preenchido, a tela não econtra o código do cliente via js.
			ExternalEntityInfo infoClienteSapiens = (ExternalEntityInfo)EntityRegister.getInstance().getCache().getByString("SAPIENS_Clientes");
			QLEqualsFilter filterCodCli = new QLEqualsFilter("codcli", codCli);
			
			ArrayList<NeoObject> noClienteSapiens = (ArrayList<NeoObject>) PersistEngine.getObjects(infoClienteSapiens.getEntityClass(), filterCodCli);
			if (noClienteSapiens != null && !noClienteSapiens.isEmpty()){
				ContratoLogUtils.logInfo("setou");
				wrapper.setValue("buscaNomeCliente", noClienteSapiens.get(0));
				
				EntityWrapper wCliente = new EntityWrapper(noClienteSapiens.get(0));
				
				NeoObject noDadosGerais = (NeoObject) wrapper.findValue("dadosGeraisContrato");
				if (noDadosGerais == null)
					noDadosGerais = AdapterUtils.createNewEntityInstance("FCGContratoDadosGeraisSapiens");
				
				EntityWrapper wDadosGerais = new EntityWrapper(noDadosGerais);
				wDadosGerais.setValue("responsavelContrato", NeoUtils.safeOutputString(wCliente.findValue("nomcli")));
				
				PersistEngine.persist(noDadosGerais);
				wrapper.setValue("dadosGeraisContrato", noDadosGerais);
			}
			
		}else{
			resumoClienteConsultado = "Cliente Novo";
			wrapper.setValue("resumoClienteConsultado", resumoClienteConsultado);
			wrapper.setValue("filtroMovimentoContrato", "3,6");
			//wrapper.setValue("buscaNomeCliente", (NeoObject) null);
		}
		//PersistEngine.persist(wrapper.getObject());
		//PersistEngine.getEntityManager().flush();
		
		
		/*if (wrapper.findValue("buscaNomeCliente2.nomcli") != null){ // caso o cliente exista
			codCli = (Long)wrapper.findValue("buscaNomeCliente2.codcli");
			nomCli = (String) wrapper.findValue("buscaNomeCliente2.nomcli");
			cgccpf = NeoUtils.safeOutputString( (Long) wrapper.findValue("buscaNomeCliente2.cgccpf") );
			tipCli = (String) wrapper.findValue("buscaNomeCliente2.tipcli");
			
			resumoClienteConsultado = "Cliente existente : cpf/cnpj " + cgccpf + " - " + nomCli;
			wrapper.setValue("resumoClienteConsultado", resumoClienteConsultado);
			
		}else{ // caso o cliente não exista
			resumoClienteConsultado = "Cliente Novo";
			wrapper.setValue("resumoClienteConsultado", resumoClienteConsultado);
		}*/
		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}
	
	
	
	public ClienteDTO findCliente(Long cgcCpf){
		ClienteDTO retorno = new ClienteDTO();
		String  query = "select codcli, nomcli, cgccpf, tipcli from e085cli where cgccpf = "+cgcCpf ; 
		ResultSet rs = OrsegupsContratoUtils.getResultSet(query, "SAPIENS");
		try {
			if (rs != null){
					retorno.setCgcCpf(cgcCpf);
					retorno.setCodCli(rs.getLong(1));
					retorno.setNomCli(rs.getString(2));
					retorno.setTipCli(rs.getString(4));
					rs.close();
					rs = null;
			}else{
				retorno = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return retorno;
		
	}

}
