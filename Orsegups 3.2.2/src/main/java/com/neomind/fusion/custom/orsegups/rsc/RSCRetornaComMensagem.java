package com.neomind.fusion.custom.orsegups.rsc;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCRetornaComMensagem  implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	
	throw new WorkflowException("Esta tarefa escalou não podendo ser solicitado ajuste de prazo.");	
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub
	
    }

}
