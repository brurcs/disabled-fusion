package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class AnotacaoVO
{
	private ColaboradorVO colaborador;
	private GregorianCalendar data;
	private String descricao;
	private String tipoAnotacao;
	
	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}
	public String getDescricao()
	{
		return descricao;
	}
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	public String getTipoAnotacao()
	{
		return tipoAnotacao;
	}
	public void setTipoAnotacao(String tipoAnotacao)
	{
		this.tipoAnotacao = tipoAnotacao;
	}
	public GregorianCalendar getData()
	{
		return data;
	}
	public void setData(GregorianCalendar data)
	{
		this.data = data;
	}
}
