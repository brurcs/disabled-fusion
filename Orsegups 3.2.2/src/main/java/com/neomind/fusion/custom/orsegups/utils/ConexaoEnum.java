package com.neomind.fusion.custom.orsegups.utils;

public enum ConexaoEnum {
  FUSION(1,"FUSIONPROD"),SAPIENS(2,"SAPIENS"),SIGMA90(3,"SIGMA90"),VETORH(4,"VETORH");
  
  private int codigo;
  private String descricao;
  ConexaoEnum(int codigo, String descricao){
	  this.codigo = codigo;
	  this.descricao = descricao;
  }
public int getCodigo() {
	return codigo;
}
public String getDescricao() {
	return descricao;
}
  
  
}
