package com.neomind.fusion.custom.orsegups.callcenter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

public class CallCenterVO
{
	private Integer id;
	
	//DADOS GERAIS DO CLIENTE
	private Integer codigo;
	private String razaoSocial;
	private String nomeFantasia;
	private BigInteger cnpjCpf;
	private String tipoCliente;
	private String tipoContrato;

	// DADOS ENDEREÇO DO CLIENTE
	private String enderecoCliente;
	private String complementoCliente;
	private String bairroClient;
	private Integer cepCliente;
	private String cidadeCliente;
	private String ufCliente;
	
	//DADOS ENDEREÇO DE COBRANÇA
	private String enderecoCobranca;
	private String numeroCobranca;
	private String complementoCobranca;
	private Integer cepCobranca;
	private String cidadeCobranca;
	private String ufCobranca;
	
	//CONTATOS - TELEFONES/E-MAIL
	private String telefone1;
	private String telefone2;
	private String telefone3;
	private String telefone4;
	private String telefone5;
	private String foneFax;
	private String foneContrato;
	private String emailCliente;
	private String emailContrato;
	private String pessoaContato;
	
	//DADOS DO CONTRATO
	private Integer numeroContrato;
	private String numeroOficialContrato;
	private String regionalContrato;
	private String situacaoContrato;
	private Integer dataBaseContrato;
	private String formPagamentoContrato;
	private Integer vencimento;
	private String inicioVigenciaContrato;
	private String fimVigenciaContrato;
	private String inicioFaturamentoContrato;
	private String fimFaturamentoContrato;
	private Integer codigoMotivoInativacao;
	private String motivoInativacao;
	private String dataInativacao;
	private String codigoServico;
	
	//TREEGRID DATA
	private String treeLabel;
	private String treeDetail;
	private Boolean isContrato;
	
	private boolean possuiRSC;
	private boolean possuiRRC;
	
	private Collection<CallCenterVO> children;
	private String senhaSite;

	public Integer getCodigo()
	{
		return codigo;
	}

	public void setCodigo(Integer codigo)
	{
		this.codigo = codigo;
	}

	public String getRazaoSocial()
	{
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial)
	{
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia()
	{
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia)
	{
		this.nomeFantasia = nomeFantasia;
	}

	public BigInteger getCnpjCpf()
	{
		return cnpjCpf;
	}

	public void setCnpjCpf(BigInteger cnpjCpf)
	{
		this.cnpjCpf = cnpjCpf;
	}

	public String getTipoCliente()
	{
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente)
	{
		this.tipoCliente = tipoCliente;
	}

	public String getTipoContrato()
	{
		return tipoContrato;
	}

	public void setTipoContrato(String tipoContrato)
	{
		this.tipoContrato = tipoContrato;
	}

	public String getEnderecoCliente()
	{
		return enderecoCliente;
	}

	public void setEnderecoCliente(String enderecoCliente)
	{
		this.enderecoCliente = enderecoCliente;
	}

	public String getComplementoCliente()
	{
		return complementoCliente;
	}

	public void setComplementoCliente(String complementoCliente)
	{
		this.complementoCliente = complementoCliente;
	}

	public String getBairroClient()
	{
		return bairroClient;
	}

	public void setBairroClient(String bairroClient)
	{
		this.bairroClient = bairroClient;
	}

	public Integer getCepCliente()
	{
		return cepCliente;
	}

	public void setCepCliente(Integer cepCliente)
	{
		this.cepCliente = cepCliente;
	}

	public String getCidadeCliente()
	{
		return cidadeCliente;
	}

	public void setCidadeCliente(String cidadeCliente)
	{
		this.cidadeCliente = cidadeCliente;
	}

	public String getUfCliente()
	{
		return ufCliente;
	}

	public void setUfCliente(String ufCliente)
	{
		this.ufCliente = ufCliente;
	}

	public String getEnderecoCobranca()
	{
		return enderecoCobranca;
	}

	public void setEnderecoCobranca(String enderecoCobranca)
	{
		this.enderecoCobranca = enderecoCobranca;
	}

	public String getNumeroCobranca()
	{
		return numeroCobranca;
	}

	public void setNumeroCobranca(String numeroCobranca)
	{
		this.numeroCobranca = numeroCobranca;
	}

	public String getComplementoCobranca()
	{
		return complementoCobranca;
	}

	public void setComplementoCobranca(String complementoCobranca)
	{
		this.complementoCobranca = complementoCobranca;
	}

	public Integer getCepCobranca()
	{
		return cepCobranca;
	}

	public void setCepCobranca(Integer cepCobranca)
	{
		this.cepCobranca = cepCobranca;
	}

	public String getUfCobranca()
	{
		return ufCobranca;
	}

	public void setUfCobranca(String ufCobranca)
	{
		this.ufCobranca = ufCobranca;
	}

	public String getTelefone1()
	{
		return telefone1;
	}

	public void setTelefone1(String telefone1)
	{
		this.telefone1 = telefone1;
	}

	public String getTelefone2()
	{
		return telefone2;
	}

	public void setTelefone2(String telefone2)
	{
		this.telefone2 = telefone2;
	}

	public String getTelefone3()
	{
		return telefone3;
	}

	public void setTelefone3(String telefone3)
	{
		this.telefone3 = telefone3;
	}

	public String getTelefone4()
	{
		return telefone4;
	}

	public void setTelefone4(String telefone4)
	{
		this.telefone4 = telefone4;
	}

	public String getTelefone5()
	{
		return telefone5;
	}

	public void setTelefone5(String telefone5)
	{
		this.telefone5 = telefone5;
	}

	public String getFoneFax()
	{
		return foneFax;
	}

	public void setFoneFax(String foneFax)
	{
		this.foneFax = foneFax;
	}

	public String getEmailCliente()
	{
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente)
	{
		this.emailCliente = emailCliente;
	}

	public String getEmailContrato()
	{
		return emailContrato;
	}

	public void setEmailContrato(String emailContrato)
	{
		this.emailContrato = emailContrato;
	}

	public String getPessoaContato()
	{
		return pessoaContato;
	}

	public void setPessoaContato(String pessoaContato)
	{
		this.pessoaContato = pessoaContato;
	}

	public Integer getNumeroContrato()
	{
		return numeroContrato;
	}

	public void setNumeroContrato(Integer numeroContrato)
	{
		this.numeroContrato = numeroContrato;
	}

	public String getNumeroOficialContrato()
	{
		return numeroOficialContrato;
	}

	public void setNumeroOficialContrato(String numeroOficialContrato)
	{
		this.numeroOficialContrato = numeroOficialContrato;
	}

	public String getRegionalContrato()
	{
		return regionalContrato;
	}

	public void setRegionalContrato(String regionalContrato)
	{
		this.regionalContrato = regionalContrato;
	}

	public String getSituacaoContrato()
	{
		return situacaoContrato;
	}

	public void setSituacaoContrato(String situacaoContrato)
	{
		this.situacaoContrato = situacaoContrato;
	}

	public Integer getDataBaseContrato()
	{
		return dataBaseContrato;
	}

	public void setDataBaseContrato(Integer dataBaseContrato)
	{
		this.dataBaseContrato = dataBaseContrato;
	}

	public String getFormPagamentoContrato()
	{
		return formPagamentoContrato;
	}

	public void setFormPagamentoContrato(String formPagamentoContrato)
	{
		this.formPagamentoContrato = formPagamentoContrato;
	}

	public Integer getVencimento()
	{
		return vencimento;
	}

	public void setVencimento(Integer vencimento)
	{
		this.vencimento = vencimento;
	}

	public String getInicioVigenciaContrato()
	{
		return inicioVigenciaContrato;
	}

	public void setInicioVigenciaContrato(String inicioVigenciaContrato)
	{
		this.inicioVigenciaContrato = inicioVigenciaContrato;
	}

	public String getFimVigenciaContrato()
	{
		return fimVigenciaContrato;
	}

	public void setFimVigenciaContrato(String fimVigenciaContrato)
	{
		this.fimVigenciaContrato = fimVigenciaContrato;
	}

	public String getInicioFaturamentoContrato()
	{
		return inicioFaturamentoContrato;
	}

	public void setInicioFaturamentoContrato(String inicioFaturamentoContrato)
	{
		this.inicioFaturamentoContrato = inicioFaturamentoContrato;
	}

	public String getFimFaturamentoContrato()
	{
		return fimFaturamentoContrato;
	}

	public void setFimFaturamentoContrato(String fimFaturamentoContrato)
	{
		this.fimFaturamentoContrato = fimFaturamentoContrato;
	}

	public Integer getCodigoMotivoInativacao()
	{
		return codigoMotivoInativacao;
	}

	public void setCodigoMotivoInativacao(Integer codigoMotivoInativacao)
	{
		this.codigoMotivoInativacao = codigoMotivoInativacao;
	}

	public String getMotivoInativacao()
	{
		return motivoInativacao;
	}

	public void setMotivoInativacao(String motivoInativacao)
	{
		this.motivoInativacao = motivoInativacao;
	}

	public String getDataInativacao()
	{
		return dataInativacao;
	}

	public void setDataInativacao(String dataInativacao)
	{
		this.dataInativacao = dataInativacao;
	}

	public Collection<CallCenterVO> getChildren()
	{
		return children;
	}

	public void setChildren(Collection<CallCenterVO> children)
	{
		this.children = children;
	}

	public String getFoneContrato()
	{
		return foneContrato;
	}

	public void setFoneContrato(String foneContrato)
	{
		this.foneContrato = foneContrato;
	}
	
	public void addChild(CallCenterVO child)
	{
		if(this.children == null)
		{
			this.children = new ArrayList<CallCenterVO>();
		}
		
		this.children.add(child);
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getTreeLabel()
	{
		return treeLabel;
	}

	public void setTreeLabel(String treeLabel)
	{
		this.treeLabel = treeLabel;
	}

	public String getTreeDetail()
	{
		return treeDetail;
	}

	public void setTreeDetail(String treeDetail)
	{
		this.treeDetail = treeDetail;
	}

	public Boolean getIsContrato()
	{
		return isContrato;
	}

	public void setIsContrato(Boolean isContrato)
	{
		this.isContrato = isContrato;
	}

	public String getCodigoServico()
	{
		return codigoServico;
	}

	public void setCodigoServico(String codigoServico)
	{
		this.codigoServico = codigoServico;
	}


	public String getCidadeCobranca()
	{
		return cidadeCobranca;
	}

	public void setCidadeCobranca(String cidadeCobranca)
	{
		this.cidadeCobranca = cidadeCobranca;
	}

	public boolean isPossuiRSC()
	{
		return possuiRSC;
	}

	public void setPossuiRSC(boolean possuiRSC)
	{
		this.possuiRSC = possuiRSC;
	}

	public boolean isPossuiRRC()
	{
		return possuiRRC;
	}

	public void setPossuiRRC(boolean possuiRRC)
	{
		this.possuiRRC = possuiRRC;
	}

	public String getSenhaSite() {
		return senhaSite;
	}

	public void setSenhaSite(String senhaSite) {
		this.senhaSite = senhaSite;
	}
}
