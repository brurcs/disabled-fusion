package com.neomind.fusion.custom.orsegups.mobile.vo;

import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Created by carla.regina on 06/09/2014.
 */
public class Inspecao
{
	private Integer id;

	private String empresa;

	private String cliente;

	private String lotacao;

	private String posto;
	
	private String codCcu;

	private String pesquisa;

	private String colaborador;

	private String funcaoColaborador;

	private String dataInspecao;

	private String gestor;

	private String obs;
	
	private String token;

	private String taskId;

	private ArrayList<Resposta> respostas;

	private Boolean isReinspecao;

	private Boolean enviada;
	
	private String datarespostaInconsistencia; // ação
	
	private String prazoAcaoInconsistencia; // data INC
	
	private String siglaRegional;

	public Boolean getEnviada()
	{
		return enviada;
	}

	public void setEnviada(Boolean enviada)
	{
		this.enviada = enviada;
	}

	public String getColaborador()
	{
		return colaborador;
	}

	public void setColaborador(String colaborador)
	{
		this.colaborador = colaborador;
	}

	public String getGestor()
	{
		return gestor;
	}

	public void setGestor(String gestor)
	{
		this.gestor = gestor;
	}

	public String getObs()
	{
		return obs;
	}

	public void setObs(String obs)
	{
		this.obs = obs;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public ArrayList<Resposta> getRespostas()
	{
		return respostas;
	}

	public void setRespostas(ArrayList<Resposta> respostas)
	{
		this.respostas = respostas;
	}

	public Boolean getIsReinspecao()
	{
		return isReinspecao;
	}

	public void setIsReinspecao(Boolean isReinspecao)
	{
		this.isReinspecao = isReinspecao;
	}

	public String getEmpresa()
	{
		return empresa;
	}

	public void setEmpresa(String empresa)
	{
		this.empresa = empresa;
	}

	public String getCliente()
	{
		return cliente;
	}

	public void setCliente(String cliente)
	{
		this.cliente = cliente;
	}

	public String getLotacao()
	{
		return lotacao;
	}

	public void setLotacao(String lotacao)
	{
		this.lotacao = lotacao;
	}

	public String getPosto()
	{
		return posto;
	}

	public void setPosto(String posto)
	{
		this.posto = posto;
	}

	public String getPesquisa()
	{
		return pesquisa;
	}

	public void setPesquisa(String pesquisa)
	{
		this.pesquisa = pesquisa;
	}

	public String getNome()
	{
		return "Inspecao " + this.id + "." + System.getProperty("line.separator") + "Empresa: "
				+ this.empresa + System.getProperty("line.separator") + "Enviada ao Fusion: "
				+ this.getEnviada() + ".";
	}

	public String getTaskId()
	{
		return taskId;
	}

	public void setTaskId(String taskId)
	{
		this.taskId = taskId;
	}

	public String getFuncaoColaborador()
	{
		return funcaoColaborador;
	}

	public void setFuncaoColaborador(String funcaoColaborador)
	{
		this.funcaoColaborador = funcaoColaborador;
	}

	public String getDataInspecao()
	{
		return dataInspecao;
	}

	public void setDataInspecao(String dataInspecao)
	{
		this.dataInspecao = dataInspecao;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	

	public String getPrazoAcaoInconsistencia() {
		return prazoAcaoInconsistencia;
	}

	public void setPrazoAcaoInconsistencia(String prazoAcaoInconsistencia) {
		this.prazoAcaoInconsistencia = prazoAcaoInconsistencia;
	}

	public String getDatarespostaInconsistencia() {
		return datarespostaInconsistencia;
	}

	public void setDatarespostaInconsistencia(String datarespostaInconsistencia) {
		this.datarespostaInconsistencia = datarespostaInconsistencia;
	}
	
	public String getCodCcu() {
		return codCcu;
	}

	public void setCodCcu(String codCcu) {
		this.codCcu = codCcu;
	}
	
	


	public String getSiglaRegional()
	{
		return siglaRegional;
	}

	public void setSiglaRegional(String siglaRegional)
	{
		this.siglaRegional = siglaRegional;
	}

	@Override
	public String toString()
	{
		return "Inspecao [id=" + id + ", empresa=" + empresa + ", cliente=" + cliente + ", lotacao=" + lotacao + ", posto=" + posto + ", codCcu=" + codCcu + ", pesquisa=" + pesquisa + ", colaborador=" + colaborador + ", funcaoColaborador=" + funcaoColaborador + ", dataInspecao=" + dataInspecao + ", gestor=" + gestor + ", obs=" + obs + ", token=" + token + ", taskId=" + taskId + ", respostas=" + respostas + ", isReinspecao=" + isReinspecao + ", enviada=" + enviada
				+ ", datarespostaInconsistencia=" + datarespostaInconsistencia + ", prazoAcaoInconsistencia=" + prazoAcaoInconsistencia + ", siglaRegional=" + siglaRegional + "]";
	}

	/*@Override
	public String toString() {
		return "Inspecao [id=" + id + ", empresa=" + empresa + ", cliente="
				+ cliente + ", lotacao=" + lotacao + ", posto=" + posto
				+ ", codCcu=" + codCcu + ", pesquisa=" + pesquisa
				+ ", colaborador=" + colaborador + ", funcaoColaborador="
				+ funcaoColaborador + ", dataInspecao=" + dataInspecao
				+ ", gestor=" + gestor + ", obs=" + obs + ", token=" + token
				+ ", taskId=" + taskId + ", respostas=" + respostas
				+ ", isReinspecao=" + isReinspecao + ", enviada=" + enviada
				+ ", datarespostaInconsistencia=" + datarespostaInconsistencia
				+ ", prazoAcaoInconsistencia=" + prazoAcaoInconsistencia + "]";
	}*/
	
	
	

	

	

	
	
}
