package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.neomind.fusion.persist.PersistEngine;

public class OrsegupsProcessUtils
{

	/**
	 * Retorna o NeoId De uma tarefa consultando e mesma pelo codigo do modelo de processo e o codigo da tarefa.
	 * Obs.: Sempre vai buscar o modelo de processo mais atual;
	 * @param processModelNeoID - NeoId do modelo de Processo
	 * @param processCode - Codigo de tarefa
	 * @return - NeoID da wfprocess
	 */
	public static Long findProcessNeoID(String processName, String processCode)
	{

		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("");
			StringBuffer sql = new StringBuffer();

			sql.append(" select wf.neoId, wf.model_neoId from dbo.WFProcess wf WITH (NOLOCK) "); 
			sql.append(" where wf.model_neoId in (select neoId from ProcessModel WITH (NOLOCK)  where name = ? and released = 1 ) and wf.code = ? ");

			pst = conn.prepareStatement(sql.toString());

			pst.setString(1, processName);
			pst.setString(2, processCode);

			rs = pst.executeQuery();

			if (rs.next())
			{
				return rs.getLong(1);
			}

			return null;
		}
		catch (Exception e)
		{
			System.out.println("Erro ao executar método findProcessNeoID");
			e.printStackTrace();
			return null;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}
	}

}
