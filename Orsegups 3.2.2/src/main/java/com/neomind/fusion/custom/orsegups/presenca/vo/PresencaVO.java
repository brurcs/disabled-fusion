package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

import com.neomind.util.NeoUtils;

public class PresencaVO
{
	private GregorianCalendar entrada;
	private GregorianCalendar saida;
	private Long coletorEntrada;
	private Long coletorSaida;
	private StatusVO status;
	private String tipoDirecao;

	public GregorianCalendar getEntrada()
	{
		return this.entrada;
	}

	public void setEntrada(GregorianCalendar entrada)
	{
		this.entrada = entrada;
	}

	public GregorianCalendar getSaida()
	{
		return this.saida;
	}

	public void setSaida(GregorianCalendar saida)
	{
		this.saida = saida;
	}

	public StatusVO getStatus()
	{
		return this.status;
	}

	public void setStatus(StatusVO status)
	{
		this.status = status;
	}

	public String getTipoDirecao() {
		return tipoDirecao;
	}

	public void setTipoDirecao(String tipoDirecao) {
		this.tipoDirecao = tipoDirecao;
	}

	public Long getColetorEntrada() {
		return coletorEntrada;
	}

	public void setColetorEntrada(Long coletorEntrada) {
		this.coletorEntrada = coletorEntrada;
	}

	public Long getColetorSaida() {
		return coletorSaida;
	}

	public void setColetorSaida(Long coletorSaida) {
		this.coletorSaida = coletorSaida;
	}

	public boolean isPresente()
	{
		return ((this.getEntrada() != null) && (this.getSaida() == null));
	}

	@Override
	public String toString()
	{
		return "PresencaVO [entrada=" + NeoUtils.safeDateFormat(entrada) + ", saida=" + NeoUtils.safeDateFormat(saida) + ", status=" + status + "]";
	}

}