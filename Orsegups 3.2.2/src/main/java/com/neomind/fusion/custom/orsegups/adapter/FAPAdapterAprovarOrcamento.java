package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPAdapterAprovarOrcamento implements TaskFinishEventListener {

	NeoPaper papelSliparIndividual = null;
	NeoPaper papelSliparAgrupado = null;

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {

		try {

			EntityWrapper processEntity = new EntityWrapper(event.getProcess().getEntity());

			Boolean abreTarefaFAP = (Boolean) processEntity.findGenericValue("abreTarefaSimplesFAP"); 

			distribuiAtividade(event);

			insereDesconto(event);

			String aplicacoesValidasTarefasNAC[] =  FAPParametrizacao.findParameter("aplicacoesTarefasNAC").split(",");
	        Long aplicacaoFap = (Long) processEntity.findGenericValue("aplicacaoPagamento.codigo");
	        boolean aplicacaoValidaNAC = false;
	        
	        for(String s : aplicacoesValidasTarefasNAC) {
	        	if(aplicacaoFap.toString().equals(s)) {
	        		aplicacaoValidaNAC = true;
	        		break;
	        	}
	        }
	     

			if(aplicacaoValidaNAC || aplicacaoFap == 13L) {
				geraTarefaCobranca(event);
			}

			if(NeoUtils.safeIsNotNull(abreTarefaFAP) && abreTarefaFAP) {
				geraTarefaSimplesFAP(event);				
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro ao executar o adapter de eventos "+this.getClass().getSimpleName());
		}

	}

	private void geraTarefaSimplesFAP(TaskFinishEvent event) {

		try {

			EntityWrapper processEntity = new EntityWrapper(event.getProcess().getEntity());
			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

			String solicitante = (String) processEntity.findField("tarefaSimplesFAP.Solicitante.code").getValue();
			String executor = (String) processEntity.findField("tarefaSimplesFAP.Executor.code").getValue();
			String titulo = (String) processEntity.findField("tarefaSimplesFAP.Titulo").getValue();
			String descricao = (String) processEntity.findField("tarefaSimplesFAP.DescricaoSolicitacao").getValue();
			GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("tarefaSimplesFAP.Prazo").getValue();

			String retorno = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

			processEntity.findField("tarefaSimplesFAP.codigoProcesso").setValue(retorno);

		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Não foi possível abrir a tarefa simples de FAP.");
		}

	}

	private void insereDesconto(TaskFinishEvent event) {

		try {

			BigDecimal valorTotal = new BigDecimal(0.0);
			BigDecimal valorDescontoTotal = new BigDecimal(0.0);

			EntityWrapper processEntity = new EntityWrapper(event.getProcess().getEntity());

			Boolean temDesconto = (Boolean) processEntity.findField("temDesconto").getValue();

			if (temDesconto) {

				valorTotal = (BigDecimal) processEntity.findField("valorTotal").getValue();
				valorDescontoTotal = (BigDecimal) processEntity.findField("valorDescontoFAP").getValue();

				BigDecimal valorDesconto = new BigDecimal(0.0);
				String observacaoDesconto = NeoUtils.safeOutputString(processEntity.findField("observacaoDesconto").getValue());
				valorDesconto = (BigDecimal) processEntity.findField("valorDesconto").getValue();

				if(valorDesconto.compareTo(new BigDecimal(0.0)) == 1) {

					valorTotal = valorTotal.subtract(valorDesconto);
					valorDescontoTotal = valorDescontoTotal.add(valorDesconto);

					NeoObject noOrcamento = AdapterUtils.createNewEntityInstance("FAPItemDeOrcamento");
					EntityWrapper ewOrcamento = new EntityWrapper(noOrcamento);

					NeoObject noTipoItemOrcamento = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPTipoItemDeOrcamento"), new QLEqualsFilter("descricao", "Desconto"));
					ewOrcamento.findField("tipoItem").setValue(noTipoItemOrcamento);
					ewOrcamento.findField("descricao").setValue(!observacaoDesconto.isEmpty() ? observacaoDesconto : "");
					ewOrcamento.findField("valor").setValue(valorDesconto);

					PersistEngine.persist(noOrcamento);

					processEntity.findField("itemOrcamento").addValue(noOrcamento);
					processEntity.findField("valorTotal").setValue(valorTotal);
					processEntity.findField("valorDescontoFAP").setValue(valorDescontoTotal);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Não foi possível inserir o desconto no Item de Orçamento.");
		}

	}

	public void distribuiAtividade(TaskFinishEvent event) {

		EntityWrapper ewProcessEntity = new EntityWrapper(event.getProcess().getEntity());

		Long codigoAplicacao = ewProcessEntity.findGenericValue("aplicacaoPagamento.codigo");

		papelSliparIndividual = getPapelResponsavelIndividual(codigoAplicacao);
		papelSliparAgrupado = getPapelResponsavelAgrupado(codigoAplicacao);

		ewProcessEntity.findField("responsavelSliparPagamento").setValue(papelSliparIndividual);
		ewProcessEntity.findField("responsavelSliparPagamentoAgrupado").setValue(papelSliparAgrupado);

	}	

	/**
	 * @param codigoAplicacao - O código da aplicação da FAP.
	 * @return NeoPaper - O papel que será atribuído como responsável da atividade.
	 * 
	 * */
	public NeoPaper getPapelResponsavelIndividual(Long codigoAplicacao) {		
		NeoPaper papel = null;

		Map<Long, String> responsaveis = new HashMap<>();

		responsaveis.put(1L, "FAPSliparPagamentoIndividualFrota");
		responsaveis.put(10L,"FAPSliparPagamentoIndividualTecnico");
		responsaveis.put(11L,"FAPSliparPagamentoIndividualTatico");

		for(Long key : responsaveis.keySet()){
			if(codigoAplicacao == key){
				papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", responsaveis.get(key)));
			}
		}
		// Caso não exista divisão para a aplicação em questão, será atribuído o papel padrão.
		if(NeoUtils.safeIsNull(papel)){
			papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[FAP] Controladoria"));
		}
		return papel;
	}

	/**
	 * @param codigoAplicacao - O código da aplicação da FAP.
	 * @return NeoPaper - O papel que será atribuído como responsável da atividade.
	 * 
	 * */
	public NeoPaper getPapelResponsavelAgrupado(Long codigoAplicacao) {		
		NeoPaper papel = null;

		Map<Long, String> responsaveis = new HashMap<>();

		responsaveis.put(1L, "FAPSliparPagamentoAgrupadoFrota");
		responsaveis.put(10L,"FAPSliparPagamentoAgrupadoTecnico");
		responsaveis.put(11L,"FAPSliparPagamentoAgrupadoTatico");

		for(Long key : responsaveis.keySet()){
			if(codigoAplicacao == key){
				papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", responsaveis.get(key)));
			}
		}
		// Caso não exista divisão para a aplicação em questão, será atribuído o papel padrão.
		if(NeoUtils.safeIsNull(papel)){
			papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[FAP] Controladoria Slipar Agrupado"));
		}
		return papel;
	}

	public void geraTarefaCobranca(TaskFinishEvent event) {

		EntityWrapper ewProcessEntity = new EntityWrapper(event.getProcess().getEntity());

		try	{

			String solicitante = OrsegupsUtils.getUserNeoPaper("solicitanteLancarCobrancaFAP");
			String executor = OrsegupsUtils.getUserNeoPaper("ResponsavelCPOLancarCobrancaFap");
			Long numeroOrdemServico = 0L;
			IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
			
			Long aplicacaoFap = (Long) ewProcessEntity.findGenericValue("aplicacaoPagamento.codigo");
			Boolean aplicacaoCoberturaDeVigilante = aplicacaoFap == 13L ? true : false;
			Boolean cobrarDoCliente = ewProcessEntity.findGenericValue("cobrarDoCliente");
			
			StringBuilder descricao = new StringBuilder();

			if ((aplicacaoCoberturaDeVigilante && cobrarDoCliente) || (!aplicacaoCoberturaDeVigilante)) {
				/* Dados Gerais para inclusão da tarefa */
				String codigoFap = ewProcessEntity.findGenericValue("wfprocess.code");
				String codigoContaSigma = ewProcessEntity.findGenericValue("contaSigma.id_central");
				String razaoCliente = ewProcessEntity.findGenericValue("contaSigma.razao");
				String laudoOS = ewProcessEntity.findGenericValue("laudoEletronica.laudo");
				String relatoOS = ewProcessEntity.findGenericValue("laudoEletronica.relato");
				List<NeoObject> listaOs = ewProcessEntity.findGenericValue("listaOrdemDeServico");
				NeoObject objetoOrdemServico = listaOs.isEmpty() == true ? null : listaOs.get(0);
				if (objetoOrdemServico != null) {
					EntityWrapper ewOrdemServico = new EntityWrapper(objetoOrdemServico);
					numeroOrdemServico = (Long) ewOrdemServico.findGenericValue("numeroDaOs") != null
							? (Long) ewOrdemServico.findGenericValue("numeroDaOs")
							: 0L;
				}
				
				String titulo = "Auditar Cobrança - OS: " + numeroOrdemServico.toString() + " - Cliente: "
						+ razaoCliente;
				
				descricao.append("<strong>BA:</strong> " + codigoContaSigma + " - " + razaoCliente + "</br>");
				descricao.append("<strong>FAP:</strong> " + codigoFap + "</br>");
				
				if (aplicacaoCoberturaDeVigilante) {
					
					titulo = "Auditar Cobrança - Cobertura de Vigilante - Cliente: "+ razaoCliente;
					
					BigDecimal valorAcobrarDoCliente = ewProcessEntity.findGenericValue("valorAcobrarDoCliente");
					String observacaoCobranca = ewProcessEntity.findGenericValue("observacaoCobrancaCliente");
					
					descricao.append("<strong>Valor a cobrar:</strong> R$ " + valorAcobrarDoCliente.toString() + " </br>");
					descricao.append("<strong>Observação da Cobrança:</strong> " + observacaoCobranca + " </br>");
				} else {
					descricao.append("<strong>Número OS:</strong> " + numeroOrdemServico.toString() + "</br>");
					descricao.append("<strong>Relato OS:</strong> " + relatoOS + "</br>");
					descricao.append("<strong>Laudo OS:</strong> " + laudoOS + "</br>");
				}
				
				descricao.append("\nAnalisar possível cobrança, encaminhar ao NAC ou finalizar tarefa.");
				
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
				prazo.set(Calendar.HOUR, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);
				
				tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao.toString(), "1", "nao", prazo);
			}		

		} catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException("Problemas ao iniciar a tarefa simples de cobrança do cliente: " + e.getErrorList().get(0).getI18nMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Problemas ao iniciar a tarefa simples de cobrança do cliente. Favor contatar a TI.");
		}
	}
}
