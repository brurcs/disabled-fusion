package com.neomind.fusion.custom.orsegups.converter;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class ARMUltimaLotacaoConverter extends StringConverter
{
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		List<NeoObject> listaArmas = new ArrayList<NeoObject>();
		EntityWrapper processEntityWrapper = new EntityWrapper(field.getForm().getObject());
		StringBuilder textoTable = new StringBuilder();

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		QLOpFilter eliminateItSelf = new QLOpFilter("numeroArma", "=", (String) processEntityWrapper.findValue("numeroArma"));
		groupFilter.addFilter(eliminateItSelf);

		listaArmas = PersistEngine.getObjects(AdapterUtils.getEntityClass("ARMArmamento"), groupFilter, -1, -1, "numeroArma ASC");

		if (NeoUtils.safeIsNotNull(listaArmas) && !listaArmas.isEmpty())
		{
			for (NeoObject oArmas : listaArmas)	
			{
				EntityWrapper wrpArmas = new EntityWrapper(oArmas);	
				List<NeoObject> listaPostos = new ArrayList<NeoObject>();

				listaPostos = (List<NeoObject>) wrpArmas.findValue("posto");
				
				if (NeoUtils.safeIsNotNull(listaPostos) && !listaPostos.isEmpty())
				{
					GregorianCalendar dtAuxLot = null;
					for (NeoObject oPosto : listaPostos)	
					{
						EntityWrapper wrpPosto = new EntityWrapper(oPosto);
						GregorianCalendar dtAltLot = (GregorianCalendar) wrpPosto.findValue("dataAlteracao");
						
						if(dtAuxLot == null || dtAltLot.after(dtAuxLot))
						{
							dtAuxLot = dtAltLot;
							textoTable = new StringBuilder();
							
							textoTable.append(NeoUtils.safeOutputString((Long)wrpPosto.findValue("posto.usu_numctr")) + " / " + NeoUtils.safeOutputString((Long)wrpPosto.findValue("posto.usu_numpos")) + " - ");
							textoTable.append(NeoUtils.safeOutputString((Long)wrpPosto.findValue("cliente.cgccpf")) + " / " + (String)wrpPosto.findValue("cliente.nomcli") + " - ");
							textoTable.append((String)wrpPosto.findValue("posto.usu_codccu") + " - " + (String)wrpPosto.findValue("posto.nomloc"));
						}
					}
				}
				else
				{
					textoTable.append("Nenhum registro encontrado.");
				}
			}
		}

		return textoTable.toString();
	}
	
	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
