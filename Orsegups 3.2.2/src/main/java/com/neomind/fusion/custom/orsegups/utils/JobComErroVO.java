package com.neomind.fusion.custom.orsegups.utils;

public class JobComErroVO {

	private String proximaExecucao;
	private String ultimaExecucao;
	private String nomeRotina;
	private String periodicidade;
	private String detalhesDoErro;
	private String erro;
	
	public String getErro() {
		return erro;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public String getProximaExecucao() {
		return proximaExecucao;
	}
	public void setProximaExecucao(String proximaExecucao) {
		this.proximaExecucao = proximaExecucao;
	}
	public String getUltimaExecucao() {
		return ultimaExecucao;
	}
	public void setUltimaExecucao(String ultimaExecucao) {
		this.ultimaExecucao = ultimaExecucao;
	}
	public String getNomeRotina() {
		return nomeRotina;
	}
	public void setNomeRotina(String nomeRotina) {
		this.nomeRotina = nomeRotina;
	}
	public String getPeriodicidade() {
		return periodicidade;
	}
	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}
	public String getDetalhesDoErro() {
		return detalhesDoErro;
	}
	public void setDetalhesDoErro(String detalhesDoErro) {
		this.detalhesDoErro = detalhesDoErro;
	}
	
}
