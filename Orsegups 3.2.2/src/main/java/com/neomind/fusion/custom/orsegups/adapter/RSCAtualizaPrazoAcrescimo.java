package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RSCAtualizaPrazoAcrescimo implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar data01 = (GregorianCalendar) processEntity.findField("dataAcresc").getValue();
		data01 = OrsegupsUtils.getNextWorkDay(data01);
		processEntity.findField("prazoAtendimento").setValue(data01);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
