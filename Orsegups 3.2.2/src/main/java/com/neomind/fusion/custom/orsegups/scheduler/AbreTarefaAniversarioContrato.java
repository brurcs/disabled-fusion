package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.ContratoVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaAniversarioContrato implements CustomJobAdapter {
	private static final Log log = LogFactory.getLog(AbreTarefaAniversarioContrato.class);
	
	public static String mensagemErro = "Erro ao executar a Rotina ABRETAREFAANIVERSARIOCONTRATO";
	
	@Override
	public void execute(CustomJobContext ctx) {
	
		System.out.println("#### INICIO DA ROTINA ABRETAREFAANIVERSARIOCONTRATO - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		
		LinkedList<ContratoVO> listaContratos = new LinkedList<>();
		
		try {
			
			listaContratos = retornaContratosAniversario();
			
			if(NeoUtils.safeIsNotNull(listaContratos)) {
				abreTarefaContratos(listaContratos);				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### ERRO DA ROTINA ABRETAREFAANIVERSARIOCONTRATO - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			throw new WorkflowException(mensagemErro);
		} finally {
			System.out.println("#### FIM DA ROTINA ABRETAREFAANIVERSARIOCONTRATO - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
		
	}

	
	private void abreTarefaContratos(LinkedList<ContratoVO> listaContratos) {
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		NeoUser solicitante = null;
		NeoUser executor = null;
		NeoPaper papelSolicitante = null;
		NeoPaper papelExecutor = null;
		String titulo = null;
		String descricao = null;
		 
		GregorianCalendar prazo = new GregorianCalendar();
		
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		
		try {

			papelSolicitante = OrsegupsUtils.getPaper("SolicitanteTarefaAniversarioContrato");
			papelExecutor = OrsegupsUtils.getPaper("ExecutorTarefaAniversarioContrato");
			
			solicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelSolicitante);
			executor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);
			
			
			for(ContratoVO contratoVO : listaContratos) {
				
				try {
					
					titulo = "Aniversário Contrato - "+contratoVO.getNomeCliente();
					
					descricao = retornaDescricao(contratoVO);
					
					iniciarTarefaSimples.abrirTarefa(solicitante.getCode(), executor.getCode(), titulo, descricao, "1", "sim", prazo);
					
				} catch (Exception e) {
					e.printStackTrace();
					log.error("Erro ");
				}
			}	
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = this.getClass().getSimpleName() +" - ERRO ao abrir tarefa de Aniversário Contrato";
			throw new WorkflowException(mensagemErro);
		}
		
		
	}


	private String retornaDescricao(ContratoVO contratoVO) {
		
		
		GregorianCalendar gcInicio = new GregorianCalendar();
		gcInicio.setTime(contratoVO.getDataInicioVigencia());
		
		String inicioDaVigencia = NeoDateUtils.safeDateFormat(gcInicio, "dd/MM/yyyy HH:mm:ss");
			
		StringBuilder corpoTarefa = new StringBuilder();
		
		corpoTarefa.append(" <html>                                                  				   												 ");
        corpoTarefa.append("    <head>                                              				   												 ");
        corpoTarefa.append("        <meta charset='utf-8'>                           				   												 ");
        corpoTarefa.append("    </head>                                              				   												 ");
		corpoTarefa.append("      <body>                                             				   												 ");
		corpoTarefa.append("          <table border='2'>                             				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("                  <th>EMPRESA/FILIAL</th>                      				   										     ");
		corpoTarefa.append("                  <td>&nbsp; "+contratoVO.getEmpresa()+" - "+contratoVO.getFilial()+" </td>       		      			 ");
		corpoTarefa.append("              </tr>                                       				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("              	<th>NOME DO CLIENTE</th>                 				   												 ");
		corpoTarefa.append("                <td>&nbsp; "+contratoVO.getNomeCliente()+" </td>           												 ");
		corpoTarefa.append("              </tr>                                      				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("              	<th>NÚMERO DO CONTRATO</th>              				   												 ");
		corpoTarefa.append("                <td>&nbsp; "+contratoVO.getNumeroContrato()+" </td>        												 ");
		corpoTarefa.append("              </tr>                                    					   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("              	<th>INÍCIO DA VIGÊNCIA</th>               				   												 ");
		corpoTarefa.append("                <td>&nbsp; "+inicioDaVigencia+" </td>        		 	   												 ");
		corpoTarefa.append("              </tr>                                       				   												 ");
		corpoTarefa.append("              <tr>                                      				   												 ");
		corpoTarefa.append("              	<th>TEMPO DE VIGÊNCIA</th>      				   														 ");
		corpoTarefa.append("                <td>&nbsp; "+contratoVO.getTempoVigencia()+" </td> 														 ");
		corpoTarefa.append("              </tr>                                      				   												 ");                                     				   												                                      				   												 
		corpoTarefa.append("          </table>                                      				   												 ");
		corpoTarefa.append("      </body>                                             				   												 ");
		corpoTarefa.append(" </html>                                                  				   												 ");
		
		return corpoTarefa.toString();
	}


	public LinkedList<ContratoVO> retornaContratosAniversario(){
		
		LinkedList<ContratoVO> listaContratos = new LinkedList<>();
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		
		try {
			
			sql.append(" SELECT CTR.USU_CODEMP EMPRESA, CTR.USU_CODFIL FILIAL, CLI.NOMCLI CLIENTE, CTR.USU_NUMCTR CONTRATO, CTR.USU_INIVIG INICIO_VIGENCIA, YEAR(GETDATE())-YEAR(CTR.USU_INIVIG) AS TEMPO_VIGENCIA ");
			sql.append(" FROM USU_T160CTR CTR ");
			sql.append(" INNER JOIN E085CLI CLI ");
			sql.append(" ON CTR.USU_CODCLI = CLI.CODCLI ");
			sql.append(" INNER JOIN USU_T160CVS CVS ");
			sql.append(" ON CTR.USU_CODEMP = CVS.USU_CODEMP ");
			sql.append(" AND CTR.USU_CODFIL = CVS.USU_CODFIL ");
			sql.append(" AND CTR.USU_NUMCTR = CVS.USU_NUMCTR ");
			sql.append(" INNER JOIN E080SER SER ");
			sql.append(" ON CVS.USU_CODEMP = SER.CODEMP ");
			sql.append(" AND CVS.USU_CODSER = SER.CODSER ");
			sql.append(" WHERE DAY(CTR.USU_INIVIG) = DAY(GETDATE()+2) ");
			sql.append(" AND MONTH(CTR.USU_INIVIG) = MONTH(GETDATE()) ");
			sql.append(" AND YEAR(CTR.USU_INIVIG) < YEAR(GETDATE()) ");
			sql.append(" AND SER.CODFAM IN ('SER101','SER103') ");
			sql.append(" AND ((CTR.USU_SITCTR = 'A') OR ((CTR.USU_SITCTR = 'I') AND (CTR.USU_DATFIM >= GETDATE()))) ");
			sql.append(" AND ((CVS.USU_SITCVS = 'A') OR ((CVS.USU_SITCVS = 'I') AND (CVS.USU_DATFIM >= GETDATE()))) ");
			sql.append(" GROUP BY CTR.USU_CODEMP, CTR.USU_CODFIL, CLI.NOMCLI, CTR.USU_NUMCTR, CTR.USU_INIVIG  ");
			
			conn = PersistEngine.getConnection("SAPIENS");
			
			pstm = conn.prepareStatement(sql.toString());

			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				ContratoVO contratoVO = new ContratoVO();
				
				contratoVO.setEmpresa(rs.getLong("EMPRESA"));
				contratoVO.setFilial(rs.getLong("FILIAL"));
				contratoVO.setNomeCliente(rs.getString("CLIENTE"));
				contratoVO.setNumeroContrato(rs.getLong("CONTRATO"));
				contratoVO.setDataInicioVigencia(rs.getTimestamp("INICIO_VIGENCIA"));
				contratoVO.setTempoVigencia(rs.getLong("TEMPO_VIGENCIA"));
				
				listaContratos.add(contratoVO);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			mensagemErro = this.getClass().getSimpleName() +" - ERRO ao abrir tarefa de Aniversário Contrato";
			throw new WorkflowException(mensagemErro);
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return listaContratos;
	}
	
}
