package com.neomind.fusion.custom.orsegups.rsc;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class RSCRegistroAtividadeCategoriaDiversos implements AdapterInterface
{
    private static final Log log = LogFactory.getLog(RSCRegistroAtividadeCategoriaDiversos.class);

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity)
    {
	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

	String mensagem = "Por favor, contatar o administrador do sistema!";
	WorkflowException exception = null;
	try
	{
	    /* Início das funções úteis --> Funções necessárias para decorrer dos testes */
	    //System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoRegistrarRSC"), "dd/MM/yyyy HH:mm:ss"));
	    /* Fim das funções úteis */

	    RSCUtils rscUtils = new RSCUtils();
	    GregorianCalendar prazoDeadLine = new GregorianCalendar();

	    InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("RSCRegistroAtividade");
	    NeoObject registro = registroAtividade.createNewInstance();
	    EntityWrapper wRegistro = new EntityWrapper(registro);

	    wRegistro.findField("responsavel").setValue(origin.getUser());
	    wRegistro.findField("dataInicialRSC").setValue(origin.getStartDate());
	    wRegistro.findField("dataFinalRSC").setValue(origin.getFinishDate());

	    /* Histórico de Atividade do Fluxo C027.001 - RSC - Categoria Diversos */
	    if (activity.getProcess().getModel().getName().equals("C027.001 - RSC - Categoria Diversos"))
	    {
		String des = "";
		if (origin.getActivityName().equalsIgnoreCase("Atender Solicitação"))
		{
		    String txtDescricao = "";
		    if (origin.getFinishByUser() == null)
		    {
			prazoDeadLine = rscUtils.retornaPrazoDeadLine("Atender Solicitação - Escalada", (GregorianCalendar) processEntity.findValue("prazoAtenderSolicitacao"));
			processEntity.findField("prazoAtenderSolicitacaoEscalada").setValue(prazoDeadLine);

			des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
			wRegistro.findField("descricao").setValue(des);
			wRegistro.findField("atividade").setValue("Atender Solicitação");
			wRegistro.findField("prazo").setValue(prazoDeadLine);

			processEntity.setValue("souPessoaResponsavel", true);
		    }
		    else
		    {
			Boolean souPessoaResponsavel = (Boolean) processEntity.findValue("souPessoaResponsavel");
			if (souPessoaResponsavel)
			{
			    if ((Long) processEntity.findValue("realizarVerificaoEficacia.codigoOpcao") == 3L)
			    {
				prazoDeadLine = rscUtils.addDiasPrazoRSC(new GregorianCalendar(), Integer.parseInt(processEntity.findValue("quantidadeDiasIniciarVerificacaoEficacia.quantidadeDias").toString()), true);
				processEntity.findField("prazoAguardandoVerificaoEficacia").setValue(prazoDeadLine);

				txtDescricao = (String) processEntity.findValue("providenciasTomadas");
				des = " --> Encaminhado a tarefa para aguardar o início da verificação de eficácia.";
				wRegistro.findField("atividade").setValue("Atender Solicitação");
				wRegistro.findField("descricao").setValue(txtDescricao + des);
				wRegistro.findField("prazo").setValue(prazoDeadLine);
			    }
			    else if (((Long) processEntity.findValue("realizarVerificaoEficacia.codigoOpcao") == 2L) || (!(Boolean) processEntity.findValue("seraAtendidoNoPrazo")))
			    {
				if (((String) processEntity.findValue("providenciasTomadas")) != null)
				{
				    txtDescricao = (String) processEntity.findValue("providenciasTomadas");
				}

				if (((String) processEntity.findValue("justificarAjusteDoPrazo")) != null)
				{
				    Calendar dataAtual  = new GregorianCalendar();
				    Calendar dataPrazoAtendimento = processEntity.findGenericValue("prazoDeAtendimento");

				    /* Evitar que o usuário peça ajuste para um dia anterior ao atual */
				    int result = dataPrazoAtendimento.compareTo(dataAtual);
				    if (result < 0) {
					mensagem = "A data do prazo de atendimento não pode ser menor que a data atual.";
					throw new WorkflowException(mensagem);
				    } else {
					txtDescricao = (String) processEntity.findValue("justificarAjusteDoPrazo");
					txtDescricao = txtDescricao + " - Solicitado ajuste do prazo para o dia " 
						+ NeoDateUtils.safeDateFormat(((GregorianCalendar) processEntity.findValue("prazoDeAtendimento")), "dd/MM/yyyy");
				    }
				}

				System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));
				prazoDeadLine = rscUtils.retornaPrazoDeadLine("Validar Prazo - Gestor", new GregorianCalendar());
				System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));
				processEntity.findField("prazoValidarAjustePrazoGestor").setValue(prazoDeadLine);

				wRegistro.findField("atividade").setValue("Atender Solicitação");
				wRegistro.findField("descricao").setValue(txtDescricao);
				wRegistro.findField("prazo").setValue(prazoDeadLine);

				processEntity.setValue("justificarAjusteDoPrazo", null);
				processEntity.setValue("providenciasTomadas", null);
			    }
			    else
			    {
				txtDescricao = (String) processEntity.findValue("providenciasTomadas");
				wRegistro.findField("atividade").setValue("Atender Solicitação");
				wRegistro.findField("descricao").setValue(txtDescricao);

				processEntity.setValue("providenciasTomadas", null);
			    }
			}
			else
			{
			    des = "Ajustado a tarefa para o colaborador " + processEntity.findValue("usuarioResponsavel.fullName") + " com a seguinte justificativa: " + processEntity.findValue("justifique");
			    wRegistro.findField("atividade").setValue("Atender Solicitação");
			    wRegistro.findField("descricao").setValue(des);
			    wRegistro.findField("prazo").setValue((GregorianCalendar) processEntity.findValue("prazoAtenderSolicitacao"));

			    /* Limpa os campos */
			    processEntity.setValue("usuarioResponsavel", null);
			    processEntity.setValue("justifique", null);
			}
		    }
		}
		else if (origin.getActivityName().equalsIgnoreCase("Atender Solicitação - Escalada"))
		{
		    if (origin.getFinishByUser() == null)
		    {
			des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
			wRegistro.findField("descricao").setValue(des);
			wRegistro.findField("atividade").setValue("Atender Solicitação - Escalada");

			String papel = (String) processEntity.findValue("superiorResponsavelExecutor.name");
			if (papel.contains("Diretor") || papel.contains("Presidente"))
			{
			    prazoDeadLine = rscUtils.retornaPrazoDeadLine("Atender Solicitação - Escalada Diretoria", (GregorianCalendar) processEntity.findValue("prazoAtenderSolicitacaoEscalada"));
			    processEntity.findField("prazoAtenderSolicitacaoEscaladaDiretoria").setValue(prazoDeadLine);
			    processEntity.findField("souPessoaResponsavel").setValue(false);
			    processEntity.findField("continuarRSC").setValue(true);

			    wRegistro.findField("prazo").setValue(prazoDeadLine);

			    rscUtils.registrarTarefaSimples(processEntity, activity);
			}
			else
			{
			    prazoDeadLine = rscUtils.retornaPrazoDeadLine("Atender Solicitação - Escalada", (GregorianCalendar) processEntity.findValue("prazoAtenderSolicitacaoEscalada"));
			    processEntity.findField("prazoAtenderSolicitacaoEscalada").setValue(prazoDeadLine);

			    wRegistro.findField("prazo").setValue(prazoDeadLine);
			}
		    }
		    else
		    {
			if ((Long) processEntity.findValue("realizarVerificaoEficacia.codigoOpcao") == 3L)
			{
			    prazoDeadLine = rscUtils.addDiasPrazoRSC(new GregorianCalendar(), Integer.parseInt(processEntity.findValue("quantidadeDiasIniciarVerificacaoEficacia.quantidadeDias").toString()), true);
			    processEntity.findField("prazoAguardandoVerificaoEficacia").setValue(prazoDeadLine);

			    String txtDescricao = (String) processEntity.findValue("providenciasTomadas");
			    des = " --> Encaminhado a tarefa para aguardar o início da verificação de eficácia.";
			    wRegistro.findField("atividade").setValue("Atender Solicitação - Escalada");
			    wRegistro.findField("descricao").setValue(txtDescricao + des);
			    wRegistro.findField("prazo").setValue(prazoDeadLine);
			}
			else
			{
			    String txtDescricao = (String) processEntity.findValue("providenciasTomadas");
			    wRegistro.findField("atividade").setValue("Atender Solicitação - Escalada");
			    wRegistro.findField("descricao").setValue(txtDescricao);
			}

			processEntity.setValue("providenciasTomadas", null);
		    }
		}
		else if (origin.getActivityName().equalsIgnoreCase("Atender Solicitação - Escalada Diretoria"))
		{
		    if (origin.getFinishByUser() == null)
		    {
			prazoDeadLine = rscUtils.retornaPrazoDeadLine("Atender Solicitação - Escalada Presidência", (GregorianCalendar) processEntity.findValue("prazoAtenderSolicitacaoEscaladaDiretoria"));
			processEntity.findField("prazoAtenderSolicitacaoEscaladaPresidencia").setValue(prazoDeadLine);

			des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
			wRegistro.findField("atividade").setValue("Atender Solicitação - Escalada Diretoria");
			wRegistro.findField("descricao").setValue(des);

			wRegistro.findField("prazo").setValue(prazoDeadLine);

			rscUtils.registrarTarefaSimples(processEntity, activity);
		    }
		    else
		    {
			if ((Long) processEntity.findValue("realizarVerificaoEficacia.codigoOpcao") == 3L)
			{
			    prazoDeadLine = rscUtils.addDiasPrazoRSC(new GregorianCalendar(), Integer.parseInt(processEntity.findValue("quantidadeDiasIniciarVerificacaoEficacia.quantidadeDias").toString()), true);
			    processEntity.findField("prazoAguardandoVerificaoEficacia").setValue(prazoDeadLine);

			    String txtDescricao = (String) processEntity.findValue("providenciasTomadas");
			    des = " --> Encaminhado a tarefa para aguardar o início da verificação de eficácia.";
			    wRegistro.findField("atividade").setValue("Atender Solicitação - Escalada Diretoria");
			    wRegistro.findField("descricao").setValue(txtDescricao + des);
			    wRegistro.findField("prazo").setValue(prazoDeadLine);

			}
			else
			{
			    String txtDescricao = (String) processEntity.findValue("providenciasTomadas");
			    wRegistro.findField("atividade").setValue("Atender Solicitação - Escalada Diretoria");
			    wRegistro.findField("descricao").setValue(txtDescricao);
			}

			GregorianCalendar gc = new GregorianCalendar();
			gc = rscUtils.addDiasPrazoRSC(gc, 1, true);
			processEntity.findField("prazoAtenderSolicitacao").setValue(gc);
			processEntity.setValue("providenciasTomadas", null);
		    }
		    processEntity.setValue("superiorResponsavelExecutor", rscUtils.retornaSuperiorResponsavelExecutor((NeoUser) processEntity.findValue("usuarioResponsavel")));
		}
		else if (origin.getActivityName().equalsIgnoreCase("Atender Solicitação - Escalada Presidência"))
		{
		    if ((Long) processEntity.findValue("realizarVerificaoEficacia.codigoOpcao") == 3L)
		    {
			prazoDeadLine = rscUtils.addDiasPrazoRSC(new GregorianCalendar(), Integer.parseInt(processEntity.findValue("quantidadeDiasIniciarVerificacaoEficacia.quantidadeDias").toString()), true);
			processEntity.findField("prazoAguardandoVerificaoEficacia").setValue(prazoDeadLine);

			String txtDescricao = (String) processEntity.findValue("providenciasTomadas");
			des = " --> Encaminhado a tarefa para aguardar o início da verificação de eficácia.";
			wRegistro.findField("atividade").setValue("Atender Solicitação - Escalada Presidência");
			wRegistro.findField("descricao").setValue(txtDescricao + des);
			wRegistro.findField("prazo").setValue(prazoDeadLine);
		    }
		    else
		    {
			String txtDescricao = (String) processEntity.findValue("providenciasTomadas");
			wRegistro.findField("atividade").setValue("Atender Solicitação - Escalada Presidência");
			wRegistro.findField("descricao").setValue(txtDescricao);
		    }
		    GregorianCalendar gc = new GregorianCalendar();
		    gc = rscUtils.addDiasPrazoRSC(gc, 1, true);
		    processEntity.findField("prazoAtenderSolicitacao").setValue(gc);
		    processEntity.setValue("providenciasTomadas", null);
		}
		else if (origin.getActivityName().equalsIgnoreCase("Aguardando Início da Verificação Eficácia"))
		{
		    if (origin.getFinishByUser() == null)
		    {
			des = "RSC encaminhada para verificação de eficácia";
			wRegistro.findField("descricao").setValue(des);
			wRegistro.findField("atividade").setValue("Aguardando Início da Verificação Eficácia");
		    }
		}
		else if (origin.getActivityName().equalsIgnoreCase("Validar Prazo - Gestor"))
		{
		    Boolean prazoValidado = (Boolean) processEntity.findValue("prazoValidado");
		    String txtDescricao = (String) processEntity.findValue("observacao");

		    if (((Long) processEntity.findValue("realizarVerificaoEficacia.codigoOpcao") == 2L) && ((Boolean) processEntity.findValue("seraAtendidoNoPrazo")))
		    {
			if (((Boolean) processEntity.findValue("encaminharParaVerificacao")))
			{
			    wRegistro.findField("atividade").setValue("Validar Prazo - Gestor");
			    wRegistro.findField("descricao").setValue(txtDescricao);
			}
			else
			{
			    wRegistro.findField("atividade").setValue("Validar Prazo - Gestor");
			    wRegistro.findField("descricao").setValue(txtDescricao);
			    wRegistro.findField("prazo").setValue(new GregorianCalendar());
			}

			processEntity.setValue("observacao", null);
		    }
		    else
		    {
			if (prazoValidado != null && prazoValidado)
			{
			    GregorianCalendar prazo = (GregorianCalendar) processEntity.findValue("prazoDeAtendimento");
			    prazo.set(Calendar.HOUR_OF_DAY, 23);
			    prazo.set(Calendar.MINUTE, 59);
			    prazo.set(Calendar.SECOND, 59);

			    processEntity.findField("prazoAtenderSolicitacao").setValue(prazo);

			    wRegistro.findField("atividade").setValue("Validar Prazo - Gestor");
			    wRegistro.findField("descricao").setValue(txtDescricao);
			    wRegistro.findField("prazo").setValue(prazo);

			    /* Limpa os campos */
			    processEntity.setValue("seraAtendidoNoPrazo", true);
			    processEntity.setValue("prazoDeAtendimento", null);
			    processEntity.setValue("justificarAjusteDoPrazo", null);
			    processEntity.setValue("prazoValidado", false);
			    processEntity.setValue("observacao", null);
			}
			else
			{
			    wRegistro.findField("atividade").setValue("Validar Prazo - Gestor");
			    wRegistro.findField("descricao").setValue(txtDescricao);
			    wRegistro.findField("prazo").setValue((GregorianCalendar) processEntity.findValue("prazoAtenderSolicitacao"));

			    /* Limpa os campos */
			    processEntity.setValue("seraAtendidoNoPrazo", true);
			    processEntity.setValue("prazoDeAtendimento", null);
			    processEntity.setValue("justificarAjusteDoPrazo", null);
			    processEntity.setValue("prazoValidado", false);
			    processEntity.setValue("observacao", null);
			}
		    }
		}

		NeoObject rscRegistroSolicitacaoCliente = (NeoObject) processEntity.findValue("RscRelatorioSolicitacaoCliente");
		EntityWrapper rscWrapper = new EntityWrapper(rscRegistroSolicitacaoCliente);

		PersistEngine.persist(registro);
		rscWrapper.findField("registroAtividades").addValue(registro);
	    }
	}
	catch (Exception e)
	{
	    log.error(mensagem);
	    e.printStackTrace();
	    throw new WorkflowException(mensagem);
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity)
    {
	// TODO Auto-generated method stub

    }
}
