package com.neomind.fusion.custom.orsegups.utils;

import java.util.GregorianCalendar;

public class ContratosInativosVO {

	private Long numeroContrato;
	private Long numeroEmpresa;
	private Long numeroFilial;
	private GregorianCalendar dataInativacao;

	public Long getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(Long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public Long getNumeroEmpresa() {
		return numeroEmpresa;
	}

	public void setNumeroEmpresa(Long numeroEmpresa) {
		this.numeroEmpresa = numeroEmpresa;
	}

	public Long getNumeroFilial() {
		return numeroFilial;
	}

	public void setNumeroFilial(Long numeroFilial) {
		this.numeroFilial = numeroFilial;
	}

	public GregorianCalendar getDataInativacao() {
		return dataInativacao;
	}

	public void setDataInativacao(GregorianCalendar dataInativacao) {
		this.dataInativacao = dataInativacao;
	}

}
