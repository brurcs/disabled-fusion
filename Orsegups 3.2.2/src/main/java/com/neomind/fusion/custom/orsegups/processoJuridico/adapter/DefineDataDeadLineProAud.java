package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.IniciarGTCs

public class DefineDataDeadLineProAud implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		if (processEntity.getValue("datProAud") != null)
		{
			GregorianCalendar datProAud = (GregorianCalendar) processEntity.getValue("datProAud");
			processEntity.setValue("deadLineDtProximaAud", (GregorianCalendar) datProAud.clone());
		}
		else
		{

		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
