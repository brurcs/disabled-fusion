package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.amazonaws.auth.RSA;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
import com.neomind.fusion.custom.orsegups.utils.AnulacaoPontoVO;

public class RotinaCadastraAnulacaoPonto implements CustomJobAdapter {
	
	
	
	private static final Log log = LogFactory.getLog(RotinaCadastraAnulacaoPonto.class);
	EntityManager entityManager;
	EntityTransaction transaction;
	
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		log.error("##### INICIO AGENDADOR DE TAREFA: Rotina Cadastra Anulação de Ponto - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		System.out.println("##### INICIO AGENDADOR DE TAREFA: Rotina Cadastra Anulação de Ponto - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long logKey = GregorianCalendar.getInstance().getTimeInMillis(); 

		Connection conn = null;
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		String ultimaExecucaoRotina = OrsegupsEmailUtils.ultimaExecucaoRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_ANULACAO_PONTO);
		
		List <AnulacaoPontoVO> anulacaoPontoVO = new ArrayList<AnulacaoPontoVO>();
		
		try {
			
			sql.append(" SELECT c.CD_CLIENTE, c.RAZAO, zona.DESCRICAO AS DESC_ZONA, h.DT_RECEBIDO ");
			sql.append(" FROM HISTORICO_SEM_CONTROLE H WITH(NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE "); 
			sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO "); 
			sql.append(" LEFT JOIN dbCIDADE cid WITH(NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE ");
			sql.append(" LEFT JOIN dbBAIRRO bai WITH(NOLOCK) ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO "); 
			sql.append(" LEFT JOIN dbEVENTO ZONA WITH(NOLOCK) ON zona.CD_CLIENTE = h.CD_CLIENTE and zona.ID_EVENTO = h.NU_AUXILIAR "); 
			sql.append(" WHERE C.TP_PESSOA != 2 ");
			sql.append(" AND C.CTRL_CENTRAL = 1 AND C.FG_ATIVO = 1 "); 
			sql.append(" AND EXISTS (SELECT RAE.ID_CODE FROM [FSOODB04\\SQL02].TIDB.DBO.RAT_ANULACAO_CUC RAE WHERE RAE.ID_CODE = H.CD_CODE) "); 
			sql.append(" AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_EmailAutomaticoDesvioDeHabito em where em.historico = h.CD_HISTORICO_SEM_CONTROLE) "); 
			sql.append(" AND ( DT_RECEBIDO > '"+ultimaExecucaoRotina+"' ) ");

			
			conn = PersistEngine.getConnection("SIGMA90");
			
			 pstm = conn.prepareStatement(sql.toString());
			 rs = pstm.executeQuery();
			 
			while(rs.next()){
				
				AnulacaoPontoVO anulacaoPonto = new AnulacaoPontoVO();
				
				Long codigoCliente = rs.getLong("CD_CLIENTE");
				String razaoSocial = rs.getString("RAZAO");
				String zonaAnulada = rs.getString("DESC_ZONA");
				
				Timestamp timestamp = rs.getTimestamp("DT_RECEBIDO");
				
				GregorianCalendar dtRecebido = new GregorianCalendar();
				dtRecebido.setTime(timestamp);
				
				anulacaoPonto.setCodigoCliente(codigoCliente);
				anulacaoPonto.setRazaoSocial(razaoSocial);
				anulacaoPonto.setDataRecebido(dtRecebido);
				anulacaoPonto.setZonaAnulada(zonaAnulada);
				
				anulacaoPontoVO.add(anulacaoPonto);
				
			}
			
			Iterator <AnulacaoPontoVO> iterator = anulacaoPontoVO.iterator();
			
			while (iterator.hasNext()){
				
				AnulacaoPontoVO ponto = iterator.next();
				
				if(!validaEventoCapturado(ponto.getCodigoCliente(), ponto.getRazaoSocial(), ponto.getDataRecebido(), ponto.getZonaAnulada())){
					String resultado = insereAnulacoesPonto(ponto.getCodigoCliente(), ponto.getRazaoSocial(), ponto.getDataRecebido(), ponto.getZonaAnulada());
					System.out.println(resultado);
				}else{
					log.warn("EVENTO JÁ INSERIDO NA TABELA RAT_ANULACAO_PONTO [CLIENTE: "+ponto.getCodigoCliente()+ " - "+ponto.getRazaoSocial()+" | DAT. RECEBIDO: "+ponto.getDataRecebido()+" | ZONA ANULADA: "+ponto.getZonaAnulada()+"]");
					System.out.println("EVENTO JÁ INSERIDO NA TABELA RAT_ANULACAO_PONTO [CLIENTE: "+ponto.getCodigoCliente()+ " - "+ponto.getRazaoSocial()+" | DAT RECEBIDO: "+ponto.getDataRecebido()+" | ZONA ANULADA: "+ponto.getZonaAnulada()+"] ");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			log.error("##### FIM AGENDADOR DE TAREFA: Rotina Cadastra Anulação de Ponto - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("##### FIM AGENDADOR DE TAREFA: Rotina Cadastra Anulação de Ponto - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
		
	}
	
	/**
	 * Insere as zonas anuladas na tabela de RAT_ANULACAO_PONTO pra Rotina 
	 * @param codigoCliente
	 * @param razaoSocial
	 * @param dataEvento
	 * @param zonaAnulada
	 * @return Se a zona foi ou não anulada, qual a zona, cliente, hora do evento.
	 */
	public String insereAnulacoesPonto(Long codigoCliente, String razaoSocial, GregorianCalendar dataEvento, String zonaAnulada){
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		Connection conn = null;
		
		String resultado = null;
		
		GregorianCalendar dtEvento = new GregorianCalendar();
		
		dtEvento = dataEvento;
		
		Timestamp timestamp = new Timestamp(dtEvento.getTimeInMillis());
		
		
		try {
			
			sql.append("INSERT INTO RAT_ANULACAO_PONTO (CODIGO_CLIENTE, RAZAO_SOCIAL, DT_EVENTO_RECEBIDO, ZONA_ANULADA, CONTROLE_ENVIO) ");
			sql.append("VALUES (?, ?, ?, ?, 0) ");
			
			conn = PersistEngine.getConnection("TIDB");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setLong(1, codigoCliente);
			pstm.setString(2, razaoSocial);
			pstm.setTimestamp(3, timestamp);
			pstm.setString(4, zonaAnulada);
			
			pstm.execute();

			resultado = "Inserido a Anulação de Ponto: [Cliente: "+codigoCliente+ " - "+razaoSocial+" | Dat. Recebido: "+timestamp+" | Zona Anulada: "+zonaAnulada+"]";
			
		} catch (Exception e) {
			resultado = "Erro ao inserir a Anulação de Ponto. [Cliente: "+codigoCliente+ " - "+razaoSocial+" | Dat. Recebido: "+timestamp+" | Zona Anulada: "+zonaAnulada+"]";
			System.out.println("ERRO ao Inserir a Anulação de Ponto do Cliente "+codigoCliente+" - "+razaoSocial+" | "+ e);
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
			log.error("##### FIM AGENDADOR DE TAREFA: Rotina Cadastra Anulação de Ponto - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("##### FIM AGENDADOR DE TAREFA: Rotina Cadastra Anulação de Ponto - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
		return resultado;
			
	}
	
	/**
	 *  Valida se o evento capturado pela consulta já foi inserido. Evitar duplicidade de eventos devido a frequência a qual a rotina é executada.
	 * @return False = Evento não foi inserido || True = Evento já foi inserido
	 */
	public boolean validaEventoCapturado(Long codigoCliente, String razaoSocial, GregorianCalendar dataEvento, String zonaAnulada){
		
		boolean resultado = false;
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		Connection conn = null;
		ResultSet rs = null;
		
		Timestamp timestamp = new Timestamp(dataEvento.getTimeInMillis());
		
		try {
			
			sql.append(" SELECT DISTINCT * FROM RAT_ANULACAO_PONTO ");
			sql.append(" WHERE CODIGO_CLIENTE = ? AND RAZAO_SOCIAL = ? AND DT_EVENTO_RECEBIDO = ? AND ZONA_ANULADA = ? ");
			
			conn = PersistEngine.getConnection("TIDB");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setLong(1, codigoCliente);
			pstm.setString(2, razaoSocial);
			pstm.setTimestamp(3, timestamp);
			pstm.setString(4, zonaAnulada);
			
			rs = pstm.executeQuery();
			
			while(rs.next()){
				resultado = true;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			log.warn("ERRO ao Validar o Evento Capturado [Cliente: "+codigoCliente+ " - "+razaoSocial+" | Dat. Recebido: "+timestamp+" | Zona Anulada: "+zonaAnulada+"]");
			System.out.println("ERRO ao Validar o Evento Capturado "+codigoCliente+" - "+razaoSocial+" | "+ e);
		}finally{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return resultado;
		
	}
	

}
