package com.neomind.fusion.custom.casvig.mobile.xml;

import java.util.Map;

import org.w3c.dom.Document;

import com.neomind.fusion.custom.casvig.mobile.xml.entity.MobileXmlInterface;
import com.neomind.fusion.entity.InstantiableEntityInfo;

/**
 * Classe que lê um xml e persiste seus dados num e-form
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public class XmlMobileParser
{
	private InstantiableEntityInfo entityInfo;
	private MobileXmlInterface clazz;

	/**
	 * Construtor da classe XmlMobileParser
	 * 
	 * @param entityInfo
	 * do e-form a ser manipulado
	 * @param clazz
	 * interface que irá iniciar ou dar continuidade a um worklfow
	 */
	public XmlMobileParser(InstantiableEntityInfo entityInfo, MobileXmlInterface clazz)
	{
		this.entityInfo = entityInfo;
		this.clazz = clazz;
	}

	/**
	 * Persite os dados do document passado no e-form
	 * 
	 * @param map
	 * representa o xml a ser persistido
	 * @return se não ocorrer nenhum erro ao persistir o e-form retorna true
	 */
	public boolean persistXml(Document map)
	{
		if (map == null)
		{
			return false;
		}
		else
		{
			if ("com.neomind.fusion.entity.dyn.PesquisaResultado".equals(entityInfo.getEntityClassName()))
			{
				this.clazz.createNewInstance(entityInfo);

				this.clazz.setInstance(map);
				// FIXME usar esses m�todos em vez de chamar direto em cada
				// interface
				// if (clazz instanceof Inspection)
				// this.clazz.startProcess();
				// else if (clazz instanceof Reinspection)
				// this.clazz.finishActivity();
			}

			return true;
		}
	}
}
