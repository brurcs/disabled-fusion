package com.neomind.fusion.custom.orsegups.eform.vo;

import java.util.Date;
import java.util.GregorianCalendar;



public class Chp1VO extends EformVO {

    public Chp1VO() {
	super();
    }
    
    private EformVO empresa;
    private EformVO operadora;
    private EformVO operadora2;
    private EformVO regional;
    private Double valor;
    private String linha;
    private String iccId;
    private GregorianCalendar dataCancelamento;
    private String dataCancelamentoFormatada;
    
    public EformVO getEmpresa() {
        return empresa;
    }
    public void setEmpresa(EformVO empresa) {
        this.empresa = empresa;
    }
    public EformVO getOperadora() {
        return operadora;
    }
    public void setOperadora(EformVO operadora) {
        this.operadora = operadora;
    }
    public EformVO getOperadora2() {
        return operadora2;
    }
    public void setOperadora2(EformVO operadora2) {
        this.operadora2 = operadora2;
    }
    public EformVO getRegional() {
        return regional;
    }
    public void setRegional(EformVO regional) {
        this.regional = regional;
    }
    public Double getValor() {
        return valor;
    }
    public void setValor(Double valor) {
        this.valor = valor;
    }
    public String getLinha() {
        return linha;
    }
    public void setLinha(String linha) {
        this.linha = linha;
    }
    public String getIccId() {
        return iccId;
    }
    public void setIccId(String iccId) {
        this.iccId = iccId;
    }
    public GregorianCalendar getDataCancelamento() {
        return dataCancelamento;
    }
    public void setDataCancelamento(GregorianCalendar dataCancelamento) {
        this.dataCancelamento = dataCancelamento;
    }
    public String getDataCancelamentoFormatada() {
        return dataCancelamentoFormatada;
    }
    public void setDataCancelamentoFormatada(String dataCancelamentoFormatada) {
        this.dataCancelamentoFormatada = dataCancelamentoFormatada;
    }    
}
