package com.neomind.fusion.custom.orsegups.processoJuridico.civel.job;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskStatus;

public class VerificaPrazosProcessoCivel implements CustomJobAdapter
{

	@Override
	public void execute(CustomJobContext ctx)
	{
		QLGroupFilter qlGroup = new QLGroupFilter("AND");
		qlGroup.addFilter(new QLEqualsFilter("processName", "J004 - Processo Cível"));
		qlGroup.addFilter(new QLRawFilter("status <> " + TaskStatus.COMPLETED.ordinal()));

		QLGroupFilter qlGroupAtividade = new QLGroupFilter("OR");
		qlGroupAtividade.addFilter(new QLEqualsFilter("taskName", "Elaborar Petição"));
		qlGroupAtividade.addFilter(new QLEqualsFilter("taskName", "Movimentar Processo"));
		qlGroupAtividade.addFilter(new QLEqualsFilter("taskName", "Inserir Dados do Processo"));
		qlGroupAtividade.addFilter(new QLEqualsFilter("taskName", "Justificar"));
		qlGroup.addFilter(qlGroupAtividade);

		List<TaskInstance> listTaskInstances = PersistEngine.getObjects(TaskInstance.class, qlGroup);

		for (TaskInstance instance : listTaskInstances)
		{
			EntityWrapper wrapper = new EntityWrapper(instance.getActivity().getProcessEntity());

			GregorianCalendar gcStartDate = (GregorianCalendar) instance.getStartDate().clone();
			gcStartDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcStartDate.set(GregorianCalendar.MINUTE, 0);
			gcStartDate.set(GregorianCalendar.SECOND, 0);
			gcStartDate.set(GregorianCalendar.MILLISECOND, 0);

			GregorianCalendar gcToday = new GregorianCalendar();
			gcToday.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcToday.set(GregorianCalendar.MINUTE, 0);
			gcToday.set(GregorianCalendar.SECOND, 0);
			gcToday.set(GregorianCalendar.MILLISECOND, 0);

			Integer dias = 0;
			while (gcToday.getTimeInMillis() > gcStartDate.getTimeInMillis())
			{
				gcStartDate.add(Calendar.DAY_OF_MONTH, 1);

				if (OrsegupsUtils.isWorkDayCalendarioJuridico(gcStartDate))
					dias++;
			}

			switch (instance.getTaskName())
			{
				case "Elaborar Petição":
					if (dias > 10)
					{
						wrapper.setValue("avancaPorJOB", true);
						RuntimeEngine.getTaskService().complete(instance, null);						
					}

					break;

				case "Movimentar Processo":
					if (dias > 90)
					{
						wrapper.setValue("avancaPorJOB", true);
						RuntimeEngine.getTaskService().complete(instance, null);						
					}
					break;
				case "Inserir Dados do Processo":
					if (dias > 5)
					{
						wrapper.setValue("avancaPorJOB", true);
						RuntimeEngine.getTaskService().complete(instance, null);						
					}
					break;
				case "Justificar":
					if (dias > 5)
					{
						wrapper.setValue("avancaPorJOB", true);
						RuntimeEngine.getTaskService().complete(instance, null);
					}
					break;

				default:
					break;
			}
		}
	}

}
