package com.neomind.fusion.custom.orsegups.autocargo.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle.ACVehicle;
import com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle.ACVehicles;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContaSigmaVO;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RotinaFalhaComunicacao implements CustomJobAdapter{ // FalhaComunicacaoVeiculos

	private final Log log = LogFactory.getLog(RotinaFalhaComunicacao.class);

	private Long key = GregorianCalendar.getInstance().getTimeInMillis();

	private SIGMAUtilsEngine engine = new SIGMAUtilsEngine();

	/**
	 * Veiculos com falha de comunicação de pelo 12 horas na data de hoje.
	 * <tt>Rastreador</tt> é a chave do mapa
	 */
	private Map<String, ACVehicle> miscommunication = new HashMap<String, ACVehicle>();

	/**
	 * Contas <tt>rastreadores</tt> que possuem eventos R001 abertos
	 */
	private Map<String, EventoVO> miscommunicationOpenEvents = new HashMap<String, EventoVO>();

	/**
	 * Número total de páginas recebidos da API de consulta de falhas de eventos
	 */
	private int totalPageNumber = 1;

	/**
	 * Inconsistencias na inserção de eventos
	 */
	private List<String> inconsistencias = new ArrayList<String>();

	int j;

	public void execute(CustomJobContext ctx) {

		for (int i = 0; i < totalPageNumber; i++) {

			try {
				this.getMiscommunicationFail(i);
			} catch (RuntimeException e) {
				this.logError(e.getMessage(), "Erro ao acessar API", "Orsegups");
				log.error("RotinaFalhaComunicacao - Erro ao acessar a API Autocargo ACEngineFalhaComunicacao [" + key + "] :");
				e.printStackTrace();
				throw new JobException("Erro ao acessar API de consulta de falha de comunicação Autocargo! Procurar no log por [" + key + "]");
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		boolean getEvents = this.getMiscommunicationEvent();

		int countTry = 0;

		while (!getEvents && countTry < 2) {
			getEvents = this.getMiscommunicationEvent();
			countTry++;

		}

		if (!miscommunication.isEmpty()) {

			for (String key : miscommunication.keySet()) {

				if (miscommunicationOpenEvents.containsKey(key)) {
					/* Já possui evento aberto, não faço nada */
					miscommunicationOpenEvents.remove(key);

				} else {

					ACVehicle v = miscommunication.get(key);

					boolean openOS = this.checkOpenOS(key);

					if (!openOS){
						try {
							String retorno = null;		    		

							if(validoParaGerarEvento12hrs(v) || (!validoParaGerarEvento12hrs(v) && v.getMiscommunication_duration().getDays() >= 10)) {
								retorno = engine.receberEventoByIdentificadorCliente(key, "R001");

								if(validToSendAMail(v)) {
									sendFailCommunicationMail(v);
									finishEvent(key);
								}
							}

							if (!retorno.equals("ACK")) {
								String cliente = "Cliente: "+ v.getClient_name() + "<br/>Veiculo: "+v.getBrand()+" "+v.getModel()+" "
										+ "<br/>Nº Modulo: "+v.getTracking_module().getNumber()+" <br/>Placa: " + v.getRegistration_plate();
								this.logError("R001", retorno, cliente);
								this.inconsistencias.add(retorno + ";" + cliente);
							}
						} catch (EmailException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}		  
				}
			}
		}

		/*
		 * Agora na lista miscommunicationOpenEvents restam apenas os eventos
		 * abertos que não estão mais em falha
		 */

		if (!miscommunicationOpenEvents.isEmpty()) {
			for (String placa : miscommunicationOpenEvents.keySet()) {
				String retorno = engine.receberEventoByIdentificadorCliente(placa, "R002");

				if (!retorno.equals("ACK")) {
					EventoVO v = miscommunicationOpenEvents.get(placa);
					String cliente = "Cliente: " + v.getCodigoCentral() + "[" + v.getParticao() + "] " + v.getFantasia() + " Placa: " + placa;
					this.logError("R002", retorno, cliente);
					this.inconsistencias.add(retorno + ";" + cliente);
				}
			}
		}
		if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == 8 || Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == 14){
			this.sendEmail();	    
		}

	}

	/**
	 * Fecha o evento R001 gerado informando que já foi enviado um e-mail notificando a falha de comunicação
	 * 
	 * @param key Placa do veículo
	 * @return <b> True: </b> Sucesso ao fechar o evento </br>
	 * 		   <b> False: </b> Fracasso ao fechar o evento
	 */
	private Boolean finishEvent(String key) {

		Boolean resultado = false;

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		Connection conn = null;

		String obsFechamento = "Encaminhado um e-mail ao cliente.";

		GregorianCalendar dataAux = new GregorianCalendar();
		Timestamp dataAtual = new Timestamp(dataAux.getTimeInMillis());

		try {

			Long cdHistorico = getCdHistorico(key);

			sql.append(" UPDATE HISTORICO SET FG_STATUS = 4, TX_OBSERVACAO_FECHAMENTO = ?, DT_FECHAMENTO = ?, CD_USUARIO_FECHAMENTO = 11010 ");
			sql.append(" WHERE CD_HISTORICO = ? ");

			conn = PersistEngine.getConnection("SIGMA90");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, obsFechamento);
			pstm.setTimestamp(2, dataAtual);
			pstm.setLong(3, cdHistorico);

			int rs = pstm.executeUpdate();

			if(rs > 0) {
				resultado = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}

		return resultado;
	}

	/**
	 * Consulta os eventos R001 abertos e retorna o CD historico
	 * 
	 * @param key Placa do veículo
	 * @return CD Histórico do evento R001 aberto
	 */
	private Long getCdHistorico(String key) {
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		Connection conn = null;
		ResultSet rs = null;

		Long cdHistorico = 0L;

		try {

			sql.append(" SELECT H.CD_HISTORICO HISTORICO FROM HISTORICO H ");
			sql.append(" INNER JOIN DBCENTRAL C ");
			sql.append(" ON H.CD_CLIENTE = C.CD_CLIENTE ");
			sql.append(" WHERE C.NM_RASTREADOR = ? ");
			sql.append(" AND H.CD_EVENTO = 'R001' ");

			conn = PersistEngine.getConnection("SIGMA90");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, key);

			rs = pstm.executeQuery();

			if(rs.next()) {
				cdHistorico = rs.getLong("HISTORICO");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return cdHistorico;
	}

	/**
	 * Efetua o envio do e-mail de Falha de Comunicação para o cliente responsável pela conta vinculada a placa
	 * 
	 * @param v Objeto que armazena os dados do veículo
	 * @throws EmailException
	 */
	private void sendFailCommunicationMail(ACVehicle v) throws EmailException {

		List<String> mails = new ArrayList<>();
		StringBuilder msg = new StringBuilder();

		String assunto = "Controle de falha de comunicação "+v.getRegistration_plate();
		String emailCliente = getClientEmail(v);

		if(NeoUtils.safeIsNotNull(emailCliente)) {
			mails.add(emailCliente);
		}else {
			mails.add("emailautomatico@orsegups.com.br");
			assunto = assunto + " - CLIENTE SEM EMAIL DO RESPONSÁVEL PREENCHIDO";
		}

		msg.append(" Prezados; ");
		msg.append(" <br> ");
		msg.append(" Conforme Controle de Manuten&ccedil&atildeo Preventiva do Sistema ORSEGUPS, informamos que o(s) ve&iacuteculo(s) listado(s) abaixo est&atildeo sem conex&atildeo com nossa plataforma de Rastreamento. ");
		msg.append(" <br> ");
		msg.append(" <h2><b> "+v.getRegistration_plate()+" </b></h2> ");
		msg.append(" <br> ");
		msg.append(" A falta de comunica&ccedil&atildeo dos dados do seu ve&iacuteculo n&atildeo necessariamente &eacute gerada diante de um problema, a mesma pode ocorrer em 03 (tr&ecircs) situa&ccedil&otildees e precisamos que estejam sendo avaliadas: ");
		msg.append(" <br> ");
		msg.append(" 01) O ve&iacuteculo se encontra momentaneamente em uma &aacuterea de sombra, isto &eacute, sem cobertura do sinal GSM da Operadora de telefonia celular e, portanto, impossibilitado temporariamente de se conectar com a nossa plataforma.  ");
		msg.append(" <br> ");
		msg.append(" 02) O ve&iacuteculo encontra-se em manuten&ccedil&atildeo mec&acircnica/el&eacutetrica ou em outras situa&ccedil&otildees que exijam o desligamento geral da energia do ve&iacuteculo. ");
		msg.append(" <br> ");
		msg.append(" 03) O Rastreador pode se encontrar em pane, exigindo uma inspe&ccedil&atildeo pela nossa equipe t&eacutecnica.. ");
		msg.append(" <br> ");
		msg.append(" Aguardamos retorno para tomar as devidas provid&ecircncias mediante a situa&ccedil&atildeo atual de seu ve&iacuteculo. ");

		OrsegupsEmailUtils.sendEmailRastreamento(mails, msg.toString(), assunto);
	}

	/**
	 * Captura o email do cliente para envio do e-mail de falha de comunicação
	 * 
	 * @param v Objeto que armazena os dados do veículo
	 * @return email do responsável pela conta
	 */
	private String getClientEmail(ACVehicle v) {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Connection conn = null;

		String emailCliente = null;

		try {

			sql.append(" SELECT C.EMAILRESP 'EMAIL' ");
			sql.append(" FROM DBCENTRAL C ");
			sql.append(" WHERE C.NM_RASTREADOR = ? ");

			conn = PersistEngine.getConnection("SIGMA90");

			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, v.getRegistration_plate());

			rs = pstm.executeQuery();

			if(rs.next()) {
				emailCliente = rs.getString("EMAIL");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return emailCliente;
	}

	/**
	 * Método que verifica se o cliente está válido para gerar os eventos no período das 12 horas (estar cadastrado no formulário)
	 * 
	 * @param v Objeto que armazena os dados do veículo
	 * @return <b> True: </b> O cliente está cadastrado no Formulário </br>
	 * 		   <b> False: </b> O cliente não está cadastrado no Formulário
	 */
	private boolean validoParaGerarEvento12hrs(ACVehicle v) {

		boolean retorno = false;

		try {

			ContaSigmaVO contaSigma = new ContaSigmaVO();

			contaSigma = getClientInformationSigma(v.getRegistration_plate());

			NeoObject noCliente = null;

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("conta", contaSigma.getCentral()));
			groupFilter.addFilter(new QLEqualsFilter("particao", Long.parseLong(contaSigma.getCentralParticao())));

			noCliente = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("excecoesClientesFalhaComunicacao"), groupFilter);

			if (NeoUtils.safeIsNotNull(noCliente)) {
				retorno = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### ERRO ao encontrar o cliente "+v.getClient_name()+" do veículo "+v.getRegistration_plate()+" no formulário ");
		}

		return retorno;
	}

	/**
	 * Método que verifica se o cliente está válido para o envio do e-mail de falha de comunicação. 
	 * Para estar apto, o cliente deve ser corporativo, e estar cadastrado no formulário excecoesClientesFalhaComunicacao
	 * 
	 * @param v Objeto que armazena os dados do veículo
	 * @return <b> True: </b> O cliente está válido para o envio do email </br>
	 * 		   <b> False: </b> O cliente não válido para o envio do email
	 */
	private boolean validToSendAMail(ACVehicle v) {

		/*-----------------------------------------------*/
		/*       Contas da Diretoria e Presidência       */
		/*-----------------------------------------------*/

		Set<String> contasExcecoes = new TreeSet<String>();

		contasExcecoes.add("R2268");
		contasExcecoes.add("R2275");
		contasExcecoes.add("R1271");
		contasExcecoes.add("R918");

		/*------------------------------------------------*/

		boolean retorno = false;

		try {

			ContaSigmaVO contaSigma = new ContaSigmaVO();

			contaSigma = getClientInformationSigma(v.getRegistration_plate());

			NeoObject noCliente = null;
			if(!contasExcecoes.contains(contaSigma.getCentral())) {
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("conta", contaSigma.getCentral()));
				groupFilter.addFilter(new QLEqualsFilter("particao", Long.parseLong(contaSigma.getCentralParticao())));

				noCliente = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("excecoesClientesFalhaComunicacao"), groupFilter);    			
			}

			if (NeoUtils.safeIsNotNull(noCliente)) {
				retorno = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### ERRO ao encontrar o cliente "+v.getClient_name()+" do veículo "+v.getRegistration_plate()+" no formulário ");
		}

		return retorno;
	}

	/**
	 * Método utilizado para identificar e armazenar a conta e partição do cliente a partir da Placa do Veículo.
	 * 
	 * @param registration_plate Placa do Veículo
	 * @return Objeto que armazena os dados do cliente
	 */
	private ContaSigmaVO getClientInformationSigma(String registration_plate) {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Connection conn = null;

		ContaSigmaVO contaSigmaVO = new ContaSigmaVO();

		try {

			sql.append(" SELECT C.ID_CENTRAL 'CONTA', C.PARTICAO 'PARTICAO' ");
			sql.append(" FROM DBCENTRAL C ");
			sql.append(" WHERE C.NM_RASTREADOR = ? ");

			conn = PersistEngine.getConnection("SIGMA90");

			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, registration_plate);

			rs = pstm.executeQuery();

			if(rs.next()) {
				contaSigmaVO.setCentral(rs.getString("CONTA"));
				contaSigmaVO.setCentralParticao(rs.getString("PARTICAO"));
			}

			return contaSigmaVO;

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return contaSigmaVO;
	}

	private boolean checkOpenOS(String placa){

		String defeitosOS = this.getDefeitos();

		int count = 0;

		while (defeitosOS.isEmpty() && count < 2){
			defeitosOS = this.getDefeitos();
		}

		if (!defeitosOS.isEmpty()){
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT ID_ORDEM FROM dbORDEM OS WITH(NOLOCK) "); 
			sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON OS.CD_CLIENTE = C.CD_CLIENTE ");
			sql.append(" WHERE (OS.FECHADO = 0 OR OS.FECHAMENTO IS NULL)   ");
			sql.append(" AND C.NM_RASTREADOR ='"+placa+"'");
			sql.append(" AND OS.IDOSDEFEITO IN ("+defeitosOS+")  ");

			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;

			try{

				conn = PersistEngine.getConnection("SIGMA90");

				stmt = conn.createStatement();

				rs = stmt.executeQuery(sql.toString());

				if (rs.next()){
					return true;
				}


			}catch (SQLException e){
				log.error("RotinaFalhaComunicacao Falha ao buscar ordens de serviço abertas");
				e.printStackTrace();
			}finally{
				OrsegupsUtils.closeConnection(conn, stmt, rs);
			}

		}

		return false;
	}

	private String getDefeitos(){

		String codigos = "";

		List<NeoObject> listaExcecoesDefeito = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ATCadastroDefeitoOS"));

		ArrayList<Integer> listaCodDefeito = new ArrayList<Integer>();

		if (listaExcecoesDefeito != null && !listaExcecoesDefeito.isEmpty()) {
			for (NeoObject excecao : listaExcecoesDefeito) {

				EntityWrapper wrappperExcecao = new EntityWrapper(excecao);

				Long codDefeito = (Long) wrappperExcecao.findField("defeito.idosdefeito").getValue();

				listaCodDefeito.add(Integer.valueOf(codDefeito.intValue()));	

			}
		}

		codigos = Joiner.on(",").join(listaCodDefeito);

		return codigos;
	}

	/**
	 * Popula o set {@link #miscommunicationOpenEvents}
	 * 
	 * @return <tt>true</tt> se executou com sucesso <tt>false</tt> caso
	 *         contrário
	 */
	private boolean getMiscommunicationEvent() {

		boolean executouComSucesso = false;

		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT C.NM_RASTREADOR, C.ID_CENTRAL, C.PARTICAO, C.FANTASIA FROM HISTORICO H WITH(NOLOCK) ");
		sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
		sql.append(" WHERE H.CD_EVENTO = 'R001' ");
		sql.append(" ORDER BY DT_PROCESSADO DESC ");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {

			conn = PersistEngine.getConnection("SIGMA90");
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());

			while (rs.next()) {

				EventoVO novoEvento = new EventoVO();

				novoEvento.setCodigoCentral(rs.getString(2));
				novoEvento.setParticao(rs.getString(3));
				novoEvento.setFantasia(rs.getString(4));

				this.miscommunicationOpenEvents.put(rs.getString(1), novoEvento);
			}

			executouComSucesso = true;

		} catch (SQLException e) {
			log.error("RotinaFalhaComunicacao Falha ao buscar eventos de falha de comunicação abertos");
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, stmt, rs);
		}

		return executouComSucesso;

	}

	/**
	 * Popula o mapa {@link #miscommunication}
	 * 
	 * @param pageNumber
	 *            {@link #totalPageNumber} Não é necessário modificar
	 */
	private void getMiscommunicationFail(int pageNumber) {

		String retorno = null;

		try {

			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet("https://www2.autocargo.com.br/api/integration/v1/miscommunication?page=" + pageNumber + "&miscommunication_control=true&miscommunication_duration_limit=12");
			request.addHeader("Authorization", "Token token=\"115fe7a39c6d1910308c335ba3f845a0f9d6495ee912068b9cf7a90f717f03a5\"");
			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");
			HttpResponse response = client.execute(request);

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				retorno = EntityUtils.toString(entity);
			}

			String totalPorPagina = response.getFirstHeader("X-Autocargo-Per-Page").getValue();
			String totalResultados = response.getFirstHeader("X-Autocargo-Total").getValue();

			int total = Integer.parseInt(totalPorPagina);

			int result = this.roundUp(Integer.parseInt(totalResultados), total);

			this.totalPageNumber = result / total;

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException(String.valueOf(response.getStatusLine().getStatusCode()));
			}

		} catch (Exception e) {
			log.error("RotinaFalhaComunicacao - Falha ao buscar falhas de comunicação via API");
			e.printStackTrace();
		}

		if(NeoUtils.safeIsNotNull(retorno)) {
			
			Gson gson = new Gson();
			ACVehicles resultList = gson.fromJson(retorno, ACVehicles.class);

			for (ACVehicle v : resultList.getVehicles()) {
				miscommunication.put(v.getRegistration_plate(), v);
			}
			
		}
	}

	private int roundUp(int toRound, int roundTo) {

		if (toRound % roundTo == 0) {
			return toRound;
		} else {
			return (roundTo - toRound % roundTo) + toRound;
		}

	}

	/**
	 * Gravar de execução na base
	 * 
	 * @param codigoEvento
	 * @param erro
	 * @param cliente
	 */
	private void logError(String codigoEvento, String erro, String cliente) {

		Connection conn = null;
		PreparedStatement pstm = null;

		String sql = "INSERT INTO FalhaComunicacaoVeiculosLogs (CODIGO_EVENTO, ERRO, CLIENTE, DATA_ERRO) VALUES (?,?,?,?)";

		try {
			conn = PersistEngine.getConnection("TIDB");

			pstm = conn.prepareStatement(sql);

			pstm.setString(1, codigoEvento);
			pstm.setString(2, erro);
			pstm.setString(3, cliente);
			pstm.setTimestamp(4, new Timestamp(Calendar.getInstance().getTimeInMillis()));

			pstm.executeUpdate();

		} catch (SQLException e) {
			log.error("RotinaFalhaComunicacao - Falha ao logar falhas de comunicação em TIDB");
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}

	}

	/**
	 * Encaminha e-mail de informe de inconsistências
	 */
	private void sendEmail() {

		if (!this.inconsistencias.isEmpty()) {

			StringBuilder dados = new StringBuilder();

			int cont = 0;

			for (String i : this.inconsistencias) {

				String a[] = i.split(";");

				int resto = (cont) % 2;
				if (resto == 0) {
					dados.append("<tr style=\"background-color: #d6e9f9\" ><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
				} else {
					dados.append("<tr><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
				}

				cont++;
			}

			StringBuilder corpoEmail = new StringBuilder();

			corpoEmail.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
			corpoEmail.append("<html>");
			corpoEmail.append("<head>");
			corpoEmail.append("<meta charset=\"ISO-8859-1\">");
			corpoEmail.append("<title>Relatório de inconsistências</title>");
			corpoEmail.append("<style>");
			corpoEmail.append("h2{");
			corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
			corpoEmail.append("}");
			corpoEmail.append(".table-p{");
			corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
			corpoEmail.append(" }");
			corpoEmail.append(".table-info{");
			corpoEmail.append("table-layout: fixed;");
			corpoEmail.append("text-align:left;");
			corpoEmail.append("font-family: Verdana;");
			corpoEmail.append("}");
			corpoEmail.append(".table-info td{");
			corpoEmail.append("overflow-x: hidden;");
			corpoEmail.append(" }");
			corpoEmail.append(".table-info thead{");
			corpoEmail.append("background-color:#303090;");
			corpoEmail.append("color: white;");
			corpoEmail.append("font-weight:bold;");
			corpoEmail.append("	}");
			corpoEmail.append("</style>");
			corpoEmail.append("</head>");
			corpoEmail.append("<body>");
			corpoEmail.append("<div>");
			corpoEmail.append("<table width=\"600\" align=\"center\">");
			corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
			corpoEmail.append("</table>");
			corpoEmail.append("<td><h2>Inconsistências de falha de comunicação</h2></td>");
			corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
			corpoEmail.append("<tr><td>As seguintes inconsistências ocorreram durante a inserção de eventos de falha de comunicação:</td></tr>");
			corpoEmail.append("</table>");
			corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
			corpoEmail.append("<thead>");
			corpoEmail.append("<tr>");
			corpoEmail.append("<td>Inconsistência</td>");
			corpoEmail.append("<td>Cliente</td>");
			corpoEmail.append("</tr>");
			corpoEmail.append("</thead>");
			corpoEmail.append("<tbody>");
			corpoEmail.append("<!-- Recebe dados do método -->");
			corpoEmail.append(dados);
			corpoEmail.append("<br>");
			corpoEmail.append("</tbody>");
			corpoEmail.append("</table>");
			corpoEmail.append("<br>");
			corpoEmail.append("<!-- Rodapé do e-mail -->");
			corpoEmail.append("<div class=\"rodape\">");
			corpoEmail.append("<table width=\"600\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
			corpoEmail.append("<tbody>");
			corpoEmail.append("<tr>");
			corpoEmail.append("<td width=\"360\" valign=\"bottom\"><img src=\"https://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-1.jpg\"></td>");
			corpoEmail
			.append("<td><a href=\"http://www.orsegups.com.br\" target=\"_blank\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-2.jpg\" width=\"260\" height=\"120\" alt=\"\"/></a></td>");
			corpoEmail.append("</tr>");
			corpoEmail.append("</tbody>");
			corpoEmail.append("</table>");
			corpoEmail.append("</div>");
			corpoEmail.append("</div>");
			corpoEmail.append("</body>");
			corpoEmail.append("</html>");

			String[] addTo = new String[2]; 
			addTo[0] = "emailautomatico@orsegups.com.br" ;
			addTo[1] = "juliana.campos@orsegups.com.br";
			String[] comCopia = new String[] { "emailautomatico@orsegups.com.br"};
			String from = "fusion@orsegups.com.br";
			String subject = "Relatório de Inconsistência de inserção de eventos de falha de comunicação";
			String html = corpoEmail.toString();

			OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);
		}

	}

}
