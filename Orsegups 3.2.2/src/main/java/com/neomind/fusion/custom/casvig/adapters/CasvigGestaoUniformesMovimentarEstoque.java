package com.neomind.fusion.custom.casvig.adapters;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class CasvigGestaoUniformesMovimentarEstoque implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(CasvigGestaoUniformesMovimentarEstoque.class);

	private String urlstr = "";
	private String NOMUSU = "";
	private String SENUSU = "";
	private String nrProcesso = "";

	// valores do eform de parametrizacao
	private String codigoEmpresa = "";
	private String codigoFilial = "";
	private String codigoTransacaoRequisitar = "";
	private String reservarEstoque = "";
	private String codigoUsuarioSolicitante = "";
	private String codigoTransacaoMovimentar = "";

	private Boolean integraComSapiens = true;
	private Boolean descontoTotalFolha = false;

	private String colaboradorFicha = "";
	private String colaboradorFusion = "";

	private NeoObject depositoDescontoFolha;

	private Collection<String> erros = new ArrayList<String>();

	private String errors = " ";

	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		log.warn("INICIAR GESTÃO DE UNIFORMES E MOVIMENTAÇÃO DE ESTOQUE ------ CasvigGestaoUniformesMovimentarEstoque");

		try
		{
			// dados eform de parametrizacao
			NeoObject noParametrosIntegracao = ((List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("FUEParametrosIntegracao"))).get(0);
			EntityWrapper ewParametrosIntegracao = new EntityWrapper(noParametrosIntegracao);
			this.codigoEmpresa = (ewParametrosIntegracao.findValue("codigoEmpresa") != null) ? ewParametrosIntegracao.findValue("codigoEmpresa").toString().trim() : "";
			this.codigoFilial = (ewParametrosIntegracao.findValue("CodigoFilial") != null) ? ewParametrosIntegracao.findValue("CodigoFilial").toString().trim() : "";
			this.codigoTransacaoRequisitar = (ewParametrosIntegracao.findValue("codigoTransacao") != null) ? ewParametrosIntegracao.findValue("codigoTransacao").toString().trim() : "";
			this.reservarEstoque = (ewParametrosIntegracao.findValue("reservaEstoque") != null) ? ewParametrosIntegracao.findValue("reservaEstoque").toString().trim() : "";
			this.codigoUsuarioSolicitante = (ewParametrosIntegracao.findValue("codigoUsuarioSolicitante") != null) ? ewParametrosIntegracao.findValue("codigoUsuarioSolicitante").toString().trim() : "";
			this.codigoTransacaoMovimentar = (ewParametrosIntegracao.findValue("codigoTransacaoMovimentar") != null) ? ewParametrosIntegracao.findValue("codigoTransacaoMovimentar").toString().trim() : "";

			// dados eform de conexao SID
			NeoObject noConexao = ((List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("conexaoSID"))).get(0);
			EntityWrapper ewConexao = new EntityWrapper(noConexao);
			this.NOMUSU = "&NOMUSU=" + ewConexao.findField("login").getValue().toString().trim();
			this.SENUSU = "&SENUSU=" + ewConexao.findField("senha").getValue().toString().trim();
			this.urlstr = ewConexao.findField("host").getValue().toString().trim();

			this.colaboradorFusion = PortalUtil.getCurrentUser().getFullName();
			this.colaboradorFicha = processEntity.findValue("fichaUniformeEPI.colaboradorRH.numcad").toString();
			this.colaboradorFicha += " - " + (String) processEntity.findValue("fichaUniformeEPI.colaboradorRH.nomfun");

			this.integraComSapiens = (Boolean) processEntity.findValue("IntegraComSapiens");

			if (this.integraComSapiens == null)
			{
				this.integraComSapiens = true;
			}

			Boolean devolucaoTotalFicha = (Boolean) processEntity.findValue("fichaUniformeEPI.DevolucaoTotal");
			this.descontoTotalFolha = (Boolean) processEntity.findValue("fichaUniformeEPI.descontoTotalFolha");

			//caso nao haja devolucao total nao tem como realizar desconto total em folha
			if (this.descontoTotalFolha == null || devolucaoTotalFicha == null || devolucaoTotalFicha == false)
			{
				this.descontoTotalFolha = false;
			}
			//caso haja desconto total em folha nao integra com o sapiens
			if (this.descontoTotalFolha)
			{
				this.integraComSapiens = false;
			}

			String SIS = "SIS=CO";
			String LOGIN = "&LOGIN=SID";
			String ACAO = "&ACAO=EXESENHA";
			String PROXACAO = "&PROXACAO=SID.Srv.AltEmpFil";
			String CodEmp = "&CodEmp=" + this.codigoEmpresa;
			String CodFil = "&CodFil=" + this.codigoFilial;
			StringBuffer inputLine = null;
			boolean userAutenticated = true;

			if (processEntity.findValue("wfprocess.neoId") != null)
			{
				this.nrProcesso = processEntity.findValue("wfprocess.neoId").toString();
			}

			if (this.integraComSapiens)
			{
				//URL para autenticar usuÃ¡rio
				String urlSID = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU;
				log.warn("Url a ser executada para autenticar usuario: " + urlSID);

				/* Efetua o login do usuÃ¡rio */
				inputLine = this.callSapiensSID(urlSID);

				if (inputLine != null && (inputLine.toString().contains("ERRO: Usuário e/ou senha inválido") || inputLine.toString().contains("<html>Time-out error...</html>")))
				{
					userAutenticated = false;
					log.info("Usuário e/ou senha inválido");
				}
				log.info("Usuário autenticado!");

				/* Definir Empresa e Filial */
				urlSID = urlSID + PROXACAO + CodEmp + CodFil;
				log.warn("Url a ser executada para definir empresa e filial: " + urlSID);

				inputLine = this.callSapiensSID(urlSID);

				if (inputLine != null && (inputLine.toString().contains("Erro 1: O servidor está temporariamente fora de operação")))
				{
					userAutenticated = false;
					errors = "O servidor está temporariamente fora de operação, entre em contato com a TI.";
					log.error(errors);
					throw new WorkflowException(errors);
				}

				if (inputLine != null && (!inputLine.toString().contains("OK")))
				{
					userAutenticated = false;
					errors = "Não foi possível definir empresa e filial:" + inputLine;
					log.error(errors);
					throw new WorkflowException(errors);
				}
				log.info("Empresa e filial definidas!");
			}
			if ((this.integraComSapiens && userAutenticated) || !this.integraComSapiens)
			{
				PROXACAO = "&PROXACAO=SID.Est.Requisitar";

				NeoObject fichaUniformeEPI = (NeoObject) processEntity.findValue("fichaUniformeEPI");

				Boolean temEntrega = false;
				Boolean temDevolucao = false;

				try
				{
					temEntrega = this.temEntrega(fichaUniformeEPI);
					temDevolucao = this.temDevolucaoEPI(fichaUniformeEPI);
				}
				catch (WorkflowException we)
				{
					throw we;
				}
				catch (Exception e)
				{
					e.printStackTrace();
					errors = "Erro ao validar existencia de entrega/devolução " + e.getMessage();
					log.error(errors);
					throw new WorkflowException(errors);
				}

				//valida se existe um usuÃ¡rio no SAPIEN correspondente ao do Fusion
				//caso nÃ£o exista ja lanÃ§a a excessÃ£o aqui antes de comeÃ§ar a integrar
				buscaUsuarioSapiensCorrespondente(PortalUtil.getCurrentUser().getCode());

				try
				{
					if ((temEntrega) && (temDevolucao))
					{

						this.efetuarEntregaDevolucaoEPI(fichaUniformeEPI);
					}
					else
					{

						if (temEntrega)
						{
							this.efetuarEntregaEPI(fichaUniformeEPI);
						}

						if (temDevolucao)
						{
							this.efetuaDevolucaoEPI(fichaUniformeEPI);
						}
					}

				}
				catch (WorkflowException we)
				{
					throw we;
				}
				catch (Exception e)
				{
					e.printStackTrace();
					errors = "Erro ao realizar entrega ou devolução: " + e.getMessage();
					log.error(errors);
					throw new WorkflowException(errors);
				}

				//caso existam erros 
				if (this.erros != null && !this.erros.isEmpty())
				{
					Collection<NeoObject> errorCollection = new ArrayList<NeoObject>();

					for (String item : this.erros)
					{
						NeoObject erro = AdapterUtils.createNewEntityInstance("FUEListaErros");

						if (erro != null)
						{
							EntityWrapper erroWrapper = new EntityWrapper(erro);
							erroWrapper.setValue("data", new GregorianCalendar());
							erroWrapper.setValue("erro", item);

							errorCollection.add(erro);
						}
					}

					processEntity.setValue("errorList", errorCollection);
					processEntity.setValue("erro", true);

				}
				else
				{
					processEntity.setValue("errorList", new ArrayList<NeoObject>());
					processEntity.setValue("erro", false);
				}
			}
			else
			{
				errors = "Usuário não autenticado, operação abortada!";
				log.error(errors);
				throw new WorkflowException(errors);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(errors);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
		deletaRequisicaoExistente(colaboradorFicha);
	}

	@SuppressWarnings("unchecked")
	public Boolean temDevolucaoEPI(NeoObject fichaUniformeEPI) throws Exception
	{
		EntityWrapper fichaUniformeEPIWrapper = new EntityWrapper(fichaUniformeEPI);

		boolean temDevolucao = false;
		Boolean devolucaoIntegrada = false;
		Boolean devolucaoTotalFicha = false;

		devolucaoTotalFicha = (Boolean) fichaUniformeEPIWrapper.findValue("DevolucaoTotal");
		temDevolucao = devolucaoTotalFicha;

		List<NeoObject> listaEntregas = (List<NeoObject>) fichaUniformeEPIWrapper.findField("listaEntregas").getValue();

		NeoObject depositoDevolucao = (NeoObject) fichaUniformeEPIWrapper.findValue("DepositoDevolucao");
		if (depositoDevolucao == null)
		{
			errors = "Erro: Não foi possivel localizar o deposito de devolução.";
			log.error(errors);
			throw new WorkflowException(errors);
		}

		Boolean descontoItemEmFolha = descontoTotalFolha;

		/* Neste item tambÃ©m valida se quantidade devolvida excede quantidade entregue */
		for (NeoObject noEntrega : listaEntregas)
		{
			Long quantidadeEntregue = 0L;
			Long quantidadeDevolvida = 0L;
			Boolean devolucaoTotalItem = false;

			EntityWrapper wrapperEntrega = new EntityWrapper(noEntrega);

			String desPro = "";
			if (wrapperEntrega != null)
			{
				String situacao = validaSituacaoDoProduto((String) wrapperEntrega.findValue("produto.codpro"));

				if (situacao == "I")
				{
					desPro = (String) wrapperEntrega.findValue("novoProduto.despro");
				}
				else
				{
					desPro = (String) wrapperEntrega.findValue("produto.despro");
				}
			}

			quantidadeEntregue = (Long) wrapperEntrega.findValue("quantidadeEntregue");

			devolucaoTotalItem = (Boolean) wrapperEntrega.findValue("DevolucaoTotal");

			if (!descontoTotalFolha)
			{
				descontoItemEmFolha = (Boolean) wrapperEntrega.findValue("descontoTotalItemFolha");
			}

			List<NeoObject> listaDevolucoes = (List<NeoObject>) wrapperEntrega.findValue("listaDevolucoes");

			for (NeoObject noDevolucao : listaDevolucoes)
			{
				EntityWrapper wrapperDevolucao = new EntityWrapper(noDevolucao);
				quantidadeDevolvida = quantidadeDevolvida + (Long) wrapperDevolucao.findValue("quantidadeDevolucao");

				devolucaoIntegrada = (Boolean) wrapperDevolucao.findValue("devolucaoIntegrada");
				if (devolucaoIntegrada == null)
				{
					devolucaoIntegrada = false;
				}

				if (devolucaoIntegrada == false)
				{
					if (!temDevolucao)
					{
						temDevolucao = true;
					}

					if (descontoItemEmFolha != null && descontoItemEmFolha)
					{
						wrapperDevolucao.setValue("depositoDevolucao", getDepositoDescontoFolha());
					}
					else
					{
						wrapperDevolucao.setValue("depositoDevolucao", depositoDevolucao);
					}

					PersistEngine.persist(noDevolucao);
				}
			}

			if (quantidadeEntregue < quantidadeDevolvida)
			{
				errors = "Erro: Quantidade devolvida excede quantidade entregue do item: " + desPro + " quantidade entregue: " + quantidadeEntregue + " quantidade devolvida: " + quantidadeDevolvida;
				log.error(errors);
				throw new WorkflowException(errors);
			}
			if (quantidadeEntregue > quantidadeDevolvida && (devolucaoTotalItem != null && devolucaoTotalItem == true) || (devolucaoTotalFicha != null && devolucaoTotalFicha == true))
			{
				//tratado desta maneira pois a variavel devolucaoTotalItem estava nula em alguns casos.
				if (!temDevolucao)
				{
					temDevolucao = true;
				}

				InstantiableEntityInfo infoDevolucao = (InstantiableEntityInfo) EntityRegister.getEntityInfo("FUEListaDevolucoes");
				NeoObject noDevolucao = infoDevolucao.createNewInstance();
				EntityWrapper devolucaoEW = new EntityWrapper(noDevolucao);

				Long quantidadeItensFaltantes = quantidadeEntregue - quantidadeDevolvida;

				devolucaoEW.setValue("quantidadeDevolucao", quantidadeItensFaltantes);
				devolucaoEW.setValue("observacao", "");
				devolucaoEW.setValue("devolucaoIntegrada", false);

				if (descontoItemEmFolha != null && descontoItemEmFolha)
				{
					devolucaoEW.setValue("depositoDevolucao", getDepositoDescontoFolha());
				}
				else
				{
					devolucaoEW.setValue("depositoDevolucao", depositoDevolucao);
				}

				PersistEngine.persist(noDevolucao);

				wrapperEntrega.findField("listaDevolucoes").addValue(noDevolucao);

				PersistEngine.merge(noEntrega);
			}
		}

		log.info("Existem Devoluções: " + temDevolucao);
		return temDevolucao;
	}

	@SuppressWarnings("unchecked")
	public void efetuarEntregaEPI(NeoObject fichaUniformeEPI) throws Exception
	{
		EntityWrapper fichaUniformeEPIWrapper = new EntityWrapper(fichaUniformeEPI);

		if (integraComSapiens)
		{
			validaEstoque(fichaUniformeEPI);
		}

		String SIS = "SIS=CO";
		String LOGIN = "&LOGIN=SID";
		String ACAO = "&ACAO=EXESENHA";
		String PROXACAO_REQUISITAR = "&PROXACAO=SID.Est.Requisitar";
		String PROXACAO_ATENDER = "&PROXACAO=SID.Est.AtenderRequisicao";
		String PROXACAO_SOLICITAR_COMPRA = "&PROXACAO=SID.Est.GravarSolCompra";

		StringBuffer inputLine = null;

		log.info("Iniciando inserção de movimentação/entregas no Sapiens via SID");
		List<NeoObject> listaEntregas = (List<NeoObject>) fichaUniformeEPIWrapper.findField("listaEntregas").getValue();

		int iterator = 0;
		String requisicao = "0";

		if (listaEntregas != null)
		{
			for (NeoObject obj : listaEntregas)
			{
				EntityWrapper wrapper = new EntityWrapper(obj);

				String desPro = "";
				if (wrapper != null)
				{
					String situacao = validaSituacaoDoProduto((String) wrapper.findValue("produto.codpro"));

					if (situacao == "I")
					{
						desPro = (String) wrapper.findValue("novoProduto.despro");
					}
					else
					{
						desPro = (String) wrapper.findValue("produto.despro");
					}
				}

				try
				{
					String urls = null;

					Boolean integradoSapiens = false;

					integradoSapiens = (Boolean) wrapper.findField("entregaIntegrada").getValue();

					if (!integradoSapiens)
					{

						if (integraComSapiens)
						{
							Boolean solicitarCompra = (Boolean) wrapper.findValue("solicitarCompra");
							if (solicitarCompra == null)
							{
								solicitarCompra = false;
							}

							String CodPro = "";
							String CodDer = "&CodDer=";
							String CodProAux = wrapper.findField("produto.codpro").getValue().toString();
							String situacao = validaSituacaoDoProduto(CodProAux);

							if (situacao == "I")
							{
								CodPro = "&CodPro=" + wrapper.findField("novoProduto.codpro").getValue().toString();
								if (wrapper.findValue("novaDerivacao") != null && !((String) wrapper.findValue("novaDerivacao.codder")).trim().isEmpty())
								{
									CodDer = "&CodDer=" + wrapper.findValue("novaDerivacao.codder").toString();
								}
								else
								{
									//Senior exige que seja passada um espqco em branco
									try
									{
										CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
									}
									catch (UnsupportedEncodingException e)
									{
										e.printStackTrace();
										errors = "Erro na derivação do produto: " + desPro + " Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
										log.error(errors);
										throw new WorkflowException(errors);
									}
								}
							}
							else
							{
								CodPro = "&CodPro=" + wrapper.findField("produto.codpro").getValue().toString();
								if (wrapper.findValue("derivacao") != null && !((String) wrapper.findValue("derivacao.codder")).trim().isEmpty())
								{
									CodDer = "&CodDer=" + wrapper.findValue("derivacao.codder").toString();
								}
								else
								{
									//Senior exige que seja passada um espqco em branco
									try
									{
										CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
									}
									catch (UnsupportedEncodingException e)
									{
										e.printStackTrace();
										errors = "Erro na derivação do produto: " + desPro + " Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
										log.error(errors);
										throw new WorkflowException(errors);
									}
								}
							}

							//							if(CodDer.contains("+")){
							//								QLGroupFilter groupFilter = new QLGroupFilter("AND");
							//								groupFilter.addFilter(new QLEqualsFilter("codpro", wrapper.findField("produto.codpro").getValue().toString()));
							//								List<NeoObject> listaProdutoDerivacao = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSFUEDER"), groupFilter);
							//								if(NeoUtils.safeIsNotNull(listaProdutoDerivacao) && !listaProdutoDerivacao.isEmpty() && listaProdutoDerivacao.size() > 1){
							//									for(NeoObject neoObjectProdDer: listaProdutoDerivacao){
							//									EntityWrapper ewProd = new EntityWrapper(neoObjectProdDer);
							//									String strProdDer = (String) ewProd.findField("codder").getValue();
							//									if(NeoUtils.safeIsNotNull(strProdDer) && !strProdDer.isEmpty())
							//										throw new WorkflowException("Erro na derivação do produto:  Código: "+CodPro+ " Derivação: "+CodDer);
							//									}
							//								  }            
							//								}

							String CcuRes = "&CcuRes=" + fichaUniformeEPIWrapper.findField("colaboradorRH.codccu").getValue().toString().trim();
							String UsuSol = "&UsuSol=" + codigoUsuarioSolicitante;
							//Busca o observacao da tela e contatena com uma mensagem desistema
							String obsMessage = "Integrado via processo R011 - Manutenção Ficha Uniforme e EPI - código " + nrProcesso;
							obsMessage += " - Colaborador: " + colaboradorFicha;
							obsMessage += " - Integrado por: " + colaboradorFusion;
							String userObs = (String) wrapper.findValue("observacao");
							if (userObs != null && !userObs.equals(""))
							{
								userObs = userObs.replace("&", " ");
								obsMessage += " - " + userObs;
							}

							String CodDep = "&CodDep=" + wrapper.findField("depositoEntrega.depositoSapiens.coddep").getValue().toString();

							// se for compra direta
							if (solicitarCompra)
							{
								String QtdSol = "&QtdSol=" + wrapper.findField("quantidadeEntregue").getValue().toString();

								//TODO criar campos no eform de parametrizacao apra tornar esse campos dinamicos
								String CtaFin = "&CtaFin=15";
								String CtaRed = "&CtaRed=4610";
								String CodTns = "&CodTns=91403";
								String ObsSol = "";
								try
								{
									ObsSol = "&ObsSol=" + URLEncoder.encode(obsMessage, "ISO-8859-1");
								}
								catch (UnsupportedEncodingException e1)
								{
									e1.printStackTrace();
									errors = "Erro: Verifique o tamanho do texto observação do item na solicitação ou um possível erro com o administrador ! Observação - ObsSol : " + ObsSol + " Erro: " + e1.getMessage();
									log.error(errors);
									throw new WorkflowException(errors);
								}
								urls = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO_SOLICITAR_COMPRA + CodPro + CodDer + QtdSol + CcuRes + CtaFin + CtaRed + UsuSol + ObsSol + CodDep + CodTns;

								log.warn("Url a ser executada para solicitar compra: " + urls);
								if (validaRequisicaoExistente(obj, colaboradorFicha) == false)
								{
									inputLine = callSapiensSID(urls);

									if (inputLine != null && inputLine.toString().contains("ERRO"))
									{
										//throw new WorkflowException(inputLine.toString() + " \n URL Entrega: " + urls);
										//throw new WorkflowException("Erro ao executar callSapiensSID verifique - ADMINISTRADOR #1 "+inputLine.toString() + " \n URL: " + urls);
										errors = "Erro ao executar callSapiensSID verifique " + inputLine.toString() + " \n URL Solicitar Compra: " + urls;
										log.error(errors);
										throw new WorkflowException(errors);
									}

									String nrSolicitacaoCompra = this.getNrSolicitacaoCompra(wrapper, obsMessage);

									//alimenta o nr da requisicao do sapiens no eform do fusion
									wrapper.setValue("codigoSapiens", nrSolicitacaoCompra);
									insereRequsicaoExistentes(obj, colaboradorFicha, nrSolicitacaoCompra);
								}

							}
							else
							{
								iterator++;

								String QtdEme = "&QtdEme=" + wrapper.findField("quantidadeEntregue").getValue().toString();
								String NumEme = "&NumEme=" + requisicao;
								String SeqEme = "&SeqEme=" + String.valueOf(iterator);
								String CodTns = "&CodTns=" + codigoTransacaoRequisitar;
								String QtdApr = "&QtdApr=" + wrapper.findField("quantidadeEntregue").getValue().toString();
								String ResEst = "&ResEst=" + reservarEstoque;
								String UsuRes = "&UsuRes=" + buscaUsuarioSapiensCorrespondente(PortalUtil.getCurrentUser().getCode());
								String ObsEme = "";
								try
								{
									ObsEme = "&ObsEme=" + URLEncoder.encode(obsMessage, "ISO-8859-1");
								}
								catch (UnsupportedEncodingException e1)
								{
									e1.printStackTrace();
									errors = "Erro: Verifique o tamanho do texto observação do item na solicitação ou um possível erro com o administrador ! Observação - ObsEme : " + ObsEme + " Erro: " + e1.getMessage();
									log.error(errors);
									throw new WorkflowException(errors);
								}
								urls = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO_REQUISITAR + CodPro + CodDer + QtdEme + NumEme + SeqEme + CodTns + CcuRes + QtdApr + ResEst + CodDep + UsuSol + UsuRes + ObsEme;

								log.warn("Url a ser executada para efetuar entrega: " + urls);
								if (validaRequisicaoExistente(obj, colaboradorFicha) == false)
								{
									inputLine = callSapiensSID(urls);

									if (inputLine != null && inputLine.toString().contains("ERRO"))
									{
										//throw new WorkflowException(inputLine.toString() + " \n URL Entrega: " + urls);
										//this.erros.add("Erro ao realizar entregado item:" + desPro + "! " + inputLine.toString() + " \n URL Entrega: " + urls);
										errors = "Erro ao realizar entrega do item:" + desPro + "! " + inputLine.toString() + " \n URL Entrega: " + urls;
										log.error(errors);
										throw new WorkflowException(errors);

									}

									//alimenta o nr da requisicao do sapiens no eform do fusion
									wrapper.setValue("codigoSapiens", inputLine.toString());

									//Alimenta o nÃºmero da transaÃ§Ã£o para ser utilizado pelos prÃ³ximos
									if (inputLine != null && requisicao.equals("0"))
									{
										//buscaf o nr da requisicao sem o que esta apos a barra
										Integer indexOfSlash = inputLine.indexOf("/");
										if (indexOfSlash != null && indexOfSlash > -1)
										{
											requisicao = inputLine.substring(0, indexOfSlash);
											if (requisicao != null)
											{
												//Para garantir que nao pule linha
												requisicao = requisicao.replaceAll("(\\r|\\n)", "");
											}
										}
									}
									insereRequsicaoExistentes(obj, colaboradorFicha, requisicao);
								}
								else
								{
									requisicao = wrapper.getValue("codigoSapiens").toString();
								}

								//Realizar o atendimento da requisocao
								NumEme = "&NumEme=" + requisicao;
								String QtdAtd = "&QtdAtd=" + wrapper.findField("quantidadeEntregue").getValue().toString();

								String urlAtenderReqisicao = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO_ATENDER + NumEme + SeqEme + QtdAtd;
								log.warn("Url a ser executada para atender req. entrega: " + urlAtenderReqisicao);
								if (validaRequisicaoAtendimentoExistente(obj, colaboradorFicha) == false)
								{
									inputLine = callSapiensSID(urlAtenderReqisicao);

									if (inputLine != null && inputLine.toString().contains("ERRO"))
									{
										//throw new WorkflowException(inputLine.toString() + " \n URL atender entrega: " + urls);
										errors = "Erro ao atender entrega do item:" + desPro + "! " + inputLine.toString() + " \n URL atender entrega: " + urls;
										log.error(errors);
										throw new WorkflowException(errors);
										//continue;
									}

									insereRequsicaoAtendimentoExistentes(obj, colaboradorFicha);

								}
								//Insere a entrega que acabou de ser persistida na lista de histÃ³rico
								insereEntregaHistorico(obj, fichaUniformeEPI, requisicao);
								wrapper.findField("entregaIntegrada").setValue(true);
								wrapper.findField("exibeRelatorio").setValue(true);

								PersistEngine.merge(obj);

							}
						}
					}
				}

				catch (Exception e)
				{
					//this.erros.add("Erro ao integrar entrega do item '" + desPro + "' com o Sapiens! Erro:" + e.getMessage());
					e.printStackTrace();
					errors = "Erro ao integrar entrega do item '" + desPro + "' com o Sapiens! Erro:" + inputLine;
					log.error(errors);
					throw new WorkflowException(errors);

				}
			}
		}
	}

	public void insereEntregaHistorico(NeoObject noEntrega, NeoObject fichaUniformeEPI, String requisicao)
	{
		/* Esta funcao deve ser invocada antes de ser atribuido o valor integrado para a entrega */
		/*
		 * Somente vai inserir os integrados = NÃ£o, pois se for sim significa que jÃ¡ estÃ¡ no
		 * histÃ³rico
		 */

		EntityWrapper wrapperEntrega = new EntityWrapper(noEntrega);
		Boolean integrado = (Boolean) wrapperEntrega.findField("entregaIntegrada").getValue();

		if (!integrado)
		{
			/* Insere o cÃ³digo identificador da entrega para busca posterior */
			wrapperEntrega.findField("codigoEntrega").setValue(String.valueOf(noEntrega.getNeoId()));
			PersistEngine.persist(noEntrega);

			InstantiableEntityInfo infoListaEntregas = (InstantiableEntityInfo) AdapterUtils.getInstantiableEntityInfo("FUEListaEntregas");
			NeoObject cloneNoEntrega = infoListaEntregas.createNewInstance();

			EntityWrapper wrapperCloneEntrega = new EntityWrapper(cloneNoEntrega);

			/* Define os valores padrÃµes que nÃ£o sÃ£o clonados */
			wrapperCloneEntrega.findField("depositoEntrega").setValue(wrapperEntrega.findField("depositoEntrega").getValue());
			wrapperCloneEntrega.findField("produto").setValue(wrapperEntrega.findField("produto").getValue());
			wrapperCloneEntrega.findField("derivacao").setValue(wrapperEntrega.findField("derivacao").getValue());
			wrapperCloneEntrega.findField("observacao").setValue(wrapperEntrega.findField("observacao").getValue().toString());
			wrapperCloneEntrega.findField("quantidadeEntregue").setValue((Long) wrapperEntrega.findField("quantidadeEntregue").getValue());
			wrapperCloneEntrega.findField("codigoEntrega").setValue(wrapperEntrega.findField("codigoEntrega").getValue());
			wrapperCloneEntrega.findField("dataEntrega").setValue(wrapperEntrega.findField("dataEntrega").getValue());
			wrapperCloneEntrega.findField("usuario").setValue(wrapperEntrega.findField("usuario").getValue());
			wrapperCloneEntrega.findField("solicitarCompra").setValue(wrapperEntrega.findValue("solicitarCompra"));
			wrapperCloneEntrega.findField("codigoSapiens").setValue(wrapperEntrega.findValue("codigoSapiens"));
			wrapperCloneEntrega.findField("certificadoAprovacao").setValue(wrapperEntrega.findValue("certificadoAprovacao"));
			wrapperCloneEntrega.findField("entregaIntegrada").setValue(true);

			PersistEngine.persist(cloneNoEntrega);

			EntityWrapper fichaUniformeEPIWrapper = new EntityWrapper(fichaUniformeEPI);

			fichaUniformeEPIWrapper.findField("historicoEntregas").addValue(cloneNoEntrega);
		}
	}

	@SuppressWarnings("unchecked")
	public Boolean temEntrega(NeoObject fichaUniformeEPI)
	{
		EntityWrapper fichaUniformeEPIWrapper = new EntityWrapper(fichaUniformeEPI);

		boolean temEntrega = false;
		boolean entregaIntegrada = false;

		List<NeoObject> listaEntregas = (List<NeoObject>) fichaUniformeEPIWrapper.findField("listaEntregas").getValue();

		NeoObject depositoEntrega = (NeoObject) fichaUniformeEPIWrapper.findValue("DepositoEntrega");
		if (depositoEntrega == null)
		{
			errors = "Erro: Não foi possivel localizar o depósito de entrega.";
			log.error(errors);
			throw new WorkflowException(errors);
		}

		try
		{
			for (NeoObject noEntrega : listaEntregas)
			{
				EntityWrapper wrapperEntrega = new EntityWrapper(noEntrega);

				entregaIntegrada = (Boolean) wrapperEntrega.findField("entregaIntegrada").getValue();

				if (entregaIntegrada == false)
				{
					if (!temEntrega)
					{
						temEntrega = true;
					}
					wrapperEntrega.setValue("depositoEntrega", depositoEntrega);

					PersistEngine.persist(noEntrega);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			errors = "Erro: Ocorreram problemas ao avaliar a existência de entregas. Erro: " + e.getMessage();
			log.error(errors);
			throw new WorkflowException(errors);
		}

		log.info("Existem Entregas: " + temEntrega);

		return temEntrega;
	}

	public void validaEstoque(NeoObject fichaUniformeEPI)
	{
		EntityWrapper fichaUniformeEPIWrapper = new EntityWrapper(fichaUniformeEPI);

		String SIS = "SIS=CO";
		String LOGIN = "&LOGIN=SID";
		String ACAO = "&ACAO=EXESENHA";
		String PROXACAO = "&PROXACAO=SID.Est.BuscarEstoqueDisp";
		StringBuffer inputLine = null;

		log.info("Iniciando inserção de movimentação/entregas no Sapiens via SID");
		List<NeoObject> listaEntregas = (List<NeoObject>) fichaUniformeEPIWrapper.findField("listaEntregas").getValue();

		if (listaEntregas != null)
		{
			for (NeoObject objEntrega : listaEntregas)
			{
				String urls = null;
				EntityWrapper wrapperEntrega = new EntityWrapper(objEntrega);

				Boolean integradoSapiens = false;
				integradoSapiens = (Boolean) wrapperEntrega.findField("entregaIntegrada").getValue();

				if (!integradoSapiens)
				{
					String CodPro = "";
					String CodDer = "&CodDer=";
					String CodProAux = wrapperEntrega.findField("produto.codpro").getValue().toString();
					String situacao = validaSituacaoDoProduto(CodProAux);

					if (situacao == "I")
					{
						CodPro = "&CodPro=" + wrapperEntrega.findField("novoProduto.codpro").getValue().toString();
						if (wrapperEntrega.findValue("novaDerivacao") != null && !((String) wrapperEntrega.findValue("novaDerivacao.codder")).trim().isEmpty())
						{
							CodDer = "&CodDer=" + wrapperEntrega.findValue("novaDerivacao.codder").toString();
						}
						else
						{
							//Senior exige que seja passada um espqco em branco
							try
							{
								CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
							}
							catch (UnsupportedEncodingException e)
							{
								e.printStackTrace();
								errors = "Erro na derivação do produto, Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
								log.error(errors);
								throw new WorkflowException(errors);
							}
						}
					}
					else
					{
						CodPro = "&CodPro=" + wrapperEntrega.findField("produto.codpro").getValue().toString();
						if (wrapperEntrega.findValue("derivacao") != null && !((String) wrapperEntrega.findValue("derivacao.codder")).trim().isEmpty())
						{
							CodDer = "&CodDer=" + wrapperEntrega.findValue("derivacao.codder").toString();
						}
						else
						{
							//Senior exige que seja passada um espqco em branco
							try
							{
								CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
							}
							catch (UnsupportedEncodingException e)
							{
								e.printStackTrace();
								errors = "Erro na derivação do produto, Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
								log.error(errors);
								throw new WorkflowException(errors);
							}
						}
					}

					//					if(CodDer.contains("+")){
					//						QLGroupFilter groupFilter = new QLGroupFilter("AND");
					//						groupFilter.addFilter(new QLEqualsFilter("codpro", wrapperEntrega.findField("produto.codpro").getValue().toString()));
					//						List<NeoObject> listaProdutoDerivacao = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSFUEDER"), groupFilter);
					//						if(NeoUtils.safeIsNotNull(listaProdutoDerivacao) && !listaProdutoDerivacao.isEmpty() && listaProdutoDerivacao.size() > 1){
					//							for(NeoObject neoObjectProdDer: listaProdutoDerivacao){
					//							EntityWrapper ewProd = new EntityWrapper(neoObjectProdDer);
					//							String strProdDer = (String) ewProd.findField("codder").getValue();
					//							if(NeoUtils.safeIsNotNull(strProdDer) && !strProdDer.isEmpty())
					//								throw new WorkflowException("Erro na derivação do produto:  Código: "+CodPro+ " Derivação: "+CodDer);
					//							}
					//						}
					//						}

					String CodDep = "&CodDep=" + wrapperEntrega.findField("depositoEntrega.depositoSapiens.coddep").getValue().toString();

					urls = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO + CodPro + CodDer + CodDep;

					log.warn("Url a ser executada para  validar estoque: " + urls);

					inputLine = callSapiensSID(urls);

					if (inputLine != null && inputLine.toString().contains("ERRO"))
					{
						errors = "Erro ao executar callSapiensSID verifique - ADMINISTRADOR - TI " + inputLine.toString() + " \n URL: " + urls;
						log.error(errors);
						throw new WorkflowException(errors);
					}
					if (inputLine != null && Long.parseLong(inputLine.toString().trim()) <= 0)
					{
						//Caso estoque seja 0 valida se precisa realizar compra direta
						Boolean solicitacaoCompra = false;
						if (situacao == "I")
						{
							solicitacaoCompra = realizaSolicitacaoCompra((String) wrapperEntrega.findValue("novoProduto.codpro"), (String) wrapperEntrega.findValue("depositoEntrega.depositoSapiens.coddep"), (String) wrapperEntrega.findValue("novaDerivacao.codder"));
						}
						else
						{
							solicitacaoCompra = realizaSolicitacaoCompra((String) wrapperEntrega.findValue("produto.codpro"), (String) wrapperEntrega.findValue("depositoEntrega.depositoSapiens.coddep"), (String) wrapperEntrega.findValue("derivacao.codder"));
						}

						if (solicitacaoCompra)
						{
							//Define este item como compra direta
							wrapperEntrega.setValue("solicitarCompra", true);
							PersistEngine.persist(objEntrega);
						}
						else
						{
							//caso nao seja compra direta e nao tenha em estoque retorna mensagem de erro
							String desPro = "";
							situacao = validaSituacaoDoProduto((String) wrapperEntrega.findValue("produto.codpro"));
							if (situacao == "I")
							{
								desPro = (String) wrapperEntrega.findValue("novoProduto.despro");
							}
							else
							{
								desPro = (String) wrapperEntrega.findValue("produto.despro");
							}
							errors = "Erro: Não existe estoque disponível para o item! " + desPro + " Código: " + CodPro + " Derivação: " + CodDer + " Depósito: " + CodDep;
							log.error(errors);
							throw new WorkflowException(errors);
						}
					}
					else
					{
						//caso tenha itens em estoque nao eh compra direta
						wrapperEntrega.setValue("solicitarCompra", false);
						PersistEngine.persist(objEntrega);
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void efetuaDevolucaoEPI(NeoObject fichaUniformeEPI)
	{
		EntityWrapper fichaUniformeEPIWrapper = new EntityWrapper(fichaUniformeEPI);

		String SIS = "SIS=CO";
		String LOGIN = "&LOGIN=SID";
		String ACAO = "&ACAO=EXESENHA";
		String PROXACAO = "&PROXACAO=SID.Est.Movimentar";
		StringBuffer inputLine = null;

		log.info("Iniciando inserção de movimentação/entregas no Sapiens via SID");
		List<NeoObject> listaEntregas = (List<NeoObject>) fichaUniformeEPIWrapper.findField("listaEntregas").getValue();

		int iterator = 0;

		if (listaEntregas != null)
		{
			for (NeoObject objEntrega : listaEntregas)
			{
				String urls = null;
				EntityWrapper wrapperEntrega = new EntityWrapper(objEntrega);

				String desPro = "";
				if (wrapperEntrega != null)
				{
					String situacao = validaSituacaoDoProduto((String) wrapperEntrega.findValue("produto.codpro"));
					if (situacao == "I")
					{
						desPro = (String) wrapperEntrega.findValue("novoProduto.despro");
					}
					else
					{
						desPro = (String) wrapperEntrega.findValue("produto.despro");
					}
				}

				try
				{
					List<NeoObject> listaDevolucoes = (List<NeoObject>) wrapperEntrega.findField("listaDevolucoes").getValue();

					Boolean descontoItemEmFolha = descontoTotalFolha;

					for (NeoObject objDevolucao : listaDevolucoes)
					{
						EntityWrapper wrapperDevolucao = new EntityWrapper(objDevolucao);

						Boolean integradoSapiens = false;

						integradoSapiens = (Boolean) wrapperDevolucao.findField("devolucaoIntegrada").getValue();

						if (!integradoSapiens)
						{
							iterator++;

							if (!descontoTotalFolha)
							{
								descontoItemEmFolha = (Boolean) wrapperEntrega.findValue("descontoTotalItemFolha");
								if (descontoItemEmFolha == null)
								{
									descontoItemEmFolha = false;
								}
							}

							if (integraComSapiens && !descontoItemEmFolha)
							{
								String CodPro = "";
								String CodDer = "&CodDer=";
								String CodProAux = wrapperEntrega.findField("produto.codpro").getValue().toString();
								String situacao = validaSituacaoDoProduto(CodProAux);

								if (situacao == "I")
								{
									CodPro = "&CodPro=" + wrapperEntrega.findField("novoProduto.codpro").getValue().toString();
									if (wrapperEntrega.findValue("novaDerivacao") != null && !((String) wrapperEntrega.findValue("novaDerivacao.codder")).trim().isEmpty())
									{
										CodDer = "&CodDer=" + wrapperEntrega.findValue("novaDerivacao.codder").toString();
									}
									else
									{
										//Senior exige que seja passada um espqco em branco
										try
										{
											CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
										}
										catch (UnsupportedEncodingException e)
										{
											e.printStackTrace();
											errors = "Erro na derivação do produto, Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
											log.error(errors);
											throw new WorkflowException(errors);
										}
									}
								}
								else
								{
									CodPro = "&CodPro=" + wrapperEntrega.findField("produto.codpro").getValue().toString();
									if (wrapperEntrega.findValue("derivacao") != null && !((String) wrapperEntrega.findValue("derivacao.codder")).trim().isEmpty())
									{
										CodDer = "&CodDer=" + wrapperEntrega.findValue("derivacao.codder").toString();
									}
									else
									{
										//Senior exige que seja passada um espqco em branco
										try
										{
											CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
										}
										catch (UnsupportedEncodingException e)
										{
											e.printStackTrace();
											errors = "Erro na derivação do produto, Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
											log.error(errors);
											throw new WorkflowException(errors);
										}
									}
								}

								String CodTns = "&CodTns=" + codigoTransacaoMovimentar;
								String CodDep = "&CodDep=" + wrapperDevolucao.findField("depositoDevolucao.depositoSapiens.coddep").getValue().toString();

								String QtdMov = "&QtdMov=" + String.valueOf(wrapperDevolucao.findField("quantidadeDevolucao").getValue());

								urls = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO + CodPro + CodDer + CodTns + CodDep + QtdMov;

								log.warn("Url a ser executada para efetuar devolucao: " + urls);
								if (validaRequisicaoExistenteDevolucao(objEntrega, objDevolucao, colaboradorFicha) == false)
								{
									inputLine = callSapiensSID(urls);

									if (inputLine != null && inputLine.toString().contains("ERRO"))
									{
										//throw new WorkflowException(inputLine.toString() + " \n URL: " + urls);
										//this.erros.add("Erro ao efetuar devoluÃ§Ã£o do item:" + desPro + "! " + inputLine.toString() + " \n URL efetuar devoluÃ§Ã£o: " + urls);
										errors = "Erro ao efetuar devolução do item:" + desPro + "! " + inputLine.toString() + " \n URL efetuar devolução: " + urls;
										log.error(errors);
										throw new WorkflowException("Erro ao efetuar devolução do item: " + desPro + " Código: " + CodPro + " Depósito: " + CodDep + " " + inputLine.toString() + " \n Verifique a URL passada para efetuar devolução - ADMINISTRADOR. ");
									}
									insereRequsicaoExistentesDevolucao(objEntrega, objDevolucao, colaboradorFicha);

									PersistEngine.persist(objDevolucao);
									//PersistEngine.commit(true);
								}
								insereDevolucoesHistorico(objDevolucao, objEntrega, fichaUniformeEPIWrapper);

								wrapperDevolucao.findField("devolucaoIntegrada").setValue(true);

								if (wrapperEntrega.findValue("exibeRelatorio") == null || (Boolean) wrapperEntrega.findValue("exibeRelatorio") == false)
								{
									wrapperEntrega.setValue("exibeRelatorio", true);
									PersistEngine.persist(objEntrega);
									//PersistEngine.commit(true);
								}
							}
						}
					}
				}
				catch (Exception e)
				{
					//this.erros.add("Erro ao integrar devolução do item '" + desPro + "' com o Sapiens! Erro:" + e.getMessage());
					e.printStackTrace();
					errors = "Erro ao integrar devolução do item '" + desPro + "' com o Sapiens! Erro:" + inputLine;
					log.error(errors);
					throw new WorkflowException(errors);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void insereDevolucoesHistorico(NeoObject noDevolucao, NeoObject noEntrega, EntityWrapper fichaUniformeEPIWrapper)
	{
		/* Esta funcao deve ser invocada antes de ser atribuido o valor integrado para a devolucao */
		/*
		 * Somente vai inserir os integrados = NÃ£o, pois se for sim significa que jÃ¡ estÃ¡ no
		 * histÃ³rico
		 */

		EntityWrapper wrapperDevolucao = new EntityWrapper(noDevolucao);
		EntityWrapper wrapperEntrega = new EntityWrapper(noEntrega);

		String codigoEntrega = (String) wrapperEntrega.findValue("codigoEntrega");

		Boolean integrado = (Boolean) wrapperDevolucao.findValue("devolucaoIntegrada");

		List<NeoObject> listaEntregasHistorico = (List<NeoObject>) fichaUniformeEPIWrapper.findValue("historicoEntregas");

		if (!integrado)
		{
			/* O objetivo aqui Ã© inserir a nova devolucao integrada na lista de histÃ³rico */
			for (NeoObject noEntregaHistorico : listaEntregasHistorico)
			{
				EntityWrapper wrapperEntregaHistorico = new EntityWrapper(noEntregaHistorico);

				/* Necessario para identificar a que devolucao a entrega pertence */
				if (wrapperEntregaHistorico.findField("codigoEntrega").getValue().toString().equals(codigoEntrega))
				{
					InstantiableEntityInfo infoListaEntregas = (InstantiableEntityInfo) AdapterUtils.getInstantiableEntityInfo("FUEListaDevolucoes");
					NeoObject cloneDevolucao = infoListaEntregas.createNewInstance();
					EntityWrapper wrapperCloneEntrega = new EntityWrapper(cloneDevolucao);

					wrapperCloneEntrega.findField("depositoDevolucao").setValue(wrapperDevolucao.findField("depositoDevolucao").getValue());
					wrapperCloneEntrega.findField("quantidadeDevolucao").setValue(wrapperDevolucao.findField("quantidadeDevolucao").getValue());
					wrapperCloneEntrega.findField("observacao").setValue(wrapperDevolucao.findField("observacao").getValue());
					wrapperCloneEntrega.findField("devolucaoIntegrada").setValue(true);

					PersistEngine.persist(cloneDevolucao);

					wrapperEntregaHistorico.findField("listaDevolucoes").addValue(cloneDevolucao);

					PersistEngine.merge(noEntregaHistorico);
				}
			}
		}
	}

	private String buscaUsuarioSapiensCorrespondente(String userCode)
	{
		//Logica para buscar codigo do usuario do sapiens referente ao usuario fusion
		String sapiensUserCode = "";
		NeoObject sapiensUser = null;
		InstantiableEntityInfo ieiEX = AdapterUtils.getInstantiableEntityInfo("SAPIENSUSUARIO");
		String login = userCode;

		QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", login);
		ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>) PersistEngine.getObjects(ieiEX.getEntityClass(), loginFilter);

		if (sapiensUsers != null && sapiensUsers.size() > 0)
		{
			sapiensUser = sapiensUsers.get(0);
		}

		if (sapiensUser != null)
		{
			EntityWrapper sapiensUserEw = new EntityWrapper(sapiensUser);
			sapiensUserCode = sapiensUserEw.findValue("codusu").toString();
		}

		if (sapiensUserCode == null || sapiensUserCode.equals(""))
		{
			errors = "Erro: Não foi possivel buscar o código de usuário sapiens referente ao usuário logado!";
			log.error(errors);
			throw new WorkflowException(errors);

		}

		return sapiensUserCode;
	}

	private StringBuffer callSapiensSID(String url)
	{
		StringBuffer inputLine = new StringBuffer();

		url = url.replaceAll(" ", "%20");
		try
		{

			URL murl = new URL(url);
			URLConnection murlc = murl.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));

			char letter;

			while ((letter = (char) in.read()) != '\uFFFF')
			{
				inputLine.append(letter);
			}

			in.close();

			log.warn("Retorno Sapiens: " + inputLine.toString());
		}
		catch (Exception e)
		{
			inputLine.append("ERRO: Problema de conexão com o Serviço Sapiens SID" + e.getStackTrace());
			e.printStackTrace();
			errors = "Problema de conexão com o Serviço Sapiens SID! Erro: " + inputLine + e.getStackTrace() + e.getMessage() + e.getCause();
			log.error(errors);
			//throw new Exception(errors);

		}
		return inputLine;
	}

	private Boolean realizaSolicitacaoCompra(String codpro, String coddep, String codder)
	{

		Boolean solicitacaoCompra = false;

		try
		{

			if (codder == null)
			{
				codder = "";
			}
			else
			{
				codder = codder.trim();
			}

			QLEqualsFilter produtoFilter = new QLEqualsFilter("codpro", codpro);
			QLEqualsFilter depositoFilter = new QLEqualsFilter("coddep", coddep);
			QLEqualsFilter derivacaoFilter = new QLEqualsFilter("codder", codder);

			QLGroupFilter groupFilter = new QLGroupFilter("AND", produtoFilter, depositoFilter, derivacaoFilter);

			List<NeoObject> objects = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSVIEWFUEPRODEP"), groupFilter);

			if (objects != null && objects.size() > 0)
			{
				NeoObject neoObject = objects.get(0);
				EntityWrapper wrapper = new EntityWrapper(neoObject);

				if (wrapper != null && wrapper.findValue("usu_episol") != null && ((String) wrapper.findValue("usu_episol")).equalsIgnoreCase("S"))
				{
					solicitacaoCompra = true;
				}
			}
		}
		catch (Exception e)
		{

			e.printStackTrace();
			errors = "Erro ao validar compra direta! Parâmetros : CODPRO: " + codpro + " CODDEP: " + coddep + " CODDER: " + codder;
			log.error(errors);
			throw new WorkflowException(errors);
		}

		log.warn("Validacao compra direta! CODPRO: " + codpro + " CODDEP: " + coddep + " CODDER: " + codder + " COMPRA DIRETA: " + solicitacaoCompra);
		return solicitacaoCompra;
	}

	private NeoObject getDepositoDescontoFolha()
	{
		NeoObject depositoDescontoFolha = this.depositoDescontoFolha;

		if (depositoDescontoFolha == null)
		{
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("GCDepositos");

			if (clazz != null)
			{
				QLEqualsFilter equalsFilter = new QLEqualsFilter("nomeDeposito", "Desconto em Folha");

				depositoDescontoFolha = PersistEngine.getNeoObject(clazz, equalsFilter);
			}
		}

		if (depositoDescontoFolha == null)
		{

			errors = "Erro: Não foi possível localizar o depósito 'Desconto em Folha'";
			log.error(errors);
			throw new WorkflowException(errors);
		}
		else
		{
			this.depositoDescontoFolha = depositoDescontoFolha;
		}

		return depositoDescontoFolha;
	}

	@SuppressWarnings("unchecked")
	private String getNrSolicitacaoCompra(EntityWrapper wrapper, String obsMessage)
	{
		String result = "";

		String codPro = "";
		String codDer = " ";
		String CodProAux = (String) wrapper.findValue("produto.codpro");
		String situacao = validaSituacaoDoProduto(CodProAux);

		if (situacao == "I")
		{
			codPro = (String) wrapper.findValue("novoProduto.codpro");
			if (wrapper.findValue("novaDerivacao") != null && !((String) wrapper.findValue("novaDerivacao.codder")).trim().isEmpty())
			{
				codDer = (String) wrapper.findValue("novaDerivacao.codder");
			}
		}
		else
		{
			codPro = (String) wrapper.findValue("produto.codpro");
			if (wrapper.findValue("derivacao") != null && !((String) wrapper.findValue("derivacao.codder")).trim().isEmpty())
			{
				codDer = (String) wrapper.findValue("derivacao.codder");
			}
		}

		Long quantidade = (Long) wrapper.findValue("quantidadeEntregue");
		String codDep = (String) wrapper.findValue("depositoEntrega.depositoSapiens.coddep");

		QLEqualsFilter codProFilter = new QLEqualsFilter("codpro", codPro);
		QLEqualsFilter codDerFilter = new QLEqualsFilter("codder", codDer);
		QLEqualsFilter qtdSolFilter = new QLEqualsFilter("qtdsol", new BigDecimal(quantidade));
		QLEqualsFilter codDepFilter = new QLEqualsFilter("coddep", codDep);
		QLEqualsFilter obsSolFilter = new QLEqualsFilter("obssol", obsMessage);

		QLGroupFilter solicitacaoFilter = new QLGroupFilter("AND");
		solicitacaoFilter.addFilter(codProFilter);
		solicitacaoFilter.addFilter(codDerFilter);
		solicitacaoFilter.addFilter(qtdSolFilter);
		solicitacaoFilter.addFilter(codDepFilter);
		solicitacaoFilter.addFilter(obsSolFilter);

		List<NeoObject> solicitacoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSSOLICITACAOCOMPRAEPI"), solicitacaoFilter);

		if (solicitacoes != null && solicitacoes.size() > 0)
		{
			NeoObject solicitacao = solicitacoes.get(0);
			EntityWrapper solicitacaoWrapper = new EntityWrapper(solicitacao);
			if (solicitacaoWrapper.findValue("numsol") != null)
			{
				result = solicitacaoWrapper.findValue("numsol").toString();
			}
		}
		log.warn("Nº Solicitação de Compra: " + result);

		return result;
	}

	private String validaSituacaoDoProduto(String codPro)
	{
		String situacao = "";

		QLGroupFilter filtroProduto = new QLGroupFilter("AND");
		filtroProduto.addFilter(new QLEqualsFilter("codemp", 1L));
		filtroProduto.addFilter(new QLEqualsFilter("codpro", codPro));
		filtroProduto.addFilter(new QLEqualsFilter("sitpro", "A"));
		List<NeoObject> lstProduto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSFUEPRO"), filtroProduto);
		if (lstProduto != null && lstProduto.size() > 0)
		{
			situacao = "A";
		}
		else
		{
			situacao = "I";
		}

		return situacao;
	}

	public void efetuarEntregaDevolucaoEPI(NeoObject fichaUniformeEPI) throws Exception
	{
		EntityWrapper fichaUniformeEPIWrapper = new EntityWrapper(fichaUniformeEPI);

		if (integraComSapiens)
		{
			validaEstoque(fichaUniformeEPI);
		}

		String SIS = "SIS=CO";
		String LOGIN = "&LOGIN=SID";
		String ACAO = "&ACAO=EXESENHA";
		String PROXACAO_REQUISITAR = "&PROXACAO=SID.Est.Requisitar";
		String PROXACAO_ATENDER = "&PROXACAO=SID.Est.AtenderRequisicao";
		String PROXACAO_SOLICITAR_COMPRA = "&PROXACAO=SID.Est.GravarSolCompra";

		StringBuffer inputLine = null;

		log.info("Iniciando inserção de movimentação/entregas no Sapiens via SID");
		List<NeoObject> listaEntregas = (List<NeoObject>) fichaUniformeEPIWrapper.findField("listaEntregas").getValue();

		int iterator = 0;
		String requisicao = "0";

		if (listaEntregas != null)
		{
			for (NeoObject obj : listaEntregas)
			{
				EntityWrapper wrapper = new EntityWrapper(obj);

				String desPro = "";
				if (wrapper != null)
				{
					String situacao = validaSituacaoDoProduto((String) wrapper.findValue("produto.codpro"));

					if (situacao == "I")
					{
						desPro = (String) wrapper.findValue("novoProduto.despro");
					}
					else
					{
						desPro = (String) wrapper.findValue("produto.despro");
					}
				}

				try
				{
					String urls = null;

					Boolean integradoSapiens = false;

					integradoSapiens = (Boolean) wrapper.findField("entregaIntegrada").getValue();

					if (!integradoSapiens)
					{

						if (integraComSapiens)
						{
							Boolean solicitarCompra = (Boolean) wrapper.findValue("solicitarCompra");
							if (solicitarCompra == null)
							{
								solicitarCompra = false;
							}

							String CodPro = "";
							String CodDer = "&CodDer=";
							String CodProAux = wrapper.findField("produto.codpro").getValue().toString();
							String situacao = validaSituacaoDoProduto(CodProAux);

							if (situacao == "I")
							{
								CodPro = "&CodPro=" + wrapper.findField("novoProduto.codpro").getValue().toString();
								if (wrapper.findValue("novaDerivacao") != null && !((String) wrapper.findValue("novaDerivacao.codder")).trim().isEmpty())
								{
									CodDer = "&CodDer=" + wrapper.findValue("novaDerivacao.codder").toString();
								}
								else
								{
									//Senior exige que seja passada um espqco em branco
									try
									{
										CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
									}
									catch (UnsupportedEncodingException e)
									{
										e.printStackTrace();
										errors = "Erro na derivação do produto: " + desPro + " Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
										log.error(errors);
										throw new WorkflowException(errors);
									}
								}
							}
							else
							{
								CodPro = "&CodPro=" + wrapper.findField("produto.codpro").getValue().toString();
								if (wrapper.findValue("derivacao") != null && !((String) wrapper.findValue("derivacao.codder")).trim().isEmpty())
								{
									CodDer = "&CodDer=" + wrapper.findValue("derivacao.codder").toString();
								}
								else
								{
									//Senior exige que seja passada um espqco em branco
									try
									{
										CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
									}
									catch (UnsupportedEncodingException e)
									{
										e.printStackTrace();
										errors = "Erro na derivação do produto: " + desPro + " Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
										log.error(errors);
										throw new WorkflowException(errors);
									}
								}
							}

							//							if(CodDer.contains("+")){
							//								QLGroupFilter groupFilter = new QLGroupFilter("AND");
							//								groupFilter.addFilter(new QLEqualsFilter("codpro", wrapper.findField("produto.codpro").getValue().toString()));
							//								List<NeoObject> listaProdutoDerivacao = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSFUEDER"), groupFilter);
							//								if(NeoUtils.safeIsNotNull(listaProdutoDerivacao) && !listaProdutoDerivacao.isEmpty() && listaProdutoDerivacao.size() > 1){
							//									for(NeoObject neoObjectProdDer: listaProdutoDerivacao){
							//									EntityWrapper ewProd = new EntityWrapper(neoObjectProdDer);
							//									String strProdDer = (String) ewProd.findField("codder").getValue();
							//									if(NeoUtils.safeIsNotNull(strProdDer) && !strProdDer.isEmpty())
							//										throw new WorkflowException("Erro na derivação do produto:  Código: "+CodPro+ " Derivação: "+CodDer);
							//									}
							//								  }            
							//								}

							String CcuRes = "&CcuRes=" + fichaUniformeEPIWrapper.findField("colaboradorRH.codccu").getValue().toString().trim();
							String UsuSol = "&UsuSol=" + codigoUsuarioSolicitante;
							//Busca o observacao da tela e contatena com uma mensagem desistema
							String obsMessage = "Integrado via processo R011 - Manutenção Ficha Uniforme e EPI - código " + nrProcesso;
							obsMessage += " - Colaborador: " + colaboradorFicha;
							obsMessage += " - Integrado por: " + colaboradorFusion;
							String userObs = (String) wrapper.findValue("observacao");
							if (userObs != null && !userObs.equals(""))
							{
								userObs = userObs.replace("&", " ");
								obsMessage += " - " + userObs;
							}

							String CodDep = "&CodDep=" + wrapper.findField("depositoEntrega.depositoSapiens.coddep").getValue().toString();

							// se for compra direta
							if (solicitarCompra)
							{
								String QtdSol = "&QtdSol=" + wrapper.findField("quantidadeEntregue").getValue().toString();

								//TODO criar campos no eform de parametrizacao apra tornar esse campos dinamicos
								String CtaFin = "&CtaFin=15";
								String CtaRed = "&CtaRed=4610";
								String CodTns = "&CodTns=91403";
								String ObsSol = "";
								try
								{
									ObsSol = "&ObsSol=" + URLEncoder.encode(obsMessage, "ISO-8859-1");
								}
								catch (UnsupportedEncodingException e1)
								{
									e1.printStackTrace();
									errors = "Erro: Verifique o tamanho do texto observação do item na solicitação ou um possível erro com o administrador ! Observação - ObsSol : " + ObsSol + " Erro: " + e1.getMessage();
									log.error(errors);
									throw new WorkflowException(errors);
								}
								urls = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO_SOLICITAR_COMPRA + CodPro + CodDer + QtdSol + CcuRes + CtaFin + CtaRed + UsuSol + ObsSol + CodDep + CodTns;

								log.warn("Url a ser executada para solicitar compra: " + urls);
								efetuaDevolucaoEPI(fichaUniformeEPI);
								if (validaRequisicaoExistente(obj, colaboradorFicha) == false)
								{

									inputLine = callSapiensSID(urls);

									if (inputLine != null && inputLine.toString().contains("ERRO"))
									{
										//throw new WorkflowException(inputLine.toString() + " \n URL Entrega: " + urls);
										//throw new WorkflowException("Erro ao executar callSapiensSID verifique - ADMINISTRADOR #1 "+inputLine.toString() + " \n URL: " + urls);
										errors = "Erro ao executar callSapiensSID verifique " + inputLine.toString() + " \n URL Solicitar Compra: " + urls;
										log.error(errors);
										throw new WorkflowException(errors);
									}

									String nrSolicitacaoCompra = this.getNrSolicitacaoCompra(wrapper, obsMessage);

									//alimenta o nr da requisicao do sapiens no eform do fusion
									wrapper.setValue("codigoSapiens", nrSolicitacaoCompra);
									insereRequsicaoExistentes(obj, colaboradorFicha, nrSolicitacaoCompra);
								}

							}
							else
							{
								iterator++;

								String QtdEme = "&QtdEme=" + wrapper.findField("quantidadeEntregue").getValue().toString();
								String NumEme = "&NumEme=" + requisicao;
								String SeqEme = "&SeqEme=" + String.valueOf(iterator);
								String CodTns = "&CodTns=" + codigoTransacaoRequisitar;
								String QtdApr = "&QtdApr=" + wrapper.findField("quantidadeEntregue").getValue().toString();
								String ResEst = "&ResEst=" + reservarEstoque;
								String UsuRes = "&UsuRes=" + buscaUsuarioSapiensCorrespondente(PortalUtil.getCurrentUser().getCode());
								String ObsEme = "";
								try
								{
									ObsEme = "&ObsEme=" + URLEncoder.encode(obsMessage, "ISO-8859-1");
								}
								catch (UnsupportedEncodingException e1)
								{
									e1.printStackTrace();
									errors = "Erro: Verifique o tamanho do texto observação do item na solicitação ou um possível erro com o administrador ! Observação - ObsEme : " + ObsEme + " Erro: " + e1.getMessage();
									log.error(errors);
									throw new WorkflowException(errors);
								}
								urls = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO_REQUISITAR + CodPro + CodDer + QtdEme + NumEme + SeqEme + CodTns + CcuRes + QtdApr + ResEst + CodDep + UsuSol + UsuRes + ObsEme;

								log.warn("Url a ser executada para efetuar entrega: " + urls);
								if (validaRequisicaoExistente(obj, colaboradorFicha) == false)
								{
									efetuaDevolucaoEPI(fichaUniformeEPI);

									inputLine = callSapiensSID(urls);

									if (inputLine != null && inputLine.toString().contains("ERRO"))
									{
										//throw new WorkflowException(inputLine.toString() + " \n URL Entrega: " + urls);
										//this.erros.add("Erro ao realizar entregado item:" + desPro + "! " + inputLine.toString() + " \n URL Entrega: " + urls);
										errors = "Erro ao realizar entrega do item:" + desPro + "! " + inputLine.toString() + " \n URL Entrega: " + urls;
										log.error(errors);
										throw new WorkflowException(errors);

									}

									//alimenta o nr da requisicao do sapiens no eform do fusion
									wrapper.setValue("codigoSapiens", inputLine.toString());

									//Alimenta o nÃºmero da transaÃ§Ã£o para ser utilizado pelos prÃ³ximos
									if (inputLine != null && requisicao.equals("0"))
									{
										//buscaf o nr da requisicao sem o que esta apos a barra
										Integer indexOfSlash = inputLine.indexOf("/");
										if (indexOfSlash != null && indexOfSlash > -1)
										{
											requisicao = inputLine.substring(0, indexOfSlash);
											if (requisicao != null)
											{
												//Para garantir que nao pule linha
												requisicao = requisicao.replaceAll("(\\r|\\n)", "");
											}
										}
									}
									insereRequsicaoExistentes(obj, colaboradorFicha, requisicao);
								}
								else
								{
									requisicao = wrapper.getValue("codigoSapiens").toString();
								}
								//Realizar o atendimento da requisocao
								NumEme = "&NumEme=" + requisicao;
								String QtdAtd = "&QtdAtd=" + wrapper.findField("quantidadeEntregue").getValue().toString();

								String urlAtenderReqisicao = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO_ATENDER + NumEme + SeqEme + QtdAtd;
								log.warn("Url a ser executada para atender req. entrega: " + urlAtenderReqisicao);
								if (validaRequisicaoAtendimentoExistente(obj, colaboradorFicha) == false)
								{
									inputLine = callSapiensSID(urlAtenderReqisicao);

									if (inputLine != null && inputLine.toString().contains("ERRO"))
									{
										//throw new WorkflowException(inputLine.toString() + " \n URL atender entrega: " + urls);
										errors = "Erro ao atender entrega do item:" + desPro + "! " + inputLine.toString() + " \n URL atender entrega: " + urls;
										log.error(errors);
										throw new WorkflowException(errors);
										//continue;
									}

									insereRequsicaoAtendimentoExistentes(obj, colaboradorFicha);

								}
								//Insere a entrega que acabou de ser persistida na lista de histÃ³rico
								insereEntregaHistorico(obj, fichaUniformeEPI, requisicao);
								wrapper.findField("entregaIntegrada").setValue(true);
								wrapper.findField("exibeRelatorio").setValue(true);

								PersistEngine.merge(obj);

							}
						}

					}
				}
				catch (Exception e)
				{
					//this.erros.add("Erro ao integrar entrega do item '" + desPro + "' com o Sapiens! Erro:" + e.getMessage());
					e.printStackTrace();
					errors = "Erro ao integrar entrega do item '" + desPro + "' com o Sapiens! Erro:" + inputLine;
					log.error(errors);
					throw new WorkflowException(errors);

				}
			}
		}
	}

	private boolean validaRequisicaoExistente(NeoObject obj, String colaborador)
	{
		EntityWrapper wrapperEntrega = new EntityWrapper(obj);
		String produto = (String) wrapperEntrega.findValue("produto.codpro");
		String deposito = (String) wrapperEntrega.findValue("depositoEntrega.depositoSapiens.coddep");
		String derivacao = (String) wrapperEntrega.findValue("derivacao.codder");

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		boolean validar = false;
		String requisicao;

		try
		{

			conn = PersistEngine.getConnection("TIDB");
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT REQUISICAO FROM FUEVERIFICAEPIEXISTENTE ");
			sql.append("WHERE CODIGOPRODUTO = ? ");
			sql.append("AND CODIGODERIVACAO = ? ");
			sql.append("AND CODIGODEPOSITO = ? ");
			sql.append("AND COLABORADOREPI = ? ");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, produto);
			pstm.setString(2, derivacao);
			pstm.setString(3, deposito);
			pstm.setString(4, colaborador);

			rs = pstm.executeQuery();

			while (rs.next())
			{
				validar = true;
				requisicao = rs.getString("requisicao");
				wrapperEntrega.setValue("codigoSapiens", requisicao);
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return validar;
	}

	private void insereRequsicaoExistentes(NeoObject obj, String colaborador, String requisicao)
	{

		EntityWrapper wrapperEntrega = new EntityWrapper(obj);
		String produto = (String) wrapperEntrega.findValue("produto.codpro");
		String deposito = (String) wrapperEntrega.findValue("depositoEntrega.depositoSapiens.coddep");
		String derivacao = (String) wrapperEntrega.findValue("derivacao.codder");

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{

			conn = PersistEngine.getConnection("TIDB");

			String sql = "INSERT INTO FUEVERIFICAEPIEXISTENTE VALUES(?,?,?,?,?)";

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, produto);
			pstm.setString(2, derivacao);
			pstm.setString(3, deposito);
			pstm.setString(4, colaborador);
			pstm.setString(5, requisicao);

			pstm.executeUpdate();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}

	private boolean validaRequisicaoAtendimentoExistente(NeoObject objEntrega, String colaborador)
	{
		EntityWrapper wrapperEntrega = new EntityWrapper(objEntrega);
		String produto = (String) wrapperEntrega.findValue("produto.codpro");
		String deposito = (String) wrapperEntrega.findValue("depositoEntrega.depositoSapiens.coddep");
		String derivacao = (String) wrapperEntrega.findValue("derivacao.codder");

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		boolean validar = false;

		try
		{
			conn = PersistEngine.getConnection("TIDB");
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT * FROM FUEVerificaEPIAtendimentoExistente ");
			sql.append("WHERE CODIGOPRODUTO = ? ");
			sql.append("AND CODIGODERIVACAO = ? ");
			sql.append("AND CODIGODEPOSITO = ? ");
			sql.append("AND COLABORADOREPI = ? ");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, produto);
			pstm.setString(2, derivacao);
			pstm.setString(3, deposito);
			pstm.setString(4, colaborador);

			rs = pstm.executeQuery();

			while (rs.next())
			{
				validar = true;
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return validar;
	}

	private void insereRequsicaoExistentesDevolucao(NeoObject objEntrega, NeoObject objDevolucao, String colaborador)
	{

		EntityWrapper wrapperEntrega = new EntityWrapper(objEntrega);
		EntityWrapper wrapperDevolucao = new EntityWrapper(objDevolucao);
		String produto = (String) wrapperEntrega.findValue("produto.codpro");
		String deposito = (String) wrapperDevolucao.findValue("depositoDevolucao.depositoSapiens.coddep");
		String derivacao = (String) wrapperEntrega.findValue("derivacao.codder");

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("TIDB");

			String sql = "INSERT INTO FUEVERIFICAEPIEXISTENTE VALUES(?,?,?,?,?)";

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, produto);
			pstm.setString(2, derivacao);
			pstm.setString(3, deposito);
			pstm.setString(4, colaborador);
			pstm.setString(5, "Devolução");

			pstm.executeUpdate();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}

	private boolean validaRequisicaoExistenteDevolucao(NeoObject objEntrega, NeoObject objDevolucao, String colaborador)
	{
		EntityWrapper wrapperEntrega = new EntityWrapper(objEntrega);
		EntityWrapper wrapperDevolucao = new EntityWrapper(objDevolucao);
		String produto = (String) wrapperEntrega.findValue("produto.codpro");
		String deposito = (String) wrapperDevolucao.findValue("depositoDevolucao.depositoSapiens.coddep");
		String derivacao = (String) wrapperEntrega.findValue("derivacao.codder");

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		boolean validar = false;

		try
		{
			conn = PersistEngine.getConnection("TIDB");
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT * FROM FUEVERIFICAEPIEXISTENTE ");
			sql.append("WHERE CODIGOPRODUTO = ? ");
			sql.append("AND CODIGODERIVACAO = ? ");
			sql.append("AND CODIGODEPOSITO = ? ");
			sql.append("AND COLABORADOREPI = ? ");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, produto);
			pstm.setString(2, derivacao);
			pstm.setString(3, deposito);
			pstm.setString(4, colaborador);

			rs = pstm.executeQuery();

			while (rs.next())
			{
				validar = true;
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return validar;
	}

	private void insereRequsicaoAtendimentoExistentes(NeoObject objEntrega, String colaborador)
	{

		EntityWrapper wrapperEntrega = new EntityWrapper(objEntrega);
		String produto = (String) wrapperEntrega.findValue("produto.codpro");
		String deposito = (String) wrapperEntrega.findValue("depositoEntrega.depositoSapiens.coddep");
		String derivacao = (String) wrapperEntrega.findValue("derivacao.codder");

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("TIDB");

			String sql = "INSERT INTO FUEVerificaEPIAtendimentoExistente VALUES(?,?,?,?)";

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, produto);
			pstm.setString(2, derivacao);
			pstm.setString(3, deposito);
			pstm.setString(4, colaborador);

			pstm.executeUpdate();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}

	private void deletaRequisicaoExistente(String colaborador)
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("TIDB");

			String sql = "delete from FUEVerificaEPIExistente where colaboradorEPI = ?";
			String sql2 = "delete from FUEVerificaEPIAtendimentoExistente where colaboradorEPI = ?";

			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, colaborador);

			pstm.executeUpdate();
			pstm = null;
			pstm = conn.prepareStatement(sql2.toString());
			pstm.setString(1, colaborador);

			pstm.executeUpdate();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}
}
