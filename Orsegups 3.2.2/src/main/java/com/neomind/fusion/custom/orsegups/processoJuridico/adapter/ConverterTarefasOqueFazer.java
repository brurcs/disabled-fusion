package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterTarefasOqueFazer
public class ConverterTarefasOqueFazer extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		NeoObject objPrincipal = field.getForm().getObject();
		EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);
		List<NeoObject> listaOqueFazerHistorico = (List<NeoObject>) wPrincipal.getValue("j002HistoricoOqueFazer");
		StringBuilder retorno = new StringBuilder();

		retorno.append(" <script type=\"text/javascript\"> function viewItemFusion(id)");
		retorno.append(" {");
		retorno.append(" var title =\"vizualizar formulário\";");
		retorno.append(" var uid;");
		retorno.append(" var entityType = '';");
		retorno.append(" var idx = '';");

		retorno.append(" var portlet = new FloatForm(title, uid, entityType, id, 'ellist_', idx, 0, 0, '', null, null, false, false, null, false, '');");
		retorno.append(" portlet.open();");
		retorno.append("}");
		retorno.append(" 	</script>");

		if (listaOqueFazerHistorico != null && listaOqueFazerHistorico.size() > 0)
		{
			retorno.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			retorno.append("			<tr style=\"cursor: auto\">");
			retorno.append("				<th style=\"cursor: auto\">Titulo</th>");
			retorno.append("				<th style=\"cursor: auto\">Descrição</th>");
			retorno.append("				<th style=\"cursor: auto; white-space: normal\">Prazo</th>");
			retorno.append("                 <th style=\"cursor: auto; white-space: normal\">Tarefas</th>");
			retorno.append("				<th style=\"cursor: auto\">Anexos</th>");
			retorno.append("			</tr>");
			retorno.append("			<tbody>");
			for (NeoObject historico : listaOqueFazerHistorico)
			{
				EntityWrapper wHistorico = new EntityWrapper(historico);

				retorno.append("		<tr>");
				retorno.append("			<td style=\"white-space: normal\">" + wHistorico.getValue("titulo") + "</td>");
				retorno.append("			<td style=\"white-space: normal\">" + wHistorico.getValue("descricao") + "</td>");
				retorno.append("			<td style=\"white-space: normal\">" + NeoDateUtils.safeDateFormat((GregorianCalendar) wHistorico.getValue("prazo"), "dd/MM/yyyy") + "</td>");
				retorno.append("			<td>");
				if (wHistorico.getValue("neoIdTarefa") != null)
				{
					Long neoIdTarefa = (long) wHistorico.getValue("neoIdTarefa");
					WFProcess processo = (WFProcess) PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", neoIdTarefa));
					retorno.append("<img onclick=\"javascript:viewItemFusion(" + neoIdTarefa + ");\" neoid=\"" + neoIdTarefa + "\" id=\"editItem_49873\" class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a>&nbsp;" + processo.getCode() + "<br>");
				}

				retorno.append("</td>");
				StringBuilder sql = new StringBuilder();
				retorno.append("			<td style=\"white-space: normal\">");
				if (wHistorico.getValue("neoIdTarefa") != null)
				{
					Connection conn = null;
					PreparedStatement pstm = null;
					ResultSet rs = null;
					Long neoIdTarefa = (long) wHistorico.getValue("neoIdTarefa");
					conn = PersistEngine.getConnection("");
					sql.append("select ra.anexo_neoId from D_Tarefa t ");
					sql.append("			inner join WFProcess w on w.neoId = t.wfprocess_neoId ");
					sql.append("inner join D_Tarefa_registroAtividades tg on tg.D_Tarefa_neoId = t.neoId ");
					sql.append("inner join D_TarefaRegistroAtividades ra on ra.neoId = tg.registroAtividades_neoId ");
					sql.append("where w.neoId = ? and ra.anexo_neoId is not null ");

					try
					{

						pstm = conn.prepareStatement(sql.toString());
						pstm.setLong(1, neoIdTarefa);
						rs = pstm.executeQuery();

						while (rs.next())
						{
							long neoIdFile = rs.getLong("anexo_neoId");
							NeoFile file = (NeoFile) PersistEngine.getObject(NeoFile.class, new QLEqualsFilter("neoId", neoIdFile));
							retorno.append("	<a style=\"cursor:pointer;\" target='_blank' href=\"" + PortalUtil.getBaseURL() + "file/download/" + file.getNeoId() + "\"'; ");
							retorno.append("			><img src=\"imagens/icones_final/document_search_16x16-trans.png\" align=\"absmiddle\" alt=" + file.getName() + "> ");
							retorno.append("			" + file.getName());
							retorno.append("</a></td>");
							
						}

					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}
			retorno.append("</td>");
			retorno.append("		</tr>");
			retorno.append("		</tbody>");
			retorno.append("		</table>");
		}
		
		return retorno.toString();

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
