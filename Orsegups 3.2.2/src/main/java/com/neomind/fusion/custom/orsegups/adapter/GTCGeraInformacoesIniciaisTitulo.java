package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class GTCGeraInformacoesIniciaisTitulo implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		String mensagem = "";
		Boolean automatico = (Boolean) processEntity.findValue("automatico");
		Boolean isProcessoJuridico = (Boolean) processEntity.findValue("isFluxoJ002");
		NeoObject tipoTituloAux = (NeoObject) processEntity.findValue("tipoTitulo");
		String observacaoAux = (String) processEntity.findValue("observacao");

		try
		{
			Long empresa = (Long) processEntity.findValue("empresa.codemp");
			if (!isProcessoJuridico)
			{
				if (empresa != null && (!origin.getActivityName().equalsIgnoreCase("Selecionar Empresa do Título")))
				{
					processEntity.setValue("automatico", true);
					automatico = (Boolean) processEntity.findValue("automatico");
				}
			}
			processEntity.setValue("isFluxoJ002", false);

			if (automatico && (!origin.getActivityName().equalsIgnoreCase("Validar Pagamento") && !origin.getActivityName().equalsIgnoreCase("Aprovar Pagamento - Diretoria Administrativa") && !origin.getActivityName().equalsIgnoreCase("Executar GTC/GLN")))
			{
				QLGroupFilter filtroFilial = new QLGroupFilter("AND");
				filtroFilial.addFilter(new QLEqualsFilter("codemp", empresa));
				filtroFilial.addFilter(new QLEqualsFilter("codfil", 1L));
				List<NeoObject> filial = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE070FIL"), filtroFilial, -1, -1, " codfil ASC");
				if (filial != null && filial.size() > 0)
				{
					processEntity.setValue("codigoFilial", filial.get(0));
				}
				else
				{
					mensagem = "Não encontrado o Código da Filial para a Empresa informada no FAP!";
					throw new WorkflowException(mensagem);
				}

				if(NeoUtils.safeIsNull(tipoTituloAux)) {
					QLEqualsFilter filtroTipoTitulo = new QLEqualsFilter("codtpt", "01");
					List<NeoObject> tipoTitulo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE002TPT"), filtroTipoTitulo);
					if (tipoTitulo != null && tipoTitulo.size() > 0)
					{
						processEntity.setValue("tipoTitulo", tipoTitulo.get(0));
					}
					else
					{
						mensagem = "Não encontrado o Tipo do Título para lançamento do Título!";
						throw new WorkflowException(mensagem);
					}					
				}

				//Fornecedor cópia de campos no FAP (Adapter - Copiar Campos GTC (Serviço), Copiar Campos GTC (Produto) ou Copiar Campos GTC (Geral))

				//Define a transação do Título (Fixa 90542 para todas as empresas)
				QLGroupFilter filtroTransacao = new QLGroupFilter("AND");
				filtroTransacao.addFilter(new QLEqualsFilter("codemp", empresa));
				filtroTransacao.addFilter(new QLEqualsFilter("codtns", "90542"));
				List<NeoObject> transacoes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE001TNS"), filtroTransacao);
				if (transacoes != null && transacoes.size() > 0)
				{
					processEntity.setValue("transacao", transacoes.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Transação 90542 para lançamento do Título!";
					throw new WorkflowException(mensagem);
				}

				//Data de Entrada é preenchida pelo sistema
				//Data de Emissão é preenchida pelo sistema
				//Valor do Título cópia de campos no FAP (Adapter - Copiar Campos GTC (Serviço), Copiar Campos GTC (Produto) ou Copiar Campos GTC (Geral))

				QLEqualsFilter filtroMoeda = new QLEqualsFilter("codmoe", "01");
				List<NeoObject> moedas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE031MOE"), filtroMoeda);
				if (moedas != null && moedas.size() > 0)
				{
					processEntity.setValue("moeda", moedas.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Moeda para lançamento do Título!";
					throw new WorkflowException(mensagem);
				}

				QLGroupFilter filtroPortador = new QLGroupFilter("AND");
				filtroPortador.addFilter(new QLEqualsFilter("codemp", empresa));
				if(empresa == 15L)
				{
					filtroPortador.addFilter(new QLEqualsFilter("codpor", "999"));
				}
				else
				{
					filtroPortador.addFilter(new QLEqualsFilter("codpor", "9999"));
				}
				List<NeoObject> portadores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE039POR"), filtroPortador);
				if (portadores != null && portadores.size() > 0)
				{
					processEntity.setValue("portador", portadores.get(0));
				}
				else
				{
					mensagem = "Não encontrado o Portador para lançamento do Título!";
					throw new WorkflowException(mensagem);
				}

				QLEqualsFilter filtroCarteira = new QLEqualsFilter("codcrt", "99");
				List<NeoObject> carteiras = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE033CRT"), filtroCarteira);
				if (carteiras != null && carteiras.size() > 0)
				{
					processEntity.setValue("carteira", carteiras.get(0));
				}
				else
				{
					mensagem = "Não encontrado a Carteira para lançamento do Título!";
					throw new WorkflowException(mensagem);
				}

				NeoObject fapAutorizacaoDePagameto = (NeoObject) processEntity.findValue("FapAutorizacaoDePagamento");
				EntityWrapper fapWrapper = new EntityWrapper(fapAutorizacaoDePagameto);
				Boolean fapAgrupadora = (Boolean) fapWrapper.findValue("fapAgrupadora");

				if (fapAgrupadora == null || !fapAgrupadora)
				{
					Long formaPagamento = (Long) fapWrapper.findValue("formaPagamento.codigoFormaPagamento");
					QLGroupFilter filtroformaPagamento = new QLGroupFilter("AND");
					filtroformaPagamento.addFilter(new QLEqualsFilter("codemp", empresa));
					filtroformaPagamento.addFilter(new QLEqualsFilter("codfpg", formaPagamento));
					List<NeoObject> formaPagamentoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE066FPG"), filtroformaPagamento);
					if (formaPagamentoList != null)
					{
						processEntity.setValue("formaPagamento", formaPagamentoList.get(0));
					}
					else
					{
						mensagem = "Não encontrado a Forma de Pagamento Boleto ou Depósito para lançamento do Título!";
						throw new WorkflowException(mensagem);
					}

					String observacao = "";
					Long aplicacaoPagamento = (Long) fapWrapper.findValue("aplicacaoPagamento.codigo");
					String nomeAplicacao = (String) fapWrapper.findValue("aplicacaoPagamento.nome");
					String code = (String) fapWrapper.findValue("wfprocess.code");

					if (aplicacaoPagamento == 1L)
					{
						String viaturaPlaca = (String) fapWrapper.findValue("viatura.placa");
						String tipoViatura = (String) fapWrapper.findValue("tipoViatura");
						String codigoRegional = (String) fapWrapper.findValue("codigoRegional");
						String anoViatura = (String) fapWrapper.findValue("anoViatura");

						if (anoViatura != null)
						{
							anoViatura = (String) fapWrapper.findValue("anoViatura");
						}
						else
						{
							anoViatura = "Ano não informado";
						}

						observacao = nomeAplicacao + " - " + viaturaPlaca + " - " + tipoViatura + " - " + anoViatura + " - Regional: " + codigoRegional + " - FAP: " + code;
					}
					else if ((aplicacaoPagamento == 2L) || (aplicacaoPagamento == 3L) || (aplicacaoPagamento == 4L) || (aplicacaoPagamento == 5L) || 
							 (aplicacaoPagamento == 6L) || (aplicacaoPagamento == 7L) || (aplicacaoPagamento == 8L) || (aplicacaoPagamento == 9L))
					{
						String codigoConta = (String) fapWrapper.findValue("codigoConta");
						String razaoSocialConta = (String) fapWrapper.findValue("razaoSocialConta");
						observacao = nomeAplicacao + " - " + codigoConta + " - " + razaoSocialConta + " - FAP: " + code;
					}

					if(NeoUtils.safeIsNull(observacaoAux)) {
						processEntity.setValue("observacao", observacao);						
					}

					BigDecimal valorOriginal = new BigDecimal(0.00);
					BigDecimal valorOriginalAux = new BigDecimal(0.00);
					BigDecimal valorDesconto = new BigDecimal(0.00);
					
					if(processEntity.findValue("produtoServico") == null){
						if(((Boolean) fapWrapper.findValue("temDesconto")) != null && ((Boolean) fapWrapper.findValue("temDesconto"))){
							valorDesconto = (BigDecimal) processEntity.findValue("valorDesconto");
						}
						valorOriginal = (BigDecimal) processEntity.findValue("valorOriginal");
						valorOriginalAux = valorOriginal;
						valorOriginal = valorOriginal.subtract(valorDesconto);
					}
					else
					{
						String produtoServico = (String) processEntity.findValue("produtoServico");
						
						if(((BigDecimal) fapWrapper.findValue("valorTotalProduto")).compareTo(new BigDecimal(0.00)) > 0 && produtoServico.equals("P"))
						{
							valorOriginal = valorOriginal.add((BigDecimal) fapWrapper.findValue("valorTotalProduto"));
							if(fapWrapper.findValue("tipoDesconto.codigoTipoDesconto") != null && (((Long) fapWrapper.findValue("tipoDesconto.codigoTipoDesconto")) == 1L || ((Long) fapWrapper.findValue("tipoDesconto.codigoTipoDesconto")) == 3L))
							{
								if(((Boolean) fapWrapper.findValue("temDesconto")) != null && ((Boolean) fapWrapper.findValue("temDesconto"))){
									valorDesconto = (BigDecimal) processEntity.findValue("valorDesconto");
								}
							}
						}
						else
						{
							valorOriginal = valorOriginal.add((BigDecimal) fapWrapper.findValue("valorTotalServicos"));
							if(fapWrapper.findValue("tipoDesconto.codigoTipoDesconto") != null && (((Long) fapWrapper.findValue("tipoDesconto.codigoTipoDesconto")) == 2L || ((Long) fapWrapper.findValue("tipoDesconto.codigoTipoDesconto")) == 3L))
							{
								if(((Boolean) fapWrapper.findValue("temDesconto")) != null && ((Boolean) fapWrapper.findValue("temDesconto"))){
									valorDesconto = (BigDecimal) processEntity.findValue("valorDesconto");
								}
							}
						}
						valorOriginalAux = valorOriginal;							
						valorOriginal = valorOriginal.subtract(valorDesconto);
					}
					
					BigDecimal valorTotal = new BigDecimal(0.00);
					valorTotal = (BigDecimal) fapWrapper.findValue("valorTotal");
					
					BigDecimal valorItemRateado = new BigDecimal(0.00);
					if (processEntity.findValue("produtoServico") != null || valorDesconto.compareTo(new BigDecimal(0.00)) > 0)
					{
						List<NeoObject> listaItens = (List<NeoObject>) fapWrapper.findValue("itemOrcamento");
						for (NeoObject itensObject : listaItens)
						{
							EntityWrapper itensWrapper = new EntityWrapper(itensObject);

							BigDecimal valorItem = (BigDecimal) itensWrapper.findValue("valor");
							String tipoItem = (String) itensWrapper.findValue("tipoItem.descricao");
							String tipoLancamento = (String) processEntity.findValue("produtoServico");

							if(NeoUtils.safeIsNull(tipoLancamento) || tipoLancamento.equals("P"))
							{
								if ((tipoItem != null && tipoItem.equals("PRODUTO")))
								{
									valorItemRateado = valorItemRateado.add((BigDecimal) valorItem);
								}
							}
							
							if(NeoUtils.safeIsNull(tipoLancamento) || tipoLancamento.equals("S"))
							{
								if ((tipoItem != null && tipoItem.equals("SERVIÇO")))
								{	
									valorItemRateado = valorItemRateado.add((BigDecimal) valorItem);
								}
							}
						}
					}

					List<NeoObject> listaCentroCusto = (List<NeoObject>) fapWrapper.findField("FapAutorizacaoDePagamento.listaCentroDeCusto").getValue();
					int qtdCentroCusto = listaCentroCusto.size();
					Boolean abrirTitulo = (Boolean) fapWrapper.findValue("abrirTitulo");
					BigDecimal valorRateio = new BigDecimal(0.00);
					
					if (abrirTitulo)
					{
						if (valorDesconto.compareTo(new BigDecimal(0.00)) == 0)
						{
							valorRateio = valorTotal.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
						}
						else
						{
							BigDecimal percentualDesconto = new BigDecimal(0.00);
							BigDecimal percentualDesconto1 = new BigDecimal(0.00);
							BigDecimal percentualDescontoAtu = new BigDecimal(0.00);
							BigDecimal valorDescontoItem = new BigDecimal(0.00);
							percentualDesconto = (valorItemRateado.divide(valorOriginalAux, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
							percentualDesconto1 = (valorItemRateado.divide(valorOriginal, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
							percentualDescontoAtu = percentualDesconto1.subtract(percentualDesconto);
							valorDescontoItem = (valorOriginal.multiply(percentualDescontoAtu)).divide(new BigDecimal(100.00), 4, RoundingMode.HALF_UP);
							valorRateio = valorItemRateado.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
							valorRateio = valorRateio.subtract(valorDescontoItem);
						}
					}
					else
					{
						if (valorDesconto.compareTo(new BigDecimal(0.00)) == 0)
						{
							valorRateio = valorTotal.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
						}
						else
						{
							BigDecimal percentualDesconto = new BigDecimal(0.00);
							BigDecimal percentualDesconto1 = new BigDecimal(0.00);
							BigDecimal percentualDescontoAtu = new BigDecimal(0.00);
							BigDecimal valorDescontoItem = new BigDecimal(0.00);
							percentualDesconto = (valorItemRateado.divide(valorOriginalAux, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
							percentualDesconto1 = (valorItemRateado.divide(valorOriginal, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
							percentualDescontoAtu = percentualDesconto1.subtract(percentualDesconto);
							valorDescontoItem = (valorOriginal.multiply(percentualDescontoAtu)).divide(new BigDecimal(100.00), 4, RoundingMode.HALF_UP);
							valorRateio = valorItemRateado.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
							valorRateio = valorRateio.subtract(valorDescontoItem);
						}
					}
					
					if (listaCentroCusto != null && !listaCentroCusto.isEmpty())
					{
						valorRateio = valorOriginal.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);

						List<NeoObject> listCentroCusto = new ArrayList<NeoObject>();
						List<NeoObject> externoCentroCusto = new ArrayList<NeoObject>();
						BigDecimal valorTotalRateio = new BigDecimal(0.00);

						for (NeoObject obj : listaCentroCusto)
						{
							EntityWrapper wrapper = new EntityWrapper(obj);
							if (wrapper != null)
							{
								NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("GTCListaCentroCusto");
								EntityWrapper centralWrapper = new EntityWrapper(noCentroCusto);

								String codigoCentroCusto = (String) wrapper.findValue("codigoCentroCusto");

								QLGroupFilter filterCcu = new QLGroupFilter("AND");
								filterCcu.addFilter(new QLEqualsFilter("codccu", codigoCentroCusto));
								filterCcu.addFilter(new QLEqualsFilter("codemp", empresa));
								filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));

								externoCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

								if (externoCentroCusto != null && !externoCentroCusto.isEmpty())
								{
									centralWrapper.setValue("codigoCentroCusto", externoCentroCusto.get(0));
									centralWrapper.setValue("valorRateio", valorRateio);

									valorTotalRateio = valorTotalRateio.add(valorRateio);
									listCentroCusto.add(noCentroCusto);
								}
								else
								{
									mensagem = "Centro de Custo " + codigoCentroCusto + " não localizado! Por favor, verificar o Centro de Custo associado ao FAP e se o mesmo existe na Empresa de lançamento do Título!";
									throw new WorkflowException(mensagem);
								}
							}
						}

						BigDecimal valorAjustado = new BigDecimal(0.00);
						int resultado = valorOriginal.compareTo(valorTotalRateio);
						BigDecimal valorSobra = new BigDecimal(0.00);
						valorOriginal.plus(new MathContext(2, RoundingMode.HALF_UP));
						valorSobra = valorTotalRateio.subtract(valorOriginal);
						
						NeoObject oCentroCusto = listCentroCusto.get(listaCentroCusto.size() - 1);
						EntityWrapper wrapperCentroCusto = new EntityWrapper(oCentroCusto);
						valorRateio = (BigDecimal) wrapperCentroCusto.findValue("valorRateio");

						if (resultado == 1)
						{
							BigDecimal retorno = valorRateio.add(valorSobra);
							valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
							wrapperCentroCusto.setValue("valorRateio", valorAjustado);
						}
						else if (resultado == -1)
						{
							BigDecimal retorno = valorRateio.subtract(valorSobra);
							valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
							wrapperCentroCusto.setValue("valorRateio", valorAjustado);
						}

						processEntity.setValue("valorOriginal", valorTotalRateio);
						QLEqualsFilter filtroLancamento = new QLEqualsFilter();
						if (qtdCentroCusto > 1)
						{
							filtroLancamento = new QLEqualsFilter("codigoLancamento", 2);
							processEntity.setValue("listaCentroCusto", listCentroCusto);
						}
						else
						{
							filtroLancamento = new QLEqualsFilter("codigoLancamento", 1);
							processEntity.setValue("centroCusto", externoCentroCusto.get(0));
						}

						NeoObject listaLancamento = PersistEngine.getObject(AdapterUtils.getEntityClass("GTCTipoLancamento"), filtroLancamento);
						if (listaLancamento != null)
						{
							processEntity.setValue("listaLancamento", listaLancamento);
						}
						else
						{
							mensagem = "Não encontrado a Forma de Lançamento do Rateio para lançamento do Titulo!";
							throw new WorkflowException(mensagem);
						}
					}
					else
					{
						mensagem = "Não encontrado Centro de Custo para o lançamento do Título!";
						throw new WorkflowException(mensagem);
					}
				}
				else
				{					
					Long formaPagamento = (Long) fapWrapper.findValue("formaPagamento.codigoFormaPagamento");
					QLGroupFilter filtroformaPagamento = new QLGroupFilter("AND");
					filtroformaPagamento.addFilter(new QLEqualsFilter("codemp", empresa));
					filtroformaPagamento.addFilter(new QLEqualsFilter("codfpg", formaPagamento));
					List<NeoObject> formaPagamentoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE066FPG"), filtroformaPagamento);
					if (formaPagamentoList != null)
					{
						processEntity.setValue("formaPagamento", formaPagamentoList.get(0));
					}
					else
					{
						mensagem = "Não encontrado a Forma de Pagamento Boleto ou Depósito para lançamento do Título!";
						throw new WorkflowException(mensagem);
					}
					
					List<NeoObject> listaFapsAgrupadas = new ArrayList<NeoObject>();

					String codigoTarefa = (String) fapWrapper.findValue("wfprocess.code");
					QLEqualsFilter codigoTarefaFilter = new QLEqualsFilter("codigoTarefa", codigoTarefa);
					listaFapsAgrupadas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), codigoTarefaFilter);

					if (NeoUtils.safeIsNotNull(listaFapsAgrupadas) && !listaFapsAgrupadas.isEmpty())
					{
						List<NeoObject> listCentroCusto = new ArrayList<NeoObject>();

						Map<Long, NeoObject> ordenaCentroCusto = new HashMap<Long, NeoObject>();
						Map<Long, BigDecimal> somaValorCentro = new HashMap<Long, BigDecimal>();

						BigDecimal valorTotalAgrupado = new BigDecimal(0.00);
						BigDecimal valorTotalAgrupadoAux = new BigDecimal(0.00);
						BigDecimal valorDesconto = new BigDecimal(0.00);
						
						if(processEntity.findValue("produtoServico") == null){
							for (NeoObject neoObject5 : listaFapsAgrupadas)
							{
								EntityWrapper registroWrapper = new EntityWrapper(neoObject5);
								valorTotalAgrupado = valorTotalAgrupado.add((BigDecimal) registroWrapper.findValue("valorTotal"));
								valorTotalAgrupadoAux = valorTotalAgrupado;
							}
							
							if((Boolean) fapWrapper.findValue("temDesconto")){
								valorDesconto = (BigDecimal) processEntity.findValue("valorDesconto");
							}
							valorTotalAgrupado = valorTotalAgrupado.subtract(valorDesconto);
						}
						else
						{
							if(((BigDecimal) fapWrapper.findValue("valorTotalProduto")).compareTo(new BigDecimal(0.00)) > 0 && processEntity.findValue("produtoServico").equals("P"))
							{
								valorTotalAgrupado = valorTotalAgrupado.add((BigDecimal) fapWrapper.findValue("valorTotalProduto"));
								if(fapWrapper.findValue("tipoDesconto.codigoTipoDesconto") != null && (((Long) fapWrapper.findValue("tipoDesconto.codigoTipoDesconto")) == 1L || ((Long) fapWrapper.findValue("tipoDesconto.codigoTipoDesconto")) == 3L))
								{
									if((Boolean) fapWrapper.findValue("temDesconto")){
										valorDesconto = (BigDecimal) processEntity.findValue("valorDesconto");
									}
								}
							}
							else
							{
								if(fapWrapper.findValue("tipoDesconto.codigoTipoDesconto") != null && (((Long) fapWrapper.findValue("tipoDesconto.codigoTipoDesconto")) == 2L || ((Long) fapWrapper.findValue("tipoDesconto.codigoTipoDesconto")) == 3L))
								{
									if((Boolean) fapWrapper.findValue("temDesconto")){
										valorDesconto = (BigDecimal) processEntity.findValue("valorDesconto");
									}
								}
								valorTotalAgrupado = valorTotalAgrupado.add((BigDecimal) fapWrapper.findValue("valorTotalServicos"));
							}
							
							valorTotalAgrupadoAux = valorTotalAgrupado;							
							valorTotalAgrupado = valorTotalAgrupado.subtract(valorDesconto);
						}

						BigDecimal valorTotalRateio = new BigDecimal(0.00);
						String observacao = "";
						String tarefasAgrupadas = "";

						for (NeoObject neoObject : listaFapsAgrupadas)
						{
							EntityWrapper registroWrapper = new EntityWrapper(neoObject);
							BigDecimal valorTotal = new BigDecimal(0.00);
							valorTotal = (BigDecimal) registroWrapper.findValue("valorTotal");

							if (codigoTarefa != registroWrapper.findValue("wfprocess.code"))
							{
								if (tarefasAgrupadas != null && !tarefasAgrupadas.isEmpty())
								{
									tarefasAgrupadas += ", ";
								}								
								tarefasAgrupadas = tarefasAgrupadas + registroWrapper.findValue("wfprocess.code");
							}							

							BigDecimal valorItemRateado = new BigDecimal(0.00);
							if (processEntity.findValue("produtoServico") != null || valorDesconto.compareTo(new BigDecimal(0.00)) > 0)
							{
								List<NeoObject> listaItens = (List<NeoObject>) registroWrapper.findValue("itemOrcamento");
								for (NeoObject itensObject : listaItens)
								{
									EntityWrapper itensWrapper = new EntityWrapper(itensObject);

									BigDecimal valorItem = (BigDecimal) itensWrapper.findValue("valor");
									String tipoItem = (String) itensWrapper.findValue("tipoItem.descricao");
									String tipoLancamento = (String) processEntity.findValue("produtoServico");

									if(NeoUtils.safeIsNull(tipoLancamento) || tipoLancamento.equals("P"))
									{
										if ((tipoItem != null && tipoItem.equals("PRODUTO")))
										{
											valorItemRateado = valorItemRateado.add((BigDecimal) valorItem);
										}
									}
									
									if(NeoUtils.safeIsNull(tipoLancamento) || tipoLancamento.equals("S"))
									{
										if ((tipoItem != null && tipoItem.equals("SERVIÇO")))
										{	
											valorItemRateado = valorItemRateado.add((BigDecimal) valorItem);
										}
									}
								}
							}

							List<NeoObject> listaCentroDeCusto = (List<NeoObject>) registroWrapper.findValue("listaCentroDeCusto");
							int qtdCentroCusto = listaCentroDeCusto.size();
							Boolean abrirTitulo = (Boolean) fapWrapper.findValue("abrirTitulo");
							BigDecimal valorRateio = new BigDecimal(0.00);
							if (abrirTitulo)
							{
								if (valorDesconto.compareTo(new BigDecimal(0.00)) == 0)
								{
									valorRateio = valorTotal.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
								}
								else
								{
									BigDecimal percentualDesconto = new BigDecimal(0.00);
									BigDecimal percentualDesconto1 = new BigDecimal(0.00);
									BigDecimal percentualDescontoAtu = new BigDecimal(0.00);
									BigDecimal valorDescontoItem = new BigDecimal(0.00);
									percentualDesconto = (valorItemRateado.divide(valorTotalAgrupadoAux, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
									percentualDesconto1 = (valorItemRateado.divide(valorTotalAgrupado, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
									percentualDescontoAtu = percentualDesconto1.subtract(percentualDesconto);
									valorDescontoItem = (valorTotalAgrupado.multiply(percentualDescontoAtu)).divide(new BigDecimal(100.00), 4, RoundingMode.HALF_UP);
									valorRateio = valorItemRateado.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
									valorRateio = valorRateio.subtract(valorDescontoItem);
								}
							}
							else
							{
								if (valorDesconto.compareTo(new BigDecimal(0.00)) == 0)
								{
									valorRateio = valorTotal.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
								}
								else
								{
									BigDecimal percentualDesconto = new BigDecimal(0.00);
									BigDecimal percentualDesconto1 = new BigDecimal(0.00);
									BigDecimal percentualDescontoAtu = new BigDecimal(0.00);
									BigDecimal valorDescontoItem = new BigDecimal(0.00);
									percentualDesconto = (valorItemRateado.divide(valorTotalAgrupadoAux, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
									percentualDesconto1 = (valorItemRateado.divide(valorTotalAgrupado, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100.00)).divide(new BigDecimal(qtdCentroCusto), 4, RoundingMode.HALF_UP);
									percentualDescontoAtu = percentualDesconto1.subtract(percentualDesconto);
									valorDescontoItem = (valorTotalAgrupado.multiply(percentualDescontoAtu)).divide(new BigDecimal(100.00), 4, RoundingMode.HALF_UP);
									valorRateio = valorItemRateado.divide(new BigDecimal(qtdCentroCusto), 2, RoundingMode.HALF_UP);
									valorRateio = valorRateio.subtract(valorDescontoItem);
								}
							}

							List<NeoObject> externoCentroCusto = new ArrayList<NeoObject>();
							for (NeoObject neoObject2 : listaCentroDeCusto)
							{
								EntityWrapper wrapper = new EntityWrapper(neoObject2);
								if (wrapper != null)
								{
									NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("GTCListaCentroCusto");
									EntityWrapper centralWrapper = new EntityWrapper(noCentroCusto);

									String codigoCentroCusto = (String) wrapper.findValue("codigoCentroCusto");

									QLGroupFilter filterCcu = new QLGroupFilter("AND");
									filterCcu.addFilter(new QLEqualsFilter("codccu", codigoCentroCusto));
									filterCcu.addFilter(new QLEqualsFilter("codemp", empresa));
									filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));

									externoCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

									if (externoCentroCusto != null && !externoCentroCusto.isEmpty())
									{
										centralWrapper.setValue("codigoCentroCusto", externoCentroCusto.get(0));
										centralWrapper.setValue("valorRateio", valorRateio);

										valorTotalRateio = valorTotalRateio.add(valorRateio);
										valorTotalRateio.setScale(4, RoundingMode.HALF_UP);

										if (somaValorCentro != null && !somaValorCentro.containsKey(Long.parseLong(codigoCentroCusto)))
										{
											somaValorCentro.put(Long.parseLong(codigoCentroCusto), valorRateio);
										}
										else if (somaValorCentro != null && somaValorCentro.containsKey(Long.parseLong(codigoCentroCusto)))
										{
											BigDecimal valorRateioAux = new BigDecimal(0.00);
											valorRateioAux = somaValorCentro.get(Long.parseLong(codigoCentroCusto));
											valorRateioAux = valorRateioAux.add(valorRateio);
											somaValorCentro.put(Long.parseLong(codigoCentroCusto), valorRateioAux);
										}

										if (ordenaCentroCusto != null && !ordenaCentroCusto.containsKey(codigoCentroCusto))
										{
											ordenaCentroCusto.put(Long.parseLong(codigoCentroCusto), noCentroCusto);
										}
									}
									else
									{
										mensagem = "Centro de Custo " + codigoCentroCusto + " não localizado! Por favor, verificar o Centro de Custo associado ao FAP e se o mesmo existe na Empresa de lançamento do Título!";
										throw new WorkflowException(mensagem);
									}
								}
							}
						}

						Set ordenaCCU = ordenaCentroCusto.entrySet();
						Iterator ordenaCcuIterator = ordenaCCU.iterator();
						while (ordenaCcuIterator.hasNext())
						{
							Map.Entry me = (Map.Entry) ordenaCcuIterator.next();
							EntityWrapper centroCusto = new EntityWrapper((NeoObject) me.getValue());
							centroCusto.findField("valorRateio").setValue(somaValorCentro.get(me.getKey()));

							listCentroCusto.add((NeoObject) me.getValue());
						}

						int resultado = valorTotalAgrupado.compareTo(valorTotalRateio);
						BigDecimal valorSobra = new BigDecimal(0.00);
						valorTotalAgrupado.plus(new MathContext(2, RoundingMode.HALF_UP));
						valorSobra = valorTotalRateio.subtract(valorTotalAgrupado);

						BigDecimal valorAjustado = new BigDecimal(0.00);
						BigDecimal valorRateio = new BigDecimal(0.00);
						
						NeoObject oCentroCusto = listCentroCusto.get(listCentroCusto.size() - 1);
						EntityWrapper wrapperCentroCusto = new EntityWrapper(oCentroCusto);
						valorRateio = (BigDecimal) wrapperCentroCusto.findValue("valorRateio");
						
						if (resultado == 1)
						{
							BigDecimal retorno = valorRateio.subtract(valorSobra);
							valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
							wrapperCentroCusto.setValue("valorRateio", valorAjustado);
						}
						else if (resultado == -1)
						{
							BigDecimal retorno = valorRateio.subtract(valorSobra);
							valorAjustado = retorno.setScale(2, RoundingMode.HALF_UP);
							wrapperCentroCusto.setValue("valorRateio", valorAjustado);
						}

						QLEqualsFilter filtroLancamento = new QLEqualsFilter("codigoLancamento", 2);
						processEntity.setValue("listaCentroCusto", listCentroCusto);
						processEntity.setValue("valorOriginal", valorTotalAgrupado);
						observacao = "Ref. FAP AGR: " + codigoTarefa + " (" + tarefasAgrupadas + ")";
						
						if(NeoUtils.safeIsNull(observacaoAux)) {
							processEntity.setValue("observacao", observacao);
						}

						NeoObject listaLancamento = PersistEngine.getObject(AdapterUtils.getEntityClass("GTCTipoLancamento"), filtroLancamento);
						if (listaLancamento != null)
						{
							processEntity.setValue("listaLancamento", listaLancamento);
						}
						else
						{
							mensagem = "Não encontrado a Forma de Lançamento do Rateio para lançamento do Titulo!";
							throw new WorkflowException(mensagem);
						}
					}
				}
			}
			else
			{
				if (empresa != null)
				{
					if(NeoUtils.safeIsNull(tipoTituloAux)) {
						QLEqualsFilter filtroTipoTitulo = new QLEqualsFilter("codtpt", "01");
						List<NeoObject> tipoTitulo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE002TPT"), filtroTipoTitulo);
						if (tipoTitulo != null && tipoTitulo.size() > 0)
						{
							processEntity.setValue("tipoTitulo", tipoTitulo.get(0));
						}
						else
						{
							mensagem = "Não encontrado o Tipo do Título para lançamento do Título!";
							throw new WorkflowException(mensagem);
						}
					}

					/*QLGroupFilter groupFilter = new QLGroupFilter("AND");
					QLEqualsFilter filtroEmpresa = new QLEqualsFilter("codemp", empresa);
					QLEqualsFilter filtroTransacao1 = new QLEqualsFilter("codtns", "90542");
					QLGroupFilter statusGroupFilter = new QLGroupFilter("OR");
					QLEqualsFilter filtroTransacao2 = new QLEqualsFilter("codtns", "90529");
					statusGroupFilter.addFilter(filtroTransacao1);
					statusGroupFilter.addFilter(filtroTransacao2);
					groupFilter.addFilter(filtroEmpresa);
					groupFilter.addFilter(statusGroupFilter);									
					List<NeoObject> transacoes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE001TNS"), groupFilter);
					if (transacoes != null && transacoes.size() > 0)
					{
						processEntity.setValue("transacao", transacoes.get(0));
					}
					else
					{
						mensagem = "Não encontrado a Transação 90542 para lançamento do Título!";
						throw new WorkflowException(mensagem);
					}*/

					QLEqualsFilter filtroMoeda = new QLEqualsFilter("codmoe", "01");
					List<NeoObject> moedas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE031MOE"), filtroMoeda);
					if (moedas != null && moedas.size() > 0)
					{
						processEntity.setValue("moeda", moedas.get(0));
					}
					else
					{
						mensagem = "Não encontrado a Moeda para lançamento do Título!";
						throw new WorkflowException(mensagem);
					}

					QLGroupFilter filtroPortador = new QLGroupFilter("AND");
					filtroPortador.addFilter(new QLEqualsFilter("codemp", empresa));
					if(empresa == 13L || empresa == 15L)
					{
						filtroPortador.addFilter(new QLEqualsFilter("codpor", "999"));
					}
					else
					{
						filtroPortador.addFilter(new QLEqualsFilter("codpor", "9999"));
					}
					List<NeoObject> portadores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE039POR"), filtroPortador);
					if (portadores != null && portadores.size() > 0)
					{
						processEntity.setValue("portador", portadores.get(0));
					}
					else
					{
						mensagem = "Não encontrado o Portador para lançamento do Título!";
						throw new WorkflowException(mensagem);
					}

					QLEqualsFilter filtroCarteira = new QLEqualsFilter("codcrt", "99");
					List<NeoObject> carteiras = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE033CRT"), filtroCarteira);
					if (carteiras != null && carteiras.size() > 0)
					{
						processEntity.setValue("carteira", carteiras.get(0));
					}
					else
					{
						mensagem = "Não encontrado a Carteira para lançamento do Título! ";
						throw new WorkflowException(mensagem);
					}
				}
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
