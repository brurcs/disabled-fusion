package com.neomind.fusion.custom.orsegups.ti;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.WFProcess;

@WebServlet(name = "ClonarPapeisServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.ti.CancelarFapServlet" })
public class CancelarFapServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public CancelarFapServlet()
	{
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("getListaUsuarios"))
		{
			ArrayList<String> listaUsuarios = this.getListaUsuarios();
			String json = new Gson().toJson(listaUsuarios);

			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.print(json);
			out.flush();

		}

		if (action.equalsIgnoreCase("cancelarFap"))
		{

			PrintWriter out = response.getWriter();
			String codes = request.getParameter("tarefasFap");
			String tarefaSimples = request.getParameter("tarefaSimples");
			String usuario = request.getParameter("usuario");
			String model = "F001 - FAP - AutorizacaoDePagamento";
			String motivo = "Cancelamento solicitado pelo(a) Sr.(a) " + usuario + ", através da tarefa simples " + tarefaSimples + ".";
			String retorno = "";
			if ((tarefaSimples == "") || (usuario == "") || (codes == ""))
			{

				retorno = "Favor Preencher todos os campos!";
				out.print(retorno);
				out.flush();
				out.close();
			}

			if ((!verificarFapAgrupadora(codes).isEmpty()) && ((tarefaSimples != "") && (usuario != "") && (codes != "")))
			{

				retorno = "Agrupadora";
				out.print(retorno);
				out.flush();
				out.close();
			}
			if ((!retorno.equals("Agrupadora")) && ((tarefaSimples != "") && (usuario != "") && (codes != "")))
			{
				retorno = cancelaProcessos(codes, model, motivo);
				out.print(retorno);
				out.flush();
				out.close();
			}

		}

		if (action.equals("mensagemAgrupadora"))
		{

			String codes = request.getParameter("tarefasFap");
			PrintWriter out = response.getWriter();
			String retorno = "";
			retorno = "FAP(S) " + verificarFapAgrupadora(codes) + " agrupadora(S)";
			out.print(retorno);
			out.flush();
			out.close();
		}

		if (action.equals("respostaTarefa"))
		{
			PrintWriter out = response.getWriter();
			String sexo = "";
			String retorno = "";
			String saudacao = "";
			String usuario = request.getParameter("usuario");
			String nome = primeiroNome(usuario);
			String codes = request.getParameter("tarefasFap");
			saudacao = saudacao();
			sexo = sexoFusion(usuario);
			retorno = respostaTarefa(sexo, nome, saudacao, codes);
			out.print(retorno);
			out.flush();
			out.close();
		}

	}

	private ArrayList<String> getListaUsuarios()
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		ArrayList<String> usuarios = new ArrayList<String>();

		try
		{

			conn = PersistEngine.getConnection("");

			String sql = "SELECT S.name FROM NeoUser U WITH(NOLOCK) " + " INNER JOIN SecurityEntity S WITH(NOLOCK)ON S.neoId = U.neoId " + " WHERE  S.active=1";

			pstm = conn.prepareStatement(sql);

			rs = pstm.executeQuery();

			while (rs.next())
			{
				usuarios.add(rs.getString("name"));
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return usuarios;
	}

	public String cancelaProcessos(String codes, String model, String motivo)
	{
		/*
		 * String xCodes[] = codes.split(",");
		 * String strCodes = "'000000'";
		 * Long code = Long.parseLong(codes);
		 * if (code < 10)
		 * {
		 * codes = "00000" + codes;
		 * }
		 * else if (code < 100)
		 * {
		 * codes = "0000" + codes;
		 * }
		 * else if (code < 1000)
		 * {
		 * codes = "000" + codes;
		 * }
		 * else if (code < 10000)
		 * {
		 * codes = "00" + codes;
		 * }
		 * else if (code < 100000)
		 * {
		 * codes = "0" + codes;
		 * }
		 * strCodes = codes;
		 * /*
		 * for (String x: xCodes){
		 * strCodes += ",'"+x+"'";
		 * }
		 */
		String query = "";
		String mensagem = "Nenhuma tarefa encontrada para cancelamento!";

		query = " select neoId from wfprocess   where model_neoid in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.name = '" + model + "' ) ) and code in  " + "(" + codes + ")";

		EntityManager entity = PersistEngine.getEntityManager();
		Integer cont = 1;
		if (entity != null)
		{
			Query q = entity.createNativeQuery(query);
			List<BigInteger> result = (List<BigInteger>) q.getResultList();

			for (BigInteger i : result)
			{
				Long id = i.longValue();

				/**
				 * FIXME Cancelar tarefas.
				 */
				WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));

				if (proc != null)
				{
					RuntimeEngine.getProcessService().cancel(proc, motivo);
					System.out.println("[Tela de cancelamento fap] - " + cont + " : Processo " + proc.getCode());
					mensagem = "Cancelamento Processado com Sucesso!";
				}
				else
				{
					System.out.println("[Tela de cancelamento fap] - " + cont + " : ERRO: Processo não localizado!");
					mensagem = "Nenhuma tarefa encontrada para cancelamento!";
				}
				cont++;
			}

			return mensagem;
		}
		else
		{
			return "Nenhuma tarefa encontrada para cancelamento!";
		}
	}

	private ArrayList<String> verificarFapAgrupadora(String fap)
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		ArrayList<String> listaFaps = new ArrayList<String>();

		try
		{

			conn = PersistEngine.getConnection("");
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT code ");
			sql.append("FROM D_FAPAutorizacaoDePagamento fap ");
			sql.append("INNER JOIN wfProcess wf  WITH (NOLOCK) ON fap.neoid = wf.entity_neoId ");
			sql.append("where fap.fapagrupadora = 1 ");
			sql.append("and wf.code in " + "(" + fap + ")");

			pstm = conn.prepareStatement(sql.toString());
			//pstm.setString(1, fap);

			rs = pstm.executeQuery();

			while (rs.next())
			{
				listaFaps.add(rs.getString("code"));
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return listaFaps;

	}

	private String respostaTarefa(String sexo, String usuario, String saudacao, String codes)
	{
		String usuarioSecao = System.getProperty("user.name").toUpperCase();

		usuarioSecao = usuarioSecao.replace(".", " ");

		String retorno = "" + saudacao + " " + sexo + " " + usuario + "\n" + "Conforme solicitado foi cancelada a(s) FAP(s) " + codes + ".\n" + "Qualquer duvida favor Entrar em contato com o departamento de TI\n" + "Registro para departamento de TI: \n" + "Caminho Menu - TI - Cancelar Fap.";

		return retorno;
	}

	private String sexoFusion(String usuario)
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Integer sexo = 0;
		String retorno = "";

		try
		{

			conn = PersistEngine.getConnection("");

			String sql = "select gender from NeoUser where fullName = '" + usuario + "'";

			pstm = conn.prepareStatement(sql);

			rs = pstm.executeQuery();

			if (rs.next())
			{
				sexo = rs.getInt("gender");
			}

			if (sexo == 0)
			{

				retorno = "Sr.";
			}
			else
			{

				retorno = "Sra.";
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return retorno;
	}

	private String saudacao()
	{

		String saudacao = "";
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTimeInMillis(new Date().getTime());

		c2.set(Calendar.HOUR_OF_DAY, new Integer("12"));
		c2.set(Calendar.MINUTE, new Integer("00"));

		if (c1.after(c2))
		{

			saudacao = "Boa tarde";
		}
		else
		{

			saudacao = "Bom dia";
		}
		return saudacao;
	}

	private String primeiroNome(String usuario)
	{

		String primeiroNome = "";
		for (int i = 0; i < usuario.length(); i++)
		{
			if ((i == 0) && (usuario.substring(i, i + 1).equalsIgnoreCase(" ")))
			{
				System.out.println("Erro: Nome digitado iniciado com tecla ESPAÇO.");
				break;
			}
			else if (!usuario.substring(i, i + 1).equalsIgnoreCase(" "))
			{
				primeiroNome += usuario.substring(i, i + 1);
			}
			else
				break;
		}
		return primeiroNome;

	}

}
