package com.neomind.fusion.custom.orsegups.juridico.relatorio.dto;

public class EscritorioDTO {

	String nome;
	Long neoId;
	
	
	
	public EscritorioDTO() {
		super();
	}

	
	public EscritorioDTO(String nome, Long neoId) {
		super();
		this.nome = nome;
		this.neoId = neoId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getNeoId() {
		return neoId;
	}

	public void setNeoId(Long neoId) {
		this.neoId = neoId;
	}	
	
	
	
	
}
