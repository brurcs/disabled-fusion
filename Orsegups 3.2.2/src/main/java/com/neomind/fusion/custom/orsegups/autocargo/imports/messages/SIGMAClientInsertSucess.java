package com.neomind.fusion.custom.orsegups.autocargo.imports.messages;


public class SIGMAClientInsertSucess {
    private String status;
    private int entityId;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public int getEntityId() {
        return entityId;
    }
    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }
    
    
}
