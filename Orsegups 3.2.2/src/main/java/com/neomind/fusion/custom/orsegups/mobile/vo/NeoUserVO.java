package com.neomind.fusion.custom.orsegups.mobile.vo;

import com.neomind.fusion.security.NeoUser;

public class NeoUserVO
{
	private String code;

	private String fullName;

	private String email;

	private String sessionToken;

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getFullName()
	{
		return fullName;
	}

	public void setFullName(String fullName)
	{
		this.fullName = fullName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getSessionToken()
	{
		return sessionToken;
	}

	public void setSessionToken(String sessionToken)
	{
		this.sessionToken = sessionToken;
	}
	
	public static NeoUserVO composeVO(NeoUser neoUser )
	{
		NeoUserVO userVO = null;
		
		if(neoUser != null)
		{
			userVO = new NeoUserVO();
			
			userVO.setCode(neoUser.getCode());
			userVO.setFullName(neoUser.getFullName());
			userVO.setEmail(neoUser.getEmail());
		}
		
		return userVO;
	}
}
