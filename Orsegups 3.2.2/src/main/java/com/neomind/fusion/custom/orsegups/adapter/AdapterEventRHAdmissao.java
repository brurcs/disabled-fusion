package com.neomind.fusion.custom.orsegups.adapter;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.util.PDFMergerUtility;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.rhAdmissaoUtils.admissaoUtils;
import com.neomind.fusion.custom.orsegups.servicos.helpers.WorkflowHelper;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

import edu.emory.mathcs.backport.java.util.Collections;

public class AdapterEventRHAdmissao implements ActivityStartEventListener, TaskAssignerEventListener, TaskFinishEventListener
{

	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(AdapterEventRHAdmissao.class);

	@SuppressWarnings({ "unchecked" })
	@Override
	public void onStart(ActivityEvent eventoAtividade) throws TaskException
	{

		try
		{

			String processo = eventoAtividade.getProcess().getModel().getName(); // Processo.

			String atividadeAtual = eventoAtividade.getActivity().getActivityName(); // Atividade atual.

			EntityWrapper wEventoAtividade = eventoAtividade.getWrapper();

			Task tarefaAnterior = retornaTarefaAnterior(eventoAtividade.getActivity()); // Tarefa anterior.

			if (atividadeAtual.equals("3. Imprimir Crachá - RH") && (!tarefaAnterior.getActivity().getActivityName().equals("5. Conferênciada Documentação") && !tarefaAnterior.getActivity().getActivityName().equals("4. Imprimir Documentação - Solicitante")))
			{

				//Registro de Atividades
				List<NeoObject> listaAtividades = wEventoAtividade.findGenericValue("registroTarefas");

				String descricao = wEventoAtividade.findGenericValue("observacao");

				String acaoAtividade = wEventoAtividade.findGenericValue("descacao");

				NeoObject registro = adicionaRegistroAtividade(descricao, acaoAtividade);

				listaAtividades.add(registro);

				//String mailSolicitante = wEventoAtividade.findGenericValue("usersolic.email");

				if (atividadeAtual.equals("3. Imprimir Crachá - RH"))
				{

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

					//INÍCIO DA ALIMENTAÇÃO DO HISTÓRICO
					String codeProcesso = eventoAtividade.getProcess().getCode(); //
					String solicitante = wEventoAtividade.findGenericValue("usersolic.fullName"); //
					GregorianCalendar dataAdmissao = wEventoAtividade.findGenericValue("dtAdmissao");
					String sDataAdmissao = sdf.format(dataAdmissao.getTime()); //
					String nomeCompleto = wEventoAtividade.findGenericValue("nomCompletoFC"); //
					String nomeCracha = wEventoAtividade.findGenericValue("nomeCracha"); //
					GregorianCalendar dataNascimento = wEventoAtividade.findGenericValue("dtNascimento");
					String sDataNascimento = sdf.format(dataNascimento.getTime()); //
					String cpf = wEventoAtividade.findGenericValue("numcpf"); //
					Long lPis = wEventoAtividade.findGenericValue("pis");
					String pis = lPis.toString(); //
					String rg = wEventoAtividade.findGenericValue("numIdentRG"); //
					String regional = wEventoAtividade.findGenericValue("regionalColab.asString"); //
					String empresa = wEventoAtividade.findGenericValue("emp.asString"); //
					Long tipoAdmissaoCodigo = wEventoAtividade.findGenericValue("tpAdmColab.codigo");
					String tipoAdmissaoDescricao = wEventoAtividade.findGenericValue("tpAdmColab.descricao");
					String filial = null;//
					if (tipoAdmissaoCodigo == 1)
					{
						filial = wEventoAtividade.findGenericValue("codfil.asString");
					}
					else
					{
						filial = wEventoAtividade.findGenericValue("codfilOperacional.asString");
					}
					String tipoAdmissao = tipoAdmissaoCodigo + " - " + tipoAdmissaoDescricao; //
					String departamento = wEventoAtividade.findGenericValue("departamento.asString"); //
					String cargo = wEventoAtividade.findGenericValue("cargos.asString"); //
					String escala = wEventoAtividade.findGenericValue("escalaTrabalho.asString");
					String modPagamento = wEventoAtividade.findGenericValue("modPagamento.codigo"); //
					String banco = "";
					String agencia = "";
					String conta = "";
					;
					String digito = "";
					if (modPagamento.equals("R"))
					{
						banco = wEventoAtividade.findGenericValue("bancoConta.asString"); //
						agencia = wEventoAtividade.findGenericValue("agenciaConta.asString"); //
						Long lConta = wEventoAtividade.findGenericValue("conban");
						conta = lConta.toString(); //
						digito = wEventoAtividade.findGenericValue("digcontaS"); //
					}
					String tipoSalario = wEventoAtividade.findGenericValue("tpSalario.asString"); //
					Boolean editarSalario = wEventoAtividade.findGenericValue("editsalario");
					String salario = null;
					BigDecimal bdSalario = null;
					if (editarSalario)
					{
						bdSalario = wEventoAtividade.findGenericValue("valorSalarioPerso");
					}
					else
					{
						bdSalario = wEventoAtividade.findGenericValue("valorSalario.valor");
					}
					salario = bdSalario.toString(); //
					Long lMatricula = wEventoAtividade.findGenericValue("matricula");
					String matricula = null;
					if (lMatricula != null)
						matricula = lMatricula.toString();
					String estado = wEventoAtividade.findGenericValue("estado.asString");
					String cidade = wEventoAtividade.findGenericValue("cidcolab.asString");
					String bairro = wEventoAtividade.findGenericValue("bairro.asString");
					String rua = wEventoAtividade.findGenericValue("ruacolab.asString");
					String logradouro = wEventoAtividade.findGenericValue("tpLogradouro.asString");
					Long lEndnum = wEventoAtividade.findGenericValue("endnum");
					String endnum = lEndnum.toString();
					String complemento = wEventoAtividade.findGenericValue("complEnd");
					String telefone01 = wEventoAtividade.findGenericValue("telefone");
					String telefone02 = wEventoAtividade.findGenericValue("telefone2");
					NeoObject oAcao = wEventoAtividade.findGenericValue("validacao");
					EntityWrapper wAcao = new EntityWrapper(oAcao);
					String acao = wAcao.findGenericValue("acao.descricao");
					String observacao = wAcao.findGenericValue("observacao");

					Boolean regitrosHistorico = admissaoUtils.addHistoricoAdmissao(codeProcesso, solicitante, sDataAdmissao, nomeCompleto, nomeCracha, sDataNascimento, cpf, pis, rg, regional, empresa, tipoAdmissao, filial, departamento, cargo, escala, modPagamento, banco, agencia, conta, digito, tipoSalario, salario, matricula, estado, cidade, bairro, rua, logradouro, endnum, complemento, telefone01, telefone02, acao, observacao);

					if (!regitrosHistorico)
					{

						throw new WorkflowException("ERRO NO REGISTRO DE HISTÓRICO");

					}

					//SETANDO INFORMAÇÕES NOS ARQUIVOS DE INTEGRAÇÃO

					String nomfun = null;

					Integer caractNomCompleto = nomeCompleto.length();

					if (caractNomCompleto > 40 && caractNomCompleto <= 80)
					{

						nomfun = nomeCompleto.replace(' ', ';');
						String nomePedacos[] = nomfun.split(";");
						int k;
						String nomSaida = "";
						for (k = 0; k < nomePedacos.length; k++)
						{
							if (k == 0 || k == (nomePedacos.length - 1))
								nomSaida = nomSaida + nomePedacos[k];
							else
								nomSaida = nomSaida + " " + nomePedacos[k].charAt(0) + " ";
						}

						nomfun = nomSaida;

					}
					else if (caractNomCompleto <= 40)
					{

						nomfun = nomeCompleto;

					}

					NeoObject documentos = wEventoAtividade.findGenericValue("documentacao");
					EntityWrapper wDocumentos = new EntityWrapper(documentos);
					EntityWrapper wIntegracaoOpe = null;
					EntityWrapper wIntegracaoAdm = null;
					//1 - ADM || 2 - OPE
					Long tpAdmColab = wEventoAtividade.findGenericValue("tpAdmColab.codigo");

					String sDataAdm = sdf.format(dataAdmissao.getTime());
					String sCargo = wEventoAtividade.findGenericValue("cargos.titred");
					String sDepartamento = wEventoAtividade.findGenericValue("departamento.descricao");
					String sRegional = wEventoAtividade.findGenericValue("regionalColab.usu_nomreg");
					String sEmpresa = wEventoAtividade.findGenericValue("emp.nomemp");

					if (tpAdmColab == 1)
					{
						NeoObject integracaoAdm = AdapterUtils.createNewEntityInstance("rhIntegracaoAdm");
						PersistEngine.persist(integracaoAdm);
						wIntegracaoAdm = new EntityWrapper(integracaoAdm);
						System.out.println(integracaoAdm);
						wIntegracaoAdm.findField("colaborador").setValue(nomfun);
						wIntegracaoAdm.findField("dataAdmissao").setValue(sDataAdm);
						wIntegracaoAdm.findField("cargo").setValue(sCargo);
						wIntegracaoAdm.findField("departamento").setValue(sDepartamento);
						wDocumentos.findField("docIntegracao").setValue(integracaoAdm);
						NeoFile o = wIntegracaoAdm.findGenericValue("file");
						wDocumentos.findField("docIntegracao").setValue(o);
						System.out.println(o);
					}
					else
					{
						NeoObject integracaoOpe = AdapterUtils.createNewEntityInstance("rhIntegracaoOpe");
						PersistEngine.persist(integracaoOpe);
						wIntegracaoOpe = new EntityWrapper(integracaoOpe);
						System.out.println(integracaoOpe);
						wIntegracaoOpe.findField("regional").setValue(sRegional);
						wIntegracaoOpe.findField("colaborador").setValue(nomfun);
						wIntegracaoOpe.findField("cargo").setValue(sCargo);
						wIntegracaoOpe.findField("empresa").setValue(sEmpresa);
						wIntegracaoOpe.findField("dataAdmissao").setValue(sDataAdm);
						NeoFile o = wIntegracaoOpe.findGenericValue("file");
						wDocumentos.findField("docIntegracao").setValue(o);
						System.out.println(o);
					}

				}

			}

			if (atividadeAtual.equals("2. Validar Admissão"))
			{

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

				PDFMergerUtility ut = new PDFMergerUtility();

				//INÍCIO DA ALIMENTAÇÃO DO HISTÓRICO
				String codeProcesso = eventoAtividade.getProcess().getCode(); //
				String solicitante = wEventoAtividade.findGenericValue("usersolic.fullName"); //
				GregorianCalendar dataAdmissao = wEventoAtividade.findGenericValue("dtAdmissao");
				String sDataAdmissao = sdf.format(dataAdmissao.getTime()); //
				String nomeCompleto = wEventoAtividade.findGenericValue("nomCompletoFC"); //
				String nomeCracha = wEventoAtividade.findGenericValue("nomeCracha"); //
				GregorianCalendar dataNascimento = wEventoAtividade.findGenericValue("dtNascimento");
				String sDataNascimento = sdf.format(dataNascimento.getTime()); //
				String cpf = wEventoAtividade.findGenericValue("numcpf"); //
				Long lPis = wEventoAtividade.findGenericValue("pis");
				String pis = lPis.toString(); //
				String rg = wEventoAtividade.findGenericValue("numIdentRG"); //
				String regional = wEventoAtividade.findGenericValue("regionalColab.asString"); //
				String empresa = wEventoAtividade.findGenericValue("emp.asString"); //
				Long tipoAdmissaoCodigo = wEventoAtividade.findGenericValue("tpAdmColab.codigo");
				String tipoAdmissaoDescricao = wEventoAtividade.findGenericValue("tpAdmColab.descricao");
				String filial = null;//
				if (tipoAdmissaoCodigo == 1)
				{
					filial = wEventoAtividade.findGenericValue("codfil.asString");
				}
				else
				{
					filial = wEventoAtividade.findGenericValue("codfilOperacional.asString");
				}
				String tipoAdmissao = tipoAdmissaoCodigo + " - " + tipoAdmissaoDescricao; //
				String departamento = wEventoAtividade.findGenericValue("departamento.asString"); //
				String cargo = wEventoAtividade.findGenericValue("cargos.asString"); //
				String escala = wEventoAtividade.findGenericValue("escalaTrabalho.asString");
				String modPagamento = wEventoAtividade.findGenericValue("modPagamento.codigo"); //
				String banco = "";
				String agencia = "";
				String conta = "";
				;
				String digito = "";
				if (modPagamento.equals("R"))
				{
					banco = wEventoAtividade.findGenericValue("bancoConta.asString"); //
					agencia = wEventoAtividade.findGenericValue("agenciaConta.asString"); //
					Long lConta = wEventoAtividade.findGenericValue("conban");
					conta = lConta.toString(); //
					digito = wEventoAtividade.findGenericValue("digcontaS"); //
				}
				String tipoSalario = wEventoAtividade.findGenericValue("tpSalario.asString"); //
				Boolean editarSalario = wEventoAtividade.findGenericValue("editsalario");
				String salario = null;
				BigDecimal bdSalario = null;
				if (editarSalario)
				{
					bdSalario = wEventoAtividade.findGenericValue("valorSalarioPerso");
				}
				else
				{
					bdSalario = wEventoAtividade.findGenericValue("valorSalario.valor");
				}
				if (bdSalario != null)
					salario = bdSalario.toString(); //
				Long lMatricula = wEventoAtividade.findGenericValue("matricula");
				String matricula = null;
				if (lMatricula != null)
					matricula = lMatricula.toString();
				String estado = wEventoAtividade.findGenericValue("estado.asString");
				String cidade = wEventoAtividade.findGenericValue("cidcolab.asString");
				String bairro = wEventoAtividade.findGenericValue("bairro.asString");
				String rua = wEventoAtividade.findGenericValue("ruacolab.asString");
				String logradouro = wEventoAtividade.findGenericValue("tpLogradouro.asString");
				Long lEndnum = wEventoAtividade.findGenericValue("endnum");
				String endnum = lEndnum.toString();
				String complemento = wEventoAtividade.findGenericValue("complEnd");
				String telefone01 = wEventoAtividade.findGenericValue("telefone");
				String telefone02 = wEventoAtividade.findGenericValue("telefone2");
				NeoObject oAcao = wEventoAtividade.findGenericValue("validacao");
				EntityWrapper wAcao = new EntityWrapper(oAcao);
				String acao = wAcao.findGenericValue("acao.descricao");
				String observacao = wAcao.findGenericValue("observacao");

				Boolean regitrosHistorico = admissaoUtils.addHistoricoAdmissao(codeProcesso, solicitante, sDataAdmissao, nomeCompleto, nomeCracha, sDataNascimento, cpf, pis, rg, regional, empresa, tipoAdmissao, filial, departamento, cargo, escala, modPagamento, banco, agencia, conta, digito, tipoSalario, salario, matricula, estado, cidade, bairro, rua, logradouro, endnum, complemento, telefone01, telefone02, acao, observacao);

				if (!regitrosHistorico)
				{

					throw new WorkflowException("ERRO NO REGISTRO DE HISTÓRICO");

				}

				//INÍCIO GERAÇÃO VISUALIZADOR
				List<NeoObject> listaVisuDocs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhListaVisualizarDocs"), new QLRawFilter("ativo = true"));

				sortNeoId(listaVisuDocs);

				try
				{

					for (NeoObject campo : listaVisuDocs)
					{

						EntityWrapper wCampo = new EntityWrapper(campo);
						String nomeCampoDocumento = wCampo.findGenericValue("documento.nome");
						String origemlDoc = wCampo.findGenericValue("documento.origem.descricaoOrigem");

						String localDoc = "";
						if (!origemlDoc.equals("PRINCIPAL"))
						{

							if (!origemlDoc.equals("VIGILANTES"))
							{
								localDoc = nomeCampoDocumento + "." + "dadosVigilancia";
							}
							else if (!origemlDoc.equals("DEPENDENTES"))
							{
								localDoc = nomeCampoDocumento + "." + "listaDepend";
							}
							else
							{
								throw new WorkflowException("ERRO NA ORIGEM DO DOCUMENTO PARA O CAMPO: " + nomeCampoDocumento);
							}

						}
						else
						{
							localDoc = nomeCampoDocumento;
						}
						NeoFile doc = wEventoAtividade.findGenericValue(localDoc + ".file");

						if (doc == null && (!nomeCampoDocumento.equals("certifEscolaridade") && !nomeCampoDocumento.equals("propEmprego") && !nomeCampoDocumento.equals("integracao") && !nomeCampoDocumento.equals("docExtratoBanco") && !nomeCampoDocumento.equals("docCertReserv")))
						{
							throw new WorkflowException("CAMPO DO FORMULÁRIO NÃO EXISTE OU NÃO POSSUI IMAGEM!");
						}
						else
						{

							String nomeArquivo = null;

							if (doc != null)
							{

								nomeArquivo = doc.getName();
								nomeArquivo = nomeArquivo.toLowerCase();

							}

							if (doc != null && (!nomeArquivo.contains(".jpg") && !nomeArquivo.contains(".gif") && !nomeArquivo.contains(".png") && !nomeArquivo.contains(".jpeg")))
								ut.addSource(doc.getAsFile());
						}

					}

				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw (WorkflowException) e;
				}

				NeoFile nomeDocumentoConferencia = NeoStorage.createFile("Documentos_Conferência.PDF");

				try
				{
					ut.setDestinationStream(new FileOutputStream(nomeDocumentoConferencia.getAsFile()));
					ut.mergeDocuments(); // Faz merge dos arquivos.
					wEventoAtividade.findField("visualizador").setValue(nomeDocumentoConferencia);

				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw (WorkflowException) e;
				}

			}

			NeoObject eform = wEventoAtividade.getObject();

			PersistEngine.persist(eform);

			if (tarefaAnterior == null)
			{

				//PRODUÇÃO É "rhDocumentos"
				NeoObject docs = AdapterUtils.createNewEntityInstance("rhDocumentos");

				PersistEngine.persist(docs);

				wEventoAtividade.getField("documentacao").setValue(docs);

				NeoObject validacao = AdapterUtils.createNewEntityInstance("rhAdmissaoValidacao");

				EntityWrapper wValidacao = new EntityWrapper(validacao);

				wValidacao.findField("acao").setValue(null);

				PersistEngine.persist(validacao);

				wEventoAtividade.getField("validacao").setValue(validacao);

			}

			if (tarefaAnterior != null && (tarefaAnterior.getTitle().contains("2.") || tarefaAnterior.getTitle().contains("Admissão.") || tarefaAnterior.getTitle().contains("1. Iniciar")))
			{

				NeoObject validacao = wEventoAtividade.findGenericValue("validacao");

				EntityWrapper wValidacao = new EntityWrapper(validacao);

				wValidacao.findField("acao").setValue(null);

			}

		}

		catch (Exception e)
		{
			e.printStackTrace();
			throw (WorkflowException) e;
		}

	}

	@Override
	public void onAssign(TaskAssignerEvent event) throws TaskException
	{

		Task tarefaAnterior = retornaTarefaAnterior(event.getActivity());

		NeoUser userSolicitante = event.getTask().getUser();

		EntityWrapper wEventoAtividade = event.getWrapper();

		String nomeColaborador = NeoUtils.safeOutputString(wEventoAtividade.findField("nomCompletoFC").getValue());
		
		if (tarefaAnterior == null)
		{

			EntityWrapper wuserSolicitate = new EntityWrapper(userSolicitante);

			Long codigoRegional = wuserSolicitate.findGenericValue("escritorioRegional.codigo");

			wEventoAtividade.findField("usersolic").setValue(userSolicitante);

			//Puxa a regional pelo usuário solicitante
			List<NeoObject> regionais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHREG"), new QLRawFilter("usu_codreg = " + codigoRegional));

			for (NeoObject regional : regionais)
			{

				if (regionais.size() == 1 && regionais != null)
				{

					wEventoAtividade.findField("regionalColab").setValue(regional);

				}

			}

			Long idUserSolicitante = userSolicitante.getNeoId();

			Long idRegional = wEventoAtividade.findGenericValue("regionalColab.neoId");

			List<NeoObject> listaregionais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhListaUserRegSede"), new QLRawFilter("usuario = " + idUserSolicitante + " and regional = " + idRegional));

			if (listaregionais.size() > 0)
			{

				wEventoAtividade.findField("escondeASO").setValue(Boolean.TRUE);

			}

		}

		NeoGroup grupoSolicitante = userSolicitante.getGroup();

		Integer groupLvlSolicitante = grupoSolicitante.getGroupLevel();

		if (groupLvlSolicitante < 500)
		{
			NeoGroup grupoResponsavel = grupoSolicitante.getUpperLevel();
			Integer lvlGrupoResponsavel = grupoResponsavel.getUpperLevel().getGroupLevel();

			while (lvlGrupoResponsavel < 500)
			{
				lvlGrupoResponsavel = grupoResponsavel.getUpperLevel().getGroupLevel();
				grupoResponsavel = grupoResponsavel.getUpperLevel();
			}

			NeoPaper papelResponsavel = grupoResponsavel.getResponsible();

			Set<NeoUser> usersGrupoResponsavel = papelResponsavel.getAllUsers();

			Integer cont = 1;

			for (NeoUser userGrupoResponsavel : usersGrupoResponsavel)
			{

				if (cont == 1)
				{

					wEventoAtividade.findField("validacao.userAprovador").setValue(userGrupoResponsavel);
					cont++;

				}

			}

		}
		else if (groupLvlSolicitante == 500)
		{

			NeoGroup grupoResponsavel = grupoSolicitante;

			NeoPaper papelResponsavel = grupoResponsavel.getResponsible();

			Set<NeoUser> usersGrupoResponsavel = papelResponsavel.getAllUsers();

			Integer cont = 1;

			for (NeoUser userGrupoResponsavel : usersGrupoResponsavel)
			{

				if (cont == 1)
				{

					wEventoAtividade.findField("validacao.userAprovador").setValue(userGrupoResponsavel);
					cont++;

				}

			}

		}

		String atividadeAtual = event.getActivity().getAsString();

		if (atividadeAtual.contains("3. Imprimir Crachá - RH"))
		{

			////REGRA PARA GERAR A TAREFA SIMPLES CASO MODO DE PAGAMENTO SEJA CHEQUE

			String modPag = wEventoAtividade.findGenericValue("modPagamento.codigo");

			if (modPag.equals("C"))
			{
				
				NeoPaper tsPapelSolicitante = OrsegupsUtils.getPaper("SolicitanteResgateDadosBancarios");
				
				NeoUser tsSolicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(tsPapelSolicitante);

				NeoObject oTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");

				EntityWrapper wTarefaSimples = new EntityWrapper(oTarefaSimples);

				wTarefaSimples.findField("Solicitante").setValue(tsSolicitante);

				NeoUser solicitante = wEventoAtividade.findGenericValue("usersolic");

				wTarefaSimples.findField("Executor").setValue(solicitante);

				wTarefaSimples.findField("Titulo").setValue("Requisitar Dados Bancários - "+nomeColaborador);

				String codProcess = event.getProcess().getCode();

				String nomeCompColab = wEventoAtividade.findGenericValue("nomCompletoFC");

				wTarefaSimples.findField("DescricaoSolicitacao").setValue("Solicito, por gentileza, que resgatem os dados bancários do futuro colaborador: " + nomeCompColab + "<br><br>Processo de Admissão de código: " + codProcess + "<br><br>Desde já muito obrigado! <br><br> Att. <br><b>" + tsSolicitante.getFullName() + "</b>");

				GregorianCalendar prazoTarefaSimples = new GregorianCalendar();

				prazoTarefaSimples.add(Calendar.HOUR, 24);

				Boolean diautil = OrsegupsUtils.isWorkDay(prazoTarefaSimples);

				if (!diautil)
				{

					prazoTarefaSimples = OrsegupsUtils.getNextWorkDay(prazoTarefaSimples);

				}

				wTarefaSimples.findField("Prazo").setValue(prazoTarefaSimples);

				ProcessModel pm = PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));

				String dadosprocesso = OrsegupsWorkflowHelper.iniciaProcessoNeoIdCode(pm, oTarefaSimples, true, tsSolicitante, true);

				String neoidProcess = dadosprocesso.substring(1, dadosprocesso.indexOf(";"));

				Integer numCarac = dadosprocesso.length();

				String codeProcess = dadosprocesso.substring(dadosprocesso.indexOf(";") + 1, numCarac);

				//Registro de Atividades
				List<NeoObject> listaAtividades = wEventoAtividade.findGenericValue("registroTarefas");

				String descricao = "Tarefa Simples aberta solicitando dados bancários oficiais do colaborador.<br><br>NeoId: " + neoidProcess + " || Código: " + codeProcess;

				String acaoAtividade = "Abertura Tarefa Simples";

				NeoObject registro = adicionaRegistroAtividade(descricao, acaoAtividade);

				listaAtividades.add(registro);

			}

		}

	}

	public Task retornaTarefaAnterior(Activity atividade)
	{
		Task tarefaAnterior = null;

		GregorianCalendar ultimaDataFim = null;

		List<Task> tarefasAnteriores = new WorkflowHelper().getTaskOrigins(atividade);

		if (tarefasAnteriores != null && tarefasAnteriores.size() > 0)
		{
			for (Task tarefa : tarefasAnteriores)
			{
				if (ultimaDataFim == null || tarefa.getActivity().getFinishDate().after(ultimaDataFim))
				{
					ultimaDataFim = tarefa.getActivity().getFinishDate();

					tarefaAnterior = tarefa;
				}
			}
		}

		return tarefaAnterior;
	}

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException
	{
		// TODO Auto-generated method stub

		try
		{
			String tarefa = event.getActivity().getActivityName();

			EntityWrapper wEventoAtividade = event.getWrapper();

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");

			GregorianCalendar dataatual = new GregorianCalendar();

			GregorianCalendar fixDtAtual = new GregorianCalendar();

			GregorianCalendar dtadmissao = wEventoAtividade.findGenericValue("dtAdmissao");

			GregorianCalendar contData = dataatual;

			Integer contIntervaloUtil = 0;

			Boolean calcUtil = null;

			String data = "";

			String sDtAdmissao = sdfDate.format(dtadmissao.getTime());

			String sDtAtual = sdfDate.format(dataatual.getTime());

			java.util.Date dDtAdmissao = sdfDate.parse(sDtAdmissao);

			java.util.Date dDtAtual = sdfDate.parse(sDtAtual);
			
			String nomeColaborador = NeoUtils.safeOutputString(wEventoAtividade.findField("nomCompletoFC").getValue());

			/************************************************************************************************************************/
			/*                                   INÍCIO VALIDAÇÃO TAMANHO ARQUIVOS (ENVIO P/ GED)                                   */
			/************************************************************************************************************************/
			
			NeoFile docRequisicaoColaborador = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("rcDoc"));
			NeoFile docCpfColaborador = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("doccpf"));
			NeoFile docRgColaborador = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("docRG"));
			NeoFile docPisColaborador = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("pisDoc"));
			NeoFile docComprovanteBancario = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("docExtratoBanco"));
			NeoFile docComprovanteResidencia = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("doccomprovante"));
			NeoFile docCertificadoEscolaridade = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("certifEscolaridade"));
			NeoFile docCarteiraTrabalho = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("docctpis"));
			NeoFile docAso = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("docaso"));
			NeoFile docCertificadoReservista = NeoUtils.safeNeoFile((NeoFile) wEventoAtividade.findGenericValue("docCertReserv"));
			
			/**
			 * |*******************************|
			 * |       Tabela de Tamanhos      |
			 * |							   |
			 * |*******************************|
			 * | Documento             |  Tam. |
			 * |*******************************|
			 * | Req. Colaborador      | 100kb |
			 * | CPF				   | 200kb |
			 * | RG					   | 200kb |
			 * | PIS				   | 200kb |
			 * | Comprov. Bancario	   | 350kb |
			 * | Comprov. Residência   | 350kb |
			 * | Cert. Escolaridade    | 270kb |
			 * | Carteira Trab. 	   | 270kb |
			 * | ASO				   | 860kb |
			 * | Certif. Reservista	   | 350kb |
			 * |					   |	   |
			 * |***********************|*******|
			 * 
			 */
			
//			if(NeoUtils.safeIsNotNull(docRequisicaoColaborador) && docRequisicaoColaborador.getContentLength() > 100000) {
//				throw new WorkflowException("Requisição do Colaborador anexado ultrapassou o tamanho permitido de 100kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docCpfColaborador) && docCpfColaborador.getContentLength() > 200000) {
//				throw new WorkflowException("Documento CPF anexado ultrapassou o tamanho permitido de 200kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docRgColaborador) && docRgColaborador.getContentLength() > 200000) {
//				throw new WorkflowException("Documento RG anexado ultrapassou o tamanho permitido de 200kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docPisColaborador) && docPisColaborador.getContentLength() > 200000) {
//				throw new WorkflowException("Documento PIS anexado ultrapassou o tamanho permitido de 200kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docComprovanteBancario) && docComprovanteBancario.getContentLength() > 350000) {
//				throw new WorkflowException("Comprovante Bancário anexado ultrapassou o tamanho permitido de 350kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docComprovanteResidencia) && docComprovanteResidencia.getContentLength() > 350000) {
//				throw new WorkflowException("Comprovante de Residência anexado ultrapassou o tamanho permitido de 350kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docCertificadoEscolaridade) && docCertificadoEscolaridade.getContentLength() > 270000) {
//				throw new WorkflowException("Certificado de Escolaridade anexado ultrapassou o tamanho permitido de 270kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docCarteiraTrabalho) && docCarteiraTrabalho.getContentLength() > 270000) {
//				throw new WorkflowException("Documento da Carteira de Trabalho anexado ultrapassou o tamanho permitido de 270kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docAso) && docAso.getContentLength() > 860000) {
//				throw new WorkflowException("ASO anexado ultrapassou o tamanho permitido de 860kb.");
//			}
//			
//			if(NeoUtils.safeIsNotNull(docCertificadoReservista) && docCertificadoReservista.getContentLength() > 350000) {
//				throw new WorkflowException("Certificado de reservista anexado ultrapassou o tamanho permitido de 350kb.");
//			}
			
			/************************************************************************************************************************/
			/*                                    FIM VALIDAÇÃO TAMANHO ARQUIVOS (ENVIO P/ GED)                                     */
			/************************************************************************************************************************/
			
			
			for (Integer i = 0; dDtAtual.before(dDtAdmissao); i++)
			{

				calcUtil = OrsegupsUtils.isWorkDay(contData);

				if (calcUtil)
				{

					contIntervaloUtil++;

				}

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dDtAtual);
				calendar.add(Calendar.DAY_OF_MONTH, 1);

				dDtAtual = calendar.getTime();

				contData.setTime(dDtAtual);

				data = sdf.format(dDtAtual.getTime());

			}

			df.setLenient(false);

			GregorianCalendar dtnascimento = wEventoAtividade.findGenericValue("dtNascimento");

			Long dt = (dtadmissao.getTimeInMillis() - dtnascimento.getTimeInMillis()) + 3600000;

			Long diasCorridosAnoLong = (dt / 86400000L);

			Integer diasDecorridosInt = Integer.valueOf(diasCorridosAnoLong.toString());

			BigDecimal dias = new BigDecimal(diasDecorridosInt);

			BigDecimal ano = new BigDecimal(365.25);

			BigDecimal idade = dias.divide(ano, 0, RoundingMode.DOWN);

			Long acao = wEventoAtividade.findGenericValue("validacao.acao.codigo");

			Boolean aprovaModPagamento = wEventoAtividade.findGenericValue("aprovaModoPag");

			String modoPagamento = wEventoAtividade.findGenericValue("modPagamento.codigo");

			Boolean usuarioAprovador = wEventoAtividade.findGenericValue("usuarioAprovador");

			if (!modoPagamento.equals("R") && (tarefa.equals("2. Validar Admissão") && acao == 5))
			{

				//ABRIR TAREFA SIMPLES

			}

			//validação da idade do colaborador
			if (idade.intValue() < 14)
			{

				throw new WorkflowException("IDADE DO FUNCIONÁRIO É INFERIOR À 14 ANOS.");

			}

			NeoFile docReservista = wEventoAtividade.findGenericValue("docCertReserv");

			String numReservista = wEventoAtividade.findGenericValue("numres");

			String digReservista = wEventoAtividade.findGenericValue("catres");

			String sexo = wEventoAtividade.findGenericValue("sexo.sigla");

			//Dados de Reservista só são obrigados se for homem e estiver entre 18 e 45 anos
			if (idade.intValue() >= 18 && idade.intValue() < 45 && (docReservista == null || numReservista == null || digReservista == null) && sexo.equals("M"))
			{

				throw new WorkflowException("OBRIGATÓRIA A INSERÇÃO DOS DADOS DO CERTIFICADO DE RESERVISTA!");

			}

			//Solicitar Aprovação
			if (acao == 1)
			{

				wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.TRUE);

			}

			//Devolver para Regional
			if (acao == 2)
			{

				wEventoAtividade.findField("tarefaAtual").setValue("1. Iniciar Admissão.");
				wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.FALSE);
				wEventoAtividade.findField("contErros").setValue(0l);

			}

			//Aprovar ou Rejeitar
			if (acao == 3 || acao == 4)
			{

				wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.FALSE);

				if (acao == 3)
				{

					wEventoAtividade.findField("tarefaAtual").setValue("2. Validar Admissão");

					NeoUser solicitante = wEventoAtividade.findGenericValue("usersolic");

					String codProcess = event.getProcess().getCode();

					wEventoAtividade.findField("exec1").setValue(solicitante);

					//Exigência do Nome Completo caso Nome contenha abreviações com , ou .
					String nomeColab = wEventoAtividade.findGenericValue("colaborador");

					String nomeCompColab = wEventoAtividade.findGenericValue("nomCompletoFC");

					String nomeCracha = wEventoAtividade.findGenericValue("nomeCracha");

					String ponto = ".";

					String virgula = ",";

					Integer iPonto = nomeColab.indexOf(ponto);

					Integer iVirgula = nomeColab.indexOf(virgula);

					//Nomes do cadastro do colaborador
					if ((nomeCompColab == null || nomeCompColab.equals("")) && (iPonto > 0 || iVirgula > 0))
					{

						throw new WorkflowException("NOME DO COLABORADOR CONTÉM ABREVIATURAS(. OU ,). FAVOR INFORMAR O CAMPO 'NOME COMPLETO DO COLABORADOR'!");

					}

					nomeColab = converte(nomeColab);

					wEventoAtividade.findField("colaborador").setValue(nomeColab);

					if (!nomeCompColab.equals("") && nomeCompColab != null)
					{

						nomeCompColab = converte(nomeCompColab);

						wEventoAtividade.findField("nomCompletoFC").setValue(nomeCompColab);

					}

					nomeCracha = converte(nomeCracha);

					wEventoAtividade.findField("nomeCracha").setValue(nomeCracha);

				}

			}

			//Efetuar a Integração ou Enviar para Sede
			if (acao == 5 || acao == 6)
			{

				String telefone = wEventoAtividade.findGenericValue("telefone");

				String telefone2 = wEventoAtividade.findGenericValue("telefone2");

				Integer tamanhoTelefone = telefone.length();

				String primeiroNumTelefone = telefone.substring(5, 6);

				Integer tamanhoTelefone2 = null;

				String primeiroNumTelefone2 = null;

				if (telefone2 != null && !telefone2.equals(""))
				{

					tamanhoTelefone2 = telefone2.length();

					primeiroNumTelefone2 = telefone2.substring(5, 6);

				}

				if ((primeiroNumTelefone.equals("9") && tamanhoTelefone < 15) || (telefone2 != null && !telefone2.equals("") && primeiroNumTelefone2.equals("9") && tamanhoTelefone2 < 15))
				{

					throw new WorkflowException("QUANDO NÚMERO DO TELEFONE INICIAR COM 9, O MESMO DEVE TER PELO MENOS 9 DÍGITOS!");
				}

				GregorianCalendar dtAso = wEventoAtividade.findGenericValue("ultExameMedic");

				if (dtAso == null)
				{

					dtAso = new GregorianCalendar();

					wEventoAtividade.findField("ultExameMedic").setValue(dtAso);

				}

				Long diferencaAso = ((dtadmissao.getTimeInMillis() - dtAso.getTimeInMillis()) + 3600000) / 86400000l;

				if (dtadmissao.before(dtAso) || dtadmissao.compareTo(dtAso) <= 0 || diferencaAso >= 60)
				{

					throw new WorkflowException("DATA DA ASO DEVE SER INFERIOR A DATA DE ADMISSÃO! PRAZO MÁXIMO DE ATÉ 2 MESES(60 DIAS) ANTES DA DATA DE ADMISSÃO!");

				}

				if (acao == 6)
				{

					NeoPaper papelRHSede = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "rhAdmissaoSede"));

					NeoUser userSolicitante = wEventoAtividade.findGenericValue("usersolic");

					@SuppressWarnings("unchecked")
					Collection<NeoUser> usersRHSede = (Collection<NeoUser>) papelRHSede.getAllUsers();

					Boolean eSede = Boolean.FALSE;

					for (NeoUser user : usersRHSede)
					{

						if (user == userSolicitante)
						{

							eSede = Boolean.TRUE;
						}

					}
					
					if (contIntervaloUtil.intValue() < 1)
					{
						throw new WorkflowException("DATA DE ADMISSÃO INCORRETA, POR FAVOR INSIRA UMA DATA COM PELO MENOS 01(UM) DIA ÚTIL DE INTERVALO.");
					}

					wEventoAtividade.findField("tarefaAtual").setValue("2. Validar Admissão");
					wEventoAtividade.findField("exec1").setValue(PortalUtil.getCurrentUser());

					//Exigência do Nome Completo caso Nome contenha abreviações com , ou .
					String nomeColab = wEventoAtividade.findGenericValue("colaborador");

					String nomeCompColab = wEventoAtividade.findGenericValue("nomCompletoFC");

					String nomeCracha = wEventoAtividade.findGenericValue("nomeCracha");

					String ponto = ".";

					String virgula = ",";

					Integer iPonto = nomeColab.indexOf(ponto);

					Integer iVirgula = nomeColab.indexOf(virgula);

					//Nomes do cadastro do colaborador
					if ((nomeCompColab == null || nomeCompColab.equals("")) && (iPonto > 0 || iVirgula > 0))
					{

						throw new WorkflowException("NOME DO COLABORADOR CONTÉM ABREVIATURAS(. OU ,). FAVOR INFORMAR O CAMPO 'NOME COMPLETO DO COLABORADOR'!");

					}

					nomeColab = converte(nomeColab);

					wEventoAtividade.findField("colaborador").setValue(nomeColab);

					if (!nomeCompColab.equals("") && nomeCompColab != null)
					{

						nomeCompColab = converte(nomeCompColab);

						wEventoAtividade.findField("nomCompletoFC").setValue(nomeCompColab);

					}

					nomeCracha = converte(nomeCracha);

					wEventoAtividade.findField("nomeCracha").setValue(nomeCracha);

					//Buscando os nomes da lista de dependentes para efetuar o mestre tratamento acima
					List<NeoObject> listaDependentes = wEventoAtividade.findGenericValue("listaDepend");

					Integer cont = 1;

					for (NeoObject itemDependentes : listaDependentes)
					{

						EntityWrapper wItemDependente = new EntityWrapper(itemDependentes);

						String nomeDependente = wItemDependente.findGenericValue("nomDependente");

						String nomeCompletoDependente = wItemDependente.findGenericValue("nomCompletoDepend");

						Integer indPonto = nomeDependente.indexOf(ponto);

						Integer indVirgula = nomeDependente.indexOf(virgula);

						if ((nomeCompletoDependente == null || nomeCompletoDependente.equals("")) && (indPonto > 0 || indVirgula > 0))
						{

							throw new WorkflowException("NOME DO " + cont + "º DEPENDENTE CONTÉM ABREVIATURAS(. OU ,). FAVOR INFORMAR O CAMPO 'NOME COMPLETO DO DEPENDENTE'!");

						}
						if (nomeCompletoDependente != null)
						{

							nomeCompletoDependente = converte(nomeCompletoDependente);

							wItemDependente.findField("nomCompletoDepend").setValue(nomeCompletoDependente);

							nomeDependente = converte(nomeDependente);

							wItemDependente.findField("nomDependente").setValue(nomeDependente);

						}
						cont++;

					}

					//Tratamento para % default de % Insalubridade e % Periculosidade
					String cargo = wEventoAtividade.findGenericValue("cargos.titcar");

					String codcar = wEventoAtividade.findGenericValue("cargos.codcar");

					EntityWrapper ewInsalubridadeXPericulosidade = retornaObjInsalubridadeXPericulosidade(codcar);
					
					if(NeoUtils.safeIsNotNull(ewInsalubridadeXPericulosidade)) {
						
						Boolean existeSelecao = null;
						Boolean existeAdicional = null;
						Boolean existeGratificacao = null;
						BigDecimal percentualInsalubridade = new BigDecimal(0.00);
						BigDecimal percentualPericulosidade = new BigDecimal(0.00);
						
						existeSelecao = (Boolean) ewInsalubridadeXPericulosidade.findGenericValue("existeSelecao");
						existeAdicional = (Boolean) ewInsalubridadeXPericulosidade.findGenericValue("existeAdicional");
						existeGratificacao = (Boolean) ewInsalubridadeXPericulosidade.findGenericValue("lancamento");
						percentualInsalubridade = (BigDecimal) ewInsalubridadeXPericulosidade.findGenericValue("percentualInsalubridade");
						percentualPericulosidade = (BigDecimal) ewInsalubridadeXPericulosidade.findGenericValue("percentualPericulosidade");
						
						
						wEventoAtividade.findField("existeSelecao").setValue(existeSelecao);
						wEventoAtividade.findField("exisAdicional").setValue(existeAdicional);
						wEventoAtividade.findField("lancamento").setValue(existeGratificacao);
						
						wEventoAtividade.findField("perInsalubridadeSele").setValue(percentualInsalubridade);
						wEventoAtividade.findField("perPericulosidade").setValue(percentualPericulosidade);
						
						if (codcar.contains("000000320") || codcar.contains("000001138")) {
							
							String numDRT = wEventoAtividade.findGenericValue("dadosVigilancia.numDRT");

							if (numDRT == null || numDRT.equals(""))
							{
								throw new WorkflowException("CARGO DE VIGILANTE EXIGE INSERÇÃO DE UM NÚMERO DE DRT!");
							}

							GregorianCalendar dtFormacao = wEventoAtividade.findGenericValue("dadosVigilancia.dtFormacCNV");

							GregorianCalendar dtReciclagem = wEventoAtividade.findGenericValue("dadosVigilancia.dtReciclagemCNV");

							if (dtFormacao == null)
							{
								throw new WorkflowException("OBRIGATÓRIA UMA DATA DE FORMAÇÃO PARA O VIGILANTE!");
							}

							Long diferencaFor = null;
							Long diferencaRec = null;

							//Acrescentamos 2 anos da data de formação e da 
							//reciclagem para cruzar e calcular a diferença

							if (dtFormacao != null)
							{
								diferencaFor = ((dtadmissao.getTimeInMillis() - dtFormacao.getTimeInMillis()) + 3600000) / 86400000l;
							}

							if (dtReciclagem != null)
							{
								diferencaRec = ((dtadmissao.getTimeInMillis() - dtReciclagem.getTimeInMillis()) + 3600000) / 86400000l;
							}

							if (diferencaFor >= 547 && dtFormacao != null)
							{

								if (diferencaRec == null || diferencaRec >= 547 || dtReciclagem == null)
								{
									throw new WorkflowException("DATA DE FORMAÇÃO VENCIDA E NÃO EXISTE RECICLAGEM VÁLIDA(ATÉ 6 MESES DA DATA DE ADMISSÃO) PARA O COLABORADOR!");
								}

							}
							else if (dtFormacao == null && dtReciclagem == null)
							{
								throw new WorkflowException("NÃO É PERMITIDO AVANÇAR ESSA TAREFA SEM DATA DE FORMAÇÃO OU RECICLAGEM VÁLIDOS!");
							}
						}else if (cargo.toLowerCase().contains("supervisor")){
							
							BigDecimal valor = new BigDecimal("30.00");
							wEventoAtividade.findField("perPericulosidade").setValue(valor);

							wEventoAtividade.findField("exisAdicional").setValue(Boolean.TRUE);
							wEventoAtividade.findField("existeSelecao").setValue(Boolean.FALSE);

							wEventoAtividade.findField("lancamento").setValue(Boolean.TRUE);
						}
						
						wEventoAtividade.findField("escondeASO").setValue(Boolean.FALSE);
						
					}
				}
				else
				{

					Boolean naoeSocial = wEventoAtividade.findGenericValue("naoeSocial");

					if (!naoeSocial)
					{

						if (contIntervaloUtil < 1)
						{

							throw new WorkflowException("DATA DE ADMISSÃO INCORRETA, POR FAVOR INSIRA UMA DATA COM PELO MENOS 01(UM) DIA ÚTIL(24HRS) DE INTERVALO.");

						}

					}

					String newObserv = wEventoAtividade.findGenericValue("validacao.observacao");

					wEventoAtividade.findField("observacao").setValue(newObserv);

					wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.FALSE);

					String newAcao = wEventoAtividade.findGenericValue("validacao.acao.descricao");

					wEventoAtividade.findField("descacao").setValue(newAcao);

					BigDecimal salarioPerso = wEventoAtividade.findGenericValue("valorSalarioPerso");

					Long tpSalario = wEventoAtividade.findGenericValue("tpSalario.codigo");

					if (salarioPerso != null)
						System.out.println("Salário personalizado: " + salarioPerso.compareTo(new BigDecimal(500)));

					//REGRA PARA EVITAR O SALÁRIO MENSALISTA PERSONALIZADO MENOR QUE R$500
					if (salarioPerso != null && salarioPerso.compareTo(new BigDecimal(500)) == -1 && tpSalario == 1)
					{

						new WorkflowException("ATENÇÃO!!! SALÁRIO INSERIDO É MUITO BAIXO PARA O TIPO MENSALISTA!");

					}

				}

				wEventoAtividade.findField("escondeASO").setValue(Boolean.FALSE);

			}
			//Enviar para Regional
			if (acao == 7)
			{
				
				Long codigoRegional = (Long) wEventoAtividade.findGenericValue("regionalColab.usu_codreg");
				NeoUser responsavelRegional = getRegionalUser(codigoRegional); 
				
				wEventoAtividade.findField("tarefaAtual").setValue("4. Imprimir Documentação - Solicitante");
				wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.FALSE);
				wEventoAtividade.findField("contErros").setValue(0l);
				wEventoAtividade.findField("exec1").setValue(responsavelRegional);

			}
			//Digitalização e Conferência	
			if (acao == 8)
			{

				wEventoAtividade.findField("tarefaAtual").setValue("5. Conferência da Documentação");
				wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.FALSE);
				wEventoAtividade.findField("contErros").setValue(0l);

			}
			//Enviar para Sede		
			if (acao == 9)
			{

				wEventoAtividade.findField("tarefaAtual").setValue("3. Imprimir Crachá - RH");
				wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.FALSE);
				wEventoAtividade.findField("contErros").setValue(0l);

			}
			//Finalizar Admissão	
			if (acao == 10)
			{

				///////////////////////////////////////////////////////////////////////////////////////////////////
				//////////////////////////// Monta os parâmetros para a tarefa de VT ////////////////////////////// 
				///////////////////////////////////////////////////////////////////////////////////////////////////
				
				GregorianCalendar dataAdmissao = new GregorianCalendar();
				
				String cidade = NeoUtils.safeString((String) wEventoAtividade.findGenericValue("cidcolab.nomcid"));
				Boolean usaVT = (Boolean) wEventoAtividade.findGenericValue("usaVT");
				
				String colaborador = NeoUtils.safeString((String) wEventoAtividade.findGenericValue("colaborador"));
				String empresa = NeoUtils.safeString((String) wEventoAtividade.findGenericValue("emp.asString"));
				String escalaVT = NeoUtils.safeString((String) wEventoAtividade.findGenericValue("listaVT.escalaEVT.asString"));
				Long matricula = (Long) wEventoAtividade.findGenericValue("matricula"); 
				dataAdmissao = wEventoAtividade.findGenericValue("dtAdmissao");

				System.out.println("[DEBUG 0] Tarefa VT - Col. "+colaborador+" "+empresa+"//"+matricula+" - "+cidade+" - "+usaVT);
				if(cidade.equalsIgnoreCase("Curitiba") && usaVT) {
					System.out.println("[DEBUG 1] Tarefa VT - Col. "+colaborador+" "+empresa+"//"+matricula+" - "+cidade+" - "+usaVT);
					abrirTarefaVT(colaborador, empresa, escalaVT, dataAdmissao, matricula);					
				}
				
				///////////////////////////////////////////////////////////////////////////////////////////////////
				
				wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.FALSE);
				wEventoAtividade.findField("contErros").setValue(0l);

				///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/////////////CRIA TAREFA SIMPLES PARA SOLICITAR ENVIO DO CONTRATO ORIGINAL PARA A SEDE
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//				String codProcess = event.getProcess().getCode();
//
//				String nomeCompColab = wEventoAtividade.findGenericValue("nomCompletoFC");
//
//				NeoUser solicitante = wEventoAtividade.findGenericValue("usersolic");
//
//				NeoPaper tsPapelSolicitante = OrsegupsUtils.getPaper("SolicitanteRequisitarEnvioContrato");
//				
//				NeoUser tsSolicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(tsPapelSolicitante);
//
//				NeoObject oTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
//
//				EntityWrapper wTarefaSimples = new EntityWrapper(oTarefaSimples);
//
//				wTarefaSimples.findField("Solicitante").setValue(tsSolicitante);
//
//				wTarefaSimples.findField("Executor").setValue(solicitante);
//
//				wTarefaSimples.findField("Titulo").setValue("Requisitar Envio do Contrato Original - "+nomeColaborador);
//
//				wTarefaSimples.findField("DescricaoSolicitacao").setValue("Solicito, por gentileza, que enviem o contrato original assinado do futuro colaborador: " + nomeCompColab + " para a Sede!<br><br>Processo de Admissão de código: " + codProcess + "<br><br> Desde já muito obrigado! <br><br> Att. <br><b>" + tsSolicitante.getFullName() + "</b>");
//
//				GregorianCalendar prazoTarefaSimples = new GregorianCalendar();
//
//				prazoTarefaSimples.add(Calendar.HOUR, 24);
//
//				Boolean diautil = OrsegupsUtils.isWorkDay(prazoTarefaSimples);
//
//				if (!diautil)
//				{
//
//					prazoTarefaSimples = OrsegupsUtils.getNextWorkDay(prazoTarefaSimples);
//
//				}
//
//				wTarefaSimples.findField("Prazo").setValue(prazoTarefaSimples);
//
//				ProcessModel pm = PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));
//
//				String dadosprocesso = OrsegupsWorkflowHelper.iniciaProcessoNeoIdCode(pm, oTarefaSimples, true, tsSolicitante, true);
//
//				String neoidProcess = dadosprocesso.substring(1, dadosprocesso.indexOf(";"));
//
//				Integer numCarac = dadosprocesso.length();
//
//				String codeProcess = dadosprocesso.substring(dadosprocesso.indexOf(";") + 1, numCarac);
//
//				//Registro de Atividades
//				List<NeoObject> listaAtividades = wEventoAtividade.findGenericValue("registroTarefas");
//
//				String descricao = "Tarefa Simples aberta solicitando envio do Contrato Original Assinado para a Sede.<br><br>NeoId: " + neoidProcess + " || Código: " + codeProcess;
//
//				String acaoAtividade = "Abertura Tarefa Simples";
//
//				NeoObject registro = adicionaRegistroAtividade(descricao, acaoAtividade);
//
//				listaAtividades.add(registro);

			}

			//Cancelar Admissão	
			if (acao == 11)
			{

				wEventoAtividade.findField("usuarioAprovador").setValue(Boolean.FALSE);
				wEventoAtividade.findField("contErros").setValue(0l);

			}

			if (!tarefa.equals("2. Validar Admissão") || acao == 2)
			{

				//Registro de Atividades
				List<NeoObject> listaAtividades = wEventoAtividade.findGenericValue("registroTarefas");

				String descricao = wEventoAtividade.findGenericValue("validacao.observacao");

				String acaoAtividade = wEventoAtividade.findGenericValue("validacao.acao.descricao");

				NeoObject registro = adicionaRegistroAtividade(descricao, acaoAtividade);

				listaAtividades.add(registro);

			}

			wEventoAtividade.findField("validacao.observacao").setValue(null);

		}
		catch (Exception e)
		{
			log.warn("ERRO NO FORMULÁRIO");
			e.printStackTrace();
			throw (WorkflowException) e;
		}

	}

	public NeoObject adicionaRegistroAtividade(String descricao, String acaoAtividade)
	{

		NeoObject oRegistrosAtividades = AdapterUtils.createNewEntityInstance("RHRegistroAtividadesAdmissao");

		EntityWrapper wRegistrosAtividades = new EntityWrapper(oRegistrosAtividades);

		wRegistrosAtividades.setValue("responsavel", PortalUtil.getCurrentUser());

		wRegistrosAtividades.setValue("atividade", acaoAtividade);

		wRegistrosAtividades.setValue("dataFinal", new GregorianCalendar());
		if (descricao == null)
		{
			wRegistrosAtividades.setValue("descricao", "");
		}
		else
		{
			wRegistrosAtividades.setValue("descricao", descricao);
		}

		PersistEngine.persist(oRegistrosAtividades);

		return oRegistrosAtividades;

	}

	public String converte(String text)
	{

		text = text.toLowerCase().replaceAll("[ãâàáä]", "a").replaceAll("[êèéë]", "e").replaceAll("[îìíï]", "i").replaceAll("[õôòóö]", "o").replaceAll("[ûúùü]", "u").replace('ç', 'c').replace('ñ', 'n').replaceAll("!", "").replaceAll("\\[\\´\\`\\?!\\@\\#\\$\\%\\¨\\*", " ").replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]", " ").replaceAll("[\\.\\;\\-\\_\\+\\'\\ª\\º\\:\\;\\/\\,]", " ").replaceAll("  ", " ").replaceAll("   ", " ").toUpperCase();

		return text;
	}

	public static void sortNeoId(Collection<? extends NeoObject> list)
	{
		Collections.sort((List<? extends NeoObject>) list, new Comparator<NeoObject>()
		{

			public int compare(NeoObject obj1, NeoObject obj2)
			{

				return ((Long) new EntityWrapper(obj1).findGenericValue("ordem") < (Long) new EntityWrapper(obj2).findGenericValue("ordem")) ? -1 : 1;
			}
		});
	}
	
	public static NeoUser getRegionalUser (Long regional) {
		
		NeoUser usuario = null;
		EntityWrapper ewResponsavel = null;
		NeoObject responsavel = null;
		
		try {
			
			responsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("rhResponsaveisRegional"), new QLEqualsFilter("regional", regional));
			
			if (NeoUtils.safeIsNotNull(responsavel)) {
				ewResponsavel = new EntityWrapper(responsavel);
				usuario = ewResponsavel.findGenericValue("responsavel");   
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return usuario;
	}
	
	public static void abrirTarefaVT(String nomeColaborador, String empresa, String escalaVT, GregorianCalendar dataAdmissao, Long matricula) {

		String erro = "Erro ao efetuar a abertura da tarefa de VT. Colaborador: "+nomeColaborador+" - Empresa: "+empresa+"//"+matricula;
		
		try {
			System.out.println("[DEBUG 2] Tarefa VT Abertura - Col. "+nomeColaborador+" "+empresa+"//"+matricula);
			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

			NeoPaper papelSolicitante = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "RH01SolicitanteTarefaVT"));
			NeoPaper papelExecutor = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "RH01ExecutorTarefaVT"));

			NeoUser solicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelSolicitante);
			NeoUser executor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);

			String titulo = "Pagamento de VT - "+nomeColaborador;

			String descricao = montaDescricao(nomeColaborador, empresa, escalaVT, dataAdmissao, matricula);

			GregorianCalendar prazo = new GregorianCalendar();
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
			prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			prazo.set(GregorianCalendar.MINUTE, 59);
			prazo.set(GregorianCalendar.MILLISECOND, 59);


			iniciarTarefaSimples.abrirTarefa(solicitante.getCode(), executor.getCode(), titulo, descricao, "1", "sim", prazo);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(erro);
			throw new WorkflowException(erro);
		}

	}
	
	public static String montaDescricao(String nomeColaborador, String empresa, String escalaVT, GregorianCalendar dtAdmissao, Long matricula) {
		
		String dataAdmissao = NeoDateUtils.safeDateFormat(dtAdmissao, "dd/MM/yyyy");
		
		StringBuilder corpoTarefa = new StringBuilder();
		
		corpoTarefa.append(" <html>                                                   ");
        corpoTarefa.append("    <head>                                                ");
        corpoTarefa.append("        <meta charset='utf-8'>                            ");
        corpoTarefa.append("    </head>                                               ");
		corpoTarefa.append("      <body>                                              ");
		corpoTarefa.append("          <table border='2'>                              ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("                  <th>EMPRESA</th>                    	  ");
		corpoTarefa.append("                  <td>&nbsp; "+empresa+" </td>            ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>MATRICULA</th>                        ");
		corpoTarefa.append("              	<td>&nbsp; "+matricula+" </td>            ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>COLABORADOR</th>                 	  ");
		corpoTarefa.append("                <td>&nbsp; "+nomeColaborador+" </td>      ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>DATA ADMISSÃO</th>                    ");
		corpoTarefa.append("                <td>&nbsp; "+dataAdmissao+" </td>         ");
		corpoTarefa.append("              </tr>                                       ");
		corpoTarefa.append("              <tr>                                        ");
		corpoTarefa.append("              	<th>ESCALA VT</th>                        ");
		corpoTarefa.append("                <td>&nbsp; "+escalaVT+" </td>             ");
		corpoTarefa.append("              </tr>                                       ");     
		corpoTarefa.append("          </table>                                        ");
		corpoTarefa.append("      </body>                                             ");
		corpoTarefa.append(" </html>                                                  ");
		
		return corpoTarefa.toString();
		
		
	}
	
	public static EntityWrapper retornaObjInsalubridadeXPericulosidade(String cargo) {

		NeoObject objeto = null;
		EntityWrapper wObjeto = null;

		try {
			objeto = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("RHInsalubridadeXPericulosidade"), new QLEqualsFilter("cargo.__neoPK.codcar", cargo));
			if(NeoUtils.safeIsNotNull(objeto)) {
				wObjeto = new EntityWrapper(objeto);				
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERRO ao obter as informações de Periculosidade/Insalubridade do cargo ("+cargo+"). Verifique o formulário [RH] Insalubridade X Periculosidade");
			throw new WorkflowException("ERRO ao obter as informações de Periculosidade/Insalubridade do cargo ("+cargo+"). Verifique o formulário [RH] Insalubridade X Periculosidade");
		}

		return wObjeto;
	}
	
}

