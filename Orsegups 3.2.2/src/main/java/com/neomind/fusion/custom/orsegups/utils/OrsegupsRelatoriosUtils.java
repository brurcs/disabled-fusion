package com.neomind.fusion.custom.orsegups.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.sigma.SigmaSinistrosVO;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class OrsegupsRelatoriosUtils {
    private static final Log log = LogFactory.getLog(OrsegupsRelatoriosUtils.class);
    private static final String NOME_RELATORIO = "RelatorioArrombamentos.jasper";
    private static final String NOME_SUB_RELATORIO = "RelatorioArrombamentos_subreport.jasper";

    public static File getRelatorioArrombamentos(String siglaRegional, GregorianCalendar dataInicial, GregorianCalendar dataFinal, Integer motivo, Boolean isProcessoAutomatico) {
	File fileProposta = null;
	Connection connSigma = null;

	
	
	try {

	    connSigma = PersistEngine.getConnection("SIGMA90");

	    StringBuffer querySinistros = new StringBuffer();

	    querySinistros.append(" SELECT c.ID_CENTRAL, e.NM_FANTASIA, cid.NOME, c.FANTASIA, c.RAZAO, c.RESPONSAVEL, h.DT_RECEBIDO, ");
	    querySinistros.append(" h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, v.NM_PLACA, c.ARMADA, h.TX_OBSERVACAO_FECHAMENTO, ");
	    querySinistros.append(" SUBSTRING(r.NM_ROTA, 1, 3) AS REGIONAL, h.CD_EVENTO, h.TX_OBSERVACAO_GERENTE, HFE.NM_FRASE_EVENTO, ");
	    querySinistros.append(" c.CGCCPF, c.ENDERECO, c.FONE1, c.FONE2, c.FG_POSSUI_SEGURO, c.CD_SEGURO_TIPO ");
	    querySinistros.append(" FROM HISTORICO_ALARME h ");
	    querySinistros.append(" INNER JOIN dbCENTRAL c ON h.CD_CLIENTE = c.CD_CLIENTE ");
	    querySinistros.append(" INNER JOIN EMPRESA e ON e.CD_EMPRESA = c.ID_EMPRESA ");
	    querySinistros.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
	    querySinistros.append(" INNER JOIN HISTORICO_FRASE_EVENTO HFE ON HFE.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
	    querySinistros.append(" LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE ");
	    querySinistros.append(" LEFT JOIN VIATURA v ON v.CD_VIATURA = h.CD_VIATURA ");
	    if (motivo != null && motivo > 0) {
		querySinistros.append(" WHERE h.CD_MOTIVO_ALARME = " + motivo);
	    } else {
		querySinistros.append(" WHERE h.CD_MOTIVO_ALARME in (36) ");
	    }
	    querySinistros.append(" AND h.DT_FECHAMENTO BETWEEN ? AND ? ");
	    if (!siglaRegional.isEmpty()) {
		querySinistros.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) = ? ");
	    }
	    querySinistros.append(" AND h.CD_USUARIO_FECHAMENTO != 9999");

	    PreparedStatement stSinistros = connSigma.prepareStatement(querySinistros.toString());
	    stSinistros.setTimestamp(1, new Timestamp(dataInicial.getTimeInMillis()));
	    if (!isProcessoAutomatico) {
		dataFinal.set(Calendar.HOUR_OF_DAY, 23);
		dataFinal.set(Calendar.MINUTE, 59);
		dataFinal.set(Calendar.SECOND, 59);
	    }
	    stSinistros.setTimestamp(2, new Timestamp(dataFinal.getTimeInMillis()));
	    if (!siglaRegional.isEmpty()) {
		stSinistros.setString(3, siglaRegional);
	    }
	    
	    
	    log.error("RELATÓRIO DE ARROMBAMENTO INICIOU GERAÇÃO DO PDF " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));  
	    
	    
	    ResultSet rsSinistros = stSinistros.executeQuery();

	    List<SigmaSinistrosVO> listaSinistros = new ArrayList<SigmaSinistrosVO>();
	    while (rsSinistros.next()) {
		
		log.error("RELATÓRIO DE ARROMBAMENTO ENCONTROU SINISTROS");  
		
		SigmaSinistrosVO sinistro = new SigmaSinistrosVO();

		sinistro.setCodigo(rsSinistros.getString("ID_CENTRAL"));
		sinistro.setEmpresaNomeFantasia(rsSinistros.getString("NM_FANTASIA"));
		sinistro.setNomeCidade(rsSinistros.getString("NOME"));
		sinistro.setNomeCentral(rsSinistros.getString("FANTASIA"));
		sinistro.setCentralRazaoSocial(rsSinistros.getString("RAZAO"));
		sinistro.setResponsavelCentral(rsSinistros.getString("RESPONSAVEL"));
		sinistro.setDataSinistro(rsSinistros.getTimestamp("DT_RECEBIDO"));
		sinistro.setDataDeslocamentoViatura(rsSinistros.getTimestamp("DT_VIATURA_DESLOCAMENTO"));
		sinistro.setDataViaturaLocal(rsSinistros.getTimestamp("DT_VIATURA_NO_LOCAL"));
		sinistro.setPlacaViatura(rsSinistros.getString("NM_PLACA"));
		sinistro.setCentralArmada(rsSinistros.getString("ARMADA"));
		sinistro.setFraseEvento(rsSinistros.getString("NM_FRASE_EVENTO"));
		sinistro.setCgccpf(rsSinistros.getString("CGCCPF"));
		sinistro.setEndereco(rsSinistros.getString("ENDERECO"));
		String telefone = rsSinistros.getString("FONE1");
		if (telefone != null){
		    String telefone2 = rsSinistros.getString("FONE2");
		    if (telefone2 != null){
			telefone += " / "+telefone2;
		    }
		}else{
		    telefone = rsSinistros.getString("FONE2");		    
		}
		sinistro.setTelefone(telefone);
		boolean fgPossuiSeguro = rsSinistros.getBoolean("FG_POSSUI_SEGURO");
		Integer tipoSeguro = rsSinistros.getInt("CD_SEGURO_TIPO");
		if (fgPossuiSeguro && tipoSeguro != null && (tipoSeguro.equals(10) || tipoSeguro.equals(6))){
		    sinistro.setIsClientePGO("SIM");
		}  else{
		    sinistro.setIsClientePGO("NÃO");
		}
		
		String obsFechamento = rsSinistros.getString("TX_OBSERVACAO_FECHAMENTO");
		
		obsFechamento += "\n\n "+ rsSinistros.getString("TX_OBSERVACAO_GERENTE");
		
		
		System.out.println(obsFechamento);
		
		sinistro.setObsFechamento(obsFechamento);
	
		sinistro.setCodigoEvento(rsSinistros.getString("CD_EVENTO"));

		listaSinistros.add(sinistro);
	    }
	    if (listaSinistros != null && listaSinistros.size() > 0) {
		log.error("RELATÓRIO DE ARROMBAMENTO INICIOU GERAÇÃO DO PDF "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));  
		fileProposta = geraPDFArrombamentos(listaSinistros);
	    }
	} catch (Exception e) {
	    Long key = GregorianCalendar.getInstance().getTimeInMillis();
	    log.error("[" + key + "] Erro na rotina de arrombamentos." + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro na rotina, procurar no log por [" + key + "]");
	} finally {
	    try {
		connSigma.close();
	    } catch (SQLException e) {
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		log.error("[" + key + "] Erro na rotina de arrombamentos." + e.getMessage());
		e.printStackTrace();
		throw new JobException("Erro na rotina, procurar no log por [" + key + "]");

	    }
	}
	return fileProposta;
    }
    
    public static File getRelatorioArrombamentosClientePGO(String siglaRegional, GregorianCalendar dataInicial, GregorianCalendar dataFinal, Integer motivo, Boolean isProcessoAutomatico) {
	File fileProposta = null;
	Connection connSigma = null;

	try {

	    connSigma = PersistEngine.getConnection("SIGMA90");

	    StringBuffer querySinistros = new StringBuffer();

	    querySinistros.append(" SELECT c.ID_CENTRAL, e.NM_FANTASIA, cid.NOME, c.FANTASIA, c.RAZAO, c.RESPONSAVEL, h.DT_RECEBIDO, ");
	    querySinistros.append(" h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, v.NM_PLACA, c.ARMADA, h.TX_OBSERVACAO_FECHAMENTO, ");
	    querySinistros.append(" SUBSTRING(r.NM_ROTA, 1, 3) AS REGIONAL, h.CD_EVENTO, h.TX_OBSERVACAO_GERENTE, HFE.NM_FRASE_EVENTO, ");
	    querySinistros.append(" c.CGCCPF, c.ENDERECO, c.FONE1, c.FONE2, c.FG_POSSUI_SEGURO, c.CD_SEGURO_TIPO ");
	    querySinistros.append(" FROM HISTORICO_ALARME h ");
	    querySinistros.append(" INNER JOIN dbCENTRAL c ON h.CD_CLIENTE = c.CD_CLIENTE ");
	    querySinistros.append(" INNER JOIN EMPRESA e ON e.CD_EMPRESA = c.ID_EMPRESA ");
	    querySinistros.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
	    querySinistros.append(" INNER JOIN HISTORICO_FRASE_EVENTO HFE ON HFE.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
	    querySinistros.append(" LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE ");
	    querySinistros.append(" LEFT JOIN VIATURA v ON v.CD_VIATURA = h.CD_VIATURA ");
	    if (motivo != null && motivo > 0) {
		querySinistros.append(" WHERE h.CD_MOTIVO_ALARME = " + motivo);
	    } else {
		querySinistros.append(" WHERE h.CD_MOTIVO_ALARME in (36) ");
	    }
	    querySinistros.append(" AND h.DT_FECHAMENTO BETWEEN ? AND ? ");
	    if (!siglaRegional.isEmpty()) {
		querySinistros.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) = ? ");
	    }
	    querySinistros.append(" AND h.CD_USUARIO_FECHAMENTO != 9999 AND c.FG_POSSUI_SEGURO = 1 AND c.CD_SEGURO_TIPO in (6,10) ");

	    PreparedStatement stSinistros = connSigma.prepareStatement(querySinistros.toString());
	    stSinistros.setTimestamp(1, new Timestamp(dataInicial.getTimeInMillis()));
	    if (!isProcessoAutomatico) {
		dataFinal.set(Calendar.HOUR_OF_DAY, 23);
		dataFinal.set(Calendar.MINUTE, 59);
		dataFinal.set(Calendar.SECOND, 59);
	    }
	    stSinistros.setTimestamp(2, new Timestamp(dataFinal.getTimeInMillis()));
	    if (!siglaRegional.isEmpty()) {
		stSinistros.setString(3, siglaRegional);
	    }
	    
	    
	    log.error("RELATÓRIO DE ARROMBAMENTO INICIOU GERAÇÃO DO PDF " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));  
	    
	    
	    ResultSet rsSinistros = stSinistros.executeQuery();

	    List<SigmaSinistrosVO> listaSinistros = new ArrayList<SigmaSinistrosVO>();
	    while (rsSinistros.next()) {
		
		log.error("RELATÓRIO DE ARROMBAMENTO ENCONTROU SINISTROS");  
		
		SigmaSinistrosVO sinistro = new SigmaSinistrosVO();

		sinistro.setCodigo(rsSinistros.getString("ID_CENTRAL"));
		sinistro.setEmpresaNomeFantasia(rsSinistros.getString("NM_FANTASIA"));
		sinistro.setNomeCidade(rsSinistros.getString("NOME"));
		sinistro.setNomeCentral(rsSinistros.getString("FANTASIA"));
		sinistro.setCentralRazaoSocial(rsSinistros.getString("RAZAO"));
		sinistro.setResponsavelCentral(rsSinistros.getString("RESPONSAVEL"));
		sinistro.setDataSinistro(rsSinistros.getTimestamp("DT_RECEBIDO"));
		sinistro.setDataDeslocamentoViatura(rsSinistros.getTimestamp("DT_VIATURA_DESLOCAMENTO"));
		sinistro.setDataViaturaLocal(rsSinistros.getTimestamp("DT_VIATURA_NO_LOCAL"));
		sinistro.setPlacaViatura(rsSinistros.getString("NM_PLACA"));
		sinistro.setCentralArmada(rsSinistros.getString("ARMADA"));
		sinistro.setFraseEvento(rsSinistros.getString("NM_FRASE_EVENTO"));
		sinistro.setCgccpf(rsSinistros.getString("CGCCPF"));
		sinistro.setEndereco(rsSinistros.getString("ENDERECO"));
		String telefone = rsSinistros.getString("FONE1");
		if (telefone != null){
		    String telefone2 = rsSinistros.getString("FONE2");
		    if (telefone2 != null){
			telefone += " / "+telefone2;
		    }
		}else{
		    telefone = rsSinistros.getString("FONE2");		    
		}
		sinistro.setTelefone(telefone);
		boolean fgPossuiSeguro = rsSinistros.getBoolean("FG_POSSUI_SEGURO");
		Integer tipoSeguro = rsSinistros.getInt("CD_SEGURO_TIPO");
		if (fgPossuiSeguro && tipoSeguro != null && (tipoSeguro.equals(10))){
		    sinistro.setIsClientePGO("SIM");
		} else{
		    sinistro.setIsClientePGO("NÃO");
		}
		
		String obsFechamento = rsSinistros.getString("TX_OBSERVACAO_FECHAMENTO");
		
		obsFechamento += "\n\n "+ rsSinistros.getString("TX_OBSERVACAO_GERENTE");
		
		
		System.out.println(obsFechamento);
		
		sinistro.setObsFechamento(obsFechamento);
	
		sinistro.setCodigoEvento(rsSinistros.getString("CD_EVENTO"));

		listaSinistros.add(sinistro);
	    }
	    if (listaSinistros != null && listaSinistros.size() > 0) {
		log.error("RELATÓRIO DE ARROMBAMENTO INICIOU GERAÇÃO DO PDF "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));  
		fileProposta = geraPDFArrombamentos(listaSinistros);
	    }
	} catch (Exception e) {
	    Long key = GregorianCalendar.getInstance().getTimeInMillis();
	    log.error("[" + key + "] Erro na rotina de arrombamentos." + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro na rotina, procurar no log por [" + key + "]");
	} finally {
	    try {
		connSigma.close();
	    } catch (SQLException e) {
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		log.error("[" + key + "] Erro na rotina de arrombamentos." + e.getMessage());
		e.printStackTrace();
		throw new JobException("Erro na rotina, procurar no log por [" + key + "]");

	    }
	}
	return fileProposta;
    }

    /**
     * Gera o PDF do relatório, utilizando JasperReports
     */
    public static File geraPDFArrombamentos(List<SigmaSinistrosVO> listaSinistros) {
	InputStream is = null;
	Map<String, Object> paramMap = new HashMap<String, Object>();
	String path = "";

	try {
	    // ...files/relatorios
	    path = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + NOME_RELATORIO;
//	    path = "C:\\ambiente_neomind\\workspace\\FusionOrsegups3.2.2\\src\\main\\resources\\reports\\RelatorioArrombamentos.jasper";
	    is = new BufferedInputStream(new FileInputStream(path));
	    paramMap = preencheParametros(listaSinistros); // obtém os
							   // parâmetros

	    if (paramMap != null) {

		File file = File.createTempFile("fusexp", ".pdf");
		file.deleteOnExit();

		JasperPrint impressao = JasperFillManager.fillReport(is, paramMap);
		if (impressao != null && file != null) {
		    JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
		    log.error("RELATÓRIO DE ARROMBAMENTO GEROU PDF "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));  
		    return file;
		}
	    }
	} catch (Exception e) {
	    Long key = GregorianCalendar.getInstance().getTimeInMillis();
	    log.error("[" + key + "] Erro na rotina de arrombamentos." + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro na rotina, procurar no log por [" + key + "]");
	}
	log.error("RELATÓRIO DE ARROMBAMENTO GERACAO PDF NULL "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss")); 
	return null;
    }

    /**
     * Preenche o mapa de parâmetros enviados ao relatório.
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> preencheParametros(List<SigmaSinistrosVO> listaSinistros) {
	Map<String, Object> paramMap = null;

	try {
	    paramMap = new HashMap<String, Object>();

	    paramMap.put("pathSub1", NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + NOME_SUB_RELATORIO);
//	    paramMap.put("pathSub1", "C:\\ambiente_neomind\\workspace\\FusionOrsegups3.2.2\\src\\main\\resources\\reports\\RelatorioArrombamentos_subreport.jasper");
	    paramMap.put("dataAtual", NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    paramMap.put("gridSinistros", listaSinistros);

	} catch (Exception e) {
	    Long key = GregorianCalendar.getInstance().getTimeInMillis();
	    log.error("[" + key + "] Erro na rotina de arrombamentos." + e.getMessage());  
	    e.printStackTrace();
	    throw new JobException("Erro na rotina, procurar no log por [" + key + "]");

	}
	return paramMap;
    }
}