package com.neomind.fusion.custom.orsegups.rsc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCInsereApontamentoCategoriaCancelamento implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			Long empresa = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
			Long filial = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
			Long contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");
			
			List<NeoObject> lstApontamentos = (List<NeoObject>) processEntity.findValue("listaApontamento");
			for (NeoObject objApontamento : lstApontamentos)
			{
				Integer seq = null;
				
				String nomeFonteDados = "SAPIENS";
				
				String sqlSelect = "SELECT USU_SEQMOV FROM USU_T160CMS ORDER BY USU_SEQMOV DESC";
				
				Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sqlSelect);
				List<Object> resultList = query.getResultList();
				if(resultList != null && !resultList.isEmpty())	{
					seq = (Integer) resultList.get(0);
					seq = seq + 1;
				}	
			
				EntityWrapper wrpApontamento = new EntityWrapper(objApontamento);
				Long posto = (Long) wrpApontamento.findValue("postoBonificacado.usu_numpos");
				GregorianCalendar dataMovimento = new GregorianCalendar();
				String servico = (String) wrpApontamento.findValue("postoBonificacado.usu_codser");
				String complementoServico = (String) wrpApontamento.findValue("postoBonificacado.usu_cplcvs");
				wrpApontamento.findValue("postoBonificacado.usu_qtdfun");
				wrpApontamento.findValue("postoBonificacado.usu_qtdcvs");
				BigDecimal precoUnitario = (BigDecimal) wrpApontamento.findValue("precoUnitario");
				BigDecimal percentualIss = (BigDecimal) wrpApontamento.findValue("postoBonificacado.usu_periss");
				BigDecimal percentualIrf = (BigDecimal) wrpApontamento.findValue("postoBonificacado.usu_perirf");
				BigDecimal percentualIns = (BigDecimal) wrpApontamento.findValue("postoBonificacado.usu_perins");
				BigDecimal percentualPit = (BigDecimal) wrpApontamento.findValue("postoBonificacado.usu_perpit");
				BigDecimal percentualCsl = (BigDecimal) wrpApontamento.findValue("postoBonificacado.usu_percsl");
				BigDecimal percentualCrt = (BigDecimal) wrpApontamento.findValue("postoBonificacado.usu_percrt");
				BigDecimal percentualOur = (BigDecimal) wrpApontamento.findValue("postoBonificacado.usu_perour");
				Long contaFinanceira = (Long) wrpApontamento.findValue("postoBonificacado.usu_ctafin");
				Long contareduzida = (Long)  wrpApontamento.findValue("postoBonificacado.usu_ctared");
				String centroCusto = (String) wrpApontamento.findValue("postoBonificacado.usu_codccu");
				GregorianCalendar dataInicio = (GregorianCalendar) wrpApontamento.findValue("postoBonificacado.usu_datini");
				GregorianCalendar dataFim = (GregorianCalendar) wrpApontamento.findValue("postoBonificacado.usu_datfim");
				String transacao = (String) wrpApontamento.findValue("postoBonificacado.usu_tnsser");
				GregorianCalendar competencia = (GregorianCalendar) wrpApontamento.findValue("competencia");
				Long tipoBonificacao = (Long) wrpApontamento.findValue("tipoBonificacao.codigoTipoBonificacao");
				String descAcreBonificacao = null;
				if(tipoBonificacao == 1L)
				{
					descAcreBonificacao = "-";
				}
				else
				{
					descAcreBonificacao = "+";
				}
				String obs = (String) wrpApontamento.findValue("observacaoNotaFiscal");
				
				
				RSCUtils.gravarApontamento(empresa, filial, contrato, posto, Long.parseLong(seq.toString()), dataMovimento, servico, complementoServico, precoUnitario, percentualIss, percentualIrf, percentualIns, 
						                   percentualPit, percentualCsl, percentualCrt, percentualOur, contaFinanceira, contareduzida, centroCusto, dataInicio, dataFim, transacao, 
						                   new GregorianCalendar(), competencia, obs, descAcreBonificacao);
			}			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Teste");
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub		
	}	
}
