package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class FCNAlteraExecutorParaGerente implements AdapterInterface {
	private static final Log log = LogFactory.getLog(FCNAlteraExecutorParaGerente.class);
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {

		System.out.println("##### INICIO ADAPTER: "+this.getClass().getSimpleName()+" - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		String mensagem = null;
		
		Long codigoRegional = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
		Boolean primeiraExecucao = (Boolean) processEntity.findValue("primeiraExecucao");
		
		try {
			
			QLFilter filter = new QLEqualsFilter("codigo", codigoRegional);
			NeoObject noResponsabilidade = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("FCNresponsabilidade"), filter);
			EntityWrapper wResponsabilidade = new EntityWrapper(noResponsabilidade);
			
			NeoPaper gerente = (NeoPaper) wResponsabilidade.findValue("respGerente");
			
			if(primeiraExecucao) {
				if(NeoUtils.safeIsNotNull(gerente) && !gerente.getAllUsers().isEmpty()) {
					processEntity.setValue("responsavelJustificativaEquipamento", gerente);
					processEntity.setValue("primeiraExecucao", false);
				}else {
					mensagem = "### ERRO: Não foi possível definir o responsável para execução. Papel: "+gerente.getName().toString();
					System.out.println(mensagem);
					throw new WorkflowException(mensagem);
				}
			}
			
		} catch (Exception e) {
			mensagem = "## [REG: "+codigoRegional+" - "+this.getClass().getSimpleName()+"] ERRO ao definir o Gerente responsável pela execução da atividade.";
			System.out.println(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		} finally {
			System.out.println("##### FIM ADAPTER: "+this.getClass().getSimpleName()+" - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}
}
