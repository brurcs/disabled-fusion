package com.neomind.fusion.custom.orsegups.ti.sigma.beans;

public class SIGMAEmpresa {
    
    private int codigo;
    private String razao;
    private String fantasia;
    private String responsavel;
    private String endereco;
    
    public int getCodigo() {
        return codigo;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public String getRazao() {
        return razao;
    }
    public void setRazao(String razao) {
        this.razao = razao;
    }
    public String getFantasia() {
        return fantasia;
    }
    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }
    public String getResponsavel() {
        return responsavel;
    }
    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }
    public String getEndereco() {
        return endereco;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    

}
