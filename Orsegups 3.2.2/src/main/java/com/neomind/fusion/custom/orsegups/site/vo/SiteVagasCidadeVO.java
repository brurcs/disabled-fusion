package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.ArrayList;
import java.util.Collection;

public class SiteVagasCidadeVO {
	
	private String mensagem;
	private String cidade;
	private Collection<SiteVagasVO> vagas = new ArrayList();
	
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public Collection<SiteVagasVO> getVagas() {
		return vagas;
	}
	public void setVagas(Collection<SiteVagasVO> vagas) {
		this.vagas = vagas;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
}
