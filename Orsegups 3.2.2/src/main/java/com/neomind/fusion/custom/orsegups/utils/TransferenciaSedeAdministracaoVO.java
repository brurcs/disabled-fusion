package com.neomind.fusion.custom.orsegups.utils;

public class TransferenciaSedeAdministracaoVO
{
	private Long numemp;
	private Long tipcol;
	private Long numcad;
	private Long numcpf;
	private String codtar;
	private String nomfun;
	private String titcar;
		
	public String getCodtar()
	{
		return codtar;
	}
	public void setCodtar(String codtar)
	{
		this.codtar = codtar;
	}
	public Long getNumemp()
	{
		return numemp;
	}
	public void setNumemp(Long numemp)
	{
		this.numemp = numemp;
	}
	public Long getTipcol()
	{
		return tipcol;
	}
	public void setTipcol(Long tipcol)
	{
		this.tipcol = tipcol;
	}
	public Long getNumcad()
	{
		return numcad;
	}
	public void setNumcad(Long numcad)
	{
		this.numcad = numcad;
	}
	public Long getNumcpf()
	{
		return numcpf;
	}
	public void setNumcpf(Long numcpf)
	{
		this.numcpf = numcpf;
	}
	public String getNomfun()
	{
		return nomfun;
	}
	public void setNomfun(String nomfun)
	{
		this.nomfun = nomfun;
	}
	public String getTitcar()
	{
		return titcar;
	}
	public void setTitcar(String titcar)
	{
		this.titcar = titcar;
	}
}
