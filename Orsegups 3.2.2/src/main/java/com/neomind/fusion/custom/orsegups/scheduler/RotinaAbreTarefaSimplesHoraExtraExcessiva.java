package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;


//com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaSimplesHoraExtraExcessiva
public class RotinaAbreTarefaSimplesHoraExtraExcessiva implements CustomJobAdapter
{

	@Override
	public void execute(CustomJobContext ctx)
	{
		System.out.println("#### Iniciando Processo de Abertura de Tarefas de Hora Extra Excessivas ####");
		GregorianCalendar dataAtual = new GregorianCalendar();
		dataAtual.add(GregorianCalendar.MONTH, 1);
		dataAtual.set(GregorianCalendar.DATE, 1);
		System.out.println(NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy HH:mm"));
		
		StringBuilder sql = new StringBuilder();
		
		sql.append(" select fun.numcad, fun.nomfun, sum(horas.horas-horas.horasDeb) horasExtras, orn.USU_CodReg, orn.NomLoc, orn.USU_LotOrn, esc.HorMes  from r034fun fun  ");
		sql.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON (fun.numloc = orn.numloc) ");
		sql.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
		sql.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= GetDate()) "); 
		sql.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc , ");
		sql.append(" ( ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  isnull(sum(sit.QtdHor),0) as horas, 0 as horasDeb  from dbo.R066sit sit ");
		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp) ");
		sql.append("                 where cal.perref = ? ");
		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu "); 
		sql.append("                 and sit.codsit in (451) ");
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) "); 
		sql.append("         union ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  0 as horas,  isnull(sum(sit.QtdHor),0) as horasDeb  from dbo.R066sit sit ");
		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp) ");
		sql.append("                 where cal.perref = ? ");
		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu "); 
		sql.append("                 and sit.codsit in (450) ");
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) "); 
		sql.append(" ) as horas "); 
		sql.append(" where  1=1 ");
		//sql.append(" and fun.numcad = ? and fun.numemp = ? and fun.tipcol = ? ");
		sql.append(" and ( horas.numcad = fun.numcad   and horas.numemp = fun.numemp and horas.tipcol = fun.tipcol ) ");
		sql.append(" group by fun.numcad, fun.nomfun, orn.USU_CodReg, orn.NomLoc, orn.USU_LotOrn, esc.HorMes ");
		sql.append(" having sum(horas.horas-horas.horasDeb) >= (esc.HorMes*0.20) "); // 20% do total que ele deve fazer no mês.

		
		Connection connection = OrsegupsUtils.getSqlConnection("VETORH") ;
		ResultSet rs = null;
		try
		{
			PreparedStatement pst = connection.prepareStatement(sql.toString());
			
			pst.setDate(1, new Date(dataAtual.getTimeInMillis()));
			pst.setDate(2, new Date(dataAtual.getTimeInMillis()));
			rs = pst.executeQuery();
			
			while (rs.next()){
				Long he = rs.getLong("horasExtras");
				
					
				Long codReg = rs.getLong("USU_CodReg");
				String lotacao = "";
				
				if (codReg == 0 && rs.getString("USU_LotOrn").equals("Coordenadoria Indenização Contratual"))
				{
					lotacao = rs.getString("NomLoc");
				}
				else if (codReg == 1L)
				{
					lotacao = rs.getString("NomLoc");
				}
				else if (codReg == 13L)
				{
					lotacao = rs.getString("NomLoc");
				}
				else
				{
					lotacao = rs.getString("USU_LotOrn");
				}
				
				System.out.println("[HORA EXTRA] - Abrir tarefa -> Funcionario: " + rs.getLong("numcad") + " - " + rs.getString("nomfun") + " codReg="+codReg+", lotacao"+lotacao );
				abrirTarefaSimples(rs.getLong("numcad"), rs.getString("nomfun"),he,codReg,lotacao);
					
				
			}
			rs.close();
			System.out.println("#### Finalizando Processo de Abertura de Tarefas de Hora Extra Excessivas ####");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public static void abrirTarefaSimples(Long numcad, String nomfun, Long horasExtras, Long codReg, String lotacao ){
		
		
		GregorianCalendar cpt = new GregorianCalendar();

				
		
		String msg = "<p>Prezado Gestor, o colaborador <b>"+numcad+"-"+nomfun+"</b> executou um saldo de "+OrsegupsUtils.getHorario(horasExtras)+" horas extras na competência, "+NeoUtils.safeDateFormat(cpt,"MM/yyyy")+".<br/> Por qual motivo?</p>";
		
		
		
		NeoObject oTarefa = AdapterUtils.createNewEntityInstance("Tarefa");
		
		final EntityWrapper wTarefa= new EntityWrapper(oTarefa);
		
		NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "dilmoberger" ) );
		NeoUser usuarioResponsavel = null;//PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
		
		
		NeoPaper papel = OrsegupsUtils.getPapelJustificarAfastamento(codReg, lotacao);

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuarioResponsavel = user;
				break;
			}
		}
		
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 2);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 23);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		
		
		wTarefa.findField("Solicitante").setValue(usuarioSolicitante);
		wTarefa.findField("Executor").setValue(usuarioResponsavel);
		wTarefa.findField("Titulo").setValue("Hora Extra em Excesso colaborador " + nomfun);
		wTarefa.findField("DescricaoSolicitacao").setValue(msg);
		wTarefa.findField("Prazo").setValue(OrsegupsUtils.getNextWorkDay(prazo));
		wTarefa.findField("dataSolicitacao").setValue(new GregorianCalendar());
		
		PersistEngine.persist(oTarefa);
		
		iniciaProcessoTarefaSimples(oTarefa, usuarioSolicitante, usuarioResponsavel);
	}

	public static Boolean iniciaProcessoTarefaSimples(NeoObject eformProcesso, NeoUser requester, NeoUser responsavel)
	{
		Boolean result = false;
	
		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
	
		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 11/03/2015
		 */
		
		final WFProcess processo = WorkflowService.startProcess(processModel, eformProcesso, false, requester);
		
		try
		{
			new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(requester);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println("[HORA EXTRA] - Abriu Tarefa simples n. " + processo.getCode());
		
		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
		
		return result;
	}

	

}
