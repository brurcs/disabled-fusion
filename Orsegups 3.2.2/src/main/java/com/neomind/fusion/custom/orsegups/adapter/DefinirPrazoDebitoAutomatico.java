package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class DefinirPrazoDebitoAutomatico implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			GregorianCalendar dataDataDebitoAutomaticoTemp = new GregorianCalendar();
			//OK
			if (activity.getName().equals("Definir Prazo Instrução de Cadastro"))
			{
				GregorianCalendar dataDebito = OrsegupsUtils.getSpecificWorkDay(dataDataDebitoAutomaticoTemp, 23L);
				dataDebito.set(Calendar.HOUR_OF_DAY, 23);
				dataDebito.set(Calendar.MINUTE, 59);
				dataDebito.set(Calendar.SECOND, 59);
				
				processEntity.setValue("dataInstrucaoCadastroDebito", dataDebito);
			}
			//OK
			if (activity.getName().equals("Definir Prazo Tratar Pendência"))
			{
				GregorianCalendar dataDebito = OrsegupsUtils.getSpecificWorkDay(dataDataDebitoAutomaticoTemp, 23L);
				dataDebito.set(Calendar.HOUR_OF_DAY, 23);
				dataDebito.set(Calendar.MINUTE, 59);
				dataDebito.set(Calendar.SECOND, 59);
				
				processEntity.findField("dataTratarPendenciaDebito").setValue(dataDebito);
			}
			
			if (activity.getName().equals("Definir Prazo Instrução de Cadastro Gatilho CDA"))//Definir Prazo Instrução de Cadastro Gatilho CDA + 5 dias
			{
				GregorianCalendar dataPrazo = null;
				GregorianCalendar dataDebito = (GregorianCalendar) processEntity.findField("dataConferenciaPagamento").getValue();
				
				dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebito, 0L);
				dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
				dataPrazo.set(Calendar.MINUTE, 59);
				dataPrazo.set(Calendar.SECOND, 59);
				
				processEntity.findField("dataGatilhoCDA").setValue(dataPrazo);
			}
			
			
			GregorianCalendar dataCor = new GregorianCalendar();
			dataCor.set(Calendar.DAY_OF_MONTH, 24);
			dataCor.set(Calendar.MONTH, 07);
			dataCor.set(Calendar.YEAR, 2015);
			
			if (activity.getName().equals("Definir Prazo Tratar Pendência Débito Gatilho CDA") && activity.getProcess().getStartDate().after(dataCor) )//Definir Prazo Tratar Pendência Débito Gatilho CDA + 5 dias
			{
				GregorianCalendar dataPrazo = null;
				GregorianCalendar dataDebito = (GregorianCalendar) processEntity.findField("dataConferenciaPagamentoTratarPendencia").getValue();
				GregorianCalendar dataDebitoAntigo = (GregorianCalendar) processEntity.findField("prazoConferencia").getValue();
				
				if (NeoUtils.safeIsNull(dataDebitoAntigo) && NeoUtils.safeIsNotNull(dataDebito)){
					dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebito, 0L);	
				}
				else if (NeoUtils.safeIsNull(dataDebito) && NeoUtils.safeIsNotNull(dataDebitoAntigo)){
					dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebitoAntigo, 0L);	
				}
				else if (NeoUtils.safeIsNotNull(dataDebitoAntigo) && NeoUtils.safeIsNotNull(dataDebito)) {
					if(dataDebito.after(dataDebitoAntigo)){
						dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebito, 0L);	
					}
					else{
						dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebitoAntigo, 0L);
					}
				}
							
				dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
				dataPrazo.set(Calendar.MINUTE, 59);
				dataPrazo.set(Calendar.SECOND, 59);
				
				processEntity.findField("dataGatilhoCDA").setValue(dataPrazo);
			}
			
			if (activity.getName().equals("Definir Prazo Tratar Pendência Débito Gatilho CDA") && activity.getProcess().getStartDate().before(dataCor) )//Definir Prazo Tratar Pendência Débito Gatilho CDA + 5 dias
			{
				GregorianCalendar dataPrazo = null;
				GregorianCalendar dataDebito = (GregorianCalendar) processEntity.findField("dataConferenciaPagamentoTratarPendencia").getValue();
				GregorianCalendar dataDebitoAntigo = (GregorianCalendar) processEntity.findField("prazoConferencia").getValue();
				
				if (NeoUtils.safeIsNull(dataDebitoAntigo) && NeoUtils.safeIsNotNull(dataDebito)){
					dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebito, 0L);	
				}
				else if (NeoUtils.safeIsNull(dataDebito) && NeoUtils.safeIsNotNull(dataDebitoAntigo)){
					dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebitoAntigo, 0L);	
				}
				else if (NeoUtils.safeIsNotNull(dataDebitoAntigo) && NeoUtils.safeIsNotNull(dataDebito)) {
					if(dataDebito.after(dataDebitoAntigo)){
						dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebito, 0L);	
					}
					else{
						dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebitoAntigo, 0L);
					}
				}
							
				dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
				dataPrazo.set(Calendar.MINUTE, 59);
				dataPrazo.set(Calendar.SECOND, 59);
				
				processEntity.findField("dataGatilhoCDA").setValue(dataPrazo);
			}
			
			//OK
			if (activity.getName().equals("Definir Prazo Conferir Pagamento Débito + 5 dias"))
			{
				GregorianCalendar dataPrazo = null;
				GregorianCalendar dataDebito = (GregorianCalendar) processEntity.findField("dataGatilhoCDA").getValue();
				
				dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataDebito, 5L);
				dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
				dataPrazo.set(Calendar.MINUTE, 59);
				dataPrazo.set(Calendar.SECOND, 59);
				
				processEntity.findField("dataConferenciaPagamentoDebotoAutomatico").setValue(dataPrazo);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ao definir prazo para Tarefa: "+activity.getCode()+", no adapter >>> "+activity.getName());
			throw new WorkflowException("Erro ao definir prazo para Tarefa: "+activity.getCode()+", no adapter >>> "+activity.getName());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
