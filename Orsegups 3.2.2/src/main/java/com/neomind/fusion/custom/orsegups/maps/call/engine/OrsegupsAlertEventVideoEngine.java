package com.neomind.fusion.custom.orsegups.maps.call.engine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.apache.axis.encoding.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import us.monoid.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.jms.NeoJMSPublisher;
import com.neomind.fusion.custom.orsegups.maps.call.vo.CallAlertEventVideoVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoHistoricoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.sigma.SigmaUtils;
import com.neomind.fusion.custom.orsegups.snep.SnepUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class OrsegupsAlertEventVideoEngine
{
	private static final Log log = LogFactory.getLog(OrsegupsAlertEventVideoEngine.class);

	private static OrsegupsAlertEventVideoEngine instance;

	public static final String filaOperacao = "";

	public static Map<String, CallAlertEventVideoVO> callingAlertEventVO;

	public static Boolean verificandoEventos = false;

	//private static final String LINK_SNEP = "http://192.168.20.241";

	private static String WEB_SERVICE_SIGMA = "http://192.168.20.218:8080/SigmaWebServices/";

	private OrsegupsAlertEventVideoEngine()
	{
		OrsegupsAlertEventVideoEngine.callingAlertEventVO = new HashMap<String, CallAlertEventVideoVO>();
	}

	public static OrsegupsAlertEventVideoEngine getInstance()
	{
		if (OrsegupsAlertEventVideoEngine.instance == null)
		{
			OrsegupsAlertEventVideoEngine.instance = new OrsegupsAlertEventVideoEngine();
		}

		return OrsegupsAlertEventVideoEngine.instance;
	}

	/*
	 * Metodo verifica se o evento for diferente de nulo e não existe não existe
	 * na fila de eventos temporário
	 */
	public void alert(EventoVO eventoVO, ViaturaVO viaturaVO)
	{
		// caso o algum operador ja esteja ligando para este evento nÃ£o realiza
		// nova ligacao
		if (eventoVO != null && !OrsegupsAlertEventVideoEngine.callingAlertEventVO.containsKey(eventoVO.getCodigoHistorico()))
		{
			try
			{
				String type = "call";
				LinkedList<String> operadoresDisponiveis = this.getOperadoresDisponiveis(OrsegupsAlertEventVideoEngine.filaOperacao, type);
				// verifica se tem algum operador disponivel
				if (operadoresDisponiveis != null && !operadoresDisponiveis.isEmpty())
				{
					String ramapOperador = operadoresDisponiveis.get(0);

					boolean pausaOk = this.pausarOperador(ramapOperador);

					if (pausaOk)
					{
						LinkedHashSet<ProvidenciaVO> numerosChamar = this.telefoneCliente(eventoVO);

						CallAlertEventVideoVO alertEventVo = new CallAlertEventVideoVO();
						alertEventVo.setCallEventoVO(eventoVO);
						alertEventVo.setCallViaturaVO(viaturaVO);
						alertEventVo.setNumerosChamar(numerosChamar);
						alertEventVo.setRamalOperador(ramapOperador);

						/*
						 * fernando.rebelo 09/03/2017
						 * A key utilizada para inserir o VO dentro do MAP, por alguma razão, não estava sendo encontrada dentro do metodo ligarParaCliente
						 * onde, lá dentr, utiliza o códigoHistorico para buscar o VO dentro do mesmo MAP
						 * 
						 * Fiz uma alteração para usar a mesma logica para inserir a key e enviar ao metodo
						 * Apesar da logica anterior estar aparentemente correta, haviam situações que dava problema
						 */
						OrsegupsAlertEventVideoEngine.callingAlertEventVO.put(eventoVO.getCodigoHistorico(), alertEventVo);

						this.ligarParaCliente(eventoVO.getCodigoHistorico());
					}
				}
			}
			catch (Exception e)
			{
			    log.error("[OrsegupsAlertEventVideoEngine] Erro enviar evento para tela");
			    e.printStackTrace();
			}
		}
	}

	/*
	 * busca no sneps quais operadores da fila estÃ£o disponiveis
	 */
	private LinkedList<String> getOperadoresDisponiveis(String fila, String type)
	{
		LinkedList<String> result = null;
		try
		{
			log.debug("Buscando lista de operadores disponiveis no Snep");
			// realizar a chamado ao snep para verificar quais operadoresestão livres no momento
			if (fila != null && !fila.isEmpty())
			{
				SnepUtils snepUtils = new SnepUtils();
				result = new LinkedList<String>();
				result.addAll(snepUtils.getOperadoresDisponiveis(fila, type));
			}
			log.debug("Solicitando ligação para Snep! Verificar status dos agentes na fila: " + fila);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * realiza a pausa do operador no snep evitando assim que ele receba
	 * ligaÃ§Ãµes da fila
	 */
	public boolean pausarOperador(String ramal)
	{
		log.debug("Pausadno operador no Snep! Ramal: " + ramal);
		String message = "";
		boolean pause = false;
		try
		{
			// realizar a chamado ao snep para pausar este operador no snep e retorna se conseguiu pausar
			if (ramal != null && !ramal.isEmpty() && ramal.length() == 4)
			{
				pause = pausarOperador(ramal);
			}
			log.debug("Solicitando ligação para Snep! Para pausaro Ramal:" + ramal + " mensagem : " + message);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return pause;
	}

	/*
	 * libera o opeador que estÃ¡ pausado no snep
	 */
	public boolean liberarOperador(String ramal)
	{
		log.debug("Liberando operador no Snep! Ramal: " + ramal);
		boolean unPause = false;
		String message = "";
		// realizar a chamado ao snep para liberar o operador pausado
		try
		{
			if (ramal != null && !ramal.isEmpty() && ramal.length() == 4)
			{
				SnepUtils snepUtils = new SnepUtils();
				unPause = snepUtils.liberarOperador(ramal);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		log.debug("Solicitando ligação para Snep! Para retirar a pausa do Ramal:" + ramal + " mensagem : " + message);
		return unPause;
	}

	/*
	 * busca uma lista de providenciaVO que são o mesmo objeto utilizado pelo
	 * CallCenter O objeto ProvidenciaVO contem o nome e o telefone dos contatos
	 */
	private LinkedHashSet<ProvidenciaVO> telefoneCliente(EventoVO eventoVO)
	{
		// buscar a lista de telefones que devem ser chamados em ordem deprioridade
		// LinkedHashSet mantém a ordem de inserção dos objetos
		LinkedHashSet<ProvidenciaVO> result = null;
		List<String> listaTelefone = null;
		ProvidenciaVO providenciaVO = null;
		log.debug("Buscando a lista de telefones do cliente para ligar! " + eventoVO.getCodigoEvento());
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try
		{
			connection = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT DBC.FONE1 AS TELEFONE1, DBC.FONE2 AS TELEFONE2, DBP.FONE1 AS TELEFONE3, DBP.FONE2 AS TELEFONE4 ");
			sql.append(" FROM dbPROVIDENCIA DBP WITH (NOLOCK) ");
			sql.append(" RIGHT JOIN dbCENTRAL DBC WITH (NOLOCK)  ON (DBC.CD_CLIENTE =  DBP.CD_CLIENTE)");
			sql.append(" WHERE DBC.ctrl_central = 1  AND DBC.CD_CLIENTE = " + eventoVO.getCodigoCliente() + " ORDER BY DBP.NU_PRIORIDADE_NIVEL2");
			statement = connection.prepareStatement(sql.toString());
			resultSet = statement.executeQuery();
			providenciaVO = new ProvidenciaVO();
			listaTelefone = new ArrayList<String>();
			result = new LinkedHashSet<ProvidenciaVO>();
			providenciaVO.setNome(eventoVO.getNomeEvento());
			int contador = 0;
			while (resultSet.next())
			{
				if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE1")) && !listaTelefone.contains(resultSet.getString("TELEFONE1")))
				{
					listaTelefone.add(new String(resultSet.getString("TELEFONE1")));
				}
				if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE2")) && !listaTelefone.contains(resultSet.getString("TELEFONE2")))
				{
					listaTelefone.add(new String(resultSet.getString("TELEFONE2")));
				}
				if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE3")) && !listaTelefone.contains(resultSet.getString("TELEFONE3")))
				{
					listaTelefone.add(new String(resultSet.getString("TELEFONE3")));
				}
				if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE4")) && !listaTelefone.contains(resultSet.getString("TELEFONE4")))
				{
					listaTelefone.add(new String(resultSet.getString("TELEFONE4")));
				}
				contador++;
			}
			if (listaTelefone != null && !listaTelefone.isEmpty())
			{
				contador = 0;
				for (String tel : listaTelefone)
				{
					try
					{
						String formato = "([0-9]{2}?)[0-9]{4}?-[0-9]{4}?";
						if ((tel == null) || (tel.length() != 13) || (!tel.matches(formato)))
						{
							tel = tel.replace("(", "").replace(")", "").replace("-", "").trim();
							tel = tel.replaceAll(" ", "");
							tel = tel.trim();
							//Long numero = Long.parseLong(tel);
							contador++;
							if (contador == 1)
								providenciaVO.setTelefone1(tel);
							if (contador == 2)
								providenciaVO.setTelefone2(tel);
							if (contador == 3)
								providenciaVO.setTelefone3(tel);
							if (contador == 4)
								providenciaVO.setTelefone4(tel);
							if (contador == 5)
								providenciaVO.setTelefone5(tel);
							if (contador == 6)
								providenciaVO.setTelefone6(tel);
						}
					}
					catch (NumberFormatException e)
					{
						log.error("Erro telefone  : " + tel + " Cliente : " + eventoVO.getCodigoCliente() + " Erro: " + e.getMessage());
						continue;
					}
				}
				providenciaVO.setCodigoCliente(Integer.parseInt(eventoVO.getCodigoCliente()));
				result.add(providenciaVO);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao efetuar a ligação evento" + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
		}
		return result;
	}

	// Corrige telefone com prefixo local
	private String verificarPrefixo(String destino)
	{

		try
		{

			LinkedList<String> prefix_list = new LinkedList<String>();
			String[] arr = { "3276", "3274", "3256", "3272", "3243", "3285", "3264", "3025", "3028", "3216", "3217", "3221", "3222", "3223", "3224", "3225", "3226", "3228", "3229", "3231", "3232", "3233", "3234", "3235", "3236", "3237", "3238", "3239", "3240", "3244", "3248", "3249", "3251", "3261", "3266", "3269", "3281", "3282", "3284", "3322", "3323", "3324", "3325", "3326", "3329", "3331", "3333", "3334", "3335", "3337", "3338", "3348", "3368", "3369", "3422", "3451", "3700", "3254",
					"3354", "3262", "3268", "3273", "3267", "3033", "3242", "3283", "3286", "3341", "3342", "3344", "3253", "3275", "3292", "3245", "3265", "3241", "3246", "3247", "3257", "3258", "3259", "3271", "3278", "3343", "3346", "3357", "3381", "3972", "3277", "3378", "3263", "3279", "3206" };

			for (String string : arr)
			{
				prefix_list.add(string);
			}
			if (destino != null && destino.isEmpty())
				log.error("Telefone destino nulo ou vazio!");

			if (destino.length() == 10 && destino.substring(0, 2) == "48")
			{
				destino = destino.substring(-8);
			}
			else if (destino.length() == 10)
			{
				destino = "0" + destino;
			}
			String prefix = null;
			
			/*
			 * Fernando.rebelo
			 * erro aqui, pois destino veio com tamanho 4, portanto o substring abaixo vai dar erro de index
			 * 06:14:25,840 ERROR OrsegupsAlertEventEngine:497 - ERRO NeoJMSPublisher DEPOIS DE EXECUTAR: String index out of range: 4
			 */
			try
			{
				prefix = destino.substring(3, 4);

				if (prefix_list.contains(prefix) && destino.substring(1, 2) == "48")
					destino = destino.substring(-8);
			}catch(Exception e)
			{
				e.getStackTrace();
				log.error("Erro no NeoJMSPublisher ao tratar o telefone de destino cujo prefixo = " + prefix);
			}
			
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return destino;
	}

	/*
	 * logica que vai selecionar qual proximo numero do cliente e realiza a
	 * ligacao ou finaliza o evento caso nÃ£o tenham mais ligacoes
	 */
	private void ligarParaCliente(String codigoHistorico)
	{
		log.debug("Ligando para cliente! " + codigoHistorico);
		
		long inicio = System.currentTimeMillis();
		
		System.out.println("[verificaEventos]["+inicio+"] 5.5.5.1 INICIOU ligarParaCliente - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
		
		try
		{
			CallAlertEventVideoVO alertEventVo = OrsegupsAlertEventVideoEngine.callingAlertEventVO.get(codigoHistorico);
			Boolean closeEvent = false;
			// System.out.println(alertEventVo.getNumerosChamar());
			if (alertEventVo != null && alertEventVo.getNumerosChamar() != null && !alertEventVo.getNumerosChamar().isEmpty())
			{
				// ProvidenciaVO providencia =
				// alertEventVo.getNumerosChamar().iterator().next();
			    
			    System.out.println("[verificaEventos]["+inicio+"] 5.5.5.2 INICIOU numeroChamar - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
				String numeroChamar = this.numeroChamar(alertEventVo);
				System.out.println("[verificaEventos]["+inicio+"] 5.5.5.3 FINALIZOU ligarParaCliente - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
				if (numeroChamar != null)
				{
					if (!numeroChamar.startsWith("0"))
						numeroChamar = verificarPrefixo(numeroChamar);
					
					log.debug("OrsegupsAlertEventVideoEngine SIGMA EVENTO HISTORICO getLigacaoSnep: " + alertEventVo.getRamalOperador() + " " + numeroChamar.trim());
					System.out.println("[verificaEventos]["+inicio+"] 5.5.5.4 FINALIZOU verificarPrefixo - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");

					// atualiza os dados do que vai ser impresso em tela
					alertEventVo.updateInfoWindow();
					
					System.out.println("[verificaEventos]["+inicio+"] 5.5.5.5 FINALIZOU updateInfoWindow - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
					// serializa o vo para json
					String alertVoJSON = null;
					Gson gson = new Gson();
					// dispara a mensagem JMS
					if(log.isDebugEnabled())
						log.debug("NeoJMSPublisher ANTES DE EXECUTAR: Agente:" + alertEventVo.getRamalOperador() + " Conta: " + alertEventVo.getCallEventoVO().getCodigoCentral() + " - Evento: " + alertEventVo.getCallEventoVO().getCodigoEvento() + " Ligando para: " + numeroChamar);
					// NeoJMSPublisher.getInstance().sendAlertEventVideoMessage(URLEncoder.encode(alertVoJSON,
					// "UTF-8"), alertEventVo.getRamalOperador());
					alertVoJSON = gson.toJson(alertEventVo);
					// System.out.println(alertVoJSON);

					
					boolean sucesso = false;
					
					System.out.println("[verificaEventos]["+inicio+"] 5.5.5.6 INICIOU sendAlertEventVideoMessage - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
					
					try
					{	
					    //PASSEI AAUQI 6
						NeoJMSPublisher.getInstance().sendAlertEventVideoMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), alertEventVo.getRamalOperador());
						sucesso = true;
					}
					catch (UnsupportedEncodingException e)
					{
						
						e.printStackTrace();
						if(log.isDebugEnabled())
							log.debug("ERRO VERIFICAR EVENTOS ramalFlag: verificarLigacaoPendente - " + e.getMessage());

					}
					catch (Exception e)
					{
						e.printStackTrace();
						if(log.isDebugEnabled())
							log.debug(e.getMessage());
					}
					System.out.println("[verificaEventos]["+inicio+"] 5.5.5.7 FINALIZOU sendAlertEventVideoMessage - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms - Sucesso: "+sucesso);
					
					// this.getLigacaoSnep(alertEventVo.getRamalOperador(),
					// numeroChamar.trim());
					// salva em log
					// String textoLog = "Ligando para cliente! " + numeroChamar
					// + " Ramal " + alertEventVo.getRamalOperador();
					// saveLog(alertEventVo.getCallEventoVO().getCodigoCliente(),
					// alertEventVo.getCallEventoVO().getCodigoHistorico(),
					// textoLog);
					
					
					//COMENTANDO ABAIXO DEVIDO A PROBLEMA DE LOG APARECER EM ATENDIMENTOS DIFERENTES
//					String operador = PortalUtil.getCurrentUser().getFullName();
//					String textoLog = " Op. CM - " + operador + ". Verificando imagens do cliente.";
					
					System.out.println("[verificaEventos]["+inicio+"] 5.5.5.8 INICIOU saveLog - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
					
//					saveLog(alertEventVo.getCallEventoVO().getCodigoCliente(), alertEventVo.getCallEventoVO().getCodigoHistorico(), textoLog);
					
					System.out.println("[verificaEventos]["+inicio+"] 5.5.5.9 FINALIZOU saveLog - historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
					if(log.isDebugEnabled())
						log.debug("NeoJMSPublisher DEPOIS DE EXECUTAR: Ramal:" + alertEventVo.getRamalOperador() + " Conta: " + alertEventVo.getCallEventoVO().getCodigoCentral() + " - Evento: " + alertEventVo.getCallEventoVO().getCodigoEvento() + " Ligado para: " + numeroChamar);
				}
				else
				{
					closeEvent = true;
				}
			}
			else
			{
				closeEvent = true;
			}

			if (closeEvent)
			{
			    System.out.println("[verificaEventos]["+inicio+"] 5.5.5.10 INICIOU closeEvent  historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
				this.fecharEvento(false, codigoHistorico, false, null, false);
				 System.out.println("[verificaEventos]["+inicio+"] 5.5.5.11 FINALIZOU closeEvent  historico: "+codigoHistorico+" Tempo: "+(System.currentTimeMillis() - inicio )+" ms");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO NeoJMSPublisher DEPOIS DE EXECUTAR: " + e.getMessage());

		}
	}

	public void getLigacaoSnep(String ramal, String numeroExterno)
	{
		String message = "";
		String ramalOrigem = null;
		String ramalDestino = null;
		try
		{

			ramalOrigem = ramal;
			ramalDestino = numeroExterno.trim();

			if (ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty())
			{
				SnepUtils snepUtils = new SnepUtils();
				snepUtils.getLigacaoSnep(ramalOrigem, ramalDestino);
			}
			log.debug("Solicitando ligação para Snep! Ramal:" + ramal + " - Numero externo: " + numeroExterno + " mensagem : " + message);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/*
	 * busca por um numero para chamar na lista atÃ© encontrar, caso ocntrÃ¡rio
	 * retorna null,
	 */
	private String numeroChamar(CallAlertEventVideoVO alertEventVo)
	{
		log.debug("Buscando proximo numero a ligar! " + alertEventVo.getCallEventoVO().getCodigoHistorico());
		String numeroChamar = null;
		Map<Integer, String> listaTelefones = null;
		try
		{

			ProvidenciaVO providencia = alertEventVo.getNumerosChamar().iterator().next();
			listaTelefones = new HashMap<Integer, String>();
			if (NeoUtils.safeIsNotNull(providencia.getTelefone1()) && !providencia.getTelefone1().isEmpty())
				listaTelefones.put(0, providencia.getTelefone1().replaceAll(" ", ""));
			if (NeoUtils.safeIsNotNull(providencia.getTelefone2()) && !providencia.getTelefone2().isEmpty())
				listaTelefones.put(1, providencia.getTelefone2().replaceAll(" ", ""));
			if (NeoUtils.safeIsNotNull(providencia.getTelefone3()) && !providencia.getTelefone3().isEmpty())
				listaTelefones.put(2, providencia.getTelefone3().replaceAll(" ", ""));
			if (NeoUtils.safeIsNotNull(providencia.getTelefone4()) && !providencia.getTelefone4().isEmpty())
				listaTelefones.put(3, providencia.getTelefone4().replaceAll(" ", ""));
			if (NeoUtils.safeIsNotNull(providencia.getTelefone5()) && !providencia.getTelefone5().isEmpty())
				listaTelefones.put(4, providencia.getTelefone5().replaceAll(" ", ""));
			if (NeoUtils.safeIsNotNull(providencia.getTelefone6()) && !providencia.getTelefone6().isEmpty())
				listaTelefones.put(5, providencia.getTelefone6().replaceAll(" ", ""));
			if (alertEventVo.getUltimoNumero() == null)
			{
				numeroChamar = listaTelefones.get(0);
				alertEventVo.setUltimoNumero(0);
			}
			else
			{

				if (alertEventVo.getUltimoNumero() < listaTelefones.size() - 1)
				{
					numeroChamar = listaTelefones.get(alertEventVo.getUltimoNumero() + 1);
					alertEventVo.setUltimoNumero(alertEventVo.getUltimoNumero() + 1);
				}
				else
				{
					numeroChamar = listaTelefones.get(0);
					alertEventVo.setUltimoNumero(0);
				}
			}
			alertEventVo.setUltimoNumeroChamado(numeroChamar);
			alertEventVo.getNumerosJaChamados().add(numeroChamar);
			// log.debug("Numero chamada snep: " + numeroChamar);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return numeroChamar;
	}

	/*
	 * adisiona mensagem no log do evento
	 */
	private void addEvetLog(EventoVO eventoVO, String message)
	{
		// logica para persistir a mensagem no log da viatura
		saveLog(eventoVO.getCodigoCliente(), eventoVO.getCodigoHistorico(), message);
		log.debug("Adicionando ao log ao evento! " + message);
	}

	/*
	 * logica que finailza o evento caso naÃµ tenha conseguid contato com o
	 * cliente ou a palavra chave esteja ok
	 */

	private void alteraDataUltimaLigacaoRamal(String ramal, String contaParticao)
	{
	    Connection conn = null;
	    PreparedStatement pstm = null;

	    StringBuilder sql = new StringBuilder();
	    
	    sql.append(" UPDATE D_CMFilaRamais SET dataLigacao = GETDATE(), conta = ?  ");
	    sql.append(" WHERE ramal = ? ");

	    try {
		conn = PersistEngine.getConnection("");
		pstm = conn.prepareStatement(sql.toString());
		
		pstm.setString(1, contaParticao);
		pstm.setString(2, ramal);

		pstm.executeUpdate();

	    } catch (SQLException e) {
		e.printStackTrace();
		log.error("##### ERRO SALVAR LOG EVENT ALERT alteraDataUltimaLigacaoRamal: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, null);
	    }
	    
//		try
//		{
//			QLGroupFilter filter = new QLGroupFilter("AND");
//			filter.addFilter(new QLEqualsFilter("ramal", ramal));
//			// View de Contas
//			List<NeoObject> ramais = null;
//			ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMFilaRamais"), filter);
//			PersistEngine.getEntityManager().flush();
//			if (ramais != null && !ramais.isEmpty())
//			{
//				NeoObject neoObject = (NeoObject) ramais.get(0);
//				EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
//				ramalUsuario.setValue("dataLigacao", new GregorianCalendar());
//				if (contaParticao != null && !contaParticao.isEmpty())
//					ramalUsuario.setValue("conta", contaParticao);
//				ramalUsuario.setValue("ramal", ramal);
//				PersistEngine.persist(neoObject);
//				PersistEngine.commit(true);
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
	}

	private boolean getRetiraPausaRamal(String ramal)
	{

		Boolean ramaisAtivos = Boolean.FALSE;
		
		Connection conn = null;
		PreparedStatement pstm = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE D_CMFilaRamais SET situacao=1 ");
		sql.append(" WHERE situacao=0 AND ramal=? ");

		try {
		    conn = PersistEngine.getConnection("");
		    pstm = conn.prepareStatement(sql.toString());

		    pstm.setString(1, ramal);

		    int rs = pstm.executeUpdate();

		    if (rs > 0){
			ramaisAtivos = Boolean.TRUE;
		    }

		} catch (SQLException e) {
		    e.printStackTrace();
		    log.error("##### ERRO SALVAR LOG EVENT ALERT getPausaRamal: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, null);
		}
		
//		List<NeoObject> ramais = null;
//		try
//		{
//			QLGroupFilter filter = new QLGroupFilter("AND");
//			filter.addFilter(new QLEqualsFilter("situacao", false));
//			filter.addFilter(new QLEqualsFilter("ramal", ramal));
//
//			ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMFilaRamais"), filter);
//			if (ramais != null && !ramais.isEmpty())
//			{
//				NeoObject neoObject = (NeoObject) ramais.get(0);
//
//				EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
//				ramalUsuario.setValue("situacao", true);
//				ramalUsuario.setValue("ramal", ramal);
//				PersistEngine.persist(neoObject);
//				PersistEngine.commit(true);
//				ramaisAtivos = Boolean.TRUE;
//				PersistEngine.getEntityManager().flush();
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//		ramais = null;
		return ramaisAtivos;
	}

	private void emEsperaEventoSigma(CallAlertEventVideoVO alertEventVo)
	{
		// logica para finalizar o evento no sigma
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		PreparedStatement preparedStatementUpdate = null;
		ResultSet rsH = null;

		try
		{
			if (alertEventVo.getCallEventoVO().getCodigoHistorico() != null)
			{
				String usuario = getUsuarioSigma();
				SigmaUtils sigmaUtils = new SigmaUtils();
				sigmaUtils.emEsperaEventoSigma(alertEventVo.getCallEventoVO().getCodigoHistorico(), usuario);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EM ESPERA emEsperaEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(null, preparedStatementHSelect, rsH);
			OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, null);
		}
		//

		log.debug("deslocando evento no Sigma! " + alertEventVo.getCallEventoVO().getCodigoHistorico());
	}

	public void cadastrarEvetLog(String codigoHistorico, String cdCliente, String message)
	{
		saveLog(cdCliente, codigoHistorico, message);
		log.debug("Adicionando ao log ao evento! " + message);
	}

	private void saveLog(String cdCliente, String cdHistorico, String textoLog)
	{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String texto = null;
		try
		{
			texto = textoLog;
			if (NeoUtils.safeIsNotNull(textoLog) && !textoLog.isEmpty() && !textoLog.contains(": Op. CM - "))
				texto = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + ": - " + textoLog + "###";
			conn = PersistEngine.getConnection("SIGMA90");
			StringTokenizer token = new StringTokenizer(texto, "###");
			conn.setAutoCommit(false);
			while (token.hasMoreTokens())
			{
				String textoLocal = token.nextToken();
				StringBuilder sqlUpdate = new StringBuilder();
				sqlUpdate.append(" UPDATE HISTORICO  SET TX_OBSERVACAO_FECHAMENTO = ? + (CHAR(10) + CHAR(13)) + TX_OBSERVACAO_FECHAMENTO  WHERE CD_HISTORICO = ?");
				st = conn.prepareStatement(sqlUpdate.toString());
				st.setString(1, textoLocal);
				st.setString(2, cdHistorico);
				int update = st.executeUpdate();
				
				int updateCount = 0;
				
				while (update < 1 && updateCount < 2){
				    update = st.executeUpdate();
				    updateCount ++;
				}
				
				
			}
			conn.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{

				e1.printStackTrace();
			}
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

	}

	/*
	 * fechar o evento no banco do sigma
	 */

	public void fecharEvento(Boolean contatoOk, String codigoHistorico, Boolean callAt, String ramal, Boolean alteracao)
	{
		if(log.isDebugEnabled())
			log.debug("Fechar evento! ContatoOk: " + contatoOk);
		try
		{
			CallAlertEventVideoVO alertEventVo = OrsegupsAlertEventVideoEngine.callingAlertEventVO.get(codigoHistorico);
			if (alertEventVo != null)
			{
				String operador = PortalUtil.getCurrentUser().getFullName();

				if (alteracao)
				{
				    if (alertEventVo.getCallEventoVO().getCodigoEvento().equals("XPE2")) {
					this.addEvetLog(alertEventVo.getCallEventoVO(), " Op. CM - " + operador + " - Evento em espera após ser feito o procedimento de ligação. Mas não finalizado no SIGMA. Ramal :" + alertEventVo.getRamalOperador() + ". EmEspera ComAlteracao");
				    }else{
					this.addEvetLog(alertEventVo.getCallEventoVO(), " Op. CM - " + operador + " - Evento em espera após ser feito a ronda online nas imagens do local monitorado. Mas não finalizado no SIGMA. Ramal :" + alertEventVo.getRamalOperador() + ". EmEspera ComAlteracao");
				    }
				    this.emEsperaEventoSigma(alertEventVo);
				}
				else if (contatoOk && callAt)
				{
					this.addEvetLog(alertEventVo.getCallEventoVO(), " Op. CM - " + operador + " - Evento encerrado após ser feito a ronda online nas imagens do local monitorado. Ramal :" + alertEventVo.getRamalOperador() + ". FecharEvento");
					this.fecharEventoSigma(alertEventVo);

				}
				else if (contatoOk && !callAt)
				{
					this.addEvetLog(alertEventVo.getCallEventoVO(), " Op. CM - " + operador + " - Evento em espera após ser feito a ronda online nas imagens do local monitorado. Mas não finalizado no SIGMA. Ramal :" + alertEventVo.getRamalOperador() + ". EmEspera");
					this.emEsperaEventoSigma(alertEventVo);
				}
				else if (!contatoOk && callAt)
				{
					this.addEvetLog(alertEventVo.getCallEventoVO(), " Op. CM - " + operador + " - Evento encerrado ser feito a ronda online nas imagens do local monitorado, ou inconsistência durante a visualização das imagens. Ramal :" + alertEventVo.getRamalOperador() + ". TratarSigma");
				}
				this.getRetiraPausaRamal(alertEventVo.getRamalOperador());
				String ramalStr = "";
				String contaStr = "";
				if (alertEventVo.getRamalOperador() != null)
					ramalStr = alertEventVo.getRamalOperador();
				if (alertEventVo.getCallEventoVO().getCodigoCentral() != null && alertEventVo.getCallEventoVO().getParticao() != null)
					contaStr = "Conta : " + alertEventVo.getCallEventoVO().getCodigoCentral() + "[" + alertEventVo.getCallEventoVO().getParticao() + "] Evento : " + alertEventVo.getCallEventoVO().getCodigoEvento() + " Historico : " + alertEventVo.getCallEventoVO().getCodigoHistorico() + " Cliente : " + alertEventVo.getCallEventoVO().getCodigoCliente();
				this.alteraDataUltimaLigacaoRamal(ramalStr, contaStr);
			}
			else if (ramal != null)
			{
				this.getRetiraPausaRamal(ramal);
				this.alteraDataUltimaLigacaoRamal(ramal, "CallAlertEventVideoVO null: Histórico: " + codigoHistorico);
			}

			if (codigoHistorico != null && OrsegupsAlertEventVideoEngine.callingAlertEventVO.containsKey(codigoHistorico))
				OrsegupsAlertEventVideoEngine.callingAlertEventVO.remove(codigoHistorico);

			Gson gson = new Gson();
			String alertVoJSON = gson.toJson(null);
			//PASSEI AQUI
			NeoJMSPublisher.getInstance().sendAlertEventVideoMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO NeoJMSPublisher: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
	}

	private void fecharEventoSigma(CallAlertEventVideoVO alertEventVo)
	{
		try
		{
			SigmaUtils sigmaUtils = new SigmaUtils();

			if (alertEventVo.getCallEventoVO().getCodigoHistorico() != null && sigmaUtils.verificaEventoHistorico(alertEventVo.getCallEventoVO().getCodigoHistorico()))
			{

				String usuario = getUsuarioSigma();

				sigmaUtils.fecharEventoSigma(alertEventVo.getCallEventoVO().getCodigoHistorico(), usuario);
				log.debug("OrsegupsAlertEventVideoEngine fechar eventos historico :  " + alertEventVo.getCallEventoVO().getCodigoHistorico());

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			log.debug("Fechando evento no Sigma! " + alertEventVo.getCallEventoVO().getCodigoHistorico());
		}

	}

	private String getUsuarioSigma()
	{
		NeoUser neoUser = null;
		if (PortalUtil.getCurrentUser() != null)
			neoUser = (NeoUser) PortalUtil.getCurrentUser();

		String cdUsuario = "11010";
		try
		{
			if (neoUser != null)
			{
				QLGroupFilter filterAnd = new QLGroupFilter("AND");
				filterAnd.addFilter(new QLEqualsFilter("usuarioFusion", neoUser));
				ArrayList<NeoObject> neoObject = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAUsuarioSigmaFusion"), filterAnd);
				PersistEngine.getEntityManager().flush();
				if (neoObject != null && !neoObject.isEmpty())
				{
					EntityWrapper entityWrapper = new EntityWrapper(neoObject.get(0));
					Long cdUsuarioL = (Long) entityWrapper.findValue("usuarioSigma.cd_usuario");
					cdUsuario = String.valueOf(cdUsuarioL);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsAlertEventVideoEngine getUsuarioSigma : " + e.getMessage());
		}
		
		return cdUsuario;
	}

	/*
	 * solicita para o snep a ligaÃ§Ã£o
	 */
	public void solicitarLigacaoSnep(String ramal, String numeroExterno)
	{
		// adicionar a logica que irá realizar a chamada ap Snep
		String message = "";

		String ramalOrigem = null;
		String ramalDestino = null;
		try
		{

			ramalOrigem = ramal;
			ramalDestino = numeroExterno.trim();

			if (ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty())
			{
				SnepUtils snepUtils = new SnepUtils();
				snepUtils.solicitarLigacaoSnep(ramalDestino, numeroExterno);
			}
			log.debug("Solicitando ligação para Snep! Ramal:" + ramal + " - Numero externo: " + numeroExterno + " mensagem : " + message);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/*
	 * metodo que vai realizar o controle de acordo com a aÃ§Ã£o que o operador
	 * tomar em tela ou seja se conseguiu contato ou nÃ£o com o cliente
	 */
	public void contato(Boolean contatoOk, String codigoHistorico, Boolean callAt, String ramal, Boolean alteracao)
	{
		log.debug("Contato realizado? " + contatoOk);
		try
		{

			if (alteracao)
			{
				this.fecharEvento(contatoOk, codigoHistorico, callAt, ramal, alteracao);
			}
			else if (contatoOk && callAt)
			{
				this.fecharEvento(contatoOk, codigoHistorico, callAt, ramal, alteracao);
			}
			else if (contatoOk && !callAt)
			{
				this.fecharEvento(contatoOk, codigoHistorico, callAt, ramal, alteracao);
			}
			else if (!contatoOk && callAt)
			{
				this.fecharEvento(contatoOk, codigoHistorico, callAt, ramal, alteracao);
			}
			else
			{
				this.ligarParaCliente(codigoHistorico);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void clearCache()
	{
		getCache();
		OrsegupsAlertEventVideoEngine.callingAlertEventVO.clear();
		OrsegupsAlertEventVideoEngine.verificandoEventos = false;
		OrsegupsAlertEventVideoEngine.instance = null;

	}

	public void getCache()
	{

		Set<String> chaves = OrsegupsAlertEventVideoEngine.callingAlertEventVO.keySet();
		for (String chave : chaves)
		{
			if (chave != null)
			{
				log.debug("---------------------------------------------------------------------------------------------------------------");
				log.debug("Historico: " + chave + " Ramal : " + OrsegupsAlertEventVideoEngine.callingAlertEventVO.get(chave).getRamalOperador() + " Evento : " + OrsegupsAlertEventVideoEngine.callingAlertEventVO.get(chave).getCallEventoVO().getCodigoEvento());
				log.debug("---------------------------------------------------------------------------------------------------------------");
			}
		}

	}

	// garante que não realizará duas verificações de eventos ao mesmo tempo
	public void verificaEventos()
	{

		Long timeExecFinal = GregorianCalendar.getInstance().getTimeInMillis();

		// medir tempo execucao
		Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
		System.out.println("[verificaEventos]["+timeExecFinal+"] 1 INICIO DA EXECUCAO - Next: getRamaisDisponiveis - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		EventoVO vo = null;
		List<String> telCal = new ArrayList<String>();
		telCal = (List<String>) getRamaisDisponiveis();

		System.out.println("[verificaEventos]["+timeExecFinal+"] 2 BUSCOU RAMAIS DISPONIVEIS Finish:getRamaisDisponiveis - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

		try
		{

			if (!OrsegupsAlertEventVideoEngine.verificandoEventos && telCal != null && !telCal.isEmpty())
			{
				OrsegupsAlertEventVideoEngine.verificandoEventos = true;
				// tipoEventos = getTipoAtendidmentoEvento(ramal);
				System.out.println("[verificaEventos]["+timeExecFinal+"] 3 INICIADO BUSCA DE EVENTOS Next: getEvento - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + " ms a lista de ramais igual a " + telCal.size());

				vo = new EventoVO();
				
				System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" RETORNOU NULLO PARA A LISTA DE EVENTOS "+telCal.toString());
				
				vo = (EventoVO) getEvento(telCal);
				
				String codigoHistorico = "";
				
				if (vo != null){
				    System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" BUSCOU O EVENTO DO METODO GETEVENTO "+vo.getCodigoHistorico());
				    codigoHistorico = vo.getCodigoHistorico();				    
				}else{
				    System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" BUSCOU O EVENTO NULLO PARA LISTA DE EVENTOS "+telCal.toString());
				}
								
				System.out.println("[verificaEventos]["+timeExecFinal+"] 4 BUSCOU EVENTOS Finish: getEvento - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + " ms a lista de ramais igual a " + telCal.size());

				// Verificar se existem os eventos E401 ,xxx2, xxx5 e x406 e chamar o metodo abaixo passando os eventos
				if (vo != null && !OrsegupsAlertEventVideoEngine.callingAlertEventVO.containsKey(vo.getCodigoHistorico()))
				{
					// System.out.println("OrsegupsAlertEventVideoEngine SIGMA EVENTO HISTORICO this alert : "
					// + vo.getCodigoHistorico());
					System.out.println("[verificaEventos]["+timeExecFinal+"] 5.1 INICIOU BUSCA DE DADOS NA PROVIDENCIA Next: getDadosProvidencia - Para o evento de historico: "+codigoHistorico+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
										
					this.alert(getDadosAcessoCliente(getDadosProvidencia(vo)), telCal);
					
					System.out.println("[verificaEventos]["+timeExecFinal+"] 5.2 FINALIZOU ENVIO DE EVENTO PARA TELA Finish: alert - Para o evento de historico: "+codigoHistorico+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

				}else{
				    System.out.println("[verificaEventos]["+timeExecFinal+"] 6 BUSCOU EVENTOS, MAS NÃO MANDOU NADA PARA A TELA (EVENTO EM MEMORIA OU NENHUM EVENTO SELECINADO) - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");				    
				}
				OrsegupsAlertEventVideoEngine.verificandoEventos = false;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("[verificaEventos]["+timeExecFinal+"] Erro na execução da rotina - Tempo: . " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms Erro : " + e);

		}
		finally
		{
			if (OrsegupsAlertEventVideoEngine.verificandoEventos)
				OrsegupsAlertEventVideoEngine.verificandoEventos = false;
		}
		System.out.println("[verificaEventos]["+timeExecFinal+"]7 FINALIZOU A VERIFICACAO DE EVENTOS: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms");
		
		String codigoHistorico = "";
		
		if (vo != null){
		    codigoHistorico = vo.getCodigoHistorico();
		}
		
		if ((GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) > 5000){
		    System.out.println("[verificaEventos][warn1]["+timeExecFinal+"]7 FINALIZOU A VERIFICACAO DE EVENTOS historico: "+codigoHistorico+"  " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms");
		}else if ((GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) > 10000){
		    System.out.println("[verificaEventos][warn2]["+timeExecFinal+"]7 FINALIZOU A VERIFICACAO DE EVENTOS historico: "+codigoHistorico+" " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms");
		}

	}

	// verificar se existem os usuários que estão autorizados para a atender pelo cliente
	private EventoVO getDadosAcessoCliente(EventoVO vo)
	{
		EventoVO eventoVOAux = null;
		try
		{
			if (vo != null && vo.getCodigoCliente() != null)
			{
				SigmaUtils sigmaUtils = new SigmaUtils();
				eventoVOAux = sigmaUtils.getDadosAcessoCliente(vo);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsAlertEventVideoEngine getDadosAcessoCliente : " + e.getMessage());
		}

		return eventoVOAux;
	}

	private EventoVO getDadosProvidencia(EventoVO vo)
	{
		EventoVO eventoVOAux = null;
		StringBuilder contentString = new StringBuilder();
		try
		{

			if (vo != null && vo.getCodigoCliente() != null)
			{
				SigmaUtils sigmaUtils = new SigmaUtils();
				eventoVOAux = sigmaUtils.getDadosProvidencia(vo);

				contentString.append("  <table class=\"table table-striped table-bordered\"  style=\"width: 100%;max-height:199px; font-family: verdana; font-size: 11px;\">");
				contentString.append(" <thead > ");
				contentString.append(" <tr data-sortable=\"true\" style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;\">");
				contentString.append(" <th data-sortable=\"true\">Providência</th>");
				contentString.append(" <th data-sortable=\"true\">Nome</th>");
				contentString.append(" <th data-sortable=\"true\">Telefone 1</th>");
				contentString.append(" <th data-sortable=\"true\">Telefone 2</th>");
				contentString.append(" <th data-sortable=\"true\">Email</th>");
				contentString.append(" <th data-sortable=\"true\">Propriedade</th>");
				contentString.append(" </tr>");
				contentString.append(" </thead>");
				contentString.append(" <tbody  style=\"width: 100%;;max-height:99px; font-family: verdana;padding: 2px 2px;font-size: 10px;overflow: auto;\">");

				if (eventoVOAux != null && eventoVOAux.getProvidenciaVOs() != null && !eventoVOAux.getProvidenciaVOs().isEmpty())
				{

					for (ProvidenciaVO providenciaVO : eventoVOAux.getProvidenciaVOs())
					{

						contentString.append(" <tr style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;\">");
						contentString.append(" <td style=\"white-space: normal\">" + providenciaVO.getCodigoProvidencia() + "</td>");
						contentString.append(" <td style=\"white-space: normal\">" + providenciaVO.getNome() + "</td>");
						contentString.append(" <td style=\"white-space: normal\"><span class=\"glyphicon glyphicon-phone-alt\" aria-hidden=\"true\">");
						contentString.append(" <span onclick='javascript:dial(\"0" + providenciaVO.getTelefone1() + "\");' style=\"font-family: verdana; font-size: 11px;font-weight: bold;\">  " + providenciaVO.getTelefone1() + " </span></span></td> ");
						contentString.append(" <td style=\"white-space: normal\"><span class=\"glyphicon glyphicon-phone-alt\" aria-hidden=\"true\">");
						contentString.append(" <span onclick='javascript:dial(\"0" + providenciaVO.getTelefone2() + "\");' style=\"font-family: verdana; font-size: 11px;font-weight: bold;\">  " + providenciaVO.getTelefone2() + " </span></span></td> ");
						contentString.append(" <td style=\"white-space: normal; text-align: right\">" + providenciaVO.getEmail() + "</td>");
						contentString.append(" <td style=\"white-space: normal; text-align: right\">" + providenciaVO.getPrioridade() + "</td>");
						contentString.append(" </tr>");

					}

					contentString.append(" </tbody>	");

					contentString.append(" </table> ");

				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsAlertEventVideoEngine SIGMA PROVIDENCIAS : " + e.getMessage());
		}

		return eventoVOAux;

	}

	public void verificarLigacaoPendente(String ramal)
	{

		boolean ramalFlag = false;
		CallAlertEventVideoVO callAlertEventVOs = null;
		Collection<CallAlertEventVideoVO> alertEventEngines = null;
		alertEventEngines = OrsegupsAlertEventVideoEngine.callingAlertEventVO.values();

		if (NeoUtils.safeIsNotNull(alertEventEngines) && !alertEventEngines.isEmpty())
		{
			for (CallAlertEventVideoVO callAlertEventVO : alertEventEngines)
			{

				callAlertEventVOs = (CallAlertEventVideoVO) callAlertEventVO;
				if (callAlertEventVO.getRamalOperador().equals(ramal) && verificaEventoHistorico(callAlertEventVO.getCallEventoVO().getCodigoHistorico()))
				{
					ramalFlag = true;
					break;
				}
			}
			if (ramalFlag)
			{

				CallAlertEventVideoVO alertEventVo = OrsegupsAlertEventVideoEngine.callingAlertEventVO.get(callAlertEventVOs.getCallEventoVO().getCodigoHistorico());

				if (NeoUtils.safeIsNotNull(callAlertEventVOs) && alertEventVo != null && alertEventVo.getRamalOperador() != null && !alertEventVo.getRamalOperador().isEmpty())
				{
					// serializa o vo para json
					Gson gson = new Gson();
					// dispara a mensagem JMS
					try
					{

						String alertVoJSON = gson.toJson(alertEventVo);
						//PASSEI AQUI 2
						NeoJMSPublisher.getInstance().sendAlertEventVideoMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), alertEventVo.getRamalOperador());
					}
					catch (UnsupportedEncodingException e)
					{
						
						e.printStackTrace();
						log.error("ERRO VERIFICAR EVENTOS: verificarLigacaoPendente - " + e.getMessage());
					}
					catch (Exception e)
					{
						e.printStackTrace();
						log.error(e.getMessage());
					}
				}
			}

		}
		if (!ramalFlag)
		{
			if (callAlertEventVOs != null && !verificaEventoHistorico(callAlertEventVOs.getCallEventoVO().getCodigoHistorico()))
			{
				OrsegupsAlertEventVideoEngine.callingAlertEventVO.remove(callAlertEventVOs.getCallEventoVO().getCodigoHistorico());

				Gson gson = new Gson();
				String alertVoJSON = gson.toJson(null);
				try
				{
				    //PASSEI AQUI 3
					NeoJMSPublisher.getInstance().sendAlertEventVideoMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);
				}
				catch (UnsupportedEncodingException e)
				{
					
					e.printStackTrace();
					log.error("ERRO VERIFICAR EVENTOS ramalFlag: verificarLigacaoPendente - " + e.getMessage());

				}
				catch (Exception e)
				{
					e.printStackTrace();
					log.error(e.getMessage());
				}

				getRetiraPausaRamal(ramal);
			}
			else if (callAlertEventVOs == null)
			{
				getRetiraPausaRamal(ramal);
			}

		}

	}

	public void verificarLigacaoPendenteHistorico(String ramal)
	{

		boolean ramalFlag = false;
		CallAlertEventVideoVO callAlertEventVOs = null;
		Collection<CallAlertEventVideoVO> alertEventEngines = null;
		alertEventEngines = OrsegupsAlertEventVideoEngine.callingAlertEventVO.values();

		if (NeoUtils.safeIsNotNull(alertEventEngines) && !alertEventEngines.isEmpty())
		{
			for (CallAlertEventVideoVO callAlertEventVO : alertEventEngines)
			{

				callAlertEventVOs = (CallAlertEventVideoVO) callAlertEventVO;
				if (callAlertEventVO.getRamalOperador().equals(ramal) && verificaEventoHistorico(callAlertEventVO.getCallEventoVO().getCodigoHistorico()))
				{
					ramalFlag = true;
					break;
				}
			}
		}
		if (!ramalFlag)
		{
			if (callAlertEventVOs != null && !verificaEventoHistorico(callAlertEventVOs.getCallEventoVO().getCodigoHistorico()))
			{
				OrsegupsAlertEventVideoEngine.callingAlertEventVO.remove(callAlertEventVOs.getCallEventoVO().getCodigoHistorico());

				Gson gson = new Gson();
				String alertVoJSON = gson.toJson(null);
				try
				{
				    //PASSEI AQUI 4
					NeoJMSPublisher.getInstance().sendAlertEventVideoMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);
				}
				catch (UnsupportedEncodingException e)
				{
					
					e.printStackTrace();
					log.error("ERRO VERIFICAR EVENTOS ramalFlag: verificarLigacaoPendente - " + e.getMessage());

				}
				catch (Exception e)
				{
					e.printStackTrace();
					log.error(e.getMessage());
				}

				getRetiraPausaRamal(ramal);
			}
			else if (callAlertEventVOs == null)
			{
				Gson gson = new Gson();
				String alertVoJSON = gson.toJson(null);
				try
				{	
				    //PASSEI AQUI 5
					NeoJMSPublisher.getInstance().sendAlertEventVideoMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);
				}
				catch (UnsupportedEncodingException e)
				{
					
					e.printStackTrace();
					log.error("ERRO VERIFICAR EVENTOS ramalFlag: verificarLigacaoPendente - " + e.getMessage());

				}
				catch (Exception e)
				{
					e.printStackTrace();
					log.error(e.getMessage());
				}
				getRetiraPausaRamal(ramal);
			}

		}
		else if (NeoUtils.safeIsNull(alertEventEngines) || (NeoUtils.safeIsNotNull(alertEventEngines) && alertEventEngines.isEmpty()))
		{
			Gson gson = new Gson();
			String alertVoJSON = gson.toJson(null);
			try
			{
			    //PASSEI AQUI 6
				NeoJMSPublisher.getInstance().sendAlertEventVideoMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);
			}
			catch (UnsupportedEncodingException e)
			{
				
				e.printStackTrace();
				log.error("ERRO VERIFICAR EVENTOS ramalFlag: verificarLigacaoPendente - " + e.getMessage());

			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error(e.getMessage());
			}

			getRetiraPausaRamal(ramal);
		}

	}

	// verifica se o evento ainda existe no historico
	private Boolean verificaEventoHistorico(String cdHistorico)
	{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		Boolean flag = Boolean.FALSE;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();
			// Verificar alteração
			sql.append(" SELECT H.CD_HISTORICO FROM HISTORICO H WITH (NOLOCK)  WHERE H.CD_HISTORICO = ?");
			st = conn.prepareStatement(sql.toString());

			st.setString(1, cdHistorico);
			rs = st.executeQuery();

			if (rs.next())
				flag = Boolean.TRUE;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT verificaEventoHistorico: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

		return flag;
	}

	public void abreOsSigma(String codigoHistorico, String descricao)
	{
		String complemento = " Operador : " + PortalUtil.getCurrentUser().getFullName() + " - ";
		complemento += " " + descricao;
		try
		{
			if (codigoHistorico != null && !codigoHistorico.isEmpty())
			{
				CallAlertEventVideoVO alertEventVo = OrsegupsAlertEventVideoEngine.callingAlertEventVO.get(codigoHistorico);
				if (alertEventVo.getCallEventoVO().getCodigoEvento().equals("XXX1"))
				{
					String tecResp = (String) getTecnicoResponsavel(alertEventVo.getCallEventoVO().getCodigoCliente());
					this.abrirOSSigma(alertEventVo.getCallEventoVO().getCodigoCliente(), alertEventVo.getCallEventoVO().getParticao(), alertEventVo.getCallEventoVO().getCodigoCentral(), alertEventVo.getCallEventoVO().getEmpresa(), "10010", "122", tecResp, "VERIFICAR FALHA X1. " + complemento);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	private String getTecnicoResponsavel(String cdCliente)
	{
		Connection conn = null;
		PreparedStatement stCol = null;
		PreparedStatement stTec = null;
		ResultSet rsCol = null;
		ResultSet rsTec = null;
		String cdTecnico = null;
		String tecResp = "";
		String nmRota = "";
		String insResp = "";
		Connection connSapiens = null;
		PreparedStatement st3 = null;
		ResultSet rsEquip = null;

		try
		{

			connSapiens = PersistEngine.getConnection("SAPIENS");
			StringBuffer sqlEquip = new StringBuffer();
			sqlEquip.append(" SELECT cvs.USU_CodSer");
			sqlEquip.append(" FROM USU_T160SIG sig");
			sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos");
			sqlEquip.append(" WHERE sig.usu_codcli = ?");
			sqlEquip.append(" AND cvs.usu_codser IN ('9002011', '9002004', '9002005', '9002014')");
			sqlEquip.append(" ORDER BY cvs.usu_sitcvs");
			st3 = connSapiens.prepareStatement(sqlEquip.toString());
			st3.setString(1, cdCliente);
			rsEquip = st3.executeQuery();

			// SOO - COM CAD - SUELLEN ROSENBROCK
			Integer colEquip = 107655;
			if (rsEquip.next())
			{
				colEquip = null;
			}
			conn = PersistEngine.getConnection("SIGMA90");

			StringBuilder sqlTec = new StringBuilder();
			sqlTec.append("    SELECT C.CD_TECNICO_RESPONSAVEL, R.NM_ROTA FROM dbCENTRAL C WITH (NOLOCK) ");
			sqlTec.append("    LEFT JOIN ROTA R ON R.CD_ROTA = C.ID_ROTA ");
			sqlTec.append("    WHERE C.CD_CLIENTE = ? ");
			stTec = conn.prepareStatement(sqlTec.toString());
			stTec.setString(1, cdCliente);
			rsTec = stTec.executeQuery();

			while (rsTec.next())
			{
				nmRota = rsTec.getString("NM_ROTA");
				tecResp = rsTec.getString("CD_TECNICO_RESPONSAVEL");
				break;
			}
			if (nmRota != null && !nmRota.isEmpty())
			{
				StringBuilder sqlCol = new StringBuilder();
				sqlCol.append("  SELECT CD_COLABORADOR FROM COLABORADOR WITH (NOLOCK) WHERE NM_COLABORADOR LIKE '" + nmRota.substring(0, 3) + "%ANL%' AND FG_ATIVO_COLABORADOR = 1 ");
				stCol = conn.prepareStatement(sqlCol.toString());
				rsCol = stCol.executeQuery();
				while (rsCol.next())
				{
					insResp = rsCol.getString("CD_COLABORADOR");
					break;
				}

			}

			if (colEquip != null)
			{
				cdTecnico = colEquip.toString();
			}
			else if (insResp != null && !insResp.isEmpty())
			{
				cdTecnico = insResp;
			}
			else if (tecResp != null && !tecResp.isEmpty())
			{
				cdTecnico = tecResp;
			}
			else if (cdTecnico == null || cdTecnico.isEmpty())
			{
				cdTecnico = "9999";
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO PESQUISAR RESP TEC EVENT ALERT verificaEventoHistorico: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, st3, rsEquip);
			OrsegupsUtils.closeConnection(null, stCol, rsCol);
			OrsegupsUtils.closeConnection(conn, stTec, rsTec);
		}
		return cdTecnico;

	}

	public void abrirOSSigma(String idCliente, String particao, String idCentral, String cdEmpresa, String solicitante, String defeito, String respTecnico, String descricao)
	{
		Map<String, String> map = new HashMap<String, String>();

		map.put("clientId", idCliente);
		map.put("partition", particao);
		map.put("centralId", idCentral);
		map.put("companyId", cdEmpresa);
		map.put("scheduledTime", "");
		map.put("estimatedTime", "");
		map.put("description", (descricao.length() >= 500 ? descricao.substring(0, 499) : descricao));
		map.put("requester", solicitante);
		map.put("defect", defeito);
		map.put("responsibleTechnician", respTecnico);

		String param2 = montaParametroOSString(map);
		try
		{
			String urlParameters = param2;
			String request = WEB_SERVICE_SIGMA + "ReceptorOSWebService";
			URL url = new URL(request);
			URLConnection conn = url.openConnection();

			conn.setDoOutput(true);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(urlParameters);
			writer.flush();

			String line;
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			while ((line = reader.readLine()) != null)
			{
				JSONObject json = new JSONObject(line);
				if (json.getString("status").equals("success"))
				{
					log.debug("Sucesso ao abrir OS tratamento de desvio de habito.");
				}
				else
				{
					log.debug("Exceção ao abrir OS tratamento de desvio de habito.");
				}
			}
			writer.close();
			reader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public static String montaParametroOSString(Map<String, String> map)
	{
		String parametro = "";

		/*
		 * clientId=?* &centralId=?* &partition=?* &companyId=?*
		 * &responsibleTechnician=?* &defect=?* &requester=?* &description=?
		 * &estimatedTime=? &scheduledTime=?
		 */
		if (map.get("clientId") != null && !map.get("clientId").equals(""))
		{
			parametro += "clientId" + "=" + map.get("clientId");
		}

		if (map.get("centralId") != null && !map.get("centralId").equals(""))
		{
			parametro += "&" + "centralId" + "=" + map.get("centralId");
		}

		if (map.get("partition") != null && !map.get("partition").equals(""))
		{
			parametro += "&" + "partition" + "=" + map.get("partition");
		}

		if (map.get("companyId") != null && !map.get("companyId").equals(""))
		{
			parametro += "&" + "companyId" + "=" + map.get("companyId");
		}

		if (map.get("responsibleTechnician") != null && !map.get("responsibleTechnician").equals(""))
		{
			parametro += "&" + "responsibleTechnician" + "=" + map.get("responsibleTechnician");
		}

		if (map.get("defect") != null && !map.get("defect").equals(""))
		{
			parametro += "&" + "defect" + "=" + map.get("defect");
		}

		if (map.get("requester") != null && !map.get("requester").equals(""))
		{
			parametro += "&" + "requester" + "=" + map.get("requester");
		}

		if (map.get("description") != null && !map.get("description").equals(""))
		{
			parametro += "&" + "description" + "=" + map.get("description");
		}

		if (map.get("estimatedTime") != null && !map.get("estimatedTime").equals(""))
		{
			parametro += "&" + "estimatedTime" + "=" + map.get("estimatedTime");
		}

		if (map.get("scheduledTime") != null && !map.get("scheduledTime").equals(""))
		{
			parametro += "&" + "scheduledTime" + "=" + map.get("scheduledTime");
		}

		try {
		    parametro = URLEncoder.encode(parametro,"UTF-8").replace("%3D", "=").replace("%26", "&");
		} catch (UnsupportedEncodingException e) {
		    log.error("[OrsegupsAlertEventVideoEngine] Erro ao montar URL para OS");
		    e.printStackTrace();
		}
		return parametro;
	}

	private EventoVO getEvento(List<String> telCal)
	{

		Set<String> listaEventos = new HashSet<String>();
		Long grupoCliente = null;
		Long timeExecFinal = GregorianCalendar.getInstance().getTimeInMillis();
		String ramalTemp = "";
		for (String valor : telCal)
		{

			String arrayValor[] = valor.split(";");

			if (arrayValor.length > 0)
			{
			    	
				String eventos[] = arrayValor[1].split(",");

				for (String evento : eventos)
				{
					listaEventos.add(evento);
				}

			}
			
			if (arrayValor != null){
			    System.out.println("#"+timeExecFinal+"# Video 1.1 Iniciou a busca do grupo. Ramal: "+arrayValor[0]);
			    grupoCliente = getGrupoRamal(arrayValor[0]);
			    ramalTemp = arrayValor[0];
			    System.out.println("#"+timeExecFinal+"# 1.2 Buscou grupo. Ramal: "+arrayValor[0]+" - Grupo:"+grupoCliente);
			    GregorianCalendar gc = new GregorianCalendar();
			    if (grupoCliente != null){
				if (grupoCliente.equals(220L)){ // Balaroti
				    System.out.println("#"+timeExecFinal+"# Video 1.3 Ramal: "+arrayValor[0]+" - Grupo:"+grupoCliente+" Balaroti");
				    int hora = gc.get(Calendar.HOUR_OF_DAY);
				    if (hora < 19 && hora > 7){
					grupoCliente = null;
				    }
				}else if (grupoCliente.equals(221L)){ // Globo
				    System.out.println("#"+timeExecFinal+"# Video 1.4 Ramal: "+arrayValor[0]+" - Grupo:"+grupoCliente+" Globo");
				    int dia = gc.get(Calendar.DAY_OF_WEEK);
				    int hora = gc.get(Calendar.HOUR_OF_DAY);
				    if (dia > 1 && dia < 7){
					if (hora < 18 && hora > 8){
					    grupoCliente = null;
					}
				    }
				}else{
				    System.out.println("#"+timeExecFinal+"# Video 1.5 Ramal: "+arrayValor[0]+" - Grupo:"+grupoCliente+" sem grupo");
				    grupoCliente = null;
				}
			    }
			}

		}

		String sqlParam = "";

		String sqlACP = " (H.CD_CODE = 'ACP') ";
		String sqlACV = " (H.CD_CODE = 'ACV') ";
		String sqlXPLE = " (h.CD_EVENTO = ('XPLE') AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0) ) ";	
		String sqlXPE2 = " (h.CD_EVENTO = ('XPE2') AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0) ) ";
		String sqlMPI = " (H.CD_CODE = 'MPI') ";

		int count = 0;

		if (listaEventos.contains("ACP"))
		{
			sqlParam += sqlACP;
			count++;
		}

		if (listaEventos.contains("ACV"))
		{
			if (count > 0)
			{
				sqlParam += " OR ";
			}
			sqlParam += sqlACV;
			count++;
		}

		if (listaEventos.contains("XPLE"))
		{
			if (count > 0)
			{
				sqlParam += " OR ";
			}
			sqlParam += sqlXPLE;
			count++;
		}
		
		if (listaEventos.contains("XPE2"))
		{
			if (count > 0)
			{
				sqlParam += " OR ";
			}
			sqlParam += sqlXPE2;
			count++;
		}
		
		if (listaEventos.contains("MPI")){
		    if (count > 0){
			sqlParam += " OR ";
		    }
		    sqlParam += sqlMPI;
		    count++;
		}
		
		
		StringBuilder sqlStr = new StringBuilder();
		// VERIFICAR ALTERAÇÃO
		sqlStr.append(" SELECT TOP(15) h.CD_HISTORICO, h.CD_EVENTO, h.NU_PRIORIDADE, c.CD_CLIENTE, c.CD_GRUPO_CLIENTE, h.FG_STATUS, fe.NM_FRASE_EVENTO,   ");
		sqlStr.append(" h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO, c.ID_CENTRAL, c.PARTICAO,  ");
		sqlStr.append(" c.FANTASIA, c.RAZAO, c.ENDERECO, cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, TX_OBSERVACAO_FECHAMENTO,  ");
		sqlStr.append(" TX_OBSERVACAO_GERENTE, r.TELEFONE, c.DATANOVAX2, c.COMPLEMENTO, e.NOME AS NOMEESTADO,  r.NM_ROTA , c.ID_EMPRESA,");
		sqlStr.append(" c.PERGUNTA AS PERGUNTA, c.RESPOSTA AS RESPOSTA, c.RESPONSAVEL AS RESPONSAVEL, c.EMAILRESP AS EMAIL,c.FONE1, c.FONE2,");
		sqlStr.append(" c.cgccpf  , c.cep, em.NM_RAZAO_SOCIAL, c.PROVIDENCIA,c.OBSTEMP, c.FANTASIA, c.NM_SENHA_COACAO, H.CD_CODE, c.SERVIDORCFTV, c.PORTACFTV, c.USERCFTV, c.SENHACFTV, h.DS_TIPO_EVENTO_RECEBIDO   ");
		sqlStr.append(" FROM HISTORICO h WITH(NOLOCK) INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		sqlStr.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK)  ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO LEFT JOIN dbBAIRRO bai WITH (NOLOCK)");
		sqlStr.append(" ON bai.ID_BAIRRO = c.ID_BAIRRO  LEFT JOIN dbCIDADE cid WITH (NOLOCK) " + " ON cid.ID_CIDADE = c.ID_CIDADE INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA");
		sqlStr.append(" LEFT JOIN dbESTADO e WITH (NOLOCK)  ON e.ID_ESTADO = cid.ID_ESTADO INNER JOIN EMPRESA em WITH (NOLOCK) ON em.CD_EMPRESA = c.ID_EMPRESA  ");
		sqlStr.append(" WHERE c.ctrl_central = 1 ");
		sqlStr.append(" AND ( " + sqlParam + " ) ");
		sqlStr.append(" AND (h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%FecharEvento%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%EmEspera%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%TratarSigma%' )  ");		
		if (grupoCliente != null){
		    sqlStr.append(" AND c.CD_GRUPO_CLIENTE = "+grupoCliente);
		}else{
		    GregorianCalendar gc = new GregorianCalendar();
		    int dia = gc.get(Calendar.DAY_OF_WEEK);
		    int hora = gc.get(Calendar.HOUR_OF_DAY);
		    String grupos = null;
		    if (hora >= 19 || hora <= 7) {
			grupos = "220";
		    }
		    if (dia > 1 && dia < 7){
			if (hora >= 18 || hora <= 8){
			    if (grupos != null){
				grupos += ",221";				    
			    }else{
				grupos = "221";
			    }
			}
		    }else{
			if (grupos != null){
			    grupos += ",221";				    
			}else{
			    grupos = "221";
			}
		    }
		    if (grupos != null){
			//(c.CD_GRUPO_CLIENTE not in ('221') OR c.CD_GRUPO_CLIENTE IS NULL)
			sqlStr.append(" AND (c.CD_GRUPO_CLIENTE not in ("+grupos+") OR c.CD_GRUPO_CLIENTE IS NULL ) ");
		    }
		}
            	sqlStr.append(" ORDER  BY  ");
            	sqlStr.append(" CASE WHEN H.CD_CODE IN ('ACP', 'ACV') THEN 1 ");
            	sqlStr.append(" WHEN H.CD_CODE IN ('MPI') THEN 2 ");
            	sqlStr.append(" ELSE 3 ");
            	sqlStr.append(" END, h.DT_RECEBIDO ASC ");


		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<EventoVO> eventList = null;

		try
		{

			eventList = new ArrayList<EventoVO>();
			connection = PersistEngine.getConnection("SIGMA90");
			statement = connection.prepareStatement(sqlStr.toString());
			resultSet = statement.executeQuery();
			EventoVO eventoVO = null;

			while (resultSet.next())
			{

				eventoVO = new EventoVO();
				String codigoHistorico = resultSet.getString("CD_HISTORICO");
				String codigoEvento = resultSet.getString("CD_EVENTO");
				Long prioridade = resultSet.getLong("NU_PRIORIDADE");
				Integer codigoCliente = resultSet.getInt("CD_CLIENTE");
				Integer status = resultSet.getInt("FG_STATUS");
				String nomeEvento = resultSet.getString("NM_FRASE_EVENTO");
				Timestamp dataRecebimento = resultSet.getTimestamp("DT_RECEBIDO");
				
				String receptora = resultSet.getString("DS_TIPO_EVENTO_RECEBIDO");
				
				if (receptora.contains("-LT-")){
				    dataRecebimento.setTime(dataRecebimento.getTime()-1000*40);
				}else{
				    dataRecebimento.setTime(dataRecebimento.getTime()-1000*10);
				}
				
				
				String codigoCentral = resultSet.getString("ID_CENTRAL");
				Long grupo = resultSet.getLong("CD_GRUPO_CLIENTE");
				String particao = resultSet.getString("PARTICAO");
				String fantasia = resultSet.getString("FANTASIA");
				String razao = resultSet.getString("RAZAO");
				String endereco = resultSet.getString("ENDERECO");
				String nomeCidade = resultSet.getString("NOMECIDADE");
				String bairro = resultSet.getString("NOMEBAIRRO");
				String observacaoFechamento = resultSet.getString("TX_OBSERVACAO_FECHAMENTO"); // log
				String observavaoGerente = resultSet.getString("TX_OBSERVACAO_GERENTE"); // log
				// gerencia
				String telefone = resultSet.getString("TELEFONE");
				String complemento = resultSet.getString("COMPLEMENTO");
				String nomeEstado = resultSet.getString("NOMEESTADO");
				String nomeRota = resultSet.getString("nm_rota");
				String empresa = resultSet.getString("ID_EMPRESA");
				String cgccpf = resultSet.getString("cgccpf");
				String pergunta = resultSet.getString("PERGUNTA");
				String resposta = resultSet.getString("RESPOSTA");
				String responsavel = resultSet.getString("RESPONSAVEL");
				String email = resultSet.getString("EMAIL");
				String telefone1 = resultSet.getString("FONE1");
				String telefone2 = resultSet.getString("FONE2");
				String cep = resultSet.getString("cep");
				String nmEmpresa = resultSet.getString("NM_RAZAO_SOCIAL");
				Timestamp dataNovoX2 = resultSet.getTimestamp("DATANOVAX2");
				String obsTemp = resultSet.getString("OBSTEMP");
				String obsProvidencia = resultSet.getString("PROVIDENCIA");
				String senhaCoacao = resultSet.getString("NM_SENHA_COACAO");
				String cuc = resultSet.getString("CD_CODE");
				System.out.println("#"+timeExecFinal+"# 4.4.4.1 Ramal: "+ramalTemp+" - Grupo:"+grupoCliente+" Conta:"+codigoCentral+" Particao:"+particao+" Cliente:"+codigoCliente);
				/**
				 * NOVOS PARAMETROS IP D-GUARD
				 */

				eventoVO.setServidorCFTV(resultSet.getString("SERVIDORCFTV") != null ? resultSet.getString("SERVIDORCFTV") : "");
				eventoVO.setPortaCFTV(resultSet.getString("PORTACFTV") != null ? resultSet.getString("PORTACFTV") : "");
				eventoVO.setUsuarioCFTV(resultSet.getString("USERCFTV") != null ? resultSet.getString("USERCFTV") : "");
				eventoVO.setSenhaCFTV(resultSet.getString("SENHACFTV") != null ? resultSet.getString("SENHACFTV") : "");

				/**
				 * FIM DOS NOVOS PARAMETROS
				 */

				eventoVO.setCodigoHistorico((codigoHistorico != null) ? codigoHistorico.toString() : "");
				eventoVO.setCodigoEvento(codigoEvento);
				eventoVO.setPrioridade((prioridade != null) ? prioridade.longValue() : null);
				eventoVO.setCodigoCliente((codigoCliente != null) ? codigoCliente.toString() : "");
				eventoVO.setDataNovoX2(NeoDateUtils.safeDateFormat(dataNovoX2, "dd/MM/yyyy HH:mm:ss"));
				eventoVO.setStatus(status.intValue());
				eventoVO.setNomeEvento(nomeEvento);
				eventoVO.setSenhaCoacao(senhaCoacao);
				eventoVO.setEmpresa(empresa);
				eventoVO.setNomeEmpresa(nmEmpresa);
				String dataRecebimentoStr = NeoDateUtils.safeDateFormat(dataRecebimento, "dd/MM/yyyy HH:mm:ss");
				eventoVO.setDataRecebimento(dataRecebimentoStr);

				if (NeoUtils.safeIsNull(complemento) || complemento.isEmpty())
					complemento = " ";
				eventoVO.setCodigoCentral(codigoCentral);
				eventoVO.setGrupo(grupo);
				eventoVO.setParticao(particao);
				eventoVO.setFantasia(fantasia);
				eventoVO.setRazao(razao);
				eventoVO.setEndereco(endereco);
				eventoVO.setNomeCidade(nomeCidade);
				eventoVO.setBairro(bairro);
				if (NeoUtils.safeIsNotNull(telefone))
				{
					telefone = telefone.replace("(", "").replace(")", "").replace("-", "").trim();
					telefone = telefone.replaceAll(" ", "");
					eventoVO.setTelefone(telefone);
				}
				else
				{
					eventoVO.setTelefone("");
				}
				eventoVO.setRota(nomeRota);
				eventoVO.setUf(nomeEstado);
				eventoVO.setPergunta(pergunta);
				eventoVO.setResposta(resposta);
				eventoVO.setResponsavel(responsavel);
				eventoVO.setEmailResponsavel(email);
				eventoVO.setObsTemp(obsTemp);
				eventoVO.setObsProvidencia(obsProvidencia);
				if (NeoUtils.safeIsNotNull(telefone1))
				{
					telefone1 = telefone1.replace("(", "").replace(")", "").replace("-", "").trim();
					telefone1 = telefone1.replaceAll(" ", "");
					eventoVO.setTelefone1("<span style=\"cursor: pointer;\" onclick='javascript:dial(\"0" + telefone1 + "\");'>" + telefone1 + "</span>");
				}
				else
				{
					eventoVO.setTelefone1("");
				}

				if (NeoUtils.safeIsNotNull(telefone2))
				{
					telefone2 = telefone2.replace("(", "").replace(")", "").replace("-", "").trim();
					telefone2 = telefone2.replaceAll(" ", "");
					eventoVO.setTelefone2("<span style=\"cursor: pointer;\" onclick='javascript:dial(\"0" + telefone2 + "\");'>" + telefone2 + "</span>");
				}
				else
				{
					eventoVO.setTelefone2("");
				}

				eventoVO.setCgccpf(cgccpf);
				eventoVO.setCep(cep);
				eventList.add(eventoVO);

				String deslocamentoViatura = " ";

				if (observacaoFechamento == null || observacaoFechamento.isEmpty())
				{
					observacaoFechamento = "Vazio";
				}
				else
				{
					// observacaoFechamento = "<li>" + observacaoFechamento;
					// observacaoFechamento =
					// observacaoFechamento.replaceAll("###", "<br><li>");
				}
				if (observavaoGerente == null || observavaoGerente.isEmpty())
				{
					observavaoGerente = "Vazio";
				}
				else
				{
					// observavaoGerente = "<li>" + observavaoGerente;
					// observavaoGerente = observavaoGerente.replace("\n",
					// "<br><li>");
				}

				if (NeoUtils.safeIsNull(deslocamentoViatura))
					deslocamentoViatura = " ";

				eventoVO.setObservacaoFechamento(observacaoFechamento);
				eventoVO.setObservavaoGerente(observavaoGerente);
				eventoVO.setCdCode(cuc);

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro Eventos retorno: " + e.getMessage());

		}finally{
		    OrsegupsUtils.closeConnection(connection, statement, resultSet);		    
		}
		EventoVO resultVo = null;
		if (eventList != null && !eventList.isEmpty())
		{
		    for (EventoVO vo : eventList)
		    {
			resultVo = new EventoVO();
			List<String> listaConta = null;
			
			System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" VERIFICANDO SE O EVENTO: "+vo.getCodigoHistorico()+" EXISTE EM MEMORIA");
			listaConta = getHistoricoFila(vo.getCodigoHistorico());
			if (vo != null && OrsegupsAlertEventVideoEngine.callingAlertEventVO != null && !OrsegupsAlertEventVideoEngine.callingAlertEventVO.containsKey(vo.getCodigoHistorico()) && (listaConta == null || listaConta.isEmpty()))
			{
			    System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" EVENTO: "+vo.getCodigoHistorico()+" NÃO EXISTE EM MEMORIA");
			    if (grupoCliente != null){
				if (vo.getGrupo().equals(grupoCliente)){
				    resultVo = vo;
				    System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" EVENTO: "+vo.getCodigoHistorico()+" EXISTE PERTENCE AO GRUPO: "+grupoCliente);
				    break;
				}
			    }else{
				resultVo = vo;
				System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" EVENTO: "+vo.getCodigoHistorico()+" NÃO TEM RELAÇÃO COM GRUPOS");
				break;
			    }
			}else{
			    System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" EVENTO: "+vo.getCodigoHistorico()+" EXISTE EM MEMORIA (CACHE)");
			}
		    }
		}
		eventList = null;
		if (resultVo != null){
		    System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" RETORNOU O EVENTO: "+resultVo.getCodigoHistorico());   
		}else{
		    System.out.println("##MONITORAMENTO## "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ")+" RETORNOU NULLO PARA A LISTA DE EVENTOS "+listaEventos.toString());
		}
		
		return resultVo;

	}
	
	private Long getGrupoRamal(String ramal){
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();
	    Long retorno = null;
	    try {
		conn = PersistEngine.getConnection("");
		    
		sql.append(" SELECT GC.cd_grupo_cliente AS grupo FROM D_CMFilaRamais r WITH(NOLOCK) ");
		sql.append(" INNER JOIN X_dbGrupoCliente gc WITH(NOLOCK) ON gc.neoId = r.grupo_neoId ");
		sql.append(" WHERE r.ramal = ? ");	    	   
		    
		pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, ramal);

		rs = pstm.executeQuery();

		while (rs.next()) {
		    retorno = rs.getLong("grupo");
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    return retorno;
	}

	private List<String> getHistoricoFila(String historico)
	{
		List<String> ramaisAtivos = null;
		try
		{
			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLOpFilter("conta", "LIKE", "%" + historico + "%"));
			List<NeoObject> ramais = null;
			ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMFilaRamais"), filter);
			PersistEngine.getEntityManager().flush();
			if (ramais != null && !ramais.isEmpty())
			{
				ramaisAtivos = new ArrayList<String>();
				for (NeoObject neoObject : ramais)
				{
					EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
					String hist = (String) ramalUsuario.getValue("conta");
					ramaisAtivos.add(hist);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT getHistoricoFila: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		return ramaisAtivos;
	}

	public String getLog(String cdCliente, String cdHistorico)
	{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String texto = "";
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT H.TX_OBSERVACAO_FECHAMENTO FROM HISTORICO H WITH (NOLOCK)  WHERE H.CD_CLIENTE = ? AND H.CD_HISTORICO = ?");
			// sql.append(" AND  h.FG_STATUS IN (5) AND (( H.CD_EVENTO LIKE 'X406')  OR (H.CD_EVENTO LIKE 'E401' AND H.CD_CODE LIKE 'DLC' ) OR (H.CD_EVENTO LIKE 'XXX2' AND H.CD_CODE LIKE 'EX2') OR (H.CD_EVENTO LIKE 'XXX5' AND H.CD_CODE LIKE 'EX5')) ");
			st = conn.prepareStatement(sql.toString());
			st.setString(1, cdCliente);
			st.setString(2, cdHistorico);
			rs = st.executeQuery();

			while (rs.next())
			{
				if (NeoUtils.safeIsNotNull(rs.getString("TX_OBSERVACAO_FECHAMENTO")))
					texto = rs.getString("TX_OBSERVACAO_FECHAMENTO");
			}

			if (texto == null || texto.isEmpty())
			{
				texto = "Vazio";
			}
			else
			{
				texto = "<li>" + texto;
				texto = texto.replaceAll("##", "<br><li>");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}
		return texto;

	}

	// verifica historico de eventos anteriores
	public EventoVO getDadosEventosHistorico(String cdCliente, String valor)
	{

		EventoVO vo = new EventoVO();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		EventoHistoricoVO eventoVO = null;

		try
		{

			if (cdCliente != null && valor != null)
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT H.DT_RECEBIDO,H.CD_EVENTO,HF.NM_FRASE_EVENTO,H.DT_ESPERA,H.DT_VIATURA_DESLOCAMENTO,H.DT_VIATURA_NO_LOCAL,H.DT_FECHAMENTO, H.NU_AUXILIAR, H.TX_OBSERVACAO_FECHAMENTO, H.NU_PRIORIDADE");
				sql.append(" FROM VIEW_HISTORICO H WITH (NOLOCK)    LEFT JOIN HISTORICO_FRASE_EVENTO HF WITH (NOLOCK)  ON HF.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO");
				sql.append(" WHERE H.CD_CLIENTE = ? AND H.DT_RECEBIDO BETWEEN GETDATE()- ? AND GETDATE() ORDER BY H.DT_RECEBIDO DESC  ");

				statement = connection.prepareStatement(sql.toString());
				statement.setString(1, cdCliente);
				statement.setInt(2, Integer.parseInt(valor));
				resultSet = statement.executeQuery();

				while (resultSet.next())
				{

					eventoVO = new EventoHistoricoVO();
					String dtRecebido = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:ss");
					String cdEvento = resultSet.getString("CD_EVENTO");
					String tipoEvento = resultSet.getString("NM_FRASE_EVENTO");
					String dtEspera = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_ESPERA"), "dd/MM/yyyy HH:mm:ss");
					String dtVtrDeslocamento = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_VIATURA_DESLOCAMENTO"), "dd/MM/yyyy HH:mm:ss");
					String dtVtrLocal = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_VIATURA_NO_LOCAL"), "dd/MM/yyyy HH:mm:ss");
					String dtFechamento = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_FECHAMENTO"), "dd/MM/yyyy HH:mm:ss");
					String nuFechamento = resultSet.getString("NU_AUXILIAR");
					String obsFechamento = resultSet.getString("TX_OBSERVACAO_FECHAMENTO");
					String prioridade = resultSet.getString("NU_PRIORIDADE");

					if (obsFechamento == null || obsFechamento.isEmpty())
					{
						obsFechamento = "Vazio";
					}
					else
					{
						// obsFechamento = "<li>" + obsFechamento;
						// obsFechamento = obsFechamento.replace("\n",
						// "<br><li>");
					}

					if (NeoUtils.safeIsNotNull(dtRecebido))
						eventoVO.setDtRecebido(dtRecebido);
					else
						eventoVO.setDtRecebido("");
					if (NeoUtils.safeIsNotNull(cdEvento))
						eventoVO.setCdEvento(cdEvento);
					else
						eventoVO.setCdEvento("");
					if (NeoUtils.safeIsNotNull(tipoEvento))
						eventoVO.setTipoEvento(tipoEvento);
					else
						eventoVO.setTipoEvento("");
					if (NeoUtils.safeIsNotNull(dtEspera))
						eventoVO.setDtEspera(dtEspera);
					else
						eventoVO.setDtEspera("");
					if (NeoUtils.safeIsNotNull(dtVtrDeslocamento))
						eventoVO.setDtVtrDeslocamento(dtVtrDeslocamento);
					else
						eventoVO.setDtVtrDeslocamento("");
					if (NeoUtils.safeIsNotNull(dtVtrLocal))
						eventoVO.setDtVtrLocal(dtVtrLocal);
					else
						eventoVO.setDtVtrLocal("");
					if (NeoUtils.safeIsNotNull(dtFechamento))
						eventoVO.setDtFechamento(dtFechamento);
					else
						eventoVO.setDtFechamento("");
					if (NeoUtils.safeIsNotNull(nuFechamento))
						eventoVO.setNuFechamento(nuFechamento);
					else
						eventoVO.setNuFechamento("");
					if (NeoUtils.safeIsNotNull(obsFechamento))
						eventoVO.setObsFechamento(obsFechamento);
					else
						eventoVO.setObsFechamento("");
					if (NeoUtils.safeIsNotNull(prioridade))
						eventoVO.setPrioridade(prioridade);
					else
						eventoVO.setPrioridade("");

					vo.addEventoHistorico(eventoVO);

				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsAlertEventVideoEngine SIGMA EVENTO HISTORICO : " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
		}
		return vo;

	}

	@SuppressWarnings("unchecked")
	private List<String> getRamaisDisponiveis()
	{

		List<String> ramaisAtivos = null;
		try
		{
			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("situacao", true));
			filter.addFilter(new QLEqualsFilter("disponivelAtendimento", true));

			List<NeoObject> ramais = null;

			ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMFilaRamais"), filter, -1, -1, "dataLigacao asc");
			PersistEngine.getEntityManager().flush();
			if (ramais != null && !ramais.isEmpty())
			{
				ramaisAtivos = new ArrayList<String>();
				for (NeoObject neoObject : ramais)
				{
					EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
					String ramal = null;
					if ((String) ramalUsuario.findValue("tipo.descricao") != null)
						ramal = (String) ramalUsuario.getValue("ramal") + ";" + (String) ramalUsuario.findValue("tipo.descricao");
					if (ramal != null && !ramal.isEmpty())
						ramaisAtivos.add(ramal);
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR getRamaisDisponiveis: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		return ramaisAtivos;

	}

	public void alert(EventoVO eventoVO, List<String> telCal)
	{
		// realizar a chamada deste metodo quando os alertas ocorrerem.

		// caso o algum operador ja esteja ligando para este evento não realiza
	    
		// nova ligacao
	    
	    	long timeExec = System.currentTimeMillis();
	    	
		try
		{
			if (eventoVO != null && !OrsegupsAlertEventVideoEngine.callingAlertEventVO.containsKey(eventoVO.getCodigoHistorico()) && telCal != null && !telCal.isEmpty())
			{
				List<String> operadoresDisponiveis = telCal;
				String[] ramalOperador = null;
				String telefone = null;
				// Descommentar
				// operadoresDisponiveis = this.getRamaisDisponiveis();
				// verifica se tem algum operador disponivel
				if (operadoresDisponiveis != null && !operadoresDisponiveis.isEmpty())
				{
					for (String valor : operadoresDisponiveis)
					{
						if (valor != null && valor.contains(";"))
						{
//							ramalOperador = valor.split(";");
//							//VERIFICACAO DE CUC OU EVENTO
//							if (ramalOperador != null && eventoVO.getCodigoEvento() != null && ramalOperador[1] != null && ( ramalOperador[1].contains(eventoVO.getCodigoEvento()) || ramalOperador[1].contains(eventoVO.getCdCode()) )  )
//							{
//								telefone = ramalOperador[0];
//								break;
//							}
							
							ramalOperador = valor.split(";");
							//VERIFICACAO DE CUC OU EVENTO
							if (ramalOperador != null && eventoVO.getCodigoEvento() != null && ramalOperador[1] != null && ( ramalOperador[1].contains(eventoVO.getCodigoEvento()) || ramalOperador[1].contains(eventoVO.getCdCode()) )  ){
							    Long grupoOperador = getGrupoRamal(ramalOperador[0]);
							    if (eventoVO.getGrupo() != null && !eventoVO.getGrupo().equals(0L)){
								GregorianCalendar gc = new GregorianCalendar();
								if (eventoVO.getGrupo() == 220L){ // Balaroti
								    int hora = gc.get(Calendar.HOUR_OF_DAY);
								    if (hora < 19 && hora > 7){
									if (grupoOperador == null){
									    telefone = ramalOperador[0];
									    break;
									}
								    }else{
									if (grupoOperador != null && grupoOperador.equals(eventoVO.getGrupo())){
									    telefone = ramalOperador[0];
									    break;
									}
								    }
								}else if (eventoVO.getGrupo() == 221L){ // Globo
								    int dia = gc.get(Calendar.DAY_OF_WEEK);
								    int hora = gc.get(Calendar.HOUR_OF_DAY);
								    if (dia > 1 && dia < 7){
									if (hora < 18 && hora > 8){
									    if (grupoOperador == null){
										telefone = ramalOperador[0];
										break;
									    }
									}else{
									    if (grupoOperador != null && grupoOperador.equals(eventoVO.getGrupo())){
										telefone = ramalOperador[0];
										break;
									    }
									}
								    }else{
									if (grupoOperador != null && grupoOperador.equals(eventoVO.getGrupo())){
									    telefone = ramalOperador[0];
									    break;
									}
								    }
								}else{
								    if (grupoOperador == null){
									telefone = ramalOperador[0];
									break;
								    }
								}
							    }else{
								if (grupoOperador == null){
								    telefone = ramalOperador[0];
								    break;
								}
							    }
							}
						}
						else
						{
							break;
						}

					}

					// operadoresDisponiveis.get(0);
					// log.debug("##### OrsegupsAlertEventVideoEngine operadoresDisponiveis: OP - "+ramalOperador+
					// " EVENTO "+eventoVO.getCodigoEvento()+" "+eventoVO.getParticao()+" - Cliente "+eventoVO.getCodigoCliente()+" Data: "
					// + NeoDateUtils.safeDateFormat(new GregorianCalendar(),
					// "dd/MM/yyyy HH:mm:ss "));
					if (telefone != null && !telefone.isEmpty())
					{
						boolean pausaOk = false;
						
						System.out.println("[verificaEventos]["+timeExec+"] 5.5.1 INICIOU BUSCA PAUSA RAMAL Next: getPausaRamal - Para o evento de historico: "+eventoVO.getCodigoHistorico()+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						
						pausaOk = this.getPausaRamal(telefone.trim());
						
						System.out.println("[verificaEventos]["+timeExec+"] 5.5.2 FINALIZOU BUSCA PAUSA RAMAL Finish: alert - Para o evento de historico: "+eventoVO.getCodigoHistorico()+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						if (pausaOk)
						{
						    System.out.println("[verificaEventos]["+timeExec+"] 5.5.3 INICIOU BUSCA DE TELEFONES CLIENTE Next: telefoneCliente - Para o evento de historico: "+eventoVO.getCodigoHistorico()+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						    
							LinkedHashSet<ProvidenciaVO> numerosChamar = this.telefoneCliente(eventoVO);
							
							System.out.println("[verificaEventos]["+timeExec+"] 5.5.4 FINALIZOU BUSCA DE TELEFONES CLIENTE Finish: telefoneCliente - Para o evento de historico: "+eventoVO.getCodigoHistorico()+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							CallAlertEventVideoVO alertEventVo = new CallAlertEventVideoVO();
							alertEventVo.setCallEventoVO(eventoVO);
							alertEventVo.setNumerosChamar(numerosChamar);
							alertEventVo.setRamalOperador(telefone.trim());
							OrsegupsAlertEventVideoEngine.callingAlertEventVO.put(eventoVO.getCodigoHistorico(), alertEventVo);
							/*
							 * fernando.rebelo 09/03/2017
							 * A key utilizada para inserir o VO dentro do MAP, por alguma razão, não estava sendo encontrada dentro do metodo ligarParaCliente
							 * onde, lá dentr, utiliza o códigoHistorico para buscar o VO dentro do mesmo MAP
							 * 
							 * 
							 * Fiz uma alteração para usar a mesma logica para inserir a key e enviar ao metodo
							 * Apesar da logica anterior estar aparentemente correta, haviam situações que dava problema
							 */
							System.out.println("[verificaEventos]["+timeExec+"] 5.5.5 INICIOU CHAMADA JMS Next: ligarParaCliente - Para o evento de historico: "+eventoVO.getCodigoHistorico()+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							
							this.ligarParaCliente(eventoVO.getCodigoHistorico());
							
							System.out.println("[verificaEventos]["+timeExec+"] 5.5.6 FINALIZOU CHAMADA JMS Finish: ligarParaCliente - Para o evento de historico: "+eventoVO.getCodigoHistorico()+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							String ramalStr = "";
							String contaStr = "";
							if (alertEventVo.getRamalOperador() != null)
								ramalStr = alertEventVo.getRamalOperador();
							if (alertEventVo != null && alertEventVo.getCallEventoVO() != null && alertEventVo.getCallEventoVO().getCodigoCentral() != null && alertEventVo.getCallEventoVO().getParticao() != null && alertEventVo.getCallEventoVO().getCodigoEvento() != null && alertEventVo.getCallEventoVO().getCodigoCliente() != null)
								contaStr = "Conta : " + alertEventVo.getCallEventoVO().getCodigoCentral() + "[" + alertEventVo.getCallEventoVO().getParticao() + "] Evento : " + alertEventVo.getCallEventoVO().getCodigoEvento() + " Historico : " + alertEventVo.getCallEventoVO().getCodigoHistorico() + " Cliente : " + alertEventVo.getCallEventoVO().getCodigoCliente();
							
							
							System.out.println("[verificaEventos]["+timeExec+"] 5.5.7 INICIOU ALTERAR DATA ULTIMA LIGACAO Next: alteraDataUltimaLigacaoRamal - Para o evento de historico: "+eventoVO.getCodigoHistorico()+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							this.alteraDataUltimaLigacaoRamal(ramalStr, contaStr);
							
							System.out.println("[verificaEventos]["+timeExec+"] 5.5.8 FINALIZOU ALTERAR DATA ULTIMA LIGACAO Finish: alteraDataUltimaLigacaoRamal - Para o evento de historico: "+eventoVO.getCodigoHistorico()+" - Tempo: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO AO RETORNAR ALERT EVENT alert " + e);
		}
	}

	private boolean getPausaRamal(String ramal)
	{
		Boolean ramaisAtivos = Boolean.TRUE;
				
		Connection conn = null;
		PreparedStatement pstm = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE D_CMFilaRamais SET situacao=0 ");
		sql.append(" WHERE situacao=1 AND ramal=? ");

		try {
		    conn = PersistEngine.getConnection("");
		    pstm = conn.prepareStatement(sql.toString());

		    pstm.setString(1, ramal);

		    int rs = pstm.executeUpdate();

		    if (rs > 0){
			ramaisAtivos = Boolean.TRUE;
		    }

		} catch (SQLException e) {
		    e.printStackTrace();
		    log.error("##### ERRO SALVAR LOG EVENT ALERT getPausaRamal: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, null);
		}
		
//		List<NeoObject> ramais = null;
//		try
//		{
//			QLGroupFilter filter = new QLGroupFilter("AND");
//			filter.addFilter(new QLEqualsFilter("situacao", true));
//			filter.addFilter(new QLEqualsFilter("ramal", ramal));
//
//			ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMFilaRamais"), filter);
//			if (ramais != null && !ramais.isEmpty())
//			{
//				NeoObject neoObject = (NeoObject) ramais.get(0);
//
//				EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
//				ramalUsuario.setValue("situacao", false);
//				ramalUsuario.setValue("ramal", ramal);
//				PersistEngine.persist(neoObject);
//				PersistEngine.commit(true);
//				ramaisAtivos = Boolean.TRUE;
//				PersistEngine.getEntityManager().flush();
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			log.error("##### ERRO SALVAR LOG EVENT ALERT getPausaRamal: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
//
//		}
//		ramais = null;
		return ramaisAtivos;
	}

	public TreeMap<String, String> getCameraCliente(String idCentral, String empresa, EventoVO eventoVO)
	{

		TreeMap<String, String> treeMap = null;

		String resposta = "";
		try
		{
			String userpass = eventoVO.getUsuarioCFTV() + ":" + eventoVO.getSenhaCFTV();

			String prefixo = "http://";

			URL oracle = new URL(prefixo + eventoVO.getServidorCFTV() + "/camerasnomes.cgi?receiver=" + empresa + "&server=" + idCentral);
			
			URLConnection yc = oracle.openConnection();
			yc.setConnectTimeout(10000);
			String basicAuth = "Basic " + new String(Base64.encode(userpass.getBytes()));
			yc.setRequestProperty("Authorization", basicAuth);
			BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null)
			{
				resposta = inputLine;
			}
			in.close();
			if (resposta != null && !resposta.isEmpty() && resposta.contains("="))
			{
				String array[] = resposta.split("&");
				treeMap = new TreeMap<String, String>();
				for (String valor : array)
				{
					if (valor != null && !valor.isEmpty() && valor.contains("="))
					{
						String parametros[] = valor.split("=");
						treeMap.put(parametros[0], (String) parametros[1]);
					}
					

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO BUSCAR CAMERAS: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}

		return treeMap;
	}

	// private String neoUser(String ramalAux)
	// {
	// List<NeoUser> users = new ArrayList<NeoUser>();
	// String resposta = "";
	// List<String> dados = new ArrayList<String>();
	// try
	// {
	// List<NeoObject> colaborador =
	// PersistEngine.getObjects(AdapterUtils.getEntityClass("colaboradores"),
	// new QLOpFilter("ramal", "LIKE", ramalAux));
	//
	// if (colaborador != null && !colaborador.isEmpty())
	// {
	//
	// for (NeoObject neoObject : colaborador)
	// {
	//
	// EntityWrapper ewColaborador = new EntityWrapper(neoObject);
	// String nome = "";
	// String ramal = "";
	//
	// nome = (String) ewColaborador.findField("fullName").getValue();
	// ramal = (String) ewColaborador.findField("ramal").getValue();
	// dados.add(nome);
	// }
	//
	// if (dados != null && !dados.isEmpty())
	// {
	// DecimalFormat f = new DecimalFormat("000000000");
	// ArrayList<HttpSession> hashMap = new ArrayList<HttpSession>();
	// SortedMap<String, HttpSession> sortedUsers = new TreeMap<String,
	// HttpSession>();
	//
	// if (hashMap != null && !hashMap.isEmpty())
	// {
	// for (HttpSession sessao : hashMap)
	// {
	//
	// final String user = (String) sessao.getAttribute("user");
	// long l = 999999999l - sessao.getLastAccessedTime();
	// sortedUsers.put(f.format(l) + user.toLowerCase(), sessao);
	//
	// }
	//
	// if (sortedUsers != null && !sortedUsers.isEmpty())
	// {
	// for (Map.Entry<String, HttpSession> entry : sortedUsers.entrySet())
	// {
	// HttpSession sessao = entry.getValue();
	// //final NeoUser user = (NeoUser) sessao.getAttribute("user");
	// final String user = (String) sessao.getAttribute("user");
	//
	// NeoUser nuser = (NeoUser) PersistEngine.getObject(NeoUser.class, new
	// QLEqualsFilter("code", user));
	// users.add(nuser);
	//
	// }
	//
	// if (users != null && !users.isEmpty())
	// {
	// for (NeoUser user : users)
	// {
	// if (dados != null && !dados.isEmpty() &&
	// dados.contains(user.getFullName()))
	// {
	// resposta = user.getFullName();
	// break;
	// }
	// }
	// }
	// }
	// }
	// }
	// }
	// }
	// catch (Exception e)
	// {
	// e.printStackTrace();
	// }
	// finally
	// {
	// return resposta;
	// }
	// }

}
