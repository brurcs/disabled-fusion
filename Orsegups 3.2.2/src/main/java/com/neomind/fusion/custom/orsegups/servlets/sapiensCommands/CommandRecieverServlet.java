package com.neomind.fusion.custom.orsegups.servlets.sapiensCommands;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "/MainCommandServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.servlets.sapiensCommands.MainCommandServlet" })
public class CommandRecieverServlet extends HttpServlet {
    
    private static final long serialVersionUID = 1L;

    public CommandRecieverServlet() {
	super();
	// TODO Auto-generated constructor stub
    }
    
    Map<String, Command> comandos = CommandsInitializer.retornaComandos();
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String acao = request.getParameter("acao");
	String codigoTarefa = null;
	Command comando = null;
	
	for(String key: comandos.keySet()){
	    if(key.equalsIgnoreCase(acao)){
		comando = comandos.get(key);
	    }	    
	}
	
	codigoTarefa = comando.execute(request);
	
	response.setContentType("text/plain");
	response.setCharacterEncoding("ISO-8859-1");
	final PrintWriter out = response.getWriter();
	out.print(codigoTarefa);	
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO Auto-generated method stub
    }

}
