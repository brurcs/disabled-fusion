package com.neomind.fusion.custom.orsegups.servlets;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.util.Log;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.AtualizaCentroCusto;
import com.neomind.fusion.custom.orsegups.contract.ContratoLogUtils;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.contract.MinutaContrato;
import com.neomind.fusion.custom.orsegups.contract.vo.FgcPrincipalVO;
import com.neomind.fusion.custom.orsegups.serasa.MyTrustManager;
import com.neomind.fusion.custom.orsegups.serasa.RegistroSerasa;
import com.neomind.fusion.custom.orsegups.serasa.RegistroSerasaFactory;
import com.neomind.fusion.custom.orsegups.serasa.SerasaUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.EFormManager;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.GeraDadosIniciais;
import com.neomind.util.NeoUtils;

@WebServlet(name="ContratoServletUtils", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils"})
public class ContratoServletUtils extends HttpServlet {

	private static final long serialVersionUID = -2837035295207229914L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String action = request.getParameter("action");
		
		if (!NeoUtils.safeIsNull(action))
		{
			if (action.equalsIgnoreCase("returnDataTree"))
			{
				returnTreeRendered(request, response);
			}else if (action.equalsIgnoreCase("doPopulaListaEquipamentos"))
			{
				this.doPopulaListaEquipamentos(request, response);
			}else if (action.equalsIgnoreCase("doLimpaListaEquipamentos"))
			{
				this.doLimpaListaEquipamentos(request, response);
			}else if(action.equalsIgnoreCase("doFindEndereco"))
			{
				this.doFindEndereco(request, response);
			}else if(action.equalsIgnoreCase("doCalculaISS"))
			{
				this.doCalculaISS(request, response);
			}else if(action.equalsIgnoreCase("doListarPostos"))
			{
				this.doListarPostos(request, response);
			}else if(action.equalsIgnoreCase("doGerarMinuta"))
			{
				this.doGerarMinuta(request, response);
			}else if(action.equalsIgnoreCase("doConsultaSerasa")){
				this.doConsultaSerasa(request, response);
			}else if(action.equalsIgnoreCase("atualizaCentroDeCusto")){
				String codTar = request.getParameter("codTar");
				String codCcuNv6 = request.getParameter("codCcu");
				this.atualizaCentroDeCusto(codTar,codCcuNv6,request, response);
			}else if(action.equalsIgnoreCase("listar")){
				String codTar = request.getParameter("codTar");
				this.visualizarEformPrincipal(codTar, request, response);
			}
		}
	}
	
	public void atualizaCentroDeCusto(String codTar, String codCcuNv6, HttpServletRequest request, HttpServletResponse response){
		try
		{
			PrintWriter out = response.getWriter();
			AtualizaCentroCusto atualizar = new AtualizaCentroCusto();
			String msgErro = "";
			if(codTar.isEmpty()){
				msgErro = "*O campo CODIGO DA TAREFA C001 e obrigatorio<br>";
			}
			
			if(codCcuNv6.isEmpty()){
				msgErro = msgErro+"*O campo CENTRO DE CUSTO DE NIVEL 6 e obrigatorio<br>";
			}
			String retorno = "";
			if(msgErro.equals("")){
				retorno = atualizar.atualizaCentroDeCusto(codCcuNv6, codTar);
			}else{
				JSONObject jsonCompetencia = new JSONObject();
				jsonCompetencia.put("msgRetorno", msgErro);
				out.print(jsonCompetencia);
				out.flush();
				out.close();
				return;
			}
			
			JSONObject jsonCompetencia = new JSONObject();
			jsonCompetencia.put("msgRetorno", retorno);
			
			out.print(jsonCompetencia);
			out.flush();
			out.close();
			}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void visualizarEformPrincipal(String codTar, HttpServletRequest request, HttpServletResponse response){
		try{
			PrintWriter out = response.getWriter();
			AtualizaCentroCusto atualizar = new AtualizaCentroCusto();
			FgcPrincipalVO fgcPrincipalVO = atualizar.visualizarEformPrincipal(codTar);
			
			if(fgcPrincipalVO != null){
				Gson gson = new Gson();
				String retornoJson = gson.toJson(fgcPrincipalVO);
				out.print(retornoJson);
				System.out.println(retornoJson);
				out.flush();
				out.close();
			}else{
				JSONObject jsonCompetencia = new JSONObject();
				jsonCompetencia.put("msgRetorno", "Nenhum registro encontrado!");
				out.print(jsonCompetencia);
				out.flush();
				out.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void doGerarMinuta(HttpServletRequest request, HttpServletResponse response)
	{
		String idProcessoContrato = NeoUtils.safeOutputString(request.getParameter("idContrato"));
		System.out.println("idProcesso: " + idProcessoContrato);
		
		boolean isCroqui = NeoUtils.safeOutputString(request.getParameter("isCroqui")).equals("true") ? true : false;
		try
		{
			OutputStream out = response.getOutputStream();
			if (idProcessoContrato != null && !idProcessoContrato.isEmpty()) 
			{
				File fileProposta = null;
				if(!isCroqui)
				{
					fileProposta = MinutaContrato.geraPDF(idProcessoContrato);
				}else
				{
					fileProposta = MinutaContrato.geraPDFCroqui();
				}
				
				if(fileProposta != null && !isCroqui)
				{
					NeoFile minuta = NeoStorage.getDefault().copy(fileProposta);
					
					NeoObject contrato = PersistEngine.getNeoObject(NeoUtils.safeLong(idProcessoContrato));
					EntityWrapper wContrato = new EntityWrapper(contrato);
					
					NeoObject historicoMinuta = AdapterUtils.createNewEntityInstance("FGCHistoricoMinuta");
					EntityWrapper wHistoricoMinuta = new EntityWrapper(historicoMinuta);
					wHistoricoMinuta.setValue("minuta", minuta);
					PersistEngine.persist(historicoMinuta);
					
					wContrato.findField("historicoMinuta").addValue(historicoMinuta);
				}
				try
				{
					String sContentType = null;
					sContentType = "application/pdf";
					response.setContentType(sContentType);
					response.addHeader("content-disposition", "attachment; filename=" + fileProposta.getName());
					//response.setCharacterEncoding("ISO-8859-1" );
					InputStream in = null;
					in = new BufferedInputStream(new FileInputStream(fileProposta));
					if (in != null)
					{
						response.setContentLength((int) in.available());
						int l;
						byte b[] = new byte[1024];
						while ((l = in.read(b, 0, b.length)) != -1)
						{
							out.write(b, 0, l);
						}
						out.flush();
						in.close();
					}
					else
					{
						System.out.println("[FLUXO CONTRATOS]-Download de Arquivo invalido");
					}
				}
				catch (Exception e)
				{
					System.out.println("[FLUXO CONTRATOS]- Erro Baixando o arquivo");
				}
				finally
				{
					out.close();
				}
			}
		}
		catch (Exception e)
		{
			ContratoLogUtils.logInfo("Erro ao imprimir doGeraMinuta "+ (isCroqui? "Croqui":"Minuta") + " - msg: " + e.getMessage());
		}
	}
	
	public static void doListarPostos(HttpServletRequest request, HttpServletResponse response)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();
		}catch(Exception e)
		{
			
		}
		
		long numCtr = NeoUtils.safeLong(NeoUtils.safeOutputString(request.getParameter("numctr")));
		long codemp = NeoUtils.safeLong(NeoUtils.safeOutputString(request.getParameter("codemp")));;
		NeoObject empresa = PersistEngine.getNeoObject(codemp);
		EntityWrapper wEmpresa = new EntityWrapper(empresa);
		
		long codfil = NeoUtils.safeLong(NeoUtils.safeOutputString(wEmpresa.findValue("codfil")));
		codemp = NeoUtils.safeLong(NeoUtils.safeOutputString(wEmpresa.findValue("codemp")));
		
		Collection<NeoObject> listaPostos = GeraDadosIniciais.retornaListaPostos(codemp, codfil, numCtr);
		String retorno = "";
		for(NeoObject posto : listaPostos)
		{
			EntityWrapper w = new EntityWrapper(posto);
			
			NeoObject postoResumo = AdapterUtils.createNewEntityInstance("FGCListaResumoPostos");
			EntityWrapper wResumo = new EntityWrapper(postoResumo);
			wResumo.setValue("numero", NeoUtils.safeOutputString(w.findValue("usu_numpos")));
			
			String cep = null;
			if (w.findValue("usu_cepctr") != null){
				cep = (String) w.findField("usu_cepctr").getValueAsString();
				cep = cep.replaceAll("-", "");
			}
			
			Long usu_cepctr = Long.parseLong(cep);
			String usu_endctr = (String) w.findField("usu_endctr").getValue();
			String usu_cplctr = (String) w.findField("usu_cplctr").getValue(); //complemento
			String usu_baictr = (String) w.findField("usu_baictr").getValue();
			String usu_ufsctr = (String) w.findField("usu_ufsctr").getValue();
			String usu_cidctr = (String) w.findField("usu_cidctr").getValue();
			
			String endereco = usu_cepctr + " - " +  usu_endctr + " - " + usu_cplctr + " - " + usu_baictr + " - " + usu_ufsctr + " - " + usu_cidctr;
			wResumo.setValue("endereco", endereco);
			
			String codServicoExt = NeoUtils.safeOutputString(w.findValue("usu_codser"));
			NeoObject complementoServico = retornaComplementoServicoCodSer(codemp, codServicoExt);
			wResumo.setValue("tipoServico", complementoServico);
			wResumo.setValue("valor", w.findValue("usu_preuni"));
			PersistEngine.persist(postoResumo);
			
			if(retorno.equals(""))
				retorno = NeoUtils.safeOutputString(postoResumo.getNeoId());
			else
				retorno += ";" + NeoUtils.safeOutputString(postoResumo.getNeoId());
		}
		
		out.print(retorno);
	}
	
	private static NeoObject retornaComplementoServicoCodSer(Long codEmpresa, String codServicoExt)
	{
		NeoObject centrocusto = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSESER"),
				new QLGroupFilter("AND",
						new QLEqualsFilter("codemp", codEmpresa),
						new QLEqualsFilter("codser", codServicoExt))) );

		return centrocusto;
	}
	
	public static void doCalculaISS(HttpServletRequest request, HttpServletResponse response)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		String form = request.getParameter("form");
		String cep =  request.getParameter("cep");
		
		ContratoLogUtils.logInfo("["+key+"] calculo iss: form:" + form + " cep:" + cep);
		
		if (cep!= null){
			cep = cep.replace("-", "");
		}
		String codSer = request.getParameter("codser");
		if (codSer != null){
			int i = codSer.indexOf(":");
			if (i>-1){
				codSer = codSer.substring(i+1);
			}else{
				codSer= null;
			}
		}
		ContratoLogUtils.logInfo("["+key+"] codser:" + codSer);
		
		String msgRetorno = "";
		EForm cadastroPosto = EFormManager.getInstance().get(form);
		NeoObject posto = cadastroPosto.getObject();
		EntityWrapper wPosto = new EntityWrapper(posto);
		
		NeoObject contrato = cadastroPosto.getCaller().getObject();
		EntityWrapper wContrato = new EntityWrapper(contrato);
		
		try
		{
			final PrintWriter out = response.getWriter();
			
			if(codSer == null){
				String alert = NeoUtils.safeOutputString("Favor fornecer o codigo do servico.");
				out.print("0; " + alert);
				return;
			}

			String vCodEmp = NeoUtils.safeOutputString(wContrato.findValue("empresa.codemp"));
			//String vCodSer = NeoUtils.safeOutputString(wPosto.findValue("complementoServico.codser")); 
			if(codSer.equals("")){
				//vCodSer = "9001311MTA";
				String alert = NeoUtils.safeOutputString("Codigo do servico nao encontrado.");
				out.print("0; " + alert);
				return;
			}
			String vCepSer = NeoUtils.safeOutputString(cep);
			
			if(vCepSer.equals(""))
			{
				String alert = NeoUtils.safeOutputString("Favor fornecer o cep no campo Endereco do Posto no Contrato.");
				out.print("0; " + alert);
				return;
			}
			
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT "); 
			sql.append(" cep.CEPINI ");
			sql.append(" FROM E008CEP cep ");
			sql.append(" WHERE "+vCepSer+" between cep.CEPINI " );
			sql.append(" AND cep.CEPFIM " );
			sql.append(" ORDER BY CEPINI " );
			
			ContratoLogUtils.logInfo("["+key+"] sql cep:" + sql.toString());
			Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
			query.setMaxResults(1);
			Collection<Object> resultList = query.getResultList();
			if (resultList != null && !resultList.isEmpty())
			{
				for (Object result : resultList)
				{
					if (result != null)
					{
						//SELECT PERISS FROM E080SXC WHERE CODEMP = :vCodEmp AND CODSER = :vCodSer AND CEPINI = :vCepIni
						String vCepIni = NeoUtils.safeOutputString(result);
						
						sql = new StringBuilder();
						sql.append("select "); 
						sql.append("sxc.PERISS ");
						sql.append("from E080SXC sxc ");
						sql.append("WHERE ");
						sql.append("sxc.codemp = " + vCodEmp);
						//sql.append(" and sxc.codser = '" + vCodSer +"'");
						sql.append(" and sxc.codser = '"+codSer+"'");
						
						sql.append(" and sxc.cepini = " + vCepIni);
						
						ContratoLogUtils.logInfo("sql periss-> " + sql.toString());
						
						query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
						resultList = query.getResultList();
						if (resultList != null && !resultList.isEmpty())
						{
							for (Object result2 : resultList)
							{
								if (result2 != null)
								{
									out.print("1;" + result2);
									return;
								}
							}
						}else
						{
							msgRetorno = "0;" + "Nao encontrado percentual de ISS para o CEP fornecido no campo Endereco do Posto no Contrato. Favor cadastrar, se necessario.";
						}
					}
				}
			
			}else
			{
				msgRetorno = "0;" + "Nao encontrado referencia de ISS para o CEP indicado no campo Endereco do Posto no Contrato. Favor cadastrar, se necessario.";
			}
			out.print(msgRetorno);
		}catch(Exception e)
		{
			ContratoLogUtils.logInfo("Erro ao calcular ISS: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void returnTreeRendered(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			response.setContentType("text/plain");
			response.setCharacterEncoding("ISO-8859-1");
			Long neoId = Long.valueOf(request.getParameter("neoid"));
			
			NeoObject noCentroCusto = PersistEngine.getNeoObject(neoId);
			String children = "";
			
			Boolean end = false;
			
			EntityWrapper ewCentroCusto = new EntityWrapper(noCentroCusto);
			
			// Faz uma repetição, armazenando o nó selecionado e seus irmãos, e buscando o pai para o próximo, até não existir mais pai.
			while(!end)
			{
				String codeEmpresa = (String) ewCentroCusto.findField("codemp").getValue().toString();
				String codeFather  = (String) ewCentroCusto.findField("ccupai").getValue();
				String codeNode    = (String) ewCentroCusto.findField("codccu").getValue();
				String cliente 	   = (String) ewCentroCusto.findField("desccu").getValue();
				
				
				QLGroupFilter filter = new QLGroupFilter("AND");
				
				// Filtro por empresa
				QLEqualsFilter filterCodemp = new QLEqualsFilter("codemp", Long.valueOf(codeEmpresa));
				filter.addFilter(filterCodemp);
				
				// Verifica se ainda existe um nó pai
				QLEqualsFilter filterCod;
				if(codeFather.equals(" "))
				{
					filterCod = new QLEqualsFilter("codccu", codeNode);
					end = true;
				}
				else
				{
					filterCod = new QLEqualsFilter("codccu", codeFather);
				}
				
				filter.addFilter(filterCod);
				
				List<NeoObject> noListaCentroCustos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filter);
				

				for(NeoObject noCentroCustoFather : noListaCentroCustos)
				{
					ewCentroCusto = new EntityWrapper(noCentroCustoFather);
					// Retorna os nós filhos 					
					String codeFatherNode = (String) ewCentroCusto.findField("codccu").getValue();
					String codeEmpresaNode = (String) ewCentroCusto.findField("codemp").getValue().toString();
					String nivel = (String) ewCentroCusto.findField("nivccu").getValue().toString();
					children = returnChildrenNode(codeFatherNode, codeEmpresaNode, end.toString(), cliente, nivel, codeNode, children);
					
				}
			}
			response.getWriter().println(children);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static String returnChildrenNode(String codeFatherNode, String codeEmpresa, String firstNode, String cliente, String nivelCC, String codeNode, String children)
	{
		// Filtros
		QLEqualsFilter filterCod;
		QLGroupFilter filter = new QLGroupFilter("AND");
		
		QLEqualsFilter filterCodemp = new QLEqualsFilter("codemp", Long.valueOf(codeEmpresa));
		filter.addFilter(filterCodemp);
		
		// Busca todos os nós 'Pai'
		if(firstNode.equals("true"))
		{
			filterCod = new QLEqualsFilter("codccu", "10");
		}
		else // Carrega os nós filhos
		{
			filterCod = new QLEqualsFilter("ccupai", codeFatherNode);
			if(Long.valueOf(nivelCC) <= 4)
				filter.addFilter(new QLEqualsFilter("codccu", codeNode));
		}
		
		filter.addFilter(filterCod);
		
		// Se for selecionar os clientes, exige a descrição de um cliente para filtros
		/*if(nivelCC.equals("3"))
		{
			QLRawFilter filterCliente = new QLRawFilter("desccu LIKE '%" + cliente  +"%'");
			filter.addFilter(filterCliente);
		}*/
		
		List<NeoObject> noListaCentroCustos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filter, -1, -1, "codccu");
		
		String dataTree = "";
		for(NeoObject noCentroCusto : noListaCentroCustos)
		{
			EntityWrapper ewCentroCusto = new EntityWrapper(noCentroCusto);
			System.out.println((String) ewCentroCusto.findField("codccu").getValue());
			//Busca os valores para a montagem da tree
			String code = (String) ewCentroCusto.findField("codccu").getValue();
			String nivel = (String) ewCentroCusto.findField("nivccu").getValue().toString();
			
			// Não exibe 'ADMINISTRAÇÃO'
			if(code.equals("20"))
				continue;
			// Não exibe os nós irmãos dos produtos e serviços
			else if(nivel.equals("2") && !code.equals(codeNode))
				continue;
			// Não exibe os nós irmãos da empresa
			else if(nivel.equals("3") && !code.equals(codeNode))
				continue;
			// Não exibe os nós irmãos do cliente
			else if(nivel.equals("4") && !code.equals(codeNode))
				continue;
			
			// Captura os valores para alimentar os nós.
			String codeFather = (String) ewCentroCusto.findField("ccupai").getValue();
			String codeEmp = (String) ewCentroCusto.findField("codemp").getValue().toString();
			String title = (String) ewCentroCusto.findField("desccu").getValue();
			Long neoId = noCentroCusto.getNeoId();
			
			if(!children.equals("") && code == codeNode)
			{
				dataTree += "{\"data\": \""+code+" - "+title+"\", \"attr\": {\"id\": \""+neoId+"\",\"title\": \""+title+"\",\"ccupai\": \""+codeFather+"\",\"codccu\": \""+code+"\",\"codemp\": \""+codeEmp+"\",\"neoid\": \""+neoId+"\",\"nivccu\": \""+nivel+"\"}, \"state\": \"open\", \"children\": "+children+"},";
			}
			else
			{
				dataTree += "{\"data\": \""+code+" - "+title+"\", \"attr\": {\"id\": \""+neoId+"\",\"title\": \""+title+"\",\"ccupai\": \""+codeFather+"\",\"codccu\": \""+code+"\",\"codemp\": \""+codeEmp+"\",\"neoid\": \""+neoId+"\",\"nivccu\": \""+nivel+"\"}, \"state\": \"closed\"},";
			}
		}
		
		// Retirna a vírgula que insere no final do data.
		return dataTree = "[" + (dataTree.equals("")?"":dataTree.substring(0, dataTree.length() - 1)) + "]";
	}
	
	private void doFindEndereco(HttpServletRequest request, HttpServletResponse response)
	{
		boolean encontrouViaWs= false;
		response.setContentType("text/html; charset=UTF-8");
		String cepBuscar = request.getParameter("cep");
		String retErr = null;
		JSONObject jo = null;

		jo = ContratoUtils.consomePaginaJson("http://correiosapi.apphb.com/cep/"+cepBuscar);
		
		String eform = request.getParameter("eform");
		String cidade = null;
		String uf = null;
		String cep = null;

			try {
				
				if (jo != null && jo.get("cep")!= null && jo.get("cep").equals(cepBuscar)){
					
					try {
						retErr = new String(jo.getString("cep").getBytes(),"UTF-8");
						retErr += ";"+ new String(jo.getString("tipoDeLogradouro").getBytes(),"UTF-8") + " " + new String(jo.getString("logradouro").getBytes(),"UTF-8");
						retErr += ";"+new String(jo.getString("bairro").getBytes(),"UTF-8");
						retErr += ";"+new String(jo.getString("cidade").getBytes(),"UTF-8");
						retErr += ";"+new String(jo.getString("estado").getBytes(),"UTF-8");
						
						/*System.out.println(new String(jo.getString("cep").getBytes(),"UTF-8") );
						System.out.println(new String(jo.getString("tipoDeLogradouro").getBytes(),"UTF-8"));
						System.out.println(new String(jo.getString("logradouro").getBytes(),"UTF-8"));
						System.out.println(new String(jo.getString("bairro").getBytes(),"UTF-8"));
						System.out.println(new String(jo.getString("cidade").getBytes(),"UTF-8"));
						System.out.println(new String(jo.getString("estado").getBytes(),"UTF-8"));*/
						
						cidade = new String(jo.getString("cidade").getBytes(),"UTF-8");
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//System.out.println(jo.getString("estado"));
					
					uf = jo.getString("estado");
					cep = jo.getString("cep");
					encontrouViaWs = true;
					
				}
			} catch (JSONException e1) {
				encontrouViaWs = false;
				//e1.printStackTrace();
			}
			
			if (!encontrouViaWs){ // pegar UF e Cidade da faixa de CEP do sapiens
				cep = cepBuscar;
				QLGroupFilter gpCep = new QLGroupFilter("AND");
				gpCep.addFilter(new QLRawFilter(cep+" between cepIni and cepFim"));
				Collection<NeoObject> objCidade = (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE008CEP"), gpCep);
				if(objCidade.size() > 0){
					for(NeoObject obj : objCidade)
					{
						EntityWrapper w = new EntityWrapper(obj);
						//uf
						uf = (String) w.findValue("sigufs");
						//cidade
						cidade = (String) w.findValue("nomcid");
					}
				}else{
					PrintWriter out;
					try {
						out = response.getWriter();
						out.write("ORSEGERR-NE;Cep não encontrado.;");
						return ;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				
			}
			
		
		String cepIni = cep.substring(0, 4);
		String cepFim = cep.substring(4, cep.length());
		try
		{
			final PrintWriter out = response.getWriter();

			
			// busca Estado para setar no eform
			NeoObject objEstado = PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSE007UFS"), new QLEqualsFilter("sigufs", uf));
			EntityWrapper wEstado = new EntityWrapper(objEstado);
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLRawFilter("sigufs = '"+ uf + "' and "+cep+" between cepIni and cepFim"));
			//gp.addFilter(new QLEqualsFilter("sigufs", uf));
			//gp.addFilter(new QLEqualsFilter("nomcid", cidade));
			Collection<NeoObject> objCidade = (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE008CEP"), gp);
			if(objCidade.size() <= 0) // caso não tenha encontrato, tenta normalizando o nome da cidade.
			{
				gp.clearFilterList();
				String str = cidade;
				str = Normalizer.normalize(str, Normalizer.Form.NFD);
				str = str.replaceAll("[^\\p{ASCII}]", "");
				
				gp.addFilter(new QLEqualsFilter("sigufs", uf));
				gp.addFilter(new QLEqualsFilter("nomcid", str));
				objCidade = (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE008CEP"), gp);
			}
			NeoObject cidadeAlvo = null;
			for(NeoObject obj : objCidade)
			{
				EntityWrapper w = new EntityWrapper(obj);
				String cepini = NeoUtils.safeOutputString(w.findValue("cepini"));
				String cepfim = NeoUtils.safeOutputString(w.findValue("cepfim"));
				
				if((NeoUtils.safeLong(cep) >= NeoUtils.safeLong(cepini)) && NeoUtils.safeLong(cep) <= NeoUtils.safeLong(cepfim))
				{
					cidadeAlvo = obj;
				}
			}
			NeoObject eformPai = PersistEngine.getNeoObject(NeoUtils.safeLong(eform));
			EntityWrapper wPai = new EntityWrapper(eformPai);
			
			
			
			// prepara o retorno
			wPai.setValue("cidade", cidadeAlvo);
			PersistEngine.persist(eformPai);
			if (cidadeAlvo != null){
				EntityWrapper wCidade = new EntityWrapper(cidadeAlvo);
				String campo = NeoUtils.safeOutputString(NeoUtils.safeOutputString(wCidade.findValue("nomcid")));
				
				String logradouro = null;
				String bairro = null;
				if (encontrouViaWs){
					logradouro = new String(jo.getString("tipoDeLogradouro").getBytes(),"UTF-8") + " " + new String(jo.getString("logradouro").getBytes(),"UTF-8");
					bairro = new String(jo.getString("bairro").getBytes(),"UTF-8");
				}else{
					logradouro = "";
					bairro = "";
				}
				
				out.print(wEstado.findValue("abrufs").toString() + ";" + cidadeAlvo.getNeoId() + ";" +campo+";" +logradouro+";" +bairro);
			}else{
				if (encontrouViaWs){
					out.write("ORSEGERR-NEF;Faixa de Cep não cadastrada na base do Sapiens. Proceder com o cadastro da faixa de CEP no Sapiens antes de proseguir;"+retErr);
				}else{
					out.write("ORSEGERR-NE;Cep não encontrado.;");
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	
	
	private void doLimpaListaEquipamentos(HttpServletRequest request, HttpServletResponse response)
	{
		/*
		 * verificar os itens da lista de equipamentos
		 * remover da lista apenas
		 */
		try
		{
			final PrintWriter out = response.getWriter();
			
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");

			String objId = request.getParameter("objId");
			
			String msgRetorno = "";
			EFormField equipamentosPosto = null;
			EFormField kitsPosto = null;
			
			EForm eform = EFormManager.getInstance().get(objId).getChildForm("equipamentosItens");
			NeoObject itens = eform.getObject();
			EntityWrapper wItens = new EntityWrapper(itens);
			
			
			equipamentosPosto = eform.getField("equipamentosPosto");
			kitsPosto = eform.getField("kitsPosto");
			
			Collection<NeoObject> equipamentosLista = equipamentosPosto.getValues2Add();
			if(equipamentosLista.size() <= 0)
			{
				//se está vazia o equipamentos ja foram persistidos
				equipamentosLista = (Collection<NeoObject>) equipamentosPosto.getValue();
			}
			
			Collection<NeoObject> kitsRemovidos = kitsPosto.getValues2Remove();
			Collection<NeoObject> itensTratar = new ArrayList<NeoObject>();
			for(NeoObject equip : equipamentosLista)
			{
				EntityWrapper wEquip = new EntityWrapper(equip);
				if(wEquip.findValue("idKit") != null)
				{
					NeoObject kitEquip = PersistEngine.getNeoObject((Long) wEquip.findValue("idKit"));
					if(kitsRemovidos.contains(kitEquip))
					{
						itensTratar.add(equip);
						//equipamentosPosto.getValues2Remove().add(equip);
						//equipamentosPosto.getValues2Add().remove(equip);
						//wItens.findField("equipamentosPosto").getValues().remove(equip);
						if(msgRetorno.equals(""))
							msgRetorno = NeoUtils.safeOutputString(equip.getNeoId());
						else
							msgRetorno += ";" + NeoUtils.safeOutputString(equip.getNeoId());
						
					}
					
				}
			}
			
			for(NeoObject o : itensTratar)
			{
				wItens.findField("equipamentosPosto").getValues().remove(o);
				//equipamentosPosto.getValues2Remove().add(o);
				equipamentosPosto.getValues2Add().remove(o);
			}
			out.print(msgRetorno);
		}catch(Exception e)
		{
			Log.warn("Erro");
		}
		
	}
	
	private void doPopulaListaEquipamentos(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			final PrintWriter out = response.getWriter();
			
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");

			String ids = request.getParameter("ids");
			
			String objId = request.getParameter("objId");
			String neoId = request.getParameter("objNeoId");
			/*
			 * TODO
			 * 
			 * não limpar a lista de quipamentos
			 * vincular ao equipamento o id do kit
			 * 
			 */

			String[] neoIds = ids.split(",");
			String neoidComboInserido = "";
			NeoObject comboInserido = null;
			for(String neoid : neoIds)
			{
				if(NeoUtils.safeLong(neoid) < 0)
				{
					neoidComboInserido = neoid;
					comboInserido = PersistEngine.getNeoObject(NeoUtils.safeLong(neoidComboInserido));
				}
			}
			
			String msgRetorno = "";
			EFormField equipamentosPosto = null;
			
			EForm eform = EFormManager.getInstance().get(objId).getChildForm("equipamentosItens");
			equipamentosPosto = eform.getField("equipamentosPosto");
			
			//pegar o obj pai, para pegar o numero do contrato
			//e o codigo do posto
			
			NeoObject objPai = PersistEngine.getNeoObject(NeoUtils.safeLong(neoId));
			EntityWrapper wrapperPai = new EntityWrapper(objPai);
			
			long sequencia = (Long) wrapperPai.findValue("sequencia");
			
			response.setContentType("text/plain");
			response.setCharacterEncoding("ISO-8859-1");

			Collection<NeoObject> listaEquipamentos = new ArrayList<NeoObject>();
			
			if(comboInserido != null)
			{
				EntityWrapper wKit = new EntityWrapper(comboInserido);
				
				long codigoKit 	= NeoUtils.safeLong(NeoUtils.safeOutputString(wKit.findValue("kit.usu_codkit")));
				long quantidade = NeoUtils.safeLong(NeoUtils.safeOutputString(wKit.findValue("quantidade")));
				NeoObject tipoInstalacao = (NeoObject) wKit.findValue("tipoInstalacao");
				NeoObject instalador = (NeoObject) wKit.findValue("instaladores");
				
				StringBuilder sql = new StringBuilder();
				sql.append("select "); 
				sql.append("kpo.usu_codpro, ");
				sql.append("kpo.usu_qtdpro ");

				sql.append("from usu_t160kpr kpo ");
				
				sql.append("where ");
				sql.append("kpo.usu_codkit = " + codigoKit);
				
				Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
				Collection<Object> resultList = query.getResultList();
				if (resultList != null && !resultList.isEmpty())
				{
					for (Object result : resultList)
					{
						if (result != null)
						{
							Object[] os = (Object[]) result;
							String cod_produto = NeoUtils.safeOutputString(os[0]);
							Long qtd = NeoUtils.safeLong(NeoUtils.safeOutputString(os[1]));
							
							QLGroupFilter filter = new QLGroupFilter("AND");
							filter.addFilter(new QLRawFilter("codpro = '"+cod_produto+"' and codEmp = 1 "));
							
							NeoObject prod = PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSEPRO"), filter);
							/*Collection<NeoObject> itens2Remove = new ArrayList<NeoObject>();
							for(NeoObject equipamento : equipamentosPosto.getValues2Remove())
							{
								EntityWrapper w = new EntityWrapper(equipamento);
								NeoObject sapiensEquip = (NeoObject) w.findValue("produto");
								if(sapiensEquip.getNeoId() == prod.getNeoId())
									itens2Remove.add(equipamento);
								
							}
							
							if(itens2Remove.size() > 0)
							{
								equipamentosPosto.getValues2Remove().removeAll(itens2Remove);
							}*/
							
							if(NeoUtils.safeIsNotNull(prod))
							{
								NeoObject produto = AdapterUtils.createNewEntityInstance("FGCListaEquipamentosPosto");
								EntityWrapper wProd = new EntityWrapper(produto);
								wProd.setValue("produto", prod);
								wProd.setValue("quantidade", qtd * quantidade);
								//wProd.setValue("codigoInstalador", "");
								wProd.setValue("instaladores", instalador);
								wProd.setValue("tipoInstalacao", tipoInstalacao);
								sequencia++;
								wProd.setValue("sequencia", sequencia);
								wProd.setValue("origemSapiens", false);
								wProd.setValue("codigoKit", NeoUtils.safeOutputString(codigoKit));
								long isCombo = comboInserido.getNeoId();
								wProd.setValue("idKit", isCombo);
								PersistEngine.persist(produto);
								wrapperPai.findField("equipamentosPosto").addValue(produto);
								listaEquipamentos.add(produto);
							}
							
						}
					}
				}
			}
			
			wrapperPai.setValue("sequencia", sequencia);
			/*if(wrapperPai.findField("equipamentosPosto") != null &&
					wrapperPai.findField("equipamentosPosto").getValues().size() > 0)
			{
				for(Object obj : wrapperPai.findValues("equipamentosPosto", 0))
				{
					NeoObject f = (NeoObject) obj;
					equipamentosPosto.getValues2Remove().add(f);
					//wrapperPai.findField("resumoGeralDespesas").removeValue(f);
				}
				PersistEngine.persist(objPai);
				//wrapperPai.findField("resumoGeralDespesas").getValues().clear();
			}*/
			
			for(NeoObject prod : listaEquipamentos)
			{
				PersistEngine.persist(prod);
				equipamentosPosto.getValues2Add().add(prod);
				if(msgRetorno.equals(""))
					msgRetorno = NeoUtils.safeOutputString(prod.getNeoId());
				else
					msgRetorno += ";" + NeoUtils.safeOutputString(prod.getNeoId());
			}
			out.print(msgRetorno);
		}catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("[FLUXO CONTRATOS]- popularListaEquipamentos > " +e.getMessage());
		}
		
	}
	
	public void doConsultaSerasa(HttpServletRequest request, HttpServletResponse response)
	{
		response.setContentType("text/plain");
		String eform = request.getParameter("eform");
		System.out.println("[FLUXO CONTRATOS]-"+eform);
		String cpfcnpj = "0";
		String tipoPessoa = "F";
		
		
		if (eform != null){
			NeoObject eformPai = PersistEngine.getNeoObject(Long.parseLong(eform));
			EntityWrapper wPai = new EntityWrapper(eformPai);
			if (wPai.findValue("buscaNomeCliente.cgccpf") != null){
				cpfcnpj = ((Long) wPai.findValue("buscaNomeCliente.cgccpf")).toString();
			}else if (wPai.findValue("novoCliente.cpf") != null){
				cpfcnpj = (String) wPai.findValue("novoCliente.cpf");
			}else if (wPai.findValue("novoCliente.cnpj") != null){
				cpfcnpj = (String) wPai.findValue("novoCliente.cnpj");
			}
			
			if (wPai.findValue("buscaNomeCliente.tipcli") != null){
				tipoPessoa = (String) wPai.findValue("buscaNomeCliente.tipcli");
			}else if (wPai.findValue("novoCliente.tipocliente.tipo") != null){
				tipoPessoa = (String) wPai.findValue("novoCliente.tipocliente.tipo");
			}
			
		}
		
		
		cpfcnpj = cpfcnpj.replaceAll("\\.","").replaceAll("/", "").replace("-", "");
		/*if (cpfcnpj.length() <= 11){
			tipoPessoa = "F";
		}else{
			tipoPessoa = "J";
		}*/
		
		tipoPessoa = tipoPessoa.toUpperCase();
		System.out.println("[FLUXO CONTRATOS]-Consulta serasa ao documento: " + cpfcnpj + ", Tipo de Pessoa: "+ tipoPessoa);
		
		PrintWriter out = null ;
		
		TrustManager[] trms = new TrustManager[]{new MyTrustManager(null)};  
        SSLContext ssl;  
          
        try{  
            ssl = SSLContext.getInstance("TLS");  
            ssl.init(null, trms, null);  
            SSLContext.setDefault(ssl);  
        } catch (NoSuchAlgorithmException e) {  
            e.printStackTrace();  
        } catch (KeyManagementException e) {  
            e.printStackTrace();  
        }
		try
		{
			out = response.getWriter();
			/*String login = "57693410";
			String senha = "40302010";*/
			
			// login bloqueado
//			String login = "67038306";
//			String senha = "832777";
			/*String login = "67038325";
			String senha = "ors10203";*/
			String login = "57201123";
			String senha = "d06m07";
			
			
			RegistroSerasa regB49C = RegistroSerasaFactory.getReg("B49C");
			regB49C.setupValores("B49C","",cpfcnpj,tipoPessoa,"C","FI","","","S","99","S","INI","A","N","","","","","","N","","","");
			RegistroSerasa regP002 = RegistroSerasaFactory.getReg("P002"); 
			regP002.setupValores("P002","RSPU","","","","","","","","");
			RegistroSerasa  regI001 = RegistroSerasaFactory.getReg("I001");
			regI001.setupValores("I001","00","D","S","N","N","N","N","N","N","N","N","N","N","N","N","","","","","","","N","N","N","","","");
			RegistroSerasa regT999 = RegistroSerasaFactory.getReg("T999");
			regT999.setupValores("T999","","","");
			
			String content2 = SerasaUtils.enviaConsultaSerasa(login, senha, regB49C.parseToString()
																			+regP002.parseToString()
																			+regI001.parseToString()
																			+regT999.parseToString()
							,true);
			
			StringBuilder resultado = new StringBuilder();
			//resultado.append( cpfcnpj + "  \n" );
			System.out.println("[FLUXO CONTRATOS]-"+content2+"\n");
			//resultado.append("Criticas: ");
			if (content2.contains("B49C") ){
				if (content2.contains("A900")){
					if (content2.contains("NADA CONSTA")){
						resultado.append("Nada Consta \n");
					}else{
						resultado.append("A900 \n");
					}
				}
				if (content2.contains("B900")){
					
					out.print("Erro"+content2.substring(content2.indexOf("B900"),content2.indexOf("B900")+115));
					System.out.println("[FLUXO CONTRATOS]-Erro " + content2.substring(content2.indexOf("B900"),content2.indexOf("B900")+115));
				}
				if (content2.contains("I10000")){
					// 
					//resultado.append("I100-00 ");
				}
				if (content2.contains("I10099")){
					// 
					//resultado.append("I100-99 ");
				}
				if (content2.contains("I101")){
					// 
					//resultado.append("I101 ");
				}
				if (content2.contains("I105")){
					// Grafias
					resultado.append("Variacoes de grafia para o documento consultado. \n");
				}
				if (content2.contains("I110")){ // 0..n
					
					resultado.append("Possui protestos. \n");
				}
				if (content2.contains("I12000") || content2.contains("I12001") || content2.contains("I12002") || content2.contains("I12003") ){
					resultado.append("Possui acoes judiciais. \n");
				}
				if (content2.contains("I14000") || content2.contains("I14001") || content2.contains("I14002") || content2.contains("I14003") ){
					resultado.append("REFIN (Restricao Financeira). \n");
				}
				if (content2.contains("I16000") || content2.contains("I16001") || content2.contains("I16002") ){
					resultado.append("Cheques sem fundos. \n");
				}
				if (content2.contains("I17000") || content2.contains("I17001") || content2.contains("I17002") ){
					resultado.append("Cheques sem fundos BB . \n");
				}
				if (content2.contains("I22000") || content2.contains("I22001") || content2.contains("I22002") || content2.contains("I22003")){
					resultado.append("PEFIN (Pendencia Financeira). \n");
				}
				
			}else{
				throw new Exception("Erro ao receber retorno do webservice do serasa. Serviço Indisponível.");
			}
			//System.out.println("Retorno: " + resultado.toString() + "\n---------------");
			out.write( resultado.toString() ); 
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
			out.write("ERRSerasa - Erro ao consultar serasa.");
		}
	}
	
	
}
