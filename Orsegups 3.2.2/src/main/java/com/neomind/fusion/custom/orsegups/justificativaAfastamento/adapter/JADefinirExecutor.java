package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.util.JAUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class JADefinirExecutor implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	boolean isProcessoAutomatico = (boolean) processEntity.findValue("isProcessoAutomatico");
	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
	if (NeoUtils.safeIsNull(origin)) {
	    if (!isProcessoAutomatico) {
		NeoUser usuario = PortalUtil.getCurrentUser();

		processEntity.findField("executor").setValue(usuario);

	    }

	} else if (origin.getActivityName().equalsIgnoreCase("Justificar ausência - Anexar documento")) {

	    NeoPaper papel = null;

	    NeoObject objTipoDocumento = (NeoObject) processEntity.findValue("tipoDocumento");

	    if (NeoUtils.safeIsNotNull(objTipoDocumento)) {
		EntityWrapper wDoc = new EntityWrapper(objTipoDocumento);

		long tipoDoc = (long) wDoc.findValue("idDocumento");

		if (tipoDoc == 1 || tipoDoc == 3) {

		    NeoObject medico = (NeoObject) processEntity.findValue("medico");
		    
		    if (medico == null){
			throw new WorkflowException("Lembre-se de informar o médico!");
		    }

		    EntityWrapper wMedico = new EntityWrapper(medico);

		    String crm = wMedico.findField("crm").getValueAsString();

		    Map<String, String> listaTopMedicos = JAUtils.getListaTopMedicos();

		    boolean topMedico = false;

		    if (listaTopMedicos.containsKey(crm)) {
			String nome = wMedico.findField("nome").getValueAsString();

			if (listaTopMedicos.get(crm).equalsIgnoreCase(nome)) {
			    papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "jaTopMedicosAtestados"));
			    topMedico = true;
			}

		    }

		    if (!topMedico) {

			NeoObject colaborador = (NeoObject) processEntity.findValue("colaborador");

			EntityWrapper wColaborador = new EntityWrapper(colaborador);

			long cpf = (long) wColaborador.findField("numcpf").getValue();

			Map<Long, String> listaTopColaboradores = JAUtils.getListaTopColaboradores();

			if (listaTopColaboradores.containsKey(cpf)) {
			    papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "jaTopColaboradoresAtestados"));
			} else {
			    papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "jaGestorAtestados"));
			}

		    }

		    processEntity.findField("gestorAtestados").setValue(papel);

		    GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazo").getValue();
		    prazo.add(Calendar.DAY_OF_MONTH, +5);

		    while (!OrsegupsUtils.isWorkDay(prazo)) {
			prazo.add(Calendar.DAY_OF_MONTH, +1);
		    }

		    processEntity.findField("prazo").setValue(prazo);

		}
	    }

	} else {
	    if (!isProcessoAutomatico) {
		NeoUser usuario = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code", PortalUtil.getCurrentUser().getCode()));
		processEntity.findField("executor").setValue(usuario);
	    }

	}
	NeoPaper papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "jaAuditor"));

	processEntity.findField("auditor").setValue(papel);
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {


    }

}
