package com.neomind.fusion.custom.orsegups.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class AdapterValidaTarefasAbertasParaPosVendaRMC implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		boolean encaminharPosVenda = true;

		if (processEntity.getValue("isRastreamento") != null)
		{
			boolean isRastreamento = (boolean) processEntity.getValue("isRastreamento");
			if (isRastreamento)
			{
				Long numeroRMC = (Long) processEntity.getValue("numeroRMC");
				String numeroContrato = String.valueOf(processEntity.getValue("numeroContrato"));

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				String codigo = activity.getCode();
				QLFilter numeroRMCQl = new QLEqualsFilter("numeroRMC", numeroRMC);
				QLFilter numeroContratoQl = new QLEqualsFilter("numeroContrato", numeroContrato);
				QLFilter situacao = new QLEqualsFilter("wfprocess.processState", 0);

				groupFilter.addFilter(numeroRMCQl);
				groupFilter.addFilter(numeroContratoQl);
				groupFilter.addFilter(situacao);

				List<NeoObject> objsRMC = PersistEngine.getObjects(AdapterUtils.getEntityClass("RelatorioMovimentacaoContrato"), groupFilter);

				if (objsRMC.size() > 1)
				{
					encaminharPosVenda = false;
				}
			}
		}

		processEntity.setValue("encaminharPosVenda", encaminharPosVenda);

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		//Nao implementado
	}

}
