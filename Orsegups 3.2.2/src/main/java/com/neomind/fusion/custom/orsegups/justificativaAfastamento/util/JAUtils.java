package com.neomind.fusion.custom.orsegups.justificativaAfastamento.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.vo.JAHistoricoAfastamentoBean;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.vo.JAHistoricoBean;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.vo.JAHistoricoMedicoBean;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.datawarehouse.NeoDataSource;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.util.NeoUtils;

/**
 * Utilidades do fluxo de afastamento
 * 
 * @author mateus.batista
 *
 */
public class JAUtils {
    
    private static final Log log = LogFactory.getLog(JAUtils.class);

    /**
     * Retorna o nome da atividade em execução
     * 
     * @param code
     * @return String nome da atividade
     */
    public static String getNomeAtividadeAtual(String code) {

	String sql = "SELECT AM.name FROM WFProcess P WITH(NOLOCK) " + " INNER JOIN D_jaJustificativaAfastamento J WITH(NOLOCK) ON J.wfprocess_neoId = P.neoId "
		+ " INNER JOIN Activity A WITH(NOLOCK) ON A.process_neoId = P.neoId " + " INNER JOIN ActivityModel AM WITH(NOLOCK) ON AM.neoId = A.model_neoId " + " WHERE P.code=? AND A.activityStatus=0 ";

	String nomeAtividade = "";

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    code = code.trim();

	    conn = PersistEngine.getConnection("");

	    pstm = conn.prepareStatement(sql);

	    pstm.setString(1, code);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		nomeAtividade = rs.getString(1);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return nomeAtividade;
    }

    /**
     * Retorno lista de afastamentos do colaborador
     * 
     * @param cpf
     * @return List com afastamentos
     */
    public static List<JAHistoricoAfastamentoBean> getListaAfastamentos(long cpf, String tarefaRemover) {

	String sql = "SELECT AFA.DatAfa, AFA.DatTer, AFA.SitAfa, SIT.DesSit, T.tarefa FROM  R038AFA AFA WITH(NOLOCK) " + " INNER JOIN r034fun FUN WITH(NOLOCK) ON FUN.numcad = AFA.NumCad AND FUN.numemp = AFA.NumEmp "
		+ " INNER JOIN R010SIT SIT WITH(NOLOCK) ON SIT.CodSit = AFA.SitAfa "
		+ " LEFT JOIN [CACUPE\\SQL02].Fusion_Producao.dbo.D_TarefaAfastamentoJustificativa T WITH(NOLOCK) ON T.numcpf = FUN.numcpf AND T.datafa = AFA.DatAfa "
		+ " WHERE FUN.numcpf = ? AND FUN.sitafa != 7 AND AFA.DatAfa >= GETDATE()-365 " 
		+ " ORDER BY AFA.DatAfa";

	// QLPresencaUtils.retornaNeoIdProcess

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	List<JAHistoricoAfastamentoBean> retorno = null;

	try {

	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql);

	    pstm.setLong(1, cpf);

	    rs = pstm.executeQuery();

	    retorno = new ArrayList<JAHistoricoAfastamentoBean>();

	    while (rs.next()) {

		JAHistoricoAfastamentoBean novoAfa = new JAHistoricoAfastamentoBean();

		Calendar dataInicio = Calendar.getInstance();

		dataInicio.setTime(rs.getDate("DatAfa"));

		novoAfa.setDataInicio(dataInicio);

		Calendar dataTermino = Calendar.getInstance();

		dataTermino.setTime(rs.getDate("DatTer"));

		novoAfa.setDataTermino(dataTermino);

		novoAfa.setSituacao(rs.getInt("SitAfa"));

		novoAfa.setDescricao(rs.getString("DesSit"));

		String tarefa = rs.getString("tarefa");

		String link = "";

		if (tarefa != null && !tarefa.isEmpty()) {
		    if (!tarefa.equals("Afastamento Informado")) {
			long wfProcess = QLPresencaUtils.retornaNeoIdProcess("[JA] Justificativa de Afastamento", tarefa);

			if (wfProcess == 0L) {
			    wfProcess = QLPresencaUtils.retornaNeoIdProcess("G001 - Tarefa Simples", tarefa);
			}

			JAUtils.getDadosProcesso(wfProcess, novoAfa);

			link = "<a href=\"https://intranet.orsegups.com.br/fusion/bpm/workflow_search.jsp?id=" + wfProcess
				+ "\" target=\"_blank\"><img title=\"Pesquisar\" src=\"https://intranet.orsegups.com.br/fusion/imagens/icones_final/visualize_blue_16x16-trans.png\" align=\"absmiddle\"> " + tarefa
				+ "</a>";
		    } else {
			link = tarefa;
		    }
		}else{
		    link = "Inserido via Vetorh (Rubi)";
		}

		novoAfa.setLink(link);

		if (novoAfa.getStatusProcesso() != 2 && !novoAfa.getLink().contains(tarefaRemover)) {
		    retorno.add(novoAfa);
		}

	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return retorno;

    }

    /**
     * Retorno lista de historico do médico
     * 
     * @param crm
     * @return List com historicos
     */
    public static List<JAHistoricoMedicoBean> getListaHistoricoMedico(String crm, String tarefaExcluir) {

	String sql = "SELECT w.code,w.neoId,w.processState,w.startDate,med.nome,med.crm,cid.codigo,cid.descricao,fun.nomfun,fun.numcad,fun.numemp FROM D_jaJustificativaAfastamento ja WITH(NOLOCK) "
		+ " inner join WFProcess w WITH(NOLOCK) on w.neoId = ja.wfprocess_neoId " + " inner join D_JAMedico med WITH(NOLOCK) on med.neoId = ja.medico_neoId "
		+ " left join D_jaCID cid WITH(NOLOCK) on cid.neoId = ja.cid_neoId " + " inner join X_VETORHRFUN funFus WITH(NOLOCK) on funFus.neoId = ja.colaborador_neoId "
		+ " inner join [FSOODB04\\SQL02].Vetorh.DBO.R034FUN fun WITH(NOLOCK) on fun.numcad = funFus.numcad and fun.numemp = funFus.numemp and fun.tipcol = funFus.tipcol "
		+ " where w.processState <> 2 and med.crm = ? and w.code != ? order by w.code ";

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	List<JAHistoricoMedicoBean> retorno = null;

	try {

	    conn = PersistEngine.getConnection("");

	    pstm = conn.prepareStatement(sql);

	    pstm.setString(1, crm);

	    pstm.setString(2, tarefaExcluir);

	    rs = pstm.executeQuery();

	    retorno = new ArrayList<JAHistoricoMedicoBean>();

	    while (rs.next()) {

		JAHistoricoMedicoBean novoAfa = new JAHistoricoMedicoBean();

		novoAfa.setNome(rs.getString("nome"));
		novoAfa.setCrm(rs.getString("crm"));
		novoAfa.setCodCid(rs.getString("codigo") != null ? rs.getString("codigo") : "");
		novoAfa.setDesCid(rs.getString("descricao") != null ? rs.getString("descricao") : "Não informado");
		novoAfa.setNomFun(rs.getString("nomfun"));
		novoAfa.setNumCad(rs.getLong("numcad"));
		novoAfa.setNumEmp(rs.getLong("numemp"));
		novoAfa.setStatusProcesso(rs.getInt("processState"));

		Calendar dataProcesso = Calendar.getInstance();
		dataProcesso.setTimeInMillis(rs.getTimestamp("startDate").getTime());
		novoAfa.setDataProcesso(dataProcesso);

		String link = "<a href=\"https://intranet.orsegups.com.br/fusion/bpm/workflow_search.jsp?id=" + rs.getLong("neoId")
			+ "\" target=\"_blank\"><img title=\"Pesquisar\" src=\"https://intranet.orsegups.com.br/fusion/imagens/icones_final/visualize_blue_16x16-trans.png\" align=\"absmiddle\"> " + rs.getString("code")
			+ "</a>";

		novoAfa.setLink(link);

		retorno.add(novoAfa);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return retorno;

    }

    /**
     * Retorna CPF do Colaborador
     * 
     * @param numCad
     *            Número do cadastro do colaborador (Matrícula)
     * @param numEmp
     *            Número da empresa do colaborador
     * @return long Número do CPF
     */
    public static long getCpfColaborador(int numCad, int numEmp) {

	String sql = "SELECT numcpf FROM R034FUN FUN WITH(NOLOCK) WHERE FUN.numcad = ? AND FUN.numemp = ?";

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	long retorno = 0L;

	try {

	    conn = PersistEngine.getConnection("VETORH");

	    pstm = conn.prepareStatement(sql);

	    pstm.setInt(1, numCad);

	    pstm.setInt(2, numEmp);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		retorno = rs.getLong(1);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return retorno;
    }

    /**
     * Retorna startdate e processState do processo
     * 
     * @param wfprocess
     * @param historico
     * @return objecto JAHistoricoBean com os dados
     */
    public static JAHistoricoBean getDadosProcesso(long wfprocess, JAHistoricoBean historico) {

	String sql = "SELECT P.startDate, P.processState FROM WFProcess P WITH(NOLOCK) WHERE P.neoId = ? ";

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    conn = PersistEngine.getConnection("");

	    pstm = conn.prepareStatement(sql);

	    pstm.setLong(1, wfprocess);

	    rs = pstm.executeQuery();

	    if (rs.next()) {

		Calendar dataProcesso = Calendar.getInstance();

		dataProcesso.setTimeInMillis(rs.getTimestamp(1).getTime());

		historico.setDataProcesso(dataProcesso);

		historico.setStatusProcesso(rs.getInt(2));
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return historico;

    }

    /**
     * Retorna wrapper com eform
     * 
     * @param idPai
     * @return EntityWrapper
     */
    public static EntityWrapper getEformPai(long idPai) {

	@SuppressWarnings("deprecation")
	NeoObject tarefa = PersistEngine.getNeoObject(idPai);
	EntityWrapper wrapper = new EntityWrapper(tarefa);

	return wrapper;
    }

    /**
     * Retorna mapa com os top médicos que emitem atestados conforme parametros
     * especificados em [JA] Parâmetros Atestados Médicos
     * 
     * @return HashMap<crm, nome>
     */
    public static Map<String, String> getListaTopMedicos() {

	List<NeoObject> listaParametros = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("jaParametrosAtestadosMedicos"));

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	Map<String, String> retorno = null;

	if (listaParametros != null && !listaParametros.isEmpty()) {
	    NeoObject parametros = listaParametros.get(0);

	    EntityWrapper wParametros = new EntityWrapper(parametros);

	    Long qtdAtestados = (Long) wParametros.findField("qtdAtestados").getValue();

	    Long qtdMedicos = (Long) wParametros.findField("qtdMedicos").getValue();

	    StringBuilder sql = new StringBuilder();

	    if (qtdMedicos > 0) {
		sql.append("SELECT TOP " + qtdMedicos + " M.crm, M.nome, COUNT(*) AS ATESTADOS FROM WFProcess P WITH(NOLOCK) ");
	    } else {
		sql.append("SELECT M.crm, M.nome, COUNT(*) AS ATESTADOS FROM WFProcess P WITH(NOLOCK) ");
	    }

	    sql.append(" INNER JOIN D_jaJustificativaAfastamento J WITH(NOLOCK) ON J.wfprocess_neoId = P.neoId");
	    sql.append(" INNER JOIN D_JAMedico M WITH(NOLOCK) ON M.neoId = J.medico_neoId");
	    sql.append(" WHERE M.neoId != 701121162");
	    sql.append(" GROUP BY M.crm , M.nome");
	    sql.append(" HAVING COUNT(*) >=" + qtdAtestados + "");
	    sql.append(" ORDER BY 2 DESC");

	    try {

		conn = PersistEngine.getConnection("");

		stmt = conn.createStatement();

		rs = stmt.executeQuery(sql.toString());

		retorno = new HashMap<>();

		while (rs.next()) {
		    retorno.put(rs.getString("crm"), rs.getString("nome"));
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, stmt, rs);
	    }

	}

	return retorno;
    }

    /**
     * Retorna lista dos 10 colaboradores com mais afastamentos por atestados
     * nos ultimos 6 meses
     * 
     * @return Map<Long,String> cpf, nome funcionario
     */
    public static Map<Long, String> getListaTopColaboradores() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	String sql = "SELECT TOP 10 FUN.numcpf, FUN.nomfun, SUM (DATEDIFF(DAY, AFA.DatAfa, AFA.DatTer)+1) AS DIAS FROM R038AFA AFA "
		+ " INNER JOIN r034fun FUN ON FUN.numcad = AFA.NumCad AND FUN.numemp = AFA.NumEmp AND FUN.tipcol = AFA.TipCol " 
		+ " WHERE AFA.DatAfa >= GETDATE()-180 and AFA.SitAfa=14 " 
		+ " AND FUN.sitafa != 7 "
		+ " GROUP BY FUN.numcpf, FUN.nomfun " 
		+ " ORDER BY 3 DESC";

	Map<Long, String> retorno = null;

	try {

	    conn = PersistEngine.getConnection("VETORH");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    retorno = new HashMap<>();

	    while (rs.next()) {
		retorno.put(rs.getLong("numcpf"), rs.getString("nomfun"));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return retorno;
    }

    public static int safeInsert(String nomeFonteDados, String sqlToInsert, int maxTrys, int intervalTry) throws Exception
    {
	int retorno = 0;
	String retInsercao = "tentardenovo";
	PreparedStatement pstData = null;
	Connection conn = null; 
	int cont = 0;
	do{
	    try {

		conn = PersistEngine.getConnection(nomeFonteDados);

		pstData = conn.prepareStatement(sqlToInsert);

		Integer result = pstData.executeUpdate();

		if(result > 0){
		    
		    retInsercao = "";
		    
		    retorno++;
		    
		}


	    } catch (SQLException e) {
		e.printStackTrace();
		System.out.println("");
		retInsercao = "tentardenovo";
		if (e.getMessage().contains("java.sql.DataTruncation")){
		    System.out.println("");
		    throw new Exception("Coluna execedeu o tamanho, SQL=> " + sqlToInsert.toString().substring(0, (sqlToInsert.toString().length() > 25 ? 25 : sqlToInsert.toString().length() )  ));
		} 
		//e.printStackTrace();
	    }finally {
		OrsegupsUtils.closeConnection(conn, pstData, null);
	    }
	    cont++;
	    try {
		if(retInsercao.equals("tentardenovo")){
		    Thread.sleep(intervalTry);
		}
	    } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}while(retInsercao.equals("tentardenovo") && cont < maxTrys);


	OrsegupsUtils.closeConnection(conn, pstData, null);

	return retorno;
    }

    public static ResultSet getResultSet(String query, String dataSourceName)
    {
	NeoDataSource data = (NeoDataSource) PersistEngine.getObject(NeoDataSource.class,
		new QLEqualsFilter("name", dataSourceName));

	if (data != null)
	{
	    Connection con = OrsegupsUtils.openConnection(data);
	    return selectTable(query, con);
	}
	return null;
    }

    public static ResultSet selectTable(String consulta, Connection con)
    {
	try
	{
	    Statement stm = (Statement) con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		    ResultSet.CONCUR_READ_ONLY);
	    ResultSet rs = stm.executeQuery(consulta);
	    if (rs.first())
	    {
		return rs;
	    }
	}
	catch (SQLException e)
	{
	    System.out.println("ERRO selectTable ->"+consulta);
	    log.error("Select table error", e);
	}
	return null;
    }

}
