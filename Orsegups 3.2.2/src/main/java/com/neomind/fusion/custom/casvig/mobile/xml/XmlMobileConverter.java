package com.neomind.fusion.custom.casvig.mobile.xml;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public abstract class XmlMobileConverter
{
	/** 
	 * @deprecated usar {@link org.apache.axis.utils.XMLUtils#newDocument(InputStream)}
	 * 
	 * @author Daniel Henrique Joppi 18/11/2009
	 */
	public static Document builderDocument(InputStream in)
	{
		try
		{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			return db.parse(in);

		}
		catch (ParserConfigurationException pce)
		{
			pce.printStackTrace();
		}
		catch (SAXException saxe)
		{
			saxe.printStackTrace();
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
		return null;
	}

	/** 
	 * @deprecated usar {@link org.apache.axis.utils.XMLUtils#newDocument()}
	 * 
	 * @author Daniel Henrique Joppi 18/11/2009
	 */
	public static Document newDocument()
	{
		try
		{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			return db.newDocument();

		}
		catch (ParserConfigurationException pce)
		{
			pce.printStackTrace();
		}
		return null;
	}

	/**
	 * Aletra o enconding do Document para o padrão ISO-8859-1.
	 * 
	 * @author Daniel Henrique Joppi 10/12/2008
	 * @param documet
	 * XML gerado.
	 * @return XML com o encoding padrão ISO-8859-1.
	 */
	public static Document setEnconding(Document documet)
	{
		try
		{
			Document doc = newDocument();

			TransformerFactory tfactory = TransformerFactory.newInstance();
			Transformer transformer = tfactory.newTransformer();
			transformer.setOutputProperty("encoding", "UTF-8");
			transformer.transform(new DOMSource(documet), new DOMResult(doc));

			return doc;
		}
		catch (TransformerException trans)
		{
			trans.printStackTrace();
		}
		return null;
	}
}
