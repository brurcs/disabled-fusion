package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteTipoContatoVO
{
	private Long id;
	private String tipo;
	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public String getTipo()
	{
		return tipo;
	}
	public void setTipo(String tipo)
	{
		this.tipo = tipo;
	}
}
