package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.sapiens.vo.ContratoClienteVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaAbreTarefaSimplesContratoSuspenso implements CustomJobAdapter {

    private Map<Integer, String> mapaUsuarios;

    @Override
    public void execute(CustomJobContext ctx) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT CTR.usu_codemp as EMP, CTR.usu_codfil AS FIL, CTR.usu_numctr AS CTR, CTR.usu_datsus AS DTSUS, CLI.CodCli, CLI.NomCli, cli.TipEmc ");
	sql.append(" FROM usu_t160ctr CTR WITH(NOLOCK) ");
	sql.append(" INNER JOIN E085CLI CLI WITH(NOLOCK) ON CLI.CodCli = CTR.usu_codcli  ");
	sql.append(" INNER JOIN usu_t160cvs CVS WITH(NOLOCK) ON CVS.usu_numctr = CTR.usu_numctr AND CVS.usu_codemp = CTR.usu_codemp and CVS.usu_codfil = CTR.usu_codfil ");
	sql.append(" WHERE ctr.usu_susfat = 'S'  ");
	sql.append(" AND ctr.usu_datsus <= GETDATE()-30  ");
	sql.append(" AND ctr.usu_datsus != '1900-12-31 00:00:00.000' ");
	sql.append(" AND ((CTR.usu_sitctr = 'A') OR (CTR.usu_sitctr = 'I' and ctr.usu_datfim >= GETDATE())) ");
	sql.append(" AND NOT EXISTS  ");
	sql.append(" (SELECT 1 FROM E140NFV NFV WITH(NOLOCK) ");
	sql.append(" INNER JOIN E140ISV ISV WITH(NOLOCK) ON ISV.NumNfv = NFV.NumNfv AND ISV.CodEmp = NFV.CodEmp AND ISV.CodFil = NFV.CodFil AND ISV.CodSnf = NFV.CodSnf ");
	sql.append(" INNER JOIN e140ide IDE WITH(NOLOCK) ON IDE.NumNfv = NFV.NumNfv AND IDE.CodEmp = NFV.CodEmp AND IDE.CodFil = NFV.CodFil AND IDE.CodSnf = NFV.CodSNF ");
	sql.append(" WHERE ISV.CodEmp= CTR.usu_codemp AND ISV.CodFil= CTR.usu_codfil AND ISV.NumCtr= CTR.usu_numctr AND NFV.SitNfv = 2 AND IDE.sitdoe = 3) ");
	sql.append(" GROUP BY CTR.usu_codemp,  CTR.usu_codfil , CTR.usu_numctr, CTR.usu_datsus, CLI.CodCli, CLI.NomCli, cli.TipEmc ");

	sql.append(" UNION ");

	sql.append(" SELECT CTR.usu_codemp as EMP, CTR.usu_codfil AS FIL, CTR.usu_numctr AS CTR, CVS.usu_datsus AS DTSUS, CLI.CodCli, CLI.NomCli, cli.TipEmc ");
	sql.append(" FROM usu_t160cvs CVS WITH(NOLOCK)  ");
	sql.append(" INNER JOIN usu_t160ctr CTR WITH(NOLOCK) ON CVS.usu_numctr = CTR.usu_numctr AND CVS.usu_codemp = CTR.usu_codemp AND CVS.usu_codfil = CTR.usu_codfil ");
	sql.append(" INNER JOIN E085CLI CLI WITH(NOLOCK) ON CLI.CodCli = CTR.usu_codcli  ");
	sql.append(" WHERE cvs.usu_susfat = 'S'  ");
	sql.append(" AND ((cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= GETDATE())) ");
	sql.append(" AND cvs.usu_datsus != '1900-12-31 00:00:00.000' ");
	sql.append(" AND cvs.usu_datsus <= GETDATE()-30 ");
	sql.append(" AND CTR.usu_susfat = 'N' ");
	sql.append(" AND NOT EXISTS  ");
	sql.append(" (SELECT 1 FROM E160CVS N WITH(NOLOCK) WHERE N.NumCtr = CVS.usu_numctr AND N.CodEmp = CVS.usu_codemp AND N.CodFil = CVS.usu_codfil and N.usu_seqagr =  CVS.usu_numpos) ");
	sql.append(" GROUP BY CTR.usu_codemp, CTR.usu_codfil, CTR.usu_numctr, CVS.usu_datsus, CodCli,NomCli,cli.TipEmc ");

	List<ContratoClienteVO> listaContratos = new ArrayList<ContratoClienteVO>();

	try {
	    conn = PersistEngine.getConnection("SAPIENS");
	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		ContratoClienteVO c = new ContratoClienteVO();

		c.setCodEmp(rs.getInt("EMP"));
		c.setCodFil(rs.getInt("FIL"));
		c.setNumCtr(rs.getInt("CTR"));

		Date dt = rs.getDate("DTSUS");
		GregorianCalendar data = new GregorianCalendar();
		data.setTimeInMillis(dt.getTime());
		c.setDatSus(data);

		c.setCodCli(rs.getInt("CodCli"));
		c.setNomCli(rs.getString("NomCli"));
		c.setTipEmc(rs.getInt("TipEmc"));

		listaContratos.add(c);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
	
	for (ContratoClienteVO c : listaContratos) {
	    
	    QLGroupFilter filter = new QLGroupFilter("AND");
	       	    
	    filter.addFilter(new QLEqualsFilter("codEmp", c.getCodEmp()));
	    filter.addFilter(new QLEqualsFilter("codFil", c.getCodFil()));
	    filter.addFilter(new QLEqualsFilter("numCtr", c.getNumCtr()));
	    filter.addFilter(new QLEqualsFilter("codCli", c.getCodCli()));
	    
	    filter.addFilter(new QLOpFilter("dataProximaVerificacao", ">=", new GregorianCalendar()));
	    
	    List<NeoObject> historico = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("jobRotinaTarefaSimplesContratoSuspenso"), filter);
	    
	    if (historico == null || historico.isEmpty()){
		String solicitante = "charlot.andrade";
		
		if (c.getCodEmp() == 18) {
		    if (c.getCodFil() == 2 || c.getCodFil() == 3) {
			solicitante = this.mapaUsuarios.get(6);
		    } else {
			solicitante = this.mapaUsuarios.get(c.getCodEmp());
		    }
		} 
		else if (c.getCodEmp() == 21 && c.getCodFil() == 4) {
		    solicitante = this.mapaUsuarios.get(2);
		}
		else {
		    solicitante = this.mapaUsuarios.get(c.getCodEmp());
		}		
		
		NeoPaper papelExecutor = null;
		NeoUser usuarioExecutor = null;
		String executor = "";
		
		if (c.getTipEmc() == 1) {
		    //ExecutorTarefaPostoSuspensoPrivado
		    papelExecutor = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "ExecutorTarefaPostoSuspensoPrivado"));
		} else if (c.getTipEmc() == 2) {
		    //ExecutorTarefaPostoSuspensoPublico
		    papelExecutor = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "ExecutorTarefaPostoSuspensoPublico"));
		}
		
		if(NeoUtils.safeIsNotNull(papelExecutor)){
		    usuarioExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);		
		    executor = usuarioExecutor.getCode();
		}
		
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 5l);
		
		String titulo = c.getCodEmp() + "-" + c.getCodFil() + "-" + c.getNumCtr() + " Justificar contrato/posto suspenso a mais de 30 dias sem faturamento";
		
		StringBuilder descricao = new StringBuilder();
		
		descricao.append("Justificar contrato/posto suspenso a mais de 30 dias e sem faturamento. <br>");
		descricao.append("Empresa: " + c.getCodEmp() + "<br>");
		descricao.append("Filial: " + c.getCodFil() + "<br>");
		descricao.append("Contrato: " + c.getNumCtr() + "<br>");
		descricao.append("Cliente: " + c.getCodCli() + "-" + c.getNomCli() + "<br>");
		descricao.append("Data suspensão: " + NeoDateUtils.safeDateFormat(c.getDatSus(), "dd/MM/yyyy") + "<br>");
		
		String tarefa = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao.toString(), "3", "sim", prazo);
		
		if (tarefa != null && !tarefa.isEmpty()){
		    this.gravarHistorico(c.getCodEmp(), c.getCodFil(), c.getNumCtr(), c.getCodCli(), tarefa);
		}		
	    }

	}

    }
    
    private void gravarHistorico (long codEmp, long codFil, long numCtr, long codCli, String tarefa){
	
	InstantiableEntityInfo historico = AdapterUtils.getInstantiableEntityInfo("jobRotinaTarefaSimplesContratoSuspenso");
	NeoObject objHistorico = historico.createNewInstance();
	EntityWrapper wrapperHistorico = new EntityWrapper(objHistorico);
	

	wrapperHistorico.findField("codEmp").setValue(codEmp);
	wrapperHistorico.findField("codFil").setValue(codFil);
	wrapperHistorico.findField("numCtr").setValue(numCtr);
	wrapperHistorico.findField("codCli").setValue(codCli);
	
	GregorianCalendar data = new GregorianCalendar();
	data.set(Calendar.HOUR_OF_DAY, 0);
	data.set(Calendar.MINUTE, 0);
	data.set(Calendar.SECOND, 0);
	data.add(Calendar.DAY_OF_MONTH, 30);
	
	wrapperHistorico.findField("dataProximaVerificacao").setValue(data);
	wrapperHistorico.findField("tarefa").setValue(tarefa);
	

	PersistEngine.persist(objHistorico);
	
    }

    public RotinaAbreTarefaSimplesContratoSuspenso() {

	mapaUsuarios = new TreeMap<Integer, String>();

	mapaUsuarios.put(1, "taise.soares");
	mapaUsuarios.put(2, "amanda.rossi");
	mapaUsuarios.put(6, "taise.soares");
	mapaUsuarios.put(7, "gileno.junior");
	
	mapaUsuarios.put(15, "amanda.rossi");
	mapaUsuarios.put(17, "amanda.rossi");
	mapaUsuarios.put(18, "francielle.neckel");
	mapaUsuarios.put(19, "amanda.rossi");

	mapaUsuarios.put(21, "gileno.junior");
	mapaUsuarios.put(22, "gileno.junior");
	mapaUsuarios.put(24, "gileno.junior");
	mapaUsuarios.put(25, "charlot.andrade");
	mapaUsuarios.put(26, "charlot.andrade");
	
	mapaUsuarios.put(27, "amanda.rossi");
	mapaUsuarios.put(28, "gileno.junior");
	mapaUsuarios.put(29, "francielle.neckel");	
	mapaUsuarios.put(30, "charlot.andrade");
	
    }

}
