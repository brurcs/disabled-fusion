package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class LeituraMensagemTIVO
{
	String userField;
	String telefone;
	GregorianCalendar dataHora;
	String conclusaoMensagem;
	public String getUserField()
	{
		return userField;
	}
	public void setUserField(String userField)
	{
		this.userField = userField;
	}
	public String getTelefone()
	{
		return telefone;
	}
	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}
	public GregorianCalendar getDataHora()
	{
		return dataHora;
	}
	public void setDataHora(GregorianCalendar dataHora)
	{
		this.dataHora = dataHora;
	}
	public String getConclusaoMensagem()
	{
		return conclusaoMensagem;
	}
	public void setConclusaoMensagem(String conclusaoMensagem)
	{
		this.conclusaoMensagem = conclusaoMensagem;
	}
	@Override
	public String toString()
	{
		return "LeituraMensagemTIVO [userField=" + userField + ", telefone=" + telefone + ", dataHora=" + dataHora + ", conclusaoMensagem=" + conclusaoMensagem + "]";
	}
	
	
	
	
}
