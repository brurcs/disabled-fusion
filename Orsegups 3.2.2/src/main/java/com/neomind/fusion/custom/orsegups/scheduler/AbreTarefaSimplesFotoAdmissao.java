package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaSimplesFotoAdmissao implements CustomJobAdapter
{
    @Override
    public void execute(CustomJobContext arg0)
    {
	Connection conn = PersistEngine.getConnection("VETORH");
	StringBuilder sql = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesFotoAdmissao");
	log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissao - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	try
	{

	    sql.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, fun.DatAdm, fun.NomFun,orn.USU_CodReg, car.TitRed, orn.USU_LotOrn, orn.NomLoc, reg.USU_NomReg ");
	    sql.append(" FROM R034FUN fun WITH (NOLOCK) ");
	    sql.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
	    sql.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.TabOrg = fun.TabOrg AND orn.NumLoc = fun.NumLoc ");
	    sql.append(" INNER JOIN USU_T200REG reg WITH (NOLOCK) ON reg.USU_CodReg = orn.usu_codreg ");
	    sql.append(" WHERE fun.DatAdm > '2018-03-13' AND fun.numemp not in (3,4,12) AND fun.sitafa <> 7 ");
	    sql.append(" AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaFotoAdmissao tad WITH (NOLOCK) ");
	    sql.append(" 			     WHERE tad.numemp = fun.numemp AND tad.tipcol = fun.tipcol AND tad.numcad = fun.numcad ");
	    sql.append("			     AND CAST(floor(CAST(tad.datadm AS FLOAT)) AS datetime) = CAST(floor(CAST(fun.datadm AS FLOAT)) AS datetime)) ");
	    sql.append(" AND orn.USU_CodReg NOT IN (0,1,9,13,15) ");

	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();

	    while (rs.next())
	    {
		GregorianCalendar datAdm = new GregorianCalendar();
		datAdm.setTime(rs.getDate("DatAdm"));

		String titulo = "Enviar foto do colaborador - " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun");
		String descricao = "Enviar em anexo a foto do colaborador " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun") + ", " + rs.getString("TitRed") + ", Regional: " + rs.getString("USU_NomReg") + ", admitido do dia " + NeoDateUtils.safeDateFormat(datAdm, "dd/MM/yyyy");
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		Long codReg = rs.getLong("USU_CodReg");

		NeoPaper papelSolicitante = new NeoPaper();
		NeoPaper papelExecutor = new NeoPaper();
		String solicitante = "";
		String executor = "";
		
		papelSolicitante = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteFotoAdmissao"));
		
		if(papelSolicitante != null && papelSolicitante.getAllUsers() != null && !papelSolicitante.getAllUsers().isEmpty()){
		    
		    for(NeoUser user : papelSolicitante.getAllUsers()){
			
			solicitante = user.getCode();
			break;
			
		    }
		    
		}
		else
		{
		    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão - Papel de Solicitante pode não existir");
		    continue;
		}
		
		papelExecutor = OrsegupsUtils.getPapelFotoAdmissao(codReg, null);

		if (papelExecutor != null && papelExecutor.getAllUsers() != null && !papelExecutor.getAllUsers().isEmpty())
		{
		    for (NeoUser user : papelExecutor.getUsers())
		    {
			executor = user.getCode();
			break;
		    }
		}
		else
		{
		    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão - Papel de Executor pode não existir");
		    continue;
		}

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

		InstantiableEntityInfo tarefaAD = AdapterUtils.getInstantiableEntityInfo("TarefaFotoAdmissao");
		NeoObject noAD = tarefaAD.createNewInstance();
		EntityWrapper ajWrapper = new EntityWrapper(noAD);

		ajWrapper.findField("numemp").setValue(rs.getLong("NumEmp"));
		ajWrapper.findField("tipcol").setValue(rs.getLong("TipCol"));
		ajWrapper.findField("numcad").setValue(rs.getLong("NumCad"));
		ajWrapper.findField("datadm").setValue(datAdm);
		ajWrapper.findField("tarefa").setValue(tarefa);
		PersistEngine.persist(noAD);

		log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão - Responsavel " + executor + " Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoDateUtils.safeDateFormat(rs.getDate("DatAdm"), "dd/MM/yyyy"));

		Thread.sleep(1000);
	    }
	}
	catch (Exception e)
	{
	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
	finally
	{

	    try
	    {
		rs.close();
		pstm.close();
		conn.close();
	    }
	    catch (SQLException e)
	    {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	    log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
    }
}