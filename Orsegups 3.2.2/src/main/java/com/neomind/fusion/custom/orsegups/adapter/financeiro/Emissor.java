package com.neomind.fusion.custom.orsegups.adapter.financeiro;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="emit", namespace = "http://www.portalfiscal.inf.br/nfe")
public class Emissor {
	
	private String cnpj;
	private String razaoSocial;
	private String nomeFantasia;
	private EnderecoEmissor enderecoEmissor;
	private String IE;
	private Long crt;
	
	@XmlElement(name="CNPJ", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	@XmlElement(name="xNome", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getRazaoSocial() {
		return razaoSocial;
	}
	
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	@XmlElement(name="xFant", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	
	@XmlElement(name="enderEmit", namespace = "http://www.portalfiscal.inf.br/nfe")
	public EnderecoEmissor getEndereco() {
		return enderecoEmissor;
	}
	
	public void setEndereco(EnderecoEmissor enderecoEmissor) {
		this.enderecoEmissor = enderecoEmissor;
	}
	
	@XmlElement(name="IE", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getIE() {
		return IE;
	}
	
	public void setIE(String iE) {
		IE = iE;
	}
	
	@XmlElement(name="CRT", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCrt() {
		return crt;
	}
	
	public void setCrt(Long crt) {
		this.crt = crt;
	}
	
}
