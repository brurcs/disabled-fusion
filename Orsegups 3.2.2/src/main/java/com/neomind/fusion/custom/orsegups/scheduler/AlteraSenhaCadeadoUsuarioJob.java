package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class AlteraSenhaCadeadoUsuarioJob implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(AlteraSenhaCadeadoUsuarioJob.class);

	@Override
	public void execute(CustomJobContext ctx)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		try{
			
			conn = PersistEngine.getConnection("");
			sql.append("update dbo.D_TICadeado set senha = ?  where usuarioFusion_neoId in (select neoId from dbo.neouser where fullname like 'zz%' )");
			
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1,geraSenha());
			
			int i = 0;
			
			i = pstm.executeUpdate();
			
			
		}catch(Exception e){
			OrsegupsUtils.closeConnection(conn, pstm, null);
			e.printStackTrace();
		    System.out.println("[" + key + "] AlteraSenhaCadeadoUsuarioJob erro no processamento." + e.getMessage());
		    log.error("AlteraSenhaCadeadoUsuarioJob erro no processamento:");
		    throw new JobException("Erro no processamento. Procurar no log por [" + key + "] " + e.getMessage());
		}
		
	}
	
	private static Long geraSenha(){
		Random r = new Random( System.currentTimeMillis() );
	    return (Long) 1000L + r.nextInt(9999);
		
	}
	
	public static void main(String[] args){
		System.out.println(geraSenha());
	}

}
