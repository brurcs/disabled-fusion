package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HorarioVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoSupervisaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.UsuarioFusionVO;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class QLPresencaSupervisaoUtils
{
	
	public static final int PP_USUARIO_FUSION = 0;
	public static final int PP_COORDENADOR_REGIONAL = 1;
	public static final int PP_GERENTE_REGIONAL = 2;
	
	private static HorarioVO horManha = new HorarioVO();
	private static HorarioVO horTarde = new HorarioVO();
	private static HorarioVO horNoite = new HorarioVO();
	private static HorarioVO horMadrugada = new HorarioVO();

	static{
		horManha.setDataInicial(new GregorianCalendar(2000, 01, 01, 6, 0, 0 ));
		horManha.setDataFinal(new GregorianCalendar(2000, 01, 01, 11, 59, 59 ));
		
		horTarde.setDataInicial(new GregorianCalendar(2000, 01, 01, 12, 0, 0 ));
		horTarde.setDataFinal(new GregorianCalendar(2000, 01, 01, 17, 59, 59 ));
		
		horNoite.setDataInicial(new GregorianCalendar(2000, 01, 01, 18, 0, 0 ));
		horNoite.setDataFinal(new GregorianCalendar(2000, 01, 01, 23, 59, 59 ));
		
		horMadrugada.setDataInicial(new GregorianCalendar(2000, 01, 01, 0, 0, 0 ));
		horMadrugada.setDataFinal(new GregorianCalendar(2000, 01, 01, 5, 59, 59 ));
	}
	
	
	/**
	 * Com base no cadastro do colaborador e centro de custo do posto supervisionado, é retornado o supervisor que deverá responder as tarefas.
	 * @param numemp - Código da Empresa do Colaborador supervisionado
	 * @param numcad - Número da matrícula do Colaborador supervisionado
	 * @param codccu - Centro de custo do posto supervisionado
	 * @return String contendo o nome do supervisor no vetorh
	 */
	public static ColaboradorVO retornaSupervisorPosto(Long numemp, Long numcad, Long tipcol, Long codccu){
		ColaboradorVO retorno = null;
		Connection connVetorh = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try{
			System.out.println("Colaborador :" + numemp + "/" + numcad + ", codccu do posto: " + codccu);
			connVetorh = PersistEngine.getConnection("VETORH");
			EscalaVO escalaColabOperacional = getEscalaColaborador(numcad,numemp,tipcol);
			// encontrar a escala do colaborador
			
			// buscar lista postos de Supervisao vinculados
			List<PostoSupervisaoVO> postosSupervisao = QLPresencaUtils.listaPostosSupervisaoVinculados(codccu);
			boolean abortarPesquisa = false;
			for (PostoSupervisaoVO psup: postosSupervisao){
				System.out.println("Verificando posto de supervisao: " + psup.getUsuCodccu());
				//buscar lista de supervisores do posto
				List<ColaboradorVO> supervisores = getListaSupervisores(psup.getUsuCodccu(), null);
				// Navegar na lista até encontrar um supervisor onde combine o grupo de escala e o turno. Vai pegar sempre o colaborador com a escala com inicio mais tardio se o turno for diurno .
				// Isto serve para os postos 8 horas/6 horas.
				if (supervisores!= null && supervisores.size() == 1){ // se houver apenas 1 colaborador no posto de supervisão, este será o responsável sempre.
					retorno = supervisores.get(0);
				}else{
					for (ColaboradorVO supervisor : supervisores){
						System.out.println("Varrendo supervisores: " + supervisor.getNumeroEmpresa() + "/" + supervisor.getNumeroCadastro() + ", " + supervisor.getNomeColaborador());
						Boolean escalaCompativel = false;
						escalaCompativel = comparaEscalas(escalaColabOperacional, supervisor.getEscala());
						
						if (escalaCompativel){
							retorno = supervisor;
							abortarPesquisa = true; // já encontrou alguem no posto corrente, não precisa navegar em outro posto
							
							if ( escalaColabOperacional.getTipTur().equals("D") && psup.getRegional().getUsuCodReg() == 1){ // se o turno for diurno e o posto for de são josé, pegar escala mais cedo
								break;
							}
							
							if (escalaColabOperacional.getTipTur().equals("N") ){ // se for turno noturno, pega a escala que inicia mais cedo
								break;
							}
							
							
							//break; //removido para poder pegar sempre a ultima escala compativel
						}
					}
					
				}
				if (abortarPesquisa){
					break; // abora a navegação nos outros postos
				}
			}
			
			if (retorno == null){
				System.out.println(retorno);
			}
			
			return retorno;
		}catch(Exception e){
			e.printStackTrace();
			return retorno;
		}finally{
			OrsegupsUtils.closeConnection(connVetorh, pst, rs);
		}
	}
	
	/**
	 * Retorna a escala do colaborador
	 * @param numCad
	 * @param numEmp
	 * @param tipCol
	 * @return
	 */
	private static EscalaVO getEscalaColaborador(Long numCad, Long numEmp, Long tipCol){
		
		EscalaVO retorno = new EscalaVO();
		StringBuffer queryColaborador = new StringBuffer();
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try{
			conn = PersistEngine.getConnection("VETORH");
			//tarefa 754996 - utilizar o grupo da escala como referencia 
			queryColaborador.append(" SELECT ESC.NOMESC, gres.usu_gruesc, gres.usu_tiptur, hes.CodTma ");
			queryColaborador.append(" FROM R034FUN FUN ");
			queryColaborador.append(" INNER JOIN R034CPL CPL ON CPL.NUMEMP = FUN.NUMEMP AND CPL.TIPCOL = FUN.TIPCOL AND CPL.NUMCAD = FUN.NUMCAD");
			queryColaborador.append(" INNER JOIN R038HES HES ON HES.NUMEMP = FUN.NUMEMP AND"); 
			queryColaborador.append("						  HES.TIPCOL = FUN.TIPCOL AND"); 
			queryColaborador.append("						  HES.NUMCAD = FUN.NUMCAD AND"); 
			queryColaborador.append("						  DATALT = (SELECT MAX(DATALT) FROM R038HES HES1 WHERE HES.NUMEMP = HES1.NUMEMP AND");
			queryColaborador.append("						  HES.TIPCOL = HES1.TIPCOL AND"); 
			queryColaborador.append("						  HES.NUMCAD = HES1.NUMCAD)"); 
			queryColaborador.append(" INNER JOIN R006ESC ESC ON ESC.CODESC = HES.CODESC");	
			queryColaborador.append(" inner join usu_t006gres gres on esc.usu_gruesc = gres.usu_gruesc ");
			queryColaborador.append(" WHERE FUN.NUMCAD = ? AND FUN.NUMEMP = ? AND FUN.tipcol = ?");
			
			pst = conn.prepareStatement(queryColaborador.toString());
			pst.setLong(1, numCad);
			pst.setLong(2, numEmp);
			pst.setLong(3, tipCol);
			rs = pst.executeQuery();
			while (rs.next()){
				retorno.setDescricao(rs.getString("NomEsc"));
				retorno.setGrupo(rs.getLong("usu_gruesc"));
				retorno.setTipTur(rs.getString("usu_tiptur"));
				retorno.setCodigoTurma(rs.getLong("CodTma"));
				retorno.setHorarios(escala2Horarios(retorno.getDescricao(), new GregorianCalendar()));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}finally{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}
				
		return retorno;
	}
	
	
	/**
	 * Rertorna lista de supervisores de um posto de supervisores com base no centro de custo.
	 * @param codccu
	 * @param gc
	 * @return
	 */
	private static List<ColaboradorVO> getListaSupervisores(Long codccu, GregorianCalendar gc){
		List<ColaboradorVO> retorno = new ArrayList<ColaboradorVO>();
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder(); 	
		sql.append(" declare @codccu numeric(9,0) ");
		sql.append(" declare @datRef DateTime ");
		sql.append(" select @codccu = ? ");
		sql.append(" select @datRef =  dateadd(d, datediff(d,0, getdate()), 0) ");  
		sql.append(" ( ");
		sql.append(" select 'N' as tipLot, fun.numemp, fun.numcad, fun.nomfun, fun.numcpf, orn.nomloc, orn.numloc, orn.usu_codccu,  esc.NomEsc+'.'+cast(hes.CodTma as varchar), esc.usu_gruesc, gres.usu_tiptur "); 
		sql.append(" from r016orn orn "); 
		sql.append(" inner join r038hlo hlo on orn.numloc = hlo.NumLoc "); 
		sql.append(" inner join r034fun fun on hlo.numcad = fun.numcad and hlo.numemp = fun.numemp ");
		sql.append(" inner join r038hes hes on hes.NumCad = fun.numcad and hes.NumEmp = fun.numemp ");
		sql.append(" inner join R006ESC esc on hes.CodEsc = esc.CodEsc ");
		sql.append(" inner join usu_t006gres gres on esc.usu_gruesc = gres.usu_gruesc ");
		sql.append(" where 1=1 ");
		sql.append(" and orn.usu_codccu = cast(@codccu as varchar) ");
		sql.append(" and orn.usu_sitati = 'S' and fun.sitafa <> 7 ");
		sql.append(" and hlo.datAlt = (select max(hlo1.datAlt) from r038hlo hlo1 where hlo1.numcad = fun.numcad and hlo1.numemp = fun.numemp) ");
		sql.append(" and hes.datAlt = (select max(hes1.datAlt) from r038hes hes1 where hes1.numcad = fun.numcad and hes1.numemp = fun.numemp) ");
		sql.append(" and not exists (select 1 from usu_t038cobfun cob1 where cob1.usu_numLocTra = orn.numloc and cob1.usu_datinccob <=  @datRef  and cob1.usu_datfim >=  @datRef and fun.numcad = cob1.usu_numcadcob) ");
		sql.append(" ) ");
		sql.append(" union ");
		sql.append(" ( ");
		sql.append(" select 'F' as tipLot, fun.numemp, fun.numcad, fun.nomfun, fun.numcpf, orn.nomloc, orn.numloc, orn.usu_codccu,  esc.NomEsc+'.'+cast(hes.CodTma as varchar), esc.usu_gruesc, gres.usu_tiptur "); 
		sql.append(" from r016orn orn  ");
		sql.append(" inner  join usu_t038cobfun cob on cob.usu_numLocTra = orn.numloc and cob.usu_datinccob <=  @datRef  and cob.usu_datfim >=  @datRef "); 
		sql.append(" inner join r034fun fun on cob.usu_numcad = fun.NumCad and cob.usu_numemp = fun.NumEmp ");
		sql.append(" inner join r038hes hes on hes.NumCad = fun.numcad and hes.NumEmp = fun.numemp ");
		sql.append(" inner join R006ESC esc on hes.CodEsc = esc.CodEsc ");
		sql.append(" inner join usu_t006gres gres on esc.usu_gruesc = gres.usu_gruesc ");
		sql.append(" where 1=1 ");
		//sql.append(" and usu_possup is not null ");
		sql.append(" and orn.usu_codccu = cast(@codccu as varchar) ");
		sql.append(" and orn.usu_sitati = 'S' and fun.sitafa <> 7 ");
		sql.append(" and hes.datAlt = (select max(hes1.datAlt) from r038hes hes1 where hes1.numcad = fun.numcad and hes1.numemp = fun.numemp) ");
		sql.append(" ) ");
		
		try{
			conn = PersistEngine.getConnection("VETORH");
			pst = conn.prepareStatement(sql.toString());
			pst.setLong(1, codccu );
			rs = pst.executeQuery();
			while (rs.next()){
				ColaboradorVO col = new ColaboradorVO();
				
				col.setNumeroEmpresa(rs.getLong("numemp"));
				col.setNumeroCadastro(rs.getLong("numcad"));
				col.setNomeColaborador(rs.getString("nomfun"));
				col.setCpf(rs.getLong("numcpf"));
				col.setEscala(getEscalaColaborador(rs.getLong("numcad"), rs.getLong("numemp"), 1L)); // seta escala
				
				retorno.add(col);
			}
			return retorno;
		}catch (SQLException e){
			e.printStackTrace();
			return retorno;
		}finally{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}
	}
	
	/**
	 * Converte uma descricao de escala para uma lista de horarios
	 * @param descricaoEscala
	 * @param datRef
	 * @return
	 */
	private static List<HorarioVO> escala2Horarios(String descricaoEscala, GregorianCalendar datRef){
		List<HorarioVO> retorno = new ArrayList<HorarioVO>();
		System.out.println("Conversão da escala " + descricaoEscala);
		try{
			String[] jornadas = descricaoEscala.split("/");
			String turma = descricaoEscala.indexOf(".") > -1 ? descricaoEscala.substring(descricaoEscala.indexOf(".")+1) : "1";
			for (String jornada: jornadas){
				if (jornada.length() != 11){
					continue; // escalas com nomes fora do padrão utilizando /
				}
				HorarioVO hvo = new HorarioVO();
				GregorianCalendar data1 = (GregorianCalendar) datRef.clone();
				data1.set(GregorianCalendar.HOUR, Integer.parseInt(jornada.substring(0,2)));
				data1.set(GregorianCalendar.MINUTE, Integer.parseInt(jornada.substring(3,5)));
				data1.set(GregorianCalendar.SECOND, 0);
				data1.set(GregorianCalendar.MILLISECOND, 0);
				GregorianCalendar data2 = (GregorianCalendar) datRef.clone();
				data2.set(GregorianCalendar.HOUR, Integer.parseInt(jornada.substring(6,8)));
				data2.set(GregorianCalendar.MINUTE, Integer.parseInt(jornada.substring(9,11)));
				data2.set(GregorianCalendar.SECOND, 0);
				data2.set(GregorianCalendar.MILLISECOND, 0);
				
				hvo.setDataInicial(data1);
				hvo.setDataFinal(data2);
				System.out.println(NeoDateUtils.safeDateFormat(data1) + " / " + NeoDateUtils.safeDateFormat(data2) + ", turma:"+ turma);
				retorno.add(hvo);
			}
			return retorno;
		
		}catch(Exception e){
			e.printStackTrace();
			return retorno;
		}
		
	}
	
	
	/**
	 * Compara duas escalas
	 * @param escalaColaborador
	 * @param escalaSupervisor
	 * @return true se foram compativeis e false se não
	 */
	private static Boolean comparaEscalas(EscalaVO escalaColaborador, EscalaVO escalaSupervisor ){
		Boolean retorno = false;
		StringBuilder debug = new StringBuilder();
		
		try{
			debug.append("Comparando escalas.\n Escalas: " + escalaColaborador.getDescricao() + " | " + escalaSupervisor.getDescricao());
			debug.append("\n turno: " + escalaColaborador.getTipTur() + " | " + escalaSupervisor.getTipTur());
			debug.append("\n turma: " + escalaColaborador.getCodigoTurma() + " | " + escalaSupervisor.getCodigoTurma());
			
			/**
			 * verifica se a turma e o turno coincidem
			 */
			if (escalaColaborador.getCodigoTurma().equals(escalaSupervisor.getCodigoTurma()) /*turma*/
				&&
				escalaColaborador.getTipTur().equals(escalaSupervisor.getTipTur()) /*turno*/
				){
			retorno = true;	
			}
			debug.append("\nResultado: " + retorno);
			System.out.println("[PRESUP] + " + debug.toString());
			return retorno;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("[PRESUP] ERR + " + debug.toString());
			return retorno;
		}
	}
	
	
	
	private static NeoPaper getPapelCoordenadorRegional(Long codreg)
	{
		String nomePapel = "";
		
		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Coordenador de Segurança";
				break;
			case 2:
				nomePapel = "Coordenador Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Coordenador Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Coordenador Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Coordenador Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Coordenador Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "CoordenadorEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "Coordenador Escritório Regional Gaspar";
				break;
			case 9:
				nomePapel = "Coordenador de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Coordenador Escritório Regional CCO";
				break;
			case 11:
				nomePapel = "Coordenador Escritório Regional RSL";
				break;
			case 12:
				nomePapel = "Coordenador Escritório Regional JGS";
				break;
			case 13:
				nomePapel = "Coordenador Escritório Regional CTA";
				break;
			case 14:
				nomePapel = "Coordenador Escritório Regional CSC";
				break;
			case 15:
				nomePapel = "Coordenador Escritório Regional CTA Sul";
				break;	
			case 16:
				nomePapel = "Coordenador Escritório Regional TRO";
				break;
			
		}
		
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",nomePapel));
		return obj;
	}
	
	private static NeoPaper getPapelCoordenadorRegional(Long codreg, String lotacao)
	{
		String nomePapel = "";
		
		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Supervisor CEREC";
				}
				
				else if (lotacao != null && lotacao.equals("Coordenadoria de Suprimentos"))
				{
					nomePapel = "Coordenador de Suprimentos";
				}
				
				else if (lotacao != null && lotacao.equals("Gerência de Rastreamento e Gestão de Frota"))
				{
					nomePapel = "Gerente Rastreamento e Gestão Frota";
				}
				
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Coordenador de Ressarcimento";
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Supervisor Comercial Privado";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Coordenador Comercial";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Responsável Justificar Afastamento Qualidade";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Coordenador Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Coordenador Almoxarifado de Eletrônica Sede";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Responsável Marketing";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Supervisor de Infraestrutura de TI";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Justificar Afastamento CM";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
				
				else if (lotacao != null && lotacao.equals("Nexti"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				
				else
				{
					nomePapel = "AtualizarAcessosSistemas";
				}

				break;
			case 1:
				nomePapel = "Coordenador de Segurança";
				break;
			case 2:
				nomePapel = "Coordenador Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Coordenador Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Coordenador Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Responsável Tratar OS em excesso";
				break;
			case 6:
				nomePapel = "Coordenador Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "CoordenadorEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "Coordenador Escritório Regional Gaspar";
				break;
			case 9:
				nomePapel = "Coordenador de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Coordenador Escritório Regional CCO";
				break;
			case 11:
				nomePapel = "Coordenador Escritório Regional RSL";
				break;
			case 12:
				nomePapel = "Coordenador Escritório Regional JGS";
				break;
			case 13:
				nomePapel = "Coordenador Escritório Regional CTA";
				break;
			case 14:
				nomePapel = "Coordenador Escritório Regional CSC";
				break;
			case 15:
				nomePapel = "Coordenador Escritório Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Coordenador Escritório Regional TRO";
				break;			
			default:
				return getPapelCoordenadorRegional(codreg);
		}
		
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",nomePapel));
		return obj;
	}
	
	
	private static NeoPaper getPapelGerenteRegional(Long codreg, String lotacao) throws WorkflowException
	{
		String nomePapel = "";
		
		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Diretor de Tecnologia da Informação";
				}
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Diretor de Planejamento";
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Diretor de Tecnologia da Informação";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Gerente Comercial Mercado Público";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Diretor de Planejamento";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Gerente Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Diretor de Tecnologia da Informação";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Diretor de Tecnologia da Informação";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Gerente de Monitoramento";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
				
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
				
				break;
			
			default:
				return getPapelGerenteRegional(codreg);

		}
	
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",nomePapel));
		if(obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}
	
	private static NeoPaper getPapelGerenteRegional(Long codreg) throws WorkflowException
	{
		String nomePapel = "";
		
		switch (codreg.intValue())
		{
			case 0:
				nomePapel = "Gerente Comercial Mercado Privado";
				break;
			case 1:
				nomePapel = "Gerente de Segurança";
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				nomePapel = "Gerente Escritório Regional Curitiba";
				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;	
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba Sul";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;

		}
	
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",nomePapel));
		if(obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}
	
	
	/**
	 * Retorna usuário seguindo as novas regras do presença de busca de supervisores juntamente com os papeis de coordenação e gerência já existentes nas regionais.
	 * <br>
	 * Obs1.: Na pesquisa por colaborador especifico, quando não o encontrar pelo CPF, a rotina irá buscar o coordenador regional pelo código da regional.
	 * <br>
	 * Obs2.: Em caso de erro ou não encontrar o usuário correspondente, o usuário juliano.clemente será retornado.
	 * @param tipoBusca 0 - colaborador especifico, 1 - coordenador, 2 - gerente regional
	 * @param usuCodreg código da regional
	 * @param numcpf cpf do colaborador
	 * @return {@linkplain NeoUser}
	 */
	public static NeoUser getUsuario(Integer tipoBusca, Long usuCodreg, Long numcpf, String lotacao ){
		return getUsuario(tipoBusca, usuCodreg, numcpf, lotacao, true);
	}
	
	/**
	 * Retorna usuário seguindo as novas regras do presença de busca de supervisores juntamente com os papeis de coordenação e gerência já existentes nas regionais.
	 * <br>
	 * Obs1.: Na pesquisa por colaborador especifico, quando não o encontrar pelo CPF, a rotina irá buscar o coordenador regional pelo código da regional.
	 * <br>
	 * Obs2.: Em caso de erro, o usuário juliano.clemente será retornado.
	 * @param tipoBusca 0 - colaborador especifico, 1 - coordenador, 2 - gerente regional
	 * @param usuCodreg código da regional
	 * @param numcpf cpf do colaborador
	 * @param encontrarCoordenador Caso seja true, nas buscas por PP_USUARIO_FUSION vai utilizar o código da regional para encontrar o papel do coordenador
	 * @return {@linkplain NeoUser}
	 */
	public static NeoUser getUsuario(Integer tipoBusca, Long usuCodreg, Long numcpf, String lotacao, Boolean encontrarCoordenador ){
		NeoUser retorno = null;
		try{
			
			switch(tipoBusca){
				case PP_USUARIO_FUSION: // se não encontrar o usuario, buscar direto o coordenador, caso não o encontre, retornar juliano.clemente 
					UsuarioFusionVO usr = QLPresencaUtils.getUsuarioFusionByCPF(numcpf);
					if (usr == null){ // não encontrou o usuario
						retorno =  getUsuario(QLPresencaSupervisaoUtils.PP_COORDENADOR_REGIONAL,usuCodreg, null, lotacao);
					}else{
						retorno =  PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usr.getCode() ));
					}
					
					break;
				case PP_COORDENADOR_REGIONAL:
					NeoPaper papelCoordenador = getPapelCoordenadorRegional(usuCodreg, lotacao);
					if (papelCoordenador != null && papelCoordenador.getAllUsers() != null && !papelCoordenador.getAllUsers().isEmpty())
					{
						for (NeoUser user : papelCoordenador.getUsers())
						{
							retorno = user;
							break;
						}
					}
					
					if (retorno == null){
						retorno = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "juliano.clemente" ));
					}
					break;
				case PP_GERENTE_REGIONAL:
					NeoPaper papelGerente = getPapelGerenteRegional(usuCodreg, lotacao);
					if (papelGerente != null && papelGerente.getAllUsers() != null && !papelGerente.getAllUsers().isEmpty())
					{
						for (NeoUser user : papelGerente.getUsers())
						{
							retorno = user;
							break;
						}
					}
					
					if (retorno == null){
						retorno = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "juliano.clemente" ));
					}
					break;
				default:
					retorno = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "juliano.clemente" ));
					break;
			}
			System.out.println("[QLPresencaSupervisaoUtils]{getUsuario("+tipoBusca+","+usuCodreg+","+numcpf+","+lotacao + ")}. Usuario retornado :"+retorno.getCode());
			return retorno;
		}catch(Exception e){
			System.out.println("[QLPresencaSupervisaoUtils]{getUsuario("+tipoBusca+","+usuCodreg+","+numcpf+","+lotacao+")}. Erro ao pesquisar:"+e.getMessage());
			e.printStackTrace();
			retorno = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "juliano.clemente" ));
			return retorno;
		}
		
	}
	
	
	/**
	 * Rertorna o ultimo colaborador lotado no posto.
	 * @param codccu
	 * @return
	 */
	public static ColaboradorVO getUltimoColaboradorLotadoPosto(Long codccu){
		ColaboradorVO retorno = new ColaboradorVO();
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder(); 	
		sql.append(" select fun.numcad, fun.numemp, fun.tipcol, fun.numcpf, fun.nomfun from r034fun fun ");
		sql.append(" inner join dbo.R038HLO hlo  on fun.numcad = hlo.numcad and fun.numemp = hlo.numemp and fun.tipcol = hlo.tipcol ");
		sql.append(" inner join dbo.r016orn orn on hlo.NumLoc = orn.numloc ");
		sql.append(" where orn.usu_Codccu = ? ");
		sql.append(" and hlo.DatAlt = (select max(hlo1.datalt) from r038hlo hlo1 where hlo1.numloc = orn.numloc ) ");
		sql.append(" and sitafa <> 7 ");
		sql.append(" and orn.usu_sitati = 'S' ");
		
		
		try{
			conn = PersistEngine.getConnection("VETORH");
			pst = conn.prepareStatement(sql.toString());
			pst.setLong(1, codccu );
			rs = pst.executeQuery();
			if (rs.next()){
				
				ColaboradorVO col = new ColaboradorVO();
				col.setNumeroEmpresa(rs.getLong("numemp"));
				col.setNumeroCadastro(rs.getLong("numcad"));
				col.setNomeColaborador(rs.getString("nomfun"));
				col.setTipoColaborador(rs.getLong("tipcol"));
				col.setCpf(rs.getLong("numcpf"));
				col.setEscala(getEscalaColaborador(rs.getLong("numcad"), rs.getLong("numemp"), 1L)); // seta escala
				retorno = col;
				
			}
			return retorno;
		}catch (SQLException e){
			e.printStackTrace();
			return retorno;
		}finally{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}
	}
	
	
	
	public static void main(String[] args){
		escala2Horarios("18:00-21:00/21:15-00:40 Sáb12h", new GregorianCalendar());
		
		System.out.println( 1 % 2);
		System.out.println( 2 % 2);
		System.out.println( 3 % 2);
		
		
		GregorianCalendar menorCompetencia = new GregorianCalendar();
		menorCompetencia.set(GregorianCalendar.DAY_OF_MONTH, 1);	
		menorCompetencia.set(GregorianCalendar.HOUR, 0);	
		menorCompetencia.set(GregorianCalendar.MINUTE, 0);	
		menorCompetencia.set(GregorianCalendar.SECOND, 0);	
		menorCompetencia.set(GregorianCalendar.MILLISECOND, 0);	
		menorCompetencia.add(GregorianCalendar.MONTH, -3);	
		System.out.println(NeoDateUtils.safeDateFormat(menorCompetencia));
	}
	
	/**
	 * Verifica se há algum caso onde é necessário desviar as tarefas de um supervisor para outro. Verificar Tarefa Simples 774855 para melhor entendimento.
	 * @param numcpf - cpf do supervisor a ser consultado
	 * @return - cpf do supervisor que está cadastrado na regra para receber o redirecionamento de tarefas. Caso não exista execção, vai retornar o cpf passado por parametro.
	 */
	public static Long recuperaSupervisorExecao(Long numcpf){
		Long retorno = 0L;
		HashMap<Long, Long> map;
		
		try{
			map = new HashMap<Long, Long>();
			map.put(8902626911L, 816417962L); // de Alyson Caiama Raimundo para Daniel Ferreira da Fonseca
			
			if (map.get(numcpf) != null){
				retorno = map.get(numcpf);
				System.out.println("Exceção de redirecionamento de supervisor: de " + numcpf + " para " + retorno );
			}else{
				retorno = numcpf;
			}
			return retorno;
		}catch(Exception e){
			e.printStackTrace();
			return numcpf;
		}
		
	}
	
}
