package com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal;

public class Campos {
	
	private String i100;
	private String i108;
	private String i110;
	private String i112;
	private String i113;
	private String i117;
	private String i119;
	
	public String getI100() {
		return i100;
	}
	public void setI100(String i100) {
		this.i100 = i100;
	}
	public String getI108() {
		return i108;
	}
	public void setI108(String i108) {
		this.i108 = i108;
	}
	public String getI112() {
		return i112;
	}
	public void setI112(String i112) {
		this.i112 = i112;
	}
	public String getI110() {
		return i110;
	}
	public void setI110(String i110) {
		this.i110 = i110;
	}
	public String getI113() {
		return i113;
	}
	public void setI113(String i113) {
		this.i113 = i113;
	}
	public String getI117() {
		return i117;
	}
	public void setI117(String i117) {
		this.i117 = i117;
	}
	public String getI119() {
		return i119;
	}
	public void setI119(String i119) {
		this.i119 = i119;
	} 
}
