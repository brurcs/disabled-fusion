package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RRCDefineTextoSolicitacaoTarefaSimples implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String code = (String) processEntity.findValue("wfprocess.code");
		GregorianCalendar dataAbertura = (GregorianCalendar) processEntity.findValue("dataAbertura");
		String textoReclamacao = (String) processEntity.findValue("textoReclamacao");
		
		String titulo = code+" - "+processEntity.findValue("cliente");

		String textoPadrao = "Tarefa aberta a partir da RRC " + code + ", de " + NeoUtils.safeDateFormat(dataAbertura, "dd/MM/yyyy") + ", escalada último nível. Texto da RRC: " + textoReclamacao;
		
		NeoObject oTarefa = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper wTarefa = new EntityWrapper(oTarefa);
		
		wTarefa.setValue("Titulo", titulo);
		wTarefa.setValue("DescricaoSolicitacao", textoPadrao);   
		
		PersistEngine.persist(oTarefa);
		
		processEntity.setValue("formTarefaSimples", oTarefa);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
