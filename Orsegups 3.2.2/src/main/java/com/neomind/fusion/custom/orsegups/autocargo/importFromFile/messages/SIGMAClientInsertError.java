package com.neomind.fusion.custom.orsegups.autocargo.importFromFile.messages;

import java.util.HashMap;

public class SIGMAClientInsertError {
    private String status;
    private HashMap<String, String> errors;
    
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public HashMap<String, String> getErrors() {
        return errors;
    }
    public void setErrors(HashMap<String, String> errors) {
        this.errors = errors;
    }
    
    
    
}
