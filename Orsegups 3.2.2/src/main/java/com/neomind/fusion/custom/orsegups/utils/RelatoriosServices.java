package com.neomind.fusion.custom.orsegups.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import com.neomind.fusion.custom.orsegups.placa.OrsegupsPlacaXpl1;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoUtils;

public class RelatoriosServices
{

	/**
	 * 
	 * @param response
	 * @param usuCodMot
	 * @param usuDatAlt
	 * @throws Exception
	 */

	public static byte[] buscaFoto(String numEmp, String tipCol, String numCad) throws SQLException
	{

		PreparedStatement st = null;
		String nomeFonteDados = "VETORH";
		Connection conn = PersistEngine.getConnection(nomeFonteDados);
		byte[] imgData = null;
		StringBuffer sqlFoto = new StringBuffer();
		sqlFoto.append(" SELECT FotEmp FROM R034FOT WHERE NumEmp = " + numEmp + " AND TipCol = " + tipCol + " AND NumCad = " + numCad);
		st = conn.prepareStatement(sqlFoto.toString());
		ResultSet rs = st.executeQuery();

		if (rs.next())
		{
			imgData = rs.getBytes("FotEmp");
		}
		rs.close();
		st.close();

		return imgData;
	}

	public static void gerarRelatorioCoberturaFerias(HttpServletResponse response, String usuCodMot, String usuDatAlt, String usuNumCad, String usuTipCol) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();

		Connection connVetorh = null;
		InputStream myInputStream = null;
		try
		{

			if (!NeoUtils.safeIsNull(usuCodMot))
				parametros.put("USU_NUMEMP", usuCodMot);
			if (!NeoUtils.safeIsNull(usuNumCad))
				parametros.put("USU_NUMCAD", usuNumCad);
			if (!NeoUtils.safeIsNull(usuTipCol))
				parametros.put("USU_TIPCOL", usuTipCol);
			if (!NeoUtils.safeIsNull(usuDatAlt))
				parametros.put("USU_DATALT", usuDatAlt);

			byte[] foto = buscaFoto(usuCodMot, usuTipCol, usuNumCad);

			if (foto != null)
			{
				myInputStream = new ByteArrayInputStream(buscaFoto(usuCodMot, usuTipCol, usuNumCad));
			}

			if (!NeoUtils.safeIsNull(myInputStream))
				parametros.put("USU_FOTCOL", myInputStream);

			connVetorh = (Connection) PersistEngine.getConnection("VETORH");

			InputStream reportMain = ReportUtils.getReportJasper("RelCoberturaFerias");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"Cobertura_Ferias.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connVetorh);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);

			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connVetorh.close();
		}
	}

	public static void gerarRelatorioComissaoTerceirizadoxls(HttpServletResponse response, String codEmp, String codFil, String datIni, String datFim, String tipIns, String ponPro, String codIns) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(codEmp))
				parametros.put("CODEMP", codEmp);
			if (!NeoUtils.safeIsNull(codFil))
				parametros.put("CODFIL", codFil);
			if (!NeoUtils.safeIsNull(datIni))
				parametros.put("DATINI", datIni);
			if (!NeoUtils.safeIsNull(datFim))
				parametros.put("DATFIM", datFim);
			if (!NeoUtils.safeIsNull(ponPro))
				parametros.put("PONPRO", ponPro);
			if (!NeoUtils.safeIsNull(codIns))
				parametros.put("CODINS", codIns);
			if (!NeoUtils.safeIsNull(tipIns))
				parametros.put("TIPINS", tipIns);

			connection = (Connection) PersistEngine.getConnection("SAPIENS");

			InputStream reportMain = ReportUtils.getReportJasper("RelComTerXls");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"ComissoesTerceirizados.xls\"");

			//			JasperReport relatoriosJasper = (JasperReport)JRLoader.loadObject(jasperURL);  

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			// xls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,  Boolean.TRUE);  
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			//          mensagem ="{\"Result\":\"OK\"}";       
			//			response.getWriter().print(mensagem);
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static void gerarRelatorioComissaoTerceirizadopdf(HttpServletResponse response, String codEmp, String codFil, String datIni, String datFim, String tipIns, String ponPro, String conDet, String codIns) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(codEmp))
				parametros.put("CODEMP", codEmp);
			if (!NeoUtils.safeIsNull(codFil))
				parametros.put("CODFIL", codFil);
			if (!NeoUtils.safeIsNull(datIni))
				parametros.put("DATINI", datIni);
			if (!NeoUtils.safeIsNull(datFim))
				parametros.put("DATFIM", datFim);
			if (!NeoUtils.safeIsNull(ponPro))
				parametros.put("PONPRO", ponPro);
			if (!NeoUtils.safeIsNull(tipIns))
				parametros.put("TIPINS", tipIns);
			if (!NeoUtils.safeIsNull(conDet))
				parametros.put("conDetail", conDet);
			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
				parametros.put("USULOG", PortalUtil.getCurrentUser().getFullName());
			if (!NeoUtils.safeIsNull(codIns))
				parametros.put("CODINS", codIns);

			connection = (Connection) PersistEngine.getConnection("SAPIENS");

			InputStream reportMain = ReportUtils.getReportJasper("relComissoesTerceirizados");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"ComissoesTerceirizados.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			//			mensagem ="{\"Result\":\"OK\"}";       
			//			response.getWriter().print(mensagem);
			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}
	
	public static void gerarRelatorioRNF(HttpServletResponse response) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
				parametros.put("USU_LOG", PortalUtil.getCurrentUser().getFullName());

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("RelRateioNf");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"RelRateioNf.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf

			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			System.out.println(byteArray);
			servletOutputStream.write(byteArray);

			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());

		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioMNC(HttpServletResponse response) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
				parametros.put("USU_LOG", PortalUtil.getCurrentUser().getFullName());

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relMNC");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"RelMNC.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf

			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			System.out.println(byteArray);
			servletOutputStream.write(byteArray);

			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());

		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}
	
	public static void gerarRelatorioXpl1Regional(HttpServletResponse response, String dataInicio, String dataFim, String cdMunicipio, String municipio) throws Exception
	{
	    	ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
		    	
		    	if (!NeoUtils.safeIsNull(dataInicio))
			{
		    	    	parametros.put("P_INICIO_FORMATADA", dataInicio.substring(8)+"/"+dataInicio.substring(5,7)+"/"+dataInicio.substring(0,4));
		    	        dataInicio = dataInicio + " 00:00:00:000";
				parametros.put("P_INICIO", dataInicio);				
			}
			if (!NeoUtils.safeIsNull(dataFim))
			{
			    	parametros.put("P_FIM_FORMATADA",dataFim.substring(8)+"/"+dataFim.substring(5,7)+"/"+dataFim.substring(0,4));
			    	dataFim = dataFim + " 23:59:59:999";
				parametros.put("P_FIM", dataFim);				
			}
			if (!NeoUtils.safeIsNull(municipio))
			{
				parametros.put("P_MUNICIPIO", municipio);
			}
			switch (Integer.parseInt(cdMunicipio)) {
			        case 10040:
				    parametros.put("P_ROTA", "IAI");
			            break;
        			case 10471:
				    parametros.put("P_ROTA", "PAE");
        			    break;
        			case 10645:
        			    parametros.put("P_ROTA", "PMJ");
        	          	    break;
        			case 10667:
        			    parametros.put("P_ROTA", "CAS");
        	          	    break;
        			case 10674:
				    parametros.put("P_ROTA", "NHO");
        			    break;
				case 10754:
				    parametros.put("P_ROTA", "GNA");
				    break;
				case 11259:
				    parametros.put("P_ROTA", "TRI");
				    break;
				case 11272:
				    parametros.put("P_ROTA", "XLN"); 
				    break;
				case 11511:
				    parametros.put("P_ROTA", "SRR");
				    break;
				case 11897:
				    parametros.put("P_ROTA", "IBD");
		          	    break;
			}
			parametros.put("P_PERIODO", "Período: "+OrsegupsPlacaXpl1.converteDataSript(dataInicio)+" à "+OrsegupsPlacaXpl1.converteDataSript(dataFim));
			

			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
				parametros.put("USU_LOG", PortalUtil.getCurrentUser().getFullName());

			connection = (Connection) PersistEngine.getConnection("");

			InputStream reportMain = ReportUtils.getReportJasper("RelatorioTrocaPlacas");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"RelatorioTrocaPlacas.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf

			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			System.out.println(byteArray);
			servletOutputStream.write(byteArray);

			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());

		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarPlanilhaCHIP(HttpServletResponse response) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{
			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("planilhaCHIP");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"planilhaCHIP.xls\"");

			//			JasperReport relatoriosJasper = (JasperReport)JRLoader.loadObject(jasperURL);  

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			// xls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,  Boolean.TRUE);  
			//xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			//          mensagem ="{\"Result\":\"OK\"}";       
			//			response.getWriter().print(mensagem);
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}
	
	public static void gerarPlanilhaRegionais(HttpServletResponse response) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{
			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("planilhaRegionais");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"planilhaRegionais.xls\"");

			//			JasperReport relatoriosJasper = (JasperReport)JRLoader.loadObject(jasperURL);  

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			// xls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,  Boolean.TRUE);  
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			//          mensagem ="{\"Result\":\"OK\"}";       
			//			response.getWriter().print(mensagem);
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}
	
	public static void gerarRelatorioProdutividadeCERECxls(HttpServletResponse response, String datIni, String datFim, String nome, String setGrup) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}
			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
			{
				parametros.put("NOMFUN", nome);
			}

			if (!NeoUtils.safeIsNull(setGrup))
			{
				parametros.put("GRUPO", setGrup);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relProdutividadeCEREC");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relProdutividadeCEREC.xls\"");

			//			JasperReport relatoriosJasper = (JasperReport)JRLoader.loadObject(jasperURL);  

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			// xls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,  Boolean.TRUE);  
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			//          mensagem ="{\"Result\":\"OK\"}";       
			//			response.getWriter().print(mensagem);
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static void gerarRelatorioProdutividadeCERECpdf(HttpServletResponse response, String datIni, String datFim, String nome, String setGrup) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}
			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
			{
				parametros.put("NOMFUN", nome);
			}

			if (!NeoUtils.safeIsNull(setGrup))
			{
				parametros.put("GRUPO", setGrup);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relProdutividadeCEREC");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relProdutividadeCEREC.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioProdutividadeCERECOSpdf(HttpServletResponse response, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}
			/*
			 * if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
			 * {
			 * parametros.put("NOMFUN", nome);
			 * }
			 */

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relProdutividadeCERECOS");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relProdutividadeCERECOS.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioProdutividadeCERECOSxls(HttpServletResponse response, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}
			/*
			 * if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
			 * {
			 * parametros.put("NOMFUN", nome);
			 * }
			 */

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relProdutividadeCERECOS");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relProdutividadeCERECOS.xls\"");

			//			JasperReport relatoriosJasper = (JasperReport)JRLoader.loadObject(jasperURL);  

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			// xls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,  Boolean.TRUE);  
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			//          mensagem ="{\"Result\":\"OK\"}";       
			//			response.getWriter().print(mensagem);
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static void gerarRelatorioSuperintendenciaPDF(HttpServletResponse response, int competencia) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();

		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(competencia))
			{
				parametros.put("COMPETENCIA", competencia);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relSuperintendencia");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relSuperintendencia.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatoriogerarRelatorioSuperintendenciaSxls(HttpServletResponse response, int competencia) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(competencia))
			{
				parametros.put("COMPETENCIA", competencia);
			}

			connection = (Connection) PersistEngine.getConnection("SAPIENS");

			InputStream reportMain = ReportUtils.getReportJasper("relSuperintendencia");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relSuperintendencia.xls\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static void gerarRelatorioCobrancaEletronicaInadinpmenciaCpdf(HttpServletResponse response, String datIni, String datFim, String relatorio) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni;
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim;
				parametros.put("DATFIM", datFim);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			if (relatorio.equals("1234P"))
			{
				InputStream reportMain = ReportUtils.getReportJasper("rel1234LigacaoPagou");
				response.setContentType("application/x-download");
				response.setHeader("Content-Disposition", "attachment; filename=\"rel1234LigacaoPagou.pdf\"");
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

				// Gera um pdf
				byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
				servletOutputStream.write(byteArray);
				servletOutputStream.flush();
			}
			else
			{
				if (relatorio.equals("123GuedesSimNao"))
				{
					InputStream reportMain = ReportUtils.getReportJasper("rel123LigacaoGuedesCobrouSimNao");
					response.setContentType("application/x-download");
					response.setHeader("Content-Disposition", "attachment; filename=\"rel123LigacaoGuerdesCobrou.pdf\"");
					JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

					// Gera um pdf
					byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
					servletOutputStream.write(byteArray);
					servletOutputStream.flush();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioCobrancaEletronicaInadinpmenciaxls(HttpServletResponse response, String datIni, String datFim, String relatorio) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(relatorio))
			{
				parametros.put("setRelCobEletr", relatorio);
			}

			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");
			JRXlsExporter xls = new JRXlsExporter();

			if (relatorio.equals("1234P"))
			{
				InputStream reportMain = ReportUtils.getReportJasper("rel1234LigacaoPagou");
				response.setContentType("application/x-download");
				response.setHeader("Content-Disposition", "attachment; filename=\"rel1234LigacaoPagou.xls\"");
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

				xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);

			}
			else
			{
				if (relatorio.equals("123GuedesSimNao"))
				{
					InputStream reportMain = ReportUtils.getReportJasper("rel123LigacaoGuedesCobrouSimNao");
					response.setContentType("application/x-download");
					response.setHeader("Content-Disposition", "attachment; filename=\"rel123LigacaoGuedesCobrouSimNao.xls\"");
					JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

					xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
				}
			}

			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{
			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioDebitoAutomaticopdf(HttpServletResponse response, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni;
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim;
				parametros.put("DATFIM", datFim);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relDebitoAutomatico");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relDebitoAutomatico.pdf\"");
			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioDebitoAutomaticoxls(HttpServletResponse response, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");
			JRXlsExporter xls = new JRXlsExporter();

			InputStream reportMain = ReportUtils.getReportJasper("relDebitoAutomatico");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relDebitoAutomatico.xls\"");
			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static File gerarRelatorioEventosFiscalSupervisor() throws Exception
	{
		InputStream is = null;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		String path = "";
		Connection connection = null;
		try
		{

			connection = (Connection) PersistEngine.getConnection("SIGMA90");

			InputStream reportMain = ReportUtils.getReportJasper("relFiscalEventos");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, paramMap, connection);

			if (paramMap != null)
			{

				File file = File.createTempFile("fusexp", ".pdf");
				file.deleteOnExit();

				if (jasperPrint != null && file != null)
				{
					JasperExportManager.exportReportToPdfFile(jasperPrint, file.getAbsolutePath());
					connection.close();
					return file;
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());

		}

		connection.close();
		return null;
	}

	public static void gerarRelatorioNegociacaoCERECpdf(HttpServletResponse response, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relNegociacaoCEREC");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relNegociacaoCEREC.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioNegociacaoCERECxls(HttpServletResponse response, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		String mensagem = "";
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relNegociacaoCEREC");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relNegociacaoCEREC.xls\"");

			//			JasperReport relatoriosJasper = (JasperReport)JRLoader.loadObject(jasperURL);  

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			// xls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,  Boolean.TRUE);  
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			//          mensagem ="{\"Result\":\"OK\"}";       
			//			response.getWriter().print(mensagem);
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static void gerarRelatorioCobrancaHumanaInadinpmenciaCpdf(HttpServletResponse response, String datIni, String datFim, String relatorio) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni;
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim;
				parametros.put("DATFIM", datFim);
			}
			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");
			if (relatorio.equals("1234P"))
			{
				InputStream reportMain = ReportUtils.getReportJasper("rel1234LigacaoPagouHumana");
				response.setContentType("application/x-download");
				response.setHeader("Content-Disposition", "attachment; filename=\"rel1234LigacaoPagouHumana.pdf\"");
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

				// Gera um pdf
				byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
				servletOutputStream.write(byteArray);
				servletOutputStream.flush();
			}
			else
			{
				if (relatorio.equals("GC"))
				{
					InputStream reportMain = ReportUtils.getReportJasper("relGerenteCobrouHumana");
					response.setContentType("application/x-download");
					response.setHeader("Content-Disposition", "attachment; filename=\"relGerenteCobrouHumana.pdf\"");
					JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

					// Gera um pdf
					byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
					servletOutputStream.write(byteArray);
					servletOutputStream.flush();
				}
				else
				{
					if (relatorio.equals("abriuTarefaCancelamento"))
					{
						InputStream reportMain = ReportUtils.getReportJasper("relSemCobrancaHumanaAbriuC029");
						response.setContentType("application/x-download");
						response.setHeader("Content-Disposition", "attachment; filename=\"relSemCobrancaHumanaAbriuC029.pdf\"");
						JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

						// Gera um pdf
						byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
						servletOutputStream.write(byteArray);
						servletOutputStream.flush();
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioCobrancaHumanaInadinpmenciaxls(HttpServletResponse response, String datIni, String datFim, String relatorio) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(relatorio))
			{
				parametros.put("setRelCobEletr", relatorio);
			}

			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}
			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");
			JRXlsExporter xls = new JRXlsExporter();

			if (relatorio.equals("1234P"))
			{
				InputStream reportMain = ReportUtils.getReportJasper("rel1234LigacaoPagouHumana");
				response.setContentType("application/x-download");
				response.setHeader("Content-Disposition", "attachment; filename=\"rel1234LigacaoPagouHumana.xls\"");
				JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

				xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);

			}
			else
			{
				if (relatorio.equals("GC"))
				{
					InputStream reportMain = ReportUtils.getReportJasper("relGerenteCobrouHumana");
					response.setContentType("application/x-download");
					response.setHeader("Content-Disposition", "attachment; filename=\"relGerenteCobrouHumana.xls\"");
					JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

					xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
				}
				else
				{
					if (relatorio.equals("abriuTarefaCancelamento"))
					{
						InputStream reportMain = ReportUtils.getReportJasper("relSemCobrancaHumanaAbriuC029");
						response.setContentType("application/x-download");
						response.setHeader("Content-Disposition", "attachment; filename=\"relSemCobrancaHumanaAbriuC029.xls\"");
						JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

						xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
					}
				}
			}

			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{
			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioChipGprsPdf(HttpServletResponse response, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relChipGprs");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relChipGprs.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioChipGprsXls(HttpServletResponse response, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("relChipGprs");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relChipGprs.xls\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static File gerarRelatorioComunicadoDeFerias(String usuCodMot, String usuDatAlt, String usuNumCad, String usuTipCol) throws Exception
	{

		Connection connection = null;
		HashMap<String, Object> parametros = new HashMap<String, Object>();

		InputStream myInputStream = null;
		try
		{

			if (!NeoUtils.safeIsNull(usuCodMot))
				parametros.put("USU_NUMEMP", usuCodMot);
			if (!NeoUtils.safeIsNull(usuNumCad))
				parametros.put("USU_NUMCAD", usuNumCad);
			if (!NeoUtils.safeIsNull(usuTipCol))
				parametros.put("USU_TIPCOL", usuTipCol);
			if (!NeoUtils.safeIsNull(usuDatAlt))
				parametros.put("USU_DATALT", usuDatAlt);

			byte[] foto = buscaFoto(usuCodMot, usuTipCol, usuNumCad);

			if (foto != null)
			{
				myInputStream = new ByteArrayInputStream(buscaFoto(usuCodMot, usuTipCol, usuNumCad));
			}

			if (!NeoUtils.safeIsNull(myInputStream))
				parametros.put("USU_FOTCOL", myInputStream);

			connection = (Connection) PersistEngine.getConnection("VETORH");

			InputStream reportMain = ReportUtils.getReportJasper("RelCoberturaFerias");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			if (parametros != null)
			{
				File file = File.createTempFile("fusexp", ".pdf");
				file.deleteOnExit();

				if (jasperPrint != null && file != null)
				{
					JasperExportManager.exportReportToPdfFile(jasperPrint, file.getAbsolutePath());
					connection.close();
					return file;
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}

		connection.close();
		return null;
	}

	public static void gerarRelatorioSapXSigPdf(HttpServletResponse response, Integer tipemc) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (tipemc == 1)
				parametros.put("tipemc", "1");
			else
				parametros.put("tipemc", "2");

			connection = (Connection) PersistEngine.getConnection("SAPIENS");

			InputStream reportMain = ReportUtils.getReportJasper("relConSapXSig");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relConSapXSig.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioMedicaoContratosPdf(HttpServletResponse response, String periodo,String sql, String isComercial) throws Exception
	{
		try
		{
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			Connection connection = null;
			connection = (Connection) PersistEngine.getConnection("");
			
			parametros.put("numctr", sql);
			InputStream reportMain = null;
			
			if(isComercial.equals("SIM")){
				 reportMain = ReportUtils.getReportJasper("relMedicaoDeContratosComercial");
			}else{
				 reportMain = ReportUtils.getReportJasper("relMedicaoDeContratos");	
			}
			

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relMedicaoDeContratos.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);
			// Gera um pdf
			ServletOutputStream servletOutputStream = response.getOutputStream();
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}

	}
	
	public static void gerarRelatorioMedicaoContratosDiarioPdf(HttpServletResponse response, String periodo,String sql) throws Exception
	{
		try
		{
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			Connection connection = null;
			connection = (Connection) PersistEngine.getConnection("");
			
			parametros.put("sql", sql);

			InputStream reportMain = ReportUtils.getReportJasper("relMedicaoContratosDiario");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relMedicaoContratosDiario.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);
			// Gera um pdf
			ServletOutputStream servletOutputStream = response.getOutputStream();
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}

	}

	public static void gerarRelatorioSapXSigXls(HttpServletResponse response, Integer tipemc) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (tipemc == 1)
				parametros.put("tipemc", "1");
			else
				parametros.put("tipemc", "2");

			connection = (Connection) PersistEngine.getConnection("SAPIENS");

			InputStream reportMain = ReportUtils.getReportJasper("relConSapXSig");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relConSapXSig.xls\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static void gerarRelatorioSigXSapPdf(HttpServletResponse response) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			connection = (Connection) PersistEngine.getConnection("SIGMA");

			InputStream reportMain = ReportUtils.getReportJasper("relConSigXSap");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"relConSigXSap.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}

	public static void gerarRelatorioSigXSapXls(HttpServletResponse response) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			connection = (Connection) PersistEngine.getConnection("SIGMA90");

			InputStream reportMain = ReportUtils.getReportJasper("relConSigXSap");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"relConSigXSap.xls\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
			//			mensagem ="{\"Result\":\"ERROR\",\"Message\":"+e.getMessage().toString()+"}";
			//			response.getWriter().print(mensagem);
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}

	public static void gerarRelatorioChp1_1(HttpServletResponse response) throws Exception {
	    
	    ServletOutputStream servletOutputStream = response.getOutputStream();
	    HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
				parametros.put("USU_LOG", PortalUtil.getCurrentUser().getFullName());

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("ChipsCorrigirClientesComRegional");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"ChipsCorrigirClientesComRegional.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf

			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			System.out.println(byteArray);
			servletOutputStream.write(byteArray);

			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());

		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	    
	}
	
	public static void gerarRelatorioChp1_2(HttpServletResponse response) throws Exception {
	    
	    ServletOutputStream servletOutputStream = response.getOutputStream();
	    HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
				parametros.put("USU_LOG", PortalUtil.getCurrentUser().getFullName());

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("ChipsRemovidos");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"ChipsRemovidos.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf

			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			System.out.println(byteArray);
			servletOutputStream.write(byteArray);

			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());

		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	    
	}

	public static void gerarRelatorioChp1_3(HttpServletResponse response) throws Exception {
	    
	    ServletOutputStream servletOutputStream = response.getOutputStream();
	    HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
				parametros.put("USU_LOG", PortalUtil.getCurrentUser().getFullName());

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("ChipsInativosEmClientes");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"ChipsInativosEmClientes.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf

			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			System.out.println(byteArray);
			servletOutputStream.write(byteArray);

			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());

		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	    
	}

	public static void gerarRelatorioChp1_4(HttpServletResponse response) throws Exception {
	    
	    ServletOutputStream servletOutputStream = response.getOutputStream();
	    HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{

			if (!NeoUtils.safeIsNull(PortalUtil.getCurrentUser()))
				parametros.put("USU_LOG", PortalUtil.getCurrentUser().getFullName());

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("ChipsXClienteContas");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"ChipsXClienteContas.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf

			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			System.out.println(byteArray);
			servletOutputStream.write(byteArray);

			//Libera a memória
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());

		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}

	}
	
	public static void gerarRelatorioTarefasDirecionadaspdf(HttpServletResponse response, String setor, String tipo, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}
			
			parametros.put("SETOR", setor == null?"": setor);

			parametros.put("TIPO", tipo == null?"": tipo);

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("G002Relatorio");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"G002Relatorio.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}
	
	public static void gerarRelatorioTarefasDirecionadasXls(HttpServletResponse response, String setor, String tipo, String datIni, String datFim) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATINI", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATFIM", datFim);
			}

			parametros.put("SETOR", setor == null?"": setor);

			parametros.put("TIPO", tipo == null?"": tipo);
			
			InputStream reportMain = ReportUtils.getReportJasper("G002Relatorio");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"G002Relatorio.xls\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);

			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}
	
	/* RELATÓRIO RSC */
	
	public static void gerarRelatorioRSCpdf(HttpServletResponse response, String solicitante, String categoria, String datIni, String datFim, String subcategoria) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATAINICIO", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATAFIM", datFim);
			}
			
			parametros.put("SOLICITANTE", solicitante == null?"": solicitante);

			parametros.put("CATEGORIA", categoria == null?"": categoria);
			
			if(NeoUtils.safeIsNotNull(categoria)) {
				if(categoria.equalsIgnoreCase("C027.001 - RSC - Categoria Diversos")) {
					parametros.put("SUBCATEGORIA", subcategoria == null?"": subcategoria);				
				}				
			}

			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			InputStream reportMain = ReportUtils.getReportJasper("RelatorioRSC");

			response.setContentType("application/x-download");

			response.setHeader("Content-Disposition", "attachment; filename=\"RelatorioRSC.pdf\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			// Gera um pdf
			byte[] byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
			servletOutputStream.write(byteArray);
			servletOutputStream.flush();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();
			connection.close();
		}
	}
	
	public static void gerarRelatorioRSCXls(HttpServletResponse response, String solicitante, String categoria, String datIni, String datFim, String subcategoria) throws Exception
	{
		ServletOutputStream servletOutputStream = response.getOutputStream();
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		Connection connection = null;
		try
		{
			connection = (Connection) PersistEngine.getConnection("FUSIONPROD");

			if (!NeoUtils.safeIsNull(datIni))
			{
				datIni = datIni + " 00:00:00:000";
				parametros.put("DATAINICIO", datIni);
			}
			if (!NeoUtils.safeIsNull(datFim))
			{
				datFim = datFim + " 23:59:59:999";
				parametros.put("DATAFIM", datFim);
			}

			parametros.put("SOLICITANTE", solicitante == null?"": solicitante);

			parametros.put("CATEGORIA", categoria == null?"": categoria);
			
			if(NeoUtils.safeIsNotNull(categoria)) {
				if(categoria.equalsIgnoreCase("C027.001 - RSC - Categoria Diversos")) {
					parametros.put("SUBCATEGORIA", subcategoria == null?"": subcategoria);				
				}			
			}
			
			InputStream reportMain = ReportUtils.getReportJasper("RelatorioRSC");
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=\"RelatorioRSC.xls\"");

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportMain, parametros, connection);

			JRXlsExporter xls = new JRXlsExporter();

			xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, servletOutputStream);
			xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
			xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

			xls.exportReport();
			servletOutputStream.flush();
			System.out.println(">>>>>>>>Exportou para XLS<<<<<<<<<<");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ::::>>>>>>  " + e.getMessage());
		}
		finally
		{

			servletOutputStream.close();

			connection.close();
		}
	}
}
