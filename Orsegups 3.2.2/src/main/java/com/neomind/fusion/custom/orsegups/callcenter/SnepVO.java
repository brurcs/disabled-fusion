package com.neomind.fusion.custom.orsegups.callcenter;

public class SnepVO
{
	private String externalNumber;
	
	private String extensionNumber;
	
	private String callerid;
	
	private String exten;

	@Deprecated
	public String getExternalNumber()
	{
		return externalNumber;
	}
	
	@Deprecated
	public void setExternalNumber(String externalNumber)
	{
		this.externalNumber = externalNumber;
	}

	@Deprecated
	public String getExtensionNumber()
	{
		return extensionNumber;
	}
	@Deprecated
	public void setExtensionNumber(String extensionNumber)
	{
		this.extensionNumber = extensionNumber;
	}

	public String getCallerid()
	{
		return callerid;
	}

	public void setCallerid(String callerid)
	{
		this.callerid = callerid;
	}

	public String getExten()
	{
		return exten;
	}

	public void setExten(String exten)
	{
		this.exten = exten;
	}
}
