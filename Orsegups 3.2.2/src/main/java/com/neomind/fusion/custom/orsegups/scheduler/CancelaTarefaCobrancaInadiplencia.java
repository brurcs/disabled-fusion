package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class CancelaTarefaCobrancaInadiplencia implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.CancelaTarefaCobrancaInadiplencia");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Cancela Tarefa Cobrança Inadiplencia - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			String textoObjeto = "";
			conn = PersistEngine.getConnection("SAPIENS");
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT wf.code, wf.neoId, tcr.CodCli, cli.NomCli FROM E301TCR AS tcr  ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_Producao.dbo.d_FCITituloSapiens AS ts ON tcr.CodEmp = ts.empresa AND tcr.CodFil = ts.filial AND tcr.NumTit = ts.numeroTitulo AND tcr.CodTpt = ts.tipoTitulo ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_Producao.dbo.D_FCICobrancaInadimplencia_listaTitulos AS lt ON lt.listaTitulos_neoId = ts.neoId ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_Producao.dbo.D_FCICobrancaInadimplencia AS hi ON hi.neoId = lt.D_FCICobrancaInadimplencia_neoId ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_Producao.dbo.wfprocess AS wf ON wf.neoId = hi.wfprocess_neoId ");
			sql.append(" INNER JOIN E085CLI AS cli ON cli.CodCli = tcr.CodCli ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_Producao.DBO.ProcessModel pm on pm.neoid = wf.model_neoid ");
			sql.append(" WHERE (wf.processState = 0 AND wf.saved = 1 AND pm.name = 'C026 - FCI - Cobrança Inadimplência') AND  ");
			sql.append(" (tcr.SitTit > 'AX' OR tcr.VctPro >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))) ");
			sql.append(" GROUP BY wf.code, wf.neoId, tcr.CodCli, cli.NomCli ");
			sql.append(" UNION ALL ");
			sql.append(" SELECT pro.code,pro.neoid, cli.CodCli, cli.NomCli FROM [CACUPE\\SQL02].fusion_Producao.dbo.WFProcess pro  ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_Producao.dbo.D_FCICobrancaInadimplencia fci ON pro.neoId = fci.wfprocess_neoId ");
			sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_Producao.dbo.X_VFUSCTRSAP sap on sap.neoId = fci.contratoSapiens_neoId ");
			sql.append(" INNER JOIN TIDB.dbo.vfus_ctrcce ctr ON ctr.pk = sap.pk ");
			sql.append(" INNER JOIN E085CLI cli ON cli.CodCli = ctr.CodCli ");
			sql.append(" WHERE processState = 0 ");
			sql.append(" AND pro.saved = 1 ");
			sql.append(" AND (    ");
			sql.append(" 	NOT EXISTS(SELECT * FROM [CACUPE\\SQL02].fusion_Producao.dbo.D_FCICobrancaInadimplencia_listaTitulos tit where tit.D_FCICobrancaInadimplencia_neoId = fci.neoid)    ");
			sql.append(" 	AND ");
			sql.append(" 	NOT EXISTS(SELECT * FROM [CACUPE\\SQL02].fusion_Producao.dbo.D_FCICobrancaInadimplencia_listaDemaisTitulos dem where dem.D_FCICobrancaInadimplencia_neoId = fci.neoid) ");
			sql.append(" )  ");

			st = conn.prepareStatement(sql.toString());
			rs = st.executeQuery();

			while (rs.next())	
			{
				String codigoTarefa = rs.getString("code");
				Long codigoProcesso = rs.getLong("neoId");
				int codigoCliente = rs.getInt("CodCli");
				String nomeCliente = rs.getString("NomCli");
				String motivoCancelamento = URLEncoder.encode("Regra Automática: Tarefa cancelada via processo automático do Sistema Fusion.");
				String urlTarefa = "";
				urlTarefa = "http://localhost/fusion/custom/jsp/orsegups/cancelProcess.jsp?motivoCancelamento=" + motivoCancelamento + "&codigoProcesso=" + codigoProcesso;

				URL url = new URL(urlTarefa);
				URLConnection uc = url.openConnection();
				BufferedReader leitorArquivo = new BufferedReader(new InputStreamReader(uc.getInputStream()));
				log.warn("##### Tarefa cancelada do Processo C026 " + codigoTarefa);
				textoObjeto += "<p><strong>Tarefa cancelada: </strong> " + codigoTarefa + "- Cliente: " + codigoCliente + " - " + nomeCliente + "</p>";
			}
			if (!textoObjeto.equals(""))
			{
				//enviarEmail(textoObjeto);
			}
			log.warn("##### FIM AGENDADOR DE TAREFA: Cancela Tarefa Cobrança Inadiplencia - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{
			log.error("##### Erro ao cancelar tarefa de cobrança Inadimplência");
			System.out.println("[" + key + "] ##### Erro ao cancelar tarefa de cobrança Inadimplência");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}
	}

	public void enviarEmail(String textoObjeto)
	{
		try
		{
			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());
			mailClone.setFromEMail("emailautomatico@orsegups.com.br");//da onde esta vindo
			mailClone.setFromName("Orsegups Partipações S.A.");

			HtmlEmail noUserEmail = new HtmlEmail();
			StringBuilder noUserMsg = new StringBuilder();

			String subject = "[Cancelamento de Tarefas - Cobrança Eletrônica Inadimplência] Resumo do dia " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy");

			noUserEmail.setCharset("ISO-8859-1");

			//fernanda.maciel@orsegups.com.br;rafael.santos@orsegups.com.br;ivanya.silva@orsegups.com.br;valtair.souza@orsegups.com.br;camila.silva@orsegups.com.br
			noUserEmail.addTo("fernanda.maciel@orsegups.com.br");
			noUserEmail.addTo("ivanya.silva@orsegups.com.br");
			noUserEmail.addTo("camila.silva@orsegups.com.br");
			noUserEmail.addCc("emailautomatico@orsegups.com.br");//para quem vai com copia

			noUserEmail.setSubject(subject);
			noUserMsg.append("<!doctype html>");
			noUserMsg.append("<html>");
			noUserMsg.append("<head>");
			noUserMsg.append("<meta charset=\"utf-8\">");
			noUserMsg.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">Este é um e-mail automático do Sistema Fusion e não deve ser respondido.<br><br>");
			noUserMsg.append("</head>");
			noUserMsg.append("<body>");
			noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
			noUserMsg.append("          <tbody>");
			noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
			noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
			noUserMsg.append("            </td>");
			noUserMsg.append("			<td>");
			noUserMsg.append(textoObjeto);
			noUserMsg.append("			</td>");
			noUserMsg.append("     </tbody></table>");
			noUserMsg.append("</body></html>");
			noUserEmail.setHtmlMsg(noUserMsg.toString());
			noUserEmail.setContent(noUserMsg.toString(), "text/html");
			mailClone.applyConfig(noUserEmail);

			noUserEmail.send();
			//OrsegupsUtils.sendEmail2SendGrid(addTo, from, subject, "", noUserMsg.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
