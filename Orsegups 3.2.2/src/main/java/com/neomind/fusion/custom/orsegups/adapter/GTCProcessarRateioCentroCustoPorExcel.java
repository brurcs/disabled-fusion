package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class GTCProcessarRateioCentroCustoPorExcel implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "";

		try
		{
			Long codigoempresa = (Long) processEntity.findValue("empresa.codemp");

			List<NeoObject> listCentroCusto = new ArrayList<NeoObject>();
			List<String> listCentroCustoInvalidos = new ArrayList<String>();
			List<NeoObject> externoCentroCusto = new ArrayList<NeoObject>();
			NeoFile anexo = (NeoFile) processEntity.findValue("anexoCentroCusto");

			/* Obrigatoriamente o arquivo excel para ser importado tem que ser salvo no formato .XLS */
			Workbook wbRateios = Workbook.getWorkbook(anexo.getAsFile());
			Sheet rateios = wbRateios.getSheet(0);

			int liTroca = rateios.getRows();
			for (int i = 0; i < liTroca; i++)
			{
				if (i != 0)
				{
					Cell empresa = rateios.getCell(0, i);
					Cell centroCusto = rateios.getCell(1, i);
					Cell valorRateio = rateios.getCell(2, i);

					if (empresa.getContents().equals(codigoempresa.toString()))
					{
						NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("GTCListaCentroCusto");
						EntityWrapper centralWrapper = new EntityWrapper(noCentroCusto);

						QLGroupFilter filterCcu = new QLGroupFilter("AND");
						filterCcu.addFilter(new QLEqualsFilter("codccu", centroCusto.getContents()));
						filterCcu.addFilter(new QLEqualsFilter("codemp", Long.parseLong(empresa.getContents())));
						filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));
						filterCcu.addFilter(new QLEqualsFilter("acerat", "S"));

						externoCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

						if (externoCentroCusto != null && !externoCentroCusto.isEmpty())
						{
							BigDecimal valorRateio1 = new BigDecimal(valorRateio.getContents().replace(",", "."));

							centralWrapper.setValue("codigoCentroCusto", externoCentroCusto.get(0));
							centralWrapper.setValue("valorRateio", valorRateio1);

							/* valorTotalRateio = valorTotalRateio.add(valorRateio); */
							listCentroCusto.add(noCentroCusto);
						}
						else
						{

							listCentroCustoInvalidos.add(centroCusto.getContents());
						}
					}
					else
					{
						mensagem = "A Empresa utilizada no Rateio do Excel, não confere com a Empresa de lançamento do Título! Por favor, verifique o Excel!";
						throw new WorkflowException(mensagem);
					}
				}
			}
			if (listCentroCustoInvalidos.isEmpty())
			{
				processEntity.setValue("listaCentroCusto", listCentroCusto);
			}
			else
			{
				mensagem = "Centro de Custo(s) não localizado(s) ou não aceitam rateio! Por favor, verifique se o(s) Centro de Custo " + listCentroCustoInvalidos + " existem na Empresa de lançamento do Título ou não aceitam rateio!";
				mensagem = mensagem.replaceAll("\\[|\\]|", "");
				throw new WorkflowException(mensagem);
			}

		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
