package com.neomind.fusion.custom.orsegups.adapter.rhAdmissaoUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsRelatoriosUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class admissaoUtils {

    //    public void jfgtjrtyj{
    //
    //	try{
    //	    GregorianCalendar dataInicial = new GregorianCalendar();
    //	    dataInicial.add(Calendar.HOUR_OF_DAY, -4);
    //	    GregorianCalendar dataFinal = new GregorianCalendar();
    //
    //	    //buscar lista de destinatários por regional
    //	    List<NeoObject> listRegionais = PersistEngine.getObjects(AdapterUtils.getEntityClass("GCEscritorioRegional"));
    //
    //	    for (NeoObject regional : listRegionais){
    //		EntityWrapper ewRegional = new EntityWrapper(regional);
    //		List<NeoPaper> listaPapeis = (List<NeoPaper>) ewRegional.findField("papelResponsaveis").getValue();
    //		String siglaRegional = (String) ewRegional.findField("siglaRegional").getValue();
    //
    //
    //		ArrayList<String> destinatarios = new ArrayList<>();
    //
    //		destinatarios.add("rodrigo.fontoura@orsegups.com.br"); 
    //		destinatarios.add("suelen.machado@orsegups.com.br"); 
    //		destinatarios.add("dirceu.costa@orsegups.com.br"); 
    //		destinatarios.add("suelen.machado@orsegups.com.br"); 
    //
    //
    //		for (NeoPaper papel : listaPapeis)
    //		{
    //		    EntityWrapper ewPapel = new EntityWrapper(papel);
    //		    Set<NeoUser> listaUsuarios = (Set<NeoUser>) ewPapel.findField("users").getValue();
    //		    for (NeoUser usuario : listaUsuarios)
    //		    {
    //			EntityWrapper ewUsuario = new EntityWrapper(usuario);
    //			String email = (String) ewUsuario.findField("email").getValue();
    //			if (email != null && !email.isEmpty() && usuario.isActive() && !destinatarios.contains(email))
    //			{
    //			    destinatarios.add(email);
    //			}
    //		    }
    //		}
    //		if (!destinatarios.isEmpty()){
    //
    //		    File arqRelatorio = OrsegupsRelatoriosUtils.getRelatorioArrombamentos(siglaRegional, dataInicial, dataFinal, null, true);
    //		    if (arqRelatorio != null){
    //
    //			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    //
    //			String datIni = df.format(dataInicial.getTime());
    //			String datFim = df.format(dataFinal.getTime());
    //
    //			StringBuilder html = new StringBuilder();
    //
    //			String portal = PortalUtil.getBaseURL();
    //
    //			html.append(" <!DOCTYPE html> ");
    //			html.append(" <html> ");
    //			html.append(" <head> ");
    //			html.append(" <title></title> ");
    //			html.append("  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=\"iso-8859-1\" /> ");
    //			html.append(" <link href=\""+portal+"/css/portal-decorator.css\" rel=\"stylesheet\" type=\"text/css\" /> ");
    //			html.append(" <style TYPE=\"text/css\"> "); 
    //			html.append(" p,pre,body,table ");   
    //			html.append(" { ");
    //			html.append(" font-size:10.0pt; ");
    //			html.append(" font-family:'Arial'; ");
    //			html.append(" } ");
    //			html.append(" </style> ");
    //			html.append(" </head> ");
    //			html.append(" <body bgcolor=\"#FFFFFF\"> ");
    //			html.append(" <table id=\"Table_01\" width=\"550\" border=\"0\" cellpadding=\"0\" ");
    //			html.append(" cellspacing=\"0\"> ");
    //			html.append(" <tr> ");
    //			html.append(" <td class=\"corpo_mail\"> ");
    //			html.append(" <p class=\"mail_tit\"><mail:title title='Relatório de Arrombamentos' /></p> ");
    //			html.append(" </td> ");
    //			html.append(" </tr>	");
    //			html.append(" <tr> ");
    //			html.append(" <td class=\"corpo_mail\" align=\"left\"> ");
    //			html.append(" <p> Em anexo segue o relatório de arrombamentos do período de <strong>"+datIni+"</strong> até <strong>"+datFim+"</strong>.</p> ");
    //			html.append(" <br>  ");
    //
    //			html.append(" Orsegups Participações S/A<br/> ");
    //			html.append(" <a href=\"http://www.orsegups.com.br\">http://www.orsegups.com.br</a> "); 
    //
    //			html.append(" </td> "); 
    //			html.append(" </tr> ");
    //
    //			html.append(" </table> ");
    //			html.append(" </body> ");
    //			html.append(" </html> ");
    //
    //
    //			/**
    //			 * Novo Envio
    //			 */
    //
    //
    //			EmailAttachment attachment = new EmailAttachment();
    //			attachment.setPath(arqRelatorio.getPath());
    //			attachment.setDisposition(EmailAttachment.ATTACHMENT);
    //			attachment.setName("Relatorio_Arrombamentos_" + siglaRegional+".pdf");
    //
    //			MultiPartEmail email = new HtmlEmail();
    //			email.setHostName("SSOOMX01");
    //			email.setSmtpPort(25);
    //
    //			try{
    //
    //			    for (String to : destinatarios) {
    //				email.addTo(to);
    //			    }
    //
    //			    email.addCc("emailautomatico@orsegups.com.br");
    //
    //			    email.setFrom("fusion@orsegups.com.br");
    //			    email.setSubject("Relatório de Arrombamentos");
    //			    email.setMsg(html.toString());
    //			    email.attach(attachment);
    //
    //			    email.send();
    //
    //			}
    //			catch (EmailException e){
    //			    e.printStackTrace();
    //			}
    //
    //			/**
    //			 * Fim novo envio
    //			 */
    //		    }
    //		}else{
    //		}
    //	    }
    //	}
    //	catch (Exception ex){
    //	    Long key = GregorianCalendar.getInstance().getTimeInMillis();
    //	    ex.printStackTrace();
    //	    throw new JobException("Erro na rotina, procurar no log por ["+key+"]");
    //	}
    //
    //    }


    public static Boolean addHistoricoAdmissao(String codeProcesso, String solicitante, String sDataAdmissao, String nomeCompleto,
	    String nomeCracha, String sDataNascimento, String cpf, String pis, String rg, String regional, String empresa, String tipoAdmissao, 
	    String filial, String departamento, String cargo, String escala, String modPagamento, String banco, String agencia, String conta, 
	    String digito, String tipoSalario, String salario, String matricula, String estado, String cidade, String bairro, String rua, 
	    String logradouro, String endnum, String complemento, String telefone01, String telefone02, String acao, String observacao){

	try{

	    NeoObject oHistoricoAdmissao = AdapterUtils.createNewEntityInstance("rhHistoricoAdmissao");

	    EntityWrapper wHistoricoAdmissao = new EntityWrapper(oHistoricoAdmissao);

	    wHistoricoAdmissao.findField("codeProcesso").setValue(codeProcesso);
	    wHistoricoAdmissao.findField("solicitante").setValue(solicitante);
	    wHistoricoAdmissao.findField("sDataAdmissao").setValue(sDataAdmissao);
	    wHistoricoAdmissao.findField("nomeCompleto").setValue(nomeCompleto);
	    wHistoricoAdmissao.findField("nomeCracha").setValue(nomeCracha);
	    wHistoricoAdmissao.findField("sDataNascimento").setValue(sDataNascimento);
	    wHistoricoAdmissao.findField("cpf").setValue(cpf);
	    wHistoricoAdmissao.findField("pis").setValue(pis);
	    wHistoricoAdmissao.findField("rg").setValue(rg);
	    wHistoricoAdmissao.findField("regional").setValue(regional);
	    wHistoricoAdmissao.findField("empresa").setValue(empresa);
	    wHistoricoAdmissao.findField("tipoAdmissao").setValue(tipoAdmissao);
	    wHistoricoAdmissao.findField("filial").setValue(filial);
	    wHistoricoAdmissao.findField("departamento").setValue(departamento);
	    wHistoricoAdmissao.findField("cargo").setValue(cargo);
	    wHistoricoAdmissao.findField("escala").setValue(escala);
	    wHistoricoAdmissao.findField("modPagamento").setValue(modPagamento);
	    wHistoricoAdmissao.findField("banco").setValue(banco);
	    wHistoricoAdmissao.findField("agencia").setValue(agencia);
	    wHistoricoAdmissao.findField("conta").setValue(conta);
	    wHistoricoAdmissao.findField("digito").setValue(digito);
	    wHistoricoAdmissao.findField("tipoSalario").setValue(tipoSalario);
	    wHistoricoAdmissao.findField("salario").setValue(salario);
	    wHistoricoAdmissao.findField("matricula").setValue(matricula);
	    wHistoricoAdmissao.findField("estado").setValue(estado);
	    wHistoricoAdmissao.findField("cidade").setValue(cidade);
	    wHistoricoAdmissao.findField("bairro").setValue(bairro);
	    wHistoricoAdmissao.findField("rua").setValue(rua);
	    wHistoricoAdmissao.findField("logradouro").setValue(logradouro);
	    wHistoricoAdmissao.findField("endnum").setValue(endnum);
	    wHistoricoAdmissao.findField("complemento").setValue(complemento);
	    wHistoricoAdmissao.findField("telefone01").setValue(telefone01);
	    wHistoricoAdmissao.findField("telefone02").setValue(telefone02);
	    wHistoricoAdmissao.findField("acao").setValue(acao);
	    wHistoricoAdmissao.findField("observacao").setValue(observacao);


	    PersistEngine.persist(oHistoricoAdmissao);

	    return Boolean.TRUE;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    throw (WorkflowException) e;
	}



    }
}