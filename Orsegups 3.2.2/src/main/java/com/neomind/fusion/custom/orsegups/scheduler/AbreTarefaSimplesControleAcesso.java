package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaSimplesControleAcesso implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext ctx)
	{
		if (OrsegupsUtils.isWorkDay(new GregorianCalendar()))
		{
			System.out.println("Iniciando job Cobrança de Eventos Controle Acesso");
			processaJob(ctx);
		}
	}

	public static void processaJob(CustomJobContext ctx)
	{
		Connection connSigma = null;
		ResultSet rsEventosKiper = null;
		PreparedStatement stEventosKiper = null;
		String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteCobrancaDeslocamentoOS");
		GregorianCalendar abertura = new GregorianCalendar();
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		String executor = "";
		int quantidadeDiasVerificados = 1;
		
		if(abertura.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.MONDAY) {
		    quantidadeDiasVerificados = 2;
		}
		
		NeoPaper papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","responsavelTarefasControleAcesso"));
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				executor = user.getCode();
				break;
			}
		}
		
		try
		{
			connSigma = PersistEngine.getConnection("SIGMA90");
			
			//Montando Query principal do job.
			StringBuffer queryEventosKiper = new StringBuffer();
			queryEventosKiper.append(" SELECT C.ID_CENTRAL,C.PARTICAO,C.RAZAO,HE.CD_EVENTO,HFE.NM_FRASE_EVENTO,HE.DT_RECEBIDO,HE.DT_FECHAMENTO,HE.TX_OBSERVACAO_FECHAMENTO,"
					+ "HE.CD_CLIENTE, HE.CD_HISTORICO FROM VIEW_HISTORICO HE ");
			queryEventosKiper.append("	INNER JOIN HISTORICO_FRASE_EVENTO HFE ON HFE.CD_FRASE_EVENTO = HE.CD_FRASE_EVENTO");
			queryEventosKiper.append("	INNER JOIN DBCENTRAL C ON C.CD_CLIENTE = HE.CD_CLIENTE");
			queryEventosKiper.append("	INNER JOIN VIEW_TIPO_PESSOA TP ON TP.TP_PESSOA = C.TP_PESSOA");
			queryEventosKiper.append("	WHERE TP.TP_PESSOA = 0 AND HE.DT_RECEBIDO >= GETDATE()-" + quantidadeDiasVerificados + "and CD_USUARIO_VIATURA_DESLOCAMENTO is not null AND DT_VIATURA_NO_LOCAL IS NOT NULL ");
			queryEventosKiper.append("  AND C.ID_EMPRESA IN (10120,10119) ");
			queryEventosKiper.append("  AND NOT EXISTS (SELECT 1 FROM [CACUPE\\SQL02].fusion_producao.DBO.D_tarefasCobrancaControleAcesso tarefa WHERE tarefa.cdHistorico = he.CD_HISTORICO");
			queryEventosKiper.append("  AND tarefa.cdCliente = he.CD_CLIENTE)");
            
			
			//Conexão com o banco do sigma.
			stEventosKiper = connSigma.prepareStatement(queryEventosKiper.toString());
			
			//Executando a Query.
			rsEventosKiper = stEventosKiper.executeQuery();
			
			//Verificando se a consulta possui resultado.
			while (rsEventosKiper.next())
			{
				//Montando dados da tarefa simples.
				String titulo = "Cobrança de Deslocamento CA - "+rsEventosKiper.getString("ID_CENTRAL")+"["+rsEventosKiper.getString("PARTICAO")+"]"+" - "+rsEventosKiper.getString("RAZAO");
				String descricao ="";
				
				descricao += " <strong>	Informar se o deslocamento abaixo deve ser cobrando.</strong> <br>";
				descricao += " <strong>	<Dados do evento> </strong><br>";
				descricao += " <strong>	Conta: </strong> "+rsEventosKiper.getString("ID_CENTRAL")+"["+rsEventosKiper.getString("PARTICAO")+"]<br>";
				descricao += " <strong>	Cliente: </strong> "+rsEventosKiper.getString("RAZAO")+"<br>";
				descricao += " <strong>	Evento: </strong> "+rsEventosKiper.getString("CD_EVENTO")+" - "+rsEventosKiper.getString("NM_FRASE_EVENTO")+"<br>";
				descricao += " <strong>	Data Recebimento: </strong> "+NeoDateUtils.safeDateFormat(rsEventosKiper.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:SS")+"<br>";
				
				if(rsEventosKiper.getTimestamp("DT_FECHAMENTO") != null){
					descricao += " <strong>	Data Fechamento: </strong> "+NeoDateUtils.safeDateFormat(rsEventosKiper.getTimestamp("DT_FECHAMENTO"), "dd/MM/yyyy HH:mm:SS")+"<br>";
				}else{
					descricao += " <strong>	Data Fechamento: </strong> <br>";
				} 
				descricao += " <strong>	Observação: </strong> "+rsEventosKiper.getString("TX_OBSERVACAO_FECHAMENTO")+"<br>";
				
				//Iniciando a tarefa simples avançando a primeira atividade.
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
				//Salvando dados no eform para registrar as tarefas que foram abertas.
				InstantiableEntityInfo tarefaCK = AdapterUtils.getInstantiableEntityInfo("tarefasCobrancaControleAcesso");
				NeoObject trarefaCK = tarefaCK.createNewInstance();
				EntityWrapper ckWrapper = new EntityWrapper(trarefaCK);
				ckWrapper.setValue("solicitante",solicitante);
				ckWrapper.setValue("executor",executor);
				ckWrapper.setValue("tarefa",tarefa);
				ckWrapper.setValue("dataAbertura",abertura);
				ckWrapper.setValue("cdHistorico",rsEventosKiper.getLong("CD_HISTORICO"));
				ckWrapper.setValue("cdCliente",rsEventosKiper.getLong("CD_CLIENTE"));
				PersistEngine.persist(trarefaCK);

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSigma, stEventosKiper, rsEventosKiper);
		}
	}
}
