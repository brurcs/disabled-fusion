package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

@Path("/pagamentoFerias")
public class EnviarReciboFeriasService {

	@GET
	@Path("/recibo")
	@Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON + "; charset=UTF-8")
	public String getRecibo() {

		
		List<ConvocacaoFeriasDTO> dtos = new ArrayList<ConvocacaoFeriasDTO>();
		
		List<NeoObject> lista = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("r001V2ProgramacaoFerias"),new QLEqualsFilter("enviaConvocacaoNexti", true));

		if (lista != null && !lista.isEmpty()) {
			for (NeoObject obj : lista) {
				EntityWrapper wp = new EntityWrapper(obj);
				
				ConvocacaoFeriasDTO dto = new ConvocacaoFeriasDTO();
				
				NeoObject colaborador = (NeoObject) wp.getValue("primeiraSugestao");
				EntityWrapper wColaborador = new EntityWrapper(colaborador);				
				
				dto.setNeoId(Integer.parseInt(wp.findField("neoId").getValue().toString()));
				dto.setMatricula(Integer.parseInt(wColaborador.findField("numcad").getValue().toString()));
				dto.setEmpresa(Integer.parseInt(wColaborador.findField("numemp").getValue().toString()));
				dto.setName("Comunicado de Pgto Férias");
				dto.setText("Prezado(a) Colaborador(a), seu pagamento de férias foi efetuado, e o recibo detalhado já está disponível para consulta em nosso APP e Portal. Boas Férias!!!");
				
				NeoFile neo = new NeoFile();
				
				neo = (NeoFile) wp.findField("anexoReciboFerias").getValue();
				
				if (neo != null) {
					dto.setFile(neo.getBytes());	
					
					dtos.add(dto);
				}
			}
		}
		
		String json = new Gson().toJson(dtos);

		return json;

	}
	
	@GET
	@Path("/atualiza/{neoId}")
	public Response atualiza(@PathParam("neoId") int neoId) {
		
		try {
			
			NeoObject r001V2 = PersistEngine.getObject(AdapterUtils.getEntityClass("r001V2ProgramacaoFerias"), new QLEqualsFilter("neoId", neoId));
			EntityWrapper wp = new EntityWrapper(r001V2);
			wp.findField("enviaConvocacaoNexti").setValue(false);
			
			List<NeoObject> registroAtividades = (List<NeoObject>) wp.getValue("r001V2RegistroAtividade");		
			
			InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades"); 
			NeoObject objRegAti = insRegAti.createNewInstance();
			EntityWrapper wRegAti = new EntityWrapper(objRegAti);
			
			
			GregorianCalendar dataAcao = new GregorianCalendar();

			wRegAti.setValue("usuario", "Integração NEXTI");
			wRegAti.setValue("dataAcao", dataAcao);
			wRegAti.setValue("descricao", "Enviado Convocação e arquivo PDF para APP e Mobile NEXTI");
			wRegAti.setValue("anexo", null);
			
			PersistEngine.persist(objRegAti);
			registroAtividades.add(objRegAti);

			wp.setValue("r001V2RegistroAtividade", registroAtividades);
					
			return Response.ok().build();	
		} catch (Exception e) {
			return Response.status(200).entity("ERRO").header("Access-Control-Allow-Origin", "*").build();	
		}
	}
}
