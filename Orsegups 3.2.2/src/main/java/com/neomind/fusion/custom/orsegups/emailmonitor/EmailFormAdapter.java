package com.neomind.fusion.custom.orsegups.emailmonitor;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.emailmonitor.vo.EmailVO;

public interface EmailFormAdapter {
	
	public NeoObject getProcessForm(EmailVO emailVO) throws Exception;

}