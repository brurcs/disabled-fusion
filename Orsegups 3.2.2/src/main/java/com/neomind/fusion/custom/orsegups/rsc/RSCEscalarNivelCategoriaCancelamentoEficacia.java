package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RSCEscalarNivelCategoriaCancelamentoEficacia implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCEscalarNivelCategoriaCancelamentoEficacia.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String erro = "Por favor, contatar o administrador do sistema!";

		try
		{
			RSCUtils rscUtils = new RSCUtils();
			GregorianCalendar prazoDeadLine = new GregorianCalendar();
			
			NeoUser responsavelExecutor = (NeoUser) wrapper.findValue("responsavelExecutor");
			Boolean visitouCliente = (Boolean) wrapper.findValue("visitouCliente");

			if (origin != null)
			{
				if (visitouCliente)
				{
					NeoPaper papelSuperior = rscUtils.retornaSuperiorResponsavelExecutor(responsavelExecutor);
					wrapper.setValue("superiorResponsavelExecutor", rscUtils.retornaSuperiorResponsavelExecutor(responsavelExecutor));
					
					if(papelSuperior.getName().contains("Diretor") || papelSuperior.getName().contains("Presidente"))
					{
						wrapper.findField("souPessoaResponsavel").setValue(false);
						wrapper.findField("continuarRSC").setValue(true);
						if(papelSuperior.getName().contains("Diretor"))
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente - Escalada Diretoria", new GregorianCalendar());
							wrapper.findField("prazoVisitarClienteEscaladaDiretoria").setValue(prazoDeadLine);
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente - Escalada Presidência", new GregorianCalendar());
							wrapper.findField("prazoVisitarClienteEscaladaPresidencia").setValue(prazoDeadLine);
						}
					}
					else
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente - Escalada", new GregorianCalendar());
						wrapper.findField("prazoVisitarClienteEscalada").setValue(prazoDeadLine);
					}
				}
				else
				{
					NeoPaper papelSuperior = rscUtils.retornaSuperiorResponsavelExecutor(responsavelExecutor);
					wrapper.setValue("superiorResponsavelExecutorTentarReversao", papelSuperior);
					
					if(papelSuperior.getName().contains("Diretor") || papelSuperior.getName().contains("Presidente"))
					{
						wrapper.findField("souPessoaResponsavel").setValue(false);
						wrapper.findField("continuarRSC").setValue(true);
						if(papelSuperior.getName().contains("Diretor"))
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Tentar Reversão - Escalada Diretoria", new GregorianCalendar());
							wrapper.findField("prazoTentarReversaoEscaladaDiretoria").setValue(prazoDeadLine);
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Tentar Reversão - Escalada Presidência", new GregorianCalendar());
							wrapper.findField("prazoTentarReversaoEscaladaPresidencia").setValue(prazoDeadLine);
						}
					}
					else
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Tentar Reversão - Escalada", new GregorianCalendar());
						wrapper.findField("prazoTentarReversaoEscalada").setValue(prazoDeadLine);
					}
				}

				/*Pego meu último valor da minha lista de Registro de Atividades e altero o valor do campo prazo*/
				List<NeoObject> objLstRegistroAtividades = (List<NeoObject>) wrapper.findValue("RscRelatorioSolicitacaoCliente.registroAtividades");
				if (NeoUtils.safeIsNotNull(objLstRegistroAtividades))
				{
					NeoObject ultima = objLstRegistroAtividades.get(objLstRegistroAtividades.size() - 1);
					EntityWrapper ewUltima = new EntityWrapper(ultima);
					
					ewUltima.findField("prazo").setValue(prazoDeadLine);
				}
			}
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}
}
