package com.neomind.fusion.custom.orsegups.ti;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceActionType;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.TaskLog;
import com.neomind.fusion.workflow.TaskLog.TaskLogType;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.AssignmentException;
import com.neomind.fusion.workflow.handler.HandlerFactory;
import com.neomind.fusion.workflow.handler.TaskHandler;
import com.neomind.fusion.workflow.task.central.search.TaskCentralIndex;
import com.neomind.fusion.workflow.task.rule.TaskRuleEngine;
import com.neomind.util.NeoUtils;

@WebServlet(name="UtilidadesController", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.ti.UtilidadesController"})
public class UtilidadesController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public UtilidadesController() {
	super();
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	String action = request.getParameter("action");
	
	if (action.equalsIgnoreCase("transferirTarefa")) {
	    String codes = request.getParameter("txt_tarefas_transferir");
	    String model = request.getParameter("cmb_modelo_transferir");
	    String userDestinoCode = request.getParameter("cmb_usuario_destino");

	    String retornoTransferencia = transferirTarefa(codes, model, userDestinoCode, "Tarefa transferida via TI");

	    request.setAttribute("resposta", retornoTransferencia);
	}else if(action.equalsIgnoreCase("Utilidades")){
	    
	}else if(action.equalsIgnoreCase("getListaUsuarios")){
	    ArrayList<String> listaUsuarios = this.getListaUsuarios();
	    String json = new Gson().toJson(listaUsuarios);
	    
	    response.setContentType("application/json");
	    PrintWriter out = response.getWriter();
	    out.print(json);
	    out.flush();
	        
	}

    }

    public String transferirTarefa(String codes, String model, String userDestinoCode, String motivo) {

	String xCodes[] = codes.split(",");
	String strCodes = "'000000'";

	for (String x : xCodes) {
	    strCodes += ",'" + x + "'";
	}

	if (model == null) {
	    return "Você precisa selecionar o modelo de processo da tarefa";
	}
	String query = null;
	query = " select ti.id, (select code from dbo.SecurityEntity n where n.neoId = ti.owner_neoid) as actualUserCode, p.neoId as wfprocess from dbo.WFProcess p ";
	query += " inner join dbo.Activity a on a.process_neoId = p.neoid ";
	query += " inner join dbo.Task t on t.activity_neoId = a.neoId ";
	query += " inner join dbo.Task_deadLine td on td.Task_neoId = t.neoId ";
	query += " inner join dbo.DeadLine d on d.neoId = td.deadLine_neoId ";
	query += " inner join dbo.TaskInstance ti on ti.task_neoId = t.neoId ";
	query += " where p.code in ( " + strCodes + " ) ";
	query += " and p.model_neoId in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.neoId = " + model + " ) ) ";

	EntityManager entity = PersistEngine.getEntityManager();
	Integer cont = 1;
	if (entity != null) {
	    Query q = entity.createNativeQuery(query);
	    
	    @SuppressWarnings("unchecked")
	    List<Object[]> result = (List<Object[]>) q.getResultList();

	    for (Object i[] : result) {
		Long id = ((BigInteger) i[0]).longValue();
		String actualUserCode = NeoUtils.safeOutputString(i[1]);
		long wfprocess = ((BigInteger) i[2]).longValue();

		try {
		    // RuntimeEngine.getTaskService().delegate(Long.valueOf(id),
		    // actualUserCode, userDestinoCode, motivo);
		    String taskId = String.valueOf(id);
		    String userId = actualUserCode;
		    String targetUserId = userDestinoCode;
		    String reason = motivo;

		    if (NeoUtils.safeIsNotNull(taskId) && NeoUtils.safeIsNotNull(userId) && NeoUtils.safeIsNotNull(targetUserId) && NeoUtils.safeIsNotNull(reason)) {
			
			String g001 = "614599909";
			
			TaskInstance taskInstance = PersistEngine.getObject(TaskInstance.class, new QLEqualsFilter("id", Long.valueOf(taskId)));
			NeoUser user = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userId));
			NeoUser targetUser = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", targetUserId));
			
			
			if (model.equalsIgnoreCase(g001)) {
			    /* Pegar responsável do target */
			    
			    NeoGroup targetGroup = targetUser.getGroup();
			    
			    NeoPaper targetPaperResponsavel = targetGroup.getResponsible();

			    /* -- */

			    List<NeoObject> listaTarefa = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Tarefa"), new QLEqualsFilter("wfprocess.neoId", wfprocess));

			    NeoObject tarefa = listaTarefa.get(0);

			    EntityWrapper wrapperTarefa = new EntityWrapper(tarefa);

			    wrapperTarefa.findField("Executor").setValue(targetUser);
			    wrapperTarefa.findField("ResponsavelExecutor").setValue(targetPaperResponsavel);

			    PersistEngine.persist(tarefa);
			}
			
			delegate(taskInstance, user, targetUser, reason);
			System.out.println("[ORSETOOL] - debug " + Long.valueOf(id) + " - " + actualUserCode + " - " + userDestinoCode + " - " + motivo);
			System.out.println(cont + " : Processo transferido");
			
			
			

		    }

		} catch (Exception e) {
		    e.printStackTrace();

		    System.out.println("[ORSETOOL] - " + cont + " : ERRO: Processo não localizado!");
		}

		/*
		 * WFProcess proc = PersistEngine.getObject(WFProcess.class, new
		 * QLEqualsFilter("neoId", id));
		 * 
		 * if (proc != null) {
		 * RuntimeEngine.getProcessService().cancel(proc, motivo);
		 * System.out.println(cont + " : Processo " + proc.getCode()); }
		 * else { System.out.println(cont +
		 * " : ERRO: Processo não localizado!"); }
		 */
		cont++;
	    }
	    return "Transferencia Processada com Sucesso!";
	} else {
	    return "Nenhuma tarefa encontrada para transferencia";
	}
    }

    public void delegate(TaskInstance taskInstance, NeoUser user, NeoUser targetUser, String reason) {
	if (targetUser == null)
	    throw new AssignmentException("");

	TaskCentralIndex.getInstance().delete(taskInstance, true);

	taskInstance.getTask().getLogs().add(createLog(TaskLogType.TRANSFER, reason, user, taskInstance.getOwner()));

	TaskHandler taskHandler = HandlerFactory.TASK.get(taskInstance.getTask());
	Task t = taskHandler.transfer(user, targetUser);

	taskInstance.setOwner(t.getUser());

	// Remove a caixa
	RuntimeEngine.getTaskService().setBox(taskInstance, null);

	taskInstance.setActionType(TaskInstanceActionType.DELEGATED);

	// Remove os marcadores
	taskInstance.getTags().clear();

	// Executa regras
	this.refresh(taskInstance);

	taskInstance.setTask(t);
    }
    
    public void refresh(TaskInstance taskInstance)
    {
    	TaskInstanceHelper.update(taskInstance);
    	TaskRuleEngine.get().run(taskInstance);
    }
    
    
    private TaskLog createLog(TaskLogType type, String reason, NeoUser user, NeoUser owner)
    {
    	TaskLog log = new TaskLog();
    	log.setAction(type);
    	log.setDateLog(new GregorianCalendar());
    	log.setDescription(reason);
    	log.setUser(user);
    	log.setOwner(owner);

    	PersistEngine.persist(log);

    	return log;
    }
    
    private ArrayList<String> getListaUsuarios(){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	ArrayList<String> usuarios = new ArrayList<String>();
	
	try{
	    
	    conn = PersistEngine.getConnection("");
	    
	    String sql = "SELECT S.code FROM NeoUser U WITH(NOLOCK) "
		    	+" INNER JOIN SecurityEntity S WITH(NOLOCK)ON S.neoId = U.neoId "
		    	+" WHERE  S.active=1";
	    
	    
	    pstm = conn.prepareStatement(sql);
	    
	    rs = pstm.executeQuery();
	    
	    while (rs.next()){
		usuarios.add(rs.getString("code"));
	    }
	    
	}catch (Exception e){
	    
	}finally{
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return usuarios;
    }
    
}
