package com.neomind.fusion.custom.orsegups.adapter.regrasSupPastas;

import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskSummary;
import com.neomind.fusion.workflow.task.rule.TaskRuleAdapter;
import com.neomind.fusion.workflow.task.rule.TaskRuleContext;

public class AdapterRegraFAPDilmo implements TaskRuleAdapter{

	@Override
	public boolean isTriggered(TaskRuleContext context){
	    	
	    boolean retorno = false;
	    
	    Task t = null;
	    TaskSummary	ts = null;
	    try{
		
		if(context.getTask() instanceof  Task){
		    t = (Task) context.getTask();
		}else{
		    ts = (TaskSummary) context.getTask();
		}

		retorno = ((t != null && t.getProcessName().equals("F001 - FAP - Autorização de Pagamento")) || (ts != null && ts.getProcessName().equals("F001 - FAP - Autorização de Pagamento")));

	    }catch(Exception e){
		System.out.println("Erro ao direcionar tarefa de FAP para subpastas. Mensagem: "+e.getMessage());
		e.printStackTrace();
	    }

	    return retorno;

	}
}

