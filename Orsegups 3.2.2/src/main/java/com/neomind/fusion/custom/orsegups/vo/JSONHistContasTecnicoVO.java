package com.neomind.fusion.custom.orsegups.vo;

public class JSONHistContasTecnicoVO {
	
	Object cliente;
	Object codigo;
	Object tipo;
	Object central;
	Object situacao;
	Object competenciaPagamento;
	Object tarefa;
	Object tecnico;

	public JSONHistContasTecnicoVO(Object central, Object cliente, Object codigo, Object tipo, Object situacao, Object competenciaPagamento, 
			Object tarefa, Object tecnico) {
		super();
		this.cliente = cliente;
		this.codigo = codigo;
		this.tipo = tipo;
		this.central = central;
		this.situacao = situacao;
		this.competenciaPagamento = competenciaPagamento;
		this.tarefa = tarefa;
		this.tecnico = tecnico;
	}

	public Object getCliente() {
		return cliente;
	}

	public void setCliente(Object cliente) {
		this.cliente = cliente;
	}

	public Object getCodigo() {
		return codigo;
	}

	public void setCodigo(Object codigo) {
		this.codigo = codigo;
	}

	public Object getTipo() {
		return tipo;
	}

	public void setTipo(Object tipo) {
		this.tipo = tipo;
	}

	public Object getCentral() {
		return central;
	}

	public void setCentral(Object central) {
		this.central = central;
	}

	public Object getSituacao() {
		return situacao;
	}

	public void setSituacao(Object situacao) {
		this.situacao = situacao;
	}

	public Object getCompetenciaPagamento() {
		return competenciaPagamento;
	}

	public void setCompetenciaPagamento(Object competenciaPagamento) {
		this.competenciaPagamento = competenciaPagamento;
	}

	public Object getTarefa() {
		return tarefa;
	}

	public void setTarefa(Object tarefa) {
		this.tarefa = tarefa;
	}
	
	public Object getTecnico() {
		return tecnico;
	}

	public void setTecnico(Object tecnico) {
		this.tecnico = tecnico;
	}
	
}
