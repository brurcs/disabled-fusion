package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaColaboradorContaBancaria implements CustomJobAdapter {

    final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaColaboradorContaBancaria");
    
    private Long key = GregorianCalendar.getInstance().getTimeInMillis();
    
    public void execute(CustomJobContext ctx) {
		
	log.warn("INICIAR AGENDADOR DE TAREFA: RotinaAbreTarefaColaboradorContaBancaria -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
		
	try{
	    
	    conn = PersistEngine.getConnection("VETORH");
	    
	    String sql = "SELECT DISTINCT FUN.numemp, FUN.numcad, FUN.nomfun , CAR.TitCar, REG.USU_CodReg "
		    +"FROM r034fun FUN WITH(NOLOCK) "
		    +"INNER JOIN R018CCU CCU WITH(NOLOCK)ON FUN.codccu = CCU.CodCcu "
		    +"INNER JOIN r016orn ORN WITH(NOLOCK) ON ORN.taborg = FUN.taborg AND ORN.numloc = FUN.numloc "
		    +"INNER JOIN USU_T200REG REG WITH(NOLOCK) ON REG.USU_CodReg = ORN.usu_codreg "
		    +"INNER JOIN R024CAR CAR WITH(NOLOCK) ON CAR.CodCar = FUN.codcar "
		    +"WHERE conban = 0 "
		    +"AND CCU.CodCcu NOT IN (162120, 162250) "
		    +"AND FUN.SitAfa <> 7 " 
		    +"AND FUN.TipCol = 1 " 
		    +"AND FUN.numemp IN (1,2,6,7,8,12,15,17,18,19,21,22) "
		    +"AND REG.USU_CodReg <> 0 "
		    +"ORDER BY usu_codreg ";
	    
	    stmt = conn.createStatement();
	    
	    rs = stmt.executeQuery(sql);
	    
	    HashMap<Long, ArrayList<ColaboradorVO>> mapaColaboradores = new HashMap<Long, ArrayList<ColaboradorVO>>();
		        
	    while (rs.next()){
		
		long codReg = rs.getLong("usu_codreg");
		
		ColaboradorVO colaborador = new ColaboradorVO();
		
		colaborador.setNumeroEmpresa(rs.getLong("numemp"));
		colaborador.setNumeroCadastro(rs.getLong("numcad"));
		colaborador.setNomeColaborador(rs.getString("nomfun"));
		colaborador.setCargo(rs.getString("TitCar"));
		
		if (mapaColaboradores.containsKey(codReg)){
		    ArrayList<ColaboradorVO> listaColaboradores = mapaColaboradores.get(codReg);
		    listaColaboradores.add(colaborador);
		}else{
		    ArrayList<ColaboradorVO> listaColaboradores = new ArrayList<ColaboradorVO>();
		    listaColaboradores.add(colaborador);
		    mapaColaboradores.put(codReg, listaColaboradores);
		}
			
	    }
	    
	    this.abrirTarefas(mapaColaboradores);
	    
	}catch (Exception e){
	    e.printStackTrace();
	    log.error("ERRO ROTINA RotinaAbreTarefaColaboradorContaBancaria [" + key + "]");
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

    }

    private void abrirTarefas(HashMap<Long, ArrayList<ColaboradorVO>> mapaColaboradores) {
		
	String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteTarefaColaboradorContaBancaria");
	
	GregorianCalendar prazo = new GregorianCalendar();

	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 7L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);
	
	String titulo = "Colaborador(es) sem conta bancária cadastrada";
	
	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	
	String tarefas = "";
	
	for (Entry<Long, ArrayList<ColaboradorVO>> entry : mapaColaboradores.entrySet()){
	    
	    long codReg = entry.getKey();
	    
	    NeoPaper paperExecutor = OrsegupsUtils.getPapelCoordenadorRegional(codReg);
		
	    String executor = OrsegupsUtils.getUserNeoPaper(paperExecutor.getCode());
	    
	    String descricao = "<b>Favor providênciar número da conta/agencia bancária dos seguintes colaboradores:</b> <br>";
	    
	    ArrayList<ColaboradorVO> listaColaboradores = entry.getValue();
	    
	    for (ColaboradorVO colaborador : listaColaboradores ){
		descricao += "Empresa: "+colaborador.getNumeroEmpresa()+" Matrícula: "+colaborador.getNumeroCadastro()+" Nome: "+colaborador.getNomeColaborador()+" Cargo: "+colaborador.getCargo()+ "<br>";
	    }
	    
	    String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);
	    
	    if (tarefa != null && !tarefa.isEmpty()) {
		tarefas += tarefa + " ";
	    } else {
		log.error("ERRO ROTINA RotinaAbreTarefaColaboradorContaBancaria [" + key + "]");
		throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	    }
	       	    
	}
	
	System.out.println(tarefas);
		
	
    }

}
