package com.neomind.fusion.custom.orsegups.fap.slip;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPSlipPrepararGLN implements AdapterInterface
{
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "";
		
		try
		{
			Long codModalidade = processEntity.findGenericValue("modalidadeContrato.codigo");
			Long tipoLancamento = processEntity.findGenericValue("produtoOuServico.codigo");
			Long codEmpresa = processEntity.findGenericValue("empresa.codemp");
			String codSolicitante = processEntity.findGenericValue("solicitante.code");
			
			BigDecimal valorPagamento = processEntity.findGenericValue("valorPagamento");
			
			GregorianCalendar gcCompetencia = processEntity.findGenericValue("competencia");
			Integer mesCompetencia = gcCompetencia.get(Calendar.MONTH)+1;
			Integer anoCompetencia = gcCompetencia.get(Calendar.YEAR);

			WFProcess process = processEntity.findGenericValue("wfprocess");
			
			List<NeoObject> listAprovadores = processEntity.findGenericValue("listaAprovadores");
			String ultimoAprovador = listAprovadores.size() > 0 ? ((NeoUser)listAprovadores.get(0)).getFullName() : "";
			
			String obs = "";
			String obsSlip = "";
			 
			Long tipoPagamento = processEntity.findGenericValue("tipoPagamento.codigo");
			if (tipoPagamento == 1l && codModalidade == 1l)
			{
				Long codContrato = processEntity.findGenericValue("dadosContrato.codContrato");
				obs += "FAP SLIP Contrato " + codContrato + " Tarefa " + process.getCode() + ". " + mesCompetencia + "/" + anoCompetencia + ". ";
				obsSlip = "FAP Slip: " + process.getCode() + " Usuário Aprovador: " + ultimoAprovador + " Contrato: " + codContrato;
				
				NeoUser nuResponsavelContrato = processEntity.findGenericValue("dadosContrato.responsavelContrato");
				codSolicitante = nuResponsavelContrato.getCode();
			}
			else if (tipoPagamento == 2l || codModalidade == 2l)
			{
				obs += "FAP SLIP AVULSO Tarefa " + process.getCode() + ". Competência:" + mesCompetencia + "/" + anoCompetencia + ".";
				obsSlip = "FAP Slip: " + process.getCode() + " Usuário Aprovador: " + ultimoAprovador + " Avulso";
			}
			
			obs += processEntity.findGenericValue("obsGTCGLN");

			QLGroupFilter filtroCondicaoPagamento = new QLGroupFilter("AND");
			filtroCondicaoPagamento.addFilter(new QLEqualsFilter("codemp", codEmpresa));
			filtroCondicaoPagamento.addFilter(new QLEqualsFilter("codcpg", "AV"));
			NeoObject noCondicaoPagamento = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("CondicaoPagamento"), filtroCondicaoPagamento);
			if (noCondicaoPagamento == null)
			{
				mensagem = "Não encontrado o Código da Condição de Pagamentos para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			NeoObject noUnidadeMedida = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE015MED"), new QLEqualsFilter("unimed", "UN"));
			if (noUnidadeMedida == null)
			{
				mensagem = "Não encontrado a Unidade de Medida para lançamento da Nota Fiscal de Entrada!";
				throw new WorkflowException(mensagem);
			}
			
			NeoObject noSerie = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE020SNF"), new QLEqualsFilter("codsnf", "U"));
			if (noSerie == null)
			{
				mensagem = "Não encontrado a Série para lançamento da Nota Fiscal de Entrada!";
				throw new WorkflowException(mensagem);
			}
			
			NeoObject noPrazo = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GLNTipoPrazo"), new QLEqualsFilter("codigo", 1l));

			QLGroupFilter filtroFormaPagamento = new QLGroupFilter("AND");
			filtroFormaPagamento.addFilter(new QLEqualsFilter("codemp", codEmpresa));
			filtroFormaPagamento.addFilter(new QLEqualsFilter("codfpg", (Long)processEntity.findValue("formaPagamento.codigoFormaPagamento")));
			NeoObject noFormaPagamento = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE066FPG"), filtroFormaPagamento);
			if (noFormaPagamento == null)
			{
				mensagem = "Não encontrado o Código da Forma de Pagamento para a Empresa informada no FAP!";
				throw new WorkflowException(mensagem);
			}

			QLGroupFilter filtroUsuarioSolicitante = new QLGroupFilter("AND");
			filtroUsuarioSolicitante.addFilter(new QLEqualsFilter("nomusu", codSolicitante));
			NeoObject solicitante = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), filtroUsuarioSolicitante);
			if (solicitante == null)
			{
				mensagem = "Não encontrado o Código do usuário Solicitante informada no FAP!";
				throw new WorkflowException(mensagem);
			}
			
			String nrDocumento = processEntity.findGenericValue("nrDocumento");
			nrDocumento = nrDocumento.replaceAll("[^0-9]", "");
			
			NeoObject oGestaLancamentoDeNota = AdapterUtils.createNewEntityInstance("GLNGestaoDeLancamentoDeNota");
			EntityWrapper wrpGestaLancamentoDeNota = new EntityWrapper(oGestaLancamentoDeNota);

			wrpGestaLancamentoDeNota.setValue("empresa", processEntity.findValue("empresa"));
			wrpGestaLancamentoDeNota.setValue("filial", processEntity.findValue("filial"));
			wrpGestaLancamentoDeNota.setValue("notaFiscal", new Long(nrDocumento.trim()));
			wrpGestaLancamentoDeNota.setValue("serieNotaFiscal", noSerie);
			wrpGestaLancamentoDeNota.setValue("responsavelNota", processEntity.findValue("executorControladoria"));
			wrpGestaLancamentoDeNota.setValue("codigoFornecedor", processEntity.findValue("fornecedor"));
			wrpGestaLancamentoDeNota.setValue("condicaoPagamento", noCondicaoPagamento);
			wrpGestaLancamentoDeNota.setValue("formaPagamento", noFormaPagamento);
			wrpGestaLancamentoDeNota.setValue("valorLiquido", processEntity.findValue("valorPagamento"));
			wrpGestaLancamentoDeNota.setValue("usuarioResponsavel", solicitante);
			wrpGestaLancamentoDeNota.setValue("dataEmissao", processEntity.findValue("dataEmissao"));
			wrpGestaLancamentoDeNota.setValue("competenciaBI", gcCompetencia);
			wrpGestaLancamentoDeNota.setValue("observacao", obs);
			wrpGestaLancamentoDeNota.setValue("obsSlip", obsSlip);
			wrpGestaLancamentoDeNota.setValue("tipoPrazo", noPrazo);
			wrpGestaLancamentoDeNota.setValue("dataVencPrimParcela", processEntity.findValue("dataVencimento"));
			
			List<NeoObject> listParcelas = processEntity.findGenericValue("parcelasGLN");
			if (listParcelas != null)
			{
				for (NeoObject noParcela : listParcelas)
				{
					wrpGestaLancamentoDeNota.findField("listaParcelas").addValue(EntityCloner.cloneNeoObject(noParcela));
				}
			}
			
			PersistEngine.persist(oGestaLancamentoDeNota);
			
			processEntity.setValue("GlnGestaoDeLancamentoDeNota", oGestaLancamentoDeNota);

			NeoObject noItem = null;
			if (tipoLancamento == 1L)
			{
				noItem = AdapterUtils.createNewEntityInstance("GLNItensDeServico");
			}
			else if (tipoLancamento == 2l)
			{
				noItem = AdapterUtils.createNewEntityInstance("GLNItensDeProduto");
			}
			
			EntityWrapper wItem = new EntityWrapper(noItem);
			
			NeoObject noTipoLancamentoRateio = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GLNTipoLancamento"), new QLEqualsFilter("codigoLancamento", 2l));
			
			wItem.setValue("unidadeMedida", noUnidadeMedida);
			wItem.setValue("quantidadeRecebida", Long.valueOf(1L));
			wItem.setValue("contaFinanceira", processEntity.findGenericValue("contaFinanceira"));
			wItem.setValue("contaContabil", processEntity.findGenericValue("contaContabil"));
			wItem.setValue("listaLancamento", noTipoLancamentoRateio);
			
			
			
			List<NeoObject> listCCU = processEntity.findGenericValue("centrosCusto.rateio");
			for (NeoObject noCCU : listCCU)
			{
				EntityWrapper wCCU = new EntityWrapper(noCCU);
				
				Long codTipoRateio = processEntity.findGenericValue("centrosCusto.tipoRateio.codigo");
				NeoObject noCentroCusto = wCCU.findGenericValue("centroCusto");
				BigDecimal valor = wCCU.findGenericValue("valor");
				
				if (codTipoRateio == 1l && valor != null)
				{
					BigDecimal valorRateio = processEntity.findGenericValue("valorPagamento");
					valor = valorRateio.multiply(valor).divide(new BigDecimal(100), BigDecimal.ROUND_UP);
				}
				
				NeoObject noCentroCustoGLN = AdapterUtils.createNewEntityInstance("GLNListaDeCentroDeCusto");
				EntityWrapper wCentroCustoGLN = new EntityWrapper(noCentroCustoGLN);
				wCentroCustoGLN.setValue("codigoCentroCusto", noCentroCusto);
				wCentroCustoGLN.setValue("valorRateio", valor);
				
				wItem.findField("listaCentroCusto").addValue(noCentroCustoGLN);
			}
			
			if (obs.length() >= 249)
			{
				obs = obs.substring(0, 249);
			}
			
			if (tipoLancamento == 1L)
			{
				wItem.setValue("observacaoDoServico", obs);
				wItem.setValue("precoUnitarioServico", valorPagamento);
				PersistEngine.persist(noItem);
				
				wrpGestaLancamentoDeNota.findField("listaItensDeServico").addValue(noItem);
			}
			else if (tipoLancamento == 2l)
			{
				wItem.setValue("observacaoDoProduto", obs);
				wItem.setValue("precoUnitarioProduto", valorPagamento);
				PersistEngine.persist(noItem);
				
				wrpGestaLancamentoDeNota.findField("listaItensDeProduto").addValue(noItem);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI! " + e.getMessage());
		}
	}

	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}