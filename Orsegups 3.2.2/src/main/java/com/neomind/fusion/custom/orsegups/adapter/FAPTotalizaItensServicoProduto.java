package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPTotalizaItensServicoProduto implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "";
		try
		{
			BigDecimal valorTotalProduto = new BigDecimal(0.0);
			BigDecimal valorTotalServico = new BigDecimal(0.0);
		
			List<NeoObject> itens = (List<NeoObject>) processEntity.findValue("itemOrcamento");
			if (itens != null && !itens.isEmpty()) 	{
				for (NeoObject item : itens) {
					
					EntityWrapper wrpItem = new EntityWrapper(item);
					
					if(wrpItem.findValue("tipoItem.descricao") != null && ((String)wrpItem.findValue("tipoItem.descricao")).equals("PRODUTO"))
					{						
						valorTotalProduto = valorTotalProduto.add((BigDecimal)wrpItem.findValue("valor"));	
					}					
					if(wrpItem.findValue("tipoItem.descricao") != null && ((String)wrpItem.findValue("tipoItem.descricao")).equals("SERVIÇO")){
						valorTotalServico = valorTotalServico.add((BigDecimal)wrpItem.findValue("valor"));	
					}
				}
			}
			
			processEntity.setValue("valorTotalProduto", valorTotalProduto);
			processEntity.setValue("valorTotalServicos", valorTotalServico);
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor contate a TI!!");
		}
	}
}
