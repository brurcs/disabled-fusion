package com.neomind.fusion.custom.orsegups.site;

import java.util.ArrayList;
import java.util.Collection;

public class CartaoPontoDataSource {
	
	private String numcad;
	private String numemp;
	private String tipcol;
	private String nomfun;
	private String ctps;
	private String cargo;
	private String admissao;
	private String periodo;
	private String mesref;
	private String horex;
	
	private String cpf;
	
	
	
	Collection<CartaoPontoMarcacoesDataSource> listaDeMarcacoes = new ArrayList<CartaoPontoMarcacoesDataSource>();



	public String getNumcad() {
		return numcad;
	}



	public void setNumcad(String numcad) {
		this.numcad = numcad;
	}



	public String getNumemp() {
		return numemp;
	}



	public void setNumemp(String numemp) {
		this.numemp = numemp;
	}



	public String getTipcol() {
		return tipcol;
	}



	public void setTipcol(String tipcol) {
		this.tipcol = tipcol;
	}



	public String getNomfun() {
		return nomfun;
	}



	public void setNomfun(String nomfun) {
		this.nomfun = nomfun;
	}



	public String getCtps() {
		return ctps;
	}



	public void setCtps(String ctps) {
		this.ctps = ctps;
	}



	public String getCargo() {
		return cargo;
	}



	public void setCargo(String cargo) {
		this.cargo = cargo;
	}



	public String getAdmissao() {
		return admissao;
	}



	public void setAdmissao(String admissao) {
		this.admissao = admissao;
	}



	public String getPeriodo() {
		return periodo;
	}



	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}



	public String getMesref() {
		return mesref;
	}



	public void setMesref(String mesref) {
		this.mesref = mesref;
	}



	public String getHorex() {
		return horex;
	}



	public void setHorex(String horex) {
		this.horex = horex;
	}



	public String getCpf() {
		return cpf;
	}



	public void setCpf(String cpf) {
		this.cpf = cpf;
	}



	public Collection<CartaoPontoMarcacoesDataSource> getListaDeMarcacoes() {
		return listaDeMarcacoes;
	}



	public void setListaDeMarcacoes(
			Collection<CartaoPontoMarcacoesDataSource> listaDeMarcacoes) {
		this.listaDeMarcacoes = listaDeMarcacoes;
	}

	
	
	
	
	
}
