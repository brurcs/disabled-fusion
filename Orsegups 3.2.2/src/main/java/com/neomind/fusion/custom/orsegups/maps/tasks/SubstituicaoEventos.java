package com.neomind.fusion.custom.orsegups.maps.tasks;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;

import br.com.segware.sigmaWebServices.webServices.EventoRecebido;
import br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoRecebidoExtensaoVO;
import com.neomind.fusion.custom.orsegups.seventh.SeventhUtils;
import com.neomind.fusion.custom.orsegups.seventh.bean.EventoArmeDesarme;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class SubstituicaoEventos {

    private SeventhUtils seventhUtils = new SeventhUtils();
    
    public static SubstituicaoEventos instance;
    
    private SubstituicaoEventos() {
	
    }
    
    public static SubstituicaoEventos getInstance(){
	if (instance == null){
	    SubstituicaoEventos.instance = new SubstituicaoEventos();
	}
	return SubstituicaoEventos.instance;
    }

    public synchronized String run() {
	
	String returnFromAccess = "";
	
	List<NeoObject> listaSubstituicao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("sigmaSubstituirEvento"));

	if (listaSubstituicao != null && !listaSubstituicao.isEmpty()) {

	    for (NeoObject objSubs : listaSubstituicao) {

		EntityWrapper wSubstituicao = new EntityWrapper(objSubs);
		boolean substituir = (boolean) wSubstituicao.findField("substituir").getValue();

		if (substituir) {
		    Connection conn = null;
		    Statement stmt = null;
		    ResultSet rs = null;

		    Map<Long, EventoRecebidoExtensaoVO> listaEventosRecebidos = null;

		    String eventos = "";

		    List<NeoObject> listaEventos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("sigmaCodigoEventosSubstituidos"));

		    if (listaEventos != null && !listaEventos.isEmpty()) {

			int count = 0;

			for (NeoObject objEvento : listaEventos) {

			    EntityWrapper wEvento = new EntityWrapper(objEvento);
			    String cdEvento = (String) wEvento.findField("codigoEvento").getValue();

			    if (count == 0) {
				eventos += " H.CD_EVENTO='" + cdEvento + "' ";
			    } else {
				eventos += " OR H.CD_EVENTO='" + cdEvento + "' ";
			    }
			    count++;

			}
		    }

		    try {

			StringBuilder sql = new StringBuilder();
						

			sql.append(" SELECT H.CD_HISTORICO, H.CD_EVENTO, H.CD_CODE, H.DT_RECEBIDO, H.NU_AUXILIAR, C.CD_CLIENTE, C.ID_CENTRAL, C.PARTICAO, C.ID_EMPRESA, ");
			sql.append(" S.SERVIDOR, C.PORTACFTV,C.USERCFTV,C.SENHACFTV, C.ARMADA, ISNULL(C.DT_ULTIMO_ARME,'1900-12-31 00:00:00.000') AS DT_ULTIMO_ARME ");
			sql.append(" FROM HISTORICO H WITH(NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON H.CD_CLIENTE = C.CD_CLIENTE ");
			sql.append(" LEFT JOIN [CACUPE\\SQL02].Fusion_Producao.dbo.D_SEVENTHServidoresHabilitados S WITH(NOLOCK) ON RTRIM(LTRIM(S.servidor)) = RTRIM(LTRIM(SERVIDORCFTV)) COLLATE Latin1_General_CI_AS ");
			sql.append(" WHERE ((H.FG_STATUS IN (0,1,8)) OR (H.FG_STATUS = 5 AND H.CD_CODE='XPA' ))  ");
			sql.append(" AND H.CD_HISTORICO_PAI IS NULL ");
			sql.append(" AND (" + eventos + ") AND C.STSERVIDORCFTV=1 AND H.CD_CODE NOT IN ('MPI','ACP','ACV') ");
			sql.append(" AND (h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%FecharEvento%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%EmEspera%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%TratarSigma%' )   ");

			conn = PersistEngine.getConnection("SIGMA90");
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());

			listaEventosRecebidos = new HashMap<Long, EventoRecebidoExtensaoVO>();

			while (rs.next()) {

			    EventoRecebido eventoRecebido = new EventoRecebido();
			    
			    if(rs.getString("CD_CODE").equals("XPA")){
				eventoRecebido.setCodigo("XVD2");
			    }else{
				eventoRecebido.setCodigo("XVID");				
			    }
			    
			    Calendar recebido = Calendar.getInstance();
			    recebido.setTimeInMillis(rs.getTimestamp("DT_RECEBIDO").getTime());
			    eventoRecebido.setData(recebido);
			    eventoRecebido.setEmpresa(rs.getLong("ID_EMPRESA"));
			    eventoRecebido.setIdCentral(rs.getString("ID_CENTRAL"));
			    eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
			    eventoRecebido.setParticao(rs.getString("PARTICAO"));
			    eventoRecebido.setProtocolo(Byte.parseByte("2"));
			    eventoRecebido.setCdCliente(rs.getInt("CD_CLIENTE"));
			    eventoRecebido.setAuxiliar(rs.getString("NU_AUXILIAR"));

			    EventoRecebidoExtensaoVO ex = new EventoRecebidoExtensaoVO();

			    ex.setCode(rs.getString("CD_CODE"));
			    
			    ex.setServidorCFTV(rs.getString("SERVIDOR"));
			    ex.setPortaCFTV(rs.getString("PORTACFTV"));
			    ex.setUserCFTV(rs.getString("USERCFTV"));
			    ex.setSenhaCFTV(rs.getString("SENHACFTV"));
			    
			    
			    ex.setDtUltimoArme(new Date(rs.getTimestamp("DT_ULTIMO_ARME").getTime()));
			    ex.setArmada(rs.getBoolean("ARMADA"));

			    ex.setEvento(eventoRecebido);

			    listaEventosRecebidos.put(rs.getLong("CD_HISTORICO"), ex);

			}

		    } catch (Exception e) {
			e.printStackTrace();
		    } finally {
			OrsegupsUtils.closeConnection(conn, stmt, rs);
		    }

		    for (Long key : listaEventosRecebidos.keySet()) {

			EventoRecebidoExtensaoVO xEvento = listaEventosRecebidos.get(key);

			boolean abrirXPL = false;
			
			String motivo = "";

			if (xEvento.getDtUltimoArme().after(xEvento.getEvento().getData().getTime())) {
			    xEvento.setArmada(!xEvento.isArmada());
			}

			if (xEvento.isArmada()) {
			    if (listaEventosRecebidos.get(key).getServidorCFTV() != null) {

				if (this.todasAsGravacoesOK(xEvento)){
				    abrirXPL = true;
				}else{
				    motivo = "FALTARAM_GRAVACOES";
				}
				
			    }else{
				motivo = "SERVIDOR_NAO_HABILITADO";
			    }
			}else{
			    motivo = "CENTRAL_DESARMADA";
			}
			
			if (abrirXPL){
			    if(xEvento.getCode().equals("XPA")){
				xEvento.getEvento().setCodigo("XPE2");
			    }else{
				xEvento.getEvento().setCodigo("XPLE");				
			    }
			}

			ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();

			long cdEventoXVID = this.verificaEventoAberto(xEvento.getEvento());
			
			this.fechaEvento(key, cdEventoXVID);
						
			if (cdEventoXVID == 0) {
			    try {
				returnFromAccess = webServiceProxy.receberEvento(xEvento.getEvento());
				
				if(xEvento.getEvento().getCodigo().equals("XVID") && !motivo.isEmpty()){
				    this.gravarLog(xEvento, motivo, key);
				}
				
			    } catch (RemoteException e) {
				e.printStackTrace();
			    }
			} else {
			    returnFromAccess = "ACK";
			}

		    }
		}

	    }

	}

	return returnFromAccess;
    }
    
    private void gravarLog(EventoRecebidoExtensaoVO evento, String motivo, Long historico){
	
	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append(" INSERT INTO SubstituicaoEventosLog (HISTORICO, CD_CLIENTE, SERVIDOR, RECEBIDO, MOTIVO) ");
	sql.append(" VALUES (?,?,?,?,?) ");

	try {
	    conn = PersistEngine.getConnection("TIDB");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, historico);
	    pstm.setLong(2, evento.getEvento().getCdCliente());
	    pstm.setString(3, evento.getServidorCFTV());
	    pstm.setTimestamp(4, new Timestamp(evento.getEvento().getData().getTimeInMillis()));
	    pstm.setString(5, motivo);
	    
	    pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
    }

    private boolean todasAsGravacoesOK(EventoRecebidoExtensaoVO evento) {
	
	EventoArmeDesarme v = new EventoArmeDesarme();

	v.setServidorCFTV(evento.getServidorCFTV());
	v.setPortaCFTV(evento.getPortaCFTV());
	v.setUsuarioCFTV(evento.getUserCFTV());
	v.setSenhaCFTV(evento.getSenhaCFTV());
	v.setIdEmpresa(String.valueOf(evento.getEvento().getEmpresa()));
	v.setIdCentral(evento.getEvento().getIdCentral());

	TreeMap<String, String> treeMap = null;
	try {
	    treeMap = seventhUtils.getCameraCliente(v);
	} catch (IOException e) {
	    e.printStackTrace();
	}

	List<String> cameras = new ArrayList<String>();
	
	if (treeMap != null && !treeMap.isEmpty()) {
	    for (Entry<String, String> entry : treeMap.entrySet()) {

		String camera = "";
		if (entry != null) {
		    camera = String.valueOf(entry.getKey());
		    cameras.add(camera);

		}
	    }

	    GregorianCalendar dataEvento = new GregorianCalendar();

	    dataEvento.setTime(evento.getEvento().getData().getTime());

	    String dataHora = NeoDateUtils.safeDateFormat(dataEvento, "dd/MM/yyyy HH:mm:ss");
	    String hora = dataHora.substring(10, dataHora.length());
	    hora = hora.trim();
	    hora = hora.replaceAll(":", "-");
	    String data = dataHora.substring(0, 10);
	    data = data.trim();
	    data = data.replaceAll("/", "-");
	    
	    if (!cameras.isEmpty()){
		
		double countTotal = cameras.size();		
		double countSucesso = 0;
		
		    for (String camera : cameras) {
			

			String resolucao = "320x240";
			String qualidade = "70";
			String formato = "jpg";

			String urlStr = "http://" + evento.getServidorCFTV() + ":" + evento.getPortaCFTV() + "/player/getimagem.cgi?camera=" + camera + "&resolucao=" + resolucao + "&qualidade=" + qualidade + "&formato=" + formato + "&data=" + data + "&hora=" + hora;
			ByteArrayOutputStream output = null;
			URL url = null;
			URLConnection uc = null;
			InputStream is = null;
			BufferedImage image = null;

			try {
			    output = new ByteArrayOutputStream();
			    String userpass = evento.getUserCFTV() + ":" + evento.getSenhaCFTV();

			    url = new URL(urlStr);
			    uc = url.openConnection();
			    uc.setConnectTimeout(10*1000);
			    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
			    uc.setRequestProperty("Authorization", basicAuth);
			    is = uc.getInputStream();

			    /**
			     * NOVO BLOCO DE VALIDACAO DE HORARIO DA IMAGEM RECEBIDA, SE
			     * IMAGEM FORA DO INTERVALO HORA DO EVENTO - MOMENTO DO
			     * REGISTRO, IMAGEM SERÁ DESCARTADA
			     */

			    String tipoRetorno = uc.getContentType();
			    /**
			     * VALIDA SE RETORNO DA REQUISIÇÃO É DO TIPO IMAGE/JPG, SE
			     * FOR TEXT/PLAIN RETORNO É UMA DAS MENSAGEM DE IMAGEM
			     * INDISPONIVEL
			     */
			    if (tipoRetorno.equalsIgnoreCase("image/jpeg")) {
				String stringLastModified = uc.getHeaderField("Last-Modified");

				DateFormat formatLastModified = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH);

				Date lastModified = formatLastModified.parse(stringLastModified);

				// String stringMomentoRegistrado = data + " " + hora;
				//
				// DateFormat formatMomentoRegistrado = new
				// SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
				//
				// Date momentoRegistrado =
				// formatMomentoRegistrado.parse(stringMomentoRegistrado);

				Date momentoEvento = evento.getEvento().getData().getTime();

				Date range1 = new Date(momentoEvento.getTime() - (1000 * 30));
				
				Date range2 = new Date(momentoEvento.getTime() + (1000 * 10));

				/**
				 * VALIDA SE MOMENTO ESTA DENTRO DO INTERVALO OU É UM
				 * DOS EXTREMOS
				 */
				if (!lastModified.before(range1) && !lastModified.after(range2)) {

				    image = ImageIO.read(is);

				    if (image != null) {
					countSucesso ++;

					double percentSucesso = (countSucesso*100)/countTotal;

					if (percentSucesso >= 20.0){
					    return true;
					}

				    } else {
					continue;
				    }
				} else {
				    continue;
				}
			    } else {
				continue;
			    }

			} catch (Exception e) {
			    e.printStackTrace();
			    continue;
			} finally {
			    try {
				if (is != null)
				    is.close();
				if (output != null) {
				    output.flush();
				    output.close();
				}
			    } catch (IOException e) {
				e.printStackTrace();
			    }
			}
		    }
		    
		    double percentSucesso = (countSucesso*100)/countTotal;
		    
		    if (percentSucesso >= 20.0){
			return true;
		    }
		
	    }else{
		return false;
	    }
	    
	}else{
	    //TODO ADICIONAR LOG SE NAO BUSCAR LAYOUT
	}
	
	return false;
    }

    private boolean fechaEvento(long cdHistorico, long cdHistoricoXVID) {

	Connection conn = null;
	PreparedStatement ps = null;

	String historico = "";

	if (cdHistoricoXVID != 0) {
	    historico = String.valueOf(cdHistoricoXVID);
	}

	try {

	    String sql = " UPDATE HISTORICO SET FG_STATUS = 4, TX_OBSERVACAO_FECHAMENTO = 'Fechamento automático. Evento sendo tratado via XVID/XPLE " + historico + "',  " + " DT_FECHAMENTO = GETDATE(), CD_USUARIO_FECHAMENTO = 11010 " + " WHERE ( CD_HISTORICO_PAI = ? OR CD_HISTORICO = ? ) ";

	    conn = PersistEngine.getConnection("SIGMA90");
	    ps = conn.prepareStatement(sql);
	    ps.setLong(1, cdHistorico);
	    ps.setLong(2, cdHistorico);

	    int rs = ps.executeUpdate();

	    int updateCount = 0;

	    while (rs < 1 && updateCount < 2) {
		rs = ps.executeUpdate();
		updateCount++;
	    }

	    if (rs > 0) {
		return true;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [ERRO_FECHAMENTO_EVENTO_ALARME]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, ps, null);
	}

	return false;
    }

    /**
     * Verifica se existe evento aberto na conta do cliente em situaÃ§Ã£o 0
     * (nÃ£o atendido)
     * 
     * @param eventoRecebido
     * @return boolean true se existe evento; false se nÃ£o
     */
    private long verificaEventoAberto(EventoRecebido eventoRecebido) {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	try {

	    String sql = "SELECT H.CD_HISTORICO FROM HISTORICO H WITH(NOLOCK) "
	    	+ " INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE "
	    	+ " LEFT JOIN RECEPCAO R WITH(NOLOCK) ON R.CD_CENTRAL = C.ID_CENTRAL AND R.CD_EMPRESA = C.ID_EMPRESA AND C.PARTICAO LIKE '%'+R.NU_PARTICAO+'%' AND R.NM_EVENTO IN ('XVID','XPLE','XVD2','XPE2') "
	    	+ " WHERE H.FG_STATUS IN (0, 5) AND H.CD_EVENTO IN ('XVID','XPLE','XVD2','XPE2') AND H.CD_CLIENTE = " + eventoRecebido.getCdCliente();

	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql);

	    if (rs.next()) {

		return rs.getLong("CD_HISTORICO");

	    } else {

		return 0;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return 0;

    }

}
