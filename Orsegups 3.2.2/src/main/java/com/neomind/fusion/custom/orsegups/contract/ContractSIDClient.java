/*
 * Adapter para Gravar via SID um novo cliente no Sapiens
 * A classe possui também métodos para gravar definições e contatos
 * @autor: Edgar Luis
 *  
 */

package com.neomind.fusion.custom.orsegups.contract;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContractSIDClient implements AdapterInterface
{
	private static Long ALTERACAO_TERMO_ADITIVO = (long) 5;

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		try
		{
			ContratoLogUtils.logInfo("Iniciando ContractSIDClient");
			Long codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
			Long movimentoContrato = (Long) wrapper.findField("movimentoContratoNovo.codTipo").getValue();

			if (movimentoContrato.equals(ALTERACAO_TERMO_ADITIVO))
			{
				Long tipoAlteracao = (Long) wrapper.findField("movimentoAlteracaoContrato.codTipo").getValue();
				Long cliente = (Long) wrapper.findField("buscaNomeCliente.codcli").getValue();
				Long numContrato = (Long) wrapper.findField("numContrato.usu_numctr").getValue();

				if (tipoAlteracao == 1)
				{
					//System.out.println("alteração de CNPJ");
					/**
					 * Cancelo o contrato selecionado
					 * Cadastro o cliente novo (sid)
					 * Cadastro novo centro de custo
					 * Cadastra o novo contrato
					 */

				}
				else if (tipoAlteracao == 2)
				{
					//System.out.println("alteração de Razao social");
					/**
					 * Atualizo o nome do cliente existente (sid ? ou direto na tabela)
					 * Atualizo o nome no centro de custo – nível 4 – E044CCU e E043PCM
					 */
					
					//ContractSIDClient.
				}
				else
				{
					/**
					 * Altera dados do contrato
					 * Adiciona/remove postos (equipamento/kits/humanos)
					 */

				}
			}
			
			if(movimentoContrato == 3 || movimentoContrato == 4 || movimentoContrato == 5 || movimentoContrato == 6)
			{
				NeoObject noCliente = (NeoObject) wrapper.findField("novoCliente").getValue();
				NeoObject noDadosContrato = (NeoObject) wrapper.findField("dadosGeraisContrato").getValue();

				/*String codCli = sidClient(noCliente);

				if (codCli != null)
				{
					sidClientDefinitions(noDadosContrato, codCli);
					sidClientContact(noCliente, codCli);
				}*/
				
				//Chama o SID para alterar a empresa e a filial
				String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
				String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
				ContractSIDClient.sidChangeEmpFil(codemp, codfil);
				
				//Cadastra um novo cliente para a empresa selecionada
				String codCli = ContractSIDClient.sidClient(wrapper, noCliente, codemp, codfil, true);
				if (codCli != null)
				{
					NeoObject oDebitoDados = (NeoObject) wrapper.findValue("dadosGeraisContrato.debitoDado"); 
					EntityWrapper wDadosDebito = new EntityWrapper(oDebitoDados);
					
					String codBan 	= NeoUtils.safeOutputString(wDadosDebito.findValue("banco.codigo"));
					String portador = NeoUtils.safeOutputString(wDadosDebito.findValue("banco.portador"));
					String codAge 	= NeoUtils.safeOutputString(wDadosDebito.findValue("agencia"));
					String ccbCli 	= NeoUtils.safeOutputString(wDadosDebito.findValue("contacorrente"));
					
					ContractSIDClient.sidClientDefinitions(noDadosContrato, codCli, 0,portador, codemp, codfil, codBan, codAge, ccbCli );
					ContractSIDClient.sidClientContact(noCliente, codCli, codemp, codfil);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ContratoLogUtils.logInfo("ContractSIDClient - Erro ao cadastrar cliente/contratos no Fluxo de contratos." + e.getMessage());
			throw new WorkflowException("ContractSIDClient - Erro ao cadastrar cliente/contratos no Fluxo de contratos." + e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	
	public static String sidClient(EntityWrapper wContrato, NeoObject cliente, String codEmp, String codFil, boolean sobrescreverDados ) throws Exception
	{		
		long movimentoContrato = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("movimentoContratoNovo.codTipo")));
		
		long tipoAlteracao = 0L;
		if (wContrato.findValue("movimentoAlteracaoContrato.codTipo") != null ){
			tipoAlteracao = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("movimentoAlteracaoContrato.codTipo")));
		}
		 																					  
		
		EntityWrapper ewCliente = new EntityWrapper(cliente);
		String codCli = null;

		/* Alimenta os parâmetros do processo para chamar o SID */

		String NomCli = NeoUtils.safeOutputString(ewCliente.getValue("razaoSocial"));
		String ApeCli = NeoUtils.safeOutputString(ewCliente.getValue("nomeFantasia"));
		
		if(NomCli.contains("'") || NomCli.contains("\"") || ApeCli.contains("'") || ApeCli.contains("\"") ){
			throw new Exception("Por favor verificar se os campos da tela possuem os caracteres \" ou '. Qualquer dúvida, contate o Departamento Comercial Privado!!!");
		}
		
		if(movimentoContrato == 1 || movimentoContrato == 2 || 
				(movimentoContrato == 5 && tipoAlteracao != 2))
		{
			NomCli = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.nomcli"));
			ApeCli = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.apecli"));
		}
		NomCli = NeoUtils.fixName(NomCli, true);
		ApeCli = NeoUtils.fixName(ApeCli, true);
		
		String usuTipCli = NeoUtils.safeOutputString(ewCliente.findValue("tipoClienteConsistencia.usu_tipcli"));
		
		String codRtr = NeoUtils.safeOutputString(ewCliente.findValue("regimeTributario.codigo"));

		String TipCli = NeoUtils.safeOutputString(ewCliente.findValue("tipocliente.tipo"));
		if(movimentoContrato == 1 || movimentoContrato == 2)
		{
			TipCli = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.tipcli"));
		}
		
		String CgcCpf = "";
		if (TipCli.equalsIgnoreCase("J"))
			CgcCpf = ewCliente.getValue("cnpj").toString().replaceAll("[./-]", "");
		else
			CgcCpf = ewCliente.getValue("cpf").toString().replaceAll("[./-]", "");

		

		String CodCli = NeoUtils.safeOutputString(ewCliente.getValue("codigoCliente"));

		String NenCli = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.numero"));
		
		String CodRam = NeoUtils.safeOutputString(ewCliente.findValue("ramoAtividade.codram"));

		String TipMer = NeoUtils.safeOutputString(ewCliente.findValue("tipoMercado.tipo"));
		String TipEmc = NeoUtils.safeOutputString(ewCliente.findValue("tipoEmpresa.tipo"));
		
//		String CliCon = "N";
		String CliCon = (Boolean) ewCliente.findGenericValue("clienteContribuinte") ? "S" : "N";
		String InsEst = NeoUtils.safeOutputString(ewCliente.getValue("inscricaoEstadual"));
		String InsMun = NeoUtils.safeOutputString(ewCliente.findValue("inscricaoMunicipal"));
		
		String SigUfs = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.estado.sigufs"));
		String EndCli = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.endereco"));
		String CplEnd = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.complemento"));
		String CepCli = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.cep"));
		String CepIni = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.cidade.cepini"));
		String BaiCli = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.bairro"));
		String CidCli = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.cidade.nomcid"));
		
	
		String nenEnt = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.numero"));;
		String SigUfsEnt = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.estado.sigufs"));
		String EndCliEnt = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.endereco"));
		String CplEndEnt = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.complemento"));
		String CepCliEnt = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.cep"));
		String CepIniEnt = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.cidade.cepini"));
		String BaiCliEnt = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.bairro"));
		String CidCliEnt = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.cidade.nomcid"));
		boolean endEntregaIgual = NeoUtils.safeBoolean(ewCliente.findValue("endEntregaIgualCliente"));
		if(!endEntregaIgual)
		{
			nenEnt =  NeoUtils.safeOutputString(ewCliente.findValue("endEntrega.numero"));
			SigUfsEnt = NeoUtils.safeOutputString(ewCliente.findValue("endEntrega.estadoCidade.estado.sigufs"));
			EndCliEnt = NeoUtils.safeOutputString(ewCliente.findValue("endEntrega.endereco"));
			CplEndEnt = NeoUtils.safeOutputString(ewCliente.findValue("endEntrega.complemento"));
			CepCliEnt = NeoUtils.safeOutputString(ewCliente.findValue("endEntrega.cep"));
			CepIniEnt = NeoUtils.safeOutputString(ewCliente.findValue("endEntrega.estadoCidade.cidade.cepini"));
			BaiCliEnt = NeoUtils.safeOutputString(ewCliente.findValue("endEntrega.bairro"));
			CidCliEnt = NeoUtils.safeOutputString(ewCliente.findValue("endEntrega.estadoCidade.cidade.nomcid"));
		}
		
		//cobrança
		String nenCob =  NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.numero"));
		String SigUfsCob = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.estado.sigufs"));
		String EndCliCob = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.endereco"));
		String CplEndCob = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.complemento"));
		String CepCliCob = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.cep"));
		String CepIniCob = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.cidade.cepini"));
		String BaiCliCob = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.bairro"));
		String CidCliCob = NeoUtils.safeOutputString(ewCliente.findValue("enderecoCliente.estadoCidade.cidade.nomcid"));
		boolean endCobrancaIgual = NeoUtils.safeBoolean(ewCliente.findValue("endCobrancaIgualCliente"));
		if(!endCobrancaIgual)
		{
			nenCob =  NeoUtils.safeOutputString(ewCliente.findValue("endCobranca.numero"));
			SigUfsCob = NeoUtils.safeOutputString(ewCliente.findValue("endCobranca.estadoCidade.estado.sigufs"));
			EndCliCob = NeoUtils.safeOutputString(ewCliente.findValue("endCobranca.endereco"));
			CplEndCob = NeoUtils.safeOutputString(ewCliente.findValue("endCobranca.complemento"));
			CepCliCob = NeoUtils.safeOutputString(ewCliente.findValue("endCobranca.cep"));
			CepIniCob = NeoUtils.safeOutputString(ewCliente.findValue("endCobranca.estadoCidade.cidade.cepini"));
			BaiCliCob = NeoUtils.safeOutputString(ewCliente.findValue("endCobranca.bairro"));
			CidCliCob = NeoUtils.safeOutputString(ewCliente.findValue("endCobranca.estadoCidade.cidade.nomcid"));
		}
		
		String CodPai = "1058";
		String TemEnt = "N";
		String TemCob = "N";
		String FonCli = NeoUtils.safeOutputString(ewCliente.findValue("telefone1"));
		String FonCl2 = NeoUtils.safeOutputString(ewCliente.findValue("telefone2"));
		String IntNet = NeoUtils.safeOutputString(ewCliente.findValue("email"));
		String FaxCli = NeoUtils.safeOutputString(ewCliente.findValue("fax"));
		
		/*
		 * verificar o mapeamento dos seguintes campos quanto a alteração:
		 * Devemos enviar um padrão ou buscamos do cliente atual?
		 * 
		 * Cliente Contribuinte ICMS
		 * Cliente e/ou Fornecedor
		 * Data/Valor limite Crédito
		 * Aceita Pedido Parte
		 */
		String SitCli = "A";
		String TriIcm = "S";
		
		String BloCre = "N";
		String TriIpi = "S";
		String TriPis = "S";
		String TriCof = "S";
		
		String RetCof = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.retcofins"));
		String RetCsl = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.retcsl"));
		String RetPis = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.retpis"));
		String RetIrf = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.retir"));
		
		if(movimentoContrato == 1 || movimentoContrato == 2 || movimentoContrato == 5)
		{
			if (tipoAlteracao == 1){
				CgcCpf = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.cpf") != null && !wContrato.findValue("novoCliente.cpf").equals("") ? wContrato.findValue("novoCliente.cpf") : wContrato.findValue("novoCliente.cnpj"));
			}else{
				CgcCpf = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.cgccpf"));
			}
			CodCli = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.codcli"));
			CodRam = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.codram"));
			TipMer = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.tipmer"));
			TipEmc = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.tipemc"));
			InsEst = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.insest"));
			
		}

		Map<String, String> params = new HashMap<String, String>();

		//obrigatorio

		if ( movimentoContrato != 5 && tipoAlteracao != 1 && wContrato.findValue("codigoCliente") == null ){
			params.put("CodCli", CodCli);
		}else if (movimentoContrato == 5 && tipoAlteracao == 1 && wContrato.findValue("codigoCliente") != null){
			params.put("CodCli", CodCli);
		}
		
		params.put("NomCli", NomCli);
		params.put("ApeCli", ApeCli);
		params.put("SigUfs", SigUfs);
		params.put("CgcCpf", CgcCpf);
		params.put("EndCli", EndCli);
		if(usuTipCli != null && !usuTipCli.trim().equals(""))	params.put("usu_tipcli", usuTipCli);
		if (codRtr != null) params.put("CodRtr", codRtr);
		params.put("TipCli", TipCli);
		params.put("TipEmc", TipEmc);

		//opcionais
		params.put("NenCli", NenCli);
		params.put("CodRam", CodRam);
		params.put("TipMer", TipMer);
		params.put("CliCon", CliCon);
		if(InsEst != null && !InsEst.trim().equals(""))	params.put("InsEst", InsEst); else params.put("InsEst", "ISENTO");
		params.put("InsMun", InsMun);
		params.put("CplEnd", CplEnd);
		params.put("CepCli", CepCli);
		params.put("CepIni", CepIni);
		params.put("BaiCli", BaiCli);
		params.put("CidCli", CidCli);
		params.put("CodPai", CodPai);
		params.put("TemEnt", TemEnt);
		params.put("TemCob", TemCob);
		params.put("FonCli", FonCli);
		params.put("FonCl2", FonCl2);
		params.put("FaxCli", FaxCli);
		params.put("IntNet", IntNet);
		params.put("SitCli", SitCli);
		params.put("BloCre", BloCre);
		params.put("TriIcm", TriIcm);
		params.put("TriIpi", TriIpi);
		params.put("TriPis", TriPis);
		params.put("TriCof", TriCof);
		
		params.put("RetCof", RetCof);
		params.put("RetCsl", RetCsl);
		params.put("RetPis", RetPis);
		params.put("RetIrf", RetIrf);
		
		//endereço de entrega
		params.put("NenEnt", nenEnt);
		params.put("EndEnt", EndCliEnt); 
		params.put("CplEnt", CplEndEnt);
		params.put("CepEnt", CepCliEnt);
		params.put("IniEnt", CepIniEnt);
		params.put("CidEnt", CidCliEnt);
		params.put("EstEnt", SigUfsEnt);
		params.put("BaiEnt", BaiCliEnt);
		
		//endereço de cobrança
		params.put("NenCob", nenCob);
		params.put("EndCob", EndCliCob); 
		params.put("CplCob", CplEndCob);
		params.put("CepCob", CepCliCob);
		params.put("IniCob", CepIniCob);
		params.put("CidCob", CidCliCob);
		params.put("EstCob", SigUfsCob);
		params.put("BaiCob", BaiCliCob);

		try
		{
			ContractSID sid = new ContractSID("EXESENHA", "SID.Cli.Gravar", "", codEmp, codFil);
			String retorno = sid.getContent(params);
			
			Long idCliente = 0L;
			if(retorno != null && retorno.contains("ERRO"))
			{
				
				if ( retorno.indexOf("ERRO") >-1 && (retorno.indexOf("Cliente") == -1 && retorno.indexOf("já") ==-1) ){
					throw new Exception(retorno);
				}
				if (sobrescreverDados && (retorno.indexOf("Cliente") > -1 && retorno.indexOf("já") > -1)){
					idCliente = NeoUtils.safeLong(retorno.substring(retorno.indexOf("Cliente")+8,retorno.indexOf("já")-1));
					return NeoUtils.safeOutputString(idCliente);
				}else{
					throw new Exception("sid.cli.Gavar: " + retorno );
				}
				
			}
			if (retorno != null && (new Integer(retorno)).toString() != null)
			{
				codCli = (new Integer(retorno)).toString();
				ewCliente.setValue("codigoCliente", codCli);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao executar o SID.Cli.Gravar."+e.getMessage());
			
		}
		return codCli;
	}

	public static void sidClientDefinitions(NeoObject contrato, String codCli, int codChamada, String porSi1, String codEmp, String codFil, String codBan, String codAge, String ccbCli ) throws Exception
	{
		Map<String, String> params = new HashMap<String, String>();

		EntityWrapper ewContrato = new EntityWrapper(contrato);

		String CodRep = NeoUtils.safeOutputString(ewContrato.findValue("representante.codrep"));
		String CodCpg = NeoUtils.safeOutputString(ewContrato.findValue("condicaoPagamento.codcpg"));

		//obrigatório
		params.put("CodCli", codCli);
		params.put("CodRep", CodRep);
		params.put("CodCpg", CodCpg);

		//Opcionais
		params.put("PorSi1", porSi1);
		params.put("CodBan", codBan);
		params.put("CodAge", codAge);
		params.put("CcbCli", ccbCli);

		try
		{
			ContractSID sid = new ContractSID("EXESENHA", "SID.Cli.GravarDefinicoes", "", codEmp, codFil);
			String retorno = sid.getContent(params);

			//Caso não haja definicação de histórico para o representante
			// Valida o parametro codChamada para evitar Looping
			if (retorno.contains("ERRO:") && retorno.contains("categoria \"REP\"") ){
				ContratoLogUtils.logInfo("Não foi possível gravar as definições do cliente, pois, o representante "+CodRep+", para a empresa "+codEmp+", não é da categoria \"REP\"");
				throw new Exception("Não foi possível gravar as definições do cliente, pois, o representante "+CodRep+", para a empresa "+codEmp+", não é da categoria \"REP\"");
			}
			if (retorno.contains("ERRO: Definições do histórico do representante") && codChamada != 1)
			{
				//Grava as definições do representante
				/*
				 *  TODO Definido com a grazi e ficou acertado que utilizaremos uma empresa e 
				 *  regiao padrao para definicao do representante. A principio estaremos cadastrando
				 *  todos os representantes na empresa 1 e regiáo 1
				 */
				sidSellerDefinitions(CodRep, codEmp, codFil);

				//Chama novamente o cadastro de definições do cliente após gravar as definições do representante
				sidClientDefinitions(contrato, codCli, 1, porSi1, codEmp, codFil, codBan, codAge, ccbCli );
			}
		}
		catch(WorkflowException e)
		{
			ContratoLogUtils.logInfo("Erro ao cadastrar definições do cliente via SID");
			e.printStackTrace();
			throw (WorkflowException) e;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao cadastrar definições do cliente via SID."+e.getMessage());
		}
	}

	public static void sidClientContact(NeoObject cliente, String CodCli, String codEmp, String codFil) throws Exception
	{
		EntityWrapper ewCliente = new EntityWrapper(cliente);
		//List<NeoObject> listaContato = (List<NeoObject>) ewCliente.getValue("contatos");
		String retorno = "";
		try
		{
			String SeqCto = "";
			String NomCto = NeoUtils.safeOutputString(ewCliente.findValue("nomeContato"));
			String NivCto = "";
			String CarCto = "";
			String FonCto = "";
			String RamCto = "";
			String FaxCto = "";
			String IntNet = "";

			Map<String, String> params = new HashMap<String, String>();

			//obrigatorio
			params.put("CodCli", CodCli);

			//opcional
			params.put("SeqCto", SeqCto);
			params.put("NomCto", NomCto);
			params.put("CodNiv", NivCto);
			params.put("CarCto", CarCto);
			params.put("FonCto", FonCto);
			params.put("RamCto", RamCto);
			params.put("FaxCto", FaxCto);
			params.put("IntNet", IntNet);

			ContractSID sid = new ContractSID("EXESENHA", "SID.Cli.GravarContato", "", codEmp, codFil);

			retorno = sid.getContent(params);
			
			if (retorno.contains("ERRO:")) {
				throw new WorkflowException("Erro ao cadastrar endereco de cobranca. " + retorno);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao executar o SID.Cli.GravarContato." + retorno);
		}
	}

	public static void sidSellerDefinitions(String CodRep, String CodEmp, String codFil) throws Exception
	{
		try
		{
			Map<String, String> params = new HashMap<String, String>();

			//obrigatorio
			params.put("CodRep", CodRep);
			params.put("CodEmp", CodEmp);
			params.put("CodRve", "1"); //codigos existentes? SUL, 1, 2, 3, 4 e 5
			params.put("IpiCom", "N");
			params.put("IcmCom", "N");
			params.put("SubCom", "N");
			params.put("FreCom", "N");
			params.put("SegCom", "N");
			params.put("EmbCom", "N");
			params.put("EncCom", "N");
			params.put("OutCom", "N");
			params.put("DarCom", "N");
			params.put("ComPri", "N");
			params.put("CriRat", "0");
			params.put("ConEst", "N");
			params.put("CatRep", "REP");
			params.put("CslCom", "N");
			params.put("InsCom", "N");
			params.put("IssCom", "N");
			params.put("CofCom", "N");
			params.put("PisCom", "N");
			params.put("IrfCom", "N");
			params.put("OurCom", "N");
			params.put("PifCom", "N");
			params.put("CffCom", "N");
			params.put("RecAdc", "N");
			params.put("RecAoc", "N");
			params.put("RecPcj", "N");
			params.put("RecPcm", "N");
			params.put("RecPcc", "N");
			params.put("RecPce", "N");
			params.put("RecPco", "N");

			ContractSID sid = new ContractSID("EXESENHA", "SID.Rep.GravarDefinicoes", "", CodEmp, codFil);

			String retorno = sid.getContent(params);
			
			System.out.println("[FLUXO CONTRATOS]- Retorno: " + retorno);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao executar o SID.Rep.GravarDefinicoes. Empresa: " + CodEmp + ", Filial: " + codFil + " err." + e.getMessage());
		}
	}
	
	public static void sidChangeEmpFil(String CodEmp, String CodFil) throws Exception
	{
		try
		{
			Map<String, String> params = new HashMap<String, String>();

			//obrigatorio
			params.put("CodEmp", CodEmp);
			params.put("CodFil", CodFil);
			
			ContractSID sid = new ContractSID("EXESENHA", "SID.Srv.AltEmpFil", "", CodEmp, CodFil);

			String retorno = sid.getContent(params);
			
			System.out.println("[FLUXO CONTRATOS]-Retorno: " + retorno);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("sidChangeEmpFil." + e.getMessage());
			/*e.printStackTrace();
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				throw new WorkflowException(e.getMessage());
			}*/
		}
	}
	
	public static String alteraRazaoSocial(Long codCli, String razaoOld, String razaoNew){
		String retSID = null;
		String queryCli = "update E085cli set NomCli = '"+razaoNew+"' where codCli = "+codCli;
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(queryCli);
		//FIXME SÓ EM PRODUÇÂO
		/*
		if (query.executeUpdate() > 0){
			retSID = "OK";
		}*/
		retSID = "OK";
		return retSID;
	}
	
	
	
	/*
	 	SeqCob	Seqüência do endereço de cobrança
		EenCob	Código do endereço de cobrança do cliente
		CepCob	CEP do endereço de cobrança
		EndCob	Endereço de cobrança
		NenCob	Número do endereço de cobrança
		CplCob	Complemento do endereço de cobrança
		BaiCob	Bairro do endereço de cobrança	
		CidCob	Cidade do endereço de cobrança
		EstCob	Estado do endereço de cobrança
		CgcCob	Número do CNPJ/CPF de cobrança
		SitReg	Situação do endereço de cobrança
	 */
	
	public static String sidClientEnderecoCobranca(String codEmp, String codFil, String codcli, Collection<NeoObject> listaEnderecosCob ) throws Exception
	{
		Map<String, String> params = new HashMap<String, String>();

		
		//obrigatório
		params.put("Codcli", codcli);
		for (NeoObject end : listaEnderecosCob){
			//Opcionais
			EntityWrapper wEnd = new EntityWrapper(end);
			
			String seqCob =  NeoUtils.safeOutputString(wEnd.findValue("seqCob"));
			
			params.put("Seqcob", NeoUtils.safeOutputString(wEnd.findValue("seqCob")) );
			params.put("CepCob", NeoUtils.safeOutputString(wEnd.findValue("cep")) );
			params.put("EndCob", NeoUtils.safeOutputString(wEnd.findValue("endereco")) );
			params.put("NenCob", NeoUtils.safeOutputString(wEnd.findValue("numero")) );
			params.put("CplCob", NeoUtils.safeOutputString(wEnd.findValue("complemento")) );
			params.put("BaiCob", NeoUtils.safeOutputString(wEnd.findValue("bairro")) );
			
			NeoObject estadoCidade = (NeoObject) wEnd.findValue("estadoCidade");
			EntityWrapper wEstadoCidade = new EntityWrapper(estadoCidade);
			
			String cidCob = NeoUtils.safeOutputString(wEstadoCidade.findValue("cidade.nomcid")); // novoCliente.enderecosCobranca.estadoCidade.cidade.nomcid // cidade
			String estCob = NeoUtils.safeOutputString(wEstadoCidade.findValue("estado.sigufs")); // novoCliente.enderecosCobranca.estadoCidade.estado.sigufs // estado
			
			params.put("CidCob", cidCob);
			params.put("EstCob", estCob); 
			params.put("SitReg", "A");
			
			try
			{
				ContractSID sid = null;
				String retorno = "";
				ContratoLogUtils.logInfo("seqCob="+ seqCob + " -  codcli=" +  codcli + ", existe? " + OrsegupsContratoUtils.existeEnderecoCobranca(codcli, seqCob) );
				if (!"".equals(seqCob) && !OrsegupsContratoUtils.existeEnderecoCobranca(codcli, seqCob) ){
					sid = new ContractSID("EXESENHA", "SID.Cli.GravarEndCobranca", "", codEmp, codFil);
					retorno = sid.getContent(params);
					ContratoLogUtils.logInfo("SID.Cli.GravarEndCobranca: " + retorno);
				}
				
				if (retorno.contains("ERRO:")) {
					throw new Exception("Erro ao cadastrar endereco de cobranca. " + retorno);
				}else{
					//return retorno;
				}
			}
			catch(WorkflowException e)
			{
				throw (WorkflowException) e;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new Exception("sidClientEnderecoCobranca." + e.getMessage());
			}
			
		}
		
		return "OK";
		



	}
	
}
