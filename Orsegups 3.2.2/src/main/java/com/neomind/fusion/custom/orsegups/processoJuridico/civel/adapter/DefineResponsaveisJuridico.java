package com.neomind.fusion.custom.orsegups.processoJuridico.civel.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class DefineResponsaveisJuridico implements AdapterInterface {
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		
		String mensagem = "Erro ao definir os responsáveis pelo Escritório e/ou Aprovação.";
		
		NeoPaper papelEscritorio = OrsegupsUtils.getPaper("j004EscritorioGuedes");
		NeoPaper papelAprovacao = OrsegupsUtils.getPaper("j004CivelAprovacao");
		NeoPaper papelFinanceiro = OrsegupsUtils.getPaper("j004CivelFinanceiro");
		
		try {
			
			if(NeoUtils.safeIsNotNull(papelEscritorio) && NeoUtils.safeIsNotNull(papelAprovacao) && NeoUtils.safeIsNotNull(papelFinanceiro)) {
				processEntity.setValue("responsavelEscritorio", papelEscritorio);
				processEntity.setValue("responsavelAprovacao", papelAprovacao);				
				processEntity.setValue("responsavelFinanceiro", papelFinanceiro);				
			}else {
				System.out.println(mensagem+" "+origin.getCode()+" - "+this.getClass().getSimpleName()+" "+NeoDateUtils.safeDateFormat(new GregorianCalendar()));
				throw new WorkflowException(mensagem);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(mensagem+" "+origin.getCode()+" - "+this.getClass().getSimpleName()+" "+NeoDateUtils.safeDateFormat(new GregorianCalendar()));
			throw new WorkflowException(mensagem);
		}
		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}
	

}
