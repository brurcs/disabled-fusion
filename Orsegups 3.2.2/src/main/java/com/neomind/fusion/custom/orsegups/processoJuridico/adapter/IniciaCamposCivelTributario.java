package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoRole;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class IniciaCamposCivelTributario implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

			NeoRole papelEscritorioCivel = (NeoRole) PersistEngine.getObject(NeoRole.class, new QLEqualsFilter("code", "j002Civel"));
			NeoRole j002GuedesPintoConAssCivel = (NeoRole) PersistEngine.getObject(NeoRole.class, new QLEqualsFilter("code", "j002GuedesPintoConAssCivel "));

			NeoObject noEscritorio = PersistEngine.getObject(AdapterUtils.getEntityClass("j002EscAdvPro"), new QLEqualsFilter("nome", "Guedes Pinto Consultores Associados - SC"));

			processEntity.setValue("papelRaiaEscRes", papelEscritorioCivel);
			processEntity.setValue("papelRaiaPoolJurEsc", j002GuedesPintoConAssCivel);
			processEntity.setValue("temPgo", true);
			processEntity.setValue("grauProcesso", "1º Grau");
			processEntity.setValue("escRecPro", noEscritorio);
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe IniciaCamposCivelTributario do fluxo J002.");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
