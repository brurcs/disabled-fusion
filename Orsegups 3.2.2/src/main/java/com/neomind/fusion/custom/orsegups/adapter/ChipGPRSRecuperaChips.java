package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class ChipGPRSRecuperaChips implements AdapterInterface 
{
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			String nomeFonteDados = "SAPIENS";
	
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			
			Long codEmp = (Long) processEntity.findValue("postoSapiens.usu_codemp");
			Long codFil = (Long) processEntity.findValue("postoSapiens.usu_codfil");
			Long numCtr = (Long) processEntity.findValue("postoSapiens.usu_numctr");
			Long numPos = (Long) processEntity.findValue("postoSapiens.usu_numpos");
			String codSer = (String) processEntity.findValue("postoSapiens.usu_codser");
				
			StringBuffer sqlSelect = new StringBuffer();
			sqlSelect.append(" SELECT usu_iccid FROM USU_T160CHP ");
			sqlSelect.append(" WHERE usu_codemp = ? AND usu_codfil = ? AND usu_numctr = ? AND usu_numpos = ? AND usu_codser = ?");
			
			PreparedStatement stSelect = connection.prepareStatement(sqlSelect.toString());
			stSelect.setLong(1, codEmp);
			stSelect.setLong(2, codFil);
			stSelect.setLong(3, numCtr);
			stSelect.setLong(4, numPos);
			stSelect.setString(5, codSer);
			
			ResultSet rs = stSelect.executeQuery();
			
			processEntity.findField("chipsGPRS").removeValues();
			
			while(rs.next()) {							
				String iccid = rs.getString("usu_iccid");
				
				QLEqualsFilter qf = new QLEqualsFilter("iccID",iccid);
				Collection<NeoObject> chipGprs = PersistEngine.getObjects(AdapterUtils.getEntityClass("TELECOMChipsGPRS"), qf);
				
				for (NeoObject chip : chipGprs)
				{
					processEntity.findField("chipsGPRS").addValue(chip);
				}
				
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel recuperar as informação no Sapiens.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
