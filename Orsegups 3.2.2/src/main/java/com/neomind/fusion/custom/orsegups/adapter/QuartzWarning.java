package com.neomind.fusion.custom.orsegups.adapter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.util.NeoDateUtils;

public class QuartzWarning
{
	
	private static final Log log = LogFactory.getLog(QuartzWarning.class);
	
	public void run(Exception e)
	{
	
		e.printStackTrace();
		System.out.println("ERRO - QUARTZ NAO INICIADO !");
		String errors = "";
		try{
			
			StringWriter erros = new StringWriter();
			e.printStackTrace(new PrintWriter(erros));
			try {
				erros.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			abrirTarefaSimples(errors);
			
		}catch(Exception ex){
			log.error("Erro ao abrir tarefa simples de alerta");
			ex.printStackTrace();
		}
		
		
		
	}
	
	
	public static void abrirTarefaSimples(String errors)
	{
		GregorianCalendar prazo = OrsegupsUtils.getNextWorkDay(new GregorianCalendar());
		prazo.set(GregorianCalendar.HOUR,23);
		prazo.set(GregorianCalendar.MINUTE,59);
		prazo.set(GregorianCalendar.SECOND,59);
		
		
		//String solicitante = "graziela.martins" ;
		String solicitante = "thiago.coelho" ;
		String executor = "danilo.silva";
		
		String origem = "2";
		
		String titulo = " Erro de inicialização do Quartz " + NeoDateUtils.safeDateFormat(new GregorianCalendar());
		
		String descricao = " Ocorreu um problema com a inicialização do Quartz do fusion. Favor verificar. ";

		descricao += "\n StackTrace: " + errors; 	
		
		
		com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples tarefaSimples = new com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples();
		String tarefa = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, origem, (String) null, OrsegupsUtils.getNextWorkDay(prazo));
		log.warn("Tarefa aberta:" + tarefa);
	}
}
