package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoUtils;

public class AlteraStatusDeslocamentoMapas implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(AlteraStatusDeslocamentoMapas.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

	
		Connection conn = PersistEngine.getConnection("SIGMA90");
		StringBuilder sqlOrdemServico = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AlteraStatusDeslocamentoMapas");
		log.warn("##### INICIAR AGENDADOR DE ALTERAÇÃO STATUS MAPAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		
		
		try
		{
			//PESQUISA POR INCONSISTENCIAS
		
			sqlOrdemServico.append(" SELECT h2.CD_VIATURA, v.NM_PLACA ");
			sqlOrdemServico.append(" FROM HISTORICO h2 ");
			sqlOrdemServico.append(" INNER JOIN VIATURA v ON v.CD_VIATURA = h2.CD_VIATURA   ");
			sqlOrdemServico.append(" WHERE FG_STATUS = ? ");
			sqlOrdemServico.append(" AND NOT EXISTS (SELECT CD_HISTORICO  ");
			sqlOrdemServico.append("  FROM HISTORICO h9 ");
			sqlOrdemServico.append("  WHERE h9.FG_STATUS = ? ");
			sqlOrdemServico.append(" AND h2.CD_VIATURA = h9.CD_VIATURA) ");
		
			
			pstm = conn.prepareStatement(sqlOrdemServico.toString());
			pstm.setString(1,"2");
			pstm.setString(2,"9");
			rs = pstm.executeQuery();
		

			while (rs.next())
			{
				
				StringBuffer sql = new StringBuffer();
				sql.append("UPDATE HISTORICO ");
				sql.append("SET FG_STATUS = ?, ");
				sql.append("WHERE CD_VIATURA = ? ");
				sql.append("AND NM_PLACAL = ? ");
				
				pstm = conn.prepareStatement(sqlOrdemServico.toString());
				pstm.setString(1,rs.getString("100"));
				pstm.setString(2,rs.getString("CD_VIATURA"));
				pstm.setString(2,rs.getString("NM_PLACAL"));
				pstm.executeUpdate();
				log.warn("##### EXECUTAR AGENDADOR DE ALTERAÇÃO STATUS MAPAS - VIATURA : "+rs.getString("CD_VIATURA")+" - PLACA : "+rs.getString("NM_PLACAL")+" - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
			
			}
			log.warn("##### CONCLUIR  AGENDADOR DE ALTERAÇÃO STATUS MAPAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE ALTERAÇÃO STATUS MAPAS:"+e.getMessage()+ " Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{	
			
			try {
				rs.close();
				pstm.close();
				conn.close();
			} catch (SQLException e) {
				log.error("##### ERRO AGENDADOR DE ALTERAÇÃO STATUS MAPAS:"+e.getMessage()+ " Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
				e.printStackTrace();
			}
			
			log.warn("##### FINALIZAR  AGENDADOR DE ALTERAÇÃO STATUS MAPAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
		}
	}
	


