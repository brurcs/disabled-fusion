package com.neomind.fusion.custom.orsegups.utils;

public class FaltaEfetivoVO
{
	private Long codreg;
	private Long codccu;
	private Long numloc;
	private Long taborg;
	private String lotorn;
	private String nomloc;
	private int qtdCon;
	private int qtdFun;
	private String nomFun;
	private String nomReg;
	
	public int getQtdCon()
	{
		return qtdCon;
	}
	public void setQtdCon(int qtdCon)
	{
		this.qtdCon = qtdCon;
	}
	public int getQtdFun()
	{
		return qtdFun;
	}
	public void setQtdFun(int qtdFun)
	{
		this.qtdFun = qtdFun;
	}
	public Long getCodreg()
	{
		return codreg;
	}
	public void setCodreg(Long codreg)
	{
		this.codreg = codreg;
	}
	public Long getCodccu()
	{
		return codccu;
	}
	public void setCodccu(Long codccu)
	{
		this.codccu = codccu;
	}
	public Long getNumloc()
	{
		return numloc;
	}
	public void setNumloc(Long numloc)
	{
		this.numloc = numloc;
	}
	public Long getTaborg()
	{
		return taborg;
	}
	public void setTaborg(Long taborg)
	{
		this.taborg = taborg;
	}
	public String getLotorn()
	{
		return lotorn;
	}
	public void setLotorn(String lotorn)
	{
		this.lotorn = lotorn;
	}
	public String getNomloc()
	{
		return nomloc;
	}
	public void setNomloc(String nomloc)
	{
		this.nomloc = nomloc;
	}
	public String getNomFun()
	{
		return nomFun;
	}
	public void setNomFun(String nomFun)
	{
		this.nomFun = nomFun;
	}
	
	public String getNomReg()
	{
	    	return nomReg;
	}
	
	public void setNomReg(String nomReg)
	{
	    	this.nomReg = nomReg;
	}
}
