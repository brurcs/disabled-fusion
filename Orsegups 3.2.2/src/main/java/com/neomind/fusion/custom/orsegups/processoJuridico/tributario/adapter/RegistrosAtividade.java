package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoRole;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter-RegistrosAtividade

public class RegistrosAtividade implements AdapterInterface, TaskAssignerEventListener, TaskFinishEventListener
{
	@Override
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String mensagemErro = "Erro!";
		
		try
		{
		    	Integer iTipoProcesso = 0;
			NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
	    	    	if (tipoProcesso != null) {
	    	    	    EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
	    	    	    iTipoProcesso = Integer.parseInt(wTipoProcesso.getValue("codigo").toString());
	    	    	}
	    	    	
			List<NeoObject> registroAtividade = (List<NeoObject>) processEntity.getValue("j002RegistroHistoricoAtividade");
			List<NeoObject> registroAtividadeArquivo = (List<NeoObject>) processEntity.getValue("j002RegistroArquivo");
			List<NeoObject> listaGTCComprovantePgto = (List<NeoObject>) processEntity.getValue("ListGTCComprovante");
			List<NeoObject> listaGTCLancar = (List<NeoObject>) processEntity.getValue("listaGtcsInfo");

			String responsavel = origin.returnResponsible();
			GregorianCalendar dataAcao = new GregorianCalendar();
			String atividade = origin.getActivityName().toString();
			InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002RegistroHistoricoAtividade");
			NeoObject objRegAti = insRegAti.createNewInstance();
			EntityWrapper wRegAti = new EntityWrapper(objRegAti);
			wRegAti.setValue("dataAcao", dataAcao);
			wRegAti.setValue("atividade", atividade);
			wRegAti.setValue("responsavel", responsavel);

			//validar dias uteis para o vencimento.
			for (NeoObject gtcLancar : listaGTCLancar)
			{
				EntityWrapper wLancar = new EntityWrapper(gtcLancar);
				List<NeoObject> listaParcelas = (List<NeoObject>) wLancar.getValue("parcelas");

				for (NeoObject parcela : listaParcelas)
				{
					EntityWrapper wParcela = new EntityWrapper(parcela);
					GregorianCalendar datVen = (GregorianCalendar) wParcela.getValue("vencimento");
					if (!OrsegupsUtils.isWorkDay(datVen))
					{
						if (mensagemErro.equals("Erro!"))
						{
							mensagemErro = "<br>";
						}
						mensagemErro = mensagemErro + "A data " + NeoDateUtils.safeDateFormat(datVen, "dd/MM/yyyy") + ", referente há parcela " + wParcela.getValue("nParcela") + " com valor " + wParcela.getValue("valorParcela") + " não é dia útil.<br>";
					}
				}

			}

			if (!mensagemErro.equals("") && !mensagemErro.equals("Erro!"))
			{
				throw new WorkflowException(mensagemErro);
			}

			if (origin.getActivityName().toString().contains("Finalizar Processo jurídico"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsFinalizarProcesso"));
				wRegAti.setValue("descricao", descricao);
				System.out.println("Finalizar Processo jurídico");
			}

			if (origin.getActivityName().toString().contains("Considerações finais sobre o processo"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsFinalProcesso"));
				wRegAti.setValue("descricao", descricao);
				System.out.println("Considerações finais sobre o processo");
			}

			if (origin.getActivityName().toString().contains("Lançar GTCs"))
			{
				String descricao = "GTC(s) lançada(s) com sucesso! Aguardando comprovante(s) de pagamento.";
				wRegAti.setValue("descricao", descricao);
				System.out.println("Exigir Sustentação Oral");
			}

			if (origin.getActivityName().toString().contains("Exigir Sustentação Oral"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsSusOra"));
				wRegAti.setValue("descricao", descricao);
				System.out.println("Exigir Sustentação Oral");
			}

			if (origin.getActivityName().toString().contains("Definir Pedidos"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsPro"));
				wRegAti.setValue("descricao", descricao);
				if (iTipoProcesso == 3) {
				    atividade = "Abrir Processo Tributário / " + atividade;
				} else if (iTipoProcesso == 2) {
				    atividade = "Abrir Processo Cível / " + atividade;
				} else {
				    atividade = "Abrir Processo Trabalhista / " + atividade;
				}
				wRegAti.setValue("atividade", atividade);

				NeoPaper papelJuridico = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "j002JuridicoOrsegups"));
				processEntity.setValue("papelRaiaJurOrs", papelJuridico);

				System.out.println("Definir Pedidos");
			}

			if (origin.getActivityName().toString().contains("Aguardar Finalização das tarefas"))
			{
				wRegAti.setValue("descricao", "Aguardando Finalização da(s) tarefa(s) do(s) pedido(s)");
				System.out.println("Aguardar Finalização das tarefas");
			}

			if (origin.getActivityName().toString().contains("Elaborar Parecer Administrativo"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsParAdm"));
				wRegAti.setValue("descricao", descricao);
				processEntity.setValue("obsParAdm", "");
				System.out.println("Elaborar Parecer Administrativo");
			}

			if (origin.getActivityName().toString().contains("Elaborar Petição"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsGerAprDef"));
				wRegAti.setValue("descricao", descricao);
				processEntity.setValue("obsGerAprDef", "");
				System.out.println("Elaborar Petição");

				//Inicio - Gerar a quantidade de meses a cobrar
				NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
				EntityWrapper wColaborador = null;
				GregorianCalendar datDem = null;
				String semData = "31/12/1900";
				GregorianCalendar datAdm = null;

				if (colaborador != null)
				{
					wColaborador = new EntityWrapper(colaborador);
					if (wColaborador.getValue("datafa") != null && !semData.equals(NeoDateUtils.safeDateFormat((GregorianCalendar) wColaborador.getValue("datafa"), "dd/MM/yyyy")))
					{
						datDem = (GregorianCalendar) wColaborador.getValue("datafa");
					}
					else
					{
						datDem = (GregorianCalendar) processEntity.getValue("dtAfa");
						if (datDem == null)
						{
							datDem = new GregorianCalendar();
						}

					}
					datAdm = (GregorianCalendar) wColaborador.getValue("datadm");

					BigDecimal diferencaMeses = null;

					List<NeoObject> pedidos = null;
					if (String.valueOf(processEntity.getValue("grauProcesso")).contains("1º"))
					{
						pedidos = (List<NeoObject>) processEntity.getValue("lisPed");
					}
					else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("2º"))
					{
						pedidos = (List<NeoObject>) processEntity.getValue("lisPedSegGrau");
					}
					else
					{
						pedidos = (List<NeoObject>) processEntity.getValue("lisPedSegGrau");
					}

					for (NeoObject pedido : pedidos)
					{
						EntityWrapper wPedido = new EntityWrapper(pedido);
						if (wPedido.getValue("qtdMesTrab") == null || (long) wPedido.getValue("qtdMesTrab") == 0)
						{
							diferencaMeses = new BigDecimal((datAdm.getTime().getTime() - datDem.getTime().getTime()) / (1000 * 60 * 60 * 24) / 30 * -1);
							wPedido.setValue("qtdMesTrab", Long.parseLong(diferencaMeses.toString()));
						}
					}

					if (String.valueOf(processEntity.getValue("grauProcesso")).contains("1º"))
					{
						processEntity.setValue("lisPed", pedidos);
					}
					else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("2º"))
					{
						processEntity.setValue("lisPedSegGrau", pedidos);
					}
					else
					{
						processEntity.setValue("lisPedSegGrau", pedidos);
					}
				}
				// Fim - Gerar a quantidade de meses a cobrar

				List<NeoObject> arquivosDefesa = (List<NeoObject>) processEntity.getValue("lisAnexosDef");
				for (NeoObject arquivoDefesa : arquivosDefesa)
				{
					EntityWrapper wArquivoDefesa = new EntityWrapper(arquivoDefesa);
					NeoObject objReArq = AdapterUtils.createNewEntityInstance("j002RegistroArquivos");
					EntityWrapper wRegArq = new EntityWrapper(objReArq);

					wRegArq.setValue("dtAnexo", new GregorianCalendar());
					wRegArq.setValue("titulo", String.valueOf(wArquivoDefesa.getValue("titulo")));
					wRegArq.setValue("respAnexo", responsavel);
					wRegArq.setValue("anexo", (NeoFile) wArquivoDefesa.getValue("arquivo"));

					PersistEngine.persist(objReArq);
					registroAtividadeArquivo.add(objReArq);
				}
				if (processEntity.findGenericValue("realizarRecurso") != null) {
				    if ((boolean)processEntity.findGenericValue("realizarRecurso") == false) {
					abrirTarefaSimplesSemRecurso(processEntity);
				    }
				}
				//Fim - Gerar a quantidade de meses a cobrar
			}
			if (origin.getActivityName().toString().contains("Aprovar Petição"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsDefApr"));
				wRegAti.setValue("descricao", descricao);
				processEntity.setValue("obsDefApr", "");
				System.out.println("Aprovar Petição");
			}

			if (origin.getActivityName().toString().contains("Protocolar Petição"))
			{
				String descricao = String.valueOf(processEntity.getValue("observacao"));
				wRegAti.setValue("descricao", descricao);
				processEntity.setValue("observacao", "");
				processEntity.setValue("listaAcaoPro", null);
				System.out.println("Protocolar Petição");

			}

			if (origin.getActivityName().toString().contains("Movimentar Processo"))
			{
			    	//NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador"); 			    
			    	//EntityWrapper wc = new EntityWrapper(colaborador);
			    	//System.out.println(processEntity.getValue("codccuInformado")+" - "+wc.getValue("numemp"));
			    	QLRawFilter raw = new QLRawFilter("codccu = " + processEntity.getValue("codccuInformado"));
			    	NeoObject sapiensCCU = PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), raw);
			    	if (sapiensCCU == null) {
			    	    mensagemErro = "Centro de Custo (Manual) é Inválido";
				    throw new WorkflowException(mensagemErro);
			    	} else {
			    	    EntityWrapper e  = new EntityWrapper(sapiensCCU);
			    	    //System.out.println(e.findValue("acerat"));
			    	    if (e.findValue("acerat").equals("N")) {
			    		mensagemErro = "Centro de Custo (Manual) está Inativo";
					throw new WorkflowException(mensagemErro);
			    	    } else {
			    		if (e.findValue("acerat").equals("S")) {
			    		    
        			    		String descricao = String.valueOf(processEntity.getValue("obsMovPro"));        
        					wRegAti.setValue("descricao", descricao);
        					processEntity.setValue("obsMovPro", "");
        					processEntity.setValue("obsSusOra", "");
        					NeoObject acao = (NeoObject) processEntity.getValue("listaAcaoPro");
        					EntityWrapper wAcao = new EntityWrapper(acao);        
        					wRegAti.setValue("atividade", atividade + " - <b>Ação : " + wAcao.getValue("acao") + " </b>");
        					//System.out.println("Movimentar Processo");
			    		}
			    	    }
			    	}
			}

			if (origin.getActivityName().toString().contains("Enviar comprovante de pagamento"))
			{
				for (NeoObject comprovantes : listaGTCComprovantePgto)
				{
					EntityWrapper wComprovanente = new EntityWrapper(comprovantes);
					if (wComprovanente.getValue("comprovante") == null)
					{
						mensagemErro = "É Preciso anexar o comprovante de pagamento em todas as GTCs";
						throw new WorkflowException(mensagemErro);
					}else{
					    NeoFile file = (NeoFile)wComprovanente.getValue("comprovante");
					    file.setName(file.getName().replaceAll(",", "."));
					}
				}
				for (NeoObject comprovantes : listaGTCComprovantePgto)
				{
					EntityWrapper wGtcAtualizar = new EntityWrapper(comprovantes);
					QLGroupFilter groupFilter = new QLGroupFilter("AND");
					QLFilter qlNeoId = new QLEqualsFilter("neoIdGtc", (Long) wGtcAtualizar.getValue("neoId"));
					groupFilter.addFilter(qlNeoId);

					List<NeoObject> objsHistorico = PersistEngine.getObjects(AdapterUtils.getEntityClass("j002HistoricoGTCs"), groupFilter);
					if (objsHistorico != null && objsHistorico.size() > 0)
					{
						EntityWrapper wRegistroHistorico = new EntityWrapper(objsHistorico.get(0));
						wRegistroHistorico.setValue("StatusGTC", "Finalizado");
						PersistEngine.persist(objsHistorico.get(0));
					}
				}
				String descricao = "Anexado comprovante de pagamento na(s) GTC(s).";
				wRegAti.setValue("descricao", descricao);
				listaGTCComprovantePgto.clear();
				processEntity.setValue("ListGTCComprovante", listaGTCComprovantePgto);

			}

			//			if (!origin.getActivityName().toString().contains("Aguardar Finalização das tarefas"))
			//			{
			PersistEngine.persist(objRegAti);
			registroAtividade.add(objRegAti);

			processEntity.setValue("j002RegistroHistoricoAtividade", registroAtividade);

			//Historico de arquivos
			List<NeoObject> arquivos = (List<NeoObject>) processEntity.getValue("j002LisArq");
			if (arquivos != null && !arquivos.isEmpty())
			{
				for (NeoObject arquivo : arquivos)
				{
					EntityWrapper wArquivo = new EntityWrapper(arquivo);
					InstantiableEntityInfo insRegArq = AdapterUtils.getInstantiableEntityInfo("j002RegistroArquivos");
					NeoObject objReArq = insRegArq.createNewInstance();
					EntityWrapper wRegArq = new EntityWrapper(objReArq);

					wRegArq.setValue("dtAnexo", new GregorianCalendar());
					wRegArq.setValue("titulo", String.valueOf(wArquivo.getValue("titulo")));
					wRegArq.setValue("respAnexo", origin.returnResponsible());
					wRegArq.setValue("anexo", (NeoFile) wArquivo.getValue("arquivo"));

					PersistEngine.persist(objReArq);

					registroAtividadeArquivo.add(objReArq);
				}
			}

			processEntity.setValue("j002LisArq", null);
			processEntity.setValue("lisAnexosDef", null);
			processEntity.setValue("j002RegistroArquivo", registroAtividadeArquivo);
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe RegistrosAtividade do fluxo J002.");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage()+"#####"+mensagemErro);
		}

	}
	
	private void abrirTarefaSimplesSemRecurso(EntityWrapper processEntity) {
	    
        	String solicitante = "dilmoberger";
        	String executor = "";
        	String titulo = "";
        	String descricao = "";
        	GregorianCalendar prazo = new GregorianCalendar();
    		prazo.add(GregorianCalendar.DATE, 2);
    	
    		while (!OrsegupsUtils.isWorkDay(prazo))
    		{
    			prazo = OrsegupsUtils.getNextWorkDay(prazo);
    		}
    		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
    		prazo.set(GregorianCalendar.MINUTE, 59);
    		prazo.set(GregorianCalendar.SECOND, 59);
        
    		NeoObject papel = (NeoObject) processEntity.findGenericValue("papelRaiaEscRes");
    		EntityWrapper en = new EntityWrapper(papel);		
    		
        	// Figueiredo e Silva
        	if (Integer.parseInt(en.getField("neoId").getValue().toString()) == 685022449) {
        
        	    executor = "gustavo.silva";
        
        	    // Guedes Pinto
        	} else if (Integer.parseInt(en.getField("neoId").getValue().toString()) == 685033492) {
        
        	    executor = "mariana.guedes";
        	}
        
        	if (!executor.equals("")) {
        
        	    processEntity.setValue("papelRaiaPoolJurEsc", processEntity.getValue("papelRaiaEscRes"));
        	    
        	    NeoObject tarefa = (NeoObject) processEntity.findGenericValue("wfprocess");
        	    EntityWrapper p = new EntityWrapper(tarefa);
        	    
        	    titulo = "J002 - " + p.getField("code").getValue().toString()+ " - " + processEntity.findGenericValue("numPro").toString() + " Informar o motivo de não recorrer no processo";
        	    descricao = "Informar o porque de não recorrer ao processo " + processEntity.findGenericValue("numPro").toString() + " da tarefa "+p.getField("code").getValue().toString()+" do fluxo J002 - Processo juridico.";
        
        	    try {
        		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
        		String code = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "Sim", prazo, null);
        	    } catch (Exception e) {
        		System.out.println("ERRO AO ABRIR TAREFA SIMPLES J002 NAO RECORREU AO PROCESSO. Classe: RegistroAtividade e Metodo: abrirTarefaSimplesSemRecurso");
        	    }
        	}
        }

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
	    
	    EntityWrapper processEntity = event.getWrapper();
	    NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
		boolean tributario = false;
        	if (tipoProcesso != null) {
        	    EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
        	    if (Integer.parseInt(wTipoProcesso.getValue("codigo").toString()) == 3)
        		    tributario=true;
        	}
		
		if (tributario) {
		    NeoRole papelJuridico = (NeoRole) PersistEngine.getObject(NeoRole.class, new QLEqualsFilter("code", "j002JuridicoOrsegups"));
		    processEntity.setValue("papelJuridicoOrse",papelJuridico);
		    processEntity.setValue("papelRaiaEscRes", papelJuridico);
		}
	    
	}

	@Override
	public void onAssign(TaskAssignerEvent event) throws TaskException {
	    
	    String atividade = event.getActivity().getModel().getName();
		EntityWrapper processEntity = event.getWrapper();
		processEntity.setValue("atividadeAtual", atividade);	    
	}
}
