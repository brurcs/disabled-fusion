package com.neomind.fusion.custom.orsegups.ti;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoUtils;

/**
 * Servlet implementation class DerrubarUsuarioSapiens
 */
@WebServlet(name = "DerrubarUsuarioSapiens", urlPatterns = {"/servlet/com.neomind.fusion.custom.orsegups.ti.DerrubarUsuarioSapiensServlet"})
public class DerrubarUsuarioSapiens extends HttpServlet {

    private static final long serialVersionUID = 1L;
    /*

    public DerrubarUsuarioSapiens() {
	super();   
    }*/

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	String nomeUsuarioFusion = PortalUtil.getCurrentUser().getCode();
	Map<String, List<String>> secoes = new HashMap<>();
	boolean derrubado = false;	
	String mensagem = null;

	String jspUrl = PortalUtil.getBaseURL()+ "custom/jsp/orsegups/ti/DerrubarUsuarioSapiens.jsp?";
	try {
	    int usuarioSapiens =  consultaCodigoUsuarioSapiens(nomeUsuarioFusion);

	    if(NeoUtils.safeIsNotNull(usuarioSapiens)){
		secoes = listaSecoes(nomeUsuarioFusion);
		derrubado = derrubarUsuario(secoes);
		if(derrubado){
		    // USUÁRIO DERRUBADO COM SUCESSO
		    mensagem = "Usuário derrubado com sucesso!";
		    request.setAttribute("mensagem", mensagem);
		} else {
		    // NÃO FOI POSSÍVEL DERRUBAR O USUÁRIO
		    mensagem = "Erro ao derrubar o usuário.";
		    request.setAttribute("mensagem", mensagem);		  
		}		

		response.sendRedirect( jspUrl+"mensagem="+mensagem);
	    }
	} catch(Exception e) {
	    mensagem = e.getMessage();
	    request.setAttribute("mensagem", mensagem);
	    System.out.println(jspUrl+"mensagem="+mensagem);
	    response.sendRedirect( jspUrl+"mensagem="+mensagem);
	    e.printStackTrace();
	}

    }

    public static Map<String, List<String>> listaSecoes(String login) throws Exception {
	//Map<String,Integer> secoes = new HashMap<>(); 
	Map<String, List<String>> secoes = new HashMap<>();
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql1 = new StringBuilder();

	Connection conn2 = null;
	ResultSet rs2 = null;
	PreparedStatement pstm2 = null;
	String sql2 = null;
	String mensagemErro = null; 

	try {
	    conn = PersistEngine.getConnection("SAPIENS");

	    sql1.append("SELECT NUMSEC FROM R911SEC WHERE USRNAM = ? OR APPUSR = ? ");
	    pstm = conn.prepareStatement(sql1.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
	    pstm.setString(1, login);
	    pstm.setString(2, login);

	    rs = pstm.executeQuery();
	    if(rs.next()){
		rs.beforeFirst();
		while(rs.next()){
		    List<String> listSecoes = secoes.get("SEC"); 
		    if (listSecoes == null){
			listSecoes = new ArrayList<>();
		    }
		    String secaoSEC = rs.getString("NUMSEC");
		    listSecoes.add(secaoSEC); 
		    secoes.put("SEC", listSecoes);
		    
		    conn2 = PersistEngine.getConnection("SAPIENS");
		    sql2 = "SELECT NUMSEC FROM R911MOD WHERE NUMSEC = ? ";
		    pstm2 = conn2.prepareStatement(sql2.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    pstm2.setString(1, secaoSEC);
		    rs2 = pstm2.executeQuery();

		    if (rs2.next()) {
		        rs2.beforeFirst();
			while (rs2.next()) {
			    List<String> listMod = secoes.get("MOD"); 
			    if (listMod == null){
				listMod = new ArrayList<>();
			    }
			    String secaoMOD = rs.getString("NUMSEC"); 
			    if (secaoMOD != null){
        			    listMod.add(secaoMOD); 
        			    secoes.put("MOD", listMod); 
			    }	
			}
		    }
    
		}
	    }
	} catch (Exception e){
	    e.printStackTrace();
	    if(mensagemErro == null){
		mensagemErro = "Erro ao consultar as seções do usuário no banco de dados.";
	    }
	    throw new Exception(mensagemErro);
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs); 
	    OrsegupsUtils.closeConnection(conn2, pstm2, rs2);
	}

	return secoes;
    }

    public static boolean derrubarUsuario(Map<String, List<String>> secoes) throws Exception {
	boolean derrubado = false;
	int derrubadoMod = 0, derrubadoSec = 0;
	Connection conn1 = null, conn2 = null;
	PreparedStatement pstm1 = null, pstm2 = null;
	StringBuilder deleteMod = new StringBuilder(), deleteSec = new StringBuilder();
	List<String> secoesSEC = new ArrayList<>(), secoesMOD = new ArrayList<>();

	secoesMOD = secoes.get("MOD"); 
	secoesSEC = secoes.get("SEC");

	try {
	    if(secoesMOD != null){
		try {
		    conn1 = PersistEngine.getConnection("SAPIENS");
		    String secoesModString = StringUtils.join(secoesMOD, ","); 
		    deleteMod.append("DELETE FROM R911MOD WHERE NUMSEC IN("+secoesModString+")");
		    pstm1 = conn1.prepareStatement(deleteMod.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    //String secoesModString = StringUtils.join(secoesMOD, ","); 
		    //pstm1.setString(1, secoesModString);	
		    System.out.println(secoesModString);
		    derrubadoMod = pstm1.executeUpdate();
		} catch (Exception e) {
		    e.printStackTrace();
		    throw new Exception("Erro ao fazer o delete na R911MOD.");
		} finally {
		    OrsegupsUtils.closeConnection(conn1, pstm1, null);
		}
	    } else {
		derrubadoMod = 1;
	    }

	    try {
		if(secoesSEC != null){
		    conn2 = PersistEngine.getConnection("SAPIENS");
		    String secoesSecString = StringUtils.join(secoesSEC, ","); 
		    deleteSec.append("DELETE FROM R911SEC WHERE NUMSEC IN("+secoesSecString+")");
		    pstm2 = conn2.prepareStatement(deleteSec.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    //String secoesSecString = StringUtils.join(secoesSEC, ","); 
		    //pstm2.setString(1, secoesSecString);
		    derrubadoSec = pstm2.executeUpdate();	
		} else {
		    derrubadoSec = 1;
		}	    		
	    } catch (Exception e) {
		e.printStackTrace();
		throw new Exception("Erro ao fazer o delete na R911SEC.");
	    } finally {
		OrsegupsUtils.closeConnection(conn2, pstm2, null);
	    }

	    if(derrubadoSec > 0 && derrubadoMod > 0){
		derrubado = true;
	    }

	    return derrubado;

	} catch (Exception e) {
	    e.printStackTrace();
	    throw new Exception("Erro inesperado ao efetuar a liberação das seções do usuário.");
	}
    }

    public int consultaCodigoUsuarioSapiens(String login) throws Exception {
	int codigoUsuario = 0;

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();
	String mensagem = null;
	try {
	    conn = PersistEngine.getConnection("SAPIENS");

	    sql.append("SELECT CODUSU FROM R999USU WHERE NOMUSU = ?");

	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setString(1, login);

	    rs = pstm.executeQuery();

	    if(rs.next()){
		codigoUsuario = rs.getInt(1);
		return codigoUsuario;
	    } else {
		mensagem = "Usuário " + login + " não encontrado no Sapiens.";
		throw new Exception(mensagem);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    if(e.getMessage() == null){
		mensagem = "Erro ao consultar o usuário " + login + " no Sapiens";
	    }
	    throw new Exception(mensagem);
	}
    }

}
