package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoUtils;

public class FCIInsereUsuarioTarefa implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		if (verificaClienteHumana(processEntity) == true)
		{
		    	NeoPaper executorHumana = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "FCIExecutorHumana"));
		    	NeoUser usuario = null;
		    
		    
		    	Set<NeoUser> usuarios = executorHumana.getAllUsers();
		    	if(NeoUtils.safeIsNotNull(usuarios))
		    	for(NeoUser user: usuarios){
		    	    usuario = user;
		    	    break;
		    	}

			processEntity.setValue("usuarioTarefa", usuario);
		}
		else
		{

			String desfpg = (String) processEntity.findValue("desfpg");
			if (!desfpg.isEmpty())
			{
				Long indice = (Long) processEntity.findValue("indice"); 
				Integer i = Integer.valueOf(indice.toString());
				Connection conn = null;
				StringBuilder sqlTarefas = new StringBuilder();
				PreparedStatement pstm = null;
				ResultSet rs = null;
				List<String> listaExecutor = new ArrayList<String>();
				conn = PersistEngine.getConnection("");

				sqlTarefas.append("select secUser.code from dbo.SecurityEntity secPaper ");
				sqlTarefas.append("inner join dbo.NeoPaper_users nps on secPaper.neoId = nps.papers_neoId ");
				sqlTarefas.append("inner join dbo.NeoUser n on n.neoId = nps.users_neoId ");
				sqlTarefas.append("inner join dbo.SecurityEntity secUser on n.neoId = secUser.neoId ");
				sqlTarefas.append("where secPaper.code = 'FCIUsuariosLigacoes' and ");
				sqlTarefas.append("secUser.active = 1 and ");
				sqlTarefas.append("n.neoId <>  1240041 and ");
				sqlTarefas.append("n.fullName not like 'zz%' ");
				sqlTarefas.append("order by secUser.code");
				
				try
				{
					pstm = conn.prepareStatement(sqlTarefas.toString());

					rs = pstm.executeQuery();

					while (rs.next())
					{
						listaExecutor.add(rs.getString("code"));
					}
					System.out.println(listaExecutor.get(i));
					NeoUser usuario = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", listaExecutor.get(i)));
					processEntity.setValue("usuarioTarefa", usuario);
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
				finally
				{

					OrsegupsUtils.closeConnection(conn, pstm, rs);

				}

			}
		
		}
	}

	private Boolean verificaClienteHumana(EntityWrapper processEntity)
	{
		
		Long codCli = (Long) processEntity.findValue("contratoSapiens.codcli");	
		Integer cliente = Integer.valueOf(codCli.toString());

		StringBuilder sqlTarefas = new StringBuilder();
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstm = null;
		String nomeFonteDados = "SAPIENS";
		boolean verificar = false;

		sqlTarefas.append("SELECT C.USU_CODCLI ");
		sqlTarefas.append("FROM USU_T160CVS P ");
		sqlTarefas.append("JOIN USU_T160CTR C ON P.USU_NUMCTR = C.USU_NUMCTR AND P.USU_CODEMP = C.USU_CODEMP AND P.USU_CODFIL = C.USU_CODFIL ");
		sqlTarefas.append("WHERE EXISTS ");
		sqlTarefas.append("(SELECT S.CODFAM ");
		sqlTarefas.append("FROM USU_T160CVS P ");
		sqlTarefas.append("JOIN USU_T160CTR CTR ON P.USU_NUMCTR = CTR.USU_NUMCTR AND P.USU_CODEMP = CTR.USU_CODEMP AND P.USU_CODFIL = CTR.USU_CODFIL ");
		sqlTarefas.append("JOIN E080SER S ON P.USU_CODSER=S.CODSER AND S.CODEMP=P.USU_CODEMP ");
		sqlTarefas.append("WHERE ((P.USU_SITCVS = 'A') OR (P.USU_SITCVS = 'I' AND P.USU_DATFIM >= GETDATE())) ");
		sqlTarefas.append("AND ((CTR.USU_SITCTR = 'A') OR (CTR.USU_SITCTR = 'I' AND CTR.USU_DATFIM >= GETDATE())) ");
		sqlTarefas.append("AND CTR.USU_NUMCTR <> 1 AND S.CODFAM = 'SER101' AND CTR.USU_CODCLI = C.USU_CODCLI) ");
		sqlTarefas.append("AND EXISTS ");
		sqlTarefas.append("(SELECT S.CODFAM ");
		sqlTarefas.append("FROM USU_T160CVS P ");
		sqlTarefas.append("JOIN USU_T160CTR CTR ON P.USU_NUMCTR = CTR.USU_NUMCTR AND P.USU_CODEMP = CTR.USU_CODEMP AND P.USU_CODFIL = CTR.USU_CODFIL ");
		sqlTarefas.append("JOIN E080SER S ON P.USU_CODSER=S.CODSER AND S.CODEMP=P.USU_CODEMP ");
		sqlTarefas.append("WHERE ((P.USU_SITCVS = 'A') OR (P.USU_SITCVS = 'I' AND P.USU_DATFIM >= GETDATE())) ");
		sqlTarefas.append("AND((CTR.USU_SITCTR = 'A') OR (CTR.USU_SITCTR = 'I' AND CTR.USU_DATFIM >= GETDATE())) ");
		sqlTarefas.append("AND CTR.USU_NUMCTR <> 1 AND ((S.CODFAM = 'SER102') OR (S.CODFAM = 'SER103')) AND CTR.USU_CODCLI = C.USU_CODCLI) ");
		sqlTarefas.append("AND ((P.USU_SITCVS = 'A') OR (P.USU_SITCVS = 'I' AND P.USU_DATFIM >= GETDATE())) ");
		sqlTarefas.append("AND ((C.USU_SITCTR = 'A') OR (C.USU_SITCTR = 'I' AND C.USU_DATFIM >= GETDATE())) ");
		sqlTarefas.append("AND C.USU_CODCLI = ? ");
		sqlTarefas.append("GROUP BY C.USU_CODCLI ");

		try
		{

			conn = PersistEngine.getConnection(nomeFonteDados);
			pstm = conn.prepareStatement(sqlTarefas.toString());
			pstm.setInt(1, cliente);
			rs = pstm.executeQuery();

			while (rs.next())
			{
				verificar = true;
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

		}

		return verificar;
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
