package com.neomind.fusion.custom.orsegups.scheduler;

import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.JsonObject;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.JsonHttpUtil;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.Job;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.scheduler.RotinaEmailComprovantePonto
public class RotinaEmailComprovantePonto implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		String charset = "UTF8";
		
		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaEmailComprovantePonto");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Rotina Email ComprovantePonto - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		GregorianCalendar lastDate = new GregorianCalendar();
		GregorianCalendar currentDate = new GregorianCalendar();
		currentDate.add(Calendar.MINUTE, -1);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		
		
		try
		{
			List<NeoObject> emails = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("QLEmailComprovantePonto"));
			NeoObject email = emails.get(0);
			EntityWrapper wrapper = new EntityWrapper(email);
			lastDate = (GregorianCalendar) wrapper.findValue("lastDate");
			System.out.println(NeoUtils.safeDateFormat(lastDate,"dd/MM/yyyy HH:mm:ss.sss"));
			System.out.println(NeoUtils.safeDateFormat(currentDate,"dd/MM/yyyy HH:mm:ss.sss"));
			
			sql.append(" SELECT acc.numcra, fun.nomfun, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data, acc.horacc, fil.numcgc, fun.numpis, fil.razsoc, fun.usu_emapon, sind.usu_emasin   ");
			sql.append(" FROM R070ACC acc  		 with(nolock) ");
			sql.append(" INNER JOIN R038HCH hch  with(nolock) ON hch.NumCra = acc.NumCra ");
			sql.append(" INNER JOIN R034FUN fun  with(nolock) ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol ");
			sql.append(" INNER JOIN R030FIL fil  with(nolock) on fil.codfil = fun.CodFil and fil.numemp = fun.NumEmp ");
			sql.append(" LEFT  JOIN r038hsi hsi  with(nolock) on (fun.numcad = hsi.numcad and fun.numemp = hsi.numemp and fun.tipcol = hsi.tipcol and hsi.datAlt = (select max(DatAlt) from r038hsi h where h.numcad = fun.numcad and h.numemp = fun.numemp and h.tipcol = fun.tipcol) ) ");
			sql.append(" LEFT  JOIN r014sin sind with(nolock) on (hsi.codsin = sind.codsin) ");
			sql.append(" WHERE acc.CodPlt = 2 AND acc.CodRlg = 1 AND acc.TipAcc >= 100 ");
			sql.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) >= ? AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) < ? ");
			
			//sql.append(" and fun.numcad in (768,887,10084,10127,876,828) ");
			//sql.append(" and fun.numcad in (768) ");
			
			sql.append(" GROUP BY acc.numcra, fun.numemp, fun.numcad, fun.nomfun, acc.HorAcc, acc.DatAcc, fil.numcgc, fun.numpis, fil.razsoc, fun.usu_emapon, sind.usu_emasin  ");
			sql.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");
			
			pstm = conn.prepareStatement(sql.toString());
			pstm.setTimestamp(1, new Timestamp(lastDate.getTimeInMillis()));
			pstm.setTimestamp(2, new Timestamp(currentDate.getTimeInMillis()));
			
			rs = pstm.executeQuery();

			while (rs.next())
			{
				String params = "";
				
				String numcra = rs.getString("numcra");
				String nomfun = rs.getString("nomfun");
				
				Timestamp dataAcesso = rs.getTimestamp("data");
				GregorianCalendar gData = new GregorianCalendar();
				gData.setTimeInMillis(dataAcesso.getTime());
				
				Long horacc = rs.getLong("horacc");
				String numcgc = rs.getString("numcgc");
				String numpis = rs.getString("numpis");
				String razsoc = rs.getString("razsoc");
				String nsr = numcra + NeoUtils.safeDateFormat(gData, "yyyyMMdd");
				Long datHorAcc = gData.getTimeInMillis();
			
				if (NeoUtils.safeIsNotNull(numcra) && NeoUtils.safeIsNotNull(nomfun) && NeoUtils.safeIsNotNull(gData) && NeoUtils.safeIsNotNull(horacc) && 
						NeoUtils.safeIsNotNull(numcgc) && NeoUtils.safeIsNotNull(numpis)) 
				{
					
					String hora = String.format("%04d", horacc);
					
					StringBuilder html = new StringBuilder();

					String origem = "pontoeletronico@orsegups.com.br";
					
					String emapon = rs.getString("usu_emapon");
					if (emapon == null || "".equals(emapon)){
						emapon = "pontoeletronicoorsegups@gmail.com";
						System.out.println("Colaborador " + nomfun + " sem email cadastrado." );
					}
					
					String destinos = "sindicatopresenca@gmail.com;"+ emapon + ";" + NeoUtils.safeOutputHTMLString(rs.getString("usu_emasin")) ;
					
					
					params +="act=assinaEnvialEmail";
					params +="&numcra="+URLEncoder.encode(numcra, charset);
					params +="&nomfun="+URLEncoder.encode(nomfun, charset);
					params +="&horacc="+horacc;
					params +="&numcgc="+URLEncoder.encode(numcgc, charset);
					params +="&numpis="+URLEncoder.encode(numpis, charset);
					params +="&razsoc="+URLEncoder.encode(razsoc, charset);
					params +="&nsr="+URLEncoder.encode(nsr, charset);
					params += "&datHorAcc="+datHorAcc;
					params +="&destinos="+URLEncoder.encode(destinos, charset);
					
					// chama a outra instancia de servidor para assinar e enviar os e-mails
					JsonObject job = JsonHttpUtil.readFromJson("http://192.168.20.229:8081/OrsegupsBryBridge/assina",params);
					
					if (job == null){
						System.out.println("Erro ao assinar marcação. Debug["+params+"]");
					}else if ( "NOK".equals(job.get("message")) ){
						System.out.println("Erro ao assinar marcação. Debug["+params+"]" + " Error: " + job.get("error"));
					}else if ( "OK".equals(job.get("message")) ){
						
					}
					
					
				}	
			}
			wrapper.findField("lastDate").setValue(currentDate);
			PersistEngine.persist(email);
		}
		catch (Exception e)
		{
			Long key = GregorianCalendar.getInstance().getTimeInMillis();
			System.out.println("["+key+"] Erro ao processa Job RotinaEmailComprovantePonto  " + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processar Job Verifique no log por ["+key+"] para maiores detalhes");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Rotina Email ComprovantePonto - LastDate: " + NeoUtils.safeDateFormat(lastDate) + " - CurrentDate: " + NeoUtils.safeDateFormat(currentDate));
		}
	}
}
