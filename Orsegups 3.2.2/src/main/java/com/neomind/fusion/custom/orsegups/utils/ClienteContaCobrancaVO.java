package com.neomind.fusion.custom.orsegups.utils;

import java.util.ArrayList;

public class ClienteContaCobrancaVO {
    
    private int codigoCliente;
    private String nomeCliente;
    private ArrayList<String> contas = new ArrayList<String>();
    private boolean cobrar = true;
    private boolean servicoExtra = false;
    private String propEquipamento = "Cliente";
    
    public void addConta (String conta){
	this.contas.add(conta);
    }
    
    public int getCodigoCliente() {
        return codigoCliente;
    }
    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }
    public String getNomeCliente() {
        return nomeCliente;
    }
    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
    public ArrayList<String> getContas() {
        return contas;
    }
    public void setContas(ArrayList<String> contas) {
        this.contas = contas;
    }
    
    public boolean isCobrar() {
        return cobrar;
    }

    public void setCobrar(boolean cobrar) {
        this.cobrar = cobrar;
    }

    public boolean isServicoExtra() {
        return servicoExtra;
    }

    public void setServicoExtra(boolean servicoExtra) {
        this.servicoExtra = servicoExtra;
    }

    public String getPropEquipamento() {
        return propEquipamento;
    }

    public void setPropEquipamento(String propEquipamento) {
        this.propEquipamento = propEquipamento;
    }

    public String toString(){
	String resultado = "Código Cliente: "+this.codigoCliente+"\n"
			+ "Nome Cliente: "+this.nomeCliente+"\n"
			+ "Contas: "+this.contas;
	
	return resultado;
    }
    
}
