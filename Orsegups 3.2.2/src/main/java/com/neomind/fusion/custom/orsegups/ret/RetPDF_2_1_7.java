package com.neomind.fusion.custom.orsegups.ret;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.parser.PdfTextExtractor;
import com.neomind.fusion.custom.orsegups.RE.TomadorDaoImpl;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RetPDF_2_1_7 {
	private boolean resumoRet;
	String path = "\\\\fsoofs01\\f$\\Site";
	
	  public static String leadingZeros(String s, int length) {
	    	String retorno = null;
	    	try {
	    		if (s.length() >= length) 
	    			return s;
	    		else 
	    			retorno = String.format("%0" + (length-s.length()) + "d%s", 0, s);
	    	}catch(Exception ex) {
	    			ex.printStackTrace();
	    	}
			return retorno;
	    }

	    //Cria PDF por TOMADOR
	    public static boolean criarArquivoPDF(String codEmp, String dataCpt, String inscricaoTomadorAnterior, PdfReader reader, String outFile, int paginaStart, int paginaEnd, boolean encontrado, Long empfil[]) {
			boolean retorno = true;
	    	try {
	    		String naoEncontrado = "";
	    		if (!encontrado){
	    			naoEncontrado = "\\naoEncontrado\\";
	    		}
	    		outFile = outFile+naoEncontrado+"\\"+codEmp+"\\"+leadingZeros(NeoUtils.safeOutputString(empfil[0]),2)+"\\"+empfil[1]+"\\"+NeoUtils.safeLong(inscricaoTomadorAnterior)+"\\RET\\"+dataCpt.replace("/", "");
	    		File file = new File(outFile);
	    		if (!file.exists())
	    			file.mkdirs();
				outFile = outFile+"\\"+NeoUtils.safeLong(inscricaoTomadorAnterior) + ".pdf";
				System.out.println(outFile);
				Document document = new Document(reader.getPageSizeWithRotation(1));
				PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
				document.open();
				for (int i = paginaStart; i <= paginaEnd; i++) {
					PdfImportedPage page = writer.getImportedPage(reader, i);
					writer.addPage(page);
				}
				document.close();
				writer.close();
			} catch (FileNotFoundException e) {
				retorno = false;
				e.printStackTrace();
			} catch (DocumentException e) {
				retorno = false;
				e.printStackTrace();
			} catch (IOException e) {
				retorno = false;
				e.printStackTrace();
			} catch (Exception ex){
				retorno = false;
				ex.printStackTrace();
			}
			return retorno;
		}
	    
	  //Cria PDF por TOMADOR
	    public static boolean criarArquivoPDF_RTRET(String codEmp, String dataCpt, String inscricaoAnterior, PdfReader reader, String outFile, int paginaStart, int paginaEnd, boolean encontrado, Long empfil[]) {
			boolean retorno = true;
	    	try {
	    		String naoEncontrado = "";
	    		if (!encontrado){
	    			naoEncontrado = "\\naoEncontrado\\";
	    		}
	    		outFile = outFile+naoEncontrado+codEmp+"\\"+leadingZeros(NeoUtils.safeOutputString(empfil[0]),2)+"\\"+empfil[1]+"\\"+NeoUtils.safeLong(inscricaoAnterior)+"\\RTRET\\"+dataCpt.replace("/", "");
	    		File file = new File(outFile);
	    		if (!file.exists())
	    			file.mkdirs();
				outFile = outFile+"\\"+NeoUtils.safeLong(inscricaoAnterior) + ".pdf";
				System.out.println(outFile);
				Document document = new Document(reader.getPageSizeWithRotation(1));
				PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
				document.open();
				for (int i = paginaStart; i <= paginaEnd; i++) {
					PdfImportedPage page = writer.getImportedPage(reader, i);
					writer.addPage(page);
				}
				document.close();
				writer.close();
			} catch (FileNotFoundException e) {
				retorno = false;
				e.printStackTrace();
			} catch (DocumentException e) {
				retorno = false;
				e.printStackTrace();
			} catch (IOException e) {
				retorno = false;
				e.printStackTrace();
			} catch (Exception ex){
				retorno = false;
				ex.printStackTrace();
			}
			return retorno;
		}
	    
	  //Ler PDF identificando se o proximo PDF  igual ao TOMADOR anterior caso nao seja ele manda criar o pdf e continua num ciclo ate ler todas as paginas do PDF 
	    public String leituraPdf(InputStream input, String dataCpt, String codEmp, String usuario, boolean separaRtret){
	    	ArrayList<String> cgccpfs = new ArrayList<String>();
	    	resumoRet = false;
	    	
	    	StringBuilder arquivosGerados = new StringBuilder();
	    	arquivosGerados.append(" Arquivos Gerados: \r\n");
	    	
	    	path += usuario.equals("herisson.ferreira") ? "\\TESTE\\" : "\\";
	    	try {
		    	PdfReader reader = new PdfReader(input, null);
				PdfTextExtractor text = new PdfTextExtractor(reader);
				int numPages = reader.getNumberOfPages();
				String inscricaoTomadorAnterior = null; 
				int paginaStart = 1;
		        int paginaEnd = 0;
		        String inscricaoTomador = null;
		        String cnpjPrestador = null;
				for (int paginaAtual = 1; paginaAtual <= numPages; paginaAtual++) {
	
					StringBuilder str = new StringBuilder(text.getTextFromPage(paginaAtual));
					
					int posicaoDATA = str.indexOf("DATA:");
             		int posicaoInscricaoStart = posicaoDATA -19;
             		int posicaoInscricaoEnd = posicaoInscricaoStart+19;
             		String inscricaoPrestadorRaw = str.substring(posicaoInscricaoStart, posicaoInscricaoEnd);
             		
             		int posicaoDATA2 = retornaIndiceCampo(str,"INSCRI",2);
             		int posicaoInscricaoStart2 = str.toString().indexOf("N",posicaoDATA2+6) -19;
             		int posicaoInscricaoEnd2 = posicaoInscricaoStart2+19;
             		String inscricaoTomadorRaw =  str.substring(posicaoInscricaoStart2, posicaoInscricaoEnd2);
             		
             		if (!(inscricaoTomadorRaw.contains(".") && inscricaoTomadorRaw.contains("/") && inscricaoPrestadorRaw.contains("-"))) {
             			 posicaoDATA2 = str.indexOf("TOMADOR/OBRA :");
             			 posicaoInscricaoStart2 = posicaoDATA2+15;
             			 posicaoInscricaoEnd2 = posicaoInscricaoStart2+19;
             			 inscricaoTomadorRaw =  str.substring(posicaoInscricaoStart2, posicaoInscricaoEnd2);             			
             		}
             		
					String[] texto = text.getTextFromPage(paginaAtual).split("\n");
					
					//ler linha a linha
					boolean isInicioTomador = false;
					boolean isResumoRET = false;
					//Recupera se � troca do novo tomador
					int idxModalidade = retornaIndiceCampoArray(texto,"O DE TOMADOR/OBRA - RET",1);
					int idxResumoRet = retornaIndiceCampoArray(texto,"RESUMO - RELA",1);
					if (idxModalidade > -1 &&  texto[idxModalidade] != null && texto[idxModalidade].contains("O DE TOMADOR/OBRA - RET") && idxResumoRet == -1 ){
						isInicioTomador = true;
					}
					if (idxResumoRet > -1 && texto[idxResumoRet] != null && texto[idxResumoRet].contains("RESUMO - RELA")){
						isResumoRET = true;
					}
					
					if (isInicioTomador) {
						//Recupera o CGC do TOMADOR
						cnpjPrestador = inscricaoPrestadorRaw.replaceAll("\\D+",""); //texto[10].replaceAll("\\D+","");
						inscricaoTomador = inscricaoTomadorRaw.replaceAll("\\D+",""); // texto[12].replaceAll("\\D+","");
						
						cgccpfs.add(inscricaoTomador);
						
						if(inscricaoTomadorAnterior == null)
							inscricaoTomadorAnterior = inscricaoTomador;
					}
					
					Boolean valid = true; // aqui ir�o colocar o metodo de busca do TOMADOR na base da senior se existir retorna true se n�o retorna false
					
					if (!inscricaoTomadorAnterior.equals(inscricaoTomador) || (paginaAtual == numPages)){
						paginaEnd = paginaAtual -1;
						Long emfil[] = TomadorDaoImpl.getEmpFil(cnpjPrestador);
         				criarArquivoPDF(codEmp,dataCpt,inscricaoTomadorAnterior,reader,path,paginaStart,paginaEnd, valid, emfil );
         				arquivosGerados.append("Empresa: "+emfil[0]+", Filial: "+emfil[1]+", Pagina: " + paginaAtual + ", Inscricao Tomador: " + inscricaoTomador+"\r\n");
         				inscricaoTomadorAnterior = inscricaoTomador;
         				paginaStart = paginaAtual;
					}
					
					if (isResumoRET){
						resumoRet = true;
						System.out.println(text.getTextFromPage(paginaAtual));
						for (String cgccpf : cgccpfs){
							paginaEnd = paginaAtual ;
							paginaStart = paginaAtual ;
							Long emfil[] = TomadorDaoImpl.getEmpFil(cnpjPrestador);
	         				criarArquivoPDF_RTRET(codEmp,dataCpt,cgccpf,reader,path,paginaStart,paginaEnd, valid, emfil );
	         				arquivosGerados.append("Empresa: "+emfil[0]+", Filial: "+emfil[1]+", Pagina: " + paginaAtual + ", Inscricao Tomador: " + inscricaoTomador+"\r\n");
						}
					}
				}
				
	    	
	    	 }catch (WorkflowException e) {
	             e.printStackTrace();
	             throw new WorkflowException(e.getErrorList().get(0).getI18nMessage());
	         }catch (Exception e) {
				e.printStackTrace();
				throw new WorkflowException("Erro ao processar a RET anexada.");
			}
	    	
	    	 return arquivosGerados.toString();
	     }
	    
	    
	    public static int retornaIndiceCampoArray(String array[], String campo, int ocorrencia ){
	    	int retorno = -1;
	    	
	    	int count = 0;
	    	for (int i = 0; i < array.length; i++){
	    		if (array[i].contains(campo)){
	    			count++;
	    			retorno = i;
	    		}
	    	}
	    	if (count != ocorrencia){
	    		retorno =-1;
	    	}
	    	
	    	return retorno;
	    	
	    }
	    
	    public static int retornaIndiceCampo(StringBuilder str, String campoBusca,int ocorrencia){
	    	int retorno = -1;
	    	int indexAtual = -1;
	    	int cont = 0;
	    	String tmp = null;
	    	String camp = null;
	    	
	    	int offsetEnd = 0;
	    	
	    	for ( int i = 0; i < ocorrencia; i++){
	    		if ( (indexAtual = str.toString().indexOf(campoBusca, indexAtual+1)) > -1){
	    			cont++;
	    		}
	    	}
	    	
	    	if (cont != ocorrencia){
	    		return 0;
	    	}else{
	    		return indexAtual;
	    	}
	    }
	    
	   
	    
	    
	public static void main(String args[]) throws IOException {
		String inFile = "C:\\tmp\\RE\\RET.PDF";
		File f = new File(inFile);
		 String dataCpt = "02/2010";
         String codEmp = "Orsegups";
		RetPDF_2_1_7 t = new RetPDF_2_1_7();
		t.leituraPdf( new FileInputStream(f), dataCpt, codEmp,"danilo.silva",true);
	}

	public boolean isResumoRet() {
		return resumoRet;
	}

	public void setResumoRet(boolean resumoRet) {
		this.resumoRet = resumoRet;
	}

	
	
	
}