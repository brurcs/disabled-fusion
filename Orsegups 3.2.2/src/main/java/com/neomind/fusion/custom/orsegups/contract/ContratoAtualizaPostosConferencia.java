package com.neomind.fusion.custom.orsegups.contract;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.persistence.Query;

import org.jfree.util.Log;

import sun.util.calendar.Gregorian;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoAtualizaPostosConferencia implements AdapterInterface{
	
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		/* 
		 * update USU_T160CVS set  usu_cptcom = ?competencia.usu_codcal, usu_ctrcli = ?dataLiberacaoComissao where 
		 * usu_numpos =?postosContrato.numPosto and usu_numctr = ?numContrato.usu_numctr and usu_codemp = ?empresa.codemp and
		 * usu_codfil = ?empresa.codfil
		*/
		
		GregorianCalendar dataLiberacaoComissao = (GregorianCalendar) processEntity.findValue("dataLiberacaoComissao");
		
		//SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
		//String ctrcli = data.format(dataLiberacaoComissao);
		
		Log.debug("Formato Data Contrato Assinado: " + ContratoUtils.retornaDataFormatoSapiens(dataLiberacaoComissao));
		
		GregorianCalendar cptcal = (GregorianCalendar)processEntity.findValue("competencia.usu_cptcal");
		String cptcom = ContratoUtils.retornaDataFormatoSapiens(cptcal);
		String numpos = NeoUtils.safeOutputString(processEntity.findValue("postosContrato.numPosto"));
		/*String numctr = NeoUtils.safeOutputString(processEntity.findValue("numContrato.usu_numctr"));*/
		String numctr = NeoUtils.safeOutputString(processEntity.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		if("".equals(numctr))
			numctr = NeoUtils.safeOutputString(processEntity.findValue("numContrato.usu_numctr"));
		String codemp = NeoUtils.safeOutputString(processEntity.findValue("empresa.codemp"));
		String codfil = NeoUtils.safeOutputString(processEntity.findValue("empresa.codfil"));
		
		StringBuilder sql = new StringBuilder();
		
		sql = new StringBuilder();
		sql.append("UPDATE USU_T160CVS SET  usu_ctrcli = '" +ContratoUtils.retornaDataFormatoSapiens(dataLiberacaoComissao)+ "' , usu_cptcom = '" + cptcom +"' ");
		sql.append(" WHERE usu_numpos = " + numpos);
		sql.append(" AND usu_numctr = " + numctr);
		sql.append(" AND usu_codemp = " + codemp);
		sql.append(" AND usu_codfil = " + codfil);
		
		Log.debug("SQL de Update USU_T160CVS: " + sql);
		
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		
		try
		{
			queryExecute.executeUpdate();
		}catch(Exception e)
		{
			throw new WorkflowException("Erro na Inserção do campo usu_datass na tabela USU_T160CVS do Sapiens");
		}
	}

	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
