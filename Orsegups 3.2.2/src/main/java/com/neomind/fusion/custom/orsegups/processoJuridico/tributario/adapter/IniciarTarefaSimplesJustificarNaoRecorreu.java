package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.IniciarTarefaSimplesJustificarNaoRecorreu

public class IniciarTarefaSimplesJustificarNaoRecorreu implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		
		Integer iTipoProcesso = 0;
		NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
    	    	if (tipoProcesso != null) {
    	    	    EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
    	    	    iTipoProcesso = Integer.parseInt(wTipoProcesso.getValue("codigo").toString());
    	    	}
		
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String titulo = "Processo :" + processEntity.getValue("numPro") + " Tarefa " + activity.getCode() + " do fluxo J002 - Processo Juridico ";
		String descricao = "Informar o porque de não recorreu no processo " + processEntity.getValue("numPro") + " referente a tarefa " + activity.getCode() + " do fluxo J002 - Processo Juridico";
		
		if (iTipoProcesso == 3) {
		    titulo = "J002 - Tributário" + activity.getCode() + " - Informar o motivo de não recorrer no processo";
		} else {
		
        		if (processEntity.getValue("colaborador") != null)
        		{
        			NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
        			EntityWrapper wColaborador = new EntityWrapper(colaborador);
        			colaborador = (NeoObject) processEntity.getValue("colaborador");
        
        			titulo = "J002 - " + activity.getCode() + " - " + wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun") + " - Informar o motivo de não recorrer no processo";
        		}
        		else
        		{
        			NeoObject autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
        			EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);
        			String cpfAut = String.valueOf(wAutor.getValue("cpfAut"));
        			String nomAut = String.valueOf(wAutor.getValue("nomAut"));
        			titulo = "J002 - " + activity.getCode() + " - " + nomAut;
        			if (cpfAut != null && !cpfAut.equals(""))
        			{
        				titulo = titulo + " / " + cpfAut;
        			}
        
        		}
		}
		String executor = "rafael";
		String solicitante = "dilmoberger";
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DATE, 2);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);

		prazo = OrsegupsUtils.getNextWorkDay(prazo);

		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
		String code = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "Sim", prazo);
		System.out.println("Tarefa aberta : " + code);

		List<NeoObject> registroAtividade = (List<NeoObject>) processEntity.getValue("j002RegistroHistoricoAtividade");
		String responsavel = origin.returnResponsible();
		GregorianCalendar dataAcao = new GregorianCalendar();
		String atividade = origin.getActivityName().toString();
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002RegistroHistoricoAtividade");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("atividade", atividade);
		wRegAti.setValue("responsavel", responsavel);
		String descricaoRegistro = "Aberto tarefa simples " + code + " para responder o porque não ter recorrido, referente a este processo.";
		wRegAti.setValue("descricao", descricaoRegistro);

		PersistEngine.persist(objRegAti);
		registroAtividade.add(objRegAti);

		processEntity.setValue("j002RegistroHistoricoAtividade", registroAtividade);
	    }catch(Exception e){
		System.out.println("Erro na classe IniciarTarefaSimplesJustificarNaoRecorreu do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
