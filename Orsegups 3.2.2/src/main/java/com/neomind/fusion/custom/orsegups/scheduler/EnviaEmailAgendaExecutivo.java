package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;

import com.neomind.fusion.custom.orsegups.adapter.vo.OrdemServicoVO;
import com.neomind.fusion.custom.orsegups.adapter.vo.RepresentanteVO;
import com.neomind.fusion.custom.orsegups.sapiens.vo.ContratoClienteVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.DBOrdemVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * Envia e-mail para o executivo informando a agenda de instalaões/habilitações do dia seguinte.
 * 
 * @author herisson.ferreira
 *
 */
public class EnviaEmailAgendaExecutivo implements CustomJobAdapter {
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		LinkedHashMap<Long, List<OrdemServicoVO>> hashOS = new LinkedHashMap<>();
		List<Long> representantes = new ArrayList<>();
		
		try {
			
			representantes = retornaRepresentantesAtivos();
			
			if (listaValida(representantes)) {
				
				for (Long representante : representantes) {
					
					try {
						
						hashOS = retornaListaOS(representante);
						
						if(eValido(hashOS)) {
							enviaEmailRepresentante(hashOS.get(representante));
						}
						
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("["+this.getClass().getSimpleName()+"] ERRO");
					}
		
				}
				
			}
			

			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Fim: "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss"));
		}
		
		
	}

	private boolean listaValida(List<Long> valor) {
		
		if (!valor.isEmpty() && valor.size() > 0) {
			return true;
		} else {
			return false;
		}
		
	}


	/**
	 * Envia um e-mail para o representante de vendas contendo todas as Instalações/Habilitações previstas para o dia seguinte
	 * 
	 * @param listaOS Lista contendo as Instalações/Habilitações
	 */
	private void enviaEmailRepresentante(List<OrdemServicoVO> listaOS) {
		
		StringBuilder msg = new StringBuilder();
		GregorianCalendar data = new GregorianCalendar();
		data.add(GregorianCalendar.DAY_OF_MONTH, 1);
		
		try {
			
			msg.append(montaCabecalho());
			
			for (OrdemServicoVO ordemServicoVO : listaOS) {
				msg.append(montaCorpoEmail(ordemServicoVO));
			}
			
			msg.append(montaRodape());
			
			OrsegupsUtils.sendEmail2Orsegups(listaOS.get(0).getRepresentante().getEmail(), "fusion@orsegups.com.br",
					"[FUSION] Agenda Instalação/Habilitação "+NeoDateUtils.safeDateFormat(data, "dd/MM/yyyy"), "", msg.toString());
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("["+this.getClass().getSimpleName()+"] ERRO ao enviar o e-mail para o representante. ");
		}
		
	}

	/**
	 * Monta o rodapé do e-mail (Fechamento das tags table - body - html)
	 * 
	 * @return rodapé em estrutura html
	 */
	private String montaRodape() {
		StringBuilder html = new StringBuilder();

		html.append("\n 			</table> ");
		html.append("\n 		</body> ");
		html.append("\n </html> ");

		return html.toString();
	}

	/**
	 * Monta o corpo do e-mail concatenando as OS vinculadas ao contrato do executivo
	 * 
	 * @param ordemServicoVO Objeto contendo os dados da OS
	 * @return corpo do e-mail em estrutura html
	 */
	private String montaCorpoEmail(OrdemServicoVO ordemServicoVO) {
		
			StringBuilder html = new StringBuilder();

			html.append("\n 					<tr> ");
			html.append("\n 						<td> ");
			html.append("\n 							" + ordemServicoVO.getOs().getId());
			html.append("\n 						</td> ");
			html.append("\n 						<td> ");
			html.append("\n 							" + ordemServicoVO.getOs().getContaParticao());
			html.append("\n 						</td> ");
			html.append("\n 						<td> ");
			html.append("\n 							" + ordemServicoVO.getOs().getCdCliente() + " - " + ordemServicoVO.getOs().getRazao());
			html.append("\n 						</td> ");
			html.append("\n 						<td> ");
			html.append("\n 							" + ordemServicoVO.getContrato().getNumCtr());
			html.append("\n 						</td> ");
			html.append("\n 					</tr>");

			return html.toString();
	}

	/**
	 * Monta o cabeçalho do e-mail, definindo o content-type, topo de tabela, e o título das colunas
	 * 
	 * @return cabeçalho em estrutura html
	 */
	private String montaCabecalho() {
		
		StringBuilder html = new StringBuilder();
		GregorianCalendar data = new GregorianCalendar();
		data.add(GregorianCalendar.DAY_OF_MONTH, 1);

		html.append(
				"\n <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> ");
		html.append("\n <html> ");
		html.append("\n 		<head> ");
		html.append("\n 			<meta charset=\"ISO-8859-1\"> ");
		html.append("\n 			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");
		html.append("\n 		</head> ");
		html.append("\n 		<body yahoo=\"yahoo\"> ");
		html.append("\n 		Prezado(a) representante, ");
		html.append("\n 		<br> ");
		html.append("\n 		Informamos que estão agendadas para <b>"+NeoDateUtils.safeDateFormat(data, "dd/MM/yyyy")+"</b> as instalações/habilitações abaixo: ");
		html.append("\n 		<br> ");
		html.append("\n 		<br> ");
		html.append("\n 			<table width=\"100%\"cellspacing=\"0\" cellpadding=\"2\" border=\"1\"> ");
		html.append("\n 					<tr> ");
		html.append("\n 						<td colspan=\"10\"align=\"center\">");
		html.append("\n 							<strong> Instalações/Habilitações </strong> ");
		html.append("\n 						</td> ");
		html.append("\n 					</tr> ");
		html.append("\n 					<tr> ");
		html.append("\n 						<td align=\"center\"> ");
		html.append("\n 							<strong>OS</strong> ");
		html.append("\n 						</td> ");
		html.append("\n 						<td align=\"center\"> ");
		html.append("\n 							<strong>Conta</strong> ");
		html.append("\n 						</td> ");
		html.append("\n 						<td align=\"center\"> ");
		html.append("\n 							<strong>Cliente</strong> ");
		html.append("\n 						</td> ");
		html.append("\n 						</td> ");
		html.append("\n 						<td align=\"center\"> ");
		html.append("\n 							<strong>Contrato</strong> ");
		html.append("\n 						</td> ");
		html.append("\n 					</tr>");

		return html.toString();
		
	}

	/**
	 * Captura todos os representantes ativos no Sistema para validação
	 * 
	 * @return lista com o código dos representantes
	 */
	public List<Long> retornaRepresentantesAtivos () {
		
		List<Long> listaRepresentantes = new ArrayList<>();
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		try {
			
			conn = PersistEngine.getConnection("SAPIENS");
			
			if (NeoUtils.safeIsNotNull(conn)) {
				
				sql.append(" SELECT CODREP FROM E090REP R ");
				sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.SECURITYENTITY S ");
				sql.append(" ON R.usu_usufus = S.code ");
				sql.append(" WHERE R.SITREP = 'A' AND S.ACTIVE = 1 ");
				sql.append(" AND ((R.INTNET IS NOT NULL) AND (R.INTNET != '')) ");
				sql.append(" AND S.NAME NOT LIKE '%ZZZ%' ");
				sql.append(" ORDER BY CODREP ");
				
				pstm = conn.prepareStatement(sql.toString());
				
				rs = pstm.executeQuery();
				
				while (rs.next()) {
					listaRepresentantes.add(rs.getLong("CODREP"));
				}
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("["+this.getClass().getSimpleName()+"] ERRO ao capturar os executivos ativos. ");
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaRepresentantes;
	}
	
	/**
	 * Captura todas as informações necessárias para o preenchimento e envio do e-mail
	 * 
	 * @param codigoRepresentante
	 * @return Lista chave-valor contendo os objetos de OS relacionados ao executivo
	 */
	public LinkedHashMap<Long, List<OrdemServicoVO>> retornaListaOS (Long codigoRepresentante) {
	
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		List<OrdemServicoVO> listaOS = new ArrayList<>();
		LinkedHashMap<Long, List<OrdemServicoVO>> hashOS = new LinkedHashMap<>();
		
		try {
			
			conn = PersistEngine.getConnection("SIGMA90");
			
			if (NeoUtils.safeIsNotNull(conn)) {
				
				sql.append(" SELECT O.ID_ORDEM OS, C.ID_CENTRAL CONTA, C.PARTICAO, C.CD_CLIENTE CLIENTE, C.RAZAO, C.CONTRATO, R.CODREP CODIGO_REPRESENTANTE, R.NOMREP REPRESENTANTE, R.INTNET EMAIL FROM dbORDEM O ");
				sql.append(" INNER JOIN dbCENTRAL C ON O.CD_CLIENTE = C.CD_CLIENTE ");
				sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160SIG S ON C.CD_CLIENTE = S.USU_CODCLI ");
				sql.append(" AND C.CONTRATO = S.USU_NUMCTR ");
				sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FCGContratoDadosGeraisSapiens D ON D.numeroContratoSapiens = C.CONTRATO ");
				sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FGCPrincipal P ON D.neoId = P.dadosGeraisContrato_neoId ");
				sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.X_SAPIENSE070FIL E ON P.empresa_neoId = E.neoId ");
				sql.append(" INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.E070FIL F ON E.codemp = F.codemp AND E.codfil = F.codfil ");
				sql.append(" INNER JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.X_SAPIENSEREP X ON D.representante_neoId = X.neoId ");
				sql.append(" INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.E090REP R ON X.codrep = R.codrep ");
				sql.append(" WHERE O.IDOSDEFEITO IN (134,184) ");
				sql.append(" AND O.FECHADO = 0 ");
				sql.append(" AND O.DEFEITO LIKE '%CONTRATO NOVO%' ");
				sql.append(" AND O.DATAAGENDADA BETWEEN ? AND ? ");
				sql.append(" AND ((R.INTNET IS NOT NULL) AND (R.INTNET != ' ')) ");
				sql.append(" AND R.CODREP = ? ");
				sql.append(" GROUP BY O.ID_ORDEM, C.ID_CENTRAL, C.PARTICAO, C.CD_CLIENTE, C.RAZAO, C.CONTRATO, R.CODREP, R.NOMREP, R.INTNET ");
				sql.append(" ORDER BY R.NOMREP ");
				
				pstm = conn.prepareStatement(sql.toString());
				
				pstm.setTimestamp(1, retornaPeriodo("inicio"));
				pstm.setTimestamp(2, retornaPeriodo("fim"));
				pstm.setLong(3, codigoRepresentante);
				
				rs = pstm.executeQuery();
				
				while (rs.next()) {
					
					OrdemServicoVO ordemVO = new OrdemServicoVO();
					DBOrdemVO dbOrdemVO = new DBOrdemVO();
					ContratoClienteVO contratoClienteVO = new ContratoClienteVO();
					RepresentanteVO representanteVO = new RepresentanteVO();
					
					dbOrdemVO.setId(rs.getLong("OS"));
					dbOrdemVO.setContaParticao(rs.getString("CONTA")+"["+rs.getString("PARTICAO")+"]");
					dbOrdemVO.setCdCliente(rs.getLong("CLIENTE"));
					dbOrdemVO.setRazao(rs.getString("RAZAO"));
					contratoClienteVO.setNumCtr(rs.getInt("CONTRATO"));
					representanteVO.setCodigoRep(rs.getLong("CODIGO_REPRESENTANTE"));
					representanteVO.setNomeRep(rs.getString("REPRESENTANTE"));
					representanteVO.setEmail(rs.getString("EMAIL"));
					
					ordemVO.setOs(dbOrdemVO);
					ordemVO.setContrato(contratoClienteVO);
					ordemVO.setRepresentante(representanteVO);
					
					listaOS.add(ordemVO);
				}
				
				if (eValido(listaOS)) {					
					hashOS.put(codigoRepresentante, listaOS);
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("["+this.getClass().getSimpleName()+"] ERRO ao capturar os dados da ordem de serviço. ");
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return hashOS;
	}
	
	/**
	 * Monta a data com os horários fixos
	 * 
	 * @param periodo
	 * @return período em timestamp
	 */
	public Timestamp retornaPeriodo (String periodo) {
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 1);
		
		if (periodo.equals("inicio")) {
			
			prazo.set(GregorianCalendar.HOUR_OF_DAY, 00);
			prazo.set(GregorianCalendar.MINUTE, 00);
			prazo.set(GregorianCalendar.SECOND, 00);
			
		} else if (periodo.equals("fim")) {
		
			prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			prazo.set(GregorianCalendar.MINUTE, 59);
			prazo.set(GregorianCalendar.SECOND, 59);
			
		}
		
		return new Timestamp(prazo.getTimeInMillis());
	}
	
	/**
	 * Valida se a variável é valida de acordo com a necessidade da classe
	 * 
	 * @param valor Lista de Objetos do tipo OSAgendamento
	 * @return <b> True </b> se a lista for válida, e <b> False </b> se o valor for inválida para a proposta da classe.
	 */
	public boolean eValido (LinkedHashMap<Long, List<OrdemServicoVO>> valor) {
		
		if (!valor.isEmpty() && valor.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Valida se a variável é valida de acordo com a necessidade da classe
	 * 
	 * @param valor Lista de Objetos do tipo OSAgendamento
	 * @return <b> True </b> se a lista for válida, e <b> False </b> se o valor for inválida para a proposta da classe.
	 */
	public boolean eValido (List<OrdemServicoVO> valor) {
		
		if (!valor.isEmpty() && valor.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
}
