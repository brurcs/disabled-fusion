package com.neomind.fusion.custom.orsegups.servlets.sapiensCommands;

import javax.servlet.http.HttpServletRequest;

public interface Command {

    public String execute(HttpServletRequest request);
    
}
