package com.neomind.fusion.custom.orsegups.contract;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.eform.converter.template.MaskTemplate;
import com.neomind.fusion.eform.mask.MaskFactory;
import com.neomind.fusion.entity.FieldInfo;
import com.neomind.fusion.entity.FieldInfoMaskTemplate;
import com.neomind.fusion.entity.FieldInfoText;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.portal.VelocityUtils;
import com.neomind.util.CustomFlags;
import com.neomind.util.NeoUtils;

public class ContractCEPConverter extends DefaultConverter{

	
	private static final Log log = LogFactory.getLog(ContractCEPConverter.class);

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		final String id = getFieldId(field);
		String rua = field.getForm().getField("endereco").getFieldId();
		String bairro = field.getForm().getField("bairro").getFieldId();
		
		String estado = field.getForm().getField("estadoCidade").getFieldId().replace("var_", "id_") + "estado__";
		String cidade = field.getForm().getField("estadoCidade").getFieldId().replace("var_", "id_") + "cidade__";
		
		String eform = field.getForm().getField("estadoCidade").getFieldId().replace("var_", "hid_");
		
		String botao = "<input type=\"button\" class=\"input_button\" id=\"bt_import\" value=\"Busca CEP\" onClick = \"javascript:buscaCEP("+ NeoUtils.safeOutputString(id) +", "+rua+", "+bairro+", "+estado+", "+cidade+", "+eform+");\"/>";
		
		StringBuilder outBuilder = new StringBuilder();
		
		String mask = field.getFieldInfo().getMask();
		
		if(mask.contains("mask:"))
			mask = "{" + (mask) + "}";
		else
			mask = "'" + (mask) + "'";
		
		
		outBuilder.append("<script type='text/javascript'>");
		outBuilder.append("$(document).ready(function() {");
		outBuilder.append("$('#" + field.getFieldId() + "').setMask(" + mask + ");");
		outBuilder.append("});");
		outBuilder.append("</script>");
		
		outBuilder.append(super.getHTMLInput(field, origin));
		
		outBuilder.append(botao);
		
		return outBuilder.toString();
	}
	
	@Override
	// otavio.campos - Pode suportar qualquer tipo de campo, mas a mascara tem que ser retirada do valor aqui.
	public Object getValueFromHTMLString(EFormField field, Object value)
	{
		String valor = NeoUtils.safeOutputString(value).replace("(", "");
		valor = valor.replace(")", "");
		valor = valor.replace(".", "");
		valor = valor.replace("-", "");
		valor = valor.replace(" ", "");
		return valor;
	}
}
