package com.neomind.fusion.custom.orsegups.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.custom.orsegups.medicaoContratos.GeraRelatorioMedicaoEmXLS;
import com.neomind.fusion.custom.orsegups.utils.RelatorioActionsEnum;
import com.neomind.fusion.custom.orsegups.utils.RelatoriosServices;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name = "RelatoriosServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet" })
public class RelatoriosServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public RelatoriosServlet()
	{
		super();
	}

	private String converteDataSript(String data) throws ParseException
	{
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime((Date) formatter.parse(data));
		return NeoUtils.safeDateFormat(calendar.getTime(), "yyyy-MM-dd");
	}

	protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException
	{

		String action = request.getParameter("action");

		if (NeoUtils.safeIsNull(action))
			throw new ServletException("Ação não definida");

		if (action.equals(RelatorioActionsEnum.RNF.getDescricao())) 
		{		    
			try
			{
				RelatoriosServices.gerarRelatorioRNF(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		
		} 
		else if (action.equals(RelatorioActionsEnum.PLANILHACHIP.getDescricao())) 
		{		    
			try
			{
				RelatoriosServices.gerarPlanilhaCHIP(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		
		} 
		else if (action.equals(RelatorioActionsEnum.PLANILHAREGIONAIS.getDescricao())) 
		{		    
			try
			{
				RelatoriosServices.gerarPlanilhaRegionais(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		
		}
		else if (action.equals(RelatorioActionsEnum.RELATORIOXPL1REGIONAL.getDescricao()))
		{
		    	String datIni = request.getParameter("dataInicio").trim();
			String datFim = request.getParameter("dataFim").trim();
			String cdMunicipio = request.getParameter("cdMunicipio").trim();
			String nome = request.getParameter("municipio").trim();
			try 
			{
				RelatoriosServices.gerarRelatorioXpl1Regional(response, datIni, datFim, cdMunicipio, nome);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.MNC.getDescricao()))
		{
			try
			{
				RelatoriosServices.gerarRelatorioMNC(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.COBERTURA_FERIAS.getDescricao()))
		{
			String usuCodMot = request.getParameter("numemp");
			String usuDatAlt = request.getParameter("datalt");
			String usuNumCad = request.getParameter("numcad");
			String usuTipCol = request.getParameter("tipcol");

			validateParameters(usuCodMot, usuDatAlt, usuNumCad, usuTipCol);

			try
			{
				RelatoriosServices.gerarRelatorioCoberturaFerias(response, usuCodMot, usuDatAlt, usuNumCad, usuTipCol);

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.COMISSOES_TERCEIRIZADOS.getDescricao()))
		{
			String codEmp = request.getParameter("codemp").trim();
			String codFil = request.getParameter("codfil").trim();
			String codIns = request.getParameter("codins").trim();
			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();
			String tipIns = request.getParameter("tipins").trim();
			String ponPro = request.getParameter("ponpro").trim();
			if (ponPro.contains("Z"))
			{
				ponPro = "0";
			}
			else
			{
				ponPro = "-1";
			}

			validateParameters(codEmp, codFil, datIni, datFim, tipIns, ponPro);

			try
			{
				String setFor = request.getParameter("setfor").trim();
				validateParameters(setFor);
				if (setFor.contains("PDF"))
				{
					String conDet = request.getParameter("condet").trim();
					validateParameters(conDet);

					RelatoriosServices.gerarRelatorioComissaoTerceirizadopdf(response, codEmp, codFil, converteDataSript(datIni), converteDataSript(datFim), tipIns, ponPro, conDet, codIns);
				}
				else
				{
					RelatoriosServices.gerarRelatorioComissaoTerceirizadoxls(response, codEmp, codFil, converteDataSript(datIni), converteDataSript(datFim), tipIns, ponPro, codIns);
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.PRODUTIVIDADE_CEREC.getDescricao()))
		{
			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();
			String nome = request.getParameter("fullName").trim();
			String setGrup = request.getParameter("setGrup").trim();

			validateParameters(datIni, datFim);

			try
			{
				String setFor = request.getParameter("setfor").trim();
				validateParameters(setFor);
				if (setFor.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioProdutividadeCERECpdf(response, converteDataSript(datIni), converteDataSript(datFim), nome, setGrup);
				}
				else
				{
					RelatoriosServices.gerarRelatorioProdutividadeCERECxls(response, converteDataSript(datIni), converteDataSript(datFim), nome, setGrup);
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.NEGOCIACAO_CEREC.getDescricao()))
		{
			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();

			validateParameters(datIni, datFim);

			try
			{
				String setFor = request.getParameter("setfor").trim();
				validateParameters(setFor);
				if (setFor.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioNegociacaoCERECpdf(response, converteDataSript(datIni), converteDataSript(datFim));
				}
				else
				{
					RelatoriosServices.gerarRelatorioNegociacaoCERECxls(response, converteDataSript(datIni), converteDataSript(datFim));
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.PRODUTIVIDADE_CERECOS.getDescricao()))
		{
			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();

			validateParameters(datIni, datFim);

			try
			{
				String setFor = request.getParameter("setfor").trim();
				validateParameters(setFor);
				if (setFor.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioProdutividadeCERECOSpdf(response, converteDataSript(datIni), converteDataSript(datFim));
				}
				else
				{
					RelatoriosServices.gerarRelatorioProdutividadeCERECOSxls(response, converteDataSript(datIni), converteDataSript(datFim));
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.SUPERINTENDENCIA.getDescricao()))
		{
			String competencia = request.getParameter("setSuperintendEncia").trim();
			int compet = Integer.parseInt(competencia);
			String forSuperintendencia = request.getParameter("setForSuperintendEncia").trim();

			try
			{
				validateParameters(forSuperintendencia);
				if (forSuperintendencia.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioSuperintendenciaPDF(response, compet);
				}
				else
				{
					RelatoriosServices.gerarRelatoriogerarRelatorioSuperintendenciaSxls(response, compet);
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.RELATORIOCOBRANCAELETRONICA.getDescricao()))
		{
			String setRel = request.getParameter("setRelCobEletr").trim();
			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();
			String forma = request.getParameter("setfor").trim();

			validateParameters(datIni, datFim);

			try
			{
				validateParameters(forma);

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				GregorianCalendar calIni = (GregorianCalendar) GregorianCalendar.getInstance();
				calIni.setTime(sdf.parse(datIni));

				GregorianCalendar calFim = (GregorianCalendar) GregorianCalendar.getInstance();
				calFim.setTime(sdf.parse(datFim));
				if (somaData(calIni, calFim) <= 31)
				{
					if (forma.contains("PDF"))
					{
						RelatoriosServices.gerarRelatorioCobrancaEletronicaInadinpmenciaCpdf(response, converteDataSript(datIni), converteDataSript(datFim), setRel);
					}
					else
					{
						RelatoriosServices.gerarRelatorioCobrancaEletronicaInadinpmenciaxls(response, converteDataSript(datIni), converteDataSript(datFim), setRel);
					}
				}
				else
				{
					response.setContentType("text/html;charset=UTF-8");
					PrintWriter out = response.getWriter();

					out.println("errorDias");
					out.close();
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.RELATORIOCOBRANCAHUMANA.getDescricao()))
		{
			String setRel = request.getParameter("setRelCobHum").trim();
			String datIni = request.getParameter("datIniCobHum").trim();
			String datFim = request.getParameter("datFimCobHum").trim();
			String forma = request.getParameter("setForCobHum").trim();

			validateParameters(datIni, datFim);

			try
			{
				validateParameters(forma);

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				GregorianCalendar calIni = (GregorianCalendar) GregorianCalendar.getInstance();
				calIni.setTime(sdf.parse(datIni));

				GregorianCalendar calFim = (GregorianCalendar) GregorianCalendar.getInstance();
				calFim.setTime(sdf.parse(datFim));
				if (somaData(calIni, calFim) <= 31)
				{
					if (forma.contains("PDF"))
					{
						RelatoriosServices.gerarRelatorioCobrancaHumanaInadinpmenciaCpdf(response, converteDataSript(datIni), converteDataSript(datFim), setRel);
					}
					else
					{
						RelatoriosServices.gerarRelatorioCobrancaHumanaInadinpmenciaxls(response, converteDataSript(datIni), converteDataSript(datFim), setRel);
					}
				}
				else
				{
					response.setContentType("text/html;charset=UTF-8");
					PrintWriter out = response.getWriter();

					out.println("errorDias");
					out.close();
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.RELATORIODEBITOAUTOMATICO.getDescricao()))
		{
			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();

			validateParameters(datIni, datFim);

			try
			{
				String setFor = request.getParameter("setfor").trim();
				validateParameters(setFor);

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				GregorianCalendar calIni = (GregorianCalendar) GregorianCalendar.getInstance();
				calIni.setTime(sdf.parse(datIni));

				if (setFor.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioDebitoAutomaticopdf(response, converteDataSript(datIni), converteDataSript(datFim));
				}
				else
				{
					RelatoriosServices.gerarRelatorioDebitoAutomaticoxls(response, converteDataSript(datIni), converteDataSript(datFim));
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.RELATORIOCOCHIPGPRS.getDescricao()))
		{
			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();

			validateParameters(datIni, datFim);

			try
			{
				String setFor = request.getParameter("setfor").trim();
				validateParameters(setFor);

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				GregorianCalendar calIni = (GregorianCalendar) GregorianCalendar.getInstance();
				calIni.setTime(sdf.parse(datIni));

				if (setFor.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioChipGprsPdf(response, converteDataSript(datIni), converteDataSript(datFim));
				}
				else
				{
					RelatoriosServices.gerarRelatorioChipGprsXls(response, converteDataSript(datIni), converteDataSript(datFim));
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.CONCILIACAO.getDescricao()))
		{
			try
			{
				String setFor = request.getParameter("setfor").trim();
				String setRelCon = request.getParameter("setRelCon").trim();

				validateParameters(setFor);

				switch (setRelCon)
				{
					case "SapXSigPri":
						if (setFor.contains("PDF"))
							RelatoriosServices.gerarRelatorioSapXSigPdf(response, 1);
						else
							RelatoriosServices.gerarRelatorioSapXSigXls(response, 1);
						break;

					case "SapXSigPub":
						if (setFor.contains("PDF"))
							RelatoriosServices.gerarRelatorioSapXSigPdf(response, 2);
						else
							RelatoriosServices.gerarRelatorioSapXSigXls(response, 2);
						break;

					case "SigXSap":
						if (setFor.contains("PDF"))
							RelatoriosServices.gerarRelatorioSigXSapPdf(response);
						else
							RelatoriosServices.gerarRelatorioSigXSapXls(response);
						break;
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		else if (action.equals(RelatorioActionsEnum.RELATORIOMEDICAOCONTRATOS.getDescricao()))
		{
			try
			{
				String periodo = request.getParameter("periodo").trim();
				String setFor = request.getParameter("setfor").trim();
				String numctr = request.getParameter("numctr").trim();
				String empresa = request.getParameter("empresa").trim();
				String cliente = request.getParameter("clienteRel").trim();
				String isComercial = request.getParameter("isComercial").trim();
				String imprimirObs = request.getParameter("imprimirObs");
				String numCtrOfi = request.getParameter("numCtrOfi");
				String dataFiltro = periodo + "-01";
				String dataCortoAfa = periodo + "-01";

				GregorianCalendar datFilto = new GregorianCalendar();
				datFilto.setTime(NeoCalendarUtils.formatDate(dataFiltro, "yyyy-MM-dd"));
				datFilto.add(GregorianCalendar.MONTH, 1);
				dataFiltro = NeoCalendarUtils.formatDate(datFilto.getTime(), "yyyy/MM/dd");

				if (!request.getParameter("empresa").trim().equals("Selecione a empresa "))
				{
					empresa = empresa.substring(0, 2);
				}

				String dataFormat = "";
				dataFormat = periodo.substring(5, 7) + "/" + periodo.substring(0, 4);
				//Colocado as tabelas do vetorh para ordenar igual ao presença.
				String sql = " SELECT ORN.NUMLOC,CLIENTE,CONTRATO,EMPRESACONTRATADA,RESPONSAVEL,PERIODO,CODREGIONAL,REGIONAL,CARGAHORPOS,FUNCAO,LOCAL,NOMECOLABORADOR, ";
				sql += " DESCMED,SITUACAO,C.DATADM,M.QTDVAGAS,NUMCADCOLABORADOR,OBSDIARIA,QTDDIARIAS AS QTDDIA,P.NUMPOS,P.NOMLOC,P.NVAGAS,P.NCOLABORADORES,C.horEx," + "C.minEx,C.horMed,C.minMed";
				if (imprimirObs != null && !imprimirObs.equals("Nao"))
				{
					sql += ",ISFALTA,DESCRICAO ";
				}
				else
				{
					sql += ", null as descricao, null as isFalta";
				}
				if (isComercial.equals("SIM"))
				{
					sql += " ,vlrPar,vlrCtr";
				}
				sql += "  FROM D_MEDMedicaoDeContratos";
				sql += "  M ";
				sql += "  INNER JOIN D_MEDMedicaoDeContratos_listaPostos ML ON ML.D_MEDMEDICAODECONTRATOS_NEOID = M.neoId";
				sql += "  INNER JOIN D_MEDPOSTO P ON P.NEOID = ML.listaPostos_neoId ";
				sql += "  INNER JOIN D_MEDPOSTO_listaColaboradores MC ON	MC.D_MEDPOSTO_neoId = P.neoId";
				sql += "  INNER JOIN D_MEDColaboradores C ON 	C.NEOID = MC.listaColaboradores_neoId   ";
				if (imprimirObs != null && !imprimirObs.equals("Nao"))
				{
					sql += "  LEFT JOIN D_MEDColaboradores_listaConsideracoes CCN ON  	CCN.D_MEDColaboradores_neoId = MC.listaColaboradores_neoId";
					sql += "  LEFT JOIN D_MEDConsideracoes CN ON CN.neoId = 	CCN.listaConsideracoes_neoId   ";
				}

				sql += "  left join D_MEDMedicaoDeContratos_listaDiarias lisdia on lisdia.D_MEDMedicaoDeContratos_neoId = M.neoId";
				sql += "  left join d_medDiarias dia on dia.neoid = lisdia.listaDiarias_neoId ";
				sql += "  INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R034FUN FUN ON fun.NUMCAD = C.numCadColaborador AND LTRIM(RTRIM(fun.nomfun)) = LTRIM(RTRIM(C.nomeColaborador)) and ((fun.sitafa <> 7) or (fun.sitafa = 7 and fun.datafa >= '" + dataCortoAfa + "' and fun.caudem <> 6))  ";
				sql += "  INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM [FSOODB04\\SQL02].VETORH.DBO.R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT < '" + dataFiltro + "')";
				sql += "  INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R006ESC esc ON esc.CodEsc = hes.CodEsc   ";
				sql += "  INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM [FSOODB04\\SQL02].VETORH.DBO.R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT < '" + dataFiltro + "')";
				sql += "  INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R016ORN ORN ON ORN.numloc = hlo.numloc AND ORN.TABORG = '203'  ";
				sql += "  INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From [FSOODB04\\SQL02].VETORH.DBO.USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt < '" + dataFiltro + "')";
				sql += "  INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM [FSOODB04\\SQL02].VETORH.DBO.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc)  ";

				if (((!periodo.equals("")) || (periodo != null)) || ((!numctr.equals("") || numctr != null)))
				{
					sql = sql + " WHERE ";
				}

				if (!periodo.equals(""))
				{
					sql = sql + " periodo ='" + dataFormat + "'";
				}

				if (!numctr.equals(""))
				{
					sql = sql + " and contrato in(" + numctr + ")";
				}

				if (!empresa.equals("Selecione a empresa"))
				{
					sql = sql + " and codEmpresaContratada in(" + empresa + ")";
				}

				if (!cliente.equals(""))
				{
					sql = sql + " and cliente like '%" + cliente + "%'";
				}

				if (!numCtrOfi.equals(""))
				{
					sql = sql + " and numOfi like '%" + numCtrOfi + "%'";
				}

				sql = sql + " order by p.codLoc, m.cliente,hie.codloc,orn.usu_datfim desc, orn.usu_cliorn, cvs.usu_sitcvs DESC,C.nomeColaborador ";
				if (setFor.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioMedicaoContratosPdf(response, periodo, sql, isComercial);
				}
				else
				{
					GeraRelatorioMedicaoEmXLS.execute(response, sql);
				}
			}

			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		} else if (action.equals(RelatorioActionsEnum.RELATORIOCHP11.getDescricao())) {
		    try
			{
				RelatoriosServices.gerarRelatorioChp1_1(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		} else if (action.equals(RelatorioActionsEnum.RELATORIOCHP12.getDescricao())) {
		    try
			{
				RelatoriosServices.gerarRelatorioChp1_2(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		} else if (action.equals(RelatorioActionsEnum.RELATORIOCHP13.getDescricao())) {
		    try
			{
				RelatoriosServices.gerarRelatorioChp1_3(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		} else if (action.equals(RelatorioActionsEnum.RELATORIOCHP14.getDescricao())) {
		    try
			{
				RelatoriosServices.gerarRelatorioChp1_4(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		} else if (action.equals(RelatorioActionsEnum.TAREFASDIRECIONADASG002.getDescricao())) {

			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();
			String setor = request.getParameter("setor").trim();
			String tipo = request.getParameter("tipo").trim();

			validateParameters(datIni, datFim);

			try
			{
				String formato = request.getParameter("formato").trim();
				validateParameters(formato);
				if (formato.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioTarefasDirecionadaspdf(response, setor, tipo, converteDataSript(datIni), converteDataSript(datFim));
				}
				else
				{
					RelatoriosServices.gerarRelatorioTarefasDirecionadasXls(response, setor, tipo, converteDataSript(datIni), converteDataSript(datFim));
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		} else if (action.equals(RelatorioActionsEnum.RELATORIORSC.getDescricao())) {

			String datIni = request.getParameter("datini").trim();
			String datFim = request.getParameter("datfim").trim();
			String solicitante = request.getParameter("colaborador").trim();
			String categoria = returnRSCProcessName(request.getParameter("categoria").trim());
			String subcategoria = request.getParameter("subcategoria").trim();

			validateParameters(datIni, datFim);

			try
			{
				String formato = request.getParameter("formato").trim();
				validateParameters(formato);
				if (formato.contains("PDF"))
				{
					RelatoriosServices.gerarRelatorioRSCpdf(response, solicitante, categoria, converteDataSript(datIni), converteDataSript(datFim), subcategoria);
				}
				else
				{
					RelatoriosServices.gerarRelatorioRSCXls(response, solicitante, categoria, converteDataSript(datIni), converteDataSript(datFim), subcategoria);
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			}
		}
		
	}

	private void validateParameters(String... parameters) throws ServletException
	{
		for (String parameter : parameters)
		{
			if (NeoUtils.safeIsNull(parameter))
			{
				throw new ServletException("Parâmetro inválido");
			}
		}
	}

	public int somaData(GregorianCalendar dataInicio, GregorianCalendar dataFim)
	{
		Date data1 = dataFim.getTime();
		Date data2 = dataInicio.getTime();

		double diffDias = Math.floor((data1.getTime() - data2.getTime()) / 1000.0 / 86400.00);
		int inteiro = (int) diffDias;
		return inteiro;
	}
	
	private String returnRSCProcessName(String codigoCategoria) {
		String categoria = null;
		
		if(codigoCategoria.equals("RSCNOVO")) {
			categoria = "C027 - RSC - Relatório de Solicitação de Cliente Novo";
		}else if(codigoCategoria.equals("DIVERSOS")) {
			categoria = "C027.001 - RSC - Categoria Diversos";
		}else if(codigoCategoria.equals("ORCAMENTO")) {
			categoria = "C027.002 - RSC - Categoria Orçamento";
		}else if(codigoCategoria.equals("ATUALIZACAO")) {
			categoria = "C027.004 - RSC - Categoria Atualização Cadastral";
		}else if(codigoCategoria.equals("ALTERACAO")) {
			categoria = "C027.005 - RSC - Categoria Alteração Boleto NF";
		}else if(codigoCategoria.equals("CANCELAMENTO")) {
			categoria = "C027.006 - RSC - Categoria Cancelamento NOVO";
		}
		
		return categoria;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.process(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.process(request, response);
	}

}
