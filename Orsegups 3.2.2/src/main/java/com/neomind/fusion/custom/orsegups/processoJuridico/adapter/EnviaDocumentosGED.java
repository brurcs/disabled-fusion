package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.e2doc.E2docIndexacaoBean;
import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class EnviaDocumentosGED implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) {

		E2docIndexacaoBean e2docIndexacaoBean = new E2docIndexacaoBean();
		e2docIndexacaoBean.setTipo("i");
		e2docIndexacaoBean.setModelo("jurídico");
		e2docIndexacaoBean.setClasse("processos");

		try {
			List<NeoObject> registroAtividadeArquivo = processEntity.findGenericValue("j002RegistroArquivo");

			for (NeoObject ra : registroAtividadeArquivo) {
				
				EntityWrapper wArquivo = new EntityWrapper(ra);
				NeoFile arquivo = wArquivo.findGenericValue("anexo");

				StringBuilder indice = new StringBuilder();
				indice.append("<indice0>NÚMERO DO PROCESSO</indice0>");
				indice.append("<valor0>");
				indice.append(processEntity.findGenericValue("numPro"));
				indice.append("</valor0>");
				indice.append("<indice1>TIPO DE PROCESSO</indice1>");
				indice.append("<valor1>");
				indice.append(processEntity.findGenericValue("tipoProcesso.descricao"));
				indice.append("</valor1>");
				indice.append("<indice2>UNIDADE FEDERATIVA</indice2>");
				indice.append("<valor2>");
				indice.append(processEntity.findGenericValue("unidadeFederativa"));
				indice.append("</valor2>");
				indice.append("<indice3>AUTOR</indice3>");
				indice.append("<valor3></valor3>");
				indice.append("<indice4>RÉU</indice4>");
				indice.append("<valor4></valor4>");
				indice.append("<indice5>MATRICULA</indice5>");
				indice.append("<valor5></valor5>");
				indice.append("<indice6>CODIGO DO CLIENTE NO SAPIENS</indice6>");
				indice.append("<valor6>"+processEntity.findGenericValue("cliente.codcli")+"</valor6>");

				e2docIndexacaoBean.setFormato(arquivo.getSufix());
				e2docIndexacaoBean.setDocumento(arquivo.getBytes());
				e2docIndexacaoBean.setIndices(indice.toString());

				E2docUtils.indexarDocumentoJuridico(e2docIndexacaoBean);
			}
		} catch (Exception e) {
			System.out.println("ERRO EnviaDocumentosGED");
			e.printStackTrace();
			throw new WorkflowException("ERRO EnviaDocumentosGED");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		// TODO Auto-generated method stub

	}

}
