package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.ContratoVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class AbreTarefaNegociacaoReajuste implements CustomJobAdapter {
	private static final Log log = LogFactory.getLog(AbreTarefaNegociacaoReajuste.class);
	
	public static String mensagemErro = "Erro ao executar a Rotina AbreTarefaNegociacaoReajuste";
	
	@Override
	public void execute(CustomJobContext ctx) {
	
		System.out.println("#### INICIO DA ROTINA ABRETAREFANEGOCIACAOREAJUSTE - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		
		LinkedList<ContratoVO> listaContratos = new LinkedList<>();
		
		try {
			
			listaContratos = retornaContratosNegociacao();
			
			if(NeoUtils.safeIsNotNull(listaContratos)) {
				abreTarefaContratos(listaContratos);				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### ERRO DA ROTINA ABRETAREFANEGOCIACAOREAJUSTE - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			throw new WorkflowException(mensagemErro);
		} finally {
			System.out.println("#### FIM DA ROTINA ABRETAREFANEGOCIACAOREAJUSTE - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
		
	}

	
	private void abreTarefaContratos(LinkedList<ContratoVO> listaContratos) {
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		NeoUser solicitante = null;
		NeoUser executor = null;
		NeoPaper papelSolicitante = null;
		NeoPaper papelExecutor = null;
		String titulo = null;
		String descricao = null;
		 
		GregorianCalendar prazo = new GregorianCalendar();
		
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		
		try {

			papelSolicitante = OrsegupsUtils.getPaper("SolicitanteTarefaNegociacaoReajuste");
			papelExecutor = OrsegupsUtils.getPaper("ExecutorTarefaNegociacaoReajuste");
			
			solicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelSolicitante);
			executor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);
			
			
			for(ContratoVO contratoVO : listaContratos) {
				
				titulo = "Negociação Reajuste - "+contratoVO.getNomeCliente();
				
				descricao = retornaDescricao(contratoVO);
				
				iniciarTarefaSimples.abrirTarefa(solicitante.getCode(), executor.getCode(), titulo, descricao, "1", "sim", prazo);
			}	
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = this.getClass().getSimpleName() +" - ERRO ao abrir tarefa de Negociação de Reajuste";
			throw new WorkflowException(mensagemErro);
		}
		
		
	}


	private String retornaDescricao(ContratoVO contratoVO) {
		
		
		GregorianCalendar gcInicio = new GregorianCalendar();
		gcInicio.setTime(contratoVO.getDataInicioVigencia());
		
		String inicioDaVigencia = NeoDateUtils.safeDateFormat(gcInicio, "dd/MM/yyyy HH:mm:ss");
			
		StringBuilder corpoTarefa = new StringBuilder();
		
		corpoTarefa.append(" <html>                                                  				   												 ");
        corpoTarefa.append("    <head>                                              				   												 ");
        corpoTarefa.append("        <meta charset='utf-8'>                           				   												 ");
        corpoTarefa.append("    </head>                                              				   												 ");
		corpoTarefa.append("      <body>                                             				   												 ");
		corpoTarefa.append("          <table border='2'>                             				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("                  <th>EMPRESA/FILIAL</th>                      				   										     ");
		corpoTarefa.append("                  <td>&nbsp; "+contratoVO.getEmpresa()+" - "+contratoVO.getFilial()+" </td>       		      			 ");
		corpoTarefa.append("              </tr>                                       				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("              	<th>NOME DO CLIENTE</th>                 				   												 ");
		corpoTarefa.append("                <td>&nbsp; "+contratoVO.getNomeCliente()+" </td>           												 ");
		corpoTarefa.append("              </tr>                                      				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("              	<th>NÚMERO DO CONTRATO</th>              				   												 ");
		corpoTarefa.append("                <td>&nbsp; "+contratoVO.getNumeroContrato()+" </td>        												 ");
		corpoTarefa.append("              </tr>                                    					   												 ");
		corpoTarefa.append("              <tr>                                      				   												 ");
		corpoTarefa.append("              	<th>NÚMERO OFICIAL DO CONTRATO</th>      				   												 ");
		corpoTarefa.append("                <td>&nbsp; "+contratoVO.getNumeroOficialContrato()+" </td> 												 ");
		corpoTarefa.append("              </tr>                                      				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("              	<th>ÍNDICE DE REAJUSTE</th>               				   												 ");
		corpoTarefa.append("                <td>&nbsp; "+contratoVO.getCodigoIndiceReajuste()+" - " +contratoVO.getDescricaoIndiceReajuste() +" </td>");
		corpoTarefa.append("              </tr>                                       				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");
		corpoTarefa.append("              	<th>INÍCIO DA VIGÊNCIA</th>               				   												 ");
		corpoTarefa.append("                <td>&nbsp; "+inicioDaVigencia+" </td>        		 	   												 ");
		corpoTarefa.append("              </tr>                                       				   												 ");
		corpoTarefa.append("              <tr>                                       				   												 ");      
		corpoTarefa.append("          </table>                                      				   												 ");
		corpoTarefa.append("      </body>                                             				   												 ");
		corpoTarefa.append(" </html>                                                  				   												 ");
		
		return corpoTarefa.toString();
	}


	public LinkedList<ContratoVO> retornaContratosNegociacao(){
		
		LinkedList<ContratoVO> listaContratos = new LinkedList<>();
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		
		GregorianCalendar dataAtual = new GregorianCalendar();
		dataAtual.add(GregorianCalendar.DAY_OF_MONTH, 30);
		dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 0);
		dataAtual.set(GregorianCalendar.MINUTE, 0);
		dataAtual.set(GregorianCalendar.SECOND, 0);
		dataAtual.set(GregorianCalendar.MILLISECOND, 0);
		
		String dataInicio = NeoDateUtils.safeDateFormat(dataAtual, "yyyy-MM-dd HH:mm:ss");
		
		try {
			
			/**
			 * IDR = Índice de Reajuste
			 * 
			 */
			sql.append(" SELECT CTR.USU_CODEMP EMPRESA, CTR.USU_CODFIL FILIAL, CLI.NOMCLI CLIENTE, CTR.USU_NUMCTR CONTRATO,  ");
			sql.append(" CTR.USU_NUMOFI CONTRATO_OFICIAL, MOE.CODMOE CODIGO_IDR, MOE.DESMOE DESCRICAO_IDR, CTR.USU_INIVIG INICIO_VIGENCIA ");
			sql.append(" FROM USU_T160CTR CTR ");
			sql.append(" INNER JOIN E085CLI CLI ");
			sql.append(" ON CTR.USU_CODCLI = CLI.CODCLI ");
			sql.append(" INNER JOIN E031MOE MOE ");
			sql.append(" ON MOE.CODMOE = CTR.USU_CODFCR ");
			sql.append(" WHERE CTR.USU_INIVIG = ? ");
			sql.append(" AND CLI.TIPEMC = 2 AND CTR.USU_CODEMP NOT IN (24) ");
			sql.append(" AND ((CTR.USU_SITCTR = 'A') OR (CTR.USU_SITCTR = 'I' AND CTR.USU_DATFIM >= GETDATE())) ");
			
			conn = PersistEngine.getConnection("SAPIENS");
			
			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, dataInicio);

			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				ContratoVO contratoVO = new ContratoVO();
				
				contratoVO.setEmpresa(rs.getLong("EMPRESA"));
				contratoVO.setFilial(rs.getLong("FILIAL"));
				contratoVO.setNomeCliente(rs.getString("CLIENTE"));
				contratoVO.setNumeroContrato(rs.getLong("CONTRATO"));
				contratoVO.setNumeroOficialContrato(rs.getString("CONTRATO_OFICIAL"));
				contratoVO.setCodigoIndiceReajuste(rs.getLong("CODIGO_IDR"));
				contratoVO.setDescricaoIndiceReajuste(rs.getString("DESCRICAO_IDR"));
				contratoVO.setDataInicioVigencia(rs.getTimestamp("INICIO_VIGENCIA"));
				
				listaContratos.add(contratoVO);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			mensagemErro = this.getClass().getSimpleName() +" - ERRO ao abrir tarefa de Negociação de Reajuste";
			throw new WorkflowException(mensagemErro);
		}

		return listaContratos;
	}
	
}
