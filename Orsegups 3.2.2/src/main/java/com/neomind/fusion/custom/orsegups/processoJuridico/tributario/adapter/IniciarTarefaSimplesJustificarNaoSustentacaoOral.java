package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.IniciarTarefaSimplesJustificarNaoSustentacaoOral

public class IniciarTarefaSimplesJustificarNaoSustentacaoOral implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{		
		Integer iTipoProcesso = 0;
		NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
    	    	if (tipoProcesso != null) {
    	    	    EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
    	    	    iTipoProcesso = Integer.parseInt(wTipoProcesso.getValue("codigo").toString());
    	    	}
    	    	
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<NeoObject> registroAtividade = (List<NeoObject>) processEntity.getValue("j002RegistroHistoricoAtividade");
		String titulo = "";
		String descricao = "";
		String executor = "";
		String solicitante = "";
		GregorianCalendar prazo = null;
		List<NeoObject> listaOqueFazerHistorico = (List<NeoObject>) processEntity.findValue("j002HistoricoOqueFazer");

		if (iTipoProcesso == 3) {
		    titulo = "J002 - Tributário" + activity.getCode() + " - Informar motivo da não Sustentação oral";
		} else {
        		if (processEntity.getValue("colaborador") != null)
        		{
        			NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
        			EntityWrapper wColaborador = new EntityWrapper(colaborador);
        			colaborador = (NeoObject) processEntity.getValue("colaborador");
        
        			titulo = "J002 - " + activity.getCode() + " - " + wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun");
        		}
        		else
        		{
        			NeoObject autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
        			EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);
        			String cpfAut = String.valueOf(wAutor.getValue("cpfAut"));
        			String nomAut = String.valueOf(wAutor.getValue("nomAut"));
        			titulo = "J002 - " + activity.getCode() + " - " + nomAut;
        			if (cpfAut != null && !cpfAut.equals(""))
        			{
        				titulo = titulo + " / " + cpfAut;
        			}
        
        		}
        		titulo = titulo + " - Informar motivo da não Sustentação oral.";
		}

		descricao = "Segue resposta sobre a não realização da Sustentação Oral: <br><br>" + String.valueOf(processEntity.findValue("obsSusOra"));
		NeoUser usr = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("name", origin.returnResponsible()));
		solicitante = "dilmoberger";

		prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		executor = usr.getCode();

		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
		String code = tarefaSimples.abrirTarefaReturnNeoIdCode(solicitante, executor, titulo, descricao, "1", "Sim", prazo);
		String[] neoIdECode = code.split(";");
		System.out.println("Tarefa aberta : " + code);

		String responsavel = origin.returnResponsible();
		String atividade = origin.getActivityName().toString();
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002RegistroHistoricoAtividade");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
		wRegAti.setValue("dataAcao", new GregorianCalendar());
		wRegAti.setValue("atividade", atividade);
		wRegAti.setValue("responsavel", responsavel);
		String descricaoRegistro = "Aberto tarefa simples " + code + " para responder a não Sustentação Oral, referente a este processo.";
		wRegAti.setValue("descricao", descricaoRegistro);

		PersistEngine.persist(objRegAti);
		registroAtividade.add(objRegAti);

		InstantiableEntityInfo insRegAtiOqueFazer = AdapterUtils.getInstantiableEntityInfo("j002HistoricoOqueFazer");
		NeoObject objRegAtiOqueFazer = insRegAtiOqueFazer.createNewInstance();
		EntityWrapper wRegAtiOQF = new EntityWrapper(objRegAtiOqueFazer);
		wRegAtiOQF.setValue("executorTar", executor);
		wRegAtiOQF.setValue("titulo", titulo);
		wRegAtiOQF.setValue("descricao", descricao);
		wRegAtiOQF.setValue("prazo", prazo);
		wRegAtiOQF.setValue("anexo", null);
		wRegAtiOQF.setValue("solicitante", solicitante);
		wRegAtiOQF.setValue("neoIdTarefa", Long.parseLong(neoIdECode[0]));
		PersistEngine.persist(objRegAtiOqueFazer);
		listaOqueFazerHistorico.add(objRegAtiOqueFazer);

		processEntity.setValue("j002RegistroHistoricoAtividade", registroAtividade);
		processEntity.setValue("j002HistoricoOqueFazer", listaOqueFazerHistorico);
	    }catch(Exception e){
		System.out.println("Erro na classe IniciarTarefaSimplesJustificarNaoSustentacaoOral do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
