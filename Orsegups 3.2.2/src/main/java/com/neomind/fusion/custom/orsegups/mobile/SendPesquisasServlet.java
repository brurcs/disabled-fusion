package com.neomind.fusion.custom.orsegups.mobile;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.vo.AssuntoVO;
import com.neomind.fusion.custom.orsegups.mobile.vo.PerguntaVO;
import com.neomind.fusion.custom.orsegups.mobile.vo.PesquisaVO;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;

/**
 * Essa classe não é mais utilizada. Utilizar a mesma classe do pacote send
 * @author neomind
 *
 */
@Deprecated
public class SendPesquisasServlet extends HttpServlet   {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String pesquisa(){
		
		InstantiableEntityInfo pesquisa = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByType("Pesquisa");
		List<NeoObject> newObjPesq = (List<NeoObject>) PersistEngine.getObjects(pesquisa.getEntityClass());
		String json = null;
		int iPes = 0;
		ArrayList<Object> pesquisas = new ArrayList<Object>();

		for (NeoObject pesquisaQuery : newObjPesq)
		{
			PesquisaVO pvo = new PesquisaVO();
			EntityWrapper wrapperPesq = new EntityWrapper(pesquisaQuery);

			pvo.setCod_pesq(String.valueOf(wrapperPesq.getObject().getNeoId()));
			pvo.setDesc_pesq((String) wrapperPesq.getValue("pesquisatxt"));				
			for(int i =0; i<= newObjPesq.size(); i++){
				

			}
			List<NeoObject> newObjAssun = ((List<NeoObject>) wrapperPesq.findValue("neoidAssunto"));

			for (NeoObject assuntoQuery : newObjAssun)
			{

				EntityWrapper wrapperAssun = new EntityWrapper(assuntoQuery);
				  
				for(int i =0; i<= newObjAssun.size(); i++){
					pvo.setCod_assun(String.valueOf(wrapperAssun.getObject().getNeoId()));
					pvo.setDesc_assun((String) wrapperAssun.getValue("descassun"));
					//
				}
				List<NeoObject> newObjPerg = ((List<NeoObject>) wrapperAssun.findValue("neoidPergunta"));
				for (NeoObject perguntaQuery : newObjPerg)
				{

					EntityWrapper wrapperPerg = new EntityWrapper(perguntaQuery);
					 
					for(int i =0; i<= newObjPerg.size(); i++){
					
						
							
						
						pvo.setCod_perg(String.valueOf(wrapperPerg.getObject().getNeoId()));
						pvo.setDesc_perg((String) wrapperPerg.getValue("perguntatxt"));
						pvo.setTip_resposta((String) wrapperPerg.getValue("tipresposta"));
						
						
					}

				}
				pesquisas.add(pvo);
				 Gson gson = new Gson();
				json = gson.toJson(pesquisas);

			}

		

		}
		System.out.println("Enviando pesquisas para mobile[com.neomind.fusion.custom.orsegups.mobile.SendPesquisasServlet]: " + json);
		return json;

	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//response.setContentType("application/json");
		response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		try {
			out.print(pesquisa());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//pesquisa();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException {

		response.setContentType("application/json");
		//response.setContentType("application/json");


		PrintWriter out = response.getWriter();
		out.print(pesquisa());
		//pesquisa();


	}	
}	
