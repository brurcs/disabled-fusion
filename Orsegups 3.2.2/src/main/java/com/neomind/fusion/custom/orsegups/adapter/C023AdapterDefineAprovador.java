package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class C023AdapterDefineAprovador implements AdapterInterface {
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		
		try {
			
			NeoPaper papelAprovador = OrsegupsUtils.getPaper("C023AprovadorSolicitacao");
			NeoUser aprovador = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelAprovador);
			
			processEntity.findField("aprovadorSolicitacao").setValue(aprovador);
			
		} catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException(e.getErrorList().get(0).getMessage().getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException(this.getClass().getSimpleName()+" - "+e.getMessage());
		}
		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}
