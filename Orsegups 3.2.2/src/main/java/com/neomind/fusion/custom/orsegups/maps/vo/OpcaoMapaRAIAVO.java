package com.neomind.fusion.custom.orsegups.maps.vo;

public class OpcaoMapaRAIAVO
{
	private String id;
	
	private String opcao;
	
	private String eventos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOpcao() {
		return opcao;
	}

	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}

	public String getEventos() {
		return eventos;
	}

	public void setEventos(String eventos) {
		this.eventos = eventos;
	}	

	
		
	
}
