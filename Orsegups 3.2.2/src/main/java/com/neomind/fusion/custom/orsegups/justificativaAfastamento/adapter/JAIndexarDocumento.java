package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.formula.CollaboratingWorkbooksEnvironment.WorkbookNotFoundException;

import com.neomind.fusion.custom.orsegups.e2doc.E2docIndexacaoBean;
import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docEngine;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoDateUtils;

public class JAIndexarDocumento implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(E2docEngine.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			NeoFile arquivo = (NeoFile) processEntity.findValue("anexoJustificativa");
			if (arquivo != null)
			{
				// colaborador
				E2docIndexacaoBean e2docIndexacaoBean = new E2docIndexacaoBean();
				Long empresa = (Long) processEntity.findValue("colaborador.numemp");
				Connection conn = PersistEngine.getConnection("VETORH");
				StringBuilder sql = new StringBuilder();
				PreparedStatement pstm = null;
				ResultSet rs = null;

				try
				{
					sql.append("select nomemp from r030emp with(nolock) where numemp = ?");
					pstm = conn.prepareStatement(sql.toString());
					pstm.setLong(1, empresa);
					rs = pstm.executeQuery();
					while (rs.next())
					{
						e2docIndexacaoBean.setEmpresa(rs.getString("nomemp"));
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					OrsegupsUtils.closeConnection(conn, pstm, rs);
				}

				e2docIndexacaoBean.setMatricula(((Long) processEntity.findValue("colaborador.numcad")).toString());
				e2docIndexacaoBean.setColaborador((String) processEntity.findValue("colaborador.nomfun"));
				e2docIndexacaoBean.setCpf((Long) processEntity.findValue("colaborador.numcpf"));

				Long idTipoDocumento = (Long) processEntity.findValue("tipoDocumento.idDocumento");
				if (idTipoDocumento.equals(1l))
				{
					e2docIndexacaoBean.setTipoDocumento("justificativa de falta (atestados, declaração eleitoral, doação de sangue e outros)");
					e2docIndexacaoBean.setClasse("atestados");
					Boolean isDataAtestadoIgualAfa = (Boolean) processEntity.getValue("isDataAtestadoIgualAfa");
					GregorianCalendar gc = null;
					if (!isDataAtestadoIgualAfa)
					{
						gc = (GregorianCalendar) processEntity.getValue("datIniAtestado");
					}
					else
					{
						gc = (GregorianCalendar) processEntity.getValue("dataIniAfa");
					}
					e2docIndexacaoBean.setData(gc.getTime());
				}
				else if (idTipoDocumento.equals(2l))
				{
					e2docIndexacaoBean.setTipoDocumento("Histórico Disciplinar");
					Long idPunicao = (Long) processEntity.findValue("listaPunicoes.id");
					if (idPunicao.equals(1l))
					{
						e2docIndexacaoBean.setClasse("advertência");
					}
					else if (idPunicao.equals(2l))
					{
						e2docIndexacaoBean.setClasse("suspensão");
					}
					Boolean isSuspensaoMesmaDataAfa = (Boolean) processEntity.getValue("isMesmaDataAfa");
					GregorianCalendar gc = null;
					if (!isSuspensaoMesmaDataAfa)
					{
						gc = (GregorianCalendar) processEntity.getValue("perIniSuspensao");
					}
					else
					{
						gc = (GregorianCalendar) processEntity.getValue("dataIniAfa");
					}
					e2docIndexacaoBean.setData(gc.getTime());
				}
				else if (idTipoDocumento.equals(3l))
				{
					//		e2docIndexacaoBean.setTipoDocumento("comunicados");
					e2docIndexacaoBean.setTipoDocumento("justificativa de falta (atestados, declaração eleitoral, doação de sangue e outros)");
					e2docIndexacaoBean.setClasse("declarações médicas");
					Boolean isDataAtestadoIgualAfa = (Boolean) processEntity.getValue("isDataAtestadoIgualAfa");
					GregorianCalendar gc = null;
					if (!isDataAtestadoIgualAfa)
					{
						gc = (GregorianCalendar) processEntity.getValue("datIniAtestado");
					}
					else
					{
						gc = (GregorianCalendar) processEntity.getValue("dataIniAfa");
					}
					e2docIndexacaoBean.setData(gc.getTime());
				}
				else if (idTipoDocumento.equals(4l))
				{

					Long idTipoAfastamento = (Long) processEntity.findValue("tipoAfastamento.codsit");
					if (idTipoAfastamento != null && idTipoAfastamento.equals(15))
					{
						e2docIndexacaoBean.setTipoDocumento("justificativa de falta (atestados, declaração eleitoral, doação de sangue e outros)");
						e2docIndexacaoBean.setClasse("declarações médicas");
					}
					else
					{
						e2docIndexacaoBean.setTipoDocumento("comunicados");
						e2docIndexacaoBean.setClasse("outros");
					}
					Boolean isDataAtestadoIgualAfa = (Boolean) processEntity.getValue("isDataAtestadoIgualAfa");
					GregorianCalendar gc = null;
					if (!isDataAtestadoIgualAfa)
					{
						gc = (GregorianCalendar) processEntity.getValue("datIniAtestado");
					}
					else
					{
						gc = (GregorianCalendar) processEntity.getValue("dataIniAfa");
					}
					e2docIndexacaoBean.setData(gc.getTime());
				}

				e2docIndexacaoBean.setObservacao(String.valueOf(Calendar.getInstance().getTimeInMillis()));

				// requisição
				e2docIndexacaoBean.setTipo("i");
				e2docIndexacaoBean.setModelo("documentação rh");
				e2docIndexacaoBean.setFormato(arquivo.getSufix());
				e2docIndexacaoBean.setDocumento(arquivo.getBytes());

				try
				{
					E2docUtils.indexarDocumento(e2docIndexacaoBean);
				}
				catch (Exception e)
				{
					System.out.println("teste");
					e.printStackTrace();
					log.error("##### ERRO AO indexar documento: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO indexar documento: " + e + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
