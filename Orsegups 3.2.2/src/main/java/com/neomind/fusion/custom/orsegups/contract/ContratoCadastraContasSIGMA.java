package com.neomind.fusion.custom.orsegups.contract;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.jfree.util.Log;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.vo.ContaSigmaVo;
import com.neomind.fusion.custom.orsegups.fap.utils.FapUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoCadastraContasSIGMA implements AdapterInterface
{
	private ArrayList<String> cdClienteRollBack;
	private static String codSapiensParticaoZero = "";

	//String dadosContaAtrelar[] = null;

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		Long key = (new GregorianCalendar()).getTimeInMillis();

		ContratoLogUtils.logInfo("[" + key + "] ContratoCadastraContasSIGMA - iniciando cadastro de conta sigma ");

		boolean byPassCriacaoConta = (ParametrizacaoFluxoContratos.findParameter("byPassCriacaoConta").equals("1") ? true : false);
		boolean criarContaSigma = true;

		List<NeoObject> listaPostos = (List<NeoObject>) wrapper.findField("postosContrato").getValues();
		boolean abreContra = true;
		for (NeoObject objPosto : listaPostos)
		{
			EntityWrapper wPos = new EntityWrapper(objPosto);
			if (wPos.findValue("abreContaSigma") == null)
			{
				abreContra = true;
			}
			else
			{
				abreContra = (boolean) wPos.findField("abreContaSigma").getValue();
			}

			if (!abreContra)
			{
				criarContaSigma = false;
				break;
			}
		}

		String modeloContrato = NeoUtils.safeOutputString(wrapper.findValue("modelo.codigo"));
		Boolean isRastreamento = modeloContrato.equals("05");
		String cgcCpf = "";
		String tipCli = String.valueOf(wrapper.findValue("novoCliente.tipocliente.tipo"));
		tipCli = tipCli.trim();
		if (tipCli.equals("F"))
		{
			cgcCpf = String.valueOf(wrapper.findValue("novoCliente.cpf"));
			if (cgcCpf.length() < 11)
			{
				int cont = 11 - cgcCpf.length();
				for (int i = 0; i < cont; i++)
				{
					cgcCpf = "0" + cgcCpf;
				}
			}

		}
		else
		{
			cgcCpf = String.valueOf(wrapper.findValue("novoCliente.cnpj"));
			if (cgcCpf.length() < 14)
			{
				int cont = 14 - cgcCpf.length();
				for (int i = 0; i < cont; i++)
				{
					cgcCpf = "0" + cgcCpf;
				}
			}

		}

		/*
		 * Collection<NeoObject> postos0 = (Collection<NeoObject>)
		 * wrapper.findField("postosContrato").getValues();
		 * for(NeoObject posto : postos0)
		 * {
		 * EntityWrapper wPosto = new EntityWrapper(posto);
		 * dadosContaAtrelar = vinculaContaExistente(modeloContrato, wPosto);
		 * }
		 */

		//if (true) throw new WorkflowException("Me Remova Exception");

		//TODO me remova
		if (!byPassCriacaoConta)
		{

			cdClienteRollBack = new ArrayList<String>();
			Long tipoMovimento = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
			Long tipoAlteracao = 0L;
			if (tipoMovimento == 5)
				tipoAlteracao = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");

			/*
			 * codigo do movimento
			 * 1 - 4 | 6 : contrato novo
			 * 5 : atualização
			 */
			String num_ctr = null;
			String cod_emp = null;
			String cod_fil = null;

			cod_emp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
			cod_fil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
			num_ctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));

			try
			{

				ContratoLogUtils.logInfo("[" + key + "] -Iniciando geração automática da minuta. " + wrapper.findValue("neoId"));
				MinutaContrato.autoGeracaoMinuta(wrapper);
				ContratoLogUtils.logInfo("[" + key + "]-geração da minuta concluída ");

				String numPosPgo = "";
				String numPosPri = "";
				
				Long nrParticao = 1l;
				String contasSigmaContrato = "";
				String codConta = null;
				String contaSIGMA = "";
				
				num_ctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
				if ("".equals(num_ctr))
					num_ctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
				
				cod_emp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
				cod_fil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
				
				boolean habilitarTeste = (ParametrizacaoFluxoContratos.findParameter("habilitarTesteAutomatico").equals("1") ? true : false);
				
				if (isRastreamento)
				{
					Boolean cartaoRFID = wrapper.findGenericValue("dadosGeraisContrato.adereCartaoRFID");
					if(cartaoRFID == null || !cartaoRFID)
					{
						contasSigmaContrato = criarNovaParticao(wrapper, listaPostos.get(0), 0l, null, null);
						contaSIGMA = contasSigmaContrato.split(",")[0];
						codConta = contasSigmaContrato.split(",")[1];
						
						ativarWebAlarme(contaSIGMA);
					}
					else {
						for (NeoObject noPosto : listaPostos)
						{
							EntityWrapper wrPosto = new EntityWrapper(noPosto);
							String tpoPosto = NeoUtils.safeOutputString(wrPosto.findValue("tipoPosto.codigo"));
							if (tpoPosto.equals("3"))
							{
								contasSigmaContrato = criarNovaParticao(wrapper, noPosto, 0l, null, null);
								contaSIGMA = contasSigmaContrato.split(",")[0];
								codConta = contasSigmaContrato.split(",")[1];
								
								wrPosto.setValue("contaSIGMA", contaSIGMA);
								
								String numPosto = NeoUtils.safeOutputString(wrPosto.findValue("numPosto"));
								
								relacionaSigmaSapiens(contaSIGMA, num_ctr, cod_emp, cod_fil, numPosto, false);
								ativarWebAlarme(contaSIGMA);
								
								break;
							}
						}
					}
					
					if(habilitarTeste){
						habilitarTesteAutomatico(contaSIGMA);
					}

					cdClienteRollBack.add(contaSIGMA);// adiciona cdCLiente para ser removido em caso de exceção neste etapa.
				}

				for (NeoObject posto : listaPostos)
				{
					EntityWrapper wPosto = new EntityWrapper(posto);
					String tipoPosto = NeoUtils.safeOutputString(wPosto.findValue("tipoPosto.codigo"));
					if (tipoPosto.equals("1"))
					{

						boolean origemSapiens = NeoUtils.safeBoolean(wPosto.findValue("origemSapiens"));
						if ((((tipoMovimento <= 4 || tipoMovimento == 6) && wPosto.findValue("contaSIGMA") == null) || (!origemSapiens && wPosto.findValue("contaSIGMA") == null)))
						{
							//criação
							/*
							 * para cada posto novo uma conta é criada no sigma
							 */
							
							String num_pos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
							
							if (criarContaSigma)
							{
								if (isRastreamento)
								{									
									contasSigmaContrato = criarNovaParticao(wrapper, posto, nrParticao++, codConta, contaSIGMA);
									contaSIGMA = contasSigmaContrato.split(",")[0];
									codConta = contasSigmaContrato.split(",")[1];									
								}
								else
								{
									contaSIGMA = criaNovaContaSIGMA(wrapper, posto);	
								}
								

								if(habilitarTeste){
									habilitarTesteAutomatico(contaSIGMA);
								}

								cdClienteRollBack.add(contaSIGMA);// adiciona cdCLiente para ser removido em caso de exceção neste etapa.
								wPosto.setValue("contaSIGMA", contaSIGMA);

							}
							else
							{
								contaSIGMA = (String) wPosto.getValue("contaSigmaSug");
								long nVinculos = verificaVinculoContaCliente(contaSIGMA, cgcCpf);
								long nCont = 0L;
								if (nVinculos != 0)
								{
									wPosto.setValue("contaSIGMA", contaSIGMA);
								}
								else
								{
									nCont = retornaNumeroTentativas(activity.getCode());
									if (nCont > 0)
									{
										wPosto.setValue("contaSIGMA", contaSIGMA);
										removeRegistraTentativa(activity.getCode());
									}
									else
									{
										registraTentativa(activity.getCode());
										throw new Exception("Não foi identificado vinculo entre o cliente e a conta sigma informada!");
									}
								}
							}

							relacionaSigmaSapiens(contaSIGMA, num_ctr, cod_emp, cod_fil, num_pos, isRastreamento);
							ativarWebAlarme(contaSIGMA);
							numPosPri = num_pos;
						}
						else if (tipoMovimento == 5)
						{
							/*
							 * somente irá atualizar no sigma se o posto em questão estiver com a opção
							 * de inativar
							 * logo o posto atual será enviado ao sigma para atualizar, e o antigo será
							 * inativado
							 * se vai atualizar o posto, a conta sigma já existe, basta setar no posto
							 * para atualizar
							 */

							String cdCliente = NeoUtils.safeOutputString(wPosto.findValue("selecaoContaSigmaContrato.usu_codcli"));
							if (!"".equals(cdCliente))
							{
								wPosto.setValue("contaSIGMA", cdCliente);
								PersistEngine.persist(posto);
							}
							else
							{
								throw new Exception("Conta Sigma não selecionada");
							}

							//String contaSIGMA = NeoUtils.safeOutputString(wPosto.findValue("contaSIGMA"));

							String num_pos = NeoUtils.safeOutputString(wPosto.findValue("numPostoAntigo"));
							if ("".equals(num_pos))
							{
								num_pos = NeoUtils.safeOutputString(wPosto.findValue("numPosto")); //todo revisar a regra pois era pra ser numPostoAntigo
							}
							/*
							 * if("".equals(contaSIGMA))
							 * {
							 * String numctr =
							 * NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr")); //
							 * pegar sempre o contrato antigo
							 * contaSIGMA = ContratoContasSIGMAWebService.buscaCDContaSigma(numctr,
							 * num_pos);
							 * wPosto.setValue("contaSIGMA", contaSIGMA);
							 * PersistEngine.persist(posto);
							 * }
							 */

							if (tipoAlteracao == 1)
								num_ctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
							else
								num_ctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));

							if (NeoUtils.safeBoolean(wPosto.findValue("duplicarDesativar")))
								altualizaContaSIGMA(wrapper, posto);
							//pega o numero do posto atual para vincula à conta sigma
							num_pos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));

							cod_emp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
							cod_fil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
							ativarWebAlarme(cdCliente);

							if (ParametrizacaoFluxoContratos.findParameter("replicarContasSigmaPostoAntigo").equals("1"))
							{
								// para cada conta vinculada ao posto antigo, vincular no posto novo
								String num_ctrAntigo = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
								String num_posAntigo = NeoUtils.safeOutputString(wPosto.findValue("numPostoAntigo"));

								ContratoLogUtils.logInfo("[" + key + "] - buscando relacionamentos sigma x sapiens para o posto " + num_posAntigo);
								List<ContaSigmaVo> contas = ContratoSigmaUtils.retornaContasSigmaPostoSapiens(cod_emp, cod_fil, num_ctrAntigo, num_posAntigo);
								if (contas.isEmpty())
								{
									ContratoLogUtils.logInfo("[" + key + "] - Não foram encontradas contas sigma vinculadas ao posto:" + num_posAntigo);
									throw new Exception("Não forma encontradas contas sigma vinculadas ao posto antigo. Posto antigo:" + num_posAntigo);
								}
								else
								{
									for (ContaSigmaVo c : contas)
									{
										ContratoLogUtils.logInfo("[" + key + "] - Vinculando contas sigma do posto:" + num_posAntigo + " no posto: " + num_pos + "\n idCentral=" + c.getIdCentral());
										revincularContaSigmaSapiens(c, num_ctr, cod_emp, cod_fil, num_pos);
										ContratoLogUtils.logInfo("[" + key + "] - vinculo ok ");
									}
									
									for (ContaSigmaVo c : contas)
									{
										ContratoLogUtils.logInfo("[" + key + "] - Vinculando partição 0 com demais contas sigma do posto:" + num_posAntigo + " no posto: " + num_pos + "\n idCentral=" + c.getIdCentral());
										revincularContaSigmaSapiens(c, num_ctr, cod_emp, cod_fil, num_pos);
										ContratoLogUtils.logInfo("[" + key + "] - vinculo ok ");
									}
								}
							}
							else
							{
								relacionaSigmaSapiens(cdCliente, num_ctr, cod_emp, cod_fil, num_pos, isRastreamento);
							}

						}
//						else if (tipoMovimento == 6)
//						{
//							//criação
//							/*
//							 * para cada posto novo uma partição é criada no sigma
//							 */
//							num_ctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
//							if ("".equals(num_ctr))
//								num_ctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
//							cod_emp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
//							cod_fil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
//							String num_pos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
//							String contaSIGMA = "";
//							if (criarContaSigma)
//							{
//								String contas = "";
//								
//								if (nrParticao == 1l)
//									contas = criarNovaParticao(wrapper, posto, nrParticao++, null);
//								else
//									contas = criarNovaParticao(wrapper, posto, nrParticao++, codConta);
//								
//								contaSIGMA = contas.split(",")[0];
//								codConta = contas.split(",")[1];
//
//								boolean habilitarTeste = (ParametrizacaoFluxoContratos.findParameter("habilitarTesteAutomatico").equals("1") ? true : false);
//
//								if(habilitarTeste){
//									habilitarTesteAutomatico(contaSIGMA);
//								}
//
//								cdClienteRollBack.add(contaSIGMA);// adiciona cdCLiente para ser removido em caso de exceção neste etapa.
//								wPosto.setValue("contaSIGMA", contaSIGMA);
//
//							}
//							else
//							{
//								contaSIGMA = (String) wPosto.getValue("contaSigmaSug");
//								long nVinculos = verificaVinculoContaCliente(contaSIGMA, cgcCpf);
//								long nCont = 0L;
//								if (nVinculos != 0)
//								{
//									wPosto.setValue("contaSIGMA", contaSIGMA);
//								}
//								else
//								{
//									nCont = retornaNumeroTentativas(activity.getCode());
//									if (nCont > 0)
//									{
//										wPosto.setValue("contaSIGMA", contaSIGMA);
//										removeRegistraTentativa(activity.getCode());
//									}
//									else
//									{
//										registraTentativa(activity.getCode());
//										throw new Exception("Não foi identificado vinculo entre o cliente e a conta sigma informada!");
//									}
//								}
//							}
//
//							//relacionaSigmaSapiens(contaSIGMA, num_ctr, cod_emp, cod_fil, num_pos);
//							ativarWebAlarme(contaSIGMA);
//							numPosPri = num_pos;
//						}
						//Se for posto do PGO.
					}
					else if (tipoPosto.equals("2"))
					{
						//Carrega variaveis para o insert da conta sigma do posto PGO.
						num_ctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
						cod_emp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
						cod_fil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
						numPosPgo = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
						System.out.println("Contrato " + num_ctr + " empresa " + cod_emp + " Filial " + cod_fil + " Posto " + numPosPgo);
						//Vinculando a conta sigma do posto principal para o posto do PGO.
						String insert = "INSERT INTO USU_T160SIG SELECT usu_codemp,usu_codfil,usu_numctr," + numPosPgo + ",usu_sigemp,usu_sigcon,usu_sigpar,usu_codcli,usu_sitlig" + " from usu_t160sig where usu_codemp = " + cod_emp + " and usu_codfil = " + cod_fil + " and usu_numctr = " + num_ctr + " and usu_numpos = " + numPosPri;

						Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(insert);
						try
						{
							queryExecute.executeUpdate();
							ContratoLogUtils.logInfo("[FLUXO CONTRATOS]-Vinculação de Contas SIGMA do posto principal ao posto do PGO" + insert);
							//Limpando variaveis caso não ocorra nenhum erro.
							numPosPri = "";
							numPosPgo = "";

						}
						catch (Exception e)
						{
							ContratoLogUtils.logInfo("Erro - [FLUXO CONTRATOS]- Vinculação de Contas SIGMA do posto principal ao posto do PGO - sql: " + insert);
							throw new WorkflowException("Erro na Vinculação da Contas SIGMA do posto principal ao posto do PGO");
						}
					}

				}
				
				if (isRastreamento)
				{
					ativarParticaoZero(codConta);
				}

				abrirOS(wrapper);

				gerarObservacoes(wrapper);

				// se chegou aqui sem erros, então vamos limpar os dados de rollback para não deixar lixo no sistema.
				if (num_ctr != null && cod_emp != null && cod_fil != null)
				{
					ContratoUtils.removeListaPostosToRollBack(Long.parseLong(num_ctr), Long.parseLong(cod_emp), Long.parseLong(cod_fil));
					ContratoUtils.removeListaCCToRollBack(Long.parseLong(num_ctr));
					//ContratoUtils.removeListaContratosToRollBack(Long.parseLong(cod_emp), Long.parseLong(cod_fil), activity.getCode());
				}
			}
			catch (Exception e)
			{
				ContratoLogUtils.logInfo("[" + key + "]ContratoCadastraContasSIGMA - Erro ao gerar conta sigma . " + e.getMessage());
				e.printStackTrace();
				if (e instanceof WorkflowException)
				{
					if (num_ctr != null && cod_emp != null && cod_fil != null)
					{
						ContratoLogUtils.logInfo("[" + key + "]ContratoCadastraContasSIGMA - Partiu rollback de Postos");
						ContratoUtils.rollBackPostos(Long.parseLong(num_ctr), Long.parseLong(cod_emp), Long.parseLong(cod_fil), ContratoUtils.retornaListaPostosToRollBack(Long.parseLong(num_ctr), Long.parseLong(cod_emp), Long.parseLong(cod_fil)));
						ContratoUtils.removeListaPostosToRollBack(Long.parseLong(num_ctr), Long.parseLong(cod_emp), Long.parseLong(cod_fil));
						//ContratoUtils.removeListaContratosToRollBack(Long.parseLong(cod_emp), Long.parseLong(cod_fil), activity.getCode());

						ContratoLogUtils.logInfo("[" + key + "]ContratoCadastraContasSIGMA - Partiu rollback de CC");
						ContratoUtils.rollBackCC(Long.parseLong(num_ctr), ContratoUtils.retornaListaCCToRollBack(Long.parseLong(num_ctr)), "CONTRATOCADASTRACONTASSIGMA - " + origin.getCode());
						ContratoUtils.removeListaCCToRollBack(Long.parseLong(num_ctr));
					}
					if (cdClienteRollBack != null && cdClienteRollBack.size() > 0)
					{
						ContratoLogUtils.logInfo("[" + key + "]ContratoCadastraContasSIGMA - Partiu rollback de Conta sigma");
						ContratoUtils.rollBackContaSigma(cdClienteRollBack);
					}
					throw (WorkflowException) e;
				}
				else
				{
					ContratoLogUtils.logInfo("[" + key + "]  Ctr:" + num_ctr + ", deu merda e vai fazer rollback dos postos e CC. Será? " + (num_ctr != null && cod_emp != null && cod_fil != null));

					if (num_ctr != null && cod_emp != null && cod_fil != null)
					{

						ContratoLogUtils.logInfo("[" + key + "]  Ctr:" + num_ctr + ", ContratoCadastraContasSIGMA - fazendo rollback de Postos");
						ContratoUtils.rollBackPostos(Long.parseLong(num_ctr), Long.parseLong(cod_emp), Long.parseLong(cod_fil), ContratoUtils.retornaListaPostosToRollBack(Long.parseLong(num_ctr), Long.parseLong(cod_emp), Long.parseLong(cod_fil)));
						ContratoUtils.removeListaPostosToRollBack(Long.parseLong(num_ctr), Long.parseLong(cod_emp), Long.parseLong(cod_fil));
						//ContratoUtils.removeListaContratosToRollBack(Long.parseLong(cod_emp), Long.parseLong(cod_fil), activity.getCode());

						ContratoLogUtils.logInfo("[" + key + "]  Ctr:" + num_ctr + ",ContratoCadastraContasSIGMA - fazendo rollback de CC");
						ContratoUtils.rollBackCC(Long.parseLong(num_ctr), ContratoUtils.retornaListaCCToRollBack(Long.parseLong(num_ctr)), "CONTRATOCADASTRACONTASSIGMA - " + origin.getCode());
						ContratoUtils.removeListaCCToRollBack(Long.parseLong(num_ctr));
					}
					if (cdClienteRollBack != null && cdClienteRollBack.size() > 0)
					{
						ContratoLogUtils.logInfo("[" + key + "]  Ctr:" + num_ctr + ",ContratoCadastraContasSIGMA - fazendo rollback de Conta sigma");
						ContratoUtils.rollBackContaSigma(cdClienteRollBack);
					}
					throw new WorkflowException("Erro ao cadastrar conta sigma." + e);
				}

			}

		}

	}

	/**
	 * Este método habilita a flag FG_AUTO_HABILITAR_CTRL_TESTE na tabela dbCentral
	 * @param contaSIGMA - cdcliente da conta para habilitar o teste.
	 * */
	public static void habilitarTesteAutomatico(String contaSIGMA) {
		String cdCliente = contaSIGMA;
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		PreparedStatement pstm2 = null;
		try {

			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT * ");
			sql.append(" FROM dbCENTRAL ");
			sql.append(" WHERE CTRL_CENTRAL = 1 ");
			sql.append(" AND FG_AUTO_HABILITAR_CTRL_TESTE = 0 ");
			sql.append(" AND ID_CENTRAL not like 'R%' ");
			sql.append(" AND ID_CENTRAL not like 'D%' ");
			sql.append(" AND ID_CENTRAL not like 'A%' ");
			sql.append(" AND ID_CENTRAL not like 'F%' ");
			sql.append(" AND PARTICAO NOT LIKE '09%'  ");
			sql.append(" AND CD_CLIENTE = ? ");				

			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, cdCliente);

			rs = pstm.executeQuery();

			if(rs.next()){
				System.out.println("[FLUXO CONTRATOS] - Habilitando teste automático na conta " + cdCliente);

				StringBuilder sql2 = new StringBuilder();

				sql2.append("UPDATE dbCENTRAL SET FG_AUTO_HABILITAR_CTRL_TESTE = 1 WHERE cd_cliente = ?");
				pstm2 = conn.prepareStatement(sql2.toString());
				pstm2.setString(1, cdCliente);
				pstm2.executeUpdate();
				pstm2.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);			
		}		
	}

	/**
	 * 
	 * @param c
	 * @param num_ctr
	 * @param cod_emp
	 * @param cod_fil
	 * @param num_pos
	 * @throws Exception
	 */

	private long verificaVinculoContaCliente(String contaSigma, String cgccpf)
	{
		Connection connTIDB = PersistEngine.getConnection("SIGMA90");
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT 1 FROM dbCENTRAL d WHERE d.CTRL_CENTRAL = 1 and CD_CLIENTE = ? and CGCCPF = ?");
		PreparedStatement stContas;
		try
		{
			stContas = connTIDB.prepareStatement(sql.toString());
			stContas.setString(1, contaSigma);
			stContas.setString(2, cgccpf);
			ResultSet rsResult = stContas.executeQuery();
			while (rsResult.next())
			{
				return 1L;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	private void registraTentativa(String tarefa)
	{
		Connection connTIDB = PersistEngine.getConnection("TIDB");
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO usu_conReg (codtar,cont) values (?,?) ");
		PreparedStatement stTarefa;
		try
		{
			stTarefa = connTIDB.prepareStatement(sql.toString());
			stTarefa.setString(1, tarefa);
			stTarefa.setLong(2, 1L);
			stTarefa.executeUpdate();
			stTarefa.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	private void removeRegistraTentativa(String tarefa)
	{
		Connection connTIDB = PersistEngine.getConnection("TIDB");
		StringBuilder sql = new StringBuilder();
		sql.append("delete usu_conReg where codtar = ? ");
		PreparedStatement stTarefa;
		try
		{
			stTarefa = connTIDB.prepareStatement(sql.toString());
			stTarefa.setString(1, tarefa);
			stTarefa.executeUpdate();
			stTarefa.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	private long retornaNumeroTentativas(String tarefa)
	{
		Connection connTIDB = PersistEngine.getConnection("TIDB");
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT CONT FROM usu_conReg WHERE codtar = ?");
		PreparedStatement stTarefa;
		try
		{
			stTarefa = connTIDB.prepareStatement(sql.toString());
			stTarefa.setString(1, tarefa);
			ResultSet rsResult = stTarefa.executeQuery();
			while (rsResult.next())
			{
				return rsResult.getLong("CONT");
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	private void revincularContaSigmaSapiens(ContaSigmaVo c, String num_ctr, String cod_emp, String cod_fil, String num_pos) throws Exception
	{
		String insert = "INSERT INTO USU_T160SIG VALUES (" + cod_emp + ", " + cod_fil + ", " + num_ctr + ", " + num_pos + ", " + c.getIdEmpresa() + ", '" + c.getIdCentral() + "', " + c.getParticao() + ", " + c.getIdEmpresa() + ", '' )";

		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(insert);
		try
		{
			queryExecute.executeUpdate();
			ContratoLogUtils.logInfo("[FLUXO CONTRATOS]-Vinculação de Contas SIGMA x SAPIENS criada.(revincularContaSigmaSapiens)");
		}
		catch (Exception e)
		{
			ContratoLogUtils.logInfo("Erro - revincularContaSigmaSapiens() - sql: " + insert);
			throw new Exception("Erro na Inserção na tabela USU_T160SIG do Sapiens (Relação das contas SIGMA e SAPIENS)");
		}

	}

	private void gerarObservacoes(EntityWrapper wrapper)
	{
		List<NeoObject> postos = (List<NeoObject>) wrapper.findField("postosContrato").getValues();

		Long usu_codemp = (Long) wrapper.findField("empresa.codemp").getValue();
		Long usu_codfil = (Long) wrapper.findField("empresa.codfil").getValue();
		Long codUsuariosapiens = 0L;
		try
		{
			codUsuariosapiens = ContratoUtils.retornaCodeUsuarioSapiens(PortalUtil.getCurrentUser().getCode());
		}
		catch (Exception e)
		{
			codUsuariosapiens = 1L;
		}

		String num_ctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		if ("".equals(num_ctr))
			num_ctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));

		for (NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			if (NeoUtils.safeOutputString(wPosto.findValue("tipoPosto.codigo")).equals("1"))
			{
				String os = (String) wPosto.findValue("numeroOS");
				if (os == null)
				{
					os = (String) wPosto.findValue("numeroOSHabilitacao");
				}
				String conta = (String) wPosto.findValue("contaSIGMA");

				Long numPosto = (Long) wPosto.findField("numPosto").getValue();
				String usu_txtobs = "OS." + os + " CONTA:" + conta;
				Long usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp, usu_codfil, NeoUtils.safeLong(num_ctr));
				String usu_codser = (String) wPosto.findValue("complementoServico.codser");
				GregorianCalendar data = new GregorianCalendar();
				long hora = (data.get(GregorianCalendar.HOUR) * 60L) + data.get(GregorianCalendar.MINUTE);
				ContratoUtils.geraObservacao(usu_codemp, usu_codfil, NeoUtils.safeLong(num_ctr), usu_seqobs, numPosto, usu_codser, "A", usu_txtobs, codUsuariosapiens, new GregorianCalendar(), hora, "");
			}
		}

	}

	public static String[] vinculaContaExistente(String modeloContrato, EntityWrapper wPosto)
	{
		String retorno[] = null;
		// 	vincula conta sigma com uma conta já existente
		if (wPosto != null)
		{

			NeoObject oContaSigmaVincular = (NeoObject) wPosto.findValue("vinculoContaSigma");

			if (oContaSigmaVincular != null)
			{

				EntityWrapper wContaSigmaVincular = new EntityWrapper(oContaSigmaVincular);
				String cdClienteContaVinculo = NeoUtils.safeOutputString(wContaSigmaVincular.findValue("usu_codcli"));

				if (cdClienteContaVinculo != null)
				{
					String dadosConta[] = ContratoSigmaUtils.retornaDadosConta(cdClienteContaVinculo);
					ContratoLogUtils.logInfo("Dados Vinculação " + dadosConta[0] + " - " + dadosConta[1] + " - " + dadosConta[2]);
					//System.out.println("Dados Vinculação " + dadosConta[0] + " - " + dadosConta[1] + " - " + dadosConta[2]);

					// verificar qual incremento no numero da particao deve ser feito
					Long particao = 1L;

					if (!"".equals(modeloContrato) && ("02".equals(modeloContrato) || "04".equals(modeloContrato)))
					{ // CFTV deve ter partição 099
						particao = NeoUtils.safeLong(ContratoSigmaUtils.retornaMaxParticao(dadosConta[0], true));
					}
					else
					{
						particao = NeoUtils.safeLong(ContratoSigmaUtils.retornaMaxParticao(dadosConta[0], false));
					}
					particao++; //incrementa partição

					ContratoLogUtils.logInfo("Dados para vinculação... ContaSigma:" + dadosConta[0] + ", particao: " + particao);
					retorno = new String[2];
					retorno[0] = dadosConta[0];
					retorno[1] = ContratoSigmaUtils.normalizaParticao(particao);

				}

			}

		}
		return retorno;
	}

	private void abrirOS(EntityWrapper wrapper) throws Exception
	{
		System.out.println("[FLUXO CONTRATOS]-Iniciando Abertura de OS");
		try
		{
			//NeoObject contrato = wrapper.getEntity();
			//EntityWrapper wContrato = new EntityWrapper(contrato);

			String modeloContrato = NeoUtils.safeOutputString(wrapper.findValue("modelo.codigo"));

			Long tipoMovimento = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
			Long tipoAlteracao = 0L;
			if (tipoMovimento == 5)
				tipoAlteracao = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");

			boolean OSReducao = tipoAlteracao == 4 ? true : false;

			Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
			int postosFaturados = 0;
			for (NeoObject posto : postos)
			{
				EntityWrapper wPosto = new EntityWrapper(posto);
				String tipoPosto = NeoUtils.safeOutputString(wPosto.findValue("tipoPosto.codigo"));
				if (!tipoPosto.equals("2"))
				{
					String contaSIGMA = NeoUtils.safeOutputString(wPosto.findValue("contaSIGMA"));
					if (contaSIGMA == null || contaSIGMA.equals(""))
					{
						if (tipoPosto.equals("3"))
							contaSIGMA = codSapiensParticaoZero;
						else {
							Log.warn("Solicitado abertura de OS para o posto #" + posto.getNeoId() + ", porém o mesmo não possui conta SIGMA.");
							continue;
						}
					}

					boolean abrirOSInstalacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSInstalacao"));
					boolean abrirOSReintalacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSReinstalacao"));
					boolean abrirOSHabilitacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSHabilitacao"));

					boolean instalado = NeoUtils.safeBoolean(wPosto.findValue("instalado"));
					boolean habilitado = NeoUtils.safeBoolean(wPosto.findValue("habilitado"));
					boolean faturado = NeoUtils.safeBoolean(wPosto.findValue("faturado"));

					if (OSReducao)
					{
						String numeroOS = ContratoInstalacaoHabilitacaoJOB.abreOsSigma(wrapper, posto, false, true);
						wPosto.setValue("numeroOS", numeroOS);
						return;
					}
					else
					{

						if (abrirOSInstalacao)
						{
							String numeroOSInstalacao = ContratoInstalacaoHabilitacaoJOB.abreOsSigma(wrapper, posto, false, false);
							System.out.println("[FLUXO CONTRATOS]-OS de instalação aberta. Numero: " + numeroOSInstalacao);
							wPosto.setValue("numeroOS", numeroOSInstalacao);
						}
						else if (abrirOSHabilitacao)
						{
							String numeroOSHabilitacao = ContratoInstalacaoHabilitacaoJOB.abreOsSigma(wrapper, posto, true, false);
							System.out.println("[FLUXO CONTRATOS]-OS de habilitação aberta. Numero: " + numeroOSHabilitacao);
							wPosto.setValue("numeroOSHabilitacao", numeroOSHabilitacao);
						}
						else if (abrirOSReintalacao)
						{
							String numeroOS = ContratoInstalacaoHabilitacaoJOB.abreOsSigma(wrapper, posto, false, false);
							wPosto.setValue("numeroOS", numeroOS);
							System.out.println("[FLUXO CONTRATOS]-OS de Reinstalação aberta. Numero: " + numeroOS);
						}
					}

				}

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}

	}

	public static String criaNovaContaSIGMA(EntityWrapper wrapper, NeoObject posto) throws Exception
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		Map<String, String> map = montaParametros(wrapper, posto, true);

		/*
		 * Regra, se o contrato for de CFTV será cadastrado na partição 099. Se Houverem equipamentos à
		 * serem cadastrados em outra partição o
		 * fusion não tratará, deverá ser feito o procedimento manual no sigma e sapiens.
		 * Caso seja um contrato de eletronica e tiver cftv, será cadastrado na partição 001 e o
		 * procedimento manual também deverá ser feito.
		 */
		String modeloContrato = NeoUtils.safeOutputString(wrapper.findValue("modelo.codigo"));
		if (!"".equals(modeloContrato) && ("02".equals(modeloContrato) || "04".equals(modeloContrato)))
		{ // CFTV deve ter partição 099
			map.put("partition", "099");
		}

		/*
		 * String active = "1";
		 * String companyId = "10017";
		 * String account = "666667";
		 * String corporationName = "NEOMIND TESTE";
		 * String tradeName = "NEOMIND TESTE";
		 * String stateId = "SC";
		 * String cityId = "10010";
		 * String districtId = "10568";
		 * String route1Id = "10062";
		 * String installerId = "22480";
		 * String branchActivityId = "10002";
		 * String sellerId = "11262";
		 * String observations = "CLIENTE CRIADO VIA WEBSERVICE - TESTE DE DESENVOLVIMENTO";
		 */

		if ((map.get("companyId") == null) || (map.get("companyId") != null && map.get("companyId").trim().equals("")))
		{
			throw new Exception("Erro ao criar Conta Sigma - Causas possíveis: (1)Regional do posto não preenchida. (2)Mapeamento de empresas Sapiens x Sigma não encontrado");
		}

		List<String> resultList = ContratoSigmaUtils.getTodasContasUsadas(NeoUtils.safeLong((String) map.get("companyId")));
		ContratoLogUtils.logInfo("[" + key + "] - contasNoSigmaListadas: " + (resultList != null ? resultList.size() : 0) + "\n");

		StringBuilder contas = new StringBuilder();
		contas.append("'0'");
		if (resultList != null && !resultList.isEmpty())
		{
			for (String result : resultList)
			{
				if (result != null)
				{
					String id_central = NeoUtils.safeOutputString(result);
					if (id_central != null && id_central.length() > 1)
					{
						contas.append(",'").append(id_central.toUpperCase()).append("'");
					}

				}
			}
		}

		ContratoLogUtils.logInfo("[" + key + "] - contasNoSigma: " + contas.toString() + "\n");

		List<String> rl = ContratoSigmaUtils.getRangeContasSigma();
		ContratoLogUtils.logInfo("[" + key + "] - Range de contas listadas: " + (rl != null ? rl.size() : 0) + "\n");
		String account = "";
		BigInteger naccount = null;
		if (rl != null && rl.size() > 0)
		{
			for (String r : rl)
			{
				if (contas.indexOf(NeoUtils.safeOutputString(r.toString().toUpperCase())) < 0)
				{
					account = NeoUtils.safeOutputString(r.toString().toUpperCase());
					naccount = new BigInteger(account, 16);
					if (naccount.longValue() >= 5000L){
						ContratoLogUtils.logInfo("[" + key + "] - contaSelecionada: " + account);
						break;
					}else{
						account = "";
					}
				}
			}
		}

		map.put("account", account);

		//expirimental - permitir criar uma conta que seja atrelada a outa.

		if (("1").equals(ParametrizacaoFluxoContratos.findParameter("ativarVinculacaoContasSigma")))
		{
			try
			{
				String[] result = vinculaContaExistente(modeloContrato, new EntityWrapper(posto));
				if (result != null)
				{
					map.put("account", result[0]);
					map.put("partition", result[1]);
					map.put("controlPartition", "1");
				}
			}
			catch (Exception e)
			{
				throw new Exception("Problema ao vincular conta sigma à outra existente. Conta:");
			}
		}

		String resposta = ContratoContasSIGMAWebService.buscaCDContaSigma(map.get("contract"), map.get("complement"));
		if (resposta == null)
		{ // se não existe

			try
			{
				String rua = map.get("addressGmap");
				String cidade = map.get("cidade");
				String uf = map.get("stateId");
				HashMap<String, String> latlng = MapsGoogleUtils.retornaLatitudeLongitude(rua, cidade, uf);

				if (latlng != null && !"".equals((String) latlng.get("lat")))
				{
					map.put("latitude", (String) latlng.get("lat"));
				}

				if (latlng != null && !"".equals((String) latlng.get("lng")))
				{
					map.put("longitude", (String) latlng.get("lng"));
				}
			}
			catch (Exception e)
			{
				System.out.println("[FLUXO CONTRATOS]-Erro ao calcular latitude e longitude do cliente. msg: " + e.getMessage());
				e.printStackTrace();
			}
			resposta = new ContratoContasSIGMAWebService().insereNovaConta(map);
			System.out.println("[FLUXO CONTRATOS]-Resposta = " + resposta + " >>>lat - lng: " + (String) map.get("latitude") + "-" + (String) map.get("longitude"));
		}

		if (resposta != null && !resposta.equals("")) // se a conta existe ou foi criada.
		{
			Long CD_CLIENTE = Long.parseLong(resposta);
			EntityWrapper wPosto = new EntityWrapper(posto);
			Collection<NeoObject> listaContatosPosto = (Collection<NeoObject>) wPosto.findField("postosContrato.vigilanciaEletronica.listaContatosPosto").getValues();
			int numPri = 1;
			for (NeoObject oContato : listaContatosPosto)
			{
				EntityWrapper wContato = new EntityWrapper(oContato);
				String nomeContato = (String) wContato.findValue("nomecontato");
				String fone1 = (String) wContato.findValue("telefone1");
				String fone2 = (String) wContato.findValue("telefone2");
				fone1 = ContratoUtils.retiraCaracteresELetras(fone1);
				fone2 = ContratoUtils.retiraCaracteresELetras(fone2);
				if (!ContratoContasSIGMAWebService.contatoJaExiste(CD_CLIENTE, nomeContato.toUpperCase()))
				{
					ContratoContasSIGMAWebService.inserirContatoSigma(CD_CLIENTE, nomeContato.toUpperCase(), fone1, fone2, "", numPri);
					numPri++;
				}
			}
			
			System.out.println("[FLUXO CONTRATOS]-Conta SIGMA criada : " + resposta);
			return resposta;
		}
		else
		{
			throw new Exception(resposta);
		}
	}
	
	public static String criarNovaParticao(EntityWrapper wrapper, NeoObject posto, Long nrParticao, String codConta, String contaSIGMA) throws Exception
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		Map<String, String> map = montaParametros(wrapper, posto, true);		
		map.put("partition", ContratoSigmaUtils.normalizaParticao(nrParticao));
		map.put("tradeName", "RASTREAMENTO \\- " + map.get("tradeName"));
		map.put("companyId", "10075");
		map.put("controlPartition", "1");
		
		EntityWrapper wPosto = new EntityWrapper(posto);	
		
		NeoObject noRegional = wrapper.findGenericValue("dadosGeraisContrato.regional");
		EntityWrapper wRegional = new EntityWrapper(noRegional);
		String regional = wRegional.findGenericValue("usu_nomreg");
		
		String route1Id = String.valueOf(ContratoContasSIGMAWebService.findRota(FapUtils.getSiglaRegional(regional) + "-AA1-D"));
		String route2Id = String.valueOf(ContratoContasSIGMAWebService.findRota(FapUtils.getSiglaRegional(regional) + "-AA1-N"));
		
		if (!route1Id.equals("-1"))
			map.put("route1Id", route1Id);
		if (!route2Id.equals("-1"))
			map.put("route2Id", route2Id);

		/*
		 * String active = "1";
		 * String companyId = "10017";
		 * String account = "666667";
		 * String corporationName = "NEOMIND TESTE";
		 * String tradeName = "NEOMIND TESTE";
		 * String stateId = "SC";
		 * String cityId = "10010";
		 * String districtId = "10568";
		 * String route1Id = "10062";
		 * String installerId = "22480";
		 * String branchActivityId = "10002";
		 * String sellerId = "11262";
		 * String observations = "CLIENTE CRIADO VIA WEBSERVICE - TESTE DE DESENVOLVIMENTO";
		 */

		if ((map.get("companyId") == null) || (map.get("companyId") != null && map.get("companyId").trim().equals("")))
		{
			throw new Exception("Erro ao criar Conta Sigma - Causas possíveis: (1)Regional do posto não preenchida. (2)Mapeamento de empresas Sapiens x Sigma não encontrado");
		}

		String account = "";
		if (codConta == null)
		{
			List<String> resultList = ContratoSigmaUtils.getTodasContasUsadas(NeoUtils.safeLong((String) map.get("companyId")));
			ContratoLogUtils.logInfo("[" + key + "] - contasNoSigmaListadas: " + (resultList != null ? resultList.size() : 0) + "\n");

			StringBuilder contas = new StringBuilder();
			contas.append("'0'");
			if (resultList != null && !resultList.isEmpty())
			{
				for (String result : resultList)
				{
					if (result != null)
					{
						String id_central = NeoUtils.safeOutputString(result);
						if (id_central != null && id_central.length() > 1)
						{
							contas.append(",'").append(id_central.toUpperCase()).append("'");
						}
					}
				}
			}

			//ContratoLogUtils.logInfo("[" + key + "] - contasNoSigma: " + contas.toString() + "\n");
					
			List<String> rl = ContratoSigmaUtils.getRangeContasSigma();
			ContratoLogUtils.logInfo("[" + key + "] - Range de contas listadas: " + (rl != null ? rl.size() : 0) + "\n");
			BigInteger naccount = null;
			if (rl != null && rl.size() > 0)
			{
				for (String r : rl)
				{
					if (contas.indexOf(NeoUtils.safeOutputString(r.toString().toUpperCase())) < 0)
					{
						account = NeoUtils.safeOutputString(r.toString().toUpperCase());
						naccount = new BigInteger(account, 16);
						if (naccount.longValue() >= 5000L){
							ContratoLogUtils.logInfo("[" + key + "] - contaSelecionada: " + account);
							break;
						}else{
							account = "";
						}
					}
				}
			}
			
			account = "R"+account;
		} else
		{
			account = codConta;
			map.put("id", contaSIGMA);
		}
		
		if (nrParticao == 0l)
		{
			map.remove("tracker");
			map.put("partition", "0");
			
			String referencias = map.get("references");
			referencias = referencias.substring(0, referencias.length()-10);
			map.put("references", referencias);
		}
		map.put("account", account);

		//String resposta = ContratoContasSIGMAWebService.buscaCDContaSigma(map.get("contract"), map.get("complement"));
		String resposta = null;
		if (resposta == null)
		{ // se não existe

			try
			{
				String rua = map.get("addressGmap");
				String cidade = map.get("cidade");
				String uf = map.get("stateId");
				HashMap<String, String> latlng = MapsGoogleUtils.retornaLatitudeLongitude(rua, cidade, uf);

				if (latlng != null && !"".equals((String) latlng.get("lat")))
				{
					map.put("latitude", (String) latlng.get("lat"));
				}

				if (latlng != null && !"".equals((String) latlng.get("lng")))
				{
					map.put("longitude", (String) latlng.get("lng"));
				}
			}
			catch (Exception e)
			{
				System.out.println("[FLUXO CONTRATOS]-Erro ao calcular latitude e longitude do cliente. msg: " + e.getMessage());
				e.printStackTrace();
			}
			resposta = new ContratoContasSIGMAWebService().insereNovaConta(map);
			System.out.println("[FLUXO CONTRATOS]-Resposta = " + resposta + " >>>lat - lng: " + (String) map.get("latitude") + "-" + (String) map.get("longitude"));
			
			if (map.get("partition").equals("0"))
				codSapiensParticaoZero = resposta;
		}

		if (resposta != null && !resposta.equals("")) // se a conta existe ou foi criada.
		{
			Long CD_CLIENTE = Long.parseLong(resposta);
			if (wPosto != null)
			{
				Collection<NeoObject> listaContatosPosto = (Collection<NeoObject>) wPosto.findField("postosContrato.vigilanciaEletronica.listaContatosPosto").getValues();
				int numPri = 1;
				for (NeoObject oContato : listaContatosPosto)
				{
					EntityWrapper wContato = new EntityWrapper(oContato);
					String nomeContato = (String) wContato.findValue("nomecontato");
					String fone1 = (String) wContato.findValue("telefone1");
					String fone2 = (String) wContato.findValue("telefone2");
					fone1 = ContratoUtils.retiraCaracteresELetras(fone1);
					fone2 = ContratoUtils.retiraCaracteresELetras(fone2);
					if (!ContratoContasSIGMAWebService.contatoJaExiste(CD_CLIENTE, nomeContato.toUpperCase()))
					{
						ContratoContasSIGMAWebService.inserirContatoSigma(CD_CLIENTE, nomeContato.toUpperCase(), fone1, fone2, "", numPri);
						numPri++;
					}
				}
			}
			
			System.out.println("[FLUXO CONTRATOS]-Conta SIGMA criada : " + resposta);
			return resposta + "," + account;
		}
		else
		{
			throw new Exception(resposta);
		}
	}
	
	public static String addZeros(Long number, Long size){
		String retorno = "";
		
		if (number == null || size == null)
			return retorno;
		
		retorno = NeoUtils.safeOutputString(number);
		
		while (retorno.length() < size)
			retorno = "0" + retorno;
		
		return retorno;
	}

	public void altualizaContaSIGMA(EntityWrapper wrapper, NeoObject posto) throws Exception
	{
		Map<String, String> map = montaParametros(wrapper, posto, false);
		int x = 0;
		ContratoContasSIGMAWebService ws = new ContratoContasSIGMAWebService();
		String resposta = ws.atualizaConta(map);
		if (resposta.equals("ok"))
		{

		}
		else
		{

			String dadosConta = " Dados da conta -> Código: " + map.get("id") + ", conta: " + map.get("account") + ", partição:" + map.get("partition");
			if (resposta.equals("account_already_exists"))
			{ // conta em duplicidade
				throw new Exception("Erro ao atualizar conta sigma - conta em duplicidade. Entrar em contato com a Central de monitoramento para correção de cadastro. " + dadosConta);
			}
			if (resposta.contains("non_partitioned_account_already_exists"))
			{ // conta não particionada em duplicidade
				throw new Exception("Erro ao atualizar conta sigma - conta não particionada em duplicidade. Entrar em contato com a Central de monitoramento para correção de cadastro. " + dadosConta);
			}
			else
			{
				throw new Exception("ContratoCadastraContasSIGMA - Erro ao atualizar conta sigma." + resposta);
			}
		}

	}

	public static Map<String, String> montaParametros(EntityWrapper wrapper, NeoObject posto, boolean insert) throws Exception
	{

		/*
		 * informações para cada posto
		 */
		EntityWrapper wPosto = null;
		try
		{
			wPosto = new EntityWrapper(posto);
		}
		catch (Exception e)
		{
			posto = AdapterUtils.createNewEntityInstance("FGCCadastroPostos");
			wPosto = new EntityWrapper(posto);
		}

		Map<String, String> map = new HashMap<String, String>();
		long tipoAcao = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		long tipoMov = 0;
		if (!insert)
			tipoMov = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");

		String active = NeoUtils.safeOutputString("1");

		//numero do contrato - Verificar de onde buscar
		String contract = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		if (contract.equals(""))
			contract = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));

		/*
		 * novo > novoCliente.ramoAtividade
		 * update > buscaNomeCliente.codram
		 */
		String groupId = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.sigmaRamoAtividade.cd_ramo_atividade"));
		/*
		 * if(!insert)
		 * groupId = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codram"));
		 */

		/*
		 * novo > novoCliente.tipoEmpresa.tipo
		 * Quando o Cliente for pessoa física, tipo 1.
		 * Quando o cliente for pessoa Jurídica, se for privado, tipo 0, senão, tipo 2.
		 */
		//F=fisica, J=juridica
		String tipcli = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipocliente.tipo"));
		//1=privada,  2=publica
		String tipemc = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipoEmpresa.tipo"));
		if (!insert || tipoAcao == 1 || tipoAcao == 2)
		{
			//F=fisica, J=juridica
			tipcli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipcli"));
			//1=privada,  2=publica
			tipemc = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipemc"));
		}
		String personType = "";
		if (tipcli.equals("F"))
			personType = NeoUtils.safeOutputString("1");
		else if (tipcli.equals("J") && tipemc.equals("1"))
			personType = NeoUtils.safeOutputString("0");
		else
			personType = NeoUtils.safeOutputString("2");

		/*
		 * novo >
		 * update > buscaNomeCliente.nomcli
		 */
		String corporationName = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		if (!insert || tipoAcao == 1 || tipoAcao == 2)
			corporationName = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.nomcli"));

		if (corporationName.length() >= 50)
			corporationName = corporationName.substring(0, 49);
		/*
		 * novo > novoCliente.nomeFantasia
		 * update > buscaNomeCliente.apecli
		 */
		String tradeName = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.nomeFantasia"));
		if (!insert || tipoAcao == 1 || tipoAcao == 2)
			tradeName = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.apecli"));

		if (tradeName.length() >= 50)
			tradeName = tradeName.substring(0, 49);

		if (tipoMov == 2)
		{
			/*
			 * se for alteração de razão social e nome fantasia
			 * deve-se buscar o valor digitado em tela
			 */
			corporationName = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
			tradeName = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.nomeFantasia"));
		}
		/*
		 * novo >
		 * update > buscaNomeCliente.cgccpf
		 */
		String taxpayerId = "";

		if (tipcli.equals("F"))
		{
			taxpayerId = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.cpf")).replace("/", "").replace(".", "").replace("-", "");
		}
		else
		{
			taxpayerId = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.cnpj")).replace("/", "").replace(".", "").replace("-", "");
		}

		if (!insert || tipoAcao == 1 || tipoAcao == 2)
		{
			/*
			 * TODO validar o tamanho do CPF/CNPJ
			 * se estiver faltando, completar com zeros a esquerda
			 * no SAPIENS o cpf/cnpj é numerico
			 */
			String tipoCliente = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipcli"));
			taxpayerId = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cgccpf"));
			if (tipoCliente.equals("F"))
			{
				//tamanho == 11
				while (taxpayerId.length() < 11)
					taxpayerId = "0" + taxpayerId;

			}
			else
			{
				while (taxpayerId.length() < 14)
					taxpayerId = "0" + taxpayerId;
			}

		}

		/*
		 * novo > novoCliente.email
		 * update > buscaNomeCliente.emanfe
		 */
		//String email = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.email"));
		String email = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.emailReferencia"));
		/*
		 * novo > novoCliente.pessoaContato.nome
		 * update > buscaNomeCliente.usu_pescon
		 */

		String responsible = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.nomeFantasia"));
		if (!tipcli.equals("F"))
			responsible = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.nomeContato"));

		if (!insert || tipoAcao == 1 || tipoAcao == 2)
		{
			//email = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.intnet"));
			responsible = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.nomcli"));
			if (!tipcli.equals("F"))
				responsible = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.usu_pescon"));
		}

		if (responsible.length() >= 50)
			responsible = responsible.substring(0, 49);

		/*
		 * novo > novoCliente.telefone1
		 * update > buscaNomeCliente.foncli
		 */
		String phone1 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
		/*
		 * if(!insert)
		 * phone1 = NeoUtils.safeOutputString("buscaNomeCliente.foncli");
		 */

		/*
		 * novo > novoCliente.telefone2
		 * update > buscaNomeCliente.foncl2
		 */
		String phone2 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone2"));
		/*
		 * if(!insert)
		 * phone2 = NeoUtils.safeOutputString("buscaNomeCliente.foncl2");
		 */

		String numPosto = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));

		System.out.println("[FLUXO CONTRATOS]-sigma - info de numero do posto -" + numPosto);
		//String references = (String) wPosto.findValue("vigilanciaEletronica.observacao"); //postosContrato.vigilanciaEletronica.observacao
		String references = (String) wPosto.findValue("vigilanciaEletronica.referencia"); //postosContrato.vigilanciaEletronica.observacao
		System.out.println("[FLUXO CONTRATOS]-sigma - references -" + references);		
		String observations = "CC:" + (String) wPosto.findValue("centroCusto.codccu"); //postosContrato.codccu
		if (wPosto.findValue("codccu") != null){ // Solicitado pela Sra. Caroline via tarefa 1144368 
			observations = "CC:" + (String) wPosto.findValue("codccu");
		}
		System.out.println("[FLUXO CONTRATOS]-sigma - observations -" + observations);

		String id = "";
		if (!insert)
			id = NeoUtils.safeOutputString(wPosto.findValue("contaSIGMA"));

		//Partição do Posto - Onde pegar esa informação???
		String partition = NeoUtils.safeOutputString("01");
		String controlPartition = NeoUtils.safeOutputString("0");
		/*
		 * novo > novoCliente.codigoCliente
		 * update > buscaNomeCliente.codcli
		 */
		String account = "";
		String companyId = null;
		if (!insert)
		{
			/*
			 * alteração de cliente deve-se usar a id_central ja existente do cliente
			 */
			Collection<NeoObject> contas = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACENTRAL"), new QLEqualsFilter("cd_cliente", NeoUtils.safeLong(id)));
			for (NeoObject conta : contas)
			{
				EntityWrapper wConta = new EntityWrapper(conta);
				account = NeoUtils.safeOutputString(wConta.findValue("id_central"));
				partition = NeoUtils.safeOutputString(wConta.findValue("particao"));
				companyId = NeoUtils.safeOutputString(wConta.findValue("id_empresa"));
				controlPartition = NeoUtils.safeOutputString(wConta.findValue("fg_controlar_particao"));
				if (controlPartition != null && !controlPartition.equals("") && controlPartition.equals("false"))
				{
					controlPartition = "0";
				}
				else if (controlPartition != null && !controlPartition.equals("") && controlPartition.equals("true"))
				{
					controlPartition = "1";
				}
				else
				{
					controlPartition = "0";
				}
			}
		}

		/*
		 * empresa.codemp
		 * Se for alteração vai usar o id_empresa vindo direto do dbcentral desconsiderando o mapeamento
		 * de empresas
		 */
		if (insert && companyId == null)
		{
			companyId = NeoUtils.safeOutputString(wPosto.findValue("regionalPosto.usu_codreg"));
		}
		String installerId = "";
		String responsibleTechnicianId = "";
		String forceOS = "1";

		responsibleTechnicianId = NeoUtils.safeOutputString(wPosto.findValue("buscaOperadorCM.cd_colaborador"));
		installerId = responsibleTechnicianId;

		if (insert && !companyId.equals(""))
		{
			NeoObject empresa = PersistEngine.getObject(AdapterUtils.getEntityClass("FGCRelacaoClienteSigmaSapiens"), new QLEqualsFilter("codClienteSapiens", companyId));
			if (empresa != null)
			{
				EntityWrapper wEmpresa = new EntityWrapper(empresa);

				boolean isOnDemand = NeoUtils.safeBoolean(wrapper.findValue("modelo.isOnDemand"));
				ContratoLogUtils.logInfo("FGCPrincipal#" + wrapper.findValue("neoId") + " isOnDemand?" + isOnDemand);
				if (isOnDemand)
				{
					companyId = "10118";
				}
				else
				{
					companyId = NeoUtils.safeOutputString(wEmpresa.findValue("codClienteSigma"));
				}

				/*
				 * NeoObject cadastroInstResp =
				 * PersistEngine.getObject(AdapterUtils.getEntityClass("FGCCadastroResponsavelInstalador"
				 * ), new QLEqualsFilter("ativo", true));
				 * if(cadastroInstResp == null)
				 * {
				 * throw new WorkflowException(
				 * "ERRO, comunicar a TI. Não encontrado responsavel tecnico ativo (FGCCadastroResponsavelInstalador)."
				 * );
				 * }
				 * EntityWrapper wCadastroInstResp = new EntityWrapper(cadastroInstResp);
				 * responsibleTechnicianId =
				 * NeoUtils.safeOutputString(wCadastroInstResp.findValue("responsavelTecnico.cd_colaborador"
				 * ));
				 * installerId =
				 * NeoUtils.safeOutputString(wCadastroInstResp.findValue("instalador.cd_colaborador"));
				 */
				//String nomeTecRespCompleto = NeoUtils.safeOutputString(wCadastroInstResp.findValue("responsavelTecnico.nm_colaborador"));
				//String nomeTecResp = nomeTecRespCompleto.substring(5, nomeTecRespCompleto.length());
				//responsibleTechnicianId = new ContratoContasSIGMAWebService().buscaListaInstaladores(companyId, nomeTecResp, wEmpresa.findValue("siglaRegional").toString());
				//nomeTecRespCompleto = NeoUtils.safeOutputString(wCadastroInstResp.findValue("instalador.nm_colaborador"));
				//nomeTecResp = nomeTecRespCompleto.substring(5, nomeTecRespCompleto.length());
				//installerId = new ContratoContasSIGMAWebService().buscaListaInstaladores(companyId, nomeTecResp, wEmpresa.findValue("siglaRegional").toString());
			}
			else
			{
				throw new Exception("ERRO, comunicar a TI. Não encontrada relação empresa SAPIENS x SIGMA (FGCCadastroResponsavelInstalador). Empresa Sapiens : " + companyId);
			}
		}
		
		String placa = NeoUtils.safeOutputString(wPosto.findGenericValue("placa"));
		placa = (placa == null || placa.equals("null")) ? "" : placa;
		map.put("tracker", placa);

		String zipCode = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.cep"));
		if (zipCode.equals(""))
			zipCode = NeoUtils.safeOutputString(wPosto.findValue("enderecoEfetivo.cep"));

		String address = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.endereco"));
		if (address.equals(""))
			address = NeoUtils.safeOutputString(wPosto.findValue("enderecoEfetivo.endereco"));

		String numero = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.numero"));
		if (numero.equals(""))
			numero = NeoUtils.safeOutputString(wPosto.findValue("enderecoEfetivo.numero"));

		String complemento = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.complemento"));
		if (complemento.equals(""))
			complemento = NeoUtils.safeOutputString(wPosto.findValue("enderecoEfetivo.complemento"));

		/*
		 * String addressGmap = address + ", " + numero;
		 * address = address + ", " + numero+ " - CPL.: "+complemento;
		 */

		address = address + ", " + numero;

		references = references + ", Complemento endereço: " + complemento + " - " + placa;

		address = ContratoContasSIGMAWebService.retiraCaracteresAcentuados(address);

		if (address.length() >= 100)
			address = address.substring(0, 99);

		String stateId = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.estado.sigufs"));
		if (stateId.equals(""))
			stateId = NeoUtils.safeOutputString(wPosto.findValue("enderecoEfetivo.estadoCidade.estado.sigufs"));

		String cidade = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.nomcid"));
		if (cidade.equals(""))
			cidade = NeoUtils.safeOutputString(wPosto.findValue("enderecoEfetivo.estadoCidade.cidade.nomcid"));
		String cityId = new ContratoContasSIGMAWebService().buscaListaCidades(stateId, cidade);

		String bairro = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.bairro"));
		if (bairro.equals(""))
			bairro = NeoUtils.safeOutputString(wPosto.findValue("enderecoEfetivo.bairro"));
		String districtId = new ContratoContasSIGMAWebService().buscaListaDistritos(cityId, bairro);
		if (districtId.equals(""))
		{
			throw new Exception("Bairro " + bairro + " não encontrado no sigma para a cidade codigo " + cityId + " - " + cidade + ". Por favor faça o cadastro do bairro e  tente novamente. ");
		}

		//sempre SOO-AA1-D-05_17
		//String route1Id = String.valueOf( ContratoContasSIGMAWebService.buscaRota("SOO-AA1-D-05_17") );
		String route1Id = String.valueOf( ParametrizacaoFluxoContratos.findParameter("rota1") );
		//sempre SOO-AA1-N-17_05
		//String route2Id = String.valueOf( ContratoContasSIGMAWebService.buscaRota("SOO-AA1-N-17_05") );
		String route2Id = String.valueOf( ParametrizacaoFluxoContratos.findParameter("rota2") );

		if (route1Id.equals("-1") || route2Id.equals("-1"))
		{
			throw new Exception("Rota 1 e/ou 2 não encontradas no sigma.");
		}

		//sempre pega daqui, pois na conta do sapiens não existe relação com este campo
		String branchActivityId = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.sigmaRamoAtividade.cd_ramo_atividade"));
		/*
		 * if(!insert)
		 * branchActivityId = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codram"));
		 */

		if (branchActivityId.equals(""))
		{
			branchActivityId = "10207";
		}

		//sistema fusion -- Está na tabela colaborador e usuário(com cd colaborador)
		// este usuário tem que estar ativo na tabela colaborador
		String sellerId = "89427";

		//String observations = NeoUtils.safeOutputString(wPosto.findValue("periodoFaturamento.obs"));

		if (!insert)
			map.put("id", id);

		map.put("active", active);
		map.put("companyId", companyId);
		map.put("account", account);
		map.put("contract", contract);

		map.put("complement", numPosto); // numero do posto
		if (tipoAcao != 5)
		{ // se for atualização de conta sigma, não alterar as informações de referencias e observações antigas da conta ao atualiza-lá
			map.put("references", references); // referencia do local
			map.put("observations", observations); // obervacoes do local
		}

		map.put("partition", partition);
		//map.put("groupId", groupId);
		map.put("personType", personType);
		map.put("corporationName", corporationName);
		map.put("tradeName", tradeName);
		map.put("taxpayerId", taxpayerId);
		map.put("responsible", responsible);
		map.put("email", email);
		map.put("phone1", phone1);
		map.put("phone2", phone2);
		map.put("zipCode", zipCode.replace("-", ""));
		map.put("address", address);
		map.put("stateId", stateId);
		map.put("cityId", cityId);
		map.put("districtId", districtId);

		map.put("installerId", installerId);
		map.put("responsibleTechnicianId", responsibleTechnicianId);
		map.put("forceOS", forceOS);

		map.put("route1Id", route1Id);
		map.put("route2Id", route2Id);
		map.put("installerId", installerId);
		map.put("branchActivityId", branchActivityId);
		map.put("sellerId", sellerId);

		map.put("controlPartition", controlPartition); // habilitado pois pode haver casos onde seja necessário trabalhar com a partição 099 e o webservice bloqueia se controlPartition for = 0

		map.put("addressGmap", address); // só é usado para encontrar a latitude e longitude do dendereço
		map.put("cidade", cidade); //parametro adicional para pegar a latitude  e longitude mais a frente no código, esse parametro não é passado ao webservice.
		return map;
	}

	/**
	 * Atravez do cd_cliente procura a conta na dbCentral para vincular ao posto do contrato na tabela
	 * usu_t160sig
	 * 
	 * @param cd_cliente
	 * @param num_ctr
	 * @param cod_emp
	 * @param cod_fil
	 * @param num_pos
	 * @throws Exception
	 */
	public static void relacionaSigmaSapiens(String cd_cliente, String num_ctr, String cod_emp, String cod_fil, String num_pos, Boolean isRastreamento) throws Exception
	{

		StringBuilder sql = new StringBuilder();
		sql.append("select ");
		sql.append("db.id_empresa, ");
		sql.append("db.id_central, ");
		sql.append("db.particao ");

		sql.append("from DBCentral db ");

		sql.append("where ");
		sql.append("db.cd_cliente = " + cd_cliente);

		Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();

		String id_empresa = "";
		String id_central = "";
		String particao = "";
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] os = (Object[]) result;
					id_empresa = NeoUtils.safeOutputString(os[0]);
					id_central = NeoUtils.safeOutputString(os[1]);
					particao = NeoUtils.safeOutputString(os[2]);
				}
			}
		}

		String insert = "INSERT INTO USU_T160SIG VALUES (" + cod_emp + ", " + cod_fil + ", " + num_ctr + ", " + num_pos + ", " + id_empresa + ", '" + id_central + "', " + particao + ", " + cd_cliente + ", '' )";
		sql = new StringBuilder();
		sql.append(insert);
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Query queryExecute2 = null;
		
		if (isRastreamento)
		{
			String insertVinculoParticaoZero = "INSERT INTO USU_T160SIG VALUES (" + cod_emp + ", " + cod_fil + ", " + num_ctr + ", " + num_pos + ", " + id_empresa + ", '" + id_central + "', 0, " + codSapiensParticaoZero + ", '' )";

			StringBuilder sql2 = new StringBuilder();
			sql2.append(insertVinculoParticaoZero);
			queryExecute2 = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql2.toString());
		}
		try
		{
			queryExecute.executeUpdate();
			System.out.println("[FLUXO CONTRATOS]-Vinculação de Contas SIGMA x SAPIENS criada.");
			
			if (isRastreamento && queryExecute2 != null)
				queryExecute2.executeUpdate();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro na Inserção na tabela USU_T160SIG do Sapiens (Relação das contas SIGMA e SAPIENS)");
		}
	}

	/**
	 * Método criado para contornar a limitação de caracteres da Descrição da OS via webservice.
	 * 
	 * @param idOs
	 * @param obs
	 * @throws Exception
	 */
	public static void updateDescricaoOS(String idOs, String obs) throws Exception
	{
		Connection con = null;
		if (idOs != null)
		{
			try
			{
				String sql = " update dbORDEM set DEFEITO = ? where ID_ORDEM = ? ";
				con = PersistEngine.getConnection("SIGMA90");
				PreparedStatement pst = con.prepareStatement(sql);
				System.out.println("****** PRINT DEFEITO PARA ANALISE [ContratoCadastraContasSIGMA] *****");
			    System.out.println(obs);
				pst.setString(1, obs);
				pst.setLong(2, new Long(idOs));
				pst.executeUpdate();
				ContratoLogUtils.logInfo("Atualização descrição da OS(" + idOs + ") feita com sucesso");
				pst.close();
			}
			catch (Exception e)
			{
				throw new Exception("Erro ao atualizar a OS " + idOs);
			}
			finally
			{
				try
				{
					if (con != null && !con.isClosed())
						con.close();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * Método criado para contornar a limitação de caracteres da Descrição da OS via webservice.
	 * 
	 * @param idOs
	 * @param obs
	 * @throws Exception
	 */
	public static void ativarWebAlarme(String cd_cliente) throws Exception
	{
		Connection con = null;
		if (cd_cliente != null)
		{
			try
			{
				String sql = " update dbcentral set senha_internet='webalarme', reportar_internet='true' where cd_cliente = ? ";
				con = PersistEngine.getConnection("SIGMA90");
				PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1, cd_cliente);
				pst.executeUpdate();
				ContratoLogUtils.logInfo("Ativacao do webalarme(" + cd_cliente + ") feita com sucesso");
				pst.close();
			}
			catch (Exception e)
			{

				throw new Exception("Erro ao ativar o webalarme: " + cd_cliente);
			}
			finally
			{
				try
				{
					if (con != null && !con.isClosed())
						con.close();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		}
	}
	
	public static void ativarParticaoZero(String id_central) throws Exception
	{
		Connection con = null;
		if (id_central != null)
		{
			try
			{
				con = PersistEngine.getConnection("SIGMA90");
				
				String sql = " update dbcentral set ctrl_central=0, fg_ativo=0 where id_central = ?";
				String sql2 = " update dbcentral set ctrl_central=1, fg_ativo=1 where id_central = ? and particao = '000'";
				
				PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1, id_central);
				pst.executeUpdate();
				
				ContratoLogUtils.logInfo("Desativando particoes. Dados: " + id_central);
				
				PreparedStatement pst2 = con.prepareStatement(sql2);
				pst2.setString(1, id_central);
				pst2.executeUpdate();
				
				ContratoLogUtils.logInfo("Ativando particao zero. Dados: " + id_central);
				
				pst.close();
				pst2.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new Exception("Erro ao ativar a particao zero: " + id_central);
			}
			finally
			{
				try
				{
					if (con != null && !con.isClosed())
						con.close();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		}
	}

	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
