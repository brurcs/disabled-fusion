package com.neomind.fusion.custom.orsegups.autocargo.beans.client;

import java.util.List;

public class ACClients {
    
    private List<ACClient> clients;

    public List<ACClient> getClients() {
        return clients;
    }

    public void setClients(List<ACClient> clients) {
        this.clients = clients;
    }
    
    

}
