package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaSimplesColaboradorSemFoto implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesColaboradorSemFoto");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples colaborador sem Foto - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		try
		{
			sql.append(" SELECT FUN.numemp,FUN.numcad,FUN.nomfun,FUN.tipcol,CAR.TitRed,orn.usu_codreg, REG.USU_NomReg,FUN.datadm FROM R034FUN FUN ");
			sql.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.TabOrg = fun.TabOrg AND orn.NumLoc = fun.NumLoc ");
			sql.append(" INNER JOIN R038HCA HCA ON HCA.NumCad = FUN.numcad AND HCA.NumEmp = FUN.numemp AND FUN.tipcol = HCA.TipCol AND HCA.DatAlt = (SELECT MAX(DatAlt) FROM R038HCA HCA1 WHERE HCA.NumCad = HCA1.numcad AND HCA.NumEmp = HCA1.numemp AND HCA1.tipcol = HCA.TipCol) ");
			sql.append(" INNER JOIN R024CAR CAR ON CAR.CODCAR = HCA.CODCAR AND CAR.EstCar = 1 ");
			sql.append(" INNER JOIN USU_T200REG REG ON REG.USU_CodReg = orn.usu_codreg ");
			//NOT LIKE 'DANIEL FELAU' = SOLICITAÇÃO VIA TAREFA 833624 - JULIANO CLEMENTE.
			sql.append(" WHERE SITAFA NOT in(3,4,6,7,8,58,74,124) AND (FUN.NOMFUN NOT LIKE 'DANIEL FELAU') AND (FUN.NOMFUN NOT LIKE 'VANDERLEI MICHELO')  AND FUN.NUMEMP < 23 AND fun.numemp not in (3,4,12) AND FUN.TIPCOL = 1 AND (CAR.TitRed NOT LIKE '%PRESIDENTE%' AND CAR.TitCar NOT LIKE '%DIRETOR%' AND CAR.TitCar NOT LIKE '%SUPERINT%') AND FUN.codfil not in (397) AND ");
			sql.append("      NOT EXISTS (SELECT 1 FROM R034FOT FOT WHERE FOT.NUMEMP = FUN.NUMEMP AND FOT.NUMCAD = FUN.NUMCAD AND FOT.TIPCOL = FUN.TIPCOL) AND  ");
			sql.append("      NOT EXISTS (SELECT 1 FROM [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_TAREFAFOTOADMISSAO TAD WITH (NOLOCK)  ");
			sql.append(" 					inner join [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.wfprocess wf on wf.code = TAD.tarefa  ");
			sql.append("		      		inner join [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_Tarefa d on d.wfprocess_neoId = wf.neoId ");
			sql.append("					WHERE TAD.NUMEMP = FUN.NUMEMP AND  ");
			sql.append("					  	  TAD.TIPCOL = FUN.TIPCOL AND  ");
			sql.append("						  TAD.NUMCAD = FUN.NUMCAD AND ");
			sql.append("						  CAST(FLOOR(CAST(TAD.DATADM AS FLOAT)) AS DATETIME) = CAST(FLOOR(CAST(FUN.DATADM AS FLOAT)) AS DATETIME) AND ");
			sql.append("						  ((d.titulo like '%Enviar foto do colaborador -%') OR (d.titulo like '%[SEM FOTO] Enviar foto do colaborador -%')) and wf.saved = 1 and wf.processState = 0) ");
			sql.append(" ORDER BY 6,5,1,2,3 ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				GregorianCalendar datAdm = new GregorianCalendar();
				datAdm.setTime(rs.getDate("DatAdm"));

				String titulo = "[SEM FOTO] Enviar foto do colaborador - " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun");
				String descricao = "Enviar em anexo a foto do colaborador " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun") + ", " + rs.getString("TitRed") + ", Regional: " + rs.getString("USU_NomReg") + ", admitido do dia " + NeoDateUtils.safeDateFormat(datAdm, "dd/MM/yyyy");
				descricao = descricao +" - Pois o mesmo não possui foto cadastrada no RUBI.";
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				Long codReg = rs.getLong("USU_CodReg");

				NeoPaper papelSolicitante = new NeoPaper();
				NeoPaper papelExecutor = new NeoPaper();
				String solicitante = "";
				String executor = "";

				papelSolicitante = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteTarefaColaboradorSemFoto"));
				
				if(papelSolicitante != null && papelSolicitante.getAllUsers() != null && !papelSolicitante.getAllUsers().isEmpty()){
				    
				    for(NeoUser user : papelSolicitante.getAllUsers()){
					
					solicitante = user.getCode();
					break;
					
				    }
				    
				}
				else
				{
				    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão - Papel de Solicitante pode não existir");
				    continue;
				}
				
				papelExecutor = OrsegupsUtils.getPapelFotoAdmissao(codReg, null);

				if (papelExecutor != null && papelExecutor.getAllUsers() != null && !papelExecutor.getAllUsers().isEmpty())
				{
				    for (NeoUser user : papelExecutor.getUsers())
				    {
					executor = user.getCode();
					break;
				    }
				}
				else
				{
				    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão - Papel de Executor pode não existir");
				    continue;
				}

				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

				InstantiableEntityInfo tarefaAD = AdapterUtils.getInstantiableEntityInfo("TarefaFotoAdmissao");
				NeoObject noAD = tarefaAD.createNewInstance();
				EntityWrapper ajWrapper = new EntityWrapper(noAD);

				ajWrapper.findField("numemp").setValue(rs.getLong("NumEmp"));
				ajWrapper.findField("tipcol").setValue(rs.getLong("TipCol"));
				ajWrapper.findField("numcad").setValue(rs.getLong("NumCad"));
				ajWrapper.findField("datadm").setValue(datAdm);
				ajWrapper.findField("tarefa").setValue(tarefa);
				PersistEngine.persist(noAD);

				log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão - Responsavel " + executor + " Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoDateUtils.safeDateFormat(rs.getDate("DatAdm"), "dd/MM/yyyy"));

				Thread.sleep(1000);
			}
		}
		catch (Exception e)
		{
			log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Foto Admissão - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}