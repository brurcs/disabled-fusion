package com.neomind.fusion.custom.orsegups.maps.call.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertEventEngine;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.RamalLigacao;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name = "OrsegupsAlertEventServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventServlet" }, asyncSupported = true)
public class OrsegupsAlertEventServlet extends HttpServlet
{


    	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(OrsegupsAlertEventServlet.class);
	private Pattern pattern;
	private Matcher matcher;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{

		resp.setContentType("text/plain");
		req.setCharacterEncoding("ISO-8859-1");
		resp.setCharacterEncoding("ISO-8859-1");

		final PrintWriter out = resp.getWriter();

		String action = "";

		if (req.getParameter("action") != null)
		{
			action = req.getParameter("action");
		}
		if (action.equalsIgnoreCase("ligarCliente"))
		{
			this.ligarCliente(req, resp, out, null);
		}
		else if (action.equalsIgnoreCase("logEvento"))
		{
			this.getLogEventos(req, resp, out, "logEvento", null);
		}
		else if (action.equalsIgnoreCase("telefone"))
		{
			String cdCliente = req.getParameter("cdCliente");
			this.telefonesCliente(req, resp, out, cdCliente, null);
		}
		else if (action.equalsIgnoreCase("getLog"))
		{
			String cdHistorico = req.getParameter("historico");
			String textoLog = new String(req.getParameter("textoLog").getBytes(),"UTF-8");
			String nomeCol = req.getParameter("nomeCol");
			this.saveLogPopUp(req, resp, out, cdHistorico, textoLog, nomeCol);
		}
		else if (action.equalsIgnoreCase("atualizaEventos"))
		{
			this.atualizaEventos(req, resp, out, null);
		}
		else if (action.equalsIgnoreCase("proximoEventoCliente"))
		{
			this.proximoEventoCliente(req, resp, out, null);
		}
		else if (action.equals("totalDeLigacoes"))
		{
			//String ramal = request.getParameter("ramal");
			String dataInical = req.getParameter("dataInical");
			String dataFinal = req.getParameter("dataFinal");

			this.getTotalLigacao(dataInical, dataFinal, out);
		}
	}

	private void getLogEventos(HttpServletRequest req, HttpServletResponse resp, PrintWriter out, String string, Object object)
	{
		OrsegupsAlertEventEngine event = OrsegupsAlertEventEngine.getInstance();

		String codigoHistorico = req.getParameter("codigoHistorico");
		String cdCliente = req.getParameter("cdCliente");
		String texto = req.getParameter("texto");

		event.cadastrarEvetLog(codigoHistorico, cdCliente, texto);

	}

	private void saveLogPopUp(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdHistorico, String textoLog, String nomeCol)
	{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String texto = textoLog;
		try
		{
			if (textoLog != null && !textoLog.isEmpty() && !textoLog.contains("null") && verificaEventoHistorico(cdHistorico))
			{
				texto = textoLog;
				if (texto.contains("AtualizarCadastro"))
				{
					texto = "#" + texto;
					System.out.println("EVENTO MAIL AtualizarCadastro " + cdHistorico + " - " + nomeCol);
				}
				else if (texto.contains("AtendimentoRealizado"))
				{
					texto = "#" + texto;
					System.out.println("EVENTO MAIL AtendimentoRealizado " + cdHistorico + " - " + nomeCol);
				}
				else if (texto.contains("SemContato"))
				{
					texto = "#" + texto;
					System.out.println("EVENTO MAIL SemContato " + cdHistorico + " - " + nomeCol);
				}
				else if (texto.contains("NaoEnviaComunicado"))
				{
					texto = "#" + texto;
					System.out.println("EVENTO MAIL NaoEnviaComunicado " + cdHistorico + " - " + nomeCol);
				}
				conn = PersistEngine.getConnection("SIGMA90");
				StringTokenizer token = new StringTokenizer(texto, ";");
				conn.setAutoCommit(false);

				while (token.hasMoreTokens())
				{
					String textoLocal = "";

					//					if(token.nextToken() != null && !token.nextToken().contains("Op") )
					//					 textoLocal = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + " - Op. CM -" + PortalUtil.getCurrentUser().getFullName() + " : " + token.nextToken();
					//									
					textoLocal = token.nextToken();
					if (textoLocal != null && textoLocal.contains(":"))
					{

						String array[] = textoLocal.split(":");
						if (array != null && array.length > 0 && !array[1].contains("  ") || !array[1].isEmpty())
						{
							StringBuilder sqlUpdate = new StringBuilder();
							//String select = "(SELECT CASE WHEN TX_OBSERVACAO_FECHAMENTO IS NULL THEN '' ELSE TX_OBSERVACAO_FECHAMENTO END AS TX_OBSERVACAO_FECHAMENTO FROM HISTORICO WITH (NOLOCK) WHERE CD_HISTORICO = 1)"
							sqlUpdate.append(" UPDATE HISTORICO  SET  TX_OBSERVACAO_FECHAMENTO = CASE WHEN(TX_OBSERVACAO_FECHAMENTO IS NULL) THEN '' ELSE TX_OBSERVACAO_FECHAMENTO END  + (CHAR(10) + CHAR(13)) + ?  WHERE CD_HISTORICO = ? ");
							st = conn.prepareStatement(sqlUpdate.toString());
							st.setString(1, textoLocal);
							st.setString(2, cdHistorico);
							st.executeUpdate();
						}

					}
					else if (textoLocal != null && !textoLocal.isEmpty())
					{
						String textoAux = textoLocal;
						if (texto != null && !texto.contains("Op"))
							textoAux = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + ": - Op. CM - " + nomeCol + " : " + textoLocal;
						StringBuilder sqlUpdate = new StringBuilder();
						sqlUpdate.append(" UPDATE HISTORICO  SET  TX_OBSERVACAO_FECHAMENTO = CASE WHEN(TX_OBSERVACAO_FECHAMENTO IS NULL) THEN '' ELSE TX_OBSERVACAO_FECHAMENTO END  + (CHAR(10) + CHAR(13)) + ?   WHERE CD_HISTORICO = ?");
						st = conn.prepareStatement(sqlUpdate.toString());
						st.setString(1, textoAux);
						st.setString(2, cdHistorico);
						st.executeUpdate();
						break;
					}
				}
				conn.commit();
				conn.setAutoCommit(true);
				OrsegupsUtils.closeConnection(conn, st, rs);

//				if (texto.contains("AtualizarCadastro"))
//				{
//					texto = "#" + texto;
//					abreTarefaAtualizacaoCadastral(cdHistorico, nomeCol);
//					System.out.println("EVENTO MAIL AtualizarCadastro " + cdHistorico + " - " + nomeCol);
//				}
//				else if (texto.contains("AtendimentoRealizado"))
//				{
//					texto = "#" + texto;
//					System.out.println("EVENTO MAIL AtendimentoRealizado " + cdHistorico + " - " + nomeCol);
//				}
//				else if (texto.contains("SemContato"))
//				{
//					texto = "#" + texto;
//					enviaEmailCliente(montaEmailEventoSemContato(cdHistorico), cdHistorico, 0);
//					System.out.println("EVENTO MAIL SemContato " + cdHistorico + " - " + nomeCol);
//				}
//				else if (texto.contains("NaoEnviaComunicado"))
//				{
//					texto = "#" + texto;
//					enviaEmailCliente(montaEmailNaoEnviarCliente(cdHistorico, nomeCol), cdHistorico, 1);
//					System.out.println("EVENTO MAIL NaoEnviaComunicado " + cdHistorico + " - " + nomeCol);
//				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

	}


	public void telefonesCliente(HttpServletRequest req, HttpServletResponse resp, PrintWriter out, String cdCliente, Object object)
	{
		// TODO buscar a lista de telefones que devem ser chamados em ordem de prioridade
		//LinkedHashSet mantém a ordem de inserção dos objetos
		
		log.debug("Buscando a lista de telefones do cliente para ligar! " + cdCliente);

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try
		{

			connection = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT DBC.FONE1 AS TELEFONE1, DBC.FONE2 AS TELEFONE2, DBP.FONE1 AS TELEFONE3, DBP.FONE2 AS TELEFONE4  ");
			sql.append(" FROM dbPROVIDENCIA DBP  ");
			sql.append(" RIGHT JOIN dbCENTRAL DBC ON (DBC.CD_CLIENTE =  DBP.CD_CLIENTE) ");
			sql.append(" WHERE DBC.ctrl_central = 1 AND DBC.CD_CLIENTE = " + cdCliente);
			statement = connection.prepareStatement(sql.toString());
			resultSet = statement.executeQuery();
			
			List<String> listaTelefone = new ArrayList<String>();
			
			while (resultSet.next())
			{

				if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE1")) && !listaTelefone.contains(resultSet.getString("TELEFONE1")))
				{
					listaTelefone.add(new String(resultSet.getString("TELEFONE1")));
				}
				else if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE2")) && !listaTelefone.contains(resultSet.getString("TELEFONE2")))
				{
					listaTelefone.add(new String(resultSet.getString("TELEFONE2")));
				}
				else if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE3")) && !listaTelefone.contains(resultSet.getString("TELEFONE3")))
				{
					listaTelefone.add(new String(resultSet.getString("TELEFONE3")));
				}
				else if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE4")) && !listaTelefone.contains(resultSet.getString("TELEFONE4")))
				{
					listaTelefone.add(new String(resultSet.getString("TELEFONE4")));
				}
				
			}
			JSONArray jsonTelefones = new JSONArray();
			if (listaTelefone != null && !listaTelefone.isEmpty())
			{
				for (String tel : listaTelefone)
				{
					String telefone = tel;
					tel.replace("(", "").replace(")", "").replace("-", "").trim();
					JSONObject jsonTelefone = new JSONObject();
					jsonTelefone.put("codtel", String.valueOf(tel));
					jsonTelefone.put("telefone", String.valueOf(telefone));
					jsonTelefones.put(jsonTelefone);
				}
			}

			out.print(jsonTelefones);
			out.flush();
			out.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao efetuar ao pesquisar telefones do cliente " + cdCliente + " - " + e.getMessage());

		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);

		}

	}

	private void ligarCliente(HttpServletRequest req, HttpServletResponse resp, PrintWriter out, Object object)
	{

		int result = 0;
		try
		{

			OrsegupsAlertEventEngine alertAitServlet = OrsegupsAlertEventEngine.getInstance();
			String ramal = req.getParameter("ramal");
			String numeroExterno = req.getParameter("numeroExterno");
			String codHistorico = req.getParameter("codHistorico");
			String cdCliente = req.getParameter("cdCliente");
			//ANTERIOR SNEP
			//		 alertAitServlet.liberarOperador(ramal);
			//		 alertAitServlet.pausarOperador(ramal);
			//		 alertAitServlet.solicitarLigacaoSnep(ramal, numeroExterno);
			//ATUAL FUSION
			
			/**
			 * Adição do novo digito para telefones móveis
			 */
			List<NeoObject> listaSubstituicao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TINonoDigito"));

			if (listaSubstituicao != null && !listaSubstituicao.isEmpty()) {

			    NeoObject objAdicionar = listaSubstituicao.get(0);

			    EntityWrapper wAdicionar = new EntityWrapper(objAdicionar);
			    
			    boolean adicionar = (boolean) wAdicionar.findField("adicionar").getValue();
			    			    
			    if(adicionar){
				if (numeroExterno.startsWith("0")){
				    numeroExterno = numeroExterno.substring(1);
				}
				if (numeroExterno.length() == 8){			    
				    if(numeroExterno.startsWith("7") || numeroExterno.startsWith("8") || numeroExterno.startsWith("9")){
					numeroExterno = "9"+numeroExterno;
				    }
		    
				}else if (numeroExterno.length() == 10){
				    String subFone = numeroExterno.substring(2);
				    
				    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
					subFone = "9"+subFone;
					numeroExterno = numeroExterno.substring(0, 2)+subFone;
				    }			
				}
			    }
			}

			alertAitServlet.getLigacaoSnep(ramal, numeroExterno);
			String textoLog = "Ligando para cliente! " + numeroExterno + " Ramal " + ramal + "###";

			alertAitServlet.cadastrarEvetLog(codHistorico, cdCliente, textoLog);
			result = 1;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		out.print(result);
		out.flush();
		out.close();
	}


	private void atualizaEventos(HttpServletRequest req, HttpServletResponse resp, PrintWriter out, Object object)
	{

		EventoVO eventoVO = null;
		try
		{

			OrsegupsAlertEventEngine alertEventServlet = OrsegupsAlertEventEngine.getInstance();
			String codHistorico = req.getParameter("codHistorico");
			String valor = req.getParameter("valor");

			eventoVO = alertEventServlet.getDadosEventosHistorico(null, codHistorico, valor);

			if (eventoVO == null)
				eventoVO = new EventoVO();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		Gson gson = new Gson();
		String eventJson = "[]";

		if (eventoVO.getEventoHistoricoVOs() != null && !eventoVO.getEventoHistoricoVOs().isEmpty())
			eventJson = gson.toJson(eventoVO.getEventoHistoricoVOs());

		out.print(eventJson);
		out.flush();
		out.close();
	}

	public void proximoEventoCliente(HttpServletRequest req, HttpServletResponse resp, PrintWriter out, Object object)
	{

		try
		{

			OrsegupsAlertEventEngine alertEvent = OrsegupsAlertEventEngine.getInstance();
			String codHistorico = req.getParameter("codHistorico");
			String dataEventoCliente = req.getParameter("dataEvento");

			alertEvent.proximoEventoCliente(codHistorico, dataEventoCliente);
			out.print(1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			out.print(0);
		}

		out.flush();
		out.close();
	}

	private void getTotalLigacao(String dataInical, String dataFinal, PrintWriter out)
	{
		OrsegupsAlertEventServlet.log.warn("##### INICIAR ROTINA RotinaLigacaoEventos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
	
		try
		{
			Gson gson = new Gson();
			conn = PersistEngine.getConnection("SIGMA90");

			List<NeoObject> listLigacaoRamal = null;
			Map<String, String> ligacaoMap = null;

			listLigacaoRamal = PersistEngine.getObjects(AdapterUtils.getEntityClass("CMRamais"));
			PersistEngine.getEntityManager().flush();

			if (NeoUtils.safeIsNotNull(listLigacaoRamal) && !listLigacaoRamal.isEmpty())
			{

				Calendar periodo = Calendar.getInstance();
				//METODO TEMPORARIO
				String periodoInicial = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");
				String periodoFinal = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");
				
				if (periodo.get(Calendar.HOUR_OF_DAY) >= 6 && periodo.get(Calendar.HOUR_OF_DAY) < 12)
				{
					periodoInicial += " 06:00:0.000";
					periodoFinal += " 11:59:0.000";
				
				}
				else if (periodo.get(Calendar.HOUR_OF_DAY) >= 12 && periodo.get(Calendar.HOUR_OF_DAY) < 18)
				{
					periodoInicial += " 12:00:0.000";
					periodoFinal += " 17:59:0.000";
				
				}
				else if (periodo.get(Calendar.HOUR_OF_DAY) >= 18 && periodo.get(Calendar.HOUR_OF_DAY) < 24)
				{
					periodoInicial += " 18:00:0.000";
					periodoFinal += " 23:59:0.000";
				
				}
				else if (periodo.get(Calendar.HOUR_OF_DAY) >= 0 && periodo.get(Calendar.HOUR_OF_DAY) < 6)
				{
					periodoInicial += " 00:00:0.000";
					periodoFinal += " 05:59:0.000";
				
				}
				String ramal = null;
				String soma = null;
				ligacaoMap = new HashMap<String, String>();
				Integer totalizador = 0;
				for (NeoObject neoObject : listLigacaoRamal)
				{

					NeoObject neoOts = (NeoObject) neoObject;
					EntityWrapper ewOts = new EntityWrapper(neoOts);
					ramal = (String) ewOts.findField("ramal").getValue();
					soma = "";
					StringBuilder sql = new StringBuilder();
					sql.append(" SELECT DISTINCT COUNT (*) AS SOMA ");
					sql.append(" FROM VIEW_HISTORICO H WITH (NOLOCK)    INNER JOIN HISTORICO_FRASE_EVENTO HF WITH (NOLOCK)  ON HF.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
					sql.append(" WHERE  H.DT_RECEBIDO BETWEEN '" + periodoInicial + "' AND '" + periodoFinal + "'   ");
					sql.append(" AND (H.CD_EVENTO LIKE 'X406' OR H.CD_EVENTO LIKE 'E401' OR H.CD_EVENTO LIKE 'XXX2' OR H.CD_EVENTO LIKE 'XXX5') AND H.TX_OBSERVACAO_FECHAMENTO LIKE '%" + ramal + "##%' ");

					pstm = conn.prepareStatement(sql.toString());
					rs = pstm.executeQuery();
					
					while (rs.next())
					{
						soma = rs.getString("SOMA");
						totalizador += Integer.parseInt(soma);
					}

					ligacaoMap.put(soma, ramal);
				}

				if (NeoUtils.safeIsNotNull(soma))
				{
					Map<String, String> map = new TreeMap<String, String>(ligacaoMap);
					
					List<RamalLigacao> ramalLigacaoList = new ArrayList<RamalLigacao>();
					for (Map.Entry<?,?> entry : map.entrySet())
					{
						RamalLigacao ligacao = new RamalLigacao();
						String valor = (String) entry.getKey();
						String chave = (String) entry.getValue();
						System.out.println("Chave " + chave + " Value " + valor);
						ligacao.setRamal(chave);
						ligacao.setTotal(Integer.parseInt(valor));
						ramalLigacaoList.add(ligacao);

					}
					if ((ramalLigacaoList != null) && (!ramalLigacaoList.isEmpty()))
					{
						String resultadoJson = gson.toJson(ramalLigacaoList);
						System.out.println(resultadoJson);
						out.print(resultadoJson);
					}

				}

			}
			OrsegupsAlertEventServlet.log.warn("##### EXECUTAR RotinaLigacaoEventos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			e.printStackTrace();
			OrsegupsAlertEventServlet.log.error("##### ERRO RotinaLigacaoEventos - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);

			}
			catch (Exception e)
			{
				e.printStackTrace();
				OrsegupsAlertEventServlet.log.error("##### ERRO RotinaLigacaoEventos - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			OrsegupsAlertEventServlet.log.warn("##### FINALIZAR RotinaLigacaoEventos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}


	public boolean validEmail(String email)
	{
		Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
		Matcher m = p.matcher(email);
		if (m.find())
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	public List<String> validarEmail(String email, String cdCliente)
	{
		List<String> listaDeEmail = new ArrayList<String>();
		matcher = pattern.matcher(email);
		String cmd = "";

		Connection conn = PersistEngine.getConnection("SIGMA90");
		PreparedStatement pstm = null;
		ResultSet rsSapiens = null;
		try
		{
			if (matcher.matches())
			{
				//email = "sistemas@orsegups.com.br";
				listaDeEmail.add(email);
			}
			else
			{
				cmd = " SELECT email FROM DBPROVIDENCIA WHERE cd_cliente = " + Long.valueOf(cdCliente) + " AND  email IS NOT NULL AND email <> ' ' ";

				pstm = conn.prepareStatement(cmd);

				ResultSet rs = pstm.executeQuery();

				while (rs.next())
				{
					String emailRs = rs.getString(1);
					matcher = pattern.matcher(emailRs);

					if (matcher.matches())
					{
						listaDeEmail.add(emailRs);
					}

				}
			}

			conn = PersistEngine.getConnection("SAPIENS");
			
			if (pstm != null){
			    pstm.close();
			}
			
			pstm = null;
			cmd = "";
			cmd = " SELECT DISTINCT cli.IntNet ";
			cmd += " FROM USU_T160SIG sig ";
			cmd += " INNER JOIN USU_T160CTR ctr ON ctr.usu_codemp = sig.usu_codemp and ctr.usu_codfil = sig.usu_codfil and ctr.usu_numctr = sig.usu_numctr ";
			cmd += " inner join dbo.E085CLI cli ON CLI.CodCli = CTR.usu_codcli ";
			cmd += " WHERE sig.usu_codcli = " + cdCliente;
			cmd += " and cli.IntNet <> ' ' ";

			pstm = conn.prepareStatement(cmd);
			rsSapiens = pstm.executeQuery();

			while (rsSapiens.next())
			{
				String[] campos = null;
				boolean achou = false;
				String emailRs = rsSapiens.getString(1).replaceAll(",", ";").replaceAll(" ", "");
				campos = emailRs.split(";");

				for (String em : campos)
				{
					matcher = pattern.matcher(em);
					if (matcher.matches())
					{
						for (String listEma : listaDeEmail)
						{
							if (listEma.equalsIgnoreCase(em))
							{
								achou = true;
							}
						}
						if (!achou)
						{
							listaDeEmail.add(em);
						}
					}
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("E-Mail X406 erro validação do e-mail: " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(null, null, rsSapiens);
			OrsegupsUtils.closeConnection(conn, pstm, rsSapiens);
		}
		return listaDeEmail;

	}


	// verifica se o evento ainda existe no historico

	private Boolean verificaEventoHistorico(String cdHistorico)
	{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		Boolean flag = Boolean.FALSE;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT H.CD_HISTORICO FROM HISTORICO H WITH (NOLOCK)  WHERE H.CD_HISTORICO = ? ");
			sql.append(" AND  h.FG_STATUS IN (0,5) AND (( H.CD_EVENTO LIKE 'X406')  OR (H.CD_EVENTO LIKE 'XVID') OR (H.CD_EVENTO LIKE 'XVD2') OR (H.CD_EVENTO LIKE 'XRED') OR (H.CD_EVENTO LIKE 'E401' AND H.CD_CODE LIKE 'DLC' ) OR (H.CD_EVENTO LIKE 'XXX2' AND H.CD_CODE LIKE 'NAR') OR (H.CD_EVENTO LIKE 'XXX5' AND H.CD_CODE LIKE 'DST'))  ");
			st = conn.prepareStatement(sql.toString());
			st.setString(1, cdHistorico);
			rs = st.executeQuery();

			if (rs.next())
				flag = Boolean.TRUE;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT verificaEventoHistorico: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}
		return flag;

	}



	public static List<String> dias(GregorianCalendar calendar)
	{
		List<String> cptList = null;
		if (NeoUtils.safeIsNotNull(calendar))
		{
			long milisecInicial = calendar.getTime().getTime();
			long milisecFinal = new GregorianCalendar().getTime().getTime();
			long dif = milisecFinal - milisecInicial;

			long dias = (((dif / 1000) / 60) / 60) / 24;
			cptList = new ArrayList<String>();
			Calendar dia = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
			for (int i = 0; i <= dias; i++)
			{
				String diaStr = dateFormat.format(dia.getTime());
				cptList.add(diaStr);
				dia.add(Calendar.DAY_OF_MONTH, -1);
			}

		}
		return cptList;
	}

}
