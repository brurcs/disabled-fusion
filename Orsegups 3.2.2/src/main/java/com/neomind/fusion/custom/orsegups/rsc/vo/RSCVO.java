package com.neomind.fusion.custom.orsegups.rsc.vo;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;

public class RSCVO
{
	Long codCli;
	Long idProcesso;
	String tarefa;
	String nome;
	String email;
	String telefone;
	String cidade;
	String bairro;
	String mensagem;
	String tipoContato;
	String origemSolicitacao;
	String idCtrSapiens;
	GregorianCalendar prazo;
	NeoUser responsavelAtendimento;
	NeoPaper responsavelExecutor;
	NeoObject tipoSolicitacao;
	NeoObject regional;
	
	public Long getCodCli()
	{
		return codCli;
	}


	public void setCodCli(Long codCli)
	{
		this.codCli = codCli;
	}


	public Long getIdProcesso()
	{
		return idProcesso;
	}


	public void setIdProcesso(Long idProcesso)
	{
		this.idProcesso = idProcesso;
	}


	public String getTarefa()
	{
		return tarefa;
	}


	public void setTarefa(String tarefa)
	{
		this.tarefa = tarefa;
	}


	public String getNome()
	{
		return nome;
	}


	public void setNome(String nome)
	{
		this.nome = nome;
	}


	public String getEmail()
	{
		return email;
	}


	public void setEmail(String email)
	{
		this.email = email;
	}


	public String getTelefone()
	{
		return telefone;
	}


	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}


	public String getCidade()
	{
		return cidade;
	}


	public void setCidade(String cidade)
	{
		this.cidade = cidade;
	}


	public String getBairro()
	{
		return bairro;
	}


	public void setBairro(String bairro)
	{
		this.bairro = bairro;
	}


	public String getMensagem()
	{
		return mensagem;
	}


	public void setMensagem(String mensagem)
	{
		this.mensagem = mensagem;
	}


	public String getTipoContato()
	{
		return tipoContato;
	}


	public void setTipoContato(String tipoContato)
	{
		this.tipoContato = tipoContato;
	}


	public String getOrigemSolicitacao()
	{
		return origemSolicitacao;
	}


	public void setOrigemSolicitacao(String origemSolicitacao)
	{
		this.origemSolicitacao = origemSolicitacao;
	}


	public GregorianCalendar getPrazo()
	{
		return prazo;
	}


	public void setPrazo(GregorianCalendar prazo)
	{
		this.prazo = prazo;
	}
	
	public NeoUser getResponsavelAtendimento()
	{
		return responsavelAtendimento;
	}


	public void setResponsavelAtendimento(NeoUser responsavelAtendimento)
	{
		this.responsavelAtendimento = responsavelAtendimento;
	}


	public NeoPaper getResponsavelExecutor()
	{
		return responsavelExecutor;
	}


	public void setResponsavelExecutor(NeoPaper responsavelExecutor)
	{
		this.responsavelExecutor = responsavelExecutor;
	}
	

	public NeoObject getTipoSolicitacao()
	{
		return tipoSolicitacao;
	}


	public void setTipoSolicitacao(NeoObject tipoSolicitacao)
	{
		this.tipoSolicitacao = tipoSolicitacao;
	}

	public NeoObject getRegional()
	{
		return regional;
	}


	public void setRegional(NeoObject regional)
	{
		this.regional = regional;
	}
	
	public String getIdCtrSapiens() {
		return idCtrSapiens;
	}


	public void setIdCtrSapiens(String idCtrSapiens) {
		this.idCtrSapiens = idCtrSapiens;
	}


	@Override
	public String toString()
	{
		return "RSCVO [codCli=" + codCli + ", idProcesso=" + idProcesso + ", tarefa=" + tarefa + ", nome=" + nome + ", email=" + email + ", telefone=" + telefone + ", cidade=" + cidade + ", bairro=" + bairro + ", mensagem=" + mensagem + ", tipoContato=" + tipoContato + ", origemSolicitacao=" + origemSolicitacao + ", prazo=" + prazo + ", idCtr=" + idCtrSapiens + "]";
	}
	
	

}
