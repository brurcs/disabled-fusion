package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.velocity.VelocityContext;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.VelocityUtils;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class HistoricoItemConverter extends EntityConverter
{
	BigDecimal total = new BigDecimal(0);
	
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		EntityWrapper wForm = new EntityWrapper(field.getForm().getObject());
		Long codEmp = wForm.findGenericValue("codEmpresa");
		Long codOrdemCompra = wForm.findGenericValue("codOrdemCompra");
		String codDer = wForm.findGenericValue("codDer");
		
		VelocityContext context = new VelocityContext();
		context.put("historicos", getHistoricoItem((String) wForm.findGenericValue("codProduto"), codEmp, codOrdemCompra, codDer));
		context.put("totalItens", parseMonetario(total));
		
		return VelocityUtils.runTemplate("custom/orsegups/FAPHistoricoItem.vm", context);
	}
	
	@Override
	protected String getHTMLView(EFormField arg0, OriginEnum arg1)
	{
		return getHTMLInput(arg0, arg1);
	}
	
	private String getHistoricoItem(String codProduto, Long codEmp, Long codOrdemCompra, String codDer)
	{
		String trHistoricoItem = "" +
				"<tr id=\"0\" uid=\"list_historicoItem__\" entitytype=\"FAPDiversosItensCompra\" idx=\"0\" style=\"visibility: visible;\">\r\n" +
				"	<td>${ordemCompra}</td>\r\n" +
				"	<td>${fornecedor}</td>\r\n" + 
				"	<td>${descricao}</td>\r\n" + 
				"	<td>${qtd}</td>\r\n" + 
				"	<td>${valorUnitario}</td>\r\n" + 
				"	<td>${valorTotal}</td>\r\n" + 
				"	<td>${entrega}</td>\r\n" + 
				"</tr>";
		
		StringBuilder historicoItem = new StringBuilder();
		
		total = new BigDecimal(0);
		Long count = 0l;
		
		List<Long> listaOrdensLidas = new ArrayList<>();
		QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("codpro", codProduto), new QLEqualsFilter("codemp", codEmp), new QLRawFilter("numocp <> " + codOrdemCompra));
		if (codDer != null && !codDer.trim().isEmpty())
		{
			qlGroup.addFilter(new QLEqualsFilter("codder", codDer.trim()));
		}
		
		List<NeoObject> listItensCompra = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE420IPO"), qlGroup, "numocp desc");
		for (NeoObject noItemCompra : listItensCompra)
		{
			if (count >= 20)
				break;
			
			EntityWrapper wItemCompra = new EntityWrapper(noItemCompra);
			
			BigDecimal valorBruto = wItemCompra.findGenericValue("vlrbru");
			if (valorBruto != null)
				total = total.add(valorBruto);

			Long codOrdemCompraItem = wItemCompra.findGenericValue("numocp");
			Boolean existe = checkExists(listaOrdensLidas, codOrdemCompraItem);
			if (existe) {
				continue;
			}
			else {
				listaOrdensLidas.add(codOrdemCompraItem);
			}
			
			Map<String, String> parametrosItem = new HashMap<String, String>();
			parametrosItem = agruparItensOrdemCompra(codOrdemCompraItem, codProduto, codEmp);
			
			StrSubstitutor sub = new StrSubstitutor(parametrosItem);
			historicoItem.append(sub.replace(trHistoricoItem));
			
			count++;
		}		
		
		return historicoItem.toString();
	}
	
	private Map<String, String> agruparItensOrdemCompra(Long codOrdemCompra, String codProduto, Long codEmp)
	{		
		Map<String, String> parametrosItem = new HashMap<String, String>();
		
		BigDecimal valorUnitario = new BigDecimal(0);
		BigDecimal totalBruto = new BigDecimal(0);
		BigDecimal quantidade = new BigDecimal(0);
		String descricao = "";
		String entrega = "";
		
		QLGroupFilter qlGroup = new QLGroupFilter("AND");
		qlGroup.addFilter(new QLEqualsFilter("codpro", codProduto));
		qlGroup.addFilter(new QLEqualsFilter("numocp", codOrdemCompra));
		qlGroup.addFilter(new QLEqualsFilter("codemp", codEmp));
		List<NeoObject> listItensCompra = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE420IPO"), qlGroup, "numocp desc");
		for (NeoObject noItemCompra : listItensCompra)
		{
			EntityWrapper wItemCompra = new EntityWrapper(noItemCompra);
			
			BigDecimal valorBruto = wItemCompra.findGenericValue("vlrbru");
			if (valorBruto != null)
				totalBruto = totalBruto.add(valorBruto);
			
			BigDecimal qtd = wItemCompra.findGenericValue("qtdped");
			if (qtd != null)
				quantidade = quantidade.add(qtd);

			descricao = wItemCompra.findGenericValue("cplipo");
			valorUnitario = wItemCompra.findGenericValue("preuni");
			entrega = NeoDateUtils.safeDateFormat((GregorianCalendar)wItemCompra.findGenericValue("datent"), "dd/MM/yyyy");
		}
		
		QLGroupFilter qlGroupOC = new QLGroupFilter("AND", new QLEqualsFilter("numocp", codOrdemCompra), new QLEqualsFilter("codemp", codEmp));
		NeoObject noOrdemCompra = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE420OCP"), qlGroupOC);
		EntityWrapper wOrdemCompra = new EntityWrapper(noOrdemCompra);
		
		Long codFornecedor = wOrdemCompra.findGenericValue("codfor");
		
		NeoObject noFornecedor = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("ETABFOR"), new QLEqualsFilter("codfor", codFornecedor));
		String nomeFornecedor = new EntityWrapper(noFornecedor).findGenericValue("nomfor");
		String apelidoFornecedor = new EntityWrapper(noFornecedor).findGenericValue("apefor");
		
		parametrosItem.put("fornecedor", nomeFornecedor);		
		parametrosItem.put("ordemCompra", codOrdemCompra.toString());
		parametrosItem.put("descricao", descricao);
		parametrosItem.put("qtd", quantidade.toString());
		parametrosItem.put("valorUnitario", parseMonetario(valorUnitario));
		parametrosItem.put("valorTotal", parseMonetario(totalBruto));
		parametrosItem.put("entrega", entrega);
		
		return parametrosItem;
	}
	
	private Boolean checkExists(List<Long> listOrdensCadastradas, Long codOrdemCompra)
	{
		for (Long ordemCompra : listOrdensCadastradas)
		{
			if (ordemCompra.equals(codOrdemCompra))
				return true;
		}
		
		return false;
	}
	
	private String parseMonetario(BigDecimal value)
	{
		NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
		
		if (value != null)
		{
			BigDecimal returnValue = value;
			returnValue = returnValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			return monetaryFormat.format(returnValue);
		}
		
		return "R$ 0,00";
	}
}
