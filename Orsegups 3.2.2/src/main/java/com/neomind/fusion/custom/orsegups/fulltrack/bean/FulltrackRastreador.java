package com.neomind.fusion.custom.orsegups.fulltrack.bean;

public class FulltrackRastreador {
    
    private int ras_ras_id;
    private String ras_ras_id_aparelho;
    private int ras_ras_status;
    private String ras_ras_prd_id;
    private String ras_ras_cli_id;
    private String ras_ras_chip;
    private String ras_ras_linha;
    private String ras_ras_data_ult_comunicacao;
    private String ras_ras_data_alterado;
    
    public int getRas_ras_id() {
        return ras_ras_id;
    }
    public void setRas_ras_id(int ras_ras_id) {
        this.ras_ras_id = ras_ras_id;
    }
    public String getRas_ras_id_aparelho() {
        return ras_ras_id_aparelho;
    }
    public void setRas_ras_id_aparelho(String ras_ras_id_aparelho) {
        this.ras_ras_id_aparelho = ras_ras_id_aparelho;
    }
    public int getRas_ras_status() {
        return ras_ras_status;
    }
    public void setRas_ras_status(int ras_ras_status) {
        this.ras_ras_status = ras_ras_status;
    }
    public String getRas_ras_prd_id() {
        return ras_ras_prd_id;
    }
    public void setRas_ras_prd_id(String ras_ras_prd_id) {
        this.ras_ras_prd_id = ras_ras_prd_id;
    }
    public String getRas_ras_cli_id() {
        return ras_ras_cli_id;
    }
    public void setRas_ras_cli_id(String ras_ras_cli_id) {
        this.ras_ras_cli_id = ras_ras_cli_id;
    }
    public String getRas_ras_chip() {
        return ras_ras_chip;
    }
    public void setRas_ras_chip(String ras_ras_chip) {
        this.ras_ras_chip = ras_ras_chip;
    }
    public String getRas_ras_linha() {
        return ras_ras_linha;
    }
    public void setRas_ras_linha(String ras_ras_linha) {
        this.ras_ras_linha = ras_ras_linha;
    }
    public String getRas_ras_data_ult_comunicacao() {
        return ras_ras_data_ult_comunicacao;
    }
    public void setRas_ras_data_ult_comunicacao(String ras_ras_data_ult_comunicacao) {
        this.ras_ras_data_ult_comunicacao = ras_ras_data_ult_comunicacao;
    }
    public String getRas_ras_data_alterado() {
        return ras_ras_data_alterado;
    }
    public void setRas_ras_data_alterado(String ras_ras_data_alterado) {
        this.ras_ras_data_alterado = ras_ras_data_alterado;
    }

    
    
}
