package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class ChipGPRSAtualizaPosto implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			String nomeFonteDados = "SAPIENS";
	
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			
			Long codEmp = (Long) processEntity.findValue("postoSapiens.usu_codemp");
			Long codFil = (Long) processEntity.findValue("postoSapiens.usu_codfil");
			Long numCtr = (Long) processEntity.findValue("postoSapiens.usu_numctr");
			Long numPos = (Long) processEntity.findValue("postoSapiens.usu_numpos");
			String codSer = (String) processEntity.findValue("postoSapiens.usu_codser");
			
			Collection<NeoObject> chips = processEntity.findField("chipsGPRS").getValues();
			for (NeoObject chip : chips)
			{
				EntityWrapper wrapperLista = new EntityWrapper(chip);
				String numTel = (String) wrapperLista.findField("numeroTelefone").getValue();
				String iccID = (String) wrapperLista.findField("iccID").getValue();
				NeoObject planoDados = (NeoObject) wrapperLista.findValue("planoDados");
				EntityWrapper planoWrapper = new EntityWrapper(planoDados);
				NeoObject operadoraPlano = (NeoObject) planoWrapper.findValue("operadora");
				EntityWrapper opeWrapper = new EntityWrapper(operadoraPlano);
				String nomeOperadora = (String) opeWrapper.findField("nome").getValue();
				
				StringBuffer sqlSelect = new StringBuffer();
				sqlSelect.append(" SELECT * FROM USU_T160CHP ");
				sqlSelect.append(" WHERE usu_codemp = ? AND usu_codfil = ? AND usu_numctr = ? AND usu_numpos = ? AND usu_iccid = ? AND usu_codser = ? ");
				
				PreparedStatement stSelect = connection.prepareStatement(sqlSelect.toString());
				stSelect.setLong(1, codEmp);
				stSelect.setLong(2, codFil);
				stSelect.setLong(3, numCtr);
				stSelect.setLong(4, numPos);
				stSelect.setString(5, iccID);
				stSelect.setString(6, codSer);
				
				ResultSet rs = stSelect.executeQuery();
				
				if(!rs.next()) {				
					StringBuffer sql = new StringBuffer();
					sql.append(" INSERT INTO USU_T160CHP ");
					sql.append(" (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_numtel,usu_iccid,usu_codser,usu_codope) ");
					sql.append(" VALUES ");
					sql.append(" (?,?,?,?,?,?,?,?) ");
				
					PreparedStatement st = connection.prepareStatement(sql.toString());
					st.setLong(1, codEmp);
					st.setLong(2, codFil);
					st.setLong(3, numCtr);
					st.setLong(4, numPos);
					st.setString(5, numTel);
					st.setString(6, iccID);
					st.setString(7, codSer);
					st.setString(8, nomeOperadora);
					
					st.executeUpdate();
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
