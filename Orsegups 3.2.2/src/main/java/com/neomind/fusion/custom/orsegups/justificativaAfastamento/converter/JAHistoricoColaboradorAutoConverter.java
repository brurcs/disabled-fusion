package com.neomind.fusion.custom.orsegups.justificativaAfastamento.converter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.util.JAUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.custom.orsegups.justificativaAfastamento.converter-HistoricoColaboradorAutoConverter
public class JAHistoricoColaboradorAutoConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{	
	    
		Long idPai = field.getForm().getObjectId();
		EntityWrapper wrapper = JAUtils.getEformPai(idPai);

		StringBuilder historico = new StringBuilder();
		String numCpf = "";
		NeoObject colaborador = (NeoObject) wrapper.findField("colaborador").getValue();
		historico.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
		historico.append("			<tr style=\"cursor: auto\">");
		historico.append("				<th style=\"cursor: auto\">Data do afastamento</th>");
		historico.append("				<th style=\"cursor: auto\">Data fim do afastamento</th>");
		historico.append("				<th style=\"cursor: auto\">Situação</th>");
		historico.append("			</tr>");
		historico.append("			<tbody>");
		if (colaborador != null)
		{
			EntityWrapper wObjCol = new EntityWrapper(colaborador);
			numCpf = String.valueOf(wObjCol.getValue("numcpf"));

			Connection connVetorh = PersistEngine.getConnection("VETORH");
			ResultSet rsColaborador = null;
			PreparedStatement stColaborador = null;
			StringBuffer queryColaborador = new StringBuffer();
			try
			{
				queryColaborador = new StringBuffer();
				queryColaborador.append(" select a.DatAfa,a.DatTer,a.SitAfa,sit.DesSit from r038afa a  ");
				queryColaborador.append(" inner join r034fun fun on fun.numcad = a.NumCad and fun.numemp = a.NumEmp ");
				queryColaborador.append(" inner join R010SIT sit on sit.CodSit = a.SitAfa ");
				queryColaborador.append(" where fun.numcpf = ? and fun.sitAfa <> 7 order by a.datAfa");

				ResultSet rsAfastamento = null;
				PreparedStatement stAfastamento = null;
				stAfastamento = connVetorh.prepareStatement(queryColaborador.toString());
				stAfastamento.setString(1, numCpf);
				rsAfastamento = stAfastamento.executeQuery();

				while (rsAfastamento.next())
				{
					historico.append("		<tr>");
					GregorianCalendar datAfa = new GregorianCalendar();
					GregorianCalendar datTer = new GregorianCalendar();
					datAfa.setTime(rsAfastamento.getDate("DatAfa"));
					datTer.setTime(rsAfastamento.getDate("DatTer"));

					historico.append("<td>" + NeoDateUtils.safeDateFormat(datAfa, "dd/MM/yyyy") + "</td>");
					historico.append("<td>" + NeoDateUtils.safeDateFormat(datTer, "dd/MM/yyyy") + "</td>");
					historico.append("<td>" + rsAfastamento.getLong("SitAfa") + " - " + rsAfastamento.getString("DesSit") + "</td>");
					historico.append("		</tr>");
				}

				historico.append("		</tbody>");
				historico.append("		</table>");

			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
			finally
			{
				OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
			}
		}
		return historico.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}

}
