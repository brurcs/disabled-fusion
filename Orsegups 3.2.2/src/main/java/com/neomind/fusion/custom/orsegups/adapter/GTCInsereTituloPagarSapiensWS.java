package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

import br.com.senior.services.gtc.G5SeniorServicesLocator;
import br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPIn;
import br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPInEntradaTitulos;
import br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPInEntradaTitulosRateio;
import br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPOut;
import br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPOutGridResult;

public class GTCInsereTituloPagarSapiensWS implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		
		Boolean efetuarIntegracao = processEntity.findGenericValue("integrarSapiens");
		if (efetuarIntegracao != null && !efetuarIntegracao)
			return;
		
		try	{
			
			GregorianCalendar dataentrada = (GregorianCalendar) processEntity.findValue("dataEntrada");
			GregorianCalendar dataemissao = (GregorianCalendar) processEntity.findValue("dataEmissao");
			if (dataentrada.before(dataemissao))
				throw new WorkflowException("Data de Entrada deve ser maior ou igual a Data de Emissão!");

			String nomeUsuario = "";
			String senhaUsuario = "";

			QLEqualsFilter filtroLogin = new QLEqualsFilter("nomeUsuario", PortalUtil.getCurrentUser().getCode().concat(".sid"));
			List<NeoObject> listLogin = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCLoginSapiens"), filtroLogin);
			if (listLogin != null && !listLogin.isEmpty())	{
				EntityWrapper wrapperLogin = new EntityWrapper(listLogin.get(0));
				nomeUsuario = wrapperLogin.findGenericValue("nomeUsuario");
				String nomeUsuarioAux = nomeUsuario.substring(0, nomeUsuario.length() - 4);

				if (nomeUsuarioAux.equals(PortalUtil.getCurrentUser().getCode()))
					senhaUsuario = wrapperLogin.findGenericValue("senhaUsuario");
				else
					throw new WorkflowException("Por favor, acesse o Fusion com um Usuário correspondente no Sapiens!");
			}
			else
				throw new WorkflowException("Não encontrado usuário para lançar o Título no Sapiens!");

			GregorianCalendar vencimentooriginal = (GregorianCalendar) processEntity.findValue("vencimentoOriginal");

			if (!OrsegupsUtils.isWorkDay(vencimentooriginal)) {
				throw new WorkflowException("Data de vencimento do Titulo não é um dia útil!");
			}
			
			TitulosEntradaTitulosLoteCPIn entradaTitulos = preencherEntradaTitulos(processEntity);

			G5SeniorServicesLocator locator = new G5SeniorServicesLocator();
			TitulosEntradaTitulosLoteCPOut entradaTitulosOut = locator.getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort().entradaTitulosLoteCP(nomeUsuario.trim(), senhaUsuario.trim(), 0, entradaTitulos);
			
			if (entradaTitulosOut != null && (entradaTitulosOut.getErroExecucao() != null || entradaTitulosOut.getResultado().contains("ERRO")))
			{
				System.out.println("GTCInsereTituloPagarSapiensSOAP - Retorno Integração. Erro: " + entradaTitulosOut.getErroExecucao() + " - Resultado: " + entradaTitulosOut.getResultado());
				TitulosEntradaTitulosLoteCPOutGridResult[] gridResults = entradaTitulosOut.getGridResult();
				if (gridResults != null && gridResults.length > 0)	{
					for (int i = 0; i < gridResults.length; i++) {
						TitulosEntradaTitulosLoteCPOutGridResult gridResult = gridResults[i];					
						throw new WorkflowException("Não foi possível realizar a integração. Motivo: " + gridResult.getTxtRet());
					}
				}
				
				throw new WorkflowException("Ocorreu um problema na integração do título com o Sapiens. Erro inesperado.");
			} else if (entradaTitulosOut != null && entradaTitulosOut.getResultado().contains("OK")) {
				TitulosEntradaTitulosLoteCPOutGridResult gridResult = entradaTitulosOut.getGridResult(0);						
				
				try	{									
					GregorianCalendar compBI = (GregorianCalendar) processEntity.findField("competenciaBI").getValue();
					Long contaFinanceira = processEntity.findGenericValue("contaFinanceira.ctafin");
					Long contaContabil = processEntity.findGenericValue("contaContabil.ctared");
					Long codResponsavel = processEntity.findGenericValue("usuarioResponsavel.codusu");
					String obsSlip = processEntity.findGenericValue("obsSlip");
					
					Map<String, Object> map = new HashMap<>();
					map.put("compBI", compBI);
					map.put("contaFinanceira", contaFinanceira);
					map.put("contaContabil", contaContabil);
					map.put("codResponsavel", codResponsavel);
					map.put("obsSlip", obsSlip);
					
					preencheDemaisDadosTitulo(gridResult, map);
				} catch (NullPointerException e) {
					e.printStackTrace();
					throw new WorkflowException("Titulo lançado porém não foi possível incluir a competência do BI no mesmo pois o campo não está preenchido!");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException(e.getErrorList().get(0).getI18nMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Não foi possível realizar a integração. Erro inesperado.");
		}
	}

	private int preencheDemaisDadosTitulo(TitulosEntradaTitulosLoteCPOutGridResult tituloInserido, Map<String, Object> map) {
		Integer codEmp = tituloInserido.getCodEmp();
		Integer codFil = tituloInserido.getCodFil();
		Integer codFor = tituloInserido.getCodFor();
		String codTpt = tituloInserido.getCodTpt();
		String numTit = tituloInserido.getNumTit();	
		
		GregorianCalendar competenciaBI = (GregorianCalendar) map.get("compBI");
		Long ctaRed = (Long) map.get("contaContabil");
		Long ctaFin = (Long) map.get("contaFinanceira");
		Long usuarioResponsavel = (Long) map.get("codResponsavel");
		String obsSlip = (String) map.get("obsSlip");
		String tipoPagamento = obsSlip == null ? "" : obsSlip.contains("Contrato") ? "Contrato" : "Avulso";
		
		Connection conn= null;
		PreparedStatement pstm = null;
		StringBuilder sql = new StringBuilder();

		try	{
			conn = PersistEngine.getConnection("SAPIENS");

			sql.append("UPDATE E501TCP SET USU_DATENT = ?, CTARED = ?, CTAFIN = ?, usu_usures = ?,  USU_OBSSLIP = ?, USU_TipPag = ? WHERE CODEMP = ? AND CODFIL = ? AND CODFOR = ? AND CODTPT = ? AND NUMTIT = ?");

			pstm = conn.prepareStatement(sql.toString());
			Date date = new Date(competenciaBI.getTimeInMillis());
			
			pstm.setDate(1, date);
			pstm.setLong(2, ctaRed);
			pstm.setLong(3, ctaFin);
			pstm.setLong(4, usuarioResponsavel);
			pstm.setString(5, obsSlip);
			pstm.setString(6, tipoPagamento);
			pstm.setInt(7, codEmp);
			pstm.setInt(8, codFil);
			pstm.setInt(9, codFor);
			pstm.setString(10, codTpt);
			pstm.setString(11, numTit);
			
			
			int updateResult = pstm.executeUpdate();

			if(updateResult == 0 ) {
				throw new WorkflowException("");			
			}

		} catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException("Título lançado porém não foi possível ajustar alguns dados do mesmo. Motivo: " + e.getErrorList().get(0).getI18nMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Título lançado porém não foi possível ajustar alguns dados do mesmo. Motivo: " + e.getMessage());
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}

		return 0;
	}

	private TitulosEntradaTitulosLoteCPIn preencherEntradaTitulos(EntityWrapper wrapper) {
		Long codEmpresa = wrapper.findGenericValue("empresa.codemp");
		Long codFilial = wrapper.findGenericValue("codigoFilial.codfil");
		Long formaPgto = wrapper.findGenericValue("formaPagamento.codfpg");
		Long codFornecedor = wrapper.findGenericValue("fornecedor.codfor");
		Long contaFinanceira = wrapper.findGenericValue("contaFinanceira.ctafin");
		Long contaContabil = wrapper.findGenericValue("contaContabil.ctared");
		Long codNaturezaGasto = wrapper.findGenericValue("naturezaGasto.codntg");

		BigDecimal vlrOriginal = (BigDecimal) wrapper.findGenericValue("valorOriginal");
		GregorianCalendar dtEntrada = wrapper.findGenericValue("dataEntrada");
		GregorianCalendar dtEmissao = wrapper.findGenericValue("dataEmissao");
		GregorianCalendar venctOriginal = wrapper.findGenericValue("vencimentoOriginal");
		wrapper.setValue("vencimentoProrrogado", venctOriginal);
		wrapper.setValue("dataProvavelPagamento", venctOriginal);
		List<NeoObject> listaCentroCustos = wrapper.findGenericValue("listaCentroCusto");
		
		String titulo = wrapper.findGenericValue("titulo");
		String tipoTitulo = wrapper.findGenericValue("tipoTitulo.codtpt");
		String codMoeda = wrapper.findGenericValue("moeda.codmoe");
		String codPortador = wrapper.findGenericValue("portador.codpor");
		String codCarteira = wrapper.findGenericValue("carteira.codcrt");
		String observacao = wrapper.findGenericValue("observacao");
		String transacao = wrapper.findGenericValue("transacao.codtns");
		String codBanco = null;
		String codAgencia = null;
		String contaBanco = null;
		
		if (formaPgto != null && formaPgto == 2L) {
			codBanco = wrapper.findGenericValue("codigoBanco");
			codAgencia = wrapper.findGenericValue("codigoAgencia");
			contaBanco = wrapper.findGenericValue("contaBanco");
		}
				
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dtEmissaoStr = dtEmissao == null ? null : formatter.format(dtEmissao.getTime());
		String dtEntradaStr = dtEntrada == null ? null : formatter.format(dtEntrada.getTime());
		String venctOriginalStr = venctOriginal == null ? null : formatter.format(venctOriginal.getTime());
		String venctProrrogadoStr = venctOriginalStr == null ? null : formatter.format(venctOriginal.getTime());
		String codCCU = wrapper.findGenericValue("centroCusto.codccu");
		
		TitulosEntradaTitulosLoteCPIn tituloEntrada = new TitulosEntradaTitulosLoteCPIn();
		TitulosEntradaTitulosLoteCPInEntradaTitulos entrada = new TitulosEntradaTitulosLoteCPInEntradaTitulos();

		tituloEntrada.setCodEmp(codEmpresa == null ? null : codEmpresa.intValue());
		tituloEntrada.setCodFil(codFilial == null ? null : codFilial.intValue());

		entrada.setCodFil(codFilial == null ? null : codFilial.intValue());
		entrada.setNumTit(titulo);
		entrada.setCodTpt(tipoTitulo);
		entrada.setCodTns(transacao == null ? null : transacao);
		entrada.setCodFor(codFornecedor == null ? null : codFornecedor.intValue());
		entrada.setObsTcp(observacao);
		entrada.setCodNtg(codNaturezaGasto == null ? null : codNaturezaGasto.intValue());
		entrada.setNumPrj(0);
		entrada.setCodFpj(0);
				
		entrada.setDatEmi(dtEmissaoStr);
		entrada.setDatEnt(dtEntradaStr);
		entrada.setVctOri(venctOriginalStr);
		entrada.setVlrOri(vlrOriginal == null ? null : vlrOriginal.doubleValue());
		entrada.setCodMoe(codMoeda);
		entrada.setVctPro(venctProrrogadoStr);
		entrada.setCodFpg(formaPgto == null ? null : formaPgto.intValue());
		entrada.setCodPor(codPortador);
		entrada.setCodCrt(codCarteira);
		entrada.setCodBan(codBanco);
		entrada.setCodAge(codAgencia);
		entrada.setCcbFor(contaBanco);
		entrada.setProJrs("S");
		entrada.setDatPpt(venctOriginalStr);
		entrada.setTitBan(null);
		entrada.setPriPgt(null);
		entrada.setPerMul(null);
		entrada.setTolMul(null);
		entrada.setJrsDia(null);
		entrada.setPerJrs(null);
		entrada.setTipJrs(null);
		entrada.setTolJrs(null);
		entrada.setDatNeg(null);
		entrada.setJrsNeg(null);
		entrada.setMulNeg(null);
		entrada.setDscNeg(null);
		entrada.setOutNeg(null);
		entrada.setCotNeg(null);
		entrada.setAntDsc(null);
		entrada.setDatDsc(null);
		entrada.setTolDsc(null);
		entrada.setPerDsc(null);
		entrada.setVlrDsc(null);
		entrada.setTipTcc(null);
		entrada.setCodCrp(null);
		entrada.setCodFav(null);
		entrada.setCodMpt(null);

		/* Ler a lista de centros de custos informadas no EFormDinâmico */		
		if (listaCentroCustos != null && !listaCentroCustos.isEmpty()) {
			Integer seq = 0;
			TitulosEntradaTitulosLoteCPInEntradaTitulosRateio[] rateioArray = new TitulosEntradaTitulosLoteCPInEntradaTitulosRateio[listaCentroCustos.size()];
			entrada.setRateio(rateioArray);
			for (NeoObject noCC : listaCentroCustos){
				TitulosEntradaTitulosLoteCPInEntradaTitulosRateio rateio = preencherRateio(noCC);
				if (rateio != null)	{
					rateio.setCtaFin(contaFinanceira == null ? null : contaFinanceira.intValue());
					rateio.setCtaRed(contaContabil == null ? null : contaContabil.intValue());
					rateio.setVlrCta(vlrOriginal == null ? null : vlrOriginal.doubleValue());

					entrada.setRateio(seq, rateio);
					seq++;
				}
			}
		} else	{			
			if (codCCU != null)	{
				TitulosEntradaTitulosLoteCPInEntradaTitulosRateio rateio = preencherRateio(codCCU, vlrOriginal.doubleValue());
				rateio.setCtaFin(contaFinanceira == null ? null : contaFinanceira.intValue());
				rateio.setCtaRed(contaContabil == null ? null : contaContabil.intValue());
				rateio.setVlrCta(vlrOriginal == null ? null : vlrOriginal.doubleValue());

				TitulosEntradaTitulosLoteCPInEntradaTitulosRateio[] rateioArray = { rateio };
				entrada.setRateio(rateioArray);
			} else {
				throw new WorkflowException("Não encontrado o Código do Centro de Custo para lançamento do Título");
			}
		}

		TitulosEntradaTitulosLoteCPInEntradaTitulos[] tituloEntradaArray = { entrada };
		tituloEntrada.setEntradaTitulos(tituloEntradaArray);

		return tituloEntrada;
	}

	private TitulosEntradaTitulosLoteCPInEntradaTitulosRateio preencherRateio(NeoObject noCC) {
		TitulosEntradaTitulosLoteCPInEntradaTitulosRateio rateio = new TitulosEntradaTitulosLoteCPInEntradaTitulosRateio();
		rateio.setNumPrj(0);
		rateio.setCodFpj(0);
		rateio.setPerCta(0d);
		rateio.setPerRat(0d);
		rateio.setObsRat("");

		EntityWrapper wCC = new EntityWrapper(noCC);
		BigDecimal valorRateio = wCC.findGenericValue("valorRateio");

		/*
		 * Ler o código do Centro de Custo informado no EForm Externo (E044CCU)
		 */
		NeoObject noCCUExt = wCC.findGenericValue("codigoCentroCusto");
		EntityWrapper wCUExt = new EntityWrapper(noCCUExt);
		String codCC = wCUExt.findGenericValue("codccu");

		if (codCC != null && valorRateio != null)
		{
			rateio.setVlrRat(valorRateio == null ? null : valorRateio.doubleValue());
			rateio.setCodCcu(codCC);

			return rateio;
		}

		return null;
	}
	
	private TitulosEntradaTitulosLoteCPInEntradaTitulosRateio preencherRateio(String codCC, Double valor) {
		TitulosEntradaTitulosLoteCPInEntradaTitulosRateio rateio = new TitulosEntradaTitulosLoteCPInEntradaTitulosRateio();
		rateio.setNumPrj(0);
		rateio.setCodFpj(0);
		rateio.setPerCta(0d);
		rateio.setPerRat(0d);
		rateio.setObsRat("");
		rateio.setVlrRat(valor);
		rateio.setCodCcu(codCC);

		return rateio;
	}


	@Override
	public void back(EntityWrapper processEntity, Activity activity) {	
		// NOTHING TO DO
	}
}
