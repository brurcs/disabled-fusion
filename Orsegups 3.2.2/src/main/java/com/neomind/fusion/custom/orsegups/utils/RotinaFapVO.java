package com.neomind.fusion.custom.orsegups.utils;

public class RotinaFapVO 
{
	private String razaoSocial = null;
	private String conta = null;
	private String endereco = null;
	private Long codigo = 0L;
	private Long cliente = 0L;
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getConta() {
		return conta;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public Long getCodigo(){
		return codigo;
	}
	public void setCodigo(Long codigo){
		this.codigo = codigo;
	}
	public Long getCliente() {
		return cliente;
	}
	public void setCliente(Long cliente) {
		this.cliente = cliente;
	}

	
	
	
	
	
	
}
