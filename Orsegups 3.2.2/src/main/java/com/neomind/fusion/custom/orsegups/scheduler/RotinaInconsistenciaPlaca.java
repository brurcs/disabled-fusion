package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle.ACVehicle;
import com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle.ACVehicles;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaInconsistenciaPlaca implements CustomJobAdapter {

	
	private final Log log = LogFactory.getLog(RotinaInconsistenciaPlaca.class);

    private Long key = GregorianCalendar.getInstance().getTimeInMillis();

    private SIGMAUtilsEngine engine = new SIGMAUtilsEngine();

    /**
     * Veiculos com falha de comunicação de pelo 12 horas na data de hoje.
     * <tt>Rastreador</tt> é a chave do mapa
     */
    private Map<String, ACVehicle> miscommunication = new HashMap<String, ACVehicle>();
	
    /**
     * Número total de páginas recebidos da API de consulta de falhas de eventos
     */
    private int totalPageNumber = 1;
    
    /**
     * Inconsistencias na inserção de eventos
     */
    private List<String> inconsistencias = new ArrayList<String>();
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		try {

			for (int i = 0; i < totalPageNumber; i++) {

				try {
					this.getMiscommunicationFail(i);
				} catch (RuntimeException e) {
					log.error("RotinaFalhaComunicacao - Erro ao acessar a API Autocargo ACEngineFalhaComunicacao [" + key + "] :");
					e.printStackTrace();
					throw new JobException("Erro ao acessar API de consulta de falha de comunicação Autocargo! Procurar no log por [" + key + "]");
				}

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			if (!miscommunication.isEmpty()) {
				
				for (String key : miscommunication.keySet()) {
					
					ACVehicle v = miscommunication.get(key);
					
					if(!isValidToSendMail(v)) {
						String cliente = "Cliente: "+ v.getClient_name() + "<br/>Veiculo: "+v.getBrand()+" "+v.getModel()+" "
								+ "<br/>Nº Modulo: "+v.getTracking_module().getNumber()+" <br/>Placa: " + v.getRegistration_plate();
						String inconsistencia = "ERRO_CENTRAL_NAO_ENCONTRADA";
						this.inconsistencias.add(inconsistencia+";"+cliente);
					}
				}	
				
				sendEmail();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	
    private boolean isValidToSendMail(ACVehicle v) {
		
    	StringBuilder sql = new StringBuilder();
    	Connection conn = null;
    	PreparedStatement pstm = null;
    	ResultSet rs = null;
    	
    	Boolean resultado = false;
    	
    	try {
			
			sql.append(" SELECT C.* ");
			sql.append(" FROM DBCENTRAL C ");
			sql.append(" WHERE C.NM_RASTREADOR = ? ");
			
			conn = PersistEngine.getConnection("SIGMA90");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setString(1, v.getRegistration_plate());

			rs = pstm.executeQuery();
			
			if(rs.next()) {
				resultado = true;
			}
    		
		} catch(Exception e){
			e.printStackTrace();
		}finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
    	
		return resultado;
	}


	/**
     * Popula o mapa {@link #miscommunication}
     * 
     * @param pageNumber
     *            {@link #totalPageNumber} Não é necessário modificar
     */
	private void getMiscommunicationFail(int pageNumber) {

		String retorno = null;

		try {

			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet("https://www2.autocargo.com.br/api/integration/v1/miscommunication?page=" + pageNumber + "&miscommunication_control=true&miscommunication_duration_limit=12");
			request.addHeader("Authorization", "Token token=\"115fe7a39c6d1910308c335ba3f845a0f9d6495ee912068b9cf7a90f717f03a5\"");
			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");
			HttpResponse response = client.execute(request);

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				retorno = EntityUtils.toString(entity);
			}

			String totalPorPagina = response.getFirstHeader("X-Autocargo-Per-Page").getValue();
			String totalResultados = response.getFirstHeader("X-Autocargo-Total").getValue();

			int total = Integer.parseInt(totalPorPagina);

			int result = this.roundUp(Integer.parseInt(totalResultados), total);

			this.totalPageNumber = result / total;

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException(String.valueOf(response.getStatusLine().getStatusCode()));
			}

		} catch (Exception e) {
			log.error("RotinaInconsistenciaPlaca - Falha ao buscar falhas de comunicação via API");
			e.printStackTrace();
		}
		
		if(NeoUtils.safeIsNotNull(retorno)) {
			
			Gson gson = new Gson();
			ACVehicles resultList = gson.fromJson(retorno, ACVehicles.class);
			
			for (ACVehicle v : resultList.getVehicles()) {
				miscommunication.put(v.getRegistration_plate(), v);
			}
			
		}
	}
	
    private int roundUp(int toRound, int roundTo) {

	if (toRound % roundTo == 0) {
	    return toRound;
	} else {
	    return (roundTo - toRound % roundTo) + toRound;
	}

    }
	
    private void sendEmail() {

	if (!this.inconsistencias.isEmpty()) {
	    
	    StringBuilder dados = new StringBuilder();

	    int cont = 0;

	    for (String i : this.inconsistencias) {

		String a[] = i.split(";");

		int resto = (cont) % 2;
		if (resto == 0) {
		    dados.append("<tr style=\"background-color: #d6e9f9\" ><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
		} else {
		    dados.append("<tr><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
		}

		cont++;
	    }

	    StringBuilder corpoEmail = new StringBuilder();

	    corpoEmail.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
	    corpoEmail.append("<html>");
	    corpoEmail.append("<head>");
	    corpoEmail.append("<meta charset=\"ISO-8859-1\">");
	    corpoEmail.append("<title>Relatório de inconsistências</title>");
	    corpoEmail.append("<style>");
	    corpoEmail.append("h2{");
	    corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-p{");
	    corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info{");
	    corpoEmail.append("table-layout: fixed;");
	    corpoEmail.append("text-align:left;");
	    corpoEmail.append("font-family: Verdana;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-info td{");
	    corpoEmail.append("overflow-x: hidden;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info thead{");
	    corpoEmail.append("background-color:#303090;");
	    corpoEmail.append("color: white;");
	    corpoEmail.append("font-weight:bold;");
	    corpoEmail.append("	}");
	    corpoEmail.append("</style>");
	    corpoEmail.append("</head>");
	    corpoEmail.append("<body>");
	    corpoEmail.append("<div>");
	    corpoEmail.append("<table width=\"600\" align=\"center\">");
	    corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<td><h2>Inconsistências de falha de comunicação</h2></td>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>As seguintes inconsistências ocorreram durante a inserção de eventos de falha de comunicação:</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
	    corpoEmail.append("<thead>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td>Inconsistência</td>");
	    corpoEmail.append("<td>Cliente</td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</thead>");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<!-- Recebe dados do método -->");
	    corpoEmail.append(dados);
	    corpoEmail.append("<br>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<br>");
	    corpoEmail.append("<!-- Rodapé do e-mail -->");
	    corpoEmail.append("<div class=\"rodape\">");
	    corpoEmail.append("<table width=\"600\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td width=\"360\" valign=\"bottom\"><img src=\"https://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-1.jpg\"></td>");
	    corpoEmail
		    .append("<td><a href=\"http://www.orsegups.com.br\" target=\"_blank\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-2.jpg\" width=\"260\" height=\"120\" alt=\"\"/></a></td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</body>");
	    corpoEmail.append("</html>");

	    String[] addTo = new String[3]; 
	    addTo[0] =  "emailautomatico@orsegups.com.br" ;
	    addTo[1] = "juliana.campos@orsegups.com.br";
	    addTo[2] = "caroline.wronski@orsegups.com.br";
	    String[] comCopia = new String[] { "emailautomatico@orsegups.com.br" };
	    String from = "fusion@orsegups.com.br";
	    String subject = "Relatório de Inconsistência de inserção de eventos de falha de comunicação";
	    String html = corpoEmail.toString();

	    OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);
	}

    }
	
}
