package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.AdapterValidarCamposObrigatorios

public class AdapterValidarCamposObrigatorios implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<NeoObject> ObjPedidos = null;
		boolean defApr = (boolean) processEntity.getValue("defApr");
		if (defApr)
		{
			if ((String.valueOf(processEntity.getValue("grauProcesso")).contains("1º")))
			{
				ObjPedidos = (List<NeoObject>) processEntity.getValue("lisPed");
			}
			else if ((String.valueOf(processEntity.getValue("grauProcesso")).contains("2º")))
			{
				ObjPedidos = (List<NeoObject>) processEntity.getValue("lisPedSegGrau");
			}
			else if ((String.valueOf(processEntity.getValue("grauProcesso")).contains("3º")))
			{
				ObjPedidos = (List<NeoObject>) processEntity.getValue("lisPedTerGrau");
			}

			String msg = "";
			if (ObjPedidos != null && ObjPedidos.size() > 0)
			{
				for (NeoObject pedido : ObjPedidos)
				{
					EntityWrapper wPedido = new EntityWrapper(pedido);
					if (wPedido.getValue("j002ClaPed") == null || ((wPedido.getValue("j002ClaPed"))).equals(""))
					{
						if (!msg.equals(""))
						{
							msg = msg + "<br>";
						}
						msg = msg + "Campo Risco é Obrigatório";
					}

					if (!msg.equals(""))
					{
						throw new WorkflowException(msg);
					}
				}
			}
		}
	    }catch(Exception e){
		
		System.out.println("Erro na classe AdapterValidarCamposObrigatorios do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
