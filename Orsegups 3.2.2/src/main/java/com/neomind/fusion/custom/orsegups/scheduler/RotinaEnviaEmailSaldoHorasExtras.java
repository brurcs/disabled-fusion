package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.SaldoHorasVO;
import com.neomind.fusion.custom.orsegups.site.SiteUtils;
import com.neomind.fusion.custom.orsegups.site.vo.HorasVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;


//com.neomind.fusion.custom.orsegups.scheduler.RotinaEnviaEmailSaldoHorasExtras
public class RotinaEnviaEmailSaldoHorasExtras implements CustomJobAdapter
{
	
	static Long numcad; 	 
	static Long numemp; 	 
	static Long tipcol; 	 
	static String nomfun; 	 
	static Long horas; 		 
	static Long codRegional; 
	static String lot; 	
	
	static StringBuilder strCss;

	@Override
	public void execute(CustomJobContext ctx)
	{
		try
		{
			GregorianCalendar gca =  new GregorianCalendar();
			GregorianCalendar gcb =  new GregorianCalendar();
			GregorianCalendar gcaFim =  new GregorianCalendar();
			gcb.set(GregorianCalendar.DATE, 1);
			gcb.add(GregorianCalendar.DATE, -1);
			
			GregorianCalendar dataCorte = new GregorianCalendar();
			dataCorte.set(GregorianCalendar.MONDAY,05);
			dataCorte.set(GregorianCalendar.DATE,01);
			dataCorte.set(GregorianCalendar.HOUR,0);
			dataCorte.set(GregorianCalendar.MINUTE,0);
			dataCorte.set(GregorianCalendar.SECOND,0);
			dataCorte.set(GregorianCalendar.MILLISECOND,0);
			
			strCss = new StringBuilder();
			strCss.append(" <head> <style> ");
			strCss.append(" .estilo { ");
			strCss.append(" 	margin:0px;padding:0px; ");
			strCss.append(" 	width:600px; ");
			strCss.append(" 	max-width:600px; ");
			//strCss.append(" 	box-shadow: 10px 10px 5px #888888; ");
			//strCss.append(" 	border:1px solid #000000; ");
			strCss.append(" 	 ");
			strCss.append(" 	-moz-border-radius-bottomleft:0px; ");
			strCss.append(" 	-webkit-border-bottom-left-radius:0px; ");
			strCss.append(" 	border-bottom-left-radius:0px; ");
			strCss.append(" "); 	
			strCss.append(" 	-moz-border-radius-bottomright:0px; ");
			strCss.append(" 	-webkit-border-bottom-right-radius:0px; ");
			strCss.append(" 	border-bottom-right-radius:0px; ");
			strCss.append(" "); 	
			strCss.append(" 	-moz-border-radius-topright:0px; ");
			strCss.append(" 	-webkit-border-top-right-radius:0px; ");
			strCss.append(" 	border-top-right-radius:0px; ");
			strCss.append(" "); 	
			strCss.append(" 	-moz-border-radius-topleft:0px; ");
			strCss.append(" 	-webkit-border-top-left-radius:0px; ");
			strCss.append(" 	border-top-left-radius:0px; ");
			strCss.append(" }.estilo table{ ");
			strCss.append("     border-collapse: collapse; ");
			strCss.append("         border-spacing: 0; ");
			strCss.append(" 	width:600px; ");
			strCss.append(" 	height:100%; ");
			strCss.append(" 	margin:0px;padding:0px; ");
			strCss.append(" }.estilo tr:last-child td:last-child { ");
			strCss.append(" 	-moz-border-radius-bottomright:0px; ");
			strCss.append(" 	-webkit-border-bottom-right-radius:0px; ");
			strCss.append(" 	border-bottom-right-radius:0px; ");
			strCss.append(" } ");
			strCss.append(" .estilo table tr:first-child td:first-child { ");
			strCss.append(" 	-moz-border-radius-topleft:0px; ");
			strCss.append(" 	-webkit-border-top-left-radius:0px; ");
			strCss.append(" 	border-top-left-radius:0px; ");
			strCss.append(" } ");
			strCss.append(" .estilo table tr:first-child td:last-child { ");
			strCss.append(" 	-moz-border-radius-topright:0px; ");
			strCss.append(" 	-webkit-border-top-right-radius:0px; ");
			strCss.append(" 	border-top-right-radius:0px; ");
			strCss.append(" }.estilo tr:last-child td:first-child{ ");
			strCss.append(" 	-moz-border-radius-bottomleft:0px; ");
			strCss.append(" 	-webkit-border-bottom-left-radius:0px; ");
			strCss.append(" 	border-bottom-left-radius:0px; ");
			strCss.append(" }.estilo tr:hover td{ ");
			strCss.append(" "); 	
			strCss.append(" } ");
			strCss.append(" .estilo tr:nth-child(odd){ background-color:#e5e5e5; } ");
			strCss.append(" .estilo tr:nth-child(even)    { background-color:#ffffff; }.estilo td{ ");
			strCss.append(" 	vertical-align:middle; ");
			strCss.append(" "); 	
			strCss.append(" "); 	
			strCss.append(" 	border:1px solid #000000; ");
			strCss.append(" 	border-width:0px 1px 1px 0px; ");
			strCss.append(" 	text-align:left; ");
			strCss.append(" 	padding:7px; ");
			strCss.append(" 	font-size:10px; ");
			strCss.append(" 	font-family:Arial; ");
			strCss.append(" 	font-weight:normal; ");
			strCss.append(" 	color:#000000; ");
			strCss.append(" }.estilo tr:last-child td{ ");
			strCss.append(" 	border-width:0px 1px 0px 0px; ");
			strCss.append(" }.estilo tr td:last-child{ ");
			strCss.append(" 	border-width:0px 0px 1px 0px; ");
			strCss.append(" }.estilo tr:last-child td:last-child{ ");
			strCss.append(" 	border-width:0px 0px 0px 0px; ");
			strCss.append(" } ");
			strCss.append(" .estilo tr:first-child td{ ");
			strCss.append(" 	background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cccccc), color-stop(1, #b2b2b2) ); ");
			strCss.append(" 	background:-moz-linear-gradient( center top, #cccccc 5%, #b2b2b2 100% ); ");
			strCss.append(" 	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#cccccc\", endColorstr=\"#b2b2b2\");	background: -o-linear-gradient(top,#cccccc,b2b2b2); ");
            strCss.append(" "); 
			strCss.append(" 	background-color:#cccccc; ");
			strCss.append(" 	border:0px solid #000000; ");
			strCss.append(" 	text-align:center; ");
			strCss.append(" 	border-width:0px 0px 1px 1px; ");
			strCss.append(" 	font-size:14px; ");
			strCss.append(" 	font-family:Arial; ");
			strCss.append(" 	font-weight:bold; ");
			strCss.append(" 	color:#000000; ");
			strCss.append(" } ");
			strCss.append(" .estilo tr:first-child:hover td{ ");
			strCss.append(" 	background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cccccc), color-stop(1, #b2b2b2) ); ");
			strCss.append(" 	background:-moz-linear-gradient( center top, #cccccc 5%, #b2b2b2 100% ); ");
			strCss.append(" 	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#cccccc\", endColorstr=\"#b2b2b2\");	background: -o-linear-gradient(top,#cccccc,b2b2b2); ");
            strCss.append(" "); 
			strCss.append(" 	background-color:#cccccc; ");
			strCss.append(" } ");
			strCss.append(" .estilo tr:first-child td:first-child{ ");
			strCss.append(" 	border-width:0px 0px 1px 0px; ");
			strCss.append(" } ");
			strCss.append(" .estilo tr:first-child td:last-child{ ");
			strCss.append(" 	border-width:0px 0px 1px 1px; ");
			strCss.append(" } ");
			strCss.append(" </style> </head>");
			
			GregorianCalendar finalMesAtual =  new GregorianCalendar();
			finalMesAtual.set(GregorianCalendar.DATE, finalMesAtual.getActualMaximum(GregorianCalendar.DATE));
			
			ArrayList<NeoObject> listaLocais = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("presencaLocaisHE"));
			for(NeoObject local : listaLocais){
				EntityWrapper wLocal = new EntityWrapper(local);
				String codloc = (String) wLocal.findValue("codloc");
				String nomloc = (String) wLocal.findValue("nomloc");
				NeoUser responsavel = (NeoUser) wLocal.findValue("coordenador");
				StringBuilder mailBody = new StringBuilder();
				
				//responsavel = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "danilo.silva" ));
						
				List<SaldoHorasVO> saldos = listaSaldoColaboradores(codloc);
				
				if (saldos != null && !saldos.isEmpty()){
					
					GregorianCalendar gc =  new GregorianCalendar();
					gc.getActualMaximum(GregorianCalendar.DATE);
					
					mailBody.append("<html>");
					mailBody.append(strCss);
					mailBody.append("<body>");
					//mailBody.append("<p><h2>Errata - Favor desconsiderar e-mail anterior</h2></p>");
					mailBody.append("<p>Prezado gestor,<br>"); 
					mailBody.append("Segue acompanhamento de saldo de banco de horas (Positivo ou negativo) dos vossos colaboradores.");
					mailBody.append("Conforme orientação da diretoria, favor avaliar compensação do saldo até a data final do período.");
					mailBody.append("Dúvidas referentes ao saldo, contatar o  Departamento Presença – Equipe da Sra. Fernanda Martins Scheidt.</p>");
					mailBody.append("<br><div class=\"estilo\"><table style='border: 1px solid silver;'>");
					mailBody.append("<tr><td colspan='4'><h3>Informe Periódico de Banco de Horas - "+nomloc+" <br>Competência referência: "+ NeoDateUtils.safeDateFormat(saldos.get(0).getPerfim(), "MM/yyyy") + " </h3></td></tr>");
					mailBody.append("<tr>");
					mailBody.append("<td><b>Empresa</b></td>");
					mailBody.append("<td><b>Matricula</b></td>");
					mailBody.append("<td><b>Nome</b></td>");
					mailBody.append("<td><b>Saldo à vencer em "+NeoDateUtils.safeDateFormat(finalMesAtual, "dd/MM/yyyy")+"</b></td>");
					mailBody.append("</tr>");
					for(SaldoHorasVO saldo : saldos){
							String saldoAVencer = "000:00";
							HashMap<String,Long> saldoBH = SiteUtils.saldoBancoHorasAdm(saldo.getNumcad(), saldo.getNumemp(), 1L);
							if(gca.getInstance().after(dataCorte)){
								saldoAVencer= OrsegupsUtils.getHorarioBanco(saldoBH.get("saldoAVencer")); 
							}else{
								if(saldo.getSaldoVencer() > 0 ) {
									saldoAVencer = OrsegupsUtils.getHorarioBanco(saldoBH.get("saldoAVencer")); 
								}
							}
							
							System.out.println(saldo);
							mailBody.append("<tr>");
							mailBody.append("<td>"+saldo.getNumemp()+"</td>");
							mailBody.append("<td>"+saldo.getNumcad()+"</td>");
							mailBody.append("<td>"+saldo.getNomFun()+"</td>");
							mailBody.append("<td>"+saldoAVencer+"</td>");
							mailBody.append("</tr>");
					}
					mailBody.append("</table></div></body></html>");
					System.out.println(responsavel.getEmail());
					OrsegupsUtils.sendEmail2Orsegups(responsavel.getEmail(), "nao.responda@orsegups.com.br", "Informe Periódico de Banco de Horas - " + nomloc, "", mailBody.toString());
					OrsegupsUtils.sendEmail2Orsegups("emailautomatico@orsegups.com.br", "nao.responda@orsegups.com.br", "Informe Periódico de Banco de Horas - " + nomloc, "", mailBody.toString());
				}
				
				
			}
			
		}
		catch (Exception e)
		{
			Long key = GregorianCalendar.getInstance().getTimeInMillis();
			System.out.println("["+key+"] Erro ao processa Job RotinaEnviaEmailSaldoHorasExtras  " + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processar Job Verifique no log por ["+key+"] para maiores detalhes");
		}
		
		
	}
	
	private static List<SaldoHorasVO> listaSaldoColaboradores(String codloc){
		
		List<SaldoHorasVO> retorno = new ArrayList<SaldoHorasVO>();
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		
		 sql.append(" select hie.codloc, orn.nomloc, func.numcad, func.numemp, func.tipcol, func.nomfun "); 
		 sql.append(" from r016orn orn "); 
		 sql.append(" INNER JOIN R016HIE hie on hie.numloc = orn.numloc "); 
		 sql.append(" left join r038hlo hlo on orn.numloc = hlo.numloc ");  
		 sql.append(" left join r034fun func on hlo.numemp = func.numemp and hlo.tipcol = func.tipcol and hlo.numcad = func.numcad "); 
		 sql.append(" where 1=1 "); 
		 sql.append(" and orn.usu_sitati = 'S' "); 
		 sql.append(" and hlo.datAlt = (select max(hlo1.datAlt) from r038hlo hlo1 where hlo1.numcad = func.numcad and hlo1.numemp = func.numemp) "); 
		 sql.append(" and sitafa <> 7 "); 
		 /*Removido a condição, pois os colaboradores de Curitiba foram transferidos da empresa 18 para 21 --> Procedimento realizado através da tarefa 1013172*/
		 sql.append("  and func.numemp not in(22)");
		 sql.append(" AND usu_colsde = 'S' ");
		 sql.append(" AND usu_tipadm in ( 1,2)  ");
		 sql.append(" and hie.codloc like ? ");  
		
		 SaldoHorasVO saldo = null; 
		try{
			connection = OrsegupsUtils.getConnection("VETORH");
			pst = connection.prepareStatement(sql.toString());
			pst.setString(1,codloc+"%");
			
			rs= pst.executeQuery();
			
			while (rs.next()){
				saldo = new SaldoHorasVO();
				GregorianCalendar mesref = new GregorianCalendar();
				
				GregorianCalendar mesAnterior = new GregorianCalendar();
				mesAnterior.add(GregorianCalendar.MONTH, -1);
				mesAnterior.set(GregorianCalendar.DATE, 1);
				
				  
				GregorianCalendar gca =  new GregorianCalendar();
				GregorianCalendar gcb =  new GregorianCalendar();
				GregorianCalendar gcaFim =  new GregorianCalendar();
				gcb.set(GregorianCalendar.DATE, 1);
				gcb.add(GregorianCalendar.DATE, -1);
				  
				gcaFim = (GregorianCalendar) gca.clone(); 
				gcaFim.add(GregorianCalendar.MONTH, 1);
				gcaFim.set(GregorianCalendar.DATE, 1);
				gcaFim.add(GregorianCalendar.DATE, -1);
				
				HorasVO horas = new HorasVO();
				HorasVO horasAnt = new HorasVO();
				SaldoHorasVO saldos = new SaldoHorasVO();
				  
				saldo.setNumcad(rs.getLong("numcad"));
				saldo.setNumemp(rs.getLong("numemp"));
				saldo.setTipcol(rs.getLong("tipcol"));
				
				horasAnt = SiteUtils.retornaSaldoHoras(saldo.getNumcad(), saldo.getNumemp(), 1L, gcb , 0, true );
				horas = SiteUtils.retornaSaldoHoras(saldo.getNumcad(), saldo.getNumemp(), 1L, gca , 0, false );
				saldos = SiteUtils.calculaSaldoFinal(horasAnt, horas);
				
				
				//saldo.setSaldoAnterior(horasAnt.getTotal());
				saldo = saldos;
				//saldo.setSaldoVencer(saldos.getSaldoVencer());
				saldo.setCodLoc(rs.getString("codloc"));
				saldo.setNomLoc(rs.getString("nomloc"));
				saldo.setNomFun(rs.getString("nomfun"));
				saldo.setNumcad(rs.getLong("numcad"));
				saldo.setNumemp(rs.getLong("numemp"));
				saldo.setTipcol(rs.getLong("tipcol"));
				saldo.setPerini(mesref);
				saldo.setPerfim(mesAnterior);
				
				retorno.add(saldo);
			}
			
			return retorno;
		}catch(Exception e){
			System.out.println("Erro ao listar saldo de horas dos colaboradores do local " + codloc);
			System.out.println(saldo);
			e.printStackTrace();
			return retorno;
		}finally{
			OrsegupsUtils.closeConnection(connection, pst, rs);
		}
		
	}
	
	
}