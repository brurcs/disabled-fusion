package com.neomind.fusion.custom.orsegups.maps.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.util.HSSFColor.GOLD;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServiceProxy;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertAitEngine;
import com.neomind.fusion.custom.orsegups.maps.call.vo.CallAlertAitVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoHistoricoVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.FilaAlertAitVO;
import com.neomind.fusion.custom.orsegups.maps.vo.AtendenteTecnicoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.AtualizaPosicaoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoEsperaVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.OpcaoMapaRAIAVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.OperandType;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLInFilter;
import com.neomind.fusion.persist.QLNotInFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.HttpSessionCollector;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name = "OrsegupsMapsServletAux", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServletAux" }, asyncSupported = true)
public class OrsegupsMapsServletAux extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(OrsegupsMapsServletAux.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/plain");
		response.setCharacterEncoding("ISO-8859-1");

		final PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		String action = "";
		//		request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
		//		AsyncContext asyncContext = request.startAsync();
		//		asyncContext.setTimeout(10);
		//		ServletRequest servReq = asyncContext.getRequest();
		//		boolean b = servReq.isAsyncStarted();

		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
		}
		String codigoTecnico = request.getParameter("codigoTecnico");

		Boolean atualizaPosicao = this.allowUpdateEventPosition(request);

		if (action.equalsIgnoreCase("viaturasCM"))
		{
			this.getViaturas(request, response, out, "viaturasCM", null);
		}
		else if (action.equalsIgnoreCase("eventosCM"))
		{
			this.getEventos(request, response, out, atualizaPosicao);
		}
		else if (action.equalsIgnoreCase("viaturasTecnicas"))
		{
			this.getViaturas(request, response, out, "viaturasTecnicas", codigoTecnico);
		}
		else if (action.equalsIgnoreCase("OS"))
		{
			this.getOS(request, response, out, codigoTecnico, atualizaPosicao);
		}
		else if (action.equalsIgnoreCase("atendentes"))
		{
			this.getAtendentesTecnicos(request, response, out);
		}
		else if (action.equalsIgnoreCase("opcoesRAIA"))
		{
			this.getOpcoesRAIA(request, response, out);
		}
		else if (action.equalsIgnoreCase("eventosRAIA"))
		{
			this.getEventosRAIA(request, response, out, atualizaPosicao);
		}		
		else if (action.equalsIgnoreCase("atualizaPosicaoEvento"))
		{
			String dataJson = request.getParameter("data");

			AtualizaPosicaoVO dataObj = gson.fromJson(dataJson, AtualizaPosicaoVO.class);

			this.atualizaPosicaoEvento(dataObj, out);
		}
		else if (action.equalsIgnoreCase("validateMachineId"))
		{
			String parametros = request.getParameter("param");
			this.validateMachineId(parametros, out);
		}
		else if (action.equalsIgnoreCase("saveLog"))
		{
			String texto = gson.fromJson(request.getParameter("texto"), String.class);
			String placa = request.getParameter("placa");
			this.saveLog(texto, placa, out);
		}
		else if (action.equalsIgnoreCase("saveLogMotorista"))
		{
			String texto = gson.fromJson(request.getParameter("texto"), String.class);
			//String placa = request.getParameter("placa");
			String cdViatura = request.getParameter("cdViatura");
			this.saveLogMotorista(texto, cdViatura, out);
		}
		else if (action.equalsIgnoreCase("logViatura"))
		{
			String placa = request.getParameter("placa");
			this.getLogViatura(request, response, out, placa);
		}
		else if (action.equalsIgnoreCase("logMotorista"))
		{
			String placa = request.getParameter("placa");
			this.getLogMotorista(request, response, out, placa);
		}
		else if (action.equalsIgnoreCase("getLogMotorista"))
		{
			String placa = request.getParameter("placa");
			this.getLogMotorista(request, response, out, placa);
		}
		else if (action.equalsIgnoreCase("inserirEventoEmEspera"))
		{
			String rota = request.getParameter("rota");
			String historico = request.getParameter("historico");
			String cliente = request.getParameter("cliente");
			String cdViatura = request.getParameter("viatura");
			this.inserirEventoEmEspera(request, response, out, rota, historico, cliente, cdViatura);
		}
		else if (action.equalsIgnoreCase("removerEventoEmEspera"))
		{
			String historico = request.getParameter("historico");
			this.removerEventoEmEspera(request, response, out, historico);
		}
		else if (action.equalsIgnoreCase("atualizarOrdemDosEventos"))
		{
			String eventos = request.getParameter("eventos");
			String[] array = eventos.split(":");
			this.atualizarOrdemDosEventos(request, response, out, array);
		}
		else if (action.equalsIgnoreCase("verificaCorEventos"))
		{
			String historico = request.getParameter("cdViaturaSigma");
			this.verificaCorEventos(request, response, out, historico);
		}
		else if (action.equalsIgnoreCase("salvarLogEvento"))
		{
			String historico = request.getParameter("historico");
			String textoLog = request.getParameter("textoLog");
			this.salvarLogEvento(request, response, out, historico, textoLog);
		}
		else if (action.equalsIgnoreCase("salvarLogEventoGerencia"))
		{
			String historico = request.getParameter("historico");
			String textoLog = request.getParameter("textoLog");
			this.salvarLogEventoGerencia(request, response, out, historico, textoLog);
		}
		else if (action.equalsIgnoreCase("deslocarEventoViatura"))
		{
			String rota = request.getParameter("rota");
			String historico = request.getParameter("historico");
			String cliente = request.getParameter("cliente");
			String cdViatura = request.getParameter("viatura");
			String codigo = request.getParameter("codigo");
			this.deslocarEventoViatura(request, response, out, rota, historico, cliente, cdViatura, codigo);
		}
		else if (action.equalsIgnoreCase("cancelarEventoIniciado"))
		{
			String historico = request.getParameter("historico");
			this.cancelarEventoIniciado(request, response, out, historico);
		}
		else if (action.equalsIgnoreCase("liberarDeslocamentoRaia"))
		{
			String historico = request.getParameter("historico");
			this.salvarLogEvento(request, response, out, historico, "#liberarDeslocamento");
		}
		else if (action.equalsIgnoreCase("localSemAlteracaoRAIA"))
		{
			String historico = request.getParameter("historico");
			this.salvarLogEvento(request, response, out, historico, "#localSemAlteracao");
		}
		else if (action.equalsIgnoreCase("localComAlteracaoRAIA"))
		{
			String historico = request.getParameter("historico");
			this.salvarLogEvento(request, response, out, historico, "#localComAlteracao");
		}
		else if (action.equalsIgnoreCase("solicitarFinalizacaoRAIA"))
		{
			String historico = request.getParameter("historico");
			this.salvarLogEvento(request, response, out, historico, "#finalizarAtendimento");
		}
		else if (action.equalsIgnoreCase("fecharEventoSigma"))
		{
			String historico = request.getParameter("historico");
			String motivoAlarme = request.getParameter("motivoAlarme");
			this.fecharEventoSigma(request, response, out, historico, motivoAlarme);
		}
		else if (action.equalsIgnoreCase("viaturaChegaLocal"))
		{
			String historico = request.getParameter("historico");
			this.viaturaChegaLocal(request, response, out, historico);
		}
		else if (action.equalsIgnoreCase("deslocarEventoViaturaIniciado"))
		{
			String historico = request.getParameter("historico");
			this.iniciarDeslocamentoEventoViatura(request, response, out, historico);
		}
		else if (action.equalsIgnoreCase("atualizaContatos"))
		{
			String cdCliente = request.getParameter("cdCliente");
			String cdHistorico = request.getParameter("cdHistorico");
			this.getDadosProvidencia(request, response, out, cdCliente, cdHistorico);
		}
		else if (action.equalsIgnoreCase("atualizaHistoricoEventos"))
		{
			String codigoHistorico = request.getParameter("historico");
			String dias = request.getParameter("dias");
			this.getDadosEventosHistorico(request, response, out, codigoHistorico, dias);
		}
		else if (action.equalsIgnoreCase("atualizaPosicaoEventoFila"))
		{
			String codHistorico = request.getParameter("codHistorico");
			String posicao = request.getParameter("posicao");
			this.atualizaPosicaoEventoFila(request, response, out, codHistorico, Integer.parseInt(posicao));
		}
		else if (action.equalsIgnoreCase("saveLogPopUp"))
		{
			String cdHistorico = request.getParameter("historico");
			String textoLog = request.getParameter("textoLog");
			this.saveLogPopUp(request, response, out, cdHistorico, textoLog);
		}
		else if (action.equalsIgnoreCase("getFilaEventos"))
		{

			this.getFilaEventos(request, response, out);
		}
		else if (action.equalsIgnoreCase("getLinkImagemOs"))
		{
			String cdOS = request.getParameter("cdOS");
			this.getLinkImagemOS(request, response, out, cdOS);
		}
		else if (action.equalsIgnoreCase("getLinkImagemVtr"))
		{
			String cdVtr = request.getParameter("cdVtr");
			this.getLinkImagemVtr(request, response, out, cdVtr);
		}
		else if (action.equalsIgnoreCase("getImagemOsVtr"))
		{
			String cdVtr = request.getParameter("cdVtr");
			String cdOrdem = request.getParameter("cdOrdem");
			this.getImageOsVtr(request, response, out, cdOrdem, cdVtr);
		}
		else if (action.equals("buscarEventoEmEsperaFila"))
		{
			this.buscarEventoEmEsperaFila(request, response, out);
		}
		else if (action.equals("getLogEvento"))
		{
			String cdCliente = request.getParameter("cdCliente");
			this.getLogEvento(request, response, out, cdCliente);
		}
		else if (action.equals("buscaMotivoAlarme"))
		{
			this.buscaMotivoAlarme(request, response, out);
		}

		out.flush();
		out.close();
	}

	@SuppressWarnings("unchecked")
	private void getViaturas(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String tipo, String codigoTecnico)
	{

		/**
		 * @author orsegups lucas.avila - Class generic.
		 * @date 08/07/2015
		 */
		Collection<ViaturaVO> viaturaVOs = new ArrayList<ViaturaVO>();
		try
		{

			Class<? extends NeoObject> viaturaClazz = null;
			viaturaClazz = AdapterUtils.getEntityClass("OTSViatura");

			String regional = request.getParameter("regional");
			if (regional != null && (regional.contains("PARC - GOLF") || regional.contains("PARC - DPSP")))
				regional = null;
			String x8IgnoreTags = request.getParameter("x8IgnoreTags");
			String ramalOrigem = request.getParameter("ramalOrigem");
			String aplicacao = request.getParameter("aplicacao");
			Boolean emUso = Boolean.TRUE;
			String strEmUso = request.getParameter("emUso");
			//			String filtrosConta = request.getParameter("filtrosConta");
			//			String filtrosNotConta = request.getParameter("filtrosNotConta");
			//			String filtrosAit = request.getParameter("filtrosAit");
			String statusVtr = request.getParameter("statusVtr");
			String ativaNaFrotaVtr = request.getParameter("ativaNaFrota");
			Boolean ativaNaFrota = Boolean.TRUE;

			if (strEmUso != null && strEmUso.equals("nao"))
			{
				emUso = Boolean.FALSE;
			}
			else if (strEmUso != null && strEmUso.equals("na"))
			{
				emUso = null;
			}

			if (ativaNaFrotaVtr != null && ativaNaFrotaVtr.equals("nao"))
			{
				ativaNaFrota = Boolean.FALSE;
			}
			else if (ativaNaFrotaVtr != null && ativaNaFrotaVtr.equals("na"))
			{
				ativaNaFrota = null;
			}

			QLFilterIsNotNull latitudeIsNotNull = new QLFilterIsNotNull("latitude");
			QLFilterIsNotNull longitudeIsNotNull = new QLFilterIsNotNull("longitude");

			QLEqualsFilter emUsoFilter = new QLEqualsFilter("emUso", emUso);

			QLEqualsFilter ativaNaFrotaFilter = new QLEqualsFilter("ativaNaFrota", ativaNaFrota);

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			
			
			if (statusVtr != null && !statusVtr.isEmpty() && !statusVtr.equalsIgnoreCase("null"))
			{
				QLGroupFilter listastatusVtrFilter = new QLGroupFilter("OR");
				StringTokenizer st = new StringTokenizer(statusVtr, ",");

				while (st.hasMoreElements())
				{
					String statusVtrStr = (String) st.nextElement();
					QLEqualsFilter viaturaCMFilter = new QLEqualsFilter("statusViatura.codigoStatus", Long.parseLong(statusVtrStr));
					listastatusVtrFilter.addFilter(viaturaCMFilter);
				}
				groupFilter.addFilter(listastatusVtrFilter);
			}
			
			if (regional != null && regional.equalsIgnoreCase("SOO") && aplicacao.contains("TAT")) {
                QLOpFilter likeMotoristaSup = new QLOpFilter("motorista", "not like", "%SOO-SUP%", OperandType.String);
                QLOpFilter likeMotoristaFis = new QLOpFilter("motorista", "not like", "%SOO-FIS%", OperandType.String);
                groupFilter.addFilter(likeMotoristaSup);
                groupFilter.addFilter(likeMotoristaFis);
            }
			
			groupFilter.addFilter(latitudeIsNotNull);
			groupFilter.addFilter(longitudeIsNotNull);
			if (emUso != null)
			{
				groupFilter.addFilter(emUsoFilter);
			}

			if (ativaNaFrota != null)
			{
				groupFilter.addFilter(ativaNaFrotaFilter);
			}

			// somente filtra pela regional da viatura se a regional for passada como parametro (lista), caso contrario traz as viaturas de todas as regionais
			if (regional != null && !regional.isEmpty() && !regional.equalsIgnoreCase("null"))
			{
				QLGroupFilter listaRegionalFilter = new QLGroupFilter("OR");
				StringTokenizer st = new StringTokenizer(regional, ",");

				while (st.hasMoreElements())
				{
					String siglaRegional = (String) st.nextElement();
					QLEqualsFilter regionalFilter = new QLEqualsFilter("codigoRegional", siglaRegional);
					listaRegionalFilter.addFilter(regionalFilter);
				}
				groupFilter.addFilter(listaRegionalFilter);
			}

			if (tipo != null && tipo.equalsIgnoreCase("viaturasCM"))
			{

				// filtra viaturas ligadas a eventos X8 com tags de exclusao
				String nomeFonteDados = "SIGMA90";
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT v.NM_PLACA FROM HISTORICO h  INNER JOIN VIATURA v  ON  v.CD_VIATURA = h.CD_VIATURA ");
				sql.append(" WHERE h.FG_STATUS IN (1, 2, 3, 9) AND h.CD_HISTORICO_PAI IS NULL");

				String sqlFiltroTag = "";
				if (x8IgnoreTags != null)
				{
					StringTokenizer stTag = new StringTokenizer(x8IgnoreTags, ",");
					int countToken = 0;
					while (stTag.hasMoreElements())
					{
						countToken++;
						String tag = (String) stTag.nextElement();

						if (countToken == 1)
						{
							sqlFiltroTag = sqlFiltroTag + " AND (  ";
							sqlFiltroTag = sqlFiltroTag + "        ( h.TX_OBSERVACAO_GERENTE LIKE '%#" + tag + "%' ) ";
						}
						else
						{
							sqlFiltroTag = sqlFiltroTag + "        OR (h.TX_OBSERVACAO_GERENTE LIKE '%#" + tag + "%' ) ";
						}

					}
					if (countToken > 0)
					{
						sqlFiltroTag = sqlFiltroTag + " ) ";
					}
				}
				sql.append(sqlFiltroTag);

				Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());

				Collection<Object> resultList = query.getResultList();

				//Statement statement = null;
				Collection<String> listaNotPlaca = new ArrayList<String>();

				if (resultList != null)
				{
					for (Object result : resultList)
					{
						if (result != null)
						{
							String placa = ((String) result);
							if (placa != null)
							{
								listaNotPlaca.add(placa);
							}
						}
					}
				}

				// cria o filtro das viaturas tagadas
				QLNotInFilter listaExclusaoViaturasFilter = new QLNotInFilter("placa", listaNotPlaca);
				groupFilter.addFilter(listaExclusaoViaturasFilter);

				// somente filtra pela aplicacao da viatura se a aplicacao for passada como parametro (lista), caso contrario traz somente as viaturas da aplicacao TAT
				if (aplicacao != null && !aplicacao.isEmpty() && !aplicacao.equalsIgnoreCase("null"))
				{
					QLGroupFilter listaAplicacaoFilter = new QLGroupFilter("OR");
					StringTokenizer st = new StringTokenizer(aplicacao, ",");

					while (st.hasMoreElements())
					{
						String siglaAplicacao = (String) st.nextElement();
						QLEqualsFilter viaturaCMFilter = new QLEqualsFilter("aplicacaoViatura.sigla", siglaAplicacao);
						listaAplicacaoFilter.addFilter(viaturaCMFilter);
					}
					groupFilter.addFilter(listaAplicacaoFilter);
				}
				else
				{
					QLEqualsFilter viaturaCMFilter = new QLEqualsFilter("aplicacaoViatura.sigla", "TAT");
					groupFilter.addFilter(viaturaCMFilter);
				}

			}
			else if (tipo != null && tipo.equalsIgnoreCase("viaturasTecnicas"))
			{
				QLEqualsFilter viaturaTecnicaFilter = new QLEqualsFilter("aplicacaoViatura.sigla", "TEC");
				groupFilter.addFilter(viaturaTecnicaFilter);

				if (codigoTecnico != null)
				{
					QLGroupFilter listaTecnicosFilter = new QLGroupFilter("OR");
					StringTokenizer st = new StringTokenizer(codigoTecnico, ",");

					while (st.hasMoreElements())
					{
						String codTec = (String) st.nextElement();
						QLEqualsFilter tecnicoFilter = new QLEqualsFilter("codigoDoTecnico", codTec);
						listaTecnicosFilter.addFilter(tecnicoFilter);
					}
					groupFilter.addFilter(listaTecnicosFilter);

				}

			}

			Collection<NeoObject> ostViaturesObj = (Collection<NeoObject>) PersistEngine.getObjects(viaturaClazz, groupFilter);

			for (NeoObject obj : ostViaturesObj)
			{
				ViaturaVO viaturaVO = this.composeViaturaVO(obj, ramalOrigem);

				if (viaturaVO != null)
				{

					viaturaVOs.add(viaturaVO);
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Gson gson = new Gson();
			StringBuilder builder = new StringBuilder();
			builder.append(gson.toJson(viaturaVOs));

			out.print(builder.toString());
		}

	}

	@SuppressWarnings("finally")
	private ViaturaVO composeViaturaVO(NeoObject ostViaturaObject, String ramalOrigem)
	{

		ViaturaVO viaturaVO = null;
		try
		{

			EntityWrapper viaturaWrapper = new EntityWrapper(ostViaturaObject);

			String dataRecepcaoStr = "";
			GregorianCalendar calendar = (GregorianCalendar) viaturaWrapper.findValue("ultimaAtualizacao");
			if (calendar != null)
			{
				dataRecepcaoStr = NeoDateUtils.safeDateFormat(calendar);
			}
			String dataMovimentacaoStr = "";
			GregorianCalendar calendarMov = (GregorianCalendar) viaturaWrapper.findValue("dataMovimentacao");
			if (calendarMov != null)
			{
				dataMovimentacaoStr = NeoDateUtils.safeDateFormat(calendarMov);
			}
			String dataUltimoDeslocamentoStr = "";
			GregorianCalendar calendarUltimoDeslocamento = (GregorianCalendar) viaturaWrapper.findValue("ultimoStatusDeslocamento");
			if (calendarUltimoDeslocamento != null)
			{
				dataUltimoDeslocamentoStr = NeoDateUtils.safeDateFormat(calendarUltimoDeslocamento);
			}
			Float latitudeFloat = 0F;
			BigDecimal latitude = (BigDecimal) viaturaWrapper.findValue("latitude");
			if (latitude != null)
			{
				latitudeFloat = latitude.floatValue();
			}

			Float longitudeFloat = 0F;
			BigDecimal longitude = (BigDecimal) viaturaWrapper.findValue("longitude");
			if (longitude != null)
			{
				longitudeFloat = longitude.floatValue();
			}

			String nomeImagem = "carro.png";
			if (viaturaWrapper.findValue("tipoViatura.nomeImagem") != null)
			{
				nomeImagem = (String) viaturaWrapper.findValue("tipoViatura.nomeImagem");
			}
			
			
			boolean controlarRota = false;
			if (viaturaWrapper.findValue("controlarRota") != null && (boolean)viaturaWrapper.findValue("controlarRota")){
			    controlarRota = (boolean) viaturaWrapper.findValue("controlarRota");
			}

			viaturaVO = new ViaturaVO();
			viaturaVO.setDataRecepcao(dataRecepcaoStr);
			viaturaVO.setLatitude(latitudeFloat);
			viaturaVO.setLongitude(longitudeFloat);
			viaturaVO.setVelocidade((Long) viaturaWrapper.findValue("velocidade"));
			viaturaVO.setTitulo((String) viaturaWrapper.findValue("titulo"));
			viaturaVO.setAltitude((Long) viaturaWrapper.findValue("altitude"));
			viaturaVO.setDirecao((Long) viaturaWrapper.findValue("direcao"));
			viaturaVO.setIgnicao((Boolean) viaturaWrapper.findValue("ignicao"));
			viaturaVO.setSatelites((Long) viaturaWrapper.findValue("satelites"));
			viaturaVO.setPlaca((String) viaturaWrapper.findValue("placa"));
			viaturaVO.setNomeMotorista((String) viaturaWrapper.findValue("motorista"));
			String moduloRastreador = (String) viaturaWrapper.findValue("rastreador.id") + " - " + (String) viaturaWrapper.findValue("rastreador.modeloEquipamento.nomeModelo");
			viaturaVO.setNomeImagem(nomeImagem);
			viaturaVO.setDeslocamentoLento(false);
			viaturaVO.setCodigoViatura((String) viaturaWrapper.findValue("codigoViatura"));
			if (viaturaWrapper.findValue("corLigacao") != null)
			{

				viaturaVO.setCorLigacao((String) viaturaWrapper.findValue("corLigacao.cor"));
				viaturaVO.setSufixoLigacao((String) viaturaWrapper.findValue("corLigacao.sufixoImagem"));

			}
			else
			{
				viaturaVO.setSufixoLigacao("");
			}
			viaturaVO.setAtrasoDeslocamento((Boolean) viaturaWrapper.findValue("atrasoDeslocamento"));
			viaturaVO.setDeslocamentoLento((Boolean) viaturaWrapper.findValue("deslocamentoNaoIniciado"));
			viaturaVO.setAlertaNoLocal((Boolean) viaturaWrapper.findValue("excessoDeTempoNoLocal"));

			//busca e valida os ramais/numeros para o snap discar
			String numeroDestino = (String) viaturaWrapper.findValue("telefone");
			viaturaVO.setTelefone(numeroDestino);
			if ((ramalOrigem == null || ramalOrigem.isEmpty() || ramalOrigem.equalsIgnoreCase("null")) && PortalUtil.getCurrentUser() != null)
			{
				EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
				ramalOrigem = (String) usuarioOrigemWrapper.findValue("ramal");
			}
			String link = OrsegupsUtils.getCallLink(ramalOrigem, numeroDestino, false);

			String imagemIgnicao = "images/led-red.gif";
			if (viaturaVO.getIgnicao())
			{
				imagemIgnicao = "images/led-green.gif";
			}
			StringBuilder contentString = new StringBuilder();
			contentString.append("<div id='content' style=\"width:500px; height:280px\"  ><div id='bodyContent'  >");
			contentString.append("<span class=\"gm-style-iw\"><b>Motorista:</b> </span><span class=\"gm-addr\">" + viaturaVO.getNomeMotorista());
			contentString.append("</span>");
			
			contentString.append("<br><span class=\"gm-style-iw\"><b>Placa:</b> </span><span class=\"gm-addr\">" + viaturaVO.getPlaca());
			
			contentString.append("</span><br><span class=\"gm-style-iw\"><b>Viatura em uso desde:</b> </span><span class=\"gm-addr\">" + dataMovimentacaoStr);
			contentString.append("</span><br><span class=\"gm-style-iw\"><b>Rastreador:</b> </span><span class=\"gm-addr\">" + moduloRastreador);
			contentString.append("</span><br><span class=\"gm-style-iw\"><b>Ignição:</b> </span><img src=\"" + imagemIgnicao + "\" />");
			contentString.append("<br><span class=\"gm-style-iw\"><b>Velocidade:</b> </span><span class=\"gm-addr\">" + viaturaVO.getVelocidade());
			contentString.append("Km/h</span>" + "<br>" + "<span class=\"gm-style-iw\"><b>Ultima Atualização:</b> </span><span class=\"gm-addr\">" + viaturaVO.getDataRecepcao() + "</span><br>");
			String dataStr = "";

			if (NeoUtils.safeIsNotNull(moduloRastreador) && moduloRastreador.contains("SEGWARE MOBILE"))
				dataStr = viaturaVO.getDataRecepcao().toString();
			else
				dataStr = dataUltimoDeslocamentoStr;

			contentString.append("<span class=\"gm-style-iw\"><b>Último Movimento:</b> </span><span class=\"gm-addr\">" + dataStr + "</span>");

			if (link != null)
			{
				contentString.append("<br><span class=\"gm-style-iw\"><b>Telefone:</b> <b>" + link + "</b></span>");
			}
			else if (numeroDestino != null && !numeroDestino.isEmpty())
			{
				contentString.append("<br><span class=\"gm-style-iw\">Telefone: <b>" + numeroDestino + "</b></span>");
			}

			//String textoLog = this.getLogViatura(ostViaturaObject);
			contentString.append("<br><a  id=\"div2\"  class=\"easyui-tooltip\" onmouseout=\"javascript:removeTooltip('" + viaturaVO.getPlaca() + "')\" onmouseenter=\"javascript:getLogViatura('" + viaturaVO.getPlaca() + "',this);\" href=\"javascript:saveInputLog('" + viaturaVO.getPlaca() + "');\"><img style=\"border : none\" src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");
			contentString.append("&nbsp<a  id=\"div2\" class=\"easyui-tooltip\" onmouseout=\"javascript:removeTooltip('" + viaturaVO.getPlaca() + "')\" onmouseenter=\"javascript:getLogMotorista('" + viaturaVO.getPlaca() + "',this);\" href=\"javascript:saveInputLogMotorista('" + viaturaVO.getCodigoViatura() + "');\"><img style=\"border : none\" src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");
			
			
			if(controlarRota){
			    contentString.append("<b> Automatizar rota: </b><label class=\"switch pull-right\"><input onClick=\"controlarRota(this,"+viaturaVO.getCodigoViatura()+")\" type=\"checkbox\" checked><div class=\"slider round\"></div></label>");
			}else{	    
			    contentString.append("<b> Automatizar rota: </b><label class=\"switch pull-right\"><input onClick=\"controlarRota(this,"+viaturaVO.getCodigoViatura()+")\" type=\"checkbox\"><div class=\"slider round\"></div></label>");
			}
			
			//			Collection<EventoEsperaVO> eventoEsperaVOs = null;

			//eventoEsperaVOs = buscarEventoEmEspera(viaturaVO.getCodigoViatura(), viaturaVO.getNomeMotorista(), "evento");
			contentString.append(" <br><div id=\"listaFila" + viaturaVO.getCodigoViatura() + "\">");
			String combo = "<br><select name=\"listaFila\" id=\"listaFila\" style=\"width:400px;\" multiple=\"multiple\">";
			combo += "<option value='0'> Clique na viatura para carregar!</option>";
			combo += "</select>";

			//			if (NeoUtils.safeIsNotNull(eventoEsperaVOs) && !eventoEsperaVOs.isEmpty())
			//			{
			//				int i = 1;
			//				for (EventoEsperaVO eventoEsperaVO : eventoEsperaVOs)
			//				{
			//					combo += "<option value='" + eventoEsperaVO.getCodHistorico() + "'>" + i + "º - " + eventoEsperaVO.getCliente() + "</option>";
			//					i++;
			//				}
			//
			//			}
			//			else
			//			{
			//				combo += "<option value='0'> Clique na viatura para carregar!</option>";
			//			}
			//			combo += "</select>";

			contentString.append(combo);

			contentString.append("</div> ");
			contentString.append("<br><span class=\"gm-style-iw\"><b>Opções:</b> </span> <a href=\"javascript:moveUp(document.getElementById('listaFila'));\" title='Mover para cima!' \" ><img style=\"border : none\" src=\"images/up.png\"/></a>" + "&nbsp<a title='Mover para baixo!' href=\"javascript:moveDown(document.getElementById('listaFila'));\" \"><img style=\"border : none\" src=\"images/down.png\"/></a>");
			contentString.append("&nbsp<a href=\"javascript:removerEventoEmEsperaLista();\" title='Remover!' \" ><img style=\"border : none\" src=\"images/remove.png\"/></a>" + "&nbsp<a href=\"javascript:atualizaEventosEmEspera();\" title='Confirmar!' ><img style=\"border : none\" src=\"images/accept.png\"/></a>");

			contentString.append("</div> </div>");
			viaturaVO.setInfoWindowContent(contentString.toString());

			if (viaturaWrapper.findValue("statusViatura") != null)
			{
				GregorianCalendar calendarAtual = new GregorianCalendar();
				calendarAtual.add(Calendar.MINUTE, -20);

				Long status = (Long) viaturaWrapper.findValue("statusViatura.codigoStatus");

				if (status != null && status != 1L && calendar.before(calendarAtual))
				{
					NeoObject statusObj = this.getStatusViatura(0L);
					if (statusObj != null)
					{
						EntityWrapper entityWrapper = new EntityWrapper(statusObj);
						viaturaVO.setCorStatus((String) entityWrapper.findValue("Cor"));
						viaturaVO.setCorTexto((String) entityWrapper.findValue("corTexto"));
					}
				}
				else
				{
					if (viaturaWrapper.findValue("statusViatura.Cor") != null)
					{
						viaturaVO.setCorStatus((String) viaturaWrapper.findValue("statusViatura.Cor"));
					}

					if (viaturaWrapper.findValue("statusViatura.corTexto") != null)
					{
						viaturaVO.setCorTexto((String) viaturaWrapper.findValue("statusViatura.corTexto"));
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return viaturaVO;
		}

	}

	private void getEventos(HttpServletRequest request, HttpServletResponse response, PrintWriter out, Boolean atualizaPosicao)
	{

		Collection<EventoVO> eventoVOs = null;
		try
		{
			String nomeFonteDados = "SIGMA90";
			String regional = request.getParameter("regional");
			String filtrosConta = request.getParameter("filtrosConta");
			String filtrosNotConta = request.getParameter("filtrosNotConta");
			String filtrosCUC = request.getParameter("filtrosCUC");
			String filtrosNotCUC = request.getParameter("filtrosNotCUC");
			String eventosGolf = request.getParameter("eventosGolf");
			String eventosDPSP = request.getParameter("eventosDPSP");
			String eventosRAIA = request.getParameter("eventosRAIA");
			Map<String, Collection<String>> filtroContaMap = new HashMap<String, Collection<String>>();
			Map<String, Collection<String>> filtroNotContaMap = new HashMap<String, Collection<String>>();

			//fitrosCUC = "ALR";
			//fitrosNotCUC = "ALN";
			if (filtrosConta != null && !filtrosConta.isEmpty())
			{
				String contasArray[] = filtrosConta.split(",");
				for (String item : contasArray)
				{
					if (item != null)
					{
						String empresaConta[] = item.split(":");

						if (empresaConta != null && empresaConta.length == 2)
						{
							String empresa = empresaConta[0];
							String conta = empresaConta[1];

							if (empresa != null && conta != null)
							{
								Collection<String> contas = filtroContaMap.get(empresa);

								if (contas == null)
								{
									contas = new ArrayList<String>();
								}

								contas.add(conta);

								filtroContaMap.put(empresa, contas);
							}
						}
					}
				}
			}
			if (filtrosNotConta != null && !filtrosNotConta.isEmpty())
			{
				String notContasArray[] = filtrosNotConta.split(",");
				for (String itemNot : notContasArray)
				{
					if (itemNot != null)
					{
						String empresaNotConta[] = itemNot.split(":");

						if (empresaNotConta != null && empresaNotConta.length == 2)
						{
							String notEmpresa = empresaNotConta[0];
							String notConta = empresaNotConta[1];

							if (notEmpresa != null && notConta != null)
							{
								Collection<String> notContas = filtroNotContaMap.get(notEmpresa);

								if (notContas == null)
								{
									notContas = new ArrayList<String>();
								}

								notContas.add(notConta);

								filtroNotContaMap.put(notEmpresa, notContas);
							}
						}
					}
				}
			}
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO, h.NU_PRIORIDADE, c.CD_CLIENTE, v.NM_VIATURA, h.FG_STATUS, fe.NM_FRASE_EVENTO, ");
			sql.append(" v.CD_VIATURA, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, c.ID_CENTRAL, c.PARTICAO, ");
			sql.append(" c.FANTASIA, c.RAZAO, c.ENDERECO, cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, TX_OBSERVACAO_FECHAMENTO, ");
			sql.append(" TX_OBSERVACAO_GERENTE, r.TELEFONE, ISNULL(h.NU_LATITUDE, c.NU_LATITUDE) AS NU_LATITUDE, ISNULL(h.NU_LONGITUDE, c.NU_LONGITUDE) AS NU_LONGITUDE, v.NM_PLACA, h.CD_USUARIO_VIATURA_NO_LOCAL, ");
			sql.append(" h.DT_VIATURA_DESLOCAMENTO, c.COMPLEMENTO, e.NOME AS NOMEESTADO, ColDsl.NM_COLABORADOR AS NOMECOLABORADORFDSL , ");
			sql.append(" ColIni.NM_COLABORADOR AS NOMECOLABORADORINI, ColLoc.NM_COLABORADOR AS NOMECOLABORADORLOC,  r.NM_ROTA, ");
			sql.append(" c.ID_EMPRESA, h.DT_EXIBIDO_VIATURA, c.CHAVE, c.CACHORRO, c.ARMA, c.REFERENCIA, c.OBSERVACAO, h.CD_CODE ");
			sql.append(" FROM HISTORICO h  ");
			sql.append(" INNER JOIN dbCENTRAL c  ON c.CD_CLIENTE = h.CD_CLIENTE ");
			sql.append(" LEFT OUTER JOIN VIATURA v  ON  v.CD_VIATURA = h.CD_VIATURA ");
			sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe  ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO ");
			sql.append(" LEFT JOIN dbBAIRRO bai  ON bai.ID_BAIRRO = c.ID_BAIRRO ");
			sql.append(" LEFT JOIN dbCIDADE cid  ON cid.ID_CIDADE = c.ID_CIDADE ");
			sql.append(" INNER JOIN ROTA r  ON r.CD_ROTA = c.ID_ROTA ");
			sql.append(" INNER JOIN dbESTADO e   ON e.ID_ESTADO = cid.ID_ESTADO ");
			sql.append(" LEFT JOIN USUARIO UsuDsl  on UsuDsl.CD_USUARIO = h.CD_USUARIO_ESPERA_DESLOCAMENTO ");
			sql.append(" LEFT JOIN COLABORADOR ColDsl  on ColDsl.CD_COLABORADOR = UsuDsl.CD_COLABORADOR ");
			sql.append(" LEFT JOIN USUARIO UsuIni  on UsuIni.CD_USUARIO = h.CD_USUARIO_VIATURA_DESLOCAMENTO ");
			sql.append(" LEFT JOIN COLABORADOR ColIni  on ColIni.CD_COLABORADOR = UsuIni.CD_COLABORADOR ");
			sql.append(" LEFT JOIN USUARIO UsuLoc  on UsuLoc.CD_USUARIO = h.CD_USUARIO_VIATURA_NO_LOCAL ");
			sql.append(" LEFT JOIN COLABORADOR ColLoc  on ColLoc.CD_COLABORADOR = UsuLoc.CD_COLABORADOR ");
			if (eventosRAIA != null && eventosRAIA.equalsIgnoreCase("true")){
				sql.append(" WHERE h.FG_STATUS IN (0, 1, 2, 3, 5, 9) ");
				sql.append(" AND c.CD_GRUPO_CLIENTE = 292 ");
				//uva
				//sql.append(" AND NOT EXISTS (SELECT 1 FROM [FSOODB04\\SQL02].TIDB.DBO.EXCECAO_EVENTOS_MAPA_RAIA EEMR WHERE EEMR.EVENTO = h.CD_EVENTO) ");
			}else {
				sql.append(" WHERE h.FG_STATUS IN (1, 2, 3, 9) ");
			}
			sql.append(" AND h.CD_HISTORICO_PAI IS NULL ");
			sql.append(" AND ((c.NU_LATITUDE is not null AND c.NU_LONGITUDE is not null) OR (h.NU_LATITUDE is not null AND h.NU_LONGITUDE is not null)) ");

			//filtra por conta e empresa
			if (filtroContaMap != null && !filtroContaMap.isEmpty())
			{
				Boolean firstEmpresa = true;
				Boolean needClose = false;

				for (String empresa : filtroContaMap.keySet())
				{
					Collection<String> contas = filtroContaMap.get(empresa);

					if (contas != null && !contas.isEmpty())
					{
						if (firstEmpresa)
						{
							sql.append(" AND (");
							firstEmpresa = false;
							needClose = true;
						}
						else
						{
							sql.append("OR");
						}

						sql.append("(");

						if (!empresa.trim().equals("*"))
						{
							sql.append(" c.ID_EMPRESA = " + empresa + " AND");
						}

						sql.append(" c.ID_CENTRAL IN (");
						Boolean first = true;
						for (String conta : contas)
						{
							if (first)
							{
								sql.append("'" + conta + "'");
								first = false;
							}
							else
							{
								sql.append(", '" + conta + "'");
							}
						}
						sql.append(") )");
					}
				}

				if (needClose)
				{
					sql.append(")");
				}
			}

			//filtra (ELIMINA) contas (e empresa)
			if (filtroNotContaMap != null && !filtroNotContaMap.isEmpty())
			{
				Boolean firstNotEmpresa = true;
				Boolean needClose = false;

				for (String notEmpresa : filtroNotContaMap.keySet())
				{
					Collection<String> notContas = filtroNotContaMap.get(notEmpresa);

					if (notContas != null && !notContas.isEmpty())
					{
						if (firstNotEmpresa)
						{
							sql.append(" AND (");
							firstNotEmpresa = false;
							needClose = true;
						}
						else
						{
							sql.append(" OR");
						}

						sql.append(" (");

						if (!notEmpresa.trim().equals("*"))
						{
							sql.append(" c.ID_EMPRESA <> " + notEmpresa + " AND");
						}

						sql.append("  c.ID_CENTRAL NOT IN (");
						Boolean first = true;
						for (String notConta : notContas)
						{
							if (first)
							{
								sql.append("'" + notConta + "'");
								first = false;
							}
							else
							{
								sql.append(", '" + notConta + "'");
							}
						}
						sql.append(") )");
					}
				}

				if (needClose)
				{
					sql.append(" )");
				}
			}

			// somente filtra pela regional da viatura se a regional for passada como parametro (lista), caso contrario traz as viaturas de todas as regionais
			if (regional != null && !regional.isEmpty() && !regional.equalsIgnoreCase("null"))
			{
				sql.append(" AND ( ");
				StringTokenizer st = new StringTokenizer(regional, ",");
				int count = 0;

				while (st.hasMoreElements())
				{
					String siglaRegional = (String) st.nextElement();
					if (count > 0)
						sql.append(" OR");
					sql.append(" r.NM_ROTA LIKE '" + siglaRegional + "%' ");

					count++;
				}
				sql.append(" ) ");
			}

			if (filtrosCUC != null && !filtrosCUC.isEmpty() && !filtrosCUC.equalsIgnoreCase("null"))
			{
				sql.append(" AND ( ");
				StringTokenizer st = new StringTokenizer(filtrosCUC, ",");
				Boolean first = true;
				sql.append(" h.CD_CODE IN (");
				while (st.hasMoreElements())
				{
					String cucStr = (String) st.nextElement();

					if (first)
					{
						sql.append("'" + cucStr + "'");
						first = false;
					}
					else
					{
						sql.append(", '" + cucStr + "'");
					}

				}
				sql.append(" )  )");
			}

			if (filtrosNotCUC != null && !filtrosNotCUC.isEmpty() && !filtrosNotCUC.equalsIgnoreCase("null"))
			{
				sql.append(" AND ( ");
				StringTokenizer st = new StringTokenizer(filtrosNotCUC, ",");
				Boolean first = true;
				sql.append(" h.CD_CODE NOT IN (");
				while (st.hasMoreElements())
				{
					String cucStr = (String) st.nextElement();

					if (first)
					{
						sql.append("'" + cucStr + "'");
						first = false;
					}
					else
					{
						sql.append(", '" + cucStr + "'");
					}

				}
				sql.append(" )  )");
			}
			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
			@SuppressWarnings("unchecked")
			Collection<Object> resultList = query.getResultList();

			//Statement statement = null;

			eventoVOs = new ArrayList<EventoVO>();

			if (resultList != null)
			{
				List<String> listEventoAgrupado = verificaEventoAgrupado();
				for (Object result : resultList)
				{
					if (result != null)
					{
						EventoVO eventoVO = this.composeEventoVO((Object[]) result, atualizaPosicao, eventosGolf, eventosDPSP, eventosRAIA, false);
						if (eventoVO != null)
						{
							if (eventoVO.getCodigoHistorico() != null && listEventoAgrupado != null && !listEventoAgrupado.isEmpty() && listEventoAgrupado.contains(eventoVO.getCodigoHistorico()))
								eventoVO.setHistoricoPai(true);

							eventoVOs.add(eventoVO);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

			Gson gson = new Gson();
			StringBuilder builder = new StringBuilder();
			builder.append(gson.toJson(eventoVOs));

			out.print(builder.toString());

		}
	}
	
	private void getEventosRAIA(HttpServletRequest request, HttpServletResponse response, PrintWriter out, Boolean atualizaPosicao)
	{

		Collection<EventoVO> eventoVOs = null;
		try
		{
			String nomeFonteDados = "SIGMA90";
			String opcaoRAIASelecioada = request.getParameter("opcaoRAIASelecioada");
			String grupoCliente = request.getParameter("grupoCliente");
			if (grupoCliente == null || grupoCliente.isEmpty()) {
				Gson gson = new Gson();
				StringBuilder builder = new StringBuilder();
				builder.append(gson.toJson(eventoVOs));
				out.print(builder.toString());
			}
			
			Collection<OpcaoMapaRAIAVO> opcoesRAIA = getOpcoesRAIA(opcaoRAIASelecioada);			
			
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO, h.NU_PRIORIDADE, c.CD_CLIENTE, v.NM_VIATURA, h.FG_STATUS, fe.NM_FRASE_EVENTO, ");
			sql.append(" v.CD_VIATURA, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, c.ID_CENTRAL, c.PARTICAO, ");
			sql.append(" c.FANTASIA, c.RAZAO, c.ENDERECO, cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, TX_OBSERVACAO_FECHAMENTO, ");
			sql.append(" TX_OBSERVACAO_GERENTE, r.TELEFONE, ISNULL(h.NU_LATITUDE, c.NU_LATITUDE) AS NU_LATITUDE, ISNULL(h.NU_LONGITUDE, c.NU_LONGITUDE) AS NU_LONGITUDE, v.NM_PLACA, h.CD_USUARIO_VIATURA_NO_LOCAL, ");
			sql.append(" h.DT_VIATURA_DESLOCAMENTO, c.COMPLEMENTO, e.NOME AS NOMEESTADO, ColDsl.NM_COLABORADOR AS NOMECOLABORADORFDSL , ");
			sql.append(" ColIni.NM_COLABORADOR AS NOMECOLABORADORINI, ColLoc.NM_COLABORADOR AS NOMECOLABORADORLOC,  r.NM_ROTA, ");
			sql.append(" c.ID_EMPRESA, h.DT_EXIBIDO_VIATURA, c.CHAVE, c.CACHORRO, c.ARMA, c.REFERENCIA, c.OBSERVACAO, h.CD_CODE ");
			sql.append(" FROM HISTORICO h  ");
			sql.append(" INNER JOIN dbCENTRAL c  ON c.CD_CLIENTE = h.CD_CLIENTE ");
			sql.append(" LEFT OUTER JOIN VIATURA v  ON  v.CD_VIATURA = h.CD_VIATURA ");
			sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe  ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO ");
			sql.append(" LEFT JOIN dbBAIRRO bai  ON bai.ID_BAIRRO = c.ID_BAIRRO ");
			sql.append(" LEFT JOIN dbCIDADE cid  ON cid.ID_CIDADE = c.ID_CIDADE ");
			sql.append(" INNER JOIN ROTA r  ON r.CD_ROTA = c.ID_ROTA ");
			sql.append(" INNER JOIN dbESTADO e   ON e.ID_ESTADO = cid.ID_ESTADO ");
			sql.append(" LEFT JOIN USUARIO UsuDsl  on UsuDsl.CD_USUARIO = h.CD_USUARIO_ESPERA_DESLOCAMENTO ");
			sql.append(" LEFT JOIN COLABORADOR ColDsl  on ColDsl.CD_COLABORADOR = UsuDsl.CD_COLABORADOR ");
			sql.append(" LEFT JOIN USUARIO UsuIni  on UsuIni.CD_USUARIO = h.CD_USUARIO_VIATURA_DESLOCAMENTO ");
			sql.append(" LEFT JOIN COLABORADOR ColIni  on ColIni.CD_COLABORADOR = UsuIni.CD_COLABORADOR ");
			sql.append(" LEFT JOIN USUARIO UsuLoc  on UsuLoc.CD_USUARIO = h.CD_USUARIO_VIATURA_NO_LOCAL ");
			sql.append(" LEFT JOIN COLABORADOR ColLoc  on ColLoc.CD_COLABORADOR = UsuLoc.CD_COLABORADOR ");
			sql.append(" WHERE h.FG_STATUS IN (1,2,3,9) ");
			sql.append(" AND c.CD_GRUPO_CLIENTE = "+grupoCliente+" ");
			sql.append(" AND h.CD_HISTORICO_PAI IS NULL ");
			sql.append(" AND ((c.NU_LATITUDE is not null AND c.NU_LONGITUDE is not null) OR (h.NU_LATITUDE is not null AND h.NU_LONGITUDE is not null)) ");
			if (opcoesRAIA != null && !opcoesRAIA.isEmpty()) {
				String eventosFiltro = null;
				for (OpcaoMapaRAIAVO opcaoMapaRAIAVO : opcoesRAIA) {
					String eventos = opcaoMapaRAIAVO.getEventos();
					if (eventos != null) {
						String[] events = eventos.split(",");
						for (String event : events) {
							if (eventosFiltro == null)
								eventosFiltro = "'"+event+"'";
							else
								eventosFiltro += ",'"+event+"'";
						}
					}
				}
				if (eventosFiltro != null) {
					sql.append(" AND H.CD_EVENTO IN ("+eventosFiltro+") ");
				}
			}

			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
			@SuppressWarnings("unchecked")
			Collection<Object> resultList = query.getResultList();

			eventoVOs = new ArrayList<EventoVO>();

			if (resultList != null)
			{
				List<String> listEventoAgrupado = verificaEventoAgrupado();
				for (Object result : resultList)
				{
					if (result != null)
					{
						EventoVO eventoVO = this.composeEventoVO((Object[]) result, atualizaPosicao, null, null, null, true);
						if (eventoVO != null)
						{
							if (eventoVO.getCodigoHistorico() != null && listEventoAgrupado != null && !listEventoAgrupado.isEmpty() && listEventoAgrupado.contains(eventoVO.getCodigoHistorico()))
								eventoVO.setHistoricoPai(true);

							eventoVOs.add(eventoVO);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

			Gson gson = new Gson();
			StringBuilder builder = new StringBuilder();
			builder.append(gson.toJson(eventoVOs));

			out.print(builder.toString());

		}
	}

	@SuppressWarnings("finally")
	private EventoVO composeEventoVO(Object[] resultSet, Boolean atualizaPosicao, String eventosGolf, String eventosDPSP, String eventosRAIA, boolean painelRAIA)
	{
		EventoVO eventoVO = null;

		try
		{
			Integer codigoHistorico = (Integer) resultSet[0]; //resultSet.getString("CD_HISTORICO");
			String codigoEvento = (String) resultSet[1]; //resultSet.getString("CD_EVENTO");
			Short prioridade = (Short) resultSet[2]; //resultSet.getLong("NU_PRIORIDADE");
			Integer codigoCliente = (Integer) resultSet[3]; //resultSet.getString("CD_CLIENTE");
			String nomeViatura = (String) resultSet[4]; //resultSet.getString("NM_VIATURA");
			Short status = (Short) resultSet[5]; //resultSet.getInt("FG_STATUS");
			String nomeEvento = (String) resultSet[6]; //resultSet.getString("NM_FRASE_EVENTO");
			Integer codigoViatura = (Integer) resultSet[7]; //resultSet.getString("CD_VIATURA");
			Timestamp dataRecebimento = (Timestamp) resultSet[8]; //resultSet.getTimestamp("DT_RECEBIDO");
			Timestamp dataDeslocamentoViatura = (Timestamp) resultSet[9]; //resultSet.getTimestamp("DT_ESPERA_DESLOCAMENTO");
			Timestamp dataViaturaNoLocal = (Timestamp) resultSet[10]; //resultSet.getTimestamp("DT_VIATURA_NO_LOCAL");
			String codigoCentral = (String) resultSet[11]; //resultSet.getString("ID_CENTRAL");
			String particao = (String) resultSet[12]; //resultSet.getString("PARTICAO");
			String fantasia = (String) resultSet[13]; //resultSet.getString("FANTASIA");
			String razao = (String) resultSet[14]; //resultSet.getString("RAZAO");
			String endereco = (String) resultSet[15]; //resultSet.getString("ENDERECO");
			String nomeCidade = (String) resultSet[16]; //resultSet.getString("NOMECIDADE");
			String bairro = (String) resultSet[17]; //resultSet.getString("NOMEBAIRRO");
			String observacaoFechamento = (String) resultSet[18]; //resultSet.getString("TX_OBSERVACAO_FECHAMENTO"); // log
			String observavaoGerente = (String) resultSet[19]; //resultSet.getString("TX_OBSERVACAO_GERENTE"); // log gerencia
			String telefone = (String) resultSet[20]; //resultSet.getString("TELEFONE");
			String latitude = (String) resultSet[21]; //resultSet.getFloat("NU_LATITUDE");
			String longitude = (String) resultSet[22]; //resultSet.getFloat("NU_LONGITUDE");
			String placa = (String) resultSet[23]; //resultSet.getString("NM_PLACA");
			Integer origemNoLocal = (Integer) resultSet[24]; //CD_USUARIO_VIATURA_NO_LOCAL
			Timestamp deslocamentoIniciado = (Timestamp) resultSet[25]; //resultSet.getTimestamp("DT_VIATURA_DESLOCAMENTO");
			String complemento = (String) resultSet[26]; //resultSet.getString("COMPLEMENTO");
			String nomeEstado = (String) resultSet[27]; //resultSet.getString("NOMEESTADO");
			String nomeColaborador = (String) resultSet[28]; //resultSet.getString("NOMECOLABORADORFDSL");
			String nomeColaboradorIniciado = (String) resultSet[29]; //resultSet.getString("NOMECOLABORADORINI");
			String nomeColaboradorLocal = (String) resultSet[30]; //resultSet.getString("NOMECOLABORADORLOC");
			String nomeRota = (String) resultSet[31]; //resultSet.getString("nm_rota");
			String empresa = String.valueOf(resultSet[32]); //resultSet.getString("ID_EMPRESA");
			Timestamp dataExibido = (Timestamp) resultSet[33]; //resultSet.getTimestamp("DT_EXIBIDO_VIATURA");
			Boolean chave = (Boolean) resultSet[34]; //resultSet.getString("CHAVE");
			Boolean cachorro = (Boolean) resultSet[35]; //resultSet.getString("CACHORRO");
			Boolean arma = (Boolean) resultSet[36]; //resultSet.getString("ARMA");
			String observacao = (String) resultSet[37]; //resultSet.getString("OBSERVACAO");
			String referencia = (String) resultSet[38]; //resultSet.getString("REFERENCIA");
			String cuc = (String) resultSet[39]; //resultSet.getString("CD_CODE");

			if (NeoUtils.safeIsNotNull(nomeColaborador) && !nomeColaborador.isEmpty())
				nomeColaborador = "(" + nomeColaborador + ")";
			if (NeoUtils.safeIsNotNull(nomeColaboradorIniciado) && !nomeColaboradorIniciado.isEmpty())
				nomeColaboradorIniciado = "(" + nomeColaboradorIniciado + ")";
			if (NeoUtils.safeIsNotNull(nomeColaboradorLocal) && !nomeColaboradorLocal.isEmpty())
				nomeColaboradorLocal = "(" + nomeColaboradorLocal + ")";

			eventoVO = new EventoVO();
			
			if (eventosDPSP != null && eventosDPSP.equalsIgnoreCase("true")){
				if (observacaoFechamento != null && observacaoFechamento.contains("#recebidovector")){
					return null;
				}				
			}
			
			eventoVO.setCodigoHistorico((codigoHistorico != null) ? codigoHistorico.toString() : "");
			eventoVO.setCodigoEvento(codigoEvento);
			eventoVO.setPrioridade((prioridade != null) ? prioridade.longValue() : null);
			eventoVO.setCodigoCliente((codigoCliente != null) ? codigoCliente.toString() : "");
			eventoVO.setNomeViatura(nomeViatura);
			eventoVO.setStatus(status.intValue());
			eventoVO.setNomeEvento(nomeEvento);
			eventoVO.setCodigoViatura((codigoViatura != null) ? codigoViatura.toString() : "");
			eventoVO.setEmpresa(empresa);

			String dataRecebimentoStr = NeoDateUtils.safeDateFormat(dataRecebimento, "dd/MM/yyyy HH:mm:ss");
			eventoVO.setDataRecebimento(dataRecebimentoStr);
			String dataDeslocamentoViaturaStr = " ";
			if (NeoUtils.safeIsNotNull(dataDeslocamentoViatura))
			{
				dataDeslocamentoViaturaStr = NeoDateUtils.safeDateFormat(dataDeslocamentoViatura, "dd/MM/yyyy HH:mm:ss");
				eventoVO.setDataDeslocamentoViatura(dataDeslocamentoViaturaStr);
			}
			String dataViaturaNoLocalStr = " ";
			if (NeoUtils.safeIsNotNull(dataDeslocamentoViatura))
			{
				dataViaturaNoLocalStr = NeoDateUtils.safeDateFormat(dataViaturaNoLocal, "dd/MM/yyyy HH:mm:ss");
				eventoVO.setDataViaturaNoLocal(dataViaturaNoLocalStr);

			}
			String dataDeslocamentoIniciadoStr = " ";
			if (NeoUtils.safeIsNotNull(deslocamentoIniciado))
				dataDeslocamentoIniciadoStr = NeoDateUtils.safeDateFormat(deslocamentoIniciado, "dd/MM/yyyy HH:mm:ss");
			if (NeoUtils.safeIsNull(complemento) || complemento.isEmpty())
				complemento = " ";
			String dataExibidoVtr = " ";
			if (NeoUtils.safeIsNotNull(dataExibido))
				dataExibidoVtr = NeoDateUtils.safeDateFormat(dataExibido, "dd/MM/yyyy HH:mm:ss");
			eventoVO.setCodigoCentral(codigoCentral);
			eventoVO.setParticao(particao);
			eventoVO.setFantasia(fantasia);
			eventoVO.setRazao(razao);
			eventoVO.setEndereco(endereco);
			eventoVO.setNomeCidade(nomeCidade);
			eventoVO.setBairro(bairro);
			eventoVO.setObservacaoFechamento(observacaoFechamento);
			eventoVO.setObservavaoGerente(observavaoGerente);
			eventoVO.setTelefone(telefone);
			eventoVO.setLatitude(Float.valueOf(latitude));
			eventoVO.setLongitude(Float.valueOf(longitude));
			if (placa == null || placa.isEmpty())
				placa = verificaCor(String.valueOf(codigoHistorico));
			else
				placa += ":0";
			eventoVO.setPlaca(placa);
			NeoUser neoUser = PortalUtil.getCurrentUser();
			Boolean readOnly = false;
			if (
				(
				 (
					 eventosGolf != null && eventosGolf.equalsIgnoreCase("true")
				 )
				|| 
				 (
					 eventosDPSP != null && eventosDPSP.equalsIgnoreCase("true")
				 )
				)
				&& neoUser != null && neoUser.getPapers() != null){
				for (NeoPaper neoPaper : neoUser.getPapers()) {
					if (neoPaper.getCode().equalsIgnoreCase("somenteVisualizacaoMapa")){
						readOnly = true;
						break;
					}
				}
			}
			if (painelRAIA)
				readOnly = true;
			NeoObject corPrioridadeEventoObj = null;
			if (nomeRota != null && nomeRota.contains("PARC - GOLF") 
				&& eventosGolf != null && eventosGolf.equalsIgnoreCase("true")){
				if (observacaoFechamento != null && observacaoFechamento.contains("#AtendimentoFinalizado")){
					corPrioridadeEventoObj = this.getCorPrioridadeEvento("GOLF");
					eventoVO.setNomeImagem("evento_amarelo");
				}else if (observacaoFechamento != null && observacaoFechamento.contains("#deslocadoGolf")){
					corPrioridadeEventoObj = this.getCorPrioridadeEvento("5");
					eventoVO.setNomeImagem("evento_azul");
				}else{
					corPrioridadeEventoObj = this.getCorPrioridadeEvento(codigoEvento);	
				}
			}else{
				if (eventosRAIA != null && eventosRAIA.equalsIgnoreCase("true")){
					if (observacaoFechamento != null && observacaoFechamento.contains("#deslocar")){
						corPrioridadeEventoObj = this.getCorPrioridadeEvento("2");
						eventoVO.setNomeImagem("evento_alerta");
					}else if (observacaoFechamento != null && observacaoFechamento.contains("#finalizar")){
						corPrioridadeEventoObj = this.getCorPrioridadeEvento("3");
						eventoVO.setNomeImagem("evento_verde");
					}else{
						corPrioridadeEventoObj = this.getCorPrioridadeEvento(codigoEvento);	
					}
				}else{
					corPrioridadeEventoObj = this.getCorPrioridadeEvento(codigoEvento);
				}
			}			
			
			if (painelRAIA) {
				if (status.intValue() == 1 && observacaoFechamento.contains("#liberarDeslocamento")) {
					eventoVO.setNomeImagem("evento_amareloffb300");
				}else if (observacaoFechamento.contains("#finalizarAtendimento")) {
					eventoVO.setNomeImagem("evento_verdeEscuro");
				}else if (status.intValue() == 3 && observacaoFechamento.contains("#localSemAlteracao")) {
					eventoVO.setNomeImagem("evento_verde");
				}else if (status.intValue() == 3 && observacaoFechamento.contains("#localComAlteracao")) {
					eventoVO.setNomeImagem("evento_alerta");
				}else if (status.intValue() == 2 || status.intValue() == 3 || status.intValue() == 9) {
					eventoVO.setNomeImagem("evento_lilas");
				}
				
			}
			
			
			//se nao encontrar pelo codigo do evento busca pelo codigo da prioridade
			if (corPrioridadeEventoObj == null && prioridade != null)
			{
				corPrioridadeEventoObj = this.getCorPrioridadeEvento(prioridade.toString());
			}

			if (corPrioridadeEventoObj != null)
			{
				EntityWrapper wrapper = new EntityWrapper(corPrioridadeEventoObj);

				String corFundo = (String) wrapper.findValue("corFundo");
				String corTexto = (String) wrapper.findValue("corTexto");

				if (corFundo != null)
				{
					eventoVO.setCorPrioridade(corFundo);
				}

				if (corTexto != null)
				{
					eventoVO.setCorTexto(corTexto);
				}
			}

			String deslocamentoViatura = " ";
			if (NeoUtils.safeIsNotNull(dataDeslocamentoViaturaStr))
			{
				deslocamentoViatura = dataDeslocamentoViaturaStr;
			}
			if (NeoUtils.safeIsNull(dataDeslocamentoViaturaStr) || dataDeslocamentoIniciadoStr.equals(deslocamentoViatura) || (status != null && status.intValue() == 2))
			{
				dataDeslocamentoIniciadoStr = " ";
				nomeColaboradorIniciado = " ";
			}
			String viaturaNoLocal = " ";

			if (dataViaturaNoLocalStr != null)
			{
				viaturaNoLocal = dataViaturaNoLocalStr;
			}
			if (NeoUtils.safeIsNull(nomeColaboradorLocal))
			{
				nomeColaboradorLocal = " ";
			}
			if (observacaoFechamento == null || observacaoFechamento.isEmpty())
			{
				observacaoFechamento = "Vazio";
			}
			else
			{
				observacaoFechamento = "<li>" + observacaoFechamento;
				observacaoFechamento = observacaoFechamento.replace("\n", "<br><li>");
			}
			if (observavaoGerente == null || observavaoGerente.isEmpty())
			{
				observavaoGerente = "Vazio";
			}
			else
			{
				observavaoGerente = "<li>" + observavaoGerente;
				observavaoGerente = observavaoGerente.replace("\n", "<br><li>");
			}
			String chaveStr = "";
			String cachorroStr = "";
			String armaStr = "";
			if (chave != null && chave)
				chaveStr = "<img src=\"images/key.png\"  />";
			if (cachorro != null && cachorro)
				cachorroStr = "<img src=\"images/dog.png\"  />";
			if (arma != null && arma)
				armaStr = "<img src=\"images/gun.png\"  />";

			if (NeoUtils.safeIsNull(deslocamentoViatura))
				deslocamentoViatura = " ";

			if (NeoUtils.safeIsNull(nomeColaborador))
				nomeColaborador = " ";
			else if (nomeColaborador.contains("CM"))
				nomeColaborador = "Operação realizada pela CM, operador: " + nomeColaborador;
			else if (nomeColaborador.contains("VTR"))
				nomeColaborador = "Operação realizada em campo, agente: : " + nomeColaborador;
			else if (nomeColaborador.contains("fusion"))
				nomeColaborador = "Operação realizada pelo Fusion, usuário padrão: COLABORADOR_PADRÃO (Código 9999)";

			if (NeoUtils.safeIsNull(dataDeslocamentoIniciadoStr))
				dataDeslocamentoIniciadoStr = " ";

			if (NeoUtils.safeIsNull(nomeColaboradorIniciado))
				nomeColaboradorIniciado = " ";
			else if (nomeColaboradorIniciado.contains("CM"))
				nomeColaboradorIniciado = "Operação realizada pela CM, operador: " + nomeColaboradorIniciado;
			else if (nomeColaboradorIniciado.contains("VTR"))
				nomeColaboradorIniciado = "Operação realizada em campo, agente: " + nomeColaboradorIniciado;
			else if (nomeColaboradorIniciado.contains("fusion"))
				nomeColaboradorIniciado = "Operação realizada pelo Fusion, usuário padrão: COLABORADOR_PADRÃO (Código 9999)";

			if (NeoUtils.safeIsNull(viaturaNoLocal))
				viaturaNoLocal = " ";

			if (NeoUtils.safeIsNull(nomeColaboradorLocal))
				nomeColaboradorLocal = " ";
			else if (nomeColaboradorLocal.contains("CM"))
				nomeColaboradorLocal = "Operação realizada pela CM, operador: " + nomeColaboradorLocal;
			else if (nomeColaboradorLocal.contains("VTR"))
				nomeColaboradorLocal = "Operação realizada em campo, agente: " + nomeColaboradorLocal;
			else if (nomeColaboradorLocal.contains("fusion"))
				nomeColaboradorLocal = "Operação realizada pelo Fusion, usuário padrão: COLABORADOR_PADRÃO (Código 9999)";

			StringBuilder contentString = new StringBuilder();
			contentString.append("<div id='content' style=\"width:550px; height:280px\" >");
			contentString.append("<div id='bodyContent'><span class=\"gm-style-iw\"><b>Cliente:</b> </span> <span class=\"gm-addr\">");
			contentString.append(codigoCentral + " [" + particao + "] " + fantasia + "&nbsp &nbsp" + chaveStr + "&nbsp" + cachorroStr + "&nbsp" + armaStr + "&nbsp ");
			if (!readOnly){
				contentString.append("<a tooltip=\"<b>Referência:</b></br>" + referencia + "<br><br><b>Observação:</b><br>" + observacao + "\"> <img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/doc_link.png\"  /></a>");
			}
			contentString.append(" </span>");
			if (!readOnly){
				contentString.append("&nbsp&nbsp<a href=\"javascript:atualizaContatos(" + eventoVO.getCodigoCliente() + ", " + eventoVO.getCodigoHistorico() + ")\" title='Informações do cliente.' ><img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/home_16x16.png\"  /></a>");
			}
			contentString.append("<br> <span class=\"gm-style-iw\"><b>Razão Social:</b> </span> <span class=\"gm-addr\">" + razao + "</span>" + "<br>");
			contentString.append("<span class=\"gm-style-iw\"><b>Endereço:</b> </span> <span class=\"gm-addr\">" + endereco);
			contentString.append("  " + complemento + "</span><br><span class=\"gm-style-iw\"><b>Bairro/Cidade:</b> </span> <span class=\"gm-addr\">");
			contentString.append(bairro + " " + nomeCidade + " " + nomeEstado + "</span><br><span class=\"gm-style-iw\"><b>Evento:</b> </span> <span class=\"gm-addr\">");
			contentString.append(codigoEvento + " - " + nomeEvento + " - " + cuc + "</span><br><span class=\"gm-style-iw\"><b>Rota:</b> </span> <span class=\"gm-addr\">" + nomeRota + "</span><br>");
			contentString.append("<span class=\"gm-style-iw\"><b>Recebido:</b> <span class=\"gm-addr\">" + dataRecebimentoStr + "</span><br><span class=\"gm-style-iw\"><b>Deslocado:</b> </span> <span class=\"gm-addr\">" + deslocamentoViatura);

			if (NeoUtils.safeIsNotNull(nomeColaborador) && !nomeColaborador.isEmpty() && nomeColaborador.contains("CM") && (!deslocamentoViatura.isEmpty()))
				contentString.append("&nbsp<span class=\"gm-style-iw\">" + (nomeViatura == null ? "" : "(" + nomeViatura + ")") + "</span>&nbsp <a tooltip=\"<b>Responsável:</b></br>" + (nomeColaborador == null ? "" : nomeColaborador) + "\"> <img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/home_16x16.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaborador) && !nomeColaborador.isEmpty() && nomeColaborador.contains("VTR") && (!deslocamentoViatura.isEmpty()))
				contentString.append("&nbsp<span class=\"gm-style-iw\" >" + (nomeViatura == null ? "" : "(" + nomeViatura + ")") + "</span>&nbsp  <a tooltip=\"<b>Responsável:</b></br>" + (nomeColaborador == null ? "" : nomeColaborador) + "\"> <img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/papel_16x16-trans.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaborador) && !nomeColaborador.isEmpty() && (!deslocamentoViatura.isEmpty()))
				contentString.append("&nbsp<span class=\"gm-style-iw\" >" + (nomeViatura == null ? "" : "(" + nomeViatura + ")") + "</span>&nbsp <a tooltip=\"<b>Responsável:</b></br>" + (nomeColaborador == null ? "" : nomeColaborador) + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/process_running.png\"  /></a></span>");
			contentString.append("<br><span class=\"gm-style-iw\"><b>Exibido:</b> </span> <span class=\"gm-addr\">" + dataExibidoVtr + "<br><span class=\"gm-style-iw\"><b>Iniciado:</b> <span class=\"gm-addr\">" + dataDeslocamentoIniciadoStr);

			if (NeoUtils.safeIsNotNull(nomeColaboradorIniciado) && !nomeColaboradorIniciado.isEmpty() && nomeColaboradorIniciado.contains("CM") && (dataDeslocamentoIniciadoStr != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a tooltip=\"<b>Responsável:</b></br>" + nomeColaboradorIniciado + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/home_16x16.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaboradorIniciado) && !nomeColaboradorIniciado.isEmpty() && nomeColaboradorIniciado.contains("VTR") && (dataDeslocamentoIniciadoStr != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a tooltip=\"<b>Responsável:</b></br>" + nomeColaboradorIniciado + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/papel_16x16-trans.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaboradorIniciado) && !nomeColaboradorIniciado.isEmpty() && (dataDeslocamentoIniciadoStr != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a tooltip=\"<b>Responsável:</b></br>" + nomeColaboradorIniciado + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/process_running.png\"  /></a></span>");
			contentString.append("<br><span class=\"gm-style-iw\"><b>No Local:</b> </span> <span class=\"gm-addr\">" + viaturaNoLocal);

			if (NeoUtils.safeIsNotNull(nomeColaboradorLocal) && !nomeColaboradorLocal.isEmpty() && nomeColaboradorLocal.contains("CM") && (viaturaNoLocal != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a tooltip=\"<b>Responsável:</b></br>" + nomeColaboradorLocal + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/home_16x16.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaboradorLocal) && !nomeColaboradorLocal.isEmpty() && nomeColaboradorLocal.contains("VTR") && (viaturaNoLocal != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a tooltip=\"<b>Responsável:</b></br>" + nomeColaboradorLocal + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/papel_16x16-trans.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaboradorLocal) && !nomeColaboradorLocal.isEmpty() && (viaturaNoLocal != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a tooltip=\"<b>Responsável:</b></br>" + nomeColaboradorLocal + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/process_running.png\"  /></a></span>");
			if (origemNoLocal != null && origemNoLocal == 9999)
			{
				contentString.append("&nbsp &nbsp<img style=\"border : none\" src=\"images/peloAA.png\"/>");
			}
			contentString.append("<br>");
			if (codigoHistorico != null)
				contentString.append(listaPosicoes(String.valueOf(codigoHistorico)));
			NeoUser user = PortalUtil.getCurrentUser();
			if (user != null && user.isAdm())
			{
				contentString.append("<a  id=\"div2\"  class=\"easyui-tooltip\"  onmouseenter=\"javascript:getLogEvento('" + codigoCliente + "',this);\" ><img style=\"border : none\" src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");
			}
			contentString.append("&nbsp</span> <a  href=\"javascript:salvaLogEvento(" + codigoHistorico + ")\" tooltip=\"<b>Log:</b></br>" + observacaoFechamento + "\">");
			contentString.append("<img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");
			contentString.append("&nbsp &nbsp<a href=\"javascript:salvaLogEventoGerencia(" + codigoHistorico + ")\" tooltip=\"<b>Log Gerência:</b></br>" + observavaoGerente + "\">");
			contentString.append("<img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/doc_link.png\"/></a>");

			if (placa != null && !placa.isEmpty())
			{
				if (atualizaPosicao)
				{
					contentString.append("&nbsp &nbsp");
					contentString.append("<a  tooltip=\"<b>Atualizar Posição</b>\" href=\"javascript:atualizaPosicaoCliente(" + codigoCliente + ",'" + placa + "')\"><img style=\"border : none\" src=\"images/atualizarPosicao.jpg\"/></a>");
				}

				//exibe a rota
				/*
				 * contentString += "&nbsp &nbsp";
				 * contentString +=
				 * "<a tooltip=\"<b>Exibir Rota</b>\" href=\"javascript:controlGoogleRouteVisualization('"
				 * + eventoVO.getCodigoHistorico() +
				 * "')\"><img style=\"border : none\" src=\"images/atualizarPosicao.jpg\"/></a>";
				 */
				//				contentString.append(" &nbsp &nbsp ");
				//				contentString.append(" <a tooltip=\"<b>Exibir Rota</b>\" href=\"javascript:controlGoogleRouteVisualization(");
				//				contentString.append(""+eventoVO.getCodigoHistorico()+ "");
				//				contentString.append( ")\"><img style=\"border : none\" src=\"images/atualizarPosicao.jpg\"/></a>");

			}

			if (status.intValue() == 9 && !readOnly)
			{
				contentString.append("&nbsp&nbsp<a href=\"javascript:viaturaNoLocal(" + codigoHistorico + ")\"  title='Chegada ao local!' ><img style=\"border : none\" src=\"images/local.png\"/></a>");
			}

			if (status.intValue() == 2 && !readOnly)
			{
				contentString.append("&nbsp &nbsp<a href=\"javascript:deslocarEventoViaturaIniciado(" + codigoHistorico + ")\"  title='Iniciar o deslocamento!' ><img style=\"border : none\" src=\"images/settings.png\"/></a>");
			}

			if (status.intValue() != 1 && !readOnly)
			{
				contentString.append("&nbsp&nbsp<a href=\"javascript:cancelarEventoIniciado(" + codigoHistorico + ")\" title='Cancelar deslocamento!' ><img style=\"border : none\" src=\"images/remove.png\"/></a>");

			}
			
			if (painelRAIA) {
				if (status.intValue() == 1 && !observacaoFechamento.contains("#liberarDeslocamento") && !observacaoFechamento.contains("#finalizarAtendimento")) {
					contentString.append("&nbsp&nbsp<a href=\"javascript:liberarDeslocamentoRaia(" + codigoHistorico + ")\" title='Solicitar deslocamento!' ><img style=\"border : none\" src=\"images/solicitarDeslocamento.png\"/></a>");
				}
				
				if (status.intValue() == 3 && !observacaoFechamento.contains("#localSemAlteracao") && !observacaoFechamento.contains("#localComAlteracao") && !observacaoFechamento.contains("#finalizarAtendimento")) {
					contentString.append("&nbsp&nbsp<a href=\"javascript:localSemAlteracaoRAIA(" + codigoHistorico + ")\" title='Local sem alteração!' ><img style=\"border : none\" src=\"images/localSemAlteracao.png\"/></a>");
					contentString.append("&nbsp&nbsp<a href=\"javascript:localComAlteracaoRAIA(" + codigoHistorico + ")\" title='Local com alteração!' ><img style=\"border : none\" src=\"images/localComAlteracao.png\"/></a>");
				}
				
				if (!observacaoFechamento.contains("#finalizarAtendimento")) {
					contentString.append("&nbsp&nbsp<a href=\"javascript:solicitarFinalizacaoRAIA(" + codigoHistorico + ")\" title='Solicitar finalização!' ><img style=\"border : none\" src=\"images/solicitarFinalizacao.png\"/></a>");
				}
				
			}
			
			if (status.intValue() == 1  && !readOnly)
			{
				String nmRotaStr = "";
				if (nomeRota.length() > 8 && nomeRota.substring(8, 8).contains("-"))
					nmRotaStr = nomeRota.substring(0, 7);
				else if (nomeRota.length() > 8)
					nmRotaStr = nomeRota.substring(0, 8);

				String cbVtrs = "<select name='cbViatura' id='cbViatura' style='width:400px;'>";
				String regionalStr = (String) nomeRota.subSequence(0, 3);
				StringBuilder builder = new StringBuilder(getMotoritas(regionalStr));
				String[] combobox = builder.toString().split("::");
				Collection<String> listaCombo = new ArrayList<String>();
				for (String stringCombo : combobox)
				{
					if (stringCombo.contains(nmRotaStr) && !listaCombo.contains(stringCombo))
					{
						listaCombo.add(stringCombo);
					}
				}

				if (listaCombo.isEmpty())
					cbVtrs += "<option value='0'  selected='selected' disabled='disabled' style='display: none'>Selecione...</option>";
				nmRotaStr = nomeRota.substring(0, 3);
				for (String stringCombo : combobox)
				{
					if (stringCombo.contains(nmRotaStr) && !listaCombo.contains(stringCombo))
					{
						listaCombo.add(stringCombo);
					}
				}
				for (String resultCombo : listaCombo)
				{
					cbVtrs += resultCombo;
				}

				cbVtrs += "</select>";
				contentString.append("<br><span class=\"gm-style-iw\"><b>Deslocar  -VTR:</b> </span> " + cbVtrs + "&nbsp &nbsp");
				String nomeCliente = eventoVO.getCodigoCentral() + "[" + eventoVO.getParticao() + "] " + eventoVO.getFantasia();

				contentString.append("<a title='Deslocar evento para fila!'   href=\"javascript:deslocarEventoFila(" + codigoHistorico + ",'" + retiraCaracteresAcentuados(nomeCliente) + "')\"><img style=\"border : none\" src=\"images/plus.png\"/></a>");
				contentString.append("&nbsp&nbsp<a href=\"javascript:deslocarEventoViatura(" + codigoHistorico + ",'" + retiraCaracteresAcentuados(nomeCliente) + "');\"  title='Deslocar evento para viatura!' ><img style=\"border : none\" src=\"images/play.png\"/></a>");

			}

			//			String cbTipoFechamento = "<br><span class=\"gm-style-iw\"><b>Fechar Evento:</b> </span>  <select name='cbTipoFechamento' id='cbTipoFechamento' style='width:400px;'>";
			//			cbTipoFechamento += comboMotivoAlarme;
			//			cbTipoFechamento += "</select>";
			if (!readOnly){
				String cbTipoFechamento = "<br><span id=\"cbTipoFechamento" + eventoVO.getCodigoHistorico() + "\">Clique no evento para ter a opção de fechar evento. </span>";
				contentString.append(cbTipoFechamento);
				contentString.append("&nbsp &nbsp<a href=\"javascript:fecharEventoNoLocal(" + codigoHistorico + ");\"  title='Fechar evento!' ><img style=\"border : none\" src=\"images/delete.png\"/></a>");
				contentString.append("&nbsp &nbsp<a href=\"javascript:removerEventoEmEspera(" + codigoHistorico + ");\"  title='Remover evento fila!' ><img style=\"border : none\" src=\"images/remove.png\"/></a>");
				contentString.append("</div></div>");
			}
			eventoVO.setInfoWindowContent(contentString.toString());
						
			if (eventoVO.getNomeViatura() != null && eventoVO.getNomeViatura().toUpperCase().contains("PARC")) {
				if (codigoHistorico != null) {				
					eventoVO.setAtrasoDeslocamento(eventoAtrasoDeslocamento(codigoHistorico.longValue()));
					boolean adiar = false;
					if (observacaoFechamento != null && observacaoFechamento.toLowerCase().contains("#60minutos"))
						adiar = true;
					eventoVO.setAtrasoFechamento(eventoAtrasoFechamento(codigoHistorico.longValue(), adiar));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return eventoVO;
		}

	}
	
	private boolean eventoAtrasoDeslocamento(Long cdHistorico) {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		try {
		    conn = PersistEngine.getConnection("SIGMA90");
		    
		    sql.append(" SELECT 1 FROM HISTORICO H "); 
		    sql.append(" WHERE H.FG_STATUS = 2 ");
		    sql.append(" AND H.DT_ESPERA_DESLOCAMENTO < DATEADD(MINUTE, -10, GETDATE()) "); 
		    sql.append(" AND H.CD_HISTORICO = ? ");
			
		    pstm = conn.prepareStatement(sql.toString());
		    
		    pstm.setLong(1, cdHistorico);

		    rs = pstm.executeQuery();

		    while (rs.next()) {
		    	return true;
		    }

		} catch (SQLException e) {
		    e.printStackTrace();
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		return false;
	}
	
	private boolean eventoAtrasoFechamento(Long cdHistorico, boolean adiar) {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		try {
		    conn = PersistEngine.getConnection("SIGMA90");
		    Integer tempoEspera = 30;
		    if (adiar)
		    	tempoEspera = 60;
		    sql.append(" SELECT 1 FROM HISTORICO H "); 
		    sql.append(" WHERE H.DT_VIATURA_DESLOCAMENTO > DT_ESPERA_DESLOCAMENTO ");
		    sql.append(" AND H.DT_VIATURA_DESLOCAMENTO < DATEADD(MINUTE, -"+tempoEspera+", GETDATE()) AND H.DT_VIATURA_NO_LOCAL IS NULL ");
		    sql.append(" AND H.CD_HISTORICO = ? ");
			
		    pstm = conn.prepareStatement(sql.toString());
		    
		    pstm.setLong(1, cdHistorico);

		    rs = pstm.executeQuery();

		    while (rs.next()) {
		    	return true;
		    }

		} catch (SQLException e) {
		    e.printStackTrace();
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private void getOS(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String codigoTecnico, Boolean atualizaPosicao)
	{
		Collection<EventoVO> eventoVOs = null;
		try
		{

			String nomeFonteDados = "SIGMA90";
			//Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT col.NM_COLABORADOR , def.DESCRICAODEFEITO, os.ID_ORDEM, os.ABERTURA, os.DEFEITO, os.ID_INSTALADOR, c.ID_EMPRESA, c.ID_CENTRAL,");
			sql.append(" c.PARTICAO , c.CD_CLIENTE, c.FANTASIA, c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, cid.NOME AS NOMECIDADE, os.FECHAMENTO,");
			sql.append(" os.FECHADO, osh.DATAINICIOEXECUCAO, osh.DATAFIMEXECUCAO, os.EXECUTADO, sol.NM_DESCRICAO, CD_COLABORADOR,");
			sql.append(" mp.NM_DESCRICAO AS NOMEMOTIVOPAUSA, p.TX_OBSERVACAO, p.CD_MOTIVO_PAUSA, os.DATAAGENDADA, c.NU_LATITUDE, c.NU_LONGITUDE");
			sql.append(" FROM dbORDEM os  " + "INNER JOIN COLABORADOR col  ON col.CD_COLABORADOR = os.ID_INSTALADOR");
			sql.append(" INNER JOIN OS_SOLICITANTE sol  ON sol.CD_OS_SOLICITANTE = os.CD_OS_SOLICITANTE");
			sql.append(" INNER JOIN OSDEFEITO def  ON def.IDOSDEFEITO = os.IDOSDEFEITO");
			sql.append(" INNER JOIN dbCENTRAL c  ON c.CD_CLIENTE = os.CD_CLIENTE");
			sql.append(" LEFT JOIN dbBAIRRO bai  ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE");
			sql.append(" LEFT JOIN dbCIDADE cid  ON cid.ID_CIDADE = c.ID_CIDADE");
			sql.append(" LEFT JOIN OSHISTORICO osh  ON osh.ID_ORDEM = os.ID_ORDEM AND osh.IDOSHISTORICO = (SELECT MIN(IDOSHISTORICO) FROM OSHISTORICO WHERE ID_ORDEM = os.ID_ORDEM)");
			sql.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA)");
			sql.append(" FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO )	");
			sql.append(" LEFT OUTER JOIN MOTIVO_PAUSA mp ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA");
			sql.append(" WHERE col.NM_COLABORADOR NOT LIKE '%TERC%' AND ( (os.FECHADO IN (0, 2, 3) ) OR");
			sql.append(" (os.FECHADO = 1 AND osh.DATAINICIOEXECUCAO IS NOT NULL AND os.FECHAMENTO >= (CAST((STR( YEAR( GETDATE() ) ) + '/' + STR( MONTH( GETDATE() ) ) + '/' + STR( DAY( GETDATE() ) ))AS DATETIME)) ) ) AND c.NU_LATITUDE is not null AND c.NU_LONGITUDE is not null");

			if (codigoTecnico != null)
			{
				sql.append(" AND os.ID_INSTALADOR IN (" + codigoTecnico + ")");
			}

			eventoVOs = new ArrayList<EventoVO>();

			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();

			for (Object result : resultList)
			{
				if (result != null)
				{
					EventoVO osVO = this.composeOsVO((Object[]) result, atualizaPosicao);
					if (osVO != null)
					{
						eventoVOs.add(osVO);
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Gson gson = new Gson();
			String eventosJSON = gson.toJson(eventoVOs);

			//log.warn("Tecnico: "+codigoTecnico+" - Os: "+eventoVOs.size());

			out.print(eventosJSON);
		}

	}

	@SuppressWarnings("unchecked")
	private EventoVO composeOsVO(Object[] resultSet, Boolean atualizaPosicao)
	{
		EventoVO eventoVO = null;

		try
		{
			eventoVO = new EventoVO();

			//String nomeColaborador = resultSet[0] // resultSet.getString("NM_COLABORADOR");
			String descricaoDefeito = (String) resultSet[1]; //resultSet.getString("DESCRICAODEFEITO");
			Integer codigoOrdemServico = (Integer) resultSet[2]; //resultSet.getString("ID_ORDEM");
			Timestamp dataAbertura = (Timestamp) resultSet[3]; //resultSet.getTimestamp("ABERTURA");

			/**
			 * @author orsegups lucas.avila - alterado Clob Hibernate.
			 * @date 08/07/2015
			 */
			Clob defeitoClob = (Clob) resultSet[4]; //resultSet.getString("DEFEITO");
			Integer codigoInstalador = (Integer) resultSet[5]; //resultSet.getString("ID_INSTALADOR");
			//Long codigoEmpresa =  resultSet[6]; //;
			String codigoCentral = (String) resultSet[7]; //resultSet.getString("ID_CENTRAL");
			String particao = (String) resultSet[8]; //resultSet.getString("PARTICAO");
			Integer codigoCliente = (Integer) resultSet[9]; // resultSet.getString("CD_CLIENTE");
			String fantasia = (String) resultSet[10]; // resultSet.getString("FANTASIA");
			String razao = (String) resultSet[11]; // resultSet.getString("RAZAO");
			String endereco = (String) resultSet[12]; //;
			String nomeBairro = (String) resultSet[13]; //;
			String nomeCidade = (String) resultSet[14]; //;
			Timestamp dataFechamento = (Timestamp) resultSet[15]; // resultSet.getTimestamp("FECHAMENTO");
			Byte fechado = (Byte) resultSet[16]; // resultSet.getLong("FECHADO");
			Timestamp dataInicioExecucao = (Timestamp) resultSet[17]; // resultSet.getTimestamp("DATAINICIOEXECUCAO");
			//Timestamp dataFimExecucao = resultSet[18]; // resultSet.getTimestamp("DATAFIMEXECUCAO");

			/**
			 * @author orsegups lucas.avila - alterado Clob Hibernate.
			 * @date 08/07/2015
			 */
			Clob executadoClob = (Clob) resultSet[19]; // resultSet.getString("EXECUTADO");
			String descricaoSolicitacao = (String) resultSet[20]; // resultSet.getString("NM_DESCRICAO");
			//CD_COLABORADOR =  resultSet[21]; //
			String nomeMotivoPausa = (String) resultSet[22]; //
			if (nomeMotivoPausa == null)
			{
				nomeMotivoPausa = "Motivo não informado";
			}
			String textoObservacao = (String) resultSet[23]; //resultSet.getString("TX_OBSERVACAO");
			if (textoObservacao == null || textoObservacao.isEmpty())
			{
				textoObservacao = "Vazio";
			}
			else
			{
				textoObservacao = "<li>" + textoObservacao;
				textoObservacao = textoObservacao.replace("\n", "<br><li>");
			}
			textoObservacao = nomeMotivoPausa + "<br>" + textoObservacao;
			String iconePausa = "imagens/icones_final/jobs_16x16.png";
			Integer codigoMotivoPausa = (Integer) resultSet[24]; //
			if (codigoMotivoPausa != null)
			{
				if (codigoMotivoPausa == 1)
				{
					iconePausa = "imagens/icones_final/process_start_16x16-trans.png";
				}
				else if (codigoMotivoPausa == 2)
				{
					iconePausa = "imagens/icones_final/process_finished.png";
				}
			}
			Timestamp dataAgendada = (Timestamp) resultSet[25]; //
			String latitude = (String) resultSet[26]; //resultSet.getFloat("NU_LATITUDE");
			String longitude = (String) resultSet[27]; //resultSet.getFloat("NU_LONGITUDE");

			String dataAberturaStr = OrsegupsUtils.getFormatedTimeStamp(dataAbertura);
			if (dataAberturaStr == null)
			{
				dataAberturaStr = "";
			}
			String dataInicioExecucaoStr = OrsegupsUtils.getFormatedTimeStamp(dataInicioExecucao);
			if (dataInicioExecucaoStr == null)
			{
				dataInicioExecucaoStr = "";
			}
			String dataFechamentoStr = OrsegupsUtils.getFormatedTimeStamp(dataFechamento);
			if (dataFechamentoStr == null)
			{
				dataFechamentoStr = "";
			}
			String dataAgendadaStr = OrsegupsUtils.getFormatedTimeStamp(dataAgendada);
			if (dataAgendadaStr == null)
			{
				dataAgendadaStr = "";
			}

			eventoVO.setCodigoHistorico((codigoOrdemServico != null) ? codigoOrdemServico.toString() : "");
			eventoVO.setLatitude(Float.valueOf(latitude));
			eventoVO.setLongitude(Float.valueOf(longitude));

			if (descricaoSolicitacao != null && !descricaoSolicitacao.isEmpty())
			{
				eventoVO.setNomeEvento(descricaoSolicitacao.substring(0, 1));
			}
			else
			{
				eventoVO.setNomeEvento("");
			}

			String placa = "";
			//busca a placa da viatura do tecnico que esta atendendo esta OS
			if (fechado != null && fechado.equals(new Byte("2")))
			{
				Class<?> viaturaClazz = AdapterUtils.getEntityClass("OTSViatura");

				QLGroupFilter filtroTecnico = new QLGroupFilter("AND");
				filtroTecnico.addFilter(new QLEqualsFilter("codigoDoTecnico", (codigoInstalador != null) ? codigoInstalador.toString() : ""));
				filtroTecnico.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));

				List<NeoObject> ostViaturasObj = (List<NeoObject>) PersistEngine.getObjects(viaturaClazz, filtroTecnico);

				if (ostViaturasObj != null && ostViaturasObj.size() > 0)
				{
					NeoObject viaturaObj = ostViaturasObj.get(0);

					EntityWrapper viaturaWrapper = new EntityWrapper(viaturaObj);

					if (viaturaWrapper.findValue("placa") != null)
					{
						placa = (String) viaturaWrapper.findValue("placa");
					}
				}
			}
			eventoVO.setPlaca(placa);

			String defeito = this.clobToString(defeitoClob);
			if (defeito == null || defeito.isEmpty())
			{
				defeito = "Vazio";
			}
			else
			{
				defeito = "<li>" + defeito;
				defeito = defeito.replace("\n", "<br><li>");
			}
			String executado = this.clobToString(executadoClob);
			if (executado == null || executado.isEmpty())
			{
				executado = "Vazio";
			}
			else
			{
				executado = "<li>" + executado;
				executado = executado.replace("\n", "<br><li>");
			}
			StringBuilder contentString = new StringBuilder();
			contentString.append("<div id='content' > <div id='bodyContent' >");
			contentString.append("<span class=\"gm-style-iw\">Número OS: <span class=\"gm-addr\">");
			contentString.append(codigoOrdemServico + "</span> <br> <span class=\"gm-style-iw\">Cliente: </span> <span class=\"gm-addr\">" + codigoCentral + " [" + particao + "] " + fantasia + " ( " + razao + " )" + "</span> <br> <span class=\"gm-style-iw\">Endereço: </span> <span class=\"gm-addr\">" + endereco + "</span><br>");
			contentString.append("<span class=\"gm-style-iw\">Bairro/Cidade: </span> <span class=\"gm-addr\">" + nomeBairro + " / " + nomeCidade);
			contentString.append("</span> <br> <span class=\"gm-style-iw\">Descrição: </span> <span class=\"gm-addr\">" + descricaoDefeito);
			contentString.append(" </span><br><span class=\"gm-style-iw\">Abertura: <span class=\"gm-addr\">" + dataAberturaStr + "</span><br><span class=\"gm-style-iw\">Agendada: <span class=\"gm-addr\">" + dataAgendadaStr + "</span><br>");
			contentString.append(" <span class=\"gm-style-iw\">Execução: <span class=\"gm-addr\">" + dataInicioExecucaoStr + "</span>" + "<br>" + "<span class=\"gm-style-iw\">Fechamento: </span> <span class=\"gm-addr\">" + dataFechamentoStr);
			contentString.append("</span> <br> <a tooltip=\"<b>Texto OS:</b></br>" + defeito + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");
			contentString.append("&nbsp &nbsp <a tooltip=\"<b>Fechamento OS:</b></br>" + executado + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/icon_accept.gif\"/></a>");

			if (fechado == 3)
			{
				contentString.append("&nbsp &nbsp<a tooltip=\"<b>Pausa OS:</b></br>" + textoObservacao + "\"><img src=\"" + PortalUtil.getBaseURL() + iconePausa + "\"/></a>");
			}

			if (placa != null && !placa.isEmpty() && atualizaPosicao)
			{
				contentString.append("<br>");
				contentString.append("<a class=\"gm-style-iw\" href=\"javascript:atualizaPosicaoCliente(" + codigoCliente + ",'" + placa + "')\">Atualizar Posição</a>");
			}
			contentString.append("</div>");
			contentString.append("</div>");
			eventoVO.setInfoWindowContent(contentString.toString());

			String nomeImagem = "os";
			switch (fechado.intValue())
			{
				case 0:
					nomeImagem = "os_0"; //vermelho
					if (dataAgendada != null)
					{
						GregorianCalendar gcDataAgendada = new GregorianCalendar();
						gcDataAgendada.setTimeInMillis(dataAgendada.getTime());
						GregorianCalendar deadlineAgenda = new GregorianCalendar();
						deadlineAgenda.set(GregorianCalendar.HOUR_OF_DAY, 0);
						deadlineAgenda.set(GregorianCalendar.MINUTE, 0);
						deadlineAgenda.set(GregorianCalendar.SECOND, 0);
						deadlineAgenda.set(GregorianCalendar.MILLISECOND, 0);
						if (gcDataAgendada.get(GregorianCalendar.HOUR_OF_DAY) > 0)
						{
							deadlineAgenda.set(GregorianCalendar.HOUR_OF_DAY, (new GregorianCalendar()).get(GregorianCalendar.HOUR_OF_DAY));
							deadlineAgenda.set(GregorianCalendar.MINUTE, (new GregorianCalendar()).get(GregorianCalendar.MINUTE));
						}

						if (gcDataAgendada.before(deadlineAgenda))
						{
							nomeImagem = "os_alerta"; //alerta - agendada atrasada														
						}
						else
						{
							nomeImagem = "os_4"; //azul - agendada							
						}
					}
					break;
				case 1:
					nomeImagem = "os_1"; //cinza
					break;
				case 2:
					nomeImagem = "os_2"; //verde
					break;
				case 3:
					nomeImagem = "os_3"; //laranja
					break;
				default:
					nomeImagem = "os";
					break;
			}
			eventoVO.setNomeImagem(nomeImagem);
			eventoVO.setCorPrioridade("#FFFFFF");
			eventoVO.setCorTexto("#000000");
			eventoVO.setType("os");

			/*
			 * nr OS
			 * cliente (concatenacao de campos)
			 * des defeito
			 * data abertura
			 * data execucao
			 * campo fechado -
			 * 0 - Aberta Vermelho
			 * 1 - Fecahda Cinza
			 * 2 - Execução Verde
			 * 3 - Pausada Laranja
			 */
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return eventoVO;
	}

	@SuppressWarnings("unchecked")
	private void getAtendentesTecnicos(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		Collection<AtendenteTecnicoVO> atendentesComTodosVOs = null;
		try
		{

			String regional = request.getParameter("regional");

			String nomeFonteDados = "SIGMA90";
			//Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

			String whereRegional = "";
			if (regional != null && !regional.isEmpty() && !regional.equalsIgnoreCase("null"))
			{
				whereRegional += regional + "";
			}
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT CD_COLABORADOR, NM_COLABORADOR FROM colaborador WHERE nm_colaborador LIKE '" + whereRegional + "% - TEC -%' " + "AND nm_colaborador NOT LIKE '%TERC%' " + "AND FG_INSTALADOR = 1 " + "AND FG_ATIVO_COLABORADOR = 1 ORDER BY NM_COLABORADOR");

			//Statement statement = null;

			Collection<AtendenteTecnicoVO> atendentesVOs = new ArrayList<AtendenteTecnicoVO>();
			atendentesComTodosVOs = new ArrayList<AtendenteTecnicoVO>();
			String codigosTodos = "";

			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();

			//while (resultSet.next())
			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] item = ((Object[]) result);

					Integer codigoAtendente = (Integer) item[0]; //resultSet.getString("CD_COLABORADOR");
					String nomeAtendente = (String) item[1]; //resultSet.getString("NM_COLABORADOR");
					if (regional != null && !regional.isEmpty() && !regional.equalsIgnoreCase("null"))
					{
						nomeAtendente = nomeAtendente.substring(nomeAtendente.lastIndexOf("- ") + 2, nomeAtendente.length());
						nomeAtendente = nomeAtendente.trim();
						nomeAtendente = nomeAtendente.substring(0, nomeAtendente.indexOf(" "));
					}

					AtendenteTecnicoVO atendenteTecnicoVO = new AtendenteTecnicoVO();
					atendenteTecnicoVO.setCodigoAtendente((codigoAtendente != null) ? codigoAtendente.toString() : "");
					atendenteTecnicoVO.setNomeAtendente(nomeAtendente);
					if (codigosTodos.equals(""))
					{
						codigosTodos = codigoAtendente.toString();
					}
					else
					{
						codigosTodos = codigosTodos + "," + codigoAtendente.toString();
					}

					atendentesVOs.add(atendenteTecnicoVO);
				}
			}
			AtendenteTecnicoVO atendenteTecnicoTodosVO = new AtendenteTecnicoVO();
			atendenteTecnicoTodosVO.setCodigoAtendente(codigosTodos);
			atendenteTecnicoTodosVO.setNomeAtendente(" ** TODOS **");
			atendentesComTodosVOs.add(atendenteTecnicoTodosVO);
			atendentesComTodosVOs.addAll(atendentesVOs);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Gson gson = new Gson();
			String atendentesJSON = gson.toJson(atendentesComTodosVOs);

			out.print(atendentesJSON);
		}

	}
	
	@SuppressWarnings("unchecked")
	private void getOpcoesRAIA(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		Collection<OpcaoMapaRAIAVO> opcaoMapaRaiaComTodosVOs = null;
		try
		{
		
			String nomeFonteDados = "TIDB";
			
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT ID, OPCAO_MENU FROM OPCOES_MAPA_RAIA WHERE ATIVO = 1 ");

			Collection<OpcaoMapaRAIAVO> opcoesMapaRAIAVOs = new ArrayList<OpcaoMapaRAIAVO>();
			opcaoMapaRaiaComTodosVOs = new ArrayList<OpcaoMapaRAIAVO>();

			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();

			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] item = ((Object[]) result);

					Integer id = (Integer) item[0]; //resultSet.getString("ID");
					String opcao = (String) item[1]; //resultSet.getString("OPCAO_MENU");
					
					OpcaoMapaRAIAVO opcaoMapaRAIAVO = new OpcaoMapaRAIAVO();
					opcaoMapaRAIAVO.setId((id != null) ? id.toString() : "");
					opcaoMapaRAIAVO.setOpcao(opcao);
					opcoesMapaRAIAVOs.add(opcaoMapaRAIAVO);
				}
			}
			OpcaoMapaRAIAVO opcaoMapaRAIATodosVO = new OpcaoMapaRAIAVO();
			opcaoMapaRAIATodosVO.setId("0");
			opcaoMapaRAIATodosVO.setOpcao(" ** TODOS **");
			opcaoMapaRaiaComTodosVOs.add(opcaoMapaRAIATodosVO);
			opcaoMapaRaiaComTodosVOs.addAll(opcoesMapaRAIAVOs);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Gson gson = new Gson();
			String opcoesJSON = gson.toJson(opcaoMapaRaiaComTodosVOs);

			out.print(opcoesJSON);
		}

	}
	
	@SuppressWarnings("unchecked")
	private Collection<OpcaoMapaRAIAVO> getOpcoesRAIA(String opcaoSelecionada) {
		Collection<OpcaoMapaRAIAVO> opcaoMapaRaiaComTodosVOs = null;
		try {
		
			String nomeFonteDados = "TIDB";
			
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT ID, OPCAO_MENU, EVENTOS FROM OPCOES_MAPA_RAIA WHERE ATIVO = 1 ");
			if (opcaoSelecionada != null && !opcaoSelecionada.equals("0")) {
				sql.append(" AND ID = "+opcaoSelecionada);
			}

			opcaoMapaRaiaComTodosVOs = new ArrayList<OpcaoMapaRAIAVO>();

			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();

			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] item = ((Object[]) result);

					Integer id = (Integer) item[0]; //resultSet.getString("ID");
					String opcao = (String) item[1]; //resultSet.getString("OPCAO_MENU");
					String eventos = (String) item[2]; //resultSet.getString("EVENTOS");
					
					OpcaoMapaRAIAVO opcaoMapaRAIAVO = new OpcaoMapaRAIAVO();
					opcaoMapaRAIAVO.setId((id != null) ? id.toString() : "");
					opcaoMapaRAIAVO.setOpcao(opcao);
					opcaoMapaRAIAVO.setEventos(eventos);
					opcaoMapaRaiaComTodosVOs.add(opcaoMapaRAIAVO);
				}
			}

		}catch (Exception e){
			e.printStackTrace();
		}
			
		return opcaoMapaRaiaComTodosVOs;

	}

	private void atualizaPosicaoEvento(AtualizaPosicaoVO vo, PrintWriter out)
	{
		Connection connection = null;
		PreparedStatement st = null;
		try
		{

			if (vo != null)
			{

				String codigoCliente = vo.getCodigoCliente();
				Float latitude = vo.getLatitude();
				Float longitude = vo.getLongitude();

				if (codigoCliente != null && latitude != null && longitude != null)
				{
					String nomeFonteDados = "SIGMA90";
					StringBuilder sqlSelect = new StringBuilder();
					sqlSelect.append("SELECT DT_ULTIMA_LOCALIZACAO FROM dbCENTRAL WHERE CD_CLIENTE = :cdCliente");

					Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sqlSelect.toString());
					query.setParameter("cdCliente", codigoCliente);
					@SuppressWarnings("unchecked")
					List<Object> resultList = query.getResultList();

					if (resultList != null && !resultList.isEmpty())
					{
						Object result = resultList.get(0);

						if (result != null)
						{
							Timestamp dataUltimaAtualizacao = (Timestamp) result;
							out.print("Não é possível atualizar a posição! \nCoordenada já atualizada em " + OrsegupsUtils.getFormatedTimeStamp(dataUltimaAtualizacao) + "!");
						}
						else
						{
							connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

							String sql = "UPDATE dbCENTRAL SET NU_LATITUDE = ?, NU_LONGITUDE = ?, DT_ULTIMA_LOCALIZACAO = GETDATE() WHERE CD_CLIENTE = ?";

							st = connection.prepareStatement(sql);
							//UPDATE
							st.setFloat(1, latitude);
							st.setFloat(2, longitude);
							//WHERE
							st.setString(3, codigoCliente);

							st.executeUpdate();
							out.print("Posição Atualizada!");

						}
					}
				}
				else
				{
					out.print("Erro ao Atualizar a Posição!");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			out.print("Erro ao Atualizar a Posição!");
		}
		finally
		{

			try
			{
				if (st != null)
					st.close();
				if (connection != null)
					connection.close();
			}
			catch (SQLException e)
			{
				log.error("Erro ao fechar o statement");
				e.printStackTrace();
			}
		}
	}

	private NeoObject getCorPrioridadeEvento(String codigoPrioridade)
	{
		NeoObject object = null;

		try
		{
			/**
			 * @author orsegups lucas.avila - Incluido extends neoobject - 29/04/2015
			 */
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("OTSCorPrioridadeEvento");

			QLEqualsFilter equalsFilter = new QLEqualsFilter("codigo", codigoPrioridade);

			object = PersistEngine.getObject(clazz, equalsFilter);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return object;
	}

	private NeoObject getStatusViatura(Long status)
	{
		NeoObject statusViatura = null;

		try
		{
			/**
			 * @author orsegups lucas.avila - Incluido extends neoobject - 29/04/2015
			 */
			Class<? extends NeoObject> statusViaturaClazz = AdapterUtils.getEntityClass("OTSStatusViatura");

			QLEqualsFilter equalsFilter = new QLEqualsFilter("codigoStatus", status);

			statusViatura = PersistEngine.getObject(statusViaturaClazz, equalsFilter);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return statusViatura;
	}

	@SuppressWarnings("finally")
	private Boolean allowUpdateEventPosition(HttpServletRequest request)
	{
		Boolean atualizaPosicao = false;
		try
		{

			String atualizaPosicaoStr = request.getParameter("atualizaPosicao");

			if (atualizaPosicaoStr != null && atualizaPosicaoStr.equalsIgnoreCase("true"))
			{
				atualizaPosicao = true;
			}
			else
			{
				NeoUser neoUser = PortalUtil.getCurrentUser();

				if (neoUser != null)
				{
					NeoPaper paper = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "AtualizaPosicaoMapa"));

					if (paper != null && neoUser.isInSecurityEntity(paper))
					{
						atualizaPosicao = true;
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return atualizaPosicao;
		}

	}

	private String clobToString(Clob data)
	{
		StringBuilder sb = new StringBuilder();
		try
		{
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine()))
			{
				sb.append(line);
			}
			br.close();
			reader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return sb.toString();
	}

	private void validateMachineId(String parametros, PrintWriter out)
	{

		out.print(MapaSecurityAcess.verificaDadosCadastradosPC(parametros));
	}

	private void saveLog(String texto, String placa, PrintWriter out)
	{
		try
		{

			QLEqualsFilter filter = new QLEqualsFilter("placa", placa);

			/**
			 * @author orsegups lucas.avila - Class generic.
			 * @date 08/07/2015
			 */
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("OTSViatura");

			NeoObject viaturaObject = PersistEngine.getObject(clazz, filter);

			if (viaturaObject != null)
			{
				NeoObject logViatura = AdapterUtils.createNewEntityInstance("OTSLogViatura");

				if (logViatura != null)
				{
					EntityWrapper viaturaWrapper = new EntityWrapper(logViatura);
					viaturaWrapper.setValue("viatura", viaturaObject);
					viaturaWrapper.setValue("dataLog", new GregorianCalendar());
					viaturaWrapper.setValue("textoLog", "PULA-LIGA " + texto);

					PersistEngine.persist(logViatura);

					out.print("Log salvo com sucesso.");
					return;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			out.print("Erro ao salvar log.");
		}

	}

	@SuppressWarnings("deprecation")
	private void saveLogMotorista(String texto, String cdviatura, PrintWriter out)
	{
		try
		{

			if (cdviatura != null && !cdviatura.isEmpty())
			{
				QLGroupFilter filterAnd = new QLGroupFilter("AND");
				//filterAnd.addFilter(new QLEqualsFilter("nm_placa", placa));
				filterAnd.addFilter(new QLEqualsFilter("cd_viatura", Long.valueOf(cdviatura)));

				ExternalEntityInfo infoVTR = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SIGMA90VIATURA");
				@SuppressWarnings("unchecked")
				List<NeoObject> listaVTR = (List<NeoObject>) PersistEngine.getObjects(infoVTR.getEntityClass(), filterAnd);

				if (listaVTR != null && !listaVTR.isEmpty())
				{

					NeoObject viaturaObject = (NeoObject) listaVTR.get(0);
					NeoObject logViatura = AdapterUtils.createNewEntityInstance("SIGMALogViatura");
					EntityWrapper placaEntityWrapper = new EntityWrapper(viaturaObject);
					String placaStr = (String) placaEntityWrapper.findValue("nm_placa");

					if (logViatura != null)
					{
						EntityWrapper viaturaWrapper = new EntityWrapper(logViatura);
						viaturaWrapper.setValue("viatura", viaturaObject);
						viaturaWrapper.setValue("dataLog", new GregorianCalendar());
						viaturaWrapper.setValue("textoLog", "PULA-LIGA Operador(a) " + PortalUtil.getCurrentUser().getFullName() + " " + texto);
						viaturaWrapper.setValue("usuario", PortalUtil.getCurrentUser());
						viaturaWrapper.setValue("placaViatura", placaStr);

						PersistEngine.persist(logViatura);

						out.print("Log salvo com sucesso.");
						return;
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			out.print("Erro ao salvar log.");
		}

	}

	private void getLogViatura(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String placa)
	{
		String resultLog = "";
		try
		{

			QLGroupFilter filterAnd = new QLGroupFilter("AND");
			filterAnd.addFilter(new QLEqualsFilter("placa", placa));
			NeoObject otsViatura = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("OTSViatura"), filterAnd);

			//String resultLog = "<a href=\"javascrit:void(0)\" class=\"closeButton\" onclick=\"removerTooltips();\"></a><br><b>Log:</b></br>";

			EntityWrapper entityWrapper = new EntityWrapper(otsViatura);
			Boolean emUso = (Boolean) entityWrapper.findValue("emUso");
			int valorMaximo = 10;
			QLEqualsFilter viaturaFilter = new QLEqualsFilter("viatura", otsViatura);
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			if (emUso)
			{
				valorMaximo = -1;

				GregorianCalendar calendar = new GregorianCalendar();
				calendar.add(Calendar.DATE, -3);
				//QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) entityWrapper.findValue("dataMovimentacao")); 
				QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) calendar);

				groupFilter.addFilter(datefilter);

			}
			groupFilter.addFilter(viaturaFilter);
			@SuppressWarnings("rawtypes")
			Class clazz = AdapterUtils.getEntityClass("OTSLogViatura");

			@SuppressWarnings("unchecked")
			Collection<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilter, -1, valorMaximo, "dataLog desc");

			if (logs != null && logs.size() > 0)
			{
				String linha = "";
				for (NeoObject log : logs)
				{
					EntityWrapper logWrapper = new EntityWrapper(log);

					String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
					String texto = (String) logWrapper.findValue("textoLog");
					linha = "<div style=\"overflow-y: scroll;\">";
					if (texto != null && !texto.isEmpty())
					{
						texto = texto.replaceAll("PULA-LIGA ", "");
						linha = "<li class=\"gm-addr\">" + formatedDate + " - " + texto + "</li>";
						resultLog += linha;
					}
				}
				linha = "</div>";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			out.print(resultLog.replaceAll("\"", "'"));
		}

	}

	private void inserirEventoEmEspera(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String rota, String historico, String cliente, String cdViatura)
	{
		String retorno = "";
		NeoObject eventos = null;
		try
		{
			if (NeoUtils.safeIsNotNull(historico))
			{
				salvarLogAcaoEvento(historico, "Executou a ação inserir evento na fila de espera.");
				int indexFim = rota.lastIndexOf("-");
				rota = rota.substring(0, indexFim);

				eventos = (NeoObject) retornaObject(historico);

				if (eventos == null)
				{

					Collection<NeoObject> eventosList = null;
					eventosList = (Collection<NeoObject>) retornaObjects(cdViatura);

					Long codigo = 0L;
					if (eventosList != null && !eventosList.isEmpty())
					{

						for (NeoObject evento : eventosList)
						{
							if (evento != null)
							{
								EntityWrapper psWrapper = new EntityWrapper(evento);
								codigo = (Long) psWrapper.findValue("codigo");
							}
						}

					}
					codigo++;
					NeoObject noPS = AdapterUtils.createNewEntityInstance("SIGMAEventosFilaEspera");

					EntityWrapper psWrapper = new EntityWrapper(noPS);

					psWrapper.findField("codHistorico").setValue(historico);
					psWrapper.findField("codRota").setValue(rota);
					psWrapper.findField("dataEspera").setValue(new GregorianCalendar());
					psWrapper.findField("cdCliente").setValue(cliente);
					psWrapper.findField("cdViaturaSigma").setValue(cdViatura);
					psWrapper.findField("codigo").setValue(codigo);
					PersistEngine.persist(noPS);
					retorno = "OK";
				}
				else
				{

					salvarLogAcaoEvento(historico, "Executou a ação remover evento da fila de espera.");

					String cdViaturaSigma = null;
					if (eventos != null)
					{
						EntityWrapper psWrapper = new EntityWrapper(eventos);
						cdViaturaSigma = (String) psWrapper.findValue("cdViaturaSigma");

						PersistEngine.remove(eventos);

					}
					if (cdViaturaSigma != null && !cdViaturaSigma.isEmpty())
					{

						Collection<NeoObject> eventosList = (Collection<NeoObject>) retornaObjects(cdViaturaSigma);
						if (eventosList != null && !eventosList.isEmpty())
						{
							Long cont = 1L;
							for (NeoObject neoObject : eventosList)
							{
								NeoObject neoObjectAux = (NeoObject) neoObject;
								EntityWrapper psWrappers = new EntityWrapper(neoObjectAux);
								psWrappers.findField("codigo").setValue(cont);
								PersistEngine.persist(neoObject);
								cont++;
							}
						}

					}

					if (cdViatura != null && !cdViatura.isEmpty())
					{
						NeoObject neoObject = (NeoObject) eventos;
						EntityWrapper entityWrapper = new EntityWrapper(neoObject);
						entityWrapper.findField("cdViaturaSigma").setValue(cdViatura);
						entityWrapper.findField("codigo").setValue(0L);
						PersistEngine.persist(neoObject);
					}
					retorno = "OK";

					//					retorno = "O EVENTO JÁ ESTA NA LISTA DE ESPERA, AGUARDANDO RETORNO.";
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "ERRO OrsegupsMapsServlet AO INSERIR inserirEventoEmEspera : " + e.getMessage();
			log.error(retorno);

		}
		finally
		{

			Gson gson = new Gson();
			String eventosJSON = gson.toJson(retorno);

			out.print(eventosJSON);

		}
	}

	@SuppressWarnings("deprecation")
	private void removerEventoEmEspera(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String historico)
	{
		String retorno = "";
		NeoObject evento = null;
		try
		{

			if (NeoUtils.safeIsNotNull(historico))
			{
				salvarLogAcaoEvento(historico, "Executou a ação remover evento da fila de espera.");
				evento = (NeoObject) retornaObject(historico);

				String cdViaturaSigma = null;
				if (evento != null)
				{
					EntityWrapper psWrapper = new EntityWrapper(evento);
					cdViaturaSigma = (String) psWrapper.findValue("cdViaturaSigma");

					PersistEngine.remove(evento);
					PersistEngine.commit(true);

				}
				if (cdViaturaSigma != null && !cdViaturaSigma.isEmpty())
				{

					Collection<NeoObject> eventosList = (Collection<NeoObject>) retornaObjects(cdViaturaSigma);
					if (eventosList != null && !eventosList.isEmpty())
					{
						Long cont = 1L;
						for (NeoObject neoObject : eventosList)
						{
							NeoObject neoObjectAux = (NeoObject) neoObject;
							EntityWrapper psWrappers = new EntityWrapper(neoObjectAux);
							psWrappers.findField("codigo").setValue(cont);
							PersistEngine.persist(neoObject);
							cont++;
						}
					}
					retorno = "OK";
				}
				else
				{
					retorno = "O EVENTO NÃO FOI ENCONTRADO NA LISTA DE ESPERA.";
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "ERRO OrsegupsMapsServlet AO REMOVER removerEventoEmEspera : " + e.getMessage();
			log.error(retorno);

		}
		finally
		{

			Gson gson = new Gson();
			String eventosJSON = gson.toJson(retorno);
			out.print(eventosJSON);

		}
	}

	private void buscarEventoEmEsperaFila(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String combo = "<br><select name=\"listaFila\" id=\"listaFila\" style=\"width:400px;\" multiple=\"multiple\">";
		try
		{

			String codigoViatura = request.getParameter("codigoViatura");
			String nomeMotorista = request.getParameter("nomeMotorista");
			Collection<EventoEsperaVO> eventosVOList = buscarEventoEmEspera(codigoViatura, nomeMotorista, "evento");

			if (NeoUtils.safeIsNotNull(eventosVOList) && !eventosVOList.isEmpty())
			{
				int i = 1;
				for (EventoEsperaVO eventoEsperaVO : eventosVOList)
				{
					combo += "<option value='" + eventoEsperaVO.getCodHistorico() + "'>" + i + "º - " + eventoEsperaVO.getCliente() + "</option>";
					i++;
				}

			}
			else
			{
				combo += "<option value='0'> Vazio!</option>";
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			combo += "</select>";
			out.print(combo);
		}
	}

	private Collection<EventoEsperaVO> buscarEventoEmEspera(String codigoViatura, String rota, String tipo)
	{
		Collection<EventoEsperaVO> eventosVOList = null;

		EventoEsperaVO eventoEsperaVO = null;
		try
		{
			removeEventosSemHistorico();
			if (codigoViatura != null && !codigoViatura.isEmpty())
			{
				//filterAnd.addFilter(new QLOpFilter("codRota", "LIKE", "%" + rota + "%"));
				Collection<NeoObject> eventosList = null;
				eventosList = (Collection<NeoObject>) retornaObjects(codigoViatura);
				eventosVOList = new ArrayList<EventoEsperaVO>();

				if (eventosList != null && !eventosList.isEmpty())
				{
					List<String> listaEventos = verificaEventoExisteHistorico();
					Long cont = 1L;
					//PersistEngine.commit(false);
					for (NeoObject object : eventosList)
					{
						NeoObject neoObject = (NeoObject) object;
						EntityWrapper eventoEsperaVOWrapper = new EntityWrapper(neoObject);
						eventoEsperaVO = new EventoEsperaVO();

						String cdHistorico = (String) eventoEsperaVOWrapper.getValue("codHistorico");

						if (cdHistorico != null && listaEventos != null && !listaEventos.isEmpty() && listaEventos.contains(cdHistorico))
						{
							eventoEsperaVOWrapper.findField("codigo").setValue(cont);
							PersistEngine.persist(neoObject);

							String cdRota = (String) eventoEsperaVOWrapper.getValue("codRota");
							GregorianCalendar dataRetorno = (GregorianCalendar) eventoEsperaVOWrapper.getValue("dataEspera");
							//Long codigo = (Long) eventoEsperaVOWrapper.getValue("codigo");
							String cdCliente = (String) eventoEsperaVOWrapper.getValue("cdCliente");
							String cdViatura = (String) eventoEsperaVOWrapper.getValue("cdViaturaSigma");

							eventoEsperaVO.setCodHistorico(cdHistorico);
							eventoEsperaVO.setCodRota(cdRota);
							eventoEsperaVO.setDataEspera(NeoDateUtils.safeDateFormat(dataRetorno, "dd/MM/yyyy HH:mm:ss"));
							//eventoEsperaVO.setCodigo(Long.parseLong(codigo));
							eventoEsperaVO.setCliente(cdCliente);
							eventoEsperaVO.setCodigoViatura(cdViatura);

							eventosVOList.add(eventoEsperaVO);
							cont++;
						}

					}

				}

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsMapsServlet AO ADD removerEventoEmEspera : " + e.getMessage());
		}

		return eventosVOList;
	}

	private void atualizarOrdemDosEventos(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String[] eventos)
	{

		NeoObject eventosObj = null;

		String retorno = "";
		try
		{

			if (NeoUtils.safeIsNotNull((Object[]) eventos))
			{
				Long codigo = 1L;
				for (String historico : eventos)
				{

					eventosObj = (NeoObject) retornaObject(historico);

					if (eventosObj != null)
					{

						NeoObject noPS = (NeoObject) eventosObj;

						EntityWrapper psWrapper = new EntityWrapper(noPS);
						psWrapper.findField("codigo").setValue(codigo);

						PersistEngine.persist(noPS);
						codigo++;
						retorno = "OK";
					}
					else
					{
						retorno = "O EVENTO JÁ ESTA NA LISTA DE ESPERA, AGUARDANDO RETORNO.";
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsMapsServlet AO ADD removerEventoEmEspera : " + e.getMessage());
		}
		finally
		{

			out.print(retorno);

		}

	}

	private void verificaCorEventos(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String codHistorico)
	{

		NeoObject eventosObj = null;
		String placa = "";
		int i = 1;

		try
		{

			if (NeoUtils.safeIsNotNull(codHistorico))
			{
				eventosObj = (NeoObject) retornaObject(codHistorico);

				if (eventosObj != null)
				{

					NeoObject noPS = (NeoObject) eventosObj;

					EntityWrapper psWrapper = new EntityWrapper(noPS);
					String cdViaturaSigma = (String) psWrapper.getValue("cdViaturaSigma");
					Collection<NeoObject> eventosList = null;
					eventosList = (Collection<NeoObject>) retornaObjects(cdViaturaSigma);
					if (eventosList != null && !eventosList.isEmpty())
					{

						for (NeoObject neoObject : eventosList)
						{
							NeoObject neoObjs = (NeoObject) neoObject;

							EntityWrapper psWrapperObjs = new EntityWrapper(neoObjs);
							String hist = (String) psWrapperObjs.getValue("codHistorico");
							if (hist.equals(codHistorico))
								break;
							i++;
						}

						QLGroupFilter groupFilters = new QLGroupFilter("AND");
						groupFilters.addFilter(new QLEqualsFilter("codigoViatura", cdViaturaSigma));
						groupFilters.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
						NeoObject objOTSViatura = PersistEngine.getObject(AdapterUtils.getEntityClass("OTSViatura"), groupFilters);

						NeoObject neoObj = (NeoObject) objOTSViatura;

						EntityWrapper psWrappers = new EntityWrapper(neoObj);

						placa = (String) psWrappers.getValue("placa");
						placa += ":" + i;

					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsMapsServlet ao verificar a cor prioridade : " + e.getMessage());
		}
		finally
		{
			out.print(placa);
		}

	}

	@SuppressWarnings("finally")
	private String verificaCor(String codHistorico)
	{

		NeoObject eventosObj = null;
		String placa = "";
		int i = 1;

		try
		{

			if (NeoUtils.safeIsNotNull(codHistorico))
			{

				eventosObj = (NeoObject) retornaObject(codHistorico);

				if (eventosObj != null)
				{

					NeoObject noPS = (NeoObject) eventosObj;

					EntityWrapper psWrapper = new EntityWrapper(noPS);
					String cdViaturaSigma = (String) psWrapper.getValue("cdViaturaSigma");
					Collection<NeoObject> eventosList = null;
					eventosList = (Collection<NeoObject>) retornaObjects(cdViaturaSigma);
					if (eventosList != null && !eventosList.isEmpty())
					{

						for (NeoObject neoObject : eventosList)
						{
							NeoObject neoObjs = (NeoObject) neoObject;

							EntityWrapper psWrapperObjs = new EntityWrapper(neoObjs);
							String hist = (String) psWrapperObjs.getValue("codHistorico");
							if (hist.equals(codHistorico))
								break;
							i++;
						}

						QLGroupFilter groupFilters = new QLGroupFilter("AND");
						groupFilters.addFilter(new QLEqualsFilter("codigoViatura", cdViaturaSigma));
						groupFilters.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
						NeoObject objOTSViatura = PersistEngine.getObject(AdapterUtils.getEntityClass("OTSViatura"), groupFilters);
						if (objOTSViatura != null)
						{
							NeoObject neoObj = (NeoObject) objOTSViatura;

							EntityWrapper psWrappers = new EntityWrapper(neoObj);

							placa = (String) psWrappers.getValue("placa");
							placa += ":" + i;

						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsMapsServlet ao verificar a cor prioridade : " + e.getMessage());
		}
		finally
		{
			return placa;
		}

	}

	private void salvarLogEvento(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdHistorico, String textoLog)
	{
		Connection conn = null;
		PreparedStatement st = null;
		PreparedStatement preparedStatementHSelect = null;
		ResultSet rsH = null;
		String texto = "";
		String retorno = "";
		String sql = "";
		try
		{

			if (NeoUtils.safeIsNotNull(textoLog) && !textoLog.isEmpty())
			{
				texto = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + " - Operador(a) " + PortalUtil.getCurrentUser().getFullName() + " : " + textoLog;
				texto = texto.replaceAll("\"", "");
				texto = texto.replaceAll("AtendimentoFinalizado", "#AtendimentoFinalizado");
				texto = texto.replaceAll("recebidovector", "#recebidovector");
				texto = texto.replaceAll("#deslocadoGolf", "deslocadoGolf");
				texto = texto.replaceAll("deslocadoGolf", "#deslocadoGolf");
				texto = texto.replaceAll("#60minutos", "60minutos");
				texto = texto.replaceAll("60minutos", "#60minutos");
				texto = texto.replaceAll("#deslocarraia", "deslocarraia");
				texto = texto.replaceAll("deslocarraia", "#deslocarraia");
				texto = texto.replaceAll("#finalizarraia", "finalizarraia");
				texto = texto.replaceAll("finalizarraia", "#finalizarraia");
			}

			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder selecHtSQL = new StringBuilder();

			selecHtSQL.append(" SELECT *");
			selecHtSQL.append(" FROM HISTORICO ");
			selecHtSQL.append(" WHERE CD_HISTORICO = ? AND TX_OBSERVACAO_FECHAMENTO IS NOT NULL ");

			preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());
			preparedStatementHSelect.setString(1, cdHistorico);
			rsH = preparedStatementHSelect.executeQuery();
			if (rsH.next())
				sql = " + (CHAR(13) + CHAR(10)) + TX_OBSERVACAO_FECHAMENTO";
			StringBuilder sqlUpdate = new StringBuilder();
			sqlUpdate.append(" UPDATE HISTORICO  SET TX_OBSERVACAO_FECHAMENTO = ? " + sql + " WHERE CD_HISTORICO = ?");
			st = conn.prepareStatement(sqlUpdate.toString());
			
			st.setString(1, texto);
			st.setString(2, cdHistorico);
			st.executeUpdate();
			retorno = "Log salvo com sucesso!";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss");
			log.error(retorno);

		}
		finally
		{
			OrsegupsUtils.closeConnection(null, preparedStatementHSelect, rsH);
			OrsegupsUtils.closeConnection(conn, st, null);
			out.print(retorno);
		}

	}

	public static void removerEventosEmEspera(String cdViatura)
	{
		String retorno = "OK";
		Collection<NeoObject> eventos = null;
		try
		{

			if (NeoUtils.safeIsNotNull(cdViatura))
			{

				eventos = (Collection<NeoObject>) retornaObjects(cdViatura);

				if (eventos != null && !eventos.isEmpty())
				{
					PersistEngine.removeObjects(eventos);

				}
				else
				{
					retorno = "OS EVENTOS NÃO FORAM ENCONTRADOS NA LISTA DE ESPERA.";
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "ERRO OrsegupsMapsServlet AO REMOVER removerEventosEmEspera : " + e.getMessage();
			log.error(retorno);

		}
		finally
		{

			//		Gson gson = new Gson();
			//		String eventosJSON = gson.toJson(retorno);
			//		out.print(eventosJSON);

		}
	}

	private void cancelarEventoIniciado(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdHistorico)
	{

		Connection conn = null;
		PreparedStatement preparedStatementUpdate = null;

		try
		{
			if (NeoUtils.safeIsNotNull(cdHistorico))
			{
				salvarLogAcaoEvento(cdHistorico, "Executou a ação de cancelamento de deslocamento.");
				conn = PersistEngine.getConnection("SIGMA90");

				String updateSQL = " UPDATE HISTORICO SET FG_STATUS = 1 ,CD_VIATURA = NULL, DT_ESPERA = GETDATE() ," + " CD_USUARIO_VIATURA_DESLOCAMENTO = NULL, DT_VIATURA_DESLOCAMENTO = NULL, DT_ESPERA_DESLOCAMENTO = NULL," + " FG_EXIBIDO_VIATURA = NULL, DT_EXIBIDO_VIATURA = NULL,  CD_USUARIO_ESPERA = NULL WHERE CD_HISTORICO = ? ";

				preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
				preparedStatementUpdate.setString(1, cdHistorico);
				preparedStatementUpdate.executeUpdate();

				log.warn("OrsegupsMapsServlet setEmEsperaEvento: " + cdHistorico);

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO OrsegupsMapsServlet setEmEsperaEvento: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, null);
		}
	}

	@SuppressWarnings("finally")
	private String executaServicoSegwareDeslocamento(Long idViatura, Long idEvento)
	{
		String returnFromAccess = "";
		try
		{
			DeslocarEventoWebServiceProxy webServiceProxy = new DeslocarEventoWebServiceProxy();

			returnFromAccess = webServiceProxy.deslocarEvento(idEvento, null, idViatura);

			log.warn("##### ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA EXECUTADA COM SUCESSO - RETORNOU " + returnFromAccess + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
			log.error("##### ERRO AO EXECUTAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - RETORNOU " + returnFromAccess + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		finally
		{
			return returnFromAccess;
		}
	}

	private void salvarLogEventoGerencia(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdHistorico, String textoLog)
	{
		Connection conn = null;
		PreparedStatement st = null;
		PreparedStatement preparedStatementHSelect = null;
		ResultSet rsH = null;
		String texto = null;
		String retorno = "";
		String sql = "";
		try
		{
			
			if (NeoUtils.safeIsNotNull(textoLog) && !textoLog.isEmpty() && PortalUtil.getCurrentUser() != null)
			{
				texto = textoLog;
				texto = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + " - Operador(a) " + PortalUtil.getCurrentUser().getFullName() + " : " + textoLog;
				texto = texto.replaceAll("\"", "");
				texto = texto.replaceAll("#busy", "busy");
				texto = texto.replaceAll("busy", "#busy");
			}

			if (NeoUtils.safeIsNotNull(texto) && !texto.isEmpty() && cdHistorico != null && !cdHistorico.isEmpty())
			{
				conn = PersistEngine.getConnection("SIGMA90");

				StringBuilder selecHtSQL = new StringBuilder();

				selecHtSQL.append(" SELECT *");
				selecHtSQL.append(" FROM HISTORICO ");
				selecHtSQL.append(" WHERE CD_HISTORICO = ? AND TX_OBSERVACAO_GERENTE IS NOT NULL ");

				preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());
				preparedStatementHSelect.setString(1, cdHistorico);
				rsH = preparedStatementHSelect.executeQuery();
				if (rsH.next())
					sql = " + (CHAR(13) + CHAR(10)) + TX_OBSERVACAO_GERENTE";

				StringBuilder sqlUpdate = new StringBuilder();

				sqlUpdate.append(" UPDATE HISTORICO SET TX_OBSERVACAO_GERENTE =  ?  " + sql + " WHERE CD_HISTORICO = ?");
				st = conn.prepareStatement(sqlUpdate.toString());
				st.setString(1, texto);
				st.setString(2, cdHistorico);
				st.executeUpdate();

				retorno = "Log salvo com sucesso!";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss");
			log.error(retorno);

		}
		finally
		{
			OrsegupsUtils.closeConnection(null, preparedStatementHSelect, rsH);
			OrsegupsUtils.closeConnection(conn, st, null);
			out.print(retorno);
		}

	}

	private void deslocarEventoViatura(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String rota, String historico, String cliente, String cdViatura, String codigo)
	{
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		PreparedStatement preparedStatementUpdate = null;
		ResultSet rsH = null;
		List<EventoEsperaVO> listaHitorico = null;
		NeoObject eventos = null;
		String retorno = "OK";
		try
		{

			if (NeoUtils.safeIsNotNull(cdViatura))
			{

				salvarLogAcaoEvento(historico, "Executou a ação deslocar evento.");
				conn = PersistEngine.getConnection("SIGMA90");
				StringBuilder selecHtSQL = new StringBuilder();
				selecHtSQL.append(" SELECT H.CD_HISTORICO, H.CD_VIATURA, H.CD_CLIENTE , C.ID_CENTRAL, C.PARTICAO, C.FANTASIA");
				selecHtSQL.append(" FROM HISTORICO H ");
				selecHtSQL.append(" INNER JOIN dbCENTRAL C  ON C.CD_CLIENTE = H.CD_CLIENTE");
				selecHtSQL.append(" WHERE CD_VIATURA = ? AND FG_STATUS in (2)");

				StringBuilder updateSQL = new StringBuilder();
				updateSQL.append(" UPDATE HISTORICO SET FG_STATUS = 1 ,CD_VIATURA = NULL, DT_ESPERA = GETDATE() ," + " CD_USUARIO_VIATURA_DESLOCAMENTO = NULL, DT_VIATURA_DESLOCAMENTO = NULL, DT_ESPERA_DESLOCAMENTO = NULL," + " FG_EXIBIDO_VIATURA = NULL, DT_EXIBIDO_VIATURA = NULL,  CD_USUARIO_ESPERA = 11010 WHERE CD_HISTORICO = ?");

				preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());
				preparedStatementHSelect.setString(1, cdViatura);
				listaHitorico = new ArrayList<EventoEsperaVO>();
				rsH = preparedStatementHSelect.executeQuery();
				EventoEsperaVO eventoEsperaVO = null;
				while (rsH.next())
				{
					String fantasia = "";
					eventoEsperaVO = new EventoEsperaVO();
					if (NeoUtils.safeIsNotNull(rsH.getString("CD_HISTORICO")))
						eventoEsperaVO.setCodHistorico(rsH.getString("CD_HISTORICO"));
					if (NeoUtils.safeIsNotNull(rsH.getString("CD_VIATURA")))
						eventoEsperaVO.setCodigoViatura(rsH.getString("CD_VIATURA"));
					if (NeoUtils.safeIsNotNull(rsH.getString("FANTASIA")))
						fantasia = rsH.getString("ID_CENTRAL") + " [" + rsH.getString("PARTICAO") + "]  " + rsH.getString("FANTASIA");
					eventoEsperaVO.setCliente(fantasia);
					listaHitorico.add(eventoEsperaVO);
				}

				retorno = executaServicoSegwareDeslocamento(Long.parseLong(cdViatura), Long.parseLong(historico));
				if (NeoUtils.safeIsNotNull(listaHitorico) && !listaHitorico.isEmpty() && retorno.equals("ACK"))
				{

					for (EventoEsperaVO eventosEspera : listaHitorico)
					{
						retorno = "OK";
						if (NeoUtils.safeIsNotNull(eventosEspera.getCodHistorico()))
						{
							preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
							preparedStatementUpdate.setString(1, eventosEspera.getCodHistorico());
							preparedStatementUpdate.executeUpdate();

							int indexFim = rota.lastIndexOf("-");
							if (indexFim >= 0)
								rota = rota.substring(0, indexFim);

							eventos = (NeoObject) retornaObject(eventosEspera.getCodHistorico());

							if (eventos == null)
							{
								Collection<NeoObject> eventosList = null;
								eventosList = (Collection<NeoObject>) retornaObjects(cdViatura);
								Long cont = 1L;
								GregorianCalendar temp = new GregorianCalendar();
								NeoObject noPS = AdapterUtils.createNewEntityInstance("SIGMAEventosFilaEspera");

								EntityWrapper psWrapper = new EntityWrapper(noPS);

								psWrapper.findField("codHistorico").setValue(eventosEspera.getCodHistorico());
								psWrapper.findField("codRota").setValue(rota);
								psWrapper.findField("dataEspera").setValue(temp);
								psWrapper.findField("cdCliente").setValue(eventoEsperaVO.getCliente());
								psWrapper.findField("cdViaturaSigma").setValue(cdViatura);
								psWrapper.findField("codigo").setValue(cont);
								PersistEngine.persist(noPS);
								cont++;
								if (eventosList != null && !eventosList.isEmpty())
								{

									for (NeoObject neoObject : eventosList)
									{
										EntityWrapper entityWrapper = new EntityWrapper(neoObject);

										entityWrapper.findField("codigo").setValue(cont);
										PersistEngine.persist(neoObject);
										cont++;

									}

								}

							}
							else
							{
								retorno = "O EVENTO JÁ ESTA NA LISTA DE ESPERA, AGUARDANDO RETORNO.";
							}
						}
					}

					log.warn("EXECUTAR ALTERAÇÃO STATUS EVENTO EM ESPERA RETORNO :" + retorno + " Viatura :  " + cdViatura);
				}
				if (NeoUtils.safeIsNotNull(historico) && (retorno.equals("ACK") || (retorno.equals("O EVENTO JÁ ESTA NA LISTA DE ESPERA, AGUARDANDO RETORNO.") || retorno.equals("VIATURA_JA_DESLOCADA"))))
				{
					NeoObject eventoRemove = (NeoObject) retornaObject(historico);
					if (eventoRemove != null)
					{
						PersistEngine.remove(eventoRemove);
					}

				}
			}
		}
		catch (Exception e)
		{

			e.printStackTrace();
			log.error("##### JOGAR EVENTO EM ESPERA: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(null, preparedStatementHSelect, rsH);
			OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, null);
			out.print(retorno);
		}
		//

		log.warn("JOGAR EVENTO EM ESPERA " + cdViatura);
	}

	private void fecharEventoSigma(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String historico, String motivoAlarme)
	{
		Connection conn = null;
		PreparedStatement preparedStatementUpdatePai = null;
		PreparedStatement preparedStatementUpdate = null;
		String retorno = "OK";
		try
		{
			if (NeoUtils.safeIsNotNull(historico))
			{

				salvarLogAcaoEvento(historico, "Executou a ação finalizar evento.");
				String cdUsuario = (String) getUsuarioSigma();
				conn = PersistEngine.getConnection("SIGMA90");
				String updateSQLFilho = " UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = ?, CD_MOTIVO_ALARME = ? WHERE CD_HISTORICO_PAI = ? ";

				String updateSQLPai = " UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = ?, CD_MOTIVO_ALARME = ? WHERE CD_HISTORICO = ? ";

				conn.setAutoCommit(false);

				preparedStatementUpdate = conn.prepareStatement(updateSQLFilho.toString());
				preparedStatementUpdate.setString(1, cdUsuario);
				preparedStatementUpdate.setLong(2, Long.parseLong(motivoAlarme));
				preparedStatementUpdate.setString(3, historico);
				preparedStatementUpdate.executeUpdate();

				preparedStatementUpdatePai = conn.prepareStatement(updateSQLPai.toString());
				preparedStatementUpdatePai.setString(1, cdUsuario);
				preparedStatementUpdatePai.setLong(2, Long.parseLong(motivoAlarme));
				preparedStatementUpdatePai.setString(3, historico);
				preparedStatementUpdatePai.executeUpdate();

				conn.commit();

				NeoObject evento = (NeoObject) retornaObject(historico);
				if (evento != null)
				{
					NeoObject neoObjectAuxEvento = (NeoObject) evento;
					EntityWrapper entityWrapperEvento = new EntityWrapper(neoObjectAuxEvento);
					String cdViatura = (String) entityWrapperEvento.findValue("cdViaturaSigma");
					PersistEngine.remove(evento);

					Collection<NeoObject> eventosList = null;
					eventosList = (Collection<NeoObject>) retornaObjects(cdViatura);
					Integer cont = 1;
					if (eventosList != null && !eventosList.isEmpty())
					{

						for (NeoObject neoObject : eventosList)
						{
							NeoObject neoObjectAux = (NeoObject) neoObject;
							EntityWrapper entityWrapper = new EntityWrapper(neoObjectAux);

							entityWrapper.findField("codigo").setValue(cont);
							PersistEngine.persist(neoObjectAux);
							cont++;
						}

						retorno = "OK";
					}

				}
				log.warn("Fechar eventos Historico :  " + historico);
			}

		}
		catch (Exception e)
		{
			try
			{
				retorno = "Fechar eventos rollback Historico :  " + historico;
				conn.rollback();
				log.warn(retorno);
			}
			catch (SQLException e1)
			{
				log.error("##### ERRO SALVAR LOG EVENT TRANSAÇÃO fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
				e1.printStackTrace();
			}
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(null, preparedStatementUpdate, null);
			OrsegupsUtils.closeConnection(conn, preparedStatementUpdatePai, null);
			out.print(retorno);
		}
		log.warn("Fechando evento no Sigma! " + historico);
	}

	private void viaturaChegaLocal(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String historico)
	{
		Connection conn = null;
		PreparedStatement preparedStatementUpdate = null;
		String retorno = "OK";
		try
		{
			if (NeoUtils.safeIsNotNull(historico))
			{
				salvarLogAcaoEvento(historico, "Execução da ação chegada ao local.");
				String cdUsuario = (String) getUsuarioSigma();
				conn = PersistEngine.getConnection("SIGMA90");
				String updateSQL = " UPDATE HISTORICO SET FG_STATUS = 3 , DT_VIATURA_NO_LOCAL = GETDATE() ,CD_USUARIO_VIATURA_NO_LOCAL = ? WHERE CD_HISTORICO = ? ";

				preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
				preparedStatementUpdate.setString(1, cdUsuario);
				preparedStatementUpdate.setString(2, historico);
				preparedStatementUpdate.executeUpdate();

				log.warn("Viatura chegada ao local Historico :  " + historico);

			}
		}
		catch (Exception e)
		{

			e.printStackTrace();
			log.error("##### ERRO Viatura chegada ao local : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, null);
			out.print(retorno);
		}
		log.warn("Viatura chegada ao local ! " + historico);
	}

	private void buscaMotivoAlarme(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		ResultSet rsH = null;
		String retornoLista = "<span class=\"gm-style-iw\"><b>Fechar Evento:</b> </span>  <select name='cbTipoFechamento' id='cbTipoFechamento' style='width:400px;'>";
		try
		{

			conn = PersistEngine.getConnection("SIGMA90");
			String selecHtSQL = " SELECT CD_MOTIVO_ALARME,DS_MOTIVO_ALARME FROM MOTIVO_ALARME WITH(NOLOCK) where FG_ATIVO = 1 ";
			preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());

			rsH = preparedStatementHSelect.executeQuery();
			retornoLista += "<option value='0'  selected='selected' disabled='disabled' style='display: none'>Selecione...</option>";
			while (rsH.next())
			{
				retornoLista += "<option value=" + rsH.getString("CD_MOTIVO_ALARME") + " > " + rsH.getString("DS_MOTIVO_ALARME") + "</option>";

			}

		}
		catch (Exception e)
		{

			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
			retornoLista += "</select>";
			out.print(retornoLista);
		}
	}

	private void iniciarDeslocamentoEventoViatura(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String historico)
	{
		Connection conn = null;
		PreparedStatement preparedStatementUpdate = null;
		String retorno = "OK";
		try
		{
			if (NeoUtils.safeIsNotNull(historico))
			{
				salvarLogAcaoEvento(historico, "Executou a ação iniciar deslocamento.");
				String cdUsuario = (String) getUsuarioSigma();
				conn = PersistEngine.getConnection("SIGMA90");
				String updateSQL = " UPDATE HISTORICO SET FG_STATUS = 9, DT_VIATURA_DESLOCAMENTO = GETDATE(), CD_USUARIO_VIATURA_DESLOCAMENTO = ? WHERE CD_HISTORICO = ? ";

				preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
				preparedStatementUpdate.setString(1, cdUsuario);
				preparedStatementUpdate.setString(2, historico);
				preparedStatementUpdate.executeUpdate();

				log.warn("Viatura chegada ao local Historico :  " + historico);

			}
		}
		catch (Exception e)
		{

			e.printStackTrace();
			log.error("##### ERRO Viatura chegada ao local : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, null);
			out.print(retorno);
		}
		log.warn("Viatura chegada ao local ! " + historico);
	}

	@SuppressWarnings("finally")
	private List<String> verificaEventoExisteHistorico()
	{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<String> listaEvento = null;
		try
		{
			connection = PersistEngine.getConnection("SIGMA90");
			StringBuilder builder = new StringBuilder();
			builder.append(" SELECT CD_HISTORICO FROM HISTORICO  WHERE  FG_STATUS = 1 ");
			preparedStatement = connection.prepareStatement(builder.toString());
			//preparedStatement.setString(1, codigo);
			resultSet = preparedStatement.executeQuery();
			listaEvento = new ArrayList<String>();
			while (resultSet.next())
			{
				String historico = resultSet.getString("CD_HISTORICO");
				listaEvento.add(historico);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, preparedStatement, resultSet);
			return listaEvento;
		}
	}

	@SuppressWarnings("finally")
	private StringBuilder getMotoritas(String regional)
	{
		List<NeoObject> motoristas = null;
		StringBuilder builder = null;
		try
		{

			if (NeoUtils.safeIsNotNull(regional))
			{
				QLGroupFilter filterAnd = new QLGroupFilter("AND");
				filterAnd.addFilter(new QLOpFilter("motorista", "LIKE", regional + "%"));
				filterAnd.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
				motoristas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filterAnd);

				if (NeoUtils.safeIsNotNull(motoristas))
				{
					builder = new StringBuilder();
					for (NeoObject neoObject : motoristas)
					{
						NeoObject object = (NeoObject) neoObject;
						EntityWrapper entityWrapper = new EntityWrapper(object);
						String motorista = (String) entityWrapper.getValue("motorista");
						String codigoViatura = (String) entityWrapper.getValue("codigoViatura");
						if (!motorista.contains("- TEC") && !motorista.contains("-TEC") && !motorista.contains("-INSP") && (!motorista.contains("CTA-SUP") && (!motorista.contains("HN") || !motorista.contains("HD"))) && !motorista.contains("-MOTO") && !motorista.contains("-ADM") && !motorista.contains("OFFICE") && !motorista.contains(" ENG"))
							builder.append("<option value='" + codigoViatura + "'>" + motorista + "</option>::");
					}
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return builder;
		}

	}

	private void removeEventosSemHistorico()
	{
		try
		{

			QLGroupFilter filterAndRemove = new QLGroupFilter("AND");
			List<NeoObject> eventos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAndRemove);
			if (eventos != null)
			{
				Collection<NeoObject> collection = new ArrayList<NeoObject>();
				List<String> listaEvento = verificaEventoExisteHistorico();
				for (NeoObject neoObjects : eventos)
				{
					NeoObject neoObject = (NeoObject) neoObjects;
					EntityWrapper entityWrapper = new EntityWrapper(neoObject);
					String historico = (String) entityWrapper.getValue("codHistorico");
					if (listaEvento != null && !listaEvento.isEmpty() && !listaEvento.contains(historico))
					{
						collection.add(neoObject);
					}

				}
				if (collection != null && !collection.isEmpty())
					PersistEngine.removeObjects(collection);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void getDadosProvidencia(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdCliente, String cdHistorico)
	{

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<ProvidenciaVO> list = new ArrayList<ProvidenciaVO>();

		try
		{

			if (cdCliente != null)
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT");
				sql.append("	PROV.CD_CLIENTE,");
				sql.append("	CD_PROVIDENCIA,");
				sql.append("	PROV.NOME,");
				sql.append("	PROV.FONE1,");
				sql.append("	PROV.FONE2,");
				sql.append("	PROV.EMAIL,");
				sql.append("	PROV.NU_PRIORIDADE");
				sql.append(" FROM");
				sql.append("	dbo.dbPROVIDENCIA AS PROV ");
				sql.append("	INNER JOIN dbo.dbCENTRAL as CEN   ON CEN.CD_CLIENTE = PROV.CD_CLIENTE");
				sql.append(" WHERE ");
				sql.append("	CEN.CD_CLIENTE = ?");
				sql.append(" ORDER BY PROV.NU_PRIORIDADE_NIVEL2");

				statement = connection.prepareStatement(sql.toString());
				statement.setString(1, cdCliente);
				resultSet = statement.executeQuery();

				ProvidenciaVO providenciaVO = null;
				while (resultSet.next())
				{

					providenciaVO = new ProvidenciaVO();
					String cliente = resultSet.getString("CD_CLIENTE");
					String providencia = resultSet.getString("CD_PROVIDENCIA");
					String nome = resultSet.getString("NOME");
					String telefone1 = resultSet.getString("FONE1");
					String telefone2 = resultSet.getString("FONE2");
					String email = resultSet.getString("EMAIL");
					String prioridade = resultSet.getString("NU_PRIORIDADE");
					System.out.println(cliente + " - " + providencia + " - " + nome + " -  " + telefone1 + " - " + telefone2 + " - " + email);
					if (NeoUtils.safeIsNotNull(cliente))
					{
						providenciaVO.setCodigoCliente(Integer.parseInt(cliente));
					}
					else
					{
						providenciaVO.setCodigoCliente(0);
					}

					if (NeoUtils.safeIsNotNull(providencia))
					{
						providenciaVO.setCodigoProvidencia(Integer.parseInt(providencia));
					}
					else
					{
						providenciaVO.setCodigoProvidencia(0);
					}

					if (NeoUtils.safeIsNotNull(nome))
					{
						providenciaVO.setNome(nome);
					}
					else
					{
						providenciaVO.setCodigoCliente(0);
					}

					if (NeoUtils.safeIsNotNull(telefone1) && !telefone1.isEmpty())
					{
						telefone1 = telefone1.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone1 = telefone1.replace(" ", "");
						providenciaVO.setTelefone1("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone1 + "\"," + cdHistorico + ")'>" + telefone1 + "</a>");

						//providenciaVO.setTelefone1(telefone1);
					}
					else
					{
						providenciaVO.setTelefone1("Vazio");
					}
					if (NeoUtils.safeIsNotNull(telefone2) && !telefone2.isEmpty())
					{
						telefone2 = telefone2.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone2 = telefone2.replace(" ", "");
						providenciaVO.setTelefone2("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone2 + "\"," + cdHistorico + ")'>" + telefone2 + "</a>");
						//providenciaVO.setTelefone2(telefone2);
					}
					else
					{
						providenciaVO.setTelefone2("Vazio");
					}
					if (NeoUtils.safeIsNotNull(email))
					{
						providenciaVO.setEmail(email);
					}
					else
					{
						providenciaVO.setEmail("Vazio");
					}
					if (NeoUtils.safeIsNotNull(prioridade))
					{
						providenciaVO.setPrioridade(Integer.parseInt(prioridade));
					}
					else
					{
						providenciaVO.setPrioridade(0);
					}
					list.add(providenciaVO);

				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsAlertEventEngine SIGMA PROVIDENCIAS : " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			Gson gson = new Gson();
			String eventosJSON = gson.toJson(list);
			out.print(eventosJSON);

		}

	}

	public void getDadosEventosHistorico(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String codigoHistorico, String dias)
	{

		Connection connection = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		EventoVO vo = null;
		//Long timeExecFinal = GregorianCalendar.getInstance().getTimeInMillis();

		//Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

		try
		{

			if ((codigoHistorico != null))
			{
				//System.out.println(" 1º getDadosEventosHistorico- Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");

				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();
				sql.append(" EXEC USP_HISTORICO_EVENTOS @COD_CLIENTE = ?, @DATA = ?  ");

				vo = new EventoVO();

				if (dias == null)
					dias = "1";
				//System.out.println(" 2º getDadosEventosHistorico ant prepareStatement - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
				callableStatement = connection.prepareCall(sql.toString());
				//System.out.println(" 3º getDadosEventosHistorico pos prepareStatement - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
				callableStatement.setInt(1, Integer.parseInt(codigoHistorico));
				callableStatement.setInt(2, Integer.parseInt(dias));
				resultSet = callableStatement.executeQuery();
				//System.out.println(" 4º getDadosEventosHistorico pos resultSet - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");

				EventoHistoricoVO eventoVO = null;
				while (resultSet.next())
				{

					eventoVO = new EventoHistoricoVO();
					String dtRecebido = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:ss");
					String cdEvento = resultSet.getString("CD_EVENTO");
					String tipoEvento = resultSet.getString("NM_FRASE_EVENTO");
					String dtEspera = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_ESPERA"), "dd/MM/yyyy HH:mm:ss");
					String dtVtrDeslocamento = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_VIATURA_DESLOCAMENTO"), "dd/MM/yyyy HH:mm:ss");
					String dtVtrLocal = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_VIATURA_NO_LOCAL"), "dd/MM/yyyy HH:mm:ss");
					String dtFechamento = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_FECHAMENTO"), "dd/MM/yyyy HH:mm:ss");
					String nuFechamento = resultSet.getString("NU_AUXILIAR");
					String obsFechamento = resultSet.getString("TX_OBSERVACAO_FECHAMENTO");
					String prioridade = resultSet.getString("NU_PRIORIDADE");

					if (obsFechamento == null || obsFechamento.isEmpty())
					{
						obsFechamento = "Vazio";
					}
					else
					{
						//obsFechamento = "<li>" + obsFechamento;
						obsFechamento = obsFechamento.replace("\n", "<br><li>");
					}

					if (NeoUtils.safeIsNotNull(dtRecebido))
						eventoVO.setDtRecebido(dtRecebido);
					else
						eventoVO.setDtRecebido("");
					if (NeoUtils.safeIsNotNull(cdEvento))
						eventoVO.setCdEvento(cdEvento);
					else
						eventoVO.setCdEvento("");
					if (NeoUtils.safeIsNotNull(tipoEvento))
						eventoVO.setTipoEvento(tipoEvento);
					else
						eventoVO.setTipoEvento("");
					if (NeoUtils.safeIsNotNull(dtEspera))
						eventoVO.setDtEspera(dtEspera);
					else
						eventoVO.setDtEspera("");
					if (NeoUtils.safeIsNotNull(dtVtrDeslocamento))
						eventoVO.setDtVtrDeslocamento(dtVtrDeslocamento);
					else
						eventoVO.setDtVtrDeslocamento("");
					if (NeoUtils.safeIsNotNull(dtVtrLocal))
						eventoVO.setDtVtrLocal(dtVtrLocal);
					else
						eventoVO.setDtVtrLocal("");
					if (NeoUtils.safeIsNotNull(dtFechamento))
						eventoVO.setDtFechamento(dtFechamento);
					else
						eventoVO.setDtFechamento("");
					if (NeoUtils.safeIsNotNull(nuFechamento))
						eventoVO.setNuFechamento(nuFechamento);
					else
						eventoVO.setNuFechamento("");
					if (NeoUtils.safeIsNotNull(obsFechamento))
						eventoVO.setObsFechamento(obsFechamento);
					else
						eventoVO.setObsFechamento("");
					if (NeoUtils.safeIsNotNull(prioridade))
						eventoVO.setPrioridade(prioridade);
					else
						eventoVO.setPrioridade("");

					vo.addEventoHistorico(eventoVO);

				}
				//System.out.println(" 5º getDadosEventosHistorico fim resultSet - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsAlertEventEngine SIGMA EVENTO HISTORICO : " + e.getMessage());
		}
		finally
		{
			Gson gson = new Gson();
			String eventosJSON = gson.toJson(vo.getEventoHistoricoVOs());
			out.print(eventosJSON);
			try
			{
				if (resultSet != null)
					resultSet.close();
				if (callableStatement != null)
					callableStatement.close();
				if (connection != null)
					connection.close();

			}
			catch (Exception e2)
			{
				e2.printStackTrace();
			}

		}

	}

	public static String retiraCaracteresAcentuados(String stringFonte)
	{
		String passa = stringFonte;
		passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
		passa = passa.replaceAll("[âãàáä]", "a");
		passa = passa.replaceAll("[ÊÈÉË]", "E");
		passa = passa.replaceAll("[êèéë]", "e");
		passa = passa.replaceAll("ÎÍÌÏ", "I");
		passa = passa.replaceAll("îíìï", "i");
		passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
		passa = passa.replaceAll("[ôõòóö]", "o");
		passa = passa.replaceAll("[ÛÙÚÜ]", "U");
		passa = passa.replaceAll("[ûúùü]", "u");
		passa = passa.replaceAll("Ç", "C");
		passa = passa.replaceAll("ç", "c");
		passa = passa.replaceAll("[ýÿ]", "y");
		passa = passa.replaceAll("Ý", "Y");
		passa = passa.replaceAll("ñ", "n");
		passa = passa.replaceAll("Ñ", "N");
		passa = passa.replaceAll("[-+=*&amp;%$#@!_]", "");
		passa = passa.replaceAll("['\"]", "");
		passa = passa.replaceAll("[<>()\\{\\}]", "");
		passa = passa.replaceAll("['\\\\.,()|/]", "");
		passa = passa.replaceAll("[^!-ÿ]{1}[^ -ÿ]{0,}[^!-ÿ]{1}|[^!-ÿ]{1}", "");
		return passa;
	}

	@SuppressWarnings("finally")
	private String getUsuarioSigma()
	{
		NeoUser neoUser = (NeoUser) PortalUtil.getCurrentUser();
		String cdUsuario = "11010";
		try
		{

			QLGroupFilter filterAnd = new QLGroupFilter("AND");
			filterAnd.addFilter(new QLEqualsFilter("usuarioFusion", neoUser));
			ArrayList<NeoObject> neoObject = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAUsuarioSigmaFusion"), filterAnd);
			if (neoObject != null && !neoObject.isEmpty())
			{
				EntityWrapper entityWrapper = new EntityWrapper(neoObject.get(0));
				Long cdUsuarioL = (Long) entityWrapper.findValue("usuarioSigma.cd_usuario");
				cdUsuario = String.valueOf(cdUsuarioL);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return cdUsuario;
		}
	}

	@SuppressWarnings("finally")
	private List<String> verificaEventoAgrupado()
	{
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		ResultSet rsH = null;
		List<String> listaEventoAgrupado = null;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			String select = "SELECT DISTINCT CD_HISTORICO_PAI FROM HISTORICO   WHERE CD_HISTORICO_PAI IS NOT NULL";
			//String selecHtSQL = " SELECT TOP(1)CD_HISTORICO FROM HISTORICO   WHERE CD_HISTORICO_PAI = ? ";

			preparedStatementHSelect = conn.prepareStatement(select.toString());
			//preparedStatementHSelect.setString(1, historico);
			rsH = preparedStatementHSelect.executeQuery();
			listaEventoAgrupado = new ArrayList<String>();
			while (rsH.next())
			{
				//hPai = Boolean.TRUE;
				String hPaiStr = rsH.getString("CD_HISTORICO_PAI");
				listaEventoAgrupado.add(hPaiStr);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO verificaEventoAgrupado: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
			return listaEventoAgrupado;
		}

	}

	@SuppressWarnings("finally")
	private String listaPosicoes(String codHistorico)
	{

		NeoObject eventosObj = null;
		int i = 1;
		StringBuilder stringBuilder = new StringBuilder();

		try
		{

			if (NeoUtils.safeIsNotNull(codHistorico))
			{
				eventosObj = (NeoObject) retornaObject(codHistorico);

				if (eventosObj != null)
				{

					NeoObject noPS = (NeoObject) eventosObj;

					EntityWrapper psWrapper = new EntityWrapper(noPS);
					String cdViaturaSigma = (String) psWrapper.getValue("cdViaturaSigma");
					Collection<NeoObject> eventosList = null;
					eventosList = (Collection<NeoObject>) retornaObjects(cdViaturaSigma);
					if (eventosList != null && !eventosList.isEmpty())
					{
						stringBuilder.append("<span class=\"gm-style-iw\"><b> Posição: </b> </span> <select id='cbPosicao' ");
						stringBuilder.append("panelHeight=\"auto\" editable=\"false\" style=\"width: 40px\" >");
						while (i <= eventosList.size())
						{
							stringBuilder.append("<option value=" + i + ">" + i + "</option>");
							i++;
						}

						stringBuilder.append("</select>&nbsp<a href=\"javascript:atualizaPosicaoEventoFila(" + codHistorico + ")\" title='Confirmar!' ><img style=\"border : none\" src=\"images/accept.png\"/></a>");

					}
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsMapsServlet listaPosicoes : " + e.getMessage());
		}
		finally
		{
			if (i == 2)
			{
				stringBuilder = new StringBuilder();
				stringBuilder.append("");
			}

			return stringBuilder.toString();
		}

	}

	private void atualizaPosicaoEventoFila(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String codHistorico, int posicao)
	{

		NeoObject eventosObj = null;
		Integer i = 1;
		String retorno = "OK";
		int origem = 0;
		try
		{

			if (NeoUtils.safeIsNotNull(codHistorico))
			{
				eventosObj = (NeoObject) retornaObject(codHistorico);

				if (eventosObj != null)
				{

					NeoObject noPS = (NeoObject) eventosObj;

					EntityWrapper psWrapper = new EntityWrapper(noPS);
					String cdViaturaSigma = (String) psWrapper.getValue("cdViaturaSigma");
					Collection<NeoObject> eventosList = null;
					eventosList = (Collection<NeoObject>) retornaObjects(cdViaturaSigma);
					if (eventosList != null && !eventosList.isEmpty())
					{

						for (NeoObject neoObject : eventosList)
						{
							EntityWrapper entityWrapper = new EntityWrapper(neoObject);
							String historico = (String) entityWrapper.getValue("codHistorico");
							if (codHistorico.equals(historico))
							{
								origem = i;
								break;
							}

							i++;
						}
						i = 1;
						List<EventoEsperaVO> eventoEsperaVOsList = new ArrayList<EventoEsperaVO>();
						for (NeoObject neoObject : eventosList)
						{
							EntityWrapper entityWrapper = new EntityWrapper(neoObject);
							EventoEsperaVO eventoEsperaVO = new EventoEsperaVO();
							String cdRota = (String) entityWrapper.getValue("codRota");
							GregorianCalendar dataRetorno = (GregorianCalendar) entityWrapper.getValue("dataEspera");
							Long codigo = (Long) entityWrapper.getValue("codigo");
							String cdCliente = (String) entityWrapper.getValue("cdCliente");
							String cdViatura = (String) entityWrapper.getValue("cdViaturaSigma");
							eventoEsperaVO.setCodHistorico(codHistorico);
							eventoEsperaVO.setCodRota(cdRota);
							eventoEsperaVO.setDataEspera(NeoDateUtils.safeDateFormat(dataRetorno, "dd/MM/yyyy HH:mm:ss.SSS"));
							eventoEsperaVO.setCodigo(Long.parseLong(codigo.toString()));
							eventoEsperaVO.setCliente(cdCliente);
							eventoEsperaVO.setCodigoViatura(cdViatura);
							eventoEsperaVOsList.add(eventoEsperaVO);

						}
						EventoEsperaVO vo = eventoEsperaVOsList.get(origem - 1);
						eventoEsperaVOsList.remove(origem - 1);
						vo.setCodigo(Long.parseLong(String.valueOf(posicao)));
						posicao = posicao - 1;
						eventoEsperaVOsList.add(posicao, vo);
						i = 1;

						//Collections.sort(eventoEsperaVOsList);
						GregorianCalendar dataEspera = new GregorianCalendar();

						for (EventoEsperaVO eventoEsperaVO2 : eventoEsperaVOsList)
						{
							for (NeoObject eventoEsperaVO : eventosList)
							{
								EntityWrapper entityWrapper = new EntityWrapper(eventoEsperaVO);
								dataEspera = (GregorianCalendar) entityWrapper.getValue("dataEspera");
								if (eventoEsperaVO2.getDataEspera().equals(NeoDateUtils.safeDateFormat(dataEspera, "dd/MM/yyyy HH:mm:ss.SSS")))
								{
									entityWrapper.findField("codigo").setValue(Long.parseLong(i.toString()));
									PersistEngine.persist(eventoEsperaVO);
								}

							}
							i++;
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsMapsServlet ao verificar a cor prioridade : " + e.getMessage());
			retorno = "Erro";
		}
		finally
		{
			out.print(retorno);
		}

	}

	private void saveLogPopUp(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdHistorico, String textoLog)
	{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String texto = null;
		try
		{
			texto = textoLog;

			conn = PersistEngine.getConnection("SIGMA90");
			StringTokenizer token = new StringTokenizer(texto, ";");
			conn.setAutoCommit(false);
			while (token.hasMoreTokens())
			{

				String textoLocal = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + " - Operador(a) " + PortalUtil.getCurrentUser().getFullName() + " : " + token.nextToken();
				String array[] = textoLocal.split(":");
				if (!array[1].contains("  ") || !array[1].isEmpty())
				{
					StringBuilder sqlUpdate = new StringBuilder();
					sqlUpdate.append(" UPDATE HISTORICO  SET  TX_OBSERVACAO_FECHAMENTO = ? + (CHAR(10) + CHAR(13)) +  TX_OBSERVACAO_FECHAMENTO    WHERE CD_HISTORICO = ?");
					st = conn.prepareStatement(sqlUpdate.toString());
					st.setString(1, textoLocal);
					st.setString(2, cdHistorico);
					st.executeUpdate();
				}
			}
			conn.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	private void getLogMotorista(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String placa)
	{
		String resultLog = "";
		try
		{

			QLGroupFilter filterAnd = new QLGroupFilter("AND");
			filterAnd.addFilter(new QLEqualsFilter("nm_placa", placa));
			ExternalEntityInfo infoVTR = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SIGMA90VIATURA");
			List<NeoObject> listaVTR = (List<NeoObject>) PersistEngine.getObjects(infoVTR.getEntityClass(), filterAnd);

			if (listaVTR != null && !listaVTR.isEmpty())
			{

				//NeoObject otsViatura = (NeoObject) listaVTR.get(0);

				//			String resultLog = "<a href=\"javascrit:void(0)\" class=\"closeButton\" onclick=\"removerTooltips();\"></a><br><b>Log:</b></br>";

				//EntityWrapper entityWrapper = new EntityWrapper(otsViatura);

				QLInFilter viaturaFilter = new QLInFilter("viatura", listaVTR);
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.add(Calendar.DATE, -3);
				//QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) entityWrapper.findValue("dataMovimentacao")); 
				QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) calendar);
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(viaturaFilter);
				groupFilter.addFilter(datefilter);

				@SuppressWarnings("rawtypes")
				Class clazz = AdapterUtils.getEntityClass("SIGMALogViatura");

				Collection<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilter, -1, -1, "dataLog desc");

				if (logs != null && logs.size() > 0)
				{
					String linha = "";
					for (NeoObject log : logs)
					{
						EntityWrapper logWrapper = new EntityWrapper(log);

						String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
						String texto = (String) logWrapper.findValue("textoLog");
						linha = "<div style=\"overflow-y: scroll;\">";
						if (texto != null && !texto.isEmpty())
						{
							texto = texto.replaceAll("PULA-LIGA ", "");
							String image = "";
							if (texto.contains("excesso de tempo"))
								image = "warning_5";
							else if (texto.contains("deslocamento com atraso"))
								image = "warning_2";
							else if (texto.contains("devido a atraso após inicio do deslocamento"))
								image = "warning_9";

							if (texto.contains("Ligação atendida pelo AIT."))
							{
								image = "log";
							}
							else if (texto.contains("Ligação não atendida pelo AIT."))
							{
								image = "log";

							}
							else if (texto.contains("Ligação foi atendida e desligada pelo AIT ente de falar com operador."))
							{
								image = "log";
							}
							else if (texto.contains("Efeturar próxima ligação, varias tentativas sem sucesso!"))
							{
								image = "log";
							}
							else if (texto.contains("Falha ao efetuar a ligação, canal ocupado!"))
							{
								image = "log";
							}
							else if (texto.contains("Falha ao efetuar a ligação, numero não existe!"))
							{
								image = "log";
							}
							else if (texto.contains("Falha ao efetuar a ligação, numero não existe!"))
							{
								image = "log";
							}
							else if (texto.contains("Falha ao efetuar a ligação, caixa postal!"))
							{
								image = "log";
							}
							else if (texto.contains("Ligando para AIT devido ao alerta de deslocamento"))
							{
								image = "log";
							}

							if (texto.contains("Sistema Fusion.") || (image != "" && image.equals("log")))
								linha = "<li class=\"gm-addr\">" + formatedDate + " -  <a title=\"" + texto + "\"><img src=\"images/" + image + ".png\"  /></a></li>";
							else
								linha = "<li class=\"gm-addr\">" + formatedDate + " - " + texto + "</li>";

							resultLog += linha;
						}
					}
					linha = "</div>";
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			out.print(resultLog.replaceAll("\"", "'"));
		}

	}

	private void getFilaEventos(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		FilaAlertAitVO vo = null;
		List<FilaAlertAitVO> listAlertAitVOs = new ArrayList<FilaAlertAitVO>();
		try
		{
			String regional = request.getParameter("regional");
			if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty())
			{

				List<CallAlertAitVO> lista = new ArrayList<CallAlertAitVO>();
				TreeMap<Long, Long> listaLigacao = new TreeMap<Long, Long>();

				lista.addAll(OrsegupsAlertAitEngine.treeMap.values());
				Collections.sort(lista, new CallAlertAitVO());

				int cont = 1;

				if (OrsegupsAlertAitEngine.successCallMap != null && !OrsegupsAlertAitEngine.successCallMap.isEmpty())
				{
					for (Entry<String, CallAlertAitVO> entry : OrsegupsAlertAitEngine.successCallMap.entrySet())
					{
						if (entry != null)
						{
							CallAlertAitVO callAlertAitVO = entry.getValue();
							if (callAlertAitVO != null && callAlertAitVO.getCallViaturaVO().getTelefone() != null && !callAlertAitVO.getCallViaturaVO().getTelefone().isEmpty() && entry.getKey() != null && !entry.getKey().isEmpty())
							{
								listaLigacao.put(Long.parseLong(callAlertAitVO.getCallViaturaVO().getTelefone()), Long.parseLong(entry.getKey()));
							}
						}
					}
					//					for (CallAlertAitVO callAlertAitVO : OrsegupsAlertAitEngine.successCallMap.values())
					//					{
					//						if (callAlertAitVO.getCallViaturaVO() != null)
					//							listaLigacao.add(callAlertAitVO.getCallViaturaVO().getTelefone());
					//					}
				}

				for (CallAlertAitVO aitVO : lista)
				{

					if ((regional == null || regional.contains("null")) || regional.contains(aitVO.getCallViaturaVO().getNomeMotorista().substring(0, 3)))
					{

						vo = new FilaAlertAitVO();
						int minutesSinceLastCall = Minutes.minutesBetween(new DateTime(aitVO.getLastCallCalendar()), new DateTime(new GregorianCalendar())).getMinutes();
						int secondsSinceLastCall = Seconds.secondsBetween(new DateTime(aitVO.getLastCallCalendar()), new DateTime(new GregorianCalendar())).getSeconds();
						int minutesSinceLastCall2 = Minutes.minutesBetween(new DateTime(aitVO.getListCallCalendar()), new DateTime(new GregorianCalendar())).getMinutes();
						int secondsSinceLastCall2 = Seconds.secondsBetween(new DateTime(aitVO.getListCallCalendar()), new DateTime(new GregorianCalendar())).getSeconds();
						vo.setPosicao(String.valueOf(cont) + "º");
						vo.setAit(aitVO.getCallViaturaVO().getCodigoViatura() + " - " + aitVO.getCallViaturaVO().getNomeMotorista());
						vo.setConta(aitVO.getCallEventoVO().getCodigoCentral() + "[" + aitVO.getCallEventoVO().getParticao() + "]");
						vo.setFantasia(aitVO.getCallEventoVO().getFantasia());
						vo.setTempo(String.format("%02d", minutesSinceLastCall) + ":" + String.format("%02d", secondsSinceLastCall % 60));
						vo.setTempoFila(String.format("%02d", minutesSinceLastCall2) + ":" + String.format("%02d", secondsSinceLastCall2 % 60));
						vo.setLog("<a  id=\"div8\"  onmouseenter=\"javascript:getLogMotorista('" + aitVO.getCallViaturaVO().getPlaca() + "',this);\" href=\"javascript:saveInputLogMotorista('" + aitVO.getCallViaturaVO().getCodigoViatura() + "')\"><img style=\"border : none\" src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");

						if (aitVO.getCallViaturaVO().getTelefone() != null && !aitVO.getCallViaturaVO().getTelefone().trim().isEmpty()){
						    aitVO.getCallViaturaVO().setTelefone(aitVO.getCallViaturaVO().getTelefone().replaceAll("[^0-9]", ""));
						    if (listaLigacao != null && !listaLigacao.isEmpty() && listaLigacao.containsKey(Long.parseLong(aitVO.getCallViaturaVO().getTelefone())))
						    {
							vo.setEmAtendimento(Boolean.TRUE);
							String nomeStr = neoUser(String.valueOf(listaLigacao.get(Long.parseLong(aitVO.getCallViaturaVO().getTelefone()))));
							vo.setNomeAtendente(nomeStr);
						    }
						    
						}
						

						listAlertAitVOs.add(vo);

						cont++;
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Gson gson = new Gson();
			String alertJSON = gson.toJson(listAlertAitVOs);
			out.print(alertJSON);
		}

	}

	@SuppressWarnings("finally")
	private static NeoObject retornaObject(String codHistorico)
	{
		NeoObject eventosObj = null;
		try
		{
			if (codHistorico != null && !codHistorico.isEmpty())
			{
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("codHistorico", codHistorico));
				eventosObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), groupFilter);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR HISTORICO SIGMAEventosFilaEspera : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}
		finally
		{
			return eventosObj;
		}

	}

	@SuppressWarnings({ "unchecked", "finally" })
	private static Collection<NeoObject> retornaObjects(String cdViaturaSigma)
	{
		Collection<NeoObject> eventosList = null;
		try
		{
			if (cdViaturaSigma != null && !cdViaturaSigma.isEmpty())
			{
				QLGroupFilter filterAndVtr = new QLGroupFilter("AND");
				filterAndVtr.addFilter(new QLEqualsFilter("cdViaturaSigma", cdViaturaSigma));
				eventosList = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAndVtr, -1, -1, "codigo ASC");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR VIATURA SIGMAEventosFilaEspera : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}
		finally
		{
			return eventosList;
		}

	}

	private void getImageOsVtr(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdOrdem, String cdVtr) throws IOException
	{
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		ResultSet rsH = null;
		ServletOutputStream servletOutputStream = null;

		try
		{
			StringBuilder stringBuilder = new StringBuilder();
			String cdOpcao = null;
			if (cdVtr != null)
			{
				stringBuilder.append(" SELECT BL_IMAGEM,CD_HISTORICO FROM VTR_IMAGEM   WHERE CD_VTR_IMAGEM = ?");
				cdOpcao = cdVtr;
			}
			else if (cdOrdem != null)
			{
				stringBuilder.append(" SELECT BL_IMAGEM,CD_ORDEM_SERVICO FROM OS_IMAGEM  WHERE CD_ORDEM_SERVICO = ?");
				cdOpcao = cdOrdem;
			}

			conn = PersistEngine.getConnection("SIGMA90");
			preparedStatementHSelect = (PreparedStatement) conn.prepareStatement(stringBuilder.toString());
			preparedStatementHSelect.setString(1, cdOpcao);
			rsH = preparedStatementHSelect.executeQuery();
			byte[] imgData = null;
			if (rsH.next())
			{
				imgData = rsH.getBytes(1);
				response.setContentType("image/jpeg");
				response.setContentLength(imgData.length);
				servletOutputStream = response.getOutputStream();
				servletOutputStream.write(imgData);
			}
			else
			{
				log.error("##### ERRO AO LISTAR IMAGEM - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR IMAGEM : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}
		finally
		{
			servletOutputStream.flush();
			servletOutputStream.close();
			OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
		}

	}

	private void getLinkImagemVtr(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdVtr)
	{
		StringBuffer galeria = new StringBuffer();

		if (cdVtr != null)
		{
			Connection conn = OrsegupsUtils.getSqlConnection("SIGMA90");
			PreparedStatement st = null;
			ResultSet rs = null;
			cdVtr = getListaEventosCliente(cdVtr);
			StringBuffer sqlIdFotos = new StringBuffer();
			sqlIdFotos.append(" SELECT CD_VTR_IMAGEM FROM VTR_IMAGEM   WHERE CD_HISTORICO IN (?)");
			try
			{
				st = conn.prepareStatement(sqlIdFotos.toString());
				st.setString(1, cdVtr);
				rs = st.executeQuery();

				galeria.append("<div class=\"sidebar-image-gallery\">");

				while (rs.next())
				{
					Long cdVtrImagem = rs.getLong("CD_VTR_IMAGEM");
					galeria.append("<a target=\"_blank\" href=\"/fusion/custom/jsp/orsegups/utils/fotoSIGMAFromDB.jsp?cdVtrImagem=" + cdVtrImagem + "\"><img width=\"10%\" height=\"10%\" border=\"5\" src=\"/fusion/custom/jsp/orsegups/utils/fotoSIGMAFromDB.jsp?cdVtrImagem=" + cdVtrImagem + "\" width=\"\" height=\"\" alt=\"\"></a>&nbsp;");
				}
				galeria.append("</div>");
				out.print(galeria.toString());

			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO AO PESQUISAR IMAGEM VTR : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, st, rs);
			}

			//galeria.append("<input type=\"button\" class=\"input_button\" id=\"bt_import\" value=\""+cdHistorico+"\" onClick = \"javascript:void();\"/>");				
		}

	}

	private void getLinkImagemOS(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdOS)
	{
		StringBuffer galeria = new StringBuffer();

		if (cdOS != null)
		{
			Connection conn = OrsegupsUtils.getSqlConnection("SIGMA90");
			PreparedStatement st = null;
			ResultSet rs = null;
			StringBuffer sqlIdFotos = new StringBuffer();
			sqlIdFotos.append(" SELECT CD_OS_IMAGEM FROM OS_IMAGEM  WHERE CD_ORDEM_SERVICO IN (" + 1430362 + ") ");
			try
			{
				st = conn.prepareStatement(sqlIdFotos.toString());
				rs = st.executeQuery();

				//galeria.append("<div class=\"sidebar-image-gallery\">");
				galeria.append("<div class=\"demonstrations\"  >	");

				while (rs.next())
				{
					Long cdOSImagem = rs.getLong("CD_OS_IMAGEM");
					//galeria.append("<a target=\"_blank\" href=\"/fusion/custom/jsp/orsegups/utils/fotoSIGMAFromDB.jsp?cdOsImagem=" + cdOSImagem + "\"><img width=\"25%\" height=\"25%\" border=\"1\" src=\"/fusion/custom/jsp/orsegups/utils/fotoSIGMAFromDB.jsp?cdOsImagem=" + cdOSImagem + "\" width=\"\" height=\"\" alt=\"\"></a>&nbsp;");
					galeria.append("<a   class='lightview' data-lightview-options=\" width: 640,     height: 400   \"  data-lightview-group='example' target=\"_blank\" href=\"/fusion/custom/jsp/orsegups/utils/fotoSIGMAFromDB.jsp?cdOsImagem=" + cdOSImagem + "\"><img  class=\"lightview\"  width=\"18%\" height=\"18%\" src=\"/fusion/custom/jsp/orsegups/utils/fotoSIGMAFromDB.jsp?cdOsImagem=" + cdOSImagem + "\" width=\"\" height=\"\" alt=\"\"></a>&nbsp;");

				}
				galeria.append("</div>");
				out.print(galeria.toString());
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO AO PESQUISAR IMAGEM OS : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, st, rs);

			}

			//galeria.append("<input type=\"button\" class=\"input_button\" id=\"bt_import\" value=\""+cdHistorico+"\" onClick = \"javascript:void();\"/>");				
		}

	}

	private String getListaEventosCliente(String cdCliente)
	{

		String eventos = "";
		if (cdCliente != null)
		{

			Connection conn = OrsegupsUtils.getSqlConnection("SIGMA90");
			PreparedStatement st = null;
			ResultSet rs = null;

			StringBuffer sqlIdFotos = new StringBuffer();
			sqlIdFotos.append(" SELECT CD_HISTORICO FROM VIEW_HISTORICO  WHERE CD_CLIENTE = ? ORDER BY DT_FECHAMENTO ASC");
			try
			{
				st = conn.prepareStatement(sqlIdFotos.toString());
				st.setString(1, cdCliente);
				rs = st.executeQuery();
				int contador = 0;
				while (rs.next())
				{
					if (contador > 0)
						eventos += ",";
					String cdHistorico = rs.getString("CD_HISTORICO");
					eventos += cdHistorico;
					contador++;
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO AO PESQUISAR LISTA DE EVENTOS CLIENTE: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, st, rs);

			}

		}
		return eventos;
	}

//	private String getListaOSCliente(String cdCliente)
//	{
//
//		String ordemServico = "";
//		if (cdCliente != null)
//		{
//
//			Connection conn = null;
//			PreparedStatement st = null;
//			ResultSet rs = null;
//
//			StringBuffer sqlIdFotos = new StringBuffer();
//			sqlIdFotos.append(" SELECT ID_ORDEM FROM dbORDEM  WHERE CD_CLIENTE = ? ORDER BY ID_ORDEM ASC");
//			try
//			{
//				conn = OrsegupsUtils.getSqlConnection("SIGMA90");
//				st = conn.prepareStatement(sqlIdFotos.toString());
//				st.setString(1, cdCliente);
//				rs = st.executeQuery();
//				int contador = 0;
//				while (rs.next())
//				{
//					if (contador > 0)
//						ordemServico += ",";
//					String cdOs = rs.getString("ID_ORDEM");
//					ordemServico += cdOs;
//					contador++;
//				}
//
//			}
//			catch (Exception e)
//			{
//				e.printStackTrace();
//				log.error("##### ERRO AO PESQUISAR LISTA DE OS CLIENTE: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
//
//			}
//			finally
//			{
//				OrsegupsUtils.closeConnection(conn, st, rs);
//
//			}
//
//		}
//		return ordemServico;
//	}

	private void salvarLogAcaoEvento(String historico, String texto)
	{

		if (historico != null && !historico.isEmpty() && texto != null && !texto.isEmpty())
		{

			Connection conn = null;
			PreparedStatement st = null;
			ResultSet rs = null;

			StringBuffer sqlIdFotos = new StringBuffer();

			sqlIdFotos.append("  SELECT  h.CD_EVENTO, ");
			sqlIdFotos.append("  REPLACE(SUBSTRING(r.NM_ROTA, CHARINDEX('AA', r.NM_ROTA), 4), '-', '') AS ROTA, SUBSTRING(r.NM_ROTA, 1, 3) AS REG, ");
			sqlIdFotos.append("  c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.CD_CLIENTE, c.ID_EMPRESA,h.CD_VIATURA, ");
			sqlIdFotos.append("  VI.NM_PLACA,VI.NM_VIATURA  ");
			sqlIdFotos.append("  FROM HISTORICO h WITH (NOLOCK) ");
			sqlIdFotos.append("  INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE  ");
			sqlIdFotos.append("  INNER JOIN rota r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			sqlIdFotos.append("  INNER JOIN dbCODE cuc WITH (NOLOCK) ON cuc.ID_CODE = h.CD_CODE ");
			sqlIdFotos.append("  LEFT JOIN VIATURA VI WITH (NOLOCK) ON h.CD_VIATURA = VI.CD_VIATURA ");
			sqlIdFotos.append("  WHERE h.CD_HISTORICO = ?  ");

			try
			{
				conn = OrsegupsUtils.getSqlConnection("SIGMA90");
				st = conn.prepareStatement(sqlIdFotos.toString());
				st.setString(1, historico);
				rs = st.executeQuery();
				EventoVO eventoVO = null;
				while (rs.next())
				{
					eventoVO = new EventoVO();

					String codigoEvento = (rs.getString("CD_EVENTO") == null ? "" : rs.getString("CD_EVENTO"));
					eventoVO.setCodigoEvento(codigoEvento);

					String rota = (rs.getString("ROTA") == null ? "" : rs.getString("ROTA"));
					eventoVO.setRota(rota);

					String idCentral = (rs.getString("ID_CENTRAL") == null ? "" : rs.getString("ID_CENTRAL"));
					eventoVO.setCodigoCentral(idCentral);

					String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
					eventoVO.setParticao(particao);

					String fantasia = (rs.getString("FANTASIA") == null ? "" : rs.getString("FANTASIA"));
					eventoVO.setFantasia(fantasia);

					String cdCliente = (rs.getString("CD_CLIENTE") == null ? "" : rs.getString("CD_CLIENTE"));
					eventoVO.setCodigoCliente(cdCliente);

					String empresa = (rs.getString("ID_EMPRESA") == null ? "" : rs.getString("ID_EMPRESA"));
					eventoVO.setEmpresa(empresa);

					String codigoViatura = (rs.getString("CD_VIATURA") == null ? "" : rs.getString("CD_VIATURA"));
					eventoVO.setCodigoViatura(codigoViatura);

					String nmPlaca = (rs.getString("NM_PLACA") == null ? "" : rs.getString("NM_PLACA"));
					eventoVO.setPlaca(nmPlaca);

					String nmViatura = (rs.getString("NM_VIATURA") == null ? "" : rs.getString("NM_VIATURA"));
					eventoVO.setNomeViatura(nmViatura);

					eventoVO.setCodigoHistorico(historico);

				}
				OrsegupsUtils.closeConnection(conn, st, rs);

				if (eventoVO != null)
				{
					NeoObject logEvento = AdapterUtils.createNewEntityInstance("SIGMALogEvento");

					if (logEvento != null)
					{
						EntityWrapper eventoWrapper = new EntityWrapper(logEvento);
						eventoWrapper.setValue("historico", eventoVO.getCodigoHistorico());
						eventoWrapper.setValue("evento", eventoVO.getCodigoEvento());
						eventoWrapper.setValue("rota", eventoVO.getRota());
						eventoWrapper.setValue("codigoCentral", eventoVO.getCodigoCentral());
						eventoWrapper.setValue("particao", eventoVO.getParticao());
						eventoWrapper.setValue("fantasia", eventoVO.getFantasia());
						eventoWrapper.setValue("codigoCliente", eventoVO.getCodigoCliente());
						eventoWrapper.setValue("empresa", eventoVO.getEmpresa());
						eventoWrapper.setValue("codigoViatura", eventoVO.getCodigoViatura());
						eventoWrapper.setValue("placa", eventoVO.getPlaca());
						eventoWrapper.setValue("viatura", eventoVO.getNomeViatura());
						eventoWrapper.setValue("dataLogEvento", new GregorianCalendar());
						eventoWrapper.setValue("textoLog", "Operador(a) " + PortalUtil.getCurrentUser().getFullName() + " " + texto);
						NeoUser neoUser = (NeoUser) PortalUtil.getCurrentUser();
						eventoWrapper.setValue("usuario", neoUser);

						PersistEngine.persist(logEvento);

					}
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO AO salvarLogAcaoEvento: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, st, rs);

			}

		}
	}

	@SuppressWarnings("finally")
	private String neoUser(String ramalAux)
	{
		List<NeoUser> users = new ArrayList<NeoUser>();
		String resposta = "";
		List<String> dados = new ArrayList<String>();
		try
		{
			List<NeoObject> colaborador = PersistEngine.getObjects(AdapterUtils.getEntityClass("colaboradores"), new QLOpFilter("ramal", "LIKE", ramalAux));

			if (colaborador != null && !colaborador.isEmpty())
			{

				for (NeoObject neoObject : colaborador)
				{

					EntityWrapper ewColaborador = new EntityWrapper(neoObject);
					String nome = "";

					nome = (String) ewColaborador.findField("fullName").getValue();
					dados.add(nome);
				}

				if (dados != null && !dados.isEmpty())
				{
					DecimalFormat f = new DecimalFormat("000000000");
					ArrayList<HttpSession> hashMap = new ArrayList<HttpSession>(HttpSessionCollector.getUserSessions().values());
					SortedMap<String, HttpSession> sortedUsers = new TreeMap<String, HttpSession>(Collections.reverseOrder());

					if (hashMap != null && !hashMap.isEmpty())
					{
						for (HttpSession sessao : hashMap)
						{

							long l = 999999999l - sessao.getLastAccessedTime();
							sortedUsers.put(f.format(l), sessao);

						}

						if (sortedUsers != null && !sortedUsers.isEmpty())
						{
							for (Map.Entry<String, HttpSession> entry : sortedUsers.entrySet())
							{
								HttpSession sessao = entry.getValue();
								//final NeoUser user = (NeoUser) sessao.getAttribute("user");
								final String user = (String) sessao.getAttribute("user");

								NeoUser nuser = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", user));
								users.add(nuser);

							}

							if (users != null && !users.isEmpty())
							{
								for (NeoUser user : users)
								{
									if (dados != null && !dados.isEmpty() && dados.contains(user.getFullName()))
									{
										resposta = user.getCode();
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return resposta;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void getLogEvento(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdCliente)
	{

		try
		{

			if (cdCliente != null && !cdCliente.isEmpty())
			{

				String resultLog = "";

				GregorianCalendar calendar = new GregorianCalendar();
				calendar.add(Calendar.DATE, -3);
				QLOpFilter datefilter = new QLOpFilter("dataLogEvento", ">=", (GregorianCalendar) calendar);
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				QLEqualsFilter emUsoFilter = new QLEqualsFilter("codigoCliente", cdCliente);
				groupFilter.addFilter(emUsoFilter);
				groupFilter.addFilter(datefilter);

				Class clazz = AdapterUtils.getEntityClass("SIGMALogEvento");

				Collection<NeoObject> logs = (Collection<NeoObject>) PersistEngine.getObjects(clazz, groupFilter, -1, -1, "dataLogEvento desc");

				if (logs != null && !logs.isEmpty())
				{
					String linha = "";
					for (NeoObject log : logs)
					{
						EntityWrapper logWrapper = new EntityWrapper(log);

						String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLogEvento")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
						String texto = (String) logWrapper.findValue("textoLog");
						String viatura = (String) logWrapper.findValue("viatura");
						String evento = (String) logWrapper.findValue("evento");
						//String historico = (String) logWrapper.findValue("historico");
						linha = "<div style=\"overflow-y: scroll;\">";
						if (texto != null && !texto.isEmpty())
						{

							linha = "<li class=\"gm-addr\">" + formatedDate + " - " + texto + " Para o atendente, " + viatura + " em um evento " + evento + ".</li>";

							resultLog += linha;
						}
					}
					linha = "</div>";
				}

				out.print(resultLog.replaceAll("\"", "'"));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

}
