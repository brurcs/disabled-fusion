package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.util.NeoUtils;

public class EscalaPostoVO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private Long numloc;
	private Long taborg;
	private List<HorarioVO> horariosSegunda;
	private List<HorarioVO> horariosTerca;
	private List<HorarioVO> horariosQuarta;
	private List<HorarioVO> horariosQuinta;
	private List<HorarioVO> horariosSexta;
	private List<HorarioVO> horariosSabado;
	private List<HorarioVO> horariosDomingo;
	private List<HorarioVO> horariosFeriado;
	private Long diaSemana;
	private Long permanenciaPosto;

	public Long getNumloc()
	{
		return numloc;
	}

	public void setNumloc(Long numloc)
	{
		this.numloc = numloc;
	}

	public Long getTaborg()
	{
		return taborg;
	}

	public void setTaborg(Long taborg)
	{
		this.taborg = taborg;
	}

	public List<HorarioVO> getHorariosHoje()
	{
		if (diaSemana == null) {
			System.out.println("Teste");
		}
		
		List<HorarioVO> horarios = new ArrayList<HorarioVO>();
		Integer diaSemInt = Integer.valueOf(diaSemana.toString());  
		
		switch (diaSemInt)
		{
			case 1:
				horarios = horariosSegunda;
				break;
			case 2:
				horarios = horariosTerca;
				break;
			case 3:
				horarios = horariosQuarta;
				break;
			case 4:
				horarios = horariosQuinta;
				break;
			case 5:
				horarios = horariosSexta;
				break;
			case 6:
				horarios = horariosSabado;
				break;
			case 7:
				horarios = horariosDomingo;
				break;
			case 8:
				horarios = horariosFeriado;
				break;
			default:
				break;
		}
		return horarios;
	}

	public Long getDiaSemana()
	{
		return diaSemana;
	}

	public void setDiaSemana(Long diaSemana)
	{
		this.diaSemana = diaSemana;
	}

	public List<HorarioVO> getHorariosSegunda()
	{
		return horariosSegunda;
	}

	public void setHorariosSegunda(List<HorarioVO> horariosSegunda)
	{
		this.horariosSegunda = horariosSegunda;
	}

	public List<HorarioVO> getHorariosTerca()
	{
		return horariosTerca;
	}

	public void setHorariosTerca(List<HorarioVO> horariosTerca)
	{
		this.horariosTerca = horariosTerca;
	}

	public List<HorarioVO> getHorariosQuarta()
	{
		return horariosQuarta;
	}

	public void setHorariosQuarta(List<HorarioVO> horariosQuarta)
	{
		this.horariosQuarta = horariosQuarta;
	}

	public List<HorarioVO> getHorariosQuinta()
	{
		return horariosQuinta;
	}

	public void setHorariosQuinta(List<HorarioVO> horariosQuinta)
	{
		this.horariosQuinta = horariosQuinta;
	}

	public List<HorarioVO> getHorariosSexta()
	{
		return horariosSexta;
	}

	public void setHorariosSexta(List<HorarioVO> horariosSexta)
	{
		this.horariosSexta = horariosSexta;
	}

	public List<HorarioVO> getHorariosSabado()
	{
		return horariosSabado;
	}

	public void setHorariosSabado(List<HorarioVO> horariosSabado)
	{
		this.horariosSabado = horariosSabado;
	}

	public List<HorarioVO> getHorariosDomingo()
	{
		return horariosDomingo;
	}

	public void setHorariosDomingo(List<HorarioVO> horariosDomingo)
	{
		this.horariosDomingo = horariosDomingo;
	}

	public List<HorarioVO> getHorariosFeriado()
	{
		return horariosFeriado;
	}

	public void setHorariosFeriado(List<HorarioVO> horariosFeriado)
	{
		this.horariosFeriado = horariosFeriado;
	}

	public Long getPermanenciaPosto()
	{
		return permanenciaPosto;
	}

	public void setPermanenciaPosto(Long permanenciaPosto)
	{
		this.permanenciaPosto = permanenciaPosto;
	}

	public String getLogContent()
	{
		return QLPresencaUtils.getLogEscalaPostoLink(this);
	}

	@Override
	public String toString()
	{

		String horarioTexto = "";
		String separador = "";

		if (getHorariosHoje() != null)
		{
			for (HorarioVO hor : getHorariosHoje())
			{
				GregorianCalendar dataIni = hor.getDataInicial();
				GregorianCalendar dataFim = hor.getDataFinal();

				horarioTexto += separador + NeoUtils.safeDateFormat(dataIni, "HH:mm") + "-" + NeoUtils.safeDateFormat(dataFim, "HH:mm");
				separador = " / ";
			}
		}

		if (horarioTexto.isEmpty())
		{
			horarioTexto = "Sem Escala";
		}
		return horarioTexto;
	}
}