package com.neomind.fusion.custom.orsegups.sigma;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;

import us.monoid.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.callcenter.SearchObjectVO;
import com.neomind.fusion.custom.orsegups.callcenter.SigmaVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoAcessoClienteVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.DBOrdemVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.OSDefeitoVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.OSSolicitanteVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;

public class SigmaUtils
{
	private static final Log log = LogFactory.getLog(SigmaUtils.class);
	private static String WEB_SERVICE_SIGMA = "http://192.168.20.218:8080/SigmaWebServices/";

	public static String getLogColaboradorLink(Long codigoColaborador)
	{
		String contentString = "";

		if (codigoColaborador != null)
		{
			String href = "href=\"javascript:saveInputLog('" + codigoColaborador + "', 'colaborador');\"";

			String textoLog = getLogColaborador(codigoColaborador);

			contentString = " <a  tabIndex=\"-1\" " + href + " onmouseover=\"return overlib('" + textoLog + "', CAPTION, 'Log', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/document_v_16x16-trans.png\"/></a> ";
		}

		return contentString;
	}

	public static String getLogOSLink(Long idOrdem)
	{
		String contentString = "";

		if (idOrdem != null)
		{
			String href = "href=\"javascript:saveInputLog('" + idOrdem + "', 'os');\"";

			String textoLog = getLogOS(idOrdem);

			contentString = " <a  tabIndex=\"-1\" " + href + " onmouseover=\"return overlib('" + textoLog + "', CAPTION, 'Log da OS', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/document_v_16x16-trans.png\"/></a> ";
		}

		return contentString;
	}

	public static String getLogViaturaLink(Long codigoViatura)
	{
		String contentString = "";

		if (codigoViatura != null)
		{
			String href = "href=\"javascript:saveInputLog('" + codigoViatura + "', 'viatura');\"";

			String textoLog = getLogViatura(codigoViatura);

			contentString = " <a  tabIndex=\"-1\" " + href + " onmouseover=\"return overlib('" + textoLog + "', CAPTION, 'Log', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/document_v_16x16-trans.png\"/></a> ";
		}

		return contentString;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String getLogColaborador(Long codigoColaborador)
	{
		String resultLog = "<b>Log:</b></br>";

		List<NeoObject> objColaborador = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACOLABORADOR"), new QLEqualsFilter("cd_colaborador", codigoColaborador));

		if (objColaborador != null && objColaborador.size() > 0)
		{

			QLEqualsFilter colFilter = new QLEqualsFilter("colaborador", objColaborador.get(0));

			GregorianCalendar datalimite = new GregorianCalendar();
			Long tempoLogColaborador = ((Long) getQLParameters().findValue("tempoLogColaborador"));
			datalimite.add(Calendar.DAY_OF_MONTH, -tempoLogColaborador.intValue());

			QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) datalimite);

			QLGroupFilter groupFilterColaborador = new QLGroupFilter("AND");
			groupFilterColaborador.addFilter(colFilter);
			groupFilterColaborador.addFilter(datefilter);

			Class clazz = AdapterUtils.getEntityClass("SIGMALogColaborador");

			Collection<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilterColaborador, -1, -1, "dataLog desc");

			if (logs != null && logs.size() > 0)
			{
				for (NeoObject log : logs)
				{
					EntityWrapper logWrapper = new EntityWrapper(log);

					String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
					String texto = (String) NeoUtils.removeTagsFromText((String) logWrapper.findValue("textoLog"));

					if (texto != null && !texto.isEmpty())
					{
						String linha = "<li>" + formatedDate + " - " + texto + "</li>";
						resultLog += linha;
					}
				}
			}
		}
		return resultLog.replaceAll("\"", "'");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String getLogViatura(Long codigoViatura)
	{
		String resultLog = "<b>Log:</b></br>";

		List<NeoObject> objViatura = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMA90VIATURA"), new QLEqualsFilter("cd_viatura", codigoViatura));

		if (objViatura != null && objViatura.size() > 0)
		{

			QLEqualsFilter vtrFilter = new QLEqualsFilter("viatura", objViatura.get(0));

			GregorianCalendar datalimite = new GregorianCalendar();
			Long tempoLogViatura = ((Long) getQLParameters().findValue("tempoLogViatura"));
			datalimite.add(Calendar.DAY_OF_MONTH, -tempoLogViatura.intValue());

			QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) datalimite);

			QLGroupFilter groupFilterViatura = new QLGroupFilter("AND");
			groupFilterViatura.addFilter(vtrFilter);
			groupFilterViatura.addFilter(datefilter);

			Class clazz = AdapterUtils.getEntityClass("SIGMALogViatura");

			Collection<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilterViatura, -1, -1, "dataLog desc");

			if (logs != null && logs.size() > 0)
			{
				for (NeoObject log : logs)
				{
					EntityWrapper logWrapper = new EntityWrapper(log);

					String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
					String texto = (String) logWrapper.findValue("textoLog");

					if (texto != null && !texto.isEmpty())
					{
						String linha = "<li>" + formatedDate + " - " + texto + "</li>";
						resultLog += linha;
					}
				}
			}
		}
		return resultLog.replaceAll("\"", "'");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String getLogOS(Long idOS)
	{
		String resultLog = "<b>Log:</b></br>";

		List<NeoObject> objOS = PersistEngine.getObjects(AdapterUtils.getEntityClass("dbORDEM"), new QLEqualsFilter("id_ordem", idOS));

		if (objOS != null && objOS.size() > 0)
		{

			QLEqualsFilter osFilter = new QLEqualsFilter("os", objOS.get(0));

			GregorianCalendar datalimite = new GregorianCalendar();
			Long tempoLogViatura = ((Long) getQLParameters().findValue("tempoLogOS"));
			datalimite.add(Calendar.DAY_OF_MONTH, -tempoLogViatura.intValue());

			QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) datalimite);

			QLGroupFilter groupFilterOS = new QLGroupFilter("AND");
			groupFilterOS.addFilter(osFilter);
			groupFilterOS.addFilter(datefilter);

			Class clazz = AdapterUtils.getEntityClass("SIGMALogOS");

			Collection<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilterOS, -1, -1, "dataLog desc");

			if (logs != null && logs.size() > 0)
			{
				for (NeoObject log : logs)
				{
					EntityWrapper logWrapper = new EntityWrapper(log);

					String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
					String texto = (String) logWrapper.findValue("textoLog");

					if (texto != null && !texto.isEmpty())
					{
						String linha = "<li>" + formatedDate + " - " + texto + "</li>";
						resultLog += linha;
					}
				}
			}
		}
		return resultLog.replaceAll("\"", "'");
	}

	@SuppressWarnings("unchecked")
	public static EntityWrapper getQLParameters()
	{
		List<NeoObject> listaParametros = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAParametros"));
		EntityWrapper wrapperParametros = null;

		if (listaParametros != null)
		{
			wrapperParametros = new EntityWrapper(listaParametros.get(0));
		}

		return wrapperParametros;
	}

	public DBOrdemVO buscaOS(Long idOrdem)
	{
		DBOrdemVO ordem = new DBOrdemVO();
		Connection connSigma = PersistEngine.getConnection("SIGMA90");

		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT ord.ID_ORDEM, ord.ID_INSTALADOR, ord.IDOSDEFEITO, ord.CD_OS_SOLICITANTE, ord.DEFEITO, ord.DATAAGENDADA, ");
			sql.append(" 		cen.CD_CLIENTE, cen.ID_CENTRAL, cen.PARTICAO, cen.FANTASIA, cen.RAZAO, rel.NM_COLABORADOR, ISNULL(pat.NU_QUANTIDADE, 0) AS FRANQUIA ");
			sql.append(" FROM DBORDEM ord ");
			sql.append(" INNER JOIN DBCENTRAL cen ON cen.CD_CLIENTE = ord.CD_CLIENTE ");
			sql.append(" INNER JOIN COLABORADOR rel ON rel.CD_COLABORADOR = cen.CD_TECNICO_RESPONSAVEL ");
			sql.append(" LEFT OUTER JOIN PATRIMONIO pat ON pat.CD_CLIENTE = cen.CD_CLIENTE ");
			sql.append(" LEFT OUTER JOIN PRODUTO pro ON pro.CD_PRODUTO = pat.CD_PRODUTO AND pro.CD_PRODUTO = 20042 ");
			sql.append(" WHERE ord.ID_ORDEM = ? ");

			PreparedStatement st = connSigma.prepareStatement(sql.toString());
			st.setLong(1, idOrdem);

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				ordem.setId(rs.getLong("ID_ORDEM"));
				ordem.setIdInstalador(rs.getLong("ID_INSTALADOR"));
				ordem.setIdDefeito(rs.getLong("IDOSDEFEITO"));
				ordem.setIdSolicitacao(rs.getLong("CD_OS_SOLICITANTE"));
				ordem.setDescricao(rs.getString("DEFEITO"));
				ordem.setCdCliente(rs.getLong("CD_CLIENTE"));
				ordem.setContaParticao(rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]");
				ordem.setRazao(rs.getString("RAZAO"));
				ordem.setFantasia(rs.getString("FANTASIA"));
				ordem.setTecnicoForcarOS(rs.getString("NM_COLABORADOR"));

				if (rs.getTimestamp("DATAAGENDADA") != null)
				{
					GregorianCalendar data = new GregorianCalendar();
					data.setTime(rs.getTimestamp("DATAAGENDADA"));

					ordem.setData(NeoUtils.safeDateFormat(data, "dd/MM/yyyy"));
					ordem.setHora(String.format("%02d", data.get(Calendar.HOUR_OF_DAY)));
					ordem.setMinuto(String.format("%02d", data.get(Calendar.MINUTE)));
				}

			}

			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connSigma.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return ordem;
	}

	public List<ColaboradorVO> listaColaboradores()
	{
		List<ColaboradorVO> colaboradores = new ArrayList<ColaboradorVO>();
		ColaboradorVO colaborador = new ColaboradorVO();

		Connection connSigma = PersistEngine.getConnection("SIGMA90");

		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT col.CD_COLABORADOR, col.NM_COLABORADOR ");
			sql.append(" FROM COLABORADOR col WHERE col.FG_ATIVO_COLABORADOR = 1 ");
			sql.append(" ORDER BY col.NM_COLABORADOR ");

			PreparedStatement st = connSigma.prepareStatement(sql.toString());

			ResultSet rs = st.executeQuery();

			//Situacaos
			while (rs.next())
			{
				colaborador = new ColaboradorVO();
				colaborador.setId(rs.getLong("CD_COLABORADOR"));
				colaborador.setNome(rs.getString("NM_COLABORADOR"));
				colaboradores.add(colaborador);
			}

			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connSigma.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return colaboradores;
	}

	public List<OSDefeitoVO> listaDefeitos()
	{
		List<OSDefeitoVO> defeitos = new ArrayList<OSDefeitoVO>();
		OSDefeitoVO defeito = new OSDefeitoVO();

		Connection connSigma = PersistEngine.getConnection("SIGMA90");

		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT def.IDOSDEFEITO, def.DESCRICAODEFEITO ");
			sql.append(" FROM OSDEFEITO def WHERE def.FG_ATIVO = 1 ");
			sql.append(" ORDER BY def.DESCRICAODEFEITO ");

			PreparedStatement st = connSigma.prepareStatement(sql.toString());

			ResultSet rs = st.executeQuery();

			//Situacaos
			while (rs.next())
			{
				defeito = new OSDefeitoVO();
				defeito.setId(rs.getLong("IDOSDEFEITO"));
				defeito.setDescricao(rs.getString("DESCRICAODEFEITO"));
				defeitos.add(defeito);
			}

			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connSigma.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return defeitos;
	}

	public List<OSSolicitanteVO> listaSolicitantes()
	{
		List<OSSolicitanteVO> solicitantes = new ArrayList<OSSolicitanteVO>();
		OSSolicitanteVO solicitante = new OSSolicitanteVO();

		Connection connSigma = PersistEngine.getConnection("SIGMA90");

		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT sol.CD_OS_SOLICITANTE, sol.NM_DESCRICAO ");
			sql.append(" FROM OS_SOLICITANTE sol WHERE sol.FG_ATIVO = 1 ");
			sql.append(" ORDER BY sol.NM_DESCRICAO ");

			PreparedStatement st = connSigma.prepareStatement(sql.toString());

			ResultSet rs = st.executeQuery();

			//Situacaos
			while (rs.next())
			{
				solicitante = new OSSolicitanteVO();
				solicitante.setId(rs.getLong("CD_OS_SOLICITANTE"));
				solicitante.setDescricao(rs.getString("NM_DESCRICAO"));
				solicitantes.add(solicitante);
			}

			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connSigma.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return solicitantes;
	}

	/**
	 * Web Service para editar Ordens de Serviço
	 */
	public String receptorOSWebService(String idInstalador, String idDefeito, String idSolicitante, String descricao, String data, String hora, String minuto, String idOrdem)
	{
		String msg = "";

		try
		{
			StringBuilder urlParameters = new StringBuilder();

			urlParameters.append("responsibleTechnician=");
			urlParameters.append(idInstalador);

			urlParameters.append("&defect=");
			urlParameters.append(idDefeito);

			urlParameters.append("&requester=");
			urlParameters.append(idSolicitante);

			urlParameters.append("&description=");
			urlParameters.append(descricao);

			urlParameters.append("&orderId=");
			urlParameters.append(idOrdem);

			String dataHora = "";

			if (data != null && !data.equals(""))
			{
				dataHora = data.substring(6, 10) + "-" + data.substring(3, 5) + "-" + data.substring(0, 2) + "T" + hora + ":" + minuto + ":00Z";
				urlParameters.append("&scheduledTime=");
				urlParameters.append(dataHora);

				urlParameters.append("&estimatedTime=");
				urlParameters.append("1");
			}

			String request = WEB_SERVICE_SIGMA + "ReceptorOSWebService";
			URL url = new URL(request);
			URLConnection conn = url.openConnection();

			conn.setDoOutput(true);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(urlParameters.toString());
			writer.flush();

			String line;
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			while ((line = reader.readLine()) != null)
			{
				JSONObject json = new JSONObject(line);
				if (json.getString("status").equals("success"))
				{
					msg = "OK";
				}
				else
				{
					msg = json.toString();
				}
			}
			writer.close();
			reader.close();
		}
		catch (Exception e)
		{
			msg = "Erro no serviço de OS do SIGMA, favor informar a TI.";
			e.printStackTrace();
		}

		return msg;
	}

	public boolean equipamentoOrsegups(String cdCliente)
	{
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");
		boolean equipamentoOrsegups = false;
		try
		{
			// Equipamento
			StringBuffer sqlEquip = new StringBuffer();
			sqlEquip.append(" SELECT cvs.USU_CodSer ");
			sqlEquip.append(" FROM USU_T160SIG sig ");
			sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
			sqlEquip.append(" WHERE sig.usu_codcli = " + cdCliente + " ");
			sqlEquip.append(" AND cvs.usu_codser IN ('9002011', '9002004', '9002005', '9002014') ");
			sqlEquip.append(" ORDER BY cvs.usu_sitcvs ");

			PreparedStatement stEquip = connSapiens.prepareStatement(sqlEquip.toString());
			ResultSet rsEquip = stEquip.executeQuery();
			if (rsEquip.next())
			{
				equipamentoOrsegups = true;
			}

			rsEquip.close();
			stEquip.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connSapiens.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return equipamentoOrsegups;
	}

	public String fecharEventoSigma(String historico, String cdUsuario)
	{
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		PreparedStatement preparedStatementUpdate = null;
		ResultSet rsH = null;
		List<String> listaHitorico = null;
		String retorno = "OK";
		try
		{

			conn = PersistEngine.getConnection("SIGMA90");

			String selecHtSQL = " SELECT CD_HISTORICO FROM HISTORICO WITH (NOLOCK)  WHERE CD_HISTORICO_PAI = ? ";

			String updateSQL = "  UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = ? WHERE CD_HISTORICO = ? ";
			conn.setAutoCommit(false);
			String hPai = historico;
			preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());
			preparedStatementHSelect.setString(1, hPai);

			listaHitorico = new ArrayList<String>();
			rsH = preparedStatementHSelect.executeQuery();
			while (rsH.next())
			{
				if (NeoUtils.safeIsNotNull(rsH.getString("CD_HISTORICO")))
					listaHitorico.add(rsH.getString("CD_HISTORICO"));
			}

			for (String cdHistorico : listaHitorico)
			{
				preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
				preparedStatementUpdate.setString(1, cdUsuario);
				preparedStatementUpdate.setString(2, cdHistorico);
				preparedStatementUpdate.executeUpdate();

			}

			preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
			preparedStatementUpdate.setString(1, cdUsuario);
			preparedStatementUpdate.setString(2, historico);
			int update = preparedStatementUpdate.executeUpdate();
			
			int updateCount = 0;
			
			while (update < 1 && updateCount < 2){
			    update = preparedStatementUpdate.executeUpdate();
			    updateCount ++;
			}
			conn.commit();
			conn.setAutoCommit(true);
			log.debug("OrsegupsAlertEventEngine fechar eventos historico :  " + historico);

		}
		catch (Exception e)
		{
			retorno = "ERRO";
			try
			{
				conn.rollback();
				log.debug("OrsegupsAlertEventEngine fechar eventos rollback historico :  " + historico);
			}
			catch (SQLException e1)
			{
				log.error("##### ERRO SALVAR LOG EVENT TRANSAÇÃO fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
				e1.printStackTrace();
			}
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			log.debug("Fechando evento no Sigma! " + historico);
			OrsegupsUtils.closeConnection(null, preparedStatementHSelect, null);
			OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, rsH);
			return retorno;
		}

	}

	public Boolean verificaEventoHistorico(String cdHistorico)
	{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		Boolean flag = Boolean.FALSE;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT H.CD_HISTORICO FROM HISTORICO H WITH (NOLOCK)  WHERE H.CD_HISTORICO = ? ");
			//sql.append(" AND  h.FG_STATUS IN (5) AND (( H.CD_EVENTO LIKE 'X406')  OR (H.CD_EVENTO LIKE 'E401' AND H.CD_CODE LIKE 'DLC' ) OR (H.CD_EVENTO LIKE 'XXX2' AND H.CD_CODE LIKE 'EX2') OR (H.CD_EVENTO LIKE 'XXX5' AND H.CD_CODE LIKE 'EX5'))  ");
			st = conn.prepareStatement(sql.toString());
			st.setString(1, cdHistorico);
			rs = st.executeQuery();

			if (rs.next())
				flag = Boolean.TRUE;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT verificaEventoHistorico: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
			return flag;
		}

	}

	public void emEsperaEventoSigma(String historico, String usuario)
	{
		//TODO logica para finalizar o evento no sigma
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		PreparedStatement preparedStatementUpdate = null;
		ResultSet rsH = null;
		List<String> listaHitorico = null;

		try
		{
			if (verificaEventoHistorico(historico))
			{

				conn = PersistEngine.getConnection("SIGMA90");

				String selecHtSQL = " SELECT CD_HISTORICO FROM HISTORICO WITH (NOLOCK)  WHERE CD_HISTORICO_PAI = ? ";

				String updateSQL = " UPDATE HISTORICO SET FG_STATUS = 1 , DT_ESPERA = GETDATE() , CD_USUARIO_ESPERA = ? WHERE CD_HISTORICO = ? ";
				conn.setAutoCommit(false);
				String hPai = historico;
				preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());
				preparedStatementHSelect.setString(1, hPai);

				listaHitorico = new ArrayList<String>();
				rsH = preparedStatementHSelect.executeQuery();
				while (rsH.next())
				{
					if (NeoUtils.safeIsNotNull(rsH.getString("CD_HISTORICO")))
						listaHitorico.add(rsH.getString("CD_HISTORICO"));
				}

				for (String cdHistorico : listaHitorico)
				{
					preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
					preparedStatementUpdate.setString(1, usuario);
					preparedStatementUpdate.setString(2, cdHistorico);
					preparedStatementUpdate.executeUpdate();

				}

				preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
				preparedStatementUpdate.setString(1, usuario);
				preparedStatementUpdate.setString(2, historico);
				preparedStatementUpdate.executeUpdate();

				conn.commit();
				conn.setAutoCommit(true);
				log.debug("OrsegupsAlertEventEngine Em Espera " + listaHitorico.size() + " Historico :  " + historico);

			}
		}
		catch (Exception e)
		{
			try
			{
				conn.rollback();
				log.debug("OrsegupsAlertEventEngine Em Espera rollback " + listaHitorico.size() + " Historico :  " + historico);
			}
			catch (SQLException e1)
			{
				log.error("##### ERRO SALVAR LOG EVENT TRANSAÇÃO EM ESPERA emEsperaEventoSigma: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
				e1.printStackTrace();
			}
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EM ESPERA emEsperaEventoSigma: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(null, preparedStatementHSelect, rsH);
			OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, null);
		}
		log.debug("deslocando evento no Sigma! " + historico);
	}

	@SuppressWarnings("finally")
	public EventoVO getDadosProvidencia(EventoVO vo)
	{

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try
		{

			if (vo != null && vo.getCodigoCliente() != null)
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT ");
				sql.append("	PROV.CD_CLIENTE, ");
				sql.append("	CD_PROVIDENCIA, ");
				sql.append("	PROV.NOME, ");
				sql.append("	PROV.FONE1, ");
				sql.append("	PROV.FONE2, ");
				sql.append("	PROV.EMAIL, ");
				sql.append("	PROV.NU_PRIORIDADE ");
				sql.append(" FROM ");
				sql.append("	dbo.dbPROVIDENCIA AS PROV WITH (NOLOCK) ");
				sql.append("	INNER JOIN dbo.dbCENTRAL as CEN WITH (NOLOCK)  ON CEN.CD_CLIENTE = PROV.CD_CLIENTE ");
				sql.append(" WHERE  ");
				sql.append("	CEN.CD_CLIENTE = " + vo.getCodigoCliente() + " ");
				sql.append(" ORDER BY PROV.NU_PRIORIDADE_NIVEL2 ");

				statement = connection.prepareStatement(sql.toString());
				resultSet = statement.executeQuery();

				ProvidenciaVO providenciaVO = null;
				while (resultSet.next())
				{

					providenciaVO = new ProvidenciaVO();
					String cliente = resultSet.getString("CD_CLIENTE");
					String providencia = resultSet.getString("CD_PROVIDENCIA");
					String nome = resultSet.getString("NOME");
					String telefone1 = resultSet.getString("FONE1");
					String telefone2 = resultSet.getString("FONE2");
					String email = resultSet.getString("EMAIL");
					String prioridade = resultSet.getString("NU_PRIORIDADE");

					if (NeoUtils.safeIsNotNull(cliente))
					{
						providenciaVO.setCodigoCliente(Integer.parseInt(cliente));
					}
					else
					{
						providenciaVO.setCodigoCliente(0);
					}

					if (NeoUtils.safeIsNotNull(providencia))
					{
						providenciaVO.setCodigoProvidencia(Integer.parseInt(providencia));
					}
					else
					{
						providenciaVO.setCodigoProvidencia(0);
					}

					if (NeoUtils.safeIsNotNull(nome))
					{
						providenciaVO.setNome(nome);
					}
					else
					{
						providenciaVO.setCodigoCliente(0);
					}

					if (NeoUtils.safeIsNotNull(telefone1) && !telefone1.isEmpty())
					{
						telefone1 = telefone1.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone1 = telefone1.replace(" ", "");
						//providenciaVO.setTelefone1("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone1 + "\");'>" + telefone1 + "</a>");
						providenciaVO.setTelefone1(telefone1);
					}
					else
					{
						providenciaVO.setTelefone1("");
					}
					if (NeoUtils.safeIsNotNull(telefone2) && !telefone2.isEmpty())
					{
						telefone2 = telefone2.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone2 = telefone2.replace(" ", "");
						//providenciaVO.setTelefone2("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone2 + "\");'>" + telefone2 + "</a>");
						providenciaVO.setTelefone2(telefone2);
					}
					else
					{
						providenciaVO.setTelefone2("");
					}
					if (NeoUtils.safeIsNotNull(email))
					{
						providenciaVO.setEmail(email);
					}
					else
					{
						providenciaVO.setEmail("");
					}
					if (NeoUtils.safeIsNotNull(prioridade))
					{
						providenciaVO.setPrioridade(Integer.parseInt(prioridade));
					}
					else
					{
						providenciaVO.setPrioridade(0);
					}
					vo.addProvidencia(providenciaVO);
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO SIGMA PROVIDENCIAS : " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			return vo;
		}

	}

	//TODO verificar se existem os usuários que estão autorizados para a atender pelo cliente
	@SuppressWarnings("finally")
	public EventoVO getDadosAcessoCliente(EventoVO vo)
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try
		{
			if (vo != null && vo.getCodigoCliente() != null)
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT A.ID_ACESSO,A.NOME,A.NU_REGISTRO_GERAL,A.NU_CPF,A.SENHA,A.OBS,A.NU_TELEFONE  FROM dbACESSO AS A WITH (NOLOCK) WHERE A.CD_CLIENTE = ? ");
				statement = connection.prepareStatement(sql.toString());
				statement.setString(1, vo.getCodigoCliente());
				resultSet = statement.executeQuery();
				EventoAcessoClienteVO eventoAcessoCliente = null;
				while (resultSet.next())
				{
					eventoAcessoCliente = new EventoAcessoClienteVO();
					String idAcesso = resultSet.getString("ID_ACESSO");
					String nome = resultSet.getString("NOME");
					String nuRegistroGeral = resultSet.getString("NU_REGISTRO_GERAL");
					String nuCpf = resultSet.getString("NU_CPF");
					String senha = resultSet.getString("SENHA");
					String obs = resultSet.getString("OBS");
					String telefone = resultSet.getString("NU_TELEFONE");
					if (obs == null || obs.isEmpty())
					{
						obs = "Vazio";
					}
					else
					{
						//obsFechamento = "<li>" + obsFechamento;
						//obs = obs.replace("\n", "<br><li>");
					}

					if (NeoUtils.safeIsNotNull(idAcesso))
					{
						eventoAcessoCliente.setIdAcesso(idAcesso);
					}
					else
					{
						eventoAcessoCliente.setIdAcesso("");
					}

					if (NeoUtils.safeIsNotNull(nome))
					{
						eventoAcessoCliente.setNome(nome);
					}
					else
					{
						eventoAcessoCliente.setNome("");
					}

					if (NeoUtils.safeIsNotNull(nuRegistroGeral))
					{
						eventoAcessoCliente.setNuRegistroGeral(nuRegistroGeral);
					}
					else
					{
						eventoAcessoCliente.setNuRegistroGeral("");
					}

					if (NeoUtils.safeIsNotNull(nuCpf))
					{
						eventoAcessoCliente.setNuCpf(nuCpf);
					}
					else
					{
						eventoAcessoCliente.setNuCpf("");
					}
					if (NeoUtils.safeIsNotNull(senha))
					{
						eventoAcessoCliente.setSenha(senha);
					}
					else
					{
						eventoAcessoCliente.setSenha("");
					}
					if (NeoUtils.safeIsNotNull(obs))
					{
						eventoAcessoCliente.setObs(obs);
					}
					else
					{
						eventoAcessoCliente.setObs("");
					}
					if (NeoUtils.safeIsNotNull(telefone))
					{
						telefone = telefone.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone = telefone.replace(" ", "");
						eventoAcessoCliente.setTelefone(telefone);
					}
					else
					{
						eventoAcessoCliente.setTelefone("");
					}
					vo.addEventoAcessoCliente(eventoAcessoCliente);
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			return vo;
		}
	}

	public List<SigmaVO> listaContas(String parametro) throws JSONException, IOException
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<SigmaVO> listaContasSigma = null;
		try
		{

			if (parametro != null && !parametro.isEmpty())
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();

				sql.append(" SELECT ");
				sql.append(" CEN.cd_cliente,CEN.ID_empresa,CEN.ID_central,CEN.particao,ROT.NM_rota AS Rota,CEN.razao as Razao_social,CEN.cgccpf as Cnpj_cpf, ");
				sql.append(" CEN.endereco as Logradouro,BAI.nome AS Bairro,CID.nome AS Cidade,CEN.id_estado AS Uf,CEN.cep AS Cep ");
				sql.append(" FROM dbcentral AS CEN WITH (NOLOCK) ");
				sql.append(" INNER JOIN rota AS ROT WITH (NOLOCK) ON ROT.cd_rota = CEN.id_rota  ");
				sql.append(" INNER JOIN dbcidade AS CID WITH (NOLOCK) ON CID.id_cidade = CEN.id_cidade  ");
				sql.append(" INNER JOIN dbbairro AS BAI WITH (NOLOCK) ON BAI.id_bairro = CEN.id_bairro  ");
				sql.append(" WHERE  ");
				sql.append(" CEN.ctrl_central = 1 ");
				sql.append(" AND ");
				Long longTerm = SearchObjectVO.getTermoLong(parametro);
				if (longTerm != null)
				{
					sql.append("		CEN.cd_cliente = " + longTerm + " ");
					sql.append("		OR  ");
				}
				sql.append("		CEN.razao LIKE '%" + parametro + "%' ");
				sql.append("		OR  ");
				sql.append("		CEN.cgccpf = '%" + parametro + "%' ");
				sql.append("		OR  ");
				sql.append("		CEN.EMAILRESP like '%" + parametro + "%' ");
				sql.append("		OR  ");
				sql.append("		CEN.ID_central = '" + parametro + "' ");
				sql.append(" ORDER BY CEN.ID_CENTRAL,CEN.PARTICAO,CEN.RAZAO ");

				statement = connection.prepareStatement(sql.toString());

				resultSet = statement.executeQuery();
				listaContasSigma = new ArrayList<SigmaVO>();
				SigmaVO vo  = null;
				while (resultSet.next())
				{

					vo = new SigmaVO();
					//dados gerais central
					vo.setCodigoCliente(resultSet.getInt("cd_cliente"));
					vo.setCodigoEmpresa(resultSet.getInt("ID_empresa"));
					vo.setCodigoCentral(resultSet.getString("ID_central"));
					vo.setParticao(resultSet.getString("particao"));
					vo.setRota(resultSet.getString("Rota"));
					vo.setRazaoSocial(resultSet.getString("Razao_social"));
					vo.setCnpjCpf(resultSet.getString("Cnpj_cpf"));
					vo.setLogradouro(resultSet.getString("Logradouro"));
					vo.setBairro(resultSet.getString("Bairro"));
					vo.setCidade(resultSet.getString("Cidade"));
					vo.setUf(resultSet.getString("Uf"));
					vo.setCep(resultSet.getString("Cep"));
					listaContasSigma.add(vo);
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO SIGMA PROVIDENCIAS : " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			return listaContasSigma;
		}
	}
	
	public static void getAbrirOSSigma(String idCliente, String particao, String idCentral, String cdEmpresa, String solicitante, String defeito, String respTecnico, String descricao)
	{
		Map<String, String> map = new HashMap<String, String>();

		map.put("clientId", idCliente);//
		map.put("partition", particao);//
		map.put("centralId", idCentral);//
		map.put("companyId", cdEmpresa);
		map.put("scheduledTime", "");
		map.put("estimatedTime", "");
		map.put("description", (descricao.length() >= 500 ? descricao.substring(0, 499) : descricao));
		map.put("requester", solicitante);
		map.put("defect", defeito);
		map.put("responsibleTechnician", respTecnico);

		String param2 = montaParametroOSString(map);
		try
		{
			String urlParameters = param2;
			String request = WEB_SERVICE_SIGMA + "ReceptorOSWebService";
			URL url = new URL(request);
			URLConnection conn = url.openConnection();

			conn.setDoOutput(true);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(urlParameters);
			writer.flush();

			String line;
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			while ((line = reader.readLine()) != null)
			{
				JSONObject json = new JSONObject(line);
				if (json.getString("status").equals("success"))
				{
					log.warn("Sucesso ao abrir OS tratamento de desvio de habito.");
				}
				else
				{
					log.warn("Exceção ao abrir OS tratamento de desvio de habito.");
				}
			}
			writer.close();
			reader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	public static String montaParametroOSString(Map<String, String> map)
	{
		String parametro = "";

		/*
		 * clientId=?*
		 * &centralId=?*
		 * &partition=?*
		 * &companyId=?*
		 * &responsibleTechnician=?*
		 * &defect=?*
		 * &requester=?*
		 * &description=?
		 * &estimatedTime=?
		 * &scheduledTime=?
		 */
		if (map.get("clientId") != null && !map.get("clientId").equals(""))
		{
			parametro += "clientId" + "=" + map.get("clientId");
		}

		if (map.get("centralId") != null && !map.get("centralId").equals(""))
		{
			parametro += "&" + "centralId" + "=" + map.get("centralId");
		}

		if (map.get("partition") != null && !map.get("partition").equals(""))
		{
			parametro += "&" + "partition" + "=" + map.get("partition");
		}

		if (map.get("companyId") != null && !map.get("companyId").equals(""))
		{
			parametro += "&" + "companyId" + "=" + map.get("companyId");
		}

		if (map.get("responsibleTechnician") != null && !map.get("responsibleTechnician").equals(""))
		{
			parametro += "&" + "responsibleTechnician" + "=" + map.get("responsibleTechnician");
		}

		if (map.get("defect") != null && !map.get("defect").equals(""))
		{
			parametro += "&" + "defect" + "=" + map.get("defect");
		}

		if (map.get("requester") != null && !map.get("requester").equals(""))
		{
			parametro += "&" + "requester" + "=" + map.get("requester");
		}

		if (map.get("description") != null && !map.get("description").equals(""))
		{
			parametro += "&" + "description" + "=" + map.get("description");
		}

		if (map.get("estimatedTime") != null && !map.get("estimatedTime").equals(""))
		{
			parametro += "&" + "estimatedTime" + "=" + map.get("estimatedTime");
		}

		if (map.get("scheduledTime") != null && !map.get("scheduledTime").equals(""))
		{
			parametro += "&" + "scheduledTime" + "=" + map.get("scheduledTime");
		}

		parametro = URLEncoder.encode(parametro).replace("%3D", "=").replace("%26", "&");
		return parametro;
	}
}
