package com.neomind.fusion.custom.orsegups.presenca.vo;

public class SituacoesApuracaoVO
{
	private ApuracaoVO apuracao;
	private Long codigoSituacao;
	private String descricaoSituacao;
	private Long qdeHoras;
	private String observacao;
	public ApuracaoVO getApuracao()
	{
		return apuracao;
	}
	public void setApuracao(ApuracaoVO apuracao)
	{
		this.apuracao = apuracao;
	}
	public Long getCodigoSituacao()
	{
		return codigoSituacao;
	}
	public void setCodigoSituacao(Long codigoSituacao)
	{
		this.codigoSituacao = codigoSituacao;
	}
	public String getDescricaoSituacao()
	{
		return descricaoSituacao;
	}
	public void setDescricaoSituacao(String descricaoSituacao)
	{
		this.descricaoSituacao = descricaoSituacao;
	}
	public Long getQdeHoras()
	{
		return qdeHoras;
	}
	public void setQdeHoras(Long qdeHoras)
	{
		this.qdeHoras = qdeHoras;
	}
	public String getObservacao()
	{
		return observacao;
	}
	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}
}
