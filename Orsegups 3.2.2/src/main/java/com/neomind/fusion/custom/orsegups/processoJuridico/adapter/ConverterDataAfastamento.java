package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterDataAfastamento
public class ConverterDataAfastamento extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		NeoObject objPrincipal = field.getForm().getObject();
		EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);
		GregorianCalendar datAfa = (GregorianCalendar) wPrincipal.getValue("dtAfa");
		if (NeoDateUtils.safeDateFormat(datAfa, "dd/MM/yyyy") != null)
		{
			return NeoDateUtils.safeDateFormat(datAfa, "dd/MM/yyyy");
		}
		else
		{
			return "";
		}

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
