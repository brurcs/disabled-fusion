package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

class OSAgrupar {

    int cdCliente;
    String central;
    String particao;
    int idEmpresa;
    int idOrdem;
    int idOsDefeito;
    String descricaoDefeito;
    String defeito;
    Date dataAgendada;

    public String toString() {
	return "CD Cliente: " + cdCliente + "\n Central: " + central + "\n Partição: " + particao + "\n ID Empresa: " + idEmpresa + "\n ID Ordem: " + idOrdem + "\n ID Defeito: " + idOsDefeito + "\n Descrição Defeito: " + descricaoDefeito + "\n Defeito: " + defeito + "\n Data Agendada: " + dataAgendada;
    }

}

public class RotinaAgrupamentoOS666 implements CustomJobAdapter {

    // private static final Log log =
    // LogFactory.getLog(RotinaAgrupamentoOS.class);

    private static final int[] DEFEITOS_AGRUPADORES = { 166, 134, 184, 178, 164, 733};
    
    
    
    private OSAgrupar maiorDescricao = null;

    public void execute(CustomJobContext arg0) {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT ");
	sql.append(" CASE ");
	sql.append(" WHEN C.ID_EMPRESA IN(10075,10144,10127) THEN CAST(C.CD_CLIENTE AS VARCHAR)+CAST(O.ID_INSTALADOR AS VARCHAR) ");
	sql.append(" ELSE C.ID_CENTRAL+CAST(C.ID_EMPRESA AS VARCHAR)+CAST(O.ID_INSTALADOR AS VARCHAR)  ");
	sql.append(" END AS CHAVE, C.CD_CLIENTE, C.ID_CENTRAL, C.PARTICAO, C.ID_EMPRESA, O.ID_ORDEM, O.IDOSDEFEITO, D.DESCRICAODEFEITO, COALESCE(O.DEFEITO,'') AS DEFEITO, O.DATAAGENDADA, O.ABERTURA FROM ( ");
	sql.append(" SELECT ID_CENTRAL, ID_EMPRESA FROM dbORDEM O WITH(NOLOCK) ");
	sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = O.CD_CLIENTE ");
	sql.append(" WHERE O.FECHADO=0 ");
	sql.append(" GROUP BY ID_CENTRAL, ID_EMPRESA ");
	sql.append(" HAVING COUNT(*)>1 ) AS CONTAS ");
	sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.ID_CENTRAL = CONTAS.ID_CENTRAL AND C.ID_EMPRESA = CONTAS.ID_EMPRESA ");
	sql.append(" INNER JOIN dbORDEM O WITH(NOLOCK) ON O.CD_CLIENTE = C.CD_CLIENTE ");
	sql.append(" INNER JOIN OSDEFEITO D WITH(NOLOCK) ON D.IDOSDEFEITO = O.IDOSDEFEITO ");
	sql.append(" WHERE O.FECHADO=0 ");
	sql.append(" AND C.ID_CENTRAL NOT LIKE 'DDDD' ");
	sql.append(" AND substring(C.ID_CENTRAL,1,1) <> 'R' ");
	sql.append(" AND (C.CD_GRUPO_CLIENTE != 117  OR C.CD_GRUPO_CLIENTE IS NULL) ");
	sql.append(" AND (O.IDOSDEFEITO NOT IN (166,167,178,731,1017,1128) OR O.IDOSDEFEITO IS NULL) ");
	sql.append(" AND (C.ID_EMPRESA NOT IN (10119,10120) OR C.ID_EMPRESA IS NULL) ");
	sql.append(" ORDER BY C.ID_CENTRAL, C.ID_EMPRESA, C.PARTICAO "); 

	Map<String, List<OSAgrupar>> mapOS = new HashMap<String, List<OSAgrupar>>();

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {

		OSAgrupar o = new OSAgrupar();

		String chave = rs.getString("CHAVE");

		o.cdCliente = rs.getInt("CD_CLIENTE");
		o.central = rs.getString("ID_CENTRAL");
		o.particao = rs.getString("PARTICAO");
		o.idEmpresa = rs.getInt("ID_EMPRESA");
		o.idOrdem = rs.getInt("ID_ORDEM");
		o.idOsDefeito = rs.getInt("IDOSDEFEITO");
		o.descricaoDefeito = rs.getString("DESCRICAODEFEITO");
		o.defeito = rs.getString("DEFEITO");

		Timestamp tDate = rs.getTimestamp("DATAAGENDADA");

		if (tDate != null) {
		    o.dataAgendada = new Date(tDate.getTime());
		}

		List<OSAgrupar> lista = mapOS.get(chave);

		if (lista == null) {
		    lista = new ArrayList<OSAgrupar>();
		    lista.add(o);
		    mapOS.put(chave, lista);
		} else {
		    lista.add(o);
		}

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	

	if (!mapOS.isEmpty()) {
	    // VARRENDO CADA CHAVE DE AGRUPAMENTO (CADA CLIENTE)
	    	    
	    for (String key : mapOS.keySet()) {
		List<OSAgrupar> listaOS = mapOS.get(key);

		List<OSAgrupar> agrupadorasAlarme = new ArrayList<OSAgrupar>();
		List<OSAgrupar> agrupadorasCFTV = new ArrayList<OSAgrupar>();
		List<OSAgrupar> agrupaveisAlarme = new ArrayList<OSAgrupar>();
		List<OSAgrupar> agrupaveisCFTV = new ArrayList<OSAgrupar>();

		for (OSAgrupar os : listaOS) {

		    if (this.isParticaoAlarme(os.particao)) {
			if (this.isAgrupador(os.idOsDefeito)) {
			    agrupadorasAlarme.add(os);
			} else {
			    agrupaveisAlarme.add(os);
			}
		    } else if (this.isParticaoCFTV(os.particao)) {
			if (this.isAgrupador(os.idOsDefeito)) {
			    agrupadorasCFTV.add(os);
			} else {
			    agrupaveisCFTV.add(os);
			}
		    } else {
			continue;
		    }
		}
		
		this.executarAgrupamento(agrupadorasAlarme, agrupaveisAlarme);
		
		this.executarAgrupamento(agrupadorasCFTV, agrupaveisCFTV);

		
	    }
	    
	    System.out.println(maiorDescricao.defeito);
	    
	}

    }
    
    private void executarAgrupamento (List<OSAgrupar> agrupadoras, List<OSAgrupar> agrupaveis){
	
	String textoPadrao = "ATENÇÃO:_ Verificar se o cliente possui placas e se todas estão em perfeitas condições, enviar FOTO no anexo. Conferir se a(s) central(is) deste cliente estão com a programação correta de envio de RESTAURO no ato do fechamento da zona. Dúvidas entrar em contato com a Assessoria Técnica.";
	
	if (!agrupadoras.isEmpty()) {
	    OSAgrupar oldestAgrupadora = null;

	    for (OSAgrupar s : agrupadoras) {

		if (oldestAgrupadora != null) {
		    if (s.idOrdem < oldestAgrupadora.idOrdem) {
			oldestAgrupadora = s;
		    }
		} else {
		    oldestAgrupadora = s;
		}

	    }

	    for (OSAgrupar s :agrupaveis) {

		oldestAgrupadora.defeito += "\n OS Agrupada: " + s.idOrdem + " Defeito: " + s.descricaoDefeito + "\n " + s.defeito;

		fecharOS(s.idOrdem, oldestAgrupadora.idOrdem);

	    }

	    if (oldestAgrupadora.defeito.contains(textoPadrao)) {
		String defeitoAjustado = oldestAgrupadora.defeito.replace(textoPadrao, "");
		oldestAgrupadora.defeito = defeitoAjustado + "\n" + textoPadrao;
	    }

	    atualizarDefeito(oldestAgrupadora);
	    
	    if (maiorDescricao != null){
		if (oldestAgrupadora.defeito.length() > maiorDescricao.defeito.length()){
		    maiorDescricao = oldestAgrupadora;
		}
	    }else{
		maiorDescricao = oldestAgrupadora;
	    }
	    

	} else if (!agrupaveis.isEmpty()) {
	    // DESCOBRIR QUAL AGRUPAVEL MAIS VELHA
	    OSAgrupar osPai = null;

	    for (OSAgrupar os : agrupaveis) {

		// VALIDACAO DE DEFEITOS QUE POSSUEM FECHAMENTO AUTOMATICO
		if (os.idOsDefeito != 122 && os.idOsDefeito != 123 && os.idOsDefeito != 125 && os.idOsDefeito != 732) {
		    if (osPai != null) {

			if (osPai.dataAgendada != null && os.dataAgendada != null) {
			    // /TESTAR QUAL O AGENDAMENTO MAIS PROXIMO

			    if (os.dataAgendada.before(osPai.dataAgendada)) {
				// AGENDAMENTO ATUAL ANTERIOR AO DA PAI
				osPai = os;

			    }

			} else if (os.dataAgendada != null) {
			    // ATUAL TEM AGENDAMENTO E A PAI NAO
			    osPai = os;
			} else if (osPai.dataAgendada == null) {
			    // AS DUAS NAO POSSUEM AGENDAMENTO, TESTAR A MAIS VELHA (SE A PAI POSSUIR CONTINUARÁ SENDO A PAI)
			    if (os.idOrdem < osPai.idOrdem) {
				osPai = os;
			    }
			}
			// PAI POSSUI AGENDAMENTO FICA COMO ESTA

			// PAI É HABILITACAO ENTAO FICA COMO ESTA
		    } else {
			// NAO EXISTE PAI AINDA - OS MAIS VELHA
			osPai = os;
			continue;
		    }
		} else {
		    if (osPai != null) {
			if (osPai.idOsDefeito == 122 || osPai.idOsDefeito == 123 || osPai.idOsDefeito == 125 || osPai.idOsDefeito == 732) {
			    // AS DUAS FECHAM POR RESTAURO, VER QUEM POSSUI AGENDAMENTO OU É A MAIS VELHA
			    if (osPai.dataAgendada != null && os.dataAgendada != null) {
				// /TESTAR QUAL O AGENDAMENTO MAIS PROXIMO

				if (os.dataAgendada.before(osPai.dataAgendada)) {
				    // AGENDAMENTO ATUAL ANTERIOR AO DA PAI
				    osPai = os;
				}

			    } else if (os.dataAgendada != null) {
				// ATUAL TEM AGENDAMENTO E A PAI NAO
				osPai = os;
			    } else if (osPai.dataAgendada == null) {
				// AS DUAS NAO POSSUEM AGENDAMENTO, TESTAR A MAIS VELHA (SE A PAI POSSUIR CONTINUARÁ SENDO A PAI)
				if (os.idOrdem < osPai.idOrdem) {
				    osPai = os;
				}
			    }
			    // PAI POSSUI AGENDAMENTO FICA COMO ESTA
			}
			// SE NAO PAI NAO FECHA POR RESTAURO E DEVE CONTINUAR SENDO PAI
		    } else {
			osPai = os;
			continue;
		    }
		}
	    }
	    

	    // EXECUTA AGRUPAMENTO - AQUI O PAI DEVE SER DIFERENTE DE NULO,TESTAR APENAS PARA EVITAR NULL POINTER

	    if (osPai != null) {
		agrupaveis.remove(osPai);

		// LISTA NAO CONTEM MAIS O PAI, VARRER LISTA ADICIONANDO O CONTEUDO E FECHANDO

		for (OSAgrupar s : agrupaveis) {

		    osPai.defeito += "\n OS Agrupada: " + s.idOrdem + " Defeito: " + s.descricaoDefeito + "\n " + s.defeito;
		    
		    fecharOS(s.idOrdem, osPai.idOrdem);

		}

		if (osPai.defeito.contains(textoPadrao)) {
		    String defeitoAjustado = osPai.defeito.replace(textoPadrao, "");
		    osPai.defeito = defeitoAjustado + "\n" + textoPadrao;
		}

		atualizarDefeito(osPai);
		
		if (maiorDescricao != null) {
		    if (osPai.defeito.length() > maiorDescricao.defeito.length()) {
			maiorDescricao = osPai;
		    }
		} else {
		    maiorDescricao = osPai;
		}

	    }

	}
	
    }

    private boolean isParticaoAlarme(String particao) {

	try {
	    int p = Integer.parseInt(particao);

	    if (p > -1 && p < 21) {
		return true;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}

	return false;
    }

    private boolean isParticaoCFTV(String particao) {

	try {
	    int p = Integer.parseInt(particao);

	    if (p > 89 && p < 100) {
		return true;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}

	return false;
    }

    private boolean isAgrupador(int idOsDefeito) {

	for (int d : DEFEITOS_AGRUPADORES) {
	    if (d == idOsDefeito) {
		return true;
	    }
	}

	return false;
    }

    private static void fecharOS(int idOrdem, int idOrdemAgrupadora) {
	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();

	String executado = "Fechamento automático. Agrupada na OS: "+idOrdemAgrupadora;
	
	sql.append("UPDATE dbORDEM SET FECHADO = 1, OPFECHOU = 11010, FECHAMENTO = GETDATE(), IDOSCAUSADEFEITO = 642, IDOSSOLUCAO = 631, EXECUTADO = '"+executado+"' WHERE ID_ORDEM = ? ");

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, idOrdem);

	    pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
    }

    private static void atualizarDefeito(OSAgrupar os) {
	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();

	sql.append("UPDATE dbORDEM SET DEFEITO = ? WHERE ID_ORDEM = ? ");

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());
	    System.out.println("****** PRINT DEFEITO PARA ANALISE [RotinaAgrupamentoOS666] *****");
	    System.out.println(os.defeito);
	    pstm.setString(1, os.defeito);
	    pstm.setInt(2, os.idOrdem);

	    pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
    }

}
