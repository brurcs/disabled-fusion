package com.neomind.fusion.custom.orsegups.fap.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HistoricoFapDTO {
    String codigoTarefa = null;
    Long neoIdTarefa = null;
    BigDecimal valorTotal = null;
    Date dataSolicitacao = null;
    String relato = null;
    String laudo = null;
    String status = null;
    List<ItemOrcamentoDTO> itemOrcamento = new ArrayList<>();
    
    public String getCodigoTarefa() {
        return codigoTarefa;
    }
    public void setCodigoTarefa(String codigoTarefa) {
        this.codigoTarefa = codigoTarefa;
    }
    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }
    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }
    public String getRelato() {
        return relato;
    }
    public void setRelato(String relato) {
        this.relato = relato;
    }
    public String getLaudo() {
        return laudo;
    }
    public void setLaudo(String laudo) {
        this.laudo = laudo;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<ItemOrcamentoDTO> getItemOrcamento() {
        return itemOrcamento;
    }
    public void setItemOrcamento(List<ItemOrcamentoDTO> itemOrcamento) {
        this.itemOrcamento = itemOrcamento;
    }
    public Long getNeoIdTarefa() {
        return neoIdTarefa;
    }
    public void setNeoIdTarefa(Long neoIdTarefa) {
        this.neoIdTarefa = neoIdTarefa;
    }
    public BigDecimal getValorTotal() {
        return valorTotal;
    }
    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }
    
    
    
    
}
