package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteGraficoVO
{
	private String grafico;
	private String options;
	private String valor;
	
	public String getGrafico()
	{
		return grafico;
	}
	public void setGrafico(String grafico)
	{
		this.grafico = grafico;
	}
	public String getValor()
	{
		return valor;
	}
	public void setValor(String valor)
	{
		this.valor = valor;
	}
	public String getOptions()
	{
		return options;
	}
	public void setOptions(String options)
	{
		this.options = options;
	}
}
