package com.neomind.fusion.custom.orsegups.fap.slip.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.entity.EntityAdapter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class ValidarDadosContratoAdapter implements EntityAdapter
{
	@Override
	public void run(Map<String, Object> map)
	{
		/*
		 * EForm eform = (EForm) params.get(EFORM);
		 * EntityWrapper wrapper = new EntityWrapper(eform.getObject());
		 */
		
		Map<String, Object> dados = (Map<String, Object>) map.get("reqMap");
		Long formNeoId = dados.get("id_") != null ? new Long((String) dados.get("id_")) : 0l;
		
		NeoObject noFAPSlip = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlip"), formNeoId);
		PersistEngine.persist(noFAPSlip);
		
		EntityWrapper wFAPSlip = new EntityWrapper(noFAPSlip);
		
		NeoObject noContrato = wFAPSlip.findGenericValue("novoContrato");
		EntityWrapper wContrato = new EntityWrapper(noContrato);
		
		importarRateiosExcel(wContrato);
	}
	
	private boolean validarOrcamento(List<NeoObject> listOrcamento, List<NeoObject> listNovoOrcamento)
	{
		for (NeoObject noNovoOrcamento : listNovoOrcamento)
		{
			GregorianCalendar iniVigNovoOrcamento = new EntityWrapper(noNovoOrcamento).findGenericValue("inicioVigencia");
			for (NeoObject noOrcamento : listOrcamento)
			{
				GregorianCalendar iniVigOrcamento = new EntityWrapper(noOrcamento).findGenericValue("inicioVigencia");
				
				if (iniVigNovoOrcamento.before(iniVigOrcamento))
					return false;
			}
		}
		
		return true;
	}
	
	private void importarRateiosExcel(EntityWrapper wrapper)
	{
		NeoFile anexo = wrapper.findGenericValue("centroCusto.anexoCentroCusto");
		if (anexo == null)
			return;
		
		String mensagem = "";
		/* Obrigatoriamente o arquivo excel para ser importado tem que ser salvo no formato .XLS */
		Workbook wbRateios = null;
		
		try
		{
			Long codEmpresa = wrapper.findGenericValue("empresa.codemp");

			List<NeoObject> listCentroCusto = new ArrayList<NeoObject>();
			List<String> listCentroCustoInvalidos = new ArrayList<String>();			

			wbRateios = Workbook.getWorkbook(anexo.getAsFile());
			Sheet rateios = wbRateios.getSheet(0);

			int liTroca = rateios.getRows();
			for (int i = 0; i < liTroca; i++)
			{
				if (i != 0)
				{
					Cell centroCusto = rateios.getCell(0, i);
					Cell valorRateio = rateios.getCell(1, i);
					Cell obsRateio = rateios.getCell(2, i);
					
					if (centroCusto.getContents().isEmpty())
						continue;

					QLGroupFilter filterCcu = new QLGroupFilter("AND");
					filterCcu.addFilter(new QLEqualsFilter("codccu", centroCusto != null ? centroCusto.getContents().trim() : ""));
					filterCcu.addFilter(new QLEqualsFilter("codemp", codEmpresa));
					filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));
					filterCcu.addFilter(new QLEqualsFilter("acerat", "S"));

					NeoObject externoCentroCusto = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);

					if (externoCentroCusto != null)
					{
						String valorRateioStr = valorRateio.getContents();
						BigDecimal valorRateio1 = new BigDecimal(valorRateioStr.replace(",", "."));
						
						NeoObject noRateio = AdapterUtils.createNewEntityInstance("FAPSlipRateio");
						EntityWrapper wRateio = new EntityWrapper(noRateio);

						wRateio.setValue("codigoCentroCusto", centroCusto.getContents());
						wRateio.setValue("centroCusto", externoCentroCusto);
						wRateio.setValue("valor", valorRateio1); 
						if (obsRateio != null)
							wRateio.setValue("observacoes", obsRateio.getContents()); 

						/* valorTotalRateio = valorTotalRateio.add(valorRateio); */
						listCentroCusto.add(noRateio);
					}
					else
					{
						listCentroCustoInvalidos.add(centroCusto.getContents());
					}
				}
			}
			if (listCentroCustoInvalidos.isEmpty())
			{
				NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("FAPSlipCentroCusto");
				EntityWrapper wCentroCusto = new EntityWrapper(noCentroCusto);
				
				NeoObject noMetrica = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipTipoRateio"), new QLEqualsFilter("codigo", 2l));
				
				wCentroCusto.setValue("tipoRateio", noMetrica);
				wCentroCusto.setValue("anexoCentroCusto", null);
				for (NeoObject noCCU : listCentroCusto)
				{
					wCentroCusto.findField("rateio").addValue(noCCU);
				}
				
				wrapper.setValue("centroCusto", noCentroCusto);
			}
			else
			{
				mensagem = "Centro de Custo(s) não localizado(s) ou não aceitam rateio! Por favor, verifique se o(s) Centro de Custo " + listCentroCustoInvalidos + " existem na Empresa selecionada ou não aceitam rateio!";
				mensagem = mensagem.replaceAll("\\[|\\]|", "");
				throw new WorkflowException(mensagem);
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro na importação de centros de custo. Por favor, contate o Departamento de TI!");
		}
		finally {
			if (wbRateios != null)
				wbRateios.close();
		}
	}
	
	public void validarContrato(NeoObject noContrato)
	{
		try
		{			
			EntityWrapper wForm = new EntityWrapper(noContrato);
			
			Long codCriterioAprovacao = wForm.findGenericValue("criterioAprovacao.codigo");
			if (codCriterioAprovacao == null)
			{
				throw new WorkflowException("É necessário escolher um critério de aprovação.");
			}
			
			if (codCriterioAprovacao == 2l)
			{
				Long periodoMedia = wForm.findGenericValue("periodoMedia");
				if (periodoMedia > 12)
					throw new WorkflowException("O valor máximo para cálculo da média é de 12 meses");
			}
			
			//importarRateiosExcel(wForm);
			
			Boolean validarOrcamento = wForm.findGenericValue("validarOrcamento");
			List<NeoObject> listOrcamento = wForm.findGenericValue("orcamento");
			List<NeoObject> listNovoOrcamento = wForm.findGenericValue("novoOrcamento");
			
			if ((validarOrcamento != null && validarOrcamento) && ((listOrcamento == null || listOrcamento.isEmpty()) && (listNovoOrcamento == null || listNovoOrcamento.isEmpty())))
			{
				throw new WorkflowException("É necessário inserir o orçamento.");
			}
				
			if (!validarOrcamento(listOrcamento, listNovoOrcamento))
				throw new WorkflowException("A data de vigência do novo orçamento deve ser maior que as vigências anteriores.");
			
			List<NeoObject> listAnexos = wForm.findGenericValue("anexos");
			if (listAnexos.isEmpty())
				throw new WorkflowException("É necessário inserir um anexo.");
			
			BigDecimal totalRateio = wForm.findGenericValue("centroCusto.total");
			
			Long codTipoRateio = wForm.findGenericValue("centroCusto.tipoRateio.codigo");
			if (codTipoRateio == null)
			{
				throw new WorkflowException("Selecione o tipo do rateio.");
			}
			
			if (codTipoRateio == 1l)
			{
				if (totalRateio.compareTo(new BigDecimal(100)) != 0)
					throw new WorkflowException("A porcentagem total de rateios deve chegar à 100.");
			} else if (codTipoRateio == 2l)
			{
				//if (codCriterioAprovacao != 3l)
					//throw new WorkflowException("O tipo de rateio selecionado só é permitido se o valor for fixo.");
				
				BigDecimal valorContrato = wForm.findGenericValue("valorBase");
				if (totalRateio != null && valorContrato != null && totalRateio.compareTo(valorContrato) != 0 && codCriterioAprovacao == 3l)
					throw new WorkflowException("O valor total do rateio deve ser o mesmo que o valor do contrato");
			}
			
			List<NeoObject> listCCU = wForm.findGenericValue("centroCusto.rateio");
			
			for (NeoObject noCCU : listCCU)
			{
				EntityWrapper wCCU = new EntityWrapper(noCCU);
							
				String codCCU = wCCU.findGenericValue("codigoCentroCusto");				
				if (codCCU == null || codCCU.length() == 0)
					continue;
				
				Long codEmpresa = wForm.findGenericValue("empresa.codemp");
				
				QLGroupFilter filterCcu = new QLGroupFilter("AND");
				filterCcu.addFilter(new QLEqualsFilter("codccu", codCCU));
				filterCcu.addFilter(new QLEqualsFilter("codemp", codEmpresa));
				filterCcu.addFilter(new QLEqualsFilter("nivccu", 8L));
				
				NeoObject externoCentroCusto = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);
				
				if (externoCentroCusto == null)
					throw new WorkflowException("O centro de custo " + codCCU + " para a empresa " + codEmpresa + " não foi encontrado.");
				
				wCCU.setValue("centroCusto", externoCentroCusto);
			}
			
			String fornecedorFantasia = wForm.findGenericValue("fornecedor.apefor");
			Long fornecedorCNPJ = wForm.findGenericValue("fornecedor.cgccpf");
			Long codFornecedor = wForm.findGenericValue("fornecedor.codfor");
			
			wForm.setValue("fornecedorStr", fornecedorFantasia);
			wForm.setValue("fornecedorCNPJ", fornecedorCNPJ != null ? fornecedorCNPJ.toString() : "Não informado");
			wForm.setValue("codFornecedor", codFornecedor != null ? codFornecedor.toString() : "");			
		}
		catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException(e.getErrorList().get(0).getI18nMessage());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro na validação dos dados do contrato. Contate a TI.");
		}
	}
}
