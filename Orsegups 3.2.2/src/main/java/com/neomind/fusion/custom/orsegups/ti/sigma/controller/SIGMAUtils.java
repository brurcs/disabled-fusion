package com.neomind.fusion.custom.orsegups.ti.sigma.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.TreeMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.seventh.SeventhUtils;
import com.neomind.fusion.custom.orsegups.seventh.bean.EventoArmeDesarme;
import com.neomind.fusion.custom.orsegups.ti.sigma.beans.BufferCount;
import com.neomind.fusion.custom.orsegups.ti.sigma.beans.SIGMAEmpresa;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;

@Path(value = "SIGMAUtils")
public class SIGMAUtils {

    private SIGMAUtilsEngine engine = new SIGMAUtilsEngine();
    
    private SeventhUtils seventh = new SeventhUtils();

    @GET
    @Path("bufferJson")
    @Produces("application/json")
    public BufferCount bufferJson() {
	
	BufferCount buffer = engine.getBufferJson();
	
	return buffer;
    }
    
    @GET
    @Path("bufferTxt")
    @Produces("text/plain")
    public String bufferTxt() {
	
	String buffer = engine.getBufferTxt();
	
	return buffer;
    }
    
    @GET
    @Path("getEmpresas")
    @Produces("application/json")
    public List<SIGMAEmpresa> getEmpresas() {
	
	List<SIGMAEmpresa> retorno = engine.getEmpresas(); 
	
	return retorno;
    }
    
    @GET
    @Path("getNomeViaturaSigma/{codigoViatura}")
    @Produces("application/json")
    public String getNomeViaturaSigma(@PathParam("codigoViatura") String codigoViatura) {
	
	return engine.getNomeViaturaSigma(codigoViatura); 
	
    }
    
    @POST
    @Path("getCameras/{central}/{empresa}")
    @Produces("application/json")
    public TreeMap<String, String> getCameras(@PathParam("central") String central, @PathParam("empresa") String empresa) {
	
	TreeMap<String, String> retorno= null;
	
	String ip = "192.168.20.34";
	
	try {
	    EventoArmeDesarme evento = new EventoArmeDesarme();
	    
	    evento.setServidorCFTV(ip);
	    evento.setUsuarioCFTV("admin");
	    evento.setSenhaCFTV("seventh");
	    evento.setIdCentral(central);
	    evento.setIdEmpresa(empresa);
	    	    
	    retorno = seventh.getCameraCliente(evento);
	} catch (IOException e) {
	    e.printStackTrace();
	} 
	
	return retorno;
    }
    
    @POST
    @Path("getEventosAbertos/{idCentral}/{eventos}")
    @Produces("application/json")
    public List<EventoVO> getEventosAbertos(@PathParam("idCentral") int idCentral, @PathParam("eventos") String eventos) {
	
	List<EventoVO> retorno = engine.getEventosAbertos(eventos, idCentral); 
	
	return retorno;
    }
    
    @GET
    @Path("normalizarCoordenadasCentrais")
    @Produces("application/json")
    public Response teste() {
	
	long inicio = Calendar.getInstance().getTimeInMillis();
	
	engine.normalizarCoordenadasCentrais();
	
	long fim = Calendar.getInstance().getTimeInMillis();
	
	long total = (fim - inicio)/1000;

	return Response.ok("{\"status\":\"OK\", \"tempoExecucao\":"+total+" }", MediaType.APPLICATION_JSON).build();
	
    }
    
    
}
