package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityFinishEventListener;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.event.TaskTransferEvent;
import com.neomind.fusion.workflow.event.TaskTransferEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * Classe responsável pela alteração de Status e Tramitação no avanço de atividades do processo "Processo Emissão Parecer Comissão".
 */
public class AdapterEventChp1TarefaSimples implements ActivityStartEventListener, TaskAssignerEventListener, TaskTransferEventListener, ActivityFinishEventListener, TaskFinishEventListener
{

    @SuppressWarnings("unused")
private static final Log log = LogFactory.getLog(AdapterEventChp1TarefaSimples.class);
    
    @Override
    public void onFinish(TaskFinishEvent event) throws TaskException {
	// TODO Auto-generated method stub
	
	String codigo = null;
	
	String processo = null;

	try {

	    EntityWrapper wEventoAtividade = event.getWrapper();

	    String tarefa = event.getActivity().getActivityName();

	    codigo = event.getProcess().getCode();

	    processo = event.getProcess().getModel().getName();

	    Boolean enviaTarefaSimples = wEventoAtividade.findGenericValue("enviaTarefaSimples");

	    Boolean validaVisita = wEventoAtividade.findGenericValue("visitaValidada");

	    if(enviaTarefaSimples != null && validaVisita != null && enviaTarefaSimples == Boolean.TRUE && validaVisita == Boolean.FALSE){

		//Setado o usuário do maurelio.pinto fixamente para a função do SOLICITANTE da tarefa simples
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", 15382777));

		NeoUser executor = wEventoAtividade.findGenericValue("executorTarefaSimples");

		Integer OS = wEventoAtividade.findGenericValue("idOrdem");

		//abre tarefa simples
		NeoObject oTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper wTarefaSimples = new EntityWrapper(oTarefaSimples);

		wTarefaSimples.findField("Solicitante").setValue(solicitante);

		wTarefaSimples.findField("Executor").setValue(executor);

		wTarefaSimples.findField("Titulo").setValue("Solicitação da Imagem do Chip");

		String descricao = wEventoAtividade.findGenericValue("justificativaCM");

		wTarefaSimples.findField("DescricaoSolicitacao").setValue("Tarefa aberta relativa a OS: " + OS + "<br><br>"
			+ descricao + "<br><br>"
			+ "Demanda relativa a tarefa: " + tarefa + " de código: " + codigo + " do Processo: " + processo);

		GregorianCalendar prazoTarefaSimples = new GregorianCalendar();

		prazoTarefaSimples.add(Calendar.HOUR, 48);

		wTarefaSimples.findField("Prazo").setValue(prazoTarefaSimples);

		ProcessModel pmS = PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));

		OrsegupsWorkflowHelper.iniciaProcessoNeoIdCode(pmS, oTarefaSimples, true, solicitante, true);

	    }

	}
	catch (Exception e)
	{
	    log.warn("ERRO NO FORMULÁRIO");
	    System.out.println("ERRO NO DISPARO DA TAREFA SIMPLES DO PROCESSO: " + processo + " DE CÓDIGO: " + codigo);
	    e.printStackTrace();
	    throw (WorkflowException) e;
	}

    }

    @Override
    public void onFinish(ActivityEvent event) throws ActivityException {
	// TODO Auto-generated method stub

    }

    @Override
    public void onTransfer(TaskTransferEvent event) {
	// TODO Auto-generated method stub

    }

    @Override
    public void onAssign(TaskAssignerEvent event) throws TaskException {
	// TODO Auto-generated method stub

    }

    @Override
    public void onStart(ActivityEvent event) throws ActivityException {
	// TODO Auto-generated method stub

    }


}
