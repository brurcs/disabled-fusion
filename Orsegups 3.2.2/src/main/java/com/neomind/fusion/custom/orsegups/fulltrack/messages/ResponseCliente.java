package com.neomind.fusion.custom.orsegups.fulltrack.messages;

import java.util.List;

import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackCliente;

public class ResponseCliente extends Response{

    private List<FulltrackCliente> data;

    public List<FulltrackCliente> getData() {
        return data;
    }

    public void setData(List<FulltrackCliente> data) {
        this.data = data;
    }
    
    
    
}
