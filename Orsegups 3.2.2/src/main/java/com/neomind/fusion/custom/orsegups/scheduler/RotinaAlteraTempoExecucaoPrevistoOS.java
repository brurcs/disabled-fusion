package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaAlteraTempoExecucaoPrevistoOS implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(RotinaAlteraTempoExecucaoPrevistoOS.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		Connection conn = null;
		StringBuilder sqlUpdateTempoExecucaoPrevistoOS = null;
		PreparedStatement pstm = null;
		log.warn("##### INICIO UPDATE TEMPO EXECUÇÃO PREVISTO OS: update nas OS's dos clientes do Sigma - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		try
		{

			sqlUpdateTempoExecucaoPrevistoOS = new StringBuilder();
			conn = PersistEngine.getConnection("SIGMA90");
			sqlUpdateTempoExecucaoPrevistoOS.append(" UPDATE dbORDEM SET TEMPOEXECUCAOPREVISTO = 3 WHERE TEMPOEXECUCAOPREVISTO < 3 AND FECHADO = 0 ");
			pstm = conn.prepareStatement(sqlUpdateTempoExecucaoPrevistoOS.toString());
			pstm.executeUpdate();

		}
		catch (Exception e)
		{
			log.error("##### ERRO UPDATE TEMPO EXECUÇÃO PREVISTO OS: Erro  update nas OS's dos clientes do Sigma - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			System.out.println("["+key+"] ##### ERRO UPDATE TEMPO EXECUÇÃO PREVISTO OS: Erro  update nas OS's dos clientes do Sigma - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, null);
			log.warn("##### FIM DO UPDATE TEMPO EXECUÇÃO PREVISTO OS: update nas OS's dos clientes do Sigma - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
	}
}
