package com.neomind.fusion.custom.orsegups.maps.call.engine;

public enum CallAlertAitStatus
{
	ATENDIDA, NAO_ATENDIDA, ATENDIDA_DESLIGADA,PROXIMA_LIGACAO,OCUPADO,CAIXA_POSTAL,NAO_EXISTE,EM_ESPERA,AGUARDAR_30;
		
}
