package com.neomind.fusion.custom.orsegups.converter;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTaticoDAO;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTecnicoDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.HistoricoFapDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.ItemOrcamentoDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.TaticoDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.TecnicoDTO;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.utils.FornecedorVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class FAPHistoricoFluxoAutorizacaoPagamentoConverter extends StringConverter {

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin) {

		Connection conn = null;
		StringBuilder sqlHstFAPPagas = new StringBuilder();

		
		PreparedStatement pstm = null;
		PreparedStatement pstm2 = null;

		ResultSet rs = null;
		ResultSet rs2 = null;

		@SuppressWarnings("unused")
		List<NeoObject> listaProcessos = new ArrayList<NeoObject>();
		EntityWrapper processEntityWrapper = new EntityWrapper(field.getForm().getObject());
		Long codigoAplicacao = (Long) processEntityWrapper.findValue("aplicacaoPagamento.codigo");
		Long neoIdFapAtual = (Long) processEntityWrapper.findValue("wfprocess.neoId");

		StringBuilder textoTable = new StringBuilder();

		try {
			if (codigoAplicacao != null && (codigoAplicacao < 10L)) {

				// Agrupar históricos: 2,3,5,6

				conn = PersistEngine.getConnection("");

				@SuppressWarnings("unused")
				List<FornecedorVO> lstFornecedor = new ArrayList<FornecedorVO>();

				Map<String, Long> aplicacoesAgrupadoras = new HashMap<>();
				aplicacoesAgrupadoras.put("2", 2L);
				aplicacoesAgrupadoras.put("3", 3L);
				aplicacoesAgrupadoras.put("4", 4L);
				aplicacoesAgrupadoras.put("5", 5L);
				aplicacoesAgrupadoras.put("6", 6L);
				aplicacoesAgrupadoras.put("7", 7L);
				aplicacoesAgrupadoras.put("8", 8L);
				aplicacoesAgrupadoras.put("9", 9L);

				boolean agruparHistorico = FAPParametrizacao.findParameter("agruparHistoricoManutencaoInstalacao").equals("1") ? true : false;
				String dias = FAPParametrizacao.findParameter("diasAgruparHistoricoManutencaoInstalacao");
				
				if((agruparHistorico && !(aplicacoesAgrupadoras.containsValue(codigoAplicacao))) || !agruparHistorico) {
					sqlHstFAPPagas.append(" SELECT wf.neoid, wf.code, wf.processState, fap.neoid as codigoFap, fap.dataSolicitacao, fapapl.codigo, faplauadm.relato, faplauadm.laudo, faplauadm.kilometragem, ");
					sqlHstFAPPagas.append(" faplauele.relato as relatoEletronica, faplauele.laudo as laudoEletronica FROM D_FAPAUTORIZACAODEPAGAMENTO FAP		 	                                          ");
					sqlHstFAPPagas.append(" INNER JOIN WFPROCESS WF ON WF.entity_neoId = FAP.neoId   	    																					              ");
					sqlHstFAPPagas.append(" INNER JOIN D_FAPAplicacaoDoPagamento FAPAPL ON FAP.aplicacaoPagamento_neoId = FAPAPL.neoId            											                  ");
					sqlHstFAPPagas.append(" LEFT JOIN D_FAPLaudoManutencaoAdministrativo FAPLAUADM ON FAP.laudo_neoId = FAPLAUADM.neoId 														              ");
					sqlHstFAPPagas.append(" LEFT JOIN D_FAPLaudoOrdemServicoEletronica FAPLAUELE ON FAP.laudoEletronica_neoId = FAPLAUELE.neoId                                                               ");
					
					if ((codigoAplicacao == 2) || (codigoAplicacao == 3) || (codigoAplicacao == 4) || (codigoAplicacao == 5) || (codigoAplicacao == 6) || (codigoAplicacao == 7) || (codigoAplicacao == 8) || (codigoAplicacao == 9)) {
						sqlHstFAPPagas.append(" INNER JOIN X_SIGMACENTRAL DBCENFUS ON DBCENFUS.neoId = FAP.contaSigma_neoId         															              ");
						sqlHstFAPPagas.append(" INNER JOIN [FSOODB03\\SQL01].SIGMA90.DBO.DBCENTRAL SIGCEN ON SIGCEN.cd_cliente = DBCENFUS.cd_cliente                                                          ");
					} else {
						sqlHstFAPPagas.append(" INNER JOIN D_OTSViatura OTS ON FAP.viatura_neoid = OTS.neoid");
					}

					sqlHstFAPPagas.append(" WHERE WF.saved = 1 AND (WF.processState = 0 OR WF.processState = 1) AND WF.neoId <> " + neoIdFapAtual + " AND FAP.aprovado = 1                                    ");
					
					if (codigoAplicacao == 2L || codigoAplicacao == 3L || codigoAplicacao == 9L) {
						Long idEmpresa = (Long) processEntityWrapper.findValue("contaSigma.id_empresa");
						String idCentral = (String) processEntityWrapper.findValue("contaSigma.id_central");
						
						sqlHstFAPPagas.append(" AND FAPAPL.codigo = " + codigoAplicacao + " AND SIGCEN.id_Central = '" + idCentral + "' AND SIGCEN.id_empresa = " + idEmpresa);
					} else if (codigoAplicacao == 5L || codigoAplicacao == 6L || codigoAplicacao == 8L) {
						Long idEmpresa = (Long) processEntityWrapper.findValue("contaSigma.id_empresa");
						String idCentral = (String) processEntityWrapper.findValue("contaSigma.id_central");

						sqlHstFAPPagas.append(" AND FAPAPL.codigo = " + codigoAplicacao + " AND SIGCEN.id_Central = '" + idCentral + "' AND SIGCEN.id_empresa = " + idEmpresa);
					} else if ((codigoAplicacao == 4) || (codigoAplicacao == 7)) {
						Long codigoCliente = (Long) processEntityWrapper.findValue("contaSigma.cd_cliente");

						sqlHstFAPPagas.append(" AND FAPAPL.codigo = " + codigoAplicacao + " AND SIGCEN.cd_cliente = " + codigoCliente);
					} else {
						String placaViatura = (String) processEntityWrapper.findValue("viatura.placa");

						sqlHstFAPPagas.append("AND OTS.placa = '" + placaViatura + "'                                                                                                                         ");
					}
				} else if(agruparHistorico && (aplicacoesAgrupadoras.containsValue(codigoAplicacao))) {
					
					Long idEmpresa = (Long) processEntityWrapper.findValue("contaSigma.id_empresa");
					String idCentral = (String) processEntityWrapper.findValue("contaSigma.id_central");
					
					sqlHstFAPPagas.append("SELECT wf.neoid, wf.code, wf.processState, fap.neoid as codigoFap, fap.dataSolicitacao, faplauadm.relato, faplauadm.laudo, faplauadm.kilometragem, ");
					sqlHstFAPPagas.append("faplauele.relato as relatoEletronica, faplauele.laudo as laudoEletronica FROM D_FAPAUTORIZACAODEPAGAMENTO FAP                                      ");		 	                                          
					sqlHstFAPPagas.append("INNER JOIN WFPROCESS WF ON WF.entity_neoId = FAP.neoId                                                                                             ");  							                  
					sqlHstFAPPagas.append("LEFT JOIN D_FAPLaudoManutencaoAdministrativo FAPLAUADM ON FAP.laudo_neoId = FAPLAUADM.neoId                                                        "); 														              
					sqlHstFAPPagas.append("LEFT JOIN D_FAPLaudoOrdemServicoEletronica FAPLAUELE ON FAP.laudoEletronica_neoId = FAPLAUELE.neoId                                                "); 
					sqlHstFAPPagas.append("INNER JOIN D_FAPAplicacaoDoPagamento FAPAPL ON FAP.aplicacaoPagamento_neoId = FAPAPL.neoId            											                  ");
					sqlHstFAPPagas.append("INNER JOIN X_SIGMACENTRAL DBCENFUS ON DBCENFUS.neoId = FAP.contaSigma_neoId                                                                        ");         															              
					sqlHstFAPPagas.append("INNER JOIN [FSOODB03\\SQL01].SIGMA90.DBO.DBCENTRAL SIGCEN ON SIGCEN.cd_cliente = DBCENFUS.cd_cliente                                               ");                                                          		
					sqlHstFAPPagas.append("WHERE WF.saved = 1 AND (WF.processState = 0 OR WF.processState = 1) AND WF.neoId <> " + neoIdFapAtual                                               );
					sqlHstFAPPagas.append(" AND WF.STARTDATE BETWEEN GETDATE()-" + dias +  " AND GETDATE()                                                                                    ");
					
					if ((codigoAplicacao == 4) || (codigoAplicacao == 7)) {
						Long codigoCliente = (Long) processEntityWrapper.findValue("contaSigma.cd_cliente");
						
						sqlHstFAPPagas.append(" AND FAPAPL.codigo = " + codigoAplicacao + " AND SIGCEN.cd_cliente = " + codigoCliente);
					} else {
						sqlHstFAPPagas.append("AND SIGCEN.id_Central = '" + idCentral + "' AND SIGCEN.id_empresa = " + idEmpresa															   );
						
					}

				}

				pstm = conn.prepareStatement(sqlHstFAPPagas.toString());
				rs = pstm.executeQuery();

				textoTable.append("<script>");
				textoTable.append("				function showTarefa(neoid) { ");
				textoTable.append("			    	var url = '" + PortalUtil.getBaseURL() + "bpm/workflow_search.jsp?id='+neoid; ");
				textoTable.append("			    	var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,''); ");
				textoTable.append("			    	var win = window.name;  ");
				textoTable.append("			        NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win); ");
				textoTable.append("			    } ");
				textoTable.append("</script>");
				textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
				textoTable.append("			<tr style=\"cursor: auto\">");
				textoTable.append("				<th style=\"cursor: auto\">Tarefa</th>");
				textoTable.append("				<th style=\"cursor: auto\">Data</th>");
				if (codigoAplicacao == 1L) {
					textoTable.append("				<th style=\"cursor: auto\">KM</th>");
				}
				textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Relato</th>");
				textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Laudo</th>");
				textoTable.append("				<th style=\"cursor: auto\">Executado</th>");
				textoTable.append("				<th style=\"cursor: auto\">Status</th>");
				textoTable.append("			</tr>");
				textoTable.append("			<tbody>");

				BigDecimal valorTotalHistoricoFap = new BigDecimal(0.00);

				if (NeoUtils.safeIsNotNull(rs)) {
					while (rs.next()) {
						Long neoId = rs.getLong("neoid");
						String code = rs.getString("code");
						GregorianCalendar dataRaw = new GregorianCalendar();
						dataRaw.setTime(rs.getDate("dataSolicitacao"));
						//Long aplicacaoPagamento = rs.getLong("codigo");

						String textoRelato = "";
						String textoLaudo = "";
						String textoKilometragem = "";
						// Manutencao Frota
						if (codigoAplicacao == 1L) {
							textoRelato = rs.getString("relato");
							textoLaudo = rs.getString("laudo");
							textoKilometragem = rs.getString("kilometragem").toString();
							if (textoKilometragem == null) {
								textoKilometragem = "";
							}
						} // Manutencao Eletronica
						else if ((codigoAplicacao == 2L) || (codigoAplicacao == 3L) || (codigoAplicacao == 4L) || (codigoAplicacao == 5L) || (codigoAplicacao == 6L) || (codigoAplicacao == 7L) || (codigoAplicacao == 8L) || (codigoAplicacao == 9L)) {
							textoRelato = rs.getString("relatoEletronica");
							textoLaudo = rs.getString("laudoEletronica");
						}
						if (textoRelato == null) {
							textoRelato = "";
						}
						if (textoLaudo == null) {
							textoLaudo = "";
						}

						StringBuilder textoTableExecutado = new StringBuilder();
						StringBuilder sqlHstFAPItens = new StringBuilder();

						Long codigoFap = rs.getLong("codigoFap");

						sqlHstFAPItens.append(" SELECT FAPORC.DESCRICAO, FAPORC.VALOR, TIPORC.DESCRICAO AS TIPOORC                  ");
						sqlHstFAPItens.append(" FROM D_FAPAUTORIZACAODEPAGAMENTO_ITEMORCAMENTO ITMORC                               ");
						sqlHstFAPItens.append(" INNER JOIN D_FAPITEMDEORCAMENTO FAPORC ON ITMORC.ITEMORCAMENTO_NEOID = FAPORC.NEOID ");
						sqlHstFAPItens.append(" INNER JOIN D_FAPTIPOITEMDEORCAMENTO TIPORC ON FAPORC.TIPOITEM_NEOID = TIPORC.NEOID  ");
						sqlHstFAPItens.append(" WHERE ITMORC.D_FAPAUTORIZACAODEPAGAMENTO_NEOID = " + codigoFap);

						pstm2 = conn.prepareStatement(sqlHstFAPItens.toString());
						rs2 = pstm2.executeQuery();

						BigDecimal valorTotalItensFap = new BigDecimal(0.00);

						if (NeoUtils.safeIsNotNull(rs2)) {
							textoTableExecutado.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
							textoTableExecutado.append("			<tr style=\"cursor: auto\">");
							textoTableExecutado.append("				<th style=\"cursor: auto\">Tipo</th>");
							textoTableExecutado.append("				<th style=\"cursor: auto; white-space: normal\">Descrição</th>");
							textoTableExecutado.append("				<th style=\"cursor: auto\">Valor</th>");
							textoTableExecutado.append("			</tr>");
							textoTableExecutado.append("			<tbody>");
							while (rs2.next()) {
								String tipoItem = rs2.getString("TIPOORC");
								String descricaoItem = rs2.getString("descricao");
								String valorItem = NeoUtils.safeOutputString(rs2.getBigDecimal("valor"));
								BigDecimal valorItemFap = new BigDecimal(valorItem);
								/**
								 * Subtrair o valor quando o tipo do item de Orçamento for Desconto
								 */
								if(tipoItem.equals("DESCONTO")) {
									valorTotalItensFap = valorTotalItensFap.subtract((BigDecimal) valorItemFap);
								}else {
									valorTotalItensFap = valorTotalItensFap.add((BigDecimal) valorItemFap);									
								}
								textoTableExecutado.append("		<tr>");
								textoTableExecutado.append("			<td>" + tipoItem + "</td>");
								textoTableExecutado.append("			<td style=\"white-space: normal\">" + descricaoItem + "</td>");
								textoTableExecutado.append("			<td>" + valorItem + "</td>");
								textoTableExecutado.append("		</tr>");
							}
							textoTableExecutado.append("			<tr>");
							textoTableExecutado.append("				<td colspan=2 style=\"text-align: right\">" + "Total:" + "</td>");
							textoTableExecutado.append("				<td style=\"text-align: left\">" + NeoUtils.safeOutputString((BigDecimal) valorTotalItensFap) + "</td>");
							textoTableExecutado.append("			</tr>");
							textoTableExecutado.append("			</tbody>");
							textoTableExecutado.append("		</table>");
						}

						valorTotalHistoricoFap = valorTotalHistoricoFap.add((BigDecimal) valorTotalItensFap);

						String status = "";
						Long situacaoTarefa = rs.getLong("processState");
						if (situacaoTarefa == 0L) {
							status = "Em andamento";
						} else if (situacaoTarefa == 1L) {
							status = "Encerrado";
						}

						textoTable.append("		<tr>");
						textoTable.append("			<td><a href=\"javascript:showTarefa('" + neoId + "');\"><img class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a>&nbsp;" + code + "</td>");
						textoTable.append("			<td>" + NeoDateUtils.safeDateFormat(dataRaw, "dd/MM/yyyy") + "</td>");
						if (codigoAplicacao == 1L) {
							textoTable.append("			<td>" + textoKilometragem + "</td>");
						}
						textoTable.append("			<td style=\"white-space: normal\">" + textoRelato + "</td>");
						textoTable.append("			<td style=\"white-space: normal\">" + textoLaudo + "</td>");
						textoTable.append("			<td>" + textoTableExecutado.toString() + "</td>");
						textoTable.append("			<td>" + status + "</td>");
						textoTable.append("		</tr>");
					}

				} else {
					textoTable.append("		<tr>");
					if (codigoAplicacao == 1L) {
						textoTable.append("			<td colspan='7'>Nenhum registro</td>");
					} else {
						textoTable.append("			<td colspan='6'>Nenhum registro</td>");
					}
					textoTable.append("		</tr>");

				}

				textoTable.append("		<tr>");
				if (codigoAplicacao == 1L) {
					textoTable.append("				<td colspan=6 style=\"text-align: right\">" + "Total Geral:" + "</td>");
				} else {
					textoTable.append("				<td colspan=5 style=\"text-align: right\">" + "Total Geral:" + "</td>");
				}
				textoTable.append("				<td colspan=1 style=\"text-align: left\">" + NeoUtils.safeOutputString((BigDecimal) valorTotalHistoricoFap) + "</td>");
				textoTable.append("		</tr>");
				textoTable.append("			</tbody>");
				textoTable.append("		</table>");

			} else {

				textoTable.append("<script>                                                                                  ");
				textoTable.append("function showTarefa(neoid) {                                                              ");
				textoTable.append("var url = '" + PortalUtil.getBaseURL() + "bpm/workflow_search.jsp?id='+neoid;             ");
				textoTable.append("var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,'');             ");
				textoTable.append("var win = window.name;                                                                    ");
				textoTable.append("NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win);              ");
				textoTable.append("}                                                                                         ");
				textoTable.append("</script>                                                                                 ");
				textoTable.append("<table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
				textoTable.append("<thead>                                                                                   ");
				textoTable.append("<tr>                                                                                      ");
				textoTable.append("<th>Tarefa</th>                                                                           ");
				textoTable.append("<th>Data</th>                                                                             ");
				textoTable.append("<th>Relato</th>                                                                           ");
				textoTable.append("<th>Laudo</th>                                                                            ");
				textoTable.append("<th>Executado</th>                                                                        ");
				textoTable.append("<th>Status</th>                                                                           ");
				textoTable.append("</tr>                                                                                     ");
				textoTable.append("</thead>                                                                                  ");
				textoTable.append("<tbody>                                                                                   ");

				BigDecimal valorTotal = new BigDecimal(0.00);

				if (codigoAplicacao == 10) {
					TecnicoDTO tecDTO = new TecnicoDTO();
					Long codigoTecnico = null;
					codigoTecnico = (Long) processEntityWrapper.findField("tecnicoSigma.tecnico.cd_colaborador").getValue();
					tecDTO = AplicacaoTecnicoDAO.recuperaHistoricoTecnico(codigoTecnico, codigoAplicacao, neoIdFapAtual);

					List<HistoricoFapDTO> historico = tecDTO.getHistorico();

					if (NeoUtils.safeIsNotNull(historico)) {
						for (HistoricoFapDTO hist : historico) {
							this.htmlAppendHistorico(textoTable, hist);
							valorTotal = valorTotal.add(hist.getValorTotal());
						}
					}

				} else if (codigoAplicacao == 11) {
					TaticoDTO tatDTO = new TaticoDTO();
					Long codigoRota = null;
					codigoRota = (Long) processEntityWrapper.findField("rotaSigma.rotaSigma.cd_rota").getValue();
					tatDTO = AplicacaoTaticoDAO.recuperaHistoricoTatico(codigoRota, codigoAplicacao, neoIdFapAtual);

					List<HistoricoFapDTO> historico = tatDTO.getHistorico();

					if (NeoUtils.safeIsNotNull(historico)) {
						for (HistoricoFapDTO hist : historico) {
							this.htmlAppendHistorico(textoTable, hist);
							valorTotal = valorTotal.add(hist.getValorTotal());
						}
					}

				} else if (codigoAplicacao == 12) {		    
					List<HistoricoFapDTO> listaHistoricoFap = new ArrayList<>();
					Long fornecedor;

					//Validação para a etapa de Finalizar Orçamento
					try {
						fornecedor = (Long) processEntityWrapper.findField("fornecedor.codfor").getValue();
					} catch (NullPointerException e) {
						fornecedor = 0L;
					}

					if (fornecedor != 0L) {
						List<ItemOrcamentoDTO> listaItemOrcamento = null;

						conn = null;
						pstm = null;
						rs = null;
						StringBuilder sql = new StringBuilder();						

						sql.append("SELECT wf.neoid, wf.code, wf.processState, fap.neoid as codigoFap, fap.dataSolicitacao,		 ");
						sql.append("fapapl.codigo,faplauele.relato as relatoEletronica, faplauele.laudo as laudoEletronica,		 ");
						sql.append("FORN.codfor as CodigoFornecedor 									 ");
						sql.append("FROM D_FAPAUTORIZACAODEPAGAMENTO FAP 								 ");
						sql.append("INNER JOIN WFPROCESS WF ON WF.entity_neoId = FAP.neoId   						 ");
						sql.append("INNER JOIN D_FAPAplicacaoDoPagamento FAPAPL ON FAP.aplicacaoPagamento_neoId = FAPAPL.neoId 		 ");
						sql.append("LEFT JOIN D_FAPLaudoOrdemServicoEletronica FAPLAUELE ON FAP.laudoEletronica_neoId = FAPLAUELE.neoId  ");
						sql.append("LEFT JOIN X_ETABFOR as FORN ON FAP.fornecedor_neoId = FORN.neoId 					 ");
						sql.append("WHERE WF.saved = 1 AND (WF.processState = 0 OR WF.processState = 1) 				 ");
						sql.append("AND WF.neoId <> " + neoIdFapAtual + " ");
						sql.append("AND FAPAPL.codigo = 12 and codfor = " + fornecedor);

						try {
							conn = PersistEngine.getConnection("");
							pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
							rs = pstm.executeQuery();

							while (rs.next()) {
								HistoricoFapDTO historicoFap = new HistoricoFapDTO();
								String codigoTarefa = rs.getString("code");
								Date dataSolicitacao = rs.getDate("dataSolicitacao");
								int status = rs.getInt("processState");
								Long wfId = rs.getLong("neoid");
								String laudo = rs.getString("laudoEletronica");
								String relato = rs.getString("relatoEletronica");

								historicoFap.setCodigoTarefa(codigoTarefa);
								historicoFap.setDataSolicitacao(dataSolicitacao);
								historicoFap.setNeoIdTarefa(wfId);
								historicoFap.setLaudo(laudo);
								historicoFap.setRelato(relato);

								switch (status) {
								case 0:
									historicoFap.setStatus("Em andamento");
									break;
								case 1:
									historicoFap.setStatus("Encerrado");
									break;
								}

								// recupera os itens de orcamento da FAP atual
								listaItemOrcamento = AplicacaoTaticoDAO.consultaItemOrcamento(codigoTarefa, codigoAplicacao);
								valorTotal = new BigDecimal(0.00);

								if (NeoUtils.safeIsNotNull(listaItemOrcamento)) {
									for (ItemOrcamentoDTO item : listaItemOrcamento) {
										valorTotal = valorTotal.add(item.getValor());
									}

									// Objeto que representa a FAP atual recebe os itens de orçamento e o valor total
									historicoFap.setItemOrcamento(listaItemOrcamento);
									historicoFap.setValorTotal(valorTotal);
									// Objeto da aplicação recebe a lista de suas respectivas FAP's
									listaHistoricoFap.add(historicoFap);
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							throw new Exception(e.getMessage());
						} finally {
							try {
								OrsegupsUtils.closeConnection(conn, pstm, rs);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					if (NeoUtils.safeIsNotNull(listaHistoricoFap)) {	
						for (HistoricoFapDTO hist : listaHistoricoFap) {
							this.htmlAppendHistorico(textoTable, hist);
							valorTotal = valorTotal.add(hist.getValorTotal());
						}
					}
				}

				textoTable.append("<tr>                                                         ");
				textoTable.append("<td colspan=5 style=\"text-align: right\"> Total Geral: </td>");
				textoTable.append("<td>R$ " + valorTotal.setScale(2) + "</td>                   ");
				textoTable.append("</tr>                                                        ");
				textoTable.append("</tbody>                                                     ");
				textoTable.append("</table></br>                                                ");

				return textoTable.toString();
			}	    
			return textoTable.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw (WorkflowException) e;
		} finally {
			try {
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin) {
		return getHTMLInput(field, origin);
	}

	private void htmlAppendHistorico(StringBuilder html, HistoricoFapDTO historico) {
		html.append("<tr>                                                                                                                      ");
		html.append("<td><a href=\"javascript:showTarefa('" + historico.getNeoIdTarefa() + "');\">                                             ");
		html.append("<img class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\">");
		html.append("</a>&nbsp;" + historico.getCodigoTarefa() + "</td>                                                                        ");
		html.append("<td>" + NeoDateUtils.safeDateFormat(historico.getDataSolicitacao(), "dd/MM/yyyy") + "</td>                                ");
		if (historico.getRelato() != null) {
			html.append("<td>" + historico.getRelato() + "</td>                                                                                ");
		} else {
			html.append("<td> -- </td>                                                                                                         ");
		}
		if (historico.getLaudo() != null) {
			html.append("<td>" + historico.getLaudo() + "</td>                                                                                 ");
		} else {
			html.append("<td> -- </td>                                                                                                         ");
		}
		html.append("<td>                                                                                                                      ");
		html.append("<table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">                                ");
		html.append("<thead>                                                                                                                   ");
		html.append("<th> Tipo </th>                                                                                                           ");
		html.append("<th> Descrição </th>                                                                                                      ");
		html.append("<th> Valor </th>                                                                                                          ");
		html.append("</thead>                                                                                                                  ");
		html.append("<tbody>                                                                                                                   ");
		List<ItemOrcamentoDTO> listaItens = historico.getItemOrcamento();
		BigDecimal totalItens = new BigDecimal(0);
		for (ItemOrcamentoDTO item : listaItens) {
			this.htmlAppendItem(html, item);
			totalItens = totalItens.add(item.getValor());
		}
		html.append("<tr>                                                   ");
		html.append("<td colspan=2 style=\"text-align: right\"> Total: </td>");
		html.append("<td> R$ " + totalItens.setScale(2) + "</td>            ");
		html.append("</tr>                                                  ");
		html.append("</tbody>                                               ");
		html.append("</table>                                               ");
		html.append("</td>                                                  ");
		html.append("<td>" + historico.getStatus() + "</td>                ");
		html.append("</tr>                                                  ");
	}

	private void htmlAppendItem(StringBuilder html, ItemOrcamentoDTO item) {
		html.append(" <tr>                                             ");
		html.append(" <td> " + item.getTipo() + "</td>                  ");
		html.append(" <td> " + item.getDescricao() + "</td>             ");
		html.append(" <td> R$ " + item.getValor().setScale(2) + "</td> ");
		html.append(" </tr>                                            ");

	}
}