package com.neomind.fusion.custom.orsegups.contract.converter;

import org.apache.velocity.VelocityContext;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.portal.VelocityUtils;

public class EnderecoConverter extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		VelocityContext context = new VelocityContext();

		return VelocityUtils.runTemplate("custom/orsegups/editarEnderecoPosto.vm", context);
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
