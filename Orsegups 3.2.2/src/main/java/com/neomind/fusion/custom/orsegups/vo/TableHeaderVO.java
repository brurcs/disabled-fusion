package com.neomind.fusion.custom.orsegups.vo;

public class TableHeaderVO {
	int position;
	String name;
	String title;
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public TableHeaderVO(int position, String name, String title) {
		super();
		this.position = position;
		this.name = name;
		this.title = title;
	}
	
}
