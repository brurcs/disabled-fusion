package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;

public class FAPSlipBancoConverter extends EntityConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuilder outBuilder = new StringBuilder();
		outBuilder.append("<span>Padrão de Pagamento: boleto ou depósito no Banco do Brasil. Se utilizar Banco diferente, justificar no campo observação</span>");

		return super.getHTMLInput(field, origin) + outBuilder.toString();

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return super.getHTMLView(field, origin);
	}
}
