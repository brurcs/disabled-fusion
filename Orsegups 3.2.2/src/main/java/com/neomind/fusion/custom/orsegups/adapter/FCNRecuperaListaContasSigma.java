package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Collection;
import java.util.List;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FCNRecuperaListaContasSigma implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) {
		try {
			@SuppressWarnings("unchecked")
			Collection<NeoObject> contasSigma = (Collection<NeoObject>) processEntity.findValue("listaContasSigma");
			// Se ja possuir contas na lista, ignora o preenchimento
			if ((contasSigma != null) && (!contasSigma.isEmpty())) {
				return;
			}

			Long empresa = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
			Long filial = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
			Long contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

			QLGroupFilter filterCtr = new QLGroupFilter("AND");
			filterCtr.addFilter(new QLEqualsFilter("usu_codemp", empresa));
			filterCtr.addFilter(new QLEqualsFilter("usu_codfil", filial));
			filterCtr.addFilter(new QLEqualsFilter("usu_numctr", contrato));

			ExternalEntityInfo infoContaSigma = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SIGMAVCLIENTES");

			StringBuilder sql = new StringBuilder();
			sql.append(" select sig.usu_codcli from usu_t160sig sig  ");
			sql.append(" where sig.usu_codemp = " + empresa);
			sql.append(" and sig.usu_codfil = " + filial);
			sql.append(" and usu_numctr = " + contrato);
			sql.append(" order by usu_codcli ");

			Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();

			boolean isListEmpty = true;
			if ((resultList != null) && (resultList.size() > 0)) {
				for (Object result : resultList) {
					String cdCli = NeoUtils.safeOutputString(result);
					Long cd_cli = Long.valueOf(Long.parseLong(cdCli));
					QLEqualsFilter filterConta = new QLEqualsFilter("cd_cliente", cd_cli);
					List<NeoObject> listaObjectConta = PersistEngine.getObjects(infoContaSigma.getEntityClass(),filterConta);
					if ((listaObjectConta != null) && (!listaObjectConta.isEmpty())) {
						NeoObject noConta = (NeoObject) listaObjectConta.get(0);
						if (noConta != null) {
							processEntity.findField("listaContasSigma").addValue(noConta);
							isListEmpty = false;
						}
					}
				}
			}
			processEntity.setValue("incluirContaSigma", Boolean.valueOf(isListEmpty));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro ao Recuperar a Congta Sigma na classe FCNRecuperaListaContasSigma"+ activity.getCode());
			throw new WorkflowException("Erro ao Recuperar a Congta Sigma!"+ e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {

	}

}
