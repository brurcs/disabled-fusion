package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

import br.com.segware.sigmaWebServices.webServices.EventoRecebido;
import br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy;

/**
 * <b> Regra de Negócio: </b>
 * 
 * Achar eventos E302 (Bateria Baixa), que em até 5horas para trás possuem E301 (Falta de Energia Elétrica)
 * sem um R301 (Restauro de energia elétrica), e a partir 
 * 
 * @author herisson.ferreira
 *
 */
public class RotinaGeraEventoXENG implements CustomJobAdapter {
	private static final Log log = LogFactory.getLog(RotinaGeraEventoXENG.class);
	private static final String nomeRotina = "RotinaGeraEventoXENG";
	private static final String evento = "XENG";
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		System.out.println("#### INICIO DA ROTINA GERAR EVENTO XENG - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		
		List<EventoRecebido> listaEventos = new ArrayList<EventoRecebido>();
		
		try {
			
			String ultimaExecucao = ultimaExecucaoRotina(nomeRotina);
			String execucaoAtual = ultimaExecucaoRotinaRange(nomeRotina);
			listaEventos = retornaEvento(ultimaExecucao, execucaoAtual);
			
			for(EventoRecebido eventoAux : listaEventos) {
				
				try {	
					executaServicoSegware(eventoAux);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#### ERRO AO GERAR EVENTO XENG: BA: "+eventoAux.getIdCentral()+"["+eventoAux.getParticao()+"] - CLIENTE:"+eventoAux.getCdCliente());
				}
				
			}
			
			atualizaUltimaExecucao(nomeRotina);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### ERRO ROTINA GERAR EVENTO XENG - Data: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		} finally {
			System.out.println("#### FIM DA ROTINA GERAR EVENTO XENG - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
		
		
	}
	
	/**
	 * Retornar os eventos
	 * 
	 * @param ultimaExecucao última execução da Rotina 
	 * @param execucaoAtual última execução da Rotina +9 minutos
	 */
	public List<EventoRecebido> retornaEvento(String ultimaExecucao, String execucaoAtual) {
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		List<EventoRecebido> listaEventos = new ArrayList<EventoRecebido>();
		
		try {
		
			
			sql.append("	SELECT HE302.DT_RECEBIDO, DB.CD_CLIENTE, DB.ID_EMPRESA, DB.ID_CENTRAL, DB.PARTICAO from VIEW_HISTORICO HE302  ");
			sql.append("	INNER JOIN dbCENTRAL DB ");
			sql.append("	ON HE302.CD_CLIENTE = DB.CD_CLIENTE ");
			sql.append("	WHERE HE302.cd_evento IN ('E302','E309','1301','1302','1309')");
			sql.append("	AND HE302.CD_CLIENTE IN ( ");
			sql.append("	SELECT HE301.CD_CLIENTE FROM VIEW_HISTORICO HE301 WHERE HE301.cd_evento = 'E301' ");
			sql.append("	AND HE301.CD_CLIENTE = HE302.CD_CLIENTE ");
			sql.append("	AND HE301.DT_RECEBIDO >= DATEADD(HH,-12,GETDATE()) ");
			sql.append("	AND NOT EXISTS ");
			sql.append("	(SELECT HR301.CD_CLIENTE FROM VIEW_HISTORICO HR301 ");
			sql.append("	    WHERE HR301.CD_EVENTO = 'R301' ");
			sql.append("		AND HE301.CD_CLIENTE = HR301.CD_CLIENTE ");
			sql.append("	    AND HR301.DT_RECEBIDO > HE301.DT_RECEBIDO)) ");
			sql.append("	AND HE302.DT_RECEBIDO BETWEEN ? AND ? ");
			
			conn = PersistEngine.getConnection("SIGMA90");
			
			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, ultimaExecucao);
			pstm.setString(2, execucaoAtual);

			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				EventoRecebido eventoRecebido = new EventoRecebido();

				eventoRecebido.setCodigo(evento);
				eventoRecebido.setData(new GregorianCalendar());
				eventoRecebido.setEmpresa(rs.getLong("ID_EMPRESA"));
				eventoRecebido.setIdCentral(rs.getString("ID_CENTRAL"));
				eventoRecebido.setParticao(rs.getString("PARTICAO"));
				eventoRecebido.setCdCliente(rs.getInt("CD_CLIENTE"));
				eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
				eventoRecebido.setProtocolo(Byte.parseByte("2"));
		
				listaEventos.add(eventoRecebido);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaEventos;	
	}
	
	/**
	 * Captura a última execução da rotina
	 * 
	 * @param nomeRotina Nome da rotina a ser executada
	 * @return A data da última execução da rotina
	 */
	public String ultimaExecucaoRotina(String nomeRotina) {

		String retorno = null;
		
		try	{
			
			Collection<NeoObject> monitoraAgendador = null;

			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty()){
				for (NeoObject neoObject : monitoraAgendador){
					EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

					GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

					retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return retorno;

	}

	/**
	 * Define o range máximo para execução da Rotina
	 * 
	 * @param nomeRotina Nome da rotina a ser executada
	 * @return A data da última execução da rotina + 9minutos para criar um range de alcance
	 */
	public String ultimaExecucaoRotinaRange(String nomeRotina) {

		String retorno = null;
		
		try	{
			
			Collection<NeoObject> monitoraAgendador = null;

			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty()){
				for (NeoObject neoObject : monitoraAgendador){
					
					EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

					GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

					GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

					dataFinalAgendadorAux.add(GregorianCalendar.MINUTE, 9);
					dataFinalAgendadorAux.set(GregorianCalendar.SECOND, 59);
					dataFinalAgendadorAux.set(GregorianCalendar.MILLISECOND, 59);

					retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return retorno;
	}
	
	/**
	 * Insere a data da última execução da rotina no Formulário
	 * 
	 * @param nomeRotina Nome da rotina a ser executado
	 */
	public void atualizaUltimaExecucao(String nomeRotina) {
		
		int minutos = 0;
		
		try
		{
			
			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty())
			{
				NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

				EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);
				
				GregorianCalendar dataExecucao = new GregorianCalendar();	
				minutos = dataExecucao.get(GregorianCalendar.MINUTE);
				minutos = (minutos-(minutos%10));
				
				dataExecucao.set(GregorianCalendar.MINUTE, minutos);
				dataExecucao.set(GregorianCalendar.SECOND, 00);
				dataExecucao.set(GregorianCalendar.MILLISECOND, 00);

				monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(dataExecucao);

				PersistEngine.persist(neoObject);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	/**
	 * @param eventoRecebido Objeto do Evento encontrada na Query
	 */
	private String executaServicoSegware(EventoRecebido eventoRecebido)
	{
		String returnFromAccess = null;
		
		try
		{
			ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();

			returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
			System.out.println("##### ERRO ROTINA GERAR EVENTO XENG - Data: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
		return returnFromAccess;
	}
}