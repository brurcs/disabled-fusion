package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.ContractCEPConverter;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;

public class MedConvertSituacaoCtr extends DefaultConverter
{

	private static final Log log = LogFactory.getLog(ContractCEPConverter.class);

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		/*String sitCtr = "ok";
		List<NeoObject> colaboradores = new ArrayList<NeoObject>();
		Long contrato = 0L;
		colaboradores = (List<NeoObject>) field.getForm().getField("listaColaboradores").getValue();
		contrato =  (Long) field.getForm().getField("contrato").getValue();
		String qtdVagas = String.valueOf(field.getForm().getField("qtdVagas").getValue());
		String qtdAux = "";
		String qtdAux2 = "";
		qtdAux = qtdVagas;
		
		qtdAux = qtdAux.substring(qtdAux.indexOf("/")+1, qtdAux.length());
		qtdAux2 = qtdVagas.substring(0,qtdVagas.indexOf("/"));
		
		qtdAux = String.valueOf(colaboradores.size());
		String qtdVagas2 = qtdVagas.substring(0,qtdVagas.indexOf("/")) + "/"+qtdAux;
		
		field.getForm().getField("qtdVagas").setValue(qtdVagas2);
		NeoObject medicao = field.getForm().getObject();
		EntityWrapper wMedicao = new EntityWrapper(medicao);
		wMedicao.setValue("qtdVagas", qtdVagas2);
		
		if(qtdAux2.equals(qtdAux)){
			wMedicao.setValue("alertas","ok");
			field.getForm().getField("alertas").setValue("ok");
			
		}else{
			wMedicao.setValue("alertas","qtdVagas divergentes;");
			field.getForm().getField("alertas").setValue("qtdVagas divergentes;");
		}
		
		PersistEngine.persist(medicao);
		
		
		System.out.println("contrato:"+contrato);
		String icone = "";
		
		for (int i = 0; i < colaboradores.size(); i++)
		{
			NeoObject colab = colaboradores.get(i);
			EntityWrapper wColab = new EntityWrapper(colab);
			String falta = (String) wColab.findField("descMed").getValue();
			if(!falta.contains("30")){
				sitCtr = "falta";
				break;
			}
		}

		if (sitCtr.equals("ok"))
		{
			icone = "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/certo2.png\" title=\" Nenhum colaborador(es) com falta\"/>";
		}
		else
		{
			icone = "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/exclamacao2.png\"title=\" Existe colaborador(es) com falta\"/>";
		}

		StringBuilder outBuilder = new StringBuilder();
		outBuilder.append(icone);
		*/
		return "";
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
