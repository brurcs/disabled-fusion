package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;

public class CSEIniciaProcessoCSE implements CustomJobAdapter
{

	private final Log log = LogFactory.getLog(RSCIniciaProcessoRSC.class);

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CustomJobContext ctx)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		
		EntityManager entityManager = PersistEngine.getEntityManager("VETORH");
		EntityTransaction transaction = entityManager.getTransaction();
		
		try
		{
			StringBuffer queryCobertura = new StringBuffer();
			queryCobertura.append(" SELECT cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, cob.usu_datalt, cob.usu_horini, cob.usu_codmot, orn.usu_codreg, orn2.NOMLOC as lotacao ");
			queryCobertura.append(" FROM USU_T038COBFUN cob ");
			queryCobertura.append(" INNER JOIN R016ORN orn ON orn.numloc = cob.usu_numloctra and orn.taborg = cob.usu_taborgtra ");
			queryCobertura.append(" INNER JOIN R016ORN orn2 ON orn.numloc = cob.usu_numloc and orn.taborg = cob.usu_taborg ");
			queryCobertura.append(" INNER JOIN R016HIE hie ON hie.taborg = orn.taborg AND orn.numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = orn.taborg AND h2.numloc = orn.numloc)  ");
			queryCobertura.append(" WHERE LEN(hie.CodLoc) = 27 AND orn.taborg = 203 AND cob.usu_codmot in (7,13)  ");
			queryCobertura.append(" AND orn.nomloc like 'serviço extra - ORSEGUPS%' AND cob.usu_datalt = '2013-03-01 14:30:00' ");
			queryCobertura.append(" AND NOT EXISTS (SELECT tcf.usu_profus FROM USU_T038TCF tcf ");
			queryCobertura.append(" 				WHERE tcf.usu_numemp = cob.usu_numemp AND tcf.usu_tipcol = cob.usu_tipcol AND tcf.usu_numcad = cob.usu_numcad AND tcf.usu_datini = cob.usu_datalt AND tcf.usu_horini = cob.usu_horini) ");
			queryCobertura.append(" ORDER BY cob.usu_datalt ");

			PreparedStatement stCobertura = connVetorh.prepareStatement(queryCobertura.toString());
			ResultSet rsCobertura = stCobertura.executeQuery();
			
			Long numemp = 0L;
			Long tipcol = 0L;
			Long numcad = 0L;
			GregorianCalendar gData = new GregorianCalendar();
			Long codreg = 0L;
			String lotacao = "";
			
			while (rsCobertura.next())
			{
				Timestamp data = rsCobertura.getTimestamp("usu_datalt");
				gData.setTimeInMillis(data.getTime());
				
				numemp = rsCobertura.getLong("usu_numemp");
				tipcol = rsCobertura.getLong("usu_tipcol");
				numcad = rsCobertura.getLong("usu_numcad");
				codreg = rsCobertura.getLong("usu_codreg");
				lotacao = rsCobertura.getString("lotacao");
				
				//Colaborador
				QLEqualsFilter filterNumemp = new QLEqualsFilter("numemp", numemp);
				QLEqualsFilter filterTipcol = new QLEqualsFilter("tipcol", tipcol);
				QLEqualsFilter filterNumcad = new QLEqualsFilter("numcad", numcad);
				
				QLGroupFilter filtroColaborador = new QLGroupFilter("AND");
				filtroColaborador.addFilter(filterNumemp);
				filtroColaborador.addFilter(filterTipcol);
				filtroColaborador.addFilter(filterNumcad);
				
				List<NeoObject> listColaborador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), filtroColaborador);
				
				//QLExecucao
				QLEqualsFilter filterColaborador = new QLEqualsFilter("colaborador", listColaborador.get(0));
				QLEqualsFilter filterData = new QLEqualsFilter("data", gData);
				
				QLGroupFilter filtroExecucao = new QLGroupFilter("AND");
				filtroExecucao.addFilter(filterColaborador);
				filtroExecucao.addFilter(filterData);
				
				NeoObject noExecucao = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("QLExecucao"), filtroExecucao);
				
				String nomePapel = "";
				
				switch (codreg.intValue())
				{
					case 1:
//						TAREFA 195926	nomePapel = "Gerente de Segurança";
						nomePapel = "RESPCOMFERSEG";
						break;
					case 2:
						nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
						break;
					case 3:
						nomePapel = "Gerente Escritorio Regional Brusque";
						break;
					case 4:
						nomePapel = "Gerente Escritorio Regional Blumenal";
						break;
					case 5:
						nomePapel = "Gerente Escritorio Regional Joinville";
						break;
					case 6:
						nomePapel = "Gerente Escritorio Regional Lages";
						break;
					case 7:
						nomePapel = "Gerente Escritório Regional CUA";
						break;
					case 8:
						nomePapel = "Gerente Escritorio Regional Gaspar";
						break;
					case 9:
						nomePapel = "Gerente de Asseio Conserv. e Limpeza";
						break;
					case 10:
						nomePapel = "Gerente Escritorio Regional Chapeco";
						break;
					case 11:
						nomePapel = "Gerente Escritório Regional Rio do Sul";
						break;
					case 12:
						nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
						break;
					case 13:
					    if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
					    {
						nomePapel = "Gerente Escritório Regional Curitiba";
					    }else{
						nomePapel = "Gerente Escritório Regional Curitiba Norte";
					    }
						break;
					case 16:
						nomePapel = "Gerente Escritório Regional Tubarão";
						break;

				}
			
				NeoPaper noGerente = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",nomePapel));
				if(noGerente != null && (noGerente.getAllUsers() == null || noGerente.getAllUsers().isEmpty()))
				{
					throw new WorkflowException("Gerente responsável não localizado!");
				}
				
				//CSE
				InstantiableEntityInfo instanceCSE = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByType("CSECoberturaServicoExtra");
				NeoObject noCSE = instanceCSE.createNewInstance();
				NeoUser noSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "sistemaSapiens"));
				
				EntityWrapper wrapperCSE = new EntityWrapper(noCSE);
				wrapperCSE.findField("qlExecucao").setValue(noExecucao);
				wrapperCSE.findField("gerente").setValue(noGerente);
				
				QLEqualsFilter equal = new QLEqualsFilter("Name", "C033 - Cobertura Servico Extra");
				ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
				WFProcess proc = processModel.startProcess(noCSE, true, null, null, null, null, noSolicitante);

				proc.setSaved(true);

				// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
				PersistEngine.persist(proc);
				PersistEngine.commit(true);

				// Finaliza a primeira tarefa
				Task task = null;
				final List acts = proc.getOpenActivities();
				final Activity activity1 = (Activity) acts.get(0);

				if (activity1 instanceof UserActivity)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, noSolicitante);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
				
				StringBuffer insertTarefa = new StringBuffer();
				insertTarefa.append(" INSERT INTO USU_T038TCF (usu_numemp, usu_tipcol, usu_numcad, usu_datini, usu_horini, usu_profus) ");
				insertTarefa.append(" VALUES (?, ?, ?, ?, (DATEPART(HOUR, ?) * 60) + DATEPART(MINUTE, ?), ?) ");
	
				Query queryInsereAfa = entityManager.createNativeQuery(insertTarefa.toString());
				
				queryInsereAfa.setParameter("usu_numemp", numemp);
				queryInsereAfa.setParameter("usu_tipcol", tipcol);
				queryInsereAfa.setParameter("usu_numcad", numcad);
				queryInsereAfa.setParameter("usu_datini", new Timestamp(gData.getTime().getTime()));
				queryInsereAfa.setParameter("usu_horini", new Timestamp(gData.getTime().getTime()));
				queryInsereAfa.setParameter("usu_profus", proc.getNeoId());
				
				queryInsereAfa.executeUpdate();
				transaction.commit();
			}
			
			rsCobertura.close();
			stCobertura.close();
		}
		catch (Exception e)
		{
			if (transaction.isActive())
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
	}
}
