package com.neomind.fusion.custom.orsegups.fap.slip.adapter.caixas;

import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskSummary;
import com.neomind.fusion.workflow.task.rule.TaskRuleAdapter;
import com.neomind.fusion.workflow.task.rule.TaskRuleContext;

public class FAPSlipCaixaPagamentoPresidente implements TaskRuleAdapter
{

	@Override
	public boolean isTriggered(TaskRuleContext context)
	{
		boolean retorno = false;

		Task t = null;
		TaskSummary ts = null;
		try
		{
			if (context.getTask() instanceof Task)
			{
				t = (Task) context.getTask();
			}
			else
			{
				ts = (TaskSummary) context.getTask();
			}
			
			if ((t != null && t.getProcessName().equals("F002 - FAP Slip") && t.getTitle().equals("Aprovar Pagamento")) || 
					(ts != null && ts.getProcessName().equals("F002 - FAP Slip") && ts.getTaskName().equals("Aprovar Pagamento")))
			{
				retorno = true;
			}
		}
		catch (Exception e)
		{
			System.out.println("Erro ao direcionar tarefa de FAP para subpastas. Mensagem: " + e.getMessage());
			e.printStackTrace();
		}
 
		return retorno;
	}
}