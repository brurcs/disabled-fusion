package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class ChipGPRSDefineTipoSolicitacao implements AdapterInterface
{

	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try {
			InstantiableEntityInfo tipoSolicitacao = AdapterUtils.getInstantiableEntityInfo("ChipsGPRSPostoTipoSolicitacao");
			QLEqualsFilter filterTipoSolicitacao = new QLEqualsFilter("codigo", 3L);
			NeoObject noTipoSolicitacao = (NeoObject) PersistEngine.getObject(tipoSolicitacao.getEntityClass(), filterTipoSolicitacao);
			processEntity.setValue("tipoSolicitacao", noTipoSolicitacao);
			processEntity.setValue("regraAutomatica", Boolean.FALSE);
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Erro ao Avançar para Definir o Tipo da Solicitação Com Defeito: CódigoChip"+activity.getCode());
			throw new WorkflowException("Erro ao Avançar para Definir o Tipo da Solicitação Com Defeito ");
		}
	}

}
