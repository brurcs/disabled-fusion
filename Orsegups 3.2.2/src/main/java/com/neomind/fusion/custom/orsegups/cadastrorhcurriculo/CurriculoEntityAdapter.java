package com.neomind.fusion.custom.orsegups.cadastrorhcurriculo;

import java.util.Map;

import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.entity.EntityAdapter;
import com.neomind.fusion.entity.EntityWrapper;

public class CurriculoEntityAdapter implements EntityAdapter
{

	@Override
	public void run(Map<String, Object> params)
	{
		try
		{
			EForm eform = (EForm) params.get(EFORM);	
			
			EntityWrapper wrapper = new EntityWrapper(eform.getObject());
			
			wrapper.setValue("cidadeStr", wrapper.findValue("cidade.nomcid"));
			wrapper.setValue("bairroStr", wrapper.findValue("bairro.nombai"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
