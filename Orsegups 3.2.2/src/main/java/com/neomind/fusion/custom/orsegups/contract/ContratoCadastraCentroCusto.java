package com.neomind.fusion.custom.orsegups.contract;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoUtils;

public class ContratoCadastraCentroCusto implements AdapterInterface{
	
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		//pegar o centro de custo selecionado
		//identificar o nível deste CC
		//criar estrutura abaixo deste CC, até o nível 8
		
		
		
		//SAPIENSE044CCU
		NeoObject centroCustoSelecionado = (NeoObject) processEntity.findValue("postosContrato.centroCusto");
		
		if(NeoUtils.safeIsNull(centroCustoSelecionado))
		{
			//trata objeto nulo
		}
		
		
		
		//identificado o nível deve-se criar os níveis abaixo para todas as arvores
		
		if(criaHierarquiaCentroCusto(centroCustoSelecionado, processEntity))
		{
			
		}
	}
	
	public  boolean criaHierarquiaCentroCusto(NeoObject centroCustoSelecionado, EntityWrapper processEntity)
	{
		long nivelCCSelecionado = 0;
		nivelCCSelecionado = identificaNivelArvore(centroCustoSelecionado);
		
		if(NeoUtils.safeIsNull(nivelCCSelecionado))
		{
			//trata objeto nulo
		}
		return true;
	}
	
	public long identificaNivelArvore(NeoObject centroCustoSelecionado)
	{
		long nivelAlvo = 0;
		//identifica o nível deste CC
		
		return nivelAlvo;
	}

	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
