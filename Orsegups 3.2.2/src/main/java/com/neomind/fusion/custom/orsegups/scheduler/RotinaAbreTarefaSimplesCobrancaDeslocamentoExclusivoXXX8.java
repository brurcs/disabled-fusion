package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesCobrancaDeslocamentoExclusivoXXX8 implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesCobrancaDeslocamentoExclusivo.class);

    @Override
    public void execute(CustomJobContext arg0) {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Connection conn = null;
	StringBuilder sql = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;
	log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	
	try {
		
		sql.append("select C.ID_CENTRAL, ");
		sql.append("     		C.PARTICAO, ");
		sql.append("     		C.RAZAO, ");
		sql.append("     		C.FANTASIA, ");
		sql.append("     		C.CD_CLIENTE, ");
		sql.append("     		H.TX_OBSERVACAO_FECHAMENTO, ");
		sql.append("     		H.DT_RECEBIDO, ");
		sql.append("            H.CD_EVENTO, ");
		sql.append("            REPLACE(REPLACE(REPLACE(REPLACE(hfe.NM_FRASE_EVENTO, 'NÃO LIGAR', ''), 'ROUBO', ''), '()', ''), '  ', ' ') as NM_FRASE_EVENTO, ");
		sql.append("            h.CD_HISTORICO ");
		sql.append("       from [FSOODB03\\SQL01].SIGMA90.dbo.VIEW_HISTORICO h ");
		sql.append(" INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
		sql.append(" INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.GRUPO_CLIENTE G ON C.CD_GRUPO_CLIENTE = G.CD_GRUPO_CLIENTE ");
		sql.append(" INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.HISTORICO_FRASE_EVENTO hfe with (nolock) ON hfe.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
		sql.append("      where h.cd_evento in ('XXX8', 'XDSL') ");
		sql.append("        and h.dt_viatura_no_local > DATEADD(DAY,-1,GETDATE()) ");
		sql.append("        and G.NM_DESCRICAO like '%CORP-RAIA DROGASIL%'");
	    sql.append(" AND NOT EXISTS (SELECT 1 FROM [FSOODB04\\SQL02].TIDB.DBO.HISTORICO_TAREFAS_COBRANCA_DESLOCAMENTO_XXX8 HT WHERE HT.HISTORICO = H.CD_HISTORICO) ");	    
	    
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();
	    
	    while (rs.next()) {
		String solicitante = OrsegupsUtils.getUserNeoPaper("solicitanteCobrancaDeslocamentoExclusivo");
		if (solicitante == null){
		    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Regra: solicitante não encontrado");
		    continue;
		}
		String executor = OrsegupsUtils.getUserNeoPaper("executorCobrancaDeslocamentoExclusivo");
		if (executor == null){
		    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Regra: executor não encontrado");
		    continue;
		}
		String tituloAux = "Deslocamento Tático por XXX8 Solicitado pelo Cliente Raia Drogasil - "+rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]% ";

		String titulo = "Deslocamento Tático por XXX8 Solicitado pelo Cliente Raia Drogasil - "+rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] "+rs.getString("FANTASIA")+"("+rs.getString("RAZAO")+")";

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
		descricao += " <strong>Conta :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
		descricao = descricao + " <strong>Razão Social :</strong> " + rs.getString("RAZAO") + " <br>";
		descricao = descricao + " <strong>Nome Fantasia :</strong> " + rs.getString("FANTASIA") + " <br>";
		descricao = descricao + " <strong>Evento :</strong> " + rs.getString("CD_EVENTO") + " - " + rs.getString("NM_FRASE_EVENTO") + " <br>";
		Date dtRecebido = rs.getDate("DT_RECEBIDO");
		String dataRecebido = NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy");
		String horaRecebido = rs.getString("DT_RECEBIDO").substring(11, 16);
		descricao = descricao + " <strong>Recebido :</strong> " + dataRecebido+" "+horaRecebido + " <br>";
		descricao = descricao + " <strong>Log evento :</strong> <br>" + rs.getString("TX_OBSERVACAO_FECHAMENTO") + "<br>";

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		
		if (tarefa != null && !tarefa.isEmpty() && !tarefa.contains("Erro")){
		    gravarHistoricoTarefa(rs.getLong("CD_HISTORICO"), tarefa);
		}
		
		log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Responsavel : " + executor + " Tarefa: " + tarefa);
		System.out.println("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Responsavel : " + executor + " Tarefa: " + tarefa);

	    }

	} catch (Exception e) {
	    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - ERRO AO GERAR TAREFA");
	    System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - ERRO AO GERAR TAREFA");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {

	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - ERRO DE EXECUÇÃO");
		e.printStackTrace();
	    }

	    log.warn("##### FINALIZAR  AGENDADOR DE TAREFA:Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Data: "
		    + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}

    }
    
    private void gravarHistoricoTarefa(Long historico, String tarefa){
	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("TIDB");
	    
	    sql.append(" INSERT INTO HISTORICO_TAREFAS_COBRANCA_DESLOCAMENTO_XXX8 (HISTORICO, TAREFA) VALUES (?,?) ");
	    
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setLong(1, historico);
	    pstm.setLong(2, new Long(tarefa));
	    
	    pstm.executeUpdate();

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
    }
    
}
