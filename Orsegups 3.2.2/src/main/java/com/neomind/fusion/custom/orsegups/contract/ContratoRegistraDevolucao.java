package com.neomind.fusion.custom.orsegups.contract;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoRegistraDevolucao implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		try{
			boolean aprovadoAnalise = NeoUtils.safeBoolean(wrapper.findValue("aprovadoAnalise"));
			boolean necessarioAprovacaoValores = NeoUtils.safeBoolean(wrapper.findValue("necessarioAprovacaoValores"));
			
			
			
			NeoObject tramite = AdapterUtils.createNewEntityInstance("FGChistoricoTramite");
			EntityWrapper wTramite = new EntityWrapper(tramite);
			wTramite.setValue("usuario", PortalUtil.getCurrentUser().getCode() );
			wTramite.setValue("obs", NeoUtils.safeOutputString(wrapper.findValue("motivo")) );
			
			if (!aprovadoAnalise && necessarioAprovacaoValores){
				wTramite.setValue("acao", "Processo reprovado pela análise com necessidade de aprovação de valores." );
			}else if (!aprovadoAnalise && !necessarioAprovacaoValores){
				wTramite.setValue("acao", "Processo reprovado pela análise sem necessidade de aprovação de valores." );
			}else{
				wTramite.setValue("acao", "Reprovou aprovado." );
			}
			
			PersistEngine.persist(tramite);
			
			wrapper.findField("listaTramites").addValue(tramite);
			wrapper.setValue("motivo","");
			wrapper.setValue("obsTramite","");
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Erro devolver contrato. msg: "+e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}

	

}
