package com.neomind.fusion.custom.orsegups.fap.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsGroupLevels;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;



public class FAPEscalarAtividadeOrcamentoReprovado implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {

		gravarHistorico(origin, processEntity);
		escalarAtividade(processEntity);

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {	
	}



	public void gravarHistorico(Task origin, EntityWrapper processEntity) {

		InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("FAPRegistroAtividade");
		NeoObject registro = registroAtividade.createNewInstance();
		EntityWrapper wRegistro = new EntityWrapper(registro);

		wRegistro.findField("responsavel").setValue(origin.getUser());

		wRegistro.findField("dataInicial").setValue(origin.getStartDate());
		wRegistro.findField("dataFinal").setValue(origin.getFinishDate());

		wRegistro.findField("atividade").setValue("Aprovou o FAP");
		wRegistro.findField("descricao").setValue(origin.getUser().getFullName() + " não realizou a atividade de Revisão do Orçamento.");

		PersistEngine.persist(registro);
		processEntity.findField("registroAtividades").addValue(registro);
	}


	public void escalarAtividade(EntityWrapper processEntity) {

		NeoUser executorAtual = processEntity.findGenericValue("solicitanteOrcamento");
		NeoPaper responsavelGrupoExecutorAtual = executorAtual.getGroup().getResponsible();

		NeoUser novoExecutor = null;


		if (executorAtual.getPapers().contains(responsavelGrupoExecutorAtual) 
				&& !responsavelGrupoExecutorAtual.getName().contains("Presidente")) {     
			novoExecutor = executorAtual.getGroup().getUpperLevel().getResponsible().getAllUsers().iterator().next();	
		} else {
			novoExecutor = executorAtual.getGroup().getResponsible().getAllUsers().iterator().next();
		}

		processEntity.setValue("solicitanteOrcamento", novoExecutor);

	}




}
