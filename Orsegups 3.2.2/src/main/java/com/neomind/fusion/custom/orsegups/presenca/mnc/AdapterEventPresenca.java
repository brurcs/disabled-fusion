package com.neomind.fusion.custom.orsegups.presenca.mnc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class AdapterEventPresenca implements TaskFinishEventListener {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(AdapterEventPresenca.class);

    @Override
    public void onFinish(TaskFinishEvent event) throws TaskException {
	// TODO Auto-generated method stub

	try {
	    String tarefa = event.getActivity().getActivityName();

	    NeoUser responsavelEscalada = null;
	    if (tarefa != null) {
		if (tarefa.equals("Conferir Cobertura")) {
		    responsavelEscalada = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "beatriz.malmann"));
		} else {
		    NeoPaper responsavel = null;
		    if (tarefa.equals("Realizar Cálculo")) {
			responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[MNC] RH Calculo Cobertura"));
		    } else if (tarefa.equals("Realizar Movimento Numerário")) {
			responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[MNC] Financeiro Pagamento"));
		    }
		    if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty()) {
			for (NeoUser user : responsavel.getUsers()) {
			    responsavelEscalada = user;
			    break;
			}
		    }
		}
	    }
	    EntityWrapper wEventoAtividade = event.getWrapper();
	    wEventoAtividade.findField("responsavelEscalada").setValue(responsavelEscalada);
	} catch (Exception e) {
	    log.warn("ERRO NO FORMULÁRIO ao finalizar");
	    e.printStackTrace();
	    throw (WorkflowException) e;
	}

    }

}
