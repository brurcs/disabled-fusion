/**
 * ModeloPlanoGerarContasIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano;

public class ModeloPlanoGerarContasIn  implements java.io.Serializable {
    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasInGridCta[] gridCta;

    public ModeloPlanoGerarContasIn() {
    }

    public ModeloPlanoGerarContasIn(
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasInGridCta[] gridCta) {
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.gridCta = gridCta;
    }


    /**
     * Gets the flowInstanceID value for this ModeloPlanoGerarContasIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this ModeloPlanoGerarContasIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this ModeloPlanoGerarContasIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this ModeloPlanoGerarContasIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the gridCta value for this ModeloPlanoGerarContasIn.
     * 
     * @return gridCta
     */
    public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasInGridCta[] getGridCta() {
        return gridCta;
    }


    /**
     * Sets the gridCta value for this ModeloPlanoGerarContasIn.
     * 
     * @param gridCta
     */
    public void setGridCta(com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasInGridCta[] gridCta) {
        this.gridCta = gridCta;
    }

    public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasInGridCta getGridCta(int i) {
        return this.gridCta[i];
    }

    public void setGridCta(int i, com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.ModeloPlanoGerarContasInGridCta _value) {
        this.gridCta[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModeloPlanoGerarContasIn)) return false;
        ModeloPlanoGerarContasIn other = (ModeloPlanoGerarContasIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.gridCta==null && other.getGridCta()==null) || 
             (this.gridCta!=null &&
              java.util.Arrays.equals(this.gridCta, other.getGridCta())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getGridCta() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGridCta());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGridCta(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModeloPlanoGerarContasIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "modeloPlanoGerarContasIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gridCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gridCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "modeloPlanoGerarContasInGridCta"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
