package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class DefineGerenteRSCCancelamento implements AdapterInterface {
	
	private static final Log log = LogFactory.getLog(DefineGerenteRSCCancelamento.class);
	private static String mensagemErro = "Erro na execução do Adapter de Eventos Define Gerente RSC";
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	
		System.out.println(" ###### INICIO DO ADAPTER "+this.getClass().getSimpleName()+" "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		
		String siglaRegional = null;
		NeoUser gerente = null;
		
		try {
			
			siglaRegional = processEntity.findGenericValue("rscRelatorioSolicitacaoCliente.regional.siglaRegional");
			
			gerente = retornaGerenteRegional(siglaRegional);
			
			processEntity.findField("gerenteRegional").setValue(gerente);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" ###### ERRO NO ADAPTER "+this.getClass().getSimpleName()+" "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			throw new WorkflowException(mensagemErro);
		} finally {
			System.out.println(" ###### FIM DO ADAPTER "+this.getClass().getSimpleName()+" "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		
		
		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

	public NeoUser retornaGerenteRegional(String siglaRegional) {
		
		NeoPaper papelGerente = null;
		NeoUser gerente = null;
		
		try {
			
			papelGerente = OrsegupsUtils.getPaper("GerenteRSCCancelamento"+siglaRegional);
			
			gerente = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelGerente);
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "ERRO ao capturar o gerente responsável pela Regional "+siglaRegional;
			throw new WorkflowException(mensagemErro);
		}
		
		return gerente;
		
	}
}
