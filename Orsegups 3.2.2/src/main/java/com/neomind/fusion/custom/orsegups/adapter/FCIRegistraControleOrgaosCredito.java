package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCIRegistraControleOrgaosCredito implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(FCIInsereObservacaoTituloSapiens.class);


	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Long ocorrencia = 1L; // SPC
		String tipoCliente =  (String)processEntity.findField("contratoSapiens.tipcli").getValue();
		if ("J".equals(tipoCliente))
		{
			ocorrencia = 2L; // SERASA
		}
		List<NeoObject> listaTitulos = (List<NeoObject>) processEntity.findField("listaTitulos").getValue();
		
		if (listaTitulos != null && !listaTitulos.isEmpty()) {
			
			for (NeoObject titulo : listaTitulos) {
				EntityWrapper wrapper = new EntityWrapper(titulo);
				if (wrapper != null) {
					Long codEmp = (Long)wrapper.findValue("empresa");
					Long codFil = (Long)wrapper.findValue("filial");
					String numTit = (String)wrapper.findValue("numeroTitulo");
					String codTpt = (String)wrapper.findValue("tipoTitulo");
					//Boolean 
					// Verifica se jah estah registrado
					if (!tituloJaRegistrado(codEmp, codFil, numTit, codTpt))
					{
						GregorianCalendar data = new GregorianCalendar( (new GregorianCalendar()).get(GregorianCalendar.YEAR), (new GregorianCalendar()).get(GregorianCalendar.MONTH), (new GregorianCalendar()).get(GregorianCalendar.DAY_OF_MONTH) );
						String texto = "Fusion: Inserido o título no orgão de crédito pela Tarefa FCI: " + activity.getProcess().getCode();
						Long codigoUsuarioSapiens = buscaUsuarioSapiensCorrespondente(((NeoUser)processEntity.findField("usuario13").getValue()).getCode());
						
						try	{
							this.insereRegistroOrgaoCreditoSapiens(codEmp, codFil, numTit, codTpt, data, texto, codigoUsuarioSapiens, ocorrencia);
							this.insereObservacaoTituloSapiens(codEmp, codFil, numTit, codTpt, data, 0L, texto, codigoUsuarioSapiens, ocorrencia);

						} catch (WorkflowException we)	{
							throw we;
						} catch (Exception e) {
							e.printStackTrace();
							throw new WorkflowException(e.getMessage());
						}						
						
					}
					
				}
			}
		}
			
	}


	public void insereRegistroOrgaoCreditoSapiens(Long codEmp, Long codFil, String numTit, String codTpt, GregorianCalendar data, String texto, Long usuario, Long ocorrencia) throws Exception
	{
		
			String nomeFonteDados = "SAPIENS";
			
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			String sql = "INSERT INTO USU_T301OTI (usu_codemp, usu_codfil, usu_numtit, usu_codtpt, usu_codocr, usu_datins, usu_datrem, usu_obsocr, usu_usuins, usu_usurem) ";
			sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			PreparedStatement st = null;
			try
			{	
				st = connection.prepareStatement(sql);
				st.setLong(1, codEmp);
				st.setLong(2, codFil);
				st.setString(3, numTit);
				st.setString(4, codTpt);
				st.setLong(5, ocorrencia);
				st.setTimestamp(6, new Timestamp(data.getTimeInMillis()));
				st.setTimestamp(7, new Timestamp((new GregorianCalendar(1900, GregorianCalendar.DECEMBER, 31)).getTimeInMillis()) );
				st.setString(8, texto);
				st.setLong(9, usuario);
				st.setLong(10, 0L);
				
				st.executeUpdate();
			}
			catch (SQLException e)
			{
				throw e;
			}
			finally
			{
				if (st != null)
				{
					try
					{
						st.close();
						connection.close();
					}
					catch (SQLException e)
					{
						log.error("Erro ao fechar o statement");
						e.printStackTrace();
					}
				}
			}

	} 

	@SuppressWarnings("unchecked")
	public void insereObservacaoTituloSapiens(Long codEmp, Long codFil, String numTit, String codTpt, GregorianCalendar data, Long hora, String texto, Long usuario, Long ocorrencia) throws Exception
	{

			Integer sequencial = 1;
			
			String nomeFonteDados = "SAPIENS";
			
			String sqlSelect = "SELECT COUNT(*)+1 FROM E301MOR WHERE codemp = :codemp AND codfil = :codfil AND numtit = :numtit AND codtpt = :codtpt ";
			
			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sqlSelect);
			query.setParameter("codemp", codEmp);
			query.setParameter("codfil", codFil);
			query.setParameter("numtit", numTit);
			query.setParameter("codtpt", codTpt);
			List<Object> resultList = query.getResultList();
			
			if(resultList != null && !resultList.isEmpty())	{
				sequencial = (Integer)resultList.get(0);
			}	
			
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			
			String sql = "INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov, USU_CodOcr) ";
			sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			PreparedStatement st = null;
			try
			{	
				st = connection.prepareStatement(sql);
				st.setLong(1, codEmp);
				st.setLong(2, codFil);
				st.setString(3, numTit);
				st.setString(4, codTpt);
				st.setInt(5, sequencial);
				st.setString(6, "M");
				st.setString(7, texto);
				st.setLong(8, usuario);
				st.setTimestamp(9, new Timestamp(data.getTimeInMillis()));
				st.setLong(10, hora);
				st.setLong(11, ocorrencia);
				
				st.executeUpdate();
			}
			catch (SQLException e)
			{
				throw e;
			}
			finally
			{
				if (st != null)
				{
					try
					{
						st.close();
						connection.close();
					}
					catch (SQLException e)
					{
						log.error("Erro ao fechar o statement");
						e.printStackTrace();
					}
				}
			}

	} 
	
	private Boolean tituloJaRegistrado(Long codEmp, Long codFil, String numTit, String codTpt)
	{
		Boolean tituloRegistrado = false;
		
		QLGroupFilter filterRegistro = new QLGroupFilter("AND");
		filterRegistro.addFilter(new QLEqualsFilter("usu_codemp", codEmp));
		filterRegistro.addFilter(new QLEqualsFilter("usu_codfil", codFil));
		filterRegistro.addFilter(new QLEqualsFilter("usu_numtit", numTit));
		filterRegistro.addFilter(new QLEqualsFilter("usu_codtpt", codTpt));
		QLGroupFilter filterRegistroSemRemocao = new QLGroupFilter("OR");
		GregorianCalendar gc = new GregorianCalendar(1900, GregorianCalendar.DECEMBER, 31);
		filterRegistroSemRemocao.addFilter(new QLEqualsFilter("usu_datrem", gc));
		filterRegistroSemRemocao.addFilter(new QLFilterIsNull("usu_datrem"));
		filterRegistro.addFilter(filterRegistroSemRemocao);
         
		List<NeoObject> listaRegistro = (List<NeoObject>)PersistEngine.getObjects(((ExternalEntityInfo)EntityRegister.getInstance().getCache().getByType("SAPIENSUSUTOTI")).getEntityClass(), filterRegistro); 
		if (listaRegistro != null && (!listaRegistro.isEmpty()))
		{
			tituloRegistrado = true;
		}
		
		return tituloRegistrado;
	}


	private Long buscaUsuarioSapiensCorrespondente(String userCode)
	{
		//Logica para buscar codigo do usuario do sapiens referente ao usuario fusion
		Long sapiensUserCode = 0L;
		NeoObject sapiensUser = null;
		InstantiableEntityInfo ieiEX = AdapterUtils.getInstantiableEntityInfo("SAPIENSUSUARIO");
		String login = userCode;

		QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", login);
		@SuppressWarnings("unchecked")
		ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>) PersistEngine.getObjects(ieiEX.getEntityClass(), loginFilter);

		if (sapiensUsers != null && sapiensUsers.size() > 0)
		{
			sapiensUser = sapiensUsers.get(0);
		}

		if (sapiensUser != null)
		{
			EntityWrapper sapiensUserEw = new EntityWrapper(sapiensUser);
			sapiensUserCode = (Long)sapiensUserEw.findValue("codusu");
		}

		if (sapiensUserCode == null || sapiensUserCode == 0L)
		{
			log.error("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion.");
			throw new WorkflowException("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion.");
		}

		return sapiensUserCode;
	}
	
}
