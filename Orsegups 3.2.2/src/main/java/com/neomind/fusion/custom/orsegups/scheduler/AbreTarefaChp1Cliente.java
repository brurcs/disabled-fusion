package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaChp1Cliente implements CustomJobAdapter
{
    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(AbreTarefaOperacaoEvento.class);

    @Override
    public void execute(CustomJobContext arg0)
    {	
	Connection conn = PersistEngine.getConnection("SIGMA90"); 
	
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaOperacaoEvento");
	log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Operacao Evento - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Operacao Evento - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis(); 

	try
	{
	    StringBuffer  varname1 = new StringBuffer();
	    varname1.append("SELECT h.CD_HISTORICO, ");
	    varname1.append("	           h.CD_EVENTO, ");
	    varname1.append("	           h.CD_CLIENTE, ");
	    varname1.append("	           c.ID_EMPRESA, ");
	    varname1.append("	           c.ID_CENTRAL, ");
	    varname1.append("	           c.PARTICAO, ");
	    varname1.append("	           c.RAZAO, ");
	    varname1.append("	           C.FANTASIA, ");
	    varname1.append("	           h.TX_OBSERVACAO_FECHAMENTO, ");
	    varname1.append("	           (c.ENDERECO+', '+bai.nome+', '+cid.nome+', '+cid.id_estado) AS ENDERECO, ");
	    varname1.append("	           v.NM_VIATURA ");
	    varname1.append("	    FROM   [FSOODB03\\SQL01].SIGMA90.DBO.view_historico h ");
	    varname1.append("	           INNER JOIN [FSOODB03\\SQL01].SIGMA90.DBO.dbcentral c ");
	    varname1.append("	                   ON c.cd_cliente = h.cd_cliente ");
	    varname1.append("	           LEFT OUTER JOIN [FSOODB03\\SQL01].SIGMA90.DBO.viatura v ");
	    varname1.append("	                        ON v.cd_viatura = h.cd_viatura ");
	    varname1.append("	           LEFT OUTER JOIN [FSOODB03\\SQL01].SIGMA90.DBO.dbcidade cid ");
	    varname1.append("	                        ON cid.id_cidade = c.id_cidade ");
	    varname1.append("	           LEFT OUTER JOIN [FSOODB03\\SQL01].SIGMA90.DBO.dbbairro bai ");
	    varname1.append("	                        ON bai.id_cidade = c.id_cidade ");
	    varname1.append("	                            AND bai.id_bairro = c.id_bairro ");
	    varname1.append("	     WHERE h.fg_status = 4 ");
	    varname1.append("	       AND h.cd_evento in ('CHP1','CHP2','CHP3')");
	    varname1.append("	       AND CONVERT(DATE, h.DT_FECHAMENTO) > CONVERT(DATE, DATEADD(DAY, -24, GETDATE())) "); // TODO Alterar para 1 dia quando colocado definitivamente e PRODUCAO	    

	    pstm = conn.prepareStatement(varname1.toString());
	    rs = pstm.executeQuery();

	    while (rs.next())
	    {
		Long cdHistorico = rs.getLong("CD_HISTORICO"); 
		Long empresaConta = rs.getLong("ID_EMPRESA");
		Long cdCliente = rs.getLong("CD_CLIENTE");
		String cdEvento = rs.getString("CD_EVENTO");
		String idCentral = rs.getString("ID_CENTRAL");
		String particao = rs.getString("PARTICAO");
		String razaoSocial = rs.getString("RAZAO");
		String fantasia = rs.getString("FANTASIA");
		String endereco = rs.getString("ENDERECO");
		String viatura = rs.getString("NM_VIATURA");
		String logViatura = rs.getString("TX_OBSERVACAO_FECHAMENTO");
		if (logViatura == null) {
		    logViatura = "";
		}

		// Abrir Tarefa
		String userSolicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteChp1Cliente"); 
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userSolicitante));
		
		//NeoPaper papelSolicitante = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteChp1Cliente"));
		
		NeoObject wkfOCP = AdapterUtils.createNewEntityInstance("Chp1Cliente");
		final EntityWrapper ewWkfOCP= new EntityWrapper(wkfOCP);

		ewWkfOCP.findField("cdHistorico").setValue(cdHistorico);
		ewWkfOCP.findField("contaSIGMA").setValue(idCentral);
		ewWkfOCP.findField("empresaConta").setValue(empresaConta);
		ewWkfOCP.findField("particao").setValue(particao);
		ewWkfOCP.findField("razaoSocial").setValue(razaoSocial);
		ewWkfOCP.findField("endereco").setValue(endereco);
		ewWkfOCP.findField("viatura").setValue(viatura);
		ewWkfOCP.findField("solicitante").setValue(solicitante);
		ewWkfOCP.findField("cdCliente").setValue(cdCliente);
		//ewWkfOCP.findField("solicitante2").setValue(papelSolicitante);
		
		int index = logViatura.indexOf("#");
		String resultadoVisita = "";
		if (index > 0) {
		    resultadoVisita = logViatura.substring(0, index);					
		} else {
		    resultadoVisita = logViatura.toString();					
		}
		Boolean visitaOK = Boolean.TRUE;
		if (resultadoVisita.contains("PENDENTE")) {
		    ewWkfOCP.findField("eventoAGerar").setValue(cdEvento.substring(0,cdEvento.length()-1)+"3");
		} else {
		    ewWkfOCP.findField("eventoAGerar").setValue(cdEvento.substring(0,cdEvento.length()-1)+"2");
		}
		ewWkfOCP.findField("visitaOK").setValue(visitaOK);
		//ewWkfOCP.findField("resultadoVisita").setValue(resultadoVisita);
		
		PersistEngine.persist(wkfOCP);
		
		ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "O040-OperacaoChp1Cliente"));
		
		OrsegupsWorkflowHelper.iniciaProcesso(pm, wkfOCP, true, solicitante, true, null);

		//OrsegupsUtils.iniciaWorkflow(wkfOCP, "O040-OperacaoChp1Cliente", solicitante, true, true);

	    }
	} catch (Exception e)
	{

	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Evento");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally
	{

	    try
	    {
		rs.close();
		pstm.close();
		conn.close();
	    }
	    catch (SQLException e)
	    {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }


	    log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Operacao Evento - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    System.out.println("##### FIM AGENDADOR DE TAREFA: Abrir Operacao Tarefa Evento - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }
}
