package com.neomind.fusion.custom.orsegups.fap.slip.dto;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

public class HistoricoOrcamentoDTO implements Comparable<HistoricoOrcamentoDTO>
{
	private Long codContrato;
	private GregorianCalendar data;
	private GregorianCalendar fimVigencia;
	private GregorianCalendar vencimento;
	private String transacao;
	private String descricao;
	private String tarefa;
	private BigDecimal valor;
	private BigDecimal saldo;
	
	
	public Long getCodContrato()
	{
		return codContrato;
	}
	public void setCodContrato(Long codContrato)
	{
		this.codContrato = codContrato;
	}
	public GregorianCalendar getData()
	{
		return data;
	}
	public void setData(GregorianCalendar data)
	{
		this.data = data;
	}
	public GregorianCalendar getFimVigencia()
	{
		return fimVigencia;
	}
	public void setFimVigencia(GregorianCalendar fimVigencia)
	{
		this.fimVigencia = fimVigencia;
	}
	public String getTransacao()
	{
		return transacao;
	}
	public void setTransacao(String transacao)
	{
		this.transacao = transacao;
	}
	public String getDescricao()
	{
		return descricao;
	}
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	public String getTarefa()
	{
		return tarefa;
	}
	public void setTarefa(String tarefa)
	{
		this.tarefa = tarefa;
	}
	public BigDecimal getValor()
	{
		return valor;
	}
	public void setValor(BigDecimal valor)
	{
		this.valor = valor;
	}
	public BigDecimal getSaldo()
	{
		return saldo;
	}
	public void setSaldo(BigDecimal saldo)
	{
		this.saldo = saldo;
	}
	@Override
	public int compareTo(HistoricoOrcamentoDTO o)
	{
		if (getData() == null || o.getData() == null) {
		      return 0;
		    }
		return getData().compareTo(o.getData());
	}
	public GregorianCalendar getVencimento()
	{
		return vencimento;
	}
	public void setVencimento(GregorianCalendar vencimento)
	{
		this.vencimento = vencimento;
	}
}
