package com.neomind.fusion.custom.orsegups.utils;

public class FornecedorVO
{
	private Long codigoFornecedor;
	private Long dataCorte;
	
	public Long getCodigoFornecedor()
	{
		return codigoFornecedor;
	}
	public void setCodigoFornecedor(Long codigoFornecedor)
	{
		this.codigoFornecedor = codigoFornecedor;
	}
	public Long getDataCorte()
	{
		return dataCorte;
	}
	public void setDataCorte(Long dataCorte)
	{
		this.dataCorte = dataCorte;
	}
		
	
}

