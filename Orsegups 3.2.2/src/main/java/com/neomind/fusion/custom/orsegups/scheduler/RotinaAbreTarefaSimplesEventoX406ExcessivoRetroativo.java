package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoUtils;

public class RotinaAbreTarefaSimplesEventoX406ExcessivoRetroativo implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesEventoX406ExcessivoRetroativo.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		List<String> regionais = new ArrayList<String>();
		regionais = OrsegupsUtils.listaRegionaisSigla();
		for (int i = 0; i < regionais.size(); i++)
		{
			String reg = regionais.get(i);
			Connection conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sqlEventoExcessivo = new StringBuilder();
			PreparedStatement pstm = null;
			ResultSet rs = null;
			final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaSimplesEventoX406Excessivo");
			log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - Regional: " + reg + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

			try
			{
				//Eventos	Excessivos	
				sqlEventoExcessivo.append(" SELECT TOP 20 h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3) AS sigla_regional, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, COUNT(*) AS QtdDsl, GETDATE()-60 as PrimeiraData,GETDATE() as UltimaData ");
				sqlEventoExcessivo.append(" FROM HISTORICO_DESARME h WITH (NOLOCK) ");
				sqlEventoExcessivo.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
				sqlEventoExcessivo.append(" INNER JOIN ROTA r WITH (NOLOCK) ON  r.CD_ROTA = c.ID_ROTA ");
				sqlEventoExcessivo.append(" WHERE h.DT_RECEBIDO BETWEEN GETDATE()-60 AND GETDATE() ");
				sqlEventoExcessivo.append(" AND h.CD_EVENTO = 'X406' ");
				sqlEventoExcessivo.append(" AND h.CD_CLIENTE NOT IN (SELECT DISTINCT CD_CLIENTE FROM PROTOCOLO_CENTRAL WHERE CD_EVENTO IN ('E130', 'E131', 'E132', 'E133', 'E134') AND CD_CODE = 'ALN') ");
				sqlEventoExcessivo.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) = ? ");
				sqlEventoExcessivo.append(" GROUP BY h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3), c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO ");
				sqlEventoExcessivo.append(" HAVING COUNT(*) >= 10 ");
				sqlEventoExcessivo.append(" ORDER BY 2, 7 DESC ");

				pstm = conn.prepareStatement(sqlEventoExcessivo.toString());
				pstm.setString(1, reg);
				rs = pstm.executeQuery();
				String executor = "";
				int contador = 0;
				while (rs.next())
				{
					String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteEventoX460ExcessivoRetroativo"); 
					String titulo = "Tratar Eventos X406 em Excesso - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] ";
					titulo = URLEncoder.encode(titulo, "ISO-8859-1");
					GregorianCalendar prazo = new GregorianCalendar();
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 5L);
					prazo.set(Calendar.HOUR_OF_DAY, 23);
					prazo.set(Calendar.MINUTE, 59);
					prazo.set(Calendar.SECOND, 59);

					NeoPaper papel = new NeoPaper();
					Long codReg = OrsegupsUtils.getCodigoRegional(rs.getString("sigla_regional"));

					if (NeoUtils.safeIsNotNull(codReg) && codReg != 13L && codReg != 9L && codReg != 1L)
						papel = OrsegupsUtils.getPapelCoordenadorRegional(codReg, null);
					else if (NeoUtils.safeIsNotNull(codReg) && codReg == 1L)
						papel = OrsegupsUtils.getPaper("Coordenador de Vigilância Eletrônica");
					else if (NeoUtils.safeIsNotNull(codReg) && codReg == 13L)
						papel = OrsegupsUtils.getPaper("Coordenador Técnico CTA");

					if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
					{
						for (NeoUser user : papel.getUsers())
						{
							executor = user.getCode();
							break;
						}
					}
					else
					{
						log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - Regra: Gerente responsável não encontrado - Papel " + papel.getName());
						continue;
					}

					String descricao = "";
					descricao = " <strong>Conta :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
					descricao = descricao + " <strong>Razão Social :</strong> " + rs.getString("RAZAO") + " <br>";
					descricao = descricao + " <strong>Nome Fantasia :</strong> " + rs.getString("FANTASIA") + " <br>";
					descricao = descricao + " <strong>Per&#237odo :</strong> " + NeoUtils.safeDateFormat(rs.getDate("PrimeiraData"), "dd/MM/yyyy") + "<strong>  &#224 </strong>" + NeoUtils.safeDateFormat(rs.getDate("UltimaData"), "dd/MM/yyyy") + "<br>";
					descricao = descricao + " <strong>Eventos :</strong> " + rs.getString("QtdDsl") + "<br>";

					descricao = URLEncoder.encode(descricao, "ISO-8859-1");

					String urlTarefa = "";
					urlTarefa = OrsegupsUtils.URL_PRODUCAO + "/fusion/custom/jsp/orsegups/iniciarTarefaSimples.jsp?avanca=sim&solicitante=" + solicitante + "&executor=" + executor + "&origem=1&descricao=" + descricao + "&titulo=" + titulo + "&prazo=" + NeoUtils.safeDateFormat(prazo, "dd/MM/yyyy");

					URL url = new URL(urlTarefa);
					URLConnection uc = url.openConnection();
					InputStream is = uc.getInputStream();

					contador++;
				}
				log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - Responsavel : " + executor + " Total de tarefas : " + contador + " Regional: " + reg);
				contador = 0;
			}
			catch (Exception e)
			{
				log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - ERRO AO GERAR TAREFA - Regional: " + reg);
				e.printStackTrace();
			}
			finally
			{

				try
				{
					OrsegupsUtils.closeConnection(conn, pstm, rs);
				}
				catch (Exception e)
				{
					log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos ou Ordens de Serviço em Excesso - ERRO DE EXECUÇÃO - Regional: " + reg);
					e.printStackTrace();
				}

				log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos e Ordens de Serviço em Excesso - Regional: " + reg + " Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
		}
	}
}
