package com.neomind.fusion.custom.orsegups.avaliacaoterceirizado;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class AvaliacaoTerceirizadoJOB implements CustomJobAdapter {

	@Override
	public void execute(CustomJobContext ctx) {
		System.out.println("[C037] - Iniciando JOB de Avaliação de Terceirizado" );
		
		int cont = 0;
		try{
			
			
			String queryTerceirizados = " select ord.ID_INSTALADOR, CD_CLIENTE, CONVERT(VARCHAR(12),ord.FECHAMENTO, 12) as Dia "+
					" from dbORDEM ord "+
					" where ord.ID_INSTALADOR in (select CD_COLABORADOR from COLABORADOR where 1=1 and cd_colaborador not in (105267,11139,105194,105341,107587,105391,105267,105332,105333,105334,105335,105336,105337,105338,105339,105340,105341,105342,105377,11189) and NM_COLABORADOR like '%SOO - TERC -%'  and FG_ATIVO_COLABORADOR = 1 and FG_INSTALADOR =1 ) "+
					" and cast (ord.FECHAMENTO as date) > cast (CURRENT_TIMESTAMP-1 as Date) "+
					" and ord.FECHADO = 1 "+
					" and ord.IDOSSOLUCAO <> 249 "+
					" group by ord.ID_INSTALADOR, CD_CLIENTE, CONVERT(VARCHAR(12),ord.FECHAMENTO, 12) "+
					" order by 2 desc ";
			
			 				
			/*NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
			NeoUser responsavel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );*/ 
			 
			NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "joel.medeiros") );
			//NeoUser responsavel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "maxlean.oliveira") );
			NeoUser responsavel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "carlos.volante") );//carlos volante
			
			
			
			Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90") ;
			ResultSet rs = OrsegupsContratoUtils.getResultSet(queryTerceirizados , "SIGMA90");
			
			if(rs!= null){
				do{ // para cada gerente de regional
				
					NeoObject eformProcesso = eformProcesso = AdapterUtils.createNewEntityInstance("AVTPrincipal");
					EntityWrapper wEformProcesso = new EntityWrapper(eformProcesso);
				
					Long id_instalador = rs.getLong(1);
					Long cd_cliente = rs.getLong(2); 
					Long fechamento = rs.getLong(3);
					String ordem = retornaOSInstalador(id_instalador, cd_cliente, fechamento) ;
					
					ArrayList<NeoObject> oTerceirizados =  (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACOLABORADOR"), new QLEqualsFilter("cd_colaborador", id_instalador) );
					
					NeoObject oInstalador = null;
					if (oTerceirizados != null && oTerceirizados.size() > 0 ){
						oInstalador = oTerceirizados.get(0);
					}
					
					NeoObject oFormTerceirizado = AdapterUtils.createNewEntityInstance("AVTTerceirizado");
					EntityWrapper wFormTerceirizado = new EntityWrapper(oFormTerceirizado);
					
					wFormTerceirizado.setValue("terceirizado", oInstalador);
					wFormTerceirizado.setValue("idOrdem", ordem);
					wFormTerceirizado.setValue("dataAtendimento", new GregorianCalendar() );
					
					
					NeoObject oInfoConta = AdapterUtils.createNewEntityInstance("AVTInfoConta");
					EntityWrapper wInfoConta = new EntityWrapper(oInfoConta);
					
					String cliente[] = retornaDadosCliente(cd_cliente);
					if (cliente!= null){
						
						
						/*
						" cen.CD_CLIENTE, "+ 															//0 
						" cen.RAZAO, "+ 																//1 
						" cen.CGCCPF, "+ 																//2 
						" cast(emp.CD_EMPRESA as varchar) + '-' + emp.NM_RAZAO_SOCIAL as EMPRESA, "+ 	//3 
						" cen.ID_CENTRAL, "+ 															//4 
						" cen.PARTICAO, "+ 																//5 
						" cen.RESPONSAVEL, "+ 															//6 
						" cen.FONE1, "+ 																//7 
						" cen.ENDERECO, "+ 																//8 
						" bai.NOME, "+ 																	//9 
						" cid.NOME, "+ 																	//10
						" est.NOME, "+ 																	//11
						" cen.CEP, "+ 																	//12
						" (select NM_ROTA from ROTA where CD_ROTA = cen.id_rota) as rota1, "+ 			//13
						" (select NM_ROTA from ROTA where CD_ROTA = cen.id_rota2) as rota2 "+ 			//14
						*/
						
						/*for (String i : cliente){
							System.out.print( "["+ i + "] ");
						}
						System.out.println();*/
						
						wInfoConta.setValue("cdCliente", cliente[0]);
						wInfoConta.setValue("idCentral", cliente[4]);
						wInfoConta.setValue("cliente", cliente[1]);
						
						// informações novas
						wInfoConta.setValue("cgccpf", cliente[2]);
						wInfoConta.setValue("particao", cliente[5]);
						wInfoConta.setValue("empresa", cliente[3]);
						wInfoConta.setValue("responsavel", cliente[6]);
						wInfoConta.setValue("fone1", cliente[7]);
						wInfoConta.setValue("endereco", cliente[8]);
						wInfoConta.setValue("bairro", cliente[9]);
						wInfoConta.setValue("cidade", cliente[10]);
						wInfoConta.setValue("estado", cliente[11]);
						wInfoConta.setValue("cep", cliente[12]);
						wInfoConta.setValue("rota1", cliente[13] + "->" + cliente[14]);
						
						
						
					}
					
					wEformProcesso.setValue("infoConta", oInfoConta);
					wEformProcesso.setValue("terceirizado", oFormTerceirizado);
					
					wEformProcesso.setValue("numeroTentativa", 1L);
					wEformProcesso.setValue("houveContato", false);
					
					
					if (!JaRegistrado(id_instalador, cd_cliente, fechamento)){
						System.out.println("Instalador:" + id_instalador +" cdCliente:" + cd_cliente + " dia(YYMMDD): "+ fechamento );
						String tarefa  = iniciaProcessoTarefaAgendametoVisita(eformProcesso, solicitante, responsavel);
						registraLancamento(id_instalador, cd_cliente, fechamento, tarefa, new GregorianCalendar() );
					}
					Thread.sleep(500);
					
					cont++;
					//break;
					
				}while(rs.next());
			}
			
		}catch(Exception e){
			Long key = GregorianCalendar.getInstance().getTimeInMillis();
			System.out.println("["+key+"] Erro ao processa Job AvaliacaoTerceirizadoJOB  " + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processar Job Verifique no log por ["+key+"] para maiores detalhes");
		}
		
	}
	
	
	/**
	 * Inicia o workflow de Avalizacao de Terceirizado e retorna <b>true</b> se o workflow foi iniciado com suscesso
	 * ou <b>false</b> caso ocorra algum erro.
	 * 
	 * @param NeoObject eformProcesso Eform do processo de tarefa simples ja com seus valores peenchidos
	 * @param NeoUser responsavel Usuario responsãvel pelo workflow que estã sendo iniciado
	 * @return Boolean true se for iniciado corretamente e false caso ocorra algum erro
	 */
	public static String iniciaProcessoTarefaAgendametoVisita(NeoObject eformProcesso, NeoUser solicitante, NeoUser responsavel)
	{
		String result = null;
		try{
			QLEqualsFilter equal = new QLEqualsFilter("Name", "C037 - Avaliação/Classificação - TERCEIRIZADO");
			ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
			
			WFProcess proc = processModel.startProcess(eformProcesso, true, null, null, null, null, responsavel);
			System.out.println("[ACT] + Tarefa " + proc.getCode() + " Aberta.");
			proc.setRequester(solicitante);
			proc.setSaved(true);
			// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
			PersistEngine.persist(proc);
			PersistEngine.commit(true);
		
			result = proc.getCode();
		}catch(Exception e){
			result = null;
		}
		
		return result;
	}
	 
	public static String retornaOSInstalador(Long cd_instalador, long cd_cliente, long diaFechameto ){
		String retorno = "";
		String sql = "select ord.id_ordem from dbOrdem ord where ord.ID_INSTALADOR = "+cd_instalador+" and CD_CLIENTE = "+cd_cliente+"  and  CONVERT(VARCHAR(12),ord.FECHAMENTO, 12) = "+diaFechameto+" and ord.FECHADO = 1";
		
		Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql);
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object id_ordem : resultList){
				retorno += NeoUtils.safeOutputString(id_ordem) + ",";
			}
			
			if (retorno.indexOf(",") > -1){
				retorno = retorno.substring(0,retorno.length()-1);
			}
		}
		System.out.println(retorno);
		return retorno;
		
	}

	public void registraLancamento(Long cd_instalador, long cd_cliente, long diaFechamento, String tarefa, GregorianCalendar dataProc ){
		NeoObject oControle = AdapterUtils.createNewEntityInstance("AVTControleJob"); 
		EntityWrapper wControle = new EntityWrapper(oControle);
		
		wControle.setValue("instalador", cd_instalador);
		wControle.setValue("cdCliente", cd_cliente);
		wControle.setValue("fechamento", diaFechamento);
		wControle.setValue("tarefa", tarefa);
		wControle.setValue("dtProcessamento", new GregorianCalendar() );
		
		PersistEngine.persist(oControle);
		
	}


	public boolean JaRegistrado(Long cd_instalador, long cd_cliente, long diaFechamento){
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("instalador",cd_instalador));
		gp.addFilter(new QLEqualsFilter("cdCliente",cd_cliente));
		gp.addFilter(new QLEqualsFilter("fechamento", diaFechamento));
		
		ArrayList<NeoObject> oListaControle = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("AVTControleJob"),gp); 
		
		if (oListaControle != null){
			for (NeoObject obj : oListaControle){
				EntityWrapper wControle = new EntityWrapper(obj);
				if (wControle.findValue("tarefa") != null){
					return true;
				}
			}
			return false;
		}else{
			return false;
		}
		
	}

	public String[] retornaDadosCliente(Long cd_cliente){
		String retorno[] = new String[15];
		
		//String sql = "select CD_CLIENTE, ID_CENTRAL, CGCCPF + ' - ' + RAZAO from dbCENTRAL where cd_Cliente =" + cd_cliente;
		String sql = " select "+ 																 
				" cen.CD_CLIENTE, "+ 															//0 
				" cen.RAZAO, "+ 																//1
				" cen.CGCCPF, "+ 																//2 
				" cast(emp.CD_EMPRESA as varchar) + '-' + emp.NM_RAZAO_SOCIAL as EMPRESA, "+ 	//3
				" cen.ID_CENTRAL, "+ 															//4
				" cen.PARTICAO, "+ 																//5
				" cen.RESPONSAVEL, "+ 															//6
				" cen.FONE1, "+ 																//7
				" cen.ENDERECO, "+ 																//8
				" bai.NOME as nomBai, "+ 																	//9
				" cid.NOME as nomCid, "+ 																	//10
				" est.NOME as nomEst, "+ 																	//11
				" cen.CEP, "+ 																	//12
				" (select NM_ROTA from ROTA where CD_ROTA = cen.id_rota) as rota1, "+ 			//13
				" (select NM_ROTA from ROTA where CD_ROTA = cen.id_rota2) as rota2 "+ 			//14
			" from dbCENTRAL cen "+  
				" join EMPRESA emp on (cen.ID_EMPRESA = emp.CD_EMPRESA) "+
				" join dbBAIRRO bai on (cen.ID_BAIRRO = bai.ID_BAIRRO) "+
				" join dbCIDADE cid on (bai.ID_CIDADE = cid.ID_CIDADE) "+
				" join dbESTADO est on (cid.ID_ESTADO = est.ID_ESTADO) "+
			" where cen.cd_Cliente = " + cd_cliente;
		
		Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql);
		Collection<Object[]> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object cliente[] : resultList){
				retorno[0] = NeoUtils.safeOutputString(cliente[0]);
				retorno[1] = NeoUtils.safeOutputString(cliente[1]);
				retorno[2] = NeoUtils.safeOutputString(cliente[2]);
				retorno[3] = NeoUtils.safeOutputString(cliente[3]);
				retorno[4] = NeoUtils.safeOutputString(cliente[4]);
				retorno[5] = NeoUtils.safeOutputString(cliente[5]);
				retorno[6] = NeoUtils.safeOutputString(cliente[6]);
				retorno[7] = NeoUtils.safeOutputString(cliente[7]);
				retorno[8] = NeoUtils.safeOutputString(cliente[8]);
				retorno[9] = NeoUtils.safeOutputString(cliente[9]);
				retorno[10] = NeoUtils.safeOutputString(cliente[10]);
				retorno[11] = NeoUtils.safeOutputString(cliente[11]);
				retorno[12] = NeoUtils.safeOutputString(cliente[12]);
				retorno[13] = NeoUtils.safeOutputString(cliente[13]);
				retorno[14] = NeoUtils.safeOutputString(cliente[14]);
			}
		}
		
		for (String i : retorno){
			System.out.print( "["+ i + "] ");
		}
		System.out.println();
		return retorno;
	}

}
