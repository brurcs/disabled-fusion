package com.neomind.fusion.custom.orsegups.contract;

import java.util.Collection;
import java.util.SortedSet;

import javax.persistence.Query;

import org.jfree.util.Log;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.CadastraCentroCusto;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoValidaRever implements AdapterInterface
{
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		/*
		 * validar a atualização do centro de custo
		 * validar a criação da tela de comparação
		 */
		Long movimentacao = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		Long alteracao = 0L;
		if(movimentacao == 5)
		{
			alteracao = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");
		}
		
		/*
		 * cliente novo eletrônica ou alteração e razão social
		 */
		if(movimentacao == 3 || alteracao == 2)
		{
			atualizaCentrosCusto(wrapper);
		}
		
		geraTabelaComparativa(wrapper);
		
		wrapper.setValue("aprovadoAnalise", null);
		ContratoLogUtils.logInfo("Iniciando geração automatica da minuta");
		MinutaContrato.autoGeracaoMinuta(wrapper);
	}
	
	private static void geraTabelaComparativa(EntityWrapper wrapper)
	{
		NeoObject cliente = (NeoObject) wrapper.findValue("novoCliente");
		NeoObject clienteBackup = (NeoObject) wrapper.findValue("novoClienteBackup");
		wrapper.findField("listaAlteracoesCliente").getValues().clear();
		ContratoGerarTabelaComparativa geraTabelaCOmp = new ContratoGerarTabelaComparativa();
		if(geraTabelaCOmp.compareFieldCliente(wrapper, cliente, clienteBackup))
		{
			wrapper.setValue("houveAlteracaoDadosCliente", true);
		}
		
		/*
		 * para contrato novo, apenas esse campo poderá conter alterações
		 */
		long movimentoContrato = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		if(movimentoContrato <= 4)
			return;
		
		NeoObject dadosGerais = (NeoObject) wrapper.findValue("dadosGeraisContrato");
		NeoObject dadosGeraisBackup = (NeoObject) wrapper.findValue("dadosGeraisContratoBackup");
		wrapper.findField("listaAlteracoesContrato").getValues().clear();
		if(!geraTabelaCOmp.compareFieldContrato(wrapper, dadosGerais, dadosGeraisBackup))
		{
			wrapper.setValue("houveAlteracaoDadosGerais", true);
		}
		
		Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
		Collection<NeoObject> postosBackup = wrapper.findField("postosContratoBackup").getValues();
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			
			wPosto.findField("listaAlteracoes").getValues().clear();
			
			long numeroPosto = 0;
			if(wPosto.findValue("numPosto") != null)
				numeroPosto = (Long) wPosto.findValue("numPosto");
			NeoObject postoBackupAlvo = null;
			for(NeoObject postoBackup : postosBackup)
			{
				EntityWrapper wPostoBackup = new EntityWrapper(postoBackup);
				long numeroPostoBackup = (Long) wPostoBackup.findValue("numPosto");
				if(numeroPostoBackup == numeroPosto)
				{
					postoBackupAlvo = postoBackup;
				}
			}
			
			if(postoBackupAlvo == null)
			{
				//fluxo de alteração, um novo posto criado
				continue;
			}
			EntityWrapper wPostoBackup = new EntityWrapper(postoBackupAlvo);
			
			SortedSet<FieldInfo> fields = posto.getInfo().getFieldSet();
			/*
			 * é necessário ter certeza que este campo ficará true, somente se achar algum campo alterado
			 * se por algum motivo o fluxo der algum loop, isso irá refletir alterações anteriores
			 */
			wPosto.setValue("houveAlteracaoPosto", false);
			for(FieldInfo fi : fields)
			{
				if(!geraTabelaCOmp.compareField(wPosto, wPostoBackup, fi.getName(), fi.getTitle()))
				{
					wPosto.setValue("houveAlteracaoPosto", true);
				}
			}
			
		}
	}
	
	private void atualizaCentrosCusto(EntityWrapper wrapper)
	{
		String codEmp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codCli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codcli"));
		String numeroContrato = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		if(numeroContrato.equals(""))
			numeroContrato = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		
		String cgcCpf = null;
		
		String queryCli = "select NomCli, cast( cgccpf as Varchar(20) ) cgccpf from e085cli where codCli = "+codCli;
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(queryCli);
		Collection<Object[]> resultList = query.getResultList();
		
		String razaoSocialOld = "";
		if (resultList != null && resultList.size() >0){
			for(Object[] r : resultList){
				razaoSocialOld = (String) r[0];
				cgcCpf = (String)r[1];
			}
		}else{
			Log.warn("updateRazaoSocial() - Nome antigo do cliente "+codCli+" não encontrado 2" );
		}
		
		String ccu = NeoUtils.safeOutputString(wrapper.findValue("centroCusto3Nivel"));
		String razaoSocial4 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial5 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial6 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String classificacaoQuartoNivel = NeoUtils.safeOutputString(wrapper.findValue("classificacaoCentroCusto4Nivel"));
		
		if("".equals(classificacaoQuartoNivel))
		{
			ccu = "178300";
			classificacaoQuartoNivel = "151830795";
			//throw new WorkflowException("Erro, informa a TI. Não encontrato a classificação do 4º nível e o centro de custo do 3º nível.");
		}
		
		Collection<String> listaCC4 = CadastraCentroCusto.atualizaCCQuartoNivel(ccu, razaoSocialOld, razaoSocial4, classificacaoQuartoNivel,4);
		for(String cc4 : listaCC4)
		{
			//System.out.println("4>"+cc4);
			Collection<String> listaCC5 = CadastraCentroCusto.atualizaCCQuintoNivel(cc4, razaoSocialOld, razaoSocial5, classificacaoQuartoNivel,5);
			for(String cc5 : listaCC5)
			{
				//System.out.println("5>>>"+cc5);
				Collection<String> listaCC6 = CadastraCentroCusto.atualizaCCSextoNivel(cc5, razaoSocialOld, razaoSocial6, classificacaoQuartoNivel,6);
			}
		}
	}
	
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
