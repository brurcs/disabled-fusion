package com.neomind.fusion.custom.orsegups.maps.call.engine;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;

@Path(value = "mapsAlertEventTest")
public class CallEventDummyTest
{
	@GET
	@Path("alertEvent")
	public void alertAitTest()
	{
		
		String eventoInfoWindow = "<div id=\"content\"><div id=\"bodyContent\"><span ><b>Cliente:</b> </span> <span >1662 [001] CHARLES DALLA COSTA FELIX</span><br><span ><b>Razão Social:</b> </span> <span >CHARLES DALLA COSTA FELIX</span><br><span ><b>Endereço:</b> </span> <span >R. PEDRO NICCO, 490  </span><br><span ><b>Bairro/Cidade:</b> </span> <span >MOSSUNGUE CURITIBA PARANÁ</span><br><span ><b>Evento:</b> </span> <span >E130 - ALARME ROUBO  DUAL DUAL GARAGEM</span><br><span ><b>Rota:</b> </span> <span >CTA-AA3-D-06_18</span><br><span ><b>Recebido:</b> <span >24/03/2014 10:29:44</span><br><span ><b>Deslocado:</b> </span> <span >24/03/2014 10:30:39&nbsp; &nbsp; <a tooltip=\"&lt;b&gt;Responsável:&lt;/b&gt;&lt;/br&gt;Operação realizada pela CM, operador: (CM - MILENE POPPER)\"> <img src=\"http://intranet.orsegups.com.br/fusion/imagens/custom/home_16x16.png\"></a></span><br><span ><b>Iniciado:</b> <span >24/03/2014 10:39:05&nbsp; &nbsp; <a tooltip=\"&lt;b&gt;Responsável:&lt;/b&gt;&lt;/br&gt;Operação realizada pela CM, operador: (CM - MILENE POPPER)\"><img src=\"http://intranet.orsegups.com.br/fusion/imagens/custom/home_16x16.png\"></a></span><br><span ><b>No Local:</b> </span> <span > </span><br><a tooltip=\"&lt;b&gt;Log:&lt;/b&gt;&lt;/br&gt;&lt;li&gt;24/03/2014 10:38:43 Log Gerência.\"><img src=\"http://intranet.orsegups.com.br/fusion/imagens/icones_final/document_v_16x16-trans.png\"></a>&nbsp; &nbsp;<a tooltip=\"&lt;b&gt;Log Gerência:&lt;/b&gt;&lt;/br&gt;&lt;li&gt;iniciado deslocamento manualmente para agente &lt;br&gt;&lt;li&gt;o memso esta com problema no sigma \"><img src=\"http://intranet.orsegups.com.br/fusion/imagens/icones_final/doc_link.png\"></a>&nbsp; &nbsp;<a tooltip=\"&lt;b&gt;Atualizar Posição&lt;/b&gt;\" href=\"javascript:atualizaPosicaoCliente(141511,'CTA8107')\"><img style=\"border : none\" src=\"images/atualizarPosicao.jpg\"></a></span></span></div></div>";

		EventoVO eventoVO = new EventoVO();
		eventoVO.setPlaca("CTA8107");
		eventoVO.setCodigoHistorico("DummyTest");
		eventoVO.setCodigoEvento("X406");
		eventoVO.setInfoWindowContent(eventoInfoWindow);
		
		//OrsegupsAlertEventEngine.getInstance().alert(eventoVO);
	}

	@GET
	@Path("clearCache")
	public void clearCache()
	{
		//OrsegupsAlertEventEngine.getInstance().alert(eventoVO);
		OrsegupsAlertEventEngine.getInstance().clearCache();
	}
	
	@GET
	@Path("getCache")
	public void getCache()
	{
		OrsegupsAlertEventEngine.getInstance().getCache();
	}
	
}
