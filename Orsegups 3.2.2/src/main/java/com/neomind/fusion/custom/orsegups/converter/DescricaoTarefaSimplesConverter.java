package com.neomind.fusion.custom.orsegups.converter;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.util.NeoUtils;

public class DescricaoTarefaSimplesConverter extends StringConverter
{
	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		String result = "";
		
		if(NeoUtils.safeIsNotNull(field.getValue())){
			result = field.getValue().toString();			
		}
		
		return result;
	}	
}
