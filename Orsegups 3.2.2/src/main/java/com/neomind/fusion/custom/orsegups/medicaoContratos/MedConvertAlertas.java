package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;

public class MedConvertAlertas extends DefaultConverter
{

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		String alertas = "";
		boolean vagasDivergentes = false;
		boolean retornoInss = false;
		boolean sitOK = true;
		alertas = (String) field.getForm().getField("alertas").getValue();
		List<NeoObject> listPostos = (List<NeoObject>) field.getForm().getField("listaPostos").getValue();

		for (NeoObject posto : listPostos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			long nVagas = (long) wPosto.findField("nVagas").getValue();
			long nColaboradores = (long) wPosto.findField("nColaboradores").getValue();
			String alertasPosto = String.valueOf(wPosto.findField("alertasPosto").getValue());
			if (nVagas != nColaboradores)
			{
				if(!alertas.contains("nVagasDivergentes")){
					if(alertas.contains("ok")){
						field.getForm().getField("alertas").setValue(";nVagasDivergentes");
						NeoObject obj =  field.getForm().getObject();
						EntityWrapper wObj = new EntityWrapper(obj);
						wObj.findField("alertas").setValue(";nVagasDivergentes");
						PersistEngine.persist(obj);
					}else{
						field.getForm().getField("alertas").setValue(alertas+";nVagasDivergentes");
						NeoObject obj =  field.getForm().getObject();
						EntityWrapper wObj = new EntityWrapper(obj);
						wObj.findField("alertas").setValue(alertas+";nVagasDivergentes");
						PersistEngine.persist(obj);
					}
					
				}
				
				vagasDivergentes = true;
			}
			if (alertasPosto.contains("retornoINSS"))
			{
				retornoInss = true;
			}
		}

		if (alertas == null)
		{
			alertas = "";
		}
		String icone = "";

		if (alertas.contains("falta"))
		{
			icone = icone + "<img src='imagens/Alerta_Vagas.png'title= 'Contrato possui colaborador(es) com menos de 30 dias.'/>";
			sitOK = false;
		}

		if (alertas.contains("colaborador demitido"))
		{
			icone = icone + "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/novo_colaborador.png\"title=\" O contrato possui um colaborador demitido no periodo.\"/>";
			sitOK = false;
		}

		if (vagasDivergentes)
		{
			icone = icone + "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/exclamacao2.png\"title=\" O Posto possui divergência entre a quantidade de vagas e colaboradores.\"/>";
			sitOK = false;
		}

		if (retornoInss)
		{
			icone = icone + "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/INSS.png\" title=\" Existe colaborador com retorno de INSS.\"/>";
		}

		if (sitOK)
		{
			icone = "<img src='imagens/certo2.png'/>";
		}

		StringBuilder outBuilder = new StringBuilder();
		outBuilder.append(icone);

		return outBuilder.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
