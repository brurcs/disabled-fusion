package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class AcessoVO
{
	private ColaboradorVO colaborador;
	private GregorianCalendar data;
	private String dataAcesso;
	private String horaAcesso;
	private Long sequencia;
	private String direcao;
	private String tipoDirecao;
	private String tipoAcesso;
	private String relogio;
	private String origem;
	private Long codigoOrganograma;
	private Long numeroLocal;
	private Long permanenciaPosto;
	private Long codigoJustificativa;
	private String obsJustificativa;
	private String operacao;
	private PostoVO posto;
	private ApuracaoVO apuracao;
	private String fone;
	private String excluir;
	private String justificativaAcesso;
	private String obsAcesso;

	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}
	public GregorianCalendar getData()
	{
		return data;
	}
	public void setData(GregorianCalendar data)
	{
		this.data = data;
	}
	public Long getSequencia()
	{
		return sequencia;
	}
	public void setSequencia(Long sequencia)
	{
		this.sequencia = sequencia;
	}
	public String getDirecao()
	{
		return direcao;
	}
	public void setDirecao(String direcao)
	{
		this.direcao = direcao;
	}
	public String getTipoDirecao()
	{
		return tipoDirecao;
	}
	public void setTipoDirecao(String tipoDirecao)
	{
		this.tipoDirecao = tipoDirecao;
	}
	public String getTipoAcesso()
	{
		return tipoAcesso;
	}
	public void setTipoAcesso(String tipoAcesso)
	{
		this.tipoAcesso = tipoAcesso;
	}
	public String getRelogio()
	{
		return relogio;
	}
	public void setRelogio(String relogio)
	{
		this.relogio = relogio;
	}
	public Long getCodigoOrganograma()
	{
		return codigoOrganograma;
	}
	public void setCodigoOrganograma(Long codigoOrganograma)
	{
		this.codigoOrganograma = codigoOrganograma;
	}
	public Long getNumeroLocal()
	{
		return numeroLocal;
	}
	public void setNumeroLocal(Long numeroLocal)
	{
		this.numeroLocal = numeroLocal;
	}
	public Long getPermanenciaPosto()
	{
		return permanenciaPosto;
	}
	public void setPermanenciaPosto(Long permanenciaPosto)
	{
		this.permanenciaPosto = permanenciaPosto;
	}
	public PostoVO getPosto()
	{
		return posto;
	}
	public void setPosto(PostoVO posto)
	{
		this.posto = posto;
	}
	public String getDataAcesso()
	{
		return dataAcesso;
	}
	public void setDataAcesso(String dataAcesso)
	{
		this.dataAcesso = dataAcesso;
	}
	public ApuracaoVO getApuracao()
	{
		return apuracao;
	}
	public void setApuracao(ApuracaoVO apuracao)
	{
		this.apuracao = apuracao;
	}
	public String getHoraAcesso()
	{
		return horaAcesso;
	}
	public void setHoraAcesso(String horaAcesso)
	{
		this.horaAcesso = horaAcesso;
	}
	public Long getCodigoJustificativa()
	{
		return codigoJustificativa;
	}
	public void setCodigoJustificativa(Long codigoJustificativa)
	{
		this.codigoJustificativa = codigoJustificativa;
	}
	public String getObsJustificativa()
	{
		return obsJustificativa;
	}
	public void setObsJustificativa(String obsJustificativa)
	{
		this.obsJustificativa = obsJustificativa;
	}
	public String getOrigem()
	{
		return origem;
	}
	public void setOrigem(String origem)
	{
		this.origem = origem;
	}
	public String getOperacao()
	{
		return operacao;
	}
	public void setOperacao(String operacao)
	{
		this.operacao = operacao;
	}
	public String getFone()
	{
		return fone;
	}
	public void setFone(String fone)
	{
		this.fone = fone;
	}
	public String getExcluir()
	{
		return excluir;
	}
	public void setExcluir(String excluir)
	{
		this.excluir = excluir;
	}
	public String getJustificativaAcesso()
	{
		return justificativaAcesso;
	}
	public void setJustificativaAcesso(String justificativaAcesso)
	{
		this.justificativaAcesso = justificativaAcesso;
	}
	public String getObsAcesso()
	{
		return obsAcesso;
	}
	public void setObsAcesso(String obsAcesso)
	{
		this.obsAcesso = obsAcesso;
	}	
}