package com.neomind.fusion.custom.orsegups.maps.vo;

import java.math.BigDecimal;

public class EventoEsperaVO implements Comparable<EventoEsperaVO>
{
	private String codHistorico;
	private String codRota;
	private String dataEspera;
	private Long codigo;
	private String cliente;
	private String codigoViatura;
	private Long peso;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private long neoId;
	
	public long getNeoId() {
	    return neoId;
	}

	public void setNeoId(long neoId) {
	    this.neoId = neoId;
	}

	public Long getPeso() {
	    return peso;
	}

	public void setPeso(Long peso) {
	    this.peso = peso;
	}

	public BigDecimal getLatitude() {
	    return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
	    this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
	    return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
	    this.longitude = longitude;
	}

	public String getCodHistorico()
	{
		return codHistorico;
	}

	public void setCodHistorico(String codHistorico)
	{
		this.codHistorico = codHistorico;
	}

	public String getCodRota()
	{
		return codRota;
	}

	public void setCodRota(String codRota)
	{
		this.codRota = codRota;
	}

	public String getDataEspera()
	{
		return dataEspera;
	}

	public void setDataEspera(String dataEspera)
	{
		this.dataEspera = dataEspera;
	}

	public Long getCodigo()
	{
		return codigo;
	}

	public void setCodigo(Long codigo)
	{
		this.codigo = codigo;
	}

	public String getCliente()
	{
		return cliente;
	}

	public void setCliente(String cliente)
	{
		this.cliente = cliente;
	}

	public String getCodigoViatura()
	{
		return codigoViatura;
	}

	public void setCodigoViatura(String codigoViatura)
	{
		this.codigoViatura = codigoViatura;
	}

	@Override
	public int compareTo(EventoEsperaVO o)
	{

		if (this.getCodigo() > o.getCodigo())
		{
			return -1;
		}
		if (this.getCodigo() < o.getCodigo())
		{
			return 1;
		}

		return 0;
	}

}
