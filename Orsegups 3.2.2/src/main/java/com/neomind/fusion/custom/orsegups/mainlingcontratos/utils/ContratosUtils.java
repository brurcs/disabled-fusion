package com.neomind.fusion.custom.orsegups.mainlingcontratos.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.vo.ContratosVO;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.vo.RegionalVO;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.vo.ServicosVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class ContratosUtils
{

	public List<ContratosVO> getLista(ContratosVO contratosVO, String dataInicio, String dataFim, String dataCadastroInicio, String dataCadastroFim)
	{

		int i = 1;
		
		StringBuffer sql = sqlContratos(contratosVO, dataInicio, dataFim, dataCadastroInicio, dataCadastroFim);

		List<ContratosVO> lista = new ArrayList();
		PreparedStatement st = null;
		ResultSet rs = null;
		Connection conn = null;
		String nomeFonteDados = "SAPIENS";

		try
		{
			conn = PersistEngine.getConnection(nomeFonteDados);
			st = conn.prepareStatement(sql.toString());

			if (!contratosVO.getServicosVO().getAbreviatura().equals(""))
			{
				String[] servico = contratosVO.getServicosVO().getAbreviatura().split(",");

				for (int j = 0; j < servico.length; j++)
				{

					if (!servico[j].equalsIgnoreCase("especializados"))
					{
											
						if (servico[j].equalsIgnoreCase("portaria"))
						{

							servico[j] = "portaria remota";
						}

						int iServico = i;
						st.setString(iServico, "%" + servico[j] + "%");
						i++;

					}

				}
			}
			if (contratosVO.getCidadeContrato() != "")
			{
				int iCidadeContrato = i;
				st.setString(iCidadeContrato, "%" + contratosVO.getCidadeContrato() + "%");
				i++;
			}

			if (contratosVO.getCidadeCliente() != "")
			{
				int iCidadeCliente = i;
				st.setString(iCidadeCliente, "%" + contratosVO.getCidadeCliente() + "%");
				i++;
			}

			if (dataInicio != "" && dataFim != "")
			{
				java.sql.Date dateInicio = null;
				java.sql.Date dateFim = null;
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				dateInicio = new java.sql.Date(((java.util.Date) formatter.parse(dataInicio)).getTime());
				dateFim = new java.sql.Date(((java.util.Date) formatter.parse(dataFim)).getTime());

				int iDataInicio = i;
				st.setDate(iDataInicio, dateInicio);
				i++;

				int iDataFim = i;
				st.setDate(iDataFim, dateFim);
				i++;
			}

			if (dataCadastroInicio != "" && dataCadastroFim != "")
			{
				java.sql.Date dateInicio = null;
				java.sql.Date dateFim = null;
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				dateInicio = new java.sql.Date(((java.util.Date) formatter.parse(dataCadastroInicio)).getTime());
				dateFim = new java.sql.Date(((java.util.Date) formatter.parse(dataCadastroFim)).getTime());

				int iDataCadastroInicio = i;
				st.setDate(iDataCadastroInicio, dateInicio);
				i++;

				int iDataCadastroFim = i;
				st.setDate(iDataCadastroFim, dateFim);
			}

			rs = st.executeQuery();

			while (rs.next())
			{

				String[] email = rs.getString("IntNet").split(";");
				for (int j = 0; j < email.length; j++)
				{

					ContratosVO vo = new ContratosVO();
					RegionalVO regionalVO = new RegionalVO();
					ServicosVO servicosVO = new ServicosVO();
					servicosVO.setDescricaoServico(rs.getString("desSer"));
					regionalVO.setNomeRegional(rs.getString("usu_nomReg"));
					vo.setEmail(email[j]);
					vo.setRazao(rs.getString("NomCli"));
					vo.setCidadeCliente(rs.getString("cidCli"));
					vo.setCidadeContrato(rs.getString("usu_cidctr"));
					vo.setSituacao(rs.getString("usu_sitCtr"));
					vo.setDataInicio(rs.getDate("usu_datIni"));
					vo.setDataCadastroInicio(rs.getDate("usu_datcad"));
					vo.setTipoCliente(rs.getString("tipCli"));
					vo.setTipoEmc(rs.getString("tipEmc"));
					vo.setCpfCnpj(rs.getString("CgcCpf"));
					vo.setRegional(regionalVO);
					vo.setServicosVO(servicosVO);
					validaString(vo);

					lista.add(vo);
				}
			}
		}

		catch (Exception e)
		{

			e.printStackTrace();

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

		return lista;

	}

	public List<RegionalVO> getLista()
	{

		StringBuffer sql = new StringBuffer();

		sql.append("select usu_codreg, usu_nomreg from usu_t200reg ");
		sql.append("where  usu_codreg not in (0,999) ");
		sql.append("order by usu_codreg ");

		List<RegionalVO> lista = new ArrayList();

		PreparedStatement st = null;
		ResultSet rs = null;
		Connection conn = null;
		String nomeFonteDados = "SAPIENS";

		try
		{
			conn = PersistEngine.getConnection(nomeFonteDados);
			st = conn.prepareStatement(sql.toString());
			rs = st.executeQuery();

			while (rs.next())
			{
				RegionalVO vo = new RegionalVO();

				vo.setCodigoRegional(rs.getString("usu_codReg"));
				vo.setNomeRegional(rs.getString("usu_nomReg"));

				lista.add(vo);
			}
		}

		catch (Exception e)
		{

			e.printStackTrace();

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

		return lista;

	}

	public List<ServicosVO> getListaServicos()
	{

		List<NeoObject> mainling = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("mainlingServicos"));

		List<ServicosVO> listaServicos = new ArrayList<ServicosVO>();

		for (NeoObject neoObject : mainling)
		{
			ServicosVO servicosVO = new ServicosVO();
			EntityWrapper wMainlig = new EntityWrapper(neoObject);
			String descricao = (String) wMainlig.findField("descricao").getValue();

			String abreviatura = (String) wMainlig.findField("abreviatura").getValue();
			servicosVO.setDescricaoServico(descricao);

			servicosVO.setAbreviatura(abreviatura);
			listaServicos.add(servicosVO);
		}

		System.out.println(listaServicos);

		return listaServicos;

	}

	private StringBuffer sqlContratos(ContratosVO contratoVO, String dataInicio, String dataFim, String dataCadastroInicio, String dataCadastroFim)
	{

		StringBuffer sql = new StringBuffer();

		sql.append("SELECT top 10000 CL.NomCli,CL.IntNet,P.usu_codser,CL.CidCli,P.usu_cidctr,r.usu_nomreg, c.usu_sitCtr, s.DesSer, p.usu_datIni, p.usu_datcad,cl.tipEmc, cl.tipCli, c.usu_numctr,cast(SUM(p.usu_preuni * p.USU_QTDCVS) as money), cl.CgcCpf ");
		sql.append("FROM USU_T160CVS P ");
		sql.append("JOIN USU_T160CTR C ON P.USU_NUMCTR = C.USU_NUMCTR AND P.USU_CODEMP = C.USU_CODEMP AND P.USU_CODFIL = C.USU_CODFIL ");
		sql.append("JOIN E085CLI CL ON CL.CODCLI=C.USU_CODCLI ");
		sql.append("JOIN USU_T200REG R ON R.USU_CODREG=P.USU_REGCVS ");
		sql.append("JOIN E080SER S ON P.USU_CODSER=S.CODSER AND S.CODEMP=P.USU_CODEMP ");
		sql.append("WHERE c.usu_numctr <> 1 ");

		if (!contratoVO.getSituacao().equalsIgnoreCase("Selecione Abaixo"))
		{
			if (contratoVO.getSituacao().equalsIgnoreCase("ativos"))
			{
				sql.append("AND ((P.USU_SITCVS = 'A') OR (P.USU_SITCVS = 'I' AND P.USU_DATFIM >= GETDATE())) ");
				sql.append("AND((C.USU_SITCTR = 'A') OR (C.USU_SITCTR = 'I' AND C.USU_DATFIM >= GETDATE())) ");
			}
			if (contratoVO.getSituacao().equalsIgnoreCase("inativos"))
			{
				sql.append("AND ((C.USU_SITCTR = 'I') and (P.USU_SITCVS = 'I')) ");

			}
		}
		if (!contratoVO.getRazao().equalsIgnoreCase("Selecione Abaixo"))
		{
			if (contratoVO.getRazao().equalsIgnoreCase("Condominio"))
			{

				sql.append("and (CL.NomCli like '%condominio%' or CL.NomCli like '%residencial%') ");
			}
			if (contratoVO.getRazao().equalsIgnoreCase("Comercial"))
			{

				sql.append("and CL.TipCli = 'J' ");
				sql.append("and (CL.NomCli not like '%condominio%' or CL.NomCli not like '%residencial%') ");
			}

			if (contratoVO.getRazao().equalsIgnoreCase("Outros"))
			{

				sql.append("and CL.TipCli = 'F' ");
				sql.append("and (CL.NomCli not like '%condominio%' or CL.NomCli not like '%residencial%') ");
			}

		}

		if (!contratoVO.getRegional().getCodigoRegional().equalsIgnoreCase(""))
		{
			sql.append("and r.usu_codReg in ("+ contratoVO.getRegional().getCodigoRegional() +") ");
		}

		if (!contratoVO.getServicosVO().getAbreviatura().equalsIgnoreCase(""))
		{

			String[] servico = contratoVO.getServicosVO().getAbreviatura().split(",");

			for (int i = 0; i < servico.length; i++)
			{

				if (servico[i].equalsIgnoreCase("especializados"))
				{
					if (servico.length == 1 || i == 0)
					{
						sql.append("AND (S.DesSer LIKE 'AUX%SER%' OR S.DesSer like '%Recepcionista%' OR S.DesSer LIKE '%JARDINEI%' OR S.DesSer LIKE '%ZELADOR%' ");
						sql.append("OR S.DesSer like '%Copeira%' or S.DesSer like 'Auxiliar Administrativo%' or S.DesSer like 'Motorista%' or S.DesSer like 'Agente de Estacionamento%' or S.DesSer like 'Manobrista%' ");
					}
					else
					{

						sql.append("OR S.DesSer LIKE 'AUX%SER%' OR S.DesSer like '%Recepcionista%' OR S.DesSer LIKE '%JARDINEI%' OR S.DesSer LIKE '%ZELADOR%' ");
						sql.append("OR S.DesSer like '%Copeira%' or S.DesSer like 'Auxiliar Administrativo%' or S.DesSer like 'Motorista%' or S.DesSer like 'Agente de Estacionamento%' or S.DesSer like 'Manobrista%' ");

					}
				}
				else
				{

					if (i == 0)
					{

						sql.append("and (s.DesSer like ? ");
					}
					else
					{

						sql.append("or s.DesSer like ? ");
					}

				}

				if (i + 1 == servico.length)
				{
					sql.append(") ");
				}
			}

		}

		if (contratoVO.getCidadeContrato() != "")
		{
			sql.append("and p.usu_cidctr like ? ");
		}

		if (contratoVO.getCidadeCliente() != "")
		{
			sql.append("and cl.cidCli like ? ");
		}

		if (dataInicio != "" && dataFim != "")
		{
			sql.append("and p.usu_datIni >= ? ");
			sql.append("and p.usu_datIni <= ? ");
		}

		if (dataCadastroInicio != "" && dataCadastroFim != "")
		{
			sql.append("and p.usu_datCad >= ? ");
			sql.append("and p.usu_datCad <= ? ");
		}

		if (!contratoVO.getTipoEmc().equalsIgnoreCase("Selecione Abaixo"))
		{
			if (contratoVO.getTipoEmc().equalsIgnoreCase("privado"))
			{

				sql.append("and cl.tipEmc = 1 ");
			}
			else if (contratoVO.getTipoEmc().equalsIgnoreCase("Público"))
			{

				sql.append("and cl.tipEmc = 2 ");
			}

		}

		if (!contratoVO.getTipoCliente().equalsIgnoreCase("Selecione Abaixo"))
		{
			if (contratoVO.getTipoCliente().equalsIgnoreCase("Física"))
			{

				sql.append("and cl.tipCli = 'F' ");
			}
			else if (contratoVO.getTipoCliente().equalsIgnoreCase("jurídica"))
			{

				sql.append("and cl.tipCli = 'J' ");
			}

		}
		sql.append("Group By CL.NomCli,CL.IntNet,P.usu_codser,CL.CidCli,P.usu_cidctr,r.usu_nomreg, c.usu_sitCtr, s.DesSer, p.usu_datIni, p.usu_datcad,cl.tipEmc, cl.tipCli, c.usu_numctr, cl.CgcCpf ");

		if (!contratoVO.getFaturamentoContrato().equalsIgnoreCase("Selecione Abaixo"))
		{

			if (contratoVO.getFaturamentoContrato().equalsIgnoreCase("varejo"))
			{
				sql.append("having CAST(SUM(P.USU_PREUNI * P.USU_QTDCVS) AS MONEY) <= 1000 ");
			}

			if (contratoVO.getFaturamentoContrato().equalsIgnoreCase("Corporativo"))
			{
				sql.append("having CAST(SUM(P.USU_PREUNI * P.USU_QTDCVS) AS MONEY) > 1000 ");
			}

		}
		sql.append("Order By cl.nomCli;");

		return sql;
	}

	private void validaString(ContratosVO vo)
	{

		if (vo.getSituacao().equals("A"))
		{
			vo.setSituacao("Ativo");
		}
		else
		{
			vo.setSituacao("Inativo");
		}

		if (vo.getTipoEmc().equals("1"))
		{

			vo.setTipoEmc("Privado");
		}
		else
		{

			vo.setTipoEmc("Público");
		}

		if (vo.getTipoCliente().equals("F"))
		{
			vo.setTipoCliente("Física");
		}
		else
		{

			vo.setTipoCliente("Jurídica");

		}
	}
	
	
}
