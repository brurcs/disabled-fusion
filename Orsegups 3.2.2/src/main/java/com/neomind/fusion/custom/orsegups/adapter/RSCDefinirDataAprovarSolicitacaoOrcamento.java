package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RSCDefinirDataAprovarSolicitacaoOrcamento implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar dataAtual = new GregorianCalendar();
		GregorianCalendar prazoAtendimento = (GregorianCalendar) processEntity.findValue("prazoAtendimento");
		Boolean aprovar = (Boolean) processEntity.findValue("aprovarAtenderSolicitacaoClienteOrcamento");
		if (dataAtual.get(GregorianCalendar.DAY_OF_MONTH) == prazoAtendimento.get(GregorianCalendar.DAY_OF_MONTH) && aprovar == Boolean.FALSE) {
			GregorianCalendar novaDataPrazoAtendimento = OrsegupsUtils.getSpecificWorkDay(prazoAtendimento, 1L);
			novaDataPrazoAtendimento.set(Calendar.HOUR_OF_DAY, 23);
			novaDataPrazoAtendimento.set(Calendar.MINUTE, 59);
			novaDataPrazoAtendimento.set(Calendar.SECOND, 59);
			processEntity.setValue("prazoAtendimento", novaDataPrazoAtendimento);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}