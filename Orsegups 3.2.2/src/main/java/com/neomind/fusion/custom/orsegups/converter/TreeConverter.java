package com.neomind.fusion.custom.orsegups.converter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.velocity.VelocityContext;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.portal.VelocityUtils;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class TreeConverter extends DefaultConverter 
{

	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		String servletPath = PortalUtil.getBaseURL();
		
		NeoObject FGCPrincipal = field.getForm().getCaller().getObject();
		EntityWrapper ewFGCPrincipal = new EntityWrapper(FGCPrincipal);
		
		NeoObject FGCPosto = field.getForm().getObject();
		EntityWrapper ewFGCPosto = new EntityWrapper(FGCPosto);
		
		VelocityContext context = new VelocityContext();
		
		String codemp = ewFGCPrincipal.findField("empresa.codemp").getValueAsString();
		
		if(ewFGCPosto.findField("centroCusto.codccu") == null)
		{
			String codcli = (ewFGCPrincipal.findField("buscaNomeCliente").getValueAsString().equals(""))?"":ewFGCPrincipal.findField("buscaNomeCliente.codcli").getValueAsString();
			
			// Cliente foi selecionado
			if(!codcli.equals(""))
			{
				String claccu = (codemp.equals("15")?"1615%":"15"+(codemp.length()==1?"0"+codemp:codemp)+"%");
				String query = "SELECT TOP 1 MIN(SUBSTRING(E044CCU.CLACCU,1,9)) AS claccu "+
						"FROM  E044CCU, USU_T160CVS, USU_T160CTR, E043PCM "+
						"WHERE E044CCU.CODEMP = " + codemp + " AND "+
	                               	"USU_T160CTR.USU_CODCLI = " + codcli + " AND "+ 
	                               	"E044CCU.CODCCU = USU_T160CVS.USU_CODCCU AND "+ 
	                               	"E044CCU.CODEMP = USU_T160CVS.USU_CODEMP AND "+
	                               	"E043PCM.CLACTA = E044CCU.CLACCU AND "+
	                               	"E044CCU.CLACCU LIKE '" + claccu + "' AND "+
	                               	"SUBSTRING(E044CCU.CLACCU,1,9) NOT IN ('150100060','150200051','150500015','150600032','150700039','150800025','151400039','151502660','151700002','151800002','150101549','161500148') AND "+  
	                               	"USU_T160CTR.USU_CODEMP = USU_T160CVS.USU_CODEMP AND "+
	                               	"USU_T160CTR.USU_CODFIL = USU_T160CVS.USU_CODFIL AND "+
	                               	"USU_T160CTR.USU_NUMCTR = USU_T160CVS.USU_NUMCTR";
	        
				ResultSet rs = OrsegupsContratoUtils.getResultSet(query, "SAPIENS");
				String result;
	
				try
				{
					if (rs != null)
					{
						do
						{
							result = rs.getString("claccu");
						}
						while (rs.next());
						
						QLGroupFilter filter = new QLGroupFilter("AND");
						
						// Filtro por empresa
						QLEqualsFilter filterCodemp = new QLEqualsFilter("codemp", Long.valueOf(codemp));
						filter.addFilter(filterCodemp);
						
						// Nível Cliente
						QLEqualsFilter filterNivccu = new QLEqualsFilter("nivccu", 4);
						filter.addFilter(filterNivccu);
						
						QLRawFilter filterClaccu = new QLRawFilter("claccu LIKE '" + result  +"%'");
						filter.addFilter(filterClaccu);				
						
						List<NeoObject> noListaCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filter);
						
						for(NeoObject noCentroCusto:noListaCentroCusto)
						{
							Long neoId = noCentroCusto.getNeoId();
							context.put("neoid", neoId.toString());
						}
					}
				}
				catch (SQLException e)
				{
					throw new WorkflowException("Problemas com a conexão ao sapiens");
				}
			}
			else
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				
				// Filtro por empresa
				QLEqualsFilter filterCodemp = new QLEqualsFilter("codemp", Long.valueOf(codemp));
				filter.addFilter(filterCodemp);
				
				// Nível Cliente
				QLEqualsFilter filterNivccu = new QLEqualsFilter("nivccu", 3);
				filter.addFilter(filterNivccu);
				
				
				// Regra da ORSEGUPS, se for codemp igual a 15 o claccu é 1615, se for diferente é 15 + código da empresa 
				String claccu = (codemp.equals("15")?"1615%":"15"+(codemp.length()==1?"0"+codemp:codemp));
				QLEqualsFilter filterClaccu = new QLEqualsFilter("claccu", claccu);
				filter.addFilter(filterClaccu);
				
				List<NeoObject> noListaCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filter);
				
				for(NeoObject noCentroCusto:noListaCentroCusto)
				{
					Long neoId = noCentroCusto.getNeoId();
					context.put("neoid", neoId.toString());
				}
			}
		}
		else
		{
			QLGroupFilter filter = new QLGroupFilter("AND");
			
			// Filtro por empresa
			filter.addFilter(new QLEqualsFilter("codemp", Long.valueOf(codemp)));
			filter.addFilter(new QLEqualsFilter("codccu", ewFGCPosto.findField("centroCusto.codccu").getValue().toString()));
			
			List<NeoObject> noListaCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filter);
			
			for(NeoObject noCentroCusto:noListaCentroCusto)
			{
				Long neoId = noCentroCusto.getNeoId();
				context.put("neoid", neoId.toString());
			}
		}
						
		context.put("servletPath", servletPath);
		
		return VelocityUtils.runTemplate("custom/orsegups/treeField.vm", context);
	}
}
