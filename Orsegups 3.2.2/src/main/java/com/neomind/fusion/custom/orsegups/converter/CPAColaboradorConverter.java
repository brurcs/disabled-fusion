package com.neomind.fusion.custom.orsegups.converter;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ait.controller.ProdutividadeAitController;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class CPAColaboradorConverter extends StringConverter
{
	private static final Log log = LogFactory.getLog(CPAColaboradorConverter.class);
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{

		Long idPai = field.getForm().getObjectId();
		NeoObject tarefa = PersistEngine.getNeoObject(idPai);
		EntityWrapper wrapper = new EntityWrapper(tarefa);

		Long cdViatura = (Long) wrapper.findValue("cdViatura");

		String colaborador = "NÃO ENCONTRADO";
		if (NeoUtils.safeIsNotNull(cdViatura))
		{
			try
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("viatura.cd_viatura", cdViatura));
				List<NeoObject> aitObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RHViaturaColaborador"), filter);
				PersistEngine.getEntityManager().flush();

				if (aitObjs != null && !aitObjs.isEmpty())
				{

					EntityWrapper aitObj = new EntityWrapper(aitObjs.get(0));

					Long cpf = (Long) aitObj.findValue("cpfInt");

					ProdutividadeAitController aitController = new ProdutividadeAitController();
					
					colaborador = aitController.retornaStringMatriculaEmpresaColaborador(cpf);
					
					colaborador = colaborador.replaceAll(";", " - ");
					
					if(colaborador == null)
						colaborador = "NÃO ENCONTRADO";
					
				}
			}
			catch (Exception e)
			{
				log.error(" RHViaturaColaborador erro ao encontrar cpf vinculado a viatura, código da viatura: "+cdViatura);
				colaborador = "NÃO ENCONTRADO";
			}
			return "<label for=\"colaborador\">"+colaborador+"</label>";

		}
		return colaborador;
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}