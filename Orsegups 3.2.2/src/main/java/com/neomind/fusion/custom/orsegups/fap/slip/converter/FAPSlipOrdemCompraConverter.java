package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.velocity.VelocityContext;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.VelocityUtils;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class FAPSlipOrdemCompraConverter extends StringConverter
{
	@Override
	protected String getHTMLInput(EFormField formField, OriginEnum originEnum)
	{
		String tr = "<tr id=\"0\" uid=\"list_solicitacaoCompra__itensCompra__\" entitytype=\"FAPSlipItensCompra\" idx=\"0\" style=\"visibility: visible;\">\r\n" + 
				"		<td colspan=\"1\" class=\"\" style=\"white-space: normal;\">\r\n" + 
				"				<b>${produto}</b> ${tipoProduto}<br>\r\n" + 
				"				${centroCusto}<br>\r\n" + 
				"				${motivoRastreamento}<br>\r\n" +
				"				${tipoOC}\r\n" + 
				"				<div style=\"display: table-cell;\">\r\n" + 
				"					<section style=\"display:${displayTimeline};\" class=\"cd-horizontal-timeline\">\r\n" + 
				"						<div class=\"timeline\">\r\n" + 
				"							<div class=\"events-wrapper\">\r\n" + 
				"								<div class=\"events\">\r\n" + 
				"									<ol>\r\n" + 
				"										${timeline}" +
				"									</ol>\r\n" + 
				"\r\n" + 
				"									<span class=\"filling-line\" aria-hidden=\"true\"></span>\r\n" + 
				"								</div> <!-- .events -->\r\n" + 
				"							</div> <!-- .events-wrapper -->\r\n" + 
				"						</div> <!-- .timeline -->\r\n" + 
				"					</section>\r\n" + 
				"				</div>\r\n" + 
				"			    <div style=\"display: table-cell; width: 10%\">\r\n" + 
				"					<table>\r\n" + 
				"				    	<thead>\r\n" +
				"				    	    <tr>\r\n" + 
				"				        	    <th colspan=\"5\">Historico de Compra</th>\r\n" + 
				"				        	</tr>\r\n" + 
				"				    	    <tr>\r\n" + 
				"				        	    <th>OC</th>\r\n" + 
				"				        	    <th>Dat. Compra</th>\r\n" + 
				"				        	    <th>Fornecedor</th>\r\n" + 
				"				        	    <th>Qtd</th>\r\n" + 
				"				        	    <th>Valor</th>\r\n" +
				"				        	</tr>\r\n" + 
				"				        </thead>\r\n" +
				"				        <tbody>\r\n" +
				"							${historicoItem}" +
				"				        </tbody>\r\n" +
				"					</table>\r\n" + 
				"				</div>\r\n" + 
				"		</td>\r\n" + 
				"		<td class=\"\"><span id=\"txt_solicitacaoCompra__itensCompra__quantidade__\">\r\n" + 
				"			<table style=\"margin: auto;\">\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"descricao\" style=\"cursor: default;border: 0px;text-align: center;\" title=\"Quantidade\">\r\n" + 
				"						${qtd} x\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"quantidade\" style=\"cursor: default;border: 0px;text-align: center;\" title=\"Valor\">\r\n" + 
				"						${valor}\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"dataEntrega\" style=\"cursor: default;border: 0px;text-align: center;\" title=\"Total\">\r\n" + 
				"						=  ${total}\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" + 
				"			</table>\r\n" + 
				"		</td>\r\n" + 
				"		<td class=\"\">${dataEntrega} </td>\r\n" + 
				"		<td class=\"\"><span id=\"txt_solicitacaoCompra__itensCompra__geral__\"></span>\r\n" + 
				"			<table>\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"regionais\" style=\"cursor: default;border: 0px;text-align: right;\" title=\"Regionais\">\r\n" + 
				"						Regionais\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"regionais\" style=\"cursor: default;border: 0px;\" title=\"Regionais\">\r\n" + 
				"						${regionais}\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"cdo\" style=\"cursor: default;border: 0px;text-align: right;\" title=\"CDO\">\r\n" + 
				"						CDO\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"cdo\" style=\"cursor: default;border: 0px;\" title=\"CDO\">\r\n" + 
				"						${cdo}\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"LAB01\" style=\"cursor: default;border: 0px;text-align: right;\" title=\"LAB01\">\r\n" + 
				"						Lab01\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"LAB01\" style=\"cursor: default;border: 0px;\" title=\"LAB01\">\r\n" + 
				"						${Lab01}\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"geral\" style=\"cursor: default;border: 0px;text-align: right;\" title=\"Geral\">\r\n" + 
				"						Geral\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"geral\" style=\"cursor: default;border: 0px;\" title=\"Geral\">\r\n" + 
				"						${geral}\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"\" style=\"cursor: default;border: 0px;text-align: center;\" title=\"\">\r\n" + 
				"						\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"\" style=\"cursor: default;border: 0px;\" title=\"\">\r\n" + 
				"						\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"itensReservados\" style=\"cursor: default;border: 0px;text-align: right;\" title=\"Itens Reservados\">\r\n" + 
				"						Itens Reservados\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"itensReservados\" style=\"cursor: default;border: 0px;\" title=\"Itens Reservados\">\r\n" + 
				"						${itensReservados}\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"estoqueMinimo\" style=\"cursor: default;border: 0px;text-align: right;\" title=\"Estoque Minimo\">\r\n" + 
				"						Estoque Minimo\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"estoqueMinimo\" style=\"cursor: default;border: 0px;\" title=\"Estoque Minimo\">\r\n" + 
				"						${estoqueMinimo}\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" + 
				"				<tr>\r\n" + 
				"					<td typename=\"estoqueMaximo\" style=\"cursor: default;border: 0px;text-align: right;\" title=\"Estoque Maximo\">\r\n" + 
				"						Estoque Maximo\r\n" + 
				"					</td>\r\n" + 
				"					<td typename=\"estoqueMaximo\" style=\"cursor: default;border: 0px;\" title=\"Estoque Maximo\">\r\n" + 
				"						${estoqueMaximo}\r\n" + 
				"					</td>\r\n" + 
				"				</tr>\r\n" +
				"			</table>\r\n" + 
				"		</td>\r\n" + 
				"	</tr>";
		
		VelocityContext context = new VelocityContext();
		
		StringBuilder itens = new StringBuilder();
		
		Long trCount = 1l;
		
		NeoObject noOrdemCompra = (NeoObject) formField.getValue();
		if (noOrdemCompra == null)
			return "";
		
		EntityWrapper wOrdemCompra = new EntityWrapper(noOrdemCompra);
		
		String tipoOC = wOrdemCompra.findGenericValue("tipoOC");
		tipoOC = tipoOC.equals("Manual") ? "<b style=\"color: red;\">" + tipoOC + "</b>" : "<b style=\"color: green;\">" + tipoOC + "</b>";
		
		BigDecimal valorTotalGeral = new BigDecimal(0);
		
		List<NeoObject> listItensCompra = wOrdemCompra.findGenericValue("itensCompra");
		for (NeoObject noItemCompra : listItensCompra)
		{
			Map<String, String> parametrosItem = new HashMap<String, String>();
			EntityWrapper wItemCompra = new EntityWrapper(noItemCompra);
			
			String codProduto = wItemCompra.findGenericValue("codProduto");  
			Long codEmp = wItemCompra.findGenericValue("codemp");
			/*String codDer = wItemCompra.findGenericValue("codder");
			System.out.println("Converter Ordem Compra - Inicio Busca Estoque: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			Map<String, Long> mapDadosEstoque = getEstoqueItem(codProduto, codEmp, codDer);
			System.out.println("Converter Ordem Compra - Fim Busca Estoque: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
			wItemCompra.setValue("regionais", mapDadosEstoque.get("regionais"));
			wItemCompra.setValue("cdo", mapDadosEstoque.get("CDO"));
			wItemCompra.setValue("lab01", mapDadosEstoque.get("LAB01"));
			wItemCompra.setValue("geral", mapDadosEstoque.get("geral"));
			wItemCompra.setValue("itensReservados", mapDadosEstoque.get("itensReservados"));
			wItemCompra.setValue("estoqueMax", mapDadosEstoque.get("estoqueMaximo"));
			wItemCompra.setValue("estoqueMin", mapDadosEstoque.get("estoqueMinimo"));*/
			
			String tipoOCItem = wItemCompra.findGenericValue("tipoOC");
			String displayTimeline = tipoOCItem.equals("Manual") ? "block" : "none";
			tipoOCItem = tipoOCItem.equals("Manual") ? "<b style=\"color: red;\">" + tipoOCItem + "</b>" : "<b style=\"color: green;\">" + tipoOCItem + "</b>";
			
			BigDecimal valorTotalItem = wItemCompra.findGenericValue("valorTotal");
			valorTotalGeral = valorTotalGeral.add(valorTotalItem);
			
			String abertRequisicao = wItemCompra.findGenericValue("abertRequisicao");
			
			String tipoProduto = "";
			QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("codpro", codProduto), new QLEqualsFilter("codemp", codEmp));
			NeoObject noPRO = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSEPRO"), qlGroup);
			if (noPRO != null)
			{
				EntityWrapper wPro = new EntityWrapper(noPRO);
				
				String usuCmpaut = wPro.findGenericValue("usu_cmpaut");
				if (usuCmpaut != null)
				{
					if (usuCmpaut.equalsIgnoreCase("N"))
						tipoProduto = " - <span style=\"color: red;\">Produto de Compra Manual</span>";
					else if (usuCmpaut.equalsIgnoreCase("S"))
						tipoProduto = " - <span style=\"color: green;\">Produto de Compra Automática</span>";
				}
			}
			
			
			if (!tipoOCItem.contains(tipoOC))
				parametrosItem.put("tipoOC", tipoOCItem);
			else 
				parametrosItem.put("tipoOC", "");
			parametrosItem.put("produto", (String) wItemCompra.findGenericValue("descricao"));
			parametrosItem.put("tipoProduto", tipoProduto);
			parametrosItem.put("centroCusto", (String) wItemCompra.findGenericValue("centroCusto"));
			parametrosItem.put("motivoRastreamento", (String) wItemCompra.findGenericValue("motivo"));
			parametrosItem.put("qtd", wItemCompra.findGenericValue("quantidade").toString());
			parametrosItem.put("dataEntrega", NeoUtils.safeDateFormat((GregorianCalendar)wItemCompra.findGenericValue("dataEntrega"), "dd/MM/yyyy"));
			parametrosItem.put("valor", wItemCompra.findGenericValue("valorUnitario").toString().replace(".", ","));
			parametrosItem.put("total", valorTotalItem.toString().replace(".", ","));
			parametrosItem.put("displayTimeline", displayTimeline);
			parametrosItem.put("trCount", (trCount++) + "");
			parametrosItem.put("nrRequisicao", (String) wItemCompra.findGenericValue("nrRequisicao"));
			parametrosItem.put("responsavelSolicitacaoRequisicao", (String) wItemCompra.findGenericValue("responsavelSolicitacaoRequisicao"));
			parametrosItem.put("responsavelAprovacaoRequisicao", (String) wItemCompra.findGenericValue("responsavelAprovacaoRequisicao"));
			parametrosItem.put("obsRequisicao", (String) wItemCompra.findGenericValue("obsRequisicao"));
			parametrosItem.put("abertRequisicao", abertRequisicao);
			parametrosItem.put("aprovRequisicao", (String) wItemCompra.findGenericValue("aprovRequisicao"));
			parametrosItem.put("nrSolicitacao", (String) wItemCompra.findGenericValue("nrSolicitacao"));
			parametrosItem.put("responsavelSolicitacao", (String) wItemCompra.findGenericValue("responsavelSolicitacao"));
			parametrosItem.put("responsavelAprovacaoSolicitacao", (String) wItemCompra.findGenericValue("responsavelAprovacaoSolicitacao"));
			parametrosItem.put("obsSolicitacao", (String) wItemCompra.findGenericValue("obsSolicitacao"));
			parametrosItem.put("abertSolicitacao", (String) wItemCompra.findGenericValue("abertSolicitacao"));
			parametrosItem.put("aprovSolicitacao", (String) wItemCompra.findGenericValue("aprovSolicitacao"));
			parametrosItem.put("timeline", buildTimeline(!(abertRequisicao == null || (abertRequisicao.trim().length() == 0))));
			parametrosItem.put("historicoItem", getHistoricoItem(wItemCompra));
			parametrosItem.put("regionais", wItemCompra.findGenericValue("regionais") != null ? wItemCompra.findGenericValue("regionais").toString() : "");
			parametrosItem.put("cdo", wItemCompra.findGenericValue("cdo") != null ? wItemCompra.findGenericValue("cdo").toString() : "");
			parametrosItem.put("Lab01", wItemCompra.findGenericValue("lab01") != null ? wItemCompra.findGenericValue("lab01").toString() : "");
			parametrosItem.put("geral", wItemCompra.findGenericValue("geral") != null ? wItemCompra.findGenericValue("geral").toString() : "");
			parametrosItem.put("itensReservados", wItemCompra.findGenericValue("itensReservados") != null ? wItemCompra.findGenericValue("itensReservados").toString() : "");
			parametrosItem.put("estoqueMaximo", wItemCompra.findGenericValue("estoqueMax") != null ? wItemCompra.findGenericValue("estoqueMax").toString() : "");
			parametrosItem.put("estoqueMinimo", wItemCompra.findGenericValue("estoqueMin") != null ? wItemCompra.findGenericValue("estoqueMin").toString() : "");
			
			StrSubstitutor sub = new StrSubstitutor(parametrosItem);
			itens.append(sub.replace(tr));
		}
		
		String obs = wOrdemCompra.findGenericValue("obs");
		obs = obs.replaceAll("\\r\\n", "<br>");
		
		context.put("itens", itens);
		context.put("empresa", wOrdemCompra.findGenericValue("empresa.codemp"));
		context.put("oc", wOrdemCompra.findGenericValue("ordemCompra"));
		context.put("fornecedor", wOrdemCompra.findGenericValue("fornecedor.nomfor"));
		context.put("obs", obs);
		context.put("dataOC", wOrdemCompra.findGenericValue("dataOC"));
		context.put("tipoOC", tipoOC);
		context.put("solicitante", wOrdemCompra.findGenericValue("solicitante"));
		context.put("formNeoId", noOrdemCompra.getNeoId());
		context.put("totalItens", parseMonetario(valorTotalGeral));

		return VelocityUtils.runTemplate("custom/orsegups/FAPOrdemCompra.vm", context);
	}
	
	@Override
	protected String getHTMLView(EFormField arg0, OriginEnum arg1)
	{
		return getHTMLInput(arg0, arg1);
	}
	
	private String getHistoricoItem(EntityWrapper wrapper)
	{
		StringBuilder historicoItem = new StringBuilder();
		
		Long count = 0l;
		
		List<NeoObject> listHistorico = wrapper.findGenericValue("historicoItem.historicoItem");
		if (listHistorico == null)
			return "";
			
		for (NeoObject noItemHistorico : listHistorico)
		{	
			if (count >= 3)
				break;
				
			Map<String, String> parametrosItem = new HashMap<String, String>();
			
			EntityWrapper wItemHistorico = new EntityWrapper(noItemHistorico);
			
			Long codOrdemCompra = wItemHistorico.findGenericValue("ordemCompra");
			Long codEmpresa = wItemHistorico.findGenericValue("codemp");
			
			QLGroupFilter qlGroupOC = new QLGroupFilter("AND", new QLEqualsFilter("numocp", codOrdemCompra), new QLEqualsFilter("codemp", codEmpresa));
			System.out.println("FAPSLIPORDEMCOMPRACONVERTER - codOrdem e codEmp: " + codOrdemCompra + " - " + codEmpresa);
			NeoObject noOrdemCompra = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE420OCP"), qlGroupOC);
			if (noOrdemCompra == null)
				continue;
			EntityWrapper wOrdemCompra = new EntityWrapper(noOrdemCompra);
			
			Long codFornecedor = wOrdemCompra.findGenericValue("codfor");
			
			NeoObject noFornecedor = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("ETABFOR"), new QLEqualsFilter("codfor", codFornecedor));
			String nomeFornecedor = new EntityWrapper(noFornecedor).findGenericValue("nomfor");
			String apelidoFornecedor = new EntityWrapper(noFornecedor).findGenericValue("apefor");
			
			parametrosItem.put("ordemCompra", codOrdemCompra.toString());
			parametrosItem.put("dataCompra", NeoDateUtils.safeDateFormat((GregorianCalendar)wItemHistorico.findGenericValue("dataCompra"), "dd/MM/yyyy"));
			parametrosItem.put("fornecedor", nomeFornecedor);
			parametrosItem.put("apeFornecedor", apelidoFornecedor.length() > 10 ? apelidoFornecedor.substring(0, 10) + "..." : apelidoFornecedor);
			parametrosItem.put("quantidade", wItemHistorico.findGenericValue("quantidade").toString());
			parametrosItem.put("valor", parseMonetario((BigDecimal) wItemHistorico.findGenericValue("valorUnitario")));
			
			String trHistoricoItem = "" +
					"<tr>\r\n" +
					"	<td typename=\"oc\" style=\"cursor: default;\" title=\"OC\">\r\n" + 
					"		${ordemCompra}\r\n" + 
					"	</td>\r\n" + 
					"	<td typename=\"data\" style=\"cursor: default;\" title=\"Data\">\r\n" + 
					"		${dataCompra}\r\n" + 
					"	</td>\r\n" + 
					"	<td typename=\"fornecedor\" style=\"cursor: default;\" title=\"${fornecedor}\">\r\n" + 
					"		${apeFornecedor}\r\n" + 
					"	</td>\r\n" + 
					"	<td typename=\"quantidade\" style=\"cursor: default;\" title=\"${quantidade}\">\r\n" + 
					"		${quantidade}\r\n" + 
					"	</td>\r\n" + 
					"	<td typename=\"valor\" style=\"cursor: default;\" title=\"${valor}\">\r\n" + 
					"		${valor}\r\n" + 
					"	</td>\r\n" + 
					"</tr>\r\n";
			
			StrSubstitutor sub = new StrSubstitutor(parametrosItem);
			historicoItem.append(sub.replace(trHistoricoItem));
			
			count++;
		}
		
		historicoItem.append("<tr><td style=\"margin: auto\" colspan=\"5\"><a onClick=\"javascript:viewItemFusion(" + ((NeoObject) wrapper.findGenericValue("historicoItem")).getNeoId() + ", " + 1 + ")\">Ver Mais</td></tr>");
		
		return historicoItem.toString();
	}
	
	private String buildTimeline(Boolean possuiRequisicao)
	{
		StringBuilder builder = new StringBuilder();
		
		if (possuiRequisicao)
		{
			builder.append("<li><a data-date=\"20/06/2016\"><div title=\"Nr. Requisicao: ${nrRequisicao} - &#013Requisitante: ${responsavelSolicitacaoRequisicao}\"><b>Requisicao</b><br>${abertRequisicao}</div></a></li>\r\n");
			builder.append("<li><a data-date=\"21/06/2016\"><div title=\"&#013Aprovador: ${responsavelAprovacaoRequisicao}\"><b>Aprovacao</b><br>${aprovRequisicao}</div></a></li>\r\n");
		}
		
		builder.append("<li><a data-date=\"24/06/2016\"><div title=\"Nr. Solicitacao: ${nrSolicitacao} - &#013Solicitante: ${responsavelSolicitacao}\"><b>Solicitacao</b><br>${abertSolicitacao}</div></a></li>\r\n");
		builder.append("<li><a data-date=\"30/06/2016\" id=\"li_last${trCount}\"><div title=\"&#013Aprovador: ${responsavelAprovacaoSolicitacao}\"><b>Aprovacao</b><br>${aprovSolicitacao}</div></a></li>");
		
		return builder.toString();
	}
	
	private String parseMonetario(BigDecimal value)
	{
		NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
		
		if (value != null)
		{
			BigDecimal returnValue = value;
			returnValue = returnValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			return monetaryFormat.format(returnValue);
		}
		
		return "R$ 0,00";
	}
}
