package com.neomind.fusion.custom.orsegups.integraRubi.adapter;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

public class RHIntegracaoBanco
{
	private static final Log log = LogFactory.getLog(RHIntegracaoBanco.class);

	public static Boolean insereDadosEstagio(EntityWrapper wForm, Long matricula, Long nrEmpresa)
	{
		Boolean result = false;

		try
		{
			if (matricula != null)
			{
				BigDecimal valorBolsa = wForm.findGenericValue("estagiario.valBol");
				Long tipoCol = wForm.findGenericValue("tpColaborador.codigo");
				String codNatEstagio = wForm.findGenericValue("estagiario.naturezaEstagio.codigo");
				Long codNivelEstagio = wForm.findGenericValue("estagiario.nivelEstagio.codigo");
				Long codInstEnsino = wForm.findGenericValue("estagiario.insEns.codoem");
				Long codAgenteIntegracao = wForm.findGenericValue("estagiario.ageInt.codoem");
				Long codEmpresaSupervisor = wForm.findGenericValue("estagiario.empCoo.numemp");
				Long codTipoSupervisor = wForm.findGenericValue("estagiario.tipCoo.codigo");
				Long codSupervisor = wForm.findGenericValue("estagiario.numCoo.numcad");
				Long cpfSupervisor = wForm.findGenericValue("estagiario.cpfCoo");
				GregorianCalendar dtPrevistaTermino = wForm.findGenericValue("estagiario.preTer");
				String dtPrevistaStr = NeoUtils.safeOutputString(dtPrevistaTermino != null ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dtPrevistaTermino.getTime()) : null);
				String nomeSupervisor = wForm.findGenericValue("estagiario.nomCoo");
				String areaAtuacao = wForm.findGenericValue("estagiario.areAtu");
				String apoliceSeguro = wForm.findGenericValue("estagiario.apoSeg");

				String consulta = " select NumCad from  R034ETG where NumEmp = " + nrEmpresa + " and TipCol = " + tipoCol + " and NumCad  = " + matricula;

				Integer nr = (Integer) selectColumn(consulta, "NumCad");

				//Registro ainda não criado
				if (nr == null || nr.intValue() == 0)
				{
					String query = "insert into R034ETG (NumEmp, TipCol, NumCad, natetg, nivetg, areatu, aposeg, valbol, preter, insens, ageint, empcoo, tipcoo, numcoo, nomcoo, cpfcoo) " + "values (" + nrEmpresa + ", " + tipoCol + ", " + matricula + ", '" + codNatEstagio + "', " + codNivelEstagio + ", '" + areaAtuacao + "', '" + apoliceSeguro + "', " + valorBolsa + ", '" + dtPrevistaStr + "', " + codInstEnsino + ", " + codAgenteIntegracao + ", " + codEmpresaSupervisor + ", " + codTipoSupervisor
							+ ", " + codSupervisor + ", '" + nomeSupervisor + "', '" + cpfSupervisor + "')";

					log.info(nrEmpresa + " - " + matricula + " - " + query);

					result = executeQuery(query);
				}
				else
				{
					//registro já existe
					result = true;
				}
			}
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereEstagiario\"");
			log.info("ERRO NO MÉTODO: \"insereEstagiario\"");
			e.printStackTrace();
			result = Boolean.FALSE;
		}

		return result;
	}

	public static Boolean insereHistoricoCracha(Long matricula, Long nrEmpresa, String DatIni)
	{
		Boolean result = false;

		try
		{
			String matriculaString = matricula.toString();
			String zeros = "";
			Integer contadorMatricula = 8 - matriculaString.length();

			for (Integer i = 0; i < contadorMatricula; i++)
				zeros = zeros + "0";

			matriculaString = zeros + matriculaString;

			String nrEmpresaString = nrEmpresa.toString();
			if (nrEmpresaString.length() == 1)
				nrEmpresaString = "0" + nrEmpresaString;

			String NumCra = "10" + nrEmpresaString + matriculaString;
			log.info("Cont NumCra - " + matriculaString + ": " + NumCra.length());

			String consulta = " select NumCad from R038HCH where  TipCra = 1 and NumEmp = " + nrEmpresa + " and TipCol = 1 and NumCad =" + matricula;
			Integer nr = (Integer) selectColumn(consulta, "NumCad");

			//Registro ainda não criado
			if (nr == null || nr == 0)
			{
				String query = " insert into R038HCH (TipCra, NumEmp, TipCol, NumCad, DatIni, NumCra, DatFim, StaAtu, StaAcc, " + "HorIni, ViaCra, HorFim, RecEPr) " + "values (1, " + nrEmpresa + ", 1, " + matricula + ", '" + DatIni + "', " + NumCra + ", '1900-12-31', 1, 1, " + "0, 0, 0, '')";
				log.info("Query inserção histórico do crachá: " + query);
				result = executeQuery(query);
			}
			else
				result = true;

		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereHistoricoCracha\"");
			e.printStackTrace();
			result = Boolean.FALSE;
		}

		return result;
	}

	@SuppressWarnings("deprecation")
	public static boolean executeQuery(String query)
	{
		Connection connSapiens = null;
		PreparedStatement pstData = null;
		Boolean retorno = false;
		
		log.info(query);

		try
		{
			connSapiens = PersistEngine.getConnection("VETORH");
			pstData = connSapiens.prepareStatement(query);
			int ret = pstData.executeUpdate();

			if (ret == 0)
				retorno = Boolean.FALSE;
			else
				retorno = Boolean.TRUE;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"executeQuery\"");
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

		return retorno;
	}

	public static boolean insereEventoHorista(Long matricula, Long nrEmpresa)
	{
		String consulta = " select NumCad from  r044fix where NumEmp = " + nrEmpresa + " and TipCol = 1 and NumCad  = " + matricula + " and CodEve = ";
		String consulta1 = consulta + "1";
		String consulta2 = consulta + "2";

		Integer nr = (Integer) selectColumn(consulta1, "NumCad");
		boolean sucesso = false;
		if (nr != null && nr > 0)
		{
			//se já foi cadastrado, não precisa cadastrar novamente
			sucesso = true;
		}
		else
		{
			String insert1 = " insert into r044fix (NumEmp, TipCol, NumCad, TabEve, CodEve, CodRat, DatIni, DatFim) " + "values (" + nrEmpresa + ", 1, " + matricula + ", 941, 1, 0, '1900-12-31 00:00:00.0', '1900-12-31 00:00:00.0')";
			sucesso = executeQuery(insert1);
		}

		if (sucesso)
		{
			nr = (Integer) selectColumn(consulta2, "NumCad");
			if (nr != null && nr > 0)
			{
				//se já foi cadastrado, não precisa cadastrar novamente
				sucesso = true;
			}
			else
			{
				String insert2 = " insert into r044fix (NumEmp, TipCol, NumCad, TabEve, CodEve, CodRat, DatIni, DatFim) " + "values (" + nrEmpresa + ", 1, " + matricula + ", 941, 2, 0, '1900-12-31 00:00:00.0', '1900-12-31 00:00:00.0')";
				sucesso = executeQuery(insert2);
			}
		}

		return sucesso;
	}

	@SuppressWarnings("deprecation")
	public static Object selectColumn(String query, String campo)
	{
		Connection connSapiens = null;
		PreparedStatement pstmt = null;
		Object nr = null;
		
		log.info(query);

		try
		{
			connSapiens = PersistEngine.getConnection("VETORH");
			pstmt = connSapiens.prepareStatement(query);

			ResultSet rs = pstmt.executeQuery();

			if (rs.next())
				nr = rs.getObject(campo);
			else
			{
				log.warn("NÃO EXISTE REGISTRO");
				return 0;
			}
		}
		catch (Exception e)
		{
			log.warn("");
			e.printStackTrace();
			return null;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstmt, null);
		}

		return nr;
	}

	public static Integer getFichaMedica(Long nrCadastro, Long nrEmpresa)
	{
		try
		{
			String query = " select codfic from R110FIC where numemp = " + nrEmpresa + " and numcad = " + nrCadastro;

			Integer nrFicha = (Integer) selectColumn(query, "codfic");

			return nrFicha;
		}
		catch (Exception e)
		{
			log.warn("");
			e.printStackTrace();
			return null;
		}
	}

	public static Boolean insereDadosVigilante(Long NumCad, Long NumEmp, String DtFormacao, String DtReciclagem, String OrgaoVig, String NumDrt, String DtValidadeCnv, String NumCnv, Boolean ExtTrans, Boolean ExtSegPes, Boolean cadastroCNV, Long codoem)
	{
		if (codoem == null)
			codoem = 0l;

		if (DtReciclagem == null)
			DtReciclagem = "1900-12-31";

		String USU_ExtTraVal = null;
		String USU_ExtSegPes = null;

		if (ExtSegPes)
			USU_ExtSegPes = "S";
		else
			USU_ExtSegPes = "N";

		if (ExtTrans)
			USU_ExtTraVal = "S";
		else
			USU_ExtTraVal = "N";

		String queryConsulta = " select USU_NumCad from USU_TCadCur where USU_NumEmp = " + NumEmp + " and USU_NumCad = " + NumCad;

		Integer qtd = (Integer) selectColumn(queryConsulta, "USU_NumCad");

		if (qtd == null || qtd.intValue() == 0)
		{
			String query = null;

			try
			{

				if (cadastroCNV)
					query = " INSERT INTO USU_TCadCur (USU_NumEmp, USU_TipCol, USU_NumCad, USU_OrgExp, USU_NumDip, USU_NumDrt, USU_ExtTraVal, " + "USU_ExtSegPes, USU_DatFor, USU_DatRec, USU_DatValCnv, USU_NuCaNaVi, USU_NumOem)" + "VALUES (" + NumEmp + ", 1, " + NumCad + ", '" + OrgaoVig + "', '" + NumDrt + "', '" + NumDrt + "', '" + USU_ExtTraVal + "', '" + USU_ExtSegPes + "', '" + DtFormacao + "', '" + DtReciclagem + "', '" + DtValidadeCnv + "', '" + NumCnv + "', " + codoem + ")";
				else
					query = " INSERT INTO USU_TCadCur (USU_NumEmp, USU_TipCol, USU_NumCad, USU_OrgExp, USU_NumDip, USU_NumDrt, USU_ExtTraVal, " + "USU_ExtSegPes, USU_DatFor, USU_DatRec, USU_NumOem)" + "VALUES (" + NumEmp + ", 1, " + NumCad + ", '" + OrgaoVig + "', '" + NumDrt + "', '" + NumDrt + "', '" + USU_ExtTraVal + "', '" + USU_ExtSegPes + "', '" + DtFormacao + "', '" + DtReciclagem + "', " + codoem + ")";

				log.info(NumEmp + " - " + NumCad + " - Query: " + query);

				return executeQuery(query);

			}
			catch (Exception e)
			{
				log.warn("ERRO NO MÉTODO: \"insereDadosVigilante\"");
				log.info("ERRO NO MÉTODO: \"insereDadosVigilante\"");
				e.printStackTrace();
				return Boolean.FALSE;
			}
		}
		else
			return true;

	}

	public static Boolean inserePersonalizados(Long matricula, Long USU_TipAdm, String USU_ColSde, String USU_CsgBrc, String USU_ValCom, Long USU_DepEmp, Long USU_FisRes, String USU_NomCra, Long nrEmpresa, Long tipCol)
	{
		boolean retorno = false;

		try
		{
			String query = " update r034fun set usu_tipadm = " + USU_TipAdm + ", usu_colsde = '" + USU_ColSde + "', usu_csgbrc = '" + USU_CsgBrc + "', usu_valcom = '" + USU_ValCom + "', usu_depemp = " + USU_DepEmp + ", usu_fisres = " + USU_FisRes + ", usu_nomcra = '" + USU_NomCra + "'  where numcad = " + matricula + " and numemp = " + nrEmpresa + " and tipcol = " + tipCol;

			if (matricula != null)
				return executeQuery(query);

		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			log.info("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			retorno = Boolean.FALSE;
		}

		return retorno;
	}

	public static Boolean insereFotoCracha(Long matricula, Long nrEmpresa, String fotemp)
	{
		try
		{
			String queryConsulta = " select NumCad from R034FOT where NumEmp = " + nrEmpresa + " and NumCad = " + matricula;
			Integer qtd = (Integer) selectColumn(queryConsulta, "NumCad");

			if (qtd == null || qtd.intValue() == 0)
			{
				String query = " INSERT INTO R034FOT (NumEmp, TipCol, NumCad, FotEmp) SELECT " + nrEmpresa + ", 1, " + matricula + "," + "BulkColumn FROM OPENROWSET( Bulk '" + fotemp + "', SINGLE_BLOB) AS BLOB";
				log.info("queryFoto: " + query);
				return executeQuery(query);
			}
			else
				return true;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	public static Boolean resgataSolicitante(String codUsuario, Long matricula, Long nrEmpresa, Long tipCol)
	{
		boolean retorno = false;

		if (codUsuario.equals("thays.santos"))
			codUsuario = "thays.passos";

		Integer codusu = null;

		try
		{
			String query = " select codusu from r999usu where nomusu like '" + codUsuario + "' ";

			codusu = selectColumn(query, "codusu") != null ? ((Number) selectColumn(query, "codusu")).intValue() : null;
			if (codusu != null)
			{
				String query2 = " update r034fun set usu_codusu = " + codusu + " where numcad = " + matricula + " and numemp = " + nrEmpresa + " and tipcol = " + tipCol;
				return executeQuery(query2);
			}
			else
				retorno = Boolean.FALSE;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = false;
		}

		return retorno;

	}

	public static Boolean insereSelecao(Long matricula, Long nrEmpresa, Long tipcol)
	{
		try
		{
			String consulta = " select numcad from  r034sel where numemp =" + nrEmpresa + " and tipcol = " + tipcol + " and numcad = " + matricula;
			Integer nr = (Integer) selectColumn(consulta, "numcad");

			//Registro ainda não criado
			if (nr == null || nr.intValue() == 0)
			{
				String query = " insert into r034sel (numemp, tipcol, numcad, codsel) values (" + nrEmpresa + ", " + tipcol + ", " + matricula + ", 2)";

				return executeQuery(query);
			}
			else
				return true;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereSelecao\"");
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	public static Boolean insereHistoricoDispensaPonto(EntityWrapper wForm, Long matricula, Long nrEmpresa)
	{
		try
		{
			Long tipoCol = wForm.findGenericValue("tpColaborador.codigo");
			String consulta = " select numcad from  R038NOT where numemp =" + nrEmpresa + " and tipcol = " + tipoCol + " and numcad = " + matricula;
			Integer nr = (Integer) selectColumn(consulta, "numcad");

			//Registro ainda não criado
			if (nr == null || nr.intValue() == 0)
			{
				GregorianCalendar dtAdmissao = wForm.findGenericValue("dtAdmissao");
				String dtAdmissaoStr = NeoUtils.safeOutputString(dtAdmissao != null ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dtAdmissao.getTime()) : null);

				String query = "insert into R038NOT (NumEmp, TipCol, NumCad, DatNot, SeqNot, TipNot) " + "values (" + nrEmpresa + ", " + tipoCol + ", " + matricula + ", '" + dtAdmissaoStr + "', 1, 11)";

				return executeQuery(query);
			}
			else
				return true;

		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereHistoricoDispensaPonto\"");
			log.info("ERRO NO MÉTODO: \"insereHistoricoDispensaPonto\"");
			e.printStackTrace();
			return false;
		}

	}

	public static Long resgataNumCad(String chave, Long numemp)
	{
		try
		{
			Integer matricula = (Integer) selectColumn(" select numcad from r034fun where numemp = '" + numemp + "' and apefun like '" + chave + "' ", "numcad");
			if (matricula != null)
				return new Long(matricula);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		
		return null;
	}

	public static Boolean insereListaVT(Long matricula, Long nrEmpresa, Long tipoColaborador, Long codEmpVT, Double numVT)
	{
		try
		{
			if (numVT == null)
				return false;

			if (codEmpVT != null && codEmpVT == 10)
				return validaVT(matricula, nrEmpresa, tipoColaborador, numVT, "usu_matjotur");
			else if (codEmpVT != null && codEmpVT == 20)
				return validaVT(matricula, nrEmpresa, tipoColaborador, numVT, "usu_matbiguacu");
			else if (codEmpVT != null && codEmpVT == 30)
				return validaVT(matricula, nrEmpresa, tipoColaborador, numVT, "usu_matestrela");
			else if (codEmpVT != null && codEmpVT == 40)
				return validaVT(matricula, nrEmpresa, tipoColaborador, numVT, "usu_matimp");
			else if (codEmpVT != null && codEmpVT == 50)
				return validaVT(matricula, nrEmpresa, tipoColaborador, numVT, "usu_matpasrap");
			else if (codEmpVT != null && codEmpVT == 110)
				return validaVT(matricula, nrEmpresa, tipoColaborador, numVT, "usu_codsic");
			else if (codEmpVT != null && codEmpVT == 120)
				return validaVT(matricula, nrEmpresa, tipoColaborador, numVT, "usu_codmet");
			else
				return Boolean.FALSE;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	private static Boolean validaVT(Long matricula, Long nrEmpresa, Long tipoColaborador, Double numVT, String campo)
	{
//		String consulta = " select numcad from  r034fun where " + campo + " =" + numVT + "and numcad = " + NumCad;
//		Integer nr = (Integer) selectColumn(consulta, "numcad");
//
//		//Registro ainda não criado
//		if (nr == null || nr.intValue() == 0)
//		{
//			String query = " update r034fun set " + campo + "  = " + numVT + " where numcad = " + NumCad;
//			return executeQuery(query);
//		}
//		else
//			return true;
		
		String query = " update r034fun set " + campo + "  = " + numVT + " where numcad = " + matricula + " and numemp = " + nrEmpresa + " and tipcol = " + tipoColaborador;
		return executeQuery(query);
	}

	public static Integer numAte(Long numemp, Long codmed)
	{
		Integer numAte = null;

		try
		{
			StringBuffer varname1 = new StringBuffer();
			varname1.append("  SELECT top 1 numate + 1 AS numate ");
			varname1.append("    FROM r110mam AS A ");
			varname1.append("   WHERE A.numemp = ");
			varname1.append(numemp);
			varname1.append("     AND A.codate = ");
			varname1.append(codmed);
			varname1.append("     AND NOT EXISTS (SELECT * ");
			varname1.append("                       FROM r110mam AS B ");
			varname1.append("                      WHERE B.numemp = ");
			varname1.append(numemp);
			varname1.append("                        AND B.codate = ");
			varname1.append(codmed);
			varname1.append("                        AND B.numate = A.numate + 1) ");
			varname1.append("     AND numate < (SELECT MAX(numate) FROM r110mam) ");
			varname1.append("order by 1");

			numAte = (Integer) selectColumn(varname1.toString(), "numate");
			if (numAte == null || numAte == 0)
				numAte = 1;
		}
		catch (Exception e)
		{
			log.warn("");
			e.printStackTrace();
			numAte = 0;
		}
		return numAte;
	}
}
