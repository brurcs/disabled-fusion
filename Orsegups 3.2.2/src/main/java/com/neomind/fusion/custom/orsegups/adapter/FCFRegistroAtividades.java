package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;


public class FCFRegistroAtividades implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(FAPRegistroAtividades.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("FCFRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			wRegistro.findField("responsavel").setValue(origin.getUser());
			
			wRegistro.findField("dataInicial").setValue(origin.getStartDate());
			wRegistro.findField("dataFinal").setValue(origin.getFinishDate());

			if (origin.getActivityName().equalsIgnoreCase("Iniciar Convocação de Funcionários"))
			{
				wRegistro.findField("atividade").setValue("Abriu a Tarefa");
				wRegistro.findField("prazo").setValue((GregorianCalendar) processEntity.findValue("prazo"));
				String txtDescricao = (String) processEntity.findValue("descricaoSolicitacao");
				wRegistro.findField("descricao").setValue(txtDescricao);
			}
			else if (origin.getActivityName().startsWith("Convocar Funcionários"))
			{
				Boolean convocacaoEfetuada = (Boolean) processEntity.findValue("convocacaoEfetuada");			
				if (convocacaoEfetuada)
				{
					wRegistro.findField("atividade").setValue("Convocou os Funcionários");
					String txtDescricao = (String) processEntity.findValue("descricaoObservacao");
					wRegistro.findField("descricao").setValue(txtDescricao);
					processEntity.setValue("descricaoObservacao", null);
				}
				else
				{
					wRegistro.findField("atividade").setValue("Não convocou os Funcionários");
					String txtDescricao = (String) processEntity.findValue("descricaoJustificativa");
					wRegistro.findField("descricao").setValue(txtDescricao);
					processEntity.setValue("descricaoJustificativa", null);
				}
			}
			else if (origin.getActivityName().startsWith("Aprovar Convocação"))
			{
				Boolean solicitanteAprova = (Boolean) processEntity.findValue("solicitanteAprova");
				
				if (solicitanteAprova)
				{
					wRegistro.findField("atividade").setValue("Aprovou convocação");
					String txtDescricao = (String) processEntity.findValue("observacaoAprovacao");
					wRegistro.findField("descricao").setValue(txtDescricao);
					processEntity.setValue("observacaoAprovacao", null);
				}
				else
				{
					wRegistro.findField("atividade").setValue("Desaprovou convocação");
					String txtDescricao = (String) processEntity.findValue("justificativaDesaprovacao");
					wRegistro.findField("descricao").setValue(txtDescricao);
					processEntity.setValue("justificativaDesaprovacao", null);
				}
			}
			else if (origin.getActivityName().startsWith("Convocação Desaprovada"))
			{
				Boolean convocacaoEfetuada = (Boolean) processEntity.findValue("convocacaoEfetuada");			
				if (convocacaoEfetuada)
				{
					wRegistro.findField("atividade").setValue("Convocou os Funcionários");
					String txtDescricao = (String) processEntity.findValue("descricaoObservacao");
					wRegistro.findField("descricao").setValue(txtDescricao);
					processEntity.setValue("descricaoObservacao", null);
				}
				else
				{
					wRegistro.findField("atividade").setValue("Não convocou os Funcionários");
					String txtDescricao = (String) processEntity.findValue("descricaoJustificativa");
					wRegistro.findField("descricao").setValue(txtDescricao);
					processEntity.setValue("descricaoJustificativa", null);
				}
			}
			
			PersistEngine.persist(registro);
			processEntity.findField("registroAtividades").addValue(registro);
		}
		catch(Exception e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
