package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCNDefinePrazoAguardarReceberEquipamentos implements AdapterInterface {
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		
		try {
			
			GregorianCalendar dataFinalMonitoramento = new GregorianCalendar();
			GregorianCalendar dataAguardarIniciativa = new GregorianCalendar();
			
			dataFinalMonitoramento = (GregorianCalendar) processEntity.findField("dataFinalMonitoramento").getValue();
			
			dataAguardarIniciativa = (GregorianCalendar) dataFinalMonitoramento.clone();
			
			dataAguardarIniciativa.add(GregorianCalendar.DAY_OF_MONTH, 10);
			
			processEntity.findField("dataAguardarIniciativa").setValue(dataAguardarIniciativa);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro ao atribuir a data de aguardo");
		}
		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}
