package com.neomind.fusion.custom.orsegups.seventh.bean;

import java.sql.Timestamp;

/**
 * Modelo dos eventos de arme/desarme utilizados pela rotina GravacaoOnDemand
 * Parametro operacao pode possuir os valores A para arme e D para desarme
 * 
 * @author mateus.batista
 *
 */
public class EventoArmeDesarme {

    private long cdCliente;
    private String idCentral;
    private String particao;
    private String idEmpresa;
    private Timestamp dtProcessado;
    private String operacao;
    private String servidorCFTV;
    private String portaCFTV;
    private String usuarioCFTV;
    private String senhaCFTV;
        
    public long getCdCliente() {
	return cdCliente;
    }

    public void setCdCliente(long cdCliente) {
	this.cdCliente = cdCliente;
    }

    public String getIdCentral() {
	return idCentral;
    }

    public void setIdCentral(String idCentral) {
	this.idCentral = idCentral;
    }

    public String getParticao() {
        return particao;
    }

    public void setParticao(String particao) {
        this.particao = particao;
    }

    public String getIdEmpresa() {
	return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
	this.idEmpresa = idEmpresa;
    }

    public Timestamp getDtProcessado() {
	return dtProcessado;
    }

    public void setDtProcessado(Timestamp dProcessado) {
	this.dtProcessado = dProcessado;
    }

    public String getOperacao() {
	return operacao;
    }

    public void setOperacao(String operacao) {
	this.operacao = operacao;
    }

    public String getServidorCFTV() {
        return servidorCFTV;
    }

    public void setServidorCFTV(String servidorCFTV) {
        this.servidorCFTV = servidorCFTV;
    }

    public String getPortaCFTV() {
        return portaCFTV;
    }

    public void setPortaCFTV(String portaCFTV) {
        this.portaCFTV = portaCFTV;
    }

    public String getUsuarioCFTV() {
        return usuarioCFTV;
    }

    public void setUsuarioCFTV(String usuarioCFTV) {
        this.usuarioCFTV = usuarioCFTV;
    }

    public String getSenhaCFTV() {
        return senhaCFTV;
    }

    public void setSenhaCFTV(String senhaCFTV) {
        this.senhaCFTV = senhaCFTV;
    }

}
