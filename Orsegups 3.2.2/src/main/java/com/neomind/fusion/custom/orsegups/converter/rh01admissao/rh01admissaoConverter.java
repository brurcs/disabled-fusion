package com.neomind.fusion.custom.orsegups.converter.rh01admissao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.FileConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.portal.PortalUtil;

public class rh01admissaoConverter extends FileConverter
{

    @Override
    public String getHTMLInput(EFormField field, OriginEnum origin)
    {
	String result = super.getHTMLInput(field, origin);
	String button = "";
	
	NeoFile nf = field.getValue() != null ? (NeoFile) field.getValue() : null;
	if(nf != null)
	{
	    // Validar extensoes para visualizar
	    List<String> extensoesPermitidas = new ArrayList<>();
	    extensoesPermitidas.add("pdf");
	    extensoesPermitidas.add("jpg");
	    extensoesPermitidas.add("jpeg");
	    extensoesPermitidas.add("png");

	    String extensaoArquivo = FilenameUtils.getExtension(nf.getName());
	    if(extensoesPermitidas.contains(extensaoArquivo.toLowerCase()))
	    {
		String link = PortalUtil.getBaseURL() + "servlet/rh01visualizar?id=" + nf.getNeoId();
		
		button = "&nbsp&nbsp&nbsp<a href=\""+link+"\" target=\"_blank\">VISUALIZAR</a>";
		
	    }
	}
	return result + button;
    }
}