package com.neomind.fusion.custom.orsegups.converter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ait.controller.ProdutividadeAitController;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentosAitVO;
import com.neomind.fusion.custom.orsegups.utils.ProdutividadeAASUPFIS;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;


public class CPAProdutividadeConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{

		Long idPai = field.getForm().getObjectId();
		NeoObject tarefa = PersistEngine.getNeoObject(idPai);
		EntityWrapper wrapper = new EntityWrapper(tarefa);
		DecimalFormat formatoReal = new DecimalFormat("#,##0.00");
		Long ait = (Long) wrapper.findValue("cdViatura");
		String calendarStr = (String) wrapper.findValue("competencia");
		StringBuilder textoTable = new StringBuilder();
		List<AtendimentosAitVO> produtividadeAASUPFISs = new ArrayList<AtendimentosAitVO>();
		produtividadeAASUPFISs = getListaProdutividade(calendarStr, ait.toString());
		if (NeoUtils.safeIsNotNull(produtividadeAASUPFISs) && !produtividadeAASUPFISs.isEmpty())
		{
			textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			textoTable.append("			<tr style=\"cursor: auto; white-space: normal\">");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Cliente</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal;\" >Evento</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Fechamento</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal;\" >Op</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal;\" >Deslocamento</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal;\" >Atendimento</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal;\" >DSL-AA</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal;\" >ATD-AA</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal;\" >TOT-AA</th>");
			textoTable.append("			</tr>");
			textoTable.append("			<tbody>");

			int count = 0;
			Double totalValorDSL = 0.0;
			Double totalValorATD = 0.0;
			Double totalDeslocamento = 0.0;
			Double totalAtendimento = 0.0;
			boolean flag = true;
			String aitAnt = "";
			for (AtendimentosAitVO aasupfisObj : produtividadeAASUPFISs)
			{
				Double valorDSL = 0.0;
				Double valorATD = 0.0;
				String aitAtual = aasupfisObj.getAit();

				if (aasupfisObj.getDeslocamento() != null && !aasupfisObj.getDeslocamento().isEmpty())
				{
					if (Integer.parseInt(aasupfisObj.getDeslocamento()) >= 0 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 10)
					{
						valorDSL = 2.0;
					}
					else if (Integer.parseInt(aasupfisObj.getDeslocamento()) > 10 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 15)
					{
						valorDSL = 1.0;
					}
					else if (Integer.parseInt(aasupfisObj.getDeslocamento()) > 15 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 20)
					{
						valorDSL = 0.50;
					}

					if (Integer.parseInt(aasupfisObj.getAtendimento()) >= 0 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 15)
					{
						valorATD = 1.0;
					}
					else if (Integer.parseInt(aasupfisObj.getAtendimento()) > 15 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 20)
					{
						valorATD = 0.50;
					}
					else if (Integer.parseInt(aasupfisObj.getAtendimento()) > 20 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 25)
					{
						valorATD = 0.25;
					}
				}
				if (aitAtual != null && !aitAtual.isEmpty() && aitAnt != null && !aitAnt.isEmpty() && !aitAtual.equalsIgnoreCase(aitAnt))
				{
					aitAnt = aitAtual;
					flag = true;
				}
			    if (aasupfisObj.getSituacao() != null && aasupfisObj.getSituacao().equals(1L))
				{
					textoTable.append("	<tr style=\"cursor: auto; white-space: normal\"> ");
				}
				else
				{
					textoTable.append("	<tr style=\"cursor: auto; white-space: normal\"> ");
				}

				textoTable.append("	<td style=\"cursor: auto; white-space: normal;\" >" + aasupfisObj.getCliente() + "</td> ");
				textoTable.append("	<td style=\"cursor: auto; white-space: normal;\" >" + aasupfisObj.getCdEvento() + "</td> ");
				textoTable.append("	<td style=\"cursor: auto; white-space: normal;\" >" + aasupfisObj.getDtFechamento() + "</td> ");
				textoTable.append("	<td style=\"cursor: auto; white-space: normal;\" >" + aasupfisObj.getOp() + "</td> ");
				textoTable.append("	<td style=\"cursor: auto; white-space: normal;\" >" + aasupfisObj.getDeslocamento() + "</td> ");
				textoTable.append("	<td style=\"cursor: auto; white-space: normal;\" >" + aasupfisObj.getAtendimento() + "</td> ");
				textoTable.append(" <td style=\"white-space: normal; text-align: right\">" + NeoUtils.safeFormat(formatoReal, valorDSL) + "</td>");
				textoTable.append(" <td style=\"white-space: normal; text-align: right\">" + NeoUtils.safeFormat(formatoReal, valorATD) + "</td>");
				textoTable.append(" <td style=\"white-space: normal; text-align: right\">" + NeoUtils.safeFormat(formatoReal, valorDSL + valorATD) + "</td>");
				textoTable.append(" </tr> ");

				totalDeslocamento += Integer.parseInt(aasupfisObj.getDeslocamento());
				totalAtendimento += Integer.parseInt(aasupfisObj.getAtendimento());
				totalValorDSL += valorDSL;
				totalValorATD += valorATD;
				count++;

			}

			Double subTotalDeslocamento = totalDeslocamento / count;
			Double subTotalAtendimento = totalAtendimento / count;

			textoTable.append(" <tr class=\"info\"> ");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\"></td>");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\"></td> ");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\"></td> ");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\">" + count + "</td> ");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\">" + subTotalDeslocamento + "</td> ");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\">" + subTotalAtendimento + "</td> ");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\">" + NeoUtils.safeFormat(formatoReal, totalValorDSL) + "</td>");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\">" + NeoUtils.safeFormat(formatoReal, totalValorATD) + "</td> ");
			textoTable.append(" <td style=\"white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;\">" + NeoUtils.safeFormat(formatoReal, totalValorDSL + totalValorATD) + "</td>");
			textoTable.append(" </tr> ");
			textoTable.append(" </tbody>");
			textoTable.append(" </table>");

		}

		return textoTable.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}

	protected List<AtendimentosAitVO> getListaProdutividade(String competencia, String cdViatura)
	{

		List<AtendimentosAitVO> atendimentosAitVOs = null;
		try
		{
			ProdutividadeAitController produtividadeAitController = new ProdutividadeAitController();
			String array[] = competencia.split("/");

			String mes = array[0];
			String ano = array[1];

			atendimentosAitVOs = produtividadeAitController.listaAtendimentos(cdViatura, mes, ano);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return atendimentosAitVOs;
	}

}