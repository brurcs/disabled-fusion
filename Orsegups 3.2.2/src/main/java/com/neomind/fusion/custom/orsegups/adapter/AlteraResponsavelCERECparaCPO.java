package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
/**
 * Adapter responsável por definir os executores responsáveis pela Hierarquia do CPO
 * no Fluxo de Inadimplência - C025
 * 
 * @author herisson.ferreira
 *
 */
public class AlteraResponsavelCERECparaCPO implements AdapterInterface {
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		
		System.out.println(" #### INICIO DA EXECUÇÃO: Adapter AlteraResponsavelCERECparaCPO "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		
		try {
			
			String papelSupervisor = "FCNSupervisorCPO";
			String papelGerente = "FCNGerenteCPO";
			String papelDiretor = "FCNDiretorCPO";
			
			NeoPaper supervisorCPO = OrsegupsUtils.getPaper(papelSupervisor);
			NeoPaper gerenteCPO = OrsegupsUtils.getPaper(papelGerente);
			NeoPaper diretorCPO = OrsegupsUtils.getPaper(papelDiretor);
			
			processEntity.findField("supervisorFCN").setValue(supervisorCPO); // SUPERVISÃO
			processEntity.findField("gerenteFCN").setValue(gerenteCPO); // GERÊNCIA
			processEntity.findField("diretorFCN").setValue(diretorCPO); // DIRETORIA
			
		} catch (WorkflowException e) {
			e.printStackTrace();
			System.out.println("[Class: AlteraResponsavelCERECparaCPO] Não foi possível definir os responsáveis pelo CPO no Fluxo. Contate a TI para verificação.");
		} finally {
			System.out.println(" #### FIM DA EXECUÇÃO: Adapter AlteraResponsavelCERECparaCPO "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}
	

}
