package com.neomind.fusion.custom.orsegups.menu;

public class SubMenuVO
{
	private String MenuRaiz;
	private String label;
	private String href;
	private String icon;
	private String css;
	
	public String getMenuRaiz()
	{
		return MenuRaiz;
	}
	public void setMenuRaiz(String menuRaiz)
	{
		MenuRaiz = menuRaiz;
	}
	public String getLabel()
	{
		return label;
	}
	public void setLabel(String label)
	{
		this.label = label;
	}
	public String getHref()
	{
		return href;
	}
	public void setHref(String href)
	{
		this.href = href;
	}
	public String getIcon()
	{
		return icon;
	}
	public void setIcon(String icon)
	{
		this.icon = icon;
	}
	public String getCss()
	{
		return css;
	}
	public void setCss(String css)
	{
		this.css = css;
	}
}