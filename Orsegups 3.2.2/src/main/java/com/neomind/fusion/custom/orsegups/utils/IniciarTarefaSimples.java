package com.neomind.fusion.custom.orsegups.utils;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;

public class IniciarTarefaSimples
{
	public String abrirTarefa (String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo)
	{
		/**
		 * @author neomind willian.mews - Alterado para fazer o overload do método já existente
		 * @date 11/03/2015
		 */
		
		return abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo, null);
		
		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
	}
	
	public String abrirTarefaReturnNeoIdCode (String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo)
	{
		
		return abrirTarefaReturnNeoIdCode(solicitante, executor, titulo, descricao, origem, avanca, prazo, null);
		
	}
	
	public String abrirTarefa (String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo, NeoFile file)
	{

		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", solicitante));
		
		if (NeoUtils.safeIsNull(objSolicitante)) 
		{
			return "Erro #1 - Solicitante não encontrado";
		}
		
		boolean isAvanca = true;
		
		if (avanca != null && (avanca.trim().equals("nao") || avanca.trim().equals("0") || avanca.trim().equals("no")|| avanca.trim().equals("off"))) 
		{
			isAvanca = false;
		}
		
		NeoUser objExecutor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));

		if (isAvanca) 
		{
			if (NeoUtils.safeIsNull(objExecutor)) 
			{
				return "Erro #2 - Executor não encontrado";
			}
		} 
		
		if (NeoUtils.safeIsNull(titulo)) 
		{
			return "Erro #3 - Título não informado";
		}
		
		if (NeoUtils.safeIsNull(descricao)) 
		{
			return "Erro #4 - Descrição não informada";
		}
		
		InstantiableEntityInfo eOrigem = AdapterUtils.getInstantiableEntityInfo("origemTarefa");	
		NeoObject objOrigem = (NeoObject) PersistEngine.getObject(eOrigem.getEntityClass(), new QLEqualsFilter("codigo", Long.valueOf(origem)));
		
		if (NeoUtils.safeIsNull(objOrigem))
		{
			return "Erro #5 - Origem não encontrada";
		}
		
		if (NeoUtils.safeIsNull(prazo))
		{
			return "Erro #6 - Prazo não informado";
		}
		
		final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));
		
		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 11/03/2015
		 */
		
		//final WFProcess processo = WorkflowService.startProcess(pm, false, objSolicitante);
		//processo.setSaved(true);
		InstantiableEntityInfo objFormPrincipal = AdapterUtils.getInstantiableEntityInfo("tarefa");
		final NeoObject formularioProcesso = objFormPrincipal.createNewInstance();//processo.getEntity();
		final EntityWrapper formularioWrapper = new EntityWrapper(formularioProcesso);
		
		formularioWrapper.findField("Solicitante").setValue(objSolicitante);
		formularioWrapper.findField("Executor").setValue(objExecutor);
		formularioWrapper.findField("Titulo").setValue(titulo);
		formularioWrapper.findField("DescricaoSolicitacao").setValue(descricao);
		formularioWrapper.findField("Prazo").setValue(prazo);
		formularioWrapper.findField("origem").setValue(objOrigem);
		formularioWrapper.findField("dataSolicitacao").setValue(new GregorianCalendar());
		formularioWrapper.findField("AnexoSolicitante").setValue(file);
		
		PersistEngine.persist(formularioProcesso);
		
		/*
		 * @author danilo.silva - alterada forma de abrir processo 
		 * @date 01/11/2017
		 */
		String tarefa = null;
		try
		{
			tarefa = OrsegupsWorkflowHelper.iniciaProcesso(pm, formularioProcesso, false, objSolicitante, isAvanca, null);
			//new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(objSolicitante, isAvanca);
			System.out.println("[Iniciar Tarefa Simples] - "+tarefa+" - OK");
		}
		catch(Exception e)
		{
			System.out.println("[Iniciar Tarefa Simples] - "+tarefa+" - ERR");
			e.printStackTrace();
			System.out.println(e.getMessage());
			return "Erro #7 - Erro ao avançar a primeira tarefa";
		}
		
		return tarefa;
		
		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
	}
	
	public String abrirTarefaReturnNeoIdCode (String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo, NeoFile file)
	{

		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", solicitante));
		
		if (NeoUtils.safeIsNull(objSolicitante)) 
		{
			return "Erro #1 - Solicitante não encontrado";
		}
		
		boolean isAvanca = true;
		
		if (avanca != null && (avanca.trim().equals("nao") || avanca.trim().equals("0") || avanca.trim().equals("no")|| avanca.trim().equals("off"))) 
		{
			isAvanca = false;
		}
		
		NeoUser objExecutor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));

		if (isAvanca) 
		{
			if (NeoUtils.safeIsNull(objExecutor)) 
			{
				return "Erro #2 - Executor não encontrado";
			}
		} 
		
		if (NeoUtils.safeIsNull(titulo)) 
		{
			return "Erro #3 - Título não informado";
		}
		
		if (NeoUtils.safeIsNull(descricao)) 
		{
			return "Erro #4 - Descrição não informada";
		}
		
		InstantiableEntityInfo eOrigem = AdapterUtils.getInstantiableEntityInfo("origemTarefa");	
		NeoObject objOrigem = (NeoObject) PersistEngine.getObject(eOrigem.getEntityClass(), new QLEqualsFilter("codigo", Long.valueOf(origem)));
		
		if (NeoUtils.safeIsNull(objOrigem))
		{
			return "Erro #5 - Origem não encontrada";
		}
		
		if (NeoUtils.safeIsNull(prazo))
		{
			return "Erro #6 - Prazo não informado";
		}
		
		final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));
		
		//final WFProcess processo = WorkflowService.startProcess(pm, false, objSolicitante);
		InstantiableEntityInfo objFormPrincipal = AdapterUtils.getInstantiableEntityInfo("tarefa");
		final NeoObject formularioProcesso = objFormPrincipal.createNewInstance();//processo.getEntity();
		final EntityWrapper formularioWrapper = new EntityWrapper(formularioProcesso);
		
		formularioWrapper.findField("Solicitante").setValue(objSolicitante);
		formularioWrapper.findField("Executor").setValue(objExecutor);
		formularioWrapper.findField("Titulo").setValue(titulo);
		formularioWrapper.findField("DescricaoSolicitacao").setValue(descricao);
		formularioWrapper.findField("Prazo").setValue(prazo);
		formularioWrapper.findField("origem").setValue(objOrigem);
		formularioWrapper.findField("dataSolicitacao").setValue(new GregorianCalendar());
		formularioWrapper.findField("AnexoSolicitante").setValue(file);
		
 		String tarefa = null;
		try
		{
			tarefa = OrsegupsWorkflowHelper.iniciaProcesso(pm, formularioProcesso, false, objSolicitante, isAvanca, null);
			//new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(objSolicitante, isAvanca);
			System.out.println("[Iniciar Tarefa Simples] - "+tarefa+" - OK");
		}
		catch(Exception e)
		{
			System.out.println("[Iniciar Tarefa Simples] - "+tarefa+" - ERR");
			e.printStackTrace();
			return "Erro #7 - Erro ao avançar a primeira tarefa";
		}
		
		//return processo.getNeoId()+";"+processo.getCode();
		return OrsegupsProcessUtils.findProcessNeoID(pm.getName(), tarefa)+";"+tarefa;

	}
	
	public String abrirTarefaReturnNeoId(String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo)
	{
		return abrirTarefaReturnNeoId(solicitante, executor, titulo, descricao, origem, avanca, prazo, null);
	}
	
	public String abrirTarefaReturnNeoId(String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo, NeoFile file)
	{

		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", solicitante));
		
		if (NeoUtils.safeIsNull(objSolicitante)) 
		{
			return "Erro #1 - Solicitante não encontrado";
		}
		
		boolean isAvanca = true;
		
		if (avanca != null && (avanca.trim().equals("nao") || avanca.trim().equals("0") || avanca.trim().equals("no")|| avanca.trim().equals("off"))) 
		{
			isAvanca = false;
		}
		
		NeoUser objExecutor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));

		if (isAvanca) 
		{
			if (NeoUtils.safeIsNull(objExecutor)) 
			{
				return "Erro #2 - Executor não encontrado";
			}
		} 
		
		if (NeoUtils.safeIsNull(titulo)) 
		{
			return "Erro #3 - Título não informado";
		}
		
		if (NeoUtils.safeIsNull(descricao)) 
		{
			return "Erro #4 - Descrição não informada";
		}
		
		InstantiableEntityInfo eOrigem = AdapterUtils.getInstantiableEntityInfo("origemTarefa");	
		NeoObject objOrigem = (NeoObject) PersistEngine.getObject(eOrigem.getEntityClass(), new QLEqualsFilter("codigo", Long.valueOf(origem)));
		
		if (NeoUtils.safeIsNull(objOrigem))
		{
			return "Erro #5 - Origem não encontrada";
		}
		
		if (NeoUtils.safeIsNull(prazo))
		{
			return "Erro #6 - Prazo não informado";
		}
		
		final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));
		
		//final WFProcess processo = WorkflowService.startProcess(pm, false, objSolicitante);
		InstantiableEntityInfo objFormPrincipal = AdapterUtils.getInstantiableEntityInfo("tarefa");
		final NeoObject formularioProcesso = objFormPrincipal.createNewInstance();//processo.getEntity();
		final EntityWrapper formularioWrapper = new EntityWrapper(formularioProcesso);
		
		formularioWrapper.findField("Solicitante").setValue(objSolicitante);
		formularioWrapper.findField("Executor").setValue(objExecutor);
		formularioWrapper.findField("Titulo").setValue(titulo);
		formularioWrapper.findField("DescricaoSolicitacao").setValue(descricao);
		formularioWrapper.findField("Prazo").setValue(prazo);
		formularioWrapper.findField("origem").setValue(objOrigem);
		formularioWrapper.findField("dataSolicitacao").setValue(new GregorianCalendar());
		formularioWrapper.findField("AnexoSolicitante").setValue(file);
		
 		Long processNeoId = null;
		try
		{
			processNeoId = OrsegupsWorkflowHelper.iniciaProcessoNeoId(pm, formularioProcesso, false, objSolicitante, isAvanca, null);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "Erro #7 - Erro ao avançar a primeira tarefa";
		}
		
		//return processo.getNeoId()+";"+processo.getCode();
		return processNeoId.toString();

	}
}
