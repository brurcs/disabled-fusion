package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.ContratoLogUtils;
import com.neomind.fusion.custom.orsegups.contract.vo.DadosDebitoSapiensVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class RotinaAbreC035Automatico implements CustomJobAdapter {
	

	@Override
	public void execute(CustomJobContext ctx) {
		
		String tarefaC001 = null; 
		
		Long codemp = null;
		Long codfil = null; 
		Long numctr = null;
		Long codcli = null;
		
		String portador = null;
		String banco = null;
		String agencia = null;
		String conta = null;
		String txtUsuarioSolicitante = null;
		
		
		
		try{
			System.out.println("### INICIANDO ABERTURA AUTOMATICA DE TAREFAS C035 lançadas pelo Fluxo de Contratos ###");
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("abriuTarefa", false));
			List<NeoObject> filaAbertura = PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCFilaAberturaC035"), gp);
			if (filaAbertura != null && !filaAbertura.isEmpty()){
				for (NeoObject objeto: filaAbertura){
					EntityWrapper wObjeto = new EntityWrapper(objeto);
					
					codemp = 		NeoUtils.safeLong(NeoUtils.safeOutputString( wObjeto.getValue("codemp")));		
					codfil = 		NeoUtils.safeLong(NeoUtils.safeOutputString( wObjeto.getValue("codfil")));		
					numctr = 		NeoUtils.safeLong(NeoUtils.safeOutputString( wObjeto.getValue("numctr")));		
					codcli = 		NeoUtils.safeLong(NeoUtils.safeOutputString( wObjeto.getValue("codcli")));		
					tarefaC001 = 	NeoUtils.safeOutputString( wObjeto.getValue("tarefaC001"));
					banco = 		NeoUtils.safeOutputString( wObjeto.getValue("banco"));		
					portador = 		NeoUtils.safeOutputString( wObjeto.getValue("portador"));
					agencia =  		NeoUtils.safeOutputString( wObjeto.getValue("agencia"));		
					conta =    		NeoUtils.safeOutputString( wObjeto.getValue("conta"));
					txtUsuarioSolicitante = NeoUtils.safeOutputString( wObjeto.getValue("usuarioSolicitante"));
					
					NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", txtUsuarioSolicitante ) );
					NeoUser usuarioResponsavel = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "camila.silva") );
					
					DadosDebitoSapiensVO debitoVO = new DadosDebitoSapiensVO(portador, banco, agencia, conta);
					
					String tarefa = iniciaProcesso(usuarioSolicitante, usuarioResponsavel, debitoVO, tarefaC001, codemp, codfil, numctr, codcli);
					if (NeoUtils.safeOutputString(tarefa).equals("CtrNaoIntegrado")){
						// contratoa ainda não integrado, então passa batido e deixa pra testar na proxima vez.
						System.out.println("Contrato não integrado: ");
					}else{
						wObjeto.setValue("abriuTarefa",true);
						PersistEngine.persist(objeto);
					}
					
					
					
				}
			}
			System.out.println("### FINALIZANDO ABERTURA AUTOMATICA DE TAREFAS C035 lançadas pelo Fluxo de Contratos ###");
		}catch(WorkflowException e){
			Long key = GregorianCalendar.getInstance().getTimeInMillis();
			System.out.println("["+key+"] Erro ao processa Job RotinaAbreC035Automatico  " + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processar Job Verifique no log por ["+key+"] para maiores detalhes");
		}
		
	}
	
	/**
	 * Inicia Processo do tipo C035
	 * @param oEformProcesso
	 * @param solicitante
	 * @param responsavel
	 * @return
	 */
	public static String iniciaProcesso( NeoUser solicitante, NeoUser responsavel, DadosDebitoSapiensVO dadosDebito, String tarefaC001, Long codemp, Long codfil, Long numctr, Long codcli)
	{
		String result = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try{
			
			NeoObject oEformProcesso = AdapterUtils.createNewEntityInstance("CDACadastroDebitoAutomatico");
			EntityWrapper wEformProcesso = new EntityWrapper(oEformProcesso);
			
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("usu_codemp", codemp));
			gp.addFilter(new QLEqualsFilter("usu_codfil", codfil));
			gp.addFilter(new QLEqualsFilter("usu_numctr", numctr));
			gp.addFilter(new QLEqualsFilter("codcli", codcli));
			
			System.out.println("["+key+"] - Parametros da Pesquisa usu_codemp= " + codemp + ", usu_codfil="+codfil+", usu_numctr="+numctr + ", codcli="+codcli);
			
			// vfus_ctrcce
			List<NeoObject> contrato = PersistEngine.getObjects(AdapterUtils.getEntityClass("VFUSCTRSAP"), gp);
			if (contrato == null || (contrato != null && contrato.isEmpty())){
				System.out.println("["+key+"] - Contrato não integrado ainda na tabela TIDB.vfus_ctrcce ");
				return "CtrNaoIntegrado";
			}
			
			for (NeoObject ctr : contrato){
				wEformProcesso.setValue("contrato", ctr);
				System.out.println("["+key+"] - Contrato setado: "+numctr);
			}
			
			String banco 	= NeoUtils.safeOutputString(dadosDebito.getBanco());
			String portador = NeoUtils.safeOutputString(dadosDebito.getPrimeiroPortador());
			String agencia 	= NeoUtils.safeOutputString(dadosDebito.getAgencia());
			String conta 	= NeoUtils.safeOutputString(dadosDebito.getContacorrente());
			
			
			
			System.out.println("["+key+"] - Banco: " + banco + ", Agencia: " + agencia + ", Conta:" +  conta);
			
			//dados sobre o débito
			wEformProcesso.setValue("portador", portador);
			wEformProcesso.setValue("banco", banco);
			wEformProcesso.setValue("agencia", agencia);
			wEformProcesso.setValue("contaCorrente", conta);
			wEformProcesso.setValue("tarefaC001",tarefaC001);
			
			wEformProcesso.setValue("fechouAcordo", true);
			wEformProcesso.setValue("recebeuForm",true);
			wEformProcesso.setValue("negociacao", "Via executivos de vendas pelo fluxo de Contratos - C001. tarefa:"+tarefaC001);
			
			QLGroupFilter gp2 = new QLGroupFilter("AND");
			gp2.addFilter(new QLEqualsFilter("codigo", 1L)); // e-mail
			
			List<NeoObject> formasEnvio = PersistEngine.getObjects(AdapterUtils.getEntityClass("CDAFormaDeEnvio"), gp2);
			
			for (NeoObject envio : formasEnvio){
				wEformProcesso.setValue("formaEnvio", envio);
			}
			
			
			// abertura do processo
			
			QLEqualsFilter equal = new QLEqualsFilter("Name", "C035 - Cadastro de Débito Automático");
			ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
			
			WFProcess proc = processModel.startProcess(oEformProcesso, false, null, null, null, null, responsavel);
			System.out.println("[C035] ["+key+"] -  Tarefa " + proc.getCode() + " Aberta via fluxo de contratos.");
			proc.setRequester(solicitante);
			proc.setSaved(true);
			// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
			PersistEngine.persist(proc);
			PersistEngine.commit(true);
		
			result = proc.getCode();
		}catch(Exception e){
			e.printStackTrace();
			result = null;
		}
		
		
		return result;
	}

}
