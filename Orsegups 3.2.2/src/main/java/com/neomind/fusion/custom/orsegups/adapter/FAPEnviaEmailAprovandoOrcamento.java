package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class FAPEnviaEmailAprovandoOrcamento implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	ArrayList<String> listaEmails = new ArrayList<String>();
	ArrayList<String> listaEmailsCopia = new ArrayList<String>();
	try {
	    Long codigoAplicacaoPagamento = (Long) (Long) processEntity.findValue("aplicacaoPagamento.codigo");
	    String code = (String) processEntity.findValue("wfprocess.code");
	    String emailRemetente = (String) processEntity.findValue("solicitanteOrcamento.email");
	    NeoObject fornecedor = (NeoObject) processEntity.findValue("fornecedor");
	    String nomeFornecedor = "";
	    Long cgccpf = 0L;

	    if (fornecedor != null) {
		EntityWrapper wrapperFornecedor = new EntityWrapper(fornecedor);
		String emailForn = (String) wrapperFornecedor.findValue("fornecedor.intnet");
		cgccpf = (Long) wrapperFornecedor.findValue("fornecedor.cgccpf");
		nomeFornecedor = (String) wrapperFornecedor.findValue("fornecedor.nomfor");
		if (emailForn != null) {
		    StringTokenizer tokenEmailStr = new StringTokenizer(emailForn, ";");
		    while (tokenEmailStr.hasMoreElements()) {
			String email = (String) tokenEmailStr.nextElement();
			email = email.trim();
			if (email.contains("@")) {
			    listaEmails.add(email);
			}
		    }
		} else {
		    if ((codigoAplicacaoPagamento == 2L) || //Manutenção Alarme e CFTV - Mercado Privado
			(codigoAplicacaoPagamento == 4L) || //Manutenção - Rastreador Veicular
			(codigoAplicacaoPagamento == 5L) || //Instalação Alarme e CFTV - Mercado Privado
			(codigoAplicacaoPagamento == 7L) || //Instalação - Rastreador Veicular
			(codigoAplicacaoPagamento == 8L) || //Instalação - Portaria Remota
			(codigoAplicacaoPagamento == 9L))   //Manutenção - Portaria Remota 						
			
		    { 
			// Utiliza email do usuário aprovador como destinatário - Código Papel: [FAP] Aprovador Pagamento Terceiros - Privado
			String emailUsuarioAprovador = processEntity.findGenericValue("aplicacaoPagamento.papelAprovadorPagamento.users.email"); 
			if (emailUsuarioAprovador != null)
			    listaEmails.add(emailUsuarioAprovador);
			else
			    listaEmails.add("fbafbafafwfawfedson.heinz@orsegups.com.br");
		    }
		    if ((codigoAplicacaoPagamento == 3L) || //Manutenção Alarme e CFTV - Mercado Público
			(codigoAplicacaoPagamento == 6L)) { //Instalação Alarme e CFTV - Mercado Público
			
				// Recupera papel do Gerente Comercial Público - Código Papel: [FAP] Aprovador Pagamento Terceiros - Público
        			NeoPaper papelGerenteComercialPublico = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "FAPAprovadorOrcamentoMercadoPublico"));
        			if (papelGerenteComercialPublico != null)
        			{
        			    NeoUser usuarioGerenteComercialPublico = null;
        			    for (NeoUser usr : papelGerenteComercialPublico.getUsers()) {
        				usuarioGerenteComercialPublico = usr;
        				break;
        			    }
        
        			    if (usuarioGerenteComercialPublico != null) {
        				listaEmails.add(usuarioGerenteComercialPublico.getEmail());
        			    }	
        			}
        			else {
        			    listaEmails.add("rodrigo.fontoura@orsegups.com.br");
        			}			
		    }		    
		}
		
		// FAP de Patrimonio - Destinatarios: Solicitante e Aprovador - Controladoria
		if (codigoAplicacaoPagamento == 12L){
		    String emailAprovador = processEntity.findGenericValue("aprovador.email");
		    String emailSolicitante = processEntity.findGenericValue("solicitanteOrcamento.email");
		    
		    if (emailAprovador != null){
			listaEmails.add(emailAprovador);
		    }
		    if (emailSolicitante != null){
			listaEmails.add(emailSolicitante);
		    }
		}
		
	    }

	    String emailCopia = "";
	    if ((codigoAplicacaoPagamento >= 1L && codigoAplicacaoPagamento <= 9) || (codigoAplicacaoPagamento == 13L)) {
		emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
	    }

	    if (emailCopia != null) {
		StringTokenizer tokenEmailStr = new StringTokenizer(emailCopia, ";");
		while (tokenEmailStr.hasMoreElements()) {
		    String email = (String) tokenEmailStr.nextElement();
		    email = email.trim();
		    if (email.contains("@")) {
			listaEmailsCopia.add(email);
		    }
		}
	    }

	    String textoObjeto = "";
	    String textoRelato = "";
	    if ((codigoAplicacaoPagamento >= 2L && codigoAplicacaoPagamento <= 9L) || (codigoAplicacaoPagamento == 13L)){
        		textoRelato = (String) processEntity.findValue("laudoEletronica.relato");
        		textoObjeto = "<p><strong>Conta/Cliente:</strong> " + (String) processEntity.findValue("contaSigma.id_central");
        		textoObjeto = textoObjeto + "[" + (String) processEntity.findValue("contaSigma.particao") + "]";
        		textoObjeto = textoObjeto + " - " + (String) processEntity.findValue("contaSigma.razao");
        		textoObjeto = textoObjeto + " - " + (String) processEntity.findValue("contaSigma.fantasia") + "</p>";
	    }
	    else  if (codigoAplicacaoPagamento == 12L){
		textoObjeto = "<p><strong>Patrimônio:</strong> " + (String) processEntity.findValue("patrimonio.produto.despro");
		textoObjeto = textoObjeto + " - <strong>Cor</strong> " + (String) processEntity.findValue("patrimonio.cor");
		textoRelato = (String) processEntity.findValue("laudoEletronica.laudo") + "</p>";		
	    }

	    NeoUser solicitante = (NeoUser) processEntity.findValue("solicitanteOrcamento");
	    GregorianCalendar dataRaw = (GregorianCalendar) processEntity.findValue("dataSolicitacao");
	    StringBuilder html = new StringBuilder();
	    html.append("<html>");
	    html.append("<center><h1>" + "Orçamento Aprovado - " + code + "</h1></center>");
	    html.append("<br/><br/>");
	    html.append("Prezado(a) fornecedor(a) " + cgccpf + " - " + nomeFornecedor + ", segue orçamento aprovado: ");
	    html.append("<br/>");
	    html.append("<p><strong>Data:</strong> " + NeoDateUtils.safeDateFormat(dataRaw, "dd/MM/yyyy") + "</p>");
	    html.append("<p><strong>Solicitante:</strong> " + solicitante.getFullName() + " (" + solicitante.getEmail() + ")</p>");
	    html.append(textoObjeto);
	    html.append("<p><strong>Solicitação/Serviço:</strong><br/> " + textoRelato + "</p>");
	    html.append("<br/><br/>");
	    
	    if (codigoAplicacaoPagamento == 13L) {
	    	html.append("<p>Serviços Extras de vigilância humana solicitados:</p>");	    		    	
	    } else {
	    	html.append("<p>Os itens liberados para manutenção/revisão dos técnicos foram:</p>");	    	
	    }

	    List<NeoObject> listaItensOrcamento = (List<NeoObject>) processEntity.findValue("itemOrcamento");
	    if (NeoUtils.safeIsNotNull(listaItensOrcamento) && !listaItensOrcamento.isEmpty()) {
		html.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
		html.append("			<tr style=\"cursor: auto\">");
		html.append("				<th style=\"cursor: auto; text-align: left\">Tipo</th>");
		html.append("				<th style=\"cursor: auto; text-align: left\">Descrição</th>");
		html.append("				<th style=\"cursor: auto; text-align: left\">Valor</th>");
		html.append("			</tr>");
		html.append("			<tbody>");

		for (NeoObject objItemOrcamento : listaItensOrcamento) {
		    EntityWrapper itemOrcamentoWrapper = new EntityWrapper(objItemOrcamento);
		    String tipoItem = (String) itemOrcamentoWrapper.findValue("tipoItem.descricao");
		    String descricaoItem = (String) itemOrcamentoWrapper.findValue("descricao");
		    String valorItem = NeoUtils.safeOutputString((BigDecimal) itemOrcamentoWrapper.findValue("valor"));

		    html.append("		<tr>");
		    html.append("			<td>" + tipoItem + "</td>");
		    html.append("			<td>" + descricaoItem + "</td>");
		    html.append("			<td>" + valorItem + "</td>");
		    html.append("		</tr>");
		}
		html.append("		</table>");
	    }

	    html.append("</html>");

	    List<String> copiaOculta = new ArrayList<String>();
	    copiaOculta.add("emailautomatico@orsegups.com.br");

	    if (!listaEmails.isEmpty()) {
		for (String emailDst : listaEmails) {
		    OrsegupsUtils.sendEmail2Orsegups(emailDst, "fusion@orsegups.com.br", "Orçamento Aprovado - " + code, "", html.toString(), emailRemetente, listaEmailsCopia, copiaOculta);
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new WorkflowException(e.getMessage());
	}

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub

    }

}
