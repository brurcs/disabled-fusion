package com.neomind.fusion.custom.orsegups.e2doc.vo;

public class E2docDocumentoVO
{
	private String pasta;
	private String colaborador;
	private String empresa;
	private String data;
	private String tipo;
	private String situacao;
	private String observacao;
	private String CPF;
	private String modelo;
	private String Matricula;
	private String documento;
	private String versao;
	private String descricao;
	private String nomeArquivo;
	private String tamanho;
	private String id;
	private String tam;
	private String des;
	private String doc;
	
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getTam()
	{
		return tam;
	}
	public void setTam(String tam)
	{
		this.tam = tam;
	}
	public String getDes()
	{
		return des;
	}
	public void setDes(String des)
	{
		this.des = des;
	}
	public String getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(String colaborador)
	{
		this.colaborador = colaborador;
	}
	public String getEmpresa()
	{
		return empresa;
	}
	public void setEmpresa(String empresa)
	{
		this.empresa = empresa;
	}
	public String getData()
	{
		return data;
	}
	public void setData(String data)
	{
		this.data = data;
	}
	public String getTipo()
	{
		return tipo;
	}
	public void setTipo(String tipo)
	{
		this.tipo = tipo;
	}
	public String getSituacao()
	{
		return situacao;
	}
	public void setSituacao(String situacao)
	{
		this.situacao = situacao;
	}
	public String getObservacao()
	{
		return observacao;
	}
	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}
	public String getCPF()
	{
		return CPF;
	}
	public void setCPF(String cPF)
	{
		CPF = cPF;
	}
	/**
	 * @return the pasta
	 */
	public String getPasta()
	{
		return pasta;
	}
	/**
	 * @param pasta the pasta to set
	 */
	public void setPasta(String pasta)
	{
		this.pasta = pasta;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo()
	{
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo)
	{
		this.modelo = modelo;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula()
	{
		return Matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula)
	{
		Matricula = matricula;
	}
	/**
	 * @return the documento
	 */
	public String getDocumento()
	{
		return documento;
	}
	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(String documento)
	{
		this.documento = documento;
	}
	/**
	 * @return the versao
	 */
	public String getVersao()
	{
		return versao;
	}
	/**
	 * @param versao the versao to set
	 */
	public void setVersao(String versao)
	{
		this.versao = versao;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao()
	{
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	/**
	 * @return the nomeArquivo
	 */
	public String getNomeArquivo()
	{
		return nomeArquivo;
	}
	/**
	 * @param nomeArquivo the nomeArquivo to set
	 */
	public void setNomeArquivo(String nomeArquivo)
	{
		this.nomeArquivo = nomeArquivo;
	}
	/**
	 * @return the tamanho
	 */
	public String getTamanho()
	{
		return tamanho;
	}
	/**
	 * @param tamanho the tamanho to set
	 */
	public void setTamanho(String tamanho)
	{
		this.tamanho = tamanho;
	}
	public String getDoc()
	{
		return doc;
	}
	public void setDoc(String doc)
	{
		this.doc = doc;
	}

}
