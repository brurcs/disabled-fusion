package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import com.neomind.fusion.custom.orsegups.adapter.vo.OSAgendamento;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * 
 * Altera o técnico responsável pela Ordem de Serviço para um técnico de agendamento caso a OS esteja com um técnico e sem data de agendamento.
 * 
 * @author herisson.ferreira
 *
 */
public class VarreduraOsSemAgendamento implements CustomJobAdapter {
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		System.out.println("[" + this.getClass().getSimpleName() + "] Início da rotina: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss"));
		
		List<OSAgendamento> listaOS = new ArrayList<>();
		
		listaOS = retornaOrdensServicoAgendada();
		
		if (eValido(listaOS)) {
			
			for (OSAgendamento os : listaOS) {
				
				int resultado = 0;
				
				try {
					
					resultado = atualizaTecnicoAgendamento(os);
					
					insereNotaOrdemServico(os, resultado);
					
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("[" + this.getClass().getSimpleName() + "]");
				}
				
			}	
		}
		
		System.out.println("[" + this.getClass().getSimpleName() + "] Fim da rotina: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss"));
	}

	/**
	 * Busca as Ordens de Serviço que estejam com algum técnico, porém sem data de agendamento
	 * 
	 * @return Lista de objetos contendo os dados da ordem de serviço
	 */
	public List<OSAgendamento> retornaOrdensServicoAgendada () {
		
		List<OSAgendamento> listaAgendamento = new ArrayList<>();
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		try {
			
			conn = PersistEngine.getConnection("SIGMA90");
			
			if (NeoUtils.safeIsNotNull(conn)) {
				
				sql.append(" SELECT SUBSTRING(COL.NM_COLABORADOR, 1, 3) AS REGIONAL, SUBSTRING(COL.NM_COLABORADOR, 7, 3) AS REG_PUB, O.ID_ORDEM AS OS FROM dbORDEM O ");
				sql.append(" INNER JOIN dbCENTRAL C	");
				sql.append(" ON O.CD_CLIENTE = C.CD_CLIENTE	");
				sql.append(" LEFT JOIN GRUPO_CLIENTE G	");
				sql.append(" ON C.CD_GRUPO_CLIENTE = G.CD_GRUPO_CLIENTE	");
				sql.append(" INNER JOIN COLABORADOR COL	");
				sql.append(" ON O.ID_INSTALADOR = COL.CD_COLABORADOR	");
				sql.append(" WHERE O.DATAAGENDADA IS NULL	");
				sql.append(" AND O.FECHADO = 0	");
				sql.append(" AND ((G.NM_DESCRICAO IS NULL) OR (G.NM_DESCRICAO NOT LIKE '%CORP%')) ");
				sql.append(" AND COL.NM_COLABORADOR NOT LIKE '%AGENDAR%TECNICO%'	");
				sql.append(" AND O.IDOSDEFEITO NOT IN (98,919,920,1148,1152,1153,1154,1155,1156,1157,1158,1159,1160,1161,1024,1025,1026,1027,1028,1029,1030,1031,1032,1033,1034,1035,1036,1168,1169,1165)	");
				
				pstm = conn.prepareStatement(sql.toString());

				rs = pstm.executeQuery();
				
				while (rs.next()) {
					
					OSAgendamento ordemServico = new OSAgendamento();
					
					String regional = rs.getString("REGIONAL");
					
					if (regional.equals("PUB")) {
						regional = rs.getString("REG_PUB");
					}
					
					ordemServico.setNumeroOs(rs.getLong("OS"));
					ordemServico.setRegionalTecnico(regional);
					
					listaAgendamento.add(ordemServico);
					
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("[" + this.getClass().getSimpleName() + "] Erro ao retornar as Ordens de Serviço Agendada! "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss"));
		}finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaAgendamento;
	}
	
	/**
	 * Atualiza o técnico da ordem de serviço para um técnico de agendamento
	 * 
	 * @param ordemServico Objeto contendo os dados da ordem de serviço
	 */
	public int atualizaTecnicoAgendamento (OSAgendamento ordemServico) {
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		Long idInstalador = 0L;
		int resultado = 0;
		
		try {
			
			conn = PersistEngine.getConnection("SIGMA90");
			idInstalador = retornaInstalador(ordemServico.getRegionalTecnico());
			
			if (NeoUtils.safeIsNotNull(conn) && eValido(idInstalador)) {
				
				sql.append(" UPDATE dbOrdem SET ID_INSTALADOR = ? WHERE ID_ORDEM = ? ");
				
				pstm = conn.prepareStatement(sql.toString());
				
				pstm.setLong(1, idInstalador);
				pstm.setLong(2, ordemServico.getNumeroOs());
				
				resultado = pstm.executeUpdate();				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("[" + this.getClass().getSimpleName() + "] Erro ao atualizar o técnico de agendamento da OS: "+ordemServico.getNumeroOs()+" - "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss"));
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}
		
		return resultado;
	}
	
	/**
	 * Insere uma nota na Ordem de Serviço para informar que o técnico foi atualizado pela rotina devido a atender a regra de negócio
	 * 
	 * @param ordemServico Objeto contendo os dados da ordem de serviço
	 */
	public void insereNotaOrdemServico (OSAgendamento ordemServico, int resultadoAtualizacao) {
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		
		String textoNota = "Técnico atualizado via Sistema devido a OS estar com um técnico, porém sem agendamento.";
		
		try {
			
			if (resultadoAtualizacao == 0) {
				textoNota = "Erro ao atualizar o técnico para agendamento devido ao nome do técnico atual. Regional não localizada. ";
			}
			
			
			conn = PersistEngine.getConnection("SIGMA90");
			
			if (NeoUtils.safeIsNotNull(conn)) {
				
				sql.append(" INSERT INTO OS_NOTAS (ID_ORDEM, NM_COLABORADOR, DT_INSERCAO, NOTA) ");
				sql.append(" VALUES (?, ?, ?, ?) ");
				
				pstm = conn.prepareStatement(sql.toString());
				
				pstm.setLong(1, ordemServico.getNumeroOs());
				pstm.setString(2, "SISTEMA FUSION");
				pstm.setTimestamp(3, new Timestamp(new GregorianCalendar().getTimeInMillis()));
				pstm.setString(4, textoNota);
				
				pstm.execute();
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("[" + this.getClass().getSimpleName() + "] Erro ao inserir a nota na OS: "+ordemServico.getNumeroOs()+ " - "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss"));
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}
		
		
	}
	
	/**
	 * Retorna o ID correspondente ao técnico da regional informada
	 * 
	 * @param regional Sigla da Regional
	 * @return id correspondente ao técnico de agendamento da regional informada
	 */
	public Long retornaInstalador (String regional) {
		
		HashMap<String, Long> listaInstalador = new HashMap<>();
		
		listaInstalador.put("BNU", 119790L);
		listaInstalador.put("BQE", 119791L);
		listaInstalador.put("CAS", 119804L);
		listaInstalador.put("CCO", 119797L);
		listaInstalador.put("CSC", 119801L);
		listaInstalador.put("CTA", 119799L);
		listaInstalador.put("CUA", 119798L);
		listaInstalador.put("GNA", 119802L);
		listaInstalador.put("IAI", 119792L);
		listaInstalador.put("JGS", 119794L);
		listaInstalador.put("JLE", 119793L);
		listaInstalador.put("LGS", 119796L);
		listaInstalador.put("PAE", 119800L);
		listaInstalador.put("PMJ", 119803L);
		listaInstalador.put("RSL", 119795L);
		listaInstalador.put("SOO", 119550L);
		listaInstalador.put("SRR", 119805L);
		listaInstalador.put("TRI", 119806L);
		listaInstalador.put("TRO", 119807L);
		
		Long idInstalador = listaInstalador.get(regional);
		
		return idInstalador;
	}
	
	/**
	 * Valida se a variável é valida de acordo com a necessidade da classe
	 * 
	 * @param valor Variável Long
	 * @return <b> True </b> se a variável for válido, e <b> False </b> se o valor for inválido para a proposta da classe.
	 */
	public boolean eValido (Long valor) {
		
		if (NeoUtils.safeIsNotNull(valor) && valor > 0L) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Valida se a variável é valida de acordo com a necessidade da classe
	 * 
	 * @param valor Lista de Objetos do tipo OSAgendamento
	 * @return <b> True </b> se a lista for válida, e <b> False </b> se o valor for inválida para a proposta da classe.
	 */
	public boolean eValido (List<OSAgendamento> valor) {
		
		if (!valor.isEmpty() && valor.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
}
