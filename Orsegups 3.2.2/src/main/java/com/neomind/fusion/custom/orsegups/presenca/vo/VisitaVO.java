package com.neomind.fusion.custom.orsegups.presenca.vo;

public class VisitaVO
{
	private Long neoid;
	private String link;
	private String dataAgenda;
	private String dataRelato;
	private String contatoCliente;
	private String cargo;
	private String area;
	private String telefone;
	private String email;
	private String relatoGeral;
	private String relatoX8;
	private String relatoCoberturas;
	private String relatoSolicitacoes;
	private String relatoReclamacoes;
	private String relatoEquipamentos;
	private String relatoChecklist;
	private String relatoContasSigma;
	private String relatoEventos;
	private String relatoVisitas;
	
	
	public Long getNeoid()
	{
		return neoid;
	}
	public void setNeoid(Long neoid)
	{
		this.neoid = neoid;
	}
	public String getLink()
	{
		return link;
	}
	public void setLink(String link)
	{
		this.link = link;
	}
	public String getDataAgenda()
	{
		return dataAgenda;
	}
	public void setDataAgenda(String dataAgenda)
	{
		this.dataAgenda = dataAgenda;
	}
	public String getDataRelato()
	{
		return dataRelato;
	}
	public void setDataRelato(String dataRelato)
	{
		this.dataRelato = dataRelato;
	}
	public String getContatoCliente()
	{
		return contatoCliente;
	}
	public void setContatoCliente(String contatoCliente)
	{
		this.contatoCliente = contatoCliente;
	}
	public String getCargo()
	{
		return cargo;
	}
	public void setCargo(String cargo)
	{
		this.cargo = cargo;
	}
	public String getArea()
	{
		return area;
	}
	public void setArea(String area)
	{
		this.area = area;
	}
	public String getTelefone()
	{
		return telefone;
	}
	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public String getRelatoGeral()
	{
		return relatoGeral;
	}
	public void setRelatoGeral(String relatoGeral)
	{
		this.relatoGeral = relatoGeral;
	}
	public String getRelatoX8()
	{
		return relatoX8;
	}
	public void setRelatoX8(String relatoX8)
	{
		this.relatoX8 = relatoX8;
	}
	public String getRelatoCoberturas()
	{
		return relatoCoberturas;
	}
	public void setRelatoCoberturas(String relatoCoberturas)
	{
		this.relatoCoberturas = relatoCoberturas;
	}
	public String getRelatoSolicitacoes()
	{
		return relatoSolicitacoes;
	}
	public void setRelatoSolicitacoes(String relatoSolicitacoes)
	{
		this.relatoSolicitacoes = relatoSolicitacoes;
	}
	public String getRelatoReclamacoes()
	{
		return relatoReclamacoes;
	}
	public void setRelatoReclamacoes(String relatoReclamacoes)
	{
		this.relatoReclamacoes = relatoReclamacoes;
	}
	public String getRelatoEquipamentos()
	{
		return relatoEquipamentos;
	}
	public void setRelatoEquipamentos(String relatoEquipamentos)
	{
		this.relatoEquipamentos = relatoEquipamentos;
	}
	public String getRelatoChecklist()
	{
		return relatoChecklist;
	}
	public void setRelatoChecklist(String relatoChecklist)
	{
		this.relatoChecklist = relatoChecklist;
	}
	public String getRelatoContasSigma()
	{
		return relatoContasSigma;
	}
	public void setRelatoContasSigma(String relatoContasSigma)
	{
		this.relatoContasSigma = relatoContasSigma;
	}
	public String getRelatoEventos()
	{
		return relatoEventos;
	}
	public void setRelatoEventos(String relatoEventos)
	{
		this.relatoEventos = relatoEventos;
	}
	public String getRelatoVisitas()
	{
		return relatoVisitas;
	}
	public void setRelatoVisitas(String relatoVisitas)
	{
		this.relatoVisitas = relatoVisitas;
	}
}