package com.neomind.fusion.custom.orsegups.maps.call.vo;

public class EventoAcessoClienteVO
{
	private String idAcesso;
	private String nome;
	private String nuRegistroGeral;
	private String nuCpf;
	private String senha;
	private String obs;
	private String telefone;
	
	public String getIdAcesso()
	{
		return idAcesso;
	}
	public void setIdAcesso(String idAcesso)
	{
		this.idAcesso = idAcesso;
	}
	public String getNome()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	public String getNuRegistroGeral()
	{
		return nuRegistroGeral;
	}
	public void setNuRegistroGeral(String nuRegistroGeral)
	{
		this.nuRegistroGeral = nuRegistroGeral;
	}
	public String getNuCpf()
	{
		return nuCpf;
	}
	public void setNuCpf(String nuCpf)
	{
		this.nuCpf = nuCpf;
	}
	public String getSenha()
	{
		return senha;
	}
	public void setSenha(String senha)
	{
		this.senha = senha;
	}
	public String getObs()
	{
		return obs;
	}
	public void setObs(String obs)
	{
		this.obs = obs;
	}
	public String getTelefone()
	{
		return telefone;
	}
	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}
	
}
