package com.neomind.fusion.custom.orsegups.camerite.bean;

public class CameriteResponse {
    
    private boolean success;
    
    private CameriteResponseData data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public CameriteResponseData getData() {
        return data;
    }

    public void setData(CameriteResponseData data) {
        this.data = data;
    }
    
    

}
