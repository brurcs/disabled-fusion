package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.Usut160sigVO;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCNRecuperaListaContatosInadimplencia implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		// 1 Contatos Contrato
		String fone = (String) processEntity.findValue("contratoSapiens.usu_clifon");
		String email = (String) processEntity.findValue("contratoSapiens.usu_emactr");
		String nome = (String) processEntity.findValue("contratoSapiens.usu_clicon");
		if ((fone != null && !fone.replaceAll(" ", "").trim().isEmpty()) || (email != null && !email.replaceAll(" ", "").trim().isEmpty()))
		{
			NeoObject noContato = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
			EntityWrapper centralWrapper = new EntityWrapper(noContato);
			centralWrapper.setValue("origem", "Sapiens - Contrato");
			centralWrapper.setValue("nomeContato", nome);
			centralWrapper.setValue("telefone", fone);
			centralWrapper.setValue("email", email);
			PersistEngine.persist(noContato);
			processEntity.findField("contatosClientes").addValue(noContato);
		}

		// 2 Contatos Cliente
		fone = (String) processEntity.findValue("contratoSapiens.foncli");
		email = (String) processEntity.findValue("contratoSapiens.intnet");
		nome = (String) processEntity.findValue("contratoSapiens.usu_pescob");
		if ((fone != null && !fone.replaceAll(" ", "").trim().isEmpty()) || (email != null && !email.replaceAll(" ", "").trim().isEmpty()))
		{
			NeoObject noContato = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
			EntityWrapper centralWrapper = new EntityWrapper(noContato);
			centralWrapper.setValue("origem", "Sapiens - Cliente");
			centralWrapper.setValue("nomeContato", nome);
			centralWrapper.setValue("telefone", fone);
			centralWrapper.setValue("email", email);
			PersistEngine.persist(noContato);
			processEntity.findField("contatosClientes").addValue(noContato);
		}

		// 3 Contatos Cliente
		fone = (String) processEntity.findValue("contratoSapiens.foncl2");
		email = (String) processEntity.findValue("contratoSapiens.emanfe");
		nome = (String) processEntity.findValue("contratoSapiens.usu_pescob");
		if ((fone != null && !fone.replaceAll(" ", "").trim().isEmpty()) || (email != null && !email.replaceAll(" ", "").trim().isEmpty()))
		{
			NeoObject noContato = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
			EntityWrapper centralWrapper = new EntityWrapper(noContato);
			centralWrapper.setValue("origem", "Sapiens - Cliente");
			centralWrapper.setValue("nomeContato", nome);
			centralWrapper.setValue("telefone", fone);
			centralWrapper.setValue("email", email);
			PersistEngine.persist(noContato);
			processEntity.findField("contatosClientes").addValue(noContato);
		}

		// 4 Contatos Cliente
		fone = (String) processEntity.findValue("contratoSapiens.foncl3");
		email = "";
		nome = (String) processEntity.findValue("contratoSapiens.usu_pescob");
		if ((fone != null && !fone.replaceAll(" ", "").trim().isEmpty()))
		{
			NeoObject noContato = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
			EntityWrapper centralWrapper = new EntityWrapper(noContato);
			centralWrapper.setValue("origem", "Sapiens - Cliente");
			centralWrapper.setValue("nomeContato", nome);
			centralWrapper.setValue("telefone", fone);
			centralWrapper.setValue("email", email);
			PersistEngine.persist(noContato);
			processEntity.findField("contatosClientes").addValue(noContato);
		}

		// 5 Contatos Cliente
		fone = (String) processEntity.findValue("contratoSapiens.foncl4");
		email = "";
		nome = (String) processEntity.findValue("contratoSapiens.usu_pescob");
		if ((fone != null && !fone.replaceAll(" ", "").trim().isEmpty()))
		{
			NeoObject noContato = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
			EntityWrapper centralWrapper = new EntityWrapper(noContato);
			centralWrapper.setValue("origem", "Sapiens - Cliente");
			centralWrapper.setValue("nomeContato", nome);
			centralWrapper.setValue("telefone", fone);
			centralWrapper.setValue("email", email);
			PersistEngine.persist(noContato);
			processEntity.findField("contatosClientes").addValue(noContato);
		}

		// 6 Contatos Cliente
		fone = (String) processEntity.findValue("contratoSapiens.foncl5");
		email = "";
		nome = (String) processEntity.findValue("contratoSapiens.usu_pescob");
		if ((fone != null && !fone.replaceAll(" ", "").trim().isEmpty()))
		{
			NeoObject noContato = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
			EntityWrapper centralWrapper = new EntityWrapper(noContato);
			centralWrapper.setValue("origem", "Sapiens - Cliente");
			centralWrapper.setValue("nomeContato", nome);
			centralWrapper.setValue("telefone", fone);
			centralWrapper.setValue("email", email);
			PersistEngine.persist(noContato);
			processEntity.findField("contatosClientes").addValue(noContato);
		}

		// Contatos do SIGMA
		Long empresa = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
		Long filial = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
		Long contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

		StringBuilder sqlContasSigma = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("SAPIENS");

			List<Usut160sigVO> lstContasSigma = new ArrayList<Usut160sigVO>();
			sqlContasSigma.append("SELECT USU_CODEMP, USU_CODFIL, USU_NUMCTR, USU_CODCLI          ");
			sqlContasSigma.append(" FROM USU_T160SIG WHERE USU_CODEMP = " + empresa);
			sqlContasSigma.append(" AND USU_CODFIL = " + filial + " AND USU_NUMCTR = " + contrato);
			sqlContasSigma.append(" GROUP BY USU_CODEMP, USU_CODFIL, USU_NUMCTR, USU_CODCLI       ");
			sqlContasSigma.append(" ORDER BY USU_CODCLI ASC 									  ");

			//Abre Conexão com o Sapiens para consultar os fornecedores com dataCorte (getDate() - 1))
			pstm = conn.prepareStatement(sqlContasSigma.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				Usut160sigVO contasSigmaVO = new Usut160sigVO();
				contasSigmaVO.setUsu_CodEmp(rs.getLong("USU_CODEMP"));
				contasSigmaVO.setUsu_CodFil(rs.getLong("USU_CODFIL"));
				contasSigmaVO.setUsu_NumCtr(rs.getLong("USU_NUMCTR"));
				contasSigmaVO.setUsu_CodCli(rs.getLong("USU_CODCLI"));
				lstContasSigma.add(contasSigmaVO);
			}

			OrsegupsUtils.closeConnection(conn, pstm, rs);
			//Fecha Conexão com o Sapiens para consultar os fornecedores com dataCorte (getDate() - 1))

			ExternalEntityInfo infoContatoSigma = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SIGMACONTATOS");

			if (lstContasSigma != null && !lstContasSigma.isEmpty())
			{
				for (Usut160sigVO contaSigmaVO : lstContasSigma)
				{

					Long cd_cliente = contaSigmaVO.getUsu_CodCli();

					QLEqualsFilter filterConta = new QLEqualsFilter("cd_cliente", cd_cliente);

					// Fones do cadastro da Conta
					List<NeoObject> listaContas = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SIGMAVCLIENTES")).getEntityClass(), filterConta);
					if (listaContas != null && !listaContas.isEmpty())
					{
						for (NeoObject noConta : listaContas)
						{
							if (noConta != null)
							{
								EntityWrapper wrapper = new EntityWrapper(noConta);
								if (wrapper != null)
								{
									fone = (String) wrapper.findValue("fone1");
									if (fone != null && !fone.replaceAll(" ", "").trim().isEmpty())
									{
										NeoObject noContatoFusion = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
										EntityWrapper centralWrapper = new EntityWrapper(noContatoFusion);
										centralWrapper.setValue("origem", "Sigma - Conta");
										centralWrapper.setValue("nomeContato", nome);
										centralWrapper.setValue("telefone", fone);
										centralWrapper.setValue("email", "");
										PersistEngine.persist(noContatoFusion);
										processEntity.findField("contatosClientes").addValue(noContatoFusion);
									}
									fone = (String) wrapper.findValue("fone2");
									if (fone != null && !fone.replaceAll(" ", "").trim().isEmpty())
									{
										NeoObject noContatoFusion = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
										EntityWrapper centralWrapper = new EntityWrapper(noContatoFusion);
										centralWrapper.setValue("origem", "Sigma - Conta");
										centralWrapper.setValue("nomeContato", nome);
										centralWrapper.setValue("telefone", fone);
										centralWrapper.setValue("email", "");
										PersistEngine.persist(noContatoFusion);
										processEntity.findField("contatosClientes").addValue(noContatoFusion);
									}

								}
							}
						}
					}
					// Fones dos Contatos da conta
					List<NeoObject> listaContatos = (List<NeoObject>) PersistEngine.getObjects(infoContatoSigma.getEntityClass(), filterConta);
					if (listaContatos != null && !listaContatos.isEmpty())
					{
						for (NeoObject noContato : listaContatos)
						{
							if (noContato != null)
							{
								EntityWrapper wrapper = new EntityWrapper(noContato);
								if (wrapper != null)
								{
									nome = (String) wrapper.findValue("nome");
									fone = (String) wrapper.findValue("fone1");
									String fone2 = (String) wrapper.findValue("fone2");
									if (fone != null && !fone.replaceAll(" ", "").trim().isEmpty())
									{
										NeoObject noContatoFusion = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
										EntityWrapper centralWrapper = new EntityWrapper(noContatoFusion);
										centralWrapper.setValue("origem", "Sigma - Contato");
										centralWrapper.setValue("nomeContato", nome);
										centralWrapper.setValue("telefone", fone);
										centralWrapper.setValue("email", "");
										PersistEngine.persist(noContatoFusion);
										processEntity.findField("contatosClientes").addValue(noContatoFusion);
									}
									if (fone2 != null && !fone2.replaceAll(" ", "").trim().isEmpty())
									{
										NeoObject noContatoFusion = AdapterUtils.createNewEntityInstance("FCNContatosClientesInadimplencia");
										EntityWrapper centralWrapper = new EntityWrapper(noContatoFusion);
										centralWrapper.setValue("origem", "Sigma - Contato");
										centralWrapper.setValue("nomeContato", nome);
										centralWrapper.setValue("telefone", fone2);
										centralWrapper.setValue("email", "");
										PersistEngine.persist(noContatoFusion);
										processEntity.findField("contatosClientes").addValue(noContatoFusion);
									}
								}

							}
							EntityWrapper wrapperContato = new EntityWrapper(noContato);
						}
					}

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("ERRO: C026 - FCI - Cobrança Inadimplência");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new WorkflowException("ERRO: C026 - FCI - Cobrança Inadimplência");
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
