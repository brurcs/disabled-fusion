package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class OSListaOSInstalacao implements AdapterInterface {

	@Override
	public void back(EntityWrapper arg0, Activity arg1) {
	}

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		 
		GregorianCalendar dataFechamento = new GregorianCalendar();
		
		dataFechamento.add(Calendar.DAY_OF_MONTH, -1);
		dataFechamento.set(Calendar.HOUR_OF_DAY, 0);
		dataFechamento.set(Calendar.MINUTE, 0);
		dataFechamento.set(Calendar.SECOND, 0);
		dataFechamento.set(Calendar.MILLISECOND, 0);
		
		QLOpFilter filtroData = new QLOpFilter("fechamento", ">=", (GregorianCalendar) dataFechamento);
		
		List<NeoObject> listaOs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAVIEWOSINSTALACAO"),filtroData, -1, -1, "fechamento desc");
		
		if (listaOs != null && !listaOs.isEmpty())
		{
			processEntity.setValue("listaosinstalacao", listaOs);
			processEntity.setValue("possuios", true);
		}
		else
		{
			processEntity.setValue("possuios", false);
		}
		processEntity.setValue("dataReferencia", dataFechamento);
	}

}
