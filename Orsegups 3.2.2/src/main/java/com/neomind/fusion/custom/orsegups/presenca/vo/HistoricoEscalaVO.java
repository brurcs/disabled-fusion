package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class HistoricoEscalaVO
{
	private ColaboradorVO colaborador;
	private EscalaVO escala;
	private GregorianCalendar data;
	private String excluir;
	
	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}
	public EscalaVO getEscala()
	{
		return escala;
	}
	public void setEscala(EscalaVO escala)
	{
		this.escala = escala;
	}
	public GregorianCalendar getData()
	{
		return data;
	}
	public void setData(GregorianCalendar data)
	{
		this.data = data;
	}
	public String getExcluir()
	{
		return excluir;
	}
	public void setExcluir(String excluir)
	{
		this.excluir = excluir;
	}
}
