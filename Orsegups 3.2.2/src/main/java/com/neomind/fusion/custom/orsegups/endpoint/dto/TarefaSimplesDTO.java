package com.neomind.fusion.custom.orsegups.endpoint.dto;

public class TarefaSimplesDTO {

    private String solicitante;
    
    private String executor;
    
    private String titulo;
    
    private String descricao;
    
    private Long prazo;
    
    public Long getPrazo() {
        return prazo;
    }
    public void setPrazo(Long prazo) {
        this.prazo = prazo;
    }
    public String getSolicitante() {
        return solicitante;
    }
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }
    public String getExecutor() {
        return executor;
    }
    public void setExecutor(String executor) {
        this.executor = executor;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
    
    
}
