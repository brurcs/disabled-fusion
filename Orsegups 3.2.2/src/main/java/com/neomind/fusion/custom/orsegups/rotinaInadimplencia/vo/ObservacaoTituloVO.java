package com.neomind.fusion.custom.orsegups.rotinaInadimplencia.vo;

import java.util.GregorianCalendar;

public class ObservacaoTituloVO {
    
    private String observacaoTitulo;
    private GregorianCalendar dataObservacao;
    
    public String getObservacaoTitulo() {
        return observacaoTitulo;
    }
    public void setObservacaoTitulo(String observacaoTitulo) {
        this.observacaoTitulo = observacaoTitulo;
    }
    public GregorianCalendar getDataObservacao() {
        return dataObservacao;
    }
    public void setDataObservacao(GregorianCalendar dataObservacao) {
        this.dataObservacao = dataObservacao;
    }
    
    

}
