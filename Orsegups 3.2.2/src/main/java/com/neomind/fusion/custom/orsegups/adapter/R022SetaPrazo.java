package com.neomind.fusion.custom.orsegups.adapter;

//com.neomind.fusion.custom.orsegups.adapter.R022SetaPrazo
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoUtils;

public class R022SetaPrazo implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		
		GregorianCalendar gcPrazo = null;// (GregorianCalendar) processEntity.findValue("prazo");
		
		if (gcPrazo == null){
			gcPrazo = new GregorianCalendar();
			
		}
		
		gcPrazo = OrsegupsUtils.getNextWorkDay(gcPrazo);
		gcPrazo.set(GregorianCalendar.HOUR, 11);
		gcPrazo.set(GregorianCalendar.MINUTE, 59);
		gcPrazo.set(GregorianCalendar.SECOND, 59);
		System.out.println("Prazo R022: " + NeoUtils.safeDateFormat(gcPrazo));
		processEntity.setValue("prazo", gcPrazo);
				
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}
