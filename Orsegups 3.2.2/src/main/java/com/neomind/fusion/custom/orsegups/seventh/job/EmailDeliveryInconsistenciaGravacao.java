package com.neomind.fusion.custom.orsegups.seventh.job;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class EmailDeliveryInconsistenciaGravacao implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {

	
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append("SELECT * FROM GravacaoOnDemandLogs WHERE DATA_ERRO > GETDATE()-1 ");
	
	StringBuilder dadosApontamentoEmail = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("TIDB");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());
	    
	    int contApontamentos = 0;

	    while (rs.next()) {
		
		String servidor = rs.getString("SERVIDOR");
				
		if (servidor.length() > 80){
		    servidor = servidor.substring(0, 40);
		}
		
		contApontamentos ++;
		int resto = (contApontamentos) % 2;
		if (resto == 0){
		    dadosApontamentoEmail.append(
			    "<tr style=\"background-color: #d6e9f9\" ><td>"+rs.getString("ID_CENTRAL")+"</td><td>"+rs.getString("PARTICAO")+"</td><td>"+rs.getString("EMPRESA")+"</td>"
			    	+ "<td>"+ servidor +"</td><td>"+rs.getString("USUARIO")+"</td>"
			    		+"<td>"+rs.getString("SENHA")+"</td><td>"+rs.getString("MENSAGEM")+"</td><td>"+NeoDateUtils.safeDateFormat(rs.getTimestamp("DATA_ERRO"), "dd/MM/yyyy HH:mm:ss")+"</td></tr>");
		}else{
		    dadosApontamentoEmail.append("<tr><td>"+rs.getString("ID_CENTRAL")+"</td><td>"+rs.getString("PARTICAO")+"</td><td>"+rs.getString("EMPRESA")+"</td>"
			    	+ "<td>"+servidor+"</td><td>"+rs.getString("USUARIO")+"</td>"
			    		+"<td>"+rs.getString("SENHA")+"</td><td>"+rs.getString("MENSAGEM")+"</td><td>"+NeoDateUtils.safeDateFormat(rs.getTimestamp("DATA_ERRO"), "dd/MM/yyyy HH:mm:ss")+"</td></tr>");
		}
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}
	
	
	StringBuilder corpoEmail = new StringBuilder();

	corpoEmail.append("<!DOCTYPE html>");
	corpoEmail.append("<html>");
	corpoEmail.append("<head>");
	corpoEmail.append("<meta charset=\"UTF-8\">");
	corpoEmail.append("<title>Relatório de Inconsistências de Gravação</title>");
	corpoEmail.append("<style>");
	corpoEmail.append("h2{");
	corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	corpoEmail.append("}");
	corpoEmail.append(".table-p{");
	corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	corpoEmail.append(" }");
	corpoEmail.append(".table-info{");
	corpoEmail.append("table-layout: fixed;");
	corpoEmail.append("text-align:left;");
	corpoEmail.append("font-family: Verdana;");
	corpoEmail.append("}");
	corpoEmail.append(".table-info td{");
	corpoEmail.append("overflow-x: hidden;");
	corpoEmail.append(" }");
	corpoEmail.append(".table-info thead{");
	corpoEmail.append("background-color:#303090;");
	corpoEmail.append("color: white;");
	corpoEmail.append("font-weight:bold;");
	corpoEmail.append("	}");
	corpoEmail.append("</style>");
	corpoEmail.append("</head>");
	corpoEmail.append("<body>");
	corpoEmail.append("<div>");
	corpoEmail.append("<table width=\"600\" align=\"center\">");
	corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	corpoEmail.append("</table>");
	corpoEmail.append("<td><h2>Relatório de Inconsistencias de Gravação</h2></td>");
	corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	corpoEmail.append("<tr><td>Seguem detalhes das inconsistências:</td></tr>");
	corpoEmail.append("</table>");
	corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
	corpoEmail.append("<thead>");
	corpoEmail.append("<tr>");
	corpoEmail.append("<td>Conta</td>");
	corpoEmail.append("<td>Partição</td>");
	corpoEmail.append("<td>Empresa</td>");
	corpoEmail.append("<td>Servidor</td>");
	corpoEmail.append("<td>Usuario</td>");
	corpoEmail.append("<td>Senha</td>");
	corpoEmail.append("<td>Mensagem</td>");
	corpoEmail.append("<td>Data</td>");
	corpoEmail.append("</tr>");
	corpoEmail.append("</thead>");
	corpoEmail.append("<tbody>");
	corpoEmail.append("<!-- Recebe dados do método -->");
	corpoEmail.append(dadosApontamentoEmail);
	corpoEmail.append("<br>");
	corpoEmail.append("</tbody>");
	corpoEmail.append("</table>");
	corpoEmail.append("</div>");
	corpoEmail.append("</div>");
	corpoEmail.append("</body>");
	corpoEmail.append("</html>");

	String[] addTo = new String[] { "laercio.leite@orsegups.com.br"};
	String[] comCopia = new String[] { "mateus.batista@orsegups.com.br" };
	String from = "fusion@orsegups.com.br";
	String subject = "Relatório de Inconsistências de Gravação";
	String html = corpoEmail.toString();

	OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);

    }

}
