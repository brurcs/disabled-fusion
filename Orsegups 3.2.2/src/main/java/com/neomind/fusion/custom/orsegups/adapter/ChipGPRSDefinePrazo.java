package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class ChipGPRSDefinePrazo implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar dataPrazo = null;
		GregorianCalendar dataAtual = new GregorianCalendar();
		
		Long tipoSolicitacao = (Long) processEntity.findField("tipoSolicitacao.codigo").getValue();
		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazo").getValue();
		
		if (tipoSolicitacao == 1L) 
		{
			dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataAtual, 2L);
			
			if (prazo.before(dataPrazo))
			{
				throw new WorkflowException("Prazo informado deve ser de 2 dias úteis!");
			}
			
			if (!OrsegupsUtils.isWorkDay(prazo))
			{
				throw new WorkflowException("Prazo informado deve ter um dia útil!");
			}
		}
		else if (tipoSolicitacao == 2L) 
		{
			dataPrazo  = OrsegupsUtils.getSpecificWorkDay(dataAtual, 7L);
			
			if (prazo.before(dataPrazo))
			{
				throw new WorkflowException("Prazo informado deve ser de 7 dias úteis!");
			}
			
			if (!OrsegupsUtils.isWorkDay(prazo))
			{
				throw new WorkflowException("Prazo informado deve ter um dia útil!");
			}
		}
	}
}