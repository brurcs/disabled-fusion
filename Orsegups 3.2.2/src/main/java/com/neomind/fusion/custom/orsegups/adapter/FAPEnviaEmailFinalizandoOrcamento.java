package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPEnviaEmailFinalizandoOrcamento implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		ArrayList<String> listaEmails = new ArrayList<String>();
		ArrayList<String> listaEmailsCopia = new ArrayList<String>();
		try
		{
			Long aplicacaoPagamento = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
			String code = (String) processEntity.findValue("wfprocess.code");
			String emailRemetente = (String) processEntity.findValue("solicitanteOrcamento.email");
			NeoObject fornecedor = (NeoObject) processEntity.findValue("fornecedor");
			String nomeFornecedor = ""; 
			Long cgccpf = 0L;
			
			if (fornecedor != null)
			{
				EntityWrapper wrapperFornecedor = new EntityWrapper(fornecedor);
				String emailForn = (String) wrapperFornecedor.findValue("fornecedor.intnet");
				cgccpf = (Long) wrapperFornecedor.findField("fornecedor.cgccpf").getValue();
				nomeFornecedor = (String) wrapperFornecedor.findValue("fornecedor.nomfor");
				if (emailForn != null)
				{
					StringTokenizer tokenEmailStr = new StringTokenizer(emailForn, ";");
					while (tokenEmailStr.hasMoreElements())
					{
						String email = (String) tokenEmailStr.nextElement();
						email = email.trim();
						if (email.contains("@"))
						{
							listaEmails.add(email);
						}
					}
				}
			}
			
			String emailCopia = "";
			if (aplicacaoPagamento == 1L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 2L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 3L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 4L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 5L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 6L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 7L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 8L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 9L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			
			if (emailCopia != null ) {
				StringTokenizer tokenEmailStr = new StringTokenizer(emailCopia, ";");
				while (tokenEmailStr.hasMoreElements())
				{
					String email = (String) tokenEmailStr.nextElement();
					email = email.trim();
					if (email.contains("@")) {
						listaEmailsCopia.add(email);
					}
				}
			}

			String textoObjeto = "";
			String textoRelato = "";
			// Manutencao Frota
			if (aplicacaoPagamento == 1L)
			{
				textoObjeto = "<p><strong>Viatura:</strong> " + (String) processEntity.findValue("viatura.placa");
				textoObjeto = textoObjeto + " - " + (String) processEntity.findValue("viatura.tipoViatura.nome");
				textoObjeto = textoObjeto + " - ";
				if (((String) processEntity.findValue("viatura.anoViatura")) != null)
				{
					textoObjeto = textoObjeto + (String) processEntity.findValue("viatura.anoViatura");
				}
				else
				{
					textoObjeto = textoObjeto + "Ano não informado";
				}
				textoObjeto = textoObjeto + " - " + (String) processEntity.findValue("viatura.descricao");
				textoObjeto = textoObjeto + " - " + String.valueOf((Long) processEntity.findValue("laudo.kilometragem")) + "KM";
				textoRelato = (String) processEntity.findValue("laudo.relato") + "</p>";

			}
			// Manutencao Patrimonio
			else if (aplicacaoPagamento == 12L){
			    // Exibir Patrimônio e os Itens do Orçamento
			    textoObjeto = "<p><strong>Patrimônio:</strong> " + (String) processEntity.findValue("patrimonio.produto.despro");
			    textoObjeto = textoObjeto + " - <strong>Cor</strong> " + (String) processEntity.findValue("patrimonio.cor");
			    textoRelato = (String) processEntity.findValue("laudoEletronica.laudo") + "</p>";
			}

			NeoUser solicitante = (NeoUser) processEntity.findValue("solicitanteOrcamento");
			GregorianCalendar dataRaw = (GregorianCalendar) processEntity.findValue("dataSolicitacao");
			StringBuilder html = new StringBuilder();
			html.append("<html>");
			html.append("<center><h1>" + "Orçamento Aprovado - " + code + "</h1></center>");
			html.append("<br/><br/>");
			html.append("Prezado(a) fornecedor(a) " + cgccpf + " - " + nomeFornecedor + ", segue orçamento aprovado: ");
			html.append("<br/>");
			html.append("<p><strong>Data:</strong> " + NeoUtils.safeDateFormat(dataRaw, "dd/MM/yyyy") + "</p>");
			html.append("<p><strong>Solicitante:</strong> " + solicitante.getFullName() + " (" + solicitante.getEmail() + ")</p>");
			html.append(textoObjeto);
			html.append("<p><strong>Solicitação/Serviço:</strong><br/> " + textoRelato + "</p>");
			html.append("<br/><br/>");
			html.append("<p><strong>Os itens para troca/revisão são os seguintes:</strong></p>");

			List<NeoObject> listaItensOrcamento = (List<NeoObject>) processEntity.findValue("itemOrcamento");
			if (NeoUtils.safeIsNotNull(listaItensOrcamento) && !listaItensOrcamento.isEmpty())
			{
				html.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
				html.append("			<tr style=\"cursor: auto\">");
				html.append("				<th style=\"cursor: auto; text-align: left\">Tipo</th>");
				html.append("				<th style=\"cursor: auto; text-align: left\">Descrição</th>");
				html.append("				<th style=\"cursor: auto; text-align: left\">Valor</th>");
				html.append("			</tr>");
				html.append("			<tbody>");

				for (NeoObject objItemOrcamento : listaItensOrcamento)
				{
					EntityWrapper itemOrcamentoWrapper = new EntityWrapper(objItemOrcamento);
					String tipoItem = (String) itemOrcamentoWrapper.findValue("tipoItem.descricao");
					String descricaoItem = (String) itemOrcamentoWrapper.findValue("descricao");
					String valorItem = NeoUtils.safeOutputString((BigDecimal) itemOrcamentoWrapper.findValue("valor"));

					html.append("		<tr>");
					html.append("			<td>" + tipoItem + "</td>");
					html.append("			<td>" + descricaoItem + "</td>");
					html.append("			<td>" + valorItem + "</td>");
					html.append("		</tr>");
				}
				html.append("		</table>");
			}

			html.append("</html>");

			List<String> copiaOculta = new ArrayList<String>();
			copiaOculta.add("emailautomatico@orsegups.com.br");  
			
			if (!listaEmails.isEmpty())
			{
				for (String emailDst : listaEmails)
				{
					OrsegupsUtils.sendEmail2Orsegups(emailDst, "fusion@orsegups.com.br", "Orçamento Aprovado - " + code, "", html.toString(), emailRemetente, listaEmailsCopia, copiaOculta);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
