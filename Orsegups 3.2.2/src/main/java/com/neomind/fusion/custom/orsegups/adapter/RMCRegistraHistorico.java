package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class RMCRegistraHistorico implements AdapterInterface{
	
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		InstantiableEntityInfo ieiHistorico = AdapterUtils.getInstantiableEntityInfo("RMCRegistroHistorico");
		NeoObject historico = ieiHistorico.createNewInstance();
		EntityWrapper wHistorico = new EntityWrapper(historico);
		wHistorico.findField("data").setValue(origin.getFinishDate());
		wHistorico.findField("user").setValue(origin.getUser());
		wHistorico.findField("status").setValue(wrapper.findValue("motivoNaoFinalizacao.motivo"));
		wHistorico.findField("observacao").setValue(wrapper.findValue("observacao"));
		PersistEngine.persist(historico);
		wrapper.findField("historico").addValue(historico);
	}
	
	public void back (EntityWrapper wrapper, Activity act)
	{
		
	}

}
