package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class JADefinirSolicitante implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 1L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		processEntity.findField("prazo").setValue(prazo);

		NeoPaper papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "jaSolicitante"));

		NeoUser usuario = null;

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuario = user;
				break;
			}
		}
		processEntity.findField("solicitante").setValue(usuario);
		
		Long empresa = (Long) processEntity.findValue("colaborador.numemp");
		
		Long matricula = (Long) processEntity.findValue("colaborador.numcad");
		
		String nomeColaborador = (String) processEntity.findValue("colaborador.nomfun");
		
		String escalaETurma = this.getEscalaETurma(empresa, matricula);
		
		String fullName = empresa+" - "+matricula+" - "+nomeColaborador+" - "+escalaETurma;
		
		processEntity.findField("nomeColaborador").setValue(fullName);
	
		processEntity.findField("dadosColaborador").setValue(this.getCargoRegionalPosto(matricula, empresa, escalaETurma));
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
	
	private String getEscalaETurma (long empresa, long matricula){
	    
	    String sql = "SELECT ESC.NOMESC, HES.CodTma FROM r034fun FUN WITH(NOLOCK) "
		    +" INNER JOIN R038HES HES WITH(NOLOCK) ON FUN.numcad = HES.NumCad AND FUN.numemp = HES.NumEmp AND FUN.tipcol = HES.TipCol AND HES.DatAlt = " 
		    +" (SELECT MAX(DATALT) FROM R038HES HES2 WITH(NOLOCK) WHERE HES2.numcad = HES.NumCad AND HES2.numemp = HES.NumEmp AND HES2.tipcol = HES.TipCol) "
		    +" INNER JOIN R006ESC ESC WITH(NOLOCK) ON ESC.CodEsc = HES.CodEsc "
		    +" WHERE FUN.numemp=? AND FUN.numcad=? ";
	    
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;
	    
	    String retorno = "";
	    
	    try{
		conn = PersistEngine.getConnection("VETORH");
		
		pstm = conn.prepareStatement(sql);
		
		pstm.setLong(1, empresa);
		pstm.setLong(2, matricula);
		
		rs = pstm.executeQuery();
		
		while (rs.next()){
		     retorno = rs.getString(1)+" - T:"+rs.getString(2);
		     break;
		}
		
	    }catch (Exception e){
		e.printStackTrace();
	    }finally{
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    	    
	    return retorno;
	}
	
	private NeoObject getCargoRegionalPosto(long matricula, long empresa, String escalaETurma){
	   		
		String sql = "SELECT CAR.TitRed as CARGO, ORN.usu_lotorn + ' - ' + ORN.nomloc as POSTO, REG.USU_NomReg as REGIONAL, fun.datadm AS ADMISSAO FROM r034fun FUN WITH(NOLOCK) "
			+" INNER JOIN R038HLO HLO WITH(NOLOCK) ON HLO.NumCad = FUN.numcad AND HLO.NumEmp = FUN.numemp AND HLO.TipCol = FUN.tipcol AND HLO.DatAlt =  "
			+" (SELECT MAX(DATALT) FROM R038HLO HLO2 WITH(NOLOCK) WHERE HLO2.numcad = HLO.NumCad AND HLO2.numemp = HLO.NumEmp AND HLO2.tipcol = HLO.TipCol) "
			+" INNER JOIN r016orn ORN WITH(NOLOCK) ON ORN.numloc = HLO.NumLoc AND ORN.taborg = HLO.TabOrg "
			+" INNER JOIN USU_T200REG REG WITH(NOLOCK) ON REG.USU_CodReg = ORN.usu_codreg "
			+" INNER JOIN R038HCA HCA WITH(NOLOCK) ON HCA.NumCad = FUN.numcad AND HCA.NumEmp = FUN.numemp AND HCA.TipCol = FUN.tipcol AND HCA.DatAlt = "  
			+" (SELECT MAX(DATALT) FROM R038HCA HCA2 WITH(NOLOCK) WHERE HCA2.numcad = HCA.NumCad AND HCA2.numemp = HCA.NumEmp AND HCA2.tipcol = HCA.TipCol) "
			+" INNER JOIN R024CAR CAR WITH(NOLOCK) ON CAR.CodCar = HCA.CodCar AND CAR.EstCar=1 "
			+" WHERE FUN.numcad="+matricula+" AND FUN.numemp="+empresa+" AND FUN.tipcol=1 ";
		
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		
		try{
		    conn = PersistEngine.getConnection("VETORH");
		    
		    stmt = conn.createStatement();
		    
		    rs = stmt.executeQuery(sql);
		    
		    while (rs.next()){
			InstantiableEntityInfo ja = AdapterUtils.getInstantiableEntityInfo("jaDadosColaborador");
			NeoObject objJa = ja.createNewInstance();
			EntityWrapper wrapperJa = new EntityWrapper(objJa);
			
			wrapperJa.findField("cargo").setValue(rs.getString("CARGO"));
			wrapperJa.findField("posto").setValue(rs.getString("POSTO"));
			wrapperJa.findField("regional").setValue(rs.getString("REGIONAL"));
			wrapperJa.findField("escala").setValue(escalaETurma);
			
			Date date = rs.getDate("ADMISSAO");
			
			if (date != null){
			    GregorianCalendar admissao = new GregorianCalendar();			    
			    admissao.setTimeInMillis(date.getTime());	
			    wrapperJa.findField("dataAdmissao").setValue(admissao);
			}
			
			return objJa;
			
		    }
		    
		}catch(Exception e){
		    
		}finally{
		    OrsegupsUtils.closeConnection(conn, stmt, rs);
		}
			    
	    
	    return null;
	}
}
