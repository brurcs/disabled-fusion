define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "C__ambiente_neomind_workspace_FusionOrsegups3_2_2_src_main_java_com_neomind_fusion_custom_orsegups_endpoint_doc_main_js",
    "groupTitle": "C__ambiente_neomind_workspace_FusionOrsegups3_2_2_src_main_java_com_neomind_fusion_custom_orsegups_endpoint_doc_main_js",
    "name": ""
  },
  {
    "type": "post",
    "url": "/services/endpoint/qlpCoberturaColaborador",
    "title": "qlpCoberturaColaborador",
    "description": "<p>Efetua o lançamento de cobertura de colaborador para o período informado</p>",
    "version": "1.0.0",
    "name": "qlpCoberturaColaborador",
    "group": "QLP",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Sua chave de acesso à API.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "defaultValue": "application/json",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Authorization: \"seutoken\"\nContent-Type: application/json",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colaborador",
            "description": "<p>Colaborador que efetuará a substuição</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colaboradorASubstituir",
            "description": "<p>Colaborador que será substituido</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "dataInicio",
            "description": "<p>Tempo em milésimos de segundos referente a data de inicio da cobertura</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "dataFim",
            "description": "<p>Tempo em milésimos de segundos referente a data de inicio da cobertura</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idMotivo",
            "description": "<p>ID de um dos motivos da cobertura: <br> 1\tReciclagem <br> 2\tFalta de Efetivo <br> 3\tTreinamento <br> 4\tAusência (a justificar) <br> 5\tFolga <br> 6\tAtestado <br> 7\tDispensa Sindicato <br> 8\tExtensão de horário <br> 9\tSuspensão <br> 10\tCobertura SDF <br> 11\tServiço Extra <br> 12\tCobertura de almoço <br> 13\tAtraso de rendição <br> 14\tTreinamento para cobertura de férias <br> 15\tTreinamento para efetivação <br> 16\tInversão de escala <br> 17\tRemanejamento <br> 18\tCobertura Semanal <br></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "observacao",
            "description": "<p>Observação da cobertura</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemplo Requisição:",
          "content": "{\n\t\"colaborador\": \"19-1-999452\",\n\t\"colaboradorASubstituir\": \"19-1-999449\",\n\t\"dataInicio\": 1514462400000,\n\t\"dataFim\": 1514466000000,\n\t\"idMotivo\": 2,\n\t\"observacao\": \"Teste TI\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./OrsegupsRESTService.java",
    "groupTitle": "QLP"
  }
] });
