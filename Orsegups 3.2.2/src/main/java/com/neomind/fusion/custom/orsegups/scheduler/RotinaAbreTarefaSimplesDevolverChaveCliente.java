package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesDevolverChaveCliente implements CustomJobAdapter
{
	
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesOrdemServicoEmAtraso.class);
	
	public void execute(CustomJobContext ctx)
	{
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		
		log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Devolver Chave Cliente - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		try{
			
			sql.append("SELECT DISTINCT CEN.ID_CENTRAL AS CONTA, CEN.PARTICAO, CEN.RAZAO AS RAZAOSOCIAL, CEN.FANTASIA, SUBSTRING(r.NM_ROTA, 1, 3) AS REGIONAL ");
			sql.append("FROM dbCENTRAL CEN WITH(NOLOCK) ");
			sql.append("INNER JOIN ROTA r WITH (NOLOCK) ON  r.CD_ROTA = CEN.ID_ROTA ");
			sql.append("INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.USU_T160SIG sig WITH(NOLOCK) ON sig.usu_codcli = CEN.CD_CLIENTE ");
			sql.append("INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.USU_T160CVS cvs WITH (NOLOCK) ON cvs.usu_codemp = sig.usu_codemp ");
			sql.append("AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
			sql.append("INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.USU_T160CTR CTR WITH (NOLOCK) ON ctr.usu_codemp = cvs.usu_codemp ");
			sql.append("AND CTR.usu_codfil = cvs.usu_codfil AND CTR.usu_numctr = cvs.usu_numctr ");
			sql.append("INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.E085CLI cli WITH (NOLOCK) ON CLI.CodCli = CTR.usu_codcli ");
			sql.append("WHERE CEN.CHAVE=1 ");
			sql.append("AND CEN.PARTICAO = ");
			sql.append("(SELECT MIN(CEN2.PARTICAO) FROM DBCENTRAL AS CEN2 WHERE CEN2.ID_CENTRAL = CEN.ID_CENTRAL ");
			sql.append("AND CEN2.ID_EMPRESA = CEN.ID_EMPRESA) ");
			sql.append("AND CEN.CTRL_CENTRAL=1 ");
			sql.append("AND (CTR.USU_SITCTR LIKE ('I') AND CTR.USU_DATFIM = (GETDATE() -1)) ");
			sql.append("ORDER BY CEN.RAZAO ");
			
			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			
			while (rs.next()){
				String conta = (rs.getString("CONTA") != null) ? rs.getString("CONTA") : "";
				String particao = (rs.getString("PARTICAO")!= null) ? rs.getString("PARTICAO") : "";
				String razaoSocial = (rs.getString("RAZAOSOCIAL") != null) ? rs.getString("RAZAOSOCIAL") : "";
				String fantasia = (rs.getString("FANTASIA") != null) ? rs.getString("FANTASIA") : "";
				String regional = (rs.getString("REGIONAL") != null) ? rs.getString("REGIONAL") : "";
				
				String solicitante = "fernanda.maciel";
				//TODO CORRIGIR CHAMADA DO EXECUTOR
				//String executor = OrsegupsUtils.getExecutorOSDevolverChaveCliente(regional);
				String executor = "teste";
				String titulo = "Devolução de chave do cliente";
				
				String descricao = "<p><strong>Cliente: </strong>"+ conta +"["+particao+"] " + razaoSocial + "<br>"
							+"<strong>Fantasia:</strong> "+fantasia+"<br>"
							+"<strong>Descrição:</strong> Cliente está finalizando seu contrato, sendo necessário a devolução da chave de sua residencia "
							+ "que está sob posse da Orsegups.<br></p>";
				
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 5L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);
				
				
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);
				
				if (tarefa != null && !tarefa.isEmpty()){
					log.warn("#SUCESSO# Abrir Tarefa Simples Devolver Chave do Cliente. Tarefa: "+tarefa+". Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					
				}else{
					log.warn("#FALHA# Abrir Tarefa Simples Devolver Chave do Cliente. Dados: "+conta+", "+razaoSocial+", "+regional+", "+solicitante+", "+executor+". Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					
				}
				
			}
			
		}catch (SQLException e){
			e.printStackTrace();
		}finally{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		

	}
	
}
