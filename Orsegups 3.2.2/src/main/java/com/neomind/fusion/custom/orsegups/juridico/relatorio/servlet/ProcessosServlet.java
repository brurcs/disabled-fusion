package com.neomind.fusion.custom.orsegups.juridico.relatorio.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.neomind.fusion.custom.orsegups.juridico.relatorio.dao.EscritorioDAO;
import com.neomind.fusion.custom.orsegups.juridico.relatorio.dao.EscritorioDAOImpl;
import com.neomind.fusion.custom.orsegups.juridico.relatorio.dao.ProcessoDAO;
import com.neomind.fusion.custom.orsegups.juridico.relatorio.dao.ProcessoDAOImpl;
import com.neomind.fusion.custom.orsegups.juridico.relatorio.dto.ProcessoDTO;
import com.neomind.fusion.custom.orsegups.juridico.relatorio.util.SistemaUtil;

/**
 * Servlet implementation class ProcessosServlet
 */
@WebServlet(name = "ProcessosServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.juridico.relatorio.servlet.ProcessosServlet" })
public class ProcessosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public List<ProcessoDTO> lastQuery;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProcessosServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProcessoDAO p = new ProcessoDAOImpl();
		EscritorioDAO e = new EscritorioDAOImpl(); 

		String action = request.getParameter("action");
		String dat1 = request.getParameter("dat1");
		String dat2 = request.getParameter("dat2");
		String dat3 = request.getParameter("dat3");
		String dat4 = request.getParameter("dat4");
		String escRec = request.getParameter("escRec");
		
		if (action.equals("getAllAbertos")) {
			String queryAdd="";
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			try {
				if (dat1 != null && dat2 != null) {
					queryAdd = queryAdd +" and j002.dataCitacao between '"+ dat1 +"' and '"+ dat2+"'";
				}
				if(escRec != null) {
					queryAdd += " and j002.escRecPro_neoId = " + escRec;
				}
					attToLastQuery(p.listaTudoAberto(queryAdd));
					response.getWriter().write(SistemaUtil.listToString(lastQuery));				
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (action.equals("getAllFinalizados")) {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			try {
				String queryAdd="";
				if ( dat1 != null && dat2 != null ) {
					queryAdd = queryAdd +" and j002.dataCitacao between '"+ dat1 +"' and '"+ dat2+"'";
				} 
				if ( dat3 != null && dat4 != null ) {
					queryAdd = queryAdd +" and wf.finishDate between '"+ dat3 +"' and '"+ dat4+"'";
				} 
				if( escRec != null ) {
					queryAdd += " and j002.escRecPro_neoId = " + escRec;
				}
				attToLastQuery(p.listaTudoFinalizado(queryAdd));
				response.getWriter().write(SistemaUtil.listToString(lastQuery));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (action.equals("downloadXLS")) {
			try {
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename= download.xls");
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "no-store");
				response.addHeader("Cache-Control", "max-age=0");
				createXLS(response);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}else if(action.equals("getEscritorios")){
			try {
				response.getWriter().write(SistemaUtil.listToString(e.listaTodosEscResp()));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void createXLS(HttpServletResponse response) throws Exception {
		OutputStream out = response.getOutputStream();
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Processos");
		CellStyle cellStyle ; 
		int rowNum = 0;
		int cellNum = 0;
		//==============CRIA CABEÇALHO============
		Row row = sheet.createRow(rowNum);	
			cellStyle = row.getSheet().getWorkbook().createCellStyle();
			cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
			Cell cell = row.createCell(cellNum);
			cell.setCellValue("Autor");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("Centro de custo");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("Situação");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("CCU Manual");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("Situação");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("Processo");
			cell.setCellStyle(cellStyle);
			cellNum++;			
			cell = row.createCell(cellNum);
			cell.setCellValue("Data Citação");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("Data Geração");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("Meses");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("Escritorio");
			cell.setCellStyle(cellStyle);
			cellNum++;
			cell = row.createCell(cellNum);
			cell.setCellValue("Tarefa");
			cell.setCellStyle(cellStyle);
			cellNum = 0;
			rowNum++;
		//=========================================
		
		
		
		
		
		for (ProcessoDTO processo : lastQuery) {
			row = sheet.createRow(rowNum);
			for (int i = 0; i < 1; i++) {
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getAutor());
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getCentroDeCusto());				
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue((processo.getCentroDeCustoSit().equals("")?"NAO INFORMADO":processo.getCentroDeCustoSit().equals("A")?"ATIVO":"INATIVO"));				
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getCentroDeCustoManual());				
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue((processo.getCentroDeCustoManualSit().equals("")?"NAO INFORMADO":processo.getCentroDeCustoManualSit().equals("A")?"ATIVO":"INATIVO"));				
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getNroProcesso());				
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getDataCitacao());
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getDataFinalizacao());
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getMesesDiff());
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getEscritorio());
				cellNum++;
				cell = row.createCell(cellNum);
				cell.setCellValue(processo.getNroTarefa());
			}
			rowNum++;
			cellNum = 0;
		}
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);
		sheet.autoSizeColumn(5);
		sheet.autoSizeColumn(6);
		sheet.autoSizeColumn(7);
		sheet.autoSizeColumn(8);
		sheet.autoSizeColumn(9);
		sheet.autoSizeColumn(10);
		sheet.setAutoFilter(new CellRangeAddress(0, rowNum, 0, 10));

		workbook.write(out);
		out.close();
	}

	//Utilizado para armazenar a ultima query e agilizar o salvamento em XLS depois
	public void attToLastQuery(List<ProcessoDTO> lq) {
		lastQuery = new ArrayList<ProcessoDTO>(lq);
	}

}
