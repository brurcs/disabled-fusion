package com.neomind.fusion.custom.orsegups.scheduler;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class EmailDeliveryXVIDComImagens implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliveryXVIDComImagens.class);
    @SuppressWarnings("unused")
    private Pattern pattern;
    private final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String EVT_XVID = "XVID";

    private boolean comRegistro = false;
    private List<String> errosRegistro;

    @SuppressWarnings({ "unchecked", "static-access", "deprecation" })
    @Override
    public void execute(CustomJobContext arg0) {

	int adicionados = 0;

	log.warn("E-Mail XVID Inicio execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailAutomaticoDesvioDeHabito");
	InstantiableEntityInfo ExcecoesEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CadastroExcecoesEmailsDesvioDeHabito");
	InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailDelivery");
	InstantiableEntityInfo infoImagens = (InstantiableEntityInfo) EntityRegister.getEntityInfo("SIGMAImagensEventoXVID");

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	Connection conn = null;
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	pattern = Pattern.compile(EMAIL_PATTERN);

	String ultimaExecucaoRotina = OrsegupsEmailUtils.ultimaExecucaoRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_XVID_COM_IMAGENS);

	try {

	    strSigma.append(" 	 SELECT c.OBSERVACAO, c.CGCCPF, c.FANTASIA, c.RAZAO, c.ID_CENTRAL,c.ID_EMPRESA, c.PARTICAO, c.ENDERECO, cid.NOME AS NM_CIDADE, bai.NOME AS NM_BAIRRO, h.DT_FECHAMENTO, c.EMAILRESP,");
	    strSigma.append(" 	 hfe.NM_FRASE_EVENTO as NM_FRASE_EVENTO, ");
	    strSigma.append(" 	 h.TX_OBSERVACAO_FECHAMENTO, ISNULL(ma.DS_MOTIVO_ALARME, 'N/A - SEM ALTERAÇÃO') as motivo, h.CD_HISTORICO AS CD_HISTORICO, ");
	    strSigma.append(" 	 h.CD_EVENTO, c.CD_CLIENTE,  c.NU_LATITUDE, c.NU_LONGITUDE, h.DT_RECEBIDO, c.SERVIDORCFTV, c.PORTACFTV, c.USERCFTV, c.SENHACFTV  ");
	    strSigma.append(" 	  FROM VIEW_HISTORICO h  WITH (NOLOCK)    ");
	    strSigma.append(" 	 INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA   ");
	    strSigma.append(" 	 INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
	    strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
	    strSigma.append(" 	 LEFT JOIN MOTIVO_ALARME ma ON ma.CD_MOTIVO_ALARME = h.CD_MOTIVO_ALARME  ");
	    strSigma.append(" 	 LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE  ");
	    strSigma.append(" 	 LEFT JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO  ");
	    strSigma.append(" 	 WHERE C.TP_PESSOA != 2 AND (H.CD_EVENTO = 'XVID' OR H.CD_EVENTO ='XVD2' OR H.CD_EVENTO ='XVI1' OR H.CD_EVENTO ='XPLE' OR H.CD_EVENTO ='XPE2' OR (H.CD_EVENTO = 'XXX7' AND H.CD_CODE='VID')  ");
	    strSigma.append("    OR H.CD_EVENTO IN ('E806','E808','E809','E830','E832') OR (H.CD_EVENTO = 'E401' AND H.CD_CODE = 'C52'))");
	    strSigma.append(" 	 AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_EmailAutomaticoDesvioDeHabito em where em.historico = h.CD_HISTORICO)  ");
	    strSigma.append("    	 AND (h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#AtualizarCadastro%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#NaoEnviaComunicado%')   ");
	    strSigma.append(" 	 AND CD_USUARIO_FECHAMENTO <> 9999     ");
	    strSigma.append("   	 AND LEN(h.TX_OBSERVACAO_FECHAMENTO)>1 ");
	    strSigma.append(" 	 AND ( DT_FECHAMENTO > '" + ultimaExecucaoRotina + "' ) ");

	    // CONTA B409 99 -> strSigma.append(" AND H.cd_cliente = 96674");

	    conn = PersistEngine.getConnection("SIGMA90");

	    pstm = conn.prepareStatement(strSigma.toString());

	    OrsegupsEmailUtils.inserirFimRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_XVID_COM_IMAGENS);

	    rs = pstm.executeQuery();

	    String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
	    // String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");

	    while (rs.next()) {

		this.comRegistro = false;
		this.errosRegistro = new ArrayList<String>();

		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));

		String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");
		if (!clienteComExcecao) {
		    String cgcCpf = rs.getString("CGCCPF") == null ? "" : rs.getString("CGCCPF").replaceAll("([.\\-/])", "").trim();
		    String fantasia = (rs.getString("FANTASIA") == null ? "" : rs.getString("FANTASIA"));
		    String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
		    String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
		    String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
		    String cidade = (rs.getString("NM_CIDADE") == null ? "" : rs.getString("NM_CIDADE"));
		    String bairro = (rs.getString("NM_BAIRRO") == null ? "" : rs.getString("NM_BAIRRO"));
		    Timestamp dtViaturaNoLocal = rs.getTimestamp("DT_FECHAMENTO");

		    String nomeFraseEvento = (rs.getString("NM_FRASE_EVENTO") == null ? "" : rs.getString("NM_FRASE_EVENTO"));
		    String txObsevacaoFechamento = (rs.getString("TX_OBSERVACAO_FECHAMENTO") == null ? "" : rs.getString("TX_OBSERVACAO_FECHAMENTO"));
		    String motivo = (rs.getString("motivo") == null ? "" : rs.getString("motivo"));
		    String evento = (rs.getString("CD_EVENTO") == null ? "" : rs.getString("CD_EVENTO"));
		    String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
		    String cdCliente = (rs.getString("CD_CLIENTE") == null ? "" : rs.getString("CD_CLIENTE"));
		    String dataAtendimento = NeoDateUtils.safeDateFormat(dtViaturaNoLocal, "dd/MM/yyyy");
		    String horaAtendimento = rs.getString("DT_FECHAMENTO").substring(11, 16);
		    String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));
		    Timestamp dtRecebido = rs.getTimestamp("DT_RECEBIDO");
		    // String dtRecebidoStr = NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy HH:mm:ss");

		    String central = rs.getString("ID_CENTRAL");
		    int empresa = rs.getInt("ID_EMPRESA");

		    log.warn("[XVID] fantasia:" + fantasia + ", razao:" + razao + ", email" + email);

		    EventoVO eventoVO = new EventoVO();

		    eventoVO.setCodigoCentral(central);
		    eventoVO.setEmpresa(String.valueOf(empresa));
		    eventoVO.setDtRecebido(dtRecebido);
		    eventoVO.setCodigoHistorico(historico);
		    eventoVO.setRazao(razao);
		    eventoVO.setServidorCFTV(rs.getString("SERVIDORCFTV") != null ? rs.getString("SERVIDORCFTV") : "");
		    eventoVO.setPortaCFTV(rs.getString("PORTACFTV") != null ? rs.getString("PORTACFTV") : "");
		    eventoVO.setUsuarioCFTV(rs.getString("USERCFTV") != null ? rs.getString("USERCFTV") : "");
		    eventoVO.setSenhaCFTV(rs.getString("SENHACFTV") != null ? rs.getString("SENHACFTV") : "");

		    this.getImagensEvento(eventoVO);

		    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
			String array[] = txObsevacaoFechamento.split("###");
			String textoFormatado = "";
			for (String string : array) {
			    textoFormatado += string + "<br/>";
			}
			array = txObsevacaoFechamento.split(" ");
			List<String> respostas = new ArrayList<String>();
			List<String> cameras = new ArrayList<String>();
			int i = 0;
			for (String string : array) {
			    if (string.contains("R:")) {
				String arrayResp[] = string.split("\\n");
				if (!respostas.contains(arrayResp[0])) {
				    respostas.add(arrayResp[0]);
				}
			    }
			    if (string.equals("camera") || string.equals("câmera")) {
				if (array.length > i + 1) {
				    String c = string + " " + array[i + 1];
				    if (!c.contains("(") && !c.contains(")") && !c.contains("{") && !c.contains("}") && !c.contains("[") && !c.contains("]")) {
					cameras.add(c);
				    }
				}
			    } else {
				if (string.contains("camera") || string.contains("câmera")) {
				    if (string.contains(":")) {
					String arrayCam[] = string.split(":");
					cameras.clear();
					if (arrayCam.length > 1 && array.length > i + 1) {
					    cameras.add(arrayCam[1] + " " + array[i + 1]);
					}
				    } else {
					String arrayCam[] = string.split("\\n");
					for (String s : arrayCam) {
					    if (s.equals("camera") || s.equals("câmera")) {
						if (array.length > i + 1) {
						    cameras.add(s + " " + array[i + 1]);
						}
					    }
					}
				    }
				}
			    }
			    i++;
			}

			if (!textoFormatado.isEmpty()) {
			    textoFormatado = textoFormatado.replaceAll("#", "");
			    textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			    textoFormatado = textoFormatado.replaceAll(";", "");
			    textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
			    // textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);

			    if (respostas.size() > 0) {
				for (String resposta : respostas) {
				    textoFormatado = textoFormatado.replaceAll(resposta, "<br/>" + resposta + "<br/><br/>");
				}
			    }

			    if (cameras.size() > 0) {
				for (String camera : cameras) {
				    textoFormatado = textoFormatado.replaceAll(camera, "<br/>" + camera);
				}
			    }

			    textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
			    textoFormatado = textoFormatado.replaceAll("Ramal", "a partir do ramal ");

			    textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
			    textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
			    textoFormatado = textoFormatado.replaceAll("TratarSigma", "");
			    textoFormatado = textoFormatado.replaceAll("#ComAlteracao", "");

			    textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
			    textoFormatado = textoFormatado.replaceAll("SemContato", " Não houve sucesso em nenhuma tentativa de contato.");
			    textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
			    textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");

			    txObsevacaoFechamento = textoFormatado;
			}
		    }
		    boolean flagAtualizadoNAC = observacao.contains("#AC");
		    List<String> emailClie = OrsegupsEmailUtils.validarEmail(email, cdCliente, flagAtualizadoNAC);
		    if ((emailClie != null) && (!emailClie.isEmpty())) {
			log.warn("[XVID] email do cliente validado");

			final String tipo = OrsegupsEmailUtils.TIPO_RAT_VERIFICACAO;

			Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

			String pasta = null;
			String remetente = null;
			String grupo = null;

			if (params != null) {
			    pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
			    remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
			    grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
			    String neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);
			    Long tipRat = 7L;
			    String ratingToken = DigestUtils.sha256Hex(cgcCpf + Long.toString(tipRat) + Long.toString(new Date().getTime()));
			    
			    for (String emailFor : emailClie) {
				log.warn("[XVID] addTo: " + emailFor);
				StringBuilder noUserMsg = new StringBuilder();
				List<NeoObject> neoObjects = null;
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("email", emailFor));
				groupFilter.addFilter(new QLEqualsFilter("tratarXVID", Boolean.TRUE));

				neoObjects = PersistEngine.getObjects(ExcecoesEmail.getEntityClass(), groupFilter);

				if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty())) {

				    noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, ratingToken));

				    noUserMsg.append("\n <table width=\"600\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td colspan=\"6\" style=\"padding-left:1%;padding-right:18%;\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Informamos o atendimento ao seguinte evento de seu sistema de segurança:</p>");
				    noUserMsg.append("\n </td></tr> </br>");
				    noUserMsg.append("\n <tr align='center'> ");
				    noUserMsg.append("\n <td colspan=\"6\" style=\"text-align: center;\"> <p style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;text-align:center;color:#007FFF;font-weight:bold;padding:10px;margin-top:0px;\">VERIFICAÇÃO DE IMAGENS DO CFTV</p>");
				    noUserMsg.append("\n </td> </tr><br/>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <tr width=\"600\"> ");
				    noUserMsg
					    .append("\n <td  style=\"padding:5px;\"><h2 style=\"border-bottom:1px solid #CCC;margin:0px 15px 20px 5px;padding-bottom:10px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">DETALHES SOBRE O ATENDIMENTO</td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <td><table width=\"600\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"8\">");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + txObsevacaoFechamento + "</p></td>");
					noUserMsg.append("\n </tr>");
				    }
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td colspan=\"6\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:14px;\">");
				    noUserMsg.append("\n <img src=\"https://maps.googleapis.com/maps/api/staticmap?center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
					    + "&zoom=19&size=300x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
					    + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "\" width=\"300\" height=\"300\" alt=\"\"/></p><p style=\"font-family: 'Verdana';font-weight:normal;font-size:8px;width: 300px;\"></p></td>");
				    noUserMsg.append("\n <td width=\"10%\"><p><br>");
				    noUserMsg.append("\n </p></td>");
				    noUserMsg.append("\n <td width=\"51%\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Local </br> <strong>" + razao + "</strong></br>");
				    noUserMsg.append("\n <strong>" + endereco + "</strong></br>");
				    noUserMsg.append("\n <strong>" + bairro + "</strong></br>");
				    noUserMsg.append("\n <strong>" + cidade + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Data <br> <strong>" + dataAtendimento + "</strong></br>");
				    noUserMsg.append("\n Hora  </br> <strong>" + horaAtendimento + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Evento </br> ");
				    // noUserMsg.append("\n <strong>VERIFICAÇÃO IMAGENS DO CLIENTE</strong></p> ");
				    noUserMsg.append("\n <strong>" + nomeFraseEvento + "</strong></p> ");

				    noUserMsg.append("\n </td></tr>");
				    noUserMsg.append("\n <tr>");

				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n </table>");
				    noUserMsg.append("\n <br/><table width=\"600\" border=\"0\" style=\"margin-bottom:-3px;\">");
				    noUserMsg.append("\n <tbody>");

				    List<NeoObject> listaImagens = new ArrayList<NeoObject>();

				    int y = 0;

				    QLGroupFilter filter = new QLGroupFilter("AND");
				    filter.addFilter(new QLEqualsFilter("historico", historico));

				    listaImagens = (List<NeoObject>) PersistEngine.getObjects(infoImagens.getEntityClass(), filter, -1, -1, "camera asc");
				    String lisLnk = "";

				    if (listaImagens != null && !listaImagens.isEmpty()) {
					for (NeoObject neoObject : listaImagens) {
					    if (y == 2) {
						noUserMsg.append("\n </tr>");
						noUserMsg.append("\n <tr>");
						y = 0;
					    }
					    EntityWrapper wpneo = new EntityWrapper(neoObject);
					    //
					    noUserMsg.append("\n <td valign=\"top\"> ");
					    noUserMsg.append(" <img src=\"https://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("imagem")).getNeoId() + "\" height=\"240\" width=\"320\" border=\"0\" alt=\"Imagens\"></td>");
					    y++;
					    lisLnk = lisLnk + "https://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("imagem")).getNeoId() + ";";
					}
					//
				    }
				    try {
					List<String> urls = OrsegupsEmailUtils.getImagensAmazonS3(Integer.valueOf(historico));
					if (urls != null) {
					    comRegistro = true;
					    for (String url : urls) {
						if (y == 2) {
						    noUserMsg.append("\n </tr>");
						    noUserMsg.append("\n <tr>");
						    y = 0;
						}
						//
						noUserMsg.append("\n <td valign=\"top\"> ");
						noUserMsg.append(" <img src=\"" + url + "\" height=\"240\" width=\"320\" border=\"0\" alt=\"Imagens\"></td>");
						y++;
						lisLnk = lisLnk + url + ";";
					    }
					} else {
					    try {
						errosRegistro = OrsegupsEmailUtils.getErroGravacaoImagem(Integer.parseInt(historico));
					    } catch (Exception e) {
						e.printStackTrace();
					    }

					    if (!errosRegistro.isEmpty()) {
						comRegistro = true;
					    }
					}
				    } catch (Exception e) {
					log.error("##ImagensAmazonS3## Erro ao buscar imagens para o evento: " + historico);
					e.printStackTrace();
				    }
				    noUserMsg.append("\n </tr>");

				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n <br/></table>");

				    // FIM DO BLOCO DE IMAGENS;

				    noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, email, remetente, ratingToken));
				    noUserMsg.append("\n <br><p style=\"font-family: 'Verdana';font-weight:normal;font-size:6px;text-align:justify; margin: 0 27%;\">CD Cliente: ");
				    noUserMsg.append(cdCliente + " Conta: " + central + " Partição: " + particao + " Razão: " + razao + " CR: " + this.comRegistro);
				    for (String e : this.errosRegistro) {
					noUserMsg.append("|e id:");
					noUserMsg.append(e);
				    }
				    noUserMsg.append("</p></td>");

				    NeoObject emailHis = infoHis.createNewInstance();
				    EntityWrapper emailHisWp = new EntityWrapper(emailHis);
				    adicionados = adicionados + 1;
				    emailHisWp.findField("fantasia").setValue(fantasia);
				    emailHisWp.findField("razao").setValue(razao);
				    emailHisWp.findField("particao").setValue(particao);
				    emailHisWp.findField("endereco").setValue(endereco);
				    emailHisWp.findField("cidade").setValue(cidade);
				    emailHisWp.findField("bairro").setValue(bairro);
				    emailHisWp.findField("dataFechamento").setValue(dataAtendimento);
				    emailHisWp.findField("horaFechamento").setValue(horaAtendimento);
				    // emailHisWp.findField("nomeViatura").setValue(nomeViatura);
				    // emailHisWp.findField("textoObservacaoFechamentos").setValue(txObsevacaoFechamento);
				    emailHisWp.findField("motivoAlarme").setValue(motivo);
				    emailHisWp.findField("evento").setValue(evento);
				    emailHisWp.findField("enviadoPara").setValue(emailClie.toString());
				    emailHisWp.findField("nomeFraseEvento").setValue(nomeFraseEvento);
				    emailHisWp.findField("historico").setValue(historico);
				    PersistEngine.persist(emailHis);
				    
				    // TESTE E-MAIL UTILIZANDO RECURSO FUSION
				    //String subject = "Relatório de Atendimento XVID - " + fantasia;
				    //OrsegupsEmailUtils.sendTestEmail(noUserMsg, subject);

				    GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
				    NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
				    EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
				    emailEnvioWp.findField("de").setValue("cm.rat" + remetente);
				    // emailEnvioWp.findField("para").setValue(emailFor +
				    emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br;copia@orsegups.com.br");
				    emailEnvioWp.findField("assunto").setValue("Relatório de Atendimento XVID - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
				    emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
				    emailEnvioWp.findField("datCad").setValue(dataCad);
				    PersistEngine.persist(emaiEnvio);

				    try {

					ObjRatMobile objRat = new ObjRatMobile();
					//
					/**
					 * Tipos de Rats para mobile:
					 * RATATENDIMENTOTATICO(0L,
					 * "Atendimento TÃƒÂ¡tico"),
					 * RATDESVIODEHABITO(1L,
					 * "Desvio de hÃƒÂ¡bito");
					 */
					objRat.setTipRat(tipRat);
					objRat.setRatingToken(ratingToken);
					objRat.setInformativo("Informamos o atendimento ao seguinte evento de seu sistema de segurança:");
					objRat.setEvento(nomeFraseEvento);
					objRat.setResultado("");
					objRat.setHashId("Relatório de Atendimento XVID - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
					objRat.setResultadoDoAtendimento("VERIFICAÇÃO DE IMAGENS DO CFTV");
					objRat.setAtendidoPor("");
					objRat.setLocal(razao + " - " + endereco + " - " + bairro + " - " + cidade);
					objRat.setDataAtendimento(dataAtendimento);
					objRat.setHoraAtendimento(horaAtendimento);
					objRat.setLnkFotoLocal("https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyDie-lwDjXfRv0diccYyQtC8ZuGt-P4scs&center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
						+ "&zoom=19&size=500x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
						+ rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE"));
					objRat.setLnkFotoAit("");
					objRat.setCdCliente(cdCliente);

					if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					    objRat.setObservacao(txObsevacaoFechamento);
					} else {
					    objRat.setObservacao("");
					}
					objRat.setEmpRat(grupo);
					objRat.setNeoId(neoId);
					objRat.setLnkImg(lisLnk);

					if (!cgcCpf.equals("")) {
					    objRat.setCgcCpf(Long.parseLong(cgcCpf));
					    IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();
					    integracao.inserirInformacoesPush(objRat);
					}

				    } catch (Exception e) {
					e.printStackTrace();
				    }

				    log.warn("Cliente [XVID]: " + fantasia + ", E-mail: " + emailFor);
				}
			    }
			    OrsegupsUtils.sendEmailWhatsAppRAT(Long.valueOf(cdCliente), "Relatório de Atendimento XVID - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento, endereco + " - " + bairro + " - " + cidade, dataAtendimento + " " + horaAtendimento);
			} else {
			    if (!empresasNotificadas.contains(empresa)) {
				OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
				empresasNotificadas.add(empresa);
			    }
			}
		    } else {
			log.warn("[XVID] Email do cliente invalido vou vazio ");
		    }
		}
	    }
	    log.warn("E-Mail XVID Fim execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	} catch (Exception e) {
	    log.error("E-Mail XVID erro no processamento:");
	    System.out.println("[" + key + "] E-Mail XVID erro no processamento: " + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    public static List<String> dias(GregorianCalendar calendar) {
	List<String> cptList = null;
	if (NeoUtils.safeIsNotNull(calendar)) {
	    long milisecInicial = calendar.getTime().getTime();
	    long milisecFinal = new GregorianCalendar().getTime().getTime();
	    long dif = milisecFinal - milisecInicial;

	    long dias = (((dif / 1000) / 60) / 60) / 24;
	    cptList = new ArrayList<String>();
	    Calendar dia = Calendar.getInstance();
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
	    for (int i = 0; i <= dias; i++) {
		String diaStr = dateFormat.format(dia.getTime());
		cptList.add(diaStr);
		dia.add(Calendar.DAY_OF_MONTH, -1);
	    }

	}
	return cptList;
    }

    /**
     * Retorna as imagens registradas pelo operador no momento do atendimento do
     * evento
     * 
     * @param central
     * @param empresa
     * @param dtRecebido
     * @param historico
     * @param razao
     * @throws FileNotFoundException
     */

    private void getImagensEvento(EventoVO eventoVO) throws FileNotFoundException {
	// String central, int empresa, Timestamp dtRecebido, String historico,
	// String razao;

	String historico = eventoVO.getCodigoHistorico();

	InputStream is = null;

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	BufferedImage image = null;

	File outputfile = null;

	/**
	 * Imagens tiradas pelo AIT
	 */
	try {

	    conn = PersistEngine.getConnection("SIGMA90");

	    String sql = "SELECT BL_IMAGEM FROM VTR_IMAGEM WITH(NOLOCK) WHERE CD_HISTORICO IN (" + historico + ")";

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    int count = 0;

	    while (rs.next()) {

		is = rs.getBinaryStream("BL_IMAGEM");

		image = ImageIO.read(is);

		outputfile = new File("image.jpg");

		if (image != null) {
		    ImageIO.write(image, "jpg", outputfile);

		    InstantiableEntityInfo colAit = AdapterUtils.getInstantiableEntityInfo("SigmaImagensEventoXVID");

		    NeoObject noInatUsu = colAit.createNewInstance();
		    EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);

		    NeoFile newFile = new NeoFile();
		    newFile.setName(outputfile.getName());
		    newFile.setStorage(NeoStorage.getDefault());
		    PersistEngine.persist(newFile);
		    PersistEngine.evict(newFile);
		    OutputStream outi = newFile.getOutputStream();
		    InputStream in = null;
		    in = new BufferedInputStream(new FileInputStream(outputfile));
		    NeoStorage.copy(in, outi);
		    newFile.setOrigin(NeoFile.Origin.TEMPLATE);
		    tmeWrapper.findField("historico").setValue(historico);
		    tmeWrapper.findField("camera").setValue("camera" + count);
		    tmeWrapper.findField("imagem").setValue(newFile);
		    tmeWrapper.findField("dataCadastro").setValue(new GregorianCalendar());
		    PersistEngine.persist(noInatUsu);

		    count++;

		}

	    }

	} catch (Exception e) {
	    e.printStackTrace();

	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	/**
	 * Fim imagens AIT
	 */

	/**
	 * Imagens registradas pelo botÃ£o da tela de atendimetno de eventos
	 */
	QLGroupFilter filtroImagens = new QLGroupFilter("AND");
	filtroImagens.addFilter(new QLEqualsFilter("cdHistorico", Long.valueOf(historico)));

	@SuppressWarnings("unchecked")
	List<NeoObject> listaImagensRegistradas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("cmRegistroImagens"), filtroImagens, -1, -1, "data asc");

	List<GregorianCalendar> listaDatasEvento = null;

	if (listaImagensRegistradas != null && !listaImagensRegistradas.isEmpty()) {

	    this.comRegistro = true;

	    System.out.println("RATXVIDPRINT Encontrou registros de print " + historico);

	    listaDatasEvento = new ArrayList<GregorianCalendar>();

	    for (NeoObject registro : listaImagensRegistradas) {

		EntityWrapper wrapperDadosRegistro = new EntityWrapper(registro);

		GregorianCalendar dataEvento = (GregorianCalendar) wrapperDadosRegistro.findField("data").getValue();

		listaDatasEvento.add(dataEvento);

		System.out.println("RATXVIDPRINT Adicionou horario registrado para o evento " + historico);

	    }

	}

	/**
	 * COMO PASSAM A VALER APENAS AS IMAGENS DE SNAPSHOT, METODOS DE BUSCA
	 * DE CAMERA FICARAM DENTRO DA VALIDAÃ‡AO DA LISTA DE MOMENTOS
	 */

	if (listaDatasEvento != null && !listaDatasEvento.isEmpty()) {

	    TreeMap<String, String> treeMap = this.getCameraCliente(eventoVO);

	    List<String> cameras = new ArrayList<String>();

	    if (treeMap != null && !treeMap.isEmpty()) {
		for (Entry<String, String> entry : treeMap.entrySet()) {

		    String camera = "";
		    if (entry != null) {
			camera = String.valueOf(entry.getKey());
			System.out.println("Camera: " + camera);
			cameras.add(camera);

		    }
		}
	    }

	    for (int i = 0; i < listaDatasEvento.size(); i++) {

		GregorianCalendar dataEvento = listaDatasEvento.get(i);

		String dataHora = NeoDateUtils.safeDateFormat(dataEvento, "dd/MM/yyyy HH:mm:ss");
		String hora = dataHora.substring(10, dataHora.length());
		hora = hora.trim();
		hora = hora.replaceAll(":", "-");
		String data = dataHora.substring(0, 10);
		data = data.trim();
		data = data.replaceAll("/", "-");

		for (String camera : cameras) {

		    InstantiableEntityInfo colAit = AdapterUtils.getInstantiableEntityInfo("SigmaImagensEventoXVID");

		    NeoObject noInatUsu = colAit.createNewInstance();
		    EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);
		    File file = new File("image");
		    file = retornaImagem(data, hora, camera, eventoVO);
		    if (file != null) {
			NeoFile newFile = new NeoFile();
			newFile.setName(file.getName());
			newFile.setStorage(NeoStorage.getDefault());
			PersistEngine.persist(newFile);
			PersistEngine.evict(newFile);
			OutputStream outi = newFile.getOutputStream();
			InputStream in = null;
			in = new BufferedInputStream(new FileInputStream(file));
			NeoStorage.copy(in, outi);
			newFile.setOrigin(NeoFile.Origin.TEMPLATE);
			tmeWrapper.findField("historico").setValue(historico);
			tmeWrapper.findField("camera").setValue(camera);
			tmeWrapper.findField("imagem").setValue(newFile);
			tmeWrapper.findField("dataCadastro").setValue(new GregorianCalendar());
			PersistEngine.persist(noInatUsu);
		    }

		}

	    }
	}

	/**
	 * Não enviamos mais imagens aleatorias do evento, somente as capturadas
	 * pelo operador SE ESTIVER LENDO APÃ“S 07/02/2017 EXCLUIR BLOCO
	 * COMENTADO (MODIFICAÇÃO FOI VALIDADA)
	 */

	// else{
	// /**
	// * Imagens das cÃ¢meras na hora do evento
	// */
	//
	//
	// GregorianCalendar calendar = new GregorianCalendar();
	// calendar.setTimeInMillis(dtRecebido.getTime());
	//
	// String dataHora = NeoDateUtils.safeDateFormat(calendar,
	// "dd/MM/yyyy HH:mm:ss");
	//
	// System.out.println("dtRecebido: " + dataHora);
	//
	//
	// String hora = dataHora.substring(10, dataHora.length());
	// hora = hora.trim();
	// hora = hora.replaceAll(":", "-");
	// String data = dataHora.substring(0, 10);
	// data = data.trim();
	// data = data.replaceAll("/", "-");
	//
	// for (String camera : cameras) {
	//
	// InstantiableEntityInfo colAit =
	// AdapterUtils.getInstantiableEntityInfo("SigmaImagensEventoXVID");
	//
	// NeoObject noInatUsu = colAit.createNewInstance();
	// EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);
	// File file = new File("image");
	// file = retornaImagem(data, hora, camera, razao);
	// NeoFile newFile = new NeoFile();
	// newFile.setName(file.getName());
	// newFile.setStorage(NeoStorage.getDefault());
	// PersistEngine.persist(newFile);
	// PersistEngine.evict(newFile);
	// OutputStream outi = newFile.getOutputStream();
	// InputStream in = null;
	// in = new BufferedInputStream(new FileInputStream(file));
	// NeoStorage.copy(in, outi);
	// newFile.setOrigin(NeoFile.Origin.TEMPLATE);
	// tmeWrapper.findField("historico").setValue(historico);
	// tmeWrapper.findField("camera").setValue(camera);
	// tmeWrapper.findField("imagem").setValue(newFile);
	// tmeWrapper.findField("dataCadastro").setValue(new
	// GregorianCalendar());
	// PersistEngine.persist(noInatUsu);
	//
	// }
	//
	// /**
	// * Fim imagens hora evento
	// */
	// }

    }

    /**
     * Retorna cameras vinculadas Ã  conta + empresa solicitada
     * 
     * @param idCentral
     * @param empresa
     * @param razao
     *            -> Utilizado para validar servidor do D-Guard
     * @return TreeMap contendo Numero da camera e sua DescriÃ§Ã£o
     */
    private TreeMap<String, String> getCameraCliente(EventoVO eventoVO) {

	// String idCentral, String empresa, String razao

	TreeMap<String, String> treeMap = null;

	String resposta = "";
	try {
	    String userpass = eventoVO.getUsuarioCFTV() + ":" + eventoVO.getSenhaCFTV();

	    URL oracle = new URL("http://" + eventoVO.getServidorCFTV() + ":" + eventoVO.getPortaCFTV() + "/camerasnomes.cgi?receiver=" + eventoVO.getEmpresa() + "&server=" + eventoVO.getCodigoCentral());
	    URLConnection yc = oracle.openConnection();
	    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
	    yc.setRequestProperty("Authorization", basicAuth);
	    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
	    String inputLine;
	    while ((inputLine = in.readLine()) != null) {
		resposta = inputLine;
	    }
	    in.close();
	    if (resposta != null && !resposta.isEmpty() && resposta.contains("=")) {
		String array[] = resposta.split("&");
		treeMap = new TreeMap<String, String>();

		for (String valor : array) {
		    if (valor != null && !valor.isEmpty() && valor.contains("=")) {
			String parametros[] = valor.split("=");
			treeMap.put(parametros[0], (String) parametros[1]);
		    }

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return treeMap;
    }

    /**
     * Retorna imagem solicitada se a mesma estiver entre o momento da abertura
     * do evento e o momento registrado pelo operador
     * 
     * @param data
     * @param hora
     * @param camera
     * @param razao
     * @param dtRecebido
     * @return File contendo a imagem ou null
     */
    private File retornaImagem(String data, String hora, String camera, EventoVO eventoVO) {
	// String img =
	// "http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/cm/imagens/camera-motion_not_found.png";
	String resolucao = "640x480";
	String qualidade = "100";
	String formato = "jpg";
	// 192.168.20.227

	// String data, String hora, String camera, String razao, Timestamp
	// dtRecebido

	String urlStr = "http://" + eventoVO.getServidorCFTV() + ":" + eventoVO.getPortaCFTV() + "/player/getimagem.cgi?camera=" + camera + "&resolucao=" + resolucao + "&qualidade=" + qualidade + "&formato=" + formato + "&data=" + data + "&hora=" + hora;
	ByteArrayOutputStream output = null;
	URL url = null;
	URLConnection uc = null;
	InputStream is = null;
	BufferedImage image = null;
	File outputfile = null;
	try {
	    output = new ByteArrayOutputStream();
	    String userpass = eventoVO.getUsuarioCFTV() + ":" + eventoVO.getSenhaCFTV();
	    ;

	    url = new URL(urlStr);
	    uc = url.openConnection();
	    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
	    uc.setRequestProperty("Authorization", basicAuth);
	    is = uc.getInputStream();

	    /**
	     * NOVO BLOCO DE VALIDACAO DE HORARIO DA IMAGEM RECEBIDA, SE IMAGEM
	     * FORA DO INTERVALO HORA DO EVENTO - MOMENTO DO REGISTRO, IMAGEM
	     * SERÃ� DESCARTADA
	     */

	    String tipoRetorno = uc.getContentType();
	    /**
	     * VALIDA SE RETORNO DA REQUISIÇÃO É DO TIPO IMAGE/JPG, SE FOR
	     * TEXT/PLAIN RETORNO Ã‰ UMA DAS MENSAGEM DE IMAGEM INDISPONIVEL
	     */
	    if (tipoRetorno.equalsIgnoreCase("image/jpeg")) {
		String stringLastModified = uc.getHeaderField("Last-Modified");

		DateFormat formatLastModified = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH);

		Date lastModified = formatLastModified.parse(stringLastModified);

		String stringMomentoRegistrado = data + " " + hora;

		DateFormat formatMomentoRegistrado = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");

		Date momentoRegistrado = formatMomentoRegistrado.parse(stringMomentoRegistrado);

		Date momentoEvento = new Date(eventoVO.getDtRecebido().getTime());

		/**
		 * VALIDA SE MOMENTO ESTA DENTRO DO INTERVALO OU É UM DOS
		 * EXTREMOS
		 */
		if (!lastModified.before(momentoEvento) && !lastModified.after(momentoRegistrado)) {
		    outputfile = new File("image.jpg");

		    image = ImageIO.read(is);

		    if (image != null) {
			String teste[] = image.getPropertyNames();
			System.out.println(teste);
			ImageIO.write(image, "jpg", outputfile);
		    } else {

			// BufferedImage imageAux = null;
			// URL urlAux = new URL(img);
			// imageAux = ImageIO.read(urlAux);
			// ImageIO.write(imageAux, "png", outputfile);
			//
			// System.out.println("Imagem de retorno com problema camera="
			// + camera + "&resolucao=" + resolucao + "&qualidade="
			// +
			// qualidade + "&formato=" + formato + "&data=" + data +
			// "&hora=" + hora);

			/**
			 * ADICIONADO RETORNO NULO PARA CAMERAS SEM IMAGEM, NÃƒO
			 * SERÃ� MAIS ENVIADO IMAGEM DE ERRO, SE ESTIVER LENDO
			 * APÃ“S 07/01/2017 EXCLUIR BLOCO COMENTADO
			 * (MODIFICAÃ‡ÃƒO FOI VALIDADA)
			 */

			return null;
		    }
		} else {
		    return null;
		}
	    } else {
		return null;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    try {
		if (is != null)
		    is.close();
		if (output != null) {
		    output.flush();
		    output.close();
		}
	    } catch (IOException e) {
		e.printStackTrace();
	    }

	}

	return outputfile;
    }

}