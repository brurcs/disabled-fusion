package com.neomind.fusion.custom.orsegups;

import java.util.Map;

import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.entity.EntityAdapter;
import com.neomind.fusion.entity.EntityWrapper;

public class TesteEntityAdapter implements EntityAdapter
{
	@Override
	public void run(Map<String, Object> params)
	{
		try
		{
			EForm eform = (EForm) params.get(EFORM);	
			
			EntityWrapper wrapper = new EntityWrapper(eform.getObject());
			
			wrapper.setValue("copiaNome", wrapper.findValue("nome"));
			wrapper.setValue("title", wrapper.findValue("nome"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
