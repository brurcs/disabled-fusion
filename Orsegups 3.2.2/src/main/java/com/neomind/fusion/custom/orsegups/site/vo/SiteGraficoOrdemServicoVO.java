package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.GregorianCalendar;

public class SiteGraficoOrdemServicoVO
{
	private Long cdCliente;
	private GregorianCalendar data;
	private GregorianCalendar dataInicial;
	private Long tempo;
	private Long cdViatura;
	private String observacao;
	private String foto;
	private String nmViatura;
	private String nmFraseEvento;
	private String toolTipHtml;

	public Long getCdViatura()
	{
		return cdViatura;
	}
	public void setCdViatura(Long cdViatura)
	{
		this.cdViatura = cdViatura;
	}
	public String getObservacao()
	{
		return observacao;
	}
	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}
	public String getFoto()
	{
		return foto;
	}
	public void setFoto(String foto)
	{
		this.foto = foto;
	}
	public String getNmViatura()
	{
		return nmViatura;
	}
	public void setNmViatura(String nmViatura)
	{
		this.nmViatura = nmViatura;
	}
	public String getNmFraseEvento()
	{
		return nmFraseEvento;
	}
	public void setNmFraseEvento(String nmFraseEvento)
	{
		this.nmFraseEvento = nmFraseEvento;
	}
	public GregorianCalendar getData() {
		return data;
	}
	public void setData(GregorianCalendar data) {
		this.data = data;
	}
	public Long getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(Long cdCliente) {
		this.cdCliente = cdCliente;
	}
	public Long getTempo() {
		return tempo;
	}
	public void setTempo(Long tempo) {
		this.tempo = tempo;
	}
	public String getToolTipHtml()
	{
		return toolTipHtml;
	}
	public void setToolTipHtml(String toolTipHtml)
	{
		this.toolTipHtml = toolTipHtml;
	}
	public GregorianCalendar getDataInicial()
	{
		return dataInicial;
	}
	public void setDataInicial(GregorianCalendar dataInicial)
	{
		this.dataInicial = dataInicial;
	}
}
