package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RSCAguardaOrçamento implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{	
			GregorianCalendar dataLimite = (GregorianCalendar) processEntity.findValue("dataPeriodoAguardaOrcamento");
			if(dataLimite != null) {
				GregorianCalendar data = new GregorianCalendar();
				data.set(Calendar.HOUR_OF_DAY, 23);
				data.set(Calendar.MINUTE, 59);
				data.set(Calendar.SECOND, 59);
				if (data.compareTo(dataLimite) < 0){  
					processEntity.setValue("aguardaOrcamento", Boolean.TRUE);
				} else {
					processEntity.setValue("aguardaOrcamento", Boolean.FALSE);
					processEntity.setValue("aguardaQuinzeDias", Boolean.FALSE);
				}
			} else {
				GregorianCalendar dataAtual = new GregorianCalendar();
				GregorianCalendar dataFinal = OrsegupsUtils.getSpecificWorkDay(dataAtual, 60L);
				dataFinal.set(Calendar.HOUR_OF_DAY, 23);
				dataFinal.set(Calendar.MINUTE, 59);
				dataFinal.set(Calendar.SECOND, 59);
				processEntity.setValue("dataPeriodoAguardaOrcamento", dataFinal);
				processEntity.setValue("aguardaOrcamento", Boolean.TRUE);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
	/*public static void main(String args[]) {
		GregorianCalendar taskDate = new GregorianCalendar();
		GregorianCalendar dataFim = new GregorianCalendar();
		taskDate.add(taskDate.DAY_OF_MONTH, 45);
		if (taskDate.compareTo(dataFim) < 0){  
           System.out.println("A data final deve ser maior " +  
                    "ou igual a data inicial");  
        }  
	}*/
}
