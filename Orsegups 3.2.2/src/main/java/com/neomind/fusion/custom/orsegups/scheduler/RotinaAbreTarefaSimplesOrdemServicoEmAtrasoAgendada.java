package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;


public class RotinaAbreTarefaSimplesOrdemServicoEmAtrasoAgendada implements CustomJobAdapter {
    
    private Set<Integer> defeitosRastreamentoModeloA = new TreeSet<Integer>();
    private Set<Integer> defeitosRastreamentoModeloB = new TreeSet<Integer>();
	
    public class TarefaOrdemServicoAtrasoAgendada{
	String numeroOS;
	Long nuOS;
	String colaborador;
	String tipoOS;
	Timestamp abertura;
	Timestamp inicioExecucao;
	Timestamp agenda;
	String central;
	String particao;
	String razao;
	String defeito;
	int idDefeito;
	int instalador;
	int empresa;
    }
    
    private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesOrdemServicoEmAtrasoAgendada.class);

    public void execute(CustomJobContext arg0) {

    	/*-------------------------------------------*/
    	
        // Modelo A de Defeito
    	this.defeitosRastreamentoModeloA.add(1157);
    	this.defeitosRastreamentoModeloA.add(1160);
    	
    	// Modelo B de Defeito
    	
        this.defeitosRastreamentoModeloB.add(98);
        this.defeitosRastreamentoModeloB.add(919);
        this.defeitosRastreamentoModeloB.add(920);
        this.defeitosRastreamentoModeloB.add(1148);
        this.defeitosRastreamentoModeloB.add(1152);
        this.defeitosRastreamentoModeloB.add(1153);
        this.defeitosRastreamentoModeloB.add(1154);
        this.defeitosRastreamentoModeloB.add(1155);
        this.defeitosRastreamentoModeloB.add(1156);
        this.defeitosRastreamentoModeloB.add(1158);
        this.defeitosRastreamentoModeloB.add(1159);
        this.defeitosRastreamentoModeloB.add(1161);
        
        
        
        /*-------------------------------------------*/
    	
	Map<String, Long> mapRegionais = OrsegupsUtils.getRegionais();
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	if (mapRegionais != null && !mapRegionais.isEmpty()) {
	    StringBuilder txtCol = new StringBuilder();
	    List<NeoObject> cols = new ArrayList<NeoObject>();
	    try {
		cols = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("OSSigmaColaboradores"));

		if (cols != null && !cols.isEmpty()) {
		    int cont = 0;

		    for (NeoObject col : cols) {
			EntityWrapper colWrapper = new EntityWrapper(col);
			String nomCol = (String) colWrapper.findValue("colaborador.nm_colaborador");

			txtCol.append("'");
			txtCol.append(nomCol);
			txtCol.append("'");
			if (cont < cols.size() - 1) {
			    txtCol.append(",");
			}
			cont++;
		    }
		}
	    } catch (Exception e) {
		log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso 5 Dias - ERRO AO GERAR TAREFA");
		System.out.println("[" + key + "] ##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso 5 Dias - ERRO AO GERAR TAREFA");
		e.printStackTrace();
		throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	    }
	    for (Map.Entry<String, Long> entrada : mapRegionais.entrySet()) {

		String reg = entrada.getKey();
		Long codReg = entrada.getValue();

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuffer sql1 = null;
		
		List<TarefaOrdemServicoAtrasoAgendada> tarefas = new ArrayList<TarefaOrdemServicoAtrasoAgendada>();
		InstantiableEntityInfo osInfo = AdapterUtils.getInstantiableEntityInfo("SIGMAOSAtrasadaTarefaSimples");

		log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso Agendada -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try {
		    conn = PersistEngine.getConnection("SIGMA90");
		    sql1 = new StringBuffer();
		    

		    /**
		     * Alterações referentes a tarefa 740657 *Ampliar prazo de
		     * 02 para 03 dias nas tarefas em execução *Não abrir OS's
		     * de Retirada de equipamento *Tarefas de Itapoá -
		     * Direcionar ao colaborador Dany Roger Perrony
		     * 
		     * @author mateus.batista
		     */

		    // Ordens de Serviço Adicionado consulta com base do sapiens
		    // para verificar se clientes são publicos ou privados
		    // Removido dos resultados os clientes tipo 2 (TipEmc =2 na
		    // base Sapiens)
		    sql1.append(" DECLARE @DatRef  Datetime ");
		    sql1.append(" SELECT @DatRef = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE() - 365), 0) ");
		    sql1.append(" SELECT 'OS agendada a mais de 02 dias úteis sem execução' AS TIPOOS, ");
    		    sql1.append(" col.NM_COLABORADOR , def.DESCRICAODEFEITO, os.ID_ORDEM, os.ABERTURA, os.DEFEITO, os.IDOSDEFEITO, os.ID_INSTALADOR, ");
    		    sql1.append(" c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO , c.CD_CLIENTE, c.FANTASIA, c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, ");
    		    sql1.append(" cid.NOME AS NOMECIDADE, os.FECHAMENTO, os.FECHADO, osh.DATAINICIOEXECUCAO, osh.DATAFIMEXECUCAO, os.EXECUTADO, sol.NM_DESCRICAO, CD_COLABORADOR, ");
    		    sql1.append(" mp.NM_DESCRICAO AS NOMEMOTIVOPAUSA, p.TX_OBSERVACAO, p.CD_MOTIVO_PAUSA, os.DATAAGENDADA, p.DT_PAUSA ");
    		    sql1.append(" FROM dbORDEM os WITH (NOLOCK) ");
    		    sql1.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
    		    sql1.append(" INNER JOIN OS_SOLICITANTE sol WITH (NOLOCK) ON sol.CD_OS_SOLICITANTE = os.CD_OS_SOLICITANTE ");
    		    sql1.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO ");
    		    sql1.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE ");
    		    sql1.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE ");
    		    sql1.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE ");
    		    sql1.append(" LEFT JOIN OSHISTORICO osh WITH (NOLOCK) ON osh.ID_ORDEM = os.ID_ORDEM AND osh.IDOSHISTORICO = (SELECT MIN(IDOSHISTORICO) FROM OSHISTORICO WHERE ID_ORDEM = os.ID_ORDEM) ");
    		    sql1.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
    		    sql1.append(" LEFT OUTER JOIN MOTIVO_PAUSA mp ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA ");
    		    sql1.append(" WHERE (col.NM_COLABORADOR LIKE '" + reg + "%' or col.CD_COLABORADOR = 9999)  AND col.FG_ATIVO_COLABORADOR = 1 ");
    		    sql1.append(" AND os.ABERTURA > @DatRef ");
    		    sql1.append(" AND col.NM_COLABORADOR NOT IN ( " + txtCol + ") ");
    		    sql1.append(" AND col.NM_COLABORADOR NOT LIKE '%KHRONOS%' ");
    		    sql1.append(" AND col.NM_COLABORADOR NOT LIKE '%QUATENUS%' ");
    		    sql1.append(" AND (def.DESCRICAODEFEITO NOT LIKE ('OS-RETIRAR EQUIPAMENTO') AND def.DESCRICAODEFEITO NOT LIKE ('OS-DESABILITAR MONITORAMENT')) ");
    		    // sql1.append(" --AND col.NM_COLABORADOR LIKE '%TERC%' ");
    		    sql1.append(" AND os.FECHADO = 0 AND os.DATAAGENDADA IS NOT NULL AND CAST(CONVERT(CHAR(16),os.DATAAGENDADA,113) AS datetime) + 2 < GETDATE() ");
    		    sql1.append(" AND osh.DATAINICIOEXECUCAO IS NULL ");
    		    sql1.append("  ");
    		    sql1.append(" ORDER BY 1, col.NM_COLABORADOR, os.FECHAMENTO DESC, sol.NM_DESCRICAO, os.DATAAGENDADA, os.ABERTURA ");
		    pstm = conn.prepareStatement(sql1.toString());
		    rs = pstm.executeQuery();
		    
		    while (rs.next()) {
			TarefaOrdemServicoAtrasoAgendada t = new TarefaOrdemServicoAtrasoAgendada();
			t.numeroOS = rs.getString("ID_ORDEM");
			t.nuOS = Long.parseLong(t.numeroOS);
			t.tipoOS = rs.getString("TIPOOS");
			t.colaborador = rs.getString("NM_COLABORADOR");
			t.abertura = rs.getTimestamp("ABERTURA");
			t.inicioExecucao = rs.getTimestamp("DATAINICIOEXECUCAO");
			t.agenda = rs.getTimestamp("DATAAGENDADA");
			t.central = rs.getString("ID_CENTRAL");
			t.particao =  rs.getString("PARTICAO");
			t.razao = rs.getString("RAZAO");
			t.defeito = rs.getString("DEFEITO");
			t.instalador = rs.getInt("ID_INSTALADOR");
			t.empresa = rs.getInt("ID_EMPRESA");
			t.idDefeito = rs.getInt("IDOSDEFEITO");
			
			tarefas.add(t);

		    }
		    

		    
		} catch (Exception e) {
		    log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso Agendada - ERRO AO GERAR TAREFA");
		    System.out.println("[" + key + "] ##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso Agendada - ERRO AO GERAR TAREFA");
		    e.printStackTrace();
		    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, rs);
		    
		}
		for (TarefaOrdemServicoAtrasoAgendada t : tarefas) {
		    try {

			String numeroOS = t.numeroOS;
			Long nuOS = Long.parseLong(numeroOS);

			@SuppressWarnings("unchecked")
			List<NeoObject> listOSTarefa = ((List<NeoObject>) PersistEngine.getObjects(osInfo.getEntityClass(), new QLEqualsFilter("numeroOS", nuOS), 0, 1, ""));

			if (listOSTarefa != null && listOSTarefa.isEmpty()) {
			    String solicitante = null;
			    String papelSolicitante = null;
			    String titulo = t.tipoOS + " - OS: " + numeroOS + " - TEC: " + t.colaborador;
			    String tituloAux = "%TEC: " + t.colaborador;

			    GregorianCalendar dataInicio = new GregorianCalendar();
			    GregorianCalendar dataFim = new GregorianCalendar();

			    GregorianCalendar prazo = new GregorianCalendar();

			    switch (t.tipoOS) {
			    case "OS aberta a mais de 05 dias úteis sem execução e agendamento":
				dataInicio.setTime(t.abertura);
				dataFim = OrsegupsUtils.getSpecificWorkDay(dataInicio, 5L);
				papelSolicitante = "SolicitanteTarefaOSEmAtraso5Dias";
				solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				break;
			    case "OS em execução a mais de 02 dias úteis":
				dataInicio.setTime(t.inicioExecucao);
				dataFim = OrsegupsUtils.getSpecificWorkDay(dataInicio, 2L);
				papelSolicitante = "SolicitanteTarefaOSEmExecucao2Dias";
				solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				break;
			    case "OS agendada a mais de 02 dias úteis sem execução":
				dataInicio.setTime(t.agenda);
				if (t.colaborador != null && !t.colaborador.isEmpty() && t.colaborador.contains("TERC")) {
				    dataFim = OrsegupsUtils.getSpecificWorkDay(dataInicio, 2L);
				} else if (t.colaborador != null && !t.colaborador.isEmpty() && t.colaborador.contains("TERC")) {
				    dataFim = OrsegupsUtils.getSpecificWorkDay(dataInicio, 1L);
				}
				papelSolicitante = "SolicitanteTarefaOSAgendada2DiasSemExecucao";
				solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
				break;
			    }

			    if (dataFim.after(new GregorianCalendar())) {
				continue;
			    }
			    // Condição temporaria solicitação Fernanda Maciel

			    prazo.set(Calendar.HOUR_OF_DAY, 23);
			    prazo.set(Calendar.MINUTE, 59);
			    prazo.set(Calendar.SECOND, 59);

			    String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
			    descricao += "<strong><u>Acompanhar, " + t.tipoOS + ":</u></strong><br><br>";
			    descricao = descricao + "<strong>Número OS:</strong> " + numeroOS + " <br>";
			    descricao = descricao + " <strong>Cliente :</strong> " + t.central + "[" + t.particao + "] " + t.razao + " <br>";
			    descricao = descricao + " <strong>Texto OS :</strong> " + t.defeito + "<br>";
			    descricao = descricao + " <strong>Instalador :</strong> " + t.colaborador + "<br>";

			    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

			    NeoPaper papel = new NeoPaper();

			    int idInstalador = t.instalador;

			    int idEmpresa = t.empresa;

			    if (idInstalador == 89642) {
				papel = OrsegupsUtils.getPaper("TratarOSSupervisorJLE");
			    } else if (idEmpresa == 10127 || idEmpresa == 10075 || t.central.startsWith("R")) {

				solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteOSAtrasoRastreamento");
				
					if(this.defeitosRastreamentoModeloA.contains(t.idDefeito)){
						papel = OrsegupsUtils.getPaper("TratarOS2DiasRastreamentoModeloA");					
					}else if(this.defeitosRastreamentoModeloB.contains(t.idDefeito)){
						papel = OrsegupsUtils.getPaper("TratarOS2DiasRastreamentoModeloB");
					}else {
						papel = OrsegupsUtils.getPaper("TratarOSAtrasoRastreamento");						
					}
				
			    } else if (codReg != null && !codReg.equals(13L)) {
				if (t.colaborador != null && !t.colaborador.isEmpty() && t.colaborador.contains("TEC") && t.colaborador.contains("ITH")) {
				    papel = OrsegupsUtils.getPaper("TratarOSAtrasoEscritorioRegionalITH");
				} else {
				    // TODO ADICIONAR RESPONSAVEIS DAS NOVAS
				    // REGIONAIS
				    papel = OrsegupsUtils.getPapelTratarOsEmAtraso(codReg);
				}
			    }

			    else if (codReg != null && codReg.equals(13L)) {
				if (t.colaborador != null && !t.colaborador.isEmpty() && t.colaborador.contains("CORP")) {
				    papel = OrsegupsUtils.getPaper("TratarOSAtrasoEscritorioRegionalCTACORP");
				} else {
				    papel = OrsegupsUtils.getPapelTratarOsEmAtraso(codReg);
				}
			    }

			    String executor = "";
			    // Caso não localizar o papel abre a tarefa para o
			    // Fabio

			    // TODO SE NAO LOCALIZAR EXECUTOR, ABRIR PARA EDSON HEINZ
			    if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
				for (NeoUser user : papel.getUsers()) {
				    executor = user.getCode();
				    break;
				}

				String retorno = "";
				try {
				    retorno = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

				} catch (Exception e) {
				    e.printStackTrace();
				}

				if (retorno != null && (!retorno.trim().contains("Erro"))) {
				    NeoObject objOS = osInfo.createNewInstance();
				    EntityWrapper wrpOS = new EntityWrapper(objOS);
				    wrpOS.findField("numeroOS").setValue(nuOS);
				    wrpOS.findField("numeroTarefa").setValue(Long.parseLong(retorno));
				    wrpOS.findField("dataCadastro").setValue(new GregorianCalendar());

				    PersistEngine.persist(objOS);

				} else {
				    log.error(" Erro ao criar tarefa Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				}
			    } else {
				log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso Agendada - Papel sem usuário definido - Regional: " + reg + " - Data: "
					+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			    }

			}

		    } catch (Exception e) {
			log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso Agendada - ERRO AO GERAR TAREFA");
			System.out.println("[" + key + "] ##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso Agendada - ERRO AO GERAR TAREFA");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		    }
		}
		
		
		log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso Agendada - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		
		
	    }
	} else {

	}
    }
}