package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaCorrecaoLog implements CustomJobAdapter {

	private static final Log log = LogFactory.getLog(RotinaCorrecaoLog.class);

	@Override
	public void execute(CustomJobContext arg0) {

		Connection conn = null;
		List<String> listaCmd = null;
		PreparedStatement pstm = null;
		
		log.warn("##### INICIAR  CORREÇÃO LOG : Rotina update correção log - Viatura - Os - Colaboradores - Posto ----- Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try {
					conn =  PersistEngine.getConnection("SIGMA90");
					conn.setAutoCommit(false);
					listaCmd = new ArrayList<String>();
					
					
					//SIGMA Viatura
					listaCmd.add(" UPDATE D_SigmaLogViatura SET textoLog = REPLACE(textoLog, '''' ,'´') ");
					listaCmd.add(" UPDATE D_SigmaLogViatura SET textoLog = REPLACE(textoLog, '\"' ,'´') ");
					listaCmd.add(" UPDATE D_SigmaLogViatura SET textoLog = REPLACE(textoLog, 'Ø' ,' ') ");
					listaCmd.add(" UPDATE D_SigmaLogViatura SET textoLog = REPLACE(textoLog, CHAR(13), ' ') ");
					listaCmd.add(" UPDATE D_SigmaLogViatura SET textoLog = REPLACE(textoLog, CHAR(10), ' ') ");   
					listaCmd.add(" UPDATE D_SigmaLogViatura SET textoLog = REPLACE(textoLog, CHAR(13)+CHAR(10), ' ') "); 
					
					//SIGMA Os
					listaCmd.add(" UPDATE D_SigmaLogOs SET textoLog = REPLACE(textoLog, '''' ,'´') ");
					listaCmd.add(" UPDATE D_SigmaLogOs SET textoLog = REPLACE(textoLog, '\"' ,'´') ");
					listaCmd.add(" UPDATE D_SigmaLogOs SET textoLog = REPLACE(textoLog, 'Ø' ,' ') ");
					listaCmd.add(" UPDATE D_SigmaLogOs SET textoLog = REPLACE(textoLog, CHAR(13), ' ') ");
					listaCmd.add(" UPDATE D_SigmaLogOs SET textoLog = REPLACE(textoLog, CHAR(10), ' ') ");  
					listaCmd.add(" UPDATE D_SigmaLogOs SET textoLog = REPLACE(textoLog, CHAR(13)+CHAR(10), ' ') "); 

					//SIGMA Colaboradores
					listaCmd.add(" UPDATE D_SigmaLogColaborador SET textoLog = REPLACE(textoLog, '''' ,'´') ");
					listaCmd.add(" UPDATE D_SigmaLogColaborador SET textoLog = REPLACE(textoLog, '\"' ,'´') ");
					listaCmd.add(" UPDATE D_SigmaLogColaborador SET textoLog = REPLACE(textoLog, 'Ø' ,' ') ");
					listaCmd.add(" UPDATE D_SigmaLogColaborador SET textoLog = REPLACE(textoLog, CHAR(13), ' ') ");
					listaCmd.add(" UPDATE D_SigmaLogColaborador SET textoLog = REPLACE(textoLog, CHAR(10), ' ') ");
					listaCmd.add(" UPDATE D_SigmaLogColaborador SET textoLog = REPLACE(textoLog, CHAR(13)+CHAR(10), ' ') "); 

					//PRESENÇA Colaboradores
					listaCmd.add(" UPDATE D_QLLogPresencaColaborador SET textoLog = REPLACE(textoLog, '''' ,'´') ");
					listaCmd.add(" UPDATE D_QLLogPresencaColaborador SET textoLog = REPLACE(textoLog, '\"' ,'´') ");
					listaCmd.add(" UPDATE D_QLLogPresencaColaborador SET textoLog = REPLACE(textoLog, 'Ø' ,' ') ");
					listaCmd.add(" UPDATE D_QLLogPresencaColaborador SET textoLog = REPLACE(textoLog, CHAR(13), ' ') ");
					listaCmd.add(" UPDATE D_QLLogPresencaColaborador SET textoLog = REPLACE(textoLog, CHAR(10), ' ') ");
					listaCmd.add(" UPDATE D_QLLogPresencaColaborador SET textoLog = REPLACE(textoLog, CHAR(13)+CHAR(10), ' ') "); 

					//PRESENÇA Posto
					listaCmd.add(" UPDATE D_QLLogPresencaPosto SET textoLog = REPLACE(textoLog, '''' ,'´') ");
					listaCmd.add(" UPDATE D_QLLogPresencaPosto SET textoLog = REPLACE(textoLog, '\"' ,'´') ");
					listaCmd.add(" UPDATE D_QLLogPresencaPosto SET textoLog = REPLACE(textoLog, 'Ø' ,' ') ");
					listaCmd.add(" UPDATE D_QLLogPresencaPosto SET textoLog = REPLACE(textoLog, CHAR(13), ' ') ");
					listaCmd.add(" UPDATE D_QLLogPresencaPosto SET textoLog = REPLACE(textoLog, CHAR(10), ' ') ");
					listaCmd.add(" UPDATE D_QLLogPresencaPosto SET textoLog = REPLACE(textoLog, CHAR(13)+CHAR(10), ' ') "); 
		
					for (String sql : listaCmd)
					{
						pstm = conn.prepareStatement(sql.toString());
						pstm.executeUpdate();
					}
					conn.commit();
					conn.setAutoCommit(true);
			
			log.warn("##### EXECUTAR  CORREÇÃO LOG : Rotina update correção log - Viatura - Os - Colaboradores - Posto -----  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			
		} catch (Exception e) {
			log.error("##### ERRO  CORREÇÃO LOG : Erro ao executar update correção log!.");
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				OrsegupsUtils.closeConnection(conn, pstm, null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			log.warn("##### FINALIZAR  CORREÇÃO LOG : Rotina update correção log - Viatura - Os - Colaboradores - Posto ----- Data: "	+ NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
	}
}
