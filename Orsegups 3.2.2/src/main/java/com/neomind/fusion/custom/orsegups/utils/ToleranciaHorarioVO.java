package com.neomind.fusion.custom.orsegups.utils;


/**
 * @author lucas.alison
 *
 */
public class ToleranciaHorarioVO{
    private Long horBat;
    private Long tolAnt;
    private Long tolApo;
    private Long faiMov;
    private Long seqMar;

    public Long getHorBat() {
        return horBat;
    }
    public void setHorBat(Long horBat) {
        this.horBat = horBat;
    }
    public Long getTolAnt() {
        return tolAnt;
    }
    public void setTolAnt(Long tolAnt) {
        this.tolAnt = tolAnt;
    }
    public Long getTolApo() {
        return tolApo;
    }
    public void setTolApo(Long tolApo) {
        this.tolApo = tolApo;
    }
    public Long getFaiMov() {
        return faiMov;
    }
    public void setFaiMov(Long faiMov) {
        this.faiMov = faiMov;
    }
    public Long getSeqMar() {
	return seqMar;
    }
    public void setSeqMar(Long seqMar) {
	this.seqMar = seqMar;
    }
  
 }
