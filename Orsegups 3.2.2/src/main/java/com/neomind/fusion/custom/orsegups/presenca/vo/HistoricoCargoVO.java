package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class HistoricoCargoVO
{
	private ColaboradorVO colaborador;
	private String cargo;
	private GregorianCalendar data;
	
	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}
	public String getCargo()
	{
		return cargo;
	}
	public void setCargo(String cargo)
	{
		this.cargo = cargo;
	}
	public GregorianCalendar getData()
	{
		return data;
	}
	public void setData(GregorianCalendar data)
	{
		this.data = data;
	}
}
