package com.neomind.fusion.custom.orsegups.servlets;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="RelatoriosProdutividade", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosProdutividade"})
public class RelatoriosProdutividade extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		try
		{
			PrintWriter out = resp.getWriter();
			FileInputStream fis = new FileInputStream("\\\\ssooap02\\SapiensNFe\\18-1-Grade\\XML\\8491597000126\\XML_NFSe\\2013\\05\\16\\8491597000126_1_203560-procNFSe.xml");
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			while (true)
			{
				String s = br.readLine();
				if (s == null)
					break;
				out.println(s);
				out.flush();
			}
			fis.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}
