package com.neomind.fusion.custom.orsegups.rsc.cancelamentoNovo;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
/**
 * Adapter de Escalonamento do Fluxo C027.006 - RSC Cancelamento (Novo)
 * 
 * Hierarquia de Escalonamento igual a de tarefa Simples. 
 * 1° Escalonamento: Responsável do Grupo do usuário que deixou escalar;
 * A partir do 2° Escalonamento: Responsável do Grupo de nível superior do usuário que deixou escalar;
 * 
 * @author herisson.ferreira
 *
 */
public class RSCCancelamentoEscalaNivel implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {

		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		try {

			NeoUser executorAtual = origin.getUser();
			NeoPaper responsavelGrupoExecutorAtual = executorAtual.getGroup().getResponsible();

			NeoUser novoExecutor = null;
			NeoPaper papelNovoExecutor = null;

			NeoUser ultimoExecutor = executorAtual;
			

			Boolean responsavelAtividade = processEntity.findGenericValue("souPessoaResponsavel");
			if (responsavelAtividade != null 
					&& !responsavelAtividade 
						&& origin.getActivityName().equalsIgnoreCase("Tentativa de Reversão do Cancelamento")  
							&& origin.getFinishByUser() != null) {
				NeoUser nuRespCorreto = processEntity.findGenericValue("usuarioResponsavel");
				if (nuRespCorreto != null)
					novoExecutor = nuRespCorreto;

				processEntity.setValue("usuarioResponsavel", null);
				processEntity.setValue("souPessoaResponsavel", true);
				processEntity.setValue("responsavelExecutor", novoExecutor);

			} else {

				if (executorAtual.getPapers().contains(responsavelGrupoExecutorAtual)){ // Caso o usuário seja o responsável do seu grupo, pega o responsável do nível superior		    
					papelNovoExecutor = executorAtual.getGroup().getUpperLevel().getResponsible();
					novoExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelNovoExecutor);	
				} else {
					papelNovoExecutor = executorAtual.getGroup().getResponsible();
					novoExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelNovoExecutor);
				}

				if (novoExecutor == null){
					novoExecutor = executorAtual;
				}

				// Atribui o executor a partir do Adapter ao qual o mesmo passou, possibilitando assim, reaproveitar a classe.
				if (activity.getActivityName().equalsIgnoreCase("Definir Gestor Responsável")) 
				{
					processEntity.setValue("gestorResponsavel", novoExecutor);
				}
				else if (activity.getActivityName().equalsIgnoreCase("Escalar Justificativa")) 
				{
					processEntity.setValue("responsavelExecutor", novoExecutor);	
				}


				processEntity.setValue("ultimoExecutor", ultimoExecutor);
			}

		}catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro na classe: "+this.getClass()+". Verifique junto ao setor de TI!");
		} 
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {

	}

}
