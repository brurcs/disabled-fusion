package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class GTCValidaUsuario implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		String usuario = activity.getProcess().getRequester().getCode();

		EntityWrapper wrapperLista = new EntityWrapper(processEntity.getObject());

		NeoPaper responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[GTC] Solicitante"));

		if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
		{
			for (NeoUser user : responsavel.getUsers())
			{

				if (usuario.equals(user.getCode()))
				{

					wrapperLista.setValue("possuiPapelGTC", true);
					break;
				}

			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
