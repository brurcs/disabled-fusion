package com.neomind.fusion.custom.orsegups.adapter.vo;

public class RepresentanteVO
{
	private Long codigoRep;
	private String nomeRep;
	private String tipoRep;
	private String iconeRep;
	private String nomReg;
	private String email;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNomReg()
	{
		return nomReg;
	}

	public void setNomReg(String nomReg)
	{
		this.nomReg = nomReg;
	}

	public Long getCodigoRep()
	{
		return codigoRep;
	}

	public void setCodigoRep(Long codigoRep)
	{
		this.codigoRep = codigoRep;
	}

	public String getNomeRep()
	{
		return nomeRep;
	}

	public void setNomeRep(String nomeRep)
	{
		this.nomeRep = nomeRep;
	}

	public String getTipoRep()
	{
		return tipoRep;
	}

	public void setTipoRep(String tipoRep)
	{
		this.tipoRep = tipoRep;
	}

	public String getIconeRep()
	{
		return iconeRep;
	}

	public void setIconeRep(String iconeRep)
	{
		this.iconeRep = iconeRep;
	}

}
