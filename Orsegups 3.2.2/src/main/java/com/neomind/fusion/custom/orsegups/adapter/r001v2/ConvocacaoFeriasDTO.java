package com.neomind.fusion.custom.orsegups.adapter.r001v2;


public class ConvocacaoFeriasDTO {

	Integer neoId;
	String name; // Pagamento Efetuado
	String text; // TEXTO padrao
	Integer empresa;
	Integer matricula;
	byte[] file;

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public Integer getNeoId() {
		return neoId;
	}

	public void setNeoId(Integer neoId) {
		this.neoId = neoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
}
