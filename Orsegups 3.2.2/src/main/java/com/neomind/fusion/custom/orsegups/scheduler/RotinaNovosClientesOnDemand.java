package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaNovosClientesOnDemand implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaNovosClientesOnDemand.class);
	private Pattern pattern;
	private Matcher matcher;
	private final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");

	@Override
	public void execute(CustomJobContext arg0)
	{
		log.warn("##### INICIAR ROTINA RotinaNovosClientesOn Demand - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{

			conn = PersistEngine.getConnection("SAPIENS");

			StringBuilder sql = new StringBuilder();
			sql.append("   SELECT cli.CodCli,ctr.usu_datini,sct.usu_desser,cvs.usu_numctr,cvs.usu_numpos,cen.cd_cliente,cen.EMAILRESP ");
			sql.append("   FROM e085cli cli  WITH (NOLOCK)  ");
			sql.append("   INNER JOIN  usu_t160ctr ctr  WITH (NOLOCK) ON (cli.codcli  = ctr.usu_codcli)   ");
			sql.append("   INNER JOIN usu_t160cvs cvs  WITH (NOLOCK) ON (cvs.usu_numctr = ctr.usu_numctr and  ");
			sql.append("   cvs.usu_codfil = ctr.usu_codfil and cvs.usu_codemp = ctr.usu_codemp)  ");
			sql.append("   INNER JOIN  usu_t160sct sct WITH (NOLOCK) ON (sct.usu_serctr  = ctr.usu_serctr)   ");
			sql.append("   INNER JOIN USU_T160SIG sig WITH (NOLOCK)  ON sig.usu_numctr = ctr.usu_numctr AND sig.usu_codfil = ctr.usu_codfil    ");
			sql.append("   INNER JOIN  ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.dbCentral cen WITH (NOLOCK)  ON cen.cd_cliente = sig.usu_codcli    ");
			sql.append("   WHERE ((cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >=  GETDATE())) and cli.tipemc = 1 AND sct.usu_serctr in(5,6)  ");
			sql.append("   and ctr.usu_datini = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0) ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String codCli = rs.getString("CodCli");
				String numCtr = rs.getString("usu_numctr");
				String numPos = rs.getString("usu_numpos");
				String datIni = rs.getString("usu_datini");
				String cdCliente = rs.getString("cd_cliente");
				String emailCliente = rs.getString("EMAILRESP");
				enviaEmailCliente(montaHtml(cdCliente), emailCliente, codCli, numCtr, numPos, datIni, cdCliente);
				//enviaEmailRotinaTeste(montaHtml(rs.getString("cd_cliente")));

			}

			log.warn("##### EXECUTAR RotinaNovosClientesOn Demand - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{

			log.error("##### ERRO RotinaNovosClientesOn Demand - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("[" + key + "] ##### ERRO RotinaNovosClientesOn Demand - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

			log.warn("##### FINALIZAR RotinaNovosClientesOn Demand - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	private void enviaEmailRotinaTeste(String returnFromAccess)
	{

		try
		{

			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			mailClone.setFromEMail("fusion@orsegups.com.br");
			mailClone.setFromName("Orsegups Partipações S.A.");
			if (mailClone != null)
			{

				HtmlEmail noUserEmail = new HtmlEmail();

				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("william.bastos@orsegups.com.br");
				noUserEmail.addTo("mateus.batista@orsegups.com.br");

				noUserEmail.setSubject("E-mail - On Demand");

				noUserEmail.setHtmlMsg(returnFromAccess);
				noUserEmail.setContent(returnFromAccess, "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO RotinaNovosClientesOn Demand - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}

	}

	public List<String> validarEmail(String email, String cdCliente)
	{
		List<String> listaDeEmail = new ArrayList<String>();

		String cmd = "";

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rsSapiens = null;
		ResultSet rs = null;
		try
		{
			matcher = pattern.matcher(email);
			conn = PersistEngine.getConnection("SIGMA90");
			if (matcher.matches())
			{
				//email = "sistemas@orsegups.com.br";
				listaDeEmail.add(email);
			}
			else
			{
				cmd = " SELECT email FROM DBPROVIDENCIA WITH(NOLOCK) WHERE cd_cliente = " + Long.valueOf(cdCliente) + " AND  email IS NOT NULL AND email <> ' ' ";

				pstm = conn.prepareStatement(cmd);

				rs = pstm.executeQuery();

				while (rs.next())
				{
					String emailRs = rs.getString(1);
					matcher = pattern.matcher(emailRs);

					if (matcher.matches())
					{
						listaDeEmail.add(emailRs);
					}

				}
			}
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			conn = PersistEngine.getConnection("SAPIENS");

			cmd = "";
			cmd = " SELECT DISTINCT cli.IntNet ";
			cmd += " FROM USU_T160SIG sig WITH(NOLOCK) ";
			cmd += " INNER JOIN USU_T160CTR ctr WITH(NOLOCK) ON  ctr.usu_codemp = sig.usu_codemp and ctr.usu_codfil = sig.usu_codfil and ctr.usu_numctr = sig.usu_numctr ";
			cmd += " inner join dbo.E085CLI cli WITH(NOLOCK) ON CLI.CodCli = CTR.usu_codcli ";
			cmd += " WHERE sig.usu_codcli = " + cdCliente;
			cmd += " and cli.IntNet <> ' ' ";

			pstm = conn.prepareStatement(cmd);
			rsSapiens = pstm.executeQuery();

			while (rsSapiens.next())
			{
				String[] campos = null;
				boolean achou = false;
				String emailRs = rsSapiens.getString(1).replaceAll(",", ";").replaceAll(" ", "");
				campos = emailRs.split(";");

				for (String em : campos)
				{
					matcher = pattern.matcher(em);
					if (matcher.matches())
					{
						for (String listEma : listaDeEmail)
						{
							if (listEma.equalsIgnoreCase(em))
							{
								achou = true;
							}
						}
						if (!achou)
						{
							listaDeEmail.add(em);
						}
					}
				}

			}
		}
		catch (Exception e)
		{

			log.error("E-Mail Desvio de hábito outros - erro validação do e-mail: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(null, null, rs);
			OrsegupsUtils.closeConnection(conn, pstm, rsSapiens);
		}
		return listaDeEmail;

	}

	private void enviaEmailCliente(String msn, String emailCli, String codCli, String numCtr, String numPos, String datIni, String cdCliente)
	{

		try
		{
			if (emailCli != null && !emailCli.isEmpty() && codCli != null && !codCli.isEmpty() && msn != null && !msn.toString().isEmpty() && numCtr != null && !numCtr.isEmpty() && numPos != null && !numPos.isEmpty() && datIni != null && !datIni.isEmpty())
			{

				pattern = Pattern.compile(EMAIL_PATTERN);
				InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getEntityInfo("EmailAutomaticoOnDemand");
				InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getEntityInfo("EmailDelivery");
				List<String> emailCliente = null;

				emailCliente = validarEmail(emailCli, cdCliente);

				if ((emailCliente != null) && (!emailCliente.isEmpty()))
				{
					log.warn("[On Demand] email do cliente validado");

					for (String emailFor : emailCliente)
					{
						log.warn("[On Demand] addTo: " + emailFor);
						QLGroupFilter groupFilter = new QLGroupFilter("AND");
						groupFilter.addFilter(new QLEqualsFilter("codcli", codCli));
						groupFilter.addFilter(new QLEqualsFilter("numctr", numCtr));
						groupFilter.addFilter(new QLEqualsFilter("numpos", numPos));
						groupFilter.addFilter(new QLEqualsFilter("datini", datIni));
						groupFilter.addFilter(new QLEqualsFilter("cdCliente", cdCliente));

						@SuppressWarnings("unchecked")
						List<NeoObject> neoObjects = PersistEngine.getObjects(infoHis.getEntityClass(), groupFilter);

						if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty()))
						{

							NeoObject emailHis = infoHis.createNewInstance();
							EntityWrapper emailHisWp = new EntityWrapper(emailHis);
							emailHisWp.findField("codcli").setValue(codCli);
							emailHisWp.findField("numctr").setValue(numCtr);
							emailHisWp.findField("numpos").setValue(numPos);
							emailHisWp.findField("datini").setValue(datIni);
							emailHisWp.findField("cdCliente").setValue(cdCliente);

							PersistEngine.persist(emailHis);

							GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
							NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
							EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
							emailEnvioWp.findField("de").setValue("cm.rat@orsegups.com.br");
							emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br");
							emailEnvioWp.findField("assunto").setValue("E-mail Código de Cliente - On Demand");
							emailEnvioWp.findField("Mensagem").setValue(msn);
							emailEnvioWp.findField("datCad").setValue(dataCad);
							PersistEngine.persist(emaiEnvio);
						}
					}
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO ao enviar e-mail novo cliente On Demand - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}

	}

	private String montaHtml(String cdCliente)
	{
		StringBuilder noUserMsg = new StringBuilder();
		try
		{
			noUserMsg.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
			noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> ");
			noUserMsg.append(" <style TYPE=\"text/css\"> ");
			noUserMsg.append("   p,pre,body,table ");
			noUserMsg.append("   { ");
			noUserMsg.append("   font-size:10.0pt; ");
			noUserMsg.append("   font-family:'Arial';");
			noUserMsg.append("   } ");
			noUserMsg.append("  </style> ");
			noUserMsg.append(" </head><body>");
			noUserMsg.append("<p><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/topo-email-ferias.jpg\" width=\"100%\" height=\"211\" alt=\"\"/></td></p>");
			noUserMsg.append("<p>Prezado Cliente,</p>");
			noUserMsg.append("<p>Confirmamos vossa adesão ao serviço On Demand da empresa Orsegups.</p>");
			noUserMsg.append("<p>Seu código de cliente é " + cdCliente + ".</p>");
			noUserMsg.append("<p>Este código deverá ser fornecido á Orsegups caso a ligação seja realizada através de um telefone não cadastrado em seu perfil de cliente.</p>");
			noUserMsg.append("<p>Agradecemos vossa escolha e colocamo-nos a disposição.</p>");
			noUserMsg.append("<p>Atenciosamente,</p>");
			noUserMsg.append("<p>NAC</p>");
			noUserMsg.append("<p>Núcleo de Atendimento ao Cliente</p>");
			noUserMsg.append("<p> <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/rodape-email-ferias.jpg\" width=\"800\" height=\"211\" alt=\"\"/></p> ");
			noUserMsg.append("</body></html>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return noUserMsg.toString();
	}

}
