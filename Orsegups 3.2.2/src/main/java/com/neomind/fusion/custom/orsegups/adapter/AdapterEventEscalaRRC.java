package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/**
 * <b> Regra de Negócio: </b>
 * 		• 2 RRCs ou mais nos últimos 90 dias para o mesmo cliente -> Escalar para o gerente
 * 		• 3 RRCs ou mais nos últimos 120 dias para o mesmo cliente -> Escalar para o superintendente
 * 
 * 
 * @author herisson.ferreira
 *
 */
public class AdapterEventEscalaRRC implements TaskFinishEventListener {

	public static String mensagemErro = "Erro ao avançar a tarefa de RRC!";
	
	public static final String campoCoordenador = "dataResponderRrc";
	public static final String campoGerente = "dataResponderRrcGerenteEscalada";
	
	
	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
		
		try {
			
			EntityWrapper processEntity = event.getWrapper(); 
			Long codigoCliente = NeoUtils.safeLong(processEntity.findGenericValue("clienteSapiens.codcli").toString());
			
			if (NeoUtils.safeIsNotNull(codigoCliente)) { 
				
				boolean escalaCoordenador = possuiQuantidadeRRC(codigoCliente, 90L, 2L);
				boolean escalaGerente = possuiQuantidadeRRC(codigoCliente, 120L, 3L);
				
				if(escalaGerente) {
					escalaAtividade(campoCoordenador, processEntity);
					escalaAtividade(campoGerente, processEntity);
				} else if(escalaCoordenador) {
					escalaAtividade(campoCoordenador, processEntity);
				}					
				
			}
			
		} catch (WorkflowException e) {
			e.printStackTrace();
			mensagemErro = "Erro: "+e.getErrorList().get(0).getI18nMessage();
			System.out.println(mensagemErro);
			throw new WorkflowException(mensagemErro);
		}
		
	}
	
	
	/**
	 * Verifica a quantidade de RRCs abertas para o cliente nos últimos X dias. 
	 * @return <b> True </b> = Possui quantidade igual superior a exigida.
	 * 		   <b> False </b> = Possui quantidade inferior a exigida.
	 */
	public Boolean possuiQuantidadeRRC(Long codigoCliente, Long quantidadeDias, Long quantidadeRRC) {
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		Connection conn = null;
		ResultSet rs = null; 
		
		Boolean resultado = false;
		
		try {
			
			sql.append(" SELECT COUNT(*) AS 'QTD_RRC'FROM D_RRCRelatorioReclamacaoCliente R ");
			sql.append(" INNER JOIN X_SAPIENS_Clientes X ");
			sql.append(" ON X.neoId = R.clienteSapiens_neoId ");
			sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.E085CLI C ");
			sql.append(" ON X.codcli = C.codcli ");
			sql.append(" INNER JOIN WFProcess W ");
			sql.append(" ON R.wfprocess_neoId = W.neoid ");
			sql.append(" WHERE C.codcli = ? AND r.dataAbertura >= GETDATE()-? ");
			sql.append(" AND w.saved = 1 ");
			
			conn = PersistEngine.getConnection("");
			
			pstm = conn.prepareStatement(sql.toString());

			pstm.setLong(1, codigoCliente);
			pstm.setLong(2, quantidadeDias);

			rs = pstm.executeQuery();
			
			if(rs.next()) {	
				if(rs.getLong("QTD_RRC") >= quantidadeRRC) {
					resultado = true;
				}else {
					resultado = false;
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao consultar a quantidade de RRCs do cliente.";
		}
		
		
		return resultado;
	}
	
	/**
	 * Atualiza o prazo da atividade para forçar o escalonamento da Tarefa.
	 * 
	 * @param campoPrazo Campo correspondente ao prazo da atividade
	 * @param processEntity EntityWrapper do formulário de RRC 
	 */
	public static void escalaAtividade(String campoPrazo, EntityWrapper processEntity) {
			
		try {
			
			InstantiableEntityInfo ieRRC = AdapterUtils.getInstantiableEntityInfo("RRCRelatorioReclamacaoCliente");
			NeoObject oRRC = ieRRC.createNewInstance();
			
			NeoObject departamentoResponsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("IMTipoInspecao"), new QLEqualsFilter("codigo", 119L)); // Código correspondente ao "Setor" Nac - Escalonamento
			
			GregorianCalendar dataAtual = new GregorianCalendar();
			dataAtual.add(GregorianCalendar.DAY_OF_MONTH, -1);
			
			processEntity.findField("departamentoResponsavel").setValue(departamentoResponsavel);
			processEntity.findField(campoPrazo).setValue(dataAtual);
			
			PersistEngine.persist(departamentoResponsavel);
			PersistEngine.persist(oRRC);
			
		} catch (WorkflowException e) {
			e.printStackTrace();
			mensagemErro = e.getErrorList().get(0).getI18nMessage();
			throw new WorkflowException(mensagemErro);
		}

	}
	
}
