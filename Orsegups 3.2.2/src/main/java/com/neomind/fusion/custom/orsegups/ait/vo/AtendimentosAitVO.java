package com.neomind.fusion.custom.orsegups.ait.vo;

public class AtendimentosAitVO {
	
	private String cdHistorico;
	private String regional;
	private String ait;
	private String cliente;
	private String cdEvento;
	private String dtFechamento;
	private String op;
	private String deslocamento;
	private String atendimento;
	private String cdViatura;
	private String empresa;
	private String filial;
	private String matricula;
	private String cpf;
	private String relato;
	private String argumento;
	private Long situacao;
	private String txFechamento;
	private String txGerente;
	private Long wfProcess;
	
	public String getTxFechamento()
	{
		return txFechamento;
	}


	public void setTxFechamento(String txFechamento)
	{
		this.txFechamento = txFechamento;
	}


	public String getTxGerente()
	{
		return txGerente;
	}


	public void setTxGerente(String txGerente)
	{
		this.txGerente = txGerente;
	}


	public String getRelato()
	{
		return relato;
	}


	public void setRelato(String relato)
	{
		this.relato = relato;
	}


	public String getArgumento()
	{
		return argumento;
	}


	public void setArgumento(String argumento)
	{
		this.argumento = argumento;
	}


	public Long getSituacao()
	{
		return situacao;
	}


	public void setSituacao(Long situacao)
	{
		this.situacao = situacao;
	}


	public String getCdViatura() {
		return cdViatura;
	}


	public void setCdViatura(String cdViatura) {
		this.cdViatura = cdViatura;
	}
	
	public String getCdHistorico() {
		return cdHistorico;
	}


	public void setCdHistorico(String cdHistorico) {
		this.cdHistorico = cdHistorico;
	}
	
	public String getRegional() {
		return regional;
	}


	public void setRegional(String regional) {
		this.regional = regional;
	}
	
	public String getAit() {
		return ait;
	}


	public void setAit(String ait) {
		this.ait = ait;
	}
	
	public String getCliente() {
		return cliente;
	}


	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	
	public String getCdEvento() {
		return cdEvento;
	}


	public void setCdEvento(String cdEvento) {
		this.cdEvento = cdEvento;
	}
	
	public String getDtFechamento() {
		return dtFechamento;
	}


	public void setDtFechamento(String dtFechamento) {
		this.dtFechamento = dtFechamento;
	}
	
	public String getOp() {
		return op;
	}


	public void setOp(String op) {
		this.op = op;
	}
	
	public String getDeslocamento() {
		return deslocamento;
	}


	public void setDeslocamento(String deslocamento) {
		this.deslocamento = deslocamento;
	}
	
	public String getAtendimento() {
		return atendimento;
	}


	public void setAtendimento(String atendimento) {
		this.atendimento = atendimento;
	}
	
	public String getEmpresa() {
		return empresa;
	}


	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
	public String getFilial() {
		return filial;
	}


	public void setFilial(String filial) {
		this.filial = filial;
	}
	
	public String getMatricula() {
		return matricula;
	}


	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public Long getWfProcess() {
	    return wfProcess;
	}


	public void setWfProcess(Long wfProcess) {
	    this.wfProcess = wfProcess;
	}
	
	
} 
