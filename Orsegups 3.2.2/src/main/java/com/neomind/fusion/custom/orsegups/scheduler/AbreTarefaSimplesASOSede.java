package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesASOSede implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesASO");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		GregorianCalendar dataCorte = new GregorianCalendar();
		
		dataCorte.set(Calendar.HOUR_OF_DAY, 0);
		dataCorte.set(Calendar.MINUTE, 0);
		dataCorte.set(Calendar.SECOND, 0);
		String dataCorteString = NeoUtils.safeDateFormat(dataCorte, "dd/MM/yyyy");
		
		

		try
		{
			sql.append("SELECT FUN.NUMEMP, FUN.TIPCOL, FUN.NUMCAD, FUN.NOMFUN, FUN.DATADM, FUN.NUMCPF, REG.USU_CODREG,");
			sql.append(" CPL.ULTEXM, CASE ");
			sql.append("WHEN DATEDIFF(YEAR , FUN.DATNAS , GETDATE()) > 45 ");
			sql.append("THEN DATEADD(MONTH,11,CPL.ULTEXM) ");
			sql.append(" ELSE DATEADD(MONTH,23,CPL.ULTEXM) ");
			sql.append("END AS PROEXM, CAR.TITRED, ORN.USU_LOTORN, ORN.NOMLOC, DATEADD(YEAR,2,CPL.ULTEXM) as proxData ");
			sql.append("FROM R034FUN FUN ");
			sql.append("INNER JOIN R034CPL CPL ON CPL.NUMEMP = FUN.NUMEMP AND CPL.TIPCOL = FUN.TIPCOL AND CPL.NUMCAD = FUN.NUMCAD ");
			sql.append("INNER JOIN R024CAR CAR ON CAR.ESTCAR = FUN.ESTCAR AND CAR.CODCAR = FUN.CODCAR ");
			sql.append("INNER JOIN R016ORN ORN ON ORN.NUMLOC = FUN.NUMLOC AND ORN.TABORG = FUN.TABORG ");
			sql.append("INNER JOIN USU_T200REG REG ON REG.USU_CODREG = ORN.USU_CODREG ");
			sql.append("WHERE FUN.TIPCOL = 1 AND FUN.SITAFA IN (1,2) AND REG.USU_CODREG IN (0,1,9) AND CPL.ULTEXM <> '1900-12-31' AND case ");
			sql.append(" WHEN DATEDIFF(YEAR , FUN.DATNAS , GETDATE()) > 45");
			sql.append(" THEN DATEADD(MONTH,11,CPL.ULTEXM) ");
			sql.append("ELSE DATEADD(MONTH,23,CPL.ULTEXM) ");
			sql.append(" End > = GETDATE()");
			sql.append(" And Not Exists (Select * From [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaASO tas WITH (NOLOCK) WHERE tas.numcpf = fun.numcpf ");
			sql.append("And CAST(floor(CAST(tas.proexm As FLOAT)) As datetime) > CAST(floor(CAST(cpl.ultexm AS FLOAT)) As datetime))");
			
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				String ultimoExame = NeoUtils.safeDateFormat(rs.getDate("ultexm"), "dd/MM/yyyy");
				String proximoExame = NeoUtils.safeDateFormat(rs.getDate("proexm"), "dd/MM/yyyy");
				String proxData = NeoUtils.safeDateFormat(rs.getDate("proxData"), "dd/MM/yyyy");
				String lotacao = rs.getString("USU_LOTORN"); 
				

				if (dataCorteString.equals(proximoExame))
				{

					String solicitante = "";	
					Long codReg = rs.getLong("USU_CodReg");
					if(codReg == 0){
						 solicitante = "edimara.bona";	
					}else{
						solicitante = "thiami.tomasi";	
					}
				
					String titulo = "Agendar o próximo ASO - Atestado de Saúde Ocupacional do colaborador " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun");

					String descricao = "";
					descricao = " <strong>Colaborador:</strong> " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun") + "<br>";
					descricao = descricao + " <strong>Cargo:</strong> " + rs.getString("TitRed") + "<br>";
					descricao = descricao + " <strong>Posto:</strong> " + rs.getString("USU_LotOrn") + " - " + rs.getString("NomLoc") + "<br>";
					descricao = descricao + " <strong>Último Exame:</strong> " + ultimoExame + "<br>";
					descricao = descricao + " <strong>Próximo Exame:</strong> " + proxData + "<br>";

					GregorianCalendar prazo = new GregorianCalendar();
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 20L);
					prazo.set(Calendar.HOUR_OF_DAY, 23);
					prazo.set(Calendar.MINUTE, 59);
					prazo.set(Calendar.SECOND, 59);

					

					NeoPaper papel = new NeoPaper();
					String executor = "";

					papel = OrsegupsUtils.getPapelResponsavelASOSede(codReg, lotacao);

					if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
					{
						for (NeoUser user : papel.getUsers())
						{
							executor = user.getCode();
							break;
						}
					}
					else
					{
						log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO - Papel " + papel.getCode() + " sem usuário definido - Colaborador: " + rs.getInt("numemp") + "/" + rs.getInt("numcad") + " - Data: " + NeoUtils.safeDateFormat(rs.getDate("proexm"), "dd/MM/yyyy"));
						continue;
					}

					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

					InstantiableEntityInfo tarefaAS = AdapterUtils.getInstantiableEntityInfo("tarefaASO");
					NeoObject no = tarefaAS.createNewInstance();
					EntityWrapper wrapper = new EntityWrapper(no);

					GregorianCalendar proexm = new GregorianCalendar();
					proexm.setTime(rs.getDate("proxData"));

					wrapper.findField("numemp").setValue(rs.getLong("numemp"));
					wrapper.findField("tipcol").setValue(rs.getLong("tipcol"));
					wrapper.findField("numcad").setValue(rs.getLong("numcad"));
					wrapper.findField("numcpf").setValue(rs.getLong("numcpf"));
					wrapper.findField("proexm").setValue(proexm);
					wrapper.findField("tarefa").setValue(tarefa);
					PersistEngine.persist(no);

					log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO - Responsavel " + executor + " Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoUtils.safeDateFormat(rs.getDate("DatAdm"), "dd/MM/yyyy"));

					Thread.sleep(1000);
				}
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Validade Registro Armamento");
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Validade Registro Armamento");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
