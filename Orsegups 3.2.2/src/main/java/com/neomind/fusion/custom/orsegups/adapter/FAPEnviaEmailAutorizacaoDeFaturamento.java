package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPEmailUtils;
import com.neomind.fusion.custom.orsegups.fap.utils.FapUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPEnviaEmailAutorizacaoDeFaturamento implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {

	ArrayList<String> listaEmails = new ArrayList<String>();
	ArrayList<String> listaEmailsCopia = new ArrayList<String>();
	try {
	    processEntity.setValue("aceitaNotaFiscal", true);
	    Long aplicacaoPagamento = (Long) (Long) processEntity.findValue("aplicacaoPagamento.codigo");

	    String code = (String) processEntity.findValue("wfprocess.code");
	    NeoObject fornecedor = (NeoObject) processEntity.findValue("fornecedor");
	    String nomeFornecedor = "";
	    Long cgccpf = 0L;

	    if (fornecedor != null) {
		EntityWrapper wrapperFornecedor = new EntityWrapper(fornecedor);
		String emailForn = (String) wrapperFornecedor.findValue("fornecedor.intnet");
		cgccpf = (Long) wrapperFornecedor.findValue("fornecedor.cgccpf");
		nomeFornecedor = (String) wrapperFornecedor.findValue("fornecedor.nomfor");
		if (emailForn != null) {
		    StringTokenizer tokenEmailStr = new StringTokenizer(emailForn, ";");
		    while (tokenEmailStr.hasMoreElements()) {
			String email = (String) tokenEmailStr.nextElement();
			email = email.trim();
			if (email.contains("@")) {
			    listaEmails.add(email);
			}
		    }
		}
	    }

	    String emailCopia = "";
	    emailCopia = (String) processEntity.findValue("solicitanteOrcamento.email");
	    emailCopia = emailCopia + ";" + (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");

	    if (emailCopia != null) {
		StringTokenizer tokenEmailStr = new StringTokenizer(emailCopia, ";");
		while (tokenEmailStr.hasMoreElements()) {
		    String email = (String) tokenEmailStr.nextElement();
		    email = email.trim();
		    if (email.contains("@")) {
			listaEmailsCopia.add(email);
		    }
		}
	    }

	    List<NeoObject> listaFapsAgrupadas = new ArrayList<NeoObject>();
	    QLEqualsFilter codigoTarefaFilter = new QLEqualsFilter("codigoTarefa", code);
	    listaFapsAgrupadas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), codigoTarefaFilter);

	    String atividadeAnterior = "";
	    NeoUser owner = null;
	    if (origin != null) {
		atividadeAnterior = origin.getInstance().getActivity().getActivityName();
		owner = origin.getInstance().getOwner();
	    }

	    if ((atividadeAnterior.contains("Slipar Pagamento") && owner == null) || !atividadeAnterior.contains("Slipar Pagamento") || origin == null) {
		QLEqualsFilter filter = new QLEqualsFilter("wfprocess.code", code);
		NeoObject fapAgrupadora = PersistEngine.getObject(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), filter);

		EntityWrapper wrapperFornecedor = new EntityWrapper(fornecedor);
		String emailForn = wrapperFornecedor.findGenericValue("fornecedor.intnet");
		
		String regional = "";
		NeoUser aprovador = null;
		String emailAprovador = "";

		//String corpoEmail = FAPEmailUtils.autorizacaoFaturamentoFapTatico(fapAgrupadora);
		String corpoEmail = "";

		/*
		 * Aplicacações 10 e 11 usarão novos métodos e classes para
		 * envio de e-mail
		 */
		switch (Integer.parseInt(aplicacaoPagamento.toString())) {
		case 11:
		    String nomeRota = processEntity.findGenericValue("rotaSigma.rotaSigma.nm_rota");
		    regional = FapUtils.getSiglaRegional(nomeRota);
		    aprovador = FapUtils.getAprovador(regional);
		    emailAprovador = aprovador.getEmail();

		    corpoEmail = FAPEmailUtils.autorizacaoFaturamentoFapTatico(fapAgrupadora);
		    FAPEmailUtils.enviaEmailFap(corpoEmail, emailForn, emailAprovador, false);
		    break;
		case 10:
		    String nomeTecnico = processEntity.findGenericValue("tecnicoSigma.tecnico.nm_colaborador");
		    regional = FapUtils.getSiglaRegional(nomeTecnico);
		    NeoObject noRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), new QLEqualsFilter("siglaRegional", regional));
		    NeoObject noUsuariosRegional = noRegional != null ? PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPTecnicoUsuarios"), new QLEqualsFilter("regional", noRegional)) : null;
		    aprovador = new EntityWrapper(noUsuariosRegional).findGenericValue("aprovador");
		    emailAprovador = aprovador.getEmail();

		    corpoEmail = FAPEmailUtils.autorizacaoFaturamentoFapTecnico(fapAgrupadora);
		    FAPEmailUtils.enviaEmailFap(corpoEmail, emailForn, emailAprovador, true);
		    break;
		default:
		    // DO NOTHING
		    break;
		}

	    }

	    /* Aplicações de 1 a 9 irão enviar email usando o código abaixo */
	    StringBuilder html = new StringBuilder();

	    html.append("<html>");
	    html.append("<center><h1>" + "Autorização de Faturamento - Aprovado" + "</h1></center>");
	    html.append("<br/>");
	    html.append("Prezado(a) fornecedor(a) " + cgccpf + " - " + nomeFornecedor + ", as FAP's liberadas para faturamento foram: ");
	    html.append("<br/>");

	    if (NeoUtils.safeIsNotNull(listaFapsAgrupadas) && !listaFapsAgrupadas.isEmpty()) {
		html.append("		<table border=1 class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");

		html.append("			<tr style=\"cursor: auto\">");
		html.append("				<th style=\"cursor: auto; text-align: left\">FAP</th>");
		if ((aplicacaoPagamento == 2L) || (aplicacaoPagamento == 3L) || (aplicacaoPagamento == 4L) || (aplicacaoPagamento == 5L) || (aplicacaoPagamento == 6L) || (aplicacaoPagamento == 7L) || (aplicacaoPagamento == 8L) || (aplicacaoPagamento == 9L)) {
		    html.append("				<th style=\"cursor: auto; text-align: left\">Conta</th>");
		} else {
		    html.append("				<th style=\"cursor: auto; text-align: left\">Placa</th>");
		}
		html.append("				<th style=\"cursor: auto; text-align: left\">Tipo do Item</th>");
		html.append("				<th style=\"cursor: auto; text-align: left\">Descrição do Item</th>");
		html.append("				<th style=\"cursor: auto; text-align: left\">Valor</th>");
		html.append("			</tr>");
		html.append("			<tbody>");

		BigDecimal valorTotalFapAgr = new BigDecimal(0.00);
		BigDecimal valorTotalHistoricoProduto = new BigDecimal(0.00);
		BigDecimal valorTotalHistoricoServico = new BigDecimal(0.00);
		BigDecimal valorTotalDesconto = new BigDecimal(0.00);

		for (NeoObject neoObject : listaFapsAgrupadas) {
		    EntityWrapper registroWrapper = new EntityWrapper(neoObject);
		    @SuppressWarnings("unchecked")
		    List<NeoObject> listaItensOrcamento = (List<NeoObject>) registroWrapper.findValue("itemOrcamento");

		    BigDecimal valorTotalFap = new BigDecimal(0.00);

		    if (NeoUtils.safeIsNotNull(listaItensOrcamento) && !listaItensOrcamento.isEmpty()) {
			for (NeoObject objItemOrcamento : listaItensOrcamento) {
			    EntityWrapper itemOrcamentoWrapper = new EntityWrapper(objItemOrcamento);
			    String tipoItem = (String) itemOrcamentoWrapper.findValue("tipoItem.descricao");
			    String descricaoItem = (String) itemOrcamentoWrapper.findValue("descricao");
			    String valorItem = NeoUtils.safeOutputString((BigDecimal) itemOrcamentoWrapper.findValue("valor"));
			    if (itemOrcamentoWrapper.findValue("tipoItem.descricao") != null && ((String) itemOrcamentoWrapper.findValue("tipoItem.descricao")).equals("DESCONTO")) {
			    	valorTotalFap = valorTotalFap.subtract((BigDecimal) itemOrcamentoWrapper.findValue("valor"));
			    }else {			    	
			    	valorTotalFap = valorTotalFap.add((BigDecimal) itemOrcamentoWrapper.findValue("valor"));
			    }

			    if (itemOrcamentoWrapper.findValue("tipoItem.descricao") != null && ((String) itemOrcamentoWrapper.findValue("tipoItem.descricao")).equals("PRODUTO")) {
				valorTotalHistoricoProduto = valorTotalHistoricoProduto.add((BigDecimal) itemOrcamentoWrapper.findValue("valor"));
			    }
			    if (itemOrcamentoWrapper.findValue("tipoItem.descricao") != null && ((String) itemOrcamentoWrapper.findValue("tipoItem.descricao")).equals("SERVIÇO")) {
				valorTotalHistoricoServico = valorTotalHistoricoServico.add((BigDecimal) itemOrcamentoWrapper.findValue("valor"));
			    }
			    if (itemOrcamentoWrapper.findValue("tipoItem.descricao") != null && ((String) itemOrcamentoWrapper.findValue("tipoItem.descricao")).equals("DESCONTO")) {
			    	valorTotalDesconto = valorTotalDesconto.add((BigDecimal) itemOrcamentoWrapper.findValue("valor"));
				}

			    html.append("		<tr style=\"margin-bottom: 5px;\">");
			    html.append("			<td>" + registroWrapper.findValue("wfprocess.code") + "</td>");
			    if ((aplicacaoPagamento == 2L) || (aplicacaoPagamento == 3L) || (aplicacaoPagamento == 4L) || (aplicacaoPagamento == 5L) || (aplicacaoPagamento == 6L) || (aplicacaoPagamento == 7L) || (aplicacaoPagamento == 8L) || (aplicacaoPagamento == 9L)) {
				String central = "";
				String particao = "";
				String contaSigma = "";

				central = (String) registroWrapper.findValue("contaSigma.id_central");
				particao = (String) registroWrapper.findValue("contaSigma.particao");
				contaSigma = central + "[" + particao + "]";

				html.append("			<td>" + contaSigma + "</td>");
			    } else {
				String placa = "";
				placa = (String) registroWrapper.findValue("viatura.placa");

				html.append("			<td>" + placa + "</td>");
			    }
			    html.append("			<td>" + tipoItem + "</td>");
			    html.append("			<td>" + descricaoItem + "</td>");
			    html.append("			<td>" + "&nbsp;" + valorItem + "</td>");
			    html.append("		</tr>");
			    html.append("		<tr style=\"margin-bottom: 5px;\">");
			    html.append("			<td colspan=4 style=\"text-align: right\">" + "<b>Valor Total da FAP&nbsp;</b> " + "</td>");
			    html.append("			<td colspan=1 style=\"text-align: left\">" + "&nbsp;" + NeoUtils.safeOutputString(valorTotalFap) + "</td>");
			    html.append("		</tr>");
			}
		    }

		    valorTotalFapAgr = valorTotalFapAgr.add((BigDecimal) valorTotalFap);
		}
		html.append("			<tr style=\"margin-bottom: 5px;\">");
		html.append("				<td colspan=4 style=\"text-align: right\">" + "<b>Total Geral Produto&nbsp;</b> " + "</td>");
		html.append("				<td colspan=1 style=\"text-align: left\">" + "&nbsp;" + NeoUtils.safeOutputString(valorTotalHistoricoProduto) + "</td>");
		html.append("			</tr>");
		html.append("			<tr style=\"margin-bottom: 5px;\">");
		html.append("				<td colspan=4 style=\"text-align: right\">" + "<b>Total Geral Serviço&nbsp;</b> " + "</td>");
		html.append("				<td colspan=1 style=\"text-align: left\">" + "&nbsp;" + NeoUtils.safeOutputString(valorTotalHistoricoServico) + "</td>");
		html.append("			</tr>");
		html.append("			<tr style=\"margin-bottom: 5px;\">");
		html.append("				<td colspan=4 style=\"text-align: right\">" + "<b>Total Geral Desconto&nbsp;</b> " + "</td>");
		html.append("				<td colspan=1 style=\"text-align: left\">" + "&nbsp;" + NeoUtils.safeOutputString(valorTotalDesconto) + "</td>");
		html.append("			</tr>");
		html.append("			<tr style=\"margin-bottom: 5px;\">");
		html.append("				<td colspan=4 style=\"text-align: right\">" + "<b>Total Geral&nbsp;</b> " + "</td>");
		html.append("				<td colspan=1 style=\"text-align: left\">" + "&nbsp;" + NeoUtils.safeOutputString(valorTotalFapAgr) + "</td>");
		html.append("			</tr>");
		html.append("		</table>");
	    }

	    String textoObjeto = "";

	    html.append("<br/>");

	    if (aplicacaoPagamento >= 2L && aplicacaoPagamento <= 7L) {
	    	html.append(FAPEmailUtils.montaInfoComplementaresManutencaoInstalacao());
	    } else if (aplicacaoPagamento == 1L) {
	    	textoObjeto = "<strong>INFORMAÇÕES COMPLEMENTARES:</strong> <br/><br/>";
	 	    textoObjeto = textoObjeto + "<strong>1) PRAZOS PARA ENVIO:</strong> <br/>";
	 	    textoObjeto = textoObjeto + "A nota fiscal e o boleto devem ser enviados IMEDIATAMENTE para ORSEGUPS em até 5 dias úteis antes do vencimento, que é dia 12.<br/><br/>";
	 	    textoObjeto = textoObjeto + "Notas fiscais que chegarem fora deste prazo, serão pagas somente no mês seguinte, obedecendo a mesma data de seu vencimento.<br/><br/>";
	 	    textoObjeto = textoObjeto + "CADA NOTA FISCAL DEVE TER UM BOLETO. Quem recebe via depósito bancário não há alteração.<br/><br/>";
	 	    textoObjeto = textoObjeto + "<strong>2) ENDEREÇO PARA ENVIO:</strong> <br/>";
	 	    textoObjeto = textoObjeto + "E-MAIL: fap@orsegups.com.br. (*Notas Fiscais Eletrônicas podem ser enviadas apenas por e-mail.)<br/><br/>";
	 	    textoObjeto = textoObjeto + "CORREIO: Rua Getúlio Vargas, Nº: 2729, Centro, São José/SC - CEP: 88.103-400. A/C Depto de Controladoria.<br/>";
	 	    textoObjeto = textoObjeto + "Dúvidas quanto a emissão de sua nota fiscal ou informações de pagamentos podem ser obtidas através do e-mail: fap@orsegups.com.br ou através do telefone (48) 3381-6652.";
	    } else {
	    	textoObjeto = "<strong>INFORMAÇÕES COMPLEMENTARES:</strong> <br/><br/>";
	 	    textoObjeto = textoObjeto + "<strong>1) PRAZOS PARA ENVIO:</strong> <br/>";
	 	    textoObjeto = textoObjeto + "A nota fiscal e o boleto devem ser enviados IMEDIATAMENTE para ORSEGUPS ou em até 5 dias úteis antes do vencimento, obedecendo as datas de pagamento descritas no item 3.<br/><br/>";
	 	    textoObjeto = textoObjeto + "Notas fiscais que chegarem fora deste prazo serão pagas somente no mês seguinte obedecendo a mesma data de seu vencimento, conforme item 3.<br/><br/>";
	 	    textoObjeto = textoObjeto + "<strong>2) ENDEREÇO PARA ENVIO:</strong> <br/>";
	 	    textoObjeto = textoObjeto + "CORREIO: Rua Getúlio Vargas, Nº: 2729, Centro, São José/SC - CEP: 88.103-400. A/C Depto de Controladoria.<br/>";
	 	    textoObjeto = textoObjeto + "E-MAIL: fap@orsegups.com.br. (*Notas Fiscais Eletrônicas podem ser enviadas apenas por e-mail.)<br/><br/>";
	 	    textoObjeto = textoObjeto + "<strong>3) DATAS DE PAGAMENTOS: </strong> <br/>";
	 	    textoObjeto = textoObjeto + "PRESTADOR DE SERVIÇO DE MANUTENÇÃO DE VEÍCULOS VENCIMENTO É TODO DIA 12.<br/>";
	 	    textoObjeto = textoObjeto + "PRESTADOR DE SERVIÇO DE MANUTENÇÃO EM CLIENTES VENCIMENTO TODO DIA 30.<br/><br/>";
	 	    textoObjeto = textoObjeto + "<strong><font color='FF0000'>AVISO:</font></strong> <br/>";
	 	    textoObjeto = textoObjeto + "<font color='FF0000'>A PARTIR DE <strong>01/04/2016</strong>, CADA NOTA FISCAL DEVE TER UM BOLETO. Quem recebe via depósito bancário não há alteração.</font><br/><br/>";
	 	    textoObjeto = textoObjeto + "Dúvidas quanto a emissão de sua nota fiscal ou informações de pagamentos podem ser obtidas através do e-mail: fap@orsegups.com.br ou através do telefone (48) 3381-6652.";
	    }
	    

	    html.append(textoObjeto);

	    List<String> copiaOculta = new ArrayList<String>();
	    copiaOculta.add("emailautomatico@orsegups.com.br");
	    
	    if (aplicacaoPagamento != 10 && aplicacaoPagamento != 11) {
		if (!listaEmails.isEmpty()) {
		    for (String emailDst : listaEmails) {
			OrsegupsUtils.sendEmail2Orsegups(emailDst, "fusion@orsegups.com.br", "Autorização de Faturamento - Aprovado", "", html.toString(), "fap@orsegups.com.br", listaEmailsCopia, copiaOculta);
		    }
		} else {
		    String emailDst = "controladoria@orsegups.com.br";
		    OrsegupsUtils.sendEmail2Orsegups(emailDst, "fusion@orsegups.com.br", "Fornecedor Sem E-mail (Autorização de Faturamento - Aprovado)", "", html.toString(), "fap@orsegups.com.br", listaEmailsCopia, copiaOculta);
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new WorkflowException(e.getMessage());
	}
    }

	@Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub

    }

}
