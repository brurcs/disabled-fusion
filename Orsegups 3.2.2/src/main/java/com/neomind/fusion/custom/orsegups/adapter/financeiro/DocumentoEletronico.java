package com.neomind.fusion.custom.orsegups.adapter.financeiro;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ide", namespace = "http://www.portalfiscal.inf.br/nfe")
public class DocumentoEletronico {
	
	private Long codigoUnidadeFederativa;
	private Long codigoNotaFiscal;
	private String naturezaOperacao;
	private Long especieDocumento;
	private String serie;
	private long numeroNotaFiscal;
	private String dataEmissao;
	private String dataSaida;
	private String tipoNotaFiscal;
	private Long codigoOperacao;
	private Long codigoMunicipio;
	private Long tipoImposto;
	private Long tipoEmissao;
	private Long codigoVerificador;
	private Long finalidadeNota;
	private String processoEmissao;
	private String versaoSapiens;
	
	@XmlElement(name = "cUF", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCodigoUnidadeFederativa() {
		return codigoUnidadeFederativa;
	}
	
	public void setCodigoUnidadeFederativa(Long codigoUnidadeFederativa) {
		this.codigoUnidadeFederativa = codigoUnidadeFederativa;
	}
	
	@XmlElement(name = "cNF", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCodigoNotaFiscal() {
		return codigoNotaFiscal;
	}
	
	public void setCodigoNotaFiscal(Long codigoNotaFiscal) {
		this.codigoNotaFiscal = codigoNotaFiscal;
	}
	
	@XmlElement(name = "natOp", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getNaturezaOperacao() {
		return naturezaOperacao;
	}
	
	public void setNaturezaOperacao(String naturezaOperacao) {
		this.naturezaOperacao = naturezaOperacao;
	}
	
	@XmlElement(name = "mod", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getEspecieDocumento() {
		return especieDocumento;
	}
	
	public void setEspecieDocumento(Long especieDocumento) {
		this.especieDocumento = especieDocumento;
	}
	
	@XmlElement(name = "serie", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getSerie() {
		return serie;
	}
	
	public void setSerie(String serie) {
		this.serie = serie;
	}
	
	@XmlElement(name = "nNF", namespace = "http://www.portalfiscal.inf.br/nfe")
	public long getNumeroNotaFiscal() {
		return numeroNotaFiscal;
	}
	
	public void setNumeroNotaFiscal(long numeroNotaFiscal) {
		this.numeroNotaFiscal = numeroNotaFiscal;
	}
	
	@XmlElement(name = "dhEmi", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getDataEmissao() {
		return dataEmissao;
	}
	
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	
	@XmlElement(name = "dhSaiEnt", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getDataSaida() {
		return dataSaida;
	}
	
	public void setDataSaida(String dataSaida) {
		this.dataSaida = dataSaida;
	}
	
	@XmlElement(name = "tpNF", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getTipoNotaFiscal() {
		return tipoNotaFiscal;
	}
	
	public void setTipoNotaFiscal(String tipoNotaFiscal) {
		this.tipoNotaFiscal = tipoNotaFiscal;
	}
	
	@XmlElement(name = "idDest", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCodigoOperacao() {
		return codigoOperacao;
	}

	public void setCodigoOperacao(Long codigoOperacao) {
		this.codigoOperacao = codigoOperacao;
	}

	@XmlElement(name = "cMunFG", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCodigoMunicipio() {
		return codigoMunicipio;
	}
	
	public void setCodigoMunicipio(Long codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	
	@XmlElement(name = "tpImp", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getTipoImposto() {
		return tipoImposto;
	}
	
	public void setTipoImposto(Long tipoImposto) {
		this.tipoImposto = tipoImposto;
	}

	@XmlElement(name = "tpEmis", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getTipoEmissao() {
		return tipoEmissao;
	}

	public void setTipoEmissao(Long tipoEmissao) {
		this.tipoEmissao = tipoEmissao;
	}

	@XmlElement(name = "cDV", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getCodigoVerificador() {
		return codigoVerificador;
	}

	public void setCodigoVerificador(Long codigoVerificador) {
		this.codigoVerificador = codigoVerificador;
	}

	@XmlElement(name = "finNFe", namespace = "http://www.portalfiscal.inf.br/nfe")
	public Long getFinalidadeNota() {
		return finalidadeNota;
	}

	public void setFinalidadeNota(Long finalidadeNota) {
		this.finalidadeNota = finalidadeNota;
	}

	@XmlElement(name = "procEmi", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getProcessoEmissao() {
		return processoEmissao;
	}

	public void setProcessoEmissao(String processoEmissao) {
		this.processoEmissao = processoEmissao;
	}

	@XmlElement(name = "verProc", namespace = "http://www.portalfiscal.inf.br/nfe")
	public String getVersaoSapiens() {
		return versaoSapiens;
	}

	public void setVersaoSapiens(String versaoSapiens) {
		this.versaoSapiens = versaoSapiens;
	}
	
}
