package com.neomind.fusion.custom.orsegups.RE;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;


public class RePDF_5_X {

    public static String leadingZeros(String s, int length) {
    	String retorno = null;
    	try {
    		if (s.length() >= length) 
    			return s;
    		else 
    			retorno = String.format("%0" + (length-s.length()) + "d%s", 0, s);
    	}catch(Exception ex) {
    			ex.printStackTrace();
    	}
		return retorno;
    }

    //Cria PDF por TOMADOR
    public static boolean criarArquivoPDF(String codEmp, String dataCpt, String inscricaoTomadorAnterior, PdfReader reader, String outFile, int paginaStart, int paginaEnd, boolean encontrado) {
		boolean retorno = true;
    	try {
    		String naoEncontrado = "";
    		if (!encontrado){
    			naoEncontrado = "\\naoEncontrado\\";
    		}
    		outFile = outFile+naoEncontrado+"\\"+codEmp+"\\"+inscricaoTomadorAnterior+"\\"+dataCpt.replace("/", "");
    		File file = new File(outFile);
    		if (!file.exists())
    			file.mkdirs();
			outFile = outFile+"\\RE-" + codEmp + "-" + dataCpt.replace("/", "") + "-"+ leadingZeros(inscricaoTomadorAnterior, 14) + ".pdf";
			Document document = new Document(reader.getPageSizeWithRotation(1));
			PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
			document.open();
			for (int i = paginaStart; i <= paginaEnd; i++) {
				PdfImportedPage page = writer.getImportedPage(reader, i);
				writer.addPage(page);
			}
			document.close();
			writer.close();
		} catch (FileNotFoundException e) {
			retorno = false;
			e.printStackTrace();
		} catch (DocumentException e) {
			retorno = false;
			e.printStackTrace();
		} catch (IOException e) {
			retorno = false;
			e.printStackTrace();
		} catch (Exception ex){
			retorno = false;
			ex.printStackTrace();
		}
		return retorno;
	}
    
    //Ler PDF identificando se o proximo PDF � igual ao TOMADOR anterior caso n�o seje ele manda criar o pdf e continua num ciclo ate ler todas as paginas do PDF 
    public String leituraPdf(InputStream input, String dataCpt, String codEmp){
    	String arquivosGerados = " Arquivos Gerados: \r\n"; 
    	try {
             PdfReader reader = new PdfReader(input);
             int totalPaginas = reader.getNumberOfPages();
             System.out.println ("Number of pages : " + totalPaginas);
         
             PdfReaderContentParser parser = new PdfReaderContentParser(reader);
             TextExtractionStrategy strategy;
             
             String inscricaoTomadorAnterior = "";
             int paginaStart = 1;
             int paginaEnd = 0;
             
             for (int paginaAtual = 1; paginaAtual <= totalPaginas; paginaAtual++) {
                 
             	strategy = parser.processContent(paginaAtual, new SimpleTextExtractionStrategy());
                String textoPagina = strategy.getResultantText();

             	if (textoPagina != null) {
                 	
             		// Padrao de localizacao (linha apos o campo FPAS "515") na string
             		int posicao515 = textoPagina.indexOf("515\n");
             		int posicaoInscricaoStart = posicao515 + 4;
             		int posicaoInscricaoEnd = textoPagina.indexOf("\n", posicaoInscricaoStart);
             		String inscricaoTomadorRaw = textoPagina.substring(posicaoInscricaoStart, posicaoInscricaoEnd);
             		
             		// Obtendo apenas nos digitos
             		String inscricaoTomador = inscricaoTomadorRaw.replaceAll("\\D+","");
                 	
             		// TODO: Verificando se � valido na base de tomadores de servico do Vetorh
             		Boolean valid = true; // aqui ir�o colocar o metodo de busca do TOMADOR na base da senior
             		

           			if (paginaAtual == 1) {
           				inscricaoTomadorAnterior = inscricaoTomador;
					}

                   	if (!inscricaoTomador.equals(inscricaoTomadorAnterior)) {
           				paginaEnd = paginaAtual -1;
           				String arquivoOut = "c:/temp"; // caminho de cria��o dos arquivos separados
           				criarArquivoPDF(codEmp,dataCpt,inscricaoTomadorAnterior,reader,arquivoOut,paginaStart,paginaEnd, valid);
           				inscricaoTomadorAnterior = inscricaoTomador;
           				paginaStart = paginaAtual;
                     	arquivosGerados = arquivosGerados+ "P�gina: " + paginaAtual + ", Inscri��o Tomador Raw: " + inscricaoTomadorRaw + ", Inscri��o Tomador: " + inscricaoTomador+"\r\n";
                     	
 					}
 				}                
             }            
         } 
         catch (Exception e) {
             e.printStackTrace();
         }
    	 return arquivosGerados;
     }
    
}
