package com.neomind.fusion.custom.orsegups.callcenter;

public class ProvidenciaVO
{
	private Integer codigoCliente;
	
	private Integer codigoProvidencia;
	
	private String nome;
	
	private String telefone1;
	
	private String telefone2;
	
	private String telefone3;
	
	private String telefone4;
	
	private String telefone5;
	
	private String telefone6;

	private String email;
	
	private Integer prioridade;

	public Integer getCodigoCliente()
	{
		return codigoCliente;
	}

	public void setCodigoCliente(Integer codigoCliente)
	{
		this.codigoCliente = codigoCliente;
	}

	public Integer getCodigoProvidencia()
	{
		return codigoProvidencia;
	}

	public void setCodigoProvidencia(Integer codigoProvidencia)
	{
		this.codigoProvidencia = codigoProvidencia;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public String getTelefone1()
	{
		return telefone1;
	}

	public void setTelefone1(String telefone1)
	{
		this.telefone1 = telefone1;
	}

	public String getTelefone2()
	{
		return telefone2;
	}

	public void setTelefone2(String telefone2)
	{
		this.telefone2 = telefone2;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public Integer getPrioridade()
	{
		return prioridade;
	}

	public void setPrioridade(Integer prioridade)
	{
		this.prioridade = prioridade;
	}

	public String getTelefone3()
	{
		return telefone3;
	}

	public void setTelefone3(String telefone3)
	{
		this.telefone3 = telefone3;
	}

	public String getTelefone4()
	{
		return telefone4;
	}

	public void setTelefone4(String telefone4)
	{
		this.telefone4 = telefone4;
	}

	public String getTelefone5()
	{
		return telefone5;
	}

	public void setTelefone5(String telefone5)
	{
		this.telefone5 = telefone5;
	}

	public String getTelefone6()
	{
		return telefone6;
	}

	public void setTelefone6(String telefone6)
	{
		this.telefone6 = telefone6;
	}
}
