package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import com.neomind.fusion.custom.orsegups.adapter.AgendamentoDTO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoDateUtils;


public class RotinaConfirmacaoAgendamento implements CustomJobAdapter{
	
	@Override
	public void execute(CustomJobContext ctx) {
		Set<AgendamentoDTO> agendamentos = null;
		
		agendamentos = consultaAgendamentos();
		
		for(AgendamentoDTO agendamento : agendamentos) {
			try	{
				GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 2L);			
				NeoUser solicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteRotinaConfirmacaoAgendamento")));
				NeoUser executor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "ExecutorRotinaConfirmacaoAgendamento")));			
				
				String descricao = montaCorpoTarefa(agendamento);
				String titulo = "Confirmar Agendamento - OS: " + agendamento.getNumeroOS() + " - " + agendamento.getContaSigma() + " " + agendamento.getRazaoCliente();
				
				IniciarTarefaSimples tarefa = new IniciarTarefaSimples();
				String codigoTarefa = tarefa.abrirTarefaReturnNeoId(solicitante.getCode(), executor.getCode(), titulo, descricao, "1", "sim", prazo, null);
				
				System.out.println(codigoTarefa);
				
			} catch (Exception e){
				e.printStackTrace();
				System.out.println("ROTINA CONFIRMACAO AGENDAMENTO. ERRO AO INICIAR TAREFA PARA OS: " + agendamento.getNumeroOS());
			}
			
		}						
	}
	
	public Set<AgendamentoDTO> consultaAgendamentos(){
		Set<AgendamentoDTO> listaAgendamentos = new HashSet<>();
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		
		GregorianCalendar dataAtual = new GregorianCalendar();
		dataAtual.add(GregorianCalendar.DAY_OF_MONTH, 5);
		dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 0);
		dataAtual.set(GregorianCalendar.MINUTE, 0);
		dataAtual.set(GregorianCalendar.SECOND, 0);
		
		String dataInicial = NeoDateUtils.safeDateFormat(dataAtual, "yyyy-MM-dd HH:mm:ss");
		
		dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 23);
		dataAtual.set(GregorianCalendar.MINUTE, 59);
		dataAtual.set(GregorianCalendar.SECOND, 59);
		
		String dataFinal = NeoDateUtils.safeDateFormat(dataAtual, "yyyy-MM-dd HH:mm:ss");
		
		try	{
			
			conn  = PersistEngine.getConnection("SIGMA90");
			
			sql.append(" SELECT  ID_ORDEM, CAST(DEF.IDOSDEFEITO AS VARCHAR(4)) + ' - ' + DEF.DESCRICAODEFEITO AS DEFEITO, ");
			sql.append(" DEFEITO AS DESC_DEFEITO, DATAAGENDADA, COL.NM_COLABORADOR,                                       ");
			sql.append(" C.ID_CENTRAL, C.RAZAO, SUBSTRING(COL.NM_COLABORADOR, 1, 3) AS REGIONAL				              ");
			sql.append(" FROM DBORDEM OS																			      ");
			sql.append(" INNER JOIN dbCENTRAL C ON OS.CD_CLIENTE = C.CD_CLIENTE										      ");
			sql.append(" INNER JOIN COLABORADOR COL ON COL.CD_COLABORADOR = OS.ID_INSTALADOR						      ");
			sql.append(" INNER JOIN OSDEFEITO DEF ON DEF.IDOSDEFEITO = OS.IDOSDEFEITO                                     ");
			sql.append(" WHERE DATAAGENDADA BETWEEN '" + dataInicial + "' AND '" + dataFinal + "'"           			   );
			sql.append(" AND DEF.IDOSDEFEITO IN (166,167,1067)                                                            ");
			sql.append(" ORDER BY OS.DATAAGENDADA DESC																      ");
			
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			
			while(rs.next()) {
				AgendamentoDTO agendamentoDTO = new AgendamentoDTO();
				
				GregorianCalendar dtAgd = new GregorianCalendar();
				dtAgd.setTime(rs.getTimestamp("DATAAGENDADA"));
				
				agendamentoDTO.setNumeroOS(rs.getLong("ID_ORDEM"));
				agendamentoDTO.setDefeito(rs.getString("DEFEITO"));
				agendamentoDTO.setDescricaoDefeito(rs.getString("DESC_DEFEITO"));
				agendamentoDTO.setDataAgendamento(NeoDateUtils.safeDateFormat(dtAgd, "dd-MM-yyy HH:mm:ss"));
				agendamentoDTO.setTecnicoResponsavel(rs.getString("NM_COLABORADOR"));
				agendamentoDTO.setContaSigma(rs.getString("ID_CENTRAL"));
				agendamentoDTO.setRazaoCliente(rs.getString("RAZAO"));
				agendamentoDTO.setRegional(rs.getString("REGIONAL"));
				
				listaAgendamentos.add(agendamentoDTO);
			}				
			
		} catch (Exception e) {
			System.out.println("### ROTINA CONFIRMACAO AGENDAMENTO - ERRO AO BUSCAR LISTA DE AGENDAMENTOS");
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
					
		return listaAgendamentos;
	}
	
	
	public String montaCorpoTarefa (AgendamentoDTO agendamento) {
		StringBuilder corpoTarefa = new StringBuilder();
		
		corpoTarefa.append(" <html>                                                                  ");
        corpoTarefa.append("    <head>                                                               ");
        corpoTarefa.append("        <meta charset='utf-8'>                                           ");
        corpoTarefa.append("    </head>                                                              ");
		corpoTarefa.append("      <body>                                                             ");
		corpoTarefa.append("          <table border='2' class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">                                             ");
		corpoTarefa.append("              <tr>                                                       ");
		corpoTarefa.append("                  <th>OS</th>                                            ");
		corpoTarefa.append("                  <td>&nbsp; "+agendamento.getNumeroOS()+" </td>         ");
		corpoTarefa.append("              </tr>                                                      ");
		corpoTarefa.append("              <tr>                                                       ");
		corpoTarefa.append("              	<th>DEFEITO</th>                                         ");
		corpoTarefa.append("              	<td>&nbsp; "+agendamento.getDefeito()+" </td>            ");
		corpoTarefa.append("              </tr>                                                      ");
		corpoTarefa.append("              <tr>                                                       ");
		corpoTarefa.append("              <th style=\"cursor: auto; white-space: normal\">DESCRIÇÃO OS</th> ");
		corpoTarefa.append("               <td>&nbsp; "+agendamento.getDescricaoDefeito()+"</td> ");
		corpoTarefa.append("              </tr>                                                      ");
		corpoTarefa.append("              <tr>                                                       ");
		corpoTarefa.append("              	<th>DATA AGENDADA</th>                                   ");
		corpoTarefa.append("                <td>&nbsp; "+agendamento.getDataAgendamento()+" </td>    ");
		corpoTarefa.append("              </tr>                                                      ");
		corpoTarefa.append("              <tr>                                                       ");
		corpoTarefa.append("              	<th>TECNICO RESP.</th>                                   ");
		corpoTarefa.append("                <td>&nbsp; "+agendamento.getTecnicoResponsavel()+" </td> ");
		corpoTarefa.append("              </tr>                                                      ");
		corpoTarefa.append("              <tr>                                                       ");
		corpoTarefa.append("              	<th>BA</th>                                              ");
		corpoTarefa.append("                <td>&nbsp; "+agendamento.getContaSigma()+" </td>         ");
		corpoTarefa.append("              </tr>                                                      ");
		corpoTarefa.append("              <tr>                                                       ");
		corpoTarefa.append("              	<th>CLIENTE</th>                                         ");
		corpoTarefa.append("                <td>&nbsp; "+agendamento.getRazaoCliente()+"</td>        ");
		corpoTarefa.append("              </tr>                                                      ");         
		corpoTarefa.append("          </table>                                                       ");
		corpoTarefa.append("      </body>                                                            ");
		corpoTarefa.append(" </html>                                                                 ");
	
		return corpoTarefa.toString();
		
	}
	
}


