package com.neomind.fusion.custom.orsegups.fap.slip.job;

import java.util.List;

import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskStatus;

public class FAPSlipAvancaOCIntegracaoJob implements CustomJobAdapter
{
	Boolean printLog = Boolean.valueOf(false);

	public void execute(CustomJobContext arg0)
	{
		System.out.println("Inicio FAPSlipAvancaOCIntegracaoJob");
		
		NeoUser nuExecutor = PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));
		
		QLGroupFilter qlFilter = new QLGroupFilter("AND", new QLEqualsFilter("activity.name", "Aguardando Integração"), new QLEqualsFilter("activity.process.model.name", "F002 - FAP Slip"));
		List<Task> listTasks = PersistEngine.getObjects(Task.class, qlFilter);
		System.out.println("FAPSlipAvancaOCIntegracaoJob: Tarefas pendentes - " + listTasks.size());
		for (Task task : listTasks)
		{
			if (task.getStatus().equals(TaskStatus.READY))
			{
				System.out.println("FAPSlipAvancaOCIntegracaoJob: Avançando tarefa " + task);
				
				RuntimeEngine.getTaskService().complete(task, nuExecutor);

				System.out.println("FAPSlipAvancaOCIntegracaoJob: Tarefa " + task + " avançada com sucesso!");
			}
		}
	}
}