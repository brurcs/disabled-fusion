package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

import br.com.segware.sigmaWebServices.webServices.EventoRecebido;
import br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy;

/**
 * Rotina responsável por gerar um evento XPLT no Sigma, para quando houver furo de agendamento na Ordem de Serviço.
 * 
 * <br><br>
 * <b> Furo de Agendamento: </b> Possui uma data e horário de agendamento, porém não iniciou ou finalizou o atendimento após o horário.
 * 
 * @author herisson.ferreira
 *
 */
public class RotinaGeraEventoXPLT implements CustomJobAdapter {

	private static final String nomeRotina = "RotinaGeraEventoXPLT";
	private static final String evento = "XPLT";
	
	@Override
	public void execute(CustomJobContext ctx) {
	
		System.out.println("#### INICIO DA ROTINA GERAR EVENTO XPLT executaServicoSegware - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		
		List<EventoRecebido> listaOS = new ArrayList<EventoRecebido>();
		
		try {
			
			String ultimaExecucao = ultimaExecucaoRotina(nomeRotina);
			String execucaoAtual = ultimaExecucaoRotinaRange(nomeRotina);
			listaOS = retornaOs(ultimaExecucao, execucaoAtual);
			
			for(EventoRecebido eventoAux : listaOS) {
				
				try {
					abrirTarefa(eventoAux);
					
					/**
					 * O campo é setado para vazio devido a falta de informação mapeada sobre a estrutura do Evento.
					 * Como há dúvidas sobre a funcionalidade do mesmo, foi acordado não enviar para o evento. Porém o mesmo é necessário para a abertura da tarefa. 
					 */
					eventoAux.setNomePessoa("");
					
					executaServicoSegware(eventoAux);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#### ERRO AO GERAR EVENTO PRA OS: "+eventoAux.getCodigo()+" - BA: "+eventoAux.getIdCentral()+"["+eventoAux.getParticao()+"] - CLIENTE:"+eventoAux.getCdCliente());
				}
				
			}
			
			atualizaUltimaExecucao(nomeRotina);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### ERRO ROTINA GERAR EVENTO XPLT executaServicoSegware - Data: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		} finally {
			System.out.println("#### FIM DA ROTINA GERAR EVENTO XPLT executaServicoSegware - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
		
	}
	
	/**
	 * Responsável por efetuar o processo de abertura de tarefa
	 * 
	 * @param eventoRecebido Objeto da Ordem de Serviço encontrada na Query
	 */
	public void abrirTarefa(EventoRecebido eventoRecebido) {
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		NeoUser solicitante = null;
		NeoUser executor = null;
		
		String papelSolicitante = null;
		String papelExecutor = null;
		
		
		if (eventoRecebido.getIdCentral().substring(0, 1).equals("R")) {
			papelSolicitante = "SolicitanteTarefaXPLTRastreamento";
			papelExecutor = "ExecutorTarefaXPLTRastreamento";
		}
		else {
			papelSolicitante = "SolicitanteTarefaXPLT";
			papelExecutor = "ExecutorTarefaXPLT";
		}
		
		solicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel((NeoPaper)PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", papelSolicitante)));; 
		executor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel((NeoPaper)PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", papelExecutor)));;
	
		
		String titulo = null;
		String tituloAux = null;
		
		titulo = "Técnico: "+eventoRecebido.getNomePessoa()+" - Furo de Agendamento - "+eventoRecebido.getIdCentral()+"["+eventoRecebido.getParticao()+"]";
		tituloAux = "Técnico: "+eventoRecebido.getNomePessoa()+" - Furo de Agendamento%";
		
		String descricao = null;
		
		descricao = retornaDescricaoTarefa(eventoRecebido, tituloAux);
				
		GregorianCalendar prazo = new GregorianCalendar();
		
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);

		if(NeoUtils.safeIsNotNull(solicitante) && NeoUtils.safeIsNotNull(executor))  {
			iniciarTarefaSimples.abrirTarefa(solicitante.getCode(), executor.getCode(), titulo, descricao.replaceAll("\\s\\s+"," "), "1", "sim", prazo);			
		}else {
			enviaEmail(eventoRecebido);
		}
		
	}
	
	/**
	 * Responsável por efetuar a montagem e envio do e-mail
	 * 
	 * @param eventoRecebido Objeto da Ordem de Serviço encontrada na Query
	 */
	private void enviaEmail(EventoRecebido eventoRecebido) {
		
		GregorianCalendar dataBase = new GregorianCalendar();
		Date dataAux = eventoRecebido.getData().getTime();
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		
		String dataExecucao = NeoDateUtils.safeDateFormat(dataBase, "dd/MM/YYYY HH:mm:ss");
		
		List<String> emails = new ArrayList<>();
		emails.add("suportesistemas@orsegups.com.br");
		
		/**
		 * HTML Encoding
		 * 
		 * á = &aacute;
		 * ã = &atilde;
		 * é = &eacute;
		 * ç = &ccedil;
		 */
		StringBuilder msg = new StringBuilder();
		
		msg.append("	<h1>Rotina Gera Evento XPLT</h1>																															");
		msg.append("																																								");
		msg.append("	<p>Verificar os pap&eacute;is SolicitanteTarefaXPLT e ExecutorTarefaXPLT, pois est&atilde;o sem um usu&aacute;rio preenchido ou ativo.</p>					");
		msg.append("	<p>Exce&ccedil;&atilde;o gerada por: "+this.getClass()+" </p>																								");
		msg.append("	<p>Rotina executada em&nbsp;&quot;<strong>"+dataExecucao+"</strong>.&quot;&nbsp;</p>																		");
		msg.append("	<p>Dados do Furo de Agendamento:</p>																														");
		msg.append("																																								");
		msg.append("	<table align=\"left\" border=\"2\" cellpadding=\"1\" cellspacing=\"1\" style=\"width: 500px;\" summary=\"Dados do Furo de Agendamento\">					");
		msg.append("		<tbody>																																					");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Data de Execu&ccedil;&atilde;o da Rotina</th>    																				");
		msg.append("				<td>"+simpleDateFormat.format(dataAux)+"</td>																									");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Conta</th>																													");
		msg.append("				<td>"+eventoRecebido.getIdCentral()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Parti&ccedil;&atilde;o</th>																									");
		msg.append("				<td>"+eventoRecebido.getParticao()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">C&oacute;digo do Cliente (Sigma)</th>																							");
		msg.append("				<td>"+eventoRecebido.getCdCliente()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Nome do T&eacute;cnico</th>																									");
		msg.append("				<td>"+eventoRecebido.getNomePessoa()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Log do Evento</th>																											");
		msg.append("				<td>"+eventoRecebido.getLogEvento()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("		</tbody>																																				");
		msg.append("	</table>																																					");
		msg.append("																																								");
		msg.append("	<p>&nbsp;</p>																																				");
		msg.append("	<p>&nbsp;</p>																																				");
		msg.append("	<p>&nbsp;</p>																																				");
		msg.append("	<p>&nbsp;</p>																																				");

		
		String assunto = "[Rotina Gera Evento XPLT] Verificar Papeis sem usuario";
		
		OrsegupsEmailUtils.sendEmailAutomatic(emails, msg.toString(), assunto);
		
	}

	/**
	 * Monta a descrição da tarefa
	 * 
	 * @param eventoRecebido Objeto da Ordem de Serviço encontrada na Query
	 * @param tituloAux
	 * @return Descrição da Tarefa
	 */
	public String retornaDescricaoTarefa(EventoRecebido eventoRecebido, String tituloAux) {
		
		String razao = retornaRazaoSocial(eventoRecebido.getCdCliente());
		
	    String descricao = getHistoricoTarefaSimples(tituloAux);
	    
	    Date dataAux = eventoRecebido.getData().getTime();
	    
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYY HH:mm:ss");
	    
	    descricao += "<b> Conta </b>: " + eventoRecebido.getIdCentral() + "[" + eventoRecebido.getParticao() + "] " + " <br>";
	    descricao = descricao + "<b> Razão Social </b>: " + (razao.isEmpty() ? "" : razao) + " <br>";
	    descricao = descricao + "<b> Data Recebido </b>: " + simpleDateFormat.format(dataAux) +"<br>";
	    descricao = descricao + "<b> Eventos </b>: " + eventoRecebido.getCodigo() + "<br>";
	    descricao = descricao + "<b> Log do Evento </b>: " + eventoRecebido.getLogEvento() + "<br>";
	
	    return descricao;
	}
	
	/**
	 * Captura a razão social do cliente
	 * 
	 * @param cdCliente Código do Cliente no Sigma
	 * @return Razão Social do Cliente
	 */
	public String retornaRazaoSocial(int cdCliente) {
		
	    Connection conn = null;
	    StringBuilder sql = new StringBuilder();
	    PreparedStatement pstm = null;
	    ResultSet rs = null;
	    
	    String razao = null;
	    
	    try {
			sql.append(" SELECT razao FROM DBCENTRAL ");
			sql.append(" WHERE CD_CLIENTE = ? ");
			
			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setInt(1, cdCliente);
			
			rs = pstm.executeQuery();
			
			while(rs.next()) {
				razao = rs.getString("razao");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
	    
	    return razao;
	}
	
	
	/**
	 * Retornar as Ordens de Serviço 
	 * 
	 * @param ultimaExecucao Última execução da Rotina 
	 * @param execucaoAtual Última execução da Rotina +9 minutos
	 */
	public List<EventoRecebido> retornaOs(String ultimaExecucao, String execucaoAtual) {
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		List<EventoRecebido> listaOS = new ArrayList<EventoRecebido>();
		
		try {
		
			/**
			 * Lista todas as ordens de serviço com agendamento em atraso
			 * 
			 * ATRASO: Já passou o agendamento, porém não há data de início no atendimento.
			 */
			sql.append(" SELECT OS.ID_ORDEM, OS.CD_CLIENTE, DB.ID_EMPRESA, DB.ID_CENTRAL, DB.PARTICAO, DB.RAZAO,OS.DATAAGENDADA, ");
			sql.append(" OS.DEFEITO,COL.NM_COLABORADOR FROM DBORDEM OS ");
			sql.append(" INNER JOIN DBCENTRAL DB ");
			sql.append(" ON DB.CD_CLIENTE = OS.CD_CLIENTE ");
			sql.append(" INNER JOIN COLABORADOR COL ");
			sql.append(" ON OS.ID_INSTALADOR = COL.CD_COLABORADOR ");
			sql.append(" WHERE OS.DATAAGENDADA BETWEEN ? AND ? ");
			sql.append(" AND OS.DATAEXECUTADA IS NULL ");
			sql.append(" AND OS.DT_INICIO_EXECUCAO IS NULL ");
			sql.append(" AND OS.FECHAMENTO IS NULL ");
			sql.append(" AND DB.ID_CENTRAL NOT LIKE 'R%'");
			sql.append(" AND OS.ID_ORDEM NOT IN ");			
			sql.append(" (SELECT ID_ORDEM FROM dbORDEM WHERE IDOSDEFEITO IN (166,1067) AND DEFEITO NOT LIKE '%#XPLT%') ");
			
			conn = PersistEngine.getConnection("SIGMA90");
			
			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, ultimaExecucao);
			pstm.setString(2, execucaoAtual);

			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				String logEvento = "Evento gerado automaticamente devido a furo de agendamento de OS. \n";
				logEvento += "BA: "+rs.getString("ID_CENTRAL")+" - "+rs.getString("RAZAO")+" \n";
				logEvento += "OS: "+rs.getString("ID_ORDEM")+" \n";
				logEvento += "Data do Agendamento: "+rs.getString("DATAAGENDADA")+" \n";
				logEvento += "Técnico Responsável: "+rs.getString("NM_COLABORADOR")+" \n";
				
				EventoRecebido eventoRecebido = new EventoRecebido();

				eventoRecebido.setCodigo(evento);
				eventoRecebido.setData(new GregorianCalendar());
				eventoRecebido.setEmpresa(rs.getLong("ID_EMPRESA"));
				eventoRecebido.setIdCentral(rs.getString("ID_CENTRAL"));
				eventoRecebido.setParticao(rs.getString("PARTICAO"));
				eventoRecebido.setCdCliente(rs.getInt("CD_CLIENTE"));
				eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
				eventoRecebido.setProtocolo(Byte.parseByte("2"));
				eventoRecebido.setLogEvento(logEvento);
				eventoRecebido.setNomePessoa(rs.getString("NM_COLABORADOR"));
				
				listaOS.add(eventoRecebido);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaOS;	
	}
	
	/**
	 * Captura a última execução da rotina
	 * 
	 * @param nomeRotina Nome da rotina a ser executada
	 * @return A data da última execução da rotina
	 */
	public String ultimaExecucaoRotina(String nomeRotina) {

		String retorno = null;
		
		try	{
			
			Collection<NeoObject> monitoraAgendador = null;

			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty()){
				for (NeoObject neoObject : monitoraAgendador){
					EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

					GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

					retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return retorno;

	}

	/**
	 * Define o range máximo para execução da Rotina
	 * 
	 * @param nomeRotina Nome da rotina a ser executada
	 * @return A data da última execução da rotina + 10minutos para criar um range de alcance
	 */
	public String ultimaExecucaoRotinaRange(String nomeRotina) {

		String retorno = null;
		
		try	{
			
			Collection<NeoObject> monitoraAgendador = null;

			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty()){
				for (NeoObject neoObject : monitoraAgendador){
					
					EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

					GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

					GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

					dataFinalAgendadorAux.add(GregorianCalendar.MINUTE, 9);
					dataFinalAgendadorAux.set(GregorianCalendar.SECOND, 59);
					dataFinalAgendadorAux.set(GregorianCalendar.MILLISECOND, 59);

					retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return retorno;
	}
	
	/**
	 * Insere a data da última execução da rotina no Formulário
	 * 
	 * @param nomeRotina Nome da rotina a ser executado
	 */
	public void atualizaUltimaExecucao(String nomeRotina) {
		
		int minutos = 0;
		
		try
		{
			
			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty())
			{
				NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

				EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);
				
				GregorianCalendar dataExecucao = new GregorianCalendar();	
				minutos = dataExecucao.get(GregorianCalendar.MINUTE);
				minutos = (minutos-(minutos%10));
				
				dataExecucao.set(GregorianCalendar.MINUTE, minutos);
				dataExecucao.set(GregorianCalendar.SECOND, 00);
				dataExecucao.set(GregorianCalendar.MILLISECOND, 00);

				monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(dataExecucao);

				PersistEngine.persist(neoObject);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	/**
	 * @param eventoRecebido Objeto da Ordem de Serviço encontrada na Query
	 */
	private String executaServicoSegware(EventoRecebido eventoRecebido)
	{
		String returnFromAccess = null;
		
		try
		{
			ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();

			returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
			
			if (!returnFromAccess.equalsIgnoreCase("ACK")) {
				System.out.println(webServiceProxy.receberEvento(eventoRecebido));
				System.out.println("##### ERRO AO GERAR EVENTO XPLT:  " + eventoRecebido.getIdCentral() + eventoRecebido.getLogEvento() + " Erro: " + returnFromAccess);
/*				String erroEventoXPLT = "##### ERRO AO GERAR EVENTO XPLT:  " + eventoRecebido.getIdCentral() + eventoRecebido.getLogEvento() + " Erro: " + returnFromAccess;
				enviarEmailAvisoErro(erroEventoXPLT);*/
			
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
			System.out.println("##### ERRO ROTINA GERAR EVENTO XPLT executaServicoSegware - Data: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
		return returnFromAccess;
	}
	
    public static String getHistoricoTarefaSimples(String titulo) {
        
    	titulo = titulo.replace("[", "\\[");
    
	StringBuilder tarefas = new StringBuilder();
	StringBuilder retorno = new StringBuilder();

	retorno.append("<strong>Histórico da conta:</strong> ");

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT TOP 5 P.code, P.neoId, P.startDate FROM D_Tarefa T WITH(NOLOCK) ");
	sql.append(" INNER JOIN WFProcess P WITH(NOLOCK) ON P.neoId = T.wfprocess_neoId ");
	sql.append(" WHERE titulo LIKE ? ESCAPE '\\' ");
	sql.append(" AND P.processState != 2 ");
	sql.append(" ORDER BY T.neoId DESC ");

	boolean possuiHistorico = false;

	try {
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setString(1, titulo.trim());

	    rs = pstm.executeQuery();

	    while (rs.next()) {

		possuiHistorico = true;

		String code = rs.getString("code");
		String neoId = rs.getString("neoId");
		Date data = rs.getDate("startDate");
		
		tarefas.append("<tr>");
		tarefas.append("<td>"+NeoDateUtils.safeDateFormat(new Date(data.getTime()), "dd/MM/yyyy")+"</td>");
		tarefas.append("<td><a href=\"https://intranet.orsegups.com.br/fusion/bpm/workflow_search.jsp?id=" + neoId + "\" target=\"_blank\"><img title=\"Pesquisar\" ");
		tarefas.append("src=\"https://intranet.orsegups.com.br/fusion/imagens/icones_final/visualize_blue_16x16-trans.png\" align=\"absmiddle\"> " + code + "</a></td>");
		tarefas.append("</tr>");

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	if (possuiHistorico) {
	    retorno.append("<br>");
	    retorno.append(" <table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\"> ");
	    retorno.append(" <tr style=\"text-align: left;\"> ");
	    retorno.append(" <th>Data</th><th>Tarefa</th>");
	    retorno.append(" </tr> ");
	    retorno.append(tarefas);
	    retorno.append("</table> ");
	}else{        	    
	    retorno.append(" Não possui histórico");
	}

	retorno.append("<br>");

	return retorno.toString();
}
	
    
   /* private void enviarEmailAvisoErro(String erroEventoXPLT) {
		MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
		MultiPartEmail email = new HtmlEmail();
		StringBuilder eMail = new StringBuilder();
		try		{
			
		eMail.append("Prezados(as), \n");
		eMail.append("Os seguintes erros ocorreram ao gerar eventos XPLT. \n");
		eMail.append(erroEventoXPLT);
		email.setSubject("ERRO NA ROTINA XPLT");
		email.addTo("maria.oliveira@orsegups.com.br");
		email.setFrom(settings.getFromEMail(), settings.getFromName());
		email.setSmtpPort(settings.getPort());
		email.setHostName(settings.getSmtpServer());
		email.setMsg(eMail.toString());
		email.send();
		} catch (Exception e)	{
			e.printStackTrace();
		}
	}*/
	
	
}
