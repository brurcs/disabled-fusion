package com.neomind.fusion.custom.orsegups.callcenter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

@WebServlet(name="CallCenterUpdateValuesServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterUpdateValuesServlet"}, asyncSupported=true)
public class CallCenterUpdateValuesServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	private Map<String, String> fieldTableMap = new HashMap<String, String>();

	{
		//Primeiro Parâmetro: banco
		//Segundo Parâmetro: #banco# ; #tabela# ; #campo#; #pk#
		fieldTableMap.put("cnpjCpfSigma", "SIGMA90;dbo.dbcentral;cgccpf;cd_cliente");
		fieldTableMap.put("emailCentral", "SIGMA90;dbo.dbcentral;EMAILRESP;cd_cliente");
		
		fieldTableMap.put("cnpjCpf", "SAPIENS;E085CLI;CGCCPF;codCli");
		fieldTableMap.put("emailCliente", "SAPIENS;E085CLI;INTNET;codCli");
		fieldTableMap.put("emailContrato", "SAPIENS;USU_T160CTR;USU_EMACTR;USU_CODCLI");
		fieldTableMap.put("telefone1", "SAPIENS;E085CLI;FONCLI;codCli");
		fieldTableMap.put("telefone2", "SAPIENS;E085CLI;FONCL2;codCli");
		fieldTableMap.put("telefone3", "SAPIENS;E085CLI;FONCL3;codCli");
		fieldTableMap.put("telefone4", "SAPIENS;E085CLI;FONCL4;codCli");
		fieldTableMap.put("telefone5", "SAPIENS;E085CLI;FONCL5;codCli");
		fieldTableMap.put("foneContrato", "SAPIENS;USU_T160CTR;USU_CLIFON;USU_CODCLI");
		fieldTableMap.put("foneFax", "SAPIENS;E085CLI;FAXCLI;codCli");
		
		fieldTableMap.put("enderecoCobranca", "SAPIENS;E085CLI;ENDCOB;codCli");
		fieldTableMap.put("numeroCobranca", "SAPIENS;E085CLI;NENCOB;codCli");
		fieldTableMap.put("complementoCobranca", "SAPIENS;E085CLI;CPLCOB;codCli");
		fieldTableMap.put("cepCobranca", "SAPIENS;E085CLI;CEPCOB;codCli");
		fieldTableMap.put("cidadeCobranca", "SAPIENS;E085CLI;CIDCOB;codCli");
		fieldTableMap.put("ufCobranca", "SAPIENS;E085CLI;ESTCOB;codCli");
		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@SuppressWarnings("unchecked")
	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException 
	{
		String fieldId = req.getParameter("fieldId");
		String codCli = req.getParameter("codCli");
		String value = req.getParameter("value");
		
		if (!NeoUtils.safeIsNull(fieldId, codCli, value))
		{
			String[] params = fieldTableMap.get(fieldId).split(";");
			String banco = params[0];
			String tabela = params[1];
			String campo = params[2];
			String pk = params[3];
			value = value.toUpperCase();
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE ").append(tabela);
			sql.append(" SET ").append(campo).append(" = :value");
			sql.append(" WHERE ").append(pk).append(" = :codCli ");
			
			Query query = PersistEngine.getEntityManager(banco).createNativeQuery(sql.toString());
			
			try
			{
				query.setParameter("value", value);
				query.setParameter("codCli", codCli);
				query.executeUpdate();
			}
			catch (Exception e)
			{
				//retorna erro na transacao
				resp.getWriter().write("Ocorreu um erro na alteração do valor: " + campo);
				e.printStackTrace();
			}
		}
		else
		{
			//retorna erro por falta de parametros
			resp.getWriter().write("Ocorreu um erro ao modificar pois existem dados incorretos.");
		}
	}
}
