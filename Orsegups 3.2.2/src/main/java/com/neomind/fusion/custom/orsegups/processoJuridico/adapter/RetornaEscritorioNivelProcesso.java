package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoRole;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.RetornaEscritorioNivelProcesso

public class RetornaEscritorioNivelProcesso implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		String grauProcesso = String.valueOf(processEntity.getValue("grauProcesso"));
		
		
		NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
		boolean tributario = false;
        	if (tipoProcesso != null) {
        	    EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
        	    if (Integer.parseInt(wTipoProcesso.getValue("codigo").toString()) == 3)
        		    tributario=true;
        	}
		
		if (tributario) {
		    NeoRole papelJuridico = (NeoRole) PersistEngine.getObject(NeoRole.class, new QLEqualsFilter("code", "j002JuridicoOrsegups"));
		    processEntity.setValue("papelJuridicoOrse",papelJuridico);
		    processEntity.setValue("papelRaiaEscRes", papelJuridico);
		} else {

		    	NeoObject objEscritorio = (NeoObject) processEntity.getValue("escRecPro");
			EntityWrapper wEscritorio = new EntityWrapper(objEscritorio);
		    	
        		if (grauProcesso.contains("1º"))
        		{
        			NeoPaper nivEscResEjurOrs = (NeoPaper) wEscritorio.getValue("papelOrseEEsc");
        			processEntity.setValue("papelRaiaPoolJurEsc", nivEscResEjurOrs);
        		}
        		else if (grauProcesso.contains("2º"))
        		{
        			NeoPaper nivEscResEjurOrsSeguGrau = (NeoPaper) wEscritorio.getValue("papelOrseEEscSegGrau");
        			processEntity.setValue("papelRaiaPoolJurEsc", nivEscResEjurOrsSeguGrau);
        		}
        		else if (grauProcesso.contains("3º"))
        		{
        			NeoPaper nivEscResEjurOrsTeruGrau = (NeoPaper) wEscritorio.getValue("papelOrseEEscTerGrau");
        			processEntity.setValue("papelRaiaPoolJurEsc", nivEscResEjurOrsTeruGrau);
        
        		}
        		else if (grauProcesso.contains("4º"))
        		{
        			NeoPaper nivEscResEjurOrsQuartGrau = (NeoPaper) wEscritorio.getValue("papelOrseEEscQuaGrau");
        			processEntity.setValue("papelRaiaPoolJurEsc", nivEscResEjurOrsQuartGrau);
        
        		}
		}
	    }catch(Exception e){
		System.out.println("Erro na classe RetornaEscritorioNivelProcesso do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
