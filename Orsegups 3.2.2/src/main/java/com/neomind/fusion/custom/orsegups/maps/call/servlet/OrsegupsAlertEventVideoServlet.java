package com.neomind.fusion.custom.orsegups.maps.call.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertEventVideoEngine;
import com.neomind.fusion.custom.orsegups.maps.call.vo.CallAlertEventVideoVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoAcessoClienteVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoHistoricoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.sigma.SigmaUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;


@WebServlet(name = "OrsegupsAlertEventVideoServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet" }, asyncSupported = true)
public class OrsegupsAlertEventVideoServlet extends HttpServlet
{

	private static final Log log = LogFactory.getLog(OrsegupsAlertEventVideoServlet.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{

		resp.setContentType("text/plain");
		resp.setCharacterEncoding("ISO-8859-1");

		final PrintWriter out = resp.getWriter();

		String action = "";

		if (req.getParameter("action") != null)
		{
			action = req.getParameter("action");
		}
		if (action.equalsIgnoreCase("getDadosProvidencia"))
		{
			this.getDadosProvidencia(req, resp, out);
		}
		if (action.equalsIgnoreCase("getAtualizaEventos"))
		{
			this.getAtualizaEventos(req, resp, out);
		}
		if (action.equalsIgnoreCase("getDadosAcessoCliente"))
		{
			this.getDadosAcessoCliente(req, resp, out);
		}
		if (action.equalsIgnoreCase("getLog"))
		{
			this.getLog(req, resp, out);
		}
		if (action.equalsIgnoreCase("getCameras"))
		{
			this.getCameras(req, resp, out);
		}
		if (action.equalsIgnoreCase("getData"))
		{
			this.getData(req, resp, out);
		}
		if (action.equalsIgnoreCase("atualizaHistoricoEventos"))
		{
			String codigoHistorico = req.getParameter("historico");
			String dias = req.getParameter("dias");
			this.atualizaHistoricoEventos(req, resp, out, codigoHistorico, dias);
		}
		if (action.equalsIgnoreCase("atualizaContatos"))
		{
			String cdCliente = req.getParameter("cdCliente");
			String cdHistorico = req.getParameter("cdHistorico");
			this.getDadosProvidencia(req, resp, out, cdCliente, cdHistorico);
		}
		if (action.equalsIgnoreCase("saveLogPopUp"))
		{
			String cdHistorico = req.getParameter("historico");
			String textoLog = req.getParameter("textoLog");
			String nomeCol = req.getParameter("nomeCol");
			this.saveLogPopUp(req, resp, out, cdHistorico, textoLog, nomeCol);
		}
		if (action.equalsIgnoreCase("getCamerasCliente"))
		{
			this.getCamerasCliente(req, resp, out);
		}
		if (action.equalsIgnoreCase("getCamerasClienteLayout"))
		{
			this.getCamerasClienteLayout(req, resp, out);
		}
		
		if (action.equalsIgnoreCase("registrarImagens")){
		    
		    long cdHistorico = Long.valueOf(req.getParameter("cdHistorico"));
		    
		    System.out.println("RATXVIDPRINT Iniciou gravação "+cdHistorico);
		    
		    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		    
		    String data = req.getParameter("data")+" "+req.getParameter("hora");
		    
		    String horas[] = data.split(":");
		    
		    Date date = null;

			try{
				System.out.println(horas.length);
				if (horas.length > 2){
					date = format.parse(data);
				}
				else{
					date = format2.parse(data);
				}

			}
			catch (ParseException e){
				System.out.println("RATXVIDPRINT Erro ao converter data!");
				e.printStackTrace();
			}
		    
		    Calendar calendario = new GregorianCalendar();
		    
		    if (date != null){
		    	calendario.setTime(date);
		    	
		    	this.registrarImagens(cdHistorico, calendario);	    	
		    }
		   
		}
		
	}

	private void registrarImagens(long cdHistorico, Calendar calendario)
	{
		try {
		    InstantiableEntityInfo historico = AdapterUtils.getInstantiableEntityInfo("cmRegistroImagens");
		    NeoObject objHistorico = historico.createNewInstance();
		    EntityWrapper wrapperHistorico = new EntityWrapper(objHistorico);
		    
		    wrapperHistorico.findField("cdHistorico").setValue(cdHistorico);
		    wrapperHistorico.findField("data").setValue(calendario);
		    
		    PersistEngine.persist(objHistorico);
		    System.out.println("RATXVIDPRINT Gravou no e-form "+cdHistorico);
		} catch (Exception e) {
		    System.out.println("RATXVIDPRINT Erro ao gravar no e-form "+cdHistorico);

		    e.printStackTrace();
		}
		
		
	}


	private void getDadosProvidencia(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{
		EventoVO eventoVOAux = null;
		StringBuilder contentString = new StringBuilder();
		String cdCliente = req.getParameter("cdCliente");

		try
		{

			if (cdCliente != null && !cdCliente.isEmpty())
			{
				EventoVO vo = new EventoVO();
				vo.setCodigoCliente(cdCliente);
				SigmaUtils sigmaUtils = new SigmaUtils();
				eventoVOAux = sigmaUtils.getDadosProvidencia(vo);

				contentString.append(" <table class=\"table table-striped table-bordered\"  style=\"width: 100%;max-height:199px; font-family: verdana; font-size: 11px;\"> ");
				contentString.append(" <thead >  ");
				contentString.append(" <tr data-sortable=\"true\" style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;\"> ");
				contentString.append(" <th data-sortable=\"true\">Providência</th> ");
				contentString.append(" <th data-sortable=\"true\">Nome</th> ");
				contentString.append(" <th data-sortable=\"true\">Telefone 1</th> ");
				contentString.append(" <th data-sortable=\"true\">Telefone 2</th> ");
				contentString.append(" <th data-sortable=\"true\">Email</th> ");
				contentString.append(" <th data-sortable=\"true\">Propriedade</th> ");
				contentString.append(" </tr> ");
				contentString.append(" </thead> ");
				contentString.append(" <tbody  style=\"width: 100%;;max-height:99px; font-family: verdana;padding: 2px 2px;font-size: 10px;overflow: auto;\"> ");

				if (eventoVOAux != null && eventoVOAux.getProvidenciaVOs() != null && !eventoVOAux.getProvidenciaVOs().isEmpty())
				{

					for (ProvidenciaVO providenciaVO : eventoVOAux.getProvidenciaVOs())
					{

						contentString.append(" <tr style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;\"> ");
						contentString.append(" <td style=\"white-space: normal\">" + providenciaVO.getCodigoProvidencia() + "</td> ");
						contentString.append(" <td style=\"white-space: normal\">" + providenciaVO.getNome() + "</td> ");
						contentString.append(" <td style=\"white-space: normal\"><span class=\"glyphicon glyphicon-phone-alt\" aria-hidden=\"true\">");
						contentString.append(" <span onclick='javascript:dial(\"0" + providenciaVO.getTelefone1() + "\");' style=\"font-family: verdana; font-size: 11px;font-weight: bold;cursor: pointer;\">  " + providenciaVO.getTelefone1() + " </span></span></td> ");
						contentString.append(" <td style=\"white-space: normal\"><span class=\"glyphicon glyphicon-phone-alt\" aria-hidden=\"true\">");
						contentString.append(" <span onclick='javascript:dial(\"0" + providenciaVO.getTelefone2() + "\");' style=\"font-family: verdana; font-size: 11px;font-weight: bold;cursor: pointer;\">  " + providenciaVO.getTelefone2() + " </span></span></td>  ");
						contentString.append(" <td style=\"white-space: normal; text-align: right\">" + providenciaVO.getEmail() + "</td> ");
						contentString.append(" <td style=\"white-space: normal; text-align: right\">" + providenciaVO.getPrioridade() + "</td> ");
						contentString.append(" </tr> ");

					}

				}

				contentString.append(" </tbody>	");

				contentString.append(" </table> ");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsAlertEventVideoEngine SIGMA PROVIDENCIAS : " + e.getMessage());
		}
		finally
		{
			out.print(contentString.toString());
			out.flush();
			out.close();

		}

	}

	private void getDadosAcessoCliente(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{
		EventoVO eventoVOAux = null;
		StringBuilder contentString = new StringBuilder();
		String cdCliente = req.getParameter("cdCliente");
		try
		{
			if (cdCliente != null && !cdCliente.isEmpty() && !cdCliente.equals("null"))
			{

				EventoVO vo = new EventoVO();
				vo.setCodigoCliente(cdCliente);
				SigmaUtils sigmaUtils = new SigmaUtils();
				eventoVOAux = sigmaUtils.getDadosAcessoCliente(vo);

				contentString.append(" <table class=\"table table-striped table-bordered\" id=\"dgUsuarios\"  style=\"width: 100%;max-height:199px; font-family: verdana; font-size: 11px;\"> ");
				contentString.append(" <thead >  ");
				contentString.append(" <tr data-sortable=\"true\" style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;\"> ");
				contentString.append(" <th data-sortable=\"true\">Código</th> ");
				contentString.append(" <th data-sortable=\"true\">Nome</th> ");
				contentString.append(" <th data-sortable=\"true\">Rg</th> ");
				contentString.append(" <th data-sortable=\"true\">Cpf</th> ");
				contentString.append(" <th data-sortable=\"true\">Senha</th> ");
				contentString.append(" <th data-sortable=\"true\">Telefone</th> ");
				contentString.append(" <th data-sortable=\"true\">Obs</th> ");
				contentString.append(" </tr> ");
				contentString.append(" </thead> ");
				contentString.append(" <tbody  style=\"width: 100%;;max-height:99px; font-family: verdana;padding: 2px 2px;font-size: 10px;overflow: auto;\"> ");

				if (eventoVOAux != null && eventoVOAux.getEventoAcessoClienteVOs() != null && !eventoVOAux.getEventoAcessoClienteVOs().isEmpty())
				{

					for (EventoAcessoClienteVO eventoAcessoClienteVO : eventoVOAux.getEventoAcessoClienteVOs())
					{
						contentString.append(" <tr style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;\"> ");
						contentString.append(" <td style=\"white-space: normal\">" + eventoAcessoClienteVO.getIdAcesso() + "</td> ");
						contentString.append(" <td style=\"white-space: normal\">" + eventoAcessoClienteVO.getNome() + "</td> ");
						contentString.append(" <td style=\"white-space: normal; text-align: right\">" + eventoAcessoClienteVO.getNuRegistroGeral() + "</td> ");
						contentString.append(" <td style=\"white-space: normal; text-align: right\">" + eventoAcessoClienteVO.getNuCpf() + "</td> ");
						contentString.append(" <td style=\"white-space: normal; text-align: right\">" + eventoAcessoClienteVO.getSenha() + "</td> ");
						contentString.append(" <td style=\"white-space: normal\"><span class=\"glyphicon glyphicon-phone-alt\" aria-hidden=\"true\">");
						contentString.append(" <span onclick='javascript:dial(\"0" + eventoAcessoClienteVO.getTelefone() + "\");' style=\"font-family: verdana; font-size: 11px;font-weight: bold;cursor: pointer;\">  " + eventoAcessoClienteVO.getTelefone() + " </span></span></td>  ");
						contentString.append(" <td style=\"white-space: normal; text-align: right\">");
						contentString.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-container=\"body\"");
						contentString.append(" data-toggle=\"popover\" data-container=\"body\" data-placement=\"right\" title=\"Log Evento\"");
						contentString.append(" data-content=\"" + eventoAcessoClienteVO.getObs() + "\"> ");
						contentString.append(" <span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span> ");
						contentString.append(" </button> ");
						contentString.append(" </td> ");
						contentString.append(" </tr> ");
					}

				}
				contentString.append(" </tbody>	");

				contentString.append(" </table> ");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsAlertEventEngine getDadosAcessoCliente : " + e.getMessage());
		}
		finally
		{
			out.print(contentString.toString());
			out.flush();
			out.close();
		}
	}

	private void getAtualizaEventos(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{

		EventoVO eventoVO = null;
		StringBuilder contentString = new StringBuilder();
		try
		{

			OrsegupsAlertEventVideoEngine alertEventServlet = OrsegupsAlertEventVideoEngine.getInstance();
			String cdCliente = req.getParameter("cdCliente");
			String valor = req.getParameter("valor");

			eventoVO = alertEventServlet.getDadosEventosHistorico(cdCliente, valor);

			contentString.append(" <table class=\"table table-bordered table-striped\" id=\"dgHistorico\" style=\"width: 100%;max-height:199px; font-family: verdana; font-size: 11px;overflow:auto;\" disabled=\"disabled\">  ");
			contentString.append(" <thead >  ");
			contentString.append(" <tr data-sortable=\"true\" style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;\"> ");
			contentString.append(" <th data-sortable=\"true\">Recebido</th> ");
			contentString.append(" <th data-sortable=\"true\">Evento</th> ");
			contentString.append(" <th data-sortable=\"true\">Aux</th> ");
			contentString.append(" <th data-sortable=\"true\">Descrição</th> ");
			contentString.append(" <th data-sortable=\"true\">Em Espera</th> ");
			contentString.append(" <th data-sortable=\"true\">Deslocou</th> ");
			contentString.append(" <th data-sortable=\"true\">No Local</th> ");
			contentString.append(" <th data-sortable=\"true\">Fechado</th> ");
			contentString.append(" <th data-sortable=\"true\">Obs</th> ");
			contentString.append(" </tr> ");
			contentString.append(" </thead> ");
			contentString.append(" <tbody  style=\"width: 100%;;max-height:99px; font-family: verdana;padding: 2px 2px;font-size: 10px;overflow: auto;\"> ");

			if (eventoVO != null && eventoVO.getEventoHistoricoVOs() != null && !eventoVO.getEventoHistoricoVOs().isEmpty())
			{

				for (EventoHistoricoVO eventoHistoricoVO : eventoVO.getEventoHistoricoVOs())
				{

					switch (Integer.parseInt(eventoHistoricoVO.getPrioridade()))
					{
						case 1:
							contentString.append(" <tr style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;color:#001739;\"> ");
							break;
						case 2:
							contentString.append(" <tr style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;color:#ff001a;\"> ");
							break;
						case 3:
							contentString.append(" <tr style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;color:#229c07;\"> ");
							break;
						case 4:
							contentString.append(" <tr style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;color:#ff9b02;\"> ");
							break;
						case 5:
							contentString.append(" <tr style=\"width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;color:#0260ff;\"> ");
							break;

					}

					contentString.append(" <td style=\"white-space: normal\">" + eventoHistoricoVO.getDtRecebido() + "</td> ");
					contentString.append(" <td style=\"white-space: normal\">" + eventoHistoricoVO.getCdEvento() + "</td> ");
					contentString.append(" <td style=\"white-space: normal\">" + eventoHistoricoVO.getNuFechamento() + "</td> ");
					contentString.append(" <td style=\"white-space: normal\">" + eventoHistoricoVO.getTipoEvento() + "</td> ");
					contentString.append(" <td style=\"white-space: normal\">" + eventoHistoricoVO.getDtEspera() + "</td> ");
					contentString.append(" <td style=\"white-space: normal\">" + eventoHistoricoVO.getDtVtrDeslocamento() + "</td> ");
					contentString.append(" <td style=\"white-space: normal\">" + eventoHistoricoVO.getDtVtrLocal() + "</td> ");
					contentString.append(" <td style=\"white-space: normal; text-align: right\">" + eventoHistoricoVO.getDtFechamento() + "</td> ");
					contentString.append(" <td style=\"white-space: normal; text-align: right\">");
					contentString.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-container=\"body\"");
					contentString.append(" data-toggle=\"popover\" data-container=\"body\" data-placement=\"right\" title=\"Log Evento\"");
					contentString.append(" data-content=\"" + eventoHistoricoVO.getObsFechamento().replaceAll("###", "\r\n") + "\"> ");
					contentString.append(" <span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span> ");
					contentString.append(" </button> ");
					contentString.append(" </td> ");
					contentString.append(" </tr> ");

				}

				contentString.append(" </tbody>	");

				contentString.append(" </table> ");

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		out.print(contentString.toString());
		out.flush();
		out.close();
	}

	private void getLog(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{

		String texto = req.getParameter("texto");
		String cdHistorico = req.getParameter("cdHistorico");
		String cdCliente = req.getParameter("cdCliente");
		int result = 0;

		try
		{

			OrsegupsAlertEventVideoEngine.getInstance().cadastrarEvetLog(cdHistorico, cdCliente, texto);

			result = 1;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			out.print(result);
			out.flush();
			out.close();
		}

	}

	private void getCameras(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{

		String idCamera = req.getParameter("idCamera");
		String data = req.getParameter("data");
		String hora = req.getParameter("hora");
		
		int countVideos = 0;
		StringBuilder contentString = new StringBuilder();
		try
		{
			if (idCamera != null && !idCamera.isEmpty() && data != null && !data.isEmpty() && hora != null && !hora.isEmpty())
			{

				if (countVideos == 0)
				{

					contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\"   >  ");
					contentString.append(" <img style=\"width: 100%;height:100%;\" class=\"embed-responsive-item\"   src=\"" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/seventh/mjpegStreamDGuardImagem.jsp?camera=" + idCamera + "&data=" + data + "&hora=" + hora + "&qualidade=80&formato=jpg\"   id=\"" + idCamera + "\" name=\"" + idCamera + "\"   alt=\"" + idCamera + "\"></img>");

					contentString.append(" </div> ");
					contentString.append(" <div align=\"left\" style=\"padding: 0px;margin:0px;\"> ");
					contentString.append(" <button type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btnPlayStop\"  style=\"margin: 0px;\"> ");
					contentString.append(" <span class=\"glyphicon glyphicon-play\"  onclick=\"playAllCam()\" style=\"margin: 0px;\"></span></button> ");
					contentString.append(" <button type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btnSpeed\"  style=\"margin: 0px;\"> ");
					contentString.append(" <span class=\"glyphicon glyphicon-fast-forward\"  onclick=\"speedCam()\" style=\"margin: 0px;\"></span></button> ");
					contentString.append(" <input type=\"number\" min=\"1\" max=\"3\" value=\"2\" id=\"numSpeed\" style=\"width:30px;\" > </input> ");
					contentString.append(" <input type=\"radio\" title=\"-3min\" name=\"optradio\" onclick=\"playAllCamTime('-180')\">  ");
					contentString.append(" <input type=\"radio\" title=\"-2min\" name=\"optradio\" onclick=\"playAllCamTime('-120')\">  ");
					contentString.append(" <input type=\"radio\" title=\"-1min\" name=\"optradio\" onclick=\"playAllCamTime('-60')\">  ");
					contentString.append(" <input type=\"radio\" title=\"-30s\" name=\"optradio\" onclick=\"playAllCamTime('-30')\"> ");
					contentString.append(" <input type=\"radio\" title=\"-20s\" name=\"optradio\" onclick=\"playAllCamTime('-20')\">  ");
					contentString.append(" <input type=\"radio\" title=\"-10s\" name=\"optradio\" onclick=\"playAllCamTime('-10')\">  ");
					contentString.append(" <input type=\"radio\" title=\"-5s\" name=\"optradio\" onclick=\"playAllCamTime('-5')\">  ");
					contentString.append(" <input type=\"radio\" title=\"Retornar ao início\" name=\"optradio\" onclick=\"playAllCamTime('0')\"> ");
					contentString.append(" <input type=\"radio\" title=\"5s\" name=\"optradio\" onclick=\"playAllCamTime('5')\">  ");
					contentString.append(" <input type=\"radio\" title=\"10s\" name=\"optradio\" onclick=\"playAllCamTime('10')\">  ");
					contentString.append(" <input type=\"radio\" title=\"20s\" name=\"optradio\" onclick=\"playAllCamTime('20')\">   ");
					contentString.append(" <input type=\"radio\" title=\"30s\" name=\"optradio\" onclick=\"playAllCamTime('30')\">   ");
					contentString.append(" <input type=\"radio\" title=\"1min\" name=\"optradio\" onclick=\"playAllCamTime('60')\">  ");
					contentString.append(" <input type=\"radio\" title=\"2min\" name=\"optradio\" onclick=\"playAllCamTime('120')\">  ");
					contentString.append(" <input type=\"radio\" title=\"3min\" name=\"optradio\" onclick=\"playAllCamTime('180')\">  ");
					contentString.append(" <button type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btnLive\" title=\"Imagem ao vivo\" style=\"margin: 0px;\"> ");
					contentString.append(" <span class=\"glyphicon glyphicon-expand\"  onclick=\"searchCamLive()\" style=\"margin: 0px;\"></span></button> ");
					contentString.append(" </div>  ");
					contentString.append(" <span class=\"duration\" id=\"sessionDiv\">00:00</span>  ");

				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO EVENTO VIDEO CAMERAS: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			out.print(contentString.toString());
			out.flush();
			out.close();
		}

	}

	private void getData(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{

		String data = req.getParameter("data");
		String minutos = req.getParameter("minutos");

		StringBuilder contentString = new StringBuilder();
		try
		{
			if (data != null && !data.isEmpty() && !data.contains("null") && minutos != null && !minutos.isEmpty() && !minutos.contains("null"))
			{

				minutos = minutos.trim();

				GregorianCalendar calendar = new GregorianCalendar();

				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date = (java.util.Date) formatter.parse(data);
				calendar.setTime(date);
				System.out.println(data);
				if (minutos.contains("-"))
				{
					minutos = minutos.replaceAll("-", "");
					calendar.add(Calendar.SECOND, -Integer.parseInt(minutos));
				}
				else
				{

					calendar.add(Calendar.SECOND, Integer.parseInt(minutos));
				}

				contentString.append(NeoDateUtils.safeDateFormat(calendar, "dd-MM-yyyy HH-mm-ss"));

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO EVENTO VIDEO CAMERAS MODIFICAR DATA: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			out.print(contentString.toString());
			out.flush();
			out.close();
		}

	}

	public void atualizaHistoricoEventos(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String codigoHistorico, String dias)
	{

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		EventoVO vo = null;

		try
		{

			if ((codigoHistorico != null))
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT H.DT_RECEBIDO,H.CD_EVENTO,HF.NM_FRASE_EVENTO,H.DT_ESPERA,H.DT_VIATURA_DESLOCAMENTO,H.DT_VIATURA_NO_LOCAL,H.DT_FECHAMENTO, H.NU_AUXILIAR, H.TX_OBSERVACAO_FECHAMENTO, H.NU_PRIORIDADE");
				sql.append(" FROM VIEW_HISTORICO H WITH (NOLOCK)    LEFT JOIN HISTORICO_FRASE_EVENTO HF WITH (NOLOCK)  ON HF.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO");
				sql.append(" WHERE H.CD_CLIENTE = ? AND H.DT_RECEBIDO BETWEEN GETDATE()- ? AND GETDATE() ORDER BY H.DT_RECEBIDO DESC ");

				vo = new EventoVO();

				if (dias == null)
					dias = "1";
				statement = connection.prepareStatement(sql.toString());
				statement.setString(1, codigoHistorico);
				statement.setInt(2, Integer.parseInt(dias));
				resultSet = statement.executeQuery();

				EventoHistoricoVO eventoVO = null;
				while (resultSet.next())
				{

					eventoVO = new EventoHistoricoVO();
					String dtRecebido = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:ss");
					String cdEvento = resultSet.getString("CD_EVENTO");
					String tipoEvento = resultSet.getString("NM_FRASE_EVENTO");
					String dtEspera = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_ESPERA"), "dd/MM/yyyy HH:mm:ss");
					String dtVtrDeslocamento = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_VIATURA_DESLOCAMENTO"), "dd/MM/yyyy HH:mm:ss");
					String dtVtrLocal = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_VIATURA_NO_LOCAL"), "dd/MM/yyyy HH:mm:ss");
					String dtFechamento = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_FECHAMENTO"), "dd/MM/yyyy HH:mm:ss");
					String nuFechamento = resultSet.getString("NU_AUXILIAR");
					String obsFechamento = resultSet.getString("TX_OBSERVACAO_FECHAMENTO");
					String prioridade = resultSet.getString("NU_PRIORIDADE");

					if (obsFechamento == null || obsFechamento.isEmpty())
					{
						obsFechamento = "Vazio";
					}
					else
					{
						//obsFechamento = "<li>" + obsFechamento;
						obsFechamento = obsFechamento.replace("\n", "<br><li>");
					}

					if (NeoUtils.safeIsNotNull(dtRecebido))
						eventoVO.setDtRecebido(dtRecebido);
					else
						eventoVO.setDtRecebido("");
					if (NeoUtils.safeIsNotNull(cdEvento))
						eventoVO.setCdEvento(cdEvento);
					else
						eventoVO.setCdEvento("");
					if (NeoUtils.safeIsNotNull(tipoEvento))
						eventoVO.setTipoEvento(tipoEvento);
					else
						eventoVO.setTipoEvento("");
					if (NeoUtils.safeIsNotNull(dtEspera))
						eventoVO.setDtEspera(dtEspera);
					else
						eventoVO.setDtEspera("");
					if (NeoUtils.safeIsNotNull(dtVtrDeslocamento))
						eventoVO.setDtVtrDeslocamento(dtVtrDeslocamento);
					else
						eventoVO.setDtVtrDeslocamento("");
					if (NeoUtils.safeIsNotNull(dtVtrLocal))
						eventoVO.setDtVtrLocal(dtVtrLocal);
					else
						eventoVO.setDtVtrLocal("");
					if (NeoUtils.safeIsNotNull(dtFechamento))
						eventoVO.setDtFechamento(dtFechamento);
					else
						eventoVO.setDtFechamento("");
					if (NeoUtils.safeIsNotNull(nuFechamento))
						eventoVO.setNuFechamento(nuFechamento);
					else
						eventoVO.setNuFechamento("");
					if (NeoUtils.safeIsNotNull(obsFechamento))
						eventoVO.setObsFechamento(obsFechamento);
					else
						eventoVO.setObsFechamento("");
					if (NeoUtils.safeIsNotNull(prioridade))
						eventoVO.setPrioridade(prioridade);
					else
						eventoVO.setPrioridade("");

					vo.addEventoHistorico(eventoVO);

				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO SIGMA EVENTO HISTORICO : " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			Gson gson = new Gson();
			String eventosJSON = gson.toJson(vo.getEventoHistoricoVOs());
			out.print(eventosJSON);
		}

	}

	private void getDadosProvidencia(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdCliente, String cdHistorico)
	{

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		List<ProvidenciaVO> list = new ArrayList<ProvidenciaVO>();

		try
		{

			if (cdCliente != null)
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT");
				sql.append("	PROV.CD_CLIENTE,");
				sql.append("	CD_PROVIDENCIA,");
				sql.append("	PROV.NOME,");
				sql.append("	PROV.FONE1,");
				sql.append("	PROV.FONE2,");
				sql.append("	PROV.EMAIL,");
				sql.append("	PROV.NU_PRIORIDADE");
				sql.append(" FROM");
				sql.append("	dbo.dbPROVIDENCIA AS PROV WITH (NOLOCK)");
				sql.append("	INNER JOIN dbo.dbCENTRAL as CEN WITH (NOLOCK)  ON CEN.CD_CLIENTE = PROV.CD_CLIENTE");
				sql.append(" WHERE ");
				sql.append("	CEN.CD_CLIENTE = ?");
				sql.append(" ORDER BY PROV.NU_PRIORIDADE_NIVEL2");

				statement = connection.prepareStatement(sql.toString());
				statement.setString(1, cdCliente);
				resultSet = statement.executeQuery();

				ProvidenciaVO providenciaVO = null;
				
				while (resultSet.next())
				{

					providenciaVO = new ProvidenciaVO();
					String cliente = resultSet.getString("CD_CLIENTE");
					String providencia = resultSet.getString("CD_PROVIDENCIA");
					String nome = resultSet.getString("NOME");
					String telefone1 = resultSet.getString("FONE1");
					String telefone2 = resultSet.getString("FONE2");
					String email = resultSet.getString("EMAIL");
					String prioridade = resultSet.getString("NU_PRIORIDADE");
					System.out.println(cliente + " - " + providencia + " - " + nome + " -  " + telefone1 + " - " + telefone2 + " - " + email);
					if (NeoUtils.safeIsNotNull(cliente))
					{
						providenciaVO.setCodigoCliente(Integer.parseInt(cliente));
					}
					else
					{
						providenciaVO.setCodigoCliente(0);
					}

					if (NeoUtils.safeIsNotNull(providencia))
					{
						providenciaVO.setCodigoProvidencia(Integer.parseInt(providencia));
					}
					else
					{
						providenciaVO.setCodigoProvidencia(0);
					}

					if (NeoUtils.safeIsNotNull(nome))
					{
						providenciaVO.setNome(nome);
					}
					else
					{
						providenciaVO.setCodigoCliente(0);
					}

					if (NeoUtils.safeIsNotNull(telefone1) && !telefone1.isEmpty())
					{
						telefone1 = telefone1.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone1 = telefone1.replace(" ", "");
						providenciaVO.setTelefone1("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone1 + "\"," + cdHistorico + ")'>" + telefone1 + "</a>");

						//providenciaVO.setTelefone1(telefone1);
					}
					else
					{
						providenciaVO.setTelefone1("Vazio");
					}
					if (NeoUtils.safeIsNotNull(telefone2) && !telefone2.isEmpty())
					{
						telefone2 = telefone2.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone2 = telefone2.replace(" ", "");
						providenciaVO.setTelefone2("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone2 + "\"," + cdHistorico + ")'>" + telefone2 + "</a>");
						//providenciaVO.setTelefone2(telefone2);
					}
					else
					{
						providenciaVO.setTelefone2("Vazio");
					}
					if (NeoUtils.safeIsNotNull(email))
					{
						providenciaVO.setEmail(email);
					}
					else
					{
						providenciaVO.setEmail("Vazio");
					}
					if (NeoUtils.safeIsNotNull(prioridade))
					{
						providenciaVO.setPrioridade(Integer.parseInt(prioridade));
					}
					else
					{
						providenciaVO.setPrioridade(0);
					}
					list.add(providenciaVO);

				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO SIGMA PROVIDENCIAS : " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			Gson gson = new Gson();
			String eventosJSON = gson.toJson(list);
			out.print(eventosJSON);

		}

	}

	private void saveLogPopUp(HttpServletRequest request, HttpServletResponse response, PrintWriter out, String cdHistorico, String textoLog, String nomeCol)
	{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String texto = null;
		try
		{
			texto = textoLog;

			conn = PersistEngine.getConnection("SIGMA90");
			StringTokenizer token = new StringTokenizer(texto, ";");
			conn.setAutoCommit(false);
			while (token.hasMoreTokens())
			{
				String textoLocal = "";

				//				if(token.nextToken() != null && !token.nextToken().contains("Op") )
				//				 textoLocal = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + " - Op. CM -" + PortalUtil.getCurrentUser().getFullName() + " : " + token.nextToken();
				//				
				textoLocal = token.nextToken();
				if (textoLocal != null && textoLocal.contains(":"))
				{

					String array[] = textoLocal.split(":");
					if (array != null && array.length > 0 && !array[1].contains("  ") || !array[1].isEmpty())
					{
						StringBuilder sqlUpdate = new StringBuilder();
						sqlUpdate.append(" UPDATE HISTORICO  SET  TX_OBSERVACAO_FECHAMENTO = ? + (CHAR(10) + CHAR(13)) +  TX_OBSERVACAO_FECHAMENTO    WHERE CD_HISTORICO = ?");
						st = conn.prepareStatement(sqlUpdate.toString());
						st.setString(1, textoLocal);
						st.setString(2, cdHistorico);
						st.executeUpdate();
					}

				}
				else if (textoLocal != null && !textoLocal.isEmpty())
				{
					String textoAux = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + " - Op. CM - " + nomeCol + " : " + textoLocal;
					StringBuilder sqlUpdate = new StringBuilder();
					sqlUpdate.append(" UPDATE HISTORICO  SET  TX_OBSERVACAO_FECHAMENTO = ? + (CHAR(10) + CHAR(13)) +  TX_OBSERVACAO_FECHAMENTO    WHERE CD_HISTORICO = ?");
					st = conn.prepareStatement(sqlUpdate.toString());
					st.setString(1, textoAux);
					st.setString(2, cdHistorico);
					st.executeUpdate();
				}
			}
			conn.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

	}

	private void getCamerasCliente(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{

		String idCamera = req.getParameter("idCamera");
		String idCentral = req.getParameter("idCentral");
		String empresa = req.getParameter("idEmpresa");
		String data = req.getParameter("data");
		String hora = req.getParameter("hora");
		
		String historico = req.getParameter("historico");
		int countVideos = 0;
		StringBuilder contentString = new StringBuilder();
		try
		{
			if (idCamera != null && !idCamera.isEmpty() && idCentral != null && !idCentral.isEmpty() && empresa != null && !empresa.isEmpty() && data != null && !data.isEmpty() && hora != null && !hora.isEmpty() && historico != null && !historico.isEmpty() && OrsegupsAlertEventVideoEngine.callingAlertEventVO != null && OrsegupsAlertEventVideoEngine.callingAlertEventVO.containsKey(historico))
			{
				CallAlertEventVideoVO callingAlertEventVO = OrsegupsAlertEventVideoEngine.callingAlertEventVO.get(historico);
				
				EventoVO eventoVO = callingAlertEventVO.getCallEventoVO();
				
				if (callingAlertEventVO != null)
				{
					TreeMap<String, String> treeMap = OrsegupsAlertEventVideoEngine.getInstance().getCameraCliente(idCentral, empresa, eventoVO);
					List<String> cameras = new ArrayList<String>();
					if (treeMap != null && !treeMap.isEmpty())
					{
						for (Entry<String, String> entry : treeMap.entrySet())
						{
							String camera = "";
							if (entry != null)
							{
								camera = String.valueOf(entry.getKey());
								String descricaoCamera = entry.getValue();
								if (callingAlertEventVO.getCallEventoVO().getCdCode() != null && callingAlertEventVO.getCallEventoVO().getCdCode().equals("ACP") && descricaoCamera != null && !descricaoCamera.contains("[VIP]"))
								{
									cameras.add(camera);
								}
								else if (callingAlertEventVO.getCallEventoVO().getCdCode() != null && callingAlertEventVO.getCallEventoVO().getCdCode().equals("ACV"))
								{
									if (descricaoCamera != null && descricaoCamera.contains("[VIP]"))
									{
										cameras.add(0, camera);
									}
									else
									{
										cameras.add(camera);
									}
								}else if (callingAlertEventVO.getCallEventoVO().getCdCode() != null && callingAlertEventVO.getCallEventoVO().getCdCode().equals("MPI"))	{
									if (descricaoCamera != null && descricaoCamera.contains("[VIP]")){
										cameras.add(0, camera);
									}
									else{
									cameras.add(camera);
									}
								}
							}
						}
					}
					if (cameras != null && !cameras.isEmpty())
					{

						contentString.append(" <ul class=\"list-unstyled video-list-thumbs row\" style=\"width: 100%;\"> ");

						String priemiraCamera = idCamera;
						String segundaCamera = cameras.get(1);
						cameras.remove(priemiraCamera);
						cameras.remove(segundaCamera);
						cameras.add(0, priemiraCamera);
						cameras.add(1, segundaCamera);
						if (cameras != null && !cameras.isEmpty())
						{
							for (String camera : cameras)
							{

								if (countVideos == 0)
								{
									camera = idCamera;
									contentString.append(" <li class=\"col-xs-8 col-sm-6\" style=\"padding: 0px;margin:0px;\"> ");
									contentString.append(" <a href=\"#\" id=\"videoPrincipal\" > ");
									contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\"   >  ");
									contentString.append(" <img style=\"width: 100%;height:100%;\" class=\"embed-responsive-item\"  id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");
									contentString.append(" </div> ");
									contentString.append(" <span class=\"duration\" id=\"sessionDiv\" style=\"visibility:collapse\">00:00</span>  ");
									contentString.append(" </a> ");
									contentString.append(" </li> ");

								}
								else if (countVideos == 1)
								{

									contentString.append(" <li class=\"col-xs-8 col-sm-6\" style=\"padding: 0px;margin:0px;\"> ");
									contentString.append(" <a href=\"#\" title=\"Monitoramento de imagens\"> ");
									contentString.append(" <div id=\"videoPrincipal\"  class=\"embed-responsive embed-responsive-16by9\"   >  ");
									contentString.append(" <img style=\"width: 100%;height:100%;\" class=\"embed-responsive-item\"   id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");
									contentString.append(" </div> ");
									contentString.append(" </a> ");
									contentString.append(" </li> ");
								}

								if (countVideos >= 2 && countVideos < 4)
								{
									contentString.append(" <li class=\"col-xs-8 col-sm-6\" style=\"padding: 0px;margin:0px;\">  ");
									contentString.append(" <a href=\"#\" title=\"Monitoramento de imagens\">  ");
									contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\">  ");
									contentString.append(" <img  class=\"embed-responsive-item\" onClick=\"javascrit:trocarCamerasCliente('" + idCentral + "','" + empresa + "','" + camera + "')\"  id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");

									contentString.append(" </div> ");
									contentString.append(" <!-- <h2>Monitoramento de imagens</h2> --> ");
									contentString.append(" </a> ");
									contentString.append(" </li> ");
								}
								else if (countVideos >= 4)
								{
									contentString.append(" <li class=\"col-lg-3 col-md-4 col-sm-4 col-xs-6\" style=\"padding: 0px;margin:0px;\">  ");
									contentString.append(" <a href=\"#\" title=\"Monitoramento de imagens\">  ");
									contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\">  ");
									contentString.append(" <img  class=\"embed-responsive-item\" onClick=\"javascrit:trocarCamerasCliente('" + idCentral + "','" + empresa + "','" + camera + "')\"  id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");

									contentString.append(" </div> ");
									contentString.append(" <!-- <h2>Monitoramento de imagens</h2> --> ");
									contentString.append(" </a> ");
									contentString.append(" </li> ");
								}

								countVideos++;
							}

							contentString.append(" </ul> ");
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			out.print(contentString.toString());
			out.flush();
			out.close();
		}

	}

	private void getCamerasClienteLayout(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{

		String idCamera = req.getParameter("idCamera");
		String idCentral = req.getParameter("idCentral");
		String empresa = req.getParameter("idEmpresa");
		String data = req.getParameter("data");
		String hora = req.getParameter("hora");
		String layout = req.getParameter("layout");
		String historico = req.getParameter("historico");
		int countVideos = 0;
		StringBuilder contentString = new StringBuilder();
		try
		{
			if (idCamera != null && !idCamera.isEmpty() && idCentral != null && !idCentral.isEmpty() && empresa != null && !empresa.isEmpty() && data != null && !data.isEmpty() && hora != null && !historico.isEmpty() && hora != null && !historico.isEmpty())
			{

				if (layout != null && !layout.isEmpty())
				{
					switch (Integer.parseInt(layout))
					{
						case 1:
							layout = "col-xs-12 col-sm-6 col-lg-8";
							break;
						case 2:
							layout = "col-xs-8 col-sm-6";
							break;
						case 3:
							layout = "col-xs-6 col-md-4";
							break;
						case 4:
							layout = "col-lg-3 col-md-4 col-xs-6";
							break;

						default:
							break;
					}

					CallAlertEventVideoVO callingAlertEventVO = OrsegupsAlertEventVideoEngine.callingAlertEventVO.get(historico);
					if (callingAlertEventVO != null)
					{	
					    	EventoVO eventoVO = callingAlertEventVO.getCallEventoVO();
						TreeMap<String, String> treeMap = OrsegupsAlertEventVideoEngine.getInstance().getCameraCliente(idCentral, empresa, eventoVO);
						List<String> cameras = new ArrayList<String>();
						if (treeMap != null && !treeMap.isEmpty())
						{
							for (Entry<String, String> entry : treeMap.entrySet())
							{
								String camera = "";
								if (entry != null)
								{
									camera = String.valueOf(entry.getKey());
									String descricaoCamera = entry.getValue();
									if (callingAlertEventVO.getCallEventoVO().getCdCode() != null && callingAlertEventVO.getCallEventoVO().getCdCode().equals("ACP") && descricaoCamera != null && !descricaoCamera.contains("[VIP]"))
									{
										cameras.add(camera);
									}
									else if (callingAlertEventVO.getCallEventoVO().getCdCode() != null && callingAlertEventVO.getCallEventoVO().getCdCode().equals("ACV"))
									{
										if (descricaoCamera != null && descricaoCamera.contains("[VIP]"))
										{
											cameras.add(0, camera);
										}
										else
										{
											cameras.add(camera);
										}
									}else if (callingAlertEventVO.getCallEventoVO().getCdCode() != null && (callingAlertEventVO.getCallEventoVO().getCodigoEvento().equalsIgnoreCase("XPLE") 
										||  callingAlertEventVO.getCallEventoVO().getCodigoEvento().equalsIgnoreCase("XPE2"))){
									    cameras.add(camera);
									}else if (callingAlertEventVO.getCallEventoVO().getCdCode() != null && callingAlertEventVO.getCallEventoVO().getCdCode().equals("MPI"))	{
										if (descricaoCamera != null && descricaoCamera.contains("[VIP]")){
											cameras.add(0, camera);
										}
										else{
										cameras.add(camera);
										}
									}
								}
							}
						}

						if (cameras != null && !cameras.isEmpty())
						{

							contentString.append(" <ul class=\"list-unstyled video-list-thumbs row\" style=\"width: 100%;\"> ");

							String priemiraCamera = idCamera;
							String segundaCamera = cameras.get(1);
							cameras.remove(priemiraCamera);
							cameras.remove(segundaCamera);
							cameras.add(0, priemiraCamera);
							cameras.add(1, segundaCamera);
							if (cameras != null && !cameras.isEmpty())
							{
								for (String camera : cameras)
								{

									if (countVideos == 0)
									{
										camera = idCamera;
										if (layout.equals("col-xs-12 col-sm-6 col-lg-8"))
											contentString.append(" <li class=\"" + layout + "\" style=\"padding: 0px;margin:0px;width: 100%;\"> ");
										else
											contentString.append(" <li class=\"" + layout + "\" style=\"padding: 0px;margin:0px;\"> ");
										contentString.append(" <a href=\"#\" id=\"videoPrincipal\" > ");
										contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\"   >  ");
										contentString.append(" <img style=\"width: 100%;height:100%;\" class=\"embed-responsive-item\"  id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");
										contentString.append(" </div> ");
										contentString.append(" <span class=\"duration\" id=\"sessionDiv\" style=\"visibility:collapse\">00:00</span>  ");
										contentString.append(" </a> ");
										contentString.append(" </li> ");

									}
									else if (countVideos == 1)
									{

										if (layout.equals("col-xs-12 col-sm-6 col-lg-8"))
											contentString.append(" <li class=\"" + layout + "\" style=\"padding: 0px;margin:0px;width: 100%;\"> ");
										else
											contentString.append(" <li class=\"" + layout + "\" style=\"padding: 0px;margin:0px;\"> ");
										contentString.append(" <a href=\"#\" title=\"Monitoramento de imagens\"> ");
										contentString.append(" <div id=\"videoPrincipal\"  class=\"embed-responsive embed-responsive-16by9\"   >  ");
										contentString.append(" <img style=\"width: 100%;height:100%;\" class=\"embed-responsive-item\"   id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");
										contentString.append(" </div> ");
										contentString.append(" </a> ");
										contentString.append(" </li> ");
									}

									if (countVideos >= 2 && countVideos < 4)
									{
										if (layout.equals("col-xs-12 col-sm-6 col-lg-8"))
											contentString.append(" <li class=\"" + layout + "\" style=\"padding: 0px;margin:0px;width: 100%;\"> ");
										else
											contentString.append(" <li class=\"" + layout + "\" style=\"padding: 0px;margin:0px;\"> ");
										contentString.append(" <a href=\"#\" title=\"Monitoramento de imagens\">  ");
										contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\">  ");
										contentString.append(" <img  class=\"embed-responsive-item\" onClick=\"javascrit:trocarCamerasCliente('" + idCentral + "','" + empresa + "','" + camera + "')\"  id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");

										contentString.append(" </div> ");
										contentString.append(" <!-- <h2>Monitoramento de imagens</h2> --> ");
										contentString.append(" </a> ");
										contentString.append(" </li> ");
									}
									else if (countVideos >= 4)
									{
										if (layout.equals("col-xs-12 col-sm-6 col-lg-8"))
											contentString.append(" <li class=\"" + layout + "\" style=\"padding: 0px;margin:0px;width: 100%;\"> ");
										else
											contentString.append(" <li class=\"" + layout + "\" style=\"padding: 0px;margin:0px;\"> ");
										contentString.append(" <a href=\"#\" title=\"Monitoramento de imagens\">  ");
										contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\">  ");
										contentString.append(" <img  class=\"embed-responsive-item\" onClick=\"javascrit:trocarCamerasCliente('" + idCentral + "','" + empresa + "','" + camera + "')\"  id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");

										contentString.append(" </div> ");
										contentString.append(" <!-- <h2>Monitoramento de imagens</h2> --> ");
										contentString.append(" </a> ");
										contentString.append(" </li> ");
									}

									countVideos++;
								}

								contentString.append(" </ul> ");
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			out.print(contentString.toString());
			out.flush();
			out.close();
		}

	}
}
