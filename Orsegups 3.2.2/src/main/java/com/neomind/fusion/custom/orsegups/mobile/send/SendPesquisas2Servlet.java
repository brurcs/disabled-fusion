package com.neomind.fusion.custom.orsegups.mobile.send;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.vo2.AssuntoVO;
import com.neomind.fusion.custom.orsegups.mobile.vo2.PerguntaVO;
import com.neomind.fusion.custom.orsegups.mobile.vo2.PesquisaVO;
import com.neomind.fusion.custom.orsegups.mobile.vo2.inspetoriaPerguntasVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name="SendPesquisas2Servlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.mobile.send.SendPesquisas2Servlet"})
public class SendPesquisas2Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public String pesquisa(HttpServletRequest request) {
		
		
		String regional = request.getParameter("siglaRegional");
		
		System.out.println("Sigla da Regional a ser filtrada: " + regional);
		
		QLGroupFilter gpRegional = new QLGroupFilter("AND");
		gpRegional.addFilter(new QLEqualsFilter("sigla",regional));
		
		List<NeoObject> lstObjPesqReg = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("IMREGIONAL"),gpRegional);
		
		Long neoIdRegional = 0L;
		if (lstObjPesqReg != null && lstObjPesqReg.size() > 0 ){
			NeoObject oReg = lstObjPesqReg.get(0);
			EntityWrapper wReg = new EntityWrapper(oReg);
			neoIdRegional = NeoUtils.safeLong( NeoUtils.safeOutputString( wReg.findValue("neoId") ) );
			//System.out.println("Regional Encontrada:" +wReg.findValue("nome") + "-" + wReg.findValue("codigo"));
		}
		QLGroupFilter gpf = new QLGroupFilter("AND" );
		gpf.addFilter(new QLRawFilter("regional_neoId=" + neoIdRegional));
		
		List<NeoObject> listPesquisas = PersistEngine.getObjects(AdapterUtils.getInstantiableEntityInfo("Pesquisa").getEntityClass(),gpf);
		
		String json = null;
		
		inspetoriaPerguntasVO inspetoriaPerguntasVO = new inspetoriaPerguntasVO();
		
		ArrayList<PesquisaVO> pesquisas = new ArrayList<PesquisaVO>();
		for (NeoObject pesquisaQuery : listPesquisas) {
			PesquisaVO pesquisaVO = new PesquisaVO();
			EntityWrapper wrapperPesq = new EntityWrapper(pesquisaQuery);
			
			pesquisaVO.setNeoId( NeoUtils.safeOutputString(wrapperPesq.getValue("neoId")) );
			pesquisaVO.setTxtPesquisa(((String) wrapperPesq.getValue("pesquisatxt")).replace("'", "''"));

			List<NeoObject> newObjAssun = ((List<NeoObject>) wrapperPesq.findValue("neoidAssunto"));
			
			ArrayList<AssuntoVO> assuntos = new ArrayList<AssuntoVO>();

			for (NeoObject assuntoQuery : newObjAssun) {
				AssuntoVO assuntoVO= new AssuntoVO();
				EntityWrapper wrapperAssun = new EntityWrapper(assuntoQuery);
				
				assuntoVO.setNeoId( NeoUtils.safeOutputString(wrapperAssun.getValue("neoId")) );
				assuntoVO.setTxtAssunto(((String) wrapperAssun.getValue("descassun")).replace("'", "''"));

				List<NeoObject> newObjPerg = ((List<NeoObject>) wrapperAssun.findValue("neoidPergunta"));
				
				ArrayList<PerguntaVO> perguntas = new ArrayList<PerguntaVO>();
				
				for (NeoObject perguntaQuery : newObjPerg) {
					PerguntaVO perguntaVO = new PerguntaVO();
					EntityWrapper wrapperPerg = new EntityWrapper(perguntaQuery);
					
					perguntaVO.setNeoId( NeoUtils.safeOutputString(wrapperPerg.getValue("neoId")) );
					perguntaVO.setTxtPergunta(((String) wrapperPerg.getValue("perguntatxt")).replace("'", "''"));
					
					perguntas.add(perguntaVO);

				}
				assuntoVO.setPerguntas(perguntas);
				assuntos.add(assuntoVO);
			}
			pesquisaVO.setAssuntos(assuntos);
			pesquisas.add(pesquisaVO);
		}
		inspetoriaPerguntasVO.setPesquisas(pesquisas);
		inspetoriaPerguntasVO.setError(false);
		
		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		json = g.toJson(inspetoriaPerguntasVO);
		//Gson gson = new Gson();
		//json = gson.toJson(inspetoriaPerguntasVO);
		System.out.println("Send pesquisas -> da regional ["+regional+"] -> " + json );
		return json;

	}
	
	private String generateError(){
		inspetoriaPerguntasVO inspetoriaPerguntasVO = new inspetoriaPerguntasVO();
		inspetoriaPerguntasVO.setError(true);
		Gson gson = new Gson();
		return gson.toJson(inspetoriaPerguntasVO);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doRequest(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doRequest(request, response);
	}
	
	private void doRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		PrintWriter out = response.getWriter();
		try {
			out.print(pesquisa(request));

		} catch (Exception e) {
			e.printStackTrace();
			out.print(generateError());
		}
	}
}
