package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Timestamp;

public class ApontamentoVO {
	
	
	private Timestamp competencia;
	private Long empresa;
	private Long filial;
	private Long contrato;
	private Long posto;
	private Long sequenciaApontamento;
	private String valorUnitario;
	private String adicionaOuSubtrai;
	
	public Timestamp getCompetencia() {
		return competencia;
	}
	public void setCompetencia(Timestamp competencia) {
		this.competencia = competencia;
	}
	public Long getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Long empresa) {
		this.empresa = empresa;
	}
	public Long getFilial() {
		return filial;
	}
	public void setFilial(Long filial) {
		this.filial = filial;
	}
	public Long getContrato() {
		return contrato;
	}
	public void setContrato(Long contrato) {
		this.contrato = contrato;
	}
	public Long getPosto() {
		return posto;
	}
	public void setPosto(Long posto) {
		this.posto = posto;
	}
	public Long getSequenciaApontamento() {
		return sequenciaApontamento;
	}
	public void setSequenciaApontamento(Long sequenciaApontamento) {
		this.sequenciaApontamento = sequenciaApontamento;
	}
	public String getValorUnitario() {
		return valorUnitario;
	}
	public void setValorUnitario(String valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	public String getAdicionaOuSubtrai() {
		return adicionaOuSubtrai;
	}
	public void setAdicionaOuSubtrai(String adicionaOuSubtrai) {
		this.adicionaOuSubtrai = adicionaOuSubtrai;
	}	 

}
