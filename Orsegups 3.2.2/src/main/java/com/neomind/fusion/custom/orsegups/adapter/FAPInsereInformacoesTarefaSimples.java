package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class FAPInsereInformacoesTarefaSimples implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Long aplicacaoPagamento = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
		String tituloTarefa = "";
		String descricaoSolicitacao = "";
		Map<String, String> mapParametrosDescricao = new HashMap<String, String>();
		
		if (aplicacaoPagamento == 1L) {
			
			//Título: Ref.: Processo FAP <código> - <aplicação> - <placa> - <fornecedor>
			String codigoTarefaSimples = (String) processEntity.findValue("wfprocess.code");
			String descricaoAplicacao = (String) processEntity.findValue("aplicacaoPagamento.nome");
			String placaViatura = (String) processEntity.findValue("viatura.placa");
			String tipoViatura = (String) processEntity.findValue("viatura.tipoViatura.nome");
			Long codigoFornecedor = (Long) processEntity.findValue("fornecedor.codfor");
			String nomeFornecedor = (String) processEntity.findValue("fornecedor.nomfor");
			
			tituloTarefa = "Ref.: Proceso FAP " + codigoTarefaSimples + " - " + descricaoAplicacao + " - " + placaViatura + " - " + tipoViatura + " - " + codigoFornecedor.toString() + " - "+ nomeFornecedor;
			
			String nomeSolicitante = (String) processEntity.findValue("solicitanteOrcamento.fullName");
			GregorianCalendar dataSolicitacao = (GregorianCalendar) processEntity.findValue("dataSolicitacao");
			String kmViatura = String.valueOf((Long) processEntity.findValue("laudo.kilometragem"));
			String nomeCondutor = (String) processEntity.findValue("laudo.condutor");
			String descricaoTipoManutencao = (String) processEntity.findValue("laudo.tipoManutencao.descricao");
			String descricaoRelato = (String) processEntity.findValue("laudo.relato");
			String descricaoLaudo = (String) processEntity.findValue("laudo.laudo");
			BigDecimal valorTotal = (BigDecimal) processEntity.findValue("valorTotal");
			String responsavelPagamento = (String) processEntity.findValue("responsavelPagamento.nome");
			
			mapParametrosDescricao.put("nomeSolicitante", nomeSolicitante);
			mapParametrosDescricao.put("dataSolicitacao", NeoDateUtils.safeDateFormat(dataSolicitacao, "dd/MM/yyyy"));
			mapParametrosDescricao.put("kmViatura", kmViatura);
			mapParametrosDescricao.put("nomeCondutor", nomeCondutor);
			mapParametrosDescricao.put("descricaoTipoManutencao", descricaoTipoManutencao);
			mapParametrosDescricao.put("descricaoRelato", descricaoRelato);
			mapParametrosDescricao.put("descricaoLaudo", descricaoLaudo);
			mapParametrosDescricao.put("valorTotal", valorTotal+"");
			mapParametrosDescricao.put("responsavelPagamento", responsavelPagamento);
		}
		else if ((aplicacaoPagamento == 2L) || (aplicacaoPagamento == 3L) || (aplicacaoPagamento == 4L) ||
				 (aplicacaoPagamento == 5L) || (aplicacaoPagamento == 6L) || (aplicacaoPagamento == 7L) ||
				 (aplicacaoPagamento == 8L) || (aplicacaoPagamento == 9L))
		{
			//Título: Ref.: Processo FAP <código> - <aplicação> - <conta> - <fantasiaCliente>
			String codigoTarefaSimples = (String) processEntity.findValue("wfprocess.code");
			String descricaoAplicacao = (String) processEntity.findValue("aplicacaoPagamento.nome");
			String contaSigma = (String) processEntity.findValue("codigoConta"); 
			String nomeFantasia = (String) processEntity.findValue("contaSigma.fantasia");
			
			tituloTarefa = "Ref.: Proceso FAP " + codigoTarefaSimples + " - " + descricaoAplicacao + " - " + contaSigma + " - " + nomeFantasia;
			
			String nomeSolicitante = (String) processEntity.findValue("solicitanteOrcamento.fullName");
			GregorianCalendar dataSolicitacao = (GregorianCalendar) processEntity.findValue("dataSolicitacao");
			String descricaoRelato = (String) processEntity.findValue("laudoEletronica.relato");
			descricaoRelato = descricaoRelato.replaceAll("\\r\\n|\\n", "");
			String descricaoLaudo = (String) processEntity.findValue("laudoEletronica.laudo");
			descricaoLaudo = descricaoLaudo.replaceAll("\\r\\n|\\n", "");
			BigDecimal valorTotal = (BigDecimal) processEntity.findValue("valorTotal");
			
			mapParametrosDescricao.put("nomeSolicitante", nomeSolicitante);
			mapParametrosDescricao.put("dataSolicitacao", NeoDateUtils.safeDateFormat(dataSolicitacao, "dd/MM/yyyy"));
			mapParametrosDescricao.put("descricaoRelato", descricaoRelato);
			mapParametrosDescricao.put("descricaoLaudo", descricaoLaudo);
			mapParametrosDescricao.put("valorTotal", valorTotal + "");
		}
		
		descricaoSolicitacao = tratarTamanhoDescricaoSolicitacao(mapParametrosDescricao, aplicacaoPagamento);
		
		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "dilmoberger"));
						
		NeoObject noTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper centralWrapper = new EntityWrapper(noTarefaSimples);
		centralWrapper.setValue("Solicitante", objSolicitante);
		centralWrapper.setValue("Titulo", tituloTarefa);
		centralWrapper.setValue("DescricaoSolicitacao", descricaoSolicitacao);
		
		processEntity.setValue("formTarefaSimples", noTarefaSimples);		
	}
	
	private String gerarDescricaoOS(Map<String, String> parametros, Long aplicacaoPagamento)
	{
		String descricaoSolicitacao = "";
		descricaoSolicitacao = descricaoSolicitacao + "<strong>Solicitante:</strong> " + parametros.get("nomeSolicitante") + "<br>";
		descricaoSolicitacao = descricaoSolicitacao + "<strong><b>Data Solicitação:</strong> " + parametros.get("dataSolicitacao") + "<br>";
		if(aplicacaoPagamento == 1L) {
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Km:</strong> " + parametros.get("kmViatura") + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Condutor</strong> " + parametros.get("nomeCondutor") + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Tipo de Manutenção:</strong> " + parametros.get("descricaoTipoManutencao") + "<br>";
		}
		descricaoSolicitacao = descricaoSolicitacao + "<strong><b>Relato:</strong> " + parametros.get("descricaoRelato") + "<br>";
		descricaoSolicitacao = descricaoSolicitacao + "<strong><b>Laudo:</strong> " + parametros.get("descricaoLaudo") + "<br>";
		descricaoSolicitacao = descricaoSolicitacao + "<strong><b>Valor Total:</strong> " + parametros.get("valorTotal") + "<br>";
		if(aplicacaoPagamento == 1L) {
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Responsável pelo pagamento:</strong> " + parametros.get("responsavelPagamento") + "<br>";
		}
		
		return NeoUtils.encodeHTMLValue(descricaoSolicitacao);
	}
	
	private String tratarTamanhoDescricaoSolicitacao(Map<String, String> mapParametrosDescricao, Long aplicacaoPagamento) {
		String descricaoSolicitacao = gerarDescricaoOS(mapParametrosDescricao, aplicacaoPagamento);
		
		if(descricaoSolicitacao.length() >= 8000) {
			
			String maiorCampoDescricao, campoASerTratado;
			String descricaoRelato = mapParametrosDescricao.get("descricaoRelato");
			String descricaoLaudo = mapParametrosDescricao.get("descricaoLaudo");
			
			if(descricaoRelato.length() > descricaoLaudo.length()){
				maiorCampoDescricao = "descricaoRelato";
				campoASerTratado = descricaoRelato;
			}else {
				maiorCampoDescricao = "descricaoLaudo";
				campoASerTratado = descricaoLaudo;
			}
			
			Integer diff = 200;
			while (descricaoSolicitacao.length() >= 8000) {
				mapParametrosDescricao.put(maiorCampoDescricao, campoASerTratado.substring(0, campoASerTratado.length() - diff));
				descricaoSolicitacao = gerarDescricaoOS(mapParametrosDescricao, aplicacaoPagamento);
				
				diff += 200;
			}
		}
		return descricaoSolicitacao;
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}
}
