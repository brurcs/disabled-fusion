package com.neomind.fusion.custom.orsegups.endpoint.dto;

public class QLPTrocaDTO {
    
    private String idNexti;
    private String colaborador;
    private long data;
    private int idPosto;
    private String observacao;
    private int idEscala;
    private int idTurma;
    
    public String getIdNexti() {
        return idNexti;
    }
    public void setIdNexti(String idNexti) {
        this.idNexti = idNexti;
    }
    public String getColaborador() {
        return colaborador;
    }
    public void setColaborador(String colaborador) {
        this.colaborador = colaborador;
    }
    public long getData() {
        return data;
    }
    public void setData(long data) {
        this.data = data;
    }
    public int getIdPosto() {
        return idPosto;
    }
    public void setIdPosto(int idPosto) {
        this.idPosto = idPosto;
    }
    public String getObservacao() {
        return observacao;
    }
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    public int getIdEscala() {
	return idEscala;
    }
    public void setIdEscala(int idEscala) {
	this.idEscala = idEscala;
    }
    public int getIdTurma() {
	return idTurma;
    }
    public void setIdTurma(int idTurma) {
	this.idTurma = idTurma;
    }
}
