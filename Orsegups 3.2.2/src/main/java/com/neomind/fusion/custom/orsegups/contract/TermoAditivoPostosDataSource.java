package com.neomind.fusion.custom.orsegups.contract;

import java.util.ArrayList;
import java.util.Collection;

public class TermoAditivoPostosDataSource
{
	private String valorFinal;
	private String valorFinalTxt;
	private String dAplica;
	private String dNaoAplica;
	private String eAplica;
	private String eNaoAplica;
	private String valorAlt;
	private String valorAltTxt;
	private String dir_listaEquipamentos;
	private String enderecoInstalacao;
	private String estadoCidadeInstalacao;
	private String referenciaInstalacao;
	private String telefoneInstalacao1;
	private String telefoneInstalacao2;
	private String telefoneInstalacao3;
	private String cpfInstalacao;
	private String bairroInstalacao;
	private String numeroInstalacao;
	private String fAplica;
	private String fNaoAplica;
	private String responsavelLocalInstalacao;
	private String telefone1Responsavel1;
	private String telefone1Responsavel2;
	private String telefone1Responsavel3;
	private String telefone2Responsavel1;
	private String telefone2Responsavel2;
	private String telefone2Responsavel3;
	private String nomeCliente;
	private String cargoResponsavel1;
	private String cargoResponsavel2;
	private String cargoResponsavel3;
	private String nomeResponsavel1;
	private String nomeResponsavel2;
	private String nomeResponsavel3;
	private String numeroPosto;
	private String numeroContrato;
	private String cepReferencia;
	private String cepInstalacao;
	private String valorInstalacao;
	private String valorInstalacaoTxt;
	private String numeroPontos;
	private String numeroPontosTxt;
	private String observacao;
	private String nomeExecutivo;
	private String novoPosto;
	private String alteracaoPosto;
	private String prazoMaximo;
	
	private Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos1 = new ArrayList<ListaEquipamentosMinutaDataSource>();
	private Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos2 = new ArrayList<ListaEquipamentosMinutaDataSource>();
	
	public String getValorFinal()
	{
		return valorFinal;
	}
	public void setValorFinal(String valorFinal)
	{
		this.valorFinal = valorFinal;
	}
	public String getdAplica()
	{
		return dAplica;
	}
	public void setdAplica(String dAplica)
	{
		this.dAplica = dAplica;
	}
	public String getdNaoAplica()
	{
		return dNaoAplica;
	}
	public void setdNaoAplica(String dNaoAplica)
	{
		this.dNaoAplica = dNaoAplica;
	}
	public String geteAplica()
	{
		return eAplica;
	}
	public void seteAplica(String eAplica)
	{
		this.eAplica = eAplica;
	}
	public String geteNaoAplica()
	{
		return eNaoAplica;
	}
	public void seteNaoAplica(String eNaoAplica)
	{
		this.eNaoAplica = eNaoAplica;
	}
	public String getValorAlt()
	{
		return valorAlt;
	}
	public void setValorAlt(String valorAlt)
	{
		this.valorAlt = valorAlt;
	}
	public String getValorAltTxt()
	{
		return valorAltTxt;
	}
	public void setValorAltTxt(String valorAltTxt)
	{
		this.valorAltTxt = valorAltTxt;
	}
	public String getDir_listaEquipamentos()
	{
		return dir_listaEquipamentos;
	}
	public void setDir_listaEquipamentos(String dir_listaEquipamentos)
	{
		this.dir_listaEquipamentos = dir_listaEquipamentos;
	}
	public String getEnderecoInstalacao()
	{
		return enderecoInstalacao;
	}
	public void setEnderecoInstalacao(String enderecoInstalacao)
	{
		this.enderecoInstalacao = enderecoInstalacao;
	}
	public String getEstadoCidadeInstalacao()
	{
		return estadoCidadeInstalacao;
	}
	public void setEstadoCidadeInstalacao(String estadoCidadeInstalacao)
	{
		this.estadoCidadeInstalacao = estadoCidadeInstalacao;
	}
	public String getReferenciaInstalacao()
	{
		return referenciaInstalacao;
	}
	public void setReferenciaInstalacao(String referenciaInstalacao)
	{
		this.referenciaInstalacao = referenciaInstalacao;
	}
	public String getTelefoneInstalacao2()
	{
		return telefoneInstalacao2;
	}
	public void setTelefoneInstalacao2(String telefoneInstalacao2)
	{
		this.telefoneInstalacao2 = telefoneInstalacao2;
	}
	public String getTelefoneInstalacao1()
	{
		return telefoneInstalacao1;
	}
	public void setTelefoneInstalacao1(String telefoneInstalacao1)
	{
		this.telefoneInstalacao1 = telefoneInstalacao1;
	}
	public String getTelefoneInstalacao3()
	{
		return telefoneInstalacao3;
	}
	public void setTelefoneInstalacao3(String telefoneInstalacao3)
	{
		this.telefoneInstalacao3 = telefoneInstalacao3;
	}
	public String getCpfInstalacao()
	{
		return cpfInstalacao;
	}
	public void setCpfInstalacao(String cpfInstalacao)
	{
		this.cpfInstalacao = cpfInstalacao;
	}
	public String getBairroInstalacao()
	{
		return bairroInstalacao;
	}
	public void setBairroInstalacao(String bairroInstalacao)
	{
		this.bairroInstalacao = bairroInstalacao;
	}
	public String getNumeroInstalacao()
	{
		return numeroInstalacao;
	}
	public void setNumeroInstalacao(String numeroInstalacao)
	{
		this.numeroInstalacao = numeroInstalacao;
	}
	public String getfAplica()
	{
		return fAplica;
	}
	public void setfAplica(String fAplica)
	{
		this.fAplica = fAplica;
	}
	public String getfNaoAplica()
	{
		return fNaoAplica;
	}
	public void setfNaoAplica(String fNaoAplica)
	{
		this.fNaoAplica = fNaoAplica;
	}
	public String getResponsavelLocalInstalacao()
	{
		return responsavelLocalInstalacao;
	}
	public void setResponsavelLocalInstalacao(String responsavelLocalInstalacao)
	{
		this.responsavelLocalInstalacao = responsavelLocalInstalacao;
	}
	public String getTelefone1Responsavel1()
	{
		return telefone1Responsavel1;
	}
	public void setTelefone1Responsavel1(String telefone1Responsavel1)
	{
		this.telefone1Responsavel1 = telefone1Responsavel1;
	}
	public String getTelefone1Responsavel2()
	{
		return telefone1Responsavel2;
	}
	public void setTelefone1Responsavel2(String telefone1Responsavel2)
	{
		this.telefone1Responsavel2 = telefone1Responsavel2;
	}
	public String getTelefone1Responsavel3()
	{
		return telefone1Responsavel3;
	}
	public void setTelefone1Responsavel3(String telefone1Responsavel3)
	{
		this.telefone1Responsavel3 = telefone1Responsavel3;
	}
	public String getTelefone2Responsavel1()
	{
		return telefone2Responsavel1;
	}
	public void setTelefone2Responsavel1(String telefone2Responsavel1)
	{
		this.telefone2Responsavel1 = telefone2Responsavel1;
	}
	public String getTelefone2Responsavel2()
	{
		return telefone2Responsavel2;
	}
	public void setTelefone2Responsavel2(String telefone2Responsavel2)
	{
		this.telefone2Responsavel2 = telefone2Responsavel2;
	}
	public String getTelefone2Responsavel3()
	{
		return telefone2Responsavel3;
	}
	public void setTelefone2Responsavel3(String telefone2Responsavel3)
	{
		this.telefone2Responsavel3 = telefone2Responsavel3;
	}
	public String getNomeCliente()
	{
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente)
	{
		this.nomeCliente = nomeCliente;
	}
	public String getCargoResponsavel1()
	{
		return cargoResponsavel1;
	}
	public void setCargoResponsavel1(String cargoResponsavel1)
	{
		this.cargoResponsavel1 = cargoResponsavel1;
	}
	public String getCargoResponsavel2()
	{
		return cargoResponsavel2;
	}
	public void setCargoResponsavel2(String cargoResponsavel2)
	{
		this.cargoResponsavel2 = cargoResponsavel2;
	}
	public String getCargoResponsavel3()
	{
		return cargoResponsavel3;
	}
	public void setCargoResponsavel3(String cargoResponsavel3)
	{
		this.cargoResponsavel3 = cargoResponsavel3;
	}
	public String getNomeResponsavel1()
	{
		return nomeResponsavel1;
	}
	public void setNomeResponsavel1(String nomeResponsavel1)
	{
		this.nomeResponsavel1 = nomeResponsavel1;
	}
	public String getNomeResponsavel2()
	{
		return nomeResponsavel2;
	}
	public void setNomeResponsavel2(String nomeResponsavel2)
	{
		this.nomeResponsavel2 = nomeResponsavel2;
	}
	public String getNomeResponsavel3()
	{
		return nomeResponsavel3;
	}
	public void setNomeResponsavel3(String nomeResponsavel3)
	{
		this.nomeResponsavel3 = nomeResponsavel3;
	}
	public String getNumeroPosto()
	{
		return numeroPosto;
	}
	public void setNumeroPosto(String numeroPosto)
	{
		this.numeroPosto = numeroPosto;
	}
	public String getNumeroContrato()
	{
		return numeroContrato;
	}
	public void setNumeroContrato(String numeroContrato)
	{
		this.numeroContrato = numeroContrato;
	}
	public Collection<ListaEquipamentosMinutaDataSource> getListaEquipamentos1()
	{
		return listaEquipamentos1;
	}
	public void setListaEquipamentos1(Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos1)
	{
		this.listaEquipamentos1 = listaEquipamentos1;
	}
	public Collection<ListaEquipamentosMinutaDataSource> getListaEquipamentos2()
	{
		return listaEquipamentos2;
	}
	public void setListaEquipamentos2(Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos2)
	{
		this.listaEquipamentos2 = listaEquipamentos2;
	}
	
	public String getValorFinalTxt()
	{
		return valorFinalTxt;
	}
	public void setValorFinalTxt(String valorFinalTxt)
	{
		this.valorFinalTxt = valorFinalTxt;
	}
	public String getCepReferencia()
	{
		return cepReferencia;
	}
	public void setCepReferencia(String cepReferencia)
	{
		this.cepReferencia = cepReferencia;
	}
	public String getCepInstalacao()
	{
		return cepInstalacao;
	}
	public void setCepInstalacao(String cepInstalacao)
	{
		this.cepInstalacao = cepInstalacao;
	}
	public String getValorInstalacao()
	{
		return valorInstalacao;
	}
	public void setValorInstalacao(String valorInstalacao)
	{
		this.valorInstalacao = valorInstalacao;
	}
	public String getValorInstalacaoTxt()
	{
		return valorInstalacaoTxt;
	}
	public void setValorInstalacaoTxt(String valorInstalacaoTxt)
	{
		this.valorInstalacaoTxt = valorInstalacaoTxt;
	}
	public String getNumeroPontos()
	{
		return numeroPontos;
	}
	public void setNumeroPontos(String numeroPontos)
	{
		this.numeroPontos = numeroPontos;
	}
	public String getNumeroPontosTxt()
	{
		return numeroPontosTxt;
	}
	public void setNumeroPontosTxt(String numeroPontosTxt)
	{
		this.numeroPontosTxt = numeroPontosTxt;
	}
	public String getObservacao()
	{
		return observacao;
	}
	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}
	public String getNomeExecutivo()
	{
		return nomeExecutivo;
	}
	public void setNomeExecutivo(String nomeExecutivo)
	{
		this.nomeExecutivo = nomeExecutivo;
	}
	public String getNovoPosto()
	{
		return novoPosto;
	}
	public void setNovoPosto(String novoPosto)
	{
		this.novoPosto = novoPosto;
	}
	public String getAlteracaoPosto()
	{
		return alteracaoPosto;
	}
	public void setAlteracaoPosto(String alteracaoPosto)
	{
		this.alteracaoPosto = alteracaoPosto;
	}
	public String getPrazoMaximo() {
		return prazoMaximo;
	}
	public void setPrazoMaximo(String prazoMaximo) {
		this.prazoMaximo = prazoMaximo;
	}
	
}
