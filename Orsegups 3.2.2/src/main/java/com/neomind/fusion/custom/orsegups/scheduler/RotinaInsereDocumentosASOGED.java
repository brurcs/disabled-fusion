package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.e2doc.E2docIndexacaoBean;
import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.ti.TarefasVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * Rotina responsável por capturar e inserir o arquivo de ASO das tarefas de Atestado Ocupacional no GED
 * 
 * @author herisson.ferreira
 *
 */
public class RotinaInsereDocumentosASOGED implements CustomJobAdapter {

	private static String mensagemErro = "Erro ao Inserir o documento ASO no GED";
	
	@Override
	public void execute(CustomJobContext ctx) {
	
		LinkedList<TarefasVO> listaTarefas = new LinkedList<>();
		ColaboradorVO colaboradorVO = new ColaboradorVO();
		
		listaTarefas = retornaListaDocumentos();
		
		for(TarefasVO tarefasVO : listaTarefas) {
			
			try {
				
				colaboradorVO = montaDadosColaborador(tarefasVO.getNeoId());
				
				System.out.println("## "+this.getClass().getSimpleName()+" - Colaborador: "+colaboradorVO.getNumeroEmpresa()+"//"+colaboradorVO.getNumeroCadastro()+" - "+colaboradorVO.getNomeColaborador());
				
				insereDocumentoGED(colaboradorVO, tarefasVO);
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(mensagemErro);
			}
			
		}
		
	}

	/**
	 * 
	 * Efetua a inserção do arquivo de ASO no GED.
	 * 
	 * @param colaboradorVO Objeto contendo os dados do colaborador
	 * @param tarefasVO Objeto contendo o neoId da tarefa
	 */
	private void insereDocumentoGED(ColaboradorVO colaboradorVO, TarefasVO tarefasVO) {
		
		try {
			
			E2docIndexacaoBean e2docIndexacaoBean = new E2docIndexacaoBean();

			GregorianCalendar dataAtual = new GregorianCalendar();

			if(NeoUtils.safeIsNotNull(colaboradorVO.getNumeroCadastro()) && NeoUtils.safeIsNotNull(colaboradorVO.getNomeColaborador()) && NeoUtils.safeIsNotNull(colaboradorVO.getCpf()) && NeoUtils.safeIsNotNull(colaboradorVO.getNumeroEmpresa()) && NeoUtils.safeIsNotNull(tarefasVO)) {

				String empresa = retornaNomeEmpresa(colaboradorVO.getNumeroEmpresa());
				NeoFile arquivo = NeoUtils.safeNeoFile(retornaArquivoAso(tarefasVO));

				e2docIndexacaoBean.setEmpresa(empresa);
				e2docIndexacaoBean.setMatricula(colaboradorVO.getNumeroCadastro().toString());
				e2docIndexacaoBean.setColaborador(colaboradorVO.getNomeColaborador());
				e2docIndexacaoBean.setCpf(colaboradorVO.getCpf());	
				e2docIndexacaoBean.setTipoDocumento("A.S.O");
				e2docIndexacaoBean.setClasse("ASO");
				e2docIndexacaoBean.setData(dataAtual.getTime());
				e2docIndexacaoBean.setObservacao(String.valueOf(Calendar.getInstance().getTimeInMillis()));

				e2docIndexacaoBean.setTipo("i");
				e2docIndexacaoBean.setModelo("documentação rh");
				e2docIndexacaoBean.setFormato(arquivo.getSufix());
				e2docIndexacaoBean.setDocumento(arquivo.getBytes());

				Boolean resultado = E2docUtils.indexarDocumento(e2docIndexacaoBean);

				System.out.println("## "+this.getClass().getSimpleName()+" - Colaborador: "+colaboradorVO.getNumeroEmpresa()+"//"+colaboradorVO.getNumeroCadastro()+" - "+colaboradorVO.getNomeColaborador()+" Resultado: "+resultado);

			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao indexar o arquivo ASO no GED. - Adapter: "+this.getClass().getSimpleName();
			System.out.println(mensagemErro);
		}
		
	}

	/**
	 * Captura o último arquivo anexado na tarefa
	 * 
	 * @param tarefasVO Objeto contendo o neoId da tarefa
	 * @return Arquivo de ASO anexo na tarefa
	 */
	private NeoFile retornaArquivoAso(TarefasVO tarefasVO) {

		NeoFile arquivoASO = null;
		NeoFile arquivoAux = null;
		
		try {
			
			NeoObject noHistorico = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("Tarefa"), tarefasVO.getNeoId());
			EntityWrapper wHistorico = new EntityWrapper(noHistorico);
			List<NeoObject> listaRegistroAtividades = wHistorico.findGenericValue("registroAtividades");
			
			sortId(listaRegistroAtividades);
			
			for (int i = 0; i < listaRegistroAtividades.size(); i++)
			{
				arquivoAux = new EntityWrapper(listaRegistroAtividades.get(i)).findGenericValue("anexo");
				
				if(NeoUtils.safeIsNotNull(arquivoAux)) {
					arquivoASO = arquivoAux;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao retornar o arquivo ASO. - Adapter: "+this.getClass().getSimpleName();
			System.out.println(mensagemErro);
		}
		
		return arquivoASO;
	}

	/**
	 * Retorna o nome da empresa por extenso
	 * 
	 * @param numeroEmpresa Número da empresa do colaborador
	 * @return Nome da empresa correspondente
	 */
	private String retornaNomeEmpresa(Long numeroEmpresa) {
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		String nomeEmpresa = null;
		
		try {
			
			sql.append(" SELECT NOMEMP FROM R030EMP WITH(NOLOCK) WHERE NUMEMP = ? ");
			
			conn = PersistEngine.getConnection("VETORH");
			
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, numeroEmpresa);
			rs = pstm.executeQuery();
			
			if(rs.next()){
			    nomeEmpresa = rs.getString("nomemp"); 
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao retornar o nome da empresa "+numeroEmpresa+". - Adapter: "+this.getClass().getSimpleName();
			System.out.println(mensagemErro);
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return nomeEmpresa;
	}

	/**
	 * Monta os dados do colaborador a partir da descrição da solicitação da tarefa
	 * 
	 * @param neoId Identificador da tarefa
	 * @return Objeto contendo os dados do colaborador
	 */
	private ColaboradorVO montaDadosColaborador(Long neoId){
		
		ColaboradorVO colaboradorVO = new ColaboradorVO();
		
		try {
			
			NeoObject noHistorico = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("Tarefa"), neoId);
			EntityWrapper wHistorico = new EntityWrapper(noHistorico);
			List<NeoObject> listaRegistroAtividades = wHistorico.findGenericValue("registroAtividades");
			
			String descricaoSolicitacao = null;
			
			sortId(listaRegistroAtividades);
			
			descricaoSolicitacao = new EntityWrapper(listaRegistroAtividades.get(0)).findGenericValue("descricao");
			
			System.out.println(this.getClass().getSimpleName() + "("+descricaoSolicitacao.toString()+")");
			
			colaboradorVO = desmembraDescricao(descricaoSolicitacao);
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao montar os dados do colaborador. - Adapter: "+this.getClass().getSimpleName();
			System.out.println(mensagemErro);
		}
		
		return colaboradorVO;
	}

	/**
	 * "Desmonta" a descrição da tarefa em busca dos dados referentes ao colaborador
	 * 
	 * @param descricaoSolicitacao
	 * @return Objeto contendo os dados do colaborador
	 */
	private ColaboradorVO desmembraDescricao(String descricaoSolicitacao) {
		
		ColaboradorVO colaboradorVO = new ColaboradorVO();
		
		try {

		    descricaoSolicitacao = descricaoSolicitacao.replaceAll("<strong>","").replaceAll("</strong>","").replaceAll("<br>",";").replaceAll(":",";");
		    
		    String[] descricaoSplit = descricaoSolicitacao.split(";");
		    String[] colaboradorSplit = descricaoSplit[3].replaceAll("/", "-").split("-");
		    
		    String cpf = descricaoSplit[1].trim();
		    String empresa = colaboradorSplit[0].trim();
		    String matricula = colaboradorSplit[1].trim();
		    String colaborador = colaboradorSplit[2].trim();
		    
			colaboradorVO.setCpf(Long.parseLong(cpf));
			colaboradorVO.setNumeroEmpresa(Long.parseLong(empresa));
			colaboradorVO.setNumeroCadastro(Long.parseLong(matricula));
			colaboradorVO.setNomeColaborador(colaborador);
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			mensagemErro = "Erro ao converter String para Long. - Adapter: "+this.getClass().getSimpleName();
			System.out.println(mensagemErro);
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao montar os dados do colaborador. - Adapter: "+this.getClass().getSimpleName();
			System.out.println(mensagemErro);
		}
		
		return colaboradorVO;
		
	}

	/**
	 * Ordena os registros pelo neoId
	 * 
	 * @param listaRegistroAtividades
	 */
	private void sortId(List<NeoObject> listaRegistroAtividades) {
		
		Collections.sort(listaRegistroAtividades, new Comparator<NeoObject>()
		{
			@Override
			public int compare(NeoObject o1, NeoObject o2)
			{
				return o1.getNeoId() < o2.getNeoId() ? -1 : 1;
			}
		});
		
	}

	/**
	 * Captura o neoId das tarefas de ASO finalizadas no dia anterior
	 * 
	 * @return Lista de Objeto contendo o neoId da tarefa
	 */
	private LinkedList<TarefasVO> retornaListaDocumentos() {
		
		LinkedList<TarefasVO> listaTarefas = new LinkedList<>();
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		GregorianCalendar dataBase = new GregorianCalendar();
		dataBase.add(GregorianCalendar.DAY_OF_MONTH, -1);
		dataBase.set(GregorianCalendar.HOUR_OF_DAY, 00);
		dataBase.set(GregorianCalendar.MINUTE, 00);
		dataBase.set(GregorianCalendar.SECOND, 00);
		dataBase.set(GregorianCalendar.MILLISECOND, 00);

		String dataInicio = NeoDateUtils.safeDateFormat(dataBase, "yyyy-MM-dd HH:mm:ss");

		dataBase.set(GregorianCalendar.HOUR_OF_DAY, 23);
		dataBase.set(GregorianCalendar.MINUTE, 59);
		dataBase.set(GregorianCalendar.SECOND, 59);
		dataBase.set(GregorianCalendar.MILLISECOND, 59);

		String dataFim = NeoDateUtils.safeDateFormat(dataBase, "yyyy-MM-dd HH:mm:ss");
		
		try {
		
			sql.append(" SELECT t.neoId FROM D_Tarefa t ");
			sql.append(" INNER JOIN WFProcess w ");
			sql.append(" ON t.wfprocess_neoId = w.neoId ");
			sql.append(" WHERE t.titulo LIKE '%Agendar o próximo ASO%' ");
			sql.append(" AND t.dataConclusao BETWEEN ? AND ? ");
			sql.append(" AND w.processState != 2 ");
			
			conn = PersistEngine.getConnection("");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setString(1, dataInicio);
			pstm.setString(2, dataFim);
			
			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				TarefasVO tarefasVO = new TarefasVO();
				
				tarefasVO.setNeoId(rs.getLong("neoId"));

				listaTarefas.add(tarefasVO);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			mensagemErro = "Erro ao consultar as tarefas de ASO. - Adapter: "+this.getClass().getSimpleName();
			System.out.println(mensagemErro);
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaTarefas;
	}
	
}
