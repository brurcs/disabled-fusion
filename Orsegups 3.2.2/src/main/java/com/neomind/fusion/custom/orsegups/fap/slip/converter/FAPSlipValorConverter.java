package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.BigDecimalConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;

public class FAPSlipValorConverter extends BigDecimalConverter
{
	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		EntityWrapper wrapper = new EntityWrapper(field.getForm().getObject());
		Boolean demandaAprovacao = wrapper.findGenericValue("demandaAprovacao");
		Long tipoPagamento = wrapper.findGenericValue("tipoPagamento.codigo");
		
		StringBuilder outBuilder = new StringBuilder();
		if (demandaAprovacao != null) {
			if (!demandaAprovacao && tipoPagamento != 2l)
				outBuilder.append("<span style=\"color: green\"><b>    Autorização Automática</b></span>");
			else 
				outBuilder.append("<span style=\"color: red\"><b>    Autorização Manual</b></span>");
		}
		
		return super.getHTMLView(field, origin) + outBuilder.toString();
	}
}
