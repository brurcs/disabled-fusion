package com.neomind.fusion.custom.orsegups.converter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.portal.VelocityUtils;

public class TreeRefreshCentroCustoConverter extends EntityConverter 
{
	private static final Log log = LogFactory.getLog(TreeRefreshCentroCustoConverter.class);
	
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		String idHiddenField = "id_" + field.getForm().getFieldPath() + field.getFieldCode() + "__";
		
		VelocityContext context = new VelocityContext();
		
		String servletPath = PortalUtil.getBaseURL();
		
		context.put("id_hidden_field", idHiddenField);
		context.put("servletPath", servletPath);

		return super.getHTMLInput(field, origin) + VelocityUtils.runTemplate("custom/orsegups/treeFieldRefresh.vm", context);
	}

}
