package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaEventosComDeslocamento implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaEventosComDeslocamento.class);

    @Override
    public void execute(CustomJobContext arg0) {
	log.warn("##### INICIAR ROTINA DE VERIFICAÇÃO DE EVENTOS COM DESLOCAMENTO - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	// LISTA COMANDOS SQL
	List<String> listaCmd = null;
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	
	String excessoes = this.getExcecoesGrupo();

	try {
	    /**
	     * Deslocamentos nas últimas 6h, de particoes diferentes de 0
	     * (pânico), de eventos recebidos (elimina X%), de zonas que não
	     * estejam anuladas e que tenham sido fechados com motivo de disparo
	     * em falso
	     */
	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuilder sql = new StringBuilder();
	    sql.append(" SELECT DISTINCT h.CD_CLIENTE, h.NU_AUXILIAR ");
	    sql.append(" FROM HISTORICO_ALARME h  WITH (NOLOCK) ");
	    sql.append(" INNER JOIN dbEVENTO z  WITH (NOLOCK) ON z.CD_CLIENTE = h.CD_CLIENTE AND z.ID_EVENTO = h.NU_AUXILIAR AND z.FG_ANULADA = 0  ");
	    sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON  C.CD_CLIENTE = H.CD_CLIENTE ");
	    sql.append(" WHERE h.DT_VIATURA_DESLOCAMENTO >= DATEADD(HOUR, -6, GETDATE()) ");
	    
	    if (excessoes != null && !excessoes.isEmpty()){
		sql.append(" AND ( C.CD_GRUPO_CLIENTE NOT IN ("+excessoes+") OR CD_GRUPO_CLIENTE IS NULL) ");
	    }
	    
	    sql.append(" AND h.NU_AUXILIAR <> 0 ");
	    sql.append(" AND h.CD_EVENTO NOT LIKE 'X%' ");
	    sql.append(" AND h.CD_EVENTO NOT IN ('E831') ");
	    sql.append(" AND (h.CD_MOTIVO_ALARME = 48 OR (h.CD_MOTIVO_ALARME IS NULL AND h.TX_OBSERVACAO_VIATURA LIKE '%1-VISTORIA FEITA LOCAL SEM ALTERAÇÃO.%')) ");
	    sql.append(" AND h.NU_AUXILIAR NOT LIKE '%F%' ");
	    sql.append(" AND h.NU_AUXILIAR NOT LIKE '%C%' ");
	    sql.append(" AND h.NU_AUXILIAR NOT LIKE '%B%' ");
	    sql.append(" AND h.NU_AUXILIAR NOT LIKE '%D%' ");
	    sql.append(" AND h.NU_AUXILIAR NOT LIKE '%E%' ");
	    sql.append(" AND h.NU_AUXILIAR NOT LIKE '%A%' ");
	    sql.append(" AND C.ID_EMPRESA NOT IN (10119,10120) ");
	    sql.append(" ORDER BY h.CD_CLIENTE, h.NU_AUXILIAR ");

	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();
	    conn.setAutoCommit(false);

	    listaCmd = new ArrayList<String>();
	    while (rs.next()) {
		String cdCliente = rs.getString("CD_CLIENTE");
		String nuAuxiliar = rs.getString("NU_AUXILIAR");

		// Insere Limpeza de Zonas Anuladas
		listaCmd.add(" INSERT INTO TAREFA_CENTRAL (CD_CLIENTE, DT_EXECUCAO_TAREFA, TP_TAREFA, FG_STATUS) VALUES (" + cdCliente + ", DATEADD(HOUR, 12, GETDATE()), 5, 0) ");

		// Anular a Zona
		listaCmd.add(" UPDATE dbEVENTO SET FG_ANULADA = 1 WHERE CD_CLIENTE = " + cdCliente + " AND ID_EVENTO = '" + nuAuxiliar + "' ");
	    }

	    for (String cmd : listaCmd) {

		pstm = conn.prepareStatement(cmd.toString());
		pstm.executeUpdate();
	    }

	    conn.commit();
	    conn.setAutoCommit(true);
	    log.warn("##### EXECUTAR ROTINA DE VERIFICAÇÃO DE EVENTOS COM DESLOCAMENTO - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO ROTINA DE VERIFICAÇÃO DE EVENTOS COM DESLOCAMENTO: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    try {
		conn.rollback();
	    } catch (SQLException e1) {
		e1.printStackTrace();
	    }
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
		log.error("##### ERRO ROTINA DE VERIFICAÇÃO DE EVENTOS COM DESLOCAMENTO: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    }
	    log.warn("##### FINALIZAR ROTINA DE VERIFICAÇÃO DE EVENTOS COM DESLOCAMENTO - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
    }

    private String getExcecoesGrupo() {

	String codigos = "";

	List<NeoObject> listaExcecoesDefeito = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("AZCadastroExcessaoGrupo"));

	ArrayList<Integer> listaCodDefeito = new ArrayList<Integer>();

	if (listaExcecoesDefeito != null && !listaExcecoesDefeito.isEmpty()) {
	    for (NeoObject excecao : listaExcecoesDefeito) {

		EntityWrapper wrappperExcecao = new EntityWrapper(excecao);

		Long codDefeito = (Long) wrappperExcecao.findField("grupo.cd_grupo_cliente").getValue();

		listaCodDefeito.add(Integer.valueOf(codDefeito.intValue()));

	    }
	}

	codigos = Joiner.on(",").join(listaCodDefeito);

	return codigos;
    }
}
