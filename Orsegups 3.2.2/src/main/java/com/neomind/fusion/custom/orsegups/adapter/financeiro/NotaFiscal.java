package com.neomind.fusion.custom.orsegups.adapter.financeiro;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "NFe", namespace = "http://www.portalfiscal.inf.br/nfe")
public class NotaFiscal {
	
	private List<InfoNotaFiscal> infoNotaFiscal;

	@XmlElement(name="infNFe", namespace = "http://www.portalfiscal.inf.br/nfe")
	public List<InfoNotaFiscal> getInfoNotaFiscal() {
		return infoNotaFiscal;
	}

	public void setInfoNotaFiscal(List<InfoNotaFiscal> infoNotaFiscal) {
		this.infoNotaFiscal = infoNotaFiscal;
	}
	
}
