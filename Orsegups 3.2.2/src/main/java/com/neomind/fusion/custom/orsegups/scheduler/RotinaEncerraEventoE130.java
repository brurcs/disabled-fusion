package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaEncerraEventoE130 implements CustomJobAdapter
{

	public static final String EVT_E130 = "E130";
	public static final String EVT_E301 = "E301";
	public static final String EVT_E131 = "E131";
	public static final String EVT_E134 = "E134";
	public static final String EVT_E250 = "E250";
	public static final String EVT_XXX1 = "XXX1";

	private static final Log log = LogFactory.getLog(RotinaEncerraEventoE130.class);

	public void execute(CustomJobContext arg0)
	{

		int contadorE130 = 0;
		int contadorE301 = 0;
		int contadorE131 = 0;
		int contadorE134 = 0;
		int contadorE250 = 0;
		int contadorXXX1 = 0;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{

			//*********************************************************************************************************************************
			// Trata eventos E130 
			log.warn("##### INICIAR ROTINA REMOÇÃO DE EVENTOS E130 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			QLGroupFilter groupFilterE130 = new QLGroupFilter("AND");
			groupFilterE130.addFilter(new QLEqualsFilter("habilitado", Boolean.TRUE));
			groupFilterE130.addFilter(new QLEqualsFilter("tratarE130", Boolean.TRUE));

			List<NeoObject> neoObjectsE130 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMARegional"), groupFilterE130);
			if ((neoObjectsE130 != null) && (!neoObjectsE130.isEmpty()))
			{
		
			    	//regional = montaStringFiltroRegional(neoObjectsE130);

				log.warn("##### EXECUTAR ROTINA REMOÇÃO DE EVENTOS E130 Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				
				for (NeoObject neoObject : neoObjectsE130)
				{
					EntityWrapper neoObjectRegional = new EntityWrapper(neoObject);
					
					String regional = "'" + (String) neoObjectRegional.getValue("regional") + "'";
					
					//Boolean utilizarPeriodo = (Boolean) neoObjectRegional.getValue("utilizarPeriodo");
													
					contadorE130 = finalizaEvento(regional, 2, EVT_E130);					    
										
					if (contadorE130 > 0)
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E130 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE130 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
					else
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E130 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE130 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
				}
								

			}
			else
			{
				log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E130 - Não foram encontradas regionais habilitadas e/ou com evento E130 marcados para finalização - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}

			//*********************************************************************************************************************************
			//trata Eventos E301 
			log.warn("##### INICIAR ROTINA REMOÇÃO DE EVENTOS E301 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			QLGroupFilter groupFilterE301 = new QLGroupFilter("AND");
			groupFilterE301.addFilter(new QLEqualsFilter("habilitado", Boolean.TRUE));
			groupFilterE301.addFilter(new QLEqualsFilter("tratarE301", Boolean.TRUE));

			List<NeoObject> neoObjectsE301 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMARegional"), groupFilterE301);
			if ((neoObjectsE301 != null) && (!neoObjectsE301.isEmpty()))
			{
			    //regional = montaStringFiltroRegional(neoObjectsE301);
			    
			    for (NeoObject neoObject : neoObjectsE301){
				EntityWrapper neoObjectRegional = new EntityWrapper(neoObject);
				
				String regional = "'" + (String) neoObjectRegional.getValue("regional") + "'";
				
				//Boolean utilizarPeriodo = (Boolean) neoObjectRegional.getValue("utilizarPeriodo");
				
				log.warn("##### EXECUTAR ROTINA REMOÇÃO DE EVENTOS E301 Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				//if (utilizarPeriodo){
				    GregorianCalendar inicio = (GregorianCalendar) neoObjectRegional.findField("E301Inicio").getValue();
				    GregorianCalendar fim = (GregorianCalendar) neoObjectRegional.findField("E301Fim").getValue();
				    contadorE301 = finalizaEventoPeriodo(regional,EVT_E301, inicio, fim);
				    
				    if (contadorE301 > 0)
				    {
					log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E301 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE301 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				    }
				    else
				    {
					log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E301 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE301 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				    }
				}
//				else{
//				    contadorE301 = finalizaEvento(regional, 1.5, EVT_E301);
//				    
//				}
				
				
//			    }
				
			}
			else
			{
				log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E301 - Não foram encontradas regionais habilitadas e/ou com evento E130 marcados para finalização - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}

			//*********************************************************************************************************************************
			//trata Eventos E131
			log.warn("##### INICIAR ROTINA REMOÇÃO DE EVENTOS E131 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			QLGroupFilter groupFilterE131 = new QLGroupFilter("AND");
			groupFilterE131.addFilter(new QLEqualsFilter("habilitado", Boolean.TRUE));
			groupFilterE131.addFilter(new QLEqualsFilter("tratarE131", Boolean.TRUE));

			List<NeoObject> neoObjectsE131 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMARegional"), groupFilterE131);
			if ((neoObjectsE131 != null) && (!neoObjectsE131.isEmpty()))
			{
			    
				for (NeoObject neoObject : neoObjectsE131)
				{
					EntityWrapper neoObjectRegional = new EntityWrapper(neoObject);
					
					String regional = "'" + (String) neoObjectRegional.getValue("regional") + "'";
					
					//Boolean utilizarPeriodo = (Boolean) neoObjectRegional.getValue("utilizarPeriodo");
					
					log.warn("##### EXECUTAR ROTINA REMOÇÃO DE EVENTOS E131 Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					    					   
					contadorE131 = finalizaEvento(regional, 2, EVT_E131);
					
					if (contadorE131 > 0)
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E131 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE131 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
					else
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E131 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE131 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
				}
				

			}
			else
			{
				log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E301 - Não foram encontradas regionais habilitadas e/ou com evento E131 marcados para finalização - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}

			//*********************************************************************************************************************************
			//trata Eventos E134 
			log.warn("##### INICIAR ROTINA REMOÇÃO DE EVENTOS E134 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			QLGroupFilter groupFilterE134 = new QLGroupFilter("AND");
			groupFilterE134.addFilter(new QLEqualsFilter("habilitado", Boolean.TRUE));
			groupFilterE134.addFilter(new QLEqualsFilter("tratarE134", Boolean.TRUE));

			List<NeoObject> neoObjectsE134 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMARegional"), groupFilterE134);
			if ((neoObjectsE134 != null) && (!neoObjectsE134.isEmpty()))
			{
				for (NeoObject neoObject : neoObjectsE134)
				{
					EntityWrapper neoObjectRegional = new EntityWrapper(neoObject);
					
					String regional = "'" + (String) neoObjectRegional.getValue("regional") + "'";
					
					//Boolean utilizarPeriodo = (Boolean) neoObjectRegional.getValue("utilizarPeriodo");
					
					log.warn("##### EXECUTAR ROTINA REMOÇÃO DE EVENTOS E134 Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
								    					   
					    contadorE134 = finalizaEvento(regional, 2, EVT_E134);
					
					if (contadorE134 > 0)
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E134 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE134 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
					else
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E134 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE134 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
				}
				

			}
			

			//*********************************************************************************************************************************
			//trata Eventos E250 
			log.warn("##### INICIAR ROTINA REMOÇÃO DE EVENTOS E250 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			QLGroupFilter groupFilterE250 = new QLGroupFilter("AND");
			groupFilterE250.addFilter(new QLEqualsFilter("habilitado", Boolean.TRUE));
			groupFilterE250.addFilter(new QLEqualsFilter("tratarE250", Boolean.TRUE));

			List<NeoObject> neoObjectsE250 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMARegional"), groupFilterE250);
			if ((neoObjectsE250 != null) && (!neoObjectsE250.isEmpty()))
			{
				for (NeoObject neoObject : neoObjectsE250)
				{
					EntityWrapper neoObjectRegional = new EntityWrapper(neoObject);
					
					String regional = "'" + (String) neoObjectRegional.getValue("regional") + "'";
					
					//Boolean utilizarPeriodo = (Boolean) neoObjectRegional.getValue("utilizarPeriodo");
					
					log.warn("##### EXECUTAR ROTINA REMOÇÃO DE EVENTOS E250 Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					
					GregorianCalendar inicio = (GregorianCalendar) neoObjectRegional.findField("E250Inicio").getValue();
					GregorianCalendar fim = (GregorianCalendar) neoObjectRegional.findField("E250Fim").getValue();
					    
					contadorE250 = this.finalizaEventoPeriodo(regional, EVT_E250, inicio, fim);
					    
					
					if (contadorE250 > 0)
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E250//"
					    	+ ""
					    	+ "/6///////////////////////////////////////////////["
					    	+ "////// - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE250 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
					else
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E250 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorE250 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
				}
				

			}
			
			else
			{
				log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E250 - Não foram encontradas regionais habilitadas e/ou com evento E250 marcados para finalização - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			
			//*********************************************************************************************************************************
			//trata Eventos XXX1 
			log.warn("##### INICIAR ROTINA REMOÇÃO DE EVENTOS XXX1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			QLGroupFilter groupFilterXXX1 = new QLGroupFilter("AND");
			groupFilterXXX1.addFilter(new QLEqualsFilter("habilitado", Boolean.TRUE));
			groupFilterXXX1.addFilter(new QLEqualsFilter("tratarXXX1", Boolean.TRUE));

			List<NeoObject> neoObjectsXXX1 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMARegional"), groupFilterXXX1);
			if ((neoObjectsXXX1 != null) && (!neoObjectsXXX1.isEmpty()))
			{
				for (NeoObject neoObject : neoObjectsXXX1)
				{
					EntityWrapper neoObjectRegional = new EntityWrapper(neoObject);
					
					String regional = "'" + (String) neoObjectRegional.getValue("regional") + "'";
					
					//Boolean utilizarPeriodo = (Boolean) neoObjectRegional.getValue("utilizarPeriodo");
					
					log.warn("##### EXECUTAR ROTINA REMOÇÃO DE EVENTOS XXX1 Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					
					    GregorianCalendar inicio = (GregorianCalendar) neoObjectRegional.findField("XXX1Inicio").getValue();
					    GregorianCalendar fim = (GregorianCalendar) neoObjectRegional.findField("XXX2Fim").getValue();

					    contadorXXX1 = this.finalizaEventoPeriodo(regional, EVT_XXX1, inicio, fim);
					    
					
					
					if (contadorXXX1 > 0)
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E250//"
					    	+ ""
					    	+ "/6///////////////////////////////////////////////["
					    	+ "////// - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorXXX1 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
					else
					{
					    log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS E250 - Regional " + regional + " - GERANDO UM TOTAL DE " + contadorXXX1 + " EVENTOS TRATADOS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
				}
				

			}
			
			else
			{
				log.warn("##### FINALIZAR ROTINA REMOÇÃO DE EVENTOS XXX1 - Não foram encontradas regionais habilitadas e/ou com evento XXX1 marcados para finalização - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}

			for (NeoObject neoObjectReg : neoObjectsE130)
			{
				EntityWrapper entityWrapper = new EntityWrapper(neoObjectReg);
				entityWrapper.findField("habilitado").setValue(Boolean.FALSE);
				entityWrapper.findField("tratarE130").setValue(Boolean.FALSE);
				entityWrapper.findField("utilizarPeriodo").setValue(Boolean.FALSE);
				PersistEngine.persist(neoObjectReg);
			}

			for (NeoObject neoObjectReg : neoObjectsE301)
			{
				EntityWrapper entityWrapper = new EntityWrapper(neoObjectReg);
				entityWrapper.findField("habilitado").setValue(Boolean.FALSE);
				entityWrapper.findField("tratarE301").setValue(Boolean.FALSE);
				entityWrapper.findField("utilizarPeriodo").setValue(Boolean.FALSE);
				PersistEngine.persist(neoObjectReg);
			}

			for (NeoObject neoObjectReg : neoObjectsE131)
			{
				EntityWrapper entityWrapper = new EntityWrapper(neoObjectReg);
				entityWrapper.findField("habilitado").setValue(Boolean.FALSE);
				entityWrapper.findField("tratarE131").setValue(Boolean.FALSE);
				entityWrapper.findField("utilizarPeriodo").setValue(Boolean.FALSE);
				PersistEngine.persist(neoObjectReg);
			}

			for (NeoObject neoObjectReg : neoObjectsE134)
			{
				EntityWrapper entityWrapper = new EntityWrapper(neoObjectReg);
				entityWrapper.findField("habilitado").setValue(Boolean.FALSE);
				entityWrapper.findField("tratarE134").setValue(Boolean.FALSE);
				entityWrapper.findField("utilizarPeriodo").setValue(Boolean.FALSE);
				PersistEngine.persist(neoObjectReg);
			}
			
			for (NeoObject neoObjectReg : neoObjectsE250)
			{
				EntityWrapper entityWrapper = new EntityWrapper(neoObjectReg);
				entityWrapper.findField("habilitado").setValue(Boolean.FALSE);
				entityWrapper.findField("tratarE250").setValue(Boolean.FALSE);
				entityWrapper.findField("utilizarPeriodo").setValue(Boolean.FALSE);
				PersistEngine.persist(neoObjectReg);
			}
			
			for (NeoObject neoObjectReg : neoObjectsXXX1)
			{
				EntityWrapper entityWrapper = new EntityWrapper(neoObjectReg);
				entityWrapper.findField("habilitado").setValue(Boolean.FALSE);
				entityWrapper.findField("tratarXXX1").setValue(Boolean.FALSE);
				entityWrapper.findField("utilizarPeriodo").setValue(Boolean.FALSE);
				PersistEngine.persist(neoObjectReg);
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS E130: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("[" + key + "] ##### ERRO ROTINA REMOÇÃO DE EVENTOS E130: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
	}

	private void finalizaEventosFilhos(String historico){
	    
	    Connection conn = null;
	    PreparedStatement pstm = null;

	    StringBuilder sql = new StringBuilder();
	    sql.append(" UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = 11010, CD_MOTIVO_ALARME = 55, TX_OBSERVACAO_FECHAMENTO = 'Fechado por rotina de controle CM.' WHERE CD_HISTORICO_PAI = ? ");
	    
	    try {
		conn = PersistEngine.getConnection("SIGMA90");
		pstm = conn.prepareStatement(sql.toString());

		pstm.setInt(1, Integer.parseInt(historico));

		pstm.executeUpdate();

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, null);
	    }
	}
	
	public int finalizaEventoPeriodo(String regional, String tipoEvento, GregorianCalendar inicio, GregorianCalendar fim)
	{
		
		int contador = 0;
		Connection conn = null;
		PreparedStatement pstm = null;
		PreparedStatement preparedStatementUpdate = null;
		ResultSet rs = null;
		List<String> listaHistoricos = null;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();
			
			String dataIni = NeoDateUtils.safeDateFormat(inicio, "yyyy-MM-dd HH:mm:ss");
			
			String dataFim = NeoDateUtils.safeDateFormat(fim, "yyyy-MM-dd HH:mm:ss");

			sql.delete(0, sql.length());
			sql.append(" SELECT h.CD_HISTORICO ");
			sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE");
			sql.append(" INNER JOIN ROTA R WITH (NOLOCK) ON R.CD_ROTA = c.ID_ROTA");
			
			if (tipoEvento.equals(EVT_XXX1)){
			    sql.append(" WHERE  h.FG_STATUS IN (1,5)  AND h.CD_HISTORICO_PAI IS NULL AND ");			    
			}else{
			    sql.append(" WHERE  h.FG_STATUS = 1  AND h.CD_HISTORICO_PAI IS NULL AND ");			    
			}
			
			
			
			sql.append(" h.DT_PROCESSADO BETWEEN '"+dataIni+"' AND '"+dataFim+"' ");
			sql.append(" AND h.CD_EVENTO LIKE '" + tipoEvento + "'");
						
			if (!tipoEvento.equals(EVT_E250) && !tipoEvento.equals(EVT_XXX1)){
			    sql.append("  AND NOT EXISTS (SELECT * FROM HISTORICO Hs WHERE Hs.CD_HISTORICO_PAI = h.CD_HISTORICO) ");
			}
			
			if ((regional != null) && (regional != "") && (!regional.isEmpty()))
			{
				sql.append(" AND SUBSTRING(R.NM_ROTA, 1, 3) IN (" + regional + ")");
			}
			conn.setAutoCommit(false);

			pstm = conn.prepareStatement(sql.toString());

			rs = pstm.executeQuery();

			listaHistoricos = new ArrayList<String>();
			contador = 0;
			while (rs.next())
			{
				String historico = rs.getString("CD_HISTORICO");
				if ((historico != null) && (!historico.isEmpty()))
				{
					listaHistoricos.add(historico);
				}
				contador++;
			}
			String his;
			if ((listaHistoricos != null) && (!listaHistoricos.isEmpty()))
			{
				String updateSQLPai = " UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = 11010, CD_MOTIVO_ALARME = 55, TX_OBSERVACAO_FECHAMENTO = 'Fechado por rotina de controle CM.' WHERE CD_HISTORICO = ? ";
				for (@SuppressWarnings("rawtypes")
				Iterator localIterator2 = listaHistoricos.iterator(); localIterator2.hasNext();)
				{
					his = (String) localIterator2.next();
					
					if (tipoEvento.equals(EVT_E250) || tipoEvento.equals(EVT_XXX1)){
					    this.finalizaEventosFilhos(his);					  
					}
									
					preparedStatementUpdate = conn.prepareStatement(updateSQLPai.toString());
					preparedStatementUpdate.setString(1, his);
					preparedStatementUpdate.executeUpdate();
				}
			}
			conn.commit();
			conn.setAutoCommit(true);
			return contador;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			contador = 0;
			log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS E130: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
			try
			{
				OrsegupsUtils.closeConnection(null, preparedStatementUpdate, null);
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
				log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS E130: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			return contador;
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(null, preparedStatementUpdate, null);
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS E130: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
		}

	}
	
	/**
	 * Finaliza Eventos selecionados nas regionais sigma habilitadas para limpeza
	 * 
	 * @param regional
	 * @param horas
	 * @param tipoEvento
	 * @return
	 */
	public int finalizaEvento(String regional, double horas, String tipoEvento)
	{
		
		int contador = 0;
		Connection conn = null;
		PreparedStatement pstm = null;
		PreparedStatement preparedStatementUpdate = null;
		ResultSet rs = null;
		List<String> listaHistoricos = null;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sql = new StringBuilder();

			sql.delete(0, sql.length());
			sql.append(" SELECT h.CD_HISTORICO ");
			sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE");
			sql.append(" INNER JOIN ROTA R WITH (NOLOCK) ON R.CD_ROTA = c.ID_ROTA");
			sql.append(" WHERE  h.FG_STATUS = 1  AND h.CD_HISTORICO_PAI IS NULL AND ");
			sql.append(" h.DT_PROCESSADO < DATEADD(HOUR, -" + horas + ",GETDATE())");
			sql.append(" AND h.CD_EVENTO LIKE '" + tipoEvento + "' AND NOT EXISTS (SELECT * FROM HISTORICO Hs WHERE Hs.CD_HISTORICO_PAI = h.CD_HISTORICO) ");
			if ((regional != null) && (regional != "") && (!regional.isEmpty()))
			{
				sql.append(" AND SUBSTRING(R.NM_ROTA, 1, 3) IN (" + regional + ")");
			}
			conn.setAutoCommit(false);

			pstm = conn.prepareStatement(sql.toString());

			rs = pstm.executeQuery();

			listaHistoricos = new ArrayList<String>();
			contador = 0;
			while (rs.next())
			{
				String historico = rs.getString("CD_HISTORICO");
				if ((historico != null) && (!historico.isEmpty()))
				{
					listaHistoricos.add(historico);
				}
				contador++;
			}
			String his;
			if ((listaHistoricos != null) && (!listaHistoricos.isEmpty()))
			{
				String updateSQLPai = " UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = 11010, CD_MOTIVO_ALARME = 55, TX_OBSERVACAO_FECHAMENTO = 'Fechado por rotina de controle CM.' WHERE CD_HISTORICO = ? ";
				for (@SuppressWarnings("rawtypes")
				Iterator localIterator2 = listaHistoricos.iterator(); localIterator2.hasNext();)
				{
					his = (String) localIterator2.next();
					
					if (tipoEvento.equals(EVT_E250) || tipoEvento.equals(EVT_XXX1)){
					    this.finalizaEventosFilhos(his);					  
					}
					
					System.out.println(tipoEvento + " - CD_EVENTO: " + his);
					preparedStatementUpdate = conn.prepareStatement(updateSQLPai.toString());
					preparedStatementUpdate.setString(1, his);
					preparedStatementUpdate.executeUpdate();
				}
			}
			conn.commit();
			conn.setAutoCommit(true);
			return contador;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			contador = 0;
			log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS E130: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
			try
			{
				OrsegupsUtils.closeConnection(null, preparedStatementUpdate, null);
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
				log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS E130: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			return contador;
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(null, preparedStatementUpdate, null);
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA REMOÇÃO DE EVENTOS E130: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
		}

	}

	public String montaStringFiltroRegional(List<NeoObject> neoObjects)
	{
		String regional = "";
		int contador = 0;

		for (NeoObject neoObject : neoObjects)
		{
			EntityWrapper neoObjectRegional = new EntityWrapper(neoObject);
			if (contador > 0)
			{
				regional = regional + ",";
			}
			regional = regional + "'" + (String) neoObjectRegional.getValue("regional") + "'";

			contador++;
		}
		return regional;
	}
}
