package com.neomind.fusion.custom.orsegups.seventh.dto;

import java.util.ArrayList;
import java.util.List;

public class SeventhCameraIDsDTO {
    
    private String servidor;
    private List<String> ids;

    public String getServidor() {
	return servidor;
    }

    public void setServidor(String servidor) {
	this.servidor = servidor;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }
    
    public SeventhCameraIDsDTO(){
	this.ids = new ArrayList<>();
    }
    
    
    
}
