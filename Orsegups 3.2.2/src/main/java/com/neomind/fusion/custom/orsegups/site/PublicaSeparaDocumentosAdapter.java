package com.neomind.fusion.custom.orsegups.site;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.RE.RePDF_2_1_7;
import com.neomind.fusion.custom.orsegups.ret.RetPDF_2_1_7;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;


public class PublicaSeparaDocumentosAdapter implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper wrapper,	Activity activity) {
		
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		NeoObject tipo = (NeoObject) wrapper.findValue("tipoDocumento");
		Integer codigo = 0;
		try{
			
			String cpt = NeoUtils.safeOutputString(wrapper.findValue("cpt"));
			cpt = cpt.trim();
			if ( !(cpt.length() == 7 && cpt.indexOf("/")==2) ){
				throw new Exception("O formato da Competência deve ser MM/AAAA -> Exemplo: 04/2015");
			}
			
			if (tipo != null){
				EntityWrapper wTipo = new EntityWrapper(tipo);
				codigo = NeoUtils.safeInteger( NeoUtils.safeOutputString(wTipo.findValue("codTipoDocumento")) );
				
				switch(codigo) {
				
				case 6: // CONECSOCIAL
					publicaCONECSOCIAL(wrapper);
					break;
				case 7: // RE - 02
					publicaRE(origin, wrapper, activity, true, true, true);
					break;
				case 8: // RET - 30
					publicaRET(origin, wrapper, activity, true);
					break;
				case 9:	// (arquivo por empresa) CND Trabalhista - 46
					publicaCNDTrabalhista(wrapper);
					break;
				case 10: // (Vem da RET) RTRET (Relatorio analitico RET) - 53
					throw new Exception("Este documento já é publicado automaticamente ao fazer o processamento da RET");
				case 11: // (Arquivo avulso por empresa, filial e cliente )Relatorio Analitico GPS - 50
					publicaRAGPS(wrapper);
					break;
				case 12: // (Arquivo avulso por empresa, filial e cliente ) INSS Deposito Judicial - 45
					publicaINSSDJ(wrapper);
					break;
				case 13: // (Arquivo avulso por empresa, filial e cliente ) Relatorio Analitico GRF - 49
					publicaRAGRF(wrapper);
					break;
				case 14: // (Vem da RE) Resumo de Fechamento - Empresa - 51
					throw new Exception("Este documento já é publicado automaticamente ao fazer o processamento da RE");
				case 15: // (Vem da RE) Resumo de Fechamento - Empresa FGTS - 52
					throw new Exception("Este documento já é publicado automaticamente ao fazer o processamento da RE");
				case 16: // (Vem da RE) Resumo das informacoes a previdencia por tomador - 56
					throw new Exception("Este documento já é publicado automaticamente ao fazer o processamento da RE");
				case 17: // CND Estadual
					publicaCNDEST(wrapper);
					break;	
				case 19:
					publicaRE2020(origin, wrapper, activity, true, true, true);
					break;
					
				default:
					throw new Exception("A publicação deste tipo de documento não é suportada neste fluxo.");
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new  WorkflowException("["+key+"] Erro ao avançar o fluxo. " + e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}
	
	public void publicaRE2020(Task origin, EntityWrapper wrapper,	Activity activity, boolean separaResumoFechamento, boolean separaResumoFechamentoFGT, boolean separaResumoInfoPrevidencia ) throws Exception {
		try{
			
			String nomeEmpresa = "Orsegups";
			String usuario = PortalUtil.getCurrentUser().getCode();
			
			String competencia = NeoUtils.safeOutputString(wrapper.findValue("cpt"));
			NeoFile arquivoFusion = (NeoFile) wrapper.findValue("arquivo");
			
			File arquivo = new File(arquivoFusion.getAbsolutePath());
			RePDF_2_1_7 leitorPdf = new RePDF_2_1_7();
			leitorPdf.leituraPdf2020(new FileInputStream(arquivo), competencia, nomeEmpresa, usuario, separaResumoFechamento, separaResumoFechamentoFGT,separaResumoInfoPrevidencia);
		}catch(Exception e){
			throw new Exception("Erro ao fatiar os documentos de RE para envio ao site"+e.getMessage());
		}
	}
	
	public void publicaRE(Task origin, EntityWrapper wrapper,	Activity activity, boolean separaResumoFechamento, boolean separaResumoFechamentoFGT, boolean separaResumoInfoPrevidencia ) throws Exception {
		try{
			NeoUser usuario = PortalUtil.getCurrentUser();
			String strUsuario = "";
			
			strUsuario = usuario.getCode();
			
			String cpt = NeoUtils.safeOutputString(wrapper.findValue("cpt"));
			NeoFile file = (NeoFile) wrapper.findValue("arquivo");
			
			System.out.println("Caminho:"+file.getAbsolutePath());
			
			
			File f = new File(file.getAbsolutePath());
			//String dataCpt = "03/2015";
			String codEmp = "Orsegups";
			RePDF_2_1_7 t = new RePDF_2_1_7();
			t.leituraPdf( new FileInputStream(f), cpt, codEmp, strUsuario, separaResumoFechamento, separaResumoFechamentoFGT,separaResumoInfoPrevidencia);
			//if(true) throw new WorkflowException("bosta ocorrida");
		}catch(Exception e){
			throw new Exception("Erro ao fatiar os documentos de RE para envio ao site"+e.getMessage());
		}
	}
	
	public void publicaRET(Task origin, EntityWrapper wrapper,	Activity activity, boolean separaRtret) {
		RetPDF_2_1_7 t = null;
		try{
			NeoUser usuario = PortalUtil.getCurrentUser();
			String strUsuario = "";
			
			strUsuario = usuario.getCode();
			
			String cpt = NeoUtils.safeOutputString(wrapper.findValue("cpt"));
			NeoFile file = (NeoFile) wrapper.findValue("arquivo");
			
			System.out.println("Caminho:"+file.getAbsolutePath());
			
			
			File f = new File(file.getAbsolutePath());
			//String dataCpt = "03/2015";
			String codEmp = "Orsegups";
			t = new RetPDF_2_1_7();
			t.leituraPdf( new FileInputStream(f), cpt, codEmp, strUsuario, separaRtret);
			//if(true) throw new WorkflowException("bosta ocorrida");
		}catch(Exception e){
			if (t.isResumoRet()){
				throw new WorkflowException("Erro ao enviar documento de Resumo RET para ao site"+e.getMessage());
			}else{
				if (separaRtret) 
					throw new WorkflowException("Erro ao processar Resumo RET"+e.getMessage());
				else 
					throw new WorkflowException("Erro ao fatiar os documentos de RET para envio ao site"+e.getMessage());
			}
		}
	}
	
	public void publicaCNDTrabalhista(EntityWrapper wrapper) throws Exception {
		publicaDiretoSite(wrapper, 46, "CNDTRAB" );
	}
	
	public void publicaCNDEST(EntityWrapper wrapper) throws Exception {
		publicaDiretoSite(wrapper, 5, "CNDEST" );
	}
	
	public void publicaCONECSOCIAL(EntityWrapper wrapper) throws Exception {
		publicaDiretoSite(wrapper, 23, "CONECSOCIAL" );
	}
	
	public void publicaRAGPS(EntityWrapper wrapper) throws Exception {
		publicaDiretoSite(wrapper, 50, "RAGPS" );
	}
	
	public void publicaRAGRF(EntityWrapper wrapper) throws Exception  {
		publicaDiretoSite(wrapper, 49, "RAGRF" );
	}
	
	public void publicaINSSDJ(EntityWrapper wrapper) throws Exception  {
		publicaDiretoSite(wrapper, 45, "INSSDJ" );
	}
	
	
	public void publicaDiretoSite(EntityWrapper wrapper, int codDoc, String pasta) throws Exception{
		try{
			NeoUser usuario = PortalUtil.getCurrentUser();
			String strUsuario = "";
			
			strUsuario = usuario.getCode();
			
			String cpt = NeoUtils.safeOutputString(wrapper.findValue("cpt"));
			Long empresa = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("empresa")));
			Long filial = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("filial")));
			
			NeoFile file = (NeoFile) wrapper.findValue("arquivo");
			
			System.out.println("Caminho:"+file.getAbsolutePath());
			
			
			File f = new File(file.getAbsolutePath());
			//String dataCpt = "03/2015";
			String codEmp = "Orsegups";
			
			ArrayList<Long> cnpjs = new ArrayList<Long>();
			cnpjs.addAll(getCGCCPFs(empresa, filial, codDoc));
			for (Long cnpj : cnpjs){
				criarArquivoPDFNaPasta(codEmp, cpt, f, "\\\\fsoofs01\\f$\\Site"+(strUsuario.equals("danilo.silva")? "\\teste":"")+"\\",  empresa , filial,  pasta, f.exists(), NeoUtils.safeOutputString(cnpj) );
			}
			
			
			//if(true) throw new WorkflowException("bosta ocorrida");
		}catch(Exception e){
			throw new Exception("Erro ao processar arquivo codigo "+codDoc+" da pasta "+pasta+" msg: "+e.getMessage());
		}
	}
	
	
	

	public static String leadingZeros(String s, int length) {
    	String retorno = null;
    	try {
    		if (s.length() >= length) 
    			return s;
    		else 
    			retorno = String.format("%0" + (length-s.length()) + "d%s", 0, s);
    	}catch(Exception ex) {
    			ex.printStackTrace();
    	}
		return retorno;
    }
	
	//public static boolean criarArquivoPDFNaPasta(String codEmp, String dataCpt, String cgccpf, File arquivo, String outFile, boolean encontrado, Long empfil[], String pasta) {
	  public void criarArquivoPDFNaPasta(String codEmp, String cpt, File arquivo, String outFile, Long empresa, Long filial, String pasta, boolean encontrado, String cnpj) {
		boolean retorno = true;
    	try {
    		String naoEncontrado = "";
    		if (!encontrado){
    			naoEncontrado = "\\naoEncontrado\\";
    		}
    		outFile = outFile+naoEncontrado+codEmp+"\\"+leadingZeros(NeoUtils.safeOutputString(empresa),2)+"\\"+filial+"\\"+NeoUtils.safeLong(cnpj)+"\\"+pasta+"\\"+cpt.replace("/", "");

    		//if (true) throw new Exception("teste");
    		
    		File file = new File(outFile);
    		if (!file.exists())
    			 file.mkdirs();
			outFile = outFile+"\\"+NeoUtils.safeLong(cnpj) + ".pdf";
			System.out.println(outFile);
			
			if (arquivo.isFile() && !arquivo.isHidden())
			{
				String timeMills = String.valueOf(new GregorianCalendar().getTimeInMillis());
				File arquivoNovo = new File(outFile);
				File source = arquivo;
				File destination = file;

				if (!destination.exists())
				{
					destination.mkdirs();
				}
				if (!arquivoNovo.exists())
				{
					arquivoNovo.createNewFile();
				}

				FileChannel sourceChannel = null;
				FileChannel destinationChannel = null;

				try
				{
					sourceChannel = new FileInputStream(source).getChannel();
					destinationChannel = new FileOutputStream(arquivoNovo).getChannel();
					sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
					//documentoWrapper.findField("siteParametrosUploadCliente.caminhoArquivos").setValue(arquivoNovo.getAbsolutePath());
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new WorkflowException("Erro ao executar caminho e destino do arquivo! " + e.getMessage());
				}
				finally
				{
					if (sourceChannel != null && sourceChannel.isOpen())
						sourceChannel.close();
					if (destinationChannel != null && destinationChannel.isOpen())
						destinationChannel.close();
					//PersistEngine.persist(documentos);
				}

			}
			
			
		} catch (FileNotFoundException e) {
			retorno = false;
			e.printStackTrace();
		} catch (IOException e) {
			retorno = false;
			e.printStackTrace();
		} catch (Exception ex){
			retorno = false;
			ex.printStackTrace();
		}
		//return retorno;
	}
	  
	  /**
	   * lista todos cpfs/cnpjs que tenham um documento com código Especifico.
	   * @param codEmp - codigo da empresa no sapiens
	   * @param codFil - codigo da filial no sapiens
	   * @param tipDoc - codigo do documento no sapiens
	   * @return
	   */
	  public static ArrayList<Long> getCGCCPFs(Long codEmp, Long codFil, int tipDoc){
		  	ArrayList<Long> retorno = new ArrayList<Long>();
			
			try{
				StringBuilder sql = new StringBuilder();
				
				sql.append(" select distinct(cli.CgcCpf) from e085cli cli "); 
				sql.append(" join usu_t160ctr ctr on (ctr.usu_codcli = cli.codcli  and ctr.usu_sitctr = 'A' ) "); 
				sql.append(" join usu_t160doc doc on (ctr.usu_numctr = doc.usu_numctr and ctr.usu_codemp = doc.usu_codemp and ctr.usu_codfil = doc.usu_codfil and doc.usu_coddoc = ? ) "); 
				sql.append(" where ctr.usu_codemp = ? and ctr.usu_codfil = ? ");

				Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
				query.setParameter(1, tipDoc);
				query.setParameter(2, codEmp);
				query.setParameter(3, codFil);
				
				Collection<Object> resultList = query.getResultList();
				if (resultList != null && resultList.size() >0) {
					for (Object res : resultList){
						Object obj = (Object) res;
						retorno.add( NeoUtils.safeLong(NeoUtils.safeOutputString(obj)) );
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
				return null;
			}
			return retorno;
		}
	
	

}
