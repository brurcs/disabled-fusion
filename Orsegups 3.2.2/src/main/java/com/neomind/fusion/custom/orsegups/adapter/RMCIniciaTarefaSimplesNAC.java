package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RMCIniciaTarefaSimplesNAC implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "";
		try
		{
			String solicitante = origin.getUser().getCode();
			
			String executor = "";
			NeoPaper papelExecutor = new NeoPaper();
			papelExecutor = OrsegupsUtils.getPaper("TratarInconsistenciaNACRMC");
			if(papelExecutor != null && papelExecutor.getAllUsers() != null && !papelExecutor.getAllUsers().isEmpty())
			{
				for (NeoUser user : papelExecutor.getUsers())
				{
					executor = user.getCode();
					break;
				}
			}
		
			String code = "";
			
			String titulo = "Tratar Inconsistência de RMC - Referente ao Contrato " + (String) processEntity.findValue("numeroContrato");
			String descricao = (String) processEntity.findValue("obsAlmoxarife");
			String origem = "1";
			String avanca = "sim";
			
			GregorianCalendar prazo = new GregorianCalendar();
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
			prazo.set(Calendar.HOUR_OF_DAY, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);
			
			IniciarTarefaSimples iniciaTarefaSimples = new IniciarTarefaSimples();
			code = iniciaTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo);
			
			String obsAlmoxarife = (String) processEntity.findValue("obsAlmoxarife");
			obsAlmoxarife = obsAlmoxarife + "<br> Aberto a Tarefa Simples " + code + ", para tratar as inconsistências desta RMC.";
			processEntity.setValue("obsAlmoxarife", obsAlmoxarife);
		}		
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}

}
