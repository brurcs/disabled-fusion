package com.neomind.fusion.custom.orsegups.adapter.q004;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;


public class HistoricoAtividadesConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		EntityWrapper wrapper = new EntityWrapper(field.getForm().getObject());
		
		List<NeoObject> registroAtividades = wrapper.findGenericValue("registroAtividades");
		StringBuilder textoTable = new StringBuilder();
		
		if (NeoUtils.safeIsNotNull(registroAtividades) && !registroAtividades.isEmpty())
		{
			textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			textoTable.append("			<tr style=\"cursor: auto\">");
			textoTable.append("				<th style=\"cursor: auto\">Responsável</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Descrição</th>");
			textoTable.append("				<th style=\"cursor: auto\">Data da Ação</th>");
			textoTable.append("				<th style=\"cursor: auto\">Atividade</th>");
			textoTable.append("			</tr>");
			textoTable.append("			<tbody>");	
			
			for (NeoObject obj : registroAtividades)
			{
				EntityWrapper registroWrapper = new EntityWrapper(obj);
				NeoUser responsavel = (NeoUser) registroWrapper.findValue("responsavel");
				String descricao = (String) registroWrapper.findValue("descricao");
				GregorianCalendar dataFinal = (GregorianCalendar) registroWrapper.findValue("dataFinal");
				GregorianCalendar prazo = (GregorianCalendar) registroWrapper.findValue("prazo");
				String atividade = (String) registroWrapper.findValue("atividade");
				
				String dataAcao = NeoUtils.safeDateFormat(dataFinal);
				String grupo = "";
				
				if (responsavel != null && responsavel.getGroup() != null && responsavel.getGroup().getName() != null)
					grupo = responsavel.getGroup().getName();
				
				textoTable.append("		<tr>");
				textoTable.append("			<td title='" + grupo + "'>" + responsavel.getFullName() + "</td>");
				textoTable.append("			<td style=\"white-space: normal\">" + descricao + "</td>");
				textoTable.append("			<td>" + dataAcao + "</td>");
				textoTable.append("			<td>" + atividade + "</td>");
				textoTable.append("		</tr>");
			}
			
			textoTable.append("			</tbody>");
			textoTable.append("		</table>");
		}
		String txtFinal = textoTable.toString();
		
		txtFinal = txtFinal.replaceAll("\n", " ").replaceAll("\r", " ").replaceAll("&Acirc;&#160;", " ").replaceAll("Â&nbsp;", "");
		return txtFinal;
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}