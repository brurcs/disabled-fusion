package com.neomind.fusion.custom.orsegups;

import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityInfoCache;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.LoginControl;
import com.neomind.util.NeoUtils;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


@WebServlet(name="LoginFornecedor", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.LoginFornecedor"})
public class LoginFornecedor extends HttpServlet
{
  private static final Log log = LogFactory.getLog(ServletEnvioCurriculo.class);
  private static final long serialVersionUID = 1L;

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    doGet(request, response);
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    String USU_CodFor = request.getParameter("inUsuario");
    String USU_SenUsu = request.getParameter("inPass");
    Long USU_CodForLogin = null;
    try
    {
      USU_CodForLogin = Long.valueOf(Long.parseLong(USU_CodFor.trim()));
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
    QLGroupFilter filterUser = new QLGroupFilter("AND");
    filterUser.addFilter(new QLEqualsFilter("usu_codfor", USU_CodForLogin));
    filterUser.addFilter(new QLEqualsFilter("usu_senusu", USU_SenUsu));
    filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));

    request.setAttribute("user", "convidadoCurriculo");
    request.setAttribute("pass", "guestCV0rs3gup5");

    PortalUtil.getCurrentSession().setMaxInactiveInterval(1000000000);
    LoginControl.getInstance().login(request, response);

    ExternalEntityInfo infoViewUsuarios = (ExternalEntityInfo)EntityRegister.getInstance().getCache().getByString("VFUSUSUSAP");
    List user = PersistEngine.getObjects(infoViewUsuarios.getEntityClass(), filterUser);
    if (!user.isEmpty())
    {
      USU_CodFor = NeoUtils.encodeDecodablePassword(USU_CodForLogin.toString());
      response.sendRedirect(PortalUtil.getBaseURL() + "portal_orsegups/listagemCotacao.jsp?USU_CodFor=" + USU_CodFor);
    }
    else
    {
      String URL = PortalUtil.getBaseURL() + "portal_orsegups/index.jsp";
      response.sendRedirect(URL);
    }
  }
}