package com.neomind.fusion.custom.orsegups.rsc.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCTipoContatoVO;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;

/**
 * Classe criada para centralizar todas aberturas de RSC no modelo novo, inclusivda as do site.
 * @author danilo.silva
 *
 */
public class RSCHelper
{
	
	private static final Log log = LogFactory.getLog(RSCHelper.class);
	
	/**
	 * Abre uma tarefa C027 - Registro de Solcitação do cliente 
	 * 
	 * @param rsc - Informações sobre a RSC a ser aberta.
	 * @return
	 * @throws Exception
	 */
	public static String abrirRSC(RSCVO rsc, String papelSolicitante, Boolean enviaEmail) throws Exception
	{
		return abrirRSC(rsc, papelSolicitante, enviaEmail, false, false);
	}
	
	
	public static String abrirRSC(RSCVO rsc, String papelSolicitante, Boolean enviaEmail, Boolean avancaPrimeiraAtividade, Boolean assign) throws Exception
	{
		try
		{
			System.out.println("Abertura de RSC - Processando requisição ->  " + rsc);
			
			//Cria Variaveis para diferenciar RSC de RRC
			String eform = "RSCRelatorioSolicitacaoClienteNovo";
			String workflow = "C027 - RSC - Relatório de Solicitação de Cliente Novo";
			String fieldTelefone = "telefones";
			String fieldContato = "contato";
			String fieldMensagem = "descricao";
			//String valueSolicitante = "SITERESPONSAVELRSCCADASTRAL";

			RSCTipoContatoVO rscTipoContato = consultaTipoContatoRSC(rsc.getTipoContato());
			String valueMensagem = "Tipo do Contato: " + (rscTipoContato != null? rscTipoContato.getDescricao() : "") + " \nDescrição: ";
			

			//Cria Instancia do Eform Principal
			InstantiableEntityInfo infoSolicitacao = AdapterUtils.getInstantiableEntityInfo(eform);
			NeoObject noSolicitacao = infoSolicitacao.createNewInstance();
			EntityWrapper solicitacaoWrapper = new EntityWrapper(noSolicitacao);

			//Busca Origem da Solicitacao
			QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", rsc.getOrigemSolicitacao());// Site Orsegups
			NeoObject origemObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("RRCOrigem"), filterOrigem);

			solicitacaoWrapper.setValue("prazoRegistrarRSC",OrsegupsUtils.getNextWorkDay(rsc.getPrazo())); //RSCANTIGA


			//Seta os valores
			solicitacaoWrapper.setValue("origem", origemObj);
			//solicitacaoWrapper.setValue("cidade", rsc.getCidade());
			//solicitacaoWrapper.setValue("bairro", rsc.getBairro());
			solicitacaoWrapper.setValue("email", rsc.getEmail());
			solicitacaoWrapper.setValue(fieldContato, rsc.getNome());
			solicitacaoWrapper.setValue(fieldTelefone, rsc.getTelefone());
			solicitacaoWrapper.setValue(fieldMensagem, valueMensagem + rsc.getMensagem());
			solicitacaoWrapper.setValue("responsavelAtendimento", rsc.getResponsavelAtendimento());
			solicitacaoWrapper.setValue("tipoSolicitacao", rsc.getTipoSolicitacao());
			solicitacaoWrapper.setValue("regional", rsc.getRegional());
			solicitacaoWrapper.setValue("idCtrSapiens", rsc.getIdCtrSapiens());
			
			QLGroupFilter filterUser = new QLGroupFilter("AND");
			filterUser.addFilter(new QLEqualsFilter("usu_codcli", rsc.getCodCli()));
			filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));

			//Busca Cliente
			List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);

			NeoObject clienteObj = null;

			if (users != null)
			{
				EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
				Long codcli = (Long) usuarioCliente.getValue("usu_codcli");

				QLEqualsFilter filterCliente = new QLEqualsFilter("codcli", codcli);
				clienteObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCliente);
			}

			if (clienteObj != null)
			{
				solicitacaoWrapper.findField("clienteSapiens").setValue(clienteObj);
			}

			//Salva o Eform
			PersistEngine.persist(noSolicitacao);

			// Cria Instancia do WorkFlow
			QLEqualsFilter equal = new QLEqualsFilter("name", workflow);
			List<ProcessModel> processModelList = (List<ProcessModel>) PersistEngine.getObjects(ProcessModel.class, equal, " neoId DESC ");
			ProcessModel processModel = processModelList.get(0);

			NeoUser solicitante = buscaSolicitante(papelSolicitante);

			/**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do
			 *         Fusion
			 * @date 11/03/2015
			 */

			
			final WFProcess processo = WorkflowService.startProcess(processModel, noSolicitacao, assign, solicitante);
			log.info("Solicitacao aberta " + processo.getCode());
			try
			{
				//new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante, avancaPrimeiraAtividade);
			}
			catch (Exception e)
			{
				throw new Exception("Erro ao avançar primeira atividade.", e);
			}
			
			String tarefa = null;
			tarefa = processo.getCode();
			
			// Comentado momentaneamente pois as tarefas não estavam abrindo em pool como deveriam.
			
			/*try{
				tarefa = OrsegupsWorkflowHelper.iniciaProcesso(processModel, noSolicitacao, assign, solicitante, false, null);
			}catch(Exception e){
				throw new Exception("Erro ao criar tarefa.", e);
			}*/

			/**
			 * FIM ALTERAÇÕES - NEOMIND
			 */

			if (enviaEmail){
				disparaEmails(rsc, tarefa);
			}
			return tarefa;

		}
		catch (Exception e)
		{
			log.error("Erro ao processar requisição. " + rsc , e);
			throw new Exception("Erro ao processar requisição.", e);
		}
	}

	/**
	 * Envia o e-mail de abertura de RSC
	 * @param rsc
	 * @param tarefa
	 */
	private static void disparaEmails(RSCVO rsc, String tarefa)
	{
		// Dispara email ao cliente
		Map<String, Object> paramMap = new HashMap<String, Object>();

		paramMap.put("tarefa", tarefa);
		paramMap.put("nome", rsc.getNome());
		paramMap.put("tipo", rsc.getTipoContato());
		paramMap.put("mensagem", rsc.getMensagem());

		String[] destinatarios = null;
		if (rsc.getEmail().indexOf(",") > -1)
		{
			destinatarios = rsc.getEmail().split(",");
		}
		else
		{
			destinatarios = rsc.getEmail().split(";");
		}

		for (String mail : destinatarios)
		{
			//mail = "danilo.silva@orsegups.com.br";
			FusionRuntime.getInstance().getMailEngine().sendEMail(mail.trim(), "/portal_orsegups/emailAberturaRSC.jsp", paramMap);
		}
		
	}
	
	
	/**
	 * Consulta o tipo do contato da RSC
	 * @param tipoContato
	 * @return
	 */
	private static RSCTipoContatoVO consultaTipoContatoRSC(String tipoContato)
	{
		RSCTipoContatoVO retorno = null;
		
		//Busca o Tipo de Contato
		QLEqualsFilter filterTipoContato = new QLEqualsFilter("neoId", NeoUtils.safeLong(tipoContato) );
		NeoObject tipoContatoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("tipoContato"), filterTipoContato);
		if (tipoContatoObj != null){
			
			EntityWrapper tipoContatoWrapper = new EntityWrapper(tipoContatoObj);
			Long idTipoContato = (Long) tipoContatoWrapper.getValue("neoId");
			String descricaoTipoContato = (String) tipoContatoWrapper.getValue("tipo");
			retorno = new RSCTipoContatoVO(NeoUtils.safeOutputString(idTipoContato), descricaoTipoContato);
			
		}
		
		return retorno;
		
	}

	/**
	 * Retorna usuário de um papel, se houver mais de um, vai retornar apenas o primeiro encontrado.
	 * @param papelSolicitante
	 * @return
	 */
	private static NeoUser buscaSolicitante(String papelSolicitante)
	{
		//Busca o solicitante
		NeoPaper papel = new NeoPaper();
		NeoUser solicitante = null;
		String executor = "";
		NeoPaper obj = null;
		try{
			obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", papelSolicitante));
			papel = (NeoPaper) obj;

			if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers())
				{
					executor = user.getCode();
					break;
				}

			}

			solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
			
			return solicitante;
			
		}catch(Exception e){
			log.error("Erro ao consultar Papel: "+papelSolicitante,e);
			return solicitante;
		}
		
		
	}

}
