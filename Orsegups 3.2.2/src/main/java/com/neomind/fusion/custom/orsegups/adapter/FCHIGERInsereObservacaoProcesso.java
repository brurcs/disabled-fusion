package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCHIGERInsereObservacaoProcesso implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(FCHIGERInsereObservacaoProcesso.class);


	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		
		List<NeoObject> listaTitulos = (List<NeoObject>) processEntity.findField("listTitulos").getValue();
		
		String codigo = activity.getCode();
		System.out.println("Codigo FCHIGERInsereObservacaoProcesso Inicio: "+codigo);
		
		if (listaTitulos != null && !listaTitulos.isEmpty()) {			
			try	{
				GregorianCalendar dataAux = new GregorianCalendar();
				GregorianCalendar data = new GregorianCalendar( dataAux.get(GregorianCalendar.YEAR), dataAux.get(GregorianCalendar.MONTH), dataAux.get(GregorianCalendar.DAY_OF_MONTH));
				Time hora = new Time(dataAux.get(GregorianCalendar.HOUR_OF_DAY), dataAux.get(GregorianCalendar.MINUTE), dataAux.get(GregorianCalendar.MILLISECOND));
				Long horaInt = (hora.getHours() * 60L ) + hora.getMinutes();
				
				String texto = "Iniciado o Processo " + activity.getProcessName() + " - Tarefa: " + activity.getProcess().getCode();
				
				Long codigoUsuarioSapiens = 100L; //sapienssid 
				
				Long ocorrencia = 22L; //ABERTURA DE PROCESSO
				insereObservacaoTituloSapiens(listaTitulos, data, horaInt, texto, codigoUsuarioSapiens, ocorrencia, codigo);
			} catch (WorkflowException we)	{
				System.out.print("erro stacktrace adapter WorkflowException FCHIGERInsereObservacaoProcesso");
				throw we;
			} catch (Exception e) {
                System.out.print("INICIO stacktrace adapter FCHIGERInsereObservacaoProcesso");
                e.printStackTrace();
                System.out.print("FIM stacktrace adapter FCHIGERInsereObservacaoProcesso");
				throw new WorkflowException(e.getMessage());
			}						
		}
		System.out.println("Codigo FCHIGERInsereObservacaoProcesso Fim: "+codigo);
	}

	@SuppressWarnings("unchecked")
	public void insereObservacaoTituloSapiens(List<NeoObject> listaTitulos, GregorianCalendar data, Long hora, String texto, Long usuario, Long ocorrencia, String codigo) throws Exception
	{
		System.out.println("Codigo insereObservacaoTituloSapiens Inicio: "+codigo);
		for (NeoObject titulo : listaTitulos) {
			EntityWrapper wrapper = new EntityWrapper(titulo);
			Long empresa = (Long) wrapper.findValue("empresa");
			Long filial = (Long) wrapper.findValue("filial");
			String numeroTitulo = (String) wrapper.findValue("numeroTitulo");
			String tipoTitulo = (String) wrapper.findValue("tipoTitulo");
			Integer sequencial = 1;
			
			String nomeFonteDados = "SAPIENS";
			
			String sqlSelect = "SELECT (CASE WHEN max(seqmov) > 0 THEN MAX(seqmov)+1 ELSE 1 END) FROM E301MOR WHERE codemp = :codemp AND codfil = :codfil AND numtit = :numtit AND codtpt = :codtpt ";
			
			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sqlSelect);
			query.setParameter("codemp", empresa);
			query.setParameter("codfil", filial);
			query.setParameter("numtit", numeroTitulo);
			query.setParameter("codtpt", tipoTitulo);
			List<Object> resultList = query.getResultList();
			
			if(resultList != null && !resultList.isEmpty())	{
				sequencial = (Integer)resultList.get(0);
			}	
			
			Connection connection = null;
			connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			
			String sql = "INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov, USU_CodOcr) ";
			sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			PreparedStatement st = null;
			try
			{	
				st = connection.prepareStatement(sql);
				st.setLong(1, empresa);
				st.setLong(2, filial);
				st.setString(3, numeroTitulo);
				st.setString(4, tipoTitulo);
				st.setInt(5, sequencial);
				st.setString(6, "A");
				st.setString(7, texto);
				st.setLong(8, usuario);
				st.setTimestamp(9, new Timestamp(data.getTimeInMillis()));
				st.setLong(10, hora);
				st.setLong(11, ocorrencia);
				
				System.out.println("C028 INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov, USU_CodOcr) VALUES ("+empresa+", "+filial+", "+numeroTitulo+", "+tipoTitulo+", "+sequencial+", 'A', "+texto+", "+usuario+", "+new Timestamp(data.getTimeInMillis())+", "+hora+", "+ocorrencia+")");
				
				st.executeUpdate();
				System.out.println("Codigo insereObservacaoTituloSapiens Fim C028: "+codigo);
				System.out.println("Insert que será feito na MOR C028: INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov, USU_CodOcr) VALUES ("+empresa+", "+filial+", "+numeroTitulo+", "+tipoTitulo+", "+sequencial+", M, "+texto+", "+usuario+", "+new Timestamp(data.getTimeInMillis())+", (DATEPART(HOUR, "+new Timestamp(data.getTimeInMillis())+") * 60) + DATEPART(MINUTE, "+hora+"), "+ocorrencia+") ");
				System.out.println("Texto Que Foi para o Sapiens C028 : Texto: "+texto+", Ocorrencia: "+ocorrencia+", Data: "+data+", Data e Hora: "+new Timestamp(data.getTimeInMillis()));
				System.out.println("Codigo Classe FCHIGERInsereObservacaoProcesso método insereObservacaoTituloSapiens Fim: "+codigo);
			}
			catch (Exception e)
			{	System.out.println("Erro ao inserir na MOR classe FCHIGERInsereObservacaoProcesso do metodo insereObservacaoTituloSapiens: "+e.getMessage());
				throw e;
			}
			finally
			{
				try
				{
					OrsegupsUtils.closeConnection(connection, st, null);
				}
				catch (Exception e)
				{
					log.error("Erro ao fechar o statement");
					e.printStackTrace();
					throw e;
				}
			}

		}

	}
}
