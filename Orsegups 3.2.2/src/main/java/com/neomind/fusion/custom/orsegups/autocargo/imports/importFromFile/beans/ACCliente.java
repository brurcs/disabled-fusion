package com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.beans;

public class ACCliente {
    
    private int id;
    private int idLegado;
    private String operadora;
    private String nome;
    private String telefone;
    private String cep;
    private String rua;
    private String numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private String estado;
    private String cdCliente;
    private String erros;
    private String rastreador;
    private String referencia;
    private String idCentral;
        
    public String getIdCentral() {
        return idCentral;
    }
    public void setIdCentral(String idCentral) {
        this.idCentral = idCentral;
    }
    public String getRastreador() {
        return rastreador;
    }
    public void setRastreador(String rastreador) {
        this.rastreador = rastreador;
    }
    public String getReferencia() {
        return referencia;
    }
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getIdLegado() {
        return idLegado;
    }
    public void setIdLegado(int idLegado) {
        this.idLegado = idLegado;
    }
    public String getOperadora() {
        return operadora;
    }
    public void setOperadora(String operadora) {
        this.operadora = operadora;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getTelefone() {
        return telefone;
    }
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    public String getCep() {
        return cep;
    }
    public void setCep(String cep) {
        this.cep = cep;
    }
    public String getRua() {
        return rua;
    }
    public void setRua(String rua) {
        this.rua = rua;
    }
    public String getComplemento() {
        return complemento;
    }
    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }
    public String getNumero() {
        return numero;
    }
    public void setNumero(String numero) {
        this.numero = numero;
    }
    public String getBairro() {
        return bairro;
    }
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
    public String getCidade() {
        return cidade;
    }
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getCdCliente() {
        return cdCliente;
    }
    public void setCdCliente(String cdCliente) {
        this.cdCliente = cdCliente;
    }
    public String getErros() {
        return erros;
    }
    public void setErros(String erros) {
        this.erros = erros;
    }
    
    

}
