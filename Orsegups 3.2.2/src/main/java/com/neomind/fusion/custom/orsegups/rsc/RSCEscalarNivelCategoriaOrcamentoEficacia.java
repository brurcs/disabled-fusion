package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RSCEscalarNivelCategoriaOrcamentoEficacia implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCEscalarNivelCategoriaOrcamentoEficacia.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String erro = "Por favor, contatar o administrador do sistema!";
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		try
		{
			if (origin != null)
			{
				RSCUtils rscUtils = new RSCUtils();
				GregorianCalendar prazoDeadLine = new GregorianCalendar();

				NeoPaper ultimoUsuarioAgendar = null;
				NeoPaper ultimoUsuarioVisitar = null;

				NeoUser executivoResponsavelRSC = (NeoUser) processEntity.findValue("responsavelExecutor");
				QLEqualsFilter ftrGrupoExecutivo = new QLEqualsFilter("grupoExecutivoRegional", executivoResponsavelRSC.getGroup());
				NeoObject grupoExecutivo = PersistEngine.getObject(AdapterUtils.getEntityClass("RSCHierarquisaOrcamento"), ftrGrupoExecutivo);

				if (NeoUtils.safeIsNotNull(grupoExecutivo))
				{
					EntityWrapper wrpResponsavelExecutivo = new EntityWrapper(grupoExecutivo);
					NeoPaper gerenteResponsavelExecutivo = (NeoPaper) wrpResponsavelExecutivo.findValue("gerenciaExecutivoRegional");
					NeoPaper superintendenciaResponsavelExecutivo = (NeoPaper) wrpResponsavelExecutivo.findValue("superintendenciaExecutivaRegional");
					NeoPaper diretoriaResponsavelExecutivo = (NeoPaper) wrpResponsavelExecutivo.findValue("diretoriaExecutivaRegional");
					NeoPaper presidenciaResponsavelExecutivo = (NeoPaper) wrpResponsavelExecutivo.findValue("presidenciaExecutivaRegional");

					if (origin.getActivityName().contains("Agendar Visita - Executivo") || origin.getActivityName().contains("Agendar Visita Escalada - Superior") || origin.getActivityName().contains("Agendar Visita Escalada - Diretoria"))
					{
						if (((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 2L)
						{
							processEntity.setValue("superiorResponsavelExecutor", gerenteResponsavelExecutivo);
							processEntity.setValue("ultimoUsuarioAgendar", gerenteResponsavelExecutivo);
						}
						else
						{
							ultimoUsuarioAgendar = (NeoPaper) processEntity.findValue("ultimoUsuarioAgendar");
							if (ultimoUsuarioAgendar.getName().equals(gerenteResponsavelExecutivo.getName()))
							{
								processEntity.setValue("superiorResponsavelExecutor", superintendenciaResponsavelExecutivo);
								processEntity.setValue("ultimoUsuarioAgendar", superintendenciaResponsavelExecutivo);
							}
							else if (ultimoUsuarioAgendar.getName().equals(superintendenciaResponsavelExecutivo.getName()))
							{
								processEntity.setValue("superiorResponsavelExecutor", diretoriaResponsavelExecutivo);
								processEntity.setValue("ultimoUsuarioAgendar", diretoriaResponsavelExecutivo);
							}
							else if (ultimoUsuarioAgendar.getName().equals(diretoriaResponsavelExecutivo.getName()))
							{
								processEntity.setValue("superiorResponsavelExecutor", presidenciaResponsavelExecutivo);
								processEntity.setValue("ultimoUsuarioAgendar", presidenciaResponsavelExecutivo);
							}
						}

						ultimoUsuarioAgendar = (NeoPaper) processEntity.findValue("ultimoUsuarioAgendar");
						if (ultimoUsuarioAgendar.getName().contains("Diretor") || ultimoUsuarioAgendar.getName().contains("Presidente"))
						{
							processEntity.findField("souPessoaResponsavel").setValue(false);
							processEntity.findField("continuarRSC").setValue(true);
							if (ultimoUsuarioAgendar.getName().contains("Diretor"))
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Agendar Visita Escalada - Diretoria", new GregorianCalendar());
								processEntity.findField("prazoAgendarVisitaEscaladaDiretoria").setValue(prazoDeadLine);
							}
							else
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Agendar Visita Escalada - Presidência", new GregorianCalendar());
								processEntity.findField("prazoAgendarVisitaEscaladaPresidencia").setValue(prazoDeadLine);
							}
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Agendar Visita Escalada - Superior", new GregorianCalendar());
							processEntity.findField("prazoAgendarVisitaEscalada").setValue(prazoDeadLine);
						}

						if (ultimoUsuarioAgendar != null)
						{
							for (NeoUser user : ultimoUsuarioAgendar.getAllUsers())
							{
								if (user.getCode().equals("dilmoberger"))
								{
									erro = "Tarefa já escalou para o último nível de hierarquia. Por favor, proceder com a finalização da tarefa!";
									throw new WorkflowException(erro);
								}
							}
						}
					}
					else
					{
						if (((Long) processEntity.findValue("etapaVerificacaoEficaciaVisita")) == 2L)
						{
							processEntity.setValue("superiorResponsavelExecutorVisitar", gerenteResponsavelExecutivo);
							processEntity.setValue("ultimoUsuarioVisitar", gerenteResponsavelExecutivo);
						}
						else
						{
							ultimoUsuarioVisitar = (NeoPaper) processEntity.findValue("ultimoUsuarioVisitar");
							if (ultimoUsuarioVisitar.getName().equals(gerenteResponsavelExecutivo.getName()))
							{
								processEntity.setValue("superiorResponsavelExecutorVisitar", superintendenciaResponsavelExecutivo);
								processEntity.setValue("ultimoUsuarioVisitar", superintendenciaResponsavelExecutivo);
							}
							else if (ultimoUsuarioVisitar.getName().equals(superintendenciaResponsavelExecutivo.getName()))
							{
								processEntity.setValue("superiorResponsavelExecutorVisitar", diretoriaResponsavelExecutivo);
								processEntity.setValue("ultimoUsuarioVisitar", diretoriaResponsavelExecutivo);

							}
							else if (ultimoUsuarioVisitar.getName().equals(diretoriaResponsavelExecutivo.getName()))
							{
								processEntity.setValue("superiorResponsavelExecutorVisitar", presidenciaResponsavelExecutivo);
								processEntity.setValue("ultimoUsuarioVisitar", presidenciaResponsavelExecutivo);
							}
						}

						ultimoUsuarioVisitar = (NeoPaper) processEntity.findValue("ultimoUsuarioVisitar");
						if (ultimoUsuarioVisitar.getName().contains("Diretor") || ultimoUsuarioVisitar.getName().contains("Presidente"))
						{
							processEntity.findField("souPessoaResponsavel").setValue(false);
							processEntity.findField("continuarRSC").setValue(true);
							if (ultimoUsuarioVisitar.getName().contains("Diretor"))
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente Escalada - Diretoria", new GregorianCalendar());
								processEntity.findField("prazoVisitarClienteEscaladaDiretoria").setValue(prazoDeadLine);

							}
							else
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente Escalada - Presidência", new GregorianCalendar());
								processEntity.findField("prazoVisitarClienteEscaladaPresidencia").setValue(prazoDeadLine);

							}
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente Escalada - Superior", new GregorianCalendar());
							processEntity.findField("prazoAgendarVisitaEscalada").setValue(prazoDeadLine);
						}

						if (ultimoUsuarioVisitar != null)
						{
							for (NeoUser user : ultimoUsuarioVisitar.getAllUsers())
							{
								if (user.getCode().equals("dilmoberger"))
								{
									erro = "Tarefa já escalou para o último nível de hierarquia. Por favor, proceder com a finalização da tarefa!";
									throw new WorkflowException(erro);
								}
							}
						}
					}
				}

				/*
				 * Pego meu último valor da minha lista de Registro de Atividades e altero o valor do
				 * campo prazo
				 */
				List<NeoObject> objLstRegistroAtividades = (List<NeoObject>) processEntity.findValue("RscRelatorioSolicitacaoCliente.registroAtividades");
				if (NeoUtils.safeIsNotNull(objLstRegistroAtividades))
				{
					NeoObject ultima = objLstRegistroAtividades.get(objLstRegistroAtividades.size() - 1);
					EntityWrapper ewUltima = new EntityWrapper(ultima);

					ewUltima.findField("prazo").setValue(prazoDeadLine);
				}
			}
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
