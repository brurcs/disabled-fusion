package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RotinaCancelaTarefaDebitoAutomatico implements CustomJobAdapter
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesRelatorioSimplesRsc.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaCancelaTarefaDebitoAutomatico");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Cancelar Tarefa Débito Automatico - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

		try
		{
			StringBuilder sql = new StringBuilder(); 
			
			sql.append(" select p.code, cda.dataInstrucaoCadastroDebito, cda.dataTratarPendenciaDebito, am.name, * "); 
			sql.append(" from activity a "); 
			sql.append(" inner join wfProcess p on p.neoId = a.process_neoId "); 
			sql.append(" inner join dbo.D_CDACadastroDebitoAutomatico cda on cda.wfprocess_neoId = p.neoId "); 
			sql.append(" left join activitymodel am on am.neoid = a.model_neoid "); 
			sql.append(" inner join processmodel pm on pm.neoid = p.model_neoid "); 
			sql.append(" where  p.processState = 0 "); 
			sql.append(" AND p.saved = 1 "); 
			sql.append(" AND a.startDate is not null "); 
			sql.append(" AND a.startDate is not null "); 
			sql.append(" and a.finishDate is null "); 
			sql.append(" and (am.name like 'Instrução de Cadastro do Débito Automático' or am.name like 'Tratar Pendência do Débito Automático') "); 
			sql.append(" and (convert(varchar(10), cda.dataInstrucaoCadastroDebito, 103) = convert(varchar(10), getDate(), 103) ");
			sql.append(" or convert(varchar(10), cda.dataTratarPendenciaDebito, 103) = convert(varchar(10), getDate(), 103)) ");
			
			Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();
			
			GregorianCalendar dataAtual = (GregorianCalendar) GregorianCalendar.getInstance();
			for(Object tarefa : resultList) {
				Object[] result = (Object[]) tarefa;
				String code = (String)result[0];
				Date dataInstrucao = (Date)result[1];
				Date dataTratar = (Date)result[2];
				String atividade = (String)result[3];
				
				String prazoInstrucao = NeoUtils.safeDateFormat(dataInstrucao, "dd/MM/yyyy HH:mm:ss");
				String prazoTratar = NeoUtils.safeDateFormat(dataTratar, "dd/MM/yyyy HH:mm:ss");
				
				String urlTarefa = "";
				urlTarefa = OrsegupsUtils.URL_PRODUCAO + "/fusion/custom/jsp/orsegups/cancelProcess.jsp?codigoCancelamento=cancelaDebitoAutomatico&codigoProcesso="+code;

				URL url = new URL(urlTarefa);
				URLConnection uc = url.openConnection();
				BufferedReader leitorArquivo = new BufferedReader(new InputStreamReader(uc.getInputStream()));
				System.out.println(code);
				
				//salvar as tarefas canceladas
				InstantiableEntityInfo cda = AdapterUtils.getInstantiableEntityInfo("CDACancelaTarefaDebitoAutomatico");
				NeoObject noCDA = cda.createNewInstance();
				EntityWrapper tmeWrapper = new EntityWrapper(noCDA);
				tmeWrapper.findField("codigoTarefa").setValue(code);
				tmeWrapper.findField("dataTarefaCancelada").setValue(dataAtual);
				tmeWrapper.findField("atividade").setValue(atividade);
				if (atividade.equals("Instrução de Cadastro do Débito Automático")) {
					tmeWrapper.findField("prazoAtividadeInstrucaoCadastro").setValue(prazoInstrucao);
				} else {
					tmeWrapper.findField("prazoAtividadeTratarPendencia").setValue(prazoTratar);
				}
				PersistEngine.persist(noCDA);
			}
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Erro ao Cancelar Tarefa Débito Automatico");
			e.printStackTrace();
			System.out.println("GENDADOR DE TAREFA: Erro ao Cancelar Tarefa Débito Automatico na Classe RotinaCancelaTarefaDebitoAutomatico");
		}
		log.warn("##### FIM AGENDADOR DE TAREFA: Cancelar Tarefa Débito Automatico - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}

}

