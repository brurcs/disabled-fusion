package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;

public class FAPSlipTipoPagamentoConverter extends EntityConverter
{
	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		String color = "";
		
		NeoObject noTipoPagamento = (NeoObject) field.getValue();
		if (noTipoPagamento != null)
		{
			EntityWrapper wTipoPagamento = new EntityWrapper(noTipoPagamento);
			
			Long codTipoPagamento = wTipoPagamento.findGenericValue("codigo");
			if (codTipoPagamento == 1l)
			{
				color = "blue";
			} 
			else if (codTipoPagamento == 2l)
			{
				color = "red";
			}
		}
		
		return "<div style=\"color: " + color + "; font-weight: bold;\">" + super.getHTMLView(field, origin) + "</div>";
	}
}
