package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipOrcamentoUtils;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class FAPSlipTotalDisponivelConverter extends StringConverter
{
	@Override
	protected String getHTMLView(EFormField arg0, OriginEnum arg1)
	{
		return getHTMLInput(arg0, arg1);
	}
	
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		NeoObject noOrcamento = field.getForm().getObject();
		Long codContrato = new EntityWrapper(noOrcamento).findGenericValue("codContrato");
		
		List<NeoObject> listHistoricoOrcamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlipHistoricoOrcamento"), new QLEqualsFilter("orcamento.codContrato", codContrato));
		if ((listHistoricoOrcamento == null || listHistoricoOrcamento.isEmpty()) && (field.getForm() != null && field.getForm().getCaller() != null))
		{
			NeoObject noForm = field.getForm().getCaller().getObject();
			if (noForm != null)
			{
				EntityWrapper wForm = new EntityWrapper(noForm);
				
				if (wForm.findGenericValue("dadosContrato") != null)
				{
					noOrcamento = wForm.findGenericValue("dadosContrato");
					codContrato = new EntityWrapper(noOrcamento).findGenericValue("codContrato");
					
					listHistoricoOrcamento = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlipHistoricoOrcamento"), new QLEqualsFilter("orcamento.codContrato", codContrato));
				}
			}
		}
		
		BigDecimal saldo = FAPSlipOrcamentoUtils.consultarSaldoDisponivel(noOrcamento);
		
		String color = saldo.signum() == -1 ? "red" : "";
		
		StringBuilder outBuilder = new StringBuilder();
		outBuilder.append(" <script type=\"text/javascript\"> function viewItemFusion(id)");
		outBuilder.append(" {");
		outBuilder.append(" var title =\"Visualizar Pagamento\";");
		outBuilder.append(" var uid;");
		outBuilder.append(" var entityType = '';");
		outBuilder.append(" var idx = '';");
		outBuilder.append(" var portlet = new FloatForm(title, uid, entityType, id, 'ellist_', idx, 0, 0, '', null, null, false, false, null, false, '');");
		outBuilder.append(" portlet.open();");
		outBuilder.append("}");
		outBuilder.append("</script>");
		outBuilder.append("<input  type=\"button\" class=\"input_button\" id=\"botaoExtrato\" value=\"Extrato\" " + (listHistoricoOrcamento.size() > 0 ? " onClick=\"javascript:viewItemFusion(" + listHistoricoOrcamento.get(0).getNeoId() + ", 1)\"" : "") + "/>");
		
		return "<span style=\"color: " + color + ";\">" + FAPSlipUtils.parseMonetario(saldo) + "</span>  " + outBuilder.toString();

	}
}
