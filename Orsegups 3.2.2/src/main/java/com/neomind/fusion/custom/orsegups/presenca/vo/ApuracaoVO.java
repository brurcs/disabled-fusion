package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class ApuracaoVO
{
	private ColaboradorVO colaborador;
	private GregorianCalendar data;
	private String dataApuracao;
	private String diaApuracao;
	private Long qdeExcecao;
	private String ocorrenciaApuracao;
	private String escala;
	private String horario;
	private List<AcessoVO> acessos = new ArrayList<AcessoVO>();
	private List<SituacaoAcessoVO> situacoes = new ArrayList<SituacaoAcessoVO>();
	
	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}
	public GregorianCalendar getData()
	{
		return data;
	}
	public void setData(GregorianCalendar data)
	{
		this.data = data;
	}
	public List<AcessoVO> getAcessos()
	{
		return acessos;
	}
	public void setAcessos(List<AcessoVO> acessos)
	{
		this.acessos = acessos;
	}
	public List<SituacaoAcessoVO> getSituacoes()
	{
		return situacoes;
	}
	public void setSituacoes(List<SituacaoAcessoVO> situacoes)
	{
		this.situacoes = situacoes;
	}
	public String getDataApuracao()
	{
		return dataApuracao;
	}
	public void setDataApuracao(String dataApuracao)
	{
		this.dataApuracao = dataApuracao;
	}
	public String getDiaApuracao()
	{
		return diaApuracao;
	}
	public void setDiaApuracao(String diaApuracao)
	{
		this.diaApuracao = diaApuracao;
	}
	public String getOcorrenciaApuracao()
	{
		return ocorrenciaApuracao;
	}
	public void setOcorrenciaApuracao(String ocorrenciaApuracao)
	{
		this.ocorrenciaApuracao = ocorrenciaApuracao;
	}
	public Long getQdeExcecao()
	{
		return qdeExcecao;
	}
	public void setQdeExcecao(Long qdeExcecao)
	{
		this.qdeExcecao = qdeExcecao;
	}
	public String getEscala()
	{
		return escala;
	}
	public void setEscala(String escala)
	{
		this.escala = escala;
	}
	public String getHorario()
	{
		return horario;
	}
	public void setHorario(String horario)
	{
		this.horario = horario;
	}
}
