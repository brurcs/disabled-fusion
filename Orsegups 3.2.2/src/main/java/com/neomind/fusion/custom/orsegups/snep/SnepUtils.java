package com.neomind.fusion.custom.orsegups.snep;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertEventEngine;

public class SnepUtils
{
	private static final Log log = LogFactory.getLog(SnepUtils.class);

	private static final String LINK_SNEP = "http://192.168.20.241";

	private StringBuilder callUrl(String url)
	{
		StringBuilder inputLine = null;

		try
		{
			inputLine = new StringBuilder();
			URL murl = new URL(url);
			URLConnection murlc = murl.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));

			char letter;

			while ((letter = (char) in.read()) != '\uFFFF')
			{
				inputLine.append(letter);
			}
			in.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO AO EXECUTAR A URL SNEP TELA EVENTOS: " + e.getMessage());
		}
		return inputLine;
	}

	public LinkedList<String> getOperadoresDisponiveis(String fila, String type)
	{

		LinkedList<String> result = null;
		try
		{

			log.debug("Buscando lista de operadores disponiveis no Snep");
			//TODO realizar a chamado ao snep para verificar quais operadores estão livres no momento
			String message = "";
			String[] params = null;
			String url = LINK_SNEP + "/snep/modules/services/api/?service=QueueStatus&queue=" + fila + "&type=" + type;
			if (fila != null && !fila.isEmpty())
			{
				StringBuilder buffer = this.callUrl(url);
				String resultStr = buffer.toString();
				resultStr = resultStr.replaceAll("\\{", "");
				resultStr = resultStr.replaceAll("\\}", "");
				resultStr = resultStr.replaceAll("\"", "");
				resultStr = resultStr.replaceAll(":free", "");
				resultStr = resultStr.replaceAll(":paused", "");
				resultStr = resultStr.replaceAll(":busy", "");
				resultStr = resultStr.replaceAll(":all", "");
				params = resultStr.split(",");
				if (params[0] != null && !params[0].isEmpty())
				{
					message = "Chamada efetuada a partir do ramal " + fila + ".";
				}
				else
				{
					String erro[] = params[1].split(":");
					message = "Erro ao efetuar chamada.\n" + erro[1];
				}
			}
			log.debug("Solicitando ligação para Snep! Verificar status dos agentes na fila: " + fila + " mensagem : " + message);
			//TODO remover esta linha quando integração estiver implementada
			result = new LinkedList<String>();
			for (String agente : params)
			{
				if (!agente.contains("fail") || !agente.contains("message:Nenhum membro encontrato para a relacao fila x status"))
					result.add(agente);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	public boolean pausarOperador(String ramal)
	{
		log.debug("Pausadno operador no Snep! Ramal: " + ramal);
		String message = "";
		boolean pause = false;
		try
		{

			// TODO realizar a chamado ao snep para pausar este operador no snep e retorna se conseguiu pausar

			String ramalDestino = ramal;
			//192.168.20.241
			String url = LINK_SNEP + "/snep/modules/services/api/?service=QueueMember&action=pause&pause=5&agent=" + ramal;
			if (ramalDestino != null && !ramalDestino.isEmpty() && ramalDestino.length() == 4)
			{
				StringBuilder buffer = this.callUrl(url);
				String result = buffer.toString();
				result = result.replaceAll("\\{", "");
				result = result.replaceAll("\\}", "");
				String[] params = result.split(",");
				//System.out.println("Pausa " + result);
				if (params[0] != null && params[0].contains("Success"))
				{
					message = "Chamada efetuada a partir do ramal " + ramalDestino + ".";
					pause = true;
				}
				else
				{
					String erro[] = params[1].split(":");
					message = "Erro ao efetuar chamada.\n" + erro[1];
					pause = false;
				}
			}
			log.debug("Solicitando ligação para Snep! Para pausaro Ramal:" + ramal + " mensagem : " + message);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return pause;
	}

	public boolean liberarOperador(String ramal)
	{
		log.debug("Liberando operador no Snep! Ramal: " + ramal);
		boolean unPause = false;
		String message = "";
		// TODO realizar a chamado ao snep para liberar o operador pausado
		try
		{
			String url = LINK_SNEP + "/snep/modules/services/api/?service=QueueMember&action=unpause&agent=" + ramal;
			if (ramal != null && !ramal.isEmpty() && ramal.length() == 4)
			{
				StringBuilder buffer = this.callUrl(url);
				String result = buffer.toString();
				result = result.replaceAll("\\{", "");
				result = result.replaceAll("\\}", "");
				String[] params = result.split(",");
				if (params[0] != null && params[0].contains("Success"))
				{
					message = "Chamada efetuada a partir do ramal " + ramal + ".";
					unPause = true;
				}
				else
				{
					String erro[] = params[1].split(":");
					message = "Erro ao efetuar chamada.\n" + erro[1];
					unPause = false;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		log.debug("Solicitando ligação para Snep! Para retirar a pausa do Ramal:" + ramal + " mensagem : " + message);
		return unPause;
	}
	
	public void solicitarLigacaoSnep(String ramal, String numeroExterno)
	{
		//TODO adicionar a logica que irá realizar a chamada ap Snep
		String message = "";

		String ramalOrigem = null;
		String ramalDestino = null;
		try
		{
		
		ramalOrigem = ramal;
		ramalDestino = numeroExterno.trim();

		String url = LINK_SNEP + "/snep/services/?service=Dial&agent=" + ramalOrigem + "&exten=" + ramalDestino;
		if (ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty())
		{

			if (ramalDestino.equals(ramalOrigem))
			{
				message = "Erro ao efetuar chamada.\nRamal de origem igual ao ramal de destino.";
			}
			else
			{
				StringBuilder buffer = this.callUrl(url);

				String result = buffer.toString();
				result = result.replaceAll("\\{", "");
				result = result.replaceAll("\\}", "");
				String[] params = result.split(",");
				//System.out.println("Ligaca : " + result);
				if (params[0] != null && params[0].contains("ok"))
				{
					message = "Chamada efetuada a partir do ramal " + ramalOrigem + " para o ramal " + ramalDestino + ".";

				}
				else
				{
					String erro[] = params[1].split(":");

					message = "Erro ao efetuar chamada.\n" + erro[1];
				}
			}

		}
		log.debug("Solicitando ligação para Snep! Ramal:" + ramal + " - Numero externo: " + numeroExterno + " mensagem : " + message);
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void getLigacaoSnep(String ramal, String numeroExterno)
	{
		String message = "";
		String ramalOrigem = null;
		String ramalDestino = null;
		try
		{
	
		ramalOrigem = ramal;
		ramalDestino = numeroExterno.trim();
		String url = LINK_SNEP + "/snep/services/?service=Dial&ramal=" + ramalOrigem + "&exten=" + ramalDestino;
		if (ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty())
		{
			if (ramalDestino.equals(ramalOrigem))
			{
				message = "Erro ao efetuar chamada.\nRamal de origem igual ao ramal de destino.";
			}
			else
			{
				StringBuilder buffer = this.callUrl(url);
				String result = buffer.toString();
				result = result.replaceAll("\\{", "");
				result = result.replaceAll("\\}", "");
				String[] params = result.split(",");
				//System.out.println("Ligaca : " + result);
				if (params[0] != null && params[0].contains("ok"))
				{
					message = "Chamada efetuada a partir do ramal " + ramalOrigem + " para o ramal " + ramalDestino + ".";

				}
				else
				{
					String erro[] = params[1].split(":");
					message = "Erro ao efetuar chamada.\n" + erro[1];
				}
			}
		}
		log.debug("Solicitando ligação para Snep! Ramal:" + ramal + " - Numero externo: " + numeroExterno + " mensagem : " + message);
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
