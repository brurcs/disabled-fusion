package com.neomind.fusion.custom.orsegups.converter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.BigDecimalConverter;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;

public class ContratoISSConverter extends StringConverter
{
		private static final Log log = LogFactory.getLog(TreeRefreshCentroCustoConverter.class);
		
		@Override
		protected String getHTMLInput(EFormField field, OriginEnum origin)
		{
			String botao = "<input type=\"button\" class=\"input_button\" id=\"bt_import\" value=\"Calcular Valor ISS\" onClick = \"javascript:calcularValorISS();\"/>";
			
			return super.getHTMLInput(field, origin) + botao;
		}
}
