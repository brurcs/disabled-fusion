package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Collection;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesAuditoriaArmamento implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesAuditoriaArmamento.class);

	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		log.warn("##### INICIO AGENDADOR DE TAREFA: Abre Tarefa Simples Auditoria/Inspeção Cofre de Armas - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		try
		{
			Collection<NeoObject> objResponsavelArmamento = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ARMResponsavelCofre"));
			
			if (NeoUtils.safeIsNotNull(objResponsavelArmamento) && !objResponsavelArmamento.isEmpty())
			{
				for (NeoObject objItmResponsavelArmamento : objResponsavelArmamento)
				{
					EntityWrapper wrpResponsavelArmamento = new EntityWrapper(objItmResponsavelArmamento);
					final String cofre = (String) wrpResponsavelArmamento.findValue("cofre");
					final Long regional = (Long) wrpResponsavelArmamento.findValue("responsavelCofre.usu_codreg");

					final NeoRunnable work = new NeoRunnable()
					{
						public void run() throws Exception
						{
							//Código da sub-transação
							abrirTarefa(cofre, regional);
						}
					};
					try
					{
						PersistEngine.managedRun(work);
					}
					catch (final Exception e)
					{
						log.error(e.getMessage(), e);
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Auditoria/Inspeção Cofre de Armas");
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Auditoria/Inspeção Cofre de Armas");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			log.warn("##### FIM AGENDADOR DE TAREFA: Abre Tarefa Simples Auditoria/Inspeção Cofre de Armas - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	public void abrirTarefa(String cofre, Long regional)
	{
		StringBuilder html = new StringBuilder();

		html.append("Prezado Gestor,<br><br>");
		html.append("Por gentileza, realizar os procedimentos solicitados abaixo para o " + cofre + ": <br><br>");

		html.append("1 - Verificar situações de Armas e Munições: <br>");
		html.append("a) Registro original/vencimento; <br>");
		html.append("b) Lotação/guia de trafego; <br>");
		html.append("c) Anotações atualizadas no Livro de Registro; <br>");
		html.append("d) Armazenamento adequado das armas com identificação. <br><br>");

		html.append("2 - Realizar teste com Sistema de Segurança: <br>");
		html.append("a) CFTV; <br>");
		html.append("b) Alarme; <br>");
		html.append("c) Extintor de incêndio (Verificar validade e localização); <br>");
		html.append("d) Sistema de comunicação (Nextel/Rádio HT), contrato ativo. <br>");

		NeoPaper responsavel = new NeoPaper();
		String executor = "";

		if (regional > 0L)
		{
		    if (regional == 1){
			responsavel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "ResponsavelAuditoriaArmamento"));
		    }else{
			responsavel = OrsegupsUtils.getPapelGerenteRegional(regional);
		    }
		    if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
		    {
			for (NeoUser user : responsavel.getUsers())
			{
			    executor = user.getCode();
			    break;
			}
		    }
		}
		else
		{
			executor = "luana.martins";
		}

		String solicitante = "maurelio.pinto";
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 15L);

		String titulo = "Auditoria/Inspeção Armamento do " + cofre;
		String descricao = NeoUtils.safeString(html.toString());

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo, null);

		log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abre Tarefa Simples Auditoria/Inspeção Cofre de Armas - Tarefa: " + NeoUtils.safeString(tarefa));
	}
}
