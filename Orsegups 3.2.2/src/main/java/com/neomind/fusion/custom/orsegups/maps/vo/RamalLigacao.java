package com.neomind.fusion.custom.orsegups.maps.vo;

public class RamalLigacao
{
	private String ramal;
	private int total;
	public int getTotal()
	{
		return total;
	}
	public void setTotal(int total)
	{
		this.total = total;
	}
	public String getRamal()
	{
		return ramal;
	}
	public void setRamal(String ramal)
	{
		this.ramal = ramal;
	}
}
