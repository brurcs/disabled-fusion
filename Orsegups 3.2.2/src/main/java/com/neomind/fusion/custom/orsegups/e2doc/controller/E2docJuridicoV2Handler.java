package com.neomind.fusion.custom.orsegups.e2doc.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docJuridicoEngine;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docJuridicoV2Engine;
import com.neomind.fusion.custom.orsegups.e2doc.xml.juridico.Pastas;

@Path(value = "documentosJuridicoV2")
public class E2docJuridicoV2Handler {
    
	@POST
	@Path("pesquisaJuridicoTrabalhista")
	@Produces("application/json")
	public Pastas pesquisaJuridicoTrabalhista(
		@MatrixParam("numeroProcesso") String numeroProcesso, 
		@MatrixParam("autor") String autor,
		@MatrixParam("matricula") String matricula, 
		@MatrixParam("reu") String reu) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (numeroProcesso != null && !numeroProcesso.trim().isEmpty()){
		atributosPesquisa.put("NÚMERO DO PROCESSO", numeroProcesso.trim());
	    }
	    
	    if(autor != null && !autor.trim().isEmpty()){
		atributosPesquisa.put("AUTOR", "%"+autor.trim()+"%");
	    }
	    
	    if(matricula != null && !matricula.trim().isEmpty()){
		try {
		    atributosPesquisa.put("MATRICULA", URLEncoder.encode("%"+matricula.trim()+"%","UTF-8"));
		} catch (UnsupportedEncodingException e) {
		    e.printStackTrace();
		}
	    }
	    
	    if(reu != null && !reu.trim().isEmpty()){
		atributosPesquisa.put("RÉU", "%"+reu.trim()+"%");
	    }
	    	    
	    E2docJuridicoV2Engine e2docEngine = new E2docJuridicoV2Engine();
	    
	    Pastas pastas = e2docEngine.pesquisaJuridicoV2(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("pesquisaJuridicoCivel")
	@Produces("application/json")
	public Pastas pesquisaJuridicoCivel(
		@MatrixParam("nossasEmpresasAutoras") String nossasEmpresasAutoras,
		@MatrixParam("numeroProcesso") String numeroProcesso, 
		@MatrixParam("cliente") String cliente,
		@MatrixParam("empresa") String empresa, 
		@MatrixParam("filial") String filial) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (nossasEmpresasAutoras != null && !nossasEmpresasAutoras.trim().isEmpty()){
		atributosPesquisa.put("NOSSAS EMPRESAS AUTORAS", numeroProcesso.trim());
	    }
	    
	    if (numeroProcesso != null && !numeroProcesso.trim().isEmpty()){
		atributosPesquisa.put("NÚMERO DO PROCESSO", numeroProcesso.trim());
	    }
	    
	    if(cliente != null && !cliente.trim().isEmpty()){
		atributosPesquisa.put("CLIENTE", "%"+cliente.trim()+"%");
	    }
	    
	    if(empresa != null && !empresa.trim().isEmpty()){
		try {
		    atributosPesquisa.put("EMPRESA", URLEncoder.encode("%"+empresa.trim()+"%","UTF-8"));
		} catch (UnsupportedEncodingException e) {
		    e.printStackTrace();
		}
	    }
	    
	    if(filial != null && !filial.trim().isEmpty()){
		atributosPesquisa.put("FILIAL", "%"+filial.trim()+"%");
	    }
	    	    
	    E2docJuridicoEngine e2docEngine = new E2docJuridicoEngine();
	    
	    Pastas pastas = e2docEngine.pesquisaJuridico(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("pesquisaJuridicoTributario")
	@Produces("application/json")
	public Pastas pesquisaJuridicoTributario(
		@MatrixParam("nossasEmpresasAutoras") String nossasEmpresasAutoras,
		@MatrixParam("numeroProcesso") String numeroProcesso, 
		@MatrixParam("unidadeFederativa") String unidadeFederativa,
		@MatrixParam("empresa") String empresa, 
		@MatrixParam("filial") String filial) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (numeroProcesso != null && !numeroProcesso.trim().isEmpty()){
		atributosPesquisa.put("NÚMERO DO PROCESSO", numeroProcesso.trim());
	    }
	    
	    atributosPesquisa.put("TIPO DE PROCESSO", "Tributario");
	    	    
	    if(unidadeFederativa != null && !unidadeFederativa.trim().isEmpty()){
		atributosPesquisa.put("UNIDADE FEDERATIVA", "%"+unidadeFederativa.trim()+"%");
	    }
	    
	    atributosPesquisa.put("AUTOR", "");
	    atributosPesquisa.put("RÉU", "");
	    atributosPesquisa.put("MATRICULA", "");
	    atributosPesquisa.put("CODIGO DO CLIENTE NO SAPIENS", "");
	    	    
	    E2docJuridicoV2Engine e2docEngine = new E2docJuridicoV2Engine();
	    
	    Pastas pastas = e2docEngine.pesquisaJuridicoV2(atributosPesquisa);
	    	    
	    return pastas;

	}
}
