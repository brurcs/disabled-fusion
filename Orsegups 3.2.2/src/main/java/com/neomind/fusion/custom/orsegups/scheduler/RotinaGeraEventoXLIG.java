package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.com.segware.sigmaWebServices.webServices.EventoRecebido;
import br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaGeraEventoXLIG implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaGeraEventoXLIG.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		log.warn("##### INICIAR ROTINA GERAR EVENTO XLIG - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		String returnFromAccess = "";

		try
		{
			/**
			 * o XLIG é um evento que vamos gerar pelo Fusion, para algumas contas
			 * no atendimento padrão de alarmes, nós deslocamos o AIT e caso esteja tudo OK, encerramos o
			 * evento, sem contato com o cliente
			 * entretanto alguns clientes querem que liguemos pra eles.
			 **/

			conn = PersistEngine.getConnection("SIGMA90");

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT DISTINCT h.*, h.CD_EVENTO, c.ID_CENTRAL, c.PARTICAO, c.CD_CLIENTE, c.ID_EMPRESA FROM HISTORICO_ALARME h ");
			sql.append(" INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = h.CD_CLIENTE ");
			sql.append(" WHERE h.DT_FECHAMENTO >= DATEADD(MINUTE, -5,GETDATE()) ");
			sql.append(" AND c.OBSTEMP LIKE '%#xlig%' AND h.DT_VIATURA_NO_LOCAL IS NOT NULL ORDER BY h.DT_FECHAMENTO ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String central = rs.getString("ID_CENTRAL");
				String particao = rs.getString("PARTICAO");
				String evento = rs.getString("CD_EVENTO");
				String cliente = rs.getString("CD_CLIENTE");
				String empresa = rs.getString("ID_EMPRESA");

				returnFromAccess = executaServicoSegware(evento, cliente, empresa, central, particao);
			}

			log.warn("##### EXECUTAR ROTINA GERAR EVENTO XLIG - RETORNO "+returnFromAccess+" Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
	
			log.error("##### ERRO ROTINA GERAR EVENTO XLIG - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("["+key+"] ##### ERRO ROTINA GERAR EVENTO XLIG - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA  GERAR EVENTO XLIG - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			log.warn("##### FINALIZAR ROTINA  GERAR EVENTO XLIG - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	@SuppressWarnings("finally")
	private String executaServicoSegware(String evento, String cliente, String empresa, String idCentral, String particao)
	{
		String returnFromAccess = "";
		String eventoAux = "XLIG";
		try
		{

			EventoRecebido eventoRecebido = new EventoRecebido();

			eventoRecebido.setCodigo(eventoAux);
			eventoRecebido.setData(new GregorianCalendar());
			eventoRecebido.setEmpresa(Long.parseLong(empresa));
			eventoRecebido.setIdCentral(idCentral);
			eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
			eventoRecebido.setParticao(particao);
			eventoRecebido.setProtocolo(Byte.parseByte("2"));

			ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();

			returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
			log.error("##### ERRO ROTINA GERAR EVENTO XLIG executaServicoSegware - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		finally
		{
			return returnFromAccess;
		}
	}

}
