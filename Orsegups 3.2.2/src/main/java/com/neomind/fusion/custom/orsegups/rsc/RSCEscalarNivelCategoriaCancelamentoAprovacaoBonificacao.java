package com.neomind.fusion.custom.orsegups.rsc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCEscalarNivelCategoriaCancelamentoAprovacaoBonificacao implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCEscalarNivelCategoriaCancelamentoAprovacaoBonificacao.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String erro = "Por favor, contatar o administrador do sistema!";

		try
		{
			if (origin != null)
			{
				RSCUtils rscUtils = new RSCUtils();
				if((origin.getActivityName().equalsIgnoreCase("Aprovar Bonificação")))
				{
					NeoPaper responsavelAprovacaoBonificacao = (NeoPaper) wrapper.findValue("responsavelExecutorAprovacaoBonificacao");
					for (NeoUser user : responsavelAprovacaoBonificacao.getAllUsers())
					{
						wrapper.setValue("superiorResponsavelExecutorAprovacaoBonificacao", rscUtils.retornaSuperiorResponsavelExecutor(user));
					}
				}
				else
				{
					NeoPaper responsavelSuperiorAprovacaoBonificacao = (NeoPaper) wrapper.findValue("superiorResponsavelExecutorAprovacaoBonificacao");
					for (NeoUser userResp : responsavelSuperiorAprovacaoBonificacao.getAllUsers())
					{
						if (userResp.getCode().equals("dilmoberger"))
						{
							erro = "Tarefa já escalou para o último nível de hierarquia. Por favor, proceder com a finalização da tarefa!";
							throw new WorkflowException(erro);
						}
						else
						{
							wrapper.setValue("superiorResponsavelExecutorAprovacaoBonificacao", rscUtils.retornaSuperiorResponsavelExecutor(userResp));
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}
}
