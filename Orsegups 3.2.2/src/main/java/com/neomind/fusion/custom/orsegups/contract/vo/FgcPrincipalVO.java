package com.neomind.fusion.custom.orsegups.contract.vo;

public class FgcPrincipalVO
{
	private String aCtaRed;
	private String centroCustoContrato;
	private String centroCusto6;
	private String aClaCta6;
	private String aClaCta;

	public String getaCtaRed()
	{
		return aCtaRed;
	}

	public void setaCtaRed(String aCtaRed)
	{
		this.aCtaRed = aCtaRed;
	}

	public String getCentroCustoContrato()
	{
		return centroCustoContrato;
	}

	public void setCentroCustoContrato(String centroCustoContrato)
	{
		this.centroCustoContrato = centroCustoContrato;
	}

	public String getCentroCusto6()
	{
		return centroCusto6;
	}

	public void setCentroCusto6(String centroCusto6)
	{
		this.centroCusto6 = centroCusto6;
	}

	public String getaClaCta6()
	{
		return aClaCta6;
	}

	public void setaClaCta6(String aClaCta6)
	{
		this.aClaCta6 = aClaCta6;
	}

	public String getaClaCta()
	{
		return aClaCta;
	}

	public void setaClaCta(String aClaCta)
	{
		this.aClaCta = aClaCta;
	}

}
