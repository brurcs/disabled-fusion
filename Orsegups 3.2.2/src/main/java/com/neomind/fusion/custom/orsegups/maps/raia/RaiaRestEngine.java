package com.neomind.fusion.custom.orsegups.maps.raia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class RaiaRestEngine {
	private final String SIGMA_CONNECTION = "SIGMA90";
	
	public Integer countTotalPatrimonios() {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT DISTINCT COUNT(*) OVER () AS TOTAL FROM dbcentral central WHERE central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 1 AND central.particao != 99 GROUP BY central.ID_CENTRAL ");
		return getTotalFromQuery(query.toString());
	}
	
	public Integer countTotalParticoes() {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT count(*) AS TOTAL FROM dbcentral central WHERE central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 1 AND central.particao != 99 ");
		return getTotalFromQuery(query.toString());
	}
	
	public Integer countTotalArmado() {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT count(distinct central.ID_CENTRAL) AS TOTAL FROM dbcentral central WHERE central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 1 and central.ARMADA = 1 AND central.particao != 99");
		return getTotalFromQuery(query.toString());
	}
	
	public Integer countTotalDesarmado() {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT count(distinct central.ID_CENTRAL) AS TOTAL FROM dbcentral central WHERE central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 1 and central.ARMADA = 0 AND central.particao != 99");
		return getTotalFromQuery(query.toString());
	}
	
	public Integer countTotalCftv() {
		StringBuilder query = new StringBuilder();
		query.append("SELECT count(*) AS TOTAL FROM dbcentral central WHERE central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 1 and central.particao = '099'");
		return getTotalFromQuery(query.toString());
	}
	
	
	public Integer countTotalDesabilitado() {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT count(*) AS TOTAL FROM dbcentral central WHERE central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 0 AND central.particao != 99");
		return getTotalFromQuery(query.toString());
	}
	
	
	public Integer countTotalArmadoParcial() {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT count(*) AS TOTAL FROM dbcentral central where central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 1 and central.ARMADA = 1 AND central.particao != 99");
		query.append(" and central.ID_CENTRAL NOT IN");
		query.append(" (SELECT id_central FROM dbcentral central where central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 1 and central.ARMADA = 1 AND central.particao != 99");
		query.append(" and central.ID_CENTRAL NOT IN ");
		query.append(" (SELECT ID_CENTRAL FROM dbcentral central WHERE central.CD_GRUPO_CLIENTE = 292 AND central.FG_ATIVO = 1 and central.ARMADA = 0 AND central.particao != 99)");
		query.append(")");
		
		return getTotalFromQuery(query.toString());
	}
	
	public Integer findAllAlarmeEmAndamento() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT count(*) FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE "); 
	    query.append(" WHERE H.CD_EVENTO IN ('E130','E131','E132','E133','E134') AND C.CD_GRUPO_CLIENTE = 292 AND h.CD_HISTORICO_PAI IS NULL AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getTotalFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllDesarmadoForaHorario() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE "); 
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = C.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = C.ID_CIDADE AND bai.ID_BAIRRO = C.ID_BAIRRO");
	    query.append(" WHERE H.CD_EVENTO = 'XXX5' AND C.CD_GRUPO_CLIENTE = 292 AND h.CD_HISTORICO_PAI IS NULL AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	}
	public List<RaiaVO> findAllCftvOffline() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE "); 
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = C.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = C.ID_CIDADE AND bai.ID_BAIRRO = C.ID_BAIRRO");
	    query.append(" WHERE H.CD_EVENTO = 'XRED' AND C.CD_GRUPO_CLIENTE = 292 AND h.CD_HISTORICO_PAI IS NULL AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    query.append(" AND H.CD_CLIENTE NOT IN (");
	    query.append(" SELECT CD_CLIENTE FROM HISTORICO H2 ");
	    query.append(" INNER JOIN dbCENTRAL C2 ON C2.CD_CLIENTE = H2.CD_CLIENTE "); 
		query.append(" WHERE C2.CD_GRUPO_CLIENTE = 292 AND H2.CD_EVENTO = 'RRED'");
		query.append(")");
	    return getRaiaVOFromQuery(query.toString());
	}
	public List<RaiaVO> findAllNaoArmado() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE "); 
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = C.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = C.ID_CIDADE AND bai.ID_BAIRRO = C.ID_BAIRRO");
	    query.append(" WHERE H.CD_EVENTO = 'XXX2' AND C.CD_GRUPO_CLIENTE = 292 AND h.CD_HISTORICO_PAI IS NULL AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllDeslocamento() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO h ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE ");
	    query.append(" INNER JOIN ROTA r  ON r.CD_ROTA = c.ID_ROTA ");
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = C.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = C.ID_CIDADE AND bai.ID_BAIRRO = C.ID_BAIRRO");
	    query.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL and C.CD_GRUPO_CLIENTE = 292 AND C.particao != 99 AND h.CD_HISTORICO_PAI IS NULL");
	    query.append(" AND ((c.NU_LATITUDE is not null AND c.NU_LONGITUDE is not null) OR (h.NU_LATITUDE is not null AND h.NU_LONGITUDE is not null)) AND h.FG_STATUS IN (0, 1, 2, 3, 5, 9) ");
	    return getRaiaVOFromQuery(query.toString());
	  }
	
	public List<RaiaVO> findAllPanico() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE");
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO");
	    query.append(" WHERE C.CD_GRUPO_CLIENTE = 292 AND H.CD_EVENTO IN ('E120','E122') AND C.particao != 99 AND h.CD_HISTORICO_PAI IS NULL AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllNoLocal() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG from HISTORICO h ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE ");
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO");
	    query.append(" WHERE h.DT_VIATURA_NO_LOCAL IS NOT NULL AND C.CD_GRUPO_CLIENTE = 292 AND C.particao != 99 AND h.CD_HISTORICO_PAI IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllFalhaComunicacao() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE ");
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO");
	    query.append(" WHERE H.CD_EVENTO IN ('XXX1') AND h.FG_STATUS IN (0, 1, 2, 3, 5, 9) AND C.CD_GRUPO_CLIENTE = 292 AND C.particao != 99 AND h.CD_HISTORICO_PAI IS NULL AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllMonitoramentoImagens() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE "); 
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO");
	    query.append(" WHERE C.CD_GRUPO_CLIENTE = 292 ");
	    query.append(" AND (( H.CD_CODE = 'ACP')  OR (H.CD_CODE = 'ACV' ) OR (H.CD_CODE = 'MPI' ) )  AND C.particao != 99 AND h.CD_HISTORICO_PAI IS NULL AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllFaltaEnergia() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE ");
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO");
	    query.append(" WHERE H.CD_EVENTO IN ('E301') and C.CD_GRUPO_CLIENTE = 292  AND C.particao != 99 AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0, 1, 2, 3, 5, 9) AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllBateriaBaixa() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE ");
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO");
	    query.append(" WHERE H.CD_EVENTO IN ('E302') and C.CD_GRUPO_CLIENTE = 292  AND C.particao != 99 AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0, 1, 2, 3, 5, 9) AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllManutencaoAlarme() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT d.CD_CLIENTE, d.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, d.ARMADA, d.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, o.DEFEITO AS LOG FROM DBORDEM o ");
	    query.append(" INNER JOIN dbCENTRAL d on d.CD_CLIENTE = o.CD_CLIENTE "); 
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = d.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = d.ID_CIDADE AND bai.ID_BAIRRO = d.ID_BAIRRO");
	    query.append(" WHERE d.CD_GRUPO_CLIENTE = 292 and o.FECHADO = 0 AND d.particao != 99");
	    return getRaiaVOFromQuery(query.toString());
	}
	
	public List<RaiaVO> findAllManutencaoCftv() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT d.CD_CLIENTE, d.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, d.ARMADA, d.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, o.DEFEITO AS LOG FROM DBORDEM o ");
	    query.append(" INNER JOIN dbCENTRAL d on d.CD_CLIENTE = o.CD_CLIENTE "); 
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = d.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = d.ID_CIDADE AND bai.ID_BAIRRO = d.ID_BAIRRO");
	    query.append(" WHERE d.CD_GRUPO_CLIENTE = 292 and o.FECHADO = 0 AND (d.particao = '99' OR d.particao = '099')");
	    return getRaiaVOFromQuery(query.toString());
	  }
	  public List<RaiaVO> findAllIntrusao() {
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO, h.CD_EVENTO, h.TX_OBSERVACAO_FECHAMENTO AS LOG FROM HISTORICO H ");
	    query.append(" INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = H.CD_CLIENTE ");
	    query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE");
	    query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO");
	    query.append(" LEFT JOIN HISTORICO_FRASE_EVENTO F WITh(NOLOCK) ON F.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
	    query.append(" WHERE c.CD_GRUPO_CLIENTE = 292 AND (F.NM_FRASE_EVENTO IS NULL OR (F.NM_FRASE_EVENTO NOT LIKE '%PANICO%' AND F.NM_FRASE_EVENTO NOT LIKE '%SOS%')) AND H.CD_EVENTO in('E130','E131','E132','E133','E134')  AND C.particao != 99  AND h.CD_HISTORICO_PAI IS NULL AND H.DT_ESPERA_DESLOCAMENTO IS NULL AND h.DT_VIATURA_DESLOCAMENTO IS NULL");
	    return getRaiaVOFromQuery(query.toString());
	  }
	
	public List<RaiaVO> getCentrais(){
		StringBuilder query = new StringBuilder();
		query.append(" SELECT c.CD_CLIENTE, c.ID_CENTRAL AS CONTA, cid.ID_ESTADO AS UF, c.ARMADA, c.FANTASIA, cid.NOME AS CIDADE, bai.NOME AS BAIRRO "); 
		query.append(" FROM dbCENTRAL c");
		query.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE");
		query.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO"); 
		query.append(" WHERE CD_GRUPO_CLIENTE = 292 AND FG_ATIVO = 1");
		query.append(" ORDER BY cid.ID_ESTADO");
		
		return getRaiaVOFromQuery(query.toString());
	}

	private List<RaiaVO> getRaiaVOFromQuery(String query) {
		List<RaiaVO> centrais = new ArrayList<RaiaVO>();
		Connection conn = PersistEngine.getConnection(SIGMA_CONNECTION);
		PreparedStatement pstm = null;
		try {
		    ResultSet rs = getResultFromQuery(query, conn, pstm);
		    while (rs.next()) {
	    		centrais.add(getRaiaVOFromRs(rs));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}
		return centrais;
	}
	private RaiaVO getRaiaVOFromRs(ResultSet rs) throws SQLException {
		RaiaVO vo = new RaiaVO();
    	vo.setBairro(rs.getString("BAIRRO"));
    	vo.setCidade(rs.getString("CIDADE"));
    	vo.setConta(rs.getString("CONTA"));
    	vo.setDescricao(rs.getString("FANTASIA"));
    	vo.setStatus(rs.getBoolean("ARMADA"));
    	vo.setUf(rs.getString("UF"));
    	vo.setCdCliente(rs.getString("CD_CLIENTE"));
    	try {
    		vo.setLog(rs.getString("LOG"));
		} catch (Exception e) {
			// do nothing, the column doesnt exist
		}
    	return vo;
	}
	private Integer getTotalFromQuery(String query) {
		Connection conn = PersistEngine.getConnection(SIGMA_CONNECTION);
		PreparedStatement pstm = null;
		try {
		    ResultSet rs = getResultFromQuery(query, conn, pstm);
		    while (rs.next()) {
		    	return rs.getInt(1);
		    }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}
		return 0;
	}
	private ResultSet getResultFromQuery(String query, Connection conn, PreparedStatement pstm) throws Exception {
		pstm = conn.prepareStatement(query);
	    ResultSet rs = pstm.executeQuery();
	    return rs;
	}
}
