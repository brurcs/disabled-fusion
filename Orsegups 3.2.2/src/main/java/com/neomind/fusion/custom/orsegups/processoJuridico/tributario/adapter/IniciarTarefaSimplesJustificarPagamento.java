package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.IniciarTarefaSimplesJustificarPagamento

public class IniciarTarefaSimplesJustificarPagamento implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<NeoObject> registroAtividade = (List<NeoObject>) processEntity.getValue("j002RegistroHistoricoAtividade");
		List<NeoObject> listaPagamentos = (List<NeoObject>) processEntity.getValue("listaGTCAprovar");
		List<NeoObject> listaOqueFazerHistorico = (List<NeoObject>) processEntity.findValue("j002HistoricoOqueFazer");
		boolean abrirTarefaSimples = (boolean) processEntity.getValue("abrirTarefaSimples");
		if (abrirTarefaSimples)
		{
			String titulo = "";
			StringBuilder descricao = new StringBuilder();
			String executor = "";
			String solicitante = "Dilmoberger";
			GregorianCalendar prazo = null;

			if (processEntity.getValue("colaborador") != null)
			{
				NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
				EntityWrapper wColaborador = new EntityWrapper(colaborador);
				colaborador = (NeoObject) processEntity.getValue("colaborador");

				titulo = "J002 - " + activity.getCode() + " - " + wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun");
			}
			else
			{
				NeoObject autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
				EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);
				String cpfAut = String.valueOf(wAutor.getValue("cpfAut"));
				String nomAut = String.valueOf(wAutor.getValue("nomAut"));
				titulo = "J002 - " + activity.getCode() + " - " + nomAut;
				if (cpfAut != null && !cpfAut.equals(""))
				{
					titulo = titulo + " / " + cpfAut;
				}

			}

			titulo = titulo + " - Informar motivo de pagamento";
			NeoObject tarSim = (NeoObject) processEntity.getValue("tarefaSimples");
			EntityWrapper wTarSim = new EntityWrapper(tarSim);

			descricao.append(String.valueOf(wTarSim.getValue("DescricaoSolicitacao")));
			wTarSim.setValue("DescricaoSolicitacao", "");

			prazo = (GregorianCalendar) wTarSim.getValue("Prazo");
			wTarSim.setValue("Prazo", null);

			prazo.set(Calendar.HOUR_OF_DAY, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);

			if (!OrsegupsUtils.isWorkDay(prazo))
			{
				throw new WorkflowException("O Prazo da tarefa precisa ser um dia util.");
			}

			NeoUser executorNeo = (NeoUser) wTarSim.getValue("Executor");
			wTarSim.setValue("Executor", null);
			executor = executorNeo.getCode();

			NeoFile anexo = null;
			if (wTarSim.getValue("AnexoSolicitante") != null)
			{
				anexo = (NeoFile) wTarSim.getValue("AnexoSolicitante");
			}

			IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();

			String code = tarefaSimples.abrirTarefaReturnNeoIdCode(solicitante, executor, titulo, descricao.toString(), "1", "Sim", prazo, anexo);
			String[] neoIdECode = code.split(";");

			System.out.println("Tarefa aberta : " + code);

			String responsavel = origin.returnResponsible();
			String atividade = origin.getActivityName().toString();
			InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002RegistroHistoricoAtividade");
			NeoObject objRegAti = insRegAti.createNewInstance();
			EntityWrapper wRegAti = new EntityWrapper(objRegAti);
			wRegAti.setValue("dataAcao", new GregorianCalendar());
			wRegAti.setValue("atividade", atividade);
			wRegAti.setValue("responsavel", responsavel);
			String descricaoRegistro = "Aberto tarefa simples " + code + " para responder a solicitação de pagamento, referente a este processo.";
			wRegAti.setValue("descricao", descricaoRegistro);

			PersistEngine.persist(objRegAti);
			registroAtividade.add(objRegAti);

			InstantiableEntityInfo insRegAtiOqueFazer = AdapterUtils.getInstantiableEntityInfo("j002HistoricoOqueFazer");
			NeoObject objRegAtiOqueFazer = insRegAtiOqueFazer.createNewInstance();
			EntityWrapper wRegAtiOQF = new EntityWrapper(objRegAtiOqueFazer);
			wRegAtiOQF.setValue("executorTar", executor);
			wRegAtiOQF.setValue("titulo", titulo);
			wRegAtiOQF.setValue("descricao", "Informar o motivo da solicitação de pagamento do(s) seguinte(s) itens:");
			wRegAtiOQF.setValue("prazo", prazo);
			wRegAtiOQF.setValue("anexo", null);
			wRegAtiOQF.setValue("solicitante", solicitante);
			wRegAtiOQF.setValue("neoIdTarefa", Long.parseLong(neoIdECode[0]));
			PersistEngine.persist(objRegAtiOqueFazer);
			listaOqueFazerHistorico.add(objRegAtiOqueFazer);

			processEntity.setValue("j002RegistroHistoricoAtividade", registroAtividade);
			processEntity.setValue("j002HistoricoOqueFazer", listaOqueFazerHistorico);

		}
		listaPagamentos.clear();
		processEntity.setValue("abrirTarefaSimples", false);
	    }catch(Exception e){
		System.out.println("Erro na classe IniciarTarefaSimplesJustificarPagamento do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
