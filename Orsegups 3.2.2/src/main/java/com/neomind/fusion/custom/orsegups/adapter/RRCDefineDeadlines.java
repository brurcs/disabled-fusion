package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RRCDefineDeadlines implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar prazoDeadline = (GregorianCalendar) processEntity.findValue("prazoAcao");
		if(prazoDeadline != null) {
			prazoDeadline = OrsegupsUtils.addDays(prazoDeadline, 30, true);
			processEntity.setValue("dataEscalaPrimeiraVerificacao", prazoDeadline);
		}
		
		prazoDeadline = (GregorianCalendar) processEntity.findValue("prazoCliIns");
		if(prazoDeadline != null) {
			prazoDeadline = OrsegupsUtils.addDays(prazoDeadline, 30, true);
			processEntity.setValue("dataEscalaSegundaVerificacao", prazoDeadline);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
}
