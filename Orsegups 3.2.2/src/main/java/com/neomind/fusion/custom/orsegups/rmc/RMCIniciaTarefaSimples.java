package com.neomind.fusion.custom.orsegups.rmc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;

public class RMCIniciaTarefaSimples implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(RMCIniciaTarefaSimples.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) 
	{	
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

		NeoUser usuario = PortalUtil.getCurrentUser();
		
		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 11/03/2015
		 */
		
		final WFProcess processo = WorkflowService.startProcess(processModel, false, usuario);
		final NeoObject formularioProcesso = processo.getEntity();
		final EntityWrapper formularioWrapper = new EntityWrapper(formularioProcesso);
		
		if(activity.getProcess().getModel().getName().equals("C005 - Distribuição de RMC"))
		{
			if(origin.getActivityName().equalsIgnoreCase("Agendar Instalação - Sem Equipamento") || origin.getActivityName().equalsIgnoreCase("Agendar Instalação - Sem Equipamento - Escalada") ||
			   origin.getActivityName().equalsIgnoreCase("Agendar Instalação - Com Equipamento") || origin.getActivityName().equalsIgnoreCase("Agendar Instalação - Com Equipamento - Escalada"))
			{
				//copia os campos do eForm
				formularioWrapper.findField("Solicitante").setValue(processEntity.findValue("tarefaSimplesCM.Solicitante"));
				formularioWrapper.findField("Executor").setValue(processEntity.findValue("tarefaSimplesCM.Executor"));
				formularioWrapper.findField("Titulo").setValue(processEntity.findValue("tarefaSimplesCM.Titulo"));
				formularioWrapper.findField("DescricaoSolicitacao").setValue(processEntity.findValue("tarefaSimplesCM.DescricaoSolicitacao"));
				formularioWrapper.findField("AnexoSolicitante").setValue(processEntity.findValue("tarefaSimplesCM.AnexoSolicitante"));
				formularioWrapper.findField("Prazo").setValue(processEntity.findValue("tarefaSimplesCM.Prazo"));
				formularioWrapper.findField("codigoProcesso").setValue(activity.getProcess().getNeoId());
				
				registrarAberturaTarefaSimples(formularioProcesso, processEntity);
			}
		}
		
		try
		{
			new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(usuario);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error("Erro #8 - Erro ao iniciar a primeira atividade do processo.");
		}
		
		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
	}

	public void registrarAberturaTarefaSimples(NeoObject tarefa, EntityWrapper wTarefa) {
		WFProcess wf = PersistEngine.getNeoObject(WFProcess.class, new QLEqualsFilter("entity", tarefa));
		
		EntityWrapper ew = new EntityWrapper(wf);
		Long codigoTarefa = Long.parseLong(ew.findGenericValue("code").toString());
		
		wTarefa.findField("tarefaSimplesCM.codigoProcesso").setValue(codigoTarefa);
		PersistEngine.persist(tarefa);		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}