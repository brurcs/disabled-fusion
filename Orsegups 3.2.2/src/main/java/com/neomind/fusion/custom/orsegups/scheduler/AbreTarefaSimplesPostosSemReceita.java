	package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.PostoSemReceitaVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesPostosSemReceita implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(AbreTarefaSimplesPostosSemReceita.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		String solicitante = "gilsoncesar";
		String executor = "fernanda.maciel";
		String titulo = "Analisar Postos Sem Receita";
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 10L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		Connection conn = PersistEngine.getConnection("DATAWAREHOUSE");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesPostosSemReceita");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try
		{
			List<PostoSemReceitaVO> postosSemReceita = new ArrayList<PostoSemReceitaVO>();
			/*sql.append(" SELECT cpt.NomeCompetencia, tm.NomeTipoMercado, emp.NomeEmpresa, reg.NomeRegional, cli.NomeCliente, lot.NomeLotacao, pst.NomePosto, SUM(r.ValorReceita) ValorReceita, SUM(r.ValorCusto) ValorCusto ");
			sql.append(" FROM Fact.Resultado r ");
			sql.append(" INNER JOIN Dim.Competencia cpt ON cpt.IdCompetencia = r.idCompetencia ");
			sql.append(" INNER JOIN Dim.Cliente cli ON cli.IdCliente = r.idCliente ");
			sql.append(" INNER JOIN Dim.Lotacao lot ON lot.IdLotacao = r.idLotacao ");
			sql.append(" INNER JOIN Dim.Regional reg ON reg.IdRegional = r.idRegional ");
			sql.append(" INNER JOIN Dim.Posto pst ON pst.IdPosto = r.idPosto ");
			sql.append(" INNER JOIN Dim.Empresa emp ON emp.IdEmpresa = r.idEmpresa ");
			sql.append(" INNER JOIN Dim.TipoMercado tm ON tm.IdTipoMercado = r.idMercado ");
			sql.append(" WHERE ( ");
			sql.append("         (cpt.NomeCompetencia = RIGHT(CONVERT(VARCHAR(10), DATEADD(MONTH, -2, GETDATE()), 103), 7) AND tm.NomeTipoMercado like 'Público') ");
			sql.append(" OR " );
			sql.append("         (cpt.NomeCompetencia = RIGHT(CONVERT(VARCHAR(10), DATEADD(MONTH, -1, GETDATE()), 103), 7) AND tm.NomeTipoMercado like 'Privado') ");
			sql.append("   ) ");
			sql.append(" GROUP BY cpt.NomeCompetencia, tm.NomeTipoMercado, emp.NomeEmpresa, reg.NomeRegional, cli.NomeCliente, lot.NomeLotacao, pst.NomePosto ");
			sql.append(" HAVING SUM(r.ValorCusto) > 5000 AND (SUM(r.ValorReceita) / SUM(r.ValorCusto) < 0.1) ");
			sql.append(" ORDER BY cli.NomeCliente, cpt.NomeCompetencia, reg.NomeRegional, lot.NomeLotacao, pst.NomePosto ");*/
			
			sql.append(" SELECT cpt.NomeCompetencia, tm.NomeTipoMercado, SUBSTRING(emp.codEmpresa, 3,2) as CodigoEmpresa, emp.NomeEmpresa,                                                                       ");
			sql.append("        reg.NomeRegional, ccu1.usu_codcli as CodigoCliente, cli.NomeCliente, lot.NomeLotacao,                                                                                            ");
			sql.append("        pst.NomePosto as NomePosto, cast(ROUND(SUM(r.ValorReceita),2) AS decimal (15,2)) ValorReceita,                           														 ");
			sql.append("        cast(ROUND(SUM(r.ValorCusto),2) AS decimal (15,2)) ValorCusto                               																				     ");														
			sql.append(" FROM Fact.Resultado r                                                                                                                                                                   "); 
			sql.append(" INNER JOIN Dim.Competencia cpt ON cpt.IdCompetencia = r.idCompetencia                                                                                                                   ");
			sql.append(" INNER JOIN Dim.Cliente cli ON cli.IdCliente = r.idCliente                                                                                                                               ");
			sql.append(" INNER JOIN Dim.Lotacao lot ON lot.IdLotacao = r.idLotacao                                                                                                                               ");
			sql.append(" INNER JOIN Dim.Regional reg ON reg.IdRegional = r.idRegional                                                                                                                            "); 
			sql.append(" INNER JOIN Dim.Posto pst ON pst.IdPosto = r.idPosto                                                                                                                                     ");
			sql.append(" INNER JOIN Dim.Empresa emp ON emp.IdEmpresa = r.idEmpresa                                                                                                                               ");
			sql.append(" INNER JOIN Dim.TipoMercado tm ON tm.IdTipoMercado = r.idMercado                                                                                                                         ");
			sql.append(" INNER JOIN StagingBI.sapiens.E044CCU ccu1 on ccu1.ClaCcu collate Latin1_General_CI_AS = SUBSTRING(pst.codPosto, 1,9) and ccu1.CodEmp = SUBSTRING(pst.codPosto, 3,2) and ccu1.NivCcu = 4 ");
			sql.append(" WHERE (                                                                                                                                                                                 ");
			sql.append("         (cpt.NomeCompetencia = RIGHT(CONVERT(VARCHAR(10), DATEADD(MONTH, -2, GETDATE()), 103), 7) AND tm.NomeTipoMercado like 'Público')                                                ");
			sql.append(" OR                                                                                                                                                                                      ");
			sql.append("         (cpt.NomeCompetencia = RIGHT(CONVERT(VARCHAR(10), DATEADD(MONTH, -1, GETDATE()), 103), 7) AND tm.NomeTipoMercado like 'Privado')                                                ");
			sql.append("   ) AND cli.idCliente <> 6 AND ((ccu1.usu_Codcli not in (26358)) OR (ccu1.usu_Codcli is null))                                                                                          ");
			sql.append(" GROUP BY cpt.NomeCompetencia, tm.NomeTipoMercado, ccu1.usu_codcli, emp.codEmpresa, emp.NomeEmpresa, reg.NomeRegional, cli.NomeCliente, lot.NomeLotacao, pst.NomePosto			         "); 
			sql.append(" HAVING SUM(r.ValorCusto) > 5000 AND (SUM(r.ValorReceita) / SUM(r.ValorCusto) < 0.1)                                                                                                     ");
			sql.append(" ORDER BY cli.NomeCliente, cpt.NomeCompetencia, reg.NomeRegional, lot.NomeLotacao, pst.NomePosto                                                                                         ");
			

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				PostoSemReceitaVO posto = new PostoSemReceitaVO();

				posto.setCompetencia(rs.getString("NomeCompetencia"));
				posto.setTipoMercado(rs.getString("NomeTipoMercado"));
				posto.setCodigoEmpresa(rs.getInt("CodigoEmpresa"));
				posto.setNomeEmpresa(rs.getString("NomeEmpresa"));
				posto.setNomeRegional(rs.getString("NomeRegional"));
				posto.setCodigoCliente(rs.getInt("CodigoCliente"));
				posto.setNomeCliente(rs.getString("NomeCliente"));
				posto.setNomeLotacao(rs.getString("NomeLotacao"));
				posto.setNomePosto(rs.getString("NomePosto"));
				posto.setValorReceita(rs.getBigDecimal("ValorReceita"));
				posto.setValorCusto(rs.getBigDecimal("ValorCusto"));

				postosSemReceita.add(posto);
			}

			IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
			if (postosSemReceita != null && !postosSemReceita.isEmpty())
			{
				ListIterator<PostoSemReceitaVO> i = postosSemReceita.listIterator();
				List<PostoSemReceitaVO> listaPostosCliente = new ArrayList<PostoSemReceitaVO>();
				while (i.hasNext())
				{
					PostoSemReceitaVO atual = i.next();
					PostoSemReceitaVO proximo = null;
					listaPostosCliente.add(atual);

					if (i.hasNext())
					{
						proximo = i.next();
						i.previous();
						
						if (!atual.getNomeCliente().equals(proximo.getNomeCliente()))
						{
							String descricao = montaDescricaoTarefaSimples(listaPostosCliente);
							String codigoTarefa = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
							
							log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " - Aberto a Tarefa Simples: " + codigoTarefa);
							
							listaPostosCliente.clear();
						}
					}
					else
					{
						String descricao = montaDescricaoTarefaSimples(listaPostosCliente);
						String codigoTarefa = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
						
						log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " - Aberto a Tarefa Simples: " + codigoTarefa);
					}
				}
			}
			else
			{
				log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + "A consulta não retornou resultado.");
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples (Análisa Postos Sem Receita)");
			System.out.println("["+key+"] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples (Análisa Postos Sem Receita)");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	public String montaDescricaoTarefaSimples(List<PostoSemReceitaVO> postos)
	{
		String descricao = "";

		String nomeCliente = postos.get(0).getNomeCliente();
		descricao = "Foi identificado que o(s) posto(s) abaixo relacionado(s) do cliente " + nomeCliente + " tiveram custo apropriado aos mesmos, porém sem receita.<br/><br/>";

		descricao = descricao + "<table class=\"gridbox\" cellpadding=\"0\" cellspacing=\"0\" style=\"empty-cells: show\">";
		descricao = descricao + "<thead>";
		descricao = descricao + "<tr><th colspan=10>Tabela de Postos Sem Receita</th></tr>";
		descricao = descricao + "<tr>";
		descricao = descricao + "<th>Empresa</th>";
		descricao = descricao + "<th>Competência</th>";
		descricao = descricao + "<th>Mercado</th>";
		descricao = descricao + "<th>Regional</th>";
		descricao = descricao + "<th>Cód. Cliente</th>";
		descricao = descricao + "<th>Cliente</th>";
		descricao = descricao + "<th>Lotação</th>";
		descricao = descricao + "<th>Posto</th>";
		descricao = descricao + "<th>Vlr. Receita</th>";
		descricao = descricao + "<th>Vlr. Custo</th>";
		descricao = descricao + "</tr>";
		descricao = descricao + "</thead>";
		descricao = descricao + "<tbody>";

		for (PostoSemReceitaVO posto : postos)
		{
			descricao = descricao + "<tr>";
			descricao = descricao + "<td>" + posto.getCodigoEmpresa() + "</td>";
			descricao = descricao + "<td>" + posto.getCompetencia() + "</td>";
			descricao = descricao + "<td>" + posto.getTipoMercado() + "</td>";
			descricao = descricao + "<td>" + posto.getNomeRegional() + "</td>";
			descricao = descricao + "<td>" + posto.getCodigoCliente() + "</td>";
			descricao = descricao + "<td>" + posto.getNomeCliente() + "</td>";
			descricao = descricao + "<td>" + posto.getNomeLotacao() + "</td>";
			descricao = descricao + "<td>" + posto.getNomePosto() + "</td>";
			descricao = descricao + "<td> R$ " + posto.getValorReceita() + "</td>";
			descricao = descricao + "<td> R$ " + posto.getValorCusto() + "</td>";
			descricao = descricao + "</tr>";
		}

		descricao = descricao + "</tbody>";
		descricao = descricao + "</table>";
		
		return descricao;
	}
}
