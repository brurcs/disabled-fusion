package com.neomind.fusion.custom.orsegups.e2doc.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docComercialPublicoEngine;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docEngine;
import com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pasta;
import com.neomind.fusion.custom.orsegups.e2doc.xml.privado.Pastas;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.DocumentoTreeVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.util.NeoDateUtils;

@Path(value = "documentos")
public class E2docHandler
{
	private static final Log log = LogFactory.getLog(E2docHandler.class);

	@GET
	@Path("funcionario/{cpf}")
	@Produces("application/json")
	public List<DocumentoTreeVO> documentosFuncionario(@PathParam("cpf") String cpf)
	{
		log.warn("Executando consulta de documentos do funcionário " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		List<DocumentoTreeVO> e2docDocumentoVOList = new ArrayList<DocumentoTreeVO>();
		E2docEngine e2docEngine = new E2docEngine();
		e2docDocumentoVOList = e2docEngine.pesquisaDocumentosColaborador(cpf);

		return e2docDocumentoVOList;

	}

	public List<DocumentoTreeVO> documentosFuncionarioPresenca(@PathParam("cpf") String cpf)
	{

		log.warn("Executando consulta de documentos do funcionário presença " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		E2docEngine e2docEngine = new E2docEngine();
		List<DocumentoTreeVO> documentoVOs = e2docEngine.pesquisaDocumentosColaborador(String.valueOf(cpf));

		return documentoVOs;

	}

	@GET
	@Path("colaborador/{cpf}") 
	@Produces("application/json")
	public ColaboradorVO pesquisaColaborador(@PathParam("numemp") String numemp, @PathParam("numcad") String numcad, @PathParam("cpf") String cpf)
	{
		log.warn("Executando consulta colaborador " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		E2docEngine e2docEngine = new E2docEngine();
		ColaboradorVO colaboradorVO = e2docEngine.pesquisaColaborador(numemp, numcad, cpf);

		return colaboradorVO;
	}

	@GET
	@Path("downloadImagem/{id}")
	@Produces("application/pdf")
	public Response getFile(@PathParam("id") String id) throws MalformedURLException, IOException
	{
		E2docEngine e2docEngine = new E2docEngine();

		File file = e2docEngine.retornaLinkImagemDocumento(id);
		ResponseBuilder response = Response.ok((Object)file);
		response.header("Content-Disposition", "attachment;filename=temp-"+new GregorianCalendar().getTimeInMillis()+".pdf");
		return response.build();

	}
	
//	@GET
//	@Path("downloadImagem2/{id}")
//	@Produces("application/pdf")
//	public Response getFile2(@PathParam("id") String id) throws MalformedURLException, IOException
//	{
//		E2docEngine e2docEngine = new E2docEngine();
//
//		File file = e2docEngine.retornaLinkImagemDocumento2(id);
//		ResponseBuilder response = Response.ok((Object)file);
//		response.header("Content-Disposition", "attachment;filename=temp-"+new GregorianCalendar().getTimeInMillis()+".pdf");
//		return response.build();
//
//	}
	
	@GET
	@Path("downloadImagem2/{id}")
	@Produces("application/pdf")
	public Response getFile2(@PathParam("id") String id) throws MalformedURLException, IOException
	{
	    E2docComercialPublicoEngine e2docEngine = new E2docComercialPublicoEngine();

	    HashMap<String,Object> map = e2docEngine.retornaLinkImagemDocumento2(id);
	    String ext = (String) map.get("ext");
	    File file = (File) map.get("file");
	    ResponseBuilder response = Response.ok((Object)file);
	    response.header("Content-Disposition", "attachment;filename=temp-"+new GregorianCalendar().getTimeInMillis()+"."+ext);
	    return response.build();

	}
	
	@POST
	@Path("getTiposContratosPrivados")
	@Produces("application/json")
	public Map<String, List<String>> getTiposContratosPrivados() {

		E2docEngine e2docEngine = new E2docEngine();

		return e2docEngine.getTiposContratosPrivados();

	}
	
	@POST
	@Path("pesquisarContrato")
	@Produces("application/json")
	public Pastas pesquisarContrato( @MatrixParam("cgcCpf") String cgcCpf, @MatrixParam("nomeCliente") String nomeCliente,
		@MatrixParam("contrato") String contrato, @MatrixParam("servico") String servico, @MatrixParam("status") String status, @MatrixParam("tipoDocumento") String tipoDocumento) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (cgcCpf != null && !cgcCpf.trim().isEmpty()){
		atributosPesquisa.put("CPF/CNPJ", cgcCpf.trim());
		System.out.println(cgcCpf.trim());
	    }
	    
	    if(nomeCliente != null && !nomeCliente.trim().isEmpty()){
		atributosPesquisa.put("Nome do Cliente", "%"+nomeCliente.trim()+"%");
	    }
	    
	    if(contrato != null && !contrato.trim().isEmpty()){
		atributosPesquisa.put("Numero do Contrato", contrato.trim());
	    }
	    
	    if(servico != null && !servico.trim().isEmpty()){
		atributosPesquisa.put("Tipos de Serviço", "%"+servico.trim()+"%");
	    }
	    
	    if(status != null && !status.trim().isEmpty()){
		atributosPesquisa.put("STATUS", status.trim());
	    }
	    	    
	    if(tipoDocumento != null && !tipoDocumento.trim().isEmpty()) {
	    atributosPesquisa.put("TIPO DE DOCUMENTO", tipoDocumento.trim());
	    }
	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    Pastas pastas = e2docEngine.pesquisaContratosPrivados(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("pesquisarImoveis")
	@Produces("application/json")
	public Map<String, Map<String, List<Pasta>>> pesquisaImoveis( @MatrixParam("numeroMatricula") String numeroMatricula, @MatrixParam("nomeImovel") String nomeImovel,
		@MatrixParam("municipio") String municipio, @MatrixParam("tipoImovel") String tipoImovel, 
		@MatrixParam("propRegistroImoveis") String propRegistroImoveis, @MatrixParam("propFato") String propFato) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    atributosPesquisa.put("Status", "proprios");
	    if (numeroMatricula != null && !numeroMatricula.trim().isEmpty()){
		atributosPesquisa.put("Número da Matricula", "%"+numeroMatricula.trim()+"%");
	    }
	    
	    if(nomeImovel != null && !nomeImovel.trim().isEmpty()){
		atributosPesquisa.put("Nome do Imóvel", "%"+nomeImovel.trim()+"%");
	    }
	    
	    if(municipio != null && !municipio.trim().isEmpty()){
		atributosPesquisa.put("Município", municipio.trim());
	    }
	    
	    if(tipoImovel != null && !tipoImovel.trim().isEmpty()){
		atributosPesquisa.put("Tipo Imóvel", tipoImovel.trim());
	    }
	    
	    if(propRegistroImoveis != null && !propRegistroImoveis.trim().isEmpty()){
		atributosPesquisa.put("Proprietário (RI)", "%"+propRegistroImoveis.trim()+"%");
	    }
	    
	    if(propFato != null && !propFato.trim().isEmpty()){
		atributosPesquisa.put("Proprietário (de fato)", "%"+propFato.trim()+"%");
	    }
	    	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas pastas = e2docEngine.pesquisaImoveisProprios(atributosPesquisa, "imoveis proprios / vendidos");
	    
	    
	    Map<String, Map<String, List<Pasta>>> dados = e2docEngine.getImoveisPorCliente(pastas);
	    for (String key : dados.keySet()){
		Map<String, List<Pasta>> map = dados.get(key);
		map = e2docEngine.ordernarMapImoveis(map);
		dados.put(key, map);
	    }
	    return dados;

	}
	
	@POST
	@Path("pesquisarImoveisVendidos")
	@Produces("application/json")
	public Map<String, Map<String, List<Pasta>>> pesquisaImoveisVendidos( @MatrixParam("numeroMatricula") String numeroMatricula, @MatrixParam("nomeImovel") String nomeImovel,
		@MatrixParam("municipio") String municipio, @MatrixParam("tipoImovel") String tipoImovel, 
		@MatrixParam("vendedor") String vendedor, @MatrixParam("comprador") String comprador) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    atributosPesquisa.put("Status", "vendidos");
	    
	    if (numeroMatricula != null && !numeroMatricula.trim().isEmpty()){
		atributosPesquisa.put("Número da Matricula", "%"+numeroMatricula.trim());
	    }
	    
	    if(nomeImovel != null && !nomeImovel.trim().isEmpty()){
		atributosPesquisa.put("Nome do Imóvel", "%"+nomeImovel.trim()+"%");
	    }
	    
	    if(municipio != null && !municipio.trim().isEmpty()){
		atributosPesquisa.put("Município", municipio.trim());
	    }
	    
	    if(tipoImovel != null && !tipoImovel.trim().isEmpty()){
		atributosPesquisa.put("Tipo Imóvel", tipoImovel.trim());
	    }
	    
	    if(vendedor != null && !vendedor.trim().isEmpty()){
		atributosPesquisa.put("Vendedor", "%"+vendedor.trim()+"%");
	    }
	    
	    if(comprador != null && !comprador.trim().isEmpty()){
		atributosPesquisa.put("Comprador", "%"+comprador.trim()+"%");
	    }
	    	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas pastas = e2docEngine.pesquisaImoveisProprios(atributosPesquisa, "imoveis proprios / vendidos");
	    
	    
	    Map<String, Map<String, List<Pasta>>> dados = e2docEngine.getImoveisPorCliente(pastas);
	    for (String key : dados.keySet()){
		Map<String, List<Pasta>> map = dados.get(key);
		map = e2docEngine.ordernarMapImoveis(map);
		dados.put(key, map);
	    }
	    return dados;

	}
	
	@POST
	@Path("pesquisarImoveisLocados")
	@Produces("application/json")
	public Map<String, Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>>> pesquisaImoveisLocados( @MatrixParam("nomeImovel") String nomeImovel,
		@MatrixParam("municipio") String municipio, @MatrixParam("enderecoImovel") String enderecoImovel, 
		@MatrixParam("locatario") String locatario, @MatrixParam("locador") String locador, @MatrixParam("imobiliaria") String imobiliaria,
		@MatrixParam("status") String status, @MatrixParam("tipoImovel") String tipoImovel, @MatrixParam("ano") String ano) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    
	    if(nomeImovel != null && !nomeImovel.trim().isEmpty()){
		atributosPesquisa.put("NOME DO IMÓVEL", "%"+nomeImovel.trim()+"%");
	    }
	    
	    if(municipio != null && !municipio.trim().isEmpty()){
		atributosPesquisa.put("MUNICIPIO", municipio.trim());
	    }
	    
	    if(enderecoImovel != null && !enderecoImovel.trim().isEmpty()){
		atributosPesquisa.put("ENDEREÇO DO IMÓVEL", "%"+enderecoImovel.trim()+"%");
	    }
	    
	    if(locatario != null && !locatario.trim().isEmpty()){
		atributosPesquisa.put("LOCATÁRIO", "%"+locatario.trim()+"%");
	    }
	    
	    if(locador != null && !locador.trim().isEmpty()){
		atributosPesquisa.put("LOCADOR", "%"+locador.trim()+"%");
	    }

	    if(imobiliaria != null && !imobiliaria.trim().isEmpty()){
		atributosPesquisa.put("IMOBILIARIA", "%"+imobiliaria.trim()+"%");
	    }
	    
	    if (status != null && !status.trim().isEmpty()){
		atributosPesquisa.put("Status", status);
	    }
	    
	    if (tipoImovel != null && !tipoImovel.trim().isEmpty()){
		atributosPesquisa.put("TIPO DE IMÓVEL", status);
	    }
	    
	    if (ano != null && !ano.trim().isEmpty()){
		atributosPesquisa.put("Ano", ano);
	    }
	    	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas pastas = e2docEngine.pesquisaImoveisLocados(atributosPesquisa, "imoveis locados");
	    
	    
	    Map<String, Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>>> dados = e2docEngine.getImoveisLocadosPorCliente(pastas);
	    for (String key : dados.keySet()){
		Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>> map = dados.get(key);
		map = e2docEngine.ordernarMapImoveisLocados(map);
		dados.put(key, map);
	    }
	    return dados;

	}
	
	@POST
	@Path("pesquisaImoveisComboData")
	@Produces("application/json")
	public Map<String, List<String>> pesquisaImoveisComboData() {
	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    return e2docEngine.getImoveisComboData();
	    

	}
	
	@POST
	@Path("pesquisaImoveisComboDataLocados")
	@Produces("application/json")
	public Map<String, List<String>> pesquisaImoveisComboDataLocados() {
	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    return e2docEngine.getImoveisComboDataLocados();
	    

	}
	
	@POST
	@Path("pesquisarLivrosOcorrencia")
	@Produces("application/json")
	public com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas pesquisarLivrosOcorrencia( @MatrixParam("nomePosto") String nomePosto, @MatrixParam("nomeCliente") String nomeCliente,
		@MatrixParam("cidadePosto") String cidadePosto, @MatrixParam("periodoInicial") String dataInicial, @MatrixParam("periodoFinal") String dataFinal) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (nomePosto != null && !nomePosto.trim().isEmpty()){
		atributosPesquisa.put("Nome do Posto", "%"+nomePosto.trim()+"%");
	    }
	    
	    if(nomeCliente != null && !nomeCliente.trim().isEmpty()){
		atributosPesquisa.put("Nome do Cliente", "%"+nomeCliente.trim()+"%");
	    }
	    
	    if(cidadePosto != null && !cidadePosto.trim().isEmpty()){
		atributosPesquisa.put("Cidade do Posto", "%"+cidadePosto.trim()+"%");
	    }
	    
	    if(dataInicial != null && !dataInicial.trim().isEmpty()){
		atributosPesquisa.put("Data inicial", dataInicial.replace("-", "/").trim());
	    }
	    
	    if (dataFinal != null && !dataFinal.trim().isEmpty()){
		atributosPesquisa.put("Data Final", dataFinal.replace("-","/").trim());
	    }
	    	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas pastas = e2docEngine.pesquisaLivrosOcorrencia(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("pesquisarRessarcimentos")
	@Produces("application/json")
	public com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas pesquisarRessarcimentos( @MatrixParam("numeroConta") String numeroConta, @MatrixParam("nomeCliente") String nomeCliente,
		@MatrixParam("endereco") String endereco, @MatrixParam("municipio") String municipio, @MatrixParam("modelo") String modelo) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (numeroConta != null && !numeroConta.trim().isEmpty()){		
		atributosPesquisa.put("NÚMERO DA CONTA", "%"+OrsegupsUtils.retiraCaracteresAcentuados(numeroConta.trim())+"%");
	    }
	    
	    if(nomeCliente != null && !nomeCliente.trim().isEmpty()){
		atributosPesquisa.put("NOME DO CLIENTE", "%"+OrsegupsUtils.retiraCaracteresAcentuados(nomeCliente.trim())+"%");
	    }
	    
	    if(endereco != null && !endereco.trim().isEmpty()){
		atributosPesquisa.put("ENDEREÇO", "%"+OrsegupsUtils.retiraCaracteresAcentuados(endereco.trim())+"%");
	    }
	    
	    if(municipio != null && !municipio.trim().isEmpty()){
		atributosPesquisa.put("MUNICIPIO / CIDADE", "%"+OrsegupsUtils.retiraCaracteresAcentuados(municipio.trim())+"%");
	    }
	    	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas pastas = e2docEngine.pesquisaRessarcimentos(atributosPesquisa, modelo);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("getTiposDocumentos")
	@Produces("application/json")
	public List<String> getTiposDocumentos() {
	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    return e2docEngine.getTiposDocumentosCompras();

	}
	
	@POST
	@Path("pesquisarCompras")
	@Produces("application/json")
	public com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas pesquisarCompras( @MatrixParam("dataEntrega") String dataEntrega,
			@MatrixParam("fornecedor") String fornecedor, @MatrixParam("tipoDocumento") String tipoDocumento) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (dataEntrega != null && !dataEntrega.trim().isEmpty()){
	    	atributosPesquisa.put("DATA DE ENTREGA", dataEntrega.trim().replace("-", "/"));
	    }
	    
	    if (fornecedor != null && !fornecedor.trim().isEmpty() && !fornecedor.equals("undefined")){
	    	atributosPesquisa.put("FORNECEDOR", fornecedor);
	    }
	    
	    if (tipoDocumento != null && !tipoDocumento.trim().isEmpty() && !tipoDocumento.equals("undefined")){
			atributosPesquisa.put("TIPO DE DOCUMENTO", tipoDocumento); 
    	}
	    
	    E2docEngine e2docEngine = new E2docEngine();
	    
	    com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas pastas = e2docEngine.pesquisaCompras(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("getTiposDocumentacaoLegal")
	@Produces("application/json")
	public List<String> getTiposDocumentacaoLegal() {

		E2docEngine e2docEngine = new E2docEngine();

		return e2docEngine.getTiposDocumentacaoLegal();

	}

	@POST
	@Path("pesquisarDocumentacaoLegal")
	@Produces("application/json")
	public com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas pesquisarDocumentacaoLegal( @MatrixParam("razaoSocial") String razaoSocial,
			@MatrixParam("cnpj") String cnpj, @MatrixParam("tipoDocumento") String tipoDocumento) {

		//nome da chave, valor 
		HashMap<String, String> atributosPesquisa = new HashMap<String, String>();

		if (!E2docUtils.isNullOrEmptyOrUndefined(razaoSocial)){
			atributosPesquisa.put("RAZÃO SOCIAL", "%"+razaoSocial+"%");
		}

		if (!E2docUtils.isNullOrEmptyOrUndefined(cnpj)){
			atributosPesquisa.put("CNPJ", cnpj);
		}

		if (!E2docUtils.isNullOrEmptyOrUndefined(tipoDocumento)){
			atributosPesquisa.put("TIPO DE DOCUMENTO", tipoDocumento); 
		}

		E2docEngine e2docEngine = new E2docEngine();

		com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas pastas = e2docEngine.pesquisaDocumentacaoLegal(atributosPesquisa);

		return pastas;

	}
	
	

	
	
}