package com.neomind.fusion.custom.orsegups.mobile;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.mobile.utils.MobileUtils;
import com.neomind.fusion.custom.orsegups.mobile.vo.NeoUserVO;
import com.neomind.fusion.security.NeoUser;

@WebServlet(name="MobileAuthentication", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.mobile.MobileAuthentication"})
public class MobileAuthentication extends HttpServlet
{

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		final PrintWriter out = response.getWriter();
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		String login = request.getParameter("login");
		String pass = request.getParameter("pass");
		try{
			
			System.out.println("["+key+"]Autenticando usuario mobile ->" + login);
			
			String action = request.getParameter("action");
			if (action != null)
			{
				if (action.equalsIgnoreCase("authenticate"))
				{
					String s = authetnicate(request, response);
					System.out.println("["+key+"] autenticacao ->" + s);
					if (s != null)
					{
						out.print(s);
					}
				}
				else if (action.equalsIgnoreCase("validateToken"))
				{
					String s = validateToken(request, response);
					System.out.println("["+key+"] Validacao de token ->" + s);
					if (s != null)
					{
						out.print(s);
					}
				}
			}
			
			System.out.println("["+key+"]Autenticado mobile ->" + login);
		}catch(Exception e){
			System.out.println("["+key+"] Erro na Autenticando do usuario mobile ->" + login);
			response.setStatus(401);
			out.print("NOK");
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

	}

	public String authetnicate(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String retorno = null;
		String login = request.getParameter("login");
		String pass = request.getParameter("pass");

		NeoUserVO neoUserVO = null;

		NeoUser neoUser = MobileUtils.login(login, pass);

		if (neoUser != null)
		{
			String token = MobileUtils.generateToken(neoUser);
			neoUserVO = NeoUserVO.composeVO(neoUser);
			neoUserVO.setSessionToken(token);
			Gson gson = new Gson();
			retorno = gson.toJson(neoUserVO);
		}

		return retorno;
	}

	public String validateToken(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String token = request.getParameter("token");

		NeoUserVO neoUserVO = null;

		Boolean ok = MobileUtils.validateToken(token);

		if (ok)
		{
			NeoUser neoUser = MobileUtils.getUserFromToken(token);

			if (neoUser != null)
			{
				neoUserVO = NeoUserVO.composeVO(neoUser);
				neoUserVO.setSessionToken(token);
			}
		}

		Gson gson = new Gson();

		return gson.toJson(neoUserVO);
	}

}
