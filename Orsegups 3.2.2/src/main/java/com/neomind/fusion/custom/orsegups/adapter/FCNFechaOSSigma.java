package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class FCNFechaOSSigma implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(FCNFechaOSSigma.class);
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		PreparedStatement st = null;
		String nomeFonteDados = "SIGMA90";
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		
		StringBuffer sql= new StringBuffer();
		
		sql.append("UPDATE dbORDEM ");
		sql.append("SET FECHADO = 1, OPFECHOU = 11010, FECHAMENTO = GETDATE(), IDOSCAUSADEFEITO = 1, IDOSSOLUCAO = 249, EXECUTADO = ? "); 
		sql.append("WHERE ID_ORDEM = ?");

		String textoFechmento = "";
		
		String processName = activity.getProcessName();
		if(processName != null && processName.equalsIgnoreCase(OrsegupsUtils.CANCELAMENTO_CONTRATO_INICIATIVA))
		{
			textoFechmento = "Fechamento automático pela tarefa "+activity.getProcess().getCode()+" do processo "+OrsegupsUtils.CANCELAMENTO_CONTRATO_INICIATIVA;
		}
		else if(processName != null && processName.equalsIgnoreCase(OrsegupsUtils.CANCELAMENTO_CONTRATO_INADIMPLENCIA))
		{
			textoFechmento = "Fechamento automático pela tarefa "+activity.getProcess().getCode()+" do processo "+OrsegupsUtils.CANCELAMENTO_CONTRATO_INADIMPLENCIA;
		}
		
		try
		{
			st = connection.prepareStatement(sql.toString());
			//UPDATE
			st.setString(1, textoFechmento);
			//WHERE
			st.setString(2, "");
			
			//st.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					log.error("Erro ao fechar o statement");
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
