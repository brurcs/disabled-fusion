package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServiceProxy;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RotinaDeslocarEventoViaturaRota implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaDeslocarEventoViaturaRota.class);
	
	@Override
	public void execute(CustomJobContext arg0)
	{
		this.log.warn("##### INICIAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		String returnFromAccess = "";

		try
		{
			/**
			 * Com o uso do aplicativo VTR MOBILE, o trabalho do operador de deslocamento (CM) passou a
			 * ser a avaliação
			 * e redistribuição dos atendimentos (Evento x Viatura), conforme distância, idade e
			 * prioridade. Com isto,
			 * o papel de DESLOCAR passou a ser uma operação mecânica. A ação do operador é posterior ao
			 * deslocamento.
			 * Entretanto, como o SIGMA ainda não executa este papel automaticamente, o operador é
			 * onerado (em tempo)
			 * tendo que realizar este trabalho manualmente, para a viatura da rota do evento.
			 * O objetivo deste robô é DESLOCAR um evento que esteja no status ESPERA, para a viatura da
			 * ROTA correspondente a sua conta.
			 * 
			 * Para tanto, o SIGMA disponibiliza um WEBSERVICE:Serviço:
			 * http://www.segware.com.br/dev/SigmaWebService.html#Deslocar-Evento-WebService
			 * Ele espera dois parâmetros:
			 * - Código do evento a ser deslocado
			 * - Código da Viatura a ser deslocada
			 */

			conn = PersistEngine.getConnection("SIGMA90");

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO, REPLACE(SUBSTRING(r.NM_ROTA, CHARINDEX('AA', r.NM_ROTA), 4), '-', '') AS ROTA, SUBSTRING(r.NM_ROTA, 1, 3) AS REG, ");
			sql.append(" c.ID_CENTRAL, c.PARTICAO ");
			sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			sql.append(" INNER JOIN rota r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			sql.append(" INNER JOIN dbCODE cuc WITH (NOLOCK) ON cuc.ID_CODE = h.CD_CODE ");
			sql.append(" WHERE h.FG_STATUS = 1 AND h.CD_HISTORICO_PAI IS NULL ");
			//sql.append(" AND h.CD_CLIENTE = 50931 ");
			sql.append(" AND r.NM_ROTA LIKE '%AA%' ");
			sql.append(" AND (cuc.TIPO = 1 OR h.CD_EVENTO LIKE 'XXX7') ");
			sql.append(" AND h.CD_EVENTO NOT LIKE 'XXX8' ");
			//Temporario (Regionais que usam o VTR MOBILE) - Quando todas usarem, remover
			sql.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ('CTA', 'SOO', 'BQE') ");
			sql.append(" ORDER BY h.DT_RECEBIDO ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String rota = rs.getString("ROTA");
				String regional = rs.getString("REG");
				String central = null;
				String particao = null;
				
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLOpFilter("motorista","LIKE", "%"+rota+"%"));
				groupFilter.addFilter(new QLEqualsFilter("codigoRegional", regional));
				groupFilter.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
				System.out.println(PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), groupFilter));
				List<NeoObject> listOTSViatura = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), groupFilter);
				
				central = rs.getString("ID_CENTRAL");
				particao = rs.getString("PARTICAO");
				
				if (NeoUtils.safeIsNotNull(listOTSViatura) && listOTSViatura.size() == 1)
				{
					NeoObject neoOts = listOTSViatura.get(0);
					EntityWrapper ewOts = new EntityWrapper(neoOts);
					String idViatura = (String) ewOts.findField("codigoViatura").getValue();
					Long idEvento = (Long) rs.getLong("CD_HISTORICO");

					returnFromAccess = executaServicoSegware(Long.valueOf(idViatura), idEvento);
					
				}
				else if (NeoUtils.safeIsNotNull(listOTSViatura) && (listOTSViatura.size() > 1 || listOTSViatura.isEmpty()))
				{
					if(listOTSViatura.isEmpty())
						returnFromAccess = "<strong> Não foram encontradas informações! </strong> <br>";
					else
					if(listOTSViatura.size() > 1){
						returnFromAccess += "<strong> Existem "+listOTSViatura.size()+" viaturas relacionadas favor verificar!. </strong> <br>";
						for(NeoObject neo : listOTSViatura){ 
							EntityWrapper ew = new EntityWrapper(neo);
							returnFromAccess += "<strong> Código Viadura:  </strong>"+ (String) ew.findField("codigoViatura").getValue() + " <br>";
							returnFromAccess += "<strong> Código Cliente:  </strong>"+ (String) ew.findField("codigoCliente").getValue() + " <br>";
							returnFromAccess += "<strong> Código Regional:  </strong>"+ (String) ew.findField("codigoRegional").getValue() + " <br>";
							returnFromAccess += "<strong> Placa: </strong>"+ (String) ew.findField("placa").getValue()+ " <br>";
							returnFromAccess += "<strong> Título: </strong>"+ (String) ew.findField("titulo").getValue()+ " <br>";
							returnFromAccess += "<strong> Descrição: </strong>"+ (String) ew.findField("descricao").getValue()+ " <br>";
							returnFromAccess += "<strong> Motorista: </strong>"+ (String) ew.findField("motorista").getValue()+ " <br>";
							returnFromAccess += "<strong> Telefone:  </strong>"+ (String) ew.findField("telefone").getValue() + " <br>";
							String utilizando = "Não";
							Boolean emUso = (Boolean) ew.findField("emUso").getValue();
							if(emUso)
								utilizando = "Sim";
							returnFromAccess += "<strong> Em uso: </strong>"+ utilizando + " <br>";
							returnFromAccess += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";
							
						}
						
					}	
					
				}
				
						String retorno = null; 
					if (!returnFromAccess.equals("ACK")){
						retorno = verificaRetornoWebService(returnFromAccess);
						returnFromAccess = "<strong> Erro: </strong>"+ retorno + " <br>";
					}
					 if((NeoUtils.safeIsNotNull(listOTSViatura) && (listOTSViatura.size() > 1 || listOTSViatura.isEmpty())) || (!returnFromAccess.equals("ACK")))
						enviaEmailRotina(central, particao, returnFromAccess);
					
			}

			this.log.warn("##### EXECUTAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			e.printStackTrace();
			this.log.error("##### ERRO ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			enviaEmailRotina(null, null, returnFromAccess);
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				this.log.error("##### ERRO ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			this.log.warn("##### FINALIZAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
	
	private String verificaRetornoWebService(String retorno){
		
		
		if(retorno.equals("VIATURA_NAO_ENCONTRADA"))
			retorno = "Não é possível identificar o veículo através do parâmetro idRastreador.";
		else
		if(retorno.equals("EVENTO_NAO_ENCONTRADO"))
			retorno = "Não é possível identificar o evento através de parâmetro idEvento.";
		else
		if(retorno.equals("CLIENTE_NAO_PERMITE_DESLOCAMENTO"))
			retorno = "O cliente pertence ao evento é configurado para não permitir deslocamentos carro.";
		else
		if(retorno.equals("CUC_NAO_PERMITE_DESLOCAMENTO"))
			retorno = "O CUC qual o evento pertence está configurado para não permitir deslocamentos de carro.";
		else
		if(retorno.equals("CLIENTE_E_CUC_NAO_PERMITEM_DESLOCAMENTO"))
			retorno = "Tanto o cliente e o CUC que pertence ao evento são configurados para não permitir deslocamentos carro.";
		else
		if(retorno.equals("ERRO_ATUALIZAR_STATUS_EVENTO"))
			retorno = "Ocorreu um erro inesperado ao atualizar o status do evento";
		else
		if(retorno.equals("OPERACAO_ILEGAL"))
			retorno = "A mudança do status atual do evento para o estado entrou não é permitido. Um evento no estado - On Site - ou - serviço feito - não pode ter seu status alterado para - Offset.";
		else
		if(retorno.equals("VIATURA_JA_DESLOCADA"))
			retorno = "O carro foi movido para outro evento. Esta mensagem só será devolvido se o nas configurações do desktop Sigma - Ativar compensado por um carro limitação - está habilitado.";
		else
		if(retorno.equals("DESLOCAMENTO_DE_EVENTO_FILHO_NAO_PERMITIDO"))
			retorno = "A mensagem retornada quando o agrupamento de eventos habilitado, eo evento a ser movido é um evento em cluster.";
		else
		if(retorno.equals("ACK"))
			retorno = "Operação bem-sucedida.";
		else
			retorno = "WebServiceSigma não retornou uma mensagem válida!. Mensagem de retorno: "+retorno;
		
		return retorno;
	}
	
	@SuppressWarnings("finally")
	private String executaServicoSegware(Long idViatura, Long idEvento)
	{

		String returnFromAccess = "";
		try
		{
			DeslocarEventoWebServiceProxy webServiceProxy = new DeslocarEventoWebServiceProxy();
			
			returnFromAccess = webServiceProxy.deslocarEvento(idEvento, null, idViatura);
			
			this.log.warn("##### SUCESSO ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - RETORNOU " + returnFromAccess + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage().toString();
			this.log.error("##### ERRO AO EXECUTAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - RETORNOU " + returnFromAccess + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		finally
		{
			return returnFromAccess;
		}
	}

	private void enviaEmailRotina(String central, String particao, String returnFromAccess)
	{

		try
		{

			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			mailClone.setFromEMail("nao_responda@orsegups.com.br");

			mailClone.setFromName("Orsegups Partipações S.A.");
			if (mailClone != null && mailClone.isEnabled())
			{

				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();

				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) <= 12)
				{
					saudacaoEMail = "Bom dia, ";
				}
				else if (saudacao.get(Calendar.HOUR_OF_DAY) > 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
				{
					saudacaoEMail = "Boa tarde, ";
				}
				else
				{
					saudacaoEMail = "Boa noite, ";
				}

				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("cm@orsegups.com.br");
				noUserEmail.addTo("emailautomatico@orsegups.com.br");

				noUserEmail.setSubject("Não Responda - Rotina deslocamento evento em espera para viatura responsável rota. ");

				noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				noUserMsg.append("            </td>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail + " favor verificar erro no deslocamento evento em espera para viatura responsável rota.</strong><br>");
				if (NeoUtils.safeIsNotNull(central) && NeoUtils.safeIsNotNull(particao))
					noUserMsg.append("			<strong>Conta: </strong> " + central + "[" + particao + "]" + " <br>");
				if (NeoUtils.safeIsNull(central) && NeoUtils.safeIsNull(particao))
					noUserMsg.append("			<strong>Erro: </strong> Por favor, verificar erro. <br>");
				if (NeoUtils.safeIsNotNull(returnFromAccess))
					noUserMsg.append( returnFromAccess.toString() );
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");
				noUserEmail.setHtmlMsg(noUserMsg.toString());
				noUserEmail.setContent(noUserMsg.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			this.log.error("##### ERRO AO ENVIAR EMAIL ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}

	}
	
	
}
