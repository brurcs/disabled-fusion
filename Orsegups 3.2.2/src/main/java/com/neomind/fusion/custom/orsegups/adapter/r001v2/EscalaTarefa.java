package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class EscalaTarefa implements ActivityStartEventListener, TaskFinishEventListener {

	
	@Override
	public void onStart(ActivityEvent event) throws ActivityException {
		
		System.out.println("Entrou: R001V2 ActivityStartEventListener");
		
		EntityWrapper processEntity = event.getWrapper();
		String _atividade = event.getActivity().getInstance().getTaskName(); 
		SecurityEntity seExecutor = null;
		
		final Activity act = event.getActivity();
		final Long activityNeoId = act.getNeoId();
		
		if (_atividade.contains("ESCALADA: Informar Colaborador de Férias") ||
				_atividade.contains("ESCALADA: Comunicar Vigilante Cobertura de Férias")) {
			
			seExecutor = (SecurityEntity) processEntity.findField("executorRHO").getValue();
			seExecutor = efetuaEscalada(processEntity,seExecutor,activityNeoId,"executorRHO");
			
		} else if (_atividade.contains("ESCALADA: Processar Férias")) {
			
			seExecutor = (SecurityEntity) processEntity.findField("executorRH").getValue();
			seExecutor = efetuaEscalada(processEntity,seExecutor,activityNeoId,"executorRH");
			
		} else if (_atividade.contains("ESCALADA: Confirmar Pagamento Férias - Anexar Comprovante Bancário")) {
		
			seExecutor = (SecurityEntity) processEntity.findField("executorRHComprovante").getValue();
			seExecutor = efetuaEscalada(processEntity,seExecutor,activityNeoId,"executorRHComprovante");
		
		}else if (_atividade.contains("ESCALADA: Informar Pagamento de Férias em Espécie")) {
			
			seExecutor = (SecurityEntity) processEntity.findField("executorControladoria").getValue();
			seExecutor = efetuaEscalada(processEntity,seExecutor,activityNeoId,"executorControladoria");
			
		} else if (_atividade.contains("ESCALADA: Validar Recibo de Férias")) {
		
			seExecutor = (SecurityEntity) processEntity.findField("executorFinanceiro").getValue();
			seExecutor = efetuaEscalada(processEntity,seExecutor,activityNeoId,"executorFinanceiro");
			
		} else if (_atividade.contains("ESCALADA: Realizar Pagamento de Férias e anexar Recibo Assinado")) {
		
			seExecutor = (SecurityEntity) processEntity.findField("executorPagtoFerias").getValue();
			seExecutor = efetuaEscalada(processEntity,seExecutor,activityNeoId,"executorPagtoFerias");
			
		}
		
	}
	
	private NeoUser efetuaEscalada(EntityWrapper processEntity, SecurityEntity seExecutor, Long activityNeoId, String campo) {
		
		NeoUser usuario = new NeoUser();
		
		if  (seExecutor instanceof NeoPaper) {
			for (NeoUser noUser : seExecutor.getAllUsers())
			{
				usuario = noUser;
				break;
			}
		} else {
			usuario = (NeoUser) seExecutor;
		}
		
		NeoGroup grupo = usuario.getGroup();
		
		if(grupo.getGroupLevel() >= 500) {
			
			usuario = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(grupo.getUpperLevel().getResponsible());
			
		}else {
			
			while ((grupo.getGroupLevel() < 500))
			{
				grupo = grupo.getUpperLevel();
				if (NeoUtils.safeIsNull(grupo))
				{
					throw new WorkflowException("Não foi possível obter a hierarquia de grupo do usuário executor.");
				}
			}
			
			usuario = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(grupo.getResponsible());
		}
		
		
		GregorianCalendar dataAuxiliar = (GregorianCalendar) processEntity.findGenericValue("dataLimiteProcessarFeriasAux");
		
		dataAuxiliar = OrsegupsUtils.getSpecificWorkDay(dataAuxiliar, 2L);
		
		processEntity.findField("dataLimiteProcessarFeriasAux").setValue(dataAuxiliar);
		processEntity.findField("executorRH").setValue(usuario);
		
		Activity act2 = PersistEngine.getNeoObject(Activity.class, activityNeoId);
		Task task = act2.getModel().getTaskAssigner().assign((UserActivity) act2, usuario, true);
		
		if (!usuario.getCode().equals("dilmoberger") && !usuario.toString().equals("presidente"))
			RuntimeEngine.getTaskService().complete(task, usuario);
			
		
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades"); 
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
		
		GregorianCalendar dataAcao = new GregorianCalendar();

		wRegAti.setValue("usuario", "SISTEMA FUSION");
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("descricao", "Tarefa ESCALADA para " + usuario.getFullName());
		wRegAti.setValue("anexo", null);

		processEntity.findField("r001V2RegistroAtividade").addValue(objRegAti);
		
		return usuario;
	}

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
		
		String _atividade = event.getActivity().getInstance().getTaskName(); 
		
		EntityWrapper processEntity = event.getWrapper();
		NeoUser usuario = event.getTask().getFinishByUser();
		System.out.println(usuario);
		
		if (usuario.getCode().equals("dilmoberger") || usuario.toString().equals("presidente")) {
			
			if (_atividade.contains("ESCALADA: Processar Férias")) {
				processEntity.findField("executorRH").setValue(processEntity.findField("executorTarefa").getValue());
			} else {
				processEntity.findField("executorRHO").setValue(processEntity.findField("executorTarefa").getValue());				
			}
			
			InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades"); 
			NeoObject objRegAti = insRegAti.createNewInstance();
			EntityWrapper wRegAti = new EntityWrapper(objRegAti);
			
			
			GregorianCalendar dataAcao = new GregorianCalendar();

			wRegAti.setValue("usuario", usuario.getFullName());
			wRegAti.setValue("dataAcao", dataAcao);
			wRegAti.setValue("descricao", processEntity.findField("obsPresidente").getValue());
			wRegAti.setValue("anexo", null);

			processEntity.findField("r001V2RegistroAtividade").addValue(objRegAti);
			
		}		
	}
}
