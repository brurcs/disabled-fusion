/**
 * ModeloPlanoGerarContasOutGridCtaResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano;

public class ModeloPlanoGerarContasOutGridCtaResult  implements java.io.Serializable {
    private java.lang.Integer codMpc;

    private java.lang.Integer ctaRed;

    private java.lang.String logCta;

    public ModeloPlanoGerarContasOutGridCtaResult() {
    }

    public ModeloPlanoGerarContasOutGridCtaResult(
           java.lang.Integer codMpc,
           java.lang.Integer ctaRed,
           java.lang.String logCta) {
           this.codMpc = codMpc;
           this.ctaRed = ctaRed;
           this.logCta = logCta;
    }


    /**
     * Gets the codMpc value for this ModeloPlanoGerarContasOutGridCtaResult.
     * 
     * @return codMpc
     */
    public java.lang.Integer getCodMpc() {
        return codMpc;
    }


    /**
     * Sets the codMpc value for this ModeloPlanoGerarContasOutGridCtaResult.
     * 
     * @param codMpc
     */
    public void setCodMpc(java.lang.Integer codMpc) {
        this.codMpc = codMpc;
    }


    /**
     * Gets the ctaRed value for this ModeloPlanoGerarContasOutGridCtaResult.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this ModeloPlanoGerarContasOutGridCtaResult.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the logCta value for this ModeloPlanoGerarContasOutGridCtaResult.
     * 
     * @return logCta
     */
    public java.lang.String getLogCta() {
        return logCta;
    }


    /**
     * Sets the logCta value for this ModeloPlanoGerarContasOutGridCtaResult.
     * 
     * @param logCta
     */
    public void setLogCta(java.lang.String logCta) {
        this.logCta = logCta;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModeloPlanoGerarContasOutGridCtaResult)) return false;
        ModeloPlanoGerarContasOutGridCtaResult other = (ModeloPlanoGerarContasOutGridCtaResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codMpc==null && other.getCodMpc()==null) || 
             (this.codMpc!=null &&
              this.codMpc.equals(other.getCodMpc()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.logCta==null && other.getLogCta()==null) || 
             (this.logCta!=null &&
              this.logCta.equals(other.getLogCta())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodMpc() != null) {
            _hashCode += getCodMpc().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getLogCta() != null) {
            _hashCode += getLogCta().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModeloPlanoGerarContasOutGridCtaResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "modeloPlanoGerarContasOutGridCtaResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMpc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMpc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "logCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
