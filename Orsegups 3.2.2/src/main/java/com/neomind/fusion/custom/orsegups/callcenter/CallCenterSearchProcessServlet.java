package com.neomind.fusion.custom.orsegups.callcenter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name="CallCenterSearchProcessServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterSearchProcessServlet"},asyncSupported=true)
public class CallCenterSearchProcessServlet extends HttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	public static boolean clienteComOSAbertas(int codCliente)
	{

		PreparedStatement pst = null;
		Connection conn = OrsegupsUtils.getConnection("SIGMA90");
		ResultSet rs = null;
		boolean retorno = false;
		try
		{

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT TOP 1 os.ID_ORDEM AS N_OS, ");
			sql.append("os.CD_CLIENTE AS CLIENTE, ");
			sql.append("os.abertura AS ABERTURA_DA_OS, ");
			sql.append("os.defeito AS DEFEITO, ");
			sql.append("p.DT_PAUSA AS DATA_PAUSA, ");
			sql.append("mp.NM_DESCRICAO AS DESC_PAUSA, ");
			sql.append("p.TX_OBSERVACAO AS OBS_PAUSA ");
			sql.append("FROM dbo.dbORDEM os ");
			sql.append("LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO =  ");
			sql.append("os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX ");
			sql.append("(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA ");
			sql.append("p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO) ");
			sql.append("LEFT JOIN MOTIVO_PAUSA mp WITH (NOLOCK) ON mp.CD_MOTIVO_PAUSA =  ");
			sql.append("p.CD_MOTIVO_PAUSA ");
			sql.append("WHERE ");
			sql.append("os.FECHAMENTO IS NULL AND ");
			sql.append("os.FECHADO <> 1 AND ");
			sql.append("os.OPFECHOU IS NULL AND ");
			sql.append("os.CD_CLIENTE = " + codCliente);
			sql.append(" ORDER BY abertura ");

			pst = conn.prepareStatement(sql.toString());

			rs = pst.executeQuery();

			if (rs.next())
				retorno = true;

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pst, rs);
			return retorno;
		}
	}

	public static boolean clienteComOSPausada(int codCliente)
	{
		
		PreparedStatement pst = null;
		Connection conn = OrsegupsUtils.getConnection("SIGMA90");
		ResultSet rs = null;
		boolean retorno = false;
		try
		{
		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT TOP 1 os.ID_ORDEM AS N_OS,  os.CD_CLIENTE AS CLIENTE,  os.abertura AS ABERTURA_DA_OS, os.FECHAMENTO AS OS_FECHADA, col.NM_COLABORADOR, ");
		sql.append(" os.defeito AS DEFEITO,  p.DT_PAUSA AS DATA_PAUSA,  mp.NM_DESCRICAO AS Tipo_Pausa,  p.TX_OBSERVACAO AS MOTIVO ");
		sql.append(" FROM dbo.dbORDEM os ");
		sql.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO =  os.ID_ORDEM ");
		sql.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
		sql.append(" AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX (p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) ");
		sql.append(" FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 ");
		sql.append(" WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO) ");
		sql.append(" LEFT JOIN MOTIVO_PAUSA mp WITH (NOLOCK) ON mp.CD_MOTIVO_PAUSA =  p.CD_MOTIVO_PAUSA ");
		sql.append(" WHERE  os.FECHAMENTO IS not  NULL ");
		sql.append(" AND  os.FECHADO = 1 ");
		sql.append(" AND os.OPFECHOU IS not NULL ");
		sql.append(" AND  os.CD_CLIENTE = " + codCliente);
		sql.append(" ORDER BY abertura desc");

		pst = conn.prepareStatement(sql.toString());

		rs = pst.executeQuery();

		if (rs.next())
			retorno = true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pst, rs);
			return retorno;
		}
	}

	public static boolean getRSC(String codCli)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		boolean flag = false;
		try
		{
			conn = PersistEngine.getConnection("FUSIONPROD");

			StringBuilder sql = new StringBuilder();
			/*sql.append(" SELECT TOP 1 p.PROCESSSTATE ");
			sql.append(" FROM wfProcess AS p WITH (NOLOCK)  ");
			sql.append(" INNER JOIN D_RSCRelatorioSolicitacaoCliente AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId ");
			//sql.append(" INNER JOIN processmodel AS pm WITH (NOLOCK)  ON pm.neoid = p.model_neoid ");
			sql.append(" INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rsc.clienteSapiens_neoId ");
			sql.append(" WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relatório de Solicitação de Cliente%') ");
			sql.append(" AND cli.codcli = ? ");
			sql.append(" AND p.PROCESSSTATE IN(0) AND p.saved = 1 ORDER BY p.STARTDATE ");*/
			
			sql.append(" declare @codcli as numeric ");
			sql.append(" select @codcli =  ? ");  
			sql.append(" select  have.PROCESSSTATE from "); 
            sql.append("            ( ");
            sql.append("                   ( ");  
            sql.append("                    SELECT TOP 1 p.PROCESSSTATE "); 
            sql.append("                    FROM wfProcess AS p WITH (NOLOCK) ");  
            sql.append("                    INNER JOIN D_RSCRelatorioSolicitacaoCliente AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId "); 

            sql.append("                    INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rsc.clienteSapiens_neoId "); 
            sql.append("                    WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relatório de Solicitação de Cliente%') "); 
            sql.append("                    AND cli.codcli = @codcli ");
            sql.append("                    AND p.PROCESSSTATE IN(0) AND p.saved = 1 ");  
            sql.append("                    ORDER BY p.STARTDATE "); 
            sql.append("                    ) ");
            sql.append("                    union all ");
            sql.append("                    ( ");
            sql.append("                    SELECT TOP 1 p.PROCESSSTATE "); 
            sql.append("                    FROM wfProcess AS p WITH (NOLOCK) ");  
            sql.append("                    INNER JOIN D_RSCRelatorioSolicitacaoCliente AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId "); 

            sql.append("                    INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rsc.clienteSapiens_neoId "); 
            sql.append("                    WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relatório de Solicitação de Cliente Novo%') "); 
            sql.append("                    AND cli.codcli = @codcli ");
            sql.append("                    AND p.PROCESSSTATE IN(0) AND p.saved = 1 "); 
            sql.append("                    ORDER BY p.STARTDATE "); 
            sql.append("                    ) ");  
            sql.append("           ) have ");

			//BUSCA-SE REGISTROS NA BASE
			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, codCli);
			rs = pstm.executeQuery();
			if (rs.next())
			{
				flag = true;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return flag;
		}
	}

	public static boolean getRRC(String codCli)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		boolean flag = false;
		try
		{
			conn = PersistEngine.getConnection("FUSIONPROD");

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT TOP 1 p.PROCESSSTATE ");
			sql.append(" FROM wfProcess AS p WITH (NOLOCK)  ");
			sql.append(" INNER JOIN D_RRCRelatorioReclamacaoCliente AS rrc WITH (NOLOCK) ON rrc.neoid = p.entity_neoId ");
			//sql.append(" INNER JOIN processmodel AS pm WITH (NOLOCK)  ON pm.neoid = p.model_neoid ");
			sql.append(" INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rrc.clienteSapiens_neoId ");
			sql.append(" WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%Q005 - RRC - Relatório de Reclamação de Cliente%') ");
			sql.append(" AND cli.codcli = ? ");
			sql.append(" AND p.PROCESSSTATE IN(0) AND p.saved = 1 ORDER BY p.STARTDATE ");

			//BUSCA-SE REGISTROS NA BASE
			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, codCli);
			rs = pstm.executeQuery();
			if (rs.next())
			{
				flag = true;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return flag;
		}
	}

	@SuppressWarnings("unchecked")
	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("ISO-8859-1");
		PrintWriter out;
		
		out = resp.getWriter();
		String action = "";

		if (req.getParameter("action") != null)
		{
			action = req.getParameter("action");
		}
		
		if (action.equalsIgnoreCase("getRSC"))
		{
			this.getRSC(req, resp, out);
		}
		else if (action.equalsIgnoreCase("getRRC"))
		{
			this.getRRC(req, resp, out);
		}
		else
		{
		long codigoCliente = NeoUtils.safeLong(req.getParameter("codigoCliente"));
		String pausa = req.getParameter("pausada");

		StringBuilder sql = new StringBuilder();
		if (pausa == null)
		{
			sql.append("SELECT os.ID_ORDEM AS N_OS, ");
			sql.append("os.CD_CLIENTE AS CLIENTE, ");
			sql.append("os.abertura AS ABERTURA_DA_OS, ");
			sql.append("os.defeito AS DEFEITO, ");
			sql.append("p.DT_PAUSA AS DATA_PAUSA, ");
			sql.append("mp.NM_DESCRICAO AS DESC_PAUSA, ");
			sql.append("p.TX_OBSERVACAO AS OBS_PAUSA, os.DATAAGENDADA  ");
			sql.append("FROM dbo.dbORDEM os ");
			sql.append("LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO =  ");
			sql.append("os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX ");
			sql.append("(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA ");
			sql.append("p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO) ");
			sql.append("LEFT JOIN MOTIVO_PAUSA mp WITH (NOLOCK) ON mp.CD_MOTIVO_PAUSA =  ");
			sql.append("p.CD_MOTIVO_PAUSA ");
			sql.append("WHERE ");
			sql.append("os.FECHAMENTO IS NULL AND ");
			sql.append("os.FECHADO <> 1 AND ");
			sql.append("os.OPFECHOU IS NULL AND ");
			sql.append("os.CD_CLIENTE = " + codigoCliente);
			sql.append(" ORDER BY abertura ");
		}
		else
		{
			sql.append(" SELECT os.ID_ORDEM AS N_OS,  os.CD_CLIENTE AS CLIENTE,  os.abertura AS ABERTURA_DA_OS, os.FECHAMENTO AS OS_FECHADA, ");
			sql.append(" os.defeito AS DEFEITO, p.DT_PAUSA AS DATA_PAUSA, p.TX_OBSERVACAO AS MOTIVO, col.NM_COLABORADOR, mp.NM_DESCRICAO AS Tipo_Pausa, os.DATAAGENDADA   ");
			sql.append(" FROM dbo.dbORDEM os ");
			sql.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO =  os.ID_ORDEM ");
			sql.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
			sql.append(" AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX (p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) ");
			sql.append(" FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 ");
			sql.append(" WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO) ");
			sql.append(" LEFT JOIN MOTIVO_PAUSA mp WITH (NOLOCK) ON mp.CD_MOTIVO_PAUSA =  p.CD_MOTIVO_PAUSA ");
			sql.append(" WHERE  os.FECHAMENTO IS not  NULL ");
			sql.append(" AND  os.FECHADO = 1 ");
			sql.append(" AND os.OPFECHOU IS not NULL ");
			sql.append(" AND  os.CD_CLIENTE = " + codigoCliente);
			sql.append(" ORDER BY ABERTURA_DA_OS desc  ");
		}

		

		out.println("<fieldset>");
		out.println("<table class='gridbox' cellpadding='0' cellspacing='0' style='empty-cells: show' >");

		if (pausa == null)
		{
			out.println("<thead>");
			out.println("<tr>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Código Cliente") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data Abertura da OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data Agendamento da OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Defeito") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data da Pausa OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Motivo da Pausa") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Obs da Pausa") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Técnico Responsável") + "</th>");
			out.println("</tr>");
			out.println("</thead>");
			out.println("<tbody>");
		}
		else
		{
			out.println("<thead>");
			out.println("<tr>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Código Cliente") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data Abertura da OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data Agendamento da OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data Fechamento da OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Defeito") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data da Pausa OS") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Motivo da Pausa") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Técnico Responsável") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Tipo da Pausa") + "</th>");
			out.println("</tr>");
			out.println("</thead>");
			out.println("<tbody>");
		}

		Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
		query.setMaxResults(3);
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] os = (Object[]) result;

					int idOrdem = -1;
					int cdCliente = -1;
					String abertura = null;
					String fechado = null;
					String defeito = "";
					String dtPausa = null;
					String nm_descricao = "";
					String obs = "";
					String tec = "";
					String tipoPausa = "";
					String dataAgendada = "";

					try
					{
						if (pausa == null)
						{
							idOrdem = (Integer) os[0];
							cdCliente = (Integer) os[1];
							abertura = NeoUtils.safeOutputString(os[2]);
							/**
						 	* @author orsegups lucas.avila - alterado Clob Hibernate.
						 	* @date 08/07/2015
						 	*/
							Clob c = (Clob) os[3];
							BufferedReader bf = new BufferedReader(c.getCharacterStream()); 

							String linha = null;
							String texto = "";

							int count = 0;
							while ((linha = bf.readLine()) != null)
							{
								texto = texto + linha;
								texto += "\n";

							}

							defeito = texto;
							dtPausa = NeoUtils.safeOutputString(os[4]);
							nm_descricao = NeoUtils.safeOutputString(os[5]);
							obs = NeoUtils.safeOutputString(os[6]);
							dataAgendada = NeoUtils.safeOutputString(os[7]);
						}
						else
						{
							idOrdem = (Integer) os[0];
							cdCliente = (Integer) os[1];
							abertura = NeoUtils.safeOutputString(os[2]);

							fechado = NeoUtils.safeOutputString(os[3]);

							/**
						 	* @author orsegups lucas.avila - alterado Clob Hibernate.
						 	* @date 08/07/2015
						 	*/
							Clob c = (Clob) os[4];
							BufferedReader bf = new BufferedReader(c.getCharacterStream()); 

							String linha = null;
							String texto = "";

							int count = 0;
							while ((linha = bf.readLine()) != null)
							{
								texto = texto + linha;
								texto += "\n";

							}

							defeito = texto;

							dtPausa = NeoUtils.safeOutputString(os[5]);
							nm_descricao = NeoUtils.safeOutputString(os[6]);
							tec = NeoUtils.safeOutputString(os[7]);
							tipoPausa = NeoUtils.safeOutputString(os[8]);
							dataAgendada = NeoUtils.safeOutputString(os[9]);
						}

						Connection connSapiens = null;
						PreparedStatement st3 = null;
						ResultSet rsEquip = null;
						connSapiens = PersistEngine.getConnection("SAPIENS");
						StringBuffer sqlEquip = new StringBuffer();
						sqlEquip.append(" SELECT cvs.USU_CodSer ");
						sqlEquip.append(" FROM USU_T160SIG sig ");
						sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
						sqlEquip.append(" WHERE sig.usu_codcli = " + cdCliente + " ");
						sqlEquip.append(" AND cvs.usu_codser IN ('9002035', '9002011', '9002004', '9002005', '9002014') ");
						sqlEquip.append(" ORDER BY cvs.usu_sitcvs ");
						st3 = connSapiens.prepareStatement(sqlEquip.toString());
						rsEquip = st3.executeQuery();

						//SOO - COM CAD - SUELLEN ROSENBROCK
						Integer colEquip = 108294;
						
						if (rsEquip.next())
						{
							//CM - COLABORADOR PADRAO
							//108294 - Bruno Brasil
							colEquip = 89428;
						}
						OrsegupsUtils.closeConnection(connSapiens, st3, rsEquip);
						NeoObject colaborador = null;

						QLEqualsFilter colaboradorFilter = new QLEqualsFilter("cd_colaborador", colEquip);
						List<NeoObject> colaboradores = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACOLABORADOR"), colaboradorFilter);

						if (colaboradores != null && !colaboradores.isEmpty())
						{
							colaborador = colaboradores.get(0);
							EntityWrapper entityWrapper = new EntityWrapper(colaborador);
							tec = (String) entityWrapper.findValue("nm_colaborador");
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					if (pausa == null)
					{
						out.println("<tr>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(idOrdem) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(cdCliente) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(abertura) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(dataAgendada) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(defeito) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(dtPausa) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(nm_descricao) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(obs) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(tec) + "</td>");
						out.println("</tr>");
					}
					else
					{
						out.println("<tr>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(idOrdem) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(cdCliente) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(abertura) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(dataAgendada) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(fechado) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(defeito) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(dtPausa) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(nm_descricao) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(tec) + "</td>");
						out.println("<td>" + NeoUtils.encodeHTMLValue(tipoPausa) + "</td>");
						out.println("</tr>");
					}
				}
			}
		}
		
		out.println("</tbody>");
		out.println("</table>");
		out.println("</fieldset>");
		}
	}

	private void getRSC(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String codCli = req.getParameter("codigoCliente");
		try
		{
			conn = PersistEngine.getConnection("FUSIONPROD");

			StringBuilder sql = new StringBuilder();
			/*sql.append("   SELECT p.neoId,code as codigo,name as fluxo,cpfCnpj2 as cnpj,nomeCliente as cliente,contato as contato,rscc.descricao as categoria  ");
			sql.append("   ,NUS.FULLNAME as usuario,rrcd.descricao as descricao  ");
			sql.append("   ,rrcd.siglaRegional as sigla,rsc.prazoAtendimento as prazo,p.startDate as dataInicial,p.finishDate as dataFinal,p.processState as situacao   ");
			sql.append("   FROM wfProcess AS p WITH (NOLOCK)  ");
			sql.append("   INNER JOIN NEOUSER AS NUS WITH(NOLOCK) ON NUS.NEOID = P.REQUESTER_NEOID ");
			sql.append("   INNER JOIN D_RSCRelatorioSolicitacaoCliente AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId ");
			sql.append("   INNER JOIN D_RSCCategorias AS rscc WITH(NOLOCK) ON rsc.categoriasRsc_neoid = rscc.neoid ");
			sql.append("   INNER JOIN D_RRCDestino AS rrcd WITH (NOLOCK) ON rrcd.neoid = rsc.destino_neoid ");
			sql.append("   INNER JOIN processmodel AS pm WITH (NOLOCK)  ON pm.neoid = p.model_neoid ");
			sql.append("   INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rsc.clienteSapiens_neoId  ");
			sql.append("   WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relatório de Solicitação de Cliente%')  ");
			sql.append("   AND cli.codcli = ?  ");
			sql.append("   AND p.PROCESSSTATE IN(0) AND p.saved = 1 ORDER BY p.STARTDATE ");*/
			
			  sql.append(" declare @codcli as numeric ");
			  sql.append(" select @codcli =  ? ");
			  sql.append(" ( ");
              sql.append("                 SELECT p.neoId,code as codigo,name as fluxo,cpfCnpj2 as cnpj,nomeCliente as cliente,contato as contato,rscc.descricao as categoria ");  
			  sql.append("  ,NUS.FULLNAME as usuario,rrcd.descricao as descricao ");  
			  sql.append("  ,rrcd.siglaRegional as sigla,rsc.prazoAtendimento as prazo,p.startDate as dataInicial,p.finishDate as dataFinal,p.processState as situacao ");   
			  sql.append("  FROM wfProcess AS p WITH (NOLOCK) ");  
			  sql.append("  INNER JOIN NEOUSER AS NUS WITH(NOLOCK) ON NUS.NEOID = P.REQUESTER_NEOID "); 
			  sql.append("  INNER JOIN D_RSCRelatorioSolicitacaoCliente AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId "); 
			  sql.append("  INNER JOIN D_RSCCategorias AS rscc WITH(NOLOCK) ON rsc.categoriasRsc_neoid = rscc.neoid "); 
			  sql.append("  INNER JOIN D_RRCDestino AS rrcd WITH (NOLOCK) ON rrcd.neoid = rsc.destino_neoid "); 
			  sql.append("  INNER JOIN processmodel AS pm WITH (NOLOCK)  ON pm.neoid = p.model_neoid "); 
			  sql.append("  INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rsc.clienteSapiens_neoId ");  
			  sql.append("  WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relatório de Solicitação de Cliente%') ");  
			  sql.append("  AND cli.codcli = @codcli ");  
			  sql.append("  AND p.PROCESSSTATE IN(0) AND p.saved = 1 "); 
			  sql.append("  ) ");
              sql.append("            union ");
              sql.append("            ( ");
              sql.append("            SELECT p.neoId,code as codigo,name as fluxo, CgcCpf as cnpj, nomcli as cliente, rsc.contato as contato, ");
              sql.append("                 (case when rsc.rscCategoriaDiversos_neoId is not null then 'Diversos' else ");
              sql.append("                         case when rsc.RSCCategoriaAlteracaoBoletoNF_neoId is not null then '2ª via Boleto N/F' else ");
              sql.append("                                 case when rsc.RSCCategoriaAtualizacaoCadastral_neoId is not null then 'Atualização Cadastral' else ");
              sql.append("                                         case when rsc.rscCategoriaCancelamento_neoId is not null then 'Cancelamento' else ");
              sql.append("                                                 case when rsc.rscCategoriaOrcamento_neoId is not null then 'Orçamento' else 'Aguardando Registro' end ");
              sql.append("                                         end ");
              sql.append("                                 end ");
              sql.append("                          end ");
              sql.append("                 end) as categoria ");  
			  sql.append("  ,NUS.FULLNAME as usuario, ");
			  sql.append("  er.escritorio as descricao ");  
			  sql.append("  ,er.escritorio as sigla, ");
			  sql.append("  isnull(catDiv.prazoDeAtendimento, isnull(catNF.prazoDeAtendimento, isnull(catAlt.prazoDeAtendimento,isnull(catCan.prazoTentarReversao,isnull(catOrc.prazoAgendarVisita, rsc.prazoRegistrarRSC))))) as prazo, ");
			  sql.append("  p.startDate as dataInicial,p.finishDate as dataFinal,p.processState as situacao ");   
			  sql.append("  FROM wfProcess AS p WITH (NOLOCK) ");  
			  sql.append("  INNER JOIN NEOUSER AS NUS WITH(NOLOCK) ON NUS.NEOID = P.REQUESTER_NEOID "); 
			  sql.append("  INNER JOIN D_RSCRelatorioSolicitacaoClienteNovo AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId "); 
			  sql.append("  INNER JOIN processmodel AS pm WITH (NOLOCK)  ON pm.neoid = p.model_neoid "); 
			  sql.append("  INNER JOIN X_SAPIENS_Clientes AS xcli WITH (NOLOCK)  ON xcli.neoid = rsc.clienteSapiens_neoId ");
			  sql.append("  INNER JOIN [FSOODB04\\sql02].SAPIENS.dbo.e085cli cli on xcli.codcli = cli.codcli ");
			  sql.append("  left join NEOUSER nu on rsc.responsavelExecutor_neoId = nu.neoId ");
			  sql.append("  left join dbo.D_colaboradores col on nu.neoId = col.neoId ");
			  sql.append("  left join dbo.D_escritorio er on col.escritorioRegional_neoId = er.neoId ");
			  sql.append("  left join dbo.D_RSCCategoriaDiversos catDiv on catDiv.neoId = rsc.rscCategoriaDiversos_neoId ");
			  sql.append("  left join dbo.D_RSCCategoriaAlteracaoBoletoNF catNF on catNF.neoId = rsc.RSCCategoriaAlteracaoBoletoNF_neoId ");
			  sql.append("  left join dbo.D_RSCCategoriaAtualizacaoCadastral catAlt on catAlt.neoId = rsc.RSCCategoriaAtualizacaoCadastral_neoId ");
			  sql.append("  left join dbo.D_RSCCategoriaCancelamento catCan on catCan.neoId = rsc.rscCategoriaCancelamento_neoId ");
			  sql.append("  left join dbo.D_RSCCategoriaOrcamento catOrc on catOrc.neoId = rsc.rscCategoriaOrcamento_neoId ");
			  sql.append("  WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relatório de Solicitação de Cliente Novo%') ");  
			  sql.append("  AND cli.codcli = @codcli ");
			  sql.append("  AND p.PROCESSSTATE IN(0) AND p.saved = 1 "); 
			  sql.append("  ) ");
			  sql.append("  ORDER BY p.STARTDATE "); 

			//BUSCA-SE REGISTROS NA BASE
			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, codCli);
			rs = pstm.executeQuery();
			out.println("<fieldset>");
			out.println("<table class='gridbox' cellpadding='0' cellspacing='0' style='empty-cells: show' >");

			out.println("<thead>");
			out.println("<tr>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Código") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Processo") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Dados do Processo") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Solicitante") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data de Início") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data de Conclusão") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Situação") + "</th>");
			out.println("</tr>");
			out.println("</thead>");
			out.println("<tbody>");
			GregorianCalendar prazo = new GregorianCalendar();
			while (rs.next())
			{
				out.println("<tr>");
				out.println("<td>" + NeoUtils.encodeHTMLValue(rs.getString("codigo")) + " <a title='Dados da RSC' style='cursor:pointer' onclick='javascript:showTarefa(\"" + rs.getString("neoId") + "\")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>"+ "</td>");
				out.println("<td>" + NeoUtils.encodeHTMLValue(rs.getString("fluxo")) + "</td>");
				String prazoStr = "";
				if (rs.getDate("prazo") != null)
				{
					prazo.setTime(rs.getDate("prazo"));
					prazoStr = NeoUtils.safeDateFormat(prazo, "dd/MM/yyyy");
				}
				out.println("<td>" + NeoUtils.encodeHTMLValue(rs.getString("cnpj") + " - " + rs.getString("cliente") + " - " + rs.getString("contato") + " - " + rs.getString("categoria") + " - " + rs.getString("descricao") + " - " + rs.getString("sigla") + " - " + prazoStr) + "</td>");
				out.println("<td>" + NeoUtils.encodeHTMLValue(rs.getString("usuario")) + "</td>");
				GregorianCalendar dataLigacao = new GregorianCalendar();
				String dataStr = "";
				if (rs.getDate("dataInicial") != null)
				{
					dataLigacao.setTime(rs.getDate("dataInicial"));
					dataStr = (NeoUtils.safeDateFormat(dataLigacao, "dd/MM/yyyy"));
				}
				out.println("<td>" + NeoUtils.encodeHTMLValue(dataStr) + "</td>");
				GregorianCalendar dataFinal = new GregorianCalendar();
				String dataFinalStr = "";
				if (rs.getDate("dataFinal") != null)
				{
					dataFinal.setTime(rs.getDate("dataFinal"));
					dataFinalStr = (NeoUtils.safeDateFormat(dataFinal, "dd/MM/yyyy"));
				}
				out.println("<td>" + NeoUtils.encodeHTMLValue(dataFinalStr) + "</td>");
				
				Integer situacao = rs.getInt("situacao");
				String situacaoStr = "";
				switch (situacao)
				{
					case 0:
						situacaoStr = "Em Execução";
						break;
					case 1:
						situacaoStr = "Finalizado";
						break;
					case 2:
						situacaoStr = "Cancelado";
						break;
					case 3:
						situacaoStr = "StandBy";
						break;
					case 4:
						situacaoStr = "Removido";
						break;
				}
				
				out.println("<td>" + NeoUtils.encodeHTMLValue(situacaoStr) + "</td>");
			
				out.println("</tr>");
			}

			out.println("</tbody>");
			out.println("</table>");
			out.println("</fieldset>");
			out.println("</br>");
			out.println("</br>");

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
				getRRC(req, resp, out);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
	}

	private void getRRC(HttpServletRequest req, HttpServletResponse resp, PrintWriter out)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String codCli = req.getParameter("codigoCliente");
		try
		{
			conn = PersistEngine.getConnection("FUSIONPROD");

			StringBuilder sql = new StringBuilder();
			
			sql.append("  SELECT p.neoId,code as codigo,name as fluxo,cpfCliente as cnpj,cliente as cliente,pessoaContato as contato ");
			sql.append("   ,NUS.FULLNAME as usuario,rrcd.descricao as descricao ");
			sql.append("   ,rrcd.siglaRegional as sigla,rrc.prazoAcao as prazo,p.startDate as dataInicial,p.finishDate as dataFinal,p.processState as situacao ");
			sql.append("  FROM wfProcess AS p WITH (NOLOCK) ");
			sql.append("  INNER JOIN NEOUSER AS NUS WITH(NOLOCK) ON NUS.NEOID = P.REQUESTER_NEOID  ");
			sql.append("  INNER JOIN D_RRCRelatorioReclamacaoCliente AS rrc WITH (NOLOCK) ON rrc.neoid = p.entity_neoId "); 
			sql.append("  INNER JOIN D_RRCDestino AS rrcd WITH (NOLOCK) ON rrcd.neoid = rrc.destino_neoid ");
			sql.append("  INNER JOIN processmodel AS pm WITH (NOLOCK)  ON pm.neoid = p.model_neoid "); 
			sql.append("  INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rrc.clienteSapiens_neoId "); 
			sql.append("  WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%Q005 - RRC - Relatório de Reclamação de Cliente%') "); 
			sql.append("  AND cli.codcli = ?  ");
			sql.append("  AND p.PROCESSSTATE  IN(0) AND p.saved = 1 ORDER BY p.STARTDATE  ");

			//BUSCA-SE REGISTROS NA BASE
			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, codCli);
			rs = pstm.executeQuery();
			out.println("<fieldset>");
			out.println("<table class='gridbox' cellpadding='0' cellspacing='0' style='empty-cells: show' >");

			out.println("<thead>");
			out.println("<tr>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Código") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Processo") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Dados do Processo") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Solicitante") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data de Início") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Data de Conclusão") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Situação") + "</th>");
			out.println("</tr>");
			out.println("</thead>");
			out.println("<tbody>");
			GregorianCalendar prazo = new GregorianCalendar();
			while (rs.next())
			{
				out.println("<tr>");
				out.println("<td>" + NeoUtils.encodeHTMLValue(rs.getString("codigo"))+"<a title='Dados da RRC' style='cursor:pointer' onclick='javascript:showTarefa(\"" + rs.getLong("neoId") + "\")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>"+ "</td>");
				out.println("<td>" + NeoUtils.encodeHTMLValue(rs.getString("fluxo")) + "</td>");
				String prazoStr = "";
				if (rs.getDate("prazo") != null)
				{
					prazo.setTime(rs.getDate("prazo"));
					prazoStr = NeoUtils.safeDateFormat(prazo, "dd/MM/yyyy");
				}
				out.println("<td>" + NeoUtils.encodeHTMLValue(rs.getString("cnpj") + " - " + rs.getString("cliente") + " - " + rs.getString("contato") + " - " + rs.getString("descricao") + " - " + rs.getString("sigla") + " - " + prazoStr) + "</td>");
				out.println("<td>" + NeoUtils.encodeHTMLValue(rs.getString("usuario")) + "</td>");
				GregorianCalendar dataLigacao = new GregorianCalendar();
				String dataStr = "";
				if (rs.getDate("dataInicial") != null)
				{
					dataLigacao.setTime(rs.getDate("dataInicial"));
					dataStr = (NeoUtils.safeDateFormat(dataLigacao, "dd/MM/yyyy"));
				}
				out.println("<td>" + NeoUtils.encodeHTMLValue(dataStr) + "</td>");
				GregorianCalendar dataFinal = new GregorianCalendar();
				String dataFinalStr = "";
				if (rs.getDate("dataFinal") != null)
				{
					dataFinal.setTime(rs.getDate("dataFinal"));
					dataFinalStr = (NeoUtils.safeDateFormat(dataFinal, "dd/MM/yyyy"));
				}
				out.println("<td>" + NeoUtils.encodeHTMLValue(dataFinalStr) + "</td>");
				
				Integer situacao = rs.getInt("situacao");
				String situacaoStr = "";
				switch (situacao)
				{
					case 0:
						situacaoStr = "Em Execução";
						break;
					case 1:
						situacaoStr = "Finalizado";
						break;
					case 2:
						situacaoStr = "Cancelado";
						break;
					case 3:
						situacaoStr = "StandBy";
						break;
					case 4:
						situacaoStr = "Removido";
						break;
				}
				
				out.println("<td>" + NeoUtils.encodeHTMLValue(situacaoStr) + "</td>");
			
				out.println("</tr>");
			}

			out.println("</tbody>");
			out.println("</table>");
			out.println("</fieldset>");

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
	}

}