package com.neomind.fusion.custom.casvig.mobile.xml.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.casvig.mobile.CasvigMobileServlet;
import com.neomind.fusion.custom.casvig.mobile.MobileUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public class Inspection implements MobileXmlInterface
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(Inspection.class);
	
	private NeoObject entity;

	private InstantiableEntityInfo entityInfo;

	private List<NeoObject> listReplyOK;

	public NeoObject createNewInstance(InstantiableEntityInfo entityInfo)
	{
		this.entity = entityInfo.createNewInstance();
		this.entityInfo = entityInfo;
		this.listReplyOK = new ArrayList<NeoObject>();
		return this.entity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.neomind.fusion.custom.casvig.mobile.xml.entity.MobileXmlInterface#setInstance(Document
	 *      document)
	 */
	public NeoObject setInstance(Document document)
	{
		if (entity == null)
			return null;

		boolean notConform = false;

		EntityWrapper objWrapper = new EntityWrapper(entity);
		Element elem = document.getDocumentElement(); // Resultados
		if (elem != null && elem.hasChildNodes())
		{
			NodeList nodeList = elem.getChildNodes();
			for (int k = 0; k < nodeList.getLength(); k++)
			{
				Element elemInsp = (Element) nodeList.item(k); // Resultado

				QLGroupFilter groupFilter = new QLGroupFilter("and");

				// valor tipo_inspecao

				// valor cod_wfprocess

				// valor mobile_neoId
				objWrapper.setValue("mobileNeoId", new BigDecimal(elemInsp.getAttribute("mobile_neoId"))); 
				groupFilter.addFilter(MobileUtils.createFilter("mobileNeoId", new BigDecimal(elemInsp.getAttribute("mobile_neoId"))));

				 // valor cod_pesquisa
				Long codePesq = Long.valueOf(elemInsp.getAttribute("cod_pesquisa"));
				NeoObject pesquisa = PersistEngine.getNeoObject(codePesq);

				// valor cod_postoTrabalho
				objWrapper.setValue("codposto", elemInsp.getAttribute("cod_postoTrabalho")); 
				groupFilter.addFilter(MobileUtils.createFilter("codposto", elemInsp.getAttribute("cod_postoTrabalho")));

				 // valor nome_empresa
				objWrapper.setValue("nomeempresa", elemInsp.getAttribute("nome_empresa"));
				groupFilter.addFilter(MobileUtils.createFilter("nomeempresa", elemInsp.getAttribute("nome_empresa")));

				 // valor nome_cliente
				objWrapper.setValue("nomecliente", elemInsp.getAttribute("nome_cliente"));
				groupFilter.addFilter(MobileUtils.createFilter("nomecliente", elemInsp.getAttribute("nome_cliente")));

				 // valor nome_lotacao
				objWrapper.setValue("nomelotacao", elemInsp.getAttribute("nome_lotacao"));
				groupFilter.addFilter(MobileUtils.createFilter("nomelotacao", elemInsp.getAttribute("nome_lotacao")));

				 // valor nome_postoTrabalho
				String postoTrabalho = elemInsp.getAttribute("nome_postoTrabalho");
				objWrapper.setValue("nomeposto", elemInsp.getAttribute("nome_postoTrabalho"));
				groupFilter.addFilter(MobileUtils.createFilter("nomeposto", elemInsp.getAttribute("nome_postoTrabalho")));

				// valor gestor_cliente
				objWrapper.setValue("gestorcliente", elemInsp.getAttribute("gestor_cliente")); 
				groupFilter.addFilter(MobileUtils.createFilter("gestorcliente", elemInsp.getAttribute("gestor_cliente")));

				// valor nome_colaborador
				objWrapper.setValue("colaboradorentrevistado", elemInsp.getAttribute("nome_colaborador")); 
				groupFilter.addFilter(MobileUtils.createFilter("colaboradorentrevistado", elemInsp.getAttribute("nome_colaborador")));

				// valor funcao_colaborador
				objWrapper.setValue("funccolaborador", elemInsp.getAttribute("funcao_colaborador")); 
				groupFilter.addFilter(MobileUtils.createFilter("funccolaborador", elemInsp.getAttribute("funcao_colaborador")));

				// valor nome_inspetor
				String inspetor = new String(elemInsp.getAttribute("nome_inspetor"));
				objWrapper.setValue("nomeinspetor", elemInsp.getAttribute("nome_inspetor")); 
				groupFilter.addFilter(MobileUtils.createFilter("nomeinspetor", elemInsp.getAttribute("nome_inspetor")));

				// exemplo: aaaa mm dd
				StringTokenizer dataString = new StringTokenizer(elemInsp.getAttribute("data_inspesao"));
				String ano = dataString.nextToken();
				String mes = dataString.nextToken();
				String dia = dataString.nextToken();
				GregorianCalendar calendar = new GregorianCalendar(Integer.parseInt(ano), Integer.parseInt(mes), Integer.parseInt(dia));
				// valor data_inspesao
				objWrapper.setValue("datainspecao", calendar); 
				groupFilter.addFilter(MobileUtils.createFilter("datainspecao", calendar));

				// valor descrisao_pesquisa
				objWrapper.setValue("descpesquisa", elemInsp.getAttribute("descrisao_pesquisa")); 
				groupFilter.addFilter(MobileUtils.createFilter("descpesquisa", elemInsp.getAttribute("descrisao_pesquisa")));

				// valor observacao
				objWrapper.setValue("Observacao", elemInsp.getAttribute("observacao")); 
				groupFilter.addFilter(MobileUtils.createFilter("Observacao", elemInsp.getAttribute("observacao")));

				// valor numTotalResp

				InstantiableEntityInfo infoEform = (InstantiableEntityInfo) EntityRegister.getCacheInstance().getByType("Resposta");

				List<NeoObject> list = Collections.synchronizedList(new LinkedList<NeoObject>());
				List<NeoObject> listNoConform = Collections.synchronizedList(new LinkedList<NeoObject>());
				List<NeoObject> listObservation = Collections.synchronizedList(new LinkedList<NeoObject>());

				notConform = false;
				NodeList nodeListReply = elemInsp.getChildNodes();
				for (int r = 0; r < nodeListReply.getLength(); r++)
				{
					Element elemReply = (Element) nodeListReply.item(r); // Resposta

					NeoObject findNewObj = infoEform.createNewInstance();
					EntityWrapper findObjWrapper = new EntityWrapper(findNewObj);

					QLGroupFilter groupF = new QLGroupFilter("and");

					// valor desc_perg
					findObjWrapper.setValue("descperg", elemReply.getAttribute("desc_perg")); 
					groupF.addFilter(MobileUtils.createFilter("descperg", elemReply.getAttribute("desc_perg")));

					// valor resp_perg
					findObjWrapper.setValue("respperg", elemReply.getAttribute("resp_perg")); 
					groupF.addFilter(MobileUtils.createFilter("respperg", elemReply.getAttribute("resp_perg")));

					// valor logical
					Boolean logical = MobileUtils.isConform(elemReply.getAttribute("logical"));
					findObjWrapper.setValue("booleanValor", logical);
					groupF.addFilter(MobileUtils.createFilter("booleanValor", logical));

					// valor code_perg
					findObjWrapper.setValue("codeperg", new Long(elemReply.getAttribute("code_perg"))); 
					groupF.addFilter(MobileUtils.createFilter("codeperg", new Long(elemReply.getAttribute("code_perg"))));

					if (logical != null && !logical)
					{ // se detectou não conformidade
						
						PersistEngine.persist(findNewObj);
						listNoConform.add(findObjWrapper.getObject());
						if (!notConform)
							notConform = true;
					}
					else
					{
						PersistEngine.persist(findNewObj);
						list.add(findObjWrapper.getObject());
						listReplyOK.add(findObjWrapper.getObject());
					}
				}

				// se foi detectado alguma inconsistência na pesquisa
				if (notConform)
				{
					boolean notDuplicate = true;
					for (int i = 0; i < listNoConform.size(); i++)
					{
						List<NeoObject> auxList = Collections.synchronizedList(new LinkedList<NeoObject>());
						auxList.add(listNoConform.get(i));

						for (NeoObject obs : listObservation)
						{
							auxList.add(obs);
						}

						// pega code_perg da 1 pergunta da lista OK
						Long code_perg = (Long) new EntityWrapper(listNoConform.get(i)).getValue("codeperg");
						long neoIdPergunta = NeoUtils.safeLong(String.valueOf(code_perg));
						NeoObject pergunta = PersistEngine.getNeoObject(neoIdPergunta);
						// pega o departamento responsável
						NeoObject tipoInspecao = (NeoObject) new EntityWrapper(pergunta).getValue("departamento");

						NeoObject newObjInsp = entityInfo.createNewInstance();
						EntityWrapper objWrapperInsp = new EntityWrapper(newObjInsp);
						QLGroupFilter groupFilterInsp = new QLGroupFilter("and");

						objWrapperInsp.setValue("mobileNeoId", objWrapper.getValue("mobileNeoId"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("mobileNeoId", objWrapper.getValue("mobileNeoId")));

						objWrapperInsp.setValue("codposto", objWrapper.getValue("codposto"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("codposto", objWrapper.getValue("codposto")));

						objWrapperInsp.setValue("gestorcliente", objWrapper.getValue("gestorcliente"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("gestorcliente", objWrapper.getValue("gestorcliente")));

						objWrapperInsp.setValue("nomeempresa", objWrapper.getValue("nomeempresa"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomeempresa", objWrapper.getValue("nomeempresa")));

						objWrapperInsp.setValue("nomecliente", objWrapper.getValue("nomecliente"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomecliente", objWrapper.getValue("nomecliente")));

						objWrapperInsp.setValue("nomelotacao", objWrapper.getValue("nomelotacao"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomelotacao", objWrapper.getValue("nomelotacao")));

						objWrapperInsp.setValue("nomeposto", objWrapper.getValue("nomeposto"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomeposto", objWrapper.getValue("nomeposto")));

						objWrapperInsp.setValue("colaboradorentrevistado", objWrapper.getValue("colaboradorentrevistado"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("colaboradorentrevistado", objWrapper.getValue("colaboradorentrevistado")));

						objWrapperInsp.setValue("funccolaborador", objWrapper.getValue("funccolaborador"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("funccolaborador", objWrapper.getValue("funccolaborador")));

						objWrapperInsp.setValue("nomeinspetor", objWrapper.getValue("nomeinspetor"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomeinspetor", objWrapper.getValue("nomeinspetor")));

						objWrapperInsp.setValue("datainspecao", objWrapper.getValue("datainspecao"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("datainspecao", objWrapper.getValue("datainspecao")));

						objWrapperInsp.setValue("descpesquisa", objWrapper.getValue("descpesquisa"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("descpesquisa", objWrapper.getValue("descpesquisa")));

						objWrapperInsp.setValue("Observacao", objWrapper.getValue("Observacao"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("Observacao", objWrapper.getValue("Observacao")));

						objWrapperInsp.setValue("neoidResposta", auxList);

						// adiciona lista de respostas OK
						// adiciona em 15/12/2008 - by joppi
						if(this.listReplyOK != null && !this.listReplyOK.isEmpty())
						{
							objWrapperInsp.setValue("RespostasOK", MobileUtils.cloneList(this.listReplyOK));
						}
						
						NeoObject newObjAux = (NeoObject) PersistEngine.getObject(entityInfo.getEntityClass(), groupFilterInsp);
						if (newObjAux == null || (notDuplicate && i > 0))
						{
							PersistEngine.persist(newObjInsp);
							CasvigMobileServlet.log.info("Persist new inspection -> inconsistency" + " - " + tipoInspecao.getAsString() + " - "
									+ postoTrabalho);
							// inicia o workflow
							startWorkflow(newObjInsp, inspetor, pesquisa, tipoInspecao, notConform);
						}
						else
						{
							CasvigMobileServlet.log.error("Duplicate inspection -> inconsistency" + " - " + tipoInspecao.getAsString() + " - "
									+ postoTrabalho);
							if (i == 0) // se o primeiro já constata ser inspeção duplicada
								notDuplicate = false;
						}
					}
				}
				else
				{

					// Perguntas OK

					boolean first = true;
					boolean notDuplicate = true;
					while (!list.isEmpty())
					{
						List<NeoObject> auxList = Collections.synchronizedList(new LinkedList<NeoObject>());

						// pega code_perg da 1 pergunta da lista OK
						Long code_perg = (Long) new EntityWrapper(list.get(0)).getValue("codeperg");
						long neoIdPergunta = NeoUtils.safeLong(String.valueOf(code_perg));
						NeoObject pergunta = PersistEngine.getNeoObject(neoIdPergunta);
						// pega o departamento responsável
						NeoObject tipoInspecao = (NeoObject) new EntityWrapper(pergunta).getValue("departamento");
						for (NeoObject neoObj : list)
						{
							Long aux_code_perg = (Long) new EntityWrapper(neoObj).getValue("codeperg");
							long aux_neoIdPergunta = NeoUtils.safeLong(String.valueOf(aux_code_perg));
							NeoObject auxPergunta = PersistEngine.getNeoObject(aux_neoIdPergunta);
							NeoObject auxTipoInspecao = (NeoObject) new EntityWrapper(auxPergunta).getValue("departamento");
							// verifica todas as perguntas que possuem o
							// mesmo departamento
							if (tipoInspecao.getNeoId() == auxTipoInspecao.getNeoId())
							{
								auxList.add(neoObj);
							}
						}

						// remove todas as perguntas da list ... que foram
						// adicionadas na auxList
						for (NeoObject oldNeoObj : auxList)
						{
							list.remove(oldNeoObj);
						}

						for (NeoObject obs : listObservation)
						{
							auxList.add(obs);
						}

						NeoObject newObjInsp = entityInfo.createNewInstance();
						EntityWrapper objWrapperInsp = new EntityWrapper(newObjInsp);
						QLGroupFilter groupFilterInsp = new QLGroupFilter("and");

						objWrapperInsp.setValue("mobileNeoId", objWrapper.getValue("mobileNeoId"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("mobileNeoId", objWrapper.getValue("mobileNeoId")));

						objWrapperInsp.setValue("codposto", objWrapper.getValue("codposto"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("codposto", objWrapper.getValue("codposto")));

						objWrapperInsp.setValue("gestorcliente", objWrapper.getValue("gestorcliente"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("gestorcliente", objWrapper.getValue("gestorcliente")));

						objWrapperInsp.setValue("nomeempresa", objWrapper.getValue("nomeempresa"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomeempresa", objWrapper.getValue("nomeempresa")));

						objWrapperInsp.setValue("nomecliente", objWrapper.getValue("nomecliente"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomecliente", objWrapper.getValue("nomecliente")));

						objWrapperInsp.setValue("nomelotacao", objWrapper.getValue("nomelotacao"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomelotacao", objWrapper.getValue("nomelotacao")));

						objWrapperInsp.setValue("nomeposto", objWrapper.getValue("nomeposto"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomeposto", objWrapper.getValue("nomeposto")));

						objWrapperInsp.setValue("colaboradorentrevistado", objWrapper.getValue("colaboradorentrevistado"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("colaboradorentrevistado", objWrapper.getValue("colaboradorentrevistado")));

						objWrapperInsp.setValue("funccolaborador", objWrapper.getValue("funccolaborador"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("funccolaborador", objWrapper.getValue("funccolaborador")));

						objWrapperInsp.setValue("nomeinspetor", objWrapper.getValue("nomeinspetor"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("nomeinspetor", objWrapper.getValue("nomeinspetor")));

						objWrapperInsp.setValue("datainspecao", objWrapper.getValue("datainspecao"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("datainspecao", objWrapper.getValue("datainspecao")));

						objWrapperInsp.setValue("descpesquisa", objWrapper.getValue("descpesquisa"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("descpesquisa", objWrapper.getValue("descpesquisa")));

						objWrapperInsp.setValue("Observacao", objWrapper.getValue("Observacao"));
						groupFilterInsp.addFilter(MobileUtils.createFilter("Observacao", objWrapper.getValue("Observacao")));

						objWrapperInsp.setValue("neoidResposta", auxList);

						NeoObject newObjAux = (NeoObject) PersistEngine.getObject(entityInfo.getEntityClass(), groupFilterInsp);
						if (newObjAux == null || (notDuplicate && !first))
						{
							PersistEngine.persist(newObjInsp);
							CasvigMobileServlet.log.info("Persist new inspection - OK" + " - " + tipoInspecao.getAsString() + " - " + postoTrabalho);
							// inicia o workflow
							startWorkflow(newObjInsp, inspetor, pesquisa, tipoInspecao, notConform);
							first = false;
						}
						else
						{
							CasvigMobileServlet.log.error("Duplicate inspection - OK" + " - " + tipoInspecao.getAsString() + " - " + postoTrabalho);
							if (first) // se o primeiro já constata ser
										// inspeção duplicada
								notDuplicate = false;
						}
					}
				}
			}
		}
		return entity;
	}

	public void startWorkflow(NeoObject newObj, String inspetor, NeoObject pesquisa, NeoObject tipoInspecao, boolean notConform)
	{
		// Instancia o E-Form IM_Inspetoria
		InstantiableEntityInfo entityInfo = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByType("IMInspetoria");
		NeoObject inspetoriaObj = entityInfo.createNewInstance();
		EntityWrapper inspetoriaWrapper = new EntityWrapper(inspetoriaObj);
		inspetoriaWrapper.setValue("relatorioInspecao", newObj);
		inspetoriaWrapper.setValue("tipoInspecao", tipoInspecao);
		inspetoriaWrapper.setValue("gerouInconsistencia", new Boolean(notConform));
		inspetoriaWrapper.setValue("PesquisaMobile", pesquisa);

		PersistEngine.persist(inspetoriaObj);

		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("Name", "Q003 - Inspetoria Mobile"));
		NeoUser neoUser = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("fullName", inspetor));
		
		// igor.giumelli, 15/01/2009: try/catch e PortalUtil.setCurrentUser(null)
		try
		{
			PortalUtil.setCurrentUser(neoUser);
			
			/**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
			 * @date 12/03/2015
			 */
			
			//WorkflowService.startProcess(processModel, inspetoriaObj, false, neoUser);
			String tarefa = OrsegupsWorkflowHelper.iniciaProcesso(processModel, inspetoriaObj, false, neoUser, false, null);
			System.out.println("Tarefa de inspeção " + tarefa + " aberta com sucesso");
			
			/**
			 * FIM ALTERAÇÕES - NEOMIND
			 */
			
		}catch (Exception e) 
		{
			log.error("Erro ao iniciar o wkf Inspetoria Mobile: ",e);
		}
		finally
		{
			PortalUtil.setCurrentUser(null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.neomind.fusion.custom.casvig.mobile.xml.entity.MobileXmlInterface#startProcess()
	 */
	public boolean startProcess()
	{
		// FIXME ver uma maneira de usar este método em vez do startWorkflow
		try
		{
			return true;
		}
		catch (Exception e)
		{
			return false;
		}

	}

	/**
	 * Inspeção não dá sequencia a uma atividade de sistema.
	 * 
	 * @return false.
	 */
	public boolean finishActivity()
	{
		return false;
	}
}
