package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class EnviaEmailOrsegupsMedicaoUtilsClienteSEA implements CustomJobAdapter

{

	@Override
	public void execute(CustomJobContext ctx)
	{
		System.out.println("[MED] - iniciando job Envia e-mail Para Medicao de contratos diario");
		processaJob(ctx);
	}

	public static void processaJob(CustomJobContext ctx)
	{
		GregorianCalendar datRef = new GregorianCalendar();
		datRef.add(GregorianCalendar.DATE, -1);
		try
		{
			enviaEmail(datRef);
		}
		catch (EmailException e)
		{
			e.printStackTrace();
		}
	}

	public static String enviaEmail(GregorianCalendar datRef) throws EmailException
	{
		HtmlEmail noUserEmail = new HtmlEmail();
		noUserEmail.setCharset("ISO-8859-1");
		String dataFormat = retornaDataFormatoSapiensEVetorh(datRef);
		dataFormat = dataFormat.substring(8, 10) + "/" + dataFormat.substring(5, 7) + "/" + dataFormat.substring(0, 4);
		File arqRelatorio = getRelatorioMedicao(dataFormat);
		NeoStorage neoStorage = NeoStorage.getDefault();
		NeoFile neoFile = neoStorage.copy(arqRelatorio);
		neoFile.setName("Relatorio_Medicao.pdf");
		List<String> finale = new ArrayList<String>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		finale.add(((Long) neoFile.getNeoId()).toString());
		paramMap.put("attachList", finale);
		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("emailMedicaoDeContratos"));
		String destinatario = "emailautomatico@orsegups.com.br;passinha.sc@hotmail.com";
		String separador = ";";

		if (objs.size() > 0)
		{
			for (NeoObject neoObject : objs)
			{
				EntityWrapper medWrapper = new EntityWrapper(neoObject);
				destinatario += separador + medWrapper.findValue("email");
			}
		}

		FusionRuntime.getInstance().getMailEngine().sendEMail(destinatario, "/MedicaoDeContratos/emailRelatorioMedicaoDeContratos.jsp", paramMap);

		return null;
	}

	public static File getRelatorioMedicao(String diaRef)
	{
		File fileProposta = null;
		Connection fusion = PersistEngine.getConnection("");

		try
		{
			String sql = " select contrato.codCli,contrato.codEmp,contrato.codReg,contrato.diaRef,contrato.nomEmp,contrato.nomReg,contrato.nomRes,contrato.numCtr,contrato.nomCli, posto.carPos,posto.codCcu,posto.nomloc,posto.numPos,posto.numVgs,"
					+ " posto.escpos,posto.escpos, colaborador.dataAdm,colaborador.horEnt,colaborador.horSai,colaborador.nomCar,colaborador.nomFun,colaborador.nomSit,colaborador.numCad,  "
					+ " desesc, colaborador.codjmaEntrada, colaborador.codjmaSaida, consideracoes.obsCon,consideracoes.datadm,consideracoes.horfim,consideracoes.horini,consideracoes.numcad as numcadcob,"
					+ " consideracoes.sitafa,consideracoes.titred ,consideracoes.nomFunCob, consideracoes.desEscCob, consideracoes.codjmaEntradaCob, consideracoes.codjmaSaidaCob, consideracoes.obsafa,colaborador.totalHorastrabalhadas,colaborador.totalMinutosTrabalhados"
					+ " from d_medContratos contrato left join D_medContratos_medLisPos contrato_posto on contrato_posto.D_medContratos_neoId = contrato.neoId left join d_medPostos posto on posto.neoId = contrato_posto.medLisPos_neoId left join D_medPostos_lisCol posto_colaborador on posto_colaborador.D_medPostos_neoId = posto.neoId left join d_medColaboradoresDia colaborador on colaborador.neoId = posto_colaborador.lisCol_neoId left join D_medColaboradoresDia_medObs colaborador_consideracoes on colaborador_consideracoes.D_medColaboradoresDia_neoId =  colaborador.neoId left join d_medobs consideracoes on consideracoes.neoId = colaborador_consideracoes.medObs_neoId";

			if (!diaRef.equals("") || (diaRef != null))
			{
				sql = sql + " WHERE diaRef ='" + diaRef + "' and ((contrato.codCli = 81858 and posto.nomLoc like '%SEA/SC%') or (contrato.numCtr in(60606,60607,60661) and contrato.codemp = 6)) ORDER BY codCli, nomCli, numCtr,nomloc,nomFun";
			}
			InputStream is = null;
			String path = "";
			fileProposta = null;
			try
			{
				// ...files/relatorios
				path = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "relMedicaoContratosDiario.jasper";
				is = new BufferedInputStream(new FileInputStream(path));
				HashMap<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("sql", sql);

				if (parametros != null)
				{

					File file = File.createTempFile("Medicao", ".pdf");
					file.deleteOnExit();

					JasperPrint impressao = JasperFillManager.fillReport(is, parametros, fusion);
					if (impressao != null && file != null)
					{
						JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
						return file;
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				fusion.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
		return fileProposta;
	}

	public static String retornaDataFormatoSapiensEVetorh(GregorianCalendar data)
	{
		String novaData = "";

		if (data == null)
		{
			/*
			 * 31/12/1900 o sapiens\Vetorh entende essa data como vazia
			 */
			data = new GregorianCalendar();
			data.set(data.YEAR, 1900);
			data.set(data.MONTH, 11);
			data.set(data.DAY_OF_MONTH, 31);
			data.set(Calendar.HOUR_OF_DAY, 00);
			data.set(Calendar.MINUTE, 00);
			data.set(Calendar.SECOND, 00);
		}

		if (data != null)
		{
			novaData = NeoUtils.safeDateFormat(data, "yyyy-MM-dd");
			novaData = novaData + " 00:00:00";
		}

		return novaData;
	}

}
