package com.neomind.fusion.custom.orsegups.fap.dto;

import java.util.List;

/**
 * Dados do técnico e seu respectivo fornecedor.
 * Associação feita via e-form Técnico x Fornecedor (FAPAssoc)
 * @author diogo.silva
 *
 */
public class FornecedorDTO {
    String nomeFornecedor = null;
    String codigoFornecedor = null;
    Integer dataCorte = null;
    Boolean agruparFap = null;
    String email = null;
    Long cgcCpf = null;
    String codigoTarefa = null;
    
    // Para aplicação técnico e tático
    Long codigoTecnico = null;
    String nomeTecnico = null;
    
    // Para aplicação tático
    Long codigoRota = null;
    String nomeRota = null;
    
    String agencia = null;
    String conta = null;
    String banco = null;
    
    
    List<String> listaErros;
    
    
    
    public String getCodigoTarefa() {
        return codigoTarefa;
    }
    public void setCodigoTarefa(String codigoTarefa) {
        this.codigoTarefa = codigoTarefa;
    }
    public String getNomeTecnico() {
        return nomeTecnico;
    }
    public void setNomeTecnico(String nomeTecnico) {
        this.nomeTecnico = nomeTecnico;
    }
    public String getNomeFornecedor() {
        return nomeFornecedor;
    }
    public void setNomeFornecedor(String nomeFornecedor) {
        this.nomeFornecedor = nomeFornecedor;
    }
    public String getCodigoFornecedor() {
        return codigoFornecedor;
    }
    public void setCodigoFornecedor(String codigoFornecedor) {
        this.codigoFornecedor = codigoFornecedor;
    }
    public Integer getDataCorte() {
        return dataCorte;
    }
    public void setDataCorte(Integer dataCorte) {
        this.dataCorte = dataCorte;
    }
    public Long getCodigoTecnico() {
        return codigoTecnico;
    }
    public void setCodigoTecnico(Long codigoTecnico) {
        this.codigoTecnico = codigoTecnico;
    }
    public String getAgencia() {
        return agencia;
    }
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }
    public String getConta() {
        return conta;
    }
    public void setConta(String conta) {
        this.conta = conta;
    }
    public String getBanco() {
        return banco;
    }
    public void setBanco(String banco) {
        this.banco = banco;
    }
    public List<String> getListaErros() {
        return listaErros;
    }
    public void setListaErros(List<String> listaErros) {
        this.listaErros = listaErros;
    }
    public Long getCodigoRota() {
        return codigoRota;
    }
    public void setCodigoRota(Long codigoRota) {
        this.codigoRota = codigoRota;
    }
    public String getNomeRota() {
        return nomeRota;
    }
    public void setNomeRota(String nomeRota) {
        this.nomeRota = nomeRota;
    }
    public Boolean getAgruparFap() {
        return agruparFap;
    }
    public void setAgruparFap(Boolean agruparFap) {
        this.agruparFap = agruparFap;
    }
    
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public Long getCgcCpf() {
        return cgcCpf;
    }
    public void setCgcCpf(Long cgcCpf) {
        this.cgcCpf = cgcCpf;
    }
    
    
    
    
    
}
