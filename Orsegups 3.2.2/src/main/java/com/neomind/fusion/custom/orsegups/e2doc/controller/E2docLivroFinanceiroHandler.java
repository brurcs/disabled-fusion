package com.neomind.fusion.custom.orsegups.e2doc.controller;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docLivroFinanceiroEngine;
import com.neomind.fusion.custom.orsegups.e2doc.xml.livroFinanceiro.Pastas;

@Path(value = "documentosLivroFinanceiro")
public class E2docLivroFinanceiroHandler {
    
	@POST
	@Path("pesquisaLivroFinanceiro")
	@Produces("application/json")
	public Pastas pesquisaFinanceiro(
		@MatrixParam("tiposDocumentos") String tiposDocumentos,
		@MatrixParam("tiposLivros") String tiposLivros,
		@MatrixParam("empresa") String empresa,
		@MatrixParam("numero") String numero,
		@MatrixParam("ano") String ano) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (tiposDocumentos != null && !tiposDocumentos.trim().isEmpty()){
		atributosPesquisa.put("TIPO DE DOCUMENTO", tiposDocumentos.trim());
	    }
	    
	    if (tiposLivros != null && !tiposLivros.trim().isEmpty()){
		atributosPesquisa.put("TIPO DE LIVRO", tiposLivros.trim());
	    }
	    
	    if(empresa != null && !empresa.trim().isEmpty()){
		atributosPesquisa.put("EMPRESA", "%"+empresa.trim()+"%");
	    }	    

	    if(numero != null && !numero.trim().isEmpty()){
		atributosPesquisa.put("NÚMERO", "%"+numero.trim()+"%");
	    }
	    
	    if(ano != null && !ano.trim().isEmpty()){
		atributosPesquisa.put("ANO", "%"+ano.trim()+"%");
	    }
	    	    
	    E2docLivroFinanceiroEngine e2docEngine = new E2docLivroFinanceiroEngine();
	    
	    Pastas pastas = e2docEngine.pesquisaFinanceiro(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("getTiposDocumentos")
	@Produces("application/json")
	public List<String> getTiposDocumentos() {
	    
	    E2docLivroFinanceiroEngine e2docEngine = new E2docLivroFinanceiroEngine();
	    
	    return e2docEngine.getTiposDocumentos();
	    

	}
	
	@POST
	@Path("getTiposLivros")
	@Produces("application/json")
	public List<String> getTiposLivros() {
	    
	    E2docLivroFinanceiroEngine e2docEngine = new E2docLivroFinanceiroEngine();
	    
	    return e2docEngine.getTiposLivros();
	    

	}
	
	@POST
	@Path("getEmpresas")
	@Produces("application/json")
	public List<String> getEmpresas() {
	    
	    E2docLivroFinanceiroEngine e2docEngine = new E2docLivroFinanceiroEngine();
	    
	    return e2docEngine.getEmpresas();
	    

	}

}
