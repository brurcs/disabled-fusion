package com.neomind.fusion.custom.orsegups.contract;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.vo.DadosDebitoSapiensVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/**
 * Classe com métodos estáticos para uso comum dentro do fluxo de contratos.
 * @author danilo.silva
 *
 */
public class ContratoUtils {
	
	/**
	 * Converte Data para o padrao do Sapiens para insercao/update no banco de dados
	 * @param data
	 * @return Data no formato yyyy-MM-dd HH:MI:SS
	 */
	public static String retornaDataFormatoSapiens(GregorianCalendar data)
	{
		String novaData = "";
		
		if(data == null)
		{
			/*
			 * 31/12/1900 o sapiens entende essa data como vazia
			 */
			data = new GregorianCalendar();
			data.set(data.YEAR, 1900);
			data.set(data.MONTH, 11);
			data.set(data.DAY_OF_MONTH, 31);
			data.set(Calendar.HOUR_OF_DAY, 00);
			data.set(Calendar.MINUTE, 00);
			data.set(Calendar.SECOND, 00);
		}
		
		if(data != null)
		{
			novaData = NeoUtils.safeDateFormat(data, "yyyy-MM-dd");
			novaData = novaData + " 00:00:00";
		}
		
		
		return novaData;
	}
	
	
	/**
	 * Extrai a hora de uma data, retornando apenas os 2 digitos referentes à hora
	 * @param data
	 * @return
	 */
	public static String retornaHora(GregorianCalendar data){
		String retorno = "00";
		if (data != null){
			retorno = NeoUtils.safeDateFormat(data, "HH");
		}
		return retorno;
	}
	
	/**
	 * Extrai a data e formata em DD/MM/YYYY
	 * @param data
	 * @return
	 */
	public static String retornaDataFormatada(GregorianCalendar data){
		String retorno = "00";
		if (data != null){
			retorno = NeoUtils.safeDateFormat(data, "dd/MM/yyyy");
		}
		return retorno;
	}
	
		
	/**
	 * @see retorna o codigo do usuario sapiens baseado no login do usuario fusion
	 * @param userCode 
	 * @return
	 */
	public static Long retornaCodeUsuarioSapiens(String userCode)
	{
		//Logica para buscar codigo do usuario do sapiens referente ao usuario fusion
		String sapiensUserCode = "";
		NeoObject sapiensUser = null;
		InstantiableEntityInfo ieiEX = AdapterUtils.getInstantiableEntityInfo("SAPIENSUSUARIO");
		String login = userCode;

		QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", login);
		ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>) PersistEngine.getObjects(ieiEX.getEntityClass(), loginFilter);

		if (sapiensUsers != null && sapiensUsers.size() > 0)
		{
			sapiensUser = sapiensUsers.get(0);
		}

		if (sapiensUser != null)
		{
			EntityWrapper sapiensUserEw = new EntityWrapper(sapiensUser);
			sapiensUserCode = sapiensUserEw.findValue("codusu").toString();
		}

		if (sapiensUserCode == null || sapiensUserCode.equals(""))
		{
			throw new WorkflowException("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado.");
		}

		return NeoUtils.safeLong(sapiensUserCode);
	
	}
	
	/**
	   * Consome urls que retornam strings em formato json.  
	   * @param _url - caminho completo com parametros. <br> Ex.: http://servidor:8080/SigmaWebServices/CityList?stateId=SC
	   * @return Objeto Json contendo o retorno da url
	   */
	  public static JSONObject consomePaginaJson(String _url){
		  	BufferedReader reader = null;
	      	URL url;
			try {
				url = new URL(_url);
				reader = new BufferedReader(new InputStreamReader(url.openStream()));
				StringBuffer buffer = new StringBuffer();
				int read;
				char[] chars = new char[1024];
				while ((read = reader.read(chars)) != -1)
					buffer.append(chars, 0, read);
				
				if (reader != null){
					reader.close();
				}
				System.out.println("[FLUXO CONTRATOS]- buffer: "+buffer.toString());
			    JSONObject jo = new JSONObject(buffer.toString());
			    return jo;
			} catch (MalformedURLException e) {
				//e.printStackTrace();
				return (JSONObject)null;
			} catch (JSONException e) {
				e.printStackTrace();
				return (JSONObject)null;
			}catch(FileNotFoundException e){
				System.out.println("[FLUXO CONTRATOS]-Erro: Cep não encontrado via webservice => " + e.getMessage());
				return (JSONObject)null;
			}catch (IOException e) {
				e.printStackTrace();
				return (JSONObject)null;
			}
	      
	      
	  }
	  
	  /**
	   * Retorna numero presente no endereco.
	   * @param endereco - endereco no formato -> endereco, numero <br/> Ex.: Rua dos testes, 666
	   * @return numero do endereco 
	   */
	  public static String getNumeroFromEndereco(String endereco){
		  
		  if (endereco != null && endereco.indexOf(",") > -1){
			  return endereco.substring(endereco.indexOf(",")+1, endereco.length()).trim();
		  }else{
			  return "";
		  }
	  }
	  
	  /**
	   * Remove acentuação das palavras
	   * @param stringFonte
	   * @return
	   */
	  public static String retiraCaracteresAcentuados(String stringFonte)
		{
			String passa = stringFonte;
			passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
			passa = passa.replaceAll("[âãàáä]", "a");
			passa = passa.replaceAll("[ÊÈÉË]", "E");
			passa = passa.replaceAll("[êèéë]", "e");
			passa = passa.replaceAll("ÎÍÌÏ", "I");
			passa = passa.replaceAll("îíìï", "i");
			passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
			passa = passa.replaceAll("[ôõòóö]", "o");
			passa = passa.replaceAll("[ÛÙÚÜ]", "U");
			passa = passa.replaceAll("[ûúùü]", "u");
			passa = passa.replaceAll("Ç", "C");
			passa = passa.replaceAll("ç", "c");
			passa = passa.replaceAll("[ýÿ]", "y");
			passa = passa.replaceAll("Ý", "Y");
			passa = passa.replaceAll("ñ", "n");
			passa = passa.replaceAll("Ñ", "N");
			passa = passa.replaceAll("[-+=*&amp;%$#@!_]", "");
			passa = passa.replaceAll("['\"]", "");
			passa = passa.replaceAll("[<>()\\{\\}]", "");
			passa = passa.replaceAll("['\\\\.,()|/]", "");
			passa = passa.replaceAll("[^!-ÿ]{1}[^ -ÿ]{0,}[^!-ÿ]{1}|[^!-ÿ]{1}", " ");
			return passa;
		}
	  
	  public static String retiraCaracteresELetras(String stringFonte)
		{
		  	if (stringFonte == null)
		  		return "";
		  
			String passa = stringFonte;
			passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
			passa = passa.replaceAll("[âãàáä]", "a");
			passa = passa.replaceAll("[ÊÈÉË]", "E");
			passa = passa.replaceAll("[êèéë]", "e");
			passa = passa.replaceAll("ÎÍÌÏ", "I");
			passa = passa.replaceAll("îíìï", "i");
			passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
			passa = passa.replaceAll("[ôõòóö]", "o");
			passa = passa.replaceAll("[ÛÙÚÜ]", "U");
			passa = passa.replaceAll("[ûúùü]", "u");
			passa = passa.replaceAll("Ç", "C");
			passa = passa.replaceAll("ç", "c");
			passa = passa.replaceAll("[ýÿ]", "y");
			passa = passa.replaceAll("Ý", "Y");
			passa = passa.replaceAll("ñ", "n");
			passa = passa.replaceAll("Ñ", "N");
			passa = passa.replaceAll("[-+=*&amp;%$#@!_]", "");
			passa = passa.replaceAll("['\"]", "");
			passa = passa.replaceAll("[<>()\\{\\}]", "");
			passa = passa.replaceAll("['\\\\.,()|/]", "");
			passa = passa.replaceAll("[^!-ÿ]{1}[^ -ÿ]{0,}[^!-ÿ]{1}|[^!-ÿ]{1}", " ");
			passa = passa.replaceAll("[a-z]", "");
			passa = passa.replaceAll("[A-Z]", "");
			return passa;
		}
	  
	  /**
	   * Adiciona postos inseridos pelo fusion para deletar caso ocorra erro no adapter, uma vez que, 
	   * <br>o fusion não tem como controlar o rollback das transações de banco de dados e também
	   * <br> não guarda do eform principal o estado do objeto salvo. 
	   * @param numCtr
	   * @param codEmp
	   * @param codFil
	   * @param postosInseridos
	   */
	  public static void addPostosToRollBack(Long numCtr, Long codEmp, Long codFil, List<String> postosInseridos){
		  for (String t : postosInseridos){
				NeoObject p = AdapterUtils.createNewEntityInstance("FGCRollBackPostos");
				EntityWrapper wP = new EntityWrapper(p);
				wP.setValue("numctr", numCtr);
				wP.setValue("codemp", codEmp);
				wP.setValue("codfil", codFil);
				wP.setValue("numpos", Long.parseLong(t));
				PersistEngine.persist(p);
			}
	  }
	  
	  /**
	   * Adiciona Contratos inseridos pelo fusion para verificar se a tarefa esta pegando outro número de contrato caso ocorra erro no adapter. 
	   * <br>Salva o número do contrato referente a tarefa em especifico, o Fusion não irá duplicar o contrato no sapiens deixando assim um contrato sem posto perdido no sapines.
	   * @param codEmp
	   * @param codFil
	   * @param nTarefa
	   */
	  public static void addContratoToRollBack(Long codEmp, Long codFil, String nTarefa, Long numCtr) throws Exception {
		  		Connection connection = OrsegupsUtils.getSqlConnection("TIDB");
		  		StringBuffer query = new StringBuffer();
				query.append("insert into FGCRollBackContratos (codEmp, codFil, numTar, numCtr) "
						+ "values ("+codEmp+","+codFil+",'"+nTarefa+"',"+numCtr+")");
				PreparedStatement st = null;
				try {
					st = connection.prepareStatement(query.toString());
					st.executeUpdate();
				} catch (SQLException e) {
					System.out.println("[FLUXO CONTRATOS]- Erro ao Inserir o contrato na tabela FGCRollBackContratos com as chaves"
							+ " Empresa: "+codEmp+" Filial: "+codFil+" Tarefa: "+nTarefa+" Contrato: "+numCtr);
					throw new Exception("Erro ao Inserir o contrato na tabela FGCRollBackContratos com as chaves"
							+ " Empresa: "+codEmp+" Filial: "+codFil+" Tarefa: "+nTarefa+" Contrato: "+numCtr+" entre em contato com a TI");
				}
				finally
				{
					if (st != null)
					{
						try
						{
							st.close();
							connection.close();
						}
						catch (SQLException e)
						{
							System.out.println("[FLUXO CONTRATOS] - Erro ao fechar o statement");
							e.printStackTrace();
						}
					}
				}
			}
	  
	  /**
	   * Retorna uma lista com os postos que devem ser deletados por motivo de erro nos adapters do fusion.
	   * @param numCtr
	   * @param codEmp
	   * @param codFil
	   * @return
	   */
	  public static List<String> retornaListaPostosToRollBack(Long numCtr, Long codEmp, Long codFil){
		  List<String> retorno = new ArrayList<String>();
		  QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("numctr", numCtr));
			gp.addFilter(new QLEqualsFilter("codemp", codEmp));
			gp.addFilter(new QLEqualsFilter("codfil", codFil));
			Collection<NeoObject> lista = PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCRollBackPostos"), gp); 
			for(NeoObject obj: lista){
				EntityWrapper wObj = new EntityWrapper(obj);
				retorno.add( String.valueOf(wObj.findValue("numpos")) );
				wObj = null;
			}
			//lista.clear();
			//lista = null;
			return retorno;
	  }
	   
	  /**
	   * Retorna uma lista com os postos que devem ser deletados por motivo de erro nos adapters do fusion.
	   * @param numCtr
	   * @param codEmp
	   * @param codFil
	   * @param nTarefa
	   * @return
	   */
	  public static Long retornaContratoToRollBack(Long codEmp, Long codFil, String nTarefa){
		  
		  StringBuffer query = new StringBuffer();
		  query.append("SELECT numCtr FROM FGCRollBackContratos " +
					     "where codEmp = "+codEmp+" and codFil = "+codFil+" and numTar = '"+nTarefa+"'");
		  ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), "TIDB");
			
			if (rs != null)
			{
				try
				{ 
				 return rs.getLong("numCtr");	
				}
				catch (SQLException e)
				{
					System.out.println("[FLUXO CONTRATOS] ERRO: Falha ao verificar FGCRollBackContratos para retornaContratoToRollBack.");
				}
			}
			
			return 0L;
	  }
	  
	  /**
	   * Remove a lista de postos do eform FGCRollBackPostos
	   * @param numCtr
	   * @param codEmp
	   * @param codFil
	   * @return
	   */
	  public static boolean removeListaPostosToRollBack(Long numCtr, Long codEmp, Long codFil){
		  QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("numctr", numCtr));
			gp.addFilter(new QLEqualsFilter("codemp", codEmp));
			gp.addFilter(new QLEqualsFilter("codfil", codFil));
			Collection<NeoObject> oPostos = PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCRollBackPostos"), gp);
			return PersistEngine.removeObjects(oPostos);
	  }
	  
	  
	  /**
	   * Remove a lista de postos do eform FGCRollBackPostos
	   * @param numCtr
	   * @param codEmp
	   * @param codFil
	   * @return
	   */
	  public static void removeListaContratosToRollBack(Long codEmp, Long codFil, String nTarefa){
		  
	    StringBuffer query = new StringBuffer();
	    query.append(" delete FGCRollBackContratos where codEmp = "+codEmp+" and codFil = "+codFil+" and numTar = '"+nTarefa+"'");		
		
		Connection connection = OrsegupsUtils.getSqlConnection("TIDB");
		
		ContratoLogUtils.logInfo( "ROLLBACK - CONTRATOS " + query.toString());
		
		PreparedStatement st = null;
		try
		{	
			if (query != null && !query.toString().equals("")){
				st = connection.prepareStatement(query.toString());
				System.out.println("[FLUXO CONTRATOS]- FGCRollBackContratos: "+st.executeUpdate());
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro ao deletas o contrato da tabela FGCRollBackContratos."+e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
	  }
	  
	  
	  /**S
		 * Remove os postos inseridos neste fluxo. Só deve ser chamado quando houver alguma exceção que impessa o fluxo de contuniar.
		 */
		public static void rollBackPostos(Long usu_numctr, Long usu_codemp, Long usu_codfil, List<String> postosInseridos ){
			StringBuilder query = new StringBuilder();
			if (postosInseridos != null){
				for(String posto: postosInseridos){
					query.append(" delete  usu_t160CHK where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
					query.append(" delete  usu_t160CHP where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
					query.append(" delete  usu_t160CON where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
					query.append(" delete  usu_t160CMS where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
					query.append(" delete  usu_t160EQP where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
					query.append(" delete  usu_t160JOR where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
					query.append(" delete  usu_t160KPO where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
					query.append(" delete  usu_t160OBS where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
					query.append(" delete  usu_t160CVS where usu_numctr = "+usu_numctr+" and usu_numpos = "+posto+" and usu_codemp = "+usu_codemp+" and usu_codfil="+usu_codfil+"; ");
				}
				
				Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
				
				ContratoLogUtils.logInfo( "ROLLBACK - POSTOS " + query.toString());
				
				PreparedStatement st = null;
				try
				{	
					if (query != null && !query.toString().equals("")){
						st = connection.prepareStatement(query.toString());
						System.out.println("[FLUXO CONTRATOS]- RollbackPostos: "+st.executeUpdate());
					}
				}

				catch (Exception e) {
					e.printStackTrace();
					throw new WorkflowException("Erro ao fazer rollback na inserção dos postos."+e.getMessage());
				}
				finally
				{
					if (st != null)
					{
						try
						{
							st.close();
							connection.close();
						}
						catch (SQLException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		
		
		
		  public static void addCCToRollBack(Long numCtr, Long ctaRed){
			    ContratoLogUtils.logInfo("addCCRollBack->Ctr:"+numCtr+", codccu:"+ctaRed);
				NeoObject p = AdapterUtils.createNewEntityInstance("FGCRollBackCentroCustos");
				EntityWrapper wP = new EntityWrapper(p);
				wP.setValue("numctr", numCtr);
				wP.setValue("ctared", ctaRed);
				PersistEngine.persist(p);
				//wP = null;
				//p = null;
		  }
		  
		  public static List<String> retornaListaCCToRollBack(Long numCtr){
			  List<String> retorno = new ArrayList<String>();
			  QLGroupFilter gp = new QLGroupFilter("AND");
				gp.addFilter(new QLEqualsFilter("numctr", numCtr));
				Collection<NeoObject> lista = PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCRollBackCentroCustos"), gp); 
				for(NeoObject obj: lista){
					EntityWrapper wObj = new EntityWrapper(obj);
					retorno.add( String.valueOf(wObj.findValue("ctared")) );
					wObj = null;
				}
				//lista.clear();
				//lista = null;
				return retorno;
		  }
		  
		  public static boolean removeListaCCToRollBack(Long numCtr){
			  QLGroupFilter gp = new QLGroupFilter("AND");
				gp.addFilter(new QLEqualsFilter("numctr", numCtr));
				Collection<NeoObject> oContas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCRollBackCentroCustos"), gp);
				return PersistEngine.removeObjects(oContas);
		  }
		  
		  
		  public static void rollBackCC(Long usu_numctr, List<String> ContasInseridas, String origem ){
			Long keyRollBackCC = (new GregorianCalendar()).getTimeInMillis();
			System.out.println("[FLUXO CONTRATOS] ["+keyRollBackCC+"]- fazendo rollbackCC contrato "+usu_numctr + " Origem do rollback: " + origem);
			StringBuilder query = new StringBuilder();
			if (ContasInseridas != null){
				for(String conta: ContasInseridas){
					//query.append(" begin tran; ");
					query.append(" delete e044ccu where codccu = "+conta+"; ");
					//query.append(" commit tran; ");
				}
				System.out.println("[FLUXO CONTRATOS] ["+keyRollBackCC+"] ctr: "+usu_numctr+",Centros de custo a serem excluidos ->" + query.toString());
	
				Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
				
				PreparedStatement st = null;
				try
				{	
					if (query != null && !query.toString().equals("")){
						st = connection.prepareStatement(query.toString());
						System.out.println("[FLUXO CONTRATOS] ["+keyRollBackCC+"] - ctr: "+usu_numctr+", rollbackCC: "+st.executeUpdate());
					}
				}
	
				catch (Exception e) {
					e.printStackTrace();
					throw new WorkflowException("[FLUXO CONTRATOS] ["+keyRollBackCC+"] - Erro ao fazer rollback na inserção das contas."+e.getMessage());
				}
				finally
				{
					if (st != null)
					{
						try
						{
							st.close();
							connection.close();
						}
						catch (SQLException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}
		  
		  public static void verificarNiveisCcuExistentes(String claCcu){
			  StringBuilder query = new StringBuilder();
			  StringBuilder query2 = new StringBuilder();
			  claCcu = claCcu+"0001";
			  query.append(" delete e044ccu where claccu like '"+claCcu+"%';");
			  query2.append("delete e043pcm where clacta like '"+claCcu+"%';");
			  Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
			  PreparedStatement st = null;
			  PreparedStatement st2 = null;
			  try
			{
				st = connection.prepareStatement(query.toString());
				st2 = connection.prepareStatement(query2.toString());
				System.out.println("[FLUXO CONTRATOS]- Rollbaccu via metodo verificarNiveisCcuExistentes e044ccu: "+st.executeUpdate());
				System.out.println("[FLUXO CONTRATOS]- Rollbaccu via metodo verificarNiveisCcuExistentes e043pcm: "+st2.executeUpdate());
			}
			  catch (Exception e) {
					e.printStackTrace();
					throw new WorkflowException("[FLUXO CONTRATOS] ["+claCcu+"] - Erro no metodo verificarNiveisCcuExistentes. "+e.getMessage());
				}
				finally
				{
					if (st != null)
					{
						try
						{
							st.close();
							connection.close();
						}
						catch (SQLException e)
						{
							e.printStackTrace();
						}
					}
				}
		  }
		  
		  
	  /**
	   * Remove centro de custo das tabelas e044ccu e e043pcm respectivamente pela classificacao
	   * @param aClacta
	   */
	  public static void deleteCentroCusto(String aClacta ){
		  Long keyDeleteCC = (new GregorianCalendar()).getTimeInMillis();
			StringBuilder query = new StringBuilder();
			if (aClacta != null){
				query.append(" delete e043pcm where clacta = '"+aClacta+"'; ");
				query.append(" delete e044ccu where claccu = '"+aClacta+"'; ");
	
				Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
				System.out.println("[FLUXO CONTRATOS] [" + keyDeleteCC +"] - Centros de custo removidos: " + query.toString());
				PreparedStatement st = null;
				try
				{	
					if (query != null && !query.toString().equals("")){
						st = connection.prepareStatement(query.toString());
						System.out.println("[FLUXO CONTRATOS] [" + keyDeleteCC +"] - centro de custo removido com sucesso c.c. : [1-sim/0-nao] -> "+st.executeUpdate());
					}
				}catch (Exception e) {
					e.printStackTrace();
					throw new WorkflowException("[" + keyDeleteCC +"] Erro ao deletar conta cadastrada com problema . codccu = " + aClacta + ", " +e.getMessage());
				}
				finally
				{
					if (st != null)
					{
						try
						{
							st.close();
							connection.close();
						}
						catch (SQLException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}
	  


		
	 


	public static void rollBackContaSigma(ArrayList<String> cdClienteRollBack)
	{
		StringBuilder query = new StringBuilder();
		if (cdClienteRollBack != null){
			for(String cdCliente: cdClienteRollBack){
				//query.append(" begin tran; ");
				query.append(" delete dbPROVIDENCIA where CD_CLIENTE =  "+cdCliente+"; ");
				query.append(" delete dbCentral where cd_cliente = "+cdCliente+"; ");
				//query.append(" commit tran; ");
			}
			

			Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
			
			PreparedStatement st = null;
			try
			{	
				if (query != null && !query.toString().equals("")){
					st = connection.prepareStatement(query.toString());
					System.out.println("[FLUXO CONTRATOS]-Rollback Conta Sigma: " +st.executeUpdate());
				}
			}

			catch (Exception e) {
				e.printStackTrace();
				throw new WorkflowException("Erro ao fazer rollback na inserção das contas SIGMA. "+e.getMessage());
			}
			finally
			{
				if (st != null)
				{
					try
					{
						st.close();
						connection.close();
					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	  
	  
	
	public static NeoObject retornaRepresentantePeloLogin(String loginFusion){
		NeoObject retorno = null;
		
		if (loginFusion != null && !loginFusion.isEmpty()){
			
		
			String sql ="select codrep from e090rep where usu_usufus like '"+loginFusion+"%'  and codrep not in (157) order by codrep desc"; // 157 - código duplicado do maurilio que não deve ser usado
			ResultSet rs = null;
			Long codRep = 0L;
			try{
				rs = OrsegupsContratoUtils.selectTable(sql, PersistEngine.getConnection("SAPIENS"));
				if (rs!= null){
					codRep = rs.getLong(1);
					
					QLGroupFilter gp = new QLGroupFilter("AND");
					gp.addFilter(new QLRawFilter(" codrep = " + codRep));
					ArrayList<NeoObject> representantes = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEREP"), gp );
					if (representantes != null && !representantes.isEmpty()){
						retorno = representantes.get(0);
					}
					
				}
				return retorno;
			}catch(Exception e){
				ContratoLogUtils.logInfo("Erro ao buscar representante " + loginFusion + " codrep="+codRep+" para auto-preencher no formulário. msg:" + e.getMessage());
				return null;
			}
		}
		return retorno;
		
	}
	
	public static void validaTamanhoCampo(String nomeCampo ,String valorCampo, int maxSize) throws Exception {
		if (valorCampo != null){
			if (valorCampo.length() > maxSize){
				throw new Exception("Campo " + nomeCampo + " com valor ("+valorCampo+") ultrapassou o limite máximo de "+maxSize+" caracteres");
			}
		}
	}
	
	public static String truncaCampo(String campo, int maxSize) {
		String retorno = "";
		if (campo != null && !campo.isEmpty()){
			retorno = campo.substring(0, (campo.length()>maxSize? 79:campo.length()));
		}
		
		return retorno;
	}
	
	public static String completaZerosEsquerda(String valor, int tamanhoAlvo){
		String retorno = valor;
		String zeros = "";
		if (valor != null){
			if (valor.length() < tamanhoAlvo){
				for (int i = 0 ; i < (tamanhoAlvo - valor.length()); i++ ){
					zeros += "0"; 
				}
			}
		}
		return zeros+retorno;
	}
	
	
	public static String corrigeCpFCnpj(String cgccpf, String tipoPessoa){
		String mascara = "";
		String retorno = "";
		if (cgccpf != null && "J".equals(tipoPessoa) ){
			mascara = "##.###.###/####-##";
			cgccpf = completaZerosEsquerda(cgccpf,14);
		}
		if (cgccpf != null &&  "F".equals(tipoPessoa)  ){
			mascara = "###.###.###-##";
			cgccpf = completaZerosEsquerda(cgccpf,11);
		}
		
		try {
			if (cgccpf.contains(".") &&  cgccpf.contains("/") && cgccpf.contains("-") ){
				ContratoLogUtils.logInfo(cgccpf + " já estava formatado!");
				return null;
			}
			retorno = OrsegupsUtils.formatarString(cgccpf, mascara);
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			return retorno;
		}
	}
	
	/**
	 * Verifica se um posto possui faturas  com data maior ou igual a data passada.
	 * @param numctr - Numero do contrato
	 * @param numpos - Numero do posto
	 * @param codemp - Codigo da Empresa
	 * @param codfil - Codigo da Filial
	 * @param cptAlvo - Competencia que se deseja usar na comparação
	 * @return
	 */
	public static boolean postoJaFaturado(Long numctr, Long numpos, Long codemp, Long codfil, GregorianCalendar cptAlvo){
		Boolean retorno = false;
			String sql = "select count(*) from E160CVS  where numctr = ? and usu_seqagr = ? and  codemp = ? and codfil = ? and datCpt >= DATEADD(month, DATEDIFF(month, 0, ?), 0)";
			Connection conn =  null;
			ResultSet  rs = null;
			Long count = 0L;
			PreparedStatement pst = null;
			try{
				conn = PersistEngine.getConnection("SAPIENS");
				pst = conn.prepareStatement(sql);
				
				pst.setLong(1, numctr);
				pst.setLong(2, numpos);
				pst.setLong(3, codemp);
				pst.setLong(4, codfil);
				pst.setDate(5, new Date(cptAlvo.getTimeInMillis()));
				
				rs = pst.executeQuery();
				while(rs.next()){
					count = rs.getLong(1);
					if (count > 0){
						retorno = true;
					}
				}
				rs.close();
				rs = null;
			}catch(Exception e){
				System.err.println("sql:" + sql);
				e.printStackTrace();
				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				throw new WorkflowException("Erro ao consultar faturamento do posto antigo." + e.getMessage());
			}finally{
				try{
					OrsegupsUtils.closeConnection(conn, pst, rs);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		return retorno;
	}
	
	
	/**
	 * busca na tabela e085hcl por dados de débito automatico para importar no fluxo de contratos
	 * @param usu_codcli
	 * @param usu_codemp
	 * @param usu_codfil
	 * @return
	 */
	public static DadosDebitoSapiensVO retornaDadosDebitoSapiens(String usu_codcli, String usu_codemp, String usu_codfil){
		DadosDebitoSapiensVO retorno = new DadosDebitoSapiensVO();
		String sql = "select PorSi1, codBan, codAge, ccbCli  from e085hcl h where codcli = ? and codemp = ? and codfil = ?";
		Connection conn =  null;
		ResultSet  rs = null;
		Long count = 0L;
		PreparedStatement pst = null;
		try{
			conn = PersistEngine.getConnection("SAPIENS");
			pst = conn.prepareStatement(sql);
			
			pst.setLong(1, NeoUtils.safeLong(usu_codcli));
			pst.setLong(2, NeoUtils.safeLong(usu_codemp));
			pst.setLong(3, NeoUtils.safeLong(usu_codfil));
			
			rs = pst.executeQuery();
			while(rs.next()){
				retorno = new DadosDebitoSapiensVO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
			}
			rs.close();
			rs = null;
		}catch(Exception e){
			System.err.println("sql:" + sql);
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new WorkflowException("Erro ao consultar dados de débito do cliente" + e.getMessage());
		}finally{
			try{
				OrsegupsUtils.closeConnection(conn, pst, rs);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	return retorno;
	}
	
	
	/**
	 * atualiza dados de débito automatico do cliente na tabela e085hcl
	 * @param usu_codcli
	 * @param usu_codemp
	 * @param usu_codfil
	 * @return
	 */
	public static boolean atualizaDadosDebitoSapiens(Long usu_codcli, Long usu_codemp, Long usu_codfil, DadosDebitoSapiensVO dadosDebito ){
		boolean retorno = false;
		String sql = " update e085hcl set PorSi1=?, codBan=?, codAge=?, ccbCli=?  where codcli = ? and codemp = ? and codfil = ?";
		Connection conn =  null;
		ResultSet  rs = null;
		Long count = 0L;
		PreparedStatement pst = null;
		try{
			conn = PersistEngine.getConnection("SAPIENS");
			pst = conn.prepareStatement(sql);
			
			
			pst.setString(1, dadosDebito.getPrimeiroPortador());
			pst.setString(2, dadosDebito.getBanco());
			pst.setString(3, dadosDebito.getAgencia());
			pst.setString(4, dadosDebito.getContacorrente());
			
			pst.setLong(5, usu_codcli);
			pst.setLong(6, usu_codemp);
			pst.setLong(7, usu_codfil);
			
			
			int contador = pst.executeUpdate();
			if (contador > 0 ){ retorno = true; }
			
			
		}catch(Exception e){
			System.out.println("Erro ao atualizar dados de débito do cliente ["+dadosDebito+"] >  sql:" + sql);
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new WorkflowException("Erro ao atualizar dados de débito do cliente" + e.getMessage());
		}finally{
			try{
				OrsegupsUtils.closeConnection(conn, pst, rs);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	return retorno;
	}
	
	
	public static void geraObservacao(Long usu_codemp, Long usu_codfil, Long usu_numctr, Long usu_seqobs, Long usu_numpos, String usu_codser,
			String usu_tipobs, String usu_txtobs, Long usu_usumov, GregorianCalendar usu_datmov, Long usu_hormov, String usu_codope)
	{
		/*INSERT INTO USU_T160OBS (USU_CODEMP,USU_CODFIL,USU_NUMCTR,USU_SEQOBS,USU_NUMPOS,USU_CODSER, USU_TIPOBS,USU_TXTOBS,USU_USUMOV,USU_DATMOV,USU_HORMOV,USU_CODOPE) \
				VALUES (vCodEmp, vCodFil,vNumCtr,vSeqObs,vNumPos,vCodSer,atipobs,vTxtObs,CodUsu;vDatAtu,HorSis,'O')	
		 */
		Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
		StringBuffer query = new StringBuffer();

		query.append("INSERT INTO USU_T160OBS" +
				" (USU_CODEMP,USU_CODFIL,USU_NUMCTR,USU_SEQOBS,USU_NUMPOS,USU_CODSER, " +
				"USU_TIPOBS,USU_TXTOBS,USU_USUMOV,USU_DATMOV,USU_HORMOV,USU_CODOPE, USU_NUMRMC) ");

		query.append("VALUES ("+usu_codemp +", "+ usu_codfil +", "+ usu_numctr +", "+ usu_seqobs +", "+ usu_numpos +", '"+ usu_codser 
				+"', '"+ usu_tipobs +"', '"+ usu_txtobs +"', "+ usu_usumov +", '"+ ContratoUtils.retornaDataFormatoSapiens(usu_datmov) +"', "+ usu_hormov +", '"+ usu_codope+"', 0)") ;

		PreparedStatement st = null;
		try
		{	
			st = connection.prepareStatement(query.toString());
			st.executeUpdate();
			System.out.println("[FLUXO CONTRATOS] - Contrato: "+usu_numctr+" - Posto: " +usu_numpos+ " - obs: "+usu_seqobs);
		}

		catch (Exception e) {
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				System.out.println("[FLUXO CONTRATOS] - Erro ao gerar obs posto no Fluxo de contratos."+ e.getMessage());
				throw new WorkflowException("Erro ao gerar Observações posto no Fluxo de contratos. "+e);
			}
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					System.out.println("[FLUXO CONTRATOS] - Erro ao fechar o statement");
					e.printStackTrace();
				}
			}

		}
	}
	
	/**
	 * Busca o número da ultima observação inserida USU_T160obs e incrementa +1
	 * select * FROM USU_T160obs where usu_codemp = 18 and usu_codfil = 1 and usu_numctr = 331478 oRDER BY usu_seqobs DESC
	 * 
	 * @return numero nova obs
	 */
	public static Long retornaSequenciaObservacaoContrato(Long usu_codemp, Long usu_codfil, Long numContrato)
	{
		Long numeroObsNovo = new Long (1);
		
		StringBuffer query = new StringBuffer();

		query.append("SELECT usu_seqobs FROM USU_T160obs " +
				"where usu_codemp = "+usu_codemp+" and usu_codfil = "+usu_codfil+" and usu_numctr = "+numContrato+" ORDER BY usu_seqobs DESC" );
		
		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), "SAPIENS");

		if (rs != null)
		{
			try
			{ 
				Long numeroUltimoObs = rs.getLong("usu_seqobs");
				numeroObsNovo = numeroUltimoObs + 1;
				rs.close();
				rs =null;
			}
			catch (SQLException e)
			{
				System.out.println("[FLUXO CONTRATOS] - ERRO: Falha ao verificar USU_T160obs para retornaSequenciaObservacaoContrato.");
			}
		}
		
		return numeroObsNovo;
	
	}
	
	/**
	 * Retorna os impostos do posto do contrato na seguinte ordem: <br> usu_perirf<br> usu_perins<br> usu_perpit<br> usu_percsl<br> usu_percrt<br>
	 * @param usu_numctr - numero do contrato
	 * @param usu_numpos - numero do posto
	 * @return
	 */
	public static BigDecimal[] retornaImpostos(String usu_numctr, String usu_numpos){
		BigDecimal retorno[] = new BigDecimal[6]; 
		String query = "select usu_perirf, usu_perins, usu_perpit, usu_percsl, usu_percrt, usu_periss from usu_t160cvs where usu_numctr = "+usu_numctr+" and usu_numpos = "+usu_numpos;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try{
			ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), "SAPIENS");
			if (rs != null)
			{
				retorno[0] = rs.getBigDecimal("usu_perirf");
				retorno[1] = rs.getBigDecimal("usu_perins");
				retorno[2] = rs.getBigDecimal("usu_perpit");
				retorno[3] = rs.getBigDecimal("usu_percsl");
				retorno[4] = rs.getBigDecimal("usu_percrt");
				retorno[5] = rs.getBigDecimal("usu_periss");
			}else{
				ContratoLogUtils.logInfo("["+key+"] - Contrato/posto não encontrado: " + usu_numctr + ", usu_numpos: " + usu_numpos + ", query-> " + query);
				throw new WorkflowException("["+key+"] - Erro ao consultar impostos do contrato original, posto / contrato não encontrados. Numero: " + usu_numctr + ", usu_numpos: " + usu_numpos + "");
			}
			rs.close();
			rs =null;
			return retorno;
		}catch(Exception e){
			ContratoLogUtils.logInfo("["+key+"] - Erro ao consultar os impostos do contrato numero: " + usu_numctr + ", usu_numpos: " + usu_numpos + ", query-> " + query);
			e.printStackTrace();
			//throw new WorkflowException("["+key+"] - Erro ao consultar impostos do contrato original. Numero: " + usu_numctr + ", usu_numpos: " + usu_numpos + "");
			return null;
		}
	}
	
	/**
	 * Extrai a hora de uma data, retornando apenas os 2 digitos referentes à hora
	 * @param data
	 * @return
	 */
	public static String retornaHoraFormatoSapiens(GregorianCalendar data){
		String retorno = "00";
		if (data != null){
			retorno = NeoUtils.safeOutputString( (NeoUtils.safeInteger(NeoUtils.safeDateFormat(data, "HH")) *60) + NeoUtils.safeInteger(NeoUtils.safeDateFormat(data, "mm")) ) ;
		}
		return retorno;
	}
	
	public static void main(String args[]){
		//System.out.println(truncaCampo("CONTRATO - 344112", 80));
		System.out.println(corrigeCpFCnpj("00715554000175","J"));
		 
		// System.out.println(completaZerosEsquerda("715554000175",14));
		System.out.println(retornaHoraFormatoSapiens(new GregorianCalendar()));
		 
	}
	
	public static Long retornaCodRegimeTributarioCliente(Long codcli){
		Long retorno = null ;
		
		StringBuilder query = new StringBuilder();

		query.append("SELECT CodRtr from E085CLI where codcli = " + codcli );
		
		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), "SAPIENS");

		if (rs != null)
		{
			try
			{ 
				Long codrtr = rs.getLong("CodRtr");
				retorno = codrtr ;
			}
			catch (SQLException e)
			{
				System.out.println("[FLUXO CONTRATOS] - ERRO: consultar codRtr do cliente.");
				OrsegupsUtils.closeConnection(null, null, rs);
			}
		}
		
		return retorno;
	}
	
	/**
	 * Método desenvolvido otimizar a conversão do fusion versão 2.3.5 para 3.2.x. 
	 * <br>Vai sempre retornar a primeira posição de um list de objetos.
	 * @param lista
	 * @return
	 */
	public static NeoObject getFirstNeoObjectFromList(List<NeoObject> lista){
		if (lista != null && !lista.isEmpty()){
			return lista.get(0);
		}else{
			return null;
		}
		
	}
	  
	
}
