package com.neomind.fusion.custom.orsegups.site;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.jfree.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.presenca.vo.AfastamentoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ClienteDocumentosSapiensVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoEscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HorarioVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PresencaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ReclamacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.RegistroAtividadeRSCVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.SaldoHorasVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.SolicitacaoVO;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.site.vo.HorasVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteAnotacaoColaboradorVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteContraChequeColaboradorVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteDocsMostrarVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteFilialColaboradorVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteGraficoOrdemServicoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteGraficoVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteTipoDocumentoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.i18n.I18nUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class SiteUtils
{
	//private static final String LINK_EVENTO = "http://www.webalarme.com.br/acessoControlador.php?";
	//private static final String COD_CLIENTE_EVENTO = "054";

	public NeoObject getSession(String sessionId)
	{
		QLEqualsFilter filtroSession = new QLEqualsFilter("key", sessionId);
		List<NeoObject> sessions = PersistEngine.getObjects(AdapterUtils.getEntityClass("UsuarioWebServiceSession"), filtroSession);

		NeoObject session = null;

		if (sessions != null && !sessions.isEmpty())
		{
			for (NeoObject ses : sessions)
			{
				EntityWrapper sessionWrapper = new EntityWrapper(ses);
				GregorianCalendar dataAtual = new GregorianCalendar();
				GregorianCalendar time = (GregorianCalendar) sessionWrapper.findValue("time");

				if (time.after(dataAtual))
				{
					dataAtual.add(Calendar.HOUR_OF_DAY, +1);
					sessionWrapper.setValue("time", dataAtual);
					PersistEngine.persist(ses);
					session = ses;
					break;
				}
				else
				{
					PersistEngine.remove(ses);
				}
			}
		}
		return session;
	}

	public List<NeoObject> listarContaSigmaContratoSapiens(Long cpf)
	{
		List<NeoObject> lista = new ArrayList<NeoObject>();

		QLGroupFilter filterUser = new QLGroupFilter("AND");
		filterUser.addFilter(new QLEqualsFilter("cgccpf", cpf));
		//filterUser.addFilter(new QLEqualsFilter("sitcli", "A"));

		//View de Contas
		List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterUser);

		if (users != null && !users.isEmpty())
		{
			EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
			Long codcli = (Long) usuarioCliente.getValue("codcli");

			QLGroupFilter filterOr = new QLGroupFilter("AND");
			filterOr.addFilter(new QLEqualsFilter("codcli", codcli));
			filterOr.addFilter(new QLEqualsFilter("usu_sitctr", new String("I")));
			filterOr.addFilter(new QLOpFilter("usu_datfim", ">=", new GregorianCalendar()));

			QLGroupFilter filterCtr = new QLGroupFilter("AND");
			filterCtr.addFilter(new QLEqualsFilter("codcli", codcli));
			filterCtr.addFilter(new QLEqualsFilter("usu_sitctr", new String("A")));

			QLGroupFilter filterCtrOr = new QLGroupFilter("OR");
			filterCtrOr.addFilter(filterOr);
			filterCtrOr.addFilter(filterCtr);

			List<NeoObject> ctr = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VFUSCTRSAP"), filterCtrOr);

			if (ctr != null && !ctr.isEmpty())
			{
				for (NeoObject ctrNeo : ctr)
				{
					EntityWrapper clienteCtr = new EntityWrapper(ctrNeo);
					Long codEmp = (Long) clienteCtr.findValue("usu_codemp");
					Long codFil = (Long) clienteCtr.findValue("usu_codfil");
					Long numCtr = (Long) clienteCtr.findValue("usu_numctr");

					QLGroupFilter filterSig = new QLGroupFilter("AND");
					filterSig.addFilter(new QLEqualsFilter("usu_codemp", codEmp));
					filterSig.addFilter(new QLEqualsFilter("usu_codfil", codFil));
					filterSig.addFilter(new QLEqualsFilter("usu_numctr", numCtr));

					List<NeoObject> listaAux = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTSIG"), filterSig);
					if (listaAux != null && !listaAux.isEmpty())
					{
						List<Long> clientes = new ArrayList<Long>();
						for (NeoObject sig : listaAux)
						{
							EntityWrapper wrapperSigma = new EntityWrapper(sig);
							Long codCliAux = (Long) wrapperSigma.findValue("usu_codcli");

							if (!clientes.contains(codCliAux))
							{
								clientes.add(codCliAux);
								lista.add(sig);
							}
						}
					}
				}
			}
		}
		return lista;
	}

	public String listarCdClietes(Long cpf)
	{
		List<NeoObject> sigs = this.listarContaSigmaContratoSapiens(cpf);
		StringBuilder cdClientes = new StringBuilder();
		Integer cont = 1;

		if (sigs != null && !sigs.isEmpty())
		{
			for (NeoObject sigNeo : sigs)
			{
				EntityWrapper clienteSig = new EntityWrapper(sigNeo);
				Long cdCli = (Long) clienteSig.findValue("usu_codcli");

				if (!cdClientes.toString().contains(cdCli.toString()))
				{
					cdClientes.append(cdCli.toString());
					if (sigs.size() > cont)
					{
						cdClientes.append(",");
					}
					cont++;
				}
			}
		}
		return cdClientes.toString();
	}

	public List<SiteGraficoVO> listarGraficosCliente(Long cdCliente, SiteGraficoVO graficoOS, SiteGraficoVO graficoTat, SiteGraficoVO graficoDes)
	{
		List<SiteGraficoVO> graficos = new ArrayList<SiteGraficoVO>();

		//SiteGraficoVO graficoAtendimentoTatico = this.graficoAtendimentoTatico(cdCliente);
		//SiteGraficoVO graficoDesvioHabito = this.graficoDesvioHabito(cdCliente);
		//SiteGraficoVO graficoOrdemServico = this.graficoOrdemServico(cdCliente);
		SiteGraficoVO graficoTatico = (SiteGraficoVO) graficoTat;
		SiteGraficoVO graficoDesvio = (SiteGraficoVO) graficoDes;
		SiteGraficoVO graficoOrdemServico = (SiteGraficoVO) graficoOS;

		//		if (graficoAtendimentoTatico != null)
		//		{
		//			graficos.add(graficoAtendimentoTatico);
		//		}
		//		
		//		if (graficoDesvioHabito != null)
		//		{
		//			graficos.add(graficoDesvioHabito);
		//		}

		//		if (graficoOrdemServico != null)
		//		{
		//			graficos.add(graficoOrdemServico);
		//		}

		if (graficoTatico != null)
		{
			graficos.add(graficoTatico);
		}

		if (graficoDesvio != null)
		{
			graficos.add(graficoDesvio);
		}

		if (graficoOrdemServico != null)
		{
			graficos.add(graficoOrdemServico);
		}
		return graficos;
	}

	public SiteGraficoVO graficoAtendimentoTatico(Long cdCliente)
	{
		SiteGraficoVO grafico = null;
		Connection connVetorh = PersistEngine.getConnection("SIGMA90");
		String txt = "";
		String txtList = "";
		PreparedStatement st = null;
		ResultSet rs = null;

		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT TOP 30 h.DT_VIATURA_NO_LOCAL, ");
//			sql.append("CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60 AS TEMPO ");
			
			sql.append(" (       CASE WHEN ((CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60) < 0) ");
			sql.append("         THEN ");
			sql.append("         CONVERT(INT, DATEDIFF(SECOND, h.DT_PROCESSADO, h.DT_VIATURA_NO_LOCAL)) /60 ");
			sql.append("         ELSE ");
			sql.append("         CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60 ");
			sql.append("         END) AS TEMPO ");
			
			sql.append(" FROM VIEW_HISTORICO h WITH (NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE ");
			sql.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
			sql.append(" INNER JOIN USUARIO u WITH (NOLOCK) ON u.CD_USUARIO = h.CD_USUARIO_VIATURA_NO_LOCAL ");
			sql.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = u.CD_COLABORADOR ");
			sql.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
			sql.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL ");
			sql.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL ");
			sql.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 ");
			sql.append(" AND cod.TIPO = 1 AND h.CD_CODE <> 'EFM' AND h.CD_CODE <> 'X1A' AND c.CD_CLIENTE = ? ");
			sql.append(" AND h.DT_VIATURA_NO_LOCAL >= GETDATE() - 600 ");
			sql.append(" AND (DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) < 3000 ");
			sql.append(" ORDER BY DT_VIATURA_NO_LOCAL DESC ");

			st = connVetorh.prepareStatement(sql.toString());

			st.setLong(1, cdCliente);

			rs = st.executeQuery();

			//Situacoes
			while (rs.next())
			{
				GregorianCalendar data = new GregorianCalendar();
				data.setTime(rs.getTimestamp("DT_VIATURA_NO_LOCAL"));

				String txtLine = "";

				txtLine = txtLine + "[new Date(";
				txtLine = txtLine + data.get(Calendar.YEAR);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.MONTH);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.DATE);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.HOUR_OF_DAY);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.MINUTE);
				txtLine = txtLine + "),";
				txtLine = txtLine + rs.getLong("TEMPO");
				txtLine = txtLine + "],";

				txtList = txtLine + txtList;
			}

			if (!txtList.equals(""))
			{
				txt = "[" + txtList + "]";
				grafico = new SiteGraficoVO();
				grafico.setGrafico(GraficosEnum.ATENDIMENTOTATICO.getDescricao());
				grafico.setOptions(GraficosEnum.ATENDIMENTOTATICO.getOptions());
				grafico.setValor(txt);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, st, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		return grafico;
	}

	public SiteGraficoVO graficoDesvioHabito(Long cdCliente)
	{
		SiteGraficoVO grafico = new SiteGraficoVO();
		Connection connVetorh = PersistEngine.getConnection("SIGMA90");
		String txt = "";
		String txtList = "";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			StringBuffer sql = new StringBuffer();
			/*sql.append(" SELECT TOP 30 h.DT_RECEBIDO, CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)) /60 AS TEMPO  ");
			sql.append(" FROM HISTORICO_DESARME h WITH (NOLOCK) ");
			sql.append(" WHERE h.CD_EVENTO = 'X406' ");
			sql.append(" AND h.CD_USUARIO_FECHAMENTO <> 9999  ");
			sql.append(" AND h.DT_VIATURA_DESLOCAMENTO IS NULL  ");
			sql.append(" AND h.CD_CLIENTE = ? ");
			sql.append(" ORDER BY h.DT_RECEBIDO DESC ");*/
			
			sql.append(" SELECT h.CD_CLIENTE,  h.DT_RECEBIDO,  fe.NM_FRASE_EVENTO, h.TX_OBSERVACAO_FECHAMENTO , ");
			sql.append("  CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)) /60 AS TEMPO, h.DT_FECHAMENTO ");      
			sql.append("  FROM HISTORICO_DESARME h WITH (NOLOCK) ");  
			sql.append("  INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK) ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO "); 
			sql.append("  WHERE h.CD_EVENTO = 'X406' ");  
			sql.append("  AND h.CD_USUARIO_FECHAMENTO <> 9999 ");   
			sql.append("  AND h.DT_VIATURA_DESLOCAMENTO IS NULL ");  
			sql.append("  AND h.DT_RECEBIDO >= GETDATE() - 180 ");  
			sql.append("  AND h.CD_CLIENTE = ? ");  
			sql.append("  AND h.TX_OBSERVACAO_FECHAMENTO not like '%#NaoEnviaComunicado%' ");
			sql.append("  ORDER BY  h.CD_CLIENTE, h.DT_RECEBIDO DESC ");  

			st = connVetorh.prepareStatement(sql.toString());

			st.setLong(1, cdCliente);

			rs = st.executeQuery();

			//Situacoes
			while (rs.next())
			{
				GregorianCalendar data = new GregorianCalendar();
				data.setTime(rs.getTimestamp("DT_RECEBIDO"));

				String txtLine = "";

				txtLine = txtLine + "[new Date(";
				txtLine = txtLine + data.get(Calendar.YEAR);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.MONTH);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.DATE);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.HOUR_OF_DAY);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.MINUTE);
				txtLine = txtLine + "),";
				txtLine = txtLine + rs.getLong("TEMPO");
				txtLine = txtLine + "],";

				txtList = txtLine + txtList;
			}

			if (!txtList.equals(""))
			{
				txt = "[" + txtList + "]";
			}

			grafico.setGrafico(GraficosEnum.DESVIOHABITO.getDescricao());
			grafico.setOptions(GraficosEnum.DESVIOHABITO.getOptions());
			grafico.setValor(txt);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, st, rs);

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		return grafico;
	}

	public SiteGraficoVO graficoOrdemServico(Long cdCliente)
	{
		SiteGraficoVO grafico = new SiteGraficoVO();
		Connection connVetorh = PersistEngine.getConnection("SIGMA90");
		String txt = "";
		String txtList = "";
		PreparedStatement st = null;
		ResultSet rs = null;

		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT top 30 os.ID_ORDEM , os.ABERTURA, CONVERT(FLOAT, CASE WHEN DATEDIFF(DAY, ISNULL(os.DATAAGENDADA, CASE DATEPART(DW, os.ABERTURA) WHEN 6 THEN DATEADD(DAY, 3, os.ABERTURA) WHEN 7 THEN DATEADD(DAY, 2, os.ABERTURA) WHEN 1 THEN DATEADD(DAY, 1, os.ABERTURA) ELSE os.ABERTURA END ), os.FECHAMENTO)< 0 THEN 0 ELSE DATEDIFF(DAY, ISNULL(os.DATAAGENDADA, CASE DATEPART(DW, os.ABERTURA) WHEN 6 THEN DATEADD(DAY, 3, os.ABERTURA) WHEN 7 THEN DATEADD(DAY, 2, os.ABERTURA) WHEN 1 THEN DATEADD(DAY, 1, os.ABERTURA) ELSE os.ABERTURA END ), os.FECHAMENTO) END) AS TEMPO ");
			sql.append(" FROM dbORDEM os ");
			sql.append(" INNER JOIN COLABORADOR col ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
			sql.append(" INNER JOIN USUARIO u ON u.CD_USUARIO = os.OPFECHOU ");
			sql.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
			sql.append(" LEFT JOIN MOTIVO_PAUSA mp WITH (NOLOCK) ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA ");
			sql.append(" WHERE EXISTS (SELECT e.ID_ORDEM FROM OSHISTORICO e WHERE e.ID_ORDEM = os.ID_ORDEM) ");
			sql.append(" AND SUBSTRING(col.NM_COLABORADOR, 1, 3) IN ('SOO', 'IAI', 'BQE', 'BNU', 'JLE', 'LGS', 'CUA', 'CCO', 'RSL', 'JGS', 'CTA','TRO') ");
			sql.append(" AND (os.OPFECHOU <> 11010 OR (os.OPFECHOU = 11010 AND p.CD_MOTIVO_PAUSA = 2 )) ");
			sql.append(" AND col.NM_COLABORADOR NOT LIKE '% SUP %' ");
			sql.append(" AND col.NM_COLABORADOR NOT LIKE '% TERC %' AND os.IDOSDEFEITO NOT IN (184,178,134) ");
			sql.append(" AND os.CD_CLIENTE = ? ");
			sql.append(" ORDER BY os.ABERTURA DESC ");

			st = connVetorh.prepareStatement(sql.toString());
			
			st.setLong(1, cdCliente);
			rs = st.executeQuery();
			//Situacoes
			while (rs.next())
			{
				GregorianCalendar data = new GregorianCalendar();
				data.setTime(rs.getTimestamp("ABERTURA"));
				String txtLine = "";
				txtLine = txtLine + "[new Date(";
				txtLine = txtLine + data.get(Calendar.YEAR);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.MONTH);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.DATE);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.HOUR_OF_DAY);
				txtLine = txtLine + ",";
				txtLine = txtLine + data.get(Calendar.MINUTE);
				txtLine = txtLine + "),";
				txtLine = txtLine + rs.getLong("TEMPO");
				txtLine = txtLine + "],";
				txtList = txtLine + txtList;
			}

			if (!txtList.equals(""))
			{
				txt = "[" + txtList + "]";
			}

			grafico.setGrafico(GraficosEnum.ORDEMSERVICO.getDescricao());
			grafico.setOptions(GraficosEnum.ORDEMSERVICO.getOptions());
			grafico.setValor(txt);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, st, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		return grafico;
	}

	@SuppressWarnings("unchecked")
	public List<SiteGraficoOrdemServicoVO> graficoAtendimentoTaticoList(String cdCliente)
	{
		Connection connVetorh = PersistEngine.getConnection("SIGMA90");
		List<SiteGraficoOrdemServicoVO> lista = new ArrayList<SiteGraficoOrdemServicoVO>();
		PreparedStatement st = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try
		{
			sql.append(" SELECT vtr.CD_VIATURA, c.CD_CLIENTE , h.DT_VIATURA_NO_LOCAL,  ");
//			sql.append("  CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO,  ");
//			sql.append("  h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60 AS TEMPO, ");
			
			sql.append(" (       CASE WHEN ((CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60) < 0) ");
			sql.append("         THEN ");
			sql.append("         CONVERT(INT, DATEDIFF(SECOND, h.DT_PROCESSADO, h.DT_VIATURA_NO_LOCAL)) /60 ");
			sql.append("         ELSE ");
			sql.append("         CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60 ");
			sql.append("         END) AS TEMPO, ");
			
			sql.append("  fe.NM_FRASE_EVENTO, vtr.NM_VIATURA, h.TX_OBSERVACAO_FECHAMENTO, h.DT_RECEBIDO  ");
			sql.append("  FROM VIEW_HISTORICO h WITH (NOLOCK)   ");
			sql.append("  INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE   ");
			sql.append("  INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA   ");
			sql.append("  INNER JOIN USUARIO u WITH (NOLOCK) ON u.CD_USUARIO = h.CD_USUARIO_VIATURA_NO_LOCAL   ");
			sql.append("  INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = u.CD_COLABORADOR   ");
			sql.append("  INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
			sql.append("  INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK) ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO ");
			sql.append("  WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL   ");
			sql.append("  AND h.DT_VIATURA_NO_LOCAL IS NOT NULL   ");
			sql.append("  AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0   ");
			sql.append("  AND cod.TIPO = 1 AND h.CD_CODE <> 'EFM' AND h.CD_CODE <> 'X1A' AND c.CD_CLIENTE IN ( " + cdCliente + " )   ");
			sql.append("  AND h.DT_VIATURA_NO_LOCAL >= GETDATE() - 180   ");
			sql.append("  AND (DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) < 3000   ");
			sql.append("  ORDER BY c.CD_CLIENTE , DT_VIATURA_NO_LOCAL DESC ");

			st = connVetorh.prepareStatement(sql.toString());

			//st.setLong(1, cdCliente);

			rs = st.executeQuery();
			SiteGraficoOrdemServicoVO servicoVO = null;
			//Situacoes
			int cont = 0;
			InstantiableEntityInfo ait = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("AITEmailAutomaticoAIT");

			while (rs.next() && cont < 30)
			{
				StringBuilder builder = new StringBuilder();
				NeoObject colaborador = PersistEngine.getObject(ait.getEntityClass(), new QLEqualsFilter("matricula", rs.getString("CD_VIATURA")));

				servicoVO = new SiteGraficoOrdemServicoVO();
				String foto = "";
				if (colaborador != null)
				{

					EntityWrapper wpneo = new EntityWrapper(colaborador);
					if (wpneo.getValue("fotoTec") != null)
					{
						foto = " <img style=\"width:100px;height:120px\" src=\"http://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId() + "\" border=\"0\" alt=\"Técnico Responsável\"></div>";
						servicoVO.setFoto(foto);

					}
				}
				else
				{
					servicoVO.setFoto("");
				}

				//servicoVO.setCdViatura(Long.parseLong(rs.getString("CD_VIATURA")));
				servicoVO.setNmFraseEvento(rs.getString("NM_FRASE_EVENTO"));
				servicoVO.setNmViatura(rs.getString("NM_VIATURA"));
				servicoVO.setObservacao(rs.getString("TX_OBSERVACAO_FECHAMENTO"));

				servicoVO.setCdCliente(rs.getLong("CD_CLIENTE"));
				GregorianCalendar data = new GregorianCalendar();
				data.setTime(rs.getTimestamp("DT_VIATURA_NO_LOCAL"));
				servicoVO.setDataInicial(data);

				GregorianCalendar dataFinal = new GregorianCalendar();
				dataFinal.setTime(rs.getTimestamp("DT_RECEBIDO"));
				servicoVO.setData(dataFinal);

				servicoVO.setTempo(rs.getLong("TEMPO"));

				builder.append("<div style=\"padding:5px 5px 5px 5px;font-family: Calibri,verdana,helvetica,arial,sans-serif;\">");
				builder.append("<table>");
				builder.append("<tbody>");
				builder.append("<tr>");
				builder.append("<td  style=\"width:120px;height:130px\" rowspan=\"5\" align=\"center\">");
				builder.append(foto);
				builder.append("</td>");
				builder.append("<td>");
				builder.append("<strong>Evento: </strong>" + rs.getString("NM_FRASE_EVENTO"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Ocorrido em: </strong>" + NeoDateUtils.safeDateFormat(rs.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:ss"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>No local em: </strong>" + NeoDateUtils.safeDateFormat(rs.getTimestamp("DT_VIATURA_NO_LOCAL"), "dd/MM/yyyy HH:mm:ss"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Tempo: </strong>" + rs.getLong("TEMPO") + " min");
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Atendido por: </strong>" + rs.getString("NM_VIATURA"));
				builder.append("</td>");
				builder.append("</tr>");
				if (rs.getString("TX_OBSERVACAO_FECHAMENTO") != null && !rs.getString("TX_OBSERVACAO_FECHAMENTO").isEmpty())
				{
					builder.append("<tr>");
					builder.append("<td colspan=\"2\">");
					builder.append("<strong>Informação: </strong><br/>" + NeoUtils.encodeHTMLValue(rs.getString("TX_OBSERVACAO_FECHAMENTO").replaceAll("#", "")));
					builder.append("</td>");
					builder.append("</tr>");
				}
				builder.append("</tbody>");
				builder.append("</table>");
				builder.append("</div>");

				servicoVO.setToolTipHtml(builder.toString());

				lista.add(servicoVO);
				cont++;
			}

		}
		catch (Exception e)
		{
			System.out.println("[AREACLIENTE] - graficoAtendimentoTaticoList erro -> sql:" + sql.toString());
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, st, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		return lista;
	}

	public List<SiteGraficoOrdemServicoVO> graficoDesvioHabitoList(String cdCliente)
	{
		SiteGraficoVO grafico = new SiteGraficoVO();
		Connection connVetorh = PersistEngine.getConnection("SIGMA90");
		String txt = "";
		String txtList = "";
		List<SiteGraficoOrdemServicoVO> lista = new ArrayList<SiteGraficoOrdemServicoVO>();
		PreparedStatement st = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try
		{
			sql.append(" SELECT h.CD_CLIENTE,  h.DT_RECEBIDO,  fe.NM_FRASE_EVENTO, h.TX_OBSERVACAO_FECHAMENTO ,");
			sql.append(" CONVERT(INT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)) /60 AS TEMPO, h.DT_FECHAMENTO      ");
			sql.append(" FROM HISTORICO_DESARME h WITH (NOLOCK)  ");
			sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK) ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO ");
			sql.append(" WHERE h.CD_EVENTO = 'X406'  ");
			sql.append(" AND h.CD_USUARIO_FECHAMENTO <> 9999   ");
			sql.append(" AND h.DT_VIATURA_DESLOCAMENTO IS NULL  ");
			sql.append(" AND h.DT_RECEBIDO >= GETDATE() - 180  ");
			sql.append(" AND h.CD_CLIENTE IN ( " + cdCliente + ")  ");
			sql.append(" AND h.TX_OBSERVACAO_FECHAMENTO not like '%#NaoEnviaComunicado%' "); // resolve o problema dos x406 não atendidos e que não devem aparecer
			sql.append(" ORDER BY  h.CD_CLIENTE, h.DT_RECEBIDO DESC  ");

			st = connVetorh.prepareStatement(sql.toString());

			//st.setLong(1, cdCliente);

			rs = st.executeQuery();
			SiteGraficoOrdemServicoVO servicoVO = null;
			//Situacoes
			int cont = 0;
			while (rs.next() && cont < 30)
			{
				StringBuilder builder = new StringBuilder();
				servicoVO = new SiteGraficoOrdemServicoVO();

				//servicoVO.setCdViatura(Long.parseLong(rs.getString("CD_VIATURA")));
				servicoVO.setNmFraseEvento(rs.getString("NM_FRASE_EVENTO"));
				servicoVO.setObservacao(rs.getString("TX_OBSERVACAO_FECHAMENTO"));

				servicoVO.setCdCliente(rs.getLong("CD_CLIENTE"));
				GregorianCalendar data = new GregorianCalendar();
				data.setTime(rs.getTimestamp("DT_RECEBIDO"));
				servicoVO.setDataInicial(data);

				GregorianCalendar dataFinal = new GregorianCalendar();
				dataFinal.setTime(rs.getTimestamp("DT_FECHAMENTO"));
				servicoVO.setData(dataFinal);

				servicoVO.setTempo(rs.getLong("TEMPO"));
				builder.append(" <div style=\"padding:5px 5px 5px 5px;font-family: Calibri,verdana,helvetica,arial,sans-serif;\">");
				builder.append("<table>");
				builder.append("<tbody>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Evento: </strong>" + rs.getString("NM_FRASE_EVENTO"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Ocorrido em: </strong>" + NeoDateUtils.safeDateFormat(rs.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:ss"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Encerrado em: </strong>" + NeoDateUtils.safeDateFormat(rs.getTimestamp("DT_FECHAMENTO"), "dd/MM/yyyy HH:mm:ss"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Tempo: </strong>" + rs.getLong("TEMPO") + " min");
				builder.append("</td>");
				builder.append("</tr>");
				if (rs.getString("TX_OBSERVACAO_FECHAMENTO") != null && !rs.getString("TX_OBSERVACAO_FECHAMENTO").isEmpty())
				{
					builder.append("<tr>");
					builder.append("<td>");
					builder.append("<strong>Informação: </strong><br/>" + NeoUtils.encodeHTMLValue(rs.getString("TX_OBSERVACAO_FECHAMENTO").replaceAll("#", "")));
					builder.append("</td>");
					builder.append("</tr>");
				}
				builder.append("</tbody>");
				builder.append("</table>");
				builder.append("</div>");

				servicoVO.setToolTipHtml(builder.toString());

				lista.add(servicoVO);
				cont++;

			}

		}
		catch (Exception e)
		{
			System.out.println("[AREACLIENTE] - graficoAtendimentoTaticoList erro -> sql:" + sql.toString());
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, st, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		return lista;
	}

	public List<SiteGraficoOrdemServicoVO> graficoOrdemServicoList(String cdCliente)
	{
		Connection connVetorh = PersistEngine.getConnection("SIGMA90");
		List<SiteGraficoOrdemServicoVO> lista = new ArrayList<SiteGraficoOrdemServicoVO>();
		PreparedStatement st = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try
		{

			sql.append(" SELECT os.CD_CLIENTE, os.ID_ORDEM , os.ABERTURA,  ");
			sql.append(" CONVERT(FLOAT, CASE WHEN DATEDIFF(DAY, ISNULL(os.DATAAGENDADA, CASE DATEPART(DW, os.ABERTURA)  ");
			sql.append(" WHEN 6 THEN DATEADD(DAY, 3, os.ABERTURA) WHEN 7 THEN DATEADD(DAY, 2, os.ABERTURA)  ");
			sql.append(" WHEN 1 THEN DATEADD(DAY, 1, os.ABERTURA) ELSE os.ABERTURA END ), os.FECHAMENTO)< 0 THEN 0 ELSE  ");
			sql.append(" DATEDIFF(DAY, ISNULL(os.DATAAGENDADA, CASE DATEPART(DW, os.ABERTURA) WHEN 6 THEN DATEADD(DAY, 3, os.ABERTURA)");
			sql.append(" WHEN 7 THEN DATEADD(DAY, 2, os.ABERTURA) WHEN 1 THEN DATEADD(DAY, 1, os.ABERTURA) ELSE os.ABERTURA END ), os.FECHAMENTO) END) AS TEMPO ");
			sql.append(" ,col.CD_COLABORADOR , def.DESCRICAODEFEITO,col.NM_COLABORADOR,os.DEFEITO ,os.FECHAMENTO ");
			sql.append(" FROM dbORDEM os   ");
			sql.append(" INNER JOIN COLABORADOR col ON col.CD_COLABORADOR = os.ID_INSTALADOR  ");
			sql.append(" INNER JOIN USUARIO u ON u.CD_USUARIO = os.OPFECHOU  ");
			sql.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO ");
			sql.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO )  ");
			sql.append(" LEFT JOIN MOTIVO_PAUSA mp WITH (NOLOCK) ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA ");
			sql.append(" WHERE EXISTS (SELECT e.ID_ORDEM FROM OSHISTORICO e WHERE e.ID_ORDEM = os.ID_ORDEM) ");
			sql.append(" AND SUBSTRING(col.NM_COLABORADOR, 1, 3) IN ('SOO', 'IAI', 'BQE', 'BNU', 'JLE', 'LGS', 'CUA', 'CCO', 'RSL', 'JGS', 'CTA', 'TRO')  ");
			sql.append(" AND (os.OPFECHOU <> 11010 OR (os.OPFECHOU = 11010 AND p.CD_MOTIVO_PAUSA = 2 ))  ");
			sql.append(" AND col.NM_COLABORADOR NOT LIKE '% SUP %'  ");
			sql.append(" AND col.NM_COLABORADOR NOT LIKE '% TERC %' AND os.IDOSDEFEITO NOT IN (184,178,134) ");
			sql.append(" AND os.CD_CLIENTE IN ( " + cdCliente.toString() + " )  ");
			sql.append(" AND os.ABERTURA >= getdate() - 180 ");
			sql.append(" ORDER BY os.CD_CLIENTE,os.ABERTURA DESC  ");

			st = connVetorh.prepareStatement(sql.toString());

			//st.setNString(1, cdCliente);

			rs = st.executeQuery();

			SiteGraficoOrdemServicoVO servicoVO = null;
			int cont = 0;
			//Situacoes
			while (rs.next() && cont < 30)
			{
				StringBuilder builder = new StringBuilder();
				servicoVO = new SiteGraficoOrdemServicoVO();

				servicoVO.setCdCliente(rs.getLong("CD_CLIENTE"));
				GregorianCalendar data = new GregorianCalendar();
				data.setTime(rs.getTimestamp("ABERTURA"));
				servicoVO.setData(data);

				GregorianCalendar dataFechamento = new GregorianCalendar();
				dataFechamento.setTime(rs.getTimestamp("FECHAMENTO"));
				servicoVO.setData(dataFechamento);

				servicoVO.setTempo(rs.getLong("TEMPO"));
				servicoVO.setNmViatura(rs.getString("NM_COLABORADOR"));
				InstantiableEntityInfo infoTec = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("OSEmailAutomaticoTecnicos");
				List<NeoObject> listaEmails = PersistEngine.getObjects(infoTec.getEntityClass(), new QLEqualsFilter("codigo", rs.getString("CD_COLABORADOR")), -1, -1);
				String foto = "";
				if (listaEmails != null && !listaEmails.isEmpty())
				{
					EntityWrapper wpneo = new EntityWrapper(listaEmails.get(0));
					foto = " <img  style=\"width:100px;height:120px\" src=\"http://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId() + "\" border=\"0\" alt=\"Técnico Responsável\"></div>";

				}

				builder.append(" <div style=\"padding:5px 5px 5px 5px;font-family: Calibri,verdana,helvetica,arial,sans-serif;\">");
				builder.append("<table>");
				builder.append("<tbody>");
				builder.append("<tr>");
				builder.append("<td  style=\"width:120px;height:130px\" rowspan=\"5\" align=\"center\">");
				builder.append(foto);
				builder.append("</td>");
				builder.append("<td>");
				builder.append("<strong>Ordem de Serviço: </strong>" + rs.getString("DESCRICAODEFEITO"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Aberta em: </strong>" + NeoDateUtils.safeDateFormat(rs.getTimestamp("ABERTURA"), "dd/MM/yyyy HH:mm:ss"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Atendida em: </strong>" + NeoDateUtils.safeDateFormat(rs.getTimestamp("FECHAMENTO"), "dd/MM/yyyy HH:mm:ss"));
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Tempo: </strong>" + rs.getLong("TEMPO") + " dia(s)");
				builder.append("</td>");
				builder.append("</tr>");
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("<strong>Atendida por: </strong>" + rs.getString("NM_COLABORADOR"));
				builder.append("</td>");
				builder.append("</tr>");
				if (rs.getString("DEFEITO") != null && !rs.getString("DEFEITO").isEmpty())
				{
					builder.append("<tr>");
					builder.append("<td colspan=\"2\">");
					builder.append("<strong>Descrição: </strong><br/>" + NeoUtils.encodeHTMLValue(rs.getString("DEFEITO").replaceAll("#", "")));
					builder.append("</td>");
					builder.append("</tr>");
				}
				builder.append("</tbody>");
				builder.append("</table>");
				builder.append("</div>");

				servicoVO.setToolTipHtml(builder.toString());

				lista.add(servicoVO);
				cont++;

			}

		}
		catch (Exception e)
		{
			System.out.println("[AREACLIENTE] - graficoAtendimentoTaticoList erro -> sql:" + sql.toString());
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, st, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		return lista;
	}

	public List<String> listarAreaDeInteresse()
	{

		List<NeoObject> cvai = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CVAreaInteresse"));
		List<String> areas = new ArrayList<String>();
		for (NeoObject area : cvai)
		{
			EntityWrapper wrapper = new EntityWrapper((NeoObject) area);
			String descricao = (String) wrapper.getValue("descricao");
			areas.add(descricao);
		}

		return areas;
	}

	@SuppressWarnings("unchecked")
	public List<String> listarAreas(String area)
	{

		List<NeoObject> cvsai = (List<NeoObject>) PersistEngine.getObject(AdapterUtils.getEntityClass("CVSubAreaInteresse"), area);
		List<String> listaSubAreas = new ArrayList<String>();
		for (NeoObject subArea : cvsai)
		{
			EntityWrapper wrapper = new EntityWrapper((NeoObject) subArea);
			String descricao = (String) wrapper.getValue("descricao");
			listaSubAreas.add(descricao);

		}
		return listaSubAreas;
	}

	public List<String> listarSexo()
	{

		List<NeoObject> sexos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCSexo"));
		List<String> listaSexos = new ArrayList<String>();
		for (NeoObject sexo : sexos)
		{
			EntityWrapper wrapper = new EntityWrapper((NeoObject) sexo);
			String descricao = (String) wrapper.getValue("descricao");
			listaSexos.add(descricao);
		}
		return listaSexos;
	}

	public List<String> listarEstadoCivil()
	{

		List<NeoObject> listaEstadoCivil = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("UTILestadoCivil"));
		List<String> listaEstado = new ArrayList<String>();
		for (NeoObject civil : listaEstadoCivil)
		{
			EntityWrapper wrapper = new EntityWrapper((NeoObject) civil);
			String descricao = (String) wrapper.getValue("descricao");
			listaEstado.add(descricao);
		}
		return listaEstado;
	}

	public List<String> listarTurno()
	{

		List<NeoObject> listaTurnos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CVturnoDesejado"));
		List<String> listaTurno = new ArrayList<String>();
		for (NeoObject turno : listaTurnos)
		{
			EntityWrapper wrapper = new EntityWrapper((NeoObject) turno);
			String descricao = (String) wrapper.getValue("descricao");
			listaTurno.add(descricao);
		}
		return listaTurno;
	}

	public List<String> listarOrigem()
	{

		List<NeoObject> listarComoConheceu = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("UTILorigem"));
		List<String> listaComoConheceu = new ArrayList<String>();
		for (NeoObject comoConheceu : listarComoConheceu)
		{
			EntityWrapper wrapper = new EntityWrapper((NeoObject) comoConheceu);
			String descricao = (String) wrapper.getValue("descricao");
			listaComoConheceu.add(descricao);
		}
		return listaComoConheceu;
	}

	public void criarArquivo(String diretorioStr, String pastas, String nomeArquivo, String concat) throws IOException
	{
		File diretorio = new File("\\\\fsoofs01\\f$\\Site\\Orsegups" + File.separator + pastas + File.separator);
		File arquivo = new File(diretorio, nomeArquivo.concat(".pdf"));
		FileWriter fw = null;
		BufferedWriter bw = null;
		try
		{
			if (!diretorio.mkdir())
			{
				diretorio.mkdir();
			}
			if (!arquivo.exists())
			{
				arquivo.createNewFile();
			}
			
			fw = new FileWriter(arquivo, true);

			bw = new BufferedWriter(fw);

			bw.write(concat);

			bw.newLine();

		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if (NeoUtils.safeIsNotNull(bw))
				bw.close();
			if (NeoUtils.safeIsNotNull(fw))
				fw.close();
		}
	}
	@SuppressWarnings("unchecked")
	public static List<SolicitacaoVO> listaSolicitacoes(Long codcli)
	{
		List<SolicitacaoVO> solicitacoes = new ArrayList<SolicitacaoVO>();

		try
		{
			QLEqualsFilter filterCodigo = new QLEqualsFilter("codcli", codcli);
			NeoObject cliente = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCodigo);

			if (NeoUtils.safeIsNotNull(cliente))
			{
				QLEqualsFilter filterCliente = new QLEqualsFilter("clienteSapiens", cliente);

				GregorianCalendar dataLimite = new GregorianCalendar();
				//Todo voltar para seis meses e nao seis anos
				dataLimite.add(GregorianCalendar.MONTH, -6);
				QLOpFilter qlop = new QLOpFilter("wfprocess.startDate", ">=", (GregorianCalendar) dataLimite);

				//QLRawFilter processStateFilter = new QLRawFilter("wfprocess.processState not in (" + ProcessState.canceled.ordinal() + "," + ProcessState.removed.ordinal() + ") ");

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(filterCliente);
				groupFilter.addFilter(qlop);

				List<NeoObject> rscs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoCliente"), groupFilter, -1, -1, "wfprocess.startDate desc");
				List<NeoObject> rscsNovas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoClienteNovo"), groupFilter, -1, -1, "wfprocess.startDate desc");

				
				if (NeoUtils.safeIsNotNull(rscsNovas))
				{
					for (NeoObject rsc : rscsNovas)
					{
						EntityWrapper rscWrapper = new EntityWrapper(rsc);
						String origem = NeoUtils.safeOutputString(rscWrapper.findValue("origem.sequencia"));

						WFProcess process = (WFProcess) rscWrapper.findValue("wfprocess");

						if (NeoUtils.safeIsNull(process) || process.getProcessState() == ProcessState.canceled || !process.isSaved())
						{
							continue;
						}

						List<Task> tasks = (List<Task>) process.getAllPendingTaskList();

						//Verifica se possui Task pendente
						if (NeoUtils.safeIsNotNull(tasks) && !tasks.isEmpty())
						{
							//Verifica se possui apenas uma task e se a mesma esta em execucao, neste caso nao deve mostrar na listagem
							//Teste para mostrar RSC no site
							if (process.getAllPendingTaskList().size() == 0 && !origem.equals("05"))
							{
								continue;
							}

						}
						else
						{
							//Verifica se nao possui Task pendente e se o processo esta com status em execucao 
							if (process.getProcessState().ordinal() == ProcessState.running.ordinal() && !process.getAllPendingTaskList().isEmpty())
							{
								continue;
							}
						}

						NeoUser obj = null;
						
						NeoPaper paper = (NeoPaper) rscWrapper.findValue("responsavelExecutor");
						
						if (paper != null && paper.getAllUsers() != null && !paper.getAllUsers().isEmpty()){
							for (NeoUser u : paper.getAllUsers()){
								obj = u;
								break;
							}
						}

						String codigoTarefa = (String) process.getCode();						
						String solicitante = "";
						if(process.getRequester() != null){
							solicitante = (String) process.getRequester().getFullName();
						}
						String categoria = (String) rscWrapper.findValue("origem.descricao");
						String descricao = (String) rscWrapper.findValue("descricao");
						String atendente = NeoUtils.safeOutputString(obj != null ? obj.getFullName() : "");
						String contato = (String) rscWrapper.findValue("contato");
						GregorianCalendar dataInicio = (GregorianCalendar) process.getStartDate();
						GregorianCalendar dataConclusao = (GregorianCalendar) process.getFinishDate();
						ProcessState processState = process.getProcessState();

						SolicitacaoVO solicitacao = new SolicitacaoVO();
						//solicitacao.setNeoid(neoid);
						solicitacao.setCodigoTarefa(codigoTarefa);
						solicitacao.setCategoria(categoria);
						solicitacao.setSolicitante(solicitante);
						solicitacao.setDescricao(descricao);
						solicitacao.setAtendente(atendente);
						solicitacao.setContato(contato);
						solicitacao.setSituacao(I18nUtils.getString(processState.name()));
						//solicitacao.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showSolicitacao(" + solicitacao.getNeoid().toString() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>");

						if (NeoUtils.safeIsNotNull(dataInicio))
						{
							solicitacao.setDataInicio(NeoDateUtils.safeDateFormat(dataInicio));
						}

						if (NeoUtils.safeIsNotNull(dataConclusao))
						{
							solicitacao.setDataConclusao(NeoDateUtils.safeDateFormat(dataConclusao));
						}
						
						List<RegistroAtividadeRSCVO> registroAtividade = new ArrayList<RegistroAtividadeRSCVO>();
						List<NeoObject> objsRegAtividade = (List<NeoObject>) rscWrapper.getValue("registroAtividades");
						
						for(NeoObject objRegAtividade : objsRegAtividade){
							EntityWrapper wRegAtividade = new EntityWrapper(objRegAtividade);
							RegistroAtividadeRSCVO registroAtividadeRSCVO = new RegistroAtividadeRSCVO();
							String nomeAtividade = String.valueOf(wRegAtividade.getValue("atividade"));
							String descricaoAtv =  String.valueOf(wRegAtividade.getValue("descricao"));
							GregorianCalendar dataInicialRSC = (GregorianCalendar) wRegAtividade.getValue("dataInicialRSC");
							GregorianCalendar dataFinalRSC = (GregorianCalendar) wRegAtividade.getValue("dataFinalRSC");
							GregorianCalendar prazo = (GregorianCalendar) wRegAtividade.getValue("prazo");
							NeoUser responsavel = (NeoUser) wRegAtividade.getValue("responsavel");
							Long neoId = (Long) wRegAtividade.getValue("neoId");
							
							if (NeoUtils.safeIsNotNull(dataFinalRSC))
							{
								registroAtividadeRSCVO.setDataFinalString(NeoDateUtils.safeDateFormat(dataFinalRSC));
							}
							
							if (NeoUtils.safeIsNotNull(dataFinalRSC))
							{
								registroAtividadeRSCVO.setDataInicialString(NeoDateUtils.safeDateFormat(dataInicialRSC));
							}
							
							registroAtividadeRSCVO.setDataFinalRSC(dataFinalRSC);
							registroAtividadeRSCVO.setDataInicialRSC(dataInicialRSC);
							registroAtividadeRSCVO.setDescricao(descricaoAtv);
							registroAtividadeRSCVO.setNeoId(neoId);
							registroAtividadeRSCVO.setNomeAtividade(nomeAtividade);
							registroAtividadeRSCVO.setPrazo(prazo);
							registroAtividadeRSCVO.setResponsavel(responsavel.getFullName());
							
							registroAtividade.add(registroAtividadeRSCVO);
						}

						if(objsRegAtividade.size() > 0){
							solicitacao.setRegistroAtividades(registroAtividade);
						}
						
						solicitacoes.add(solicitacao);
					}
				}
				
				//TODO RSC - Remover este bloco após 01/05/2017
				if (NeoUtils.safeIsNotNull(rscs))
				{
					for (NeoObject rsc : rscs)
					{
						EntityWrapper rscWrapper = new EntityWrapper(rsc);
						String origem = NeoUtils.safeOutputString(rscWrapper.findValue("origem.sequencia"));

						WFProcess process = (WFProcess) rscWrapper.findValue("wfprocess");

						if (NeoUtils.safeIsNull(process) || process.getProcessState() == ProcessState.canceled || !process.isSaved())
						{
							continue;
						}

						List<Task> tasks = (List<Task>) process.getAllPendingTaskList();

						//Verifica se possui Task pendente
						if (NeoUtils.safeIsNotNull(tasks) && !tasks.isEmpty())
						{
							//Verifica se possui apenas uma task e se a mesma esta em execucao, neste caso nao deve mostrar na listagem
							//Teste para mostrar RSC no site
							if (process.getAllPendingTaskList().size() == 0 && !origem.equals("05"))
							{
								continue;
							}

						}
						else
						{
							//Verifica se nao possui Task pendente e se o processo esta com status em execucao 
							if (process.getProcessState().ordinal() == ProcessState.running.ordinal() && !process.getAllPendingTaskList().isEmpty())
							{
								continue;
							}
						}

						NeoUser obj = (NeoUser) rscWrapper.findValue("responsavelAtendimento");
						String codigoTarefa = (String) process.getCode();
						String solicitante = "";
						if(process.getRequester() != null){
							solicitante = (String) process.getRequester().getFullName();	
						}else{
							solicitante = "";
						}
						String categoria = (String) rscWrapper.findValue("categoriasRsc.descricao");
						String descricao = (String) rscWrapper.findValue("descricao");
						String atendente = NeoUtils.safeOutputString(obj != null ? obj.getFullName() : "");
						String contato = (String) rscWrapper.findValue("contato");
						GregorianCalendar dataInicio = (GregorianCalendar) process.getStartDate();
						GregorianCalendar dataConclusao = (GregorianCalendar) process.getFinishDate();
						ProcessState processState = process.getProcessState();

						SolicitacaoVO solicitacao = new SolicitacaoVO();
						//solicitacao.setNeoid(neoid);
						solicitacao.setCodigoTarefa(codigoTarefa);
						solicitacao.setCategoria(categoria);
						solicitacao.setSolicitante(solicitante);
						solicitacao.setDescricao(descricao);
						solicitacao.setAtendente(atendente);
						solicitacao.setContato(contato);
						solicitacao.setSituacao(I18nUtils.getString(processState.name()));
						//solicitacao.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showSolicitacao(" + solicitacao.getNeoid().toString() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>");

						if (NeoUtils.safeIsNotNull(dataInicio))
						{
							solicitacao.setDataInicio(NeoDateUtils.safeDateFormat(dataInicio));
						}

						if (NeoUtils.safeIsNotNull(dataConclusao))
						{
							solicitacao.setDataConclusao(NeoDateUtils.safeDateFormat(dataConclusao));
						}

						solicitacoes.add(solicitacao);
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return solicitacoes;
	}
	@SuppressWarnings("unchecked")
	public static List<ReclamacaoVO> listaReclamacao(Long codcli)
	{
		List<ReclamacaoVO> solicitacoes = new ArrayList<ReclamacaoVO>();

		try
		{
			QLEqualsFilter filterCodigo = new QLEqualsFilter("codcli", codcli);
			NeoObject cliente = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCodigo);

			if (NeoUtils.safeIsNotNull(cliente))
			{

				GregorianCalendar dataLimite = new GregorianCalendar();
				dataLimite.add(GregorianCalendar.MONTH, -6);
				QLOpFilter qlop = new QLOpFilter("wfprocess.startDate", ">=", (GregorianCalendar) dataLimite);

				QLEqualsFilter filterCliente = new QLEqualsFilter("clienteSapiens", cliente);
				//QLRawFilter processStateFilter = new QLRawFilter("wfprocess.processState not in (" + ProcessState.canceled.ordinal() + "," + ProcessState.removed.ordinal() + ") ");

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(filterCliente);
				groupFilter.addFilter(qlop);

				List<NeoObject> rrcs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RRCRelatorioReclamacaoCliente"), groupFilter, -1, -1, "wfprocess.startDate desc");

				if (NeoUtils.safeIsNotNull(rrcs))
				{
					for (NeoObject rrc : rrcs)
					{
						EntityWrapper rscWrapper = new EntityWrapper(rrc);
						WFProcess process = (WFProcess) rscWrapper.findValue("wfprocess");

						if (NeoUtils.safeIsNull(process) || process.getProcessState() == ProcessState.canceled)
						{
							continue;
						}

						List<Task> tasks = (List<Task>) process.getAllPendingTaskList();

						//Verifica se possui Task pendente
						if (NeoUtils.safeIsNotNull(tasks) && !tasks.isEmpty())
						{
							//Verifica se possui apenas uma task e se a mesma esta em execucao, neste caso nao deve mostrar na listagem
							if (process.getAllPendingTaskList().size() == 0)
							{
								continue;
							}
						}
						else
						{
							//Verifica se nao possui Task pendente e se o processo esta com status em execucao 
							if (process.getProcessState().ordinal() == ProcessState.running.ordinal() && !process.getAllPendingTaskList().isEmpty())
							{
								continue;
							}
						}

						String codigoTarefa = (String) rscWrapper.findValue("wfprocess.code");
						String solicitante = (String) rscWrapper.findValue("wfprocess.requester.fullName");
						String descricao = (String) rscWrapper.findValue("textoReclamacao");
						String origem = (String) rscWrapper.findValue("origem.descricao");
						String contato = (String) rscWrapper.findValue("contato");
						//String atendente = (String) rscWrapper.findValue("responsAcao01.fullName");
						GregorianCalendar dataInicio = (GregorianCalendar) rscWrapper.findValue("wfprocess.startDate");
						GregorianCalendar dataConclusao = (GregorianCalendar) rscWrapper.findValue("wfprocess.finishDate");
						ProcessState processState = (ProcessState) rscWrapper.findValue("wfprocess.processState");

						ReclamacaoVO reclamacao = new ReclamacaoVO();
						//reclamacao.setNeoid(neoid);
						reclamacao.setCodigoTarefa(codigoTarefa);
						reclamacao.setSolicitante(solicitante);
						reclamacao.setSituacao(I18nUtils.getString(processState.name()));
						reclamacao.setTextoReclamacao(descricao);
						reclamacao.setOrigem(origem);
						reclamacao.setContato(contato);
						//reclamacao.setAtendente(atendente);
						//reclamacao.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showSolicitacao(" + reclamacao.getNeoid().toString() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>");

						if (NeoUtils.safeIsNotNull(dataInicio))
						{
							reclamacao.setDataInicio(NeoDateUtils.safeDateFormat(dataInicio));
						}

						if (NeoUtils.safeIsNotNull(dataConclusao))
						{
							reclamacao.setDataConclusao(NeoDateUtils.safeDateFormat(dataConclusao));
						}
						solicitacoes.add(reclamacao);
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return solicitacoes;
	}

	/**
	 * Retorna os dados que vão aparecer na tabela sobre as marcações de ponto do site.
	 * 
	 * @param numcad
	 * @param numemp
	 * @param tipcol
	 * @return
	 */
	public static JSONObject retornaCabecalhoCartaoPonto(Long numcad, Long numemp, long tipcol)
	{
		JSONObject objRetorno = null;
		StringBuilder sql = new StringBuilder();

		sql.append(" select fun.numcad, fun.nomfun, (cast(fun.numctp as varchar)+' '+ cast(fun.serctp as varchar)) numctp, car.TitCar, fun.datadm, fil.razsoc, fil.numcgc, fil.codfil, fil.nomFil  ");
		sql.append(" from r034fun fun ");
		sql.append(" INNER JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= DATEADD(dd, 0, DATEDIFF(dd, 0, getdate())) ) ");
		sql.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");
		//sql.append(" INNER JOIN R044CAL cal WITH (NOLOCK) ON ( DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) between iniapu and fimapu and cal.numemp = fun.numemp and tipcal = 11) ");
		sql.append(" inner join R030FIL fil WITH (NOLOCK) ON ( fun.numemp = fil.numemp and fun.codfil = fil.codfil) ");
		sql.append(" where fun.numcad = ? and fun.numemp = ? and fun.tipcol = ? ");

		Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
		ResultSet rs = null;
		try
		{

			PreparedStatement pst = connection.prepareStatement(sql.toString());

			//pst.setTimestamp(1, new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			pst.setLong(1, numcad);
			pst.setLong(2, numemp);
			pst.setLong(3, tipcol);

			rs = pst.executeQuery();

			if (rs.next())
			{

				objRetorno = new JSONObject();
				objRetorno.put("numcad", rs.getString("numcad"));
				objRetorno.put("nomfun", rs.getString("nomfun"));
				objRetorno.put("numctp", rs.getString("numctp"));
				objRetorno.put("TitCar", rs.getString("TitCar"));

				GregorianCalendar gdatadm = new GregorianCalendar();
				gdatadm.setTimeInMillis(rs.getDate("datadm").getTime());
				objRetorno.put("datadm", NeoDateUtils.safeDateFormat(gdatadm, "dd/MM/yyyy"));

				GregorianCalendar giniapu = new GregorianCalendar();
				giniapu.set(GregorianCalendar.DATE, 1);
				objRetorno.put("iniapu", NeoDateUtils.safeDateFormat(giniapu, "dd/MM/yyyy"));

				GregorianCalendar gfimapu = new GregorianCalendar();
				gfimapu.add(GregorianCalendar.MONTH, 1);
				gfimapu.set(GregorianCalendar.DATE, 1);
				gfimapu.add(GregorianCalendar.DATE, -1);
				objRetorno.put("fimapu", NeoDateUtils.safeDateFormat(gfimapu, "dd/MM/yyyy"));

				GregorianCalendar gperref = new GregorianCalendar();
				objRetorno.put("perref", NeoDateUtils.safeDateFormat(gperref, "MM/yyyy"));

				objRetorno.put("razsoc", rs.getString("razsoc"));
				objRetorno.put("numcgc", rs.getLong("numcgc"));
				objRetorno.put("codfil", rs.getLong("codfil"));
				objRetorno.put("nomFil", rs.getString("nomFil"));

			}
			rs.close();
			connection.close();
			return objRetorno;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}

	}

	public static Date retornaUltimaDataApurada(Long numcad, Long numemp, long tipcol)
	{
		Date retorno = null;

		StringBuilder sql = new StringBuilder();

		sql.append("select (case when  DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) = datApu then datApu-1 else datApu  end) datApu ");
		sql.append("from ( ");
		sql.append(" 	select max(datApu) datApu from r066sit where numcad = ? and numemp = ? and tipcol = ? ");
		sql.append(" ) as x");

		Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
		ResultSet rs = null;
		try
		{
			//System.out.println(sql.toString());
			PreparedStatement pst = connection.prepareStatement(sql.toString());

			pst.setLong(1, numcad);
			pst.setLong(2, numemp);
			pst.setLong(3, tipcol);

			rs = pst.executeQuery();

			if (rs.next())
			{
				retorno = rs.getDate("datApu");
			}
			rs.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return retorno;
	}

	/**
	 * Retorna saldo do banco de horas para um colaborador com base no mes de referencia
	 * 
	 * @param numcad - numero de matricula
	 * @param numemp - codigo da empresa
	 * @param tipcol - tipo do colaborador
	 * @param mesRef - mês de referencia
	 * @param offset - indica quantos dias para traz ou frente devem ser considerados para a soma do
	 *            saldo. <br>
	 * @param consideraSaldoAnterior - indica se deve ser debitado/creditado saldo do mês anterior ao mes
	 *            escolhido no retorno do saldo.
	 * @return quantidade de horas de saldo em minutos.
	 */
	public static HorasVO retornaSaldoHoras(Long numcad, Long numemp, long tipcol, GregorianCalendar mesRef, Integer offset, boolean consideraSaldoAnterior)
	{
		HorasVO retorno = new HorasVO();
		GregorianCalendar datini = new GregorianCalendar();
		GregorianCalendar datfim = new GregorianCalendar();

		if (offset != null)
		{
			//mesRef.add(GregorianCalendar.MONTH, offset);
			datini = (GregorianCalendar) mesRef.clone();
			datini.set(GregorianCalendar.DAY_OF_MONTH, 1);

			//datfim = (GregorianCalendar) datini.clone();
			if (consideraSaldoAnterior)
			{
				datfim.add(GregorianCalendar.MONTH, 1);
				datfim.set(GregorianCalendar.DATE, 1);
				datfim.add(GregorianCalendar.DATE, -1);
			}
			else
			{
				datfim = (GregorianCalendar) mesRef.clone();
				//datfim.add(GregorianCalendar.DATE, offset);
			}

		}

		StringBuilder sql = new StringBuilder();

		sql.append(" declare @ano varchar(4) ");
		sql.append(" declare @numemp numeric(9,0) ");
		sql.append(" declare @numcad numeric(9,0) ");
		sql.append(" declare @tipcol numeric(9,0) ");
		sql.append(" declare @datref datetime ");
		sql.append(" declare @datini datetime ");
		sql.append(" declare @datfim datetime ");
		sql.append(" select @ano = cast( year(getdate()) as varchar) ");
		sql.append(" select @numemp = ? ");
		sql.append(" select @numcad = ? ");
		sql.append(" select @tipcol = ? ");
		sql.append(" select @datini = ? ");
		sql.append(" select @datfim = ? ");

		//sql.append(" select @datini = (select case when perref < '2016-03-01 00:00:00' then '2016-03-01 00:00:00' else perref end  from dbo.R044CAL where DATEADD(dd, 0, DATEDIFF(dd, 0, @datref-28)) between iniapu and fimapu and numemp =  @numemp and tipcal = 11 ) ");
		//sql.append(" select @datfim = (select (SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,perref)+2,0))) from dbo.R044CAL where DATEADD(dd, 0, DATEDIFF(dd, 0, @datref)) between iniapu and fimapu and numemp =  @numemp and tipcal = 11) ");
		//sql.append(" select @datfim = (case when @datref < @datfim then @datref else @datfim end) ");
		sql.append(" select fun.numcad, fun.nomfun, fun.numemp, sum(horas.horas) as horas,sum(horas.horasDeb) as horasDeb , @datini, @datfim  from r034fun fun , ");
		sql.append(" 	 ( ");
		sql.append(" 	         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  isnull(sum(sit.QtdHor),0) as horas, 0 as horasDeb  from dbo.R066sit sit ");
		sql.append("                 ");
		sql.append("                 where 1=1 ");
		sql.append("                 and datApu >= @datini   and datApu < @datfim ");
		sql.append("                 and sit.codsit in (451,452) ");
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) ");
		sql.append("         union ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  0 as horas,  isnull(sum(sit.QtdHor),0) as horasDeb  from dbo.R066sit sit ");
		sql.append("                 ");
		sql.append("                 where 1=1 ");
		sql.append("                 and datApu >= @datini   and datApu < @datfim ");
		sql.append("                 and sit.codsit in (450) ");
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) ");
		sql.append(" 	 ) as horas  ");
		sql.append(" 	 where  1=1  ");
		sql.append(" 	 and fun.numcad = @numcad and fun.numemp =  @numemp and @tipcol = 1  ");
		sql.append(" 	  ");
		sql.append(" 	 and ( horas.numcad = fun.numcad   and horas.numemp = fun.numemp and horas.tipcol = fun.tipcol )  ");
		sql.append(" 	 group by fun.numcad, fun.nomfun, fun.numemp  ");

		Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			//System.out.println(sql.toString());
			pst = connection.prepareStatement(sql.toString());
			System.out.println("Data inicio:"+NeoDateUtils.safeDateFormat(datini, "dd/MM/yyyy"));
			System.out.println("Data fim:"+NeoDateUtils.safeDateFormat(datfim, "dd/MM/yyyy"));

			pst.setLong(1, numemp);
			pst.setLong(2, numcad);
			pst.setLong(3, tipcol);
			pst.setDate(4, new Date(datini.getTimeInMillis()));
			pst.setDate(5, new Date(datfim.getTimeInMillis()));
			rs = pst.executeQuery();

			if (rs.next())
			{
				retorno.setPositivas(rs.getLong("horas"));
				retorno.setNegativas(rs.getLong("horasDeb") * -1);

				Long saldoAnterior = 0L;
				if (consideraSaldoAnterior)
				{
					saldoAnterior = retornaSaldoHoras(numcad, numemp, tipcol, datini, 0);
					HorasVO h = new HorasVO();
					h.setTotal(saldoAnterior);
					retorno = h;
				}
			}
			else
			{
				retorno.setPositivas(0L);
				retorno.setNegativas(0L);
				retorno.setTotal(0L);
			}
			return retorno;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return retorno;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, pst, rs);
		}

	}
		
	/**
	 * Retorna saldo do banco de horas para um colaborador administrativo com base no mes de referencia
	 * 
	 * @param numcad - numero de matricula
	 * @param numemp - codigo da empresa
	 * @param tipcol - tipo do colaborador
	 * @return quantidade de horas de saldo em minutos.
	 */
	public static HashMap<String,Long> saldoBancoHorasAdm(Long numcad, Long numemp, long tipcol){
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();
	    
	    HashMap<String, Long> retorno = null;
	    
	    GregorianCalendar dataAnterior = new GregorianCalendar();
	    dataAnterior.set(Calendar.DAY_OF_MONTH, 1);
	    dataAnterior.set(Calendar.HOUR, 0);
	    dataAnterior.set(Calendar.MINUTE, 0);
	    dataAnterior.set(Calendar.SECOND, 0);
	    GregorianCalendar dataReferencia = new GregorianCalendar();
//	    dataReferencia.add(Calendar.MONTH, 1);
//	    dataReferencia.set(Calendar.DAY_OF_MONTH, 1);
	    dataReferencia.add(Calendar.DAY_OF_MONTH, -1);
	    dataReferencia.set(Calendar.HOUR, 23);
	    dataReferencia.set(Calendar.MINUTE, 59);
	    dataReferencia.set(Calendar.SECOND, 59);
	    
	    try {
		conn = PersistEngine.getConnection("VETORH");
		
		sql.append(" declare @dataAnterior datetime ");
		sql.append(" declare @dataReferencia datetime ");
		sql.append(" declare @matricula int ");
		sql.append(" declare @empresa int ");
		sql.append(" select @dataAnterior = ? ");
		sql.append(" select @dataReferencia = ? ");
		sql.append(" select @matricula = ? ");
		sql.append(" select @empresa = ? ");
		sql.append(" select (ISNULL((select SUM(QtdHor) from R011LAN lan ");
		sql.append(" where lan.NumCad = @matricula and NumEmp = @empresa and DatLan < @dataAnterior AND DatLan >= '2018-03-01' and SinLan = '+'),0) ");
		sql.append(" - ");
		sql.append(" ISNULL((select SUM(QtdHor) from R011LAN lan ");
		sql.append(" where lan.NumCad = @matricula and NumEmp = @empresa and DatLan < @dataAnterior AND DatLan >= '2018-03-01' and SinLan = '-'),0)) as 'saldoAlterior', ");
		sql.append(" (ISNULL((select SUM(QtdHor) from R011LAN lan ");
		sql.append(" where lan.NumCad = @matricula and NumEmp = @empresa and DatLan < @dataReferencia AND DatLan >= '2018-03-01' and SinLan = '-'),0) ");
		sql.append(" - ");
		sql.append(" ISNULL((select SUM(QtdHor) from R011LAN lan ");
		sql.append(" where lan.NumCad = @matricula and NumEmp = @empresa and DatLan < @dataAnterior AND DatLan >= '2018-03-01' and SinLan = '-'),0)) as 'saldoNegativo', ");
		sql.append(" (ISNULL((select SUM(QtdHor) from R011LAN lan ");
		sql.append(" where lan.NumCad = @matricula and NumEmp = @empresa and DatLan < @dataReferencia AND DatLan >= '2018-03-01' and SinLan = '+'),0) ");
		sql.append(" - ");
		sql.append(" ISNULL((select SUM(QtdHor) from R011LAN lan ");
		sql.append(" where lan.NumCad = @matricula and NumEmp = @empresa and DatLan < @dataAnterior AND DatLan >= '2018-03-01' and SinLan = '+'),0)) as 'saldoAtualPositivo'");
                            		
		pstm = conn.prepareStatement(sql.toString());

		pstm.setDate(1, new Date(dataAnterior.getTimeInMillis()));
		pstm.setDate(2, new Date(dataReferencia.getTimeInMillis()));
		pstm.setLong(3, numcad);
		pstm.setLong(4, numemp);

		rs = pstm.executeQuery();

		while (rs.next()) {
		    Long saldoAnterior = rs.getLong("saldoAlterior");
		    Long saldoNegativo = rs.getLong("saldoNegativo");
		    Long saldoAtualPositivo = rs.getLong("saldoAtualPositivo");
		    retorno = new HashMap<String, Long>();
		    retorno.put("saldoAnterior", saldoAnterior);
		    Long saldoAVencer = 0L;
		    if (saldoAnterior >= 0){
			saldoAVencer = saldoAnterior - saldoNegativo;
			if (saldoAVencer < 0){
			    saldoAVencer = 0L;
			}
			if (saldoNegativo > saldoAnterior){
			    Long resto = saldoNegativo - saldoAnterior;
			    saldoAtualPositivo -= resto;
			}
		    }else{
			saldoAVencer = saldoAtualPositivo + saldoAnterior;
			if (saldoAVencer > 0){
			    saldoAVencer = 0L;
			}
			if (saldoAVencer == 0L){
			    Long resto = saldoAtualPositivo + saldoAnterior;
			    saldoAtualPositivo -= resto;
			}else{
			    saldoAtualPositivo = 0L;
			}
		    }
		    retorno.put("saldoAVencer", saldoAVencer);
		    
		    retorno.put("saldoPositivo", saldoAtualPositivo);
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    
	    return retorno;
	}
	
	/**
	 * Retorna saldo do banco de horas para um colaborador administrativo com base no mes de referencia
	 * 
	 * @param numcad - numero de matricula
	 * @param numemp - codigo da empresa
	 * @param tipcol - tipo do colaborador
	 * @return quantidade de horas de saldo em minutos.
	 */
	public static HashMap<String,Long> saldoBancoHorasOperacional(Long numcad, Long numemp, long tipcol){
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();
	    
	    HashMap<String, Long> retorno = null;
	    
	    GregorianCalendar dataReferencia = new GregorianCalendar();
	    dataReferencia.add(Calendar.MONTH, 1);
	    dataReferencia.set(Calendar.DAY_OF_MONTH, 1);
	    dataReferencia.set(Calendar.HOUR, 0);
	    dataReferencia.set(Calendar.MINUTE, 0);
	    dataReferencia.set(Calendar.SECOND, 0);
	    
	    try {
		conn = PersistEngine.getConnection("VETORH");
		
		sql.append(" declare @dataAnterior datetime ");
		sql.append(" declare @dataReferencia datetime ");
		sql.append(" declare @matricula int ");
		sql.append(" declare @empresa int ");
		sql.append(" select @dataReferencia = ? ");
		sql.append(" select @matricula = ? ");
		sql.append(" select @empresa = ? ");
		sql.append(" select (ISNULL((select SUM(QtdHor) from R011LAN lan ");
		sql.append(" where lan.NumCad = @matricula and NumEmp = @empresa and DatLan < @dataReferencia AND DatLan >= '2018-03-01' and SinLan = '+'),0) ");
		sql.append(" - ");
		sql.append(" ISNULL((select SUM(QtdHor) from R011LAN lan ");
		sql.append(" where lan.NumCad = @matricula and NumEmp = @empresa and DatLan < @dataReferencia AND DatLan >= '2018-03-01' and SinLan = '-'),0)) as 'saldo' ");
                            		
		pstm = conn.prepareStatement(sql.toString());

		pstm.setDate(1, new Date(dataReferencia.getTimeInMillis()));
		pstm.setLong(2, numcad);
		pstm.setLong(3, numemp);

		rs = pstm.executeQuery();

		while (rs.next()) {
		    Long saldo = rs.getLong("saldo");
		    retorno = new HashMap<String, Long>();
		    retorno.put("saldo", saldo);
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    
	    return retorno;
	}
	
	public static Boolean isAdm(Long numcad, Long numemp, Long tipcol){
	    Boolean retorno = false;
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();

	    try {
		conn = PersistEngine.getConnection("VETORH");
		
		sql.append(" select usu_tipadm from r034fun where numcad = ? and numemp = ? and tipcol = ? ");
		
		pstm = conn.prepareStatement(sql.toString());
		
		pstm.setLong(1, numcad);
		pstm.setLong(2, numemp);
		pstm.setLong(3, tipcol);

		rs = pstm.executeQuery();

		while (rs.next()) {
		    Long tipadm = rs.getLong("usu_tipadm");
		    if (tipadm != null && tipadm == 1L){
			retorno = true;
		    }
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    return retorno;
	}

	/**
	 * Calcula diferenca entre os saldos de horas com base nas regras de banco de horas implementadas
	 * pela orsgups
	 * 
	 * @param saldoMA - Saldo Mês anterior
	 * @param saldoA - Saldo Atual
	 * @return
	 */
	public static SaldoHorasVO calculaSaldoFinal(HorasVO _saldoMA, HorasVO _saldoA)
	{
 		SaldoHorasVO retorno = new SaldoHorasVO();
		Long saldoMA = 0L;
		Long saldoA = 0L;

		if (_saldoMA.getTotal() < 0)
		{ // saldo negativo a ser compensado com os positivos do mes atual 
			saldoMA = _saldoMA.getTotal();
			saldoA = _saldoA.getPositivas();
		}
		else if (_saldoMA.getTotal() > 0)
		{ // saldo positivo que poderÃ¡ ser debitado na competencia atual
			saldoMA = _saldoMA.getTotal();
			saldoA = _saldoA.getNegativas();
		}

		// set o saldo do mes anterior
		retorno.setSaldoAnterior(saldoMA);

		//Calcular saldo Ã  vencer
		if ((saldoMA <= 0 && saldoA <= 0) || (saldoMA >= 0 && saldoA >= 0))
		{
			retorno.setSaldoVencer(saldoMA);
		}
		else if (saldoA < 0 && saldoMA >= 0L)
		{
			if (saldoA + saldoMA == 0)
			{
				retorno.setSaldoVencer(0L);
				_saldoA.setNegativas(0L);
			}
			else
			{
				if (saldoA + saldoMA > 0)
				{
					retorno.setSaldoVencer(saldoA + saldoMA);
					_saldoA.setNegativas(0L);
				}
				else
				{
					retorno.setSaldoVencer(0L);
					_saldoA.setNegativas(saldoA + saldoMA);
					
				}
			}
		}
		else
		{
			if (saldoMA + saldoA == 0)
			{
				retorno.setSaldoVencer(0L);
				_saldoA.setPositivas(0L);
			}
			else
			{
				if (saldoMA + saldoA > 0)
				{
					retorno.setSaldoVencer(0L);
					_saldoA.setPositivas(saldoMA + saldoA);
					
				}
				else
				{
					retorno.setSaldoVencer(saldoA + saldoMA);
					_saldoA.setPositivas(saldoA + saldoMA);
				}
			}
		}

		// calcula saldo atual ;
		
		
		saldoA = _saldoA.getNegativas() + _saldoA.getPositivas();
		/* if (_saldoMA.getTotal() < 0 && _saldoA.getPositivas() > 0)
		{
			if (_saldoMA.getTotal() + _saldoA.getPositivas() == 0)
			{
				saldoA = 0L;
			}
			else if (_saldoMA.getTotal() + _saldoA.getPositivas() < 0)
			{
				saldoA = 0L;
			}
			else
			{
				saldoA = _saldoMA.getTotal() + _saldoA.getPositivas();
			}
		}
		else if (_saldoMA.getTotal() > 0 && _saldoA.getNegativas() > 0)
		{
			if (_saldoMA.getTotal() + _saldoA.getNegativas() == 0)
			{
				saldoA = 0L;
			}
			else if (_saldoMA.getTotal() + _saldoA.getNegativas() > 0)
			{
				saldoA = 0L;
			}
			else
			{
				saldoA = _saldoMA.getTotal() + _saldoA.getNegativas();
			}
			saldoA = saldoA + _saldoA.getPositivas();
		}
		else
		{
			saldoA = _saldoA.getNegativas() + _saldoA.getPositivas();
		}*/

		retorno.setSaldo(saldoA);
		return retorno;
	}

	/**
	 * Calcula as apurações de saldo de um mês. Isso é utilizado para calcular os pagamentos de horas do
	 * banco(creditos e debitos para zerar o banco).
	 * 
	 * @param numcad -
	 * @param numemp -
	 * @param tipcol -
	 * @param mesRef -
	 * @param offset - offset em meses
	 * @return
	 */
	public static Long retornaSaldoHoras(Long numcad, Long numemp, long tipcol, GregorianCalendar mesRef, Integer offset)
	{
		Long retorno = 0L;
		GregorianCalendar datini = new GregorianCalendar();
		GregorianCalendar datfim = new GregorianCalendar();
		GregorianCalendar datCor = new GregorianCalendar();
		GregorianCalendar datIniSql = new GregorianCalendar();
		GregorianCalendar datFimSql = new GregorianCalendar();
		datIniSql = (GregorianCalendar) mesRef.clone();
		datIniSql.add(GregorianCalendar.MONTH,2);
		datIniSql.set(GregorianCalendar.DATE,1);
		datFimSql = (GregorianCalendar)datIniSql.clone();
		datFimSql.add(GregorianCalendar.MONTH, -1);
		
		datCor.set(GregorianCalendar.MONDAY, 05);
		datCor.set(GregorianCalendar.DATE, 01);

		GregorianCalendar datCor2 = new GregorianCalendar();
		datCor2.set(GregorianCalendar.MONDAY, 06);
		datCor2.set(GregorianCalendar.DATE, 01);

		System.out.println("date 1 :"+NeoDateUtils.safeDateFormat(datIniSql, "dd/MM/yyyy"));

		if (offset != null)
		{
			if (datini.after(datCor2))
			{
				mesRef.add(GregorianCalendar.MONTH, -1);
			}

			datini = (GregorianCalendar) mesRef.clone();

			datini.set(GregorianCalendar.DAY_OF_MONTH, 1);
			datfim = (GregorianCalendar) datini.clone();
			datfim.add(GregorianCalendar.MONTH, 1);

		}

		StringBuilder sql = new StringBuilder();

		sql.append(" select ( ");
		sql.append(" 		ISNULL((select SUM(QtdHor) from R011LAN where SinLan = '+' and NUMEMP = ? AND NUMCAD = ? AND TIPCOL = ? and DatCmp <= ? AND R011LAN.DatLan < ?),0) ");
		sql.append(" -  ");
		sql.append(" 		ISNULL((select SUM(QtdHor) from R011LAN where SinLan = '-' and NUMEMP = ? AND NUMCAD = ? AND TIPCOL = ? and DatCmp <= ? AND R011LAN.DatLan < ? ),0) ");
		sql.append(" ) as saldo ");
		Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
		ResultSet rs = null;
		System.out.println("Sql: " + sql.toString() + " Parametros: " + numcad);
		try
		{
			PreparedStatement pst = connection.prepareStatement(sql.toString());

			pst.setLong(1, numemp);
			pst.setLong(2, numcad);
			pst.setLong(3, tipcol);
			pst.setDate(4, new Date(datIniSql.getTimeInMillis()));
			pst.setDate(5, new Date(datFimSql.getTimeInMillis()));
			pst.setLong(6, numemp);
			pst.setLong(7, numcad);
			pst.setLong(8, tipcol);
			pst.setDate(9, new Date(datIniSql.getTimeInMillis()));
			pst.setDate(10, new Date(datFimSql.getTimeInMillis()));

			rs = pst.executeQuery();

			if (rs.next())
			{
				retorno = rs.getLong("SALDO");
			}
			rs.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return retorno;
	}

	public static boolean temTipoD(Long numemp, Long numcad)
	{
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM R011LAN WHERE DATLAN = '2016-04-30' AND ORILAN = 'D' AND NUMEMP = ? AND NUMCAD = ? AND TIPCOL = 1");
		Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
		ResultSet rs = null;
		PreparedStatement pst = null;

		try
		{
			pst = connection.prepareStatement(sql.toString());
			pst.setLong(1, numemp);
			pst.setLong(2, numcad);
			rs = pst.executeQuery();
			if (rs.next())
			{
				return true;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, pst, rs);
		}

		return false;
	}

	@SuppressWarnings("static-access")
	public static String retornaDataFormatoSapiensEVetorh(GregorianCalendar data)
	{
		String novaData = "";

		if (data == null)
		{
			/*
			 * 31/12/1900 o sapiens\Vetorh entende essa data como vazia
			 */
			data = new GregorianCalendar();
			data.set(data.YEAR, 1900);
			data.set(data.MONTH, 11);
			data.set(data.DAY_OF_MONTH, 31);
			data.set(Calendar.HOUR_OF_DAY, 00);
			data.set(Calendar.MINUTE, 00);
			data.set(Calendar.SECOND, 00);
		}

		if (data != null)
		{
			novaData = NeoDateUtils.safeDateFormat(data, "yyyy-MM-dd");
			novaData = novaData + " 00:00:00";
		}

		return novaData;
	}

	/**
	 * Retorna o saldo de horas extras do colaborador
	 * 
	 * @param numcad - numero de cadastro no vetor
	 * @param numemp - numero da empresa onde o colaborador está lotado
	 * @param tipcol - tipo de colaborador
	 * @param AteNDias - Limite que faz a consulta até N dias para trás, sendo que passando valor zero,
	 *            limita a 1 dia para traz. <br>
	 *            valor -1 retorna até o dia atual.
	 * @return
	 * @see {@link SiteUtils.retornaSaldoHoras}
	 */
	@Deprecated
	public static Long retornaSaldoColaborador(Long numcad, Long numemp, long tipcol, int AteNDias)
	{
		Long retorno = 0L;

		StringBuilder sql = new StringBuilder();

		sql.append(" select fun.numcad, fun.nomfun, fun.numemp, sum(horas.horas-horas.horasDeb) as saldo  from r034fun fun , ");
		sql.append(" ( ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  ISNULL(sum(sit.QtdHor),0) as horas, 0 as horasDeb  from dbo.R066sit sit ");
		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp and tipcal = 11 ) ");
		sql.append("                 where cal.perref in (select perref from dbo.R044CAL where DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) between iniapu and fimapu and numemp = ?) ");
		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu ");
		sql.append("                 and sit.codsit in (451,452) ");
		sql.append("				 and datApu < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()+" + AteNDias + ")) "); // regra adicionada em 03/08/2015 à pedido da Grazi para olhar sempre para 1 dia atras			
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) ");
		sql.append("         union ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  0 as horas,  ISNULL(sum(sit.QtdHor),0) as horasDeb  from dbo.R066sit sit ");
		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp and tipcal = 11 ) ");
		sql.append("                 where cal.perref in (select perref from dbo.R044CAL where DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) between iniapu and fimapu and numemp = ?) ");
		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu ");
		sql.append("                 and sit.codsit in (450) ");
		sql.append("				 and datApu < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()+" + AteNDias + ")) "); // regra adicionada em 03/08/2015 à pedido da Grazi para olhar sempre para 1 dia atras
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) ");
		sql.append(" ) as horas ");
		sql.append(" where  1=1 ");
		sql.append(" and fun.numcad = ? and fun.numemp = ? and fun.tipcol = ? ");
		sql.append(" and fun.numemp not in(21,22) ");
		sql.append(" and ( horas.numcad = fun.numcad   and horas.numemp = fun.numemp and horas.tipcol = fun.tipcol ) ");
		sql.append(" group by fun.numcad, fun.nomfun, fun.numemp ");

		Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
		ResultSet rs = null;
		try
		{
			//System.out.println(sql.toString());
			PreparedStatement pst = connection.prepareStatement(sql.toString());

			pst.setLong(1, numemp);
			pst.setLong(2, numemp);
			pst.setLong(3, numcad);
			pst.setLong(4, numemp);
			pst.setLong(5, tipcol);

			rs = pst.executeQuery();

			if (rs.next())
			{
				retorno = rs.getLong("saldo");
			}
			rs.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return retorno;
	}

	/**
	 * Retorna o saldo de horas extras do colaborador por competencia
	 * 
	 * @param numcad
	 * @param numemp
	 * @param tipcol
	 * @param mesRef
	 * @return
	 * @see {@link SiteUtils.retornaSaldoHoras}
	 */
	@Deprecated
	public static Long retornaSaldoColaboradorCPT(Long numcad, Long numemp, long tipcol, GregorianCalendar mesRef)
	{
		Long retorno = 0L;

		StringBuilder sql = new StringBuilder();

		sql.append(" select fun.numcad, fun.nomfun, fun.numemp, sum(horas.horas-horas.horasDeb) as saldo  from r034fun fun , ");
		sql.append(" ( ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  ISNULL(sum(sit.QtdHor),0) as horas, 0 as horasDeb  from dbo.R066sit sit ");
		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp and tipcal = 11 ) ");
		sql.append("                 where cal.perref = ? ");
		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu ");
		sql.append("                 and sit.codsit in (451,452) ");
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) ");
		sql.append("         union ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  0 as horas,  ISNULL(sum(sit.QtdHor),0) as horasDeb  from dbo.R066sit sit ");
		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp and tipcal = 11 ) ");
		sql.append("                 where cal.perref = ? ");
		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu ");
		sql.append("                 and sit.codsit in (450) ");
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) ");
		sql.append(" ) as horas ");
		sql.append(" where  1=1 ");
		sql.append(" and fun.numcad = ? and fun.numemp = ? and fun.tipcol = ? ");
		sql.append(" and fun.numemp not in(21,22) ");
		sql.append(" and ( horas.numcad = fun.numcad   and horas.numemp = fun.numemp and horas.tipcol = fun.tipcol ) ");
		sql.append(" group by fun.numcad, fun.nomfun, fun.numemp ");

		Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
		ResultSet rs = null;
		try
		{
			//System.out.println(sql.toString());
			PreparedStatement pst = connection.prepareStatement(sql.toString());

			pst.setDate(1, new Date(mesRef.getTimeInMillis()));
			pst.setDate(2, new Date(mesRef.getTimeInMillis()));
			pst.setLong(3, numcad);
			pst.setLong(4, numemp);
			pst.setLong(5, tipcol);

			rs = pst.executeQuery();

			if (rs.next())
			{
				retorno = rs.getLong("saldo");
			}
			rs.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return retorno;
	}

	/**
	 * Retorna o saldo de horas extras do colaborador na competencia anterior
	 * 
	 * @param numcad
	 * @param numemp
	 * @param tipcol
	 * @param mesRef
	 * @return
	 * @see {@link SiteUtils.retornaSaldoHoras}
	 */
	@Deprecated
	public static Long retornaSaldoColaboradorCptAnterior(Long numcad, Long numemp, long tipcol)
	{
		Long retorno = 0L;

		StringBuilder sql = new StringBuilder();

		sql.append(" select fun.numcad, fun.nomfun, fun.numemp, sum(horas.horas-horas.horasDeb) as saldo  from r034fun fun , ");
		sql.append(" ( ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  ISNULL(sum(sit.QtdHor),0) as horas, 0 as horasDeb  from dbo.R066sit sit ");
		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp and tipcal = 11 ) ");
		sql.append("                 where cal.perref in (select dateadd(m,-1,perref) from dbo.R044CAL where DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) between iniapu and fimapu and numemp = ?) ");
		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu ");
		sql.append("                 and sit.codsit in (451,452) ");
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) ");
		sql.append("         union ");
		sql.append("         ( ");
		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  0 as horas,  ISNULL(sum(sit.QtdHor),0) as horasDeb  from dbo.R066sit sit ");
		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp  and tipcal = 11) ");
		sql.append("                 where cal.perref in (select dateadd(m,-1,perref) from dbo.R044CAL where DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) between iniapu and fimapu and numemp = ?) ");
		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu ");
		sql.append("                 and sit.codsit in (450) ");
		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append("         ) ");
		sql.append(" ) as horas ");
		sql.append(" where  1=1 ");
		sql.append(" and fun.numcad = ? and fun.numemp = ? and fun.tipcol = ? ");
		sql.append(" and fun.numemp not in(21,22) ");
		sql.append(" and ( horas.numcad = fun.numcad   and horas.numemp = fun.numemp and horas.tipcol = fun.tipcol ) ");
		sql.append(" group by fun.numcad, fun.nomfun, fun.numemp ");

		Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
		ResultSet rs = null;
		try
		{
			//System.out.println(sql.toString());
			PreparedStatement pst = connection.prepareStatement(sql.toString());

			pst.setLong(1, numemp);
			pst.setLong(2, numemp);
			pst.setLong(3, numcad);
			pst.setLong(4, numemp);
			pst.setLong(5, tipcol);

			rs = pst.executeQuery();

			if (rs.next())
			{
				retorno = rs.getLong("saldo");
			}
			rs.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return retorno;
	}

	public List<ColaboradorVO> getListaColaboradores(List<EscalaVO> escalas, GregorianCalendar dataAtual, Long codcli)
	{
		Connection connVetorh = null;
		PreparedStatement stColaborador = null;
		ResultSet rsColaborador = null;
		List<ColaboradorVO> listaColaboradores = new ArrayList<ColaboradorVO>();
		ColaboradorVO colaborador = new ColaboradorVO();

		PresencaVO presenca = new PresencaVO();
		GregorianCalendar entrada;
		GregorianCalendar saida;

		GregorianCalendar dataHoje = new GregorianCalendar();

		String stringDataAtual = NeoDateUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
		String stringDataHoje = NeoDateUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");
		Boolean diaAtual = true;

		String numCraAnt = "";

		//Data diferente da data atual
		if (!stringDataAtual.endsWith(stringDataHoje))
		{
			diaAtual = false;
		}

		try
		{

			connVetorh = PersistEngine.getConnection("VETORH");
			StringBuilder queryColaborador = new StringBuilder();
			queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, orn.numloc, orn.taborg, ");
			queryColaborador.append("        esc.NomEsc, car.TitRed, sis.DesSis, hes.CodEsc, hes.CodTma, fun.EmiCar,  ");
			queryColaborador.append("        ISNULL(afa.SitAfa, 1) AS SitAfa, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, ISNULL(sit.usu_blopre, 'N') AS BloPre,  ");
			queryColaborador.append("        afa.DatAfa AS dataAfa, afa.DatTer, fun.USU_MatRes, ");
			queryColaborador.append(" 		 DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) AS DatAccS, accS.DirAcc AS DirAccS, accS.USU_TipDir AS TipDirS, accS.codrlg AS codrlgS, ");
			queryColaborador.append("        DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) AS DatAccE, accE.DirAcc AS DirAccE, accE.USU_TipDir AS TipDirE, accE.codrlg AS codrlgE ");
			queryColaborador.append(" FROM R034FUN fun  ");
			queryColaborador.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= GETDATE())");
			queryColaborador.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= GETDATE()) ");
			queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= GETDATE()) ");
			queryColaborador.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
			queryColaborador.append(" INNER JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= GETDATE()) ");
			queryColaborador.append(" INNER JOIN R024CAR car ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");
			queryColaborador.append(" LEFT JOIN R024SIS sis ON sis.SisCar = car.SisCar ");
			queryColaborador.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = hlo.TabOrg ");
			//queryColaborador.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.USU_NumLoc = orn.NumLoc AND cvs.USU_TabOrg = orn.TabOrg AND cvs.USU_SeqAlt = (Select MAX(c2.usu_seqalt) FROM USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= GETDATE()) ");
			//queryColaborador.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
			queryColaborador.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= GETDATE() AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(GETDATE() as float)) as datetime)))) ");
			//queryColaborador.append(" LEFT JOIN R038AFA afaDem WITH (NOLOCK) ON afaDem.NumEmp = fun.NumEmp AND afaDem.TipCol = fun.TipCol AND afaDem.NumCad = fun.NumCad AND afaDem.sitAfa = 7 AND afaDem.DatAfa = (SELECT MAX (DATAFA) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afaDem.NUMEMP AND TABELA001.TIPCOL = afaDem.TIPCOL AND TABELA001.NUMCAD = afaDem.NUMCAD) ");
			//queryColaborador.append(" LEFT JOIN R042CAU cauDem ON cauDem.CauDem = afaDem.CauDem ");
			queryColaborador.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
			queryColaborador.append(" LEFT JOIN R070ACC accS ON (accS.NumCra = hch.NumCra AND accS.DatAcc > DATEADD(mm, -1, GETDATE()) AND accS.TipAcc = 100 AND accS.CodPlt = 2 AND accS.usu_TabOrg = hlo.TabOrg AND accS.usu_NumLoc = hlo.NumLoc AND accS.usu_tipdir in ('P') ");
			queryColaborador.append(" 			AND DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc)) FROM R070ACC accS2 WHERE accS2.NumCra = accS.NumCra AND accS2.DatAcc > DATEADD(mm, -1, GETDATE()) AND accS2.usu_TabOrg = accS.usu_TabOrg AND accS2.usu_NumLoc = accS.usu_NumLoc AND accS2.TipAcc = 100 AND accS2.CodPlt = 2 AND accS2.usu_tipdir = accS.usu_tipdir AND DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc) <= GETDATE()))  ");
			queryColaborador.append(" LEFT JOIN R070ACC accE ON (accE.NumCra = hch.NumCra AND accE.DatAcc > DATEADD(mm, -1, GETDATE()) AND accE.TipAcc = 100 AND accE.CodPlt = 2 AND accE.usu_TabOrg = hlo.TabOrg AND accE.usu_NumLoc = hlo.NumLoc AND accE.usu_tipdir in ('P') AND accE.DirAcc = 'E' ");
			queryColaborador.append(" 			AND DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc)) FROM R070ACC accE2 WHERE accE2.NumCra = accE.NumCra AND accE2.DatAcc > DATEADD(mm, -1, GETDATE()) AND accE2.usu_TabOrg = accE.usu_TabOrg AND accE2.usu_NumLoc = accE.usu_NumLoc AND accE2.TipAcc = 100 AND accE2.CodPlt = 2 AND accE2.usu_tipdir = accE.usu_tipdir AND accE2.DirAcc = 'E' AND DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc) <= GETDATE()))  ");

			queryColaborador.append(" WHERE fun.DatAdm <= GETDATE() ");
			queryColaborador.append(" AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(GETDATE() as float)) as datetime))) ");
			queryColaborador.append(" AND fun.TipCol = 1 ");
			queryColaborador.append(" AND fun.NumEmp not in (5, 11, 14) ");

			queryColaborador.append(" AND orn.usu_codclisap = ? ");

			queryColaborador.append(" ORDER BY fun.CodTma, esc.NomEsc, fun.NumEmp, fun.NomFun ");

			Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());

			stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
			/*stColaborador.setTimestamp(1, dataAtualTS);
			stColaborador.setTimestamp(2, dataAtualTS);
			stColaborador.setTimestamp(3, dataAtualTS);
			stColaborador.setTimestamp(4, dataAtualTS);
			stColaborador.setTimestamp(5, dataAtualTS);
			stColaborador.setTimestamp(6, dataAtualTS);
			stColaborador.setTimestamp(7, dataAtualTS);
			stColaborador.setTimestamp(8, dataAtualTS);
			stColaborador.setTimestamp(9, dataAtualTS);
			stColaborador.setTimestamp(10, dataAtualTS);
			stColaborador.setTimestamp(11, dataAtualTS);
			stColaborador.setTimestamp(12, dataAtualTS);
			stColaborador.setTimestamp(13, dataAtualTS);
			stColaborador.setTimestamp(14, dataAtualTS);*/
			stColaborador.setLong(1, codcli);

			Long timeIni = GregorianCalendar.getInstance().getTimeInMillis();
			rsColaborador = stColaborador.executeQuery();
			System.out.println("SQL - SiteUtils.getListaColaboradores - Time spent:" + (GregorianCalendar.getInstance().getTimeInMillis() - timeIni) + "ms");
			while (rsColaborador.next())
			{
				colaborador = new ColaboradorVO();
				colaborador.setNumeroEmpresa(rsColaborador.getLong("NumEmp"));
				colaborador.setTipoColaborador(rsColaborador.getLong("TipCol"));
				colaborador.setNumeroCadastro(rsColaborador.getLong("NumCad"));
				colaborador.setNomeColaborador(rsColaborador.getString("NomFun"));
				colaborador.setCargo(rsColaborador.getString("TitRed"));
				//colaborador.setSistemaCargo(rsColaborador.getString("DesSis"));
				colaborador.setSituacao(rsColaborador.getString("DesSit"));
				colaborador.setAcessoBloqueado("S".equals(rsColaborador.getString("BloPre")));
				colaborador.setControlaAcesso("S".equals(rsColaborador.getString("EmiCar")));
				colaborador.setCpf(rsColaborador.getLong("NumCpf"));
				colaborador.setSexo(rsColaborador.getString("TipSex"));
				colaborador.setDataNascimento(rsColaborador.getDate("DatNas"));
				colaborador.setDataAdmissao(rsColaborador.getDate("datAdm"));
				colaborador.setNumeroLocal(rsColaborador.getLong("numloc"));
				colaborador.setCodigoOrganograma(rsColaborador.getLong("taborg"));
				colaborador.setCodigoEscala(rsColaborador.getLong("CodEsc"));
				colaborador.setTurmaEscala(rsColaborador.getLong("CodTma"));
				colaborador.setDataAfastamento(rsColaborador.getDate("dataAfa"));
				//colaborador.setDataDemissao(rsColaborador.getDate("dataDem"));
				//colaborador.setFiscal(rsColaborador.getString("USU_NomFis"));
				colaborador.setMatrizResponsabilidade(rsColaborador.getString("USU_MatRes"));
				//colaborador.setCausaDemissao(rsColaborador.getString("DesDem"));

				QLPresencaUtils.setEscalaColaborador(escalas, colaborador);

				colaborador.getEscala().setDescricao(rsColaborador.getString("NomEsc") + " / " + colaborador.getTurmaEscala());

				if (rsColaborador.getString("numcra") != null)
				{
					colaborador.setNumeroCracha(rsColaborador.getString("numcra"));
				}
				else
				{
					colaborador.setNumeroCracha("");
				}

				String obsSituacao = "";
				if (rsColaborador.getLong("SitAfa") != 1L)
				{
					GregorianCalendar dataInicioAfastamento = new GregorianCalendar();
					GregorianCalendar dataFimAfastamento = new GregorianCalendar();

					if (rsColaborador.getTimestamp("DataAfa") != null)
					{
						dataInicioAfastamento.setTime(rsColaborador.getTimestamp("DataAfa"));
					}
					if (rsColaborador.getTimestamp("DatTer") != null)
					{
						dataFimAfastamento.setTime(rsColaborador.getTimestamp("DatTer"));
					}
					if ((dataInicioAfastamento != null))
					{
						if (dataFimAfastamento.get(Calendar.YEAR) == 1900)
						{
							obsSituacao = "A partir de " + NeoDateUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy");
						}
						else
						{
							obsSituacao = "De: " + NeoDateUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy");
							obsSituacao = obsSituacao + " a " + NeoDateUtils.safeDateFormat(dataFimAfastamento, "dd/MM/yyyy");
						}
					}
				}

				if (colaborador.getDataDemissao() != null)
				{
					if (diaAtual)
					{
						obsSituacao = "Em aviso prévio até " + NeoDateUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy");
					}
					else
					{
						if (colaborador.getDataDemissao().after(dataHoje.getTime()))
						{
							obsSituacao = "Em aviso prévio até " + NeoDateUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy");
						}
						else
						{
							obsSituacao = "Demitido em " + NeoDateUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy") + " por " + colaborador.getCausaDemissao();
						}
					}
				}

				colaborador.setObsSituacao(obsSituacao);

				//presenca = getPresenca(colaborador, dataAtual);

				presenca = new PresencaVO();
				entrada = new GregorianCalendar();
				saida = new GregorianCalendar();

				String direcaoAcesso1 = rsColaborador.getString("DirAccS");
				Timestamp dataAcesso1 = rsColaborador.getTimestamp("DatAccS");
				String tipoDirecaoAcesso1 = rsColaborador.getString("TipDirS");
				Long coletor1 = rsColaborador.getLong("codrlgS");

				String direcaoAcesso2 = rsColaborador.getString("DirAccE");
				Timestamp dataAcesso2 = rsColaborador.getTimestamp("DatAccE");
				String tipoDirecaoAcesso2 = rsColaborador.getString("TipDirE");
				Long coletor2 = rsColaborador.getLong("codrlgE");

				if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
				{

					entrada = new GregorianCalendar();

					if (direcaoAcesso1.equals("E"))
					{
						entrada.setTimeInMillis(dataAcesso1.getTime());
						presenca.setEntrada(entrada);
						presenca.setTipoDirecao(tipoDirecaoAcesso1);
						presenca.setColetorEntrada(coletor1);
					}
					else if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
					{
						saida.setTimeInMillis(dataAcesso1.getTime());
						presenca.setSaida(saida);
						presenca.setTipoDirecao(tipoDirecaoAcesso1);
						presenca.setColetorSaida(coletor1);
					}
				}

				if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
				{

					saida = new GregorianCalendar();

					if (direcaoAcesso2.equals("E"))
					{
						entrada.setTimeInMillis(dataAcesso2.getTime());
						presenca.setEntrada(entrada);
						presenca.setTipoDirecao(tipoDirecaoAcesso2);
						presenca.setColetorEntrada(coletor2);
					}
					else if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
					{
						saida.setTimeInMillis(dataAcesso2.getTime());
						presenca.setSaida(saida);
						presenca.setTipoDirecao(tipoDirecaoAcesso2);
						presenca.setColetorSaida(coletor2);
					}
				}

				presenca.setStatus(QLPresencaUtils.getStatusPresenca(colaborador.getEscala(), presenca, dataAtual));
				colaborador.setPresenca(presenca);

				//Verifica duplicidade no caso de possuir dois acessos na mesma data
				if (numCraAnt.equals("") || !numCraAnt.equals(colaborador.getNumeroCracha()))
				{
					// Adicionando a lista
					listaColaboradores.add(colaborador);
				}

				numCraAnt = colaborador.getNumeroCracha();

			}
			QLPresencaUtils.getlistaAvosFerias(listaColaboradores, dataAtual);
			rsColaborador.close();
			stColaborador.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
			}
			catch (Exception e)
			{
				e.printStackTrace();

			}
		}
		return listaColaboradores;
	}

	public List<EntradaAutorizadaVO> getListaEntradasAutorizadas(Collection<ColaboradorVO> colaboradores, GregorianCalendar dataAtual, List<EscalaVO> escalas, Long codcli)
	{
		Connection connVetorh = null;
		PreparedStatement stOperacao = null;
		ResultSet rsOperacao = null;
		List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
		EntradaAutorizadaVO entradaVO = new EntradaAutorizadaVO();
		ColaboradorVO coberturaVO;

		PresencaVO presenca = new PresencaVO();
		String numcraAnt = "";
		Long datiniAnt = 0L;
		Long datfimAnt = 0L;
		String horarioAnt = "";

		//Data diferente da data atual
//		if (!stringDataAtual.endsWith(stringDataHoje))
//		{
//			diaAtual = false;
//		}
		GregorianCalendar dataFimCompetencia = new GregorianCalendar();

		try
		{
			dataFimCompetencia = OrsegupsUtils.getDiaFimCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
			dataFimCompetencia.add(Calendar.DAY_OF_MONTH, -4);
		}
		catch (ParseException e1)
		{
			e1.printStackTrace();
		}

		try
		{
			connVetorh = PersistEngine.getConnection("VETORH");
			/*** Recupera Operacoes em Posto ***/
			StringBuilder queryOperacao = new StringBuilder();
			queryOperacao.append(" SELECT cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, hch.numcra, cob.usu_datalt, cob.usu_datfim, cob.USU_NumCadCob, orn.NumLoc, orn.TabOrg, ");
			queryOperacao.append(" 		  fun.nomfun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, fun.USU_MatRes, fun.emicar, car.titred, sis.DesSis, USU_NumEmpCob, esc.NomEsc, fun2.NomFun AS NomFun2, fun2.CodTma, ");
			queryOperacao.append(" 		  cob.USU_HorIni, fun2.CodEsc, mo.USU_DesPre, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, afa.DatAfa AS dataAfa, afa.DatTer, ISNULL(sit.usu_blopre, 'N') AS BloPre, ");
			queryOperacao.append(" 		  DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) AS DatAccS, accS.DirAcc AS DirAccS, accS.usu_tipdir AS TipDirS, accS.codrlg AS codrlgS, ");
			queryOperacao.append(" 		  DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) AS DatAccE, accE.DirAcc AS DirAccE, accE.usu_tipdir AS TipDirE, accE.codrlg AS codrlgE ");
			queryOperacao.append(" FROM USU_T038COBFUN cob WITH (NOLOCK) ");
			queryOperacao.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad ");
			queryOperacao.append(" LEFT JOIN R038HCH hch ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
			queryOperacao.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
			queryOperacao.append(" LEFT JOIN R024SIS sis ON sis.SisCar = car.SisCar ");
			queryOperacao.append(" LEFT JOIN R034FUN fun2 WITH (NOLOCK) ON fun2.NumEmp = cob.USU_NumEmpCob AND fun2.TipCol = cob.USU_TipColCob AND fun2.NumCad = cob.USU_NumCadCob ");
			queryOperacao.append(" LEFT JOIN R038HES hes ON hes.NumEmp = fun2.NumEmp AND hes.TipCol = fun2.TipCol AND hes.NumCad = fun2.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= ?) ");
			queryOperacao.append(" LEFT JOIN R006ESC esc WITH (NOLOCK) ON esc.CodEsc = fun2.CodEsc ");
			queryOperacao.append(" LEFT JOIN USU_T010MoCo mo WITH (NOLOCK) ON mo.USU_CodMot = cob.USU_codMot ");
			queryOperacao.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.numloc = cob.usu_numloctra AND orn.taborg = cob.usu_taborgtra ");
			queryOperacao.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = orn.taborg AND orn.numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = orn.taborg AND h2.numloc = orn.numloc) ");
			queryOperacao.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= ? AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime)))) ");
			queryOperacao.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
			queryOperacao.append(" LEFT JOIN R070ACC accS ON (accS.NumCra = hch.NumCra AND accS.TipAcc = 100 AND accS.DatAcc > DATEADD(mm, -1, ?) AND accS.CodPlt = 2 AND accS.usu_tipdir <> 'P' AND accS.usu_TabOrg = cob.usu_taborgtra AND accS.usu_NumLoc = cob.usu_numloctra ");
			queryOperacao.append(" 			 AND DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc)) FROM R070ACC accS2 WHERE accS2.NumCra = accS.NumCra AND accS2.DatAcc > DATEADD(mm, -1, ?) AND accS2.usu_TabOrg = accS.usu_TabOrg AND accS2.usu_NumLoc = accS.usu_NumLoc AND accS2.TipAcc = 100 AND accS2.CodPlt = 2 AND accS2.usu_tipdir <> 'P' AND DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc) <= ?)) ");
			queryOperacao.append(" LEFT JOIN R070ACC accE ON (accE.NumCra = hch.NumCra AND accE.DatAcc > DATEADD(mm, -1, ?) AND accE.usu_TabOrg = cob.usu_taborgtra AND accE.usu_NumLoc = cob.usu_numloctra AND accE.TipAcc = 100 AND accE.CodPlt = 2 AND accE.DirAcc = 'E' AND accE.usu_tipdir <> 'P' ");
			queryOperacao.append(" 			 AND DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc)) FROM R070ACC accE2 WHERE accE2.NumCra = accE.NumCra AND accE2.DatAcc > DATEADD(mm, -1, ?) AND accE2.usu_TabOrg = accE.usu_TabOrg AND accE2.usu_NumLoc = accE.usu_NumLoc AND accE2.TipAcc = 100 AND accE2.CodPlt = 2 AND accE2.DirAcc = 'E' AND accE2.usu_tipdir <> 'P' AND DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc) <= ?)) ");
			queryOperacao.append(" LEFT JOIN R066APU apu ON apu.NumEmp = fun.numemp AND apu.TipCol = fun.TipCol AND apu.NumCad = fun.NumCad AND apu.DatApu >= ? AND apu.DatApu <= ? AND fun.ConRho = 2 ");
			queryOperacao.append(" LEFT JOIN R004HOR hor ON hor.CodHor = apu.HorDat AND hor.CodHor > 9000 ");
			queryOperacao.append(" LEFT JOIN R066SIT sap ON sap.NumEmp = apu.NumEmp AND sap.TipCol = apu.TipCol AND sap.NumCad = apu.NumCad AND sap.DatApu = apu.DatApu ");
			queryOperacao.append(" LEFT JOIN R010SIT sto ON sto.CodSit = sap.CodSit AND sto.SitExc = 'S' ");
			queryOperacao.append(" WHERE cast(floor(CAST(cob.USU_DatFim AS FLOAT)) AS datetime) >= CAST(floor(CAST(? AS FLOAT)) AS datetime) ");
			queryOperacao.append(" AND orn.usu_codclisap = ? ");
			queryOperacao.append(" AND mo.USU_TipCob in ('C', 'D', 'R') ");
			queryOperacao.append(" AND orn.taborg = 203 ");
			queryOperacao.append(" AND LEN(hie.CodLoc) = 27 ");
			queryOperacao.append(" AND fun.NumEmp not in (5) ");
			queryOperacao.append(" ORDER BY cob.usu_numloctra, cob.USU_Datalt ");

			stOperacao = connVetorh.prepareStatement(queryOperacao.toString());
			stOperacao.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(3, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(5, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(6, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(7, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(8, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(9, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(10, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(11, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(12, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setTimestamp(13, new Timestamp(dataAtual.getTimeInMillis()));
			stOperacao.setLong(14, codcli);

			rsOperacao = stOperacao.executeQuery();
			while (rsOperacao.next())
			{
				coberturaVO = new ColaboradorVO();
				entradaVO = new EntradaAutorizadaVO();
				presenca = new PresencaVO();
				EscalaVO escala = new EscalaVO();

				coberturaVO.setNumeroEmpresa(rsOperacao.getLong("usu_numemp"));
				coberturaVO.setTipoColaborador(rsOperacao.getLong("usu_tipcol"));
				coberturaVO.setNumeroCadastro(rsOperacao.getLong("usu_numcad"));
				coberturaVO.setNomeColaborador(rsOperacao.getString("nomfun"));
				coberturaVO.setCargo(rsOperacao.getString("titred"));
				coberturaVO.setSistemaCargo(rsOperacao.getString("DesSis"));
				coberturaVO.setSituacao(rsOperacao.getString("DesSit"));
				coberturaVO.setAcessoBloqueado("S".equals(rsOperacao.getString("BloPre")));
				coberturaVO.setControlaAcesso("S".equals(rsOperacao.getString("EmiCar")));
				coberturaVO.setCpf(rsOperacao.getLong("NumCpf"));
				coberturaVO.setNumeroLocal(rsOperacao.getLong("numloc"));
				coberturaVO.setCodigoOrganograma(rsOperacao.getLong("taborg"));
				coberturaVO.setDataAfastamento(rsOperacao.getDate("dataAfa"));
				coberturaVO.setSexo(rsOperacao.getString("TipSex"));
				coberturaVO.setDataNascimento(rsOperacao.getDate("DatNas"));
				coberturaVO.setDataAdmissao(rsOperacao.getDate("datAdm"));
				coberturaVO.setMatrizResponsabilidade(rsOperacao.getString("USU_MatRes"));
				//coberturaVO.setQdeExcecao(rsOperacao.getLong("qdeExcecao"));

				if (rsOperacao.getString("numcra") != null)
				{
					coberturaVO.setNumeroCracha(rsOperacao.getString("numcra"));
				}
				else
				{
					coberturaVO.setNumeroCracha("");
				}

				entradaVO.setDescricaoCobertura(rsOperacao.getString("usu_despre"));
				entradaVO.setHoraInicial(rsOperacao.getLong("usu_horini"));

				GregorianCalendar dataInicio = (GregorianCalendar) dataAtual.clone();
				GregorianCalendar dataFim = (GregorianCalendar) dataAtual.clone();

				dataInicio.setTime(rsOperacao.getTimestamp("usu_datalt"));
				entradaVO.setDataInicial(OrsegupsUtils.atribuiHora(dataInicio));

				dataFim.setTime(rsOperacao.getTimestamp("usu_datfim"));
				entradaVO.setDataFinal(OrsegupsUtils.atribuiHora(dataFim));

				HorarioVO horario = new HorarioVO();
				horario.setDataInicial(dataInicio);
				horario.setDataFinal(dataFim);

				// Se USU_NumCadCob é igual a 0, é cobertura de posto, senão é cobertura de colaborador
				if (rsOperacao.getLong("USU_NumCadCob") == 0L)
				{

					escala.setHorarios(QLPresencaUtils.getHorariosOperacoesPostos(horario, dataAtual));
					escala.setDescricao(NeoDateUtils.safeDateFormat(dataInicio, "HH:mm") + "-" + NeoDateUtils.safeDateFormat(dataFim, "HH:mm"));
					String descHorarios = escala.toString();
					escala.setDescricaoHorarios(descHorarios);

					coberturaVO.setEscala(escala);

					//presenca = getPresenca(coberturaVO, dataAtual);
				}
				else
				{
					if (colaboradores != null)
					{
						for (ColaboradorVO substituidoVO : colaboradores)
						{
							if (substituidoVO.getNumeroCadastro().equals(rsOperacao.getLong("USU_NumCadCob")) && substituidoVO.getNumeroEmpresa().equals(rsOperacao.getLong("USU_NumEmpCob")))
							{
								entradaVO.setColaboradorSubstituido(substituidoVO);
								coberturaVO.setEscala(substituidoVO.getEscala().clone());
								coberturaVO.getEscala().setConsiderarEscala(Boolean.TRUE);

								GregorianCalendar dtInicioAux = (GregorianCalendar) horario.getDataInicial().clone();
								GregorianCalendar dtAtualAux = (GregorianCalendar) dataAtual.clone();

								dtInicioAux.set(Calendar.HOUR_OF_DAY, 0);
								dtInicioAux.set(Calendar.MINUTE, 0);
								dtInicioAux.set(Calendar.SECOND, 0);
								dtInicioAux.set(Calendar.MILLISECOND, 0);

								dtAtualAux.set(Calendar.HOUR_OF_DAY, 0);
								dtAtualAux.set(Calendar.MINUTE, 0);
								dtAtualAux.set(Calendar.SECOND, 0);
								dtAtualAux.set(Calendar.MILLISECOND, 0);

								if (dtInicioAux.after(dtAtualAux))
								{
									coberturaVO.getEscala().setHorarios(new ArrayList<HorarioVO>());
								}

								String descHorarios = coberturaVO.getEscala().toString();
								coberturaVO.getEscala().setDescricaoHorarios(descHorarios);

								break;
							}
						}
					}
					// Se não for localizado o substituido, seta valores padroes
					if (entradaVO.getColaboradorSubstituido() == null)
					{
						ColaboradorVO substituidoVO = new ColaboradorVO();
						substituidoVO.setNomeColaborador("NÃO LOCALIZADO");
						coberturaVO.setEscala(new EscalaVO());
						coberturaVO.getEscala().setDescricao("NÃO LOCALIZADA");
						coberturaVO.getEscala().setHorarios(new ArrayList<HorarioVO>());
						entradaVO.setColaboradorSubstituido(substituidoVO);
					}
				}

				if (coberturaVO.getEscala() != null)
				{
					//presenca = getPresenca(coberturaVO, dataAtual);

					presenca = new PresencaVO();
					GregorianCalendar gcDataAcesso1 = new GregorianCalendar();
					GregorianCalendar gcDataAcesso2 = new GregorianCalendar();

					String direcaoAcesso1 = rsOperacao.getString("DirAccS");
					Timestamp dataAcesso1 = rsOperacao.getTimestamp("DatAccS");
					Long coletor1 = rsOperacao.getLong("codrlgS");

					String direcaoAcesso2 = rsOperacao.getString("DirAccE");
					Timestamp dataAcesso2 = rsOperacao.getTimestamp("DatAccE");
					Long coletor2 = rsOperacao.getLong("codrlgS");

					if (coberturaVO.getEscala().getHorarios() != null && !coberturaVO.getEscala().getHorarios().isEmpty())
					{
						if (direcaoAcesso1 != null)
						{

							gcDataAcesso1.setTimeInMillis(dataAcesso1.getTime());

							if (direcaoAcesso1.equals("E"))
							{
								presenca.setEntrada(gcDataAcesso1);
								presenca.setColetorEntrada(coletor1);
							}
							else if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
							{
								presenca.setSaida(gcDataAcesso1);
								presenca.setColetorSaida(coletor1);
							}
						}

						if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
						{

							gcDataAcesso2.setTimeInMillis(dataAcesso2.getTime());

							if (direcaoAcesso2.equals("E"))
							{
								presenca.setEntrada(gcDataAcesso2);
								presenca.setColetorEntrada(coletor2);
							}
							else if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
							{
								presenca.setSaida(gcDataAcesso2);
								presenca.setColetorSaida(coletor2);
							}
						}
					}

					presenca.setStatus(QLPresencaUtils.getStatusPresenca(coberturaVO.getEscala(), presenca, dataAtual));
					coberturaVO.setPresenca(presenca);
				}

				Long datiniNew = entradaVO.getDataInicial().getTimeInMillis();
				Long datfimNew = entradaVO.getDataFinal().getTimeInMillis();

				String horarioNew = "";

				if (coberturaVO != null && coberturaVO.getEscala() != null)
				{
					horarioNew = coberturaVO.getEscala().toString();
				}

				//Regra para remover inconsistencia quando colaborador esta afastado e não esteja cobrindo ele mesmo
				if (entradaVO.getColaboradorSubstituido() != null && !coberturaVO.getNumeroCracha().equals(entradaVO.getColaboradorSubstituido().getNumeroCracha()) && coberturaVO.isAcessoBloqueado())
				{
					coberturaVO.getEscala().setConsiderarEscala(Boolean.FALSE);
				}

				//Regra para remover inconsistencia quando colaborador nao emite cartao ponto
				if (!coberturaVO.isControlaAcesso())
				{
					coberturaVO.getEscala().setConsiderarEscala(Boolean.FALSE);
				}

				//Verifica duplicidade no caso de possuir dois acessos na mesma data
				if (!numcraAnt.equals(coberturaVO.getNumeroCracha()) || !datiniAnt.toString().equals(datiniNew.toString()) || !datfimAnt.toString().equals(datfimNew.toString()) || !horarioAnt.toString().equals(horarioNew))
				{
					entradaVO.setColaborador(coberturaVO);
					entradasAutorizadas.add(entradaVO);
					//System.out.println(coberturaVO.getEscala());
				}

				numcraAnt = coberturaVO.getNumeroCracha();
				datiniAnt = entradaVO.getDataInicial().getTimeInMillis();
				datfimAnt = entradaVO.getDataFinal().getTimeInMillis();

				if (entradaVO.getColaborador() != null && entradaVO.getColaborador().getEscala() != null)
				{
					horarioAnt = entradaVO.getColaborador().getEscala().toString();
				}
			}
			rsOperacao.close();
			stOperacao.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
		return entradasAutorizadas;
	}

	public List<ClienteVO> getClientes(Collection<ColaboradorVO> colaboradores, Collection<EntradaAutorizadaVO> entradasAutorizadas, GregorianCalendar dataAtual, Long codcli, Long usu_numpos)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		List<ClienteVO> listaClientes = new ArrayList<ClienteVO>();
		List<PostoVO> listaPostos = new ArrayList<PostoVO>();

		GregorianCalendar dataHoje = new GregorianCalendar();

		String stringDataAtual = NeoDateUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
		String stringDataHoje = NeoDateUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");
		Boolean diaAtual = true;

		//Data diferente da data atual
		if (!stringDataAtual.endsWith(stringDataHoje))
		{
			diaAtual = false;
		}

		try
		{
			/*** Recupera Postos ***/
			StringBuffer queryPostos = new StringBuffer();
			queryPostos.append(" SELECT SUBSTRING(hie.CodLoc, 1, 12) AS NumLocCli, orn.numloc, orn.taborg, hie.codloc, SUBSTRING(hie.CodLoc, 1, 24) AS CodPai, cvs.usu_nomloc, cvs.usu_desser, cvs.usu_numctr, cvs.usu_numpos, orn.usu_lotorn, ");
			queryPostos.append(" 		cvs.usu_codreg, orn.usu_cliorn, cvs.usu_sitcvs, cvs.usu_qtdcon, cvs.usu_numloc, cvs.usu_codccu, cvs.usu_codemp, cvs.usu_codfil, cvs.usu_endloc, ");
			queryPostos.append(" 		cid.NomCid, cid.EstCid, cvs.usu_endcep, orn.usu_cplctr,orn.usu_nomcid, cvs.usu_preuni * cvs.usu_qtdcvs AS valor, cvs.usu_datini, cvs.usu_datfim, cvs.usu_fimvig, ");
			queryPostos.append(" 		cvs.usu_nomloc AS cvs_nomloc, cvs.usu_codccu AS cvs_codccu, cvs.usu_numctr AS cvs_numctr, cvs.usu_numpos AS cvs_numpos, pos.usu_peresc ");
			queryPostos.append(" FROM R016ORN orn WITH (NOLOCK) ");
			queryPostos.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
			queryPostos.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc) ");
			queryPostos.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CVS pos ON pos.usu_codemp = cvs.usu_codemp AND pos.usu_codfil = cvs.usu_codfil AND pos.usu_numctr = cvs.usu_numctr AND pos.usu_numpos = cvs.usu_numpos ");
			queryPostos.append(" LEFT JOIN R074CID cid WITH (NOLOCK) ON cid.CodCid = orn.usu_codcid ");
			queryPostos.append(" WHERE (LEN(hie.CodLoc) = 27) ");
			queryPostos.append(" AND (cvs.USU_SITCVS = 'S' OR cvs.usu_datfim >= ? OR cvs.usu_datfim = '1900-12-31' OR EXISTS (SELECT * FROM R034FUN fun WHERE fun.TIPCOL = 1 AND cvs.usu_numloc = fun.numloc AND cvs.usu_taborg = fun.taborg AND (fun.sitafa <> 7 OR (fun.sitafa = 7 AND fun.datafa > getDate())))) ");
			queryPostos.append(" AND orn.usu_codclisap = ? ");
			if (usu_numpos != null)
			{
				queryPostos.append(" AND cvs.usu_numpos = ? ");
			}
			queryPostos.append(" ORDER BY hie.codloc ");

			PreparedStatement stPostos = connVetorh.prepareStatement(queryPostos.toString());
			stPostos.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
			stPostos.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
			stPostos.setLong(3, codcli);
			if (usu_numpos != null)
			{
				stPostos.setLong(4, usu_numpos);
			}

			Long timeIni = GregorianCalendar.getInstance().getTimeInMillis();
			ResultSet rsPostos = stPostos.executeQuery();
			System.out.println("SQL - SiteUtils.getClientes - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeIni) + "ms");

			String codigoLocalClienteAnterior = "";
			String nomeClienteAnterior = "";
			Boolean primeiroRegistro = true;
			ClienteVO cliente = new ClienteVO();

			String codigoCliente = "";
			String nomeCliente = "";

			while (rsPostos.next())
			{
				codigoCliente = rsPostos.getString("NumLocCli");
				nomeCliente = rsPostos.getString("usu_cliorn");
				PostoVO posto = new PostoVO();

				posto.setNomePosto(rsPostos.getString("usu_nomloc"));
				posto.setCentroCusto(rsPostos.getString("usu_codccu"));
				posto.setCodigoPai(rsPostos.getString("CodPai"));
				posto.setServico(rsPostos.getString("usu_desser"));
				posto.setSituacao((rsPostos.getString("usu_sitcvs").equals("S") ? "Ativo" : "Inativo"));
				posto.setVagas(rsPostos.getLong("usu_qtdcon"));
				posto.setNumeroContrato(rsPostos.getLong("usu_numctr"));
				posto.setNumeroPosto(rsPostos.getLong("usu_numpos"));
				posto.setEndereco(rsPostos.getString("usu_endloc"));
				posto.setCep(rsPostos.getString("usu_endcep"));
				posto.setValor(rsPostos.getBigDecimal("valor"));
				posto.setDataInicio(rsPostos.getDate("usu_datini"));
				posto.setDataFim(rsPostos.getDate("usu_datfim"));
				posto.setDataFimVigencia(rsPostos.getDate("usu_fimvig"));
				posto.setTelefone("");
				posto.setNumeroLocal(rsPostos.getLong("numloc"));
				posto.setCodigoLocal(rsPostos.getString("codloc"));
				posto.setCodigoOrganograma(rsPostos.getLong("taborg"));
				posto.setCidade(rsPostos.getString("nomcid"));
				posto.setEstado(rsPostos.getString("estcid"));
				posto.setLotacao(rsPostos.getString("usu_lotorn"));
				posto.setBairro(rsPostos.getString("usu_nomcid"));
				posto.setNumero(rsPostos.getString("usu_cplctr"));
				posto.setAjusteEscala(rsPostos.getLong("usu_peresc"));

				GregorianCalendar gcDatIni = new GregorianCalendar();
				gcDatIni.setTimeInMillis(rsPostos.getDate("usu_datini").getTime());

				posto.setStrDataInicio(NeoDateUtils.safeDateFormat(gcDatIni, "dd/MM/yyyy"));

				//System.out.println(posto.getNumeroLocal());

				if (posto.getSituacao().equalsIgnoreCase("Ativo"))
				{
					posto.setEscalaPosto(QLPresencaUtils.getJornadaEscalaPosto(rsPostos.getLong("usu_codemp"), rsPostos.getLong("usu_codfil"), rsPostos.getLong("usu_numctr"), rsPostos.getLong("usu_numpos"), null, dataAtual, rsPostos.getLong("usu_peresc"), true));

					GregorianCalendar newDataAtual = (GregorianCalendar) dataAtual.clone();
					newDataAtual.set(Calendar.HOUR_OF_DAY, 0);
					newDataAtual.set(Calendar.MINUTE, 0);
					newDataAtual.set(Calendar.SECOND, 0);
					newDataAtual.set(Calendar.MILLISECOND, 0);

					GregorianCalendar newDataFim = new GregorianCalendar();
					newDataFim.setTime(posto.getDataFim());
					newDataFim.set(Calendar.HOUR_OF_DAY, 0);
					newDataFim.set(Calendar.MINUTE, 0);
					newDataFim.set(Calendar.SECOND, 0);
					newDataFim.set(Calendar.MILLISECOND, 0);

					if (newDataFim.after(newDataAtual))
					{
						posto.setSituacao("Encerrando");
					}
				}

				if (diaAtual && posto.getDataInicio().after(dataAtual.getTime()))
				{
					posto.setSituacao("Iniciando");
				}

				posto.setTelefone(QLPresencaUtils.getTelefonePosto(posto.getCentroCusto()));
				posto.setColaboradores(QLPresencaUtils.getColaboradores(colaboradores, posto, dataAtual));
				posto.setEntradasAutorizadas(QLPresencaUtils.getEntradasAutorizadas(entradasAutorizadas, posto, dataAtual));
				posto.setStatus(QLPresencaUtils.getStatusPosto(posto, dataAtual));

				if (primeiroRegistro)
				{
					codigoLocalClienteAnterior = codigoCliente;
					nomeClienteAnterior = nomeCliente;
					primeiroRegistro = false;
				}

				if (!codigoLocalClienteAnterior.equals(codigoCliente))
				{
					// Mudou o cliente, adiciona a lista de postos (antiga) ao cliente 
					// e o cliente a lista de clientes, renovando o cliente e adicionando o
					// novo posto a nova lista de postos
					cliente = new ClienteVO();
					if (SiteUtils.isClienteHumanas(codcli))
					{
						cliente.setIsCliente("1");
					}
					else
					{
						cliente.setIsCliente("0");
					}
					cliente.setCodigoCliente(codigoLocalClienteAnterior);
					cliente.setNomeCliente(nomeClienteAnterior);
					cliente.setPostos(listaPostos);
					listaClientes.add(cliente);

					listaPostos = new ArrayList<PostoVO>();
					cliente = new ClienteVO();
					codigoLocalClienteAnterior = codigoCliente;
					nomeClienteAnterior = nomeCliente;
				}
				listaPostos.add(posto);
			}
			cliente = new ClienteVO();
			if (SiteUtils.isClienteHumanas(codcli))
			{
				cliente.setIsCliente("1");
			}
			else
			{
				cliente.setIsCliente("0");
			}
			cliente.setCodigoCliente(codigoCliente);
			cliente.setNomeCliente(nomeCliente);
			cliente.setPostos(listaPostos);
			listaClientes.add(cliente);

			rsPostos.close();
			stPostos.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
		return listaClientes;
	}

	/**
	 * Pega a ultima data de mudança de local do colaborador dentro dos postos do cliente.
	 * Esta função permitirá que mostremos no presença site, apenas históricos do colaborado pertinentes
	 * à empresa atual.
	 * 
	 * @return data corte
	 */
	public static GregorianCalendar retornaDataCorteColaborador(Long numEmp, Long tipCol, Long numcad, Long numLoc)
	{

		GregorianCalendar retorno = null;
		Connection connVetorh = PersistEngine.getConnection("VETORH");

		String query = " select min(hlo.datAlt) as dataCorte " + " from R038HLO hlo  " + " INNER JOIN R034FUN fun ON fun.NumEmp = hlo.NumEmp AND fun.NumCad = hlo.NumCad AND fun.TipCol = hlo.TipCol " + " INNER JOIN R016ORN orn ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg " + " inner join R016HIE hie on (orn.numloc = hie.numloc and orn.taborg = hie.taborg) " + " WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? "
				+ " and substring(hie.CodLoc,1,12) = (select substring(CodLoc,1,12) from r016hie where numloc = ?) ";
		try
		{
			PreparedStatement pstData = connVetorh.prepareStatement(query);

			pstData.setLong(1, numEmp);
			pstData.setLong(2, tipCol);
			pstData.setLong(3, numcad);

			pstData.setLong(4, numLoc);

			ResultSet rs = pstData.executeQuery();

			if (rs.next())
			{
				Timestamp t = rs.getTimestamp("dataCorte");
				retorno = new GregorianCalendar();
				retorno.setTimeInMillis(t.getTime());
			}
			rs.close();
			rs = null;
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

	}

	public static List<HistoricoLocalVO> listaHistoricoLocal(Long numemp, Long tipcol, Long numcad, GregorianCalendar datCorte)
	{
		List<HistoricoLocalVO> locais = new ArrayList<HistoricoLocalVO>();
		HistoricoLocalVO local = new HistoricoLocalVO();

		Connection connVetorh = PersistEngine.getConnection("VETORH");

		try
		{
			StringBuffer sqlLocal = new StringBuffer();
			sqlLocal.append(" select fun.NumEmp, fun.TipCol, fun.NumCad, orn.NumLoc, orn.TabOrg, orn.usu_lotorn, orn.nomloc, orn.USU_CodCcu, hlo.DatAlt from R038HLO hlo ");
			sqlLocal.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hlo.NumEmp AND fun.NumCad = hlo.NumCad AND fun.TipCol = hlo.TipCol ");
			sqlLocal.append(" INNER JOIN R016ORN orn ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg ");
			sqlLocal.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? and hlo.datAlt >= ?");
			sqlLocal.append(" ORDER BY hlo.datalt desc ");

			PreparedStatement stLocal = connVetorh.prepareStatement(sqlLocal.toString());
			stLocal.setLong(1, numemp);
			stLocal.setLong(2, tipcol);
			stLocal.setLong(3, numcad);

			Timestamp t = new Timestamp(datCorte.getTimeInMillis());
			stLocal.setTimestamp(4, t);

			ResultSet rsLocal = stLocal.executeQuery();

			//Locals
			while (rsLocal.next())
			{
				local = new HistoricoLocalVO();

				Timestamp data = rsLocal.getTimestamp("DatAlt");
				GregorianCalendar gData = new GregorianCalendar();
				gData.setTimeInMillis(data.getTime());

				ColaboradorVO colaborador = new ColaboradorVO();
				colaborador.setNumeroEmpresa(rsLocal.getLong("NumEmp"));
				colaborador.setTipoColaborador(rsLocal.getLong("Tipcol"));
				colaborador.setNumeroCadastro(rsLocal.getLong("NumCad"));

				PostoVO posto = new PostoVO();
				posto.setNumeroLocal(rsLocal.getLong("NumLoc"));
				posto.setCodigoOrganograma(rsLocal.getLong("TabOrg"));
				posto.setLotacao(rsLocal.getString("usu_lotorn"));
				posto.setNomePosto(rsLocal.getString("NomLoc"));
				posto.setCentroCusto(rsLocal.getString("USU_CodCcu"));

				local.setColaborador(colaborador);
				local.setPosto(posto);
				local.setData(gData);

				locais.add(local);
			}

			rsLocal.close();
			stLocal.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return locais;
	}

	public static List<AfastamentoVO> listaAfastamentos(Long numemp, Long tipcol, Long numcad, GregorianCalendar datCorte)
	{
		List<AfastamentoVO> afastamentos = new ArrayList<AfastamentoVO>();
		AfastamentoVO afastamento = new AfastamentoVO();

		Connection connVetorh = PersistEngine.getConnection("VETORH");

		try
		{
			StringBuffer sqlAfastamento = new StringBuffer();
			sqlAfastamento.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, afa.DatAfa, afa.HorAfa, afa.DatTer, afa.ObsAfa, sit.DesSit FROM R038AFA afa ");
			sqlAfastamento.append(" INNER JOIN R034FUN fun ON fun.NumEmp = afa.NumEmp AND afa.NumCad = fun.NumCad AND fun.TipCol = afa.TipCol ");
			sqlAfastamento.append(" INNER JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
			sqlAfastamento.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? and afa.datAfa >= ? ");
			sqlAfastamento.append(" ORDER BY afa.datafa desc ");

			PreparedStatement stAfastamento = connVetorh.prepareStatement(sqlAfastamento.toString());
			stAfastamento.setLong(1, numemp);
			stAfastamento.setLong(2, tipcol);
			stAfastamento.setLong(3, numcad);

			Timestamp t = new Timestamp(datCorte.getTimeInMillis());
			stAfastamento.setTimestamp(4, t);

			ResultSet rsAfastamento = stAfastamento.executeQuery();

			//Afastamentos
			while (rsAfastamento.next())
			{
				afastamento = new AfastamentoVO();

				Timestamp dataAfaIni = rsAfastamento.getTimestamp("DatAfa");
				GregorianCalendar gDataAfaIni = new GregorianCalendar();
				gDataAfaIni.setTimeInMillis(dataAfaIni.getTime());

				Timestamp dataAfaFim = rsAfastamento.getTimestamp("DatTer");
				GregorianCalendar gDataAfaFim = new GregorianCalendar();
				gDataAfaFim.setTimeInMillis(dataAfaFim.getTime());

				ColaboradorVO colaborador = new ColaboradorVO();
				colaborador.setNumeroEmpresa(rsAfastamento.getLong("NumEmp"));
				colaborador.setTipoColaborador(rsAfastamento.getLong("Tipcol"));
				colaborador.setNumeroCadastro(rsAfastamento.getLong("NumCad"));
				colaborador.setCpf(rsAfastamento.getLong("NumCpf"));

				afastamento.setColaborador(colaborador);
				afastamento.setDataInicio(gDataAfaIni);
				afastamento.setDataFinal(gDataAfaFim);
				afastamento.setObservacao(rsAfastamento.getString("ObsAfa"));
				afastamento.setSituacao(rsAfastamento.getString("DesSit"));

				NeoUser user = PortalUtil.getCurrentUser();
				NeoPaper papel500 = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Exclusao Afastamento Situacao 500"));
				NeoPaper papel15 = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Exclusao Afastamento Situacao 15"));

				Boolean isPapel500 = Boolean.FALSE;
				Boolean isPapel15 = Boolean.FALSE;

				if (user != null && user.getPapers() != null && user.getPapers().contains(papel500))
				{
					isPapel500 = Boolean.TRUE;
				}

				if (user != null && user.getPapers() != null && user.getPapers().contains(papel15))
				{
					isPapel15 = Boolean.TRUE;
				}

				String txtExcluir = "<a title='Excluir Afastamento' style='cursor:pointer' onclick='javascript:deleteAfastamento(" + afastamento.getColaborador().getNumeroEmpresa() + "," + afastamento.getColaborador().getTipoColaborador() + "," + afastamento.getColaborador().getNumeroCadastro() + "," + afastamento.getColaborador().getCpf() + ",\"" + NeoDateUtils.safeDateFormat(afastamento.getDataInicio(), "dd/MM/yyyy") + "\")'><img src='imagens/custom/delete.png'></a>";

				if (user != null && (user.isAdm() || isPapel500) && (afastamento.getSituacao() != null && afastamento.getSituacao().equals("Situação Indefinida (QL)")))
				{
					afastamento.setExcluir(txtExcluir);
				}

				if (user != null && (user.isAdm() || isPapel15) && (afastamento.getSituacao() != null && afastamento.getSituacao().equals("Faltas")))
				{
					afastamento.setExcluir(txtExcluir);
				}

				Long numcpf = rsAfastamento.getLong("NumCpf");
				Long horafa = rsAfastamento.getLong("HorAfa");
				QLEqualsFilter filterCpf = new QLEqualsFilter("numcpf", numcpf);
				QLEqualsFilter filterData = new QLEqualsFilter("datafa", gDataAfaIni);
				QLEqualsFilter filterHora = new QLEqualsFilter("horafa", horafa);

				QLGroupFilter filterGroup = new QLGroupFilter("AND");
				filterGroup.addFilter(filterCpf);
				filterGroup.addFilter(filterData);
				filterGroup.addFilter(filterHora);

				List<NeoObject> tarefas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaAfastamentoJustificativa"), filterGroup);

				if (tarefas != null && !tarefas.isEmpty())
				{
					EntityWrapper tarefaWrapper = new EntityWrapper(tarefas.get(0));
					String tarefa = (String) tarefaWrapper.findValue("tarefa");

					if (NeoUtils.safeIsNull(tarefa))
					{
						continue;
					}

					if (tarefa.equals("Afastamento Informado"))
					{
						afastamento.setLink("<a title='Afastamento informando com antecedência' ><img src='imagens/custom/papel_16x16-info.png'></a>");
						afastamentos.add(afastamento);
						continue;
					}

					QLEqualsFilter filterTarefa = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
					ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, filterTarefa);

					QLEqualsFilter filterModel = new QLEqualsFilter("model", processModel);
					QLEqualsFilter filterCode = new QLEqualsFilter("code", tarefa);

					QLGroupFilter filterGroupWf = new QLGroupFilter("AND");
					filterGroupWf.addFilter(filterCode);
					filterGroupWf.addFilter(filterModel);

					WFProcess process = (WFProcess) PersistEngine.getObject(WFProcess.class, filterGroupWf);

					if (NeoUtils.safeIsNull(process))
					{
						break;
					}

					afastamento.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showTarefa(" + process.getNeoId() + ")'><img src='imagens/custom/preview.png'></a>");
				}

				afastamentos.add(afastamento);
			}

			rsAfastamento.close();
			stAfastamento.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return afastamentos;
	}

	public static List<EntradaAutorizadaVO> listaCoberturas(Long numemp, Long tipcol, Long numcad, GregorianCalendar datCorte)
	{
		List<EntradaAutorizadaVO> entradaAutorizadas = new ArrayList<EntradaAutorizadaVO>();
		EntradaAutorizadaVO entradaAutorizada = new EntradaAutorizadaVO();

		Connection connVetorh = PersistEngine.getConnection("VETORH");

		try
		{
			StringBuffer sqlEntradaAutorizada = new StringBuffer();
			sqlEntradaAutorizada.append(" SELECT TOP(1000) fun.NumEmp AS NumEmpFun, fun.TipCol AS TipColFun, fun.NumCad AS NumCadFun, cob.USU_DatAlt, cob.USU_HorIni, ");
			sqlEntradaAutorizada.append(" 	   cob.USU_DatFim,sub.NumEmp AS NumEmpSub, sub.TipCol AS TipColSub, sub.NumCad As NumCadSub, sub.NomFun AS NomFunSub, mot.USU_DesMot, cob.USU_ObsCob, ");
			sqlEntradaAutorizada.append(" 	   orn.USU_NomCli, orn.USU_LotOrn, orn.NomLoc, orn.USU_CodCcu, hes.CodEsc, hes.CodTma, esc.NomEsc, fun.numcpf");
			sqlEntradaAutorizada.append(" FROM USU_T038COBFUN cob ");
			sqlEntradaAutorizada.append(" INNER JOIN R034FUN fun ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad ");
			sqlEntradaAutorizada.append(" LEFT JOIN R034FUN sub ON sub.NumEmp = cob.USU_NumEmpCob AND sub.TipCol = cob.USU_TipColCob AND sub.NumCad = cob.USU_NumCadCob ");
			sqlEntradaAutorizada.append(" LEFT JOIN R038HES hes ON hes.NumEmp = sub.NumEmp AND hes.TipCol = sub.TipCol AND hes.NumCad = sub.NumCad AND hes.DatAlt = ( ");
			sqlEntradaAutorizada.append(" 	   SELECT MAX (DatAlt) FROM R038HES hes2 WHERE hes2.NumEmp = hes.NumEmp AND hes2.TipCol = hes.TipCol AND hes2.NumCad = hes.NumCad AND hes2.DatAlt <= cob.USU_DatAlt) ");
			sqlEntradaAutorizada.append(" LEFT JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
			sqlEntradaAutorizada.append(" INNER JOIN R016ORN orn ON orn.NumLoc = cob.USU_NumLocTra AND orn.TabOrg = cob.USU_TabOrgTra ");
			sqlEntradaAutorizada.append(" INNER JOIN USU_T010MoCo mot ON mot.USU_CodMot = cob.USU_codMot ");
			sqlEntradaAutorizada.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? AND mot.USU_CodMot not in (4, 5, 8) and cob.USU_DatAlt >= ? ");
			sqlEntradaAutorizada.append(" ORDER BY cob.USU_DatAlt DESC ");

			PreparedStatement stEntradaAutorizada = connVetorh.prepareStatement(sqlEntradaAutorizada.toString());
			stEntradaAutorizada.setLong(1, numemp);
			stEntradaAutorizada.setLong(2, tipcol);
			stEntradaAutorizada.setLong(3, numcad);

			Timestamp t = new Timestamp(datCorte.getTimeInMillis());
			stEntradaAutorizada.setTimestamp(4, t);

			ResultSet rsEntradaAutorizada = stEntradaAutorizada.executeQuery();

			//EntradaAutorizadas
			while (rsEntradaAutorizada.next())
			{
				entradaAutorizada = new EntradaAutorizadaVO();

				Timestamp dataCobIni = rsEntradaAutorizada.getTimestamp("USU_DatAlt");
				GregorianCalendar gDataCobIni = new GregorianCalendar();
				gDataCobIni.setTimeInMillis(dataCobIni.getTime());

				SimpleDateFormat sfinc = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				String dataCoberturaInicial = sfinc.format(dataCobIni.getTime());

				Timestamp dataCobFim = rsEntradaAutorizada.getTimestamp("USU_DatFim");
				GregorianCalendar gDataCobFim = new GregorianCalendar();

				SimpleDateFormat sfFim = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				String dataCoberturaFinal = sfFim.format(dataCobFim.getTime());

				if (dataCobFim != null)
				{
					gDataCobFim.setTimeInMillis(dataCobFim.getTime());
				}

				PostoVO posto = new PostoVO();
				posto.setCentroCusto(rsEntradaAutorizada.getString("USU_CodCcu"));
				posto.setLotacao(rsEntradaAutorizada.getString("USU_LotOrn"));
				posto.setNomePosto(rsEntradaAutorizada.getString("NomLoc"));

				entradaAutorizada.setDataInicial(gDataCobIni);
				entradaAutorizada.setDataFinal(gDataCobFim);
				entradaAutorizada.setHoraInicial(rsEntradaAutorizada.getLong("USU_HorIni"));
				entradaAutorizada.setDescricaoCobertura(rsEntradaAutorizada.getString("USU_DesMot"));
				entradaAutorizada.setPosto(posto);
				entradaAutorizada.setObservacao(rsEntradaAutorizada.getString("USU_ObsCob"));

				ColaboradorVO colaborador = new ColaboradorVO();

				colaborador.setNumeroEmpresa(rsEntradaAutorizada.getLong("NumEmpFun"));
				colaborador.setTipoColaborador(rsEntradaAutorizada.getLong("TipColFun"));
				colaborador.setNumeroCadastro(rsEntradaAutorizada.getLong("NumCadFun"));
				colaborador.setCpf(rsEntradaAutorizada.getLong("numcpf"));

				entradaAutorizada.setColaborador(colaborador);

				ColaboradorVO substituido = new ColaboradorVO();
				EscalaVO escala = new EscalaVO();

				// Se USU_NumCadCob é igual a 0, é cobertura de posto, senão é cobertura de colaborador
				if (rsEntradaAutorizada.getLong("NumCadSub") == 0L)
				{

					escala.setDescricao(NeoDateUtils.safeDateFormat(gDataCobIni, "HH:mm") + "-" + NeoDateUtils.safeDateFormat(gDataCobFim, "HH:mm"));
					substituido.setEscala(escala);
				}
				else
				{
					substituido.setNumeroEmpresa(rsEntradaAutorizada.getLong("NumEmpSub"));
					substituido.setTipoColaborador(rsEntradaAutorizada.getLong("TipColSub"));
					substituido.setNumeroCadastro(rsEntradaAutorizada.getLong("NumCadSub"));
					substituido.setNomeColaborador(rsEntradaAutorizada.getString("NomFunSub"));

					escala.setCodigoEscala(rsEntradaAutorizada.getLong("CodEsc"));
					escala.setCodigoTurma(rsEntradaAutorizada.getLong("CodTma"));
					escala.setDescricao(rsEntradaAutorizada.getString("NomEsc") + " / " + escala.getCodigoTurma());
					substituido.setEscala(escala);
				}
				entradaAutorizada.setColaboradorSubstituido(substituido);

				String txtExcluir = "";

				NeoUser user = PortalUtil.getCurrentUser();
				NeoPaper papelOperador = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Operador do Quadro de Lotação Presença"));

				Boolean isPapelOperador = Boolean.FALSE;

				if (user != null && user.getPapers() != null && user.getPapers().contains(papelOperador))
				{
					isPapelOperador = Boolean.TRUE;
				}

				if (user != null && (user.isAdm() || isPapelOperador))
				{
					txtExcluir = QLPresencaUtils.getLinkBtnDelete(entradaAutorizada);
				}

				SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
				String dataInicial = sf.format(entradaAutorizada.getDataInicial().getTime());

				if (entradaAutorizada.getColaboradorSubstituido() != null)
				{
					if (user != null && (user.isAdm() || isPapelOperador))
					{
						txtExcluir = "<a title='Excluir Cobertura' style='cursor:pointer' onclick='javascript:deleteCobertura(" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "," + entradaAutorizada.getColaborador().getTipoColaborador() + "," + entradaAutorizada.getColaborador().getNumeroCadastro() + ",\"" + dataInicial + "\"," + entradaAutorizada.getHoraInicial() + "," + entradaAutorizada.getColaboradorSubstituido().getNumeroEmpresa() + ","
								+ entradaAutorizada.getColaboradorSubstituido().getTipoColaborador() + "," + entradaAutorizada.getColaboradorSubstituido().getNumeroCadastro() + ", " + colaborador.getCpf() + ", \"" + entradaAutorizada.getDescricaoCobertura() + "\" ,\"" + dataCoberturaFinal + "\" ,\"" + dataCoberturaInicial + "\")'><img src='imagens/custom/delete.png'></a>";
					}
				}
				else
				{
					if (user != null && (user.isAdm() || isPapelOperador))
					{
						txtExcluir = "<a title='Excluir Cobertura' style='cursor:pointer' onclick='javascript:deleteCobertura(" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "," + entradaAutorizada.getColaborador().getTipoColaborador() + "," + entradaAutorizada.getColaborador().getNumeroCadastro() + ",\"" + dataInicial + "\"," + entradaAutorizada.getHoraInicial() + ", '', '', '', " + colaborador.getCpf() + ", \"" + entradaAutorizada.getDescricaoCobertura() + "\", \""
								+ dataCoberturaFinal + "\", \"" + dataCoberturaInicial + "\")'><img src='imagens/custom/delete.png'></a>";
					}
				}

				entradaAutorizada.setExcluir(txtExcluir);

				entradaAutorizadas.add(entradaAutorizada);
			}

			rsEntradaAutorizada.close();
			stEntradaAutorizada.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return entradaAutorizadas;
	}

	public static List<HistoricoEscalaVO> listaHistoricoEscala(Long numemp, Long tipcol, Long numcad, GregorianCalendar datCorte)
	{
		List<HistoricoEscalaVO> escalas = new ArrayList<HistoricoEscalaVO>();
		HistoricoEscalaVO escala = new HistoricoEscalaVO();

		Connection connVetorh = PersistEngine.getConnection("VETORH");

		try
		{
			StringBuffer sqlEscala = new StringBuffer();
			sqlEscala.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.numcpf, esc.CodEsc, esc.NomEsc, hes.CodTma, hes.DatAlt from R038HES hes ");
			sqlEscala.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hes.NumEmp AND fun.NumCad = hes.NumCad AND fun.TipCol = hes.TipCol ");
			sqlEscala.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
			sqlEscala.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? and hes.DatAlt >= ? ");
			sqlEscala.append(" ORDER BY hes.datalt desc ");

			PreparedStatement stEscala = connVetorh.prepareStatement(sqlEscala.toString());
			stEscala.setLong(1, numemp);
			stEscala.setLong(2, tipcol);
			stEscala.setLong(3, numcad);

			Timestamp t = new Timestamp(datCorte.getTimeInMillis());
			stEscala.setTimestamp(4, t);

			ResultSet rsEscala = stEscala.executeQuery();

			//Escalas
			while (rsEscala.next())
			{
				escala = new HistoricoEscalaVO();

				Timestamp data = rsEscala.getTimestamp("DatAlt");
				GregorianCalendar gData = new GregorianCalendar();
				gData.setTimeInMillis(data.getTime());

				ColaboradorVO colaborador = new ColaboradorVO();
				colaborador.setNumeroEmpresa(rsEscala.getLong("NumEmp"));
				colaborador.setTipoColaborador(rsEscala.getLong("Tipcol"));
				colaborador.setNumeroCadastro(rsEscala.getLong("NumCad"));
				colaborador.setCpf(rsEscala.getLong("NumCpf"));

				EscalaVO esc = new EscalaVO();
				esc.setCodigoEscala(rsEscala.getLong("CodEsc"));
				esc.setDescricao(rsEscala.getString("NomEsc") + " / " + rsEscala.getLong("CodTma"));

				escala.setColaborador(colaborador);
				escala.setEscala(esc);
				escala.setData(gData);

				NeoUser user = PortalUtil.getCurrentUser();
				NeoPaper papelOperador = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Operador do Quadro de Lotação Presença"));
				String txtExcluir = "";

				Boolean isPapelOperador = Boolean.FALSE;

				if (user != null && user.getPapers() != null && user.getPapers().contains(papelOperador))
				{
					isPapelOperador = Boolean.TRUE;
				}

				if (user != null && (user.isAdm() || isPapelOperador))
				{
					txtExcluir = "<a title='Excluir histórico de escala' style='cursor:pointer' onclick='javascript:deleteHistoricoEscala(" + colaborador.getNumeroEmpresa() + "," + colaborador.getTipoColaborador() + "," + colaborador.getNumeroCadastro() + "," + colaborador.getCpf() + ",\"" + NeoDateUtils.safeDateFormat(gData, "dd/MM/yyyy") + "\")'><img src='imagens/custom/delete.png'></a>";
				}

				escala.setExcluir(txtExcluir);

				escalas.add(escala);
			}

			rsEscala.close();
			stEscala.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return escalas;
	}

	public static Boolean isClienteHumanas(Long usu_codcli)
	{
		Boolean retorno = false;

		StringBuilder sql = new StringBuilder();

		System.out.println("cliente:" + usu_codcli);

		sql.append(" select 1 from e085cli cli ");
		sql.append(" 		join usu_t160ctr ctr on (cli.codcli = ctr.usu_codcli and ctr.usu_sitctr = 'A' ) ");
		sql.append(" where 1=1 and codcli = ? ");
		sql.append(" and exists ( ");
		sql.append(" select 1 ");
		sql.append(" from usu_t160cvs c ");
		sql.append(" where 1=1 and c.usu_sitcvs = 'A' ");
		sql.append(" and c.usu_numctr = ctr.usu_numctr ");
		sql.append(" and c.usu_codemp = ctr.usu_codemp ");
		sql.append(" and exists (select 1 from e080ser where codfam in ('SER101','SER109') and codser = c.usu_codser ) ) ");

		Connection conSapiens = PersistEngine.getConnection("SAPIENS");

		try
		{
			PreparedStatement pst = conSapiens.prepareStatement(sql.toString());

			pst.setLong(1, usu_codcli);

			ResultSet rs = pst.executeQuery();
			if (rs.next())
			{
				retorno = true;
			}
			rs.close();
			rs = null;
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			try
			{
				conSapiens.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

	}

	public static boolean isPresencaLiberadoSite(String codcli)
	{

		try
		{
			//presencaSiteLiberado
			QLGroupFilter filtro = new QLGroupFilter("AND");
			filtro.addFilter(new QLEqualsFilter("codigoCliente", codcli));

			List<NeoObject> liberacoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("presencaSiteLiberado"), filtro);
			if (liberacoes != null && !liberacoes.isEmpty())
			{

				NeoObject liberacao = liberacoes.get(0);
				EntityWrapper wLiberacao = new EntityWrapper(liberacao);

				if (NeoUtils.safeBoolean(wLiberacao.findValue("situacao")))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("##### ERRO siteUtils.isPresencaLiberadoSite - Msg: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			return false;
		}
	}

	public static Long retornaCodigoClienteSapiens(Long numLoc, Long tabOrg)
	{

		Long retorno = null;
		Connection connVetorh = PersistEngine.getConnection("VETORH");

		String query = " select R016ORN.usu_codclisap from R016ORN where numloc = ? and taborg = ? ";

		try
		{
			PreparedStatement pstData = connVetorh.prepareStatement(query);

			pstData.setLong(1, numLoc);
			pstData.setLong(2, tabOrg);

			ResultSet rs = pstData.executeQuery();

			if (rs.next())
			{
				retorno = rs.getLong("usu_codclisap");
			}
			rs.close();
			rs = null;
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

	}

	/**
	 * Retorna a competencia no formato desejado
	 * 
	 * @param data
	 * @return
	 */
	public static String getCompetencia(GregorianCalendar data, String formato)
	{
		String competencia = "";
		GregorianCalendar newData = (GregorianCalendar) data.clone();

		if (newData.get(GregorianCalendar.DAY_OF_MONTH) > 15)
		{
			newData.add(Calendar.MONTH, +1);
			competencia = NeoDateUtils.safeDateFormat(newData, formato);
		}
		else
		{
			competencia = NeoDateUtils.safeDateFormat(newData, formato);
		}

		return competencia;
	}

	public static String recuperaIntnetcliente(Long codcli)
	{
		String retorno = null;
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");

		String query = " select intnet from e085cli where codcli = ? ";

		try
		{
			PreparedStatement pstData = connSapiens.prepareStatement(query);

			pstData.setLong(1, codcli);

			ResultSet rs = pstData.executeQuery();

			if (rs.next())
			{
				retorno = rs.getString("intnet");
			}
			rs.close();
			rs = null;
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				connSapiens.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
	}

	public static Boolean atualizaEmailCli(Long codcli, String email)
	{
		Boolean retorno = false;
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");

		String query = " update e085cli set intnet = ?  where codcli = ? ";

		PreparedStatement pstData = null;

		try
		{
			pstData = connSapiens.prepareStatement(query);

			pstData.setString(1, email);
			pstData.setLong(2, codcli);

			int ret = pstData.executeUpdate();

			if (ret > 0)
			{
				retorno = true;
			}
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}
	}

	public static Boolean gravaLogAlteracaoCli(Long codcli, String numcpf, String log, GregorianCalendar dataAtual)
	{
		Long cpf = NeoUtils.safeLong(numcpf.replace(".", "").replace("/", "").replace("-", ""));
		Boolean retorno = false;
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");

		String query = " INSERT INTO dbo.usu_t038cli ( usu_codcli, usu_cgccpf, usu_intnet, usu_datger, usu_horger ) VALUES ( ?,?,?,?,?) ";

		PreparedStatement pstData = null;

		try
		{
			pstData = connSapiens.prepareStatement(query);

			pstData.setLong(1, codcli);
			pstData.setLong(2, cpf);
			pstData.setString(3, log);
			Date dataGer = new Date(dataAtual.getTimeInMillis());
			pstData.setDate(4, dataGer);
			Long hora = (dataAtual.get(GregorianCalendar.HOUR) * 60L) + dataAtual.get(GregorianCalendar.MINUTE);
			pstData.setLong(5, hora);

			int ret = pstData.executeUpdate();

			if (ret > 0)
			{
				retorno = true;
			}
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}
	}

	/**
	 * Retorna os parametros de aceito do cliente para preenchimento dos checkboxes no site na lista de
	 * documentos e lista de NFs e boletos.
	 * 
	 * @param codcli
	 * @return array com duas posição booleanas - [0] - aceite de NF, [1] - aceite de documentos
	 */
	public static Boolean[] recuperaAceiteNfDoc(Long codcli)
	{
		Boolean retorno[] = { false, false };

		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("codcli", codcli));
		ArrayList<NeoObject> aceitesExistentes = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SiteAceiteNFDOC"), gp);
		if (aceitesExistentes != null && !aceitesExistentes.isEmpty())
		{

			for (NeoObject oAceite : aceitesExistentes)
			{
				EntityWrapper wAceite = new EntityWrapper(oAceite);
				retorno[0] = NeoUtils.safeBoolean(NeoUtils.safeOutputString(wAceite.findValue("recnf")));
				retorno[1] = NeoUtils.safeBoolean(NeoUtils.safeOutputString(wAceite.findValue("recdoc")));
			}
		}

		return retorno;
	}

	public static Boolean atualizaCodFenUsut160CTR(Long codcli)
	{
		Boolean retorno = false;
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");

		String query = " update usu_t160ctr set usu_codfen = 1  where usu_codcli = ? and usu_sitctr = 'A' ";

		PreparedStatement pstData = null;

		try
		{
			pstData = connSapiens.prepareStatement(query);

			pstData.setLong(1, codcli);

			int ret = pstData.executeUpdate();

			if (ret > 0)
			{
				retorno = true;
			}

			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}
	}

	/**
	 * Atualiza (concatena) adicionados pelo cliente aos e-mails do contrato. Utilizado pelo site no
	 * aceite de nota fiscal
	 * 
	 * @param codcli
	 * @param email
	 * @return
	 */
	public static Boolean atualizaEmailContratos(Long codcli, String email)
	{
		Boolean retorno = false;
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");
		ResultSet rs = null;
		String query = " select usu_numctr,  usu_emactr from usu_t160ctr where usu_codcli = ? and usu_sitctr ='A' "; //" update usu_t160ctr set usu_codfen = 1  where usu_codcli = ? and usu_sitctr = 'A' ";
		String updateQuery = " update usu_t160ctr set usu_emactr = ?  where usu_codcli = ? and usu_numctr = ? and usu_sitctr = 'A' ";

		PreparedStatement pstUpdate = null;
		PreparedStatement pstData = null;

		try
		{
			pstData = connSapiens.prepareStatement(query);

			pstData.setLong(1, codcli);

			rs = pstData.executeQuery();
			HashMap<Long, String> emailsContrato = new HashMap<Long, String>();
			while (rs.next())
			{
				emailsContrato.put(rs.getLong("usu_numctr"), rs.getString("usu_emactr"));
			}

			Iterator<Long> ite = emailsContrato.keySet().iterator();

			if (ite != null && ite.hasNext())
			{
				do
				{
					Long numctr = ite.next();
					pstUpdate = connSapiens.prepareStatement(updateQuery);
					pstUpdate.setString(1, NeoUtils.safeOutputString(emailsContrato.get(numctr)).trim() + ";"+ email);
					pstUpdate.setLong(2, codcli);
					pstUpdate.setLong(3, numctr);

					int ret = pstUpdate.executeUpdate();

					if (ret > 0)
					{
						retorno = true;
					}
					pstUpdate.close();

				}
				while (ite.hasNext());
			}

			return retorno;

		}
		catch (Exception e)
		{
			System.out.println("Erro atualizaEmailContratos - codcli:" + codcli);
			e.printStackTrace();
			return null;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstData, rs);
		}
	}

	/**
	 * Gera Arquivo pdf com a ficha ponto. Se codcli for diferente de null, traz todas fichas de todos
	 * colaboradores alocados nos postos do cliente.
	 * 
	 * @param codcli - codigo do cliente
	 * @param numemp - numero da empresa
	 * @param numcad - numero da matricula
	 * @param tipcol - tipo do colaborador
	 * @param perref - periodo de referencia
	 * @return
	 */
	public static File geraFichaPontoPDF(Long codcli, Long numemp, Long numcad, Long tipcol, String perref)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		InputStream is = null;
		String dir_report = "";

		GregorianCalendar mesRef = new GregorianCalendar();
		try
		{
			mesRef = OrsegupsUtils.stringToGregorianCalendar("01" + perref, "ddMMyyyy");
		}
		catch (ParseException e1)
		{
			e1.printStackTrace();
		}

		System.out.println("Site - Imprimindo ficha ponto -> codcli=" + codcli + ", numemp=" + numemp + ", numcad=" + numcad + ", tipcol=" + tipcol + ",perref=" + perref);

		try
		{
			ArrayList<CartaoPontoDataSource> lcpds = new ArrayList<CartaoPontoDataSource>();
			dir_report = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "repFichaPonto.jasper";

			is = new BufferedInputStream(new FileInputStream(dir_report));
			if (paramMap != null)
			{

				File file = File.createTempFile("repFichaPonto_" + GregorianCalendar.getInstance().getTimeInMillis(), ".pdf");
				file.deleteOnExit();
				JasperPrint impressao = null;
				//Collection<CartaoPontoMarcacoesDataSource> listaDeMarcacoes = new ArrayList<CartaoPontoMarcacoesDataSource>();

				lcpds = (ArrayList<CartaoPontoDataSource>) listaCpfColaboradores(codcli, mesRef);

				if (lcpds != null && !lcpds.isEmpty())
				{
					for (CartaoPontoDataSource cartao : lcpds)
					{
						cartao.setListaDeMarcacoes(SiteUtils.listaMarcacoes(cartao.getCpf(), mesRef));
						cartao.setHorex(OrsegupsUtils.getHorario(SiteUtils.retornaSaldoColaboradorCPT(NeoUtils.safeLong(cartao.getNumcad()), NeoUtils.safeLong(cartao.getNumemp()), NeoUtils.safeLong(cartao.getTipcol()), mesRef)));
						//cartao.setHorex(OrsegupsUtils.getHorario(SiteUtils.retornaSaldoColaboradorCPT(NeoUtils.safeLong("768"), NeoUtils.safeLong("18"), NeoUtils.safeLong("1"), mesRef)));

					}
				}

				String dir_marcacoes = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "repFichaPonto_sub_marcacoes.jasper";
				paramMap.put("dir_marcacoes", dir_marcacoes);

				System.out.println("Lista de marcações:" + lcpds.size());

				JRBeanCollectionDataSource jrdts = new JRBeanCollectionDataSource(lcpds);
				impressao = JasperFillManager.fillReport(is, paramMap, jrdts);

				if (impressao != null && file != null)
				{
					JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
					return file;
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ao gerar o PDF do Relatório de Ficha Ponto dos Colaboradores!!");
		}
		return null;
	}

	private static List<CartaoPontoDataSource> listaCpfColaboradores(Long usuCodcli, GregorianCalendar mesRef)
	{
		ArrayList<CartaoPontoDataSource> cpfs = new ArrayList<CartaoPontoDataSource>();
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		StringBuffer queryColaborador = new StringBuffer();
		PreparedStatement stColaborador = null;
		ResultSet rsColaborador = null;
		try
		{

			queryColaborador.append(" SELECT fun.NumEmp, fun.NumCpf, fun.numcad, fun.TipCol, fun.nomfun, (cast(fun.numctp as varchar)+' '+ cast(fun.serctp as varchar)) numctp, car.TitCar, fun.datadm "); // , cal.iniapu, cal.fimapu, cal.perref 
			queryColaborador.append(" FROM R034FUN fun ");
			queryColaborador.append("         INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= ?) ");
			queryColaborador.append("         INNER JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= DATEADD(dd, 0, DATEDIFF(dd, 0, ?)) ) ");
			queryColaborador.append("         INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");
			//queryColaborador.append("         INNER JOIN R044CAL cal WITH (NOLOCK) ON ( DATEADD(dd, 0, DATEDIFF(dd, 0, ?)) between iniapu and fimapu and cal.numemp = fun.numemp) "); 
			queryColaborador.append("         INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = hlo.TabOrg ");
			queryColaborador.append("         LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= ? AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime)))) ");
			queryColaborador.append(" WHERE fun.DatAdm <= ? ");
			queryColaborador.append("         AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(? as float)) as datetime))) ");
			queryColaborador.append("         AND fun.TipCol = 1 ");
			queryColaborador.append("         AND fun.NumEmp not in (5, 11, 14) ");
			queryColaborador.append("         AND orn.usu_codclisap = ? ");
			queryColaborador.append(" ORDER BY fun.NumEmp, fun.NomFun ");

			stColaborador = connVetorh.prepareStatement(queryColaborador.toString());

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTimeInMillis(mesRef.getTimeInMillis());
			gc.set(GregorianCalendar.DAY_OF_MONTH, gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));

			stColaborador.setDate(1, new Date(gc.getTimeInMillis()));
			stColaborador.setDate(2, new Date(gc.getTimeInMillis()));
			stColaborador.setDate(3, new Date(gc.getTimeInMillis()));
			stColaborador.setDate(4, new Date(gc.getTimeInMillis()));
			stColaborador.setDate(5, new Date(gc.getTimeInMillis()));
			stColaborador.setDate(6, new Date(gc.getTimeInMillis()));
			//stColaborador.setDate(7, new Date(gc.getTimeInMillis()));
			stColaborador.setLong(7, usuCodcli);

			rsColaborador = stColaborador.executeQuery();
			while (rsColaborador.next())
			{
				CartaoPontoDataSource cp = new CartaoPontoDataSource();
				cp.setCpf(rsColaborador.getString("NumCpf"));
				cp.setNumcad(rsColaborador.getString("numcad"));
				cp.setNumemp(rsColaborador.getString("NumEmp"));
				cp.setTipcol(rsColaborador.getString("TipCol"));
				cp.setNomfun(rsColaborador.getString("nomfun"));
				cp.setCtps(rsColaborador.getString("numctp"));
				cp.setCargo(rsColaborador.getString("TitCar"));

				GregorianCalendar gcAdm = new GregorianCalendar();
				gcAdm.setTimeInMillis(rsColaborador.getDate("datadm").getTime());
				cp.setAdmissao(NeoDateUtils.safeDateFormat(gcAdm, "dd/MM/yyyy"));

				GregorianCalendar datIni = new GregorianCalendar();
				GregorianCalendar datFim = new GregorianCalendar();
				datIni.setTimeInMillis(mesRef.getTimeInMillis());
				datFim.setTimeInMillis(mesRef.getTimeInMillis());
				datIni.set(GregorianCalendar.DATE, 1);
				datFim.set(GregorianCalendar.DAY_OF_MONTH, datFim.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));

				String strPeriodo = NeoDateUtils.safeDateFormat(datIni, "dd/MM/yyyy") + " à " + NeoDateUtils.safeDateFormat(datFim, "dd/MM/yyyy");
				cp.setPeriodo(strPeriodo);

				cp.setMesref(NeoDateUtils.safeDateFormat(mesRef, "MM/yyyy"));

				cp.setHorex(NeoUtils.safeOutputString(retornaSaldoColaborador(rsColaborador.getLong("numcad"), rsColaborador.getLong("NumEmp"), rsColaborador.getLong("TipCol"), 0)));
				cpfs.add(cp);
			}
			//rsColaborador.close();
			//stColaborador.close();
			return cpfs;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ao consultar colaboradores da Empresa" + e.getMessage());
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
			}
			catch (Exception e1)
			{
				e.printStackTrace();
			}
			return null;
		}

	}

	private static List<CartaoPontoMarcacoesDataSource> listaMarcacoes(String numCpf, GregorianCalendar mesRef)
	{
		ArrayList<CartaoPontoMarcacoesDataSource> listaPonto = new ArrayList<CartaoPontoMarcacoesDataSource>();
		Connection connVetorh = PersistEngine.getConnection("VETORH");

		StringBuffer sqlAcesso = new StringBuffer();
		sqlAcesso.append(" SELECT acc.DatAcc, acc.HorAcc, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data, acc.dirAcc, acc.codrlg, fun.numcad, fun.numemp, fun.tipcol, jma.DesJMa, acc.usu_obsjma, acc.TipAcc, tac.DesTAc FROM R070ACC acc ");
		sqlAcesso.append(" inner join R070TAC tac on (acc.tipacc = tac.TipAcc) ");
		sqlAcesso.append(" left join r076jma jma on jma.CodJMa = acc.usu_codjma ");
		sqlAcesso.append(" INNER JOIN R038HCH hch ON hch.NumCra = acc.NumCra ");
		sqlAcesso.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol ");
		sqlAcesso.append(" WHERE fun.NumCpf = ? AND acc.CodPlt = 2 AND (acc.TipAcc >= 100 or (acc.TipAcc = 1 and acc.oriacc = 'R')) AND acc.DatAcc >= ? AND acc.DatAcc <= ? ");
		sqlAcesso.append(" GROUP BY acc.DatAcc, acc.HorAcc, acc.dirAcc, acc.codrlg, fun.numcad, fun.numemp, fun.tipcol, jma.DesJMa, acc.usu_obsjma, acc.TipAcc, tac.DesTAc  ");
		//sqlAcesso.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc, acc.HorAcc asc ");
		sqlAcesso.append(" ORDER BY  acc.DatAcc desc,acc.HorAcc asc ");

		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{

			GregorianCalendar datIni = new GregorianCalendar();
			GregorianCalendar datFim = new GregorianCalendar();
			datIni.setTimeInMillis(mesRef.getTimeInMillis());
			datIni.set(GregorianCalendar.DATE, 1);

			datFim.setTimeInMillis(mesRef.getTimeInMillis());
			datFim.set(GregorianCalendar.DAY_OF_MONTH, mesRef.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));

			System.out.println("[" + numCpf + "]Período referência de:" + NeoDateUtils.safeDateFormat(datIni, "dd/MM/yyyy") + " à " + NeoDateUtils.safeDateFormat(datFim, "dd/MM/yyyy"));

			st = connVetorh.prepareStatement(sqlAcesso.toString());
			st.setString(1, numCpf);
			st.setDate(2, new Date(datIni.getTimeInMillis()));
			st.setDate(3, new Date(datFim.getTimeInMillis()));

			rs = st.executeQuery();
			Long dia = 0L;

			CartaoPontoMarcacoesDataSource cpds = new CartaoPontoMarcacoesDataSource();
			String marcacoes = "";
			while (rs.next())
			{
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeInMillis(rs.getTime("data").getTime());
				//System.out.println("Data :" + NeoDateUtils.safeDateFormat(gc,"dd/MM/yyyy HH:mm"));

				if (dia == 0L)
				{
					dia = rs.getDate("DatAcc").getTime();
					GregorianCalendar gcDatAcc = new GregorianCalendar();
					gcDatAcc.setTimeInMillis(rs.getTime("data").getTime());
					marcacoes += "  " + NeoDateUtils.safeDateFormat(gcDatAcc, "HH:mm");
					//System.out.print(NeoDateUtils.safeDateFormat(gcDatAcc,"HH:mm") + " ");

					GregorianCalendar gcData = new GregorianCalendar();
					gcData.setTimeInMillis(rs.getDate("DatAcc").getTime());
					cpds.setData(NeoDateUtils.safeDateFormat(gcData, "dd/MM/yyyy"));

					GregorianCalendar gcDiaSemana = new GregorianCalendar();
					gcDiaSemana.setTimeInMillis(rs.getDate("DatAcc").getTime());

					int diaSemana = gcDiaSemana.get(GregorianCalendar.DAY_OF_WEEK);

					switch (diaSemana)
					{
						case 1:
							cpds.setDiaSemana("domingo");
							break;
						case 2:
							cpds.setDiaSemana("segunda");
							break;
						case 3:
							cpds.setDiaSemana("terça");
							break;
						case 4:
							cpds.setDiaSemana("quarta");
							break;
						case 5:
							cpds.setDiaSemana("quinta");
							break;
						case 6:
							cpds.setDiaSemana("sexta");
							break;
						case 7:
							cpds.setDiaSemana("sábado");
							break;

						default:
							break;
					}

				}
				else if (dia != rs.getDate("DatAcc").getTime())
				{
					dia = rs.getDate("DatAcc").getTime();

					//System.out.println();

					cpds.setMarcacoes(marcacoes);
					listaPonto.add(cpds);
					marcacoes = "";
					cpds = new CartaoPontoMarcacoesDataSource();

					GregorianCalendar gcDatAcc = new GregorianCalendar();
					gcDatAcc.setTimeInMillis(rs.getTime("data").getTime());
					marcacoes += "  " + NeoDateUtils.safeDateFormat(gcDatAcc, "HH:mm");

					GregorianCalendar gcData = new GregorianCalendar();
					gcData.setTimeInMillis(rs.getDate("DatAcc").getTime());
					cpds.setData(NeoDateUtils.safeDateFormat(gcData, "dd/MM/yyyy"));

					GregorianCalendar gcDiaSemana = new GregorianCalendar();
					gcDiaSemana.setTimeInMillis(rs.getDate("DatAcc").getTime());

					int diaSemana = gcDiaSemana.get(GregorianCalendar.DAY_OF_WEEK);

					switch (diaSemana)
					{
						case 1:
							cpds.setDiaSemana("domingo");
							break;
						case 2:
							cpds.setDiaSemana("segunda");
							break;
						case 3:
							cpds.setDiaSemana("terça");
							break;
						case 4:
							cpds.setDiaSemana("quarta");
							break;
						case 5:
							cpds.setDiaSemana("quinta");
							break;
						case 6:
							cpds.setDiaSemana("sexta");
							break;
						case 7:
							cpds.setDiaSemana("sábado");
							break;

						default:
							break;
					}

					//System.out.print(NeoDateUtils.safeDateFormat(gcDatAcc,"HH:mm") + " ");

				}
				else
				{
					GregorianCalendar gcDatAcc = new GregorianCalendar();
					gcDatAcc.setTimeInMillis(rs.getTime("data").getTime());
					marcacoes += "  " + NeoDateUtils.safeDateFormat(gcDatAcc, "HH:mm");
					//System.out.print("["+NeoDateUtils.safeDateFormat(gcDatAcc,"HH:mm") + "] ");
				}

			}
			//System.out.println();
			return listaPonto;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				OrsegupsUtils.closeConnection(connVetorh, st, rs);
			}
			catch (Exception e1)
			{
				e.printStackTrace();
			}
			return null;
		}

	}

	/**
	 * Utilizar mostrarDocumento(Long codcli, Long codigoDocumento)}
	 * 
	 * @param codcli
	 * @return
	 */
	@Deprecated
	public static boolean mostrarCartaoPonto(Long codcli)
	{
		Boolean ret = false;
		Connection conn = PersistEngine.getConnection("SAPIENS");

		StringBuffer sql = new StringBuffer();
		sql.append(" select distinct(cli.codcli) from e085cli cli ");
		sql.append(" join usu_t160ctr ctr on (ctr.usu_codcli = cli.codcli) ");
		sql.append(" join usu_t160doc doc on (ctr.usu_numctr = doc.usu_numctr and ctr.usu_codemp = doc.usu_codemp and ctr.usu_codfil = doc.usu_codfil) ");
		sql.append(" where doc.usu_coddoc = 16 and cli.codcli = ? "); // usu_coddoc = 16 -> cartao ponto

		PreparedStatement st;
		try
		{

			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codcli);

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				ret = true;
			}
			rs.close();
			st.close();
			return ret;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.close();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();

			}
			return ret;
		}

	}

	/**
	 * Utilizar mostrarDocumento(Long codcli, Long codigoDocumento)}
	 * 
	 * @param codcli
	 * @return
	 */
	@Deprecated
	public static boolean mostrarRET(Long codcli)
	{
		Boolean ret = false;
		Connection conn = PersistEngine.getConnection("SAPIENS");

		StringBuffer sql = new StringBuffer();
		sql.append(" select distinct(cli.codcli) from e085cli cli ");
		sql.append(" join usu_t160ctr ctr on (ctr.usu_codcli = cli.codcli) ");
		sql.append(" join usu_t160doc doc on (ctr.usu_numctr = doc.usu_numctr and ctr.usu_codemp = doc.usu_codemp and ctr.usu_codfil = doc.usu_codfil) ");
		sql.append(" where doc.usu_coddoc = 30 and cli.codcli = ? "); // usu_coddoc = 30 -> Documento RET

		PreparedStatement st;
		try
		{

			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codcli);

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				ret = true;
			}
			rs.close();
			st.close();
			return ret;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.close();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();

			}
			return ret;
		}

	}

	/**
	 * Verifica se o cliente tem o documento
	 * 
	 * @param codcli
	 * @param codigoDocumento
	 * @return
	 */
	public static boolean mostrarDocumento(Long codcli, Long numemp, Long codigoDocumento)
	{
		Boolean ret = false;
		Connection conn = PersistEngine.getConnection("SAPIENS");

		StringBuffer sql = new StringBuffer();
		sql.append(" select distinct(cli.codcli) from e085cli cli ");
		sql.append(" join usu_t160ctr ctr on (ctr.usu_codcli = cli.codcli  and ( (ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate()-(30*6) ) ) ) ");
		sql.append(" join usu_t160doc doc on (ctr.usu_numctr = doc.usu_numctr and ctr.usu_codemp = doc.usu_codemp and ctr.usu_codfil = doc.usu_codfil and doc.usu_coddoc = ?) ");
		sql.append(" where cli.codcli = ? and ctr.usu_codemp = ? ");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{

			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codigoDocumento);
			st.setLong(2, codcli);
			st.setLong(3, numemp);

			rs = st.executeQuery();

			if (rs.next())
			{
				ret = true;
			}

			OrsegupsUtils.closeConnection(conn, st, rs);

			return ret;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			OrsegupsUtils.closeConnection(conn, st, rs);
			return ret;
		}

	}
	
	/**
	 * retorna os tipos de documentos de um cliente para mostrar no site ou não.
	 * 
	 * @param codcli
	 * @param codigoDocumento
	 * @return
	 */
	public static List<SiteDocsMostrarVO> loadTiposDocumentos(Long codcli)
	{
		List<SiteDocsMostrarVO> listaDocsCliente = new ArrayList<SiteDocsMostrarVO>();
		
		Boolean ret = false;
		Connection conn = PersistEngine.getConnection("SAPIENS");

		StringBuffer sql = new StringBuffer();
		sql.append(" select doc.USU_CodDoc, doc.USU_CodEmp, cli.codcli from e085cli cli "); 
		sql.append("  join usu_t160ctr ctr on (ctr.usu_codcli = cli.codcli  and ( (ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate()-(30*6) ) ) ) "); 
		sql.append("  join usu_t160doc doc on (ctr.usu_numctr = doc.usu_numctr and ctr.usu_codemp = doc.usu_codemp and ctr.usu_codfil = doc.usu_codfil)  ");
		sql.append("  where cli.codcli = ?");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{

			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codcli);

			rs = st.executeQuery();

			while (rs.next())
			{
				listaDocsCliente.add(new SiteDocsMostrarVO(rs.getLong("USU_CodDoc"), rs.getLong("USU_CodEmp"), rs.getLong("codcli")));
			} 

			OrsegupsUtils.closeConnection(conn, st, rs);

			return listaDocsCliente;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			OrsegupsUtils.closeConnection(conn, st, rs);
			return listaDocsCliente;
		}

	}

	/**
	 * Retorna codigos dos documetos que o cliente tem cadastrado em seus contratos do sapiens
	 * 
	 * @param codcli
	 * @param numemp
	 * @param codigoDocumento
	 * @return
	 */
	public static List<ClienteDocumentosSapiensVO> documentosClienteSapiens(Long codcli, Long numemp)
	{

		List<ClienteDocumentosSapiensVO> retorno = new ArrayList<ClienteDocumentosSapiensVO>();

		Connection conn = PersistEngine.getConnection("SAPIENS");

		StringBuffer sql = new StringBuffer();
		sql.append(" select doc.usu_coddoc, doc.USU_CodEmp, doc.USU_CodFil from e085cli cli ");
		sql.append(" join usu_t160ctr ctr on (ctr.usu_codcli = cli.codcli  and ( (ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate()-(30*6) ) ) ) ");
		sql.append(" join usu_t160doc doc on (ctr.usu_numctr = doc.usu_numctr and ctr.usu_codemp = doc.usu_codemp and ctr.usu_codfil = doc.usu_codfil ) ");
		sql.append(" where cli.codcli = ? and ctr.usu_codemp = ? ");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{

			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codcli);
			st.setLong(2, numemp);

			rs = st.executeQuery();

			while (rs.next())
			{
				ClienteDocumentosSapiensVO cdsVO = new ClienteDocumentosSapiensVO();
				cdsVO.setUsuCodDoc(rs.getLong("usu_coddoc"));
				cdsVO.setUsuCodEmp(rs.getLong("USU_CodEmp"));
				cdsVO.setUsuCodFil(rs.getLong("USU_CodFil"));
				retorno.add(cdsVO);
			}

			OrsegupsUtils.closeConnection(conn, st, rs);

			return retorno;
		}
		catch (Exception e)
		{
			Log.error("Erro List<ClienteDocumentosSapiensVO> documentosClienteSapiens(Long codcli, Long numemp) ->" + e.getMessage());
			e.printStackTrace();
			OrsegupsUtils.closeConnection(conn, st, rs);
			return retorno;
		}

	}

	/**
	 * Retora lista de tipos de documentos disponíveis no site.
	 * 
	 * @return
	 */
	public List<SiteTipoDocumentoVO> getListaTiposDocumentos()
	{
		List<NeoObject> tiposDocumentos = PersistEngine.getObjects(AdapterUtils.getEntityClass("SITETipoDocumentoCliente"));
		List<SiteTipoDocumentoVO> retorno = new ArrayList<SiteTipoDocumentoVO>();

		if (tiposDocumentos != null)
		{
			for (NeoObject tdoc : tiposDocumentos)
			{
				EntityWrapper wTdoc = new EntityWrapper(tdoc);
				SiteTipoDocumentoVO stdvo = new SiteTipoDocumentoVO();
				stdvo.setCodDoc(NeoUtils.safeLong(NeoUtils.safeOutputString(wTdoc.findValue("codTipoDocumento"))));
				stdvo.setCodigoSapiens(NeoUtils.safeLong(NeoUtils.safeOutputString(wTdoc.findValue("codigoSapiens"))));
				stdvo.setDescricao(NeoUtils.safeOutputString(wTdoc.findValue("descricaoTipoDocumento")));
				stdvo.setPasta(NeoUtils.safeOutputString(wTdoc.findValue("pasta")));
				retorno.add(stdvo);
			}
		}

		return retorno;

	}

	/**
	 * Retora lista de tipos de documentos disponíveis no site.
	 * 
	 * @return
	 */
	public List<SiteTipoDocumentoVO> getListaTiposDocumentosSapiens()
	{
		List<SiteTipoDocumentoVO> retorno = new ArrayList<SiteTipoDocumentoVO>();

		Connection conn = PersistEngine.getConnection("SAPIENS");

		StringBuffer sql = new StringBuffer();
		sql.append(" select usu_coddoc, usu_desdoc from usu_t130doc ");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{

			st = conn.prepareStatement(sql.toString());

			rs = st.executeQuery();

			while (rs.next())
			{
				SiteTipoDocumentoVO stdvo = new SiteTipoDocumentoVO();
				stdvo.setCodDoc(0L);
				stdvo.setCodigoSapiens(rs.getLong("usu_coddoc"));
				stdvo.setDescricao(rs.getString("usu_desdoc"));
				//pasta nao tem como preencher
				retorno.add(stdvo);
			}

			OrsegupsUtils.closeConnection(conn, st, rs);

			return retorno;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			OrsegupsUtils.closeConnection(conn, st, rs);
			return retorno;
		}

	}

	/**
	 * Utilizado para inserir nas observações do contrato, informação de aceite de recebimento de
	 * documentos pelo site.
	 * 
	 * @param codcli
	 */
	public static void inserirObservacaoContrato(Long codcli)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		Log.info("[RECNFDOC] [" + key + "] - Inserindo Observação de recebimento de documentos eletronicos do cliente codcli=" + codcli);
		Connection conn = PersistEngine.getConnection("SAPIENS");

		StringBuffer sql = new StringBuffer();
		sql.append(" select usu_numctr, usu_codemp, usu_codfil, usu_codcli from usu_t160ctr where usu_codcli = ?  and usu_sitctr = 'A' ");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{

			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codcli);

			rs = st.executeQuery();

			while (rs.next())
			{
				Long usuNumctr = rs.getLong("usu_numctr");
				Long usuCodEmp = rs.getLong("usu_codemp");
				Long usuCodFil = rs.getLong("usu_codfil");

				Long seqObs = ContratoUtils.retornaSequenciaObservacaoContrato(usuCodEmp, usuCodFil, usuNumctr);

				if (seqObs != null)
				{
					Log.info("[RECNFDOC] [" + key + "] - Inserindo Observação seq:" + seqObs + " para o contrato:" + usuNumctr);
					Long usu_usumov = ContratoUtils.retornaCodeUsuarioSapiens("fusion");
					GregorianCalendar usu_datmov = new GregorianCalendar();
					Long usu_hormov = (long) ((usu_datmov.get(GregorianCalendar.HOUR_OF_DAY) * 60) + usu_datmov.get(GregorianCalendar.MINUTE));
					String usu_codope = "";
					String usu_txtobs = "Cliente aceitou recebimento de NF e Documentos por meio eletrônico via site. ";
					ContratoUtils.geraObservacao(usuCodEmp, usuCodFil, usuNumctr, seqObs, null, null, "A", usu_txtobs, usu_usumov, usu_datmov, usu_hormov, usu_codope);
					Log.info("[RECNFDOC] [" + key + "] - Observação seq:" + seqObs + " para o contrato:" + usuNumctr + " inserida com sucesso.");
				}

			}

			OrsegupsUtils.closeConnection(conn, st, rs);

		}
		catch (Exception e)
		{
			Log.error("[RECNFDOC] [" + key + "] - Erro ao inserir observações no contrato " + e.getMessage());
			e.printStackTrace();
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

	}

	/**
	 * Retorna dados da filial pelo vetor. Este método é utilizada para exibir o nome da empresa no
	 * contracheque do site.
	 * 
	 * @param numemp
	 * @param codfil
	 * @return
	 * @throws Exception
	 */
	public static SiteFilialColaboradorVO retornaDadosFilial(Long numemp, Long codfil) throws Exception
	{
		SiteFilialColaboradorVO retorno = new SiteFilialColaboradorVO();
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		Connection conn = null;
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("VETORH");

			sql.append(" select fil.razsoc, fil.numcgc, fil.codfil, fil.nomFil from r030fil fil where fil.numemp = ? and fil.codfil = ? ");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, numemp);
			pstm.setLong(2, codfil);

			rs = pstm.executeQuery();

			if (rs.next())
			{
				retorno = new SiteFilialColaboradorVO();
				retorno.setRazsoc(rs.getString("razsoc"));
				retorno.setNumcgc(rs.getLong("numcgc"));
				retorno.setCodfil(rs.getLong("codfil"));
				retorno.setNomFil(rs.getString("nomFil"));
			}

			return retorno;
		}
		catch (Exception e)
		{
			System.out.println("Erro ao consultar clientes sem inspeção [" + key + "]" + e.getMessage());
			e.printStackTrace();
			throw new Exception("Erro ao consultar clientes sem inspeção: chave de log:" + key);
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

		}
	}

	/**
	 * Retorna os contratos ativos com postos ativos de um cliente.
	 * 
	 * @param codcli
	 * @return
	 */
	public static List<Long> getContratosAtivosCliente(Long codcli)
	{
		List<Long> retorno = new ArrayList<Long>();

		Connection conn = null;

		StringBuffer sql = new StringBuffer();

		sql.append(" select ctr.usu_numctr from usu_t160ctr ctr ");
		sql.append(" inner join e085cli cli on ctr.usu_codcli = cli.codcli ");
		sql.append(" where ctr.usu_sitctr = 'A' ");
		sql.append(" and cli.codcli = ? ");
		sql.append(" and exists( select 1 from usu_t160cvs cvs where cvs.usu_codemp = ctr.usu_codemp and cvs.usu_codfil = ctr.usu_codfil and cvs.usu_numctr = ctr. usu_numctr and cvs.usu_sitcvs = 'A' ) ");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{
			conn = PersistEngine.getConnection("SAPIENS");
			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codcli);

			rs = st.executeQuery();

			while (rs.next())
			{
				retorno.add(rs.getLong(1));
			}
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return retorno;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

	}

	/**
	 * Abre RSC em nome do cliente para ser tratada pelo nac. Exemplo. Alteração de email onde o NAC
	 * precisa revisar o e-mail.
	 * 
	 * @param rsc - Informações sobre a RSC a ser aberta.
	 * @return
	 * @throws Exception
	 *//*
	public static String abrirRSC(SiteRSCVO rsc) throws Exception
	{
		try
		{
			System.out.println("RSC_ATU_CADASTRAL - Processando requisição -> " + rsc);
			
			 * SiteUtils siteUtils = new SiteUtils();
			 * String sessionId = request.getParameter("sessionId");
			 * //Busca os valores por Request
			 * String nome = request.getParameter("nome");
			 * String email = request.getParameter("email");
			 * String telefone = request.getParameter("telefone");
			 * String mensagem = request.getParameter("mensagem");
			 * String cidade = request.getParameter("cidade");
			 * String bairro = request.getParameter("bairro");
			 * Long tipoContato = 12430375L; // atualização cadastral (D_tipoContato)
			 * GregorianCalendar gcPrazo = new GregorianCalendar();
			 * gcPrazo.add(GregorianCalendar.DATE, 1);
			 * gcPrazo.set(GregorianCalendar.HOUR, 23);
			 * gcPrazo.set(GregorianCalendar.MINUTE, 59);
			 * gcPrazo.set(GregorianCalendar.SECOND, 59);
			 * gcPrazo = OrsegupsUtils.getNextWorkDay(gcPrazo);
			 

			//Long origem = 18500144L; // origem via site (D_RRCOrigem)

			//Cria Variaveis para diferenciar RSC de RRC
			String eform = "RSCRelatorioSolicitacaoClienteNovo";
			String workflow = "C027 - RSC - Relatório de Solicitação de Cliente Novo";
			String fieldTelefone = "telefones";
			String fieldContato = "contato";
			String fieldMensagem = "descricao";
			String valueSolicitante = "SITERESPONSAVELRSCCADASTRAL";

			//Busca o Tipo de Contato
			QLEqualsFilter filterTipoContato = new QLEqualsFilter("neoId", Long.valueOf(rsc.getTipoContato()));
			NeoObject tipoContatoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("tipoContato"), filterTipoContato);
			EntityWrapper tipoContatoWrapper = new EntityWrapper(tipoContatoObj);
			Long idTipoContato = (Long) tipoContatoWrapper.getValue("neoId");
			String descricaoTipoContato = (String) tipoContatoWrapper.getValue("tipo");

			String valueMensagem = "Tipo do Contato: " + descricaoTipoContato + " via site - \nDescrição: ";

			//Cria Instancia do Eform Principal
			InstantiableEntityInfo infoSolicitacao = AdapterUtils.getInstantiableEntityInfo(eform);
			NeoObject noSolicitacao = infoSolicitacao.createNewInstance();
			EntityWrapper solicitacaoWrapper = new EntityWrapper(noSolicitacao);

			//Busca Origem da Solicitacao
			QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", "05");// Site Orsegups
			NeoObject origemObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("RRCOrigem"), filterOrigem);

			GregorianCalendar gcPrazo = new GregorianCalendar();
			gcPrazo.add(GregorianCalendar.DATE, 5);
			gcPrazo.set(GregorianCalendar.HOUR, 23);
			gcPrazo.set(GregorianCalendar.MINUTE, 59);
			gcPrazo.set(GregorianCalendar.SECOND, 59);
			
			if (idTipoContato == 11715151L)
			{
				solicitacaoWrapper.setValue("prazoAcao", rsc.getPrazo());
			}
			else
			{
				//solicitacaoWrapper.setValue("prazoAtendimento", rsc.getPrazo());
				solicitacaoWrapper.setValue("prazoRegistrarRSC",OrsegupsUtils.getNextWorkDay(gcPrazo)); //RSCANTIGA
			}

			//Seta os valores
			solicitacaoWrapper.setValue("origem", origemObj);
			//solicitacaoWrapper.setValue("cidade", rsc.getCidade());
			//solicitacaoWrapper.setValue("bairro", rsc.getBairro());
			solicitacaoWrapper.setValue("email", rsc.getEmail());
			solicitacaoWrapper.setValue(fieldContato, rsc.getNome());
			solicitacaoWrapper.setValue(fieldTelefone, rsc.getTelefone());
			solicitacaoWrapper.setValue(fieldMensagem, valueMensagem + rsc.getMensagem());

			QLGroupFilter filterUser = new QLGroupFilter("AND");
			filterUser.addFilter(new QLEqualsFilter("usu_codcli", rsc.getCodCli()));
			filterUser.addFilter(new QLEqualsFilter("usu_situsu", "A"));

			//Busca Cliente
			List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTUSU"), filterUser);

			NeoObject clienteObj = null;

			if (users != null)
			{
				EntityWrapper usuarioCliente = new EntityWrapper(users.get(0));
				Long codcli = (Long) usuarioCliente.getValue("usu_codcli");

				QLEqualsFilter filterCliente = new QLEqualsFilter("codcli", codcli);
				clienteObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCliente);
			}

			if (clienteObj != null)
			{
				solicitacaoWrapper.findField("clienteSapiens").setValue(clienteObj);
			}

			//Salva o Eform
			PersistEngine.persist(noSolicitacao);

			// Cria Instancia do WorkFlow
			QLEqualsFilter equal = new QLEqualsFilter("name", workflow);
			List<ProcessModel> processModelList = (List<ProcessModel>) PersistEngine.getObjects(ProcessModel.class, equal, " neoId DESC ");
			ProcessModel processModel = processModelList.get(0);

			//Busca o solicitante
			NeoPaper papel = new NeoPaper();
			String executor = "";
			NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", valueSolicitante));
			papel = (NeoPaper) obj;

			if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers())
				{
					executor = user.getCode();
					break;
				}

			}

			NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));

			*//**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do
			 *         Fusion
			 * @date 11/03/2015
			 *//*

			final WFProcess processo = WorkflowService.startProcess(processModel, noSolicitacao, false, solicitante);
			System.out.println("solicitacao aberta mobile. " + processo.getCode());
			try
			{
				new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante, false);
			}
			catch (Exception e)
			{
				throw new Exception("Erro ao avançar primeira atividade.", e);
			}

			*//**
			 * FIM ALTERAÇÕES - NEOMIND
			 *//*

			// Dispara email ao cliente
			Map<String, Object> paramMap = new HashMap<String, Object>();

			paramMap.put("tarefa", processo.getCode());
			paramMap.put("nome", rsc.getNome());
			paramMap.put("tipo", rsc.getTipoContato());
			paramMap.put("mensagem", rsc.getMensagem());

			String[] destinatarios = null;
			if (rsc.getEmail().indexOf(",") > -1)
			{
				destinatarios = rsc.getEmail().split(",");
			}
			else
			{
				destinatarios = rsc.getEmail().split(";");
			}

			for (String mail : destinatarios)
			{
				//mail = "danilo.silva@orsegups.com.br";
				FusionRuntime.getInstance().getMailEngine().sendEMail(mail.trim(), "/portal_orsegups/emailAberturaRSC.jsp", paramMap);
			}

			return processo.getCode();

		}
		catch (Exception e)
		{
			System.out.println("Erro ao processar requisição. RSC_ATU_CADASTRAL->" + rsc + " MSG: " + e.getMessage());
			throw new Exception("Erro ao processar requisição.", e);
		}
	}*/

	/**
	 * Retorna os dados basicos docliente necessários para abetura de rsc
	 * 
	 * @param codcli
	 * @return
	 */
	public static RSCVO retornaDadosClienteRSC(Long codcli)
	{
		RSCVO retorno = new RSCVO();

		Connection conn = null;

		StringBuffer sql = new StringBuffer();
		sql.append(" select cli.codcli, cli.apecli, cli.intnet, cli.foncli, cli.cidcli, cli.baicli from e085cli cli where cli.codcli = ? ");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{
			conn = PersistEngine.getConnection("SAPIENS");
			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codcli);

			rs = st.executeQuery();

			if (rs.next())
			{
				retorno.setCodCli(rs.getLong("codcli"));
				retorno.setNome(rs.getString("apecli"));
				retorno.setEmail(rs.getString("intnet"));
				retorno.setTelefone(rs.getString("foncli"));
				retorno.setCidade(rs.getString("cidcli"));
				retorno.setBairro(rs.getString("baicli"));
			}
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}
	}

	/**
	 * Calcula a diferença de datas em meses subtraindo d2 de d1
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static int getDifMesesEntreDatas(GregorianCalendar d1, GregorianCalendar d2)
	{

		int diffYear = d2.get(Calendar.YEAR) - d1.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + d2.get(Calendar.MONTH) - d1.get(Calendar.MONTH);

		return diffMonth;
	}

	/**
	 * Calcula a diferença de datas em dias subtraindo d2 de d1
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static int getDifDiasEntreDatas(GregorianCalendar d1, GregorianCalendar d2)
	{
		int retorno = 0;
		Long diff = d2.getTimeInMillis() - d1.getTimeInMillis();
		retorno = (int) (diff / (1000 * 60 * 60 * 24));
		return retorno;
	}

	/**
	 * Retorna a ultima data da competencia do ponto anterior com base na data atual.<br>
	 * Obs.: A competencia fecha à cada 2 meses.
	 * 
	 * @return
	 */
	public static GregorianCalendar getCptAnterior()
	{
		GregorianCalendar retorno = new GregorianCalendar();
		int mes = GregorianCalendar.getInstance().get(GregorianCalendar.MONTH);

		if (mes % 2 == 0)
		{
			retorno.add(GregorianCalendar.MONTH, -1);
		}
		else
		{
			retorno.add(GregorianCalendar.MONTH, -2);
		}

		retorno.add(GregorianCalendar.MONTH, +1);
		retorno.set(GregorianCalendar.DATE, 1);
		retorno.add(GregorianCalendar.DATE, -1);

		retorno.set(GregorianCalendar.HOUR, 0);
		retorno.set(GregorianCalendar.MINUTE, 0);
		retorno.set(GregorianCalendar.SECOND, 0);
		retorno.set(GregorianCalendar.MILLISECOND, 0);

		return retorno;

	}

	/**
	 * Consulta tipo do evento pelo código
	 * 
	 * @param codeve
	 * @return
	 */
	public static Long consultaTipoEvento(Long codeve, Long codtab)
	{
		Long retorno = 0L;

		Connection conn = null;

		StringBuffer sql = new StringBuffer();
		sql.append(" select tipeve from r008evc evc where evc.CodEve = ? and evc.CodTab = ? ");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{
			conn = PersistEngine.getConnection("VETORH");
			st = conn.prepareStatement(sql.toString());
			st.setLong(1, codeve);
			st.setLong(2, codtab);

			rs = st.executeQuery();

			if (rs.next())
			{
				retorno = rs.getLong("tipeve");
			}
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return retorno;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}
	}
	
	public static List<SiteAnotacaoColaboradorVO> listaHistoricoDisciplinar(String numcpf)
	{
		List<SiteAnotacaoColaboradorVO> anotacoes = new ArrayList<SiteAnotacaoColaboradorVO>();
		SiteAnotacaoColaboradorVO anotacao = new SiteAnotacaoColaboradorVO();

		Connection conn = PersistEngine.getConnection("VETORH");
		PreparedStatement stAnotacao = null;
		ResultSet rsAnotacao = null;
		
		try
		{
			StringBuffer sqlAnotacao = new StringBuffer();
						
			/*sqlAnotacao.append(" SELECT ano.NumEmp, ano.TipCol, ano.NumCad, ano.DatNot, tip.DesNot, ano.NotFic FROM R038NOT ano "); 
			sqlAnotacao.append(" inner join r034fun fun on ano.NumEmp = fun.numemp  AND ano.TipCol = fun.tipcol AND ano.NumCad = fun.numcad ");
			sqlAnotacao.append(" LEFT JOIN R022NOT tip ON tip.CodNot = ano.TipNot "); 
			sqlAnotacao.append(" WHERE tip.codNot in (2,3) and fun.numcpf = ? ");
			sqlAnotacao.append(" ORDER BY ano.DatNot desc ");*/
			
			sqlAnotacao.append("  declare @cpf as numeric ");
			sqlAnotacao.append("  select @cpf =  ? "); 
			
			sqlAnotacao.append(" ( "); 
			sqlAnotacao.append(" SELECT 1 as ordem, ano.NumEmp, ano.TipCol, ano.NumCad, ano.DatNot, tip.DesNot, ano.NotFic "); 
			sqlAnotacao.append(" FROM R038NOT ano ");  
			sqlAnotacao.append(" inner join r034fun fun on ano.NumEmp = fun.numemp  AND ano.TipCol = fun.tipcol AND ano.NumCad = fun.numcad ");  
			sqlAnotacao.append(" LEFT JOIN R022NOT tip ON tip.CodNot = ano.TipNot ");  
			sqlAnotacao.append(" WHERE tip.codNot in (2,3)  and fun.numcpf = @cpf "); 
			//sqlAnotacao.append(" and fun.numemp in ( select numemp from r038afa xafa where xafa.datafa =  ( select max(a.datafa) from r038afa a where  a.numcad in (select f.numcad from r034fun f where f.numcpf = @cpf) and a.SitAfa in (6,7) ) ) "); 
			sqlAnotacao.append(" and fun.numcad in (select f.numcad from r034fun f where f.numcpf = @cpf  and f.datafa = '1900-12-31 00:00:00') "); 
			sqlAnotacao.append(" ) "); 
			sqlAnotacao.append(" union  ");
			sqlAnotacao.append(" ( "); 
			sqlAnotacao.append(" select 2 as ordem, afa.numemp, afa.tipcol, afa.numcad, afa.datafa as DatNot, 'DEMISSAO JUSTA CAUSA' as DesNot, 'Demissão por Justa Causa' as NotFic ");  
			sqlAnotacao.append(" from r038afa afa "); 
			sqlAnotacao.append(" inner join r034fun fun on fun.numemp = afa.numemp and fun.numcad = afa.numcad and fun.numcpf = @cpf "); 
			sqlAnotacao.append(" where 1=1 ");  
			sqlAnotacao.append(" and afa.SitAfa = 7 AND afa.CauDem = 1 ");   
			sqlAnotacao.append(" and afa.SitAfa in ( 6,7 ) "); 
			sqlAnotacao.append(" and afa.datafa = ( select max(a.datafa) from r038afa a where  a.numcad in (select f.numcad from r034fun f where f.numcpf = @cpf) and a.SitAfa in (6,7) ) "); 
			sqlAnotacao.append(" ) "); 
			sqlAnotacao.append(" ORDER BY ordem asc, DatNot desc ");  
			
					
			stAnotacao = conn.prepareStatement(sqlAnotacao.toString());
			stAnotacao.setString(1, numcpf);
			rsAnotacao = stAnotacao.executeQuery();

			//Anotacaos
			while (rsAnotacao.next()){
				anotacao = new SiteAnotacaoColaboradorVO();

				Timestamp data = rsAnotacao.getTimestamp("DatNot");
				GregorianCalendar gData = new GregorianCalendar();
				gData.setTimeInMillis(data.getTime());
				
				anotacao.setOrdem(rsAnotacao.getLong("ordem"));
				anotacao.setData(gData);
				anotacao.setTipoAnotacao(rsAnotacao.getString("DesNot"));
				anotacao.setDescricao(rsAnotacao.getString("NotFic"));

				anotacao.setNumEmp(rsAnotacao.getLong("NumEmp"));
				anotacao.setTipCol(rsAnotacao.getLong("Tipcol"));
				anotacao.setNumCad(rsAnotacao.getLong("NumCad"));

				anotacoes.add(anotacao);
			}

			rsAnotacao.close();
			stAnotacao.close();
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			OrsegupsUtils.closeConnection(conn, stAnotacao, rsAnotacao);
		}
		return anotacoes;
	}
	
	/**
	 * Verifica se o colaborador demitido pode acessar o site.
	 * @param numcpf - 
	 * @param prazoDias - 
	 * @return
	 */
	public static boolean verificaPrazoAcessoSite(Long numcpf, Integer prazoDias){
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try
		{
			conn = PersistEngine.getConnection("VETORH");
			StringBuffer sql = new StringBuffer();
			
			sql.append(" declare @cpf as numeric ");
			sql.append(" select @cpf =  ? ");
			sql.append(" select max(a.datafa) from r038afa a where  a.numcad in (select f.numcad from r034fun f where f.numcpf = @cpf) and a.SitAfa = 7 and a.caudem = 1 ");
			
			pst = conn.prepareStatement(sql.toString());
			
			pst.setLong(1, numcpf);
			
			rs = pst.executeQuery();
			
			if(rs.next()){
				GregorianCalendar dataAtual = new GregorianCalendar();
				GregorianCalendar datAfa = new GregorianCalendar();
				
				Date time = rs.getDate(1);
				
				datAfa.setTimeInMillis(time.getTime());
				
				datAfa.add(GregorianCalendar.DATE, prazoDias);
				
				System.out.println("limite acesso:"+NeoDateUtils.safeDateFormat(datAfa)+" data atual:" + NeoDateUtils.safeDateFormat(dataAtual) + " - " +dataAtual.before(datAfa));
				if (dataAtual.before(datAfa)){
					return true;
				}else{
					return false;
				}
				
			}
			
			return true;
		}catch (Exception e){
			e.printStackTrace();
			return false;
		}finally{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}
	}
	
	public static boolean loginColaboradorValido(Long numcpf, String senha){
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try
		{
			conn = PersistEngine.getConnection("VETORH");
			StringBuffer sql = new StringBuffer();
			
			sql.append(" select 1 from usu_t230usu u where u.usu_numcpf = ? and u.usu_senusu  = ? ");
			
			pst = conn.prepareStatement(sql.toString());
			
			pst.setLong(1, numcpf);
			pst.setString(2, senha);
			
			rs = pst.executeQuery();
			
			if(rs.next()){
				return true;
			}
			
			return false;
		}catch (Exception e){
			e.printStackTrace();
			return false;
		}finally{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}
	}
	
	public static List<SiteContraChequeColaboradorVO> getContraCheque(Long numemp, Long tipcol, Long numcad)
	{
		List<SiteContraChequeColaboradorVO> retorno = new ArrayList<SiteContraChequeColaboradorVO	>();

		Connection conn = null;

		StringBuffer sql = new StringBuffer();

		sql.append(" select usu_numemp, usu_tipcol, usu_numcad, usu_perref, usu_razsoc, usu_numcgc, usu_titcar, ");
		sql.append("     usu_nomesc, usu_salbas, usu_salins, usu_faixir, usu_totvct, usu_totdsc, usu_basfgt,    ");
		sql.append("     usu_fgtmes, usu_basirf, usu_vlrliq, usu_msgchq, usu_txtban, usu_txtage, usu_txtcon,    ");
		sql.append("     usu_datadm, usu_datger, ");
		sql.append("     case when chq.usu_datger is null then 1 when chq.usu_datger > DATEADD(d,DATEDIFF(d,0,getdate()-0 ),0) then 0 else 1 end   mostra_site  "); //mostrarsite
		sql.append(" from usu_t240chq chq where 1=1 ");
		sql.append(" and usu_numemp = ? and usu_tipcol = ? and usu_numcad = ? ");
		sql.append(" and usu_perref > getdate()-120 ");
		sql.append(" order by usu_perref DESC ");

		ResultSet rs = null;

		PreparedStatement st = null;
		try
		{
			conn = PersistEngine.getConnection("VETORH");
			st = conn.prepareStatement(sql.toString());
			st.setLong(1, numemp);
			st.setLong(2, tipcol);
			st.setLong(3, numcad);

			rs = st.executeQuery();

			while (rs.next())
			{
				SiteContraChequeColaboradorVO contraChequeColaboradorVO = new SiteContraChequeColaboradorVO();

				GregorianCalendar gcPerRef = new GregorianCalendar();
				GregorianCalendar gcDatAdm = new GregorianCalendar();
				
				gcPerRef.setTime(rs.getDate("usu_perref"));
				
				contraChequeColaboradorVO.setPerRef(NeoUtils.safeDateFormat(gcPerRef, "MM/yyyy"));
				contraChequeColaboradorVO.setPerRefDate(gcPerRef);

				contraChequeColaboradorVO.setTitCar((String) rs.getString("usu_titcar"));
				
				gcDatAdm.setTime(rs.getDate("usu_datadm"));
				contraChequeColaboradorVO.setDatAdm((String) NeoUtils.safeDateFormat(gcDatAdm, "dd/MM/yyyy"));

				contraChequeColaboradorVO.setTxtBan((String) rs.getString("usu_txtban"));
				contraChequeColaboradorVO.setTxtAge((String) rs.getString("usu_txtage"));
				contraChequeColaboradorVO.setTxtCon((String) rs.getString("usu_txtcon"));

				contraChequeColaboradorVO.setSalBas(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("usu_salbas")));
				contraChequeColaboradorVO.setBasFgt(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("usu_basfgt")));
				contraChequeColaboradorVO.setSalIns(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("usu_salins")));
				contraChequeColaboradorVO.setBasIrf(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("usu_basirf")));
				contraChequeColaboradorVO.setFgtMes(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("usu_fgtmes")));
				contraChequeColaboradorVO.setTotVct(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("usu_totvct")));
				contraChequeColaboradorVO.setTotDsc(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("usu_totdsc")));
				contraChequeColaboradorVO.setVlrLiq(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rs.getBigDecimal("usu_vlrliq")));
				contraChequeColaboradorVO.setMsgChq(rs.getString("usu_msgchq"));
				
				contraChequeColaboradorVO.setMostraSite(rs.getLong("mostra_site"));
				
				retorno.add(contraChequeColaboradorVO);
			}
			return retorno;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return retorno;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, rs);
		}

	}
	
	

	public static void main(String args[])
	{
		System.out.println(getDifMesesEntreDatas(new GregorianCalendar(2016, 03, 01), new GregorianCalendar(2016, 05, 01)));
		System.out.println(getDifMesesEntreDatas(new GregorianCalendar(2016, 03, 01), new GregorianCalendar(2016, 05, 01)) % 2);
	}

}
