package com.neomind.fusion.custom.orsegups.e2doc.engine;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import com.neomind.fusion.custom.orsegups.e2doc.xml.livroFinanceiro.Pastas;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class E2docLivroFinanceiroEngine {
    
    public synchronized Pastas pesquisaFinanceiro(HashMap<String, String> atributosPesquisa) {

	String pesquisa = "<modelo>livros contabeis e fiscais</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	E2docEngine.verificaValidadeKey("RH");

	String retorno = E2docEngine.pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

	retorno = retorno.replaceAll("&", " ");

	Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosFinanceiro(retorno);
	}
	
	E2docEngine.removeDocumentosTemporarios();
	
	return pastas;
    }
    
    private Pastas tratarRetornoDocumentosFinanceiro(String retorno) {

	Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, Pastas.class);

	    pastas = (Pastas) je.getValue();

	} catch (JAXBException e) {
	    e.printStackTrace();
	}

	return pastas;

    }
    
    
    public List<String> getTiposDocumentos(){
	
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append("SELECT DISTINCT s_indice98 FROM PASTA WHERE LEN(s_indice98)>0");
	
	List<String> retorno = new ArrayList<String>();
	
	try {
	    conn = PersistEngine.getConnection("E2DOC");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {
		retorno.add(rs.getString(1));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}
	
	
	return retorno;
    }
    
    public List<String> getTiposLivros(){
	
   	Connection conn = null;
   	Statement stmt = null;
   	ResultSet rs = null;

   	StringBuilder sql = new StringBuilder();

   	sql.append("SELECT DISTINCT s_indice97 FROM PASTA WHERE LEN(s_indice97)>0");
   	
   	List<String> retorno = new ArrayList<String>();
   	
   	try {
   	    conn = PersistEngine.getConnection("E2DOC");
   	    stmt = conn.createStatement();
   	    rs = stmt.executeQuery(sql.toString());

   	    while (rs.next()) {
   		retorno.add(rs.getString(1));
   	    }

   	} catch (SQLException e) {
   	    e.printStackTrace();
   	} finally {
   	    OrsegupsUtils.closeConnection(conn, stmt, rs);
   	}
   	
   	
   	return retorno;
       }
    
    public List<String> getEmpresas(){
	
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append("SELECT DISTINCT s_indice64 FROM PASTA WHERE LEN(s_indice64)>0 ");
	sql.append("UNION ");
	sql.append("SELECT DISTINCT s_indice92 FROM PASTA WHERE LEN(s_indice92)>0 ");
	
	List<String> retorno = new ArrayList<String>();
	
	try {
	    conn = PersistEngine.getConnection("E2DOC");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {
		retorno.add(rs.getString(1));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}
	
	
	return retorno;
    }

}
