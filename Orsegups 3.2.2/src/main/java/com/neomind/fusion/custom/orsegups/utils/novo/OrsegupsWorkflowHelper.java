package com.neomind.fusion.custom.orsegups.utils.novo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.handler.HandlerFactory;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.service.PeopleAssignmentsService;
import com.neomind.fusion.workflow.simulation.WorkflowService;

/**
 * Classe utilitária criada para centralizar as lógicas de manipulação de
 * processos do Fusion. Isso facilita a evolução do produto causando o mínimo de
 * impacto nos específicos.
 * 
 * @author neomind - willian.mews
 *
 */
public class OrsegupsWorkflowHelper 
{
	private final WFProcess processo;

	public OrsegupsWorkflowHelper(WFProcess processo) 
	{
		super();
		this.processo = processo;
	}

	/**
	 * Método que avança a primeira atividade do processo.
	 * 
	 * <pre>
	 * Caso seja necessário, o sistema assumirá a primeira atividade em nome do usuário e avançará.
	 * </pre>
	 * 
	 * @param usuario
	 *            {@link NeoUser}
	 */
	public synchronized void avancaPrimeiraAtividade(NeoUser usuario, boolean isAvanca) 
	{
		List<Activity> atividadesAbertas = processo.getOpenActivities();

		if (atividadesAbertas != null && !atividadesAbertas.isEmpty()) 
		{
			Activity primeiraAtividade = atividadesAbertas.get(0);

			if (primeiraAtividade instanceof UserActivity  && primeiraAtividade.getFinishDate() == null) 
			{
				UserActivity atividadeUsuario = (UserActivity) primeiraAtividade;
				
				TaskInstance taskInstance = atividadeUsuario.getInstance();
				
				/*
				 * Por Luiz Medeiros
				 */
				if(taskInstance == null)
				{
					taskInstance = TaskInstanceHelper.create(primeiraAtividade, PeopleAssignmentsService.getAssignments((UserActivity)primeiraAtividade));
					PersistEngine.persist(taskInstance);
					atividadeUsuario.setInstance(taskInstance);
					//taskInstance = atividadeUsuario.getInstance();
				}
			
					
				
				//para assumir a atividade em pool
				primeiraAtividade.getActivity().getTaskAssigner().assign((UserActivity) atividadeUsuario, (NeoUser) usuario, true);   
				
				if (isAvanca)
				{
					//para avançar a primeira atividade
					RuntimeEngine.getTaskService().complete(taskInstance, usuario);
				}
				
				
			}
		}
	}
	
	public synchronized void avancaPrimeiraAtividadePresencaEngine(NeoUser usuario, boolean isAvanca) 
	{
		List<Activity> atividadesAbertas = processo.getOpenActivities();

		if (atividadesAbertas != null && !atividadesAbertas.isEmpty()) 
		{
			Activity primeiraAtividade = atividadesAbertas.get(0);

			if (primeiraAtividade instanceof UserActivity  && primeiraAtividade.getFinishDate() == null) 
			{
				UserActivity atividadeUsuario = (UserActivity) primeiraAtividade;
				
				TaskInstance taskInstance = atividadeUsuario.getInstance();
				
				/*
				 * Por Luiz Medeiros
				 */
				if(taskInstance == null)
				{
					taskInstance = TaskInstanceHelper.create(primeiraAtividade, PeopleAssignmentsService.getAssignments((UserActivity)primeiraAtividade));
					PersistEngine.persist(taskInstance);
					atividadeUsuario.setInstance(taskInstance);
					//taskInstance = atividadeUsuario.getInstance();
				}							
				
				
				//para assumir a atividade em pool
				Task task = primeiraAtividade.getActivity().getTaskAssigner().assign((UserActivity) atividadeUsuario, (NeoUser) usuario, true);   
				
				if (isAvanca)
				{
					//para avançar a primeira atividade
					RuntimeEngine.getTaskService().complete(task, usuario);
				}
				
				
			}
		}
	}	
	
	public synchronized void avancaPrimeiraAtividade(NeoUser usuario) 
	{
		this.avancaPrimeiraAtividade(usuario, true);
	}
	
	/**
	 * @author orsegups lucas.avila - Novo método.
	 * @date 08/07/2015
	 */
	
	/**
	 * Método que avança a primeira tarefa do processo, quando assign = true
	 * ou seja, a primeira atividade é de usuário não POOL
	 * @param usuario
	 *            {@link NeoUser}
	 */
	public synchronized void avancaPrimeiraTarefa(NeoUser usuario) 
	{ 
		Set<Activity> acts = processo.getTaskSet();
		List<Task> openTasks = new ArrayList<Task>();
		for(Activity a : acts)
		{
			if(a instanceof UserActivity && a.getFinishDate() == null)
			{				
				openTasks.addAll(((UserActivity) a).getTaskList());
			}
		}
		
		for(Task t : openTasks)
		{
			TaskInstanceHelper.assign(t);
			PersistEngine.getEntityManager().flush();
			TaskInstance taskInstance = TaskInstanceHelper.findTaskInstance(t);
			RuntimeEngine.getTaskService().complete(taskInstance, usuario);
		}
	}
	
	public synchronized static void iniciaProcesso(ProcessModel pm, NeoObject entity, boolean assign, NeoUser user)
	{
		WFProcess processo = WorkflowService.startProcess(pm, entity, false, null);
		processo.setRequester(user);
		/*if (processo != null)
		{
			Activity atividade = getAllOpenActivity(processo).get(0).getActivity();
			TaskInstance instancia = TaskInstanceHelper.findTaskInstance(atividade);
			if(instancia == null && atividade instanceof UserActivity)
			{
				UserActivity atividadeUsuario = (UserActivity) atividade;
				TaskInstance instance = TaskInstanceHelper.create(atividade, PeopleAssignmentsService.getAssignments(atividadeUsuario));
			}
		}*/
	}
	
	/**
	 * Abertura da tarefas centralizado.
	 * @param pm - Modelo do processo
	 * @param entity - Objeto formulário principal do processo
	 * @param assign - se sim, atribui a tarefa ao usuário {@link NeoUser}
	 * @param user - usuário responsável pela tarefa
	 * @param avancarPrimeiraTarefa - indica se a primeira tarefa será avançada
	 * @param requester - caso seja diferente de null, atribui o usuário como solicitante da tarefa.
	 * @return
	 */
	public synchronized static  String iniciaProcesso(ProcessModel pm, NeoObject entity, boolean assign, NeoUser user, boolean avancarPrimeiraTarefa, NeoUser requester)
	{
		WFProcess processo = WorkflowService.startProcess(pm, entity, false, null);
		processo.setSaved(true);
		if (requester != null ){
			processo.setRequester(requester);
		}else{
			processo.setRequester(user);
		}
		processo.setProcessState(ProcessState.running);
		UserActivity atividadeUsuario = null;
		if (processo != null)
		{
			//alteração - INICIO
			TaskInstance instancia = null;
			Activity atividade = null;
			if(getAllOpenActivity(processo).size() > 0)
			{
				atividade = getAllOpenActivity(processo).get(0).getActivity();
				instancia = TaskInstanceHelper.findTaskInstance(atividade);
			}else
			{
				List<TaskInstance> list = PersistEngine.getObjects(TaskInstance.class, new QLEqualsFilter("activity.process", processo));
				instancia = list.get(0);
			}
			//alteração - FIM
			
			//avancarPrimeiraTarefa(user);
			//TaskInstance instancia = TaskInstanceHelper.findTaskInstance(atividade);
			
			if(instancia == null && atividade != null && atividade instanceof UserActivity)
			{
				 atividadeUsuario = (UserActivity) atividade;
				TaskInstance instance = TaskInstanceHelper.create(atividade, PeopleAssignmentsService.getAssignments(atividadeUsuario));
				
				if(avancarPrimeiraTarefa)
					TaskInstanceHelper.complete(instance.getTask());
			}else
			{
				if(avancarPrimeiraTarefa)
				{
					new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(user, true);
				}else
				{
					new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(user, false);
						   
				}
			}
		}		

		return processo.getCode();
	}
	
	public synchronized static String iniciaProcessoNeoIdCode(ProcessModel pm, NeoObject entity, boolean assign, NeoUser user, boolean avancarPrimeiraTarefa)
	{
		WFProcess processo = pm.startProcess(entity , false, null);
        processo.setSaved(true); 
		processo.setRequester(user);
		if (processo != null)
		{
			//alteração - INICIO
			TaskInstance instancia = null;
			Activity atividade = null;
			if(getAllOpenActivity(processo).size() > 0)
			{
				atividade = getAllOpenActivity(processo).get(0).getActivity();
				instancia = TaskInstanceHelper.findTaskInstance(atividade);
			}else
			{
				List<TaskInstance> list = PersistEngine.getObjects(TaskInstance.class, new QLEqualsFilter("activity.process", processo));
				instancia = list.get(0);
			}
			//alteração - FIM
			
			//avancarPrimeiraTarefa(user);
			//TaskInstance instancia = TaskInstanceHelper.findTaskInstance(atividade);
			
			if(instancia == null && atividade != null && atividade instanceof UserActivity)
			{
				UserActivity atividadeUsuario = (UserActivity) atividade;
				TaskInstance instance = TaskInstanceHelper.create(atividade, PeopleAssignmentsService.getAssignments(atividadeUsuario));
				
				if(avancarPrimeiraTarefa)
					TaskInstanceHelper.complete(instance.getTask());
			}else
			{
				UserActivity atividadeUsuario = (UserActivity)instancia.getActivity();
				Task tarefaCriada = atividadeUsuario.getTaskAssigner().assign(atividadeUsuario, user);
				
				if(avancarPrimeiraTarefa)
				{
					TaskInstance instanciaTarefa = TaskInstanceHelper.assign(tarefaCriada);
					RuntimeEngine.getTaskService().complete(instanciaTarefa, user);
					//TaskInstanceHelper.complete(tarefaCriada);
				}
			}
		}		

		return processo.getNeoId()+";"+processo.getCode();
	}
	
	public synchronized static Long iniciaProcessoNeoId(ProcessModel pm, NeoObject entity, boolean assign, NeoUser user, boolean avancarPrimeiraTarefa, NeoUser requester)
	{
		WFProcess processo = WorkflowService.startProcess(pm, entity, false, null);
		processo.setSaved(true);
		if (requester != null ){
			processo.setRequester(requester);
		}else{
			processo.setRequester(user);
		}
		processo.setProcessState(ProcessState.running);
		UserActivity atividadeUsuario = null;
		if (processo != null)
		{
			//alteração - INICIO
			TaskInstance instancia = null;
			Activity atividade = null;
			if(getAllOpenActivity(processo).size() > 0)
			{
				atividade = getAllOpenActivity(processo).get(0).getActivity();
				instancia = TaskInstanceHelper.findTaskInstance(atividade);
			}else
			{
				List<TaskInstance> list = PersistEngine.getObjects(TaskInstance.class, new QLEqualsFilter("activity.process", processo));
				instancia = list.get(0);
			}
			//alteração - FIM
			
			//avancarPrimeiraTarefa(user);
			//TaskInstance instancia = TaskInstanceHelper.findTaskInstance(atividade);
			
			if(instancia == null && atividade != null && atividade instanceof UserActivity)
			{
				 atividadeUsuario = (UserActivity) atividade;
				TaskInstance instance = TaskInstanceHelper.create(atividade, PeopleAssignmentsService.getAssignments(atividadeUsuario));
				
				if(avancarPrimeiraTarefa)
					TaskInstanceHelper.complete(instance.getTask());
			}else
			{
				if(avancarPrimeiraTarefa)
				{
					new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(user, true);
				}else
				{
					new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(user, false);
						   
				}
			}
		}		

		return processo.getNeoId();
	}
	
	public static List<Activity> getAllOpenActivity(WFProcess process)
	{
		Set<Activity> acts = process.getTaskSet();
	
		List<Activity> openActs = new ArrayList<Activity>();
		for(Activity a : acts)
		{
			if(a.getFinishDate() == null) 
			{
				openActs.add(a);
			}
		}
		
		return openActs;
	}
	
	//Finaliza tarefa a partir de uma activity
	public synchronized static void finishTaskByActivity(Activity act)
	{
		TaskInstance instance = act.getInstance();
		
		//Caso nao tenha taskinstance trata-se de inconsistencia
		if(instance != null && instance.getOwner() != null)
		{
			RuntimeEngine.getTaskService().complete(instance, instance.getOwner());
		}
		else
		{						
			//Trata-se de activity
			if(instance != null)
			{		
				//Finaliza Tarefas que estao inconsistentes
				List<Task> tasks = PersistEngine.getObjects(Task.class, new QLEqualsFilter("activity.neoId", act.getNeoId()));			
				for(Task t : tasks)
				{
					//Caso tarefa esteja aberta
					if(t.getFinishDate() == null)
					{
						HandlerFactory.TASK.get(t).finish();
					}
				}
			}
		}
	}
	
	public static Task getTaskOrigin(Activity activity)
	{
		if(activity.getIncoming() != null)
		{
			for(Activity a : activity.getIncoming())
			{
				if(a instanceof UserActivity)
				{
					QLGroupFilter groupFilter = new QLGroupFilter("AND");
						groupFilter.addFilter(new QLEqualsFilter("activity.neoId", a.getNeoId()));
						groupFilter.addFilter(new QLFilterIsNotNull("finishDate"));
						
					List<Task> taskList = PersistEngine.getObjects(Task.class, groupFilter, -1,-1," finishDate asc ");
					
					if(taskList != null && !taskList.isEmpty())
						return taskList.get(0);
					else
						return null;
				}			
			}
			
			for(Activity a : activity.getIncoming())
			{
				return getTaskOrigin(a);
			}
		}
		
		return null;
	}
	
	public synchronized void avancaPrimeiraAtividadePool(NeoUser usuario, boolean isAvanca) 
	{
		List<Activity> atividadesAbertas = processo.getOpenActivities();

		if (atividadesAbertas != null && !atividadesAbertas.isEmpty()) 
		{
			Activity primeiraAtividade = atividadesAbertas.get(0);

			if (primeiraAtividade instanceof UserActivity  && primeiraAtividade.getFinishDate() == null) 
			{
				UserActivity atividadeUsuario = (UserActivity) primeiraAtividade;
				
				TaskInstance taskInstance = atividadeUsuario.getInstance();
				
				if (isAvanca)				{
					//para avançar a primeira atividade
					RuntimeEngine.getTaskService().complete(taskInstance, usuario);
				}
				
				
			}
		}
	}
}