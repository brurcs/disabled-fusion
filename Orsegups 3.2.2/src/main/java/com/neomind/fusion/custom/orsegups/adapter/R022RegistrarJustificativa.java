package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.adapter.R022RegistrarJustificativa
public class R022RegistrarJustificativa implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper wrapper,	Activity activity) {
		// listaTramites
		try{
			NeoObject tramite = AdapterUtils.createNewEntityInstance("HEXHistoricoTramiteHE");
			EntityWrapper wTramite = new EntityWrapper(tramite);
			wTramite.setValue("acao", "Justificou Horas Extras" );
			wTramite.setValue("usuario", PortalUtil.getCurrentUser().getCode() );
			wTramite.setValue("obs", NeoUtils.safeOutputString(wrapper.findValue("txtobs")) );
			PersistEngine.persist(tramite);
			
			wrapper.findField("listaTramites").addValue(tramite);
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Erro ao responder reprovação. msg: "+e.getMessage());
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}
