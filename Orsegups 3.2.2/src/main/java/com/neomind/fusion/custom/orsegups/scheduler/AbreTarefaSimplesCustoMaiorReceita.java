package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.PostoSemReceitaVO;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesCustoMaiorReceita implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(AbreTarefaSimplesCustoMaiorReceita.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		String solicitante = "dilmoberger";
		String executor = "gilsoncesar";
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 10L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		Connection conn = PersistEngine.getConnection("DATAWAREHOUSE");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesCustoMaiorReceita");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		int regionais[] = { 
				14, //CASCAVEL
				15, //CURITIBA SUL
				16, //TUBARAO
		};

		for (int regional : regionais)
		{
			try
			{
				//Custo representando 90% ou mais da receita
				List<PostoSemReceitaVO> postosSemReceita = new ArrayList<PostoSemReceitaVO>();
				sql.append(" SELECT Top 10 cpt.NomeCompetencia, tm.NomeTipoMercado, emp.NomeEmpresa, reg.NomeRegional, cli.NomeCliente, SUM(r.ValorReceita) ValorReceita, SUM(r.ValorCusto) ValorCusto ");
				sql.append(" FROM Fact.Resultado r                                                                                                                                              ");
				sql.append(" INNER JOIN Dim.Competencia cpt ON cpt.IdCompetencia = r.idCompetencia                                                                                              ");
				sql.append(" INNER JOIN Dim.Cliente cli ON cli.IdCliente = r.idCliente                                                                                                          ");
				sql.append(" INNER JOIN Dim.Regional reg ON reg.IdRegional = r.idRegional                                                                                                       ");
				sql.append(" INNER JOIN Dim.Empresa emp ON emp.IdEmpresa = r.idEmpresa                                                                                                          ");
				sql.append(" INNER JOIN Dim.TipoMercado tm ON tm.IdTipoMercado = r.idMercado                                                                                                    ");
				sql.append(" INNER JOIN Dim.Posto pos on pos.IdPosto = r.idPosto																												");                                                                                                   
				sql.append(" WHERE r.idNegocio IN (2, 3)                                                                                                                                        ");
				//sql.append(" AND r.idRegional IN (14, 15, 16)                                                                                                                                 ");
				sql.append(" AND reg.codRegional = ? 																																				");
				//Classificaçõs de serviço extra para não abrir tarefa
				sql.append(" AND pos.codPosto not in( '16150000100201000101','16150000100201000102','16150000100201000103','16150000100201000104',"
										   		   + "'16150000100201000105','16150000100201000106','16150000100201000107','16150000100201000108',"
										   		   + "'16150000100201000109','16150000100201000110','16150000100201000113','16150000100201000118',"
										   		   + "'16150000100201000119')																															");
				sql.append(" AND r.idMercado = 1                                                                                                                                                ");
				sql.append(" AND cpt.NomeCompetencia = RIGHT(CONVERT(VARCHAR(10), DATEADD(MONTH, -2, GETDATE()), 103), 7)                                                                       ");
				sql.append(" GROUP BY cpt.NomeCompetencia, tm.NomeTipoMercado, emp.NomeEmpresa, reg.NomeRegional, cli.NomeCliente                                                               ");
				sql.append(" HAVING SUM(r.ValorReceita) / SUM(NULLIF(r.ValorCusto,0)) < 1.1111												                                                    ");

				pstm = conn.prepareStatement(sql.toString());
				pstm.setInt(1,regional);
				rs = pstm.executeQuery();

				while (rs.next())
				{
					PostoSemReceitaVO posto = new PostoSemReceitaVO();

					posto.setCompetencia(rs.getString("NomeCompetencia"));
					posto.setTipoMercado(rs.getString("NomeTipoMercado"));
					posto.setNomeEmpresa(rs.getString("NomeEmpresa"));
					posto.setNomeRegional(rs.getString("NomeRegional"));
					posto.setNomeCliente(rs.getString("NomeCliente"));
					posto.setValorReceita(rs.getBigDecimal("ValorReceita"));
					posto.setValorCusto(rs.getBigDecimal("ValorCusto"));

					postosSemReceita.add(posto);
				}

				if (postosSemReceita != null && !postosSemReceita.isEmpty())
				{
					ListIterator<PostoSemReceitaVO> i = postosSemReceita.listIterator();
					List<PostoSemReceitaVO> listaPostosCliente = new ArrayList<PostoSemReceitaVO>();
					while (i.hasNext())
					{
						PostoSemReceitaVO atual = i.next();
						PostoSemReceitaVO proximo = null;
						listaPostosCliente.add(atual);

						if (i.hasNext())
						{
							proximo = i.next();
							i.previous();

							if (!atual.getNomeCliente().equals(proximo.getNomeCliente()))
							{
								String descricao = montaDescricaoTarefaSimples(listaPostosCliente);
								String titulo = "Analisar Cliente com custo maior que 90% da receita - " + atual.getNomeCliente();
								String codigoTarefa = abrirTarefaPostoSemReceita(solicitante, executor, titulo, descricao, "1", "sim", prazo);

								log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " - Aberto a Tarefa Simples: " + codigoTarefa);

								listaPostosCliente.clear();
							}
						}
						else
						{
							String descricao = montaDescricaoTarefaSimples(listaPostosCliente);
							String titulo = "Analisar Cliente com custo maior que 90% da receita - " + atual.getNomeCliente();
							String codigoTarefa = abrirTarefaPostoSemReceita(solicitante, executor, titulo, descricao, "1", "sim", prazo);

							log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " - Aberto a Tarefa Simples: " + codigoTarefa);
						}
					}
				}
				else
				{
					log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + "A consulta não retornou resultado.");
				}
			}
			catch (Exception e)
			{
				log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples (Custo Maior Que a Receita)");
				System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples (Custo Maior Que a Receita)");
				e.printStackTrace();
				throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
			}
			finally
			{
				try
				{
					rs.close();
					pstm.close();
					conn.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}

				log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
		}
	}

	public String abrirTarefaPostoSemReceita(String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo)
	{
		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", solicitante));

		if (NeoUtils.safeIsNull(objSolicitante))
		{
			return "Erro #1 - Solicitante não encontrado";
		}

		boolean isAvanca = true;

		if (avanca != null && (avanca.trim().equals("nao") || avanca.trim().equals("0") || avanca.trim().equals("no") || avanca.trim().equals("off")))
		{
			isAvanca = false;
		}

		NeoUser objExecutor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));

		if (isAvanca)
		{
			if (NeoUtils.safeIsNull(objExecutor))
			{
				return "Erro #2 - Executor não encontrado";
			}
		}

		if (NeoUtils.safeIsNull(titulo))
		{
			return "Erro #3 - Título não informado";
		}

		if (NeoUtils.safeIsNull(descricao))
		{
			return "Erro #4 - Descrição não informada";
		}

		InstantiableEntityInfo eOrigem = AdapterUtils.getInstantiableEntityInfo("origemTarefa");
		NeoObject objOrigem = (NeoObject) PersistEngine.getObject(eOrigem.getEntityClass(), new QLEqualsFilter("codigo", Long.valueOf(origem)));

		if (NeoUtils.safeIsNull(objOrigem))
		{
			return "Erro #5 - Origem não encontrada";
		}

		if (NeoUtils.safeIsNull(prazo))
		{
			return "Erro #6 - Prazo não informado";
		}

		final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));
		final WFProcess pmProcess = pm.startProcess(false, objSolicitante);
		final NeoObject wkfTarefaSimples = pmProcess.getEntity();
		final EntityWrapper ewWkfTarefaSimples = new EntityWrapper(wkfTarefaSimples);

		ewWkfTarefaSimples.findField("Solicitante").setValue(objSolicitante);
		ewWkfTarefaSimples.findField("Executor").setValue(objExecutor);
		ewWkfTarefaSimples.findField("Titulo").setValue(titulo);
		ewWkfTarefaSimples.findField("DescricaoSolicitacao").setValue(descricao);
		ewWkfTarefaSimples.findField("Prazo").setValue(prazo);
		ewWkfTarefaSimples.findField("origem").setValue(objOrigem);
		ewWkfTarefaSimples.findField("dataSolicitacao").setValue(new GregorianCalendar());

		pmProcess.setRequester(objSolicitante);
		pmProcess.setSaved(true);

		// Trata a primeira tarefa
		Task task = null;
		final List acts = pmProcess.getOpenActivities();
		final Activity activity1 = (Activity) acts.get(0);
		if (activity1 instanceof UserActivity)
		{
			try
			{
				if (((UserActivity) activity1).getTaskList().size() <= 0)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, objSolicitante);
					if (isAvanca)
					{
						OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
					}
				}
				else
				{
					task = ((UserActivity) activity1).getTaskList().get(0);
					if (isAvanca)
					{
						OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
					}
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				return "Erro #7 - Erro ao avançar a primeira tarefa";
			}
		}
		return pmProcess.getCode();
	}

	public String montaDescricaoTarefaSimples(List<PostoSemReceitaVO> postos)
	{
		String descricao = "";

		String nomeCliente = postos.get(0).getNomeCliente();
		descricao = "Foi identificado que o cliente " + nomeCliente + " teve custo apropriado em pelo menos 90% da receita.<br/><br/>";

		descricao = descricao + "<table class=\"gridbox\" cellpadding=\"0\" cellspacing=\"0\" style=\"empty-cells: show\">";
		descricao = descricao + "<thead>";
		descricao = descricao + "<tr><th colspan=6>Tabela de Postos Sem Receita</th></tr>";
		descricao = descricao + "<tr>";
		descricao = descricao + "<th>Competência</th>";
		descricao = descricao + "<th>Mercado</th>";
		descricao = descricao + "<th>Regional</th>";
		descricao = descricao + "<th>Cliente</th>";
		descricao = descricao + "<th>Vlr. Receita</th>";
		descricao = descricao + "<th>Vlr. Custo</th>";
		descricao = descricao + "</tr>";
		descricao = descricao + "</thead>";
		descricao = descricao + "<tbody>";

		for (PostoSemReceitaVO posto : postos)
		{
			descricao = descricao + "<tr>";
			descricao = descricao + "<td>" + posto.getCompetencia() + "</td>";
			descricao = descricao + "<td>" + posto.getTipoMercado() + "</td>";
			descricao = descricao + "<td>" + posto.getNomeRegional() + "</td>";
			descricao = descricao + "<td>" + posto.getNomeCliente() + "</td>";
			descricao = descricao + "<td> R$ " + posto.getValorReceita() + "</td>";
			descricao = descricao + "<td> R$ " + posto.getValorCusto() + "</td>";
			descricao = descricao + "</tr>";
		}

		descricao = descricao + "</tbody>";
		descricao = descricao + "</table>";

		return descricao;
	}
}
