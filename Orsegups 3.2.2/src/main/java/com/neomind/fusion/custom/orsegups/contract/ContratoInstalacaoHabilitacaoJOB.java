package com.neomind.fusion.custom.orsegups.contract;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.Mail2Render;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLDateBetweenFilter;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

//  com.neomind.fusion.custom.orsegups.contract.ContratoInstalacaoHabilitacaoJOB
public class ContratoInstalacaoHabilitacaoJOB implements CustomJobAdapter
{

	private String MODELO_CONTRATO = "Fluxo Contratos";
	private String ATIVIDADE_CHECK_POINT= "Check Point Faturamento";
	private String debugline;
	
	@Override
	public void execute(final CustomJobContext ctx)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try{
			debugline = "";
			
			GregorianCalendar gcFiltro = new GregorianCalendar();
			gcFiltro.set(Calendar.MONTH, 0);
			gcFiltro.set(Calendar.DAY_OF_MONTH, 1);
			gcFiltro.set(Calendar.YEAR, 2019);
			
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("process.model.name", MODELO_CONTRATO));
			gp.addFilter(new QLFilterIsNull("process.finishDate"));
			gp.addFilter(new QLEqualsFilter("process.processState", ProcessState.running.ordinal()));
			gp.addFilter(new QLEqualsFilter("process.saved", Boolean.TRUE));
			gp.addFilter(new QLDateBetweenFilter("process.startDate", gcFiltro, new GregorianCalendar()));
			gp.addFilter(new QLFilterIsNull("finishDate"));
			gp.addFilter(new QLFilterIsNotNull("startDate"));
			//teste
			//gp.addFilter(new QLEqualsFilter("code", "000263"));
			
			List<Activity> atividades = PersistEngine.getObjects(Activity.class, gp);
			System.out.println("[FLUXO CONTRATOS]-" + ContratoUtils.retornaDataFormatoSapiens(new GregorianCalendar()) +  " - Job de monitoramento de OS - OSs em aberto." + atividades.size());
			for(Activity atividade : atividades)
			{
				//System.out.println(validaOSFinalizada("1508978"));
				
				debugline = "tarefa:"+atividade.getProcess().getCode() + " [";
				/*Collection<Activity> atividadesAbertas = processo.getOpenActivities();
				if(atividadesAbertas.size() > 0)
				{
					for(Activity act : atividadesAbertas)
					{
						
					}
				}*/
				
				debugline += "0,";
				if(atividade.getActivityName().equals(ATIVIDADE_CHECK_POINT))
				{
					debugline += "1,";
					//processos alvos
					NeoObject contrato = atividade.getProcess().getEntity();
					EntityWrapper wContrato = new EntityWrapper(contrato);
					
					Long tipoMovimento = (Long) wContrato.findValue("movimentoContratoNovo.codTipo");
					Long tipoAlteracao = 0L;
					if(tipoMovimento == 5)
						tipoAlteracao = (Long) wContrato.findValue("movimentoAlteracaoContrato.codTipo");
					
					boolean OSReducao = tipoAlteracao == 4 ? true : false;
					
					Collection<NeoObject> postos = wContrato.findField("postosContrato").getValues();
					int postosFaturados = 0;
					for(NeoObject posto : postos)
					{
						debugline += "2,";
						EntityWrapper wPosto = new EntityWrapper(posto);
						String ctr4debug = NeoUtils.safeOutputString(wPosto.findValue("numContrato"));
						
						String contaSIGMA = NeoUtils.safeOutputString(wPosto.findValue("contaSIGMA"));
						if(contaSIGMA == null || contaSIGMA.equals(""))
						{
							debugline += "3,";
							ContratoLogUtils.logInfo("Solicitado abertura de OS para o posto #" + posto.getNeoId() + ", porém o mesmo não possui conta SIGMA.");
							continue;
						}
						
						if(OSReducao)
						{
							debugline += "4,";
							String numeroOS = abreOsSigma(wContrato, posto, false, true);
							wPosto.setValue("numeroOS", numeroOS);

							//Task t = null;
							//act.finish(t);
							OrsegupsWorkflowHelper.finishTaskByActivity(atividade);
							
							debugline +="]";
							ContratoLogUtils.logInfo(debugline);
							return;
						}
						
						/*
						 * validar se é para abrir OS somente de Habilitação
						 */
						
						boolean abrirOSInstalacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSInstalacao"));
						boolean abrirOSReintalacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSReinstalacao"));
						boolean abrirOSHabilitacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSHabilitacao"));
						
						//ContratoLogUtils.logInfo("["+ctr4debug+"] debug -1 " );
						
						if(!abrirOSInstalacao && !abrirOSReintalacao && !abrirOSHabilitacao)
						{
							debugline += "5,";
							//se não foi solicitado abrir OS de Instalação ou Reinstalação ou Habilitação, 
							//não fazer nada
							//considerar como faturado
							postosFaturados++;
							continue;
						}
						
						boolean instalado = NeoUtils.safeBoolean(wPosto.findValue("instalado"));
						boolean habilitado = NeoUtils.safeBoolean(wPosto.findValue("habilitado"));
						boolean faturado = NeoUtils.safeBoolean(wPosto.findValue("faturado"));
						
						/*
						 * se é para abrir somente OS de habilitação, os equipamentos já estão instalados
						 */
						if(abrirOSHabilitacao)
						{
							debugline += "6,";
							instalado = true;
						}
						
						if(faturado)
						{
							debugline += "7,";
							postosFaturados++;
							continue;
						}
						
						if(!instalado)
						{
							debugline += "8,";
							String numeroOSInstalacao = NeoUtils.safeOutputString(wPosto.findValue("numeroOS"));
							if(numeroOSInstalacao.equals(""))
							{
								debugline += "8.1,";
								numeroOSInstalacao = abreOsSigma(wContrato, posto, false, false);
								wPosto.setValue("numeroOS", numeroOSInstalacao);
							}
							
							if(validaOSFinalizada(numeroOSInstalacao))
							{
								debugline += "8.2,";
								wPosto.setValue("instalado", true);
								instalado = true;
								//enviar email posto instalado
							}
							
						}
						debugline += "9,";
						if(instalado && !habilitado)
						{
							debugline += "9.1,";
							String numeroOSHabilitacao = NeoUtils.safeOutputString(wPosto.findValue("numeroOSHabilitacao"));
							if(numeroOSHabilitacao.equals("") && abrirOSHabilitacao)
							{ 
								debugline += "9.1,";
								numeroOSHabilitacao = abreOsSigma(wContrato, posto, true, false);
								wPosto.setValue("numeroOSHabilitacao", numeroOSHabilitacao);
								//valida se esta instalado, se estiver abre habilitacao
								//ja esta instalado, deve validar se esta habilitado
								
							}
							
							if(validaOSFinalizada(numeroOSHabilitacao))
							{
								debugline += "9.2,";
								wPosto.setValue("habilitado", true);
								habilitado = true;
								//enviar email posto habilitado?
							}
							
						}
						
						if(instalado && habilitado && abrirOSHabilitacao && abrirOSInstalacao) // quando requer instalação + habilitação
						{
							debugline += "10,";
							//enviar email posto faturado?
							//liberaFaturamentoPosto(wContrato, posto); /Comentada chamada do método para não fazer update no campo usu_libfat ts-171826
							wPosto.setValue("faturado", true);
							//tendo um posto faturado, pode liberar o faturamento do contrato
							//liberaFaturamentoContrato(wContrato);
							postosFaturados++;
						}else if (!abrirOSInstalacao && habilitado){ // apenas habilitação
							debugline += "11,";
							//enviar email posto faturado?
							//liberaFaturamentoPosto(wContrato, posto);
							wPosto.setValue("faturado", true);
							//tendo um posto faturado, pode liberar o faturamento do contrato
							//liberaFaturamentoContrato(wContrato);
							postosFaturados++;
						}else if (!abrirOSHabilitacao && instalado){ // apenas instalação
							debugline += "12,";
							//enviar email posto faturado?
							//liberaFaturamentoPosto(wContrato, posto);
							wPosto.setValue("faturado", true);
							//tendo um posto faturado, pode liberar o faturamento do contrato
							//liberaFaturamentoContrato(wContrato);
							postosFaturados++;
						}
						debugline += "13,";	
					}
					
					if(postos.size() == postosFaturados)
					{
						debugline += "14";
						//Task t = null;
						//act.finish(t);
						OrsegupsWorkflowHelper.finishTaskByActivity(atividade);
					}
				}
				
				debugline +="]";
				//ContratoLogUtils.logInfo(debugline);
			}
			
		}catch(Exception e){
			ContratoLogUtils.logInfo("["+key+"] Erro ao processa Job ContratoInstalacaoHabilitacaoJOB  " + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processa Job Verifique no log por ["+key+"] para maiores detalhes");
		}
		
		

	}
	
	public static void liberaFaturamentoContrato(EntityWrapper wrapper)
	{
		String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		if(codctr.equals(""))
			codctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		String query = "UPDATE USU_T160CTR set usu_libfat = 'S' where usu_codfil = " + codfil+
				" and usu_codemp = " + codemp + " and usu_numctr = " + codctr;

		PreparedStatement stm = null;
		Connection connection = null;
		try
		{	
			connection = OrsegupsUtils.getSqlConnection("SAPIENS");
			stm = connection.prepareStatement(query);
			stm.executeUpdate();		
		}catch (Exception e) {
			e.printStackTrace();
			ContratoLogUtils.logInfo("Erro ao liberar contrato para faturamento - ctr:" + codctr + ", codfil:"+codfil + ", codemp:"+codemp);
			throw new WorkflowException("Erro - " + e.getMessage());
		}finally
		{
			OrsegupsUtils.closeConnection(connection, stm, null);
		}
		
		/*StringBuilder sql = new StringBuilder();
		sql.append(query);
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			queryExecute.executeUpdate();
		}catch(Exception e)
		{
			e.printStackTrace();
			ContratoLogUtils.logInfo("Erro ao liberar contrato para faturamento - ctr:" + codctr + ", codfil:"+codfil + ", codemp:"+codemp);
			throw new WorkflowException("Erro - " + e.getMessage());
		}*/
	}
	
	public static void liberaFaturamentoPosto(EntityWrapper wrapper, NeoObject posto)
	{
		String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		if(codctr.equals(""))
			codctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		EntityWrapper wPosto = new EntityWrapper(posto);
		String numposto = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
		//String dDatHoj = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		String dDatHoj = ContratoUtils.retornaDataFormatoSapiens(new GregorianCalendar());
		
		/* , usu_datini = '"+dDatHoj+"' */
		String query = "UPDATE USU_T160CVS set usu_libfat = 'S' where usu_codfil = " + codfil+
				" and usu_codemp = " + codemp + " and usu_numctr = " + codctr + " and usu_numpos = " + numposto;
		PreparedStatement stm = null;
		Connection connection = null;
		try
		{	
			connection = OrsegupsUtils.getSqlConnection("SAPIENS");
			stm = connection.prepareStatement(query);
			stm.executeUpdate();		
		}catch (Exception e) {
			e.printStackTrace();
			ContratoLogUtils.logInfo("Erro ao liberar posto para faturamento - ctr:" + codctr + ", numposto:"+numposto+", codfil:"+codfil + ", codemp:"+codemp);
			throw new WorkflowException("Erro - " + e.getMessage());
		}finally
		{
			OrsegupsUtils.closeConnection(connection, stm, null);
		}
		
		/*StringBuilder sql = new StringBuilder();
		sql.append(query);
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			queryExecute.executeUpdate();
		}catch(Exception e)
		{
			e.printStackTrace();
			ContratoLogUtils.logInfo("Erro ao liberar posto para faturamento - ctr:" + codctr + ", numposto:"+numposto+", codfil:"+codfil + ", codemp:"+codemp);
			throw new WorkflowException("Erro - " + e.getMessage());
		}*/
	}
	
	public static boolean sendMail(Activity activity, NeoObject posto, String operacao)
	{
		Set<Mail2Render> setMail2Render = new HashSet<Mail2Render>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("Entity", activity.getProcessEntity());
		params.put("Process", activity.getProcess());
		params.put("activity", activity);
		params.put("posto", posto);
		
		String modeloEmail = "mail/OrsegupsContrato.jsp";
		Collection<NeoUser> usuarios = new HashSet<NeoUser>();
		for(NeoUser u : usuarios)
		{
			Mail2Render m = new Mail2Render(u,modeloEmail , params, null,new GregorianCalendar());
			setMail2Render.add(m);
		}

		for (Mail2Render m : setMail2Render)
		{
			FusionRuntime.getInstance().getMailEngine().sendEMail(m);
		}
		
		return true;
	}
	
	public static boolean validaOSFinalizada(String numeroOS)
	{
		Boolean retorno = false;
		if (numeroOS == null || "".equals(numeroOS)){
			return false;
		}
		
		StringBuilder sql = new StringBuilder();
		sql.append("select "); 
		sql.append("os.ID_ORDEM ");
		sql.append("from dbordem os ");
		sql.append("where ");
		sql.append("os.FECHADO = 1 and ");
		sql.append("os.FECHAMENTO is not null and ");
		sql.append("os.ID_ORDEM = " + numeroOS);
		ResultSet rs = null;
		try{
			Connection con = PersistEngine.getConnection("SIGMA90");
			PreparedStatement pst = con.prepareStatement(sql.toString());
			
			rs = pst.executeQuery();
			if (rs.next()){
				retorno = true;
			}else{
				retorno = false;
			}
			rs.close();
			rs = null;
			if (pst != null) { pst.close(); pst = null;}
			if (con != null && !con.isClosed()){con.close(); con = null;}
			
			return retorno;
		}catch(Exception e){
			if (!retorno){
				ContratoLogUtils.logInfo("[ERRO JOB] - validaOSFinalizada("+numeroOS+") retornou exceção:"+e.getMessage());
			}
			return retorno;
		}
		
		
	}
	
	/**
	 * 
	 * @param wrapper
	 * @param posto
	 * @param habilitacao - abrir OS de Habilitação, senão será de Instalação/Reinstalação
	 * @param reducao - abrir OS de Redução de Equipamento
	 * @return
	 * @throws Exception 
	 */
	public static String abreOsSigma(EntityWrapper wrapper, NeoObject posto, boolean habilitacao, boolean reducao) throws Exception
	{
		Map<String, String> map = montaParametrosOS(wrapper, posto, habilitacao, reducao);
		
		String resposta = "";
		try{
			resposta = new ContratoContasSIGMAWebService().abrerturaOSSigma(map);
		}catch(Exception e){
			throw e;
		}
		
		if(!resposta.equals(""))
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			String description = NeoUtils.safeOutputString(wPosto.findValue("observacaoOS"));
			String representante = (String) wPosto.findValue("representante.nomrep");
			description += " Executivo comercial: " + representante;  
			
			ContratoLogUtils.logInfo("DESCRIÇÃO OS => "+description);
			try {
				ContratoCadastraContasSIGMA.updateDescricaoOS(resposta, description);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return resposta;
		}else
		{
			throw new Exception("Abertura de OS: " + resposta);
		}
	}
	
	public static Map<String, String> montaParametrosOS(EntityWrapper wrapper, NeoObject posto, boolean habilitacao, boolean reducao)
	{
		Map<String, String> map = new HashMap<String, String>();
		EntityWrapper wPosto = new EntityWrapper(posto);
		
		/*
		 * 
		 * ---------------------------------------------------------------------------------------------------------------------------------------
			Parameter Name 			| Type		| Required	| Default Value	| Max Size	| Allowed Values		|	About
			---------------------------------------------------------------------------------------------------------------------------------------
			clientId				| Int		| Não		|				|			|						| Code client
			partition				| Int		| Sim*		|				|			|						| Partition
			centralId				| String	| Sim*		|				|			|						| Code central
			companyId				| Int		| Sim*		|				|			|						| Company code
			scheduledTime			| Date		| Não		|				|			|						| Scheduled Time of service order
			estimatedTime			| Int		| Não		|    1			|			| {1,2,3,5,8,12,24,48}	| Estimated Time for service order
			description				| String	| Não		|				|    500	|						| Description service order
			requester				| Int		| Sim		|				|			|						| Requesting service order
			defect					| Int		| Sim		|				|			|						| Code defect service order
			responsibleTechnician	| Int		| Sim		|				|			|						| Responsible Technician
			---------------------------------------------------------------------------------------------------------------------------------------
			
			defect:
			134 - OS - habilitar monitoramento
			178 - OS - Realizar Reinstalação
 			184 - OS - Realizar instalação
 			1156 - OS-RASTR-RETIRAR INICIATIVA
 			1161 - OS-RASTR-REALIZAR INSTALAÇÃ
		 * 
		 */
		String clientId = NeoUtils.safeOutputString(wPosto.findValue("contaSIGMA"));
		String partition = NeoUtils.safeOutputString(wPosto.findValue("particao"));
		String companyId = NeoUtils.safeOutputString(wPosto.findValue("regionalPosto.usu_codreg"));

		String numeroContaSigma = NeoUtils.safeOutputString(wPosto.findValue("contaSIGMA"));
		String centralId = getCentralId(numeroContaSigma);
		
		//3-SOLICITADO PELO CLIENTE
		String requester = "10011";
		String defect = "";
		String representante = (String) wPosto.findValue("representante.nomrep");
		String description = NeoUtils.safeOutputString(wPosto.findValue("observacaoOS"));
		description += " Executivo comercial: " + representante;  
		
		if (habilitacao){
			description = description.replace("HABILITAR", "");
			description = description.replace("HABILITAÇÃO", "");
			description = description.replace("INSTALAR", "HABILITAR");
			description = description.replace("INSTALAÇÃO", "HABILITAÇÃO");
			description = description.replace("instalar", "habilitar");
			description = description.replace("instalação", "habilitação");
		}
		
		if (description.length() > 500){
			throw new WorkflowException("Observação da OS só pode ter no máximo 500 caracteres ");
		}
		
		description = ContratoContasSIGMAWebService.retiraCaracteresAcentuados(description);
		if(habilitacao)
		{
			defect = "134";
			//description = "TESTE TI - CANCELAR OS";
		}else if(reducao)
		{
			/**
			* Verifica se a conta é de Rastreamento
			* 
			* 1156 - OS-RASTR-RETIRAR INICIATIVA
			*/
			if(centralId.startsWith("R")) {
				defect = "1156";
			}else {
				defect = "167";				
			}
			
		}else
		{
			boolean tpInstalacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSInstalacao"));
			boolean tpIeinstalacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSReinstalacao"));
			if(tpInstalacao)
			{
				/**
				* Verifica se a conta é de Rastreamento
				* 
				* 1161 - OS-RASTR-REALIZAR INSTALAÇÃ
				*/
				if(centralId.startsWith("R")) {
					defect = "1161";
				}else {
					defect = "184";
				}
				//description = "TESTE TI - CANCELAR OS";
			}else if(tpIeinstalacao)
			{
				defect = "178";
				//description = "TESTE TI - CANCELAR OS";
			}
		}	
		
		String responsibleTechnicianId = "";
		if(!companyId.equals(""))
		{
			NeoObject empresa = PersistEngine.getObject(AdapterUtils.getEntityClass("FGCRelacaoClienteSigmaSapiens"), new QLEqualsFilter("codClienteSapiens", companyId));
			if(empresa != null)
			{
				EntityWrapper wEmpresa = new EntityWrapper(empresa);
				companyId = NeoUtils.safeOutputString(wEmpresa.findValue("codClienteSigma"));
				/*
				NeoObject cadastroInstResp = PersistEngine.getObject(AdapterUtils.getEntityClass("FGCCadastroResponsavelInstalador"), new QLEqualsFilter("ativo", true));
				if(cadastroInstResp == null)
				{
					throw new WorkflowException("ERRO, comunicar a TI. Não encontrado responsavel tecnico ativo (FGCCadastroResponsavelInstalador).");
				}
				EntityWrapper wCadastroInstResp = new EntityWrapper(cadastroInstResp);
				responsibleTechnicianId = NeoUtils.safeOutputString(wCadastroInstResp.findValue("responsavelTecnico.cd_colaborador"));*/
				//String nomeTecRespCompleto = NeoUtils.safeOutputString(wCadastroInstResp.findValue("responsavelTecnico.nm_colaborador"));
				//String nomeTecResp = nomeTecRespCompleto.substring(5, nomeTecRespCompleto.length());
				//responsibleTechnicianId = new ContratoContasSIGMAWebService().buscaListaInstaladores(companyId, nomeTecResp, wEmpresa.findValue("siglaRegional").toString());
			}else
			{
				throw new WorkflowException("ERRO, comunicar a TI. Não encontrada relação regional SAPIENS x empresa SIGMA (FGCCadastroResponsavelInstalador). Regional Sapiens : " + companyId);
			}
		}
		
		if (wPosto.findValue("buscaOperadorCM.cd_colaborador") != null){
			System.out.println("[FLUXO CONTRATOS]- Reponsavel Tecnico CM OS - " + NeoUtils.safeOutputString(wPosto.findValue("buscaOperadorCM.cd_colaborador")));
			responsibleTechnicianId = NeoUtils.safeOutputString(wPosto.findValue("buscaOperadorCM.cd_colaborador"));
		}else{
			throw new WorkflowException("ERRO, comunicar a TI. Não encontrado responsavel tecnico ativo (buscaOperadorCM).");
		}
		
		/*if (wPosto.findValue("buscaOperadorCM.cd_colaborador") != null){
			responsibleTechnicianId = NeoUtils.safeOutputString(wPosto.findValue("buscaOperadorCM.cd_colaborador"));
		}else{
			throw new WorkflowException("ERRO, comunicar a TI. Não encontrado responsavel tecnico ativo (FGCCadastroResponsavelInstalador).");
		}*/
		
		map.put("clientId", clientId);
		map.put("partition", partition);
		map.put("centralId", centralId);
		map.put("companyId", companyId);
		map.put("scheduledTime", "");
		map.put("estimatedTime", "");
		map.put("description", ( description.length() >= 500 ? description.substring(0,499): description ));
		map.put("requester", requester);
		map.put("defect", defect);
		map.put("responsibleTechnician",  responsibleTechnicianId);

		return map;
	}
	
	public static String getCentralId(String numeroConta)
	{
		String centralId = "";
		
		StringBuilder sql = new StringBuilder();
		sql.append("select "); 
		sql.append("conta.id_central ");

		sql.append("from dbcentral conta ");
		
		sql.append("where ");
		sql.append("conta.cd_cliente = " + numeroConta);
		
		//System.out.println("[FLUXO CONTRATOS]-job OS sigma: "+sql.toString());
		if (numeroConta != null && !numeroConta.equals("")){
			Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();
			if (resultList != null && !resultList.isEmpty())
			{
				for (Object result : resultList)
				{
					if (result != null)
					{
						centralId = NeoUtils.safeOutputString(result);
					}
				}
			}
		}
		return centralId;
	}

}
