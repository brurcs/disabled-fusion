package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.velocity.VelocityContext;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.dto.HistoricoOrcamentoDTO;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipOrcamentoUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.VelocityUtils;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class FAPSlipExtratoOrcamentoConverter extends EntityConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		return super.getHTMLInput(field, origin);
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{		
		NeoObject noHistoricoOrcamento = field.getForm().getObject();
		NeoObject noContratoOrcamento = new EntityWrapper(noHistoricoOrcamento).findGenericValue("orcamento");
		
		List<HistoricoOrcamentoDTO> listHistorico = new ArrayList<>();
		
		listHistorico.addAll(FAPSlipOrcamentoUtils.gerarHistoricoCreditosOrcamento(noContratoOrcamento));
		listHistorico.addAll(FAPSlipOrcamentoUtils.gerarHistoricoDebitosOrcamento(noContratoOrcamento));
		ordernarData(listHistorico);
		
		listHistorico.addAll(FAPSlipOrcamentoUtils.gerarHistoricoConsumoPrevistoAtualContratos(noContratoOrcamento, null, listHistorico));
		
		StringBuilder historicoItem = new StringBuilder();
		String trHistorico = "" +
				"<tr id=\"0\" uid=\"list_historicoItem__\" entitytype=\"FAPSlipHistoricoOrcamento\" idx=\"0\" style=\"visibility: visible; color: ${color}\">\r\n" +
				"	<td>${data}</td>\r\n" +
				"	<td>${vencimento}</td>\r\n" +
				"	<td>${transacao}</td>\r\n" + 
				"	<td>${descricao}</td>\r\n" + 
				"	<td>${tarefa}</td>\r\n" + 
				"	<td>${valor}</td>\r\n" + 
				"	<td>${saldo}</td>\r\n" + 
				"</tr>";
		
		BigDecimal saldo = new BigDecimal(0l);
		
		GregorianCalendar ultimaVigencia = null;
		for (HistoricoOrcamentoDTO historico : listHistorico)
		{
			Map<String, String> parametrosItem = new HashMap<String, String>();
			if (!historico.getTransacao().equals("Consumo Previsto"))
			{
				if (ultimaVigencia != null && (ultimaVigencia.before(historico.getData()) && ultimaVigencia.before(new GregorianCalendar())))
				{
					parametrosItem.put("data", NeoDateUtils.safeDateFormat(ultimaVigencia, "dd/MM/yyyy"));
					parametrosItem.put("vencimento", "");
					parametrosItem.put("transacao", "Débito Automático");
					parametrosItem.put("descricao", "Débito pelo Fim da Vigência do Orçamento");
					parametrosItem.put("tarefa", "Sistema");
					parametrosItem.put("valor", parseMonetario(new BigDecimal(0).subtract(saldo)));
					parametrosItem.put("color", "red");
					saldo = new BigDecimal(0);
					parametrosItem.put("saldo", parseMonetario(saldo));
					
					StrSubstitutor sub = new StrSubstitutor(parametrosItem);
					historicoItem.append(sub.replace(trHistorico));
				}
				
				parametrosItem = new HashMap<String, String>();
				
				if (historico.getTransacao().equals("Crédito"))
				{
					saldo = saldo.add(historico.getValor());
					ultimaVigencia = historico.getFimVigencia();
					
					parametrosItem.put("valor", parseMonetario(historico.getValor()));
					parametrosItem.put("data", NeoDateUtils.safeDateFormat(historico.getData(), "dd/MM/yyyy") + " à " + NeoDateUtils.safeDateFormat(historico.getFimVigencia(), "dd/MM/yyyy"));
					parametrosItem.put("vencimento", "");
				}
				else
				{
					saldo = saldo.subtract(historico.getValor());

					parametrosItem.put("valor", parseMonetario(new BigDecimal(0).subtract(historico.getValor())));
					parametrosItem.put("data", historico.getData() == null ? "" : NeoDateUtils.safeDateFormat(historico.getData(), "MM/yyyy"));
					parametrosItem.put("vencimento", historico.getVencimento() == null ? "" : NeoDateUtils.safeDateFormat(historico.getVencimento(), "dd/MM/yyyy"));
				}
				
				parametrosItem.put("transacao", historico.getTransacao());
				parametrosItem.put("descricao", historico.getDescricao());
				parametrosItem.put("tarefa", historico.getTarefa());
				
				parametrosItem.put("saldo", parseMonetario(saldo));
				
				StrSubstitutor sub = new StrSubstitutor(parametrosItem);
				historicoItem.append(sub.replace(trHistorico));
			}
			else
			{
				if (historico.getValor().signum() == 1)
					saldo = saldo.subtract(historico.getValor());
				else 
					saldo = saldo.add(historico.getValor());
				
				parametrosItem.put("data", NeoDateUtils.safeDateFormat(historico.getData(), "dd/MM/yyyy") + " à " + NeoDateUtils.safeDateFormat(historico.getFimVigencia(), "dd/MM/yyyy"));
				parametrosItem.put("vencimento", "");
				parametrosItem.put("transacao", historico.getTransacao());
				parametrosItem.put("descricao", historico.getDescricao());
				parametrosItem.put("tarefa", "Sistema");
				parametrosItem.put("valor", parseMonetario(new BigDecimal(0).subtract(historico.getValor())));
				parametrosItem.put("color", "blue");
				parametrosItem.put("saldo", parseMonetario(saldo));
				
				StrSubstitutor sub = new StrSubstitutor(parametrosItem);
				historicoItem.append(sub.replace(trHistorico));
			}
		}

		VelocityContext context = new VelocityContext();
		context.put("debitos", historicoItem);
		
		return VelocityUtils.runTemplate("custom/orsegups/FAPHistoricoOrcamento.vm", context);
	}
	
	private List<NeoObject> buscarCadastroOrcamento(NeoObject noOrcamento)
	{
		QLGroupFilter andFilter = new QLGroupFilter("AND", new QLRawFilter("_vo.wfprocess.processState <> 2"), new QLEqualsFilter("novoContrato", noOrcamento), new QLEqualsFilter("operacao.codigo", 3l));
		return PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), andFilter);
	}
	
	private Boolean contratoExisteHistorico(Long codContrato, List<HistoricoOrcamentoDTO> listHistorico)  
	{
		if (listHistorico != null && !listHistorico.isEmpty())
		{
			for (HistoricoOrcamentoDTO historico : listHistorico)
			{
				if (historico.getCodContrato() == codContrato) return true;
			}
		}
		
		return false;
	}

	private void ordernarData(Collection<? extends HistoricoOrcamentoDTO> list)
	{
		Collections.sort((List<? extends HistoricoOrcamentoDTO>) list, new Comparator<HistoricoOrcamentoDTO>()		
		{
			@Override
			public int compare(HistoricoOrcamentoDTO o1, HistoricoOrcamentoDTO o2)
			{
				if (o1.getData() == null || o2.getData() == null) 
				      return 0;
				else
					return o1.getData().compareTo(o2.getData());
			}
		});
	}
	
	private String parseMonetario(BigDecimal value)
	{
		NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
		
		if (value != null)
		{
			BigDecimal returnValue = value;
			returnValue = returnValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			return monetaryFormat.format(returnValue);
		}
		
		return "R$ 0,00";
	}
}
