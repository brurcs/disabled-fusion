package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.ArrayList;
import java.util.Collection;

public class AcessoListaVO
{
	private Long total;
	private Collection<AcessoVO> rows = new ArrayList();
	
	public Long getTotal()
	{
		return total;
	}
	public void setTotal(Long total)
	{
		this.total = total;
	}
	public Collection<AcessoVO> getRows()
	{
		return rows;
	}
	public void setRows(Collection<AcessoVO> rows)
	{
		this.rows = rows;
	}
}
