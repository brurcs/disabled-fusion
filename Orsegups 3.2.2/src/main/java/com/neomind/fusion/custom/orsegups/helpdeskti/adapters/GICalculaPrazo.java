package com.neomind.fusion.custom.orsegups.helpdeskti.adapters;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class GICalculaPrazo implements AdapterInterface
{
	
	private static final Log log = LogFactory.getLog(GICalculaPrazo.class);
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		
		try{
		GregorianCalendar prazo = new GregorianCalendar();
		
		String nomeSeveridade = (String) processEntity.findValue("severidade.nomeSeveridade");
		Long tempoSolucaoPrioridade = (Long) processEntity.findValue("prioridade.SLATempoSolucao");
		Long tempoSolucaoSeveridade = (Long) processEntity.findValue("severidade.SLATempoSolucao");
		Long tempoTotal = 0L;
		Long tempoDias = 0L;
		
		if (tempoSolucaoPrioridade != null && tempoSolucaoPrioridade != 0L &&
				tempoSolucaoSeveridade != null && tempoSolucaoSeveridade != 0L)
		{
			tempoTotal = tempoSolucaoPrioridade + tempoSolucaoSeveridade;
			
			if (nomeSeveridade.equals("Crítica")) 
			{
				prazo = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
			} 
			else 
			{
				tempoDias = tempoTotal / 60 /24;
				prazo  = OrsegupsUtils.getSpecificWorkDay(prazo,tempoDias);
			}
			
			prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			prazo.set(GregorianCalendar.MINUTE, 59);
			prazo.set(GregorianCalendar.SECOND, 59);
			System.out.println( NeoUtils.safeDateFormat((GregorianCalendar)processEntity.getValue("prazo"), "dd/MM/yyyy HH:mm:ss"));
			processEntity.setValue("prazo", prazo);

		}
	}
	catch (Exception e)
	{
		log.error(" Fluxo TI - Calcula Prazo - Por favor, contatar o administrador do sistema! " + e.getMessage().toString());
		throw new WorkflowException(" Fluxo TI - Calcula Prazo - Por favor, contatar o administrador do sistema! " + e.getMessage().toString());
	}
	}
}