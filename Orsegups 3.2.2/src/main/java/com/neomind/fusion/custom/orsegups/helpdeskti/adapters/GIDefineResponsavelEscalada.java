package com.neomind.fusion.custom.orsegups.helpdeskti.adapters;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/*
 * @author fernando.rebelo
 * 
 * Adapter entre as atividades de Atender Chamado e Realizar Triagem
 * Valida informações e retorna o usuário/papel que irá receber a tarefa 
 */
public class GIDefineResponsavelEscalada implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(GIDefineResponsavelEscalada.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		/*
		 * Verifica quem Originou a tarefa que avançou para este adapter
		 * quando for escalada, a partir da tarefa 'Realizar triagem',
		 * deve retornar para o usuário superior a triagem escalada
		 * Essa lógica é utilizada no caso de, na tarefa de 'Realizar Triagem',
		 * escale para um papel/grupo como responsável
		 * se for uma transferencia, desconsiderar
		 * pois o responsável vai ser setado abaixo
		 *
		 */
		

		String responsavelStr = "";
		String papel = "";
		if (NeoUtils.safeIsNotNull((processEntity.findField("papelNivelSuperior").getValue()))) {
			String papelSup = processEntity.findField("papelNivelSuperior").getValue().toString();
			System.out.println(papelSup);
		}
		
			System.out.println(processEntity.findField("wfprocess.code").getValue());
		if(processEntity.findField("wfprocess.code").getValue().equals("000106"))
			System.out.println(processEntity.findField("wfprocess.code").getValue());
		try
		{
			
			System.out.println(papel);
			NeoPaper neoPaper = null;
			if (NeoUtils.safeIsNull(processEntity.findField("papelNivelSuperior").getValue()))
				papel = "GI";
			else
			if (NeoUtils.safeIsNotNull(processEntity.findField("papelNivelSuperior").getValue()))
			{
				responsavelStr = (String) processEntity.findField("papelNivelSuperior.code").getValue();
				if (responsavelStr.equals("GI"))
				{
					papel = "GICoordenadorTI";
				}
				else if(responsavelStr.equals("GICoordenadorTI"))
				{
					papel = "GIDiretoriaTI";
				}else if(responsavelStr.equals("GIDiretoriaTI"))
					papel = "GIPresidencia";
				
			}
			if(NeoUtils.safeIsNotNull(papel))
			processEntity.findField("papelNivelSuperior").setValue((NeoPaper)OrsegupsUtils.getPaper(papel));	
		}
		catch (Exception e)
		{
			log.error(" Fluxo TI - Triagem escalada - Por favor, contatar o administrador do sistema! " + e.getMessage().toString());
			throw new WorkflowException(" Fluxo TI - Triagem escalada - Por favor, contatar o administrador do sistema! " + e.getMessage().toString());
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
