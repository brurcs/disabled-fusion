package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.ApontamentoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaAlterarInicioFaturamento implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection connSig = PersistEngine.getConnection("SIGMA90");
		Connection connSap = PersistEngine.getConnection("SAPIENS");
		StringBuilder sql = new StringBuilder();
		ResultSet rs = null;
 		PreparedStatement psSig = null;
 		PreparedStatement psSap = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAlterarInicioFaturamento");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Rotina Alterar Inicio Faturamento - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		try
		{
			sql.append(" select ord.ID_ORDEM,cen.CD_CLIENTE,cen.ID_CENTRAL,cen.PARTICAO,ctr.usu_codemp,ctr.usu_codfil,ctr.usu_numctr,pos.usu_numpos,pos.usu_codser,ctr.usu_datini as datIniCtr,pos.usu_datini as datIniPos,ord.FECHAMENTO as FECHAMENTO                ");
			sql.append(" from dbo.dbORDEM ord                                                                                                                                                                                                                          ");
			sql.append(" inner join dbo.dbCENTRAL cen on cen.CD_CLIENTE = ord.CD_CLIENTE 																																                                               ");
			sql.append(" inner join [fsoodb04\\sql02].sapiens.dbo.usu_t160sig sig on sig.usu_codcli = ord.cd_cliente 																		 							                                               ");
			sql.append(" inner join [fsoodb04\\sql02].sapiens.dbo.usu_t160cvs pos on pos.usu_codemp = sig.usu_codemp and pos.usu_codfil = sig.usu_codfil and pos.usu_numctr = sig.usu_numctr and pos.usu_numpos = sig.usu_numpos 						               "); 
			sql.append(" inner join [fsoodb04\\sql02].sapiens.dbo.usu_t160ctr ctr on ctr.usu_codemp = pos.usu_codemp and ctr.usu_codfil = pos.usu_codfil and ctr.usu_numctr = pos.usu_numctr 							                                               ");
			sql.append(" inner join [fsoodb04\\sql02].sapiens.dbo.E085CLI cli on cli.codcli = ctr.usu_codcli                                                                                                                                                           ");
			sql.append(" where ord.IDOSDEFEITO in (134,178,184,1161)                                                                                                                                                                                                        ");
		    sql.append(" and ord.FECHAMENTO >= '2017-08-19'                                                                                                                                                                                                            ");
			sql.append(" and (ord.FECHADO = 1 and cen.FG_ATIVO = 1 and cen.CTRL_CENTRAL = 1)																			                                                                                               ");
			sql.append(" and cli.tipemc = 1 																	                                                                                                                                                       ");
			sql.append(" and ((ctr.usu_susfat = 'S') OR (pos.usu_susfat = 'S'))                                                            			                                                                                                                   ");
			sql.append(" and (ctr.usu_sitctr = 'A' or (ctr.usu_sitctr = 'I' and ctr.usu_datfim > getdate())) 			                                                                                                                                               ");
			sql.append(" and (pos.usu_sitcvs = 'A' or (pos.usu_sitcvs = 'I' and pos.usu_datfim > getdate())) 			                                                                                                                                               ");
			sql.append(" and pos.usu_datini <= cast(floor(cast(ord.FECHAMENTO as float)) as datetime) 					                                                                                                                                               ");
			sql.append(" group by ord.ID_ORDEM,cen.CD_CLIENTE,cen.ID_CENTRAL,cen.PARTICAO,ctr.usu_codemp,ctr.usu_codfil,ctr.usu_numctr,pos.usu_numpos,pos.usu_codser,ctr.usu_datini,pos.usu_datini,ord.FECHAMENTO                                                      ");
			sql.append(" UNION ALL                                                                                                                             															                                               ");
			sql.append(" select ord.ID_ORDEM,cen.CD_CLIENTE,cen.ID_CENTRAL,cen.PARTICAO,ctr.usu_codemp,ctr.usu_codfil,ctr.usu_numctr,pos.usu_numpos,pos.usu_codser, ctr.usu_datini as datIniCtr,pos.usu_datini as datIniPos,p.DT_PAUSA as FECHAMENTO                   ");
			sql.append(" from dbo.dbORDEM ord                                                                                                                                                                                                                          ");
			sql.append(" inner join dbo.dbCENTRAL cen on cen.CD_CLIENTE = ord.CD_CLIENTE                                                                                                                                                                               ");
			sql.append(" inner join dbo.ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p on p.CD_ORDEM_SERVICO = ord.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM                                                               ");
			sql.append(" ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO)                                                                                                                                                           ");
			sql.append(" inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160sig sig on sig.usu_codcli = ord.cd_cliente                                                                                                                                                   ");
			sql.append(" inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160cvs pos on pos.usu_codemp = sig.usu_codemp and pos.usu_codfil = sig.usu_codfil and pos.usu_numctr = sig.usu_numctr and pos.usu_numpos = sig.usu_numpos                                       ");
			sql.append(" inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160ctr ctr on ctr.usu_codemp = pos.usu_codemp and ctr.usu_codfil = pos.usu_codfil and ctr.usu_numctr = pos.usu_numctr                                                                           ");
			sql.append(" inner join [FSOODB04\\SQL02].sapiens.dbo.E085CLI cli on cli.codcli = ctr.usu_codcli                                                                                                                                                           ");
			sql.append(" where ord.IDOSDEFEITO in (134,178,184)                                                                                                                                                                                                        ");
			sql.append(" and ord.FECHAMENTO >= '2017-08-19'                                                                                                                                                                                                            ");
			sql.append(" and (ord.FECHADO = 3 and cen.FG_ATIVO = 1 and cen.CTRL_CENTRAL = 1)                                                                                                                                                                           ");
			sql.append(" and cli.tipemc = 1                                                                                                                                                                                                                            ");
			sql.append(" and ((ctr.usu_susfat = 'S') or (pos.usu_susfat = 'S'))                                                                                                                                                                                        ");
			sql.append(" and (ctr.usu_sitctr = 'A' or (ctr.usu_sitctr = 'I' and ctr.usu_datfim > getdate()))                                                                                                                                                           ");
			sql.append(" and (pos.usu_sitcvs = 'A' or (pos.usu_sitcvs = 'I' and pos.usu_datfim > getdate()))                                                                                                                                                           ");
			sql.append(" and pos.usu_datini <= cast(floor(cast(p.DT_PAUSA as float)) as datetime)                                                                                                                                                                      ");
			sql.append(" group by ord.ID_ORDEM,cen.CD_CLIENTE,cen.ID_CENTRAL,cen.PARTICAO,ctr.usu_codemp,ctr.usu_codfil,ctr.usu_numctr,pos.usu_numpos,pos.usu_codser,ctr.usu_datini,pos.usu_datini,p.DT_PAUSA                                                          ");
			sql.append(" order by 5,6,7,8                                                                                                                                                                                                                              ");
			
			
			psSig = connSig.prepareStatement(sql.toString());
			rs = psSig.executeQuery();
			Integer numCtrAnt = 0;
			
			StringBuilder htmlFim = new StringBuilder();
			StringBuilder htmlPos = new StringBuilder();
			
			htmlFim.append("\n <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> ");
			htmlFim.append("\n <html> ");
			htmlFim.append("\n 		<head> ");
			htmlFim.append("\n 			<meta charset=\"ISO-8859-1\"> ");
			htmlFim.append("\n 			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");
			htmlFim.append("\n 		</head> ");
			htmlFim.append("\n 		<body yahoo=\"yahoo\"> ");
			
			htmlFim.append("\n 		Prezado(a), ");
			htmlFim.append("\n 		<br> ");
			htmlFim.append("\n 		Segue abaixo listagem de contratos e postos que foram ajustados a data inicio do faturamento conforme fechamento de OS: ");
			htmlFim.append("\n 		<br> ");
			htmlFim.append("\n 		<br> ");
			
			htmlPos.append("\n 			<table width=\"100%\"cellspacing=\"0\" cellpadding=\"2\" border=\"1\"> ");
			htmlPos.append("\n 					<tr> ");
			htmlPos.append("\n 						<td colspan=\"10\"align=\"center\">");
			htmlPos.append("\n 							<strong>Lista de Postos</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 					</tr> ");
			htmlPos.append("\n 					<tr> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>OS</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Conta Sigma</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Empresa</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Filial</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Contrato</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Data inicio Contrato</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Posto</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Data inicio Posto</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Data inicio ajustada de ambos</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 					</tr>");
			
			
			StringBuilder sqlSap = new StringBuilder();
			
			LinkedList<ApontamentoVO> apontamentosLista = new LinkedList<ApontamentoVO>();
			
			while (rs.next())
			{
				
				Integer idOS = rs.getInt("ID_ORDEM");
				String idCentral = rs.getString("ID_CENTRAL");
				String particao = rs.getString("PARTICAO");
				Integer codEmp = rs.getInt("usu_codemp");
				Integer codFil = rs.getInt("usu_codfil");
				Integer numCtr = rs.getInt("usu_numctr");
				Integer numPos = rs.getInt("usu_numpos");
				Date datIniCtr = rs.getDate("datIniCtr");
				Date datIniPos = rs.getDate("datIniPos");
				Date datFimOs = rs.getDate("FECHAMENTO");
				
				String codSer = rs.getString("usu_codser");
				
				apontamentosLista = verificaApontamentosPosto(codEmp, codFil, numCtr, numPos, datFimOs);
				
				if(NeoUtils.safeIsNotNull(apontamentosLista) && !apontamentosLista.isEmpty()) {
					atualizaCompetenciaApontamentos(apontamentosLista);
				}
				
				
				if (codSer != null && codSer.equals("9002010")){
				    sqlSap.append(" Update USU_T160CVS Set usu_susfat = 'N' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_numpos = " + numPos + "; ");
				}else{
				    sqlSap.append(" Update USU_T160CVS Set usu_datini = '" + NeoDateUtils.safeDateFormat(datFimOs, "yyyy-MM-dd") + "', usu_susfat = 'N' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_numpos = " + numPos + "; ");
				}
				
				
				htmlPos.append("\n 					<tr> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+idOS);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+idCentral+"["+particao+"]");
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+codEmp);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+codFil);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+numCtr);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datIniCtr, "dd/MM/yyyy"));
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+numPos);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datIniPos, "dd/MM/yyyy"));
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datFimOs, "dd/MM/yyyy"));
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 					</tr>");
				
				if (numCtrAnt == 0 || !numCtrAnt.toString().equals(numCtr.toString()))
				{
					sqlSap.append(" Update USU_T160CTR Set usu_datini = '" + NeoDateUtils.safeDateFormat(datFimOs, "yyyy-MM-dd") + "', usu_susfat = 'N' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_susfat = 'S'; ");
				}
				numCtrAnt = numCtr;
			}
			if (!sqlSap.toString().isEmpty()){
			    connSap.prepareStatement(sqlSap.toString());
			    psSap = connSap.prepareStatement(sqlSap.toString());
			    psSap.executeUpdate();
			}
			htmlFim.append(htmlPos);
			
			htmlFim.append("\n 			</table> ");
			if(NeoUtils.safeIsNotNull(apontamentosLista) && !apontamentosLista.isEmpty()) {
				htmlFim.append(montaDescricaoApontamentos(apontamentosLista));
			}
			htmlFim.append("\n 		</body> ");
			htmlFim.append("\n </html> ");
			
			System.out.println(htmlFim.toString());
			
			ArrayList<String> listaEmails = new ArrayList<String>();
			listaEmails.add("liberacaofaturamentoprivado@orsegups.com.br");
			listaEmails.add("desenvolvimento@orsegups.com.br");
			listaEmails.add("herisson.ferreira@orsegups.com.br");
			
			for (String emailDst : listaEmails)
			{
				OrsegupsUtils.sendEmail2Orsegups(emailDst, 
						"fusion@orsegups.com.br", "[SAPIENS] Alteração da data inicio do contrato e posto ", "", htmlFim.toString());
			}
			
		}
		catch (Exception e)
		{
			log.error("##### AGENDADOR DE TAREFA: Rotina Faturamento On Demand");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				if (psSap != null){
				    psSap.close();
				}
				psSig.close();
				connSap.close();
				connSig.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	
public LinkedList<ApontamentoVO> verificaApontamentosPosto(Integer empresa, Integer filial, Integer contrato, Integer posto, Date dataFinalOs) {
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Connection conn = PersistEngine.getConnection("SAPIENS");
		
		LinkedList<ApontamentoVO> listaApontamentos = new LinkedList<>();
		
		try {
			
			sql.append(" SELECT CMS.USU_DATCPT AS 'COMPETÊNCIA', CMS.USU_CODFIL AS 'FILIAL', CMS.USU_CODEMP AS 'EMPRESA', CMS.USU_NUMCTR AS 'CONTRATO', "
						+ "CMS.USU_NUMPOS AS 'POSTO', CMS.USU_SEQMOV AS 'SEQUÊNCIA', CONVERT(DECIMAL(18,2),CMS.USU_PREUNI) AS 'VALOR UNITÁRIO', CMS.USU_ADCSUB AS 'ADICIONA/SUBTRAI' ");
			sql.append(" FROM USU_T160CVS CVS ");
			sql.append(" INNER JOIN USU_T160CMS CMS ");
			sql.append(" ON CVS.USU_CODEMP = CMS.USU_CODEMP ");
			sql.append(" AND CVS.USU_CODFIL = CMS.USU_CODFIL ");
			sql.append(" AND CVS.USU_NUMCTR = CMS.USU_NUMCTR ");
			sql.append(" AND CVS.USU_NUMPOS = CMS.USU_NUMPOS ");
			sql.append(" WHERE CMS.USU_CODEMP = ? ");
			sql.append(" AND CMS.USU_CODFIL = ? ");
			sql.append(" AND CMS.USU_NUMCTR = ? ");
			sql.append(" AND CMS.USU_NUMPOS = ? ");
			sql.append(" AND ((CMS.USU_CODSER = '9002012') ");
			sql.append(" AND (CMS.USU_ADCSUB = '+') ");
			sql.append(" OR (CMS.USU_CODLAN != 51)) ");
			sql.append(" AND (MONTH(?) < MONTH(GETDATE())) ");
			sql.append(" AND ((YEAR(USU_DATCPT) = YEAR(GETDATE())) OR ((YEAR(USU_DATCPT) < YEAR(GETDATE())))) ");
			sql.append(" ORDER BY CMS.USU_DATCPT ASC ");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setInt(1, empresa);
			pstm.setInt(2, filial);
			pstm.setInt(3, contrato);
			pstm.setInt(4, posto);
			pstm.setDate(5, dataFinalOs);
			
			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				ApontamentoVO apontamento = new ApontamentoVO();
				
				apontamento.setCompetencia(rs.getTimestamp("COMPETÊNCIA"));
				apontamento.setEmpresa(rs.getLong("EMPRESA"));
				apontamento.setFilial(rs.getLong("FILIAL"));
				apontamento.setContrato(rs.getLong("CONTRATO"));
				apontamento.setPosto(rs.getLong("POSTO"));
				apontamento.setSequenciaApontamento(rs.getLong("SEQUÊNCIA"));
				apontamento.setValorUnitario(rs.getString("VALOR UNITÁRIO"));
				apontamento.setAdicionaOuSubtrai(rs.getString("ADICIONA/SUBTRAI"));
				
				listaApontamentos.add(apontamento);
			}
		   
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("");
		}
		
		
		return listaApontamentos;
	}
	
	public void atualizaCompetenciaApontamentos(LinkedList<ApontamentoVO> listaApontamentos) {
		
		Connection conn = PersistEngine.getConnection("SAPIENS");
		PreparedStatement pstm = null;
		
		GregorianCalendar dataBase = new GregorianCalendar();
		dataBase.set(GregorianCalendar.DAY_OF_MONTH, 1);
		dataBase.set(GregorianCalendar.HOUR_OF_DAY, 0);
		dataBase.set(GregorianCalendar.MINUTE, 0);
		dataBase.set(GregorianCalendar.SECOND, 0);
		dataBase.set(GregorianCalendar.MILLISECOND, 0);
		
		int linhasAfetadas = 0;
		
		for(ApontamentoVO apontamento : listaApontamentos) {

			dataBase.add(GregorianCalendar.MONTH, 1);
			Timestamp tsDataBase = new Timestamp(dataBase.getTimeInMillis());

			StringBuilder sql = new StringBuilder();
			
			try {
				
				sql.append(" UPDATE USU_T160CMS ");
				sql.append(" SET USU_DATCPT = ? ");
				sql.append(" WHERE USU_CODEMP = ? ");
				sql.append(" AND USU_CODFIL = ? ");
				sql.append(" AND USU_NUMCTR = ? ");
				sql.append(" AND USU_NUMPOS = ? ");
				sql.append(" AND USU_SEQMOV = ? ");
				
				pstm = conn.prepareStatement(sql.toString());
				
				pstm.setTimestamp(1, tsDataBase); 
				pstm.setLong(2, apontamento.getEmpresa());
				pstm.setLong(3, apontamento.getFilial());
				pstm.setLong(4, apontamento.getContrato());
				pstm.setLong(5, apontamento.getPosto());
				pstm.setLong(6, apontamento.getSequenciaApontamento());
				
				linhasAfetadas = pstm.executeUpdate();
				
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
	}
	
	public Timestamp retornaCompetenciaAtual(ApontamentoVO apontamento) {
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Connection conn = PersistEngine.getConnection("SAPIENS");
		Timestamp novaCompetencia = null;
		
		try {
			
			sql.append(" SELECT CMS.USU_DATCPT AS 'COMPETÊNCIA' FROM USU_T160CMS CMS  ");
			sql.append(" WHERE CMS.USU_CODEMP = ? AND CMS.USU_CODFIL = ?  ");
			sql.append(" AND CMS.USU_NUMCTR = ? AND CMS.USU_NUMPOS = ?  ");
			sql.append(" AND CMS.USU_SEQMOV = ?  ");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setLong(1, apontamento.getEmpresa());
			pstm.setLong(2, apontamento.getFilial());
			pstm.setLong(3, apontamento.getContrato());
			pstm.setLong(4, apontamento.getPosto());
			pstm.setLong(5, apontamento.getSequenciaApontamento());
			
			rs = pstm.executeQuery();
			
			if(rs.next()) {
				novaCompetencia = rs.getTimestamp("COMPETÊNCIA");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return novaCompetencia;
	}
	
	public String montaDescricaoApontamentos(LinkedList<ApontamentoVO> listaApontamentos) {
		
		StringBuilder apontamentos = new StringBuilder();
		
		apontamentos.append("<table width=\"100%\"cellspacing=\"0\" cellpadding=\"2\" border=\"1\"> ");
		apontamentos.append("\n  <tr> ");
		apontamentos.append("\n   <td> Empresa </td> ");
		apontamentos.append("\n   <td> Filial </td> ");
		apontamentos.append("\n   <td> Contrato </td> ");
		apontamentos.append("\n   <td> Posto </td> ");
		apontamentos.append("\n   <td> Adiciona/Subtrai </td> ");
		apontamentos.append("\n   <td> Valor Unitário </td> ");
		apontamentos.append("\n   <td> Competência Antiga </td> ");
		apontamentos.append("\n   <td> Competência Atual </td> ");
		apontamentos.append("\n  </tr> ");
		
		
		for(ApontamentoVO apontamento : listaApontamentos) {
			
			GregorianCalendar gcCompetenciaAntiga = new GregorianCalendar();
			GregorianCalendar gcCompetenciaNova = new GregorianCalendar();
			gcCompetenciaAntiga.setTimeInMillis(apontamento.getCompetencia().getTime());
			gcCompetenciaNova.setTimeInMillis(retornaCompetenciaAtual(apontamento).getTime());
			
			String dfCompetenciaAntiga = NeoDateUtils.safeDateFormat(gcCompetenciaAntiga, "dd/MM/yyyy");
			String dfCompetenciaNova = NeoDateUtils.safeDateFormat(gcCompetenciaNova, "dd/MM/yyyy");
			
			apontamentos.append("\n <tr> ");
			apontamentos.append("\n  <td> "+apontamento.getEmpresa()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getFilial()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getContrato()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getPosto()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getAdicionaOuSubtrai()+" </td> ");
			apontamentos.append("\n  <td> "+apontamento.getValorUnitario()+" </td> ");
			apontamentos.append("\n  <td> "+dfCompetenciaAntiga+" </td> ");
			apontamentos.append("\n  <td> "+dfCompetenciaNova+" </td> ");
			apontamentos.append("\n </tr> ");
			
		}
		
		apontamentos.append("\n </table> ");
		
		return apontamentos.toString();
	}
}