package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.mobile.engine.SaveInspection;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;

//com.neomind.fusion.custom.orsegups.scheduler.RotinaAvancaInspecao
public class RotinaAvancaInspecao implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAvancaInspecao.class);
	
	private static final String MODELO_PROCESSO = "Q003 - Inspetoria Mobile";
	private static final String ATIVIDADE_CHECK_POINT_REINSPECAO= "AguardarEnvioReinspecao";
	private static final String ATIVIDADE_CHECK_POINT_EFICACIA= "AguardarEnvioEficacia";

	@Override
	public void execute(CustomJobContext ctx)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try{
			
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("model.name", MODELO_PROCESSO));
			gp.addFilter(new QLFilterIsNull("finishDate"));
			gp.addFilter(new QLEqualsFilter("processState", ProcessState.running.ordinal()));
			gp.addFilter(new QLEqualsFilter("saved", Boolean.TRUE));
			//gp.addFilter(new QLEqualsFilter("code", "012827"));
			
			/*model.name = ATIVIDADE_CHECK_POINT_EFICACIA;
			startdate != null;
			finishdate == null;
			process.model.name = MODELO_PROCESSO; */
			
			
			List<WFProcess> processos = PersistEngine.getObjects(WFProcess.class, gp, "code DESC");
			for(WFProcess processo : processos)
			{
				EntityWrapper wProcesso = new EntityWrapper(processo.getEntity());
				
				Collection<Activity> atividadesAbertas = processo.getOpenActivities();
				
				if (atividadesAbertas != null && atividadesAbertas.size() > 0){
					for(Activity act : atividadesAbertas){
						if(act.getActivityName().equals(ATIVIDADE_CHECK_POINT_REINSPECAO))
						{
							System.out.println("["+key+"] Processo: "+processo.getCode()+", Atividade:"+act.getActivityName());
							
							EntityWrapper w = new EntityWrapper(act.getProcess().getEntity());
							Task task = null;
							
							if (w.findValue("dataEnvioReinspecao") != null){ // se a data de envio da reinspeção for != null, avança a atividade
								//task = ((UserActivity) act).getTaskList().get(0);
								
								final long actId = act.getNeoId();
								final NeoRunnable work = new NeoRunnable(){
								       public void run() throws Exception{
								    	   
								    	   Activity actNovo = (Activity) PersistEngine.getObject(Activity.class,actId);
								    	   
								    	   OrsegupsWorkflowHelper.finishTaskByActivity(actNovo);    	   
								       }
								};
								try{
									PersistEngine.managedRun(work);
								}
								catch (final Exception e){
									log.error("!@# Erro ao avançar");
									log.info("!@# Erro ao avançar");
									e.printStackTrace();
								}
								
								System.out.println("["+key+"] Processo: "+processo.getCode()+", ReInspeção avançada com sucesso");
								
							}
						}else if(act.getActivityName().equals(ATIVIDADE_CHECK_POINT_EFICACIA))
						{
							System.out.println("["+key+"] Processo: "+processo.getCode()+", Atividade:"+act.getActivityName());
							
							EntityWrapper w = new EntityWrapper(act.getProcess().getEntity());
							Task task = null;
							
							if (w.findValue("dataEnvioEficacia") != null){ // se a data de envio da reinspeção for != null, avança a atividade
								
								final long actId = act.getNeoId();
								final NeoRunnable work = new NeoRunnable(){
								       public void run() throws Exception{
								    	   
								    	   Activity actNovo = (Activity) PersistEngine.getObject(Activity.class,actId);
								    	   
								    	   OrsegupsWorkflowHelper.finishTaskByActivity(actNovo);    	   
								       }
								};
								try{
									PersistEngine.managedRun(work);
								}
								catch (final Exception e){
									log.error("!@# Erro ao avançar");
									log.info("!@# Erro ao avançar");
									e.printStackTrace();
								}
								
								System.out.println("["+key+"] Processo: "+processo.getCode()+", Verificação de Eficácia avançada com sucesso");
								
							}
						}
					}
				}
			}
				
			
		}catch(Exception e){
			System.err.println("["+key+"] - Erro ao avançar atividade de inspetoria mobile");
			e.printStackTrace();
			throw new JobException("Erro ao avançar atividade de inspetoria mobile. Procure no log por ["+key+"]");
		}
		
	}

}
