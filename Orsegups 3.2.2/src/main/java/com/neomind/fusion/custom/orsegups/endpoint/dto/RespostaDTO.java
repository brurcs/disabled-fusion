package com.neomind.fusion.custom.orsegups.endpoint.dto;

public class RespostaDTO {

    private String mensagem;
    private String tarefa;
    
    public String getTarefa() {
        return tarefa;
    }

    public void setTarefa(String tarefa) {
        this.tarefa = tarefa;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    
    
}

