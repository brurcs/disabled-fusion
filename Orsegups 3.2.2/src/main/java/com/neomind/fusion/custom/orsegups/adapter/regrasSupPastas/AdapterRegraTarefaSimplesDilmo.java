package com.neomind.fusion.custom.orsegups.adapter.regrasSupPastas;

import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskSummary;
import com.neomind.fusion.workflow.task.rule.TaskRuleAdapter;
import com.neomind.fusion.workflow.task.rule.TaskRuleContext;

public class AdapterRegraTarefaSimplesDilmo implements TaskRuleAdapter{

	@Override
	public boolean isTriggered(TaskRuleContext context){
	    	
	    boolean retorno = false;
	    
	    Task t = null;
	    TaskSummary	ts = null;
	    try{
		
		if(context.getTask() instanceof  Task){
		    t = (Task) context.getTask();
		    if (t.getActivity().getModel().getName().equals("Realizar tarefa - Escalada - Último Nível")){
			retorno = true;
		    }
		}else{
		    ts = (TaskSummary) context.getTask();
		    if (ts.getTaskName().equals("Realizar tarefa - Escalada - Último Nível")){
			retorno = true;
		    }
		}
	    }catch(Exception e){
		System.out.println("Erro ao direcionar tarefa simples para subpastas. Mensagem: "+e.getMessage());
		e.printStackTrace();
	    }

	    return retorno;

	}
}

