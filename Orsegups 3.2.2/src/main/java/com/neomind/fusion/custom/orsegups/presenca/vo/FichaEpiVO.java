package com.neomind.fusion.custom.orsegups.presenca.vo;

public class FichaEpiVO {
	
	private String neoId; // id do processo
	private String code;
	private String startDate;
	private String derivacao; //tamanho;
	
	private String codigoProduto;
	private String descricao;
	
	private String dataEntrega;
	private String quantidadeEntrega;
	
	private String dataDevolucao;
	private String quantidadeDevolucao;
	
	public String getNeoId() {
		return neoId;
	}
	public void setNeoId(String neoId) {
		this.neoId = neoId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getDerivacao() {
		return derivacao;
	}
	public void setDerivacao(String derivacao) {
		this.derivacao = derivacao;
	}
	public String getCodigoProduto() {
		return codigoProduto;
	}
	public void setCodigoProduto(String codigoProduto) {
		this.codigoProduto = codigoProduto;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDataEntrega() {
		return dataEntrega;
	}
	public void setDataEntrega(String dataEntrega) {
		this.dataEntrega = dataEntrega;
	}
	public String getQuantidadeEntrega() {
		return quantidadeEntrega;
	}
	public void setQuantidadeEntrega(String quantidadeEntrega) {
		this.quantidadeEntrega = quantidadeEntrega;
	}
	public String getDataDevolucao() {
		return dataDevolucao;
	}
	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}
	public String getQuantidadeDevolucao() {
		return quantidadeDevolucao;
	}
	public void setQuantidadeDevolucao(String quantidadeDevolucao) {
		this.quantidadeDevolucao = quantidadeDevolucao;
	}
	@Override
	public String toString() {
		return "FichaEpiVO [neoId=" + neoId + ", code=" + code + ", startDate="
				+ startDate + ", derivacao=" + derivacao + ", codigoProduto="
				+ codigoProduto + ", descricao=" + descricao + ", dataEntrega="
				+ dataEntrega + ", quantidadeEntrega=" + quantidadeEntrega
				+ ", dataDevolucao=" + dataDevolucao + ", quantidadeDevolucao="
				+ quantidadeDevolucao + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoProduto == null) ? 0 : codigoProduto.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FichaEpiVO other = (FichaEpiVO) obj;
		if (codigoProduto == null) {
			if (other.codigoProduto != null)
				return false;
		} else if (!codigoProduto.equals(other.codigoProduto))
			return false;
		return true;
	}
	
	
	
	
	
	
	

}
