package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class AbreTarefaSimplesAfaAcidtrabalhoEAuxDoenca
{
	public static String abreTarefaAfaAcidenteDeTrabaloEAuxDoenca(Long numEmp, Long numCad, Long tipCol, String datAfa, String desSit)
	{
		Connection connVetorh = null;
		ResultSet rsAfastamento = null;
		PreparedStatement stAfastamento = null;
		String solicitante = "dilmoberger";
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		String executor = "cida";
		String tarefa = "";

		try
		{
			connVetorh = PersistEngine.getConnection("VETORH");

			//Montando Query principal do job.
			StringBuffer queryafastamento = new StringBuffer();
			queryafastamento.append(" SELECT FUN.NUMEMP,FUN.NUMCAD,FUN.NOMFUN,CAR.TITRED,ORN.NOMLOC FROM R034FUN FUN");
			queryafastamento.append(" INNER JOIN R038HCA HCA ON HCA.NUMCAD = FUN.NUMCAD AND HCA.NUMEMP = FUN.NUMEMP AND HCA.TIPCOL = FUN.TIPCOL");
			queryafastamento.append(" AND HCA.DATALT = (SELECT MAX(DATALT) FROM R038HCA HCA2 WHERE HCA.NUMCAD = HCA2.NUMCAD AND HCA.NUMEMP = HCA2.NUMEMP AND HCA.TIPCOL = HCA2.TIPCOL)");
			queryafastamento.append(" INNER JOIN R024CAR CAR ON CAR.CODCAR = HCA.CODCAR AND CAR.ESTCAR = 1");
			queryafastamento.append(" INNER JOIN R038HLO HLO ON HLO.NUMCAD = FUN.NUMCAD AND HLO.NUMEMP = FUN.NUMEMP AND HLO.TIPCOL = FUN.TIPCOL");
			queryafastamento.append(" AND HLO.DATALT = (SELECT MAX(DATALT) FROM R038HLO HLO2 WHERE HLO.NUMCAD = HLO2.NUMCAD AND HLO.NUMEMP = HLO2.NUMEMP AND HLO.TIPCOL = HLO2.TIPCOL)");
			queryafastamento.append(" INNER JOIN R016ORN ORN ON ORN.NUMLOC = HLO.NUMLOC AND ORN.TABORG = HLO.TABORG ");
			queryafastamento.append(" WHERE  FUN.NUMCAD = ? AND FUN.NUMEMP = ? AND FUN.TIPCOL = ?");
			queryafastamento.append(" ORDER BY FUN.NUMCAD");
			
			//Conexão com o banco do Vetorh.
			stAfastamento = connVetorh.prepareStatement(queryafastamento.toString());
			stAfastamento.setLong(1, numCad);
			stAfastamento.setLong(2, numEmp);
			stAfastamento.setLong(3, tipCol);
			//Executando a Query.
			rsAfastamento = stAfastamento.executeQuery();

			//Verificando se a consulta possui resultado.
			while (rsAfastamento.next())
			{
				//Montando dados da tarefa simples.
				String titulo = desSit + " - " + numEmp + " / " + numCad + " - " + rsAfastamento.getString("NOMFUN");
				String descricao = "";
				descricao += "Coletar dados e evidenciar o afastamento abaixo. <br><br>";
				descricao += " <strong>	Colaborador: </strong>" + numEmp + "/" + numCad + " - " + rsAfastamento.getString("NOMFUN") + " <br>";
				descricao += " <strong> Cargo: </strong>" + rsAfastamento.getString("TITRED") + "<br>";
				descricao += " <strong> Posto: </strong>" + rsAfastamento.getString("NOMLOC") + "<br>";
				descricao += " <strong> Afastamento: </strong>" + desSit + "<br>";
				descricao += " <strong> Data do afastamento: </strong>" + datAfa + "<br>";

				//Iniciando a tarefa simples avançando a primeira atividade.
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
				System.out.println("Tarefa aberta: " + tarefa);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			tarefa = "ERRO AO ABRIR A TAREFA!";
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stAfastamento, rsAfastamento);
		}
		return tarefa;
	}
}
