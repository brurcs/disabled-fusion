package com.neomind.fusion.custom.orsegups.maps.call.vo;

import java.util.Comparator;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;

public class CallAlertAitVO implements Comparator<CallAlertAitVO>
{
	private ViaturaVO callViaturaVO;

	private EventoVO callEventoVO;

	private GregorianCalendar lastCallCalendar;

	private final String type = "CallAlertAitVO";
	
	private String buttonDiv;
	
	private GregorianCalendar listCallCalendar;
	
	private String discCallCalendar;

	public ViaturaVO getCallViaturaVO()
	{
		return callViaturaVO;
	}

	public void setCallViaturaVO(ViaturaVO callViaturaVO)
	{
		this.callViaturaVO = callViaturaVO;
	}

	public EventoVO getCallEventoVO()
	{
		return callEventoVO;
	}

	public void setCallEventoVO(EventoVO callEventoVO)
	{
		this.callEventoVO = callEventoVO;
	}

	public GregorianCalendar getLastCallCalendar()
	{
		return lastCallCalendar;
	}

	public void setLastCallCalendar(GregorianCalendar lastCallCalendar)
	{
		this.lastCallCalendar = lastCallCalendar;
	}

	public String getType()
	{
		return type;
	}

	public String getButtonDiv()
	{
		return buttonDiv;
	}

	public void setButtonDiv(String buttonDiv)
	{
		this.buttonDiv = buttonDiv;
	}

	public GregorianCalendar getListCallCalendar()
	{
		return listCallCalendar;
	}

	public void setListCallCalendar(GregorianCalendar listCallCalendar)
	{
		this.listCallCalendar = listCallCalendar;
	}

	public String getDiscCallCalendar()
	{
		return discCallCalendar;
	}

	public void setDiscCallCalendar(String discCallCalendar)
	{
		this.discCallCalendar = discCallCalendar;
	}

	@Override
	public int compare(CallAlertAitVO o1, CallAlertAitVO o2)
	{
	
		return o1.getListCallCalendar().compareTo(o2.getListCallCalendar());
	}




}
