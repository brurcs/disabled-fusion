package com.neomind.fusion.custom.orsegups.fulltrack.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackCliente;
import com.neomind.fusion.custom.orsegups.fulltrack.messages.ResponseCliente;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class ClientImport {
    
    public static void run() {

   	StringBuilder retorno = new StringBuilder();

   	try {

   	    URL url = new URL("https://ws.fulltrack2.com/clients/all");
   	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
   	    conn.setRequestMethod("GET");
   	    conn.setRequestProperty("apiKey", "682ee2548d4a73d8fadf72f32c52df790eb72aff");
   	    conn.setRequestProperty("secretKey", "4c02a36eebf652e2b753cb355cb763132259c65e");
   	    conn.setRequestProperty("Content-Type", "application/json");
   	    conn.setRequestProperty("Accept", "application/json");

   	    if (conn.getResponseCode() != 200) {
   		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
   	    }

   	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

   	    String output;

   	    while ((output = br.readLine()) != null) {
   		retorno.append(output + "\n");
   	    }

   	    conn.disconnect();

   	} catch (MalformedURLException e) {

   	    e.printStackTrace();

   	} catch (IOException e) {

   	    e.printStackTrace();

   	}

   	System.out.println(retorno.toString());

   	Gson gson = new Gson();
   	ResponseCliente result = gson.fromJson(retorno.toString(), ResponseCliente.class);

   	addToDB(result);

       }

       private static void addToDB(ResponseCliente r) {

   	Connection conn = null;
   	PreparedStatement pstm = null;
   	ResultSet rs = null;

   	StringBuilder sql = new StringBuilder();
   	
   	sql.append(" IF NOT EXISTS (SELECT 1 FROM FULLTRACK_CLIENTE V2 WHERE V2.ID=?) ");
   	sql.append(" INSERT INTO FULLTRACK_CLIENTE VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

   	try {
   	    conn = PersistEngine.getConnection("IMPORTACOES");
   	    pstm = conn.prepareStatement(sql.toString());
   	    
   	    for (FulltrackCliente v : r.getData() ){
   		
   		pstm.setInt(1, v.getRas_cli_id());
   		
   		pstm.setInt(2, v.getRas_cli_id());
   		
   		pstm.setString(3, v.getRas_cli_desc());
   		
   		pstm.setString(4, v.getRas_cli_razao());
   		
   		pstm.setString(5, v.getRas_cli_endereco());
   		
   		pstm.setString(6, v.getRas_cli_bairro());
   		
   		pstm.setString(7, v.getRas_cli_cep());
   		
   		pstm.setString(8, v.getRas_cli_uf());
   		
   		pstm.setString(9, v.getRas_cli_cidade());
   		
   		pstm.setString(10, v.getRas_cli_cnpj());
   		
   		pstm.setString(11, String.valueOf(v.getRas_cli_tipo()));
   		
   		pstm.setString(12, String.valueOf(v.getRas_cli_liberado()));
   		
   		pstm.setNull(13, Types.NULL);
   		
   								
   		pstm.addBatch();
   		pstm.clearParameters();
   	    }
   	       	    
   	    int[] results = pstm.executeBatch();
   	    
   	    for (int i=0; i< results.length; i++){
   		int value = results[i];
   		
   		if (value < 0) {
   		    System.out.println("### Falha ao inserir o veiculo de ID: "+r.getData().get(i).getRas_cli_id());
   		}
   		
   	    }

   	} catch (SQLException e) {
   	    e.printStackTrace();
   	} finally {
   	    OrsegupsUtils.closeConnection(conn, pstm, rs);
   	}

       }


}
