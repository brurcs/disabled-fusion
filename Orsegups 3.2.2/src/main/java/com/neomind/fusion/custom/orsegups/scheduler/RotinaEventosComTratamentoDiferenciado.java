package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaEventosComTratamentoDiferenciado implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaEventosComTratamentoDiferenciado.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaEventosComTratamentoDiferenciado");
		log.warn("##### INICIAR ROTINA DE VERIFICAÇÃO DE EVENTOS COM TRATAMENTO DIFERENCIADO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		//LISTA COMANDOS SQL
		List<String> listaCmd = null;

		try
		{	
			/**
			 * Eventos fechados em virtude de zonas anuladas nas últimas 4 horas 
			 */
			conn = PersistEngine.getConnection("SIGMA90");
			
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT h.CD_HISTORICO_ALARME, h.NU_AUXILIAR, c.ID_CENTRAL, c.ID_EMPRESA, c.PARTICAO ");
			sql.append(" FROM HISTORICO_ALARME h ");
			sql.append(" INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = h.CD_CLIENTE  ");
			sql.append(" WHERE DT_VIATURA_NO_LOCAL IS NULL ");
			sql.append(" AND CD_USUARIO_FECHAMENTO = 9999 ");
			sql.append(" AND DT_FECHAMENTO >= DATEADD(HOUR, -4, GETDATE()) ");
			sql.append(" AND TX_OBSERVACAO_FECHAMENTO LIKE 'Evento fechado automaticamente devido à zona estar anulada%' ");
			sql.append(" AND TX_OBSERVACAO_FECHAMENTO NOT LIKE 'X130' ");
			sql.append(" ORDER BY h.CD_HISTORICO_ALARME ");

			pstm = conn.prepareStatement(sql.toString());
		    rs = pstm.executeQuery();
		    conn.setAutoCommit(false);
		    
		    listaCmd = new ArrayList<String>();
		    
			while (rs.next())
			{
				//Cria novo evento (X130)
				listaCmd.add(" INSERT INTO RECEPCAO (CD_CENTRAL, NM_EVENTO, NU_AUXILIAR, NU_PARTICAO, NU_PORTA, " +
						     " NU_RECEPTORA, NU_LINHA, NU_PROTOCOLO, NU_GRUPO_RECEPTORA, DT_RECEPCAO, TP_RECEPTORA, " +
						     " CD_EMPRESA, NM_RECEPTORA, TP_RECEPCAO, CD_VIATURA_EVENTO) VALUES ("+rs.getString("ID_CENTRAL")+ " , 'X130'," +
						     " "+rs.getString("NU_AUXILIAR")+", '"+rs.getString("PARTICAO")+"', '9', NULL, NULL, '2', NULL, GETDATE(), '1', "+rs.getString("ID_EMPRESA")+" , " +
						     " 'Geração Fusion', NULL, NULL )");
				
				//Marca evento como atendido
				listaCmd.add(" UPDATE HISTORICO_ALARME SET TX_OBSERVACAO_GERENTE = 'X130' WHERE CD_HISTORICO_ALARME = "+rs.getString("CD_HISTORICO_ALARME")+" ");
				
			}
			
			for (String cmd : listaCmd)
			{
				pstm = conn.prepareStatement(cmd.toString());
				pstm.executeUpdate();

			}
			conn.commit();
			conn.setAutoCommit(true);
			log.warn("##### EXECUTAR ROTINA DE VERIFICAÇÃO DE EVENTOS COM TRATAMENTO DIFERENCIADO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO ROTINA DE VERIFICAÇÃO DE EVENTOS COM TRATAMENTO DIFERENCIADO: " +e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				
				e1.printStackTrace();
			}
		}
		finally
		{ 
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA DE VERIFICAÇÃO DE EVENTOS COM TRATAMENTO DIFERENCIADO: " +e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));

			}
				log.warn("##### FINALIZAR ROTINA DE VERIFICAÇÃO DE EVENTOS COM TRATAMENTO DIFERENCIADO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));

		}
	}

	
}

