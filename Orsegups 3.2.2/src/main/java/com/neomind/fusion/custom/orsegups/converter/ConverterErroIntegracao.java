package com.neomind.fusion.custom.orsegups.converter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterListaAnexo
public class ConverterErroIntegracao extends StringConverter
{
    @Override
    public String getHTMLInput(EFormField field, OriginEnum origin){
	NeoObject objPrincipal = field.getForm().getObject();
	EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);
	String erro = wPrincipal.findGenericValue("errosIntegracao");
	@SuppressWarnings("unchecked")
	StringBuilder retorno = new StringBuilder();

	retorno.append("		<table  style=\"border: 1px solid red\" class=\"gridbox\" cellpadding=\"1000\" cellspacing=\"1000\" width=\"80%\">");
	retorno.append("			<tr style=\"cursor: auto; background-color: #F89393\">");
	retorno.append("				<td style=\"cursor: auto; white-space: normal; text-align: left; font-weight: bold; font-size: 15px; border: 5px solid transparent; background-clip: padding-box\"><font color=\"#570101\">" + erro + "</font></th>");
	retorno.append("			</tr>");
	retorno.append("		</table>");

	return retorno.toString();
    }

    @Override
    protected String getHTMLView(EFormField field, OriginEnum origin)
    {
	return getHTMLInput(field, origin);
    }
}
