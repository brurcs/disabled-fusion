package com.neomind.fusion.custom.orsegups.fap.slip.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.job.FAPSlipOrdemCompraJob;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

@Path("/FAPOrdemCompra")
public class FAPSlipOrdemCompraService
{
	@GET
	@Path("/atualizar/{formNeoId}")
	@Produces("application/json")
	public Response atualizarOrdemCompra(@PathParam("formNeoId") Long formNeoId) throws JSONException
	{
		NeoObject noOrdemCompra = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipOrdemCompra"), formNeoId);
		EntityWrapper wOrdemCompra = new EntityWrapper(noOrdemCompra);
		
		Long codOrdemCompra = wOrdemCompra.findGenericValue("ordemCompra");
		Long codEmpresa = wOrdemCompra.findGenericValue("empresa.codemp");
		
		JSONObject jsonObj = new JSONObject();
		
		FAPSlipOrdemCompraJob job = new FAPSlipOrdemCompraJob();
		
		try
		{
			NeoObject noOrdemCompraAtualizada = job.preencherOrdemCompra(codOrdemCompra, noOrdemCompra, codEmpresa, true);
			if (noOrdemCompraAtualizada == null)
			{
				jsonObj.put("error", "Esta ordem de compra foi removida do SAPIENS. Este processo será cancelado em breve.");
				return Response.ok(jsonObj.toString()).build();
			}
			else
			{
				NeoObject noFAP = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlip"), new QLEqualsFilter("ordemCompra", noOrdemCompraAtualizada));
				if (noFAP != null)
				{
					WFProcess wfProcess = new EntityWrapper(noFAP).findGenericValue("wfprocess");
					if (wfProcess != null)
					{
						EntityWrapper wFAP = new EntityWrapper(noFAP);
						
						NeoObject noEmpresa = wFAP.findGenericValue("empresa");						
						String fornecedor = wFAP.findGenericValue("fornecedor.apefor");
						BigDecimal valorOC = wFAP.findGenericValue("valorPagamento");
						
						String tituloTarefa = "Cód: " + codOrdemCompra;
						tituloTarefa += " - " + FAPSlipUtils.parseMonetario(valorOC) + " - Empresa: " + new EntityWrapper(noEmpresa).findGenericValue("codemp") + " - " + fornecedor;
						
						wFAP.setValue("tituloTarefa", tituloTarefa);
						
						QLGroupFilter qlAndFilter = new QLGroupFilter("AND");
						qlAndFilter.addFilter(new QLEqualsFilter("code", wfProcess.getCode()));
						qlAndFilter.addFilter(new QLEqualsFilter("processName", "F002 - FAP Slip"));
						
						List<TaskInstance> listInstances = PersistEngine.getObjects(TaskInstance.class, qlAndFilter);
						if (listInstances != null && !listInstances.isEmpty())
						{
							for (TaskInstance instance : listInstances)
							{
								instance.setTitle(tituloTarefa);
							}
							
						}
					}
				}				
			}
			
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return Response.serverError().build();
		}
		
		return Response.ok().build();
	}
}
