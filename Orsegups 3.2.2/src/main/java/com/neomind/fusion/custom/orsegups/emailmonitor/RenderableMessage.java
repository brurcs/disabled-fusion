/*
 * RenderableMessage.java
 *
 * Created on 09 November 2005, 10:36
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.neomind.fusion.custom.orsegups.emailmonitor;

import java.io.*;
import java.util.ArrayList;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeUtility;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author Daniel Coppi Rauh
 * Abertura de chamados via portal de clientes.
 * 
 */

public class RenderableMessage implements Renderable
{	
	private static final Log log = LogFactory.getLog(RenderableMessage.class);
	private String subject;
	private String bodytext;
	ArrayList<Attachment> attachments;

	/** Creates a new instance of RenderableMessage */
	public RenderableMessage(Message m) throws MessagingException, IOException
	{
		subject = m.getSubject();
		attachments = new ArrayList<Attachment>();
		extractPart(m);
	}

	private void extractPart(final Part part) throws MessagingException, IOException
	{
		if (part.getContent() instanceof Multipart)
		{
			Multipart mp = (Multipart) part.getContent();
			for (int i = 0; i < mp.getCount(); i++)
			{
				extractPart(mp.getBodyPart(i));
			}
			// return;
		}

		final String disposition = part.getDisposition();

		//le os anexos do email e anexa ao chamado
		if (disposition != null && disposition.toLowerCase().equals("attachment"))
		{
			try {
				Attachment attachment = new Attachment();
				attachment.setContenttype(part.getContentType());
				attachment.setFilename(MimeUtility.decodeText(part.getFileName()));
	
				InputStream in = part.getInputStream();
	
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
	
				byte[] buffer = new byte[2048];
				int count = 0;
				while ((count = in.read(buffer)) >= 0)
				{
					bos.write(buffer, 0, count);
				}
				in.close();
				bos.close();
	
				attachment.setContent(bos.toByteArray());
				attachments.add(attachment);
			} catch (Exception e)
			{
				log.error("Erro ao pegar anexos: " + e);
			}
			  
		}

		if (part.getContentType().toLowerCase().startsWith("text/plain") && (disposition == null || (disposition != null && disposition.equals(""))))
		{
			try {
				InputStream is = part.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	
				String thisLine="";
				bodytext = thisLine;
					
				do
				{
					//le as linhas do email e salva para gravar no campo Descricao do chamado		
					if (bodytext != "") 
					{
						if ((thisLine.length() == 0) || (thisLine == " ")) 
						{
							bodytext += "\n";
							thisLine = reader.readLine();
						}
						else 
						{
							bodytext += thisLine;
							thisLine = reader.readLine();	
						}
					}
					else 
					{
						bodytext += thisLine;
						thisLine = reader.readLine();
					}
				
				}
				while (thisLine != null);
				
			} catch (Exception e){
				log.error("[MR] Erro ao validar conte�do: " + e);
			}
		}

		/*
		 * Tratar outros tipos: multipart/related, multipart/alternative,
		 * text/html...
		 */
	}

	public String getSubject()
	{
		return subject;
	}

	public String getBodytext()
	{
		return bodytext;
	}

	public int getAttachmentCount()
	{
		if (attachments == null)
			return 0;
		return attachments.size();
	}

	public Attachment getAttachment(int i)
	{
		return attachments.get(i);
	}
}