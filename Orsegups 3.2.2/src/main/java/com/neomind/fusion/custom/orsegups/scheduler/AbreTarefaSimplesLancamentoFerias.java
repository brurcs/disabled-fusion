package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.internal.bind.ReflectiveTypeAdapterFactory.Adapter;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaSimplesLancamentoFerias implements CustomJobAdapter {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(AbreTarefaSimplesLancamentoFerias.class);

    @Override
    public void execute(CustomJobContext arg0) {
	
	Connection conn = PersistEngine.getConnection("VETORH"); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesLancamentoFerias");
	log.warn("##### INICIO AGENDADOR DE TAREFA: AbreTarefaSimplesLancamentoFerias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: AbreTarefaSimplesLancamentoFerias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	
	try	{	    
		    StringBuffer  varname1 = new StringBuffer();
		    
		    varname1.append("SELECT per.NumEmp, ");
		    varname1.append("               fun.codfil, ");
		    varname1.append("               fun.tipcol, ");
		    varname1.append("               per.NumCad, ");
		    varname1.append("               fun.nomfun, ");
		    varname1.append("               orn.usu_codreg, ");
		    varname1.append("               reg.USU_NomReg as nomReg, ");
		    varname1.append("               SUM(per.AvoFer) AS AvoFer, ");
		    varname1.append("               per2.FimPer, ");
		    varname1.append("               CONVERT(varchar(20),per2.FimPer -30,103) as FimPerFormatada ");
		    varname1.append("    FROM R040PER per ");
		    varname1.append(" INNER JOIN R034FUN fun on fun.numemp = per.numemp ");
		    varname1.append("                       AND fun.tipcol = per.tipcol ");
		    varname1.append("                       AND fun.numcad = per.numcad ");
		    varname1.append(" INNER JOIN R016ORN orn ON orn.TabOrg = fun.TabOrg ");
		    varname1.append("                       AND orn.NumLoc = fun.NumLoc ");
		    varname1.append(" INNER JOIN usu_t200reg reg on orn.usu_codreg = reg.USU_CodReg ");
		    varname1.append("  LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp ");
		    varname1.append("                                     AND afa.TipCol = fun.TipCol ");
		    varname1.append("                                     AND afa.NumCad = fun.NumCad ");
		    varname1.append("                                     AND afa.DatAfa = (SELECT MAX (afa2.DatAfa) ");
		    varname1.append("                                                         FROM R038AFA afa2 ");
		    varname1.append("                                                        WHERE afa2.NUMEMP = afa.NUMEMP ");
		    varname1.append("                                                          AND afa2.TIPCOL = afa.TIPCOL ");
		    varname1.append("                                                          AND afa2.NUMCAD = afa.NUMCAD ");
		    varname1.append("                                                          AND afa2.DATAFA <= GETDATE() ");
		    varname1.append("                                                          AND (afa2.DatTer = '1900-12-31 00:00:00' OR afa2.DatTer >= cast(floor(cast(GETDATE() as float)) as datetime)))) ");
		    varname1.append("  LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
		    varname1.append("  LEFT JOIN R040PER per2 ON per2.NumCad = per.NumCad ");
		    varname1.append("                        AND per2.NumEmp = per.NumEmp ");
		    varname1.append("                        AND per2.TipCol = per.TipCol ");
		    varname1.append("                        AND per2.SitPer = 0 ");
		    varname1.append("                        AND per2.IniPer >= '2000-01-01' ");
		    varname1.append("                        AND per2.IniPer <= getdate() ");
		    varname1.append("                        AND per2.FimPer = (SELECT MAX (per3.FimPer) ");
		    varname1.append("                                             FROM R040PER per3 ");
		    varname1.append("                                            WHERE per3.NumEmp = per.NumEmp ");
		    varname1.append("                                              AND per3.TipCol = per.TipCol ");
		    varname1.append("                                              AND per3.NumCad = per.NumCad ");
		    varname1.append("                                              AND per3.SitPer = 0 ");
		    varname1.append("                                              AND per3.IniPer >= '2000-01-01' ");
		    varname1.append("                                              AND per3.IniPer <= GETDATE()) ");
		    varname1.append("   WHERE per.SitPer = 0 ");
		    varname1.append("     AND fun.tipcol = 1 ");
		    varname1.append("     AND per.iniper >= '2000-01-01' ");
		    varname1.append("     AND per.iniper <= GETDATE() ");
		    varname1.append("     AND CONVERT(varchar(20),per2.FimPer -30,103) = convert(varchar(10), GETDATE()+90, 103) ");
		    varname1.append("     AND fun.NumEmp NOT IN (3, 5, 11, 12, 13, 14, 999) ");
		    varname1.append("     AND (afa.SitAfa IS NULL ");
		    varname1.append("      OR afa.SitAfa NOT IN (3,4,7,8,74,75) ");
		    varname1.append("      OR (afa.SitAfa = 7 ");
		    varname1.append("      AND afa.DatAfa > cast(floor(cast(GETDATE() as float)) as datetime))) ");
		    varname1.append("     AND NOT EXISTS (SELECT * ");
		    varname1.append("                       FROM [CACUPE\\SQL02].fusion_Producao.DBO.d_TarefaLancamentoFerias tlf WITH (NOLOCK) ");
		    varname1.append("                       INNER JOIN [CACUPE\\SQL02].fusion_Producao.DBO.WFProcess w WITH (NOLOCK) ");
		    varname1.append("                       ON w.code = tlf.tarefa ");
		    varname1.append("                       INNER JOIN [CACUPE\\SQL02].fusion_Producao.DBO.D_Tarefa t WITH (NOLOCK)  ");
		    varname1.append("                       ON t.wfprocess_neoId = w.neoId  ");
		    varname1.append("         WHERE w.processState != 0 ");
		    varname1.append("          AND tlf.numemp = fun.NumEmp ");
		    varname1.append("          AND tlf.tipcol = fun.TipCol ");
		    varname1.append("          AND tlf.numcad = fun.NumCad ");
		    varname1.append("          AND cast(floor(cast(tlf.fimper as float)) as datetime) = cast(floor(cast(per.FimPer as float)) as datetime)) ");
		    varname1.append("   GROUP BY per.NumEmp, ");
		    varname1.append("            fun.codfil, ");
		    varname1.append("            fun.tipcol, ");
		    varname1.append("            fun.nomfun, ");
		    varname1.append("            per.NumCad, ");
		    varname1.append("            orn.usu_codreg, ");
		    varname1.append("            reg.USU_NomReg, ");
		    varname1.append("            afa.SitAfa, ");
		    varname1.append("            sit.DesSit, ");
		    varname1.append("            per2.FimPer, ");
		    varname1.append("            CONVERT(varchar(20),per2.FimPer,103) ");
		    varname1.append("     HAVING SUM(per.AvoFer) >= 21 ");
		    varname1.append("   ORDER BY SUM(per.AvoFer) DESC ");                                                                                                        
		    
		    pstm = conn.prepareStatement(varname1.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    rs = pstm.executeQuery();	
	
		    while (rs.next())  {	    	
		    	Long codEmpresa = rs.getLong("NumEmp");

				String codFilial = "";
				codFilial = ""+rs.getInt("CodFil");
				
				Long numcad = rs.getLong("NumCad");
				Long tipcol = rs.getLong("tipcol");
				Timestamp fimPeriodo = rs.getTimestamp("FimPer");
				
				String titulo = rs.getString("nomReg") + " - " + rs.getString("AvoFer") + " AVOS - " + codEmpresa + " - " + rs.getString("NumCad") + " - " + rs.getString("NomFun");
				
				String descricaoTarefa = "";
				descricaoTarefa =  rs.getString("AvoFer")+ " AVOS - " + codEmpresa + " - " + rs.getString("NumCad") + " - " + rs.getString("NomFun") + " deverá sair de férias até " + rs.getString("FimPerFormatada") + ".";
				
				if(!possuiTarefasAbertas(codEmpresa, tipcol, numcad, fimPeriodo)) {
					abrirTarefa(codEmpresa, numcad, titulo, descricaoTarefa, tipcol, fimPeriodo);					
				}
		    }
		    
		} catch (Exception e) {
		    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Lancameno Ferias");
		    e.printStackTrace();
		    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		} finally {
		    try {
			    rs.close();
				pstm.close();
				conn.close();
		    } catch (SQLException e) {
		    	e.printStackTrace();
		    }
	
		    log.warn("##### FIM AGENDADOR DE TAREFA: AbreTarefaSimplesLancamentoFerias - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    System.out.println("##### FIM AGENDADOR DE TAREFA: AbreTarefaSimplesLancamentoFerias - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
    }
    
    private boolean possuiTarefasAbertas(Long codEmpresa, Long tipcol, Long numcad, Timestamp fimPeriodo) {
    	
    	StringBuilder sql = new StringBuilder();
    	PreparedStatement pstm = null;
    	Connection conn = null;
    	ResultSet rs = null;
    	
	    try {
	    	
			sql.append(" SELECT * FROM d_TarefaLancamentoFerias tlf WITH (NOLOCK) ");
			sql.append(" INNER JOIN WFProcess w WITH (NOLOCK) ");
			sql.append(" ON w.code = tlf.tarefa ");
			sql.append(" INNER JOIN D_Tarefa t WITH (NOLOCK)  ");
			sql.append(" ON t.wfprocess_neoId = w.neoId  ");
			sql.append(" WHERE w.processState = 0 ");
			sql.append(" AND tlf.numemp = ? ");
			sql.append(" AND tlf.tipcol = ? ");
			sql.append(" AND tlf.numcad = ? ");
			sql.append(" AND cast(floor(cast(tlf.fimper as float)) as datetime) = cast(floor(cast(? as float)) as datetime) ");
			
			conn = PersistEngine.getConnection("");
			
			pstm = conn.prepareStatement(sql.toString());

			pstm.setLong(1, codEmpresa);
			pstm.setLong(2, tipcol);
			pstm.setLong(3, numcad);
			pstm.setTimestamp(4, fimPeriodo);

			rs = pstm.executeQuery();
			
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
    	 
		return false;
	}

	/**
     * Metodo de abertura de tarefas
     * 
     * @param codEmp
     * @param codFil
     * @param numCtr
     * @param numPos
     * @param numeroOS
     * @return numero da Tarefa
     */
    private void abrirTarefa(Long _codEmp, Long _numCad, String _titulo, String _descricaoTarefa, Long _tipCol, Timestamp _fimPer) throws Exception {
	
		String solicitante = OrsegupsUtils.getUserNeoPaper("solicitanteLancamentoFerias");	
		String executor = OrsegupsUtils.getUserNeoPaper("executorLancamentoFerias");
			
		GregorianCalendar fimPeriodo = new GregorianCalendar();
		fimPeriodo.setTimeInMillis(_fimPer.getTime());
		
		GregorianCalendar prazo = new GregorianCalendar();
		
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);
	
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		String codigoTarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, _titulo, _descricaoTarefa, "1", "hadouken", prazo);
		
		registraAberturaTarefa(_codEmp, _numCad, _tipCol, fimPeriodo, codigoTarefa);
    }

	private void registraAberturaTarefa(Long _codEmp, Long _numCad, Long _tipCol, GregorianCalendar fimPeriodo, String tarefa) {
		
		try {
			
			NeoObject noFormulario = AdapterUtils.getInstantiableEntityInfo("TarefaLancamentoFerias").createNewInstance();
			EntityWrapper ewFormulario = new EntityWrapper(noFormulario);
			
			ewFormulario.findField("numemp").setValue(_codEmp);
			ewFormulario.findField("tipcol").setValue(_tipCol);
			ewFormulario.findField("numcad").setValue(_numCad);
			ewFormulario.findField("fimper").setValue(fimPeriodo);
			ewFormulario.findField("tarefa").setValue(tarefa);
			
			PersistEngine.persist(noFormulario);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException(this.getClass().getSimpleName()+" - Erro ao registrar a abertura da Tarefa no Formulário TarefaLancamentoFerias");
		}
		
		
	}
}
