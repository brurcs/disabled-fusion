package com.neomind.fusion.custom.orsegups.e2doc.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.coremedia.iso.boxes.CompositionTimeToSample.Entry;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docEngine;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docPrevidenciaEngine;
import com.neomind.fusion.custom.orsegups.e2doc.xml.previdencia.Pastas;

@Path(value = "documentosPrevidencia")
public class E2docPrevidenciaHandler {
    
	@POST
	@Path("pesquisaPrevidencia")
	@Produces("application/json")
	public Pastas pesquisaPrevidencia(
		@MatrixParam("tiposDocumentos") String tiposDocumentos, @MatrixParam("empresa") String empresa,
		@MatrixParam("competencia") String competencia) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (tiposDocumentos != null && !tiposDocumentos.trim().isEmpty()){
		atributosPesquisa.put("Tipo de documento", tiposDocumentos.trim());
	    }
	    
	    if(empresa != null && !empresa.trim().isEmpty()){
		atributosPesquisa.put("Empresa", "%"+empresa.trim()+"%");
	    }
	    
	    if(competencia != null && !competencia.trim().isEmpty()){
		atributosPesquisa.put("Competência", competencia.trim());
	    }

	    E2docPrevidenciaEngine e2docEngine = new E2docPrevidenciaEngine();
	    
	    Pastas pastas = e2docEngine.pesquisaPrevidencia(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("getTiposDocumentos")
	@Produces("application/json")
	public List<String> getTiposDocumentos() {
	    
	    E2docPrevidenciaEngine e2docEngine = new E2docPrevidenciaEngine();
	    
	    return e2docEngine.getTiposDocumentos();

	}
	
	@POST
	@Path("getEmpresas")
	@Produces("application/json")
	public List<String> getEmpresas() {
	    
	    E2docPrevidenciaEngine e2docEngine = new E2docPrevidenciaEngine();
	    
	    return e2docEngine.getEmpresas();
	    

	}
	
	@GET
	@Path("downloadImagem2/{id}")
	@Produces("application/pdf")
	public Response getFile2(@PathParam("id") String id) throws MalformedURLException, IOException
	{
	    E2docPrevidenciaEngine e2docEngine = new E2docPrevidenciaEngine();

	    HashMap<String,Object> map = e2docEngine.retornaLinkImagemDocumento2(id);
	    String ext = (String) map.get("ext");
	    File file = (File) map.get("file");
	    ResponseBuilder response = Response.ok((Object)file);
	    response.header("Content-Disposition", "attachment;filename=temp-"+new GregorianCalendar().getTimeInMillis()+"."+ext);
	    return response.build();

	}
}
