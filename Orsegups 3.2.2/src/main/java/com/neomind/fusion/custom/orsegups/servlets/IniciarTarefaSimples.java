package com.neomind.fusion.custom.orsegups.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

@WebServlet(name = "IniciarTarefaSimples", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.servlets.IniciarTarefaSimples" })
public class IniciarTarefaSimples extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/plain");
		response.setCharacterEncoding("ISO-8859-1");

		final PrintWriter out = response.getWriter();

		String action = "";

		if (request.getParameter("action") != null) {
			action = request.getParameter("action");
		}

		if (action.equalsIgnoreCase("justificarDescontoBonificacao")) {

			this.justificarDescontoBonificacao(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesGerente")) {

			this.abrirTarefaSimplesGerente(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesCoordenacao")) {

			this.abrirTarefaSimplesCoordenacao(request, out);

		} else if (action.equalsIgnoreCase("verificaTarefaSimplesEmAbertoPorTitulo")) {

			this.verificaTarefaSimplesEmAbertoPorTitulo(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesAtualizaRamal")) {

			this.abrirTarefaSimplesAtualizaRamal(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesAcessosCoordenador")) {

			this.abrirTarefaSimplesAcessosCoordenador(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesAcessosGerente")) {

			this.abrirTarefaSimplesAcessosGerente(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesRescisaoComplementarSemCalculo")) {

			this.abrirTarefaSimplesRescisaoComplementarSemCalculo(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesGerenteProjetos")) {

			this.abrirTarefaSimplesGerenteProjetos(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesHorasExtrasDiarias")) {

			this.abrirTarefaSimplesHorasExtrasDiarias(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesHorasExtrasDiariasIntervalo")) {

			this.abrirTarefaSimplesHorasExtrasDiariasIntervalo(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesInterjornada")) {

			this.abrirTarefaSimplesInterjornada(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimplesPostoArmado")) {

			this.abrirTarefaSimplesPostoArmado(request, out);

		} else if (action.equalsIgnoreCase("abrirTarefaSimples")){

			this.abrirTarefaSimples(request,out);
		}
	}

	private void justificarDescontoBonificacao(HttpServletRequest request, PrintWriter out)
	{
		Long codreg = Long.valueOf(request.getParameter("codreg"));
		Long tipemc = Long.valueOf(request.getParameter("tipemc"));
		String codfam = request.getParameter("codfam");
		String solicitante = request.getParameter("codusu");//"dilmoberger";
		String executor = responsavelJustificarDescontoBonificacao(codreg, tipemc, codfam);
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String origem = "2";
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 5L);

		out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo));
	}

	private void abrirTarefaSimplesGerente(HttpServletRequest request, PrintWriter out)
	{
		try
		{
			Long codreg = Long.valueOf(request.getParameter("codreg"));
			NeoPaper papelExecutor = OrsegupsUtils.getPapelGerenteRegional(codreg);
			String executor = OrsegupsUtils.getUserNeoPaper(papelExecutor.getCode());

			String papelSolicitante = request.getParameter("papelSolicitante");//"dilmoberger (papel)";
			String solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);

			String titulo = request.getParameter("titulo");
			String descricao = request.getParameter("descricao");
			String prazoStr = request.getParameter("prazo");
			String origem = "2";
			GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), Long.parseLong(prazoStr));

			out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void abrirTarefaSimplesCoordenacao(HttpServletRequest request, PrintWriter out)
	{
		Long codreg = Long.valueOf(request.getParameter("codreg"));
		NeoPaper papelExecutor = OrsegupsUtils.getPapelRetornaEquipamento(codreg);
		String executor = OrsegupsUtils.getUserNeoPaper(papelExecutor.getCode());

		String solicitante = request.getParameter("solicitante");

		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String origem = "2";
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 10L);
		
		try {
			out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo));
		} catch (WorkflowException e){
			e.printStackTrace();
			out.print(e.getErrorList().get(0).getI18nMessage());
		} catch (Exception e){
			e.printStackTrace();
			out.print(e.getMessage());
		}	
	}

	private void abrirTarefaSimplesAtualizaRamal(HttpServletRequest request, PrintWriter out)
	{
		try{
			Long codreg = Long.valueOf(request.getParameter("codreg"));
			String lotacao = request.getParameter("lotacao");
			NeoPaper papelExecutor;	    

			//Direcionar para a Rubia - Central
			if(lotacao.equals("Coordenadoria de Monitoramento")){
				papelExecutor = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "CoordenadorDeMonitoramento"));
			}
			else{
				papelExecutor = OrsegupsUtils.getPapelCoordenadorRegional(codreg, lotacao);
			}

			String executor = OrsegupsUtils.getUserNeoPaper(papelExecutor.getCode());
			String solicitante = request.getParameter("solicitante");

			String titulo = request.getParameter("titulo");
			String descricao = request.getParameter("descricao");
			String origem = "2";
			GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), Long.valueOf(request.getParameter("prazo")));

			String retorno = abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo);

			if(retorno.contains("Erro")){
				throw new Exception(retorno);
			} else {
				out.print(retorno);
			}

		} catch (WorkflowException e){
			e.printStackTrace();
			out.print(e.getErrorList().get(0).getI18nMessage());
		}catch (Exception e){
			e.printStackTrace();
			out.print(e.getMessage());
		}
	}

	private void abrirTarefaSimplesHorasExtrasDiarias(HttpServletRequest request, PrintWriter out)
	{

		String solicitante = request.getParameter("solicitante");
		String executor = request.getParameter("executor");
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String arquivo = "\\\\fsoofs01\\Vetorh\\RondaPonto\\HRES108.pdf";
		String origem = "2";
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), Long.valueOf(7L));
		NeoFile anexo = OrsegupsUtils.criaNeoFile(new File(arquivo));

		out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo, anexo));
	}

	private void abrirTarefaSimplesHorasExtrasDiariasIntervalo(HttpServletRequest request, PrintWriter out)
	{

		String solicitante = request.getParameter("solicitante");
		String executor = request.getParameter("executor");
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String arquivo = "\\\\fsoofs01\\Vetorh\\RondaPonto\\HRAP502.pdf";
		String origem = "2";
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), Long.valueOf(7L));
		NeoFile anexo = OrsegupsUtils.criaNeoFile(new File(arquivo));

		out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo, anexo));
	}

	private void abrirTarefaSimplesInterjornada(HttpServletRequest request, PrintWriter out)
	{

		String solicitante = request.getParameter("solicitante");
		String executor = request.getParameter("executor");
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String arquivo = "\\\\fsoofs01\\Vetorh\\RondaPonto\\HRES109.pdf";
		String origem = "2";
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), Long.valueOf(7L));
		NeoFile anexo = OrsegupsUtils.criaNeoFile(new File(arquivo));

		out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo, anexo));
	}

	private void abrirTarefaSimplesGerenteProjetos(HttpServletRequest request, PrintWriter out)
	{


		String solicitante = request.getParameter("solicitante");
		String executor = request.getParameter("executor");
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String arquivo = request.getParameter("arquivo");
		String origem = "2";
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 5L);
		NeoFile anexo = OrsegupsUtils.criaNeoFile(new File(arquivo));

		out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo, anexo));
	}

	private void abrirTarefaSimplesPostoArmado(HttpServletRequest request, PrintWriter out)
	{


		String solicitante = request.getParameter("solicitante");
		String executor = request.getParameter("executor");
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String origem = "2";
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 1L);

		out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo));
	}

	public static String responsavelJustificarDescontoBonificacao(Long codreg, Long tipemc, String codfam) throws WorkflowException
	{
		String executor = "";
		String nomePapel = "";

		switch (codreg.intValue())
		{
		case 1:
			if (tipemc == 1L)
			{
				if (codfam != null && codfam.equals("SER102"))
				{
					nomePapel = "Gerente de Vigilância Eletrônica";
				}
				else
				{
					nomePapel = "Gerente de Segurança";
				}
			}
			else
			{
				nomePapel = "Gerente Comercial Mercado Público";
			}
			break;
		case 2:
			nomePapel = "Justificar Desconto Bonificacao IAI";
			break;
		case 3:
			nomePapel = "Justificar Desconto Bonificacao BQE";
			break;
		case 4:
			nomePapel = "Justificar Desconto Bonificacao BNU";
			break;
		case 5:
			nomePapel = "Justificar Desconto Bonificacao JLE";
			break;
		case 6:
			nomePapel = "Justificar Desconto Bonificacao LGS";
			break;
		case 7:
			nomePapel = "Justificar Desconto Bonificacao TRO";
			break;
		case 8:
			nomePapel = "Justificar Desconto Bonificacao GPR";
			break;
		case 9:
			nomePapel = "Justificar Desconto Bonificacao ACL";
			break;
		case 10:
			nomePapel = "Justificar Desconto Bonificacao CCO";
			break;
		case 11:
			nomePapel = "Justificar Desconto Bonificacao RSL";
			break;
		case 12:
			nomePapel = "Justificar Desconto Bonificacao JGS";
			break;
		case 13:
			if (tipemc == 1L)
			{
				if (codfam != null && codfam.equals("SER102"))
				{
					nomePapel = "Justificar Desconto Bonificacao CTA Eletronica";
				}
				else
				{
					nomePapel = "Justificar Desconto Bonificacao CTA Humana";
				}
			}
			else
			{
				nomePapel = "Gerente Comercial Mercado Público";
			}
			break;
		case 15:
			if (tipemc == 1L)
			{
				if (codfam != null && codfam.equals("SER102"))
				{
					nomePapel = "Justificar Desconto Bonificacao CTA Eletronica";
				}
				else
				{
					nomePapel = "Justificar Desconto Bonificacao CTA Humana";
				}
			}
			else
			{
				nomePapel = "Gerente Comercial Mercado Público";
			}
			break;
		case 14:
			nomePapel = "Justificar Desconto Bonificacao CSC";
			break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			for (NeoUser user : obj.getUsers())
			{
				executor = user.getCode();
				break;
			}
			if (executor.equals(""))
			{
				throw new WorkflowException("Responsável não localizado!");
			}
			return executor;
		}
		else
		{
			throw new WorkflowException("Responsável não localizado!");
		}
	}

	private static String abrirTarefa(String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo)
	{
		/**
		 * @author neomind willian.mews - Alterado para chamar classe já existente e assim manter a
		 *         lógica centralizada
		 * @date 11/03/2015
		 */

		com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples tarefaSimples = new com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples();
		return tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo);

		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
	}

	private static String abrirTarefa(String solicitante, String executor, String titulo, String descricao, String origem, String avanca, GregorianCalendar prazo,NeoFile file)
	{

		// adicionado por Dalvan Schmitz tarefa simples 808196 é solicitado enviar um arquivo em anexo. 
		com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples tarefaSimples = new com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples();
		return tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo,file);


	}

	private void verificaTarefaSimplesEmAbertoPorTitulo(HttpServletRequest request, PrintWriter out)
	{
		String titulo = request.getParameter("titulo");
		String resposta = "NA";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try
		{
			if (titulo != null && !titulo.isEmpty() && !titulo.equalsIgnoreCase("null"))
			{
				titulo = titulo.concat("%");
				StringBuilder builder = new StringBuilder();
				builder.append(" select code from WFProcess WITH(NOLOCK) INNER JOIN ProcessModel pm with (NOLOCK) on pm.neoId = model_neoId where saved = 1 and  processState = 0 and ");
				builder.append("  entity_neoid in ( ");
				builder.append(" select neoid from D_tarefa WITH(NOLOCK) where titulo like ? ) ");

				connection = PersistEngine.getConnection("FUSIONPROD");
				statement = connection.prepareStatement(builder.toString());
				statement.setString(1, titulo);
				resultSet = statement.executeQuery();
				if (resultSet.next())
				{
					if (resultSet.getString("code") != null && !resultSet.getString("code").isEmpty())
						resposta = resultSet.getString("code");
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			out.print(resposta);
		}
	}

	private void abrirTarefaSimplesAcessosCoordenador(HttpServletRequest request, PrintWriter out)
	{
		try {
			Long codreg = Long.valueOf(request.getParameter("codreg"));
			String lotacao = request.getParameter("lotacao");
			NeoPaper papelExecutor = OrsegupsUtils.getPapelCoordenadorRegional(codreg, lotacao);
			String executor = OrsegupsUtils.getUserNeoPaper(papelExecutor.getCode());

			String solicitante = request.getParameter("solicitante");

			String titulo = request.getParameter("titulo");
			String descricao = request.getParameter("descricao");
			String origem = "2";
			GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), Long.valueOf(request.getParameter("prazo")));

			String retorno = abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo);

			if(retorno.contains("Erro")){
				throw new Exception(retorno);
			} else {
				out.print(retorno);
			}

		} catch (Exception e){
			e.printStackTrace();
			out.print(e.getMessage());
		}
	}

	private void abrirTarefaSimplesAcessosGerente(HttpServletRequest request, PrintWriter out)
	{
		try {
			Long codreg = Long.valueOf(request.getParameter("codreg"));
			String lotacao = request.getParameter("lotacao");
			NeoPaper papelExecutor = OrsegupsUtils.getPapelGerenteRegional(codreg, lotacao);
			String executor = OrsegupsUtils.getUserNeoPaper(papelExecutor.getCode());

			String solicitante = request.getParameter("solicitante");

			String titulo = request.getParameter("titulo");
			String descricao = request.getParameter("descricao");
			String origem = "2";
			GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), Long.valueOf(request.getParameter("prazo")));

			String retorno = abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo);
			if(retorno.contains("Erro")){
				throw new Exception(retorno);
			} else {
				out.print(retorno);
			}
		} catch (Exception e){
			e.printStackTrace();
			out.print(e.getMessage());
		}
	}

	private void abrirTarefaSimplesRescisaoComplementarSemCalculo(HttpServletRequest request, PrintWriter out)
	{
		String solicitante = request.getParameter("solicitante");
		String executor = request.getParameter("executor");
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String origem = "2";
		GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 2L);

		out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, null, prazo));
	}

	private void abrirTarefaSimples(HttpServletRequest request, PrintWriter out){
		try {
			String solicitante = request.getParameter("solicitante");
			String executor = request.getParameter("executor");
			String titulo = request.getParameter("titulo");
			String descricao = request.getParameter("descricao");
			String origem = "2";
			GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 2L);

			out.print(abrirTarefa(solicitante, executor, titulo, descricao, origem, "nao", prazo));
		} catch (Exception e) {
			e.printStackTrace();
			out.print("Erro ao abrir tarefa.");
		}
	}

}
