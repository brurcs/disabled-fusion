package com.neomind.fusion.custom.orsegups.event;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEvent;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEventListener;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class ValidateCancelFAPAdapter implements WorkflowValidateCancelEventListener {

	@Override
	public void validateCancel(WorkflowValidateCancelEvent event) throws WorkflowException {
		
		EntityWrapper wrapper = event.getWrapper();
		WFProcess proc = event.getProcess();
		
		Boolean aprovado = (Boolean) wrapper.findValue("aprovado");
		
		System.out.println("Validando o Cancelamento de Tarefa da FAP: " + proc.getCode());
		if (aprovado){
			throw new WorkflowException("O processo " + proc.getCode() + " não pode ser cancelado, pois já foi aprovado.");
		}		
	}
}


