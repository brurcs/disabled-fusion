package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.ContratosInativosVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class RotinaCancelaContratosInativos implements CustomJobAdapter {

	private static final Log log = LogFactory.getLog(RotinaCancelaContratosInativos.class);
	Long logKey = GregorianCalendar.getInstance().getTimeInMillis(); 
	
	@Override
	public void execute(CustomJobContext arg0) {

		log.error("##### INICIO AGENDADOR DE TAREFA: RotinaCancelaContratosInativos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " ["+logKey+"]");
		System.out.println("##### INICIO AGENDADOR DE TAREFA: RotinaCancelaContratosInativos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " ["+logKey+"]");
		
		Connection conn = PersistEngine.getConnection("");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;

		GregorianCalendar dataAtual = new GregorianCalendar();

		List<ContratosInativosVO> listaContratosSemPosto = new ArrayList<ContratosInativosVO>();

		String html = null;

		try {
			/**
			 * Verifica os contratos gerados a mais de 30 dias pelo Fusion e que não possuem
			 * posto
			 */
			sql.append(" SELECT Filiais.codemp AS 'EMPRESA', Filiais.codfil AS 'FILIAL', Contratos.usu_numctr AS 'CONTRATO' FROM D_FGCPrincipal Principal ");
			sql.append(" INNER JOIN D_FCGContratoDadosGeraisSapiens DadosGerais ON Principal.dadosGeraisContrato_neoId = DadosGerais.neoId ");
			sql.append(" INNER JOIN X_SAPIENSE070FIL SapiensFil ON Principal.empresa_neoId = SapiensFil.neoId ");
			sql.append(" INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.E070FIL Filiais ON SapiensFil.codemp = Filiais.codemp AND SapiensFil.codfil = Filiais.codfil ");
			sql.append(" INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.usu_t160ctr Contratos ON DadosGerais.numeroContratoSapiens = Contratos.usu_numctr ");
			sql.append(" AND Filiais.codemp = Contratos.usu_codemp AND Filiais.codfil = Contratos.usu_codfil ");
			sql.append(" AND NOT EXISTS( ");
			sql.append(" 				SELECT USU_NUMCTR FROM  [FSOODB04\\SQL02].Sapiens.dbo.usu_t160cvs  ");
			sql.append(" 				WHERE Contratos.usu_numctr = usu_t160cvs.usu_numctr  ");
			sql.append("				AND Contratos.usu_codemp = usu_t160cvs.usu_codemp  ");
			sql.append(" 				AND Contratos.usu_codfil = usu_t160cvs.usu_codfil) ");
			sql.append(" WHERE Contratos.usu_datger <= GETDATE()-30 AND Contratos.usu_sitctr = 'A' ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			html = montaCabelhoEmail();
			html += montaCorpoSuperior();

			while (rs.next()) {

				ContratosInativosVO contratoAux = new ContratosInativosVO();

				Long contrato = rs.getLong("CONTRATO");
				Long empresa = rs.getLong("EMPRESA");
				Long filial = rs.getLong("FILIAL");

				contratoAux.setNumeroContrato(contrato);
				contratoAux.setNumeroEmpresa(empresa);
				contratoAux.setNumeroFilial(filial);
				contratoAux.setDataInativacao(dataAtual);


				listaContratosSemPosto.add(contratoAux);
			}

			Iterator<ContratosInativosVO> iterator = listaContratosSemPosto.iterator();

			while (iterator.hasNext()) {

				ContratosInativosVO contratoQueSeraInativado = iterator.next();

				int resultados = inativaContratos(contratoQueSeraInativado.getNumeroContrato(),
						contratoQueSeraInativado.getNumeroEmpresa(), contratoQueSeraInativado.getNumeroFilial(),
						contratoQueSeraInativado.getDataInativacao());
				
				/*
				 * Informa no e-mail apenas se o Update tiver afetado mais de 0 linhas
				 */
				if(resultados > 0) {
					html += montaCorpoInferior(contratoQueSeraInativado.getNumeroContrato(),
							contratoQueSeraInativado.getNumeroEmpresa(), contratoQueSeraInativado.getNumeroFilial());					
				}else {
					System.out.println("#######ERRO - NENHUMA LINHA AFETADA AO EFETUAR UPDATE NO CONTRATO "
							+ contratoQueSeraInativado.getNumeroContrato() + " , DA EMPRESA "
							+ contratoQueSeraInativado.getNumeroEmpresa() + " , E DA FILIAL "
							+ contratoQueSeraInativado.getNumeroFilial());
				}
			}

			html += montaRodapeEmail();

			ArrayList<String> destinatario = new ArrayList<String>(); 
			destinatario.add("herisson.ferreira@orsegups.com.br");
			destinatario.add("liberacaofaturamentoprivado@orsegups.com.br");

			for (String destinatarioAux : destinatario) {
				OrsegupsUtils.sendEmail2Orsegups(destinatarioAux, "fusion@orsegups.com.br",
						"[SAPIENS] Contratos Inativados gerados pelo Fusion", "", html.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("##### ERRO AGENDADOR DE TAREFA: RotinaCancelaContratosInativos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " ["+logKey+"]");
			System.out.println("##### ERRO AGENDADOR DE TAREFA: RotinaCancelaContratosInativos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " ["+logKey+"]");
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			log.error("##### FIM AGENDADOR DE TAREFA: RotinaCancelaContratosInativos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " ["+logKey+"]");
			System.out.println("##### FIM AGENDADOR DE TAREFA: RotinaCancelaContratosInativos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " ["+logKey+"]");
		}

	}

	/**
	 * 
	 * @param numeroContrato
	 * @param numeroEmpresa
	 * @param numeroFilial
	 * @return Linhas afetadas pelo Update
	 */
	public int inativaContratos(Long numeroContrato, Long numeroEmpresa, Long numeroFilial,
			GregorianCalendar dataAtual) {

		int linhasAfetadas = 0;

		Connection conn = PersistEngine.getConnection("SAPIENS");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;

		Timestamp dataAtualizacao = new Timestamp(dataAtual.getTimeInMillis());

		try {
			/**
			 * Usuário 1003 Sapiens >> ctrsid
			 * 
			 */

			sql.append(" UPDATE USU_T160CTR ");
			sql.append(
					" SET USU_LIBFAT = 'N', USU_SUSFAT = 'S', USU_USUALT = 1003, USU_DATALT = ?, USU_CODMOT = 159, USU_OBSMOT = 'Inativado via Fusion pois não possui posto vínculado.', USU_SITCTR = 'I', USU_DATFIM = ? ");
			sql.append(" WHERE USU_CODEMP = ? AND USU_CODFIL = ? AND USU_NUMCTR = ? ");

			pstm = conn.prepareStatement(sql.toString());

			pstm.setTimestamp(1, dataAtualizacao);
			pstm.setTimestamp(2, dataAtualizacao);
			pstm.setLong(3, numeroEmpresa);
			pstm.setLong(4, numeroFilial);
			pstm.setLong(5, numeroContrato);

			linhasAfetadas = pstm.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, null);
		}

		return linhasAfetadas;
	}

	public String montaCabelhoEmail() {

		StringBuilder html = new StringBuilder();

		html.append(
				"\n <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> ");
		html.append("\n <html> ");
		html.append("\n 		<head> ");
		html.append("\n 			<meta charset=\"ISO-8859-1\"> ");
		html.append("\n 			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");
		html.append("\n 		</head> ");
		html.append("\n 		<body yahoo=\"yahoo\"> ");

		html.append("\n 		Prezado(a), ");
		html.append("\n 		<br> ");
		html.append(
				"\n 		Segue abaixo listagem de contratos inativados pois não possuem Postos e foram criados a mais de 30 dias pelo Fusion. ");
		html.append("\n 		<br> ");
		html.append("\n 		<br> ");

		return html.toString();
	}

	public String montaRodapeEmail() {
		StringBuilder html = new StringBuilder();

		html.append("\n 			</table> ");
		html.append("\n 		</body> ");
		html.append("\n </html> ");

		return html.toString();
	}

	public String montaCorpoSuperior() {
		StringBuilder html = new StringBuilder();

		html.append("\n 			<table width=\"100%\"cellspacing=\"0\" cellpadding=\"2\" border=\"1\"> ");
		html.append("\n 					<tr> ");
		html.append("\n 						<td colspan=\"10\"align=\"center\">");
		html.append("\n 							<strong> Lista de Contratos Inativados </strong> ");
		html.append("\n 						</td> ");
		html.append("\n 					</tr> ");
		html.append("\n 					<tr> ");
		html.append("\n 						<td align=\"center\"> ");
		html.append("\n 							<strong>Empresa</strong> ");
		html.append("\n 						</td> ");
		html.append("\n 						<td align=\"center\"> ");
		html.append("\n 							<strong>Filial</strong> ");
		html.append("\n 						</td> ");
		html.append("\n 						<td align=\"center\"> ");
		html.append("\n 							<strong>Contrato</strong> ");
		html.append("\n 						</td> ");
		html.append("\n 					</tr>");

		return html.toString();
	}

	public String montaCorpoInferior(Long contrato, Long empresa, Long filial) {
		StringBuilder html = new StringBuilder();

		html.append("\n 					<tr> ");
		html.append("\n 						<td> ");
		html.append("\n 							" + empresa);
		html.append("\n 						</td> ");
		html.append("\n 						<td> ");
		html.append("\n 							" + filial);
		html.append("\n 						</td> ");
		html.append("\n 						<td> ");
		html.append("\n 							" + contrato);
		html.append("\n 						</td> ");
		html.append("\n 					</tr>");

		return html.toString();
	}

}
