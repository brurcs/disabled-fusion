package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPTotalizaItens implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "";
		try
		{
			preencheDescricaoTSFap(processEntity);
			
			BigDecimal valorTotal = new BigDecimal(0.0);
			BigDecimal valorDesconto = new BigDecimal(0.0);
			Map<Long, String> mapCentroCustoValido = new HashMap<Long, String>();
			List<NeoObject> codigosCentroCusto = (List<NeoObject>) processEntity.findValue("listaCentroDeCusto");
			
			List<NeoObject> itens = (List<NeoObject>) processEntity.findValue("itemOrcamento");
			if (itens != null && !itens.isEmpty()) 	{
				for (NeoObject item : itens) {
					EntityWrapper wrpItem = new EntityWrapper(item);
					String tipoDoItem = (String) wrpItem.findValue("tipoItem.descricao");
					
					// Subtrair o valor de desconto
					if(tipoDoItem.equals("DESCONTO")) {
						valorTotal = valorTotal.subtract((BigDecimal)wrpItem.findValue("valor"));
						valorDesconto = valorDesconto.add((BigDecimal)wrpItem.findValue("valor"));
					}else {
						valorTotal = valorTotal.add((BigDecimal)wrpItem.findValue("valor"));						
					}
				}
			}
			processEntity.setValue("valorTotal", valorTotal);
			processEntity.setValue("valorDescontoFAP", valorDesconto);
			
			if (codigosCentroCusto != null && !codigosCentroCusto.isEmpty())
			{

				for (NeoObject centroCusto : codigosCentroCusto)
				{
					EntityWrapper wrpItem = new EntityWrapper(centroCusto);

					Long empresa = (Long) wrpItem.findValue("codigoEmpresa");
					String codigoCentroCusto = (String) wrpItem.findValue("codigoCentroCusto");

					if (empresa != null && codigoCentroCusto != null && !codigoCentroCusto.isEmpty())
						mapCentroCustoValido.put(empresa, codigoCentroCusto);
					else
					{
						mensagem = "Verifique a empresa " + empresa + " com centro de custo " + codigoCentroCusto + ", devido a inconsistência.";
						throw new WorkflowException(mensagem);
					}
				}

				for (Entry<Long, String> entry : mapCentroCustoValido.entrySet())
				{

					QLGroupFilter filterCcu = new QLGroupFilter("AND");
					filterCcu.addFilter(new QLEqualsFilter("codccu", entry.getValue()));
					filterCcu.addFilter(new QLEqualsFilter("nivccu", 8));
					filterCcu.addFilter(new QLEqualsFilter("codemp", entry.getKey()));

					List<NeoObject> codigoCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);
					if (codigoCentroCusto == null || (codigoCentroCusto != null && codigoCentroCusto.isEmpty()))
					{
						mensagem = "Centro de custo " + entry.getValue() + " inválido. Verifique se o mesmo é de 8º nível ou da mesma empresa do posto.";
						throw new WorkflowException(mensagem);
					}
				}
			}
			
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor contate a TI!!");
		}
	}

	private void preencheDescricaoTSFap(EntityWrapper processEntity) {
		
		
		try {
			
			StringBuilder descricao = new StringBuilder();
			
			descricao.append(" FAP: "+processEntity.findField("wfprocess.code").getValueAsString());
			descricao.append(" MOTIVO DA PAUSA: "+processEntity.findField("laudoEletronica.relato").getValueAsString().replaceAll("\\s\\s+"," "));
			
			NeoObject noTarefa = AdapterUtils.createNewEntityInstance("Tarefa");
			EntityWrapper ewTarefa = new EntityWrapper(noTarefa);
			
			ewTarefa.findField("DescricaoSolicitacao").setValue(descricao.toString());

			PersistEngine.persist(noTarefa);
			
			processEntity.findField("tarefaSimplesFAP").setValue(noTarefa);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro ao preencher a descrição da tarefa simples na FAP!");
		}
		
	}
}