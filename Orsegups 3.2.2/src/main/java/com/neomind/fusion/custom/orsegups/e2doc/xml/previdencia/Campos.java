package com.neomind.fusion.custom.orsegups.e2doc.xml.previdencia;

public class Campos {
    
    private String i63;
    private String i64;
    private String i65;
    
    public String getI63() {
        return i63;
    }
    public void setI63(String i63) {
        this.i63 = i63;
    }
    public String getI64() {
        return i64;
    }
    public void setI64(String i64) {
        this.i64 = i64;
    }
    public String getI65() {
        return i65;
    }
    public void setI65(String i65) {
        this.i65 = i65;
    }
    
    
    
    
}
