package com.neomind.fusion.custom.orsegups.relatorio;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.MaskFormatter;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class ImpressaoRelatorio
{

	private static final Log log = LogFactory.getLog(ImpressaoRelatorio.class);
	private static final String NOME_RELATORIO = "FichaUniformeEPI.jasper";

	/**
	 * Preenche o mapa de parâmetros enviados ao relatório.
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> preencheParametros(NeoObject dadosEForm, Boolean imprimeFichaCompleta)
	{

		Map<String, Object> paramMap = null;

		try
		{
			paramMap = new HashMap<String, Object>();
			EntityWrapper wrapper = new EntityWrapper(dadosEForm);

			QLEqualsFilter filter1 = new QLEqualsFilter("fichaUniformeEPI", dadosEForm);
			QLEqualsFilter filter2 = new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal());
			
			QLGroupFilter groupFilter = new QLGroupFilter("AND", filter1, filter2);
			
			List<NeoObject> eformsProcesso = PersistEngine.getObjects(AdapterUtils.getEntityClass("FUEProcessoFichaUniformeEPI"), groupFilter);
			
			Boolean inclusaoFicha = false;
			EntityWrapper wrapperEformPai = null;
			
			if(eformsProcesso != null && eformsProcesso.size() > 0)
			{
				wrapperEformPai = new EntityWrapper(eformsProcesso.get(0));
			}
			
			//existe processo em execução
			if(wrapperEformPai != null)
			{
				inclusaoFicha = (Boolean) wrapperEformPai.findValue("inclusaoFicha");
			}
			inclusaoFicha = (inclusaoFicha == null) ? false : inclusaoFicha;
			String titulo = (inclusaoFicha) ? "Ficha de Uniforme / EPI" : "Recibo de Entrega/Devolução Uniforme / EPI";

			paramMap.put("pathImg1", NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "imagens" + File.separator + "orsegups.jpg");
			paramMap.put("pathSub1", NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "FichaUniformeEPI_subreport1.jasper");

			paramMap.put("titulo", titulo);
			paramMap.put("nomeColaborador", NeoUtils.safeOutputString(wrapper.findValue("colaboradorRH.nomfun")));
			paramMap.put("cargoColaborador", NeoUtils.safeOutputString(wrapper.findValue("colaboradorRH.titred")));
			paramMap.put("empresaColaborador", NeoUtils.safeOutputString(wrapper.findValue("colaboradorRH.apeemp")));
			paramMap.put("matriculaColaborador", NeoUtils.safeOutputString(wrapper.findValue("colaboradorRH.numcad")));
			paramMap.put("admissaoColaborador", NeoUtils.safeDateFormat((GregorianCalendar) wrapper.findValue("colaboradorRH.datadm"), "dd/MM/yyyy"));
			paramMap.put("dataAtual", NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

			// Subrelatório (grid fichas)
			Collection<FichaDatasource> gridFicha = populaGridFicha(wrapper, imprimeFichaCompleta);
			paramMap.put("gridFicha", gridFicha);

			return paramMap;
		}
		catch (Exception e)
		{
			log.error("Erro ao preencher o mapa de parâmetros da Ficha!", e);
		}
		return paramMap;
	}

	/**
	 * Gera o PDF do relatório, utilizando JasperReports
	 */
	public static File geraPDF(NeoObject dadosEForm, Boolean imprimeFichaCompleta)
	{
		InputStream is = null;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		String path = "";

		try
		{
			// ...files/relatorios
			path = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + NOME_RELATORIO;
			is = new BufferedInputStream(new FileInputStream(path));
			paramMap = preencheParametros(dadosEForm, imprimeFichaCompleta); // obtém os parâmetros

			if (paramMap != null)
			{

				File file = File.createTempFile("fusexp", ".pdf");
				file.deleteOnExit();

				JasperPrint impressao = JasperFillManager.fillReport(is, paramMap);
				if (impressao != null && file != null)
				{
					JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
					return file;
				}
			}
		}
		catch (Exception e)
		{
			log.error("Erro ao gerar o PDF da Ficha!!", e);
		}
		return null;
	}
	
	public static File geraPDFComCPF(String cpf)
	{
	    Connection connSigma = null;
		InputStream is = null;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("P_CPF", cpf);
		String path = "";
		connSigma = PersistEngine.getConnection("");

		try
		{
			// ...files/relatorios
			path = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "RelatorioEPI.jasper";
			is = new BufferedInputStream(new FileInputStream(path));
			//paramMap = preencheParametros(dadosEForm, imprimeFichaCompleta); // obtém os parâmetros

			if (paramMap != null)
			{

				File file = File.createTempFile("RelatorioEPI", ".pdf");
				

				JasperPrint impressao = JasperFillManager.fillReport(is, paramMap,connSigma);
				if (impressao != null && file != null)
				{
					JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
					return file;
				}
			}
		}
		catch (Exception e)
		{
			log.error("Erro ao gerar o PDF da Ficha!!", e);
		}
		return null;
	}


	public static String mascaraCPF(String valor)
	{
		try
		{
			if (valor.contains(".") || valor.contains("-") || valor.equals(""))
				return valor;

			MaskFormatter msk = new MaskFormatter(getCPFMask());
			msk.setValueContainsLiteralCharacters(false);
			if (msk != null)
			{
				return msk.valueToString(valor);
			}
			else
				return valor;
		}
		catch (Exception e)
		{
			log.error("Erro ao aplicar a máscara de CPF no valor!", e);
		}
		return "";
	}

	public static String mascaraTelefone(Object value)
	{
		try
		{
			if (value instanceof BigDecimal)
			{
				value = ((BigDecimal) value).toBigInteger();
			}
			else if (value instanceof Double)
			{
				value = ((Double) value).intValue();
			}
			MaskFormatter f = new MaskFormatter(getPhoneMask());
			f.setValueContainsLiteralCharacters(false);
			return f.valueToString(value);
		}
		catch (Exception e)
		{
			log.error("Erro ao aplicar a máscara de telefone no valor!", e);
		}
		return "";
	}

	public static String getCPFMask()
	{
		return "###.###.###-##";
	}

	public static String getPhoneMask()
	{
		return "(##) ####-####";
	}

	public static String retornaDivisoes(EntityWrapper wrapper)
	{
		String ret = "";
		try
		{
			if (wrapper != null)
			{
				String divisao = NeoUtils.safeOutputString(wrapper.findValue("Divisao.Divisao"));
				return divisao;

			}
		}
		catch (Exception e)
		{
			log.error("Erro ao retornar as divisões!", e);
		}
		return ret;
	}

	private static Collection<FichaDatasource> populaGridFicha(EntityWrapper wrapper, Boolean imprimeFichaCompleta)
	{
		if (imprimeFichaCompleta)
		{
			return populaGridFichaTotal(wrapper);
		}
		else
		{
			return populaGridFichaEntregas(wrapper);
		}
	}

	@SuppressWarnings("unchecked")
	private static Collection<FichaDatasource> populaGridFichaEntregas(EntityWrapper wrapper)
	{
		FichaDatasource dsFicha = null;
		Collection<FichaDatasource> lista = null;
		try
		{
			lista = new ArrayList<FichaDatasource>();
			Collection<NeoObject> fichas = (Collection<NeoObject>) wrapper.findValue("listaEntregas");
			if (fichas != null && fichas.size() > 0)
			{
				// Percorre a grid
				for (NeoObject ficha : fichas)
				{
					if (ficha != null)
					{
						EntityWrapper wrapFicha = new EntityWrapper(ficha);
						if (wrapFicha != null)
						{
							// verifica se existe alguma entrega a ser impressa 
							if (wrapFicha.findValue("exibeRelatorio") != null && (Boolean) wrapFicha.findValue("exibeRelatorio") == true)
							{
								Collection<FichaDatasource> listaTmp = createFichaDSItem(ficha);
								if(listaTmp != null){
									lista.addAll(listaTmp);
								}
							}
						}
					}
				}
			}
			return lista;
		}
		catch (Exception e)
		{
			log.error("Erro ao popular a grid de fichas!", e);
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	private static Collection<FichaDatasource> populaGridFichaTotal(EntityWrapper wrp)
	{
		FichaDatasource dsFicha = null;
		Collection<FichaDatasource> lista = null;
		List<EntityWrapper> listaEpis = new ArrayList<EntityWrapper>();
		
		
		NeoObject colaborador = (NeoObject) wrp.findValue("colaboradorRH");
		EntityWrapper wCol = new EntityWrapper(colaborador);
		Long numcpf = (Long) wCol.findValue("numcpf");
		
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();

		try {
		    conn = PersistEngine.getConnection("");
		    
		    sql.append(" select epi.neoId from D_FCEFichaUniformeEpi epi ");
		    sql.append(" left join X_VETORHUSUFUNFUSION fun with(nolock) on fun.neoId = epi.colaboradorRH_neoId ");
		    sql.append(" left join [FSOODB04\\SQL02].VETORH.dbo.USU_V034FUSION v034 with(nolock) on v034.pk = fun.pk ");
		    sql.append(" where v034.numcpf = "+numcpf+" ");
		    pstm = conn.prepareStatement(sql.toString());
		    rs = pstm.executeQuery();

		    while (rs.next()) {
			Long neoId = rs.getLong("neoId");
			NeoObject epi = PersistEngine.getObject(AdapterUtils.getEntityClass("FCEFichaUniformeEpi"), new QLEqualsFilter("neoId",neoId));
			if (epi != null){
			    listaEpis.add(new EntityWrapper(epi));
			}
		    }

		} catch (SQLException e) {
		    e.printStackTrace();
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		try
		{
		    
			lista = new ArrayList<FichaDatasource>();
			for (EntityWrapper wrapper : listaEpis) {
			    Collection<NeoObject> fichas = (Collection<NeoObject>) wrapper.findValue("historicoEntregas");
			    if (fichas != null && fichas.size() > 0)
			    {
				// Percorre a grid
				for (NeoObject ficha : fichas)
				{
				    Collection<FichaDatasource> listaTmp = createFichaDSItem(ficha);
				    if(listaTmp != null){
					lista.addAll(listaTmp);
				    }
				}
			    }
			}
			return lista;
		}
		catch (Exception e)
		{
			log.error("Erro ao popular a grid de fichas!", e);
		}

		return null;
	}

	private static Collection<FichaDatasource> createFichaDSItem(NeoObject ficha)
	{
		FichaDatasource dsFicha = null;
		
		Collection<FichaDatasource> dataSource = new ArrayList<FichaDatasource>();

		if (ficha != null)
		{
			EntityWrapper wrapFicha = new EntityWrapper(ficha);
			if (wrapFicha != null)
			{
				// Busca os dados da entraga
				String produto = (String) wrapFicha.findValue("produto.despro");
				if (wrapFicha.findValue("derivacao") != null && produto != null)
				{
					produto += " " + wrapFicha.findValue("derivacao.codder");
				}
				//Long nrCertificado = (Long) wrapFicha.findValue("produto.usu_codcea");
				/*if(nrCertificado == null || nrCertificado.equalsIgnoreCase("null"))
				{
					nrCertificado = "";
				}*/
				
				/*if(nrCertificado == null || nrCertificado.equals(0))
				{
					nrCertificado = 0L;
				}*/
				
				String quantidadeEntregueStr = "";
				Long quantidadeEntregue = (Long) wrapFicha.findValue("quantidadeEntregue");
				if (quantidadeEntregue != null)
				{
					quantidadeEntregueStr = quantidadeEntregue.toString();
				}
				GregorianCalendar dataEntrega = (GregorianCalendar) wrapFicha.findValue("dataEntrega");
				String dataEntregaStr = NeoUtils.safeDateFormat(dataEntrega, "dd/MM/yyyy");
				String depositoEntrega = (String) wrapFicha.findValue("depositoEntrega.nomeDeposito");
				
				String certAprovacao = wrapFicha.findGenericValue("certificadoAprovacao.cerapr");

				// Busca as devolucoes desta entrega
				Collection<NeoObject> devolucoes = (Collection<NeoObject>) wrapFicha.findValue("listaDevolucoes");

				// caso hajam devolucoes repete os dados da entrega para cada devolucao da ficha
				if (devolucoes != null && devolucoes.size() > 0)
				{
					for (NeoObject devolucao : devolucoes)
					{
						EntityWrapper wrapDevolucao = new EntityWrapper(devolucao);

						dsFicha = new FichaDatasource();
						// seta os dados da entrega no datasource
						dsFicha.setProduto(produto);
						dsFicha.setQtdEntrega(quantidadeEntregueStr);
						dsFicha.setDataEntrega(dataEntregaStr);
						dsFicha.setDepositoEntrega(depositoEntrega);
						dsFicha.setNrCertificado(certAprovacao);
						dsFicha.setVistoEmpresa(""); // não utilizado

						// busca os dados da devolucao
						GregorianCalendar dataDevolucao = (GregorianCalendar) wrapDevolucao.findValue("DataDevolucao");
						String dataDevolucaoStr = NeoUtils.safeDateFormat(dataDevolucao, "dd/MM/yyyy");
						String depositoDevolucao = (String) wrapDevolucao.findValue("depositoDevolucao.nomeDeposito");
						String quantidadeDevolvidaStr = "";
						Long quantidadeDevolvida = (Long) wrapDevolucao.findValue("quantidadeDevolucao");
						if (quantidadeDevolvida != null)
						{
							quantidadeDevolvidaStr = quantidadeDevolvida.toString();
						}

						// seta os dados da devolucao no datasource
						dsFicha.setDataDevolucao(dataDevolucaoStr);
						dsFicha.setDepositoDevolucao(depositoDevolucao);
						dsFicha.setQtdDevolucao(quantidadeDevolvidaStr);
						dsFicha.setVistoColaborador(""); // não utilizado

						// adiciona a lista de intes a serem impressos
						dataSource.add(dsFicha);
					}
				}
				else
				{
					dsFicha = new FichaDatasource();

					// seta os dados da entrega no datasource
					dsFicha.setProduto(produto);
					dsFicha.setQtdEntrega(quantidadeEntregueStr);
					dsFicha.setDataEntrega(dataEntregaStr);
					dsFicha.setDepositoEntrega(depositoEntrega);
					dsFicha.setNrCertificado(certAprovacao);
					dsFicha.setVistoEmpresa(""); // não utilizado

					// seta os dados da devolucao em branco no datasource pois nào existem devolucoes
					dsFicha.setDataDevolucao("");
					dsFicha.setDepositoDevolucao("");
					dsFicha.setVistoColaborador(""); // não utilizado

					// adiciona a lista de intes a serem impressos
					dataSource.add(dsFicha);
				}
			}
		}

		return dataSource;
	}
}
