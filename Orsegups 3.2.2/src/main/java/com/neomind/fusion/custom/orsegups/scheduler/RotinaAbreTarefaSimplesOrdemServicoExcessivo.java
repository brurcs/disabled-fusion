package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaAbreTarefaSimplesOrdemServicoExcessivo implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesOrdemServicoExcessivo.class);

    @Override
    public void execute(CustomJobContext arg0) {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	String reg = "";
	Connection conn = null;
	StringBuilder sqlOrdemServico = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;
	log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço em Excesso - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	try {
	    // Ordens de Serviço Removido Regional Jaraguá do Sul (JGS) Ref
	    // tarefa 738353
	    sqlOrdemServico.append(" SELECT TOP(100) os.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3) AS sigla_regional, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, COUNT(*) AS QtdOS ");
	    sqlOrdemServico.append(" , GETDATE()-15 as PrimeiraData ,  GETDATE() as UltimaData ");
	    sqlOrdemServico.append(" FROM dbORDEM os WITH (NOLOCK)   ");
	    sqlOrdemServico.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE ");
	    sqlOrdemServico.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO ");
	    sqlOrdemServico.append(" INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
	    sqlOrdemServico.append(" WHERE os.FECHAMENTO BETWEEN GETDATE()-15 AND GETDATE() ");
	    // sqlOrdemServico.append(" WHERE os.FECHAMENTO BETWEEN DateAdd(mm, DateDiff(mm,0,GetDate()) - 1, 0) AND DateAdd(mm, DateDiff(mm,0,GetDate()), -1) ");
	    sqlOrdemServico.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ('IAI','BQE','BNU','JLE','LGS','CUA','GPR','SOO','CCO','RSL','CTA','TRO') ");
	    sqlOrdemServico.append(" AND def.IDOSDEFEITO NOT IN (122,123,125,134,140,166,178,184,1033) ");
	    sqlOrdemServico.append(" GROUP BY os.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3), c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO ");
	    sqlOrdemServico.append(" HAVING COUNT(*) >= 3 ");
	    sqlOrdemServico.append(" ORDER BY 2, 7 DESC ");
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sqlOrdemServico.toString());

	    rs = pstm.executeQuery();

	    String executor = "";
	    while (rs.next()) {
		String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteOrdemServicoExcessivo");
		String tituloAux = "Tratar OS em excesso - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]%";
		String titulo = "Tratar OS em excesso - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] "+rs.getString("RAZAO");

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 7L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		NeoPaper papel = new NeoPaper();
		Long codReg = OrsegupsUtils.getCodigoRegional(rs.getString("sigla_regional"));

		if (NeoUtils.safeIsNotNull(codReg) && codReg != 13L && codReg != 9L && codReg != 2L) {
		    papel = OrsegupsUtils.getPapelTratarOSExcesso(codReg);
		}

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
		    for (NeoUser user : papel.getUsers()) {
			executor = user.getCode();
			break;
		    }

		    String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
		    descricao += " <strong>Conta :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
		    descricao = descricao + " <strong>Período :</strong> de " + NeoDateUtils.safeDateFormat(rs.getDate("PrimeiraData"), "dd/MM/yyyy") + " <strong> à </strong> " + NeoDateUtils.safeDateFormat(rs.getDate("UltimaData"), "dd/MM/yyyy") + "<br>";
		    descricao = descricao + " <strong>Ordens de Serviço :</strong> " + rs.getString("QtdOS") + "<br>";

		    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		    String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		    System.out.println("##### EXECUTAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço em Excesso - Responsavel: " + executor + " Regional: " + reg + " Tarefa: " + tarefa);
		    log.warn("##### EXECUTAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço em Excesso - Responsavel: " + executor + " Regional: " + reg + " Tarefa: " + tarefa);
		} else {
		    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço em Excesso - Regra: Gerente responsável não encontrado - Regional " + rs.getString("sigla_regional"));
		    continue;
		}

	    }

	} catch (Exception e) {
	    log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples  Ordens de Serviço em Excesso - ERRO AO GERAR TAREFA - Regional: " + reg);
	    System.out.println("[" + key + "] ##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples  Ordens de Serviço em Excesso - ERRO AO GERAR TAREFA - Regional: " + reg);
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {

	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples  Ordens de Serviço em Excesso - ERRO DE EXECUÇÃO - Regional: " + reg);
		e.printStackTrace();
	    }

	    log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço em Excesso Regional: " + reg + " Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
    }

}
