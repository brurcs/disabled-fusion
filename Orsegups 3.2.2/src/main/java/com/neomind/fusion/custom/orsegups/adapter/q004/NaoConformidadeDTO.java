package com.neomind.fusion.custom.orsegups.adapter.q004;

public class NaoConformidadeDTO
{
	private String personCompanyName;
	private String workplaceName;
	private String alternativeName;
	private String observation;
	private String questionName;
	private String workplaceExternalId;
	public String getPersonCompanyName()
	{
		return personCompanyName;
	}
	public void setPersonCompanyName(String personCompanyName)
	{
		this.personCompanyName = personCompanyName;
	}
	public String getWorkplaceName()
	{
		return workplaceName;
	}
	public void setWorkplaceName(String workplaceName)
	{
		this.workplaceName = workplaceName;
	}
	public String getAlternativeName()
	{
		return alternativeName;
	}
	public void setAlternativeName(String alternativeName)
	{
		this.alternativeName = alternativeName;
	}
	public String getObservation()
	{
		return observation;
	}
	public void setObservation(String observation)
	{
		this.observation = observation;
	}
	public String getQuestionName()
	{
		return questionName;
	}
	public void setQuestionName(String questionName)
	{
		this.questionName = questionName;
	}
	public String getWorkplaceExternalId()
	{
		return workplaceExternalId;
	}
	public void setWorkplaceExternalId(String workplaceExternalId)
	{
		this.workplaceExternalId = workplaceExternalId;
	}

}
