package com.neomind.fusion.custom.orsegups.rsc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;


/**
 * 
 * @author herisson.ferreira
 *
 */
public class RSCPreencheIdCtr implements TaskFinishEventListener {
	
	public static String mensagem = "Erro ao preencher o ID do Contrato!";
	
	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
	
		EntityWrapper processEntity = event.getWrapper();
		
		preencheIdCtrSapiens(processEntity);
		
	}
	
	/**
	 * 
	 * @param processEntity
	 */
	public void preencheIdCtrSapiens(EntityWrapper processEntity) {
		
		String empresa = NeoUtils.safeOutputString(processEntity.findField("contratoCliente.usu_codemp").getValue());
		String filial = NeoUtils.safeOutputString(processEntity.findField("contratoCliente.usu_codfil").getValue());
		String contrato = NeoUtils.safeOutputString(processEntity.findField("contratoCliente.usu_numctr").getValue());
		
		try {
			
			String idCtrSapiens = retornaIdCtrSapiens(empresa, filial, contrato);
			
			if(NeoUtils.safeIsNotNull(idCtrSapiens)) {
				processEntity.setValue("idCtrSapiens", idCtrSapiens);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
	}
	
	/**
	 * 
	 * @param empresa
	 * @param filial
	 * @param contrato
	 * @return ID Contrato do Orsegups ID
	 */
	public String retornaIdCtrSapiens(String empresa, String filial, String contrato) {
		
		Connection conn = PersistEngine.getConnection("SAPIENS");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		String idCtrSapiens = null;
		
		try {
			
			sql.append(" SELECT c.usu_idctr FROM USU_T160CTR C ");
			sql.append(" WHERE c.usu_codemp = ? AND c.usu_codfil = ? AND c.usu_numctr = ? ");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setString(1, empresa);
			pstm.setString(2, filial);
			pstm.setString(3, contrato);

			rs = pstm.executeQuery();
			
			if(rs.next()) {
				idCtrSapiens = rs.getString("usu_idctr");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return idCtrSapiens;
	}
	
}
