package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RotinaLigacaoEventos implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaLigacaoEventos.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		this.log.warn("##### INICIAR ROTINA RotinaLigacaoEventos - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String returnFromAccess = "";
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		try
		{

			conn = PersistEngine.getConnection("SIGMA90");

			//QLGroupFilter groupFilter = new QLGroupFilter("AND");
			//			groupFilter.addFilter(new QLOpFilter("ramal", ">", 0L));
			List<NeoObject> listLigacaoRamal = null;
			Map<String, String> ligacaoMap = null;

			listLigacaoRamal = PersistEngine.getObjects(AdapterUtils.getEntityClass("CMRamais"));
			PersistEngine.getEntityManager().flush();

			if (NeoUtils.safeIsNotNull(listLigacaoRamal) && !listLigacaoRamal.isEmpty())
			{

				Calendar periodo = Calendar.getInstance();
				//METODO TEMPORARIO
				String periodoInicial = NeoUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");
				String periodoFinal = NeoUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");
				String periodoInicialStr = NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy");
				String periodoFinalStr = NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy");

				if (periodo.get(Calendar.HOUR_OF_DAY) >= 6 && periodo.get(Calendar.HOUR_OF_DAY) < 12)
				{
					periodoInicial += " 06:00:0.000";
					periodoFinal += " 11:59:0.000";
					periodoInicialStr += " 06:00";
					periodoFinalStr += " 11:59";
				}
				else if (periodo.get(Calendar.HOUR_OF_DAY) >= 12 && periodo.get(Calendar.HOUR_OF_DAY) < 18)
				{
					periodoInicial += " 12:00:0.000";
					periodoFinal += " 17:59:0.000";
					periodoInicialStr += " 12:00";
					periodoFinalStr += " 17:59";
				}
				else if (periodo.get(Calendar.HOUR_OF_DAY) >= 18 && periodo.get(Calendar.HOUR_OF_DAY) < 24)
				{
					periodoInicial += " 18:00:0.000";
					periodoFinal += " 23:59:0.000";
					periodoInicialStr += " 18:00";
					periodoFinalStr += " 23:59";
				}
				else if (periodo.get(Calendar.HOUR_OF_DAY) >= 0 && periodo.get(Calendar.HOUR_OF_DAY) < 6)
				{
					periodoInicial += " 00:00:0.000";
					periodoFinal += " 05:59:0.000";
					periodoInicialStr += " 00:00";
					periodoFinalStr += " 05:59";
				}
				String ramal = null;
				String soma = null;
				ligacaoMap = new HashMap<String, String>();
				Integer totalizador = 0;
				for (NeoObject neoObject : listLigacaoRamal)
				{

					NeoObject neoOts = (NeoObject) neoObject;
					EntityWrapper ewOts = new EntityWrapper(neoOts);
					ramal = (String) ewOts.findField("ramal").getValue();

					StringBuilder sql = new StringBuilder();
					sql.append(" SELECT DISTINCT COUNT (*) AS SOMA ");
					sql.append(" FROM VIEW_HISTORICO H WITH (NOLOCK)    LEFT JOIN HISTORICO_FRASE_EVENTO HF WITH (NOLOCK)  ON HF.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
					sql.append(" WHERE  H.DT_RECEBIDO BETWEEN '" + periodoInicial + "' AND '" + periodoFinal + "'   ");
					sql.append(" AND (H.CD_EVENTO LIKE 'X406' OR H.CD_EVENTO LIKE 'E401' OR H.CD_EVENTO LIKE 'XXX2' OR H.CD_EVENTO LIKE 'XXX5') AND H.TX_OBSERVACAO_FECHAMENTO LIKE '%" + ramal + "##%' ");

					pstm = conn.prepareStatement(sql.toString());
					rs = pstm.executeQuery();
					DecimalFormat decimalFormat = new DecimalFormat("000");
					while (rs.next())
					{
						soma = rs.getString("SOMA");
						soma = decimalFormat.format(Double.parseDouble(soma));
						ligacaoMap.put(soma, ramal);
						totalizador += Integer.parseInt(soma);
					}
				}

				if (NeoUtils.safeIsNotNull(soma))
				{
					returnFromAccess += "<strong> Relação de chamadas no período, de: </strong>" + periodoInicialStr + " à " + periodoFinalStr + " <br>";
					returnFromAccess += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";

					Map<String, String> map = new TreeMap<String, String>(ligacaoMap);

					for (Map.Entry entry : map.entrySet())
					{
						String chave = (String) entry.getKey();
						String valor = (String) entry.getValue();
						returnFromAccess += "<strong> Ramal:  </strong>" + valor + " <br>";
						returnFromAccess += "<strong> Total:  </strong>" + chave + " <br>";
						returnFromAccess += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";
					}
					returnFromAccess += "<strong> Total Geral:  </strong>" + String.valueOf(totalizador) + " <br>";
					returnFromAccess += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";
					enviaEmailRotina(returnFromAccess);
				}

			}
			this.log.warn("##### EXECUTAR RotinaLigacaoEventos - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
		
			this.log.error("##### ERRO RotinaLigacaoEventos - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("["+key+"] ##### ERRO RotinaLigacaoEventos - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				this.log.error("##### ERRO RotinaLigacaoEventos - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			this.log.warn("##### FINALIZAR RotinaLigacaoEventos - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	private void enviaEmailRotina(String returnFromAccess)
	{

		try
		{

			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			mailClone.setFromEMail("fusion@orsegups.com.br");
			mailClone.setFromName("Orsegups Partipações S.A.");
			if (mailClone != null)
			{

				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();

				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) < 12)
				{
					saudacaoEMail = "Bom dia, ";
				}
				else if (saudacao.get(Calendar.HOUR_OF_DAY) >= 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
				{
					saudacaoEMail = "Boa tarde, ";
				}
				else
				{
					saudacaoEMail = "Boa noite, ";
				}

				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("edson.heinz@orsegups.com.br");
				noUserEmail.addTo("emailautomatico@orsegups.com.br");

				noUserEmail.setSubject("Não Responda - E-mail com a quantidade de ligações para eventos E401, X406, XXX2 E XXX5. ");

				noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				noUserMsg.append("            </td>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail + " este e-mail se refere a quantidade de ligações entre eventos e operador/ramal.</strong><br>");
				noUserMsg.append(returnFromAccess);
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");
				noUserEmail.setHtmlMsg(noUserMsg.toString());
				noUserEmail.setContent(noUserMsg.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			this.log.error("##### ERRO RotinaLigacaoEventos - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}

	}

}
