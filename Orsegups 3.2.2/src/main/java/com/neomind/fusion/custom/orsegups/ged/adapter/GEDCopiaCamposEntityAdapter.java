package com.neomind.fusion.custom.orsegups.ged.adapter;

import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.entity.EntityAdapter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class GEDCopiaCamposEntityAdapter implements EntityAdapter
{
	@Override
	public void run(Map<String, Object> params)
	{
		try
		{
			EForm eform = (EForm) params.get(EFORM);

			EntityWrapper wrapper = new EntityWrapper(eform.getObject());

			NeoObject colaborador = (NeoObject) wrapper.findValue("colaboradorExt");
			
			if (colaborador != null)
			{
				EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
				
				QLEqualsFilter empresaFilter = new QLEqualsFilter("codigo", (Long) colaboradorWrapper.findValue("numemp"));
				NeoObject empresaObj = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GCEmpresa"), empresaFilter);
				
				wrapper.setValue("nomeColaborador", colaboradorWrapper.findValue("nomfun"));
				wrapper.setValue("matricula", colaboradorWrapper.findValue("numcad"));
				wrapper.setValue("numeroCPF", colaboradorWrapper.findValue("numcpf"));
				wrapper.setValue("empresa", empresaObj);
			}
			
			String titulo = "";

			if (wrapper.findValue("empresa.nomeEmpresa") != null)
			{
				titulo += wrapper.findValue("empresa.nomeEmpresa");
				titulo += " - ";
			}
			if (wrapper.findValue("matricula") != null)
			{
				titulo += wrapper.findValue("matricula");
				titulo += " - ";
			}
			if (wrapper.findValue("nomeColaborador") != null)
			{
				titulo += wrapper.findValue("nomeColaborador");
			}

			wrapper.setValue("title", titulo);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
