package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;


public class RotinaEnviaEmailTarefasAEscalarTime implements CustomJobAdapter
{
 
    @Override
    public void execute(CustomJobContext ctx)
    {
	System.out.println("Inicio para gerar relatorio RotinaEnviaEmailTarefasAEscalarTime");
	processaJob(ctx);
	
    }


    public static void processaJob(CustomJobContext ctx){
    	
		String descricao = "";
		String email = "";
		StringBuilder html = new StringBuilder();

		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		try {
			
			html.append("\n <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> ");
			html.append("\n <html> ");
			html.append("\n 		<head> ");
			html.append("\n 			<meta charset=\"ISO-8859-1\"> ");
			html.append("\n 			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");
			html.append("\n 		</head> ");
			html.append("\n 		<body yahoo=\"yahoo\"> ");
			
			html.append("\n 		Prezado(a), ");
			html.append("\n 		<br> ");
			html.append("\n 		Segue abaixo listagem de tarefas com data de ESCALADA hoje: ");
			html.append("\n 		<br> ");
			html.append("\n 		<br> ");
			
			html.append("\n 			<table width=\"100%\"cellspacing=\"0\" cellpadding=\"2\" border=\"1\"> ");			
			html.append("\n 					<tr> ");
			html.append("\n 						<td align=\"center\"> ");
			html.append("\n 							<strong>Colaborador</strong> ");
			html.append("\n 						</td> ");
			html.append("\n 						<td align=\"center\"> ");
			html.append("\n 							<strong>Codigo Tarefa</strong> ");
			html.append("\n 						</td> ");
			html.append("\n 						<td align=\"center\"> ");
			html.append("\n 							<strong>Já Respondido</strong> ");
			html.append("\n 						</td> ");
			html.append("\n 					</tr>");
			
			StringBuffer  varname1 = new StringBuffer();
			
			varname1.append("select tabela2.nomeColaborador, ");
			varname1.append("       tabela2.email, ");
			varname1.append("       tabela2.codTarefa, ");
			varname1.append("       tabela2.jaRespondida, ");
			varname1.append("       (select max(ti.id) from TaskInstance ti where ti.processId = tabela2.proNeoId) as taski ");
			varname1.append("  from ( ");
			varname1.append("  SELECT tabela1.nomeColaborador, ");
			varname1.append("         uu.email, ");
			varname1.append("         p.code as codTarefa, ");
			varname1.append("         p.neoId as proNeoId, ");
			varname1.append("         case when ");
			varname1.append("         	(select MAX(tr.responsavel_neoId) ");
			varname1.append("            from D_Tarefa_registroAtividades ra ");
			varname1.append("      inner join D_TarefaRegistroAtividades tr on ra.registroAtividades_neoId = tr.neoId ");
			varname1.append("           where D_Tarefa_neoId = t.neoId) = tabela1.neoIdColaborador ");
			varname1.append("         then 'NAO' else 'SIM' end as jaRespondida ");
			varname1.append("           ");
			varname1.append("  FROM ( ");
			varname1.append("         select u.neoId as neoIdColaborador, ");
			varname1.append("                u.fullName as nomeColaborador, ");
			varname1.append("                u.email as emailColaborador, ");
			varname1.append("				su.code as loginColaborador, ");
			varname1.append("                sg.neoId as neoIdGrupo, ");
			varname1.append("                sg.code as codeGrupo, ");
			varname1.append("                sg.name as nomeGrupo, ");
			varname1.append("                sp.neoId as neoIdPapel, ");
			varname1.append("                sp.code as codePapel, ");
			varname1.append("                sp.name as nomePapel, ");
			varname1.append("                (select neoId from NeoUser sup WITH (NOLOCK) where sup.neoId = npu.users_neoId) as neoIdSupervisor ");
			varname1.append("           from NeoUser u ");
			varname1.append("     inner join SecurityEntity su WITH (NOLOCK) on u.neoId = su.neoId ");
			varname1.append("     inner join NeoGroup g WITH (NOLOCK) on u.group_neoId = g.neoId ");
			varname1.append("     inner join SecurityEntity sg WITH (NOLOCK) on g.neoId = sg.neoId ");
			varname1.append("     inner join NeoPaper papel WITH (NOLOCK) on g.responsible_neoId = papel.neoId ");
			varname1.append("     inner join SecurityEntity sp WITH (NOLOCK) on papel.neoId = sp.neoId ");
			varname1.append("     inner join NeoPaper_users npu WITH (NOLOCK) on papel.neoId = npu.papers_neoId ");
			varname1.append("		  where u.group_neoId = 1050786227 ");
			varname1.append("	  ) as tabela1 ");
			varname1.append("      inner join NeoUser uu WITH (NOLOCK) on uu.neoId = tabela1.neoIdSupervisor ");
			varname1.append("      inner join d_tarefa t WITH (NOLOCK) on t.executor_neoId = tabela1.neoIdColaborador ");
			varname1.append("      inner join WFProcess p WITH (NOLOCK) on t.wfprocess_neoId = p.neoId ");
			varname1.append("      where DATEDIFF(HOUR,getDate(),t.prazo) < 24 ");
			varname1.append("        and DATEDIFF(HOUR,getDate(),t.prazo) > 0 ");
			varname1.append("        and p.processState = 0 ");
			varname1.append("   ) as tabela2 ");
			varname1.append("  order by tabela2.nomeColaborador");

			pstm = conn.prepareStatement(varname1.toString());
			rs = pstm.executeQuery();
			
			while (rs.next()) {
				
				html.append("\n 					<tr> ");
				html.append("\n 						<td> ");
				html.append("\n 							"+rs.getString("nomeColaborador"));
				html.append("\n 						</td> ");
				html.append("\n 						<td><a href=\"https://intranet.orsegups.com.br/fusion/portal/render/Task?type=Task&showSummaryBtn=true&taskInstanceId="+rs.getInt("taski")+"&edit=true&readOnly=false&boxname=inbox&forcedBreadcrumb=true&from=3\"> ");
				html.append("\n 							"+rs.getInt("codTarefa"));
				html.append("\n 						</a></td> ");
				html.append("\n 						<td> ");
				html.append("\n 							"+rs.getString("jaRespondida")); 
				html.append("\n 						</td> ");
				html.append("\n 					</tr>");
								
				html.append(descricao);
				email = rs.getString("email");
			}
			
			html.append("\n 			</table> ");
			html.append("\n 		</body> ");
			html.append("\n </html> ");
			
			OrsegupsUtils.sendEmail2Orsegups("rodrigo.gregorio@orsegups.com.br", "fusion@orsegups.com.br", "Acompanhamento de Tarefas FUSION a ESCALAR", "", html.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
    
    }

}