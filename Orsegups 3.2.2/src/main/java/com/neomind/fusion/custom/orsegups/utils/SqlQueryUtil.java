package com.neomind.fusion.custom.orsegups.utils;

import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class SqlQueryUtil {
	
	private String encoding;

	public SqlQueryUtil()
	{
		this.encoding = "ISO-8859-1";
	}

	public SqlQueryUtil(String encoding)
	{
		this.encoding = encoding;
	}
	
	public String getQuery(String vmFile)
	{
		return getQuery(vmFile, new HashMap<String, Object>());
	}

	public String getQuery(String vmFile, Map<String, Object> params)
	{
		String path = this.getClass().getResource("/scripts").getPath(); // resources/scripts
		path = path.substring(1);
		return getQuery(vmFile, params, path, false);
	}

	public String getQuery(String vmFile, Map<String, Object> params, String ressourcePath,
			Boolean superTrim)
	{
		String sql = "";

		try
		{
			VelocityContext context = new VelocityContext();
			if (params != null)
			{
				for (Map.Entry<String, Object> param : params.entrySet())
				{
					context.put(param.getKey(), param.getValue());
				}
			}

			VelocityEngine ve = new VelocityEngine();
			Properties p = new Properties();
			p.setProperty("file.resource.loader.path", ressourcePath);
			ve.init(p);
			StringWriter w = new StringWriter();
			ve.mergeTemplate(vmFile, this.encoding, context, w);
			String body = w.toString();

			if (superTrim)
			{
				body = body.replaceAll("[ \\t\\r]*\\n+[ \\t\\r]*", Matcher.quoteReplacement(""));
				body = body.trim();
			}

			sql = body;
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sql;
	}
}