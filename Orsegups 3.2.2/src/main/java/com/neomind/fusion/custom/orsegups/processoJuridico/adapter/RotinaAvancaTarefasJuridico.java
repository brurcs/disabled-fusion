package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.RotinaAvancaTarefasJuridico
public class RotinaAvancaTarefasJuridico implements CustomJobAdapter
{

    private static final String ATIVIDADE_CHECK_POINT_J002 = "Aguardar Finalização das tarefas";

    @Override
    @SuppressWarnings("unchecked")
    public void execute(CustomJobContext ctx)
    {
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try
	{
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;
	    StringBuilder sql = new StringBuilder();
	    String neoIds = "";
	    boolean tarefaFinalizada = true;
	    conn = PersistEngine.getConnection("");

	    sql.append(" SELECT w.neoId ");
	    sql.append(" FROM WFProcess w ");
	    sql.append(" inner join ProcessModel p on p.neoId = w.model_neoId ");
	    sql.append(" inner join TaskInstance t on t.processId = w.neoId ");
	    sql.append(" inner join D_j002Principal i on i.wfprocess_neoId = w.neoId ");
	    sql.append(" where (p.title like '%j002%' or p.title like '%j005%') and t.status = 2 and t.taskName like 'Aguardar Finalização das tarefas%' ");

	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();

	    while (rs.next())
	    {
		if (neoIds.equals(""))
		{
		    neoIds += " neoId = " + rs.getLong("neoId");
		}
		else
		{
		    neoIds += " or neoId = " + rs.getLong("neoId");
		}
	    }

	    QLGroupFilter groupFilter = new QLGroupFilter("AND");
	    QLRawFilter qlSql = new QLRawFilter(neoIds);
	    if (neoIds != null && !neoIds.equals("")) {
		groupFilter.addFilter(qlSql);

		List<WFProcess> processos = PersistEngine.getObjects(WFProcess.class, groupFilter, "code DESC");
		for (WFProcess processo : processos)
		{
		    tarefaFinalizada = true;

		    Collection<Activity> atividadesAbertas = processo.getOpenActivities();

		    if (atividadesAbertas != null && atividadesAbertas.size() > 0)
		    {
			for (Activity act : atividadesAbertas)
			{
			    if (act.getActivityName().equals(ATIVIDADE_CHECK_POINT_J002))
			    {
				System.out.println("[" + key + "] Processo: " + processo.getCode() + ", Atividade:" + act.getActivityName());

				EntityWrapper w = new EntityWrapper(act.getProcess().getEntity());
				Task task = null;

				List<NeoObject> pedidos = null;
				if (String.valueOf(w.getValue("grauProcesso")).contains("1º"))
				{

				    pedidos = (List<NeoObject>) w.getValue("lisPed");
				}
				else if (String.valueOf(w.getValue("grauProcesso")).contains("2º"))
				{

				    pedidos = (List<NeoObject>) w.getValue("lisPedSegGrau");
				}
				else
				{
				    pedidos = (List<NeoObject>) w.getValue("lisPedSegGrau");
				}

				if (pedidos != null && pedidos.size() > 0)
				{
				    if (!tarefaFinalizada)
				    {
					break;
				    }
				    for (NeoObject pedido : pedidos)
				    {
					EntityWrapper wPedido = new EntityWrapper(pedido);
					List<NeoObject> tarefas = (List<NeoObject>) wPedido.findValue("lisTar");
					if (!tarefaFinalizada)
					{
					    break;
					}
					if (tarefas != null && tarefas.size() > 0)
					{
					    for (NeoObject tarefa : tarefas)
					    {
						EntityWrapper wTarefa = new EntityWrapper(tarefa);
						Long neoId = (Long) wTarefa.findValue("neoIdTarefa");
						tarefaFinalizada = verificaTarefasAbertas(neoId);//Metodo verificarTarefaFinalizada.
						if (!tarefaFinalizada)
						{
						    break;
						}
					    }
					}
				    }
				}
				System.out.println("Tarefa Finalizada " + tarefaFinalizada);
				if (tarefaFinalizada)
				{ // se a data de envio da reinspeção for != null, avança a atividade
				    task = ((UserActivity) act).getTaskList().get(0);
				    OrsegupsWorkflowHelper.finishTaskByActivity(act);
				    System.out.println("[" + key + "] Processo: " + processo.getCode() + ", Tarefa do fluxo Juridico avançada com sucesso");

				}
				else
				{
				    tarefaFinalizada = true;
				}
			    }
			}
		    }
		}
	    }
	}
	catch (Exception e)
	{
	    System.err.println("[" + key + "] - Erro ao avançar atividade de J002");
	    e.printStackTrace();
	    throw new JobException("Erro ao avançar tarefa do processo J002. Procure no log por [" + key + "]");
	}

    }

    public static boolean verificaTarefasAbertas(Long neoId)
    {

	QLEqualsFilter neoIdTarefa = new QLEqualsFilter("neoId", neoId);
	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(neoIdTarefa);
	groupFilter.addFilter(new QLEqualsFilter("processState", ProcessState.running.ordinal()));
	groupFilter.addFilter(new QLEqualsFilter("saved", Boolean.TRUE));

	List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("wfProcess"), groupFilter);
	if (objs != null && objs.size() > 0)
	{
	    return false;
	}
	else
	{
	    return true;
	}
    }
}
