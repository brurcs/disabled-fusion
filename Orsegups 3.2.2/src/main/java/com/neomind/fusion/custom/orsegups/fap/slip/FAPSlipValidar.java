package com.neomind.fusion.custom.orsegups.fap.slip;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.job.FAPSlipOrdemCompraJob;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.WorkflowException;

import br.com.senior.services.fap.G5SeniorServicesLocator;
import br.com.senior.services.fap.OrdemcompraaprovarIn;
import br.com.senior.services.fap.OrdemcompraaprovarInOrdemCompra;
import br.com.senior.services.fap.OrdemcompraaprovarOut;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4In;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4OutOrdemCompra;
import br.com.senior.services.fap.OrdemcomprareprovarIn;
import br.com.senior.services.fap.OrdemcomprareprovarInOrdemCompra;
import br.com.senior.services.fap.OrdemcomprareprovarOut;
import br.com.senior.services.vetorh.constants.WSConfigs;

public class FAPSlipValidar implements TaskFinishEventListener, AdapterInterface, ActivityStartEventListener
{
	Map<String, String> parametros = new HashMap<String, String>();
	Task origin = null;
	
	@Override
	public void onStart(ActivityEvent event)
	{
		String atividade = event.getActivity().getName();
		EntityWrapper wrapper = event.getWrapper();
		
		try
		{
			switch (atividade)
			{
				case "Inserir Dados Pagamento":
					wrapper.setValue("aprovarPagamento", null);
					wrapper.setValue("aprovacaoControladoria", null);
					wrapper.setValue("aprovacaoPresidente", null);
					wrapper.setValue("aprovarOrcamento", null);
					break;
				case "Cadastrar Contrato":
					wrapper.setValue("aprovarPagamento", null);
					wrapper.setValue("aprovacaoControladoria", null);
					wrapper.setValue("aprovacaoPresidente", null);
					break;
				case "Editar Contrato":
					wrapper.setValue("aprovarPagamento", null);
					wrapper.setValue("aprovacaoControladoria", null);
					wrapper.setValue("aprovacaoPresidente", null);
					break;
				case "Aprovar Pagamento":
					wrapper.setValue("aprovarPagamento", null);
					wrapper.setValue("exigirAprovacao", null);
					break;
				case "Aprovar Pagamento - Diretoria Administrativa":
					wrapper.setValue("aprovarPagamento", null);
					break;
				case "Aprovar Compra":
				case "Pré Aprovar Compra":
					wrapper.setValue("aprovarPagamento", null);
					//registrarHistorico(wrapper, "Sistema: Aprovar Ordem de Compra", "Processo iniciado para Aprovação de Compra");
					break;
				case "Reavaliar Ordem Compra":
				case "Reavaliar Ordem Compra - Almoxarifado":
					wrapper.setValue("reavaliarOrdemCompra", null);
					wrapper.setValue("aprovarPagamento", null);
					
					break;
				case "Validar Pagamento":
				case "Validar Pagamento - Gerência":
					wrapper.setValue("aprovarPagamento", null);
					wrapper.setValue("aprovacaoControladoria", true);
					break;
				case "Aprovar Contrato - Diretoria":
					wrapper.setValue("aprovarPagamento", null);
					break;
				/*case "Validar Contrato":
					wrapper.setValue("aprovacaoControladoria", null);
					break;
				
				case "Aprovar Contrato - Presidente":
					wrapper.setValue("aprovacaoPresidente", null);
					break;*/
					
				default:
					break;
			}
		}
		catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException(e.getErrorList().get(0).getMessage().getMessage());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}
	
	@Override
	public void onFinish(TaskFinishEvent event)
	{
		String atividade = event.getActivity().getName();
		EntityWrapper wrapper = event.getWrapper();
		origin = event.getTask();
		
		List<NeoObject> listaAprovadores = new ArrayList<>();
		
		String codTarefa = event.getProcess().getCode();
		String operacao = wrapper.findGenericValue("operacao.descricao");
		Long tipoPagamento = wrapper.findGenericValue("tipoPagamento.codigo");
		Long codModalidadeContrato = wrapper.findGenericValue("modalidadeContrato.codigo");
		Long codOperacao = wrapper.findGenericValue("operacao.codigo");
		Long codOrdemCompra = wrapper.findGenericValue("ordemCompra.ordemCompra");
		Long tipoLancamento = wrapper.findGenericValue("lancarNotaOuTitulo.codigoLancamento");
		Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
		Long codModalidadeContratoCadastrado = wrapper.findGenericValue("novoContrato.modalidadeContrato.codigo");
		
		String descricaoHistorico = wrapper.findGenericValue("observacoes");
		
		Boolean exigirAprovacao = wrapper.findGenericValue("exigirAprovacao");
		Boolean pgtoParcelado = wrapper.findGenericValue("pgtoParcelado");
		
		Boolean aprovacaoControladoria = wrapper.findGenericValue("aprovacaoControladoria");
		Boolean aprovacaoDiretoria = wrapper.findGenericValue("aprovarPagamento");
		Boolean aprovacaoPresidencia = wrapper.findGenericValue("aprovacaoPresidente");
		Boolean aprovacaoOrcamento = wrapper.findGenericValue("aprovarOrcamento");
		Boolean reavaliarOrdemCompra = wrapper.findGenericValue("reavaliarOrdemCompra");
		
		Boolean confirmaPgamento = wrapper.findGenericValue("confirmarPagamento");
		confirmaPgamento = confirmaPgamento == null ? false : confirmaPgamento;
		
		List<NeoObject> listAprovadoresContrato = wrapper.findGenericValue("dadosContrato.aprovadores");
		NeoUser nuResponsavelContrato = (NeoUser) wrapper.findGenericValue("novoContrato.responsavelContrato");
		
		List<NeoObject> listNovoOrcamento = wrapper.findGenericValue("novoContrato.novoOrcamento");
		
		WSConfigs config = new WSConfigs();
		G5SeniorServicesLocator locator = new G5SeniorServicesLocator();
		
		OrdemcomprabuscarPendentes4In buscaPendentesIn = new OrdemcomprabuscarPendentes4In();
		buscaPendentesIn.setCodUsu(50);
		
		try
		{
			switch (atividade)
			{
				case "Selecionar Operação":
				{
					descricaoHistorico = "Processo Iniciado. Operação: " + operacao;
					
					wrapper.setValue("solicitante", PortalUtil.getCurrentUser());
					wrapper.setValue("confirmarPagamento", true);
					
					if (codOperacao != null)
					{
						if (codOperacao == 1l)
						{
							NeoPaper npControladoria = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "FAPSlipControladoria"));
							wrapper.setValue("responsavelControladoria", npControladoria);
						}
						else
						{
							NeoPaper npControladoria = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "FapSlipControladoriaContratos"));
							wrapper.setValue("responsavelControladoria", npControladoria);
						}
					}
					
					if (codOperacao == 1l && tipoPagamento != null)
					{
						NeoObject noParametrizacao = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipParametrizacao"), new QLEqualsFilter("parametro", "enviarDiretoriaAdm"));
						EntityWrapper wParametrizacao = new EntityWrapper(noParametrizacao);
						String valor = wParametrizacao.findGenericValue("valor");
						if (valor.equals("1"))
							wrapper.setValue("enviarDiretoriaAdm", true);
						else
							wrapper.setValue("enviarDiretoriaAdm", false);
						
						if (tipoPagamento == 1l)
						{
							FAPSlipPagamento pagamento = new FAPSlipPagamento(wrapper, origin);
							pagamento.iniciarPagamentoContrato();
						}
						else if (tipoPagamento == 2l)
						{
							wrapper.setValue("tituloTarefa", "Avulso");
							wrapper.setValue("dadosContrato", null);
							
							descricaoHistorico = "Processo Iniciado. Operação: " + operacao;
							FAPSlipUtils.registrarHistorico(wrapper, origin.getActivityName(), descricaoHistorico, origin.returnResponsible());
						}
						else if (tipoPagamento == 3l)
						{
							Long codOrdemCompraManual = wrapper.findGenericValue("codOrdemCompra");
							Boolean ordemCompraValida = false;
							OrdemcomprabuscarPendentes4OutOrdemCompra ordemCompra = null;
							
							OrdemcomprabuscarPendentes4Out buscaPendentesOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().buscarPendentes_4(config.getUserService(), config.getPasswordService(), 0, buscaPendentesIn);
							if (buscaPendentesOut.getErroExecucao() == null || buscaPendentesOut.getErroExecucao().trim().equals(""))
							{
								OrdemcomprabuscarPendentes4OutOrdemCompra[] solicitacoes = buscaPendentesOut.getOrdemCompra();
								for (OrdemcomprabuscarPendentes4OutOrdemCompra solicitacao : solicitacoes)
								{
									Long codOrdemCompraWS = solicitacao.getNumero().longValue();
									if (codOrdemCompraManual.equals(codOrdemCompraWS) && (solicitacao.getCodigoEmpresa().equals(1) || solicitacao.getCodigoEmpresa().equals(30)))
									{
										QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("ordemCompra.ordemCompra", codOrdemCompra), new QLEqualsFilter("ordemCompra.empresa.codemp", solicitacao.getCodigoEmpresa().longValue()));
										List<NeoObject> listOrdensCompraCadastradas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup, "neoId desc");
										if (!listOrdensCompraCadastradas.isEmpty())
										{
											for (NeoObject noOrdemCompraCadastrada : listOrdensCompraCadastradas)
											{
												EntityWrapper wOrdemCompraCadastrada = new EntityWrapper(noOrdemCompraCadastrada);
												WFProcess process = wOrdemCompraCadastrada.findGenericValue("wfprocess");
												if (process != null && (!process.getProcessState().equals(ProcessState.canceled) || process.isOpen()))
												{
													throw new WorkflowException("A ordem de compra " + codOrdemCompraManual + " já foi iniciada na tarefa " + process.getCode());
												}
												else 
												{
													ordemCompraValida = true;
													ordemCompra = solicitacao;
												}
											}
										}
										else
										{
											ordemCompraValida = true;
											ordemCompra = solicitacao;
										}
										break;
									}
								}
							}
							 
							if (ordemCompraValida)
							{
								FAPSlipOrdemCompraJob job = new FAPSlipOrdemCompraJob();
								
								NeoObject noOrdemCompra = job.preencherOrdemCompra(codOrdemCompraManual, null, ordemCompra.getCodigoEmpresa().longValue(), false);
								if (noOrdemCompra == null) 
								{
									throw new WorkflowException("Não foi encontrada nenhuma Ordem de Compra com o código informado.");
								}
								EntityWrapper wOrdemCompra = new EntityWrapper(noOrdemCompra);
								
								String solicitante = wOrdemCompra.findGenericValue("solicitante");
								NeoUser nuSolicitante = PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", solicitante));
								if (nuSolicitante == null)
								{
									NeoPaper npReavaliadorOC = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "FAPSlipReavaliadoresOrdemCompra"));
									for (NeoUser nuReavaliador : npReavaliadorOC.getAllUsers())
									{
										nuSolicitante = nuReavaliador;
									}
								}
								
								NeoObject noTipoPagamento = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipTipoPagamento"), new QLEqualsFilter("codigo", 3l));
								NeoObject noEmpresa = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("EEMP"), new QLEqualsFilter("codemp", 1l));
								
								String fornecedor = wOrdemCompra.findGenericValue("fornecedor.apefor");
								Double valorOC = ordemCompra.getValorAproximado();
								
								NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
								
								String tituloTarefa = "Cód: " + codOrdemCompra;
								tituloTarefa += " - " + monetaryFormat.format(valorOC) + " - Empresa: " + new EntityWrapper(noEmpresa).findGenericValue("codemp") + " - " + fornecedor;
								
								wrapper.setValue("tipoPagamento", noTipoPagamento);
								wrapper.setValue("fornecedor", new EntityWrapper(noOrdemCompra).findGenericValue("fornecedor"));
								wrapper.setValue("empresa", noEmpresa);
								wrapper.setValue("solicitante", nuSolicitante);
								wrapper.setValue("ordemCompra", noOrdemCompra);
								wrapper.setValue("codOrdemCompra", codOrdemCompra);
								wrapper.setValue("valorPagamento", BigDecimal.valueOf(valorOC));
								wrapper.setValue("tituloTarefa", tituloTarefa);
							}
							else
							{
								throw new WorkflowException("Esta Ordem de Compra não está disponível para aprovação.");
							}
						}
					} 
					else if (codOperacao == 2l)
					{
						FAPSlipContrato contrato = new FAPSlipContrato(wrapper, origin);
						contrato.iniciarEdicaoContrato();
					} 
					else if (codOperacao == 3l)
					{
						FAPSlipUtils.registrarHistorico(wrapper, origin.getActivityName(), descricaoHistorico, origin.returnResponsible());
					}
					
					preencherTarefaSimples(wrapper, codTarefa);
				}
				break;
				case "Inserir Dados Pagamento":
				{
					FAPSlipPagamento pagamento = new FAPSlipPagamento(wrapper, origin);
					pagamento.inserirDadosPagamento();
				}
				break;
				case "Aprovar Pagamento":
				{
					FAPSlipPagamento pagamento = new FAPSlipPagamento(wrapper, origin);
					pagamento.aprovarPagamento();
				}
				break;
				case "Aprovar Pagamento - Orçamento":
					if (aprovacaoOrcamento)
					{
						if (descricaoHistorico.equals(""))
							descricaoHistorico = "Mensagem Automática: Orçamento Aprovado";
					}
					break;
				case "Validar Pagamento":	
				{
					FAPSlipPagamento pagamento = new FAPSlipPagamento(wrapper, origin);
					pagamento.validarPagamento("");
				}
				break;
				case "Validar Pagamento - Gerência":					
				{
					FAPSlipPagamento pagamento = new FAPSlipPagamento(wrapper, origin);
					pagamento.validarPagamento("Gerência");
				}
				break;
				case "Aprovar Pagamento - Diretoria Administrativa":
					if (aprovacaoDiretoria)
					{						
						if (descricaoHistorico.equals(""))
							descricaoHistorico = "Mensagem Automática: Pagamento Aprovado";
					}
					
					if (origin.returnResponsible().contains("em nome de Cristine Fontoura Berger") && !origin.returnResponsible().contains("Dilmo"))
						wrapper.setValue("tomarCienciaPresidente", true);
					
					FAPSlipUtils.registrarHistorico(wrapper, origin.getActivityName(), descricaoHistorico, origin.returnResponsible());
					break;
				case "Executar GTC/GLN":
					wrapper.setValue("executorControladoria", PortalUtil.getCurrentUser());
					wrapper.setValue("enviadoGTCGLN", true);
					
					if (tipoLancamento == 2l)
						FAPSlipUtils.preencherGTC(wrapper);
					
					FAPSlipUtils.registrarHistorico(wrapper, origin.getActivityName(), descricaoHistorico, origin.returnResponsible());
					break;
				case "Cadastrar Contrato": 
				{
					FAPSlipContrato contrato = new FAPSlipContrato(wrapper, origin);
					contrato.cadastrarContrato(new Long(codTarefa));
				}
				break;
				case "Editar Contrato":
				{
					FAPSlipContrato contrato = new FAPSlipContrato(wrapper, origin);
					contrato.editarContrato();
				}	
				break;
				case "Aprovar Contrato - Gerência":
				{
					FAPSlipContrato contrato = new FAPSlipContrato(wrapper, origin);
					contrato.aprovarContrato("Gerência");
				}
				break;
				case "Validar Contrato":
				{
					FAPSlipContrato contrato = new FAPSlipContrato(wrapper, origin);
					contrato.validarContrato();
				}
				break;
				case "Aprovar Contrato - Diretoria":
				{
					FAPSlipContrato contrato = new FAPSlipContrato(wrapper, origin);
					contrato.aprovarContrato("Diretoria");
				}
				break;
				case "Aprovar Contrato - Presidente":
				{
					FAPSlipContrato contrato = new FAPSlipContrato(wrapper, origin);
					contrato.aprovarContrato("Presidência");
				}
				break;
				
				case "Aprovar Compra":	
					Boolean validacaoOC = validarOrdemCompra(wrapper);
					if (!validacaoOC)
					{
						/*NeoObject noOrdemCompra = wrapper.findGenericValue("ordemCompra");
						Long codEmpresaOC = wrapper.findGenericValue("empresa.codemp");
						FAPSlipOrdemCompraJob job = new FAPSlipOrdemCompraJob();
						noOrdemCompra = job.preencherOrdemCompra(codOrdemCompra, noOrdemCompra, codEmpresaOC, true);
						wrapper.setValue("ordemCompra", noOrdemCompra);
						PersistEngine.persist(wrapper.getObject());*/
						
						throw new WorkflowException("Houveram alterações na Ordem de Compra. Por favor, avalie-as novamente.");
					}
					
					if (descricaoHistorico.equals(""))
					{
						if (aprovacaoDiretoria)
							descricaoHistorico = "Mensagem Automática: Pagamento Aprovado";
						else 
							descricaoHistorico = "Mensagem Automática: Pagamento Reprovado";
					}
					
					FAPSlipUtils.registrarHistorico(wrapper, origin.getActivityName(), descricaoHistorico, origin.returnResponsible());
					
					break;
				case "Pré Aprovar Compra":
					if (descricaoHistorico.equals(""))
					{
						if (aprovacaoDiretoria)
							descricaoHistorico = "Mensagem Automática: Pagamento Aprovado";
						else 
							descricaoHistorico = "Mensagem Automática: Pagamento Reprovado";
					}
					
					FAPSlipUtils.registrarHistorico(wrapper, origin.getActivityName(), descricaoHistorico, origin.returnResponsible());
					
					break;
				case "Aguardando Integração":
					/*if (aprovacaoDiretoria)
					{
						OrdemcomprabuscarPendentes4Out buscaPendentesOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().buscarPendentes_4(config.getUserService(), config.getPasswordService(), 0, buscaPendentesIn);
						if (buscaPendentesOut.getErroExecucao() == null || buscaPendentesOut.getErroExecucao().trim().equals(""))
						{
							OrdemcomprabuscarPendentes4OutOrdemCompra[] solicitacoes = buscaPendentesOut.getOrdemCompra();
							for (OrdemcomprabuscarPendentes4OutOrdemCompra solicitacao : solicitacoes)
							{
								if (codOrdemCompra.equals(solicitacao.getNumero().longValue()))
								{									
									OrdemcompraaprovarInOrdemCompra aprovacao = new OrdemcompraaprovarInOrdemCompra();
									aprovacao.setCodigoEmpresa(solicitacao.getCodigoEmpresa());
									aprovacao.setCodigoFilial(solicitacao.getCodigoFilial());
									aprovacao.setNumero(solicitacao.getNumero());
									aprovacao.setSequencia(0);
									
									OrdemcompraaprovarInOrdemCompra[] aprovarArray = {aprovacao};
									
									OrdemcompraaprovarIn aprovarIn = new OrdemcompraaprovarIn();
									aprovarIn.setOrdemCompra(aprovarArray);
									
									try
									{
										OrdemcompraaprovarOut aprovarOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().aprovar(config.getUserService(), config.getPasswordService(), 0, aprovarIn);
										if (aprovarOut.getErroExecucao() == null || aprovarOut.getErroExecucao().trim().equals(""))
										{
											
										}
										else {
											descricaoHistorico = "Houve um erro ao aprovar a Ordem de Compra. Tente novamente em alguns instantes.";
										}
									}
									catch (Exception e)
									{
										e.printStackTrace();
										descricaoHistorico = "Houve um erro ao aprovar a Ordem de Compra. Por favor, contate a TI.";
									}
									
								}	
							}
						}
						else {
							descricaoHistorico = "Houve um erro ao aprovar a Ordem de Compra. Tente novamente em alguns instantes.";
						}
					}*/
					break;
				case "Reavaliar Ordem Compra":	
				case "Reavaliar Ordem Compra - Almoxarifado":
					if (!reavaliarOrdemCompra)
					{
						OrdemcomprabuscarPendentes4Out buscaPendentesOut2 = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().buscarPendentes_4(config.getUserService(), config.getPasswordService(), 0, buscaPendentesIn);
						if (buscaPendentesOut2.getErroExecucao() == null || buscaPendentesOut2.getErroExecucao().trim().equals(""))
						{
							OrdemcomprabuscarPendentes4OutOrdemCompra[] solicitacoes = buscaPendentesOut2.getOrdemCompra();
							for (OrdemcomprabuscarPendentes4OutOrdemCompra solicitacao : solicitacoes)
							{
								if (codOrdemCompra.equals(solicitacao.getNumero().longValue()))
								{
									OrdemcomprareprovarInOrdemCompra reprovacao = new OrdemcomprareprovarInOrdemCompra();
									reprovacao.setCodigoEmpresa(solicitacao.getCodigoEmpresa());
									reprovacao.setCodigoFilial(solicitacao.getCodigoFilial());
									reprovacao.setNumero(solicitacao.getNumero());
									reprovacao.setSequencia(0);
									
									OrdemcomprareprovarInOrdemCompra[] reprovarArray = {reprovacao};
																		
									OrdemcomprareprovarIn reprovarIn = new OrdemcomprareprovarIn();
									reprovarIn.setOrdemCompra(reprovarArray);
									
									OrdemcomprareprovarOut reprovarOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().reprovar(config.getUserService(), config.getPasswordService(), 0, reprovarIn);
									
									if (reprovarOut.getErroExecucao() == null || reprovarOut.getErroExecucao().trim().equals(""))
									{
																	
									}
									else {
										System.out.println("APROVACAOCOMPRA - ERRO" + reprovarOut.getErroExecucao());
										throw new WorkflowException("Houve um erro ao reprovar a Ordem de Compra. Por favor, contate a TI.");
									}
								}
							}
						}
					}
					else {
						NeoObject noOrdemCompra = wrapper.findGenericValue("ordemCompra");
						Long codEmpresaOC = wrapper.findGenericValue("empresa.codemp");
					
						FAPSlipOrdemCompraJob job = new FAPSlipOrdemCompraJob();
						//noOrdemCompra = job.preencherOrdemCompra(codOrdemCompra, noOrdemCompra, codEmpresaOC, true);
						//wrapper.setValue("ordemCompra", noOrdemCompra);
					}
					
					if (descricaoHistorico.equals(""))
						descricaoHistorico = "Mensagem Automática: Compra Reavaliada";
					
					FAPSlipUtils.registrarHistorico(wrapper, origin.getActivityName(), descricaoHistorico, origin.returnResponsible());
					break;
				default:
					break;
			}
		}
		catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException(e.getErrorList().get(0).getMessage().getMessage());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}
	
	public void aprovarOrdemCompra(Integer codOrdemCompra, Integer codEmp, Integer codFil) throws RemoteException, ServiceException, WorkflowException
	{
		WSConfigs config = new WSConfigs();
		G5SeniorServicesLocator locator = new G5SeniorServicesLocator();
		
		OrdemcomprabuscarPendentes4In buscaPendentesIn = new OrdemcomprabuscarPendentes4In();
		buscaPendentesIn.setCodUsu(50);
		
		OrdemcompraaprovarInOrdemCompra aprovacao = new OrdemcompraaprovarInOrdemCompra();
		aprovacao.setCodigoEmpresa(codEmp);
		aprovacao.setCodigoFilial(codFil);
		aprovacao.setNumero(codOrdemCompra);
		aprovacao.setSequencia(0);
		
		OrdemcompraaprovarInOrdemCompra[] aprovarArray = {aprovacao};
		
		OrdemcompraaprovarIn aprovarIn = new OrdemcompraaprovarIn();
		aprovarIn.setOrdemCompra(aprovarArray);
		OrdemcompraaprovarOut aprovarOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().aprovar(config.getUserService(), config.getPasswordService(), 0, aprovarIn);
		if (aprovarOut.getErroExecucao() == null || aprovarOut.getErroExecucao().trim().equals(""))
		{
			
		}
		else {
			throw new WorkflowException("Houve um erro ao aprovar a Ordem de Compra. Por favor, contate a TI.");
		}
	}
	
	public void preencherGTC(String codProcesso)
	{
		QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("code", codProcesso), new QLEqualsFilter("model.name", "F002 - FAP Slip"));
		WFProcess wf = PersistEngine.getNeoObject(WFProcess.class, qlGroup);
		if (wf != null)
		{
			NeoObject noFAP = wf.getEntity();
			FAPSlipUtils.preencherGTC(new EntityWrapper(noFAP));
		}
	}
	
	private void preencherTarefaSimples(EntityWrapper wrapper, String codTarefa)
	{
		Long codOperacao = wrapper.findGenericValue("operacao.codigo");
		if (codOperacao == 1l)
		{
			NeoObject noTarefaAprovador = wrapper.findGenericValue("tarefaSimplesAprovador");
			NeoObject noTarefaControladoria = wrapper.findGenericValue("tarefaSimplesControladoria");
			if (noTarefaAprovador != null || noTarefaControladoria != null)
				return;
			
			noTarefaAprovador = AdapterUtils.createNewEntityInstance("Tarefa");
			EntityWrapper wTarefaAprovador = new EntityWrapper(noTarefaAprovador);
			
			wTarefaAprovador.setValue("avancaPrimeiraAtividade", true);
			wTarefaAprovador.setValue("Executor", wrapper.findGenericValue("solicitante"));
			wTarefaAprovador.setValue("Titulo", "Referente à FAP SLIP - " + codTarefa);
			wrapper.setValue("tarefaSimplesAprovador", noTarefaAprovador);
			
			noTarefaControladoria = EntityCloner.cloneNeoObject(noTarefaAprovador);
			wrapper.setValue("tarefaSimplesControladoria", noTarefaControladoria);
		} else if (codOperacao == 2l || codOperacao == 3l)
		{
			NeoObject noTarefaSimplesGerenteContrato = wrapper.findGenericValue("tarefaSimplesGerenteContrato");
			NeoObject noTarefaSimplesDiretorContrato = wrapper.findGenericValue("tarefaSimplesDiretorContrato");			
			NeoObject noTarefaSimplesPresidenteContrato = wrapper.findGenericValue("tarefaSimplesPresidenteContrato");
			if (noTarefaSimplesGerenteContrato != null || noTarefaSimplesDiretorContrato != null || noTarefaSimplesPresidenteContrato != null)
				return;
			
			noTarefaSimplesGerenteContrato = AdapterUtils.createNewEntityInstance("Tarefa");
			EntityWrapper wTarefaSimplesGerenteContrato = new EntityWrapper(noTarefaSimplesGerenteContrato);
			
			wTarefaSimplesGerenteContrato.setValue("avancaPrimeiraAtividade", true);
			wTarefaSimplesGerenteContrato.setValue("Executor", wrapper.findGenericValue("solicitante"));
			wTarefaSimplesGerenteContrato.setValue("Titulo", "Referente à FAP SLIP - " + codTarefa);
			wrapper.setValue("tarefaSimplesGerenteContrato", noTarefaSimplesGerenteContrato);
			
			noTarefaSimplesDiretorContrato = EntityCloner.cloneNeoObject(noTarefaSimplesGerenteContrato);
			wrapper.setValue("tarefaSimplesDiretorContrato", noTarefaSimplesDiretorContrato);
			
			noTarefaSimplesPresidenteContrato = EntityCloner.cloneNeoObject(noTarefaSimplesGerenteContrato);
			wrapper.setValue("tarefaSimplesPresidenteContrato", noTarefaSimplesPresidenteContrato);
		}
	}
	
	private Boolean validarOrdemCompra(EntityWrapper wrapper)
	{
		Long codEmp = wrapper.findGenericValue("empresa.codemp");
		Long codOrdemCompra = wrapper.findGenericValue("ordemCompra.ordemCompra");
		
		QLGroupFilter qlGroupItens = new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmp), new QLEqualsFilter("numocp", codOrdemCompra));
		List<NeoObject> listItensCompraSapiens = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE420IPO"), qlGroupItens);
		List<NeoObject> listItensCompraFusion = wrapper.findGenericValue("ordemCompra.itensCompra");
		if (listItensCompraSapiens.size() != listItensCompraFusion.size())
			return false;
		else
		{
			for (NeoObject noCompraSapiens : listItensCompraSapiens)
			{
				EntityWrapper wCompraSapiens = new EntityWrapper(noCompraSapiens);
				
				Long codEmpItemSapiens = wCompraSapiens.findGenericValue("codemp");
				String codProdutoItemSapiens = wCompraSapiens.findGenericValue("codpro");
				String codCCUSapiens = wCompraSapiens.findGenericValue("codccu");
				Long seqIpoSapiens = wCompraSapiens.findGenericValue("seqipo");
				
				for (NeoObject noCompraFusion : listItensCompraFusion)
				{
					EntityWrapper wCompraFusion = new EntityWrapper(noCompraFusion);
					
					Long codEmpItemFusion = wCompraFusion.findGenericValue("codemp");
					String codProdutoItemFusion = wCompraFusion.findGenericValue("codProduto");
					String codCCUFusion = wCompraFusion.findGenericValue("codccu");
					Long seqIpoFusion = wCompraFusion.findGenericValue("seqipo");
					
					if (codProdutoItemSapiens.equals(codProdutoItemFusion) && seqIpoSapiens.equals(seqIpoFusion))
					{
						BigDecimal qtdSapiens = wCompraSapiens.findGenericValue("qtdped");
						Long qtdSapiensLong = qtdSapiens.longValue();
						BigDecimal valorBrutoSapiens = wCompraSapiens.findGenericValue("vlrbru");
						
						Long qtdFusion = wCompraFusion.findGenericValue("quantidade");
						BigDecimal valorBrutoFusion = wCompraFusion.findGenericValue("valorTotal");
						
						if (!(qtdSapiensLong.equals(qtdFusion) && valorBrutoSapiens.equals(valorBrutoFusion)))
						{
							return false;
						}
					}
				}
			}
		}
		
		return true;
	}
	
	@Override
	public void back(EntityWrapper wrapper, Activity activity)
	{
		
	}

	@Override
	public void start(Task task, EntityWrapper wrapper, Activity activity)
	{
		
	}
}
