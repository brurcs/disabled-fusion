package com.neomind.fusion.custom.orsegups.maps.vo;

import java.io.Serializable;
import java.util.Calendar;

public class TrackEventVO implements Serializable
{

	private static final long serialVersionUID = 1L;

	private String id = null;

	private Calendar time = null;

	private long event = 0L;

	private float lat = -1;

	private float lng = -1;

	private long alt = 0L;

	private float speed = 0L;

	private long heading = 0L;

	private long sat = 0L;

	private long in = 0L;

	private long out = 0L;
	
	private boolean ignition = false;
	
	private String eventName = "";
	
	private long status;

	public float getLat()
	{
		return lat;
	}

	public void setLat(float lat)
	{
		this.lat = lat;
	}

	public float getLng()
	{
		return lng;
	}

	public void setLng(float lng)
	{
		this.lng = lng;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public Calendar getTime()
	{
		return time;
	}

	public void setTime(Calendar time)
	{
		this.time = time;
	}

	public long getAlt()
	{
		return alt;
	}

	public void setAlt(long alt)
	{
		this.alt = alt;
	}

	public float getSpeed()
	{
		return speed;
	}

	public void setSpeed(float speed)
	{
		this.speed = speed;
	}

	public long getHeading()
	{
		return heading;
	}

	public void setHeading(long heading)
	{
		this.heading = heading;
	}

	public long getSat()
	{
		return sat;
	}

	public void setSat(long sat)
	{
		this.sat = sat;
	}

	public long getEvent()
	{
		return event;
	}

	public void setEvent(long event)
	{
		this.event = event;
	}

	public long getIn()
	{
		return in;
	}

	public void setIn(long in)
	{
		this.in = in;
	}

	public long getOut()
	{
		return out;
	}

	public void setOut(long out)
	{
		this.out = out;
	}

	public boolean isIgnition()
	{
		return ignition;
	}

	public void setIgnition(boolean ignition)
	{
		this.ignition = ignition;
	}
	
	public String getEventName()
	{
		return eventName;
	}

	public void setEventName(String eventName)
	{
		this.eventName = eventName;
	}
	
	public long getStatus()
	{
		return status;
	}

	public void setStatus(long status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "TrackEvent [id=" + id + ", time=" + (time == null ? null : time.getTime()) + ", lat=" + lat + ", lng=" + lng + ", alt=" + alt + ", speed=" + speed + ", heading=" + heading + ", sat=" + sat + ", event=" + event + ", in=" + in + ", out=" + out + "]";
	}

}
