package com.neomind.fusion.custom.orsegups.fap.slip.converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class HistoricoPagamentosConverter extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuilder outBuilder = new StringBuilder();

		//sortNeoId(historico);
 
		outBuilder.append(" <script type=\"text/javascript\"> function viewItemFusion(id)");
		outBuilder.append(" {");
		outBuilder.append(" var title =\"Visualizar Pagamento\";");
		outBuilder.append(" var uid;");
		outBuilder.append(" var entityType = '';");
		outBuilder.append(" var idx = '';");
		outBuilder.append(" var portlet = new FloatForm(title, uid, entityType, id, 'ellist_', idx, 0, 0, '', null, null, false, false, null, false, '');");
		outBuilder.append(" portlet.open();");
		outBuilder.append("}");
		outBuilder.append("</script>");
		outBuilder.append("<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
		outBuilder.append("    <tr style=\"cursor: auto\">");
		outBuilder.append("        <th style=\"cursor: auto\"></th>");
		outBuilder.append("        <th style=\"cursor: auto\">Solicitante</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Descrição</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Empresa</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Competência</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Nr. Título</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Data da Pagamento</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Valor</th>");
		outBuilder.append("    </tr>");
		outBuilder.append("    <tbody>");
		
		List<NeoObject> historico = new ArrayList<>();
		
		if (field.getFieldCode().equals("historicoFornecedor"))
		{
			Long codFornecedor = new EntityWrapper(field.getForm().getObject()).findGenericValue("fornecedor.codfor");
			if (codFornecedor == null)
				codFornecedor = new EntityWrapper(field.getForm().getObject()).findGenericValue("novoContrato.fornecedor.codfor");
			
			historico = FAPSlipUtils.buscarPagamentosFornecedor(codFornecedor);
		}
		else
		{
			Long codContrato = new EntityWrapper(field.getForm().getObject()).findGenericValue("dadosContrato.codContrato");
			if (codContrato == null)
				codContrato = new EntityWrapper(field.getForm().getObject()).findGenericValue("novoContrato.codContrato");
			
			historico = FAPSlipUtils.buscarProcessos(codContrato, null, null);
		}
		
		if (historico.size() > 12)
			historico = historico.subList(0, 12);
		
		ordernarPorData(historico);
		
		BigDecimal total = new BigDecimal(0);
		for (NeoObject registro : historico)
		{
			EntityWrapper wRegistro = new EntityWrapper(registro);
			if (!registro.getNeoType().equals("XSAPIENSE501TCP"))
			{
				BigDecimal valor = wRegistro.findGenericValue("valorPagamento");
				if (valor != null)
					total = total.add(valor);				
					
				outBuilder.append("        <tr>");
				outBuilder.append("            <td><img onclick=\"javascript:viewItemFusion(" + registro.getNeoId() + ");\" neoid=\"" + registro.getNeoId() + "\" id=\"editItem_" + registro.getNeoId() + "\" class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a></td>");
				outBuilder.append("            <td> " + wRegistro.findGenericValue("solicitante") + " </td>");
				outBuilder.append("            <td> " + wRegistro.findGenericValue("dadosContrato.titulo") + " </td>");
				outBuilder.append("            <td> " + wRegistro.findGenericValue("dadosContrato.empresa.codemp") + " </td>");
				outBuilder.append("            <td> " + NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.findGenericValue("competencia"), "dd/MM/yyyy") + " </td>");
				outBuilder.append("            <td> " + wRegistro.findGenericValue("nrDocumento") + " </td>");
				outBuilder.append("            <td> " + NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.findGenericValue("dataVencimento"), "dd/MM/yyyy") + " </td>");
				outBuilder.append("            <td> " + FAPSlipUtils.parseMonetario(valor) + " </td>");
				outBuilder.append("        </tr>");
			}
			else
			{
				BigDecimal valor = wRegistro.findGenericValue("vlrori");
				if (valor != null)
					total = total.add(valor);
				
				String descricao = wRegistro.findGenericValue("obstcp");
				String codtpt = wRegistro.findGenericValue("codtpt");
				
				NeoObject noTipoTitulo = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE002TPT"), new QLEqualsFilter("codtpt", codtpt));
				if (noTipoTitulo != null)
				{
					EntityWrapper wTipoTitulo = new EntityWrapper(noTipoTitulo);
					
					String tipoTitulo = wTipoTitulo.findGenericValue("destpt");
					if (tipoTitulo != null)
					{
						descricao += " (" + tipoTitulo + ")";
					}
				}
				
				outBuilder.append("        <tr>");
				outBuilder.append("            <td><img onclick=\"javascript:viewItemFusion(" + registro.getNeoId() + ");\" neoid=\"" + registro.getNeoId() + "\" id=\"editItem_" + registro.getNeoId() + "\" class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a></td>");
				outBuilder.append("            <td> " + "SAPIENS" + " </td>");
				outBuilder.append("            <td> " + descricao + " </td>");
				outBuilder.append("            <td> " + wRegistro.findGenericValue("codemp") + " </td>");
				outBuilder.append("            <td> " + NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.findGenericValue("usu_datent"), "dd/MM/yyyy") + " </td>");
				outBuilder.append("            <td> " + wRegistro.findGenericValue("numtit") + " </td>");
				outBuilder.append("            <td> " + NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.findGenericValue("datppt"), "dd/MM/yyyy") + " </td>");
				outBuilder.append("            <td> " + FAPSlipUtils.parseMonetario(valor) + " </td>");
				outBuilder.append("        </tr>");
			}
		}

		outBuilder.append("	    </tbody>");
		outBuilder.append("	    <tfoot>");
		outBuilder.append("	        <tr>");
		outBuilder.append("	            <td colspan=\"7\" style=\"text-align: right;\">Total:</td>");
		outBuilder.append("	            <td style=\"text-align: center;\">" + FAPSlipUtils.parseMonetario(total) + "</td>");
		outBuilder.append("	        </tr>");
		outBuilder.append("	    </tfoot>");
		outBuilder.append("</table>");

		return outBuilder.toString();

	}
	
	/*private void criarHistoricoPagamentos(EntityWrapper wrapper) 
	{
		Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
		Long codFornecedor = wrapper.findGenericValue("fornecedor.codfor");
		
		List<NeoObject> listHistoricoFornecedor = buscarPagamentosFornecedor(codFornecedor);
		List<NeoObject> listHistoricoContrato = buscarProcessos(codContrato);
		
		for (NeoObject noEntity : listHistoricoContrato)
		{
			wrapper.findField("historicoContrato").addValue(noEntity);
		}
		
		wrapper.findField("historicoFornecedor").setValue(listHistoricoFornecedor);
	}*/
	
	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}

	public static void sortNeoId(Collection<? extends NeoObject> list)
	{
		Collections.sort((List<? extends NeoObject>) list, new Comparator<NeoObject>()
		{

			public int compare(NeoObject obj1, NeoObject obj2)
			{

				return ((Long) new EntityWrapper(obj1).findGenericValue("neoId") < (Long) new EntityWrapper(obj2).findGenericValue("neoId")) ? -1 : 1;
			}
		});
	}
	
	private void ordernarPorData(Collection<? extends NeoObject> list)
	{
		if (list == null)
			return;
		
		Collections.sort((List<? extends NeoObject>) list, new Comparator<NeoObject>()		
		{
			@Override
			public int compare(NeoObject o1, NeoObject o2)
			{
				if (o1 == null || o2 == null) 
				      return 0;
				else
				{
					String campoData = "usu_datent";
					
					if (!o1.getNeoType().equals("XSAPIENSE501TCP"))
					{
						campoData = "competencia";
					}
					
					GregorianCalendar competencia1 = new EntityWrapper(o1).findGenericValue(campoData);
					GregorianCalendar competencia2 = new EntityWrapper(o2).findGenericValue(campoData);
					
					if (competencia1 == null || competencia2 == null)
						return 0;
					
					return competencia1.compareTo(competencia2);
				}	
			}
		});
	}
}
