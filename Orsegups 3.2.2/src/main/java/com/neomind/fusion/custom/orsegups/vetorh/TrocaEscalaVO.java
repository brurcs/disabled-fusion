package com.neomind.fusion.custom.orsegups.vetorh;

public class TrocaEscalaVO
{
	private Long errada;
	private Long correta;
	public Long getErrada()
	{
		return errada;
	}
	public void setErrada(Long errada)
	{
		this.errada = errada;
	}
	public Long getCorreta()
	{
		return correta;
	}
	public void setCorreta(Long correta)
	{
		this.correta = correta;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((correta == null) ? 0 : correta.hashCode());
		result = prime * result + ((errada == null) ? 0 : errada.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrocaEscalaVO other = (TrocaEscalaVO) obj;
		if (correta == null)
		{
			if (other.correta != null)
				return false;
		}
		else if (!correta.equals(other.correta))
			return false;
		if (errada == null)
		{
			if (other.errada != null)
				return false;
		}
		else if (!errada.equals(other.errada))
			return false;
		return true;
	}
	@Override
	public String toString()
	{
		return "TrocaEscalaVO [errada=" + errada + ", correta=" + correta + "]";
	}
}
