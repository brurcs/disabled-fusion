package com.neomind.fusion.custom.orsegups.presenca.vo;

public class TarefaFaltaEfetivoVO {
	
	Long neoIdProcesso;
	String code;
	String startDate;
	String link;
	
	public Long getNeoIdProcesso() {
		return neoIdProcesso;
	}
	public void setNeoIdProcesso(Long neoIdProcesso) {
		this.neoIdProcesso = neoIdProcesso;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	

}
