package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

import br.com.segware.sigmaWebServices.webServices.EventoRecebido;
import br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy;

public class RotinaGeraEventoXRIM implements CustomJobAdapter {
	private static final Log log = LogFactory.getLog(RotinaGeraEventoXRIM.class);
	private static final String nomeRotina = "RotinaGeraEventoXRIM";
	private static final String evento = "XRIM";
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		System.out.println("#### INICIO DA ROTINA GERAR EVENTO XRIM - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		
		List<EventoRecebido> listaOS = new ArrayList<EventoRecebido>();
		
		try {
			
			String ultimaExecucao = ultimaExecucaoRotina(nomeRotina);
			String execucaoAtual = ultimaExecucaoRotinaRange(nomeRotina);
			listaOS = retornaOs(ultimaExecucao, execucaoAtual);
			
			for(EventoRecebido eventoAux : listaOS) {
				
				try {	
					executaServicoSegware(eventoAux);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#### ERRO AO GERAR EVENTO PRA OS: "+eventoAux.getCodigo()+" - BA: "+eventoAux.getIdCentral()+"["+eventoAux.getParticao()+"] - CLIENTE:"+eventoAux.getCdCliente());
				}
				
			}
			
			atualizaUltimaExecucao(nomeRotina);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### ERRO ROTINA GERAR EVENTO XRIM - Data: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		} finally {
			System.out.println("#### FIM DA ROTINA GERAR EVENTO XRIM - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
		
		
	}
	
	/**
	 * Retornar as Ordens de Serviço 
	 * 
	 * @param ultimaExecucao Última execução da Rotina 
	 * @param execucaoAtual Última execução da Rotina +9 minutos
	 */
	public List<EventoRecebido> retornaOs(String ultimaExecucao, String execucaoAtual) {
		
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		List<EventoRecebido> listaOS = new ArrayList<EventoRecebido>();
		
		try {
		
			sql.append(" SELECT OS.ID_ORDEM, OS.CD_CLIENTE, DB.ID_EMPRESA, DB.ID_CENTRAL, DB.PARTICAO, DB.RAZAO,OS.DATAAGENDADA, ");
			sql.append(" OS.DEFEITO, OS.IDOSDEFEITO,COL.NM_COLABORADOR ");
			sql.append(" FROM DBORDEM OS ");
			sql.append(" INNER JOIN DBCENTRAL DB ");
			sql.append(" ON DB.CD_CLIENTE = OS.CD_CLIENTE ");
			sql.append(" INNER JOIN COLABORADOR COL ");
			sql.append(" ON OS.ID_INSTALADOR = COL.CD_COLABORADOR ");
			sql.append(" WHERE OS.DT_INICIO_EXECUCAO BETWEEN ? AND ? ");
			sql.append(" AND DB.CD_GRUPO_CLIENTE = 323 ");
			sql.append(" AND OS.IDOSDEFEITO = 180 ");
			
			conn = PersistEngine.getConnection("SIGMA90");
			
			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, ultimaExecucao);
			pstm.setString(2, execucaoAtual);

			rs = pstm.executeQuery();
			
			while(rs.next()) {
				
				String logEvento = "Evento gerado automaticamente devido a iniciação de OS com o DEFEITO CFTV RECUPERAR IMAGENS. \n";
				logEvento += "BA: "+rs.getString("ID_CENTRAL")+" - "+rs.getString("RAZAO")+" \n";
				logEvento += "OS: "+rs.getString("ID_ORDEM")+" \n";
				logEvento += "Técnico Responsável: "+rs.getString("NM_COLABORADOR")+" \n";
				
				EventoRecebido eventoRecebido = new EventoRecebido();

				eventoRecebido.setCodigo(evento);
				eventoRecebido.setData(new GregorianCalendar());
				eventoRecebido.setEmpresa(rs.getLong("ID_EMPRESA"));
				eventoRecebido.setIdCentral(rs.getString("ID_CENTRAL"));
				eventoRecebido.setParticao(rs.getString("PARTICAO"));
				eventoRecebido.setCdCliente(rs.getInt("CD_CLIENTE"));
				eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
				eventoRecebido.setProtocolo(Byte.parseByte("2"));
				eventoRecebido.setLogEvento(logEvento);
		
				listaOS.add(eventoRecebido);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		return listaOS;	
	}
	
	/**
	 * Captura a última execução da rotina
	 * 
	 * @param nomeRotina Nome da rotina a ser executada
	 * @return A data da última execução da rotina
	 */
	public String ultimaExecucaoRotina(String nomeRotina) {

		String retorno = null;
		
		try	{
			
			Collection<NeoObject> monitoraAgendador = null;

			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty()){
				for (NeoObject neoObject : monitoraAgendador){
					EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

					GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

					retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return retorno;

	}

	/**
	 * Define o range máximo para execução da Rotina
	 * 
	 * @param nomeRotina Nome da rotina a ser executada
	 * @return A data da última execução da rotina + 9minutos para criar um range de alcance
	 */
	public String ultimaExecucaoRotinaRange(String nomeRotina) {

		String retorno = null;
		
		try	{
			
			Collection<NeoObject> monitoraAgendador = null;

			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty()){
				for (NeoObject neoObject : monitoraAgendador){
					
					EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

					GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

					GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

					dataFinalAgendadorAux.add(GregorianCalendar.MINUTE, 9);
					dataFinalAgendadorAux.set(GregorianCalendar.SECOND, 59);
					dataFinalAgendadorAux.set(GregorianCalendar.MILLISECOND, 59);

					retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return retorno;
	}
	
	/**
	 * Insere a data da última execução da rotina no Formulário
	 * 
	 * @param nomeRotina Nome da rotina a ser executado
	 */
	public void atualizaUltimaExecucao(String nomeRotina) {
		
		int minutos = 0;
		
		try
		{
			
			QLGroupFilter filter = new QLGroupFilter("AND");
			filter.addFilter(new QLEqualsFilter("nomeRotina", nomeRotina));

			List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

			if (monitoraAgendador != null && !monitoraAgendador.isEmpty())
			{
				NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

				EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);
				
				GregorianCalendar dataExecucao = new GregorianCalendar();	
				minutos = dataExecucao.get(GregorianCalendar.MINUTE);
				minutos = (minutos-(minutos%10));
				
				dataExecucao.set(GregorianCalendar.MINUTE, minutos);
				dataExecucao.set(GregorianCalendar.SECOND, 00);
				dataExecucao.set(GregorianCalendar.MILLISECOND, 00);

				monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(dataExecucao);

				PersistEngine.persist(neoObject);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	/**
	 * @param eventoRecebido Objeto da Ordem de Serviço encontrada na Query
	 */
	private String executaServicoSegware(EventoRecebido eventoRecebido)
	{
		String returnFromAccess = null;
		
		try
		{
			ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();

			returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
			
			if(returnFromAccess != "ACK") {
				enviaEmailErro(returnFromAccess, eventoRecebido);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
			System.out.println("##### ERRO ROTINA GERAR EVENTO XRIM - Data: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
		return returnFromAccess;
	}

	private void enviaEmailErro(String returnFromAccess, EventoRecebido eventoRecebido) {
		
		GregorianCalendar dataBase = new GregorianCalendar();
		Date dataAux = eventoRecebido.getData().getTime();
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		
		String dataExecucao = NeoDateUtils.safeDateFormat(dataBase, "dd/MM/YYYY HH:mm:ss");
		
		List<String> emails = new ArrayList<>();
		emails.add("herisson.ferreira@orsegups.com.br");
		
		/**
		 * HTML Encoding
		 * 
		 * á = &aacute;
		 * ã = &atilde;
		 * é = &eacute;
		 * ç = &ccedil;
		 */
		StringBuilder msg = new StringBuilder();
		
		msg.append("	<h1>Rotina Gera Evento XRIM</h1>																															");
		msg.append("																																								");
		msg.append("	<p>Erro ao gerar o evento XRIM </p>																														");
		msg.append("	<p>Exce&ccedil;&atilde;o gerada por: "+this.getClass()+" </p>																								");
		msg.append("	<p>Rotina executada em&nbsp;&quot;<strong>"+dataExecucao+"</strong>.&quot;&nbsp;</p>																		");
		msg.append("	<p>Dados do Evento:</p>																																		");
		msg.append("																																								");
		msg.append("	<table align=\"left\" border=\"2\" cellpadding=\"1\" cellspacing=\"1\" style=\"width: 500px;\" summary=\"Dados do Evento XRIM\">							");
		msg.append("		<tbody>																																					");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Data de Execu&ccedil;&atilde;o da Rotina</th>    																				");
		msg.append("				<td>"+simpleDateFormat.format(dataAux)+"</td>																									");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Conta</th>																													");
		msg.append("				<td>"+eventoRecebido.getIdCentral()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Parti&ccedil;&atilde;o</th>																									");
		msg.append("				<td>"+eventoRecebido.getParticao()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">C&oacute;digo do Cliente (Sigma)</th>																							");
		msg.append("				<td>"+eventoRecebido.getCdCliente()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Nome do T&eacute;cnico</th>																									");
		msg.append("				<td>"+eventoRecebido.getNomePessoa()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Log do Evento</th>																											");
		msg.append("				<td>"+eventoRecebido.getLogEvento()+"</td>																										");
		msg.append("			</tr>																																				");
		msg.append("			<tr>																																				");
		msg.append("				<th scope=\"row\">Erro: </th>																													");
		msg.append("				<td>"+returnFromAccess+"</td>																													");
		msg.append("			</tr>																																				");
		msg.append("		</tbody>																																				");
		msg.append("	</table>																																					");
		msg.append("																																								");
		msg.append("	<p>&nbsp;</p>																																				");
		msg.append("	<p>&nbsp;</p>																																				");
		msg.append("	<p>&nbsp;</p>																																				");
		msg.append("	<p>&nbsp;</p>																																				");

		
		String assunto = "[Rotina Gera Evento XRIM] Erro ao gerar evento";
		
		OrsegupsEmailUtils.sendEmailAutomatic(emails, msg.toString(), assunto);
		
	}

}
