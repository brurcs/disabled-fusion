package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaAtualizaStatusContaSigma implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaAtualizaStatusContaSigma.class);

    @Override
    public void execute(CustomJobContext arg0) {
	
	    Connection conn = null;
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    log.warn("##### INICIO CONSULTA STATUS CONTAS dos clientes do Sigma - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    Long key = GregorianCalendar.getInstance().getTimeInMillis();
	    

	    try {
		
		String sql =  " SELECT CD_CLIENTE FROM dbCENTRAL WITH(NOLOCK) WHERE CTRL_CENTRAL = 0 AND FG_ATIVO != 0 ";
		
		conn = PersistEngine.getConnection("SIGMA90");
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		
		while (rs.next()){
		    this.updateDBCentral(rs.getInt("CD_CLIENTE"));
		}

	    } catch (Exception e) {
		log.error("##### ERRO: UPDATE STATUS CONTA dos clientes do Sigma: Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		e.printStackTrace();
		throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	    } finally {
		OrsegupsUtils.closeConnection(conn, stmt, rs);
		log.warn("##### UPDATE STATUS CONTA dos clientes do Sigma - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    }
	
	

    }

    private void updateDBCentral(int cdCliente) {

	if (cdCliente != 0) {

	    Connection conn = null;
	    PreparedStatement pstm = null;
	    log.warn("##### INICIO UPDATE STATUS CONTA dos clientes do Sigma - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    Long key = GregorianCalendar.getInstance().getTimeInMillis();
	    

	    try {
		
		conn = PersistEngine.getConnection("SIGMA90");
		String sqlUpdate = " UPDATE dbCENTRAL SET FG_ATIVO = 0 WHERE CD_CLIENTE=? ";
		pstm = conn.prepareStatement(sqlUpdate);
		
		pstm.setInt(1, cdCliente);
		pstm.executeUpdate();

	    } catch (Exception e) {
		log.error("##### ERRO: UPDATE STATUS CONTA dos clientes do Sigma: Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		e.printStackTrace();
		throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, null);
		log.warn("##### UPDATE STATUS CONTA dos clientes do Sigma - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    }
	    
	}


    }
}