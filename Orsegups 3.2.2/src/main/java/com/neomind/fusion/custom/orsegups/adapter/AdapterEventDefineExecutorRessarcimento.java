package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class AdapterEventDefineExecutorRessarcimento implements TaskFinishEventListener {
	private static final Log log = LogFactory.getLog(AdapterEventDefineExecutorRessarcimento.class);
	
	public static String mensagemErro = "Erro inesperado. Por favor contate o Departamento de TI.";
	
	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
		
		System.out.println("#### INICIO DO ADAPTER DE EVENTOS: AdapterEventDefineExecutorRessarcimento DATA: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		
		try {
			
			InstantiableEntityInfo fRessarcimento = AdapterUtils.getInstantiableEntityInfo("RESProcessoDeRessarcimento");
			NeoObject oRessarcimento = fRessarcimento.createNewInstance();
			EntityWrapper wRessarcimento = new EntityWrapper(event.getProcess().getEntity());
			
			Long tipoMercado = (Long) wRessarcimento.findField("cliente.tipemc").getValue();
			Long regional = (Long) wRessarcimento.findField("regCorrespDois.codigo").getValue();
			
			NeoPaper papelResponsavel = retornaPapelResponsavel(regional, tipoMercado);
			NeoPaper papelResponsavelControladoria = retornaPapelResponsavelControladoria(regional, tipoMercado);
			
			if(NeoUtils.safeIsNotNull(papelResponsavel) && NeoUtils.safeIsNotNull(papelResponsavelControladoria)) {
				
				wRessarcimento.findField("responsavelRessarcimento").setValue(papelResponsavel);
				wRessarcimento.findField("responsavelRessarcimentoControladoria").setValue(papelResponsavelControladoria);
				
				PersistEngine.persist(oRessarcimento);
				
			}else {
				System.out.println("#### ERRO NO ADAPTER DE EVENTOS: AdapterEventDefineExecutorRessarcimento DATA: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
				mensagemErro = "Não foi possível definir o responsável pela execução do ressarcimento. Entre em contato com a TI.";
				throw new WorkflowException(mensagemErro);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException(mensagemErro);
		}
		
		System.out.println("#### FIM DO ADAPTER DE EVENTOS: AdapterEventDefineExecutorRessarcimento DATA: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}

	/**
	 * 
	 * @param codigoRegional
	 * @param codigoTipoMercado
	 * @return Papel da Regional Responsável pelo Cliente
	 */
	public NeoPaper retornaPapelResponsavel(Long codigoRegional, Long codigoTipoMercado) {
		
		EntityWrapper ewResponsavel = null;
		NeoPaper papelResponsavel = null;
		NeoObject responsavel = null;
		
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
    	groupFilter.addFilter(new QLEqualsFilter("regional", codigoRegional));
    	groupFilter.addFilter(new QLEqualsFilter("tipoMercado", codigoTipoMercado));
    	responsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("responsaveisRessarcimento"), groupFilter);

    	if (NeoUtils.safeIsNotNull(responsavel)) {
    		ewResponsavel = new EntityWrapper(responsavel);
    	    papelResponsavel = ewResponsavel.findGenericValue("responsavel");   
    	}else {
    		mensagemErro = "["+this.getClass().getSimpleName()+"] Não foi possível definir um responsável para a tarefa.";
    	}
	
		return papelResponsavel;
	}
	
	/**
	 * 
	 * @param codigoRegional
	 * @param codigoTipoMercado
	 * @return Papel do Responsável na Controladoria pela Regional
	 */
	public NeoPaper retornaPapelResponsavelControladoria(Long codigoRegional, Long codigoTipoMercado) {
		
		EntityWrapper ewResponsavel = null;
		NeoPaper papelResponsavel = null;
		NeoObject responsavel = null;
		
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
    	groupFilter.addFilter(new QLEqualsFilter("regional", codigoRegional));
    	groupFilter.addFilter(new QLEqualsFilter("tipoMercado", codigoTipoMercado));
    	responsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("responsaveisRessarcimentoControladoria"), groupFilter);

    	if (NeoUtils.safeIsNotNull(responsavel)) {
    		ewResponsavel = new EntityWrapper(responsavel);
    	    papelResponsavel = ewResponsavel.findGenericValue("responsavel");   
    	}else {
    		mensagemErro = "["+this.getClass().getSimpleName()+"] Não foi possível definir um responsável pela controladoria para a tarefa.";
    	}
	
		return papelResponsavel;
	}

}
