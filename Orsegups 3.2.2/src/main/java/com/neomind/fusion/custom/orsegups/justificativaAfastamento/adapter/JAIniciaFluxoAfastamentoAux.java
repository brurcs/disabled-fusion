package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.presenca.vo.JustificativaFaltaVO;
import com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaOS;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;


/**
 * 
 * @author jorge.filho
 *
 */
public class JAIniciaFluxoAfastamentoAux implements CustomJobAdapter {

    private static final Log log = LogFactory.getLog(RotinaAbreTarefaOS.class);

    public StringBuilder logEmail;
    protected JustificativaFaltaVO justificativaFaltaVO;

    int contJFN;
    int contJF;
    int contJDS;
    int contJDC;

    int contJFN_ERR;
    int contJF_ERR;
    int contJDS_ERR;
    int contJDC_ERR;

    static String cidadesExecao = "";

    @Override
    public void execute(CustomJobContext arg0) {
	Connection conn = PersistEngine.getConnection("VETORH");
	StringBuilder sql = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {
	    //sql.append(" DECLARE @DatRef  Datetime ");
	   // sql.append(" SELECT @DatRef = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))  ");
	    sql.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, afa.datafa, afa.horafa, fun.NomFun,orn.USU_CodReg, car.TitRed, orn.USU_LotOrn, orn.NomLoc, afa.DatAfa, afa.DatTer, afa.USU_AfaInf, fis.USU_NomFis, afa.SitAfa, afa.CauDem, orn.usu_codccu, orn.usu_codcid ");
	    sql.append(" FROM R038AFA afa WITH (NOLOCK) ");
	    sql.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = afa.NumEmp AND fun.TipCol = afa.TipCol AND fun.NumCad = afa.NumCad ");
	    sql.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
	    sql.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <=  afa.DatAfa) ");
	    sql.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.TabOrg = hlo.TabOrg AND orn.NumLoc = hlo.NumLoc ");
	    sql.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
	    sql.append(" WHERE afa.SitAfa NOT IN (2,3,4,6,7,13,16,20,68,74,113,117,500) ");
	    sql.append(" AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.d_TarefaAfastamentoJustificativa aj WITH (NOLOCK) ");
	    sql.append(" 			  WHERE aj.numcpf = fun.numcpf AND CAST(floor(CAST(aj.datafa AS FLOAT)) AS datetime) BETWEEN CAST(floor(CAST(afa.datafa AS FLOAT)) AS datetime) AND CAST(floor(CAST(afa.DatTer AS FLOAT)) AS datetime) AND aj.horafa = afa.horafa  ) ");
	    //sql.append(" AND afa.DatTer <= @DatRef-1 ");
	    sql.append(" AND orn.USU_CodReg IN (0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29)  AND afa.DatTer >= getdate()-30 ");	   
//	    sql.append(" AND orn.nomloc NOT IN ('Afastados Sede Administração','Transferência Sede Administração','Aviso Prévio Sede Administração','SESMT') ");

	    logEmail = new StringBuilder();
	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();

	    while (rs.next()) {
		justificativaFaltaVO = new JustificativaFaltaVO(rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol"), rs.getString("USU_AfaInf"),
			rs.getTimestamp("DatAfa"), rs.getLong("HorAfa"), rs.getTimestamp("DatTer"), rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"),
			rs.getString("USU_LotOrn"), rs.getString("TitRed"), rs.getString("USU_NomFis"), "", rs.getLong("usu_codccu"), rs.getLong("usu_codcid"));
		System.out.println("Falta não justificada " + justificativaFaltaVO);
		final NeoRunnable work = new NeoRunnable() {
		    public void run() throws Exception {
			abreTarefaFaltaNaoJustificada(justificativaFaltaVO.getNumemp(), justificativaFaltaVO.getNumcad(), justificativaFaltaVO.getNomFun(), justificativaFaltaVO.getNumcpf(),
				justificativaFaltaVO.getTipcol(), justificativaFaltaVO.getAfainf(), justificativaFaltaVO.getDatafa(), justificativaFaltaVO.getHorafa(), justificativaFaltaVO.getDatter(),
				justificativaFaltaVO.getSitafa(), justificativaFaltaVO.getCaudem(), justificativaFaltaVO.getCodreg(), justificativaFaltaVO.getNomloc(), justificativaFaltaVO.getLotorn(),
				justificativaFaltaVO.getTitred(), justificativaFaltaVO.getNomfis(), justificativaFaltaVO.getUsuCodccu(), justificativaFaltaVO.getUsuCodCid());

		    }
		};
		try {
		    contJFN++;
		    PersistEngine.managedRun(work);
		} catch (final Exception e) {
		    System.out.println(e.getMessage() + e);
		    e.printStackTrace();
		    long key = Calendar.getInstance().getTimeInMillis();
		    log.error("[" + key + "]  ERRO AO INICIAR AO FLUXO DE AFASTAMENTO " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    throw new JobException("Erro ao fazer iniciar fluxo de afastamento! Procurar no log por [" + key + "]");
		}

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
    }

    private void abreTarefaFaltaNaoJustificada(String numemp, String numcad, String nomFun, Long numcpf, Long tipcol, String afainf, Timestamp datafa, Long horafa, Timestamp datter, Long sitafa, Long caudem,
	    Long codreg, String nomloc, String lotorn, String titred, String nomfis, Long usuCodccu, Long usuCodcid) throws SQLException, Exception {
	Connection conn = null;
	PreparedStatement stCount = null;
	ResultSet rsCount = null;

	try {

	    conn = PersistEngine.getConnection("VETORH");

	    String afaInf = afainf;

	    if (afaInf != null && !afaInf.equals("") && afaInf.equals("S")) {
		Long numCpf = numcpf;
		Timestamp dataTermino = datter;
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(dataTermino.getTime());

		StringBuilder sqlCount = new StringBuilder();
		sqlCount.append(" SELECT SUM(DATEDIFF(DAY, afa.DatAfa, CASE WHEN afa.DatAfa = afa.DatTer THEN DATEADD(dd, 1, afa.DatTer) ELSE afa.DatTer END)) AS count ");
		sqlCount.append(" FROM R038AFA afa ");
		sqlCount.append(" INNER JOIN R034FUN fun ON fun.NumEmp = afa.NumEmp AND fun.TipCol = afa.TipCol AND fun.NumCad = afa.NumCad ");
		sqlCount.append(" WHERE afa.SitAfa = 15 AND fun.NumCpf = ? AND afa.USU_AfaInf = 'S' ");
		sqlCount.append(" AND afa.DatTer >= DATEADD(MM, -12, ?) AND afa.DatTer <= ? ");

		stCount = conn.prepareStatement(sqlCount.toString());

		stCount.setLong(1, numCpf);
		stCount.setTimestamp(2, new Timestamp(gData.getTimeInMillis()));
		stCount.setTimestamp(3, new Timestamp(gData.getTimeInMillis()));

		rsCount = stCount.executeQuery();

		if (rsCount.next()) {
		    Long count = rsCount.getLong("count");
		    if (count == null || count <= 3) {
			logEmail.append("- Obs: tem registros de afastamentos informado no periodo menor que 3");
			InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
			NeoObject noAJ = tarefaAJ.createNewInstance();
			EntityWrapper ajWrapper = new EntityWrapper(noAJ);

			GregorianCalendar datAfa = new GregorianCalendar();
			datAfa.setTime(datafa);

			ajWrapper.findField("numemp").setValue(NeoUtils.safeLong(numemp));
			ajWrapper.findField("tipcol").setValue(tipcol);
			ajWrapper.findField("numcad").setValue(NeoUtils.safeLong(numcad));
			ajWrapper.findField("numcpf").setValue(numCpf);
			ajWrapper.findField("datafa").setValue(datAfa);
			ajWrapper.findField("horafa").setValue(horafa);
			ajWrapper.findField("tarefa").setValue("Afastamento Informado");
			ajWrapper.findField("SitAfa").setValue(sitafa);
			ajWrapper.findField("CauDem").setValue(caudem);

			PersistEngine.persist(noAJ);
			return;
		    } else {
			logEmail.append("- Obs: NAO tem registro de afastamento informado no periodo. [count=" + count + "]");
		    }
		}
	    }

	    String descricao = "";
	    descricao = " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomFun + "<br>";
	    descricao = descricao + " <strong>Cargo:</strong> " + titred + "<br>";
	    descricao = descricao + " <strong>Posto:</strong> " + lotorn + " - " + nomloc + "<br>";
	    descricao = descricao + " <strong>Responsável:</strong> " + nomfis + "<br>";

	    GregorianCalendar dataInicioAfastamento = new GregorianCalendar();
	    GregorianCalendar dataFimAfastamento = new GregorianCalendar();
	    dataInicioAfastamento.setTime(datafa);
	    dataFimAfastamento.setTime(datter);

	    System.out.println("dataInicioAfastamento: " + NeoDateUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy"));
	    System.out.println("dataFimAfastamento: " + NeoDateUtils.safeDateFormat(dataFimAfastamento, "dd/MM/yyyy"));

	    if (dataInicioAfastamento.equals(dataFimAfastamento)) {
		descricao = descricao + " <strong>Data da falta:</strong> " + NeoDateUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy") + "<br>";
	    } else {
		descricao = descricao + " <strong>Período da falta:</strong> " + NeoDateUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy") + " Ã  " + NeoDateUtils.safeDateFormat(dataFimAfastamento, "dd/MM/yyyy")
			+ "<br>";
	    }

	    InstantiableEntityInfo insAfastamento = AdapterUtils.getInstantiableEntityInfo("jaJustificativaAfastamento");
	    NeoObject objAfastamento = insAfastamento.createNewInstance();
	    EntityWrapper wAfastamento = new EntityWrapper(objAfastamento);

	    NeoPaper papelSolicitante = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "jaSolicitante"));

	    NeoUser solicitanteInicial = null;

	    for (NeoUser user : papelSolicitante.getUsers()) {
		solicitanteInicial = user;
		break;
	    }
	    NeoPaper executorInicial = null;
	    if (codreg == 9) {
		executorInicial = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "jaExecutorJustificativaAsseio"));
	    } else {
		executorInicial = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "jaExecutorJustificativa"));
	    }

	    wAfastamento.setValue("executor", executorInicial);
	    wAfastamento.setValue("solicitante", solicitanteInicial);
	    wAfastamento.setValue("dataIniAfa", dataInicioAfastamento);
	    wAfastamento.setValue("dataFimAfa", dataFimAfastamento);

	    // INICIO - Criar objeto do colaborador cobertura para o R030
	    QLOpFilter qlNumCad = new QLOpFilter("numcad", "=", Long.parseLong(numcad));
	    QLOpFilter qlNumEmp = new QLOpFilter("numemp", "=", Long.parseLong(numemp));
	    QLGroupFilter groupFilterColaborador = new QLGroupFilter("AND");
	    groupFilterColaborador.addFilter(qlNumCad);
	    groupFilterColaborador.addFilter(qlNumEmp);
	    List<NeoObject> objsColaborador = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHRFUN"), groupFilterColaborador);
	    for (NeoObject objCol : objsColaborador) {
		wAfastamento.setValue("colaborador", objCol);
		break;
	    }

	    // Inicio - Start processo
	    QLEqualsFilter equal = new QLEqualsFilter("Name", "[JA] Justificativa de Afastamento");
	    ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
	    wAfastamento.setValue("isProcessoAutomatico", true);
	    final WFProcess processo = processModel.startProcess(objAfastamento, false, null, null, null, null, solicitanteInicial);
	    processo.setSaved(true);
	    PersistEngine.persist(processo);
	    PersistEngine.commit(true);

	    try {
		new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitanteInicial, true);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	    // FIM - Start processo

	    InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
	    NeoObject noAJ = tarefaAJ.createNewInstance();
	    EntityWrapper ajWrapper = new EntityWrapper(noAJ);

	    try {
		GregorianCalendar datAfa = new GregorianCalendar();
		datAfa.setTime(datafa);

		ajWrapper.setValue("numemp", NeoUtils.safeLong(numemp));
		ajWrapper.setValue("tipcol", tipcol);
		ajWrapper.setValue("numcad", NeoUtils.safeLong(numcad));
		ajWrapper.setValue("numcpf", numcpf);
		ajWrapper.setValue("datafa", datAfa);
		ajWrapper.setValue("horafa", horafa);
		ajWrapper.setValue("tarefa", processo.getCode());
		ajWrapper.setValue("SitAfa", sitafa);
		ajWrapper.setValue("CauDem", caudem);

		PersistEngine.persist(noAJ);
		logEmail.append(" - reg no eform Ok. ");
	    } catch (Exception e) {
		System.out.println("Regra: Falta não justificada - erro ao registrar no eform ");
		e.printStackTrace();
		logEmail.append(" - reg no eform Falhou. ");
	    }

	    System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: Falta não justificada - Responsavel " + executorInicial.getCode() + " Colaborador: " + numemp + "/"
		    + numcad + " - Data: " + NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy"));
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	} finally {
	    OrsegupsUtils.closeConnection(conn, stCount, rsCount);
	}

    }

}
