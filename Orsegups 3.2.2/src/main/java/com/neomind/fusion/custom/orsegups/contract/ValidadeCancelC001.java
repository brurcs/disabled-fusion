package com.neomind.fusion.custom.orsegups.contract;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEvent;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEventListener;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.contract.ValidadeCancelC001
public class ValidadeCancelC001 implements WorkflowValidateCancelEventListener {

	@Override
	public void validateCancel(WorkflowValidateCancelEvent event)
			throws WorkflowException {
		EntityWrapper wrapper = event.getWrapper();
		WFProcess proc = event.getProcess();
		System.out.println("validando cancelameto da tarefa : "+proc.getCode());
		if (true){
			throw new WorkflowException("O processo "+proc.getCode()+" não pode ser cancelado. Para cancelar solicite à TI via tarefa simples.");
		}
		
	}

}


