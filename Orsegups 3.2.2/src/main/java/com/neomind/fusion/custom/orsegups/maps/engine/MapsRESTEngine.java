package com.neomind.fusion.custom.orsegups.maps.engine;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.maps.vo.MapsPainelRotasVO;
import com.neomind.fusion.custom.orsegups.maps.vo.RotaVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class MapsRESTEngine {

    SIGMAUtilsEngine sigmaEngine = new SIGMAUtilsEngine();

    /**
     * Retorna lista de viaturas ativas na frota e em uso, disponiveis para
     * controle de rota
     * 
     * @return
     */
    public List<MapsPainelRotasVO> getViaturasDisponiveis() {

	// long inicio = Calendar.getInstance().getTimeInMillis();

	Map<String, List<ViaturaVO>> viaturas = new HashMap<String, List<ViaturaVO>>();
	Map<String, Boolean> controlarRegional = new HashMap<String, Boolean>();

	List<MapsPainelRotasVO> retorno = new ArrayList<MapsPainelRotasVO>();

	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	groupFilter.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));

	/**
	 * Lista de viaturas ativas e em uso na frota
	 */
	List<NeoObject> listOTSViatura = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), groupFilter);

	if (listOTSViatura != null && !listOTSViatura.isEmpty()) {

	    for (NeoObject viatura : listOTSViatura) {

		EntityWrapper wViatura = new EntityWrapper(viatura);

		String viaturaFusion = (String) wViatura.getValue("motorista");

		String codigoViatura = (String) wViatura.getValue("codigoViatura");

		boolean controlarRota = (boolean) wViatura.getValue("controlarRota");

		String rotaEfetiva = (String) wViatura.getValue("rotaEfetiva");

		if (!viaturaFusion.contains("- TEC") && !viaturaFusion.contains("-TEC") && !viaturaFusion.contains("-INSP") && !viaturaFusion.contains("-MOTO") && !viaturaFusion.contains("OFFICE") && !viaturaFusion.contains(" ENG ") && !codigoViatura.isEmpty() && codigoViatura.length() <= 6) {

		    ViaturaVO novaViatura = new ViaturaVO();

		    String nomeViatura = sigmaEngine.getNomeViaturaSigma(wViatura.findField("codigoViatura").getValueAsString());

		    if (!nomeViatura.isEmpty()) {

			String regional = nomeViatura.substring(0, 3);
			novaViatura.setNomeMotorista(nomeViatura);
			novaViatura.setCodigoViatura(codigoViatura);
			novaViatura.setControlarRota(controlarRota);
			novaViatura.setRotaEfetiva(rotaEfetiva);

			@SuppressWarnings("unchecked")
			List<NeoObject> listRotas = (List<NeoObject>) wViatura.getValue("listaRotasEfetivas");

			if (listRotas != null && !listRotas.isEmpty()) {

			    List<RotaVO> listaRotasEfetivas = new ArrayList<RotaVO>();

			    for (NeoObject objRota : listRotas) {
				EntityWrapper wRota = new EntityWrapper(objRota);

				String nomeRota = (String) wRota.getValue("nomeRota");

				RotaVO v = new RotaVO();

				v.setNomeRota(nomeRota);

				listaRotasEfetivas.add(v);

			    }

			    novaViatura.setListaRotasEfetivas(listaRotasEfetivas);
			}

			Map<String, String> mapaRota = sigmaEngine.getRegionalRota(nomeViatura);

			novaViatura.setRotasDisponiveis(this.getRotasRegional(mapaRota.get(SIGMAUtilsEngine.REGIONAL)));

			if (controlarRota) {
			    if (!controlarRegional.containsKey(regional)) {
				controlarRegional.put(regional, Boolean.TRUE);
			    }
			}

			if (viaturas.containsKey(regional)) {
			    List<ViaturaVO> listaViaturas = viaturas.get(regional);
			    listaViaturas.add(novaViatura);
			} else {
			    List<ViaturaVO> listaViaturas = new ArrayList<ViaturaVO>();
			    listaViaturas.add(novaViatura);
			    viaturas.put(regional, listaViaturas);
			}

		    }

		}

	    }

	    for (String key : viaturas.keySet()) {
		MapsPainelRotasVO rota = new MapsPainelRotasVO();

		Boolean controlarRotaRegional = controlarRegional.get(key);

		if (controlarRotaRegional != null) {
		    rota.setControlarRotaRegional(true);
		} else {
		    rota.setControlarRotaRegional(false);
		}

		rota.setNomeRegional(key);
		rota.setViaturas(viaturas.get(key));
		retorno.add(rota);
	    }

	}
	// long fim = Calendar.getInstance().getTimeInMillis();

	// System.out.println("Tempo total via Hibernate: "+((fim-inicio)/1000));

	return retorno;

    }

    /**
     * Faz ativaÃ§Ã£o/desativaÃ§Ã£o do controle da rota da viatura
     * 
     * @param viatura
     * @param acao
     *            ; 1 Ativar; 0 Desativar
     * @return true para sucesso na operaÃ§Ã£o, false para erro na operaÃ§Ã£o
     */
    public boolean setControleRota(long viatura, int acao) {

	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	groupFilter.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
	groupFilter.addFilter(new QLEqualsFilter("codigoViatura", String.valueOf(viatura)));

	List<NeoObject> listOTSViatura = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), groupFilter);

	if (listOTSViatura != null && !listOTSViatura.isEmpty()) {
	    NeoObject objViatura = listOTSViatura.get(0);

	    EntityWrapper wViatura = new EntityWrapper(objViatura);

	    if (acao == 1) {
		wViatura.setValue("controlarRota", Boolean.TRUE);
	    } else if (acao == 0) {
		wViatura.setValue("controlarRota", Boolean.FALSE);
	    }

	    try {
		PersistEngine.persist(objViatura);
	    } catch (Exception e) {
		e.printStackTrace();
		return false;
	    }

	    return true;

	} else {
	    return false;
	}

    }

    private List<String> getRotasRegional(String regional) {

	List<String> retorno = new ArrayList<String>();

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	try {

	    String sql = " SELECT NM_VIATURA FROM VIATURA WITH(NOLOCK) WHERE FG_ATIVO=1 AND NM_VIATURA LIKE '" + regional + "%' ";

	    conn = PersistEngine.getConnection("SIGMA90");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    while (rs.next()) {

		String viatura = rs.getString(1);

		if (!viatura.contains("- TEC") && !viatura.contains("-TEC") && !viatura.contains("-INSP") && !viatura.contains("-MOTO") && !viatura.contains("-ADM") && !viatura.contains("OFFICE") && !viatura.contains("ENG") && !viatura.contains("ME.")) {

		    Map<String, String> regionalRota = sigmaEngine.getRegionalRota(viatura);

		    if (!regionalRota.isEmpty()) {
			String novaRota = regionalRota.get(SIGMAUtilsEngine.REGIONAL).trim() + "-" + regionalRota.get(SIGMAUtilsEngine.ROTA).trim();
			if (!retorno.contains(novaRota)) {
			    retorno.add(novaRota);
			}
		    }

		}
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return retorno;
    }

    public String setRotaEfetiva(long viatura, String rota) {

	String retorno = "";

	String regional = rota.substring(0, 3);

	boolean encontrouAlguemNaRota = false;

	QLGroupFilter filtroViaturaRegional = new QLGroupFilter("AND");
	filtroViaturaRegional.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	filtroViaturaRegional.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
	filtroViaturaRegional.addFilter(new QLEqualsFilter("codigoRegional", regional));

	List<NeoObject> listViaturaRegional = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filtroViaturaRegional);

	if (listViaturaRegional != null && !listViaturaRegional.isEmpty()) {

	    for (NeoObject objViatura : listViaturaRegional) {
		EntityWrapper wViatura = new EntityWrapper(objViatura);

		@SuppressWarnings("unchecked")
		List<NeoObject> listRotas = (List<NeoObject>) wViatura.getValue("listaRotasEfetivas");

		if (listRotas != null && !listRotas.isEmpty()) {
		    for (NeoObject objRota : listRotas) {
			EntityWrapper wRota = new EntityWrapper(objRota);

			String rotaNaLista = (String) wRota.getValue("nomeRota");

			if (rotaNaLista.equalsIgnoreCase(rota)) {
			    encontrouAlguemNaRota = true;

			    String nomeViaturaNaFrota = (String) wViatura.getValue("motorista");

			    retorno = "A viatura " + nomeViaturaNaFrota + " encontra-se ativa nessa rota.";

			}
		    }
		}
	    }
	}

	if (!encontrouAlguemNaRota) {
	    QLGroupFilter filtroViatura = new QLGroupFilter("AND");
	    filtroViatura.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	    filtroViatura.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
	    filtroViatura.addFilter(new QLEqualsFilter("codigoViatura", String.valueOf(viatura)));

	    List<NeoObject> listViatura = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filtroViatura);

	    if (listViatura != null && !listViatura.isEmpty()) {
		NeoObject objViatura = listViatura.get(0);

		EntityWrapper wViatura = new EntityWrapper(objViatura);

		InstantiableEntityInfo OTSRota = AdapterUtils.getInstantiableEntityInfo("OTSItemRotaEfetiva");
		NeoObject novaRota = OTSRota.createNewInstance();
		EntityWrapper wRota = new EntityWrapper(novaRota);
		wRota.setValue("nomeRota", rota);
		PersistEngine.persist(novaRota);
		wViatura.findField("listaRotasEfetivas").addValue(novaRota);

		try {
		    PersistEngine.persist(objViatura);
		    retorno = "OK! AlteraÃ§Ã£o gravada com sucesso.";
		} catch (Exception e) {
		    e.printStackTrace();
		    retorno = "Erro ao gravar alteraÃ§Ã£o. Por favor tente novamente.";
		}

	    } else {
		retorno = "Parece que a viatura nÃ£o encontra-se mais em uso.";
	    }

	}

	return retorno;
    }

    public String removerRotaEfetiva(long viatura, String rota) {

	String retorno = "";

	QLGroupFilter filtroViaturaNaRota = new QLGroupFilter("AND");
	filtroViaturaNaRota.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	filtroViaturaNaRota.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
	filtroViaturaNaRota.addFilter(new QLEqualsFilter("codigoViatura", String.valueOf(viatura)));
	filtroViaturaNaRota.addFilter(new QLEqualsFilter("rotaEfetiva", rota));

	List<NeoObject> listViaturaNaRota = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filtroViaturaNaRota);

	if (listViaturaNaRota != null && !listViaturaNaRota.isEmpty()) {
	    NeoObject objViatura = listViaturaNaRota.get(0);

	    EntityWrapper wViatura = new EntityWrapper(objViatura);

	    wViatura.setValue("rotaEfetiva", null);

	    try {
		PersistEngine.persist(objViatura);
		retorno = "OK! AlteraÃ§Ã£o gravada com sucesso.";
	    } catch (Exception e) {
		e.printStackTrace();
		retorno = "Ops! Erro ao gravar alteraÃ§Ã£o. Por favor tente novamente.";
	    }

	} else {
	    retorno = "Ops! Parece que a viatura nÃ£o estÃ¡ mais em uso.";
	}

	return retorno;
    }

    public boolean setControleTodasRotas(int acao) {

	Connection conn = null;
	Statement stmt = null;

	try {
	    String sql = "";

	    if (acao == 0) {
		sql = "UPDATE D_OTSViatura SET controlarRota=0 WHERE ativaNaFrota=1 ";
	    } else if (acao == 1) {
		sql = "UPDATE D_OTSViatura SET controlarRota=1 WHERE ativaNaFrota=1 ";
	    }

	    conn = PersistEngine.getConnection("");

	    stmt = conn.createStatement();

	    int rs = stmt.executeUpdate(sql);

	    if (rs > 0) {
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, null);
	}

	return false;
    }

}
