package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.CoberturaPostoMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.ColaboradorMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.ColaboradoresDemitidosPostosMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.ConsideracoesMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.DiariaMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.MedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.PostosMedicaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class OrsegupsMedicaoUtilsMensal
{

	public static List<NeoObject> validaAfastamento(Long numEmp, Long numCad, Long tipCol, GregorianCalendar datRef, Long numLoc)

	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsAfastamento = null;
		PreparedStatement stAfastamento = null;
		NeoObject consideracoes = null;
		NeoObject consideracoesBKP = null;
		List<NeoObject> listConsideracoes = new ArrayList<NeoObject>();

		GregorianCalendar periodoFim = new GregorianCalendar();
		periodoFim = (GregorianCalendar) datRef.clone();
		periodoFim.set(GregorianCalendar.DATE, 1);
		periodoFim.add(GregorianCalendar.MONTH, 1);
		String dataFiltro = retornaDataFormatoSapiensEVetorh(periodoFim);

		GregorianCalendar datini = new GregorianCalendar();
		datini = (GregorianCalendar) periodoFim.clone();
		datini.set(GregorianCalendar.DATE, 1);

		datini.add(GregorianCalendar.MONTH, -1);
		String dataFiltroIni = retornaDataFormatoSapiensEVetorh(datini);

		StringBuilder queryAfastamento = new StringBuilder();
		try
		{
			queryAfastamento.append(" SELECT afa.sitafa,SIT2.desSit,afa.obsafa,CONVERT(VARCHAR(15),AFA.DATAFA,103) AS DATAFA,CONVERT(VARCHAR(15),AFA.DatTer,103) AS DatTer,CONVERT(VARCHAR(15),COB.USU_DATALT,103) as USU_DATALT,isnull(COB.USU_NUMCADCOB,0) AS COLEFE,COB.USU_NUMEMPCOB,COB.USU_NUMCAD AS COLSUB,FUN.NOMFUN,");
			queryAfastamento.append("  FUN2.nomfun as funCob,FUN2.numemp as EMPSUB,FUN.numemp,SIT.DesSit,CAR.TitRed,CONVERT(VARCHAR(15),fun2.datadm,103) as datadm,");
			queryAfastamento.append(" CASE WHEN (EXISTS(SELECT 1 FROM USU_T038COBFUN WHERE USU_NUMLOCTRA = ? AND CAST(USU_DATALT AS DATE) = AFA.DATAFA AND (USU_NUMCADCOB =0 OR USU_NUMCADCOB IS NULL)))");
			queryAfastamento.append("  THEN 1 ELSE 0 END AS COBPOS ");
			queryAfastamento.append(" FROM R038AFA AFA");
			queryAfastamento.append(" LEFT JOIN USU_T038COBFUN COB ON COB.USU_NUMCADCOB = AFA.NUMCAD AND COB.USU_NUMEMPCOB = AFA.NUMEMP AND COB.USU_TIPCOLCOB = AFA.TIPCOL AND AFA.DATAFA = CAST(COB.USU_DATALT AS DATE)");
			queryAfastamento.append(" LEFT JOIN R034FUN FUN ON FUN.NUMCAD = COB.USU_NUMCADCOB AND FUN.NUMEMP = COB.USU_NUMEMPCOB AND FUN.TIPCOL = COB.USU_TIPCOLCOB");
			queryAfastamento.append(" LEFT JOIN R034FUN FUN2 ON FUN2.NUMCAD = COB.usu_numcad AND FUN2.NUMEMP = COB.usu_numemp AND FUN2.TIPCOL = COB.usu_tipcol");
			queryAfastamento.append(" LEFT JOIN R010SIT SIT ON SIT.CodSit = FUN2.SitAfa");
			queryAfastamento.append(" LEFT JOIN R010SIT SIT2 ON SIT2.CodSit = afa.SitAfa");
			queryAfastamento.append(" LEFT JOIN R038HCA HCA ON HCA.NumEmp = FUN.numemp AND HCA.NumCad = FUN.numcad AND HCA.TipCol = FUN.tipcol AND HCA.DatAlt =");
			queryAfastamento.append(" (SELECT MAX(DATALT) FROM R038HCA HCA2 WHERE HCA2.NumEmp = HCA.NumEmp AND HCA2.TipCol = HCA.TipCol AND HCA2.NumCad = HCA.NumCad)");
			queryAfastamento.append(" LEFT JOIN R024CAR CAR ON CAR.CodCar = HCA.CodCar and car.estcar = 1");
			queryAfastamento.append(" WHERE AFA.NUMCAD = ? AND AFA.NUMEMP = ? AND AFA.TIPCOL = ? AND ( (AFA.DATAFA >= ? and AFA.DATAFA < ?) OR (AFA.DATTER >= ? and AFA.DATTER < ?) ) ");

			stAfastamento = connVetorh.prepareStatement(queryAfastamento.toString());

			stAfastamento.setLong(1, numLoc);
			stAfastamento.setLong(2, numCad);
			stAfastamento.setLong(3, numEmp);
			stAfastamento.setLong(4, tipCol);
			stAfastamento.setString(5, dataFiltroIni);
			stAfastamento.setString(6, dataFiltro);
			stAfastamento.setString(7, dataFiltroIni);
			stAfastamento.setString(8, dataFiltro);
			String datAfa = "";

			rsAfastamento = stAfastamento.executeQuery();
			while (rsAfastamento.next())
			{
				datAfa = rsAfastamento.getString("DATTER");
				consideracoes = AdapterUtils.createNewEntityInstance("Medconsideracoes");
				EntityWrapper wConsideracoes = new EntityWrapper(consideracoes);
				if (datAfa.equals("1900/12/31"))
				{
					datAfa = "indeterminado";
				}
				if (rsAfastamento.getLong("COLEFE") != 0L)
				{
					wConsideracoes.setValue("descricao", "Colaborador possui uma Cobertura " + " do colaborador " + rsAfastamento.getLong("EMPSUB") + "/" + rsAfastamento.getLong("COLSUB") + " - " + rsAfastamento.getString("funCob") + " devido a sua falta no periodo de :" + rsAfastamento.getString("DATAFA") + " até " + rsAfastamento.getString("DATTER") + " Obs: " + rsAfastamento.getString("desSit"));
					wConsideracoes.setValue("isFalta", false);
					listConsideracoes.add(consideracoes);
					PersistEngine.persist(consideracoes);
					consideracoesBKP = consideracoes;
					PersistEngine.persist(consideracoesBKP);
				}
				else
				{
					if ((rsAfastamento.getLong("sitafa") == 15) || (rsAfastamento.getLong("sitafa") == 24) || (rsAfastamento.getLong("sitafa") == 66) || (rsAfastamento.getLong("sitafa") == 80) || (rsAfastamento.getLong("sitafa") == 86))
					{
						Long qtdFaltas = retornaPeriodoDoAfastamento(rsAfastamento.getString("DATAFA"), rsAfastamento.getString("DATTER"));

						for (int i = 0; i < qtdFaltas; i++)
						{
							wConsideracoes.setValue("isFalta", true);
							wConsideracoes.setValue("descricao", "Colaborador possui um falta no dia de " + rsAfastamento.getString("DATAFA") + " sem cobertura. Obs: " + rsAfastamento.getString("desSit"));
							listConsideracoes.add(consideracoes);
						}
					}
					else
					{
						wConsideracoes.setValue("isFalta", false);
						wConsideracoes.setValue("descricao", "Colaborador possui um Afastamento no periodo de " + rsAfastamento.getString("DATAFA") + " até " + rsAfastamento.getString("DATTER") + " sem cobertura. Obs: " + rsAfastamento.getString("desSit"));
						listConsideracoes.add(consideracoes);
					}

					PersistEngine.persist(consideracoes);
					consideracoesBKP = consideracoes;
				}
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stAfastamento, rsAfastamento);
		}
		return listConsideracoes;
	}

	public static Long cargaHorariaDoPosto(Long usu_codemp, Long usu_codfil, Long usu_numctr, Long usu_numpos)
	{
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");

		ResultSet rsPosto = null;
		PreparedStatement stPosto = null;
		StringBuilder queryCargaHorPos = new StringBuilder();
		Long horIni = 0L;
		Long auxIni = 0L;
		Long horFim = 0L;
		Long diaSem = 0L;
		Long carga = 0L;
		Long intervalo = 0L;
		int cont = 0;
		try
		{
			queryCargaHorPos.append(" SELECT USU_HORINI,USU_HORFIM,USU_DIASEM FROM USU_T160JOR WHERE USU_NUMCTR = ? AND USU_NUMPOS = ? AND USU_CODEMP = ? AND USU_CODFIL = ? " + "order by USU_DIASEM,USU_HORINI,USU_HORFIM ");
			stPosto = connSapiens.prepareStatement(queryCargaHorPos.toString());
			stPosto.setLong(1, usu_numctr);
			stPosto.setLong(2, usu_numpos);
			stPosto.setLong(3, usu_codemp);
			stPosto.setLong(4, usu_codfil);
			rsPosto = stPosto.executeQuery();
			while (rsPosto.next())
			{

				horIni = rsPosto.getLong("USU_HORINI");
				horFim = rsPosto.getLong("USU_HORFIM");

				if (diaSem != rsPosto.getLong("USU_DIASEM"))
				{

					if (rsPosto.getLong("USU_HORFIM") == 1439)
					{
						horFim = 1440L;
					}
					else
					{
						horFim = rsPosto.getLong("USU_HORFIM");
					}
					carga = (horFim - horIni) / 60;
					auxIni = horIni;
					diaSem = rsPosto.getLong("USU_DIASEM");
					intervalo = horFim;
					cont++;
					if (cont == 2)
					{
						intervalo = 0L;
					}
				}
				else
				{
					if (rsPosto.getLong("USU_HORFIM") == 1439)
					{
						horFim = 1440L;
					}
					else
					{
						horFim = rsPosto.getLong("USU_HORFIM");
					}
					horFim = rsPosto.getLong("USU_HORFIM");
					carga = (horFim - auxIni) / 60;
					intervalo = (horIni - intervalo) / 60;
					cont++;
				}
				if (cont == 2)
				{
					if (carga >= 8L)
					{
						carga = carga - intervalo;
					}
					break;
				}
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, stPosto, rsPosto);
		}

		return carga;
	}

	public static String getEscalaColaborador(Long numemp, Long numcad, Long tipcol)
	{
		Connection connSapiens = PersistEngine.getConnection("VETORH");
		String escala = "";
		ResultSet rsPosto = null;
		PreparedStatement stPosto = null;
		StringBuilder queryEscala = new StringBuilder();

		try
		{
			queryEscala.append("select top 1 (ESC.NomEsc+'/'+(convert(varchar,hes.CodTma))) as escalda from R038HES hes ");
			queryEscala.append(" inner join R006ESC ESC ON ESC.CodEsc = HES.CodEsc ");
			queryEscala.append(" where hes.NumCad = ? and hes.NumEmp = ? and hes.tipcol = ? ");
			queryEscala.append(" order by datalt desc ");

			stPosto = connSapiens.prepareStatement(queryEscala.toString());
			stPosto.setLong(1, numcad);
			stPosto.setLong(2, numemp);
			stPosto.setLong(3, tipcol);
			rsPosto = stPosto.executeQuery();

			while (rsPosto.next())
			{
				escala = rsPosto.getString("escalda");
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, stPosto, rsPosto);
		}

		return escala;
	}

	public static List<String> pontoColaborador(Long numEmp, Long numCad, Long tipCol, GregorianCalendar datAcc)
	{
		List<String> ponto = new ArrayList<String>();
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsPonto = null;
		PreparedStatement stPonto = null;
		StringBuilder queryPonto = new StringBuilder();

		queryPonto.append(" SELECT ACC.HORACC/60 AS HORA,ISNULL(JMA.DESJMA,'')AS DESJMA , CAST(ROUND(((ACC.HORACC/60.0) - (ACC.HORACC/60)) * 60,2 ) AS INT) AS MINUTOS FROM R038HCH HCH");
		queryPonto.append(" INNER JOIN R070ACC ACC ON ACC.NUMCRA = HCH.NUMCRA ");
		queryPonto.append(" AND DATINI =   (SELECT MAX(DATINI) FROM R038HCH HCH2 WHERE HCH2.NUMCRA = HCH.NUMCRA");
		queryPonto.append(" AND DATACC = ACC.DATACC )");
		queryPonto.append(" INNER JOIN R070ACC ENTRADA ON ENTRADA.NUMCRA = ACC.NUMCRA");
		queryPonto.append("	   AND ENTRADA.DATACC = ACC.DATACC ");
		queryPonto.append(" AND ACC.HORACC = ENTRADA.HORACC ");
		queryPonto.append(" AND ENTRADA.HORACC = (SELECT MIN(horacc) FROM R070ACC A where A.numcra = ACC.NUMCRA  AND A.DATACC = ACC.DATACC AND A.TIPACC = 100)");
		queryPonto.append(" LEFT JOIN R076JMA JMA ON JMA.CODJMA = ACC.USU_CODJMA ");
		queryPonto.append(" WHERE HCH.NUMCAD = ? AND HCH.NUMEMP = ? AND HCH.TIPCOL = ? AND ACC.DATACC = ? AND ACC.TIPACC = 100");
		queryPonto.append(" UNION ");
		queryPonto.append(" SELECT ACC.HORACC/60 AS HORA,ISNULL(JMA.DESJMA,'')AS DESJMA , CAST(ROUND(((ACC.HORACC/60.0) - (ACC.HORACC/60)) * 60,2 ) AS INT) FROM R038HCH HCH");
		queryPonto.append(" INNER JOIN R070ACC ACC ON ACC.NUMCRA = HCH.NUMCRA ");
		queryPonto.append(" AND DATINI =   (SELECT MAX(DATINI) FROM R038HCH HCH2 WHERE HCH2.NUMCRA = HCH.NUMCRA");
		queryPonto.append(" AND DATACC = ACC.DATACC )	");
		queryPonto.append(" INNER JOIN R070ACC SAIDA ON SAIDA.NUMCRA = ACC.NUMCRA");
		queryPonto.append(" AND SAIDA.DATACC = ACC.DATACC ");
		queryPonto.append(" AND ACC.HORACC = SAIDA.HORACC ");
		queryPonto.append(" AND SAIDA.HORACC = (SELECT MAX(horacc) FROM R070ACC A where A.numcra = ACC.NUMCRA AND a.DATACC = ACC.DATACC AND a.TIPACC = 100)");
		queryPonto.append(" LEFT JOIN R076JMA JMA ON JMA.CODJMA = ACC.USU_CODJMA ");
		queryPonto.append(" WHERE HCH.NUMCAD = ? AND HCH.NUMEMP = ? AND HCH.TIPCOL = ? AND ACC.DATACC = ? AND ACC.TIPACC = 100");
		try
		{

			stPonto = connVetorh.prepareStatement(queryPonto.toString());
			stPonto.setLong(1, numCad);
			stPonto.setLong(2, numEmp);
			stPonto.setLong(3, tipCol);
			stPonto.setString(4, retornaDataFormatoSapiensEVetorh(datAcc));
			stPonto.setLong(5, numCad);
			stPonto.setLong(6, numEmp);
			stPonto.setLong(7, tipCol);
			stPonto.setString(8, retornaDataFormatoSapiensEVetorh(datAcc));
			rsPonto = stPonto.executeQuery();

			String hora = " ";
			String minutos = " ";
			while (rsPonto.next())
			{
				hora = rsPonto.getString("HORA");
				minutos = rsPonto.getString("MINUTOS");
				if (rsPonto.getLong("HORA") < 10)
				{
					hora = "0" + hora;
				}

				if (rsPonto.getLong("MINUTOS") < 10)
				{
					minutos = "0" + minutos;
				}
				ponto.add(hora + ":" + minutos);
				String desjma = rsPonto.getString("DESJMA");
				if ((desjma != null) && (!desjma.equals("")))
				{
					ponto.add(rsPonto.getString("DESJMA"));
				}
				else
				{
					ponto.add("branco");
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stPonto, rsPonto);
		}

		return ponto;
	}

	public static List<String> empresas()
	{
		List<String> empresas = new ArrayList<String>();
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsEmpresa = null;
		PreparedStatement stEmpresa = null;
		StringBuilder queryEmpresa = new StringBuilder();
		empresas.add("Selecione a empresa");

		queryEmpresa.append(" select NumEmp,NomEmp from r030emp where numemp in(1,2,6,7,8,12,13,15,17,18,19,21,22) order by 1");
		try
		{
			stEmpresa = connVetorh.prepareStatement(queryEmpresa.toString());
			rsEmpresa = stEmpresa.executeQuery();
			while (rsEmpresa.next())
			{
				empresas.add(rsEmpresa.getString("NumEmp") + " - " + rsEmpresa.getString("NomEmp"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stEmpresa, rsEmpresa);
		}

		return empresas;
	}

	public static List<String> competencias()
	{
		List<String> competencias = new ArrayList<String>();
		GregorianCalendar datAtu = new GregorianCalendar();
		int cont = 0;
		competencias.add("Selecione a competência...");
		for (cont = 1; cont <= 6; cont++)
		{
			competencias.add(NeoUtils.safeDateFormat(datAtu, "MM/yyyy"));
			datAtu.add(GregorianCalendar.MONTH, -1);
		}

		return competencias;
	}

	public static boolean getRegionaisDePesquisa(int codReg)
	{
		NeoUser usuario = PortalUtil.getCurrentUser();
		NeoPaper responsavelSOO = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelMedicaoDeContratosSaoJose"));

		for (NeoUser usr : responsavelSOO.getAllUsers())
		{
			if (usuario.getNeoId() == usr.getNeoId())
			{
				return true;
			}
		}

		return false;
	}

	public static String retornaDataFormatoSapiensEVetorh(GregorianCalendar data)
	{
		String novaData = "";

		if (data == null)
		{
			/*
			 * 31/12/1900 o sapiens\Vetorh entende essa data como vazia
			 */
			data = new GregorianCalendar();
			data.set(data.YEAR, 1900);
			data.set(data.MONTH, 11);
			data.set(data.DAY_OF_MONTH, 31);
			data.set(Calendar.HOUR_OF_DAY, 00);
			data.set(Calendar.MINUTE, 00);
			data.set(Calendar.SECOND, 00);
		}

		if (data != null)
		{
			novaData = NeoUtils.safeDateFormat(data, "yyyy-MM-dd");
			novaData = novaData + " 00:00:00";
		}

		return novaData;
	}

	public static void registaBKPMedicaoDeContratos(NeoObject obj, String eformBKP)
	{

		EntityWrapper wObj = new EntityWrapper(obj);

		if (eformBKP.equals("MEDMedicaoDeContratos"))
		{
			NeoObject medicaoBKP = AdapterUtils.createNewEntityInstance("MEDMedicaoDeContratosBKP");

			EntityWrapper wMedicaoBKPBKP = new EntityWrapper(medicaoBKP);
			wMedicaoBKPBKP.setValue("cliente", wObj.findField("cliente").getValue());
			wMedicaoBKPBKP.setValue("contrato", wObj.findField("contrato").getValue());
			wMedicaoBKPBKP.setValue("codEmpresaContratada", wObj.findField("codEmpresaContratada").getValue());
			wMedicaoBKPBKP.setValue("empresaContratada", wObj.findField("empresaContratada").getValue());
			wMedicaoBKPBKP.setValue("responsavel", wObj.findField("responsavel").getValue());
			wMedicaoBKPBKP.setValue("periodo", wObj.findField("periodo").getValue());
			wMedicaoBKPBKP.setValue("codRegional", wObj.findField("codRegional").getValue());
			wMedicaoBKPBKP.setValue("regional", wObj.findField("regional").getValue());
			PersistEngine.persist(medicaoBKP);
		}

		if (eformBKP.equals("MEDColaboradores"))
		{
			NeoObject colaboradorMedicaoBKP = AdapterUtils.createNewEntityInstance("MEDColaboradoresBKP");
			EntityWrapper wColaboradorBKP = new EntityWrapper(colaboradorMedicaoBKP);
			List<NeoObject> consideracoes = new ArrayList<NeoObject>();
			List<NeoObject> consideracoesBKP = new ArrayList<NeoObject>();
			consideracoes = (List<NeoObject>) wObj.findField("listaConsideracoes").getValues();
			for (NeoObject consideracao : consideracoes)
			{
				EntityWrapper wConsideracoes = new EntityWrapper(consideracao);
				NeoObject medConsideracoesBKP = AdapterUtils.createNewEntityInstance("MedconsideracoesBKP");
				EntityWrapper wConsideracoesBKP = new EntityWrapper(medConsideracoesBKP);
				wConsideracoesBKP.setValue("descricao", wConsideracoes.findField("descricao").getValue());
				wConsideracoesBKP.setValue("isFalta", wConsideracoes.findField("isFalta").getValue());
				consideracoesBKP.add(medConsideracoesBKP);
				PersistEngine.persist(medConsideracoesBKP);

			}
			if (consideracoes.size() > 0)
			{
				wColaboradorBKP.setValue("listaConsideracoes", consideracoesBKP);
			}
			wColaboradorBKP.setValue("cargaHorPos", wObj.findField("cargaHorPos").getValue());
			wColaboradorBKP.setValue("funcao", wObj.findField("funcao").getValue());
			wColaboradorBKP.setValue("local", wObj.findField("local").getValue());
			wColaboradorBKP.setValue("numCadColaborador", wObj.findField("numCadColaborador").getValue());
			wColaboradorBKP.setValue("nomeColaborador", wObj.findField("nomeColaborador").getValue());
			wColaboradorBKP.setValue("datAdm", wObj.findField("datAdm").getValue());
			wColaboradorBKP.setValue("descMed", wObj.findField("descMed").getValue());
			wColaboradorBKP.setValue("situacao", wObj.findField("situacao").getValue());
			PersistEngine.persist(colaboradorMedicaoBKP);
		}
	}

	public static String enviaEmail(GregorianCalendar datRef) throws EmailException
	{
		HtmlEmail noUserEmail = new HtmlEmail();
		noUserEmail.setCharset("ISO-8859-1");
		try
		{
			String dataFormat = retornaDataFormatoSapiensEVetorh(datRef);
			dataFormat = dataFormat.substring(5, 7) + "/" + dataFormat.substring(0, 4);
			File arqRelatorio = getRelatorioMedicao(dataFormat);
			NeoStorage neoStorage = NeoStorage.getDefault();
			NeoFile neoFile = neoStorage.copy(arqRelatorio);
			neoFile.setName("Relatorio_Medicao.pdf");
			List<String> finale = new ArrayList<String>();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			finale.add(((Long) neoFile.getNeoId()).toString());
			paramMap.put("attachList", finale);
			List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("enviaEmailMedicaoDeContratosMensal"));
			String destinatario = "jorge.filho@orsegups.com.br ";
			String separador = ";";
			/*
			 * if (objs.size() > 0)
			 * {
			 * for (NeoObject neoObject : objs)
			 * {
			 * EntityWrapper medWrapper = new EntityWrapper(neoObject);
			 * destinatario += separador + medWrapper.findValue("email");
			 * }
			 * }
			 */

			FusionRuntime.getInstance().getMailEngine().sendEMail(destinatario, "/MedicaoDeContratos/emailRelatorioMedicaoDeContratosMensal.jsp", paramMap);
			noUserEmail.send();

		}
		catch (EmailException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static String InsereDemitidosNoPeriodo(Long numctr, GregorianCalendar datini, GregorianCalendar datfim) throws SQLException
	{
		String datiniString = retornaDataFormatoSapiensEVetorh(datini);
		String datfimString = retornaDataFormatoSapiensEVetorh(datfim);
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		String retorno = "";

		String sql = "select distinct(c.usu_numloc), a.numcad, a.numemp, c.usu_numpos, c.usu_numctr, fun.nomfun,convert(varchar, a.datAfa, 103) as datAfa from usu_t038cvs c " + " inner join R038HLO h on h.NumLoc = c.usu_numloc and h.TabOrg = 203 " + "  inner join R038AFA a on a.NumCad = h.NumCad and a.NumEmp = h.NumEmp " + "  and a.DatAfa >= '" + datiniString + "' and a.DatAfa <= '" + datfimString + "' " + "  and a.SitAfa = 7 "
				+ "  inner join R034fun fun on fun.numcad = a.numcad and fun.tipcol = a.tipcol and fun.numemp = a.numemp " + "  where c.usu_numctr = " + numctr;

		ResultSet rsAfa = null;
		PreparedStatement stAfa = null;
		stAfa = connVetorh.prepareStatement(sql.toString());
		rsAfa = stAfa.executeQuery();

		if (rsAfa.next())
		{
			NeoObject colDemObj = AdapterUtils.createNewEntityInstance("medColaboradoresDemitidosPorPosto");
			EntityWrapper wColDem = new EntityWrapper(colDemObj);
			ColaboradoresDemitidosPostosMedicaoVO colDem = new ColaboradoresDemitidosPostosMedicaoVO();
			colDem.setNumcad(rsAfa.getLong("numcad"));
			colDem.setNumemp(rsAfa.getLong("numemp"));
			colDem.setNumpos(rsAfa.getLong("usu_numpos"));
			colDem.setDatDmsString(rsAfa.getString("datAfa"));
			colDem.setNumctr(rsAfa.getLong("usu_numctr"));
			colDem.setNomfun(rsAfa.getString("nomfun"));

			try
			{
				colDem.setDatDms(OrsegupsUtils.stringToGregorianCalendar(colDem.getDatDmsString(), "dd/MM/yyyy"));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			wColDem.setValue("numcad", colDem.getNumcad());
			wColDem.setValue("numemp", colDem.getNumemp());
			wColDem.setValue("nomfun", colDem.getNomfun());
			wColDem.setValue("dataDms", colDem.getDatDms());
			wColDem.setValue("numpos", colDem.getNumpos());
			wColDem.setValue("numctr", colDem.getNumctr());

			PersistEngine.persist(colDemObj);

			if (retorno.equals(""))
			{
				retorno = "colaborador demitido;";
			}
		}

		return retorno;
	}

	public static File getRelatorioMedicao(String mesRef)
	{
		File fileProposta = null;
		Connection fusion = PersistEngine.getConnection("");
		String dataFiltro = "01/" + mesRef;

		GregorianCalendar datFilto = new GregorianCalendar();
		datFilto.setTime(NeoCalendarUtils.formatDate(dataFiltro, "dd/MM/yyyy"));
		datFilto.add(GregorianCalendar.MONTH, 1);
		dataFiltro = NeoCalendarUtils.formatDate(datFilto.getTime(), "yyyy/MM/dd");
		try
		{
			String sql = " SELECT ORN.NUMLOC,cliente,contrato,empresacontratada,responsavel, 	periodo,codRegional,regional,cargahorpos,funcao,local,nomecolaborador,	" + " descmed,situacao,C.datAdm, 	numcadColaborador,isFalta,descricao FROM D_MEDMedicaoDeContratos " + " M INNER JOIN D_MEDMedicaoDeContratos_listaColaboradores MC ON	MC.D_MEDMedicaoDeContratos_neoId = M.neoId	" + " INNER JOIN D_MEDColaboradores C ON 	C.NEOID = MC.listaColaboradores_neoId "
					+ " LEFT JOIN D_MEDColaboradores_listaConsideracoes CCN ON  	CCN.D_MEDColaboradores_neoId = MC.listaColaboradores_neoId	LEFT JOIN D_MEDConsideracoes CN ON CN.neoId = 	CCN.listaConsideracoes_neoId " + " INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R034FUN FUN ON fun.NUMCAD = C.numCadColaborador AND fun.NUMEMP = M.codEmpresaContratada "
					+ " INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM [FSOODB04\\SQL02].VETORH.DBO.R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT < '" + dataFiltro + "') " + " INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R006ESC esc ON esc.CodEsc = hes.CodEsc "
					+ " INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM [FSOODB04\\SQL02].VETORH.DBO.R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT < '" + dataFiltro + "') "
					+ " INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R016ORN ORN ON ORN.numloc = hlo.numloc AND ORN.TABORG = '203' " + " INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From [FSOODB04\\SQL02].VETORH.DBO.USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt < '" + dataFiltro + "') "
					+ " INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM [FSOODB04\\SQL02].VETORH.DBO.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc)";
			if (!mesRef.equals("") || (mesRef != null))
			{
				sql = sql + " WHERE periodo ='" + mesRef + "' " + " ORDER BY m.cliente,orn.usu_datfim desc, orn.usu_cliorn, cvs.usu_sitcvs DESC, hie.codloc,C.nomeColaborador ";
			}
			InputStream is = null;
			String path = "";
			fileProposta = null;
			try
			{
				// ...files/relatorios
				path = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "relMedicaoDeContratos.jasper";
				is = new BufferedInputStream(new FileInputStream(path));

				HashMap<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("numctr", sql);

				if (parametros != null)
				{

					File file = File.createTempFile("Medicao", ".pdf");
					file.deleteOnExit();

					JasperPrint impressao = JasperFillManager.fillReport(is, parametros, fusion);
					if (impressao != null && file != null)
					{
						JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
						return file;
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				fusion.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
		return fileProposta;
	}

	public static List<NeoObject> retornaRegistros() throws SQLException
	{

		String sql = "SELECT cliente,contrato,empresacontratada,responsavel," + " periodo,codRegional,regional,alertas FROM D_MEDMedicaoDeContratos M	where periodo = '04/2016' ";
		List<NeoObject> retorno = new ArrayList<NeoObject>();

		Connection connFusion = PersistEngine.getConnection("");
		ResultSet rsMed = null;
		PreparedStatement stMed = null;
		stMed = connFusion.prepareStatement(sql.toString());
		rsMed = stMed.executeQuery();
		while (rsMed.next())
		{
			NeoObject medicao = AdapterUtils.createNewEntityInstance("MEDMedicaoDeContratos");

			EntityWrapper wMedicao = new EntityWrapper(medicao);
			wMedicao.setValue("cliente", rsMed.getString("cliente"));
			wMedicao.setValue("empresaContratada", rsMed.getString("empresacontratada"));
			wMedicao.setValue("contrato", rsMed.getLong("contrato"));
			wMedicao.setValue("responsavel", rsMed.getString("responsavel"));
			wMedicao.setValue("periodo", rsMed.getString("periodo"));
			wMedicao.setValue("codRegional", rsMed.getLong("codRegional"));
			wMedicao.setValue("regional", rsMed.getString("regional"));
			wMedicao.setValue("alertas", rsMed.getString("alertas"));
			retorno.add(medicao);
		}

		return retorno;
	}

	public static Long retornaPeriodoDoAfastamento(String datIni, String datFim)
	{
		GregorianCalendar dataInicio = new GregorianCalendar();
		GregorianCalendar dataFinal = new GregorianCalendar();
		Long cont = 0L;
		dataInicio.setTime(NeoCalendarUtils.formatDate(datIni, "dd/MM/yyyy"));
		dataFinal.setTime(NeoCalendarUtils.formatDate(datFim, "dd/MM/yyyy"));

		if (dataInicio.equals(dataFinal))
		{
			cont = 1L;
		}
		else
		{
			while (!dataInicio.equals(dataFinal))
			{
				cont++;
				dataInicio.add(GregorianCalendar.DATE, 1);
			}
		}

		return cont;
	}

	public static int retornaInicioDesconto(String datAdm, GregorianCalendar datRef)
	{
		int desconto = 30;
		GregorianCalendar datIni = new GregorianCalendar();
		GregorianCalendar datAdmG = null;
		datIni = (GregorianCalendar) datRef.clone();
		datIni.set(GregorianCalendar.DATE, 1);
		datIni.add(GregorianCalendar.DATE, -1);
		try
		{
			datAdmG = OrsegupsUtils.stringToGregorianCalendar(datAdm, "yyyy-MM-dd");
			if (datAdmG.before(datRef) && datAdmG.after(datIni))
			{
				desconto--;
				int diaIni = datAdmG.get(GregorianCalendar.DAY_OF_YEAR);
				int diaFim = datRef.get(GregorianCalendar.DAY_OF_YEAR);
				desconto = diaFim - diaIni;
			}
		}

		catch (ParseException e)
		{
			e.printStackTrace();
		}
		return desconto;
	}

	public static int verificaRetornoDoInss(long numcad, long numemp, long tipcol, GregorianCalendar datRef)
	{
		GregorianCalendar datMes = (GregorianCalendar) datRef.clone();
		datMes.set(GregorianCalendar.DATE, 1);
		String datAfa = NeoDateUtils.safeDateFormat(datMes, "yyyy-MM-dd");
		datMes.add(GregorianCalendar.MONTH, 1);
		datMes.add(GregorianCalendar.DATE, -1);
		String datAfaFim = NeoDateUtils.safeDateFormat(datMes, "yyyy-MM-dd");
		Connection connVetorh = null;
		ResultSet rsAfastamento = null;
		PreparedStatement stAfastamento = null;
		StringBuilder sql = new StringBuilder();
		try
		{
			connVetorh = PersistEngine.getConnection("VETORH");
			sql.append("SELECT ((DATEDIFF(DAY," + datAfa + ",DATTER)))+1 as desconto ,CONVERT(VARCHAR,DATTER,103) AS DATTER FROM R038AFA WHERE NUMCAD = ? AND NUMEMP = ? AND TIPCOL = ? AND DATAFA < ? AND DATTER >= ? and DATTER <= ?");
			stAfastamento = connVetorh.prepareStatement(sql.toString());
			stAfastamento.setLong(1, numcad);
			stAfastamento.setLong(2, numemp);
			stAfastamento.setLong(3, tipcol);
			stAfastamento.setString(4, datAfa);
			stAfastamento.setString(5, datAfa);
			stAfastamento.setString(6, datAfaFim);
			rsAfastamento = stAfastamento.executeQuery();

			while (rsAfastamento.next())
			{
				return 30 - rsAfastamento.getInt("desconto");
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stAfastamento, rsAfastamento);
		}

		return 0;
	}

	public static String retornaDataAfastamento(long numcad, long numemp, long tipcol, long sitafa, GregorianCalendar datRef)
	{
		GregorianCalendar datMes = (GregorianCalendar) datRef.clone();
		datMes.set(GregorianCalendar.DATE, 1);
		String datAfa = NeoDateUtils.safeDateFormat(datMes, "yyyy-MM-dd");
		datMes.add(GregorianCalendar.MONTH, 1);
		datMes.add(GregorianCalendar.DATE, -1);
		String datAfaFim = NeoDateUtils.safeDateFormat(datMes, "yyyy-MM-dd");
		Connection connVetorh = null;
		ResultSet rsAfastamento = null;
		PreparedStatement stAfastamento = null;
		StringBuilder sql = new StringBuilder();
		try
		{
			connVetorh = PersistEngine.getConnection("VETORH");
			sql.append("SELECT CONVERT(VARCHAR,DATTER,103) AS DATTER FROM R038AFA WHERE NUMCAD = ? AND NUMEMP = ? AND TIPCOL = ? AND DATAFA < ? AND DATTER >= ? and DATTER <= ? and sitafa = ? ");
			stAfastamento = connVetorh.prepareStatement(sql.toString());
			stAfastamento.setLong(1, numcad);
			stAfastamento.setLong(2, numemp);
			stAfastamento.setLong(3, tipcol);
			stAfastamento.setString(4, datAfa);
			stAfastamento.setString(5, datAfa);
			stAfastamento.setString(6, datAfaFim);
			stAfastamento.setLong(7, sitafa);
			rsAfastamento = stAfastamento.executeQuery();

			while (rsAfastamento.next())
			{
				return rsAfastamento.getString("DATTER");
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stAfastamento, rsAfastamento);
		}
		return "";
	}

	public static List<MedicaoVO> listaMedicoes(String empresa, String competencia, String cliente, String cbTodos, String cbVagas, String cbFaltas, String cbDemit, String cbOK, String nOficialContrato, String regionalPesquisa)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		if (!regionalPesquisa.equals(""))
		{
			regionalPesquisa = regionalPesquisa.replace("\"", "");
		}
		if (empresa != null && !empresa.isEmpty())
		{
			long lEmpresa = Long.parseLong(empresa);
			QLFilter empresaQl = new QLEqualsFilter("empresaContratada", lEmpresa);
			groupFilter.addFilter(empresaQl);
		}

		if (competencia != null && !competencia.isEmpty())
		{
			QLFilter qlCompetencia = new QLEqualsFilter("periodo", competencia);
			groupFilter.addFilter(qlCompetencia);
		}

		if (cliente != null && !cliente.isEmpty())
		{
			QLRawFilter qlCliente = new QLRawFilter("cliente like '%" + cliente + "%'");
			groupFilter.addFilter(qlCliente);
		}

		if (regionalPesquisa != null && !regionalPesquisa.equals(""))
		{
			QLRawFilter qlRegionalPesquisa = new QLRawFilter("codRegional in (" + regionalPesquisa + ")");
			groupFilter.addFilter(qlRegionalPesquisa);
		}

		if (nOficialContrato != null && !nOficialContrato.isEmpty())
		{
			QLRawFilter qlNOficialContrato = new QLRawFilter("numOfi like '%" + nOficialContrato + "%'");
			groupFilter.addFilter(qlNOficialContrato);
		}

		String filtroAlertasInicioFim = "(";
		String filtroAlertas = "";
		if (cbTodos != null && !cbTodos.isEmpty())
		{
			filtroAlertas += "alertas like '%%'";
		}

		if (cbVagas != null && !cbVagas.isEmpty())
		{
			if (filtroAlertas != null && !filtroAlertas.isEmpty())
			{
				filtroAlertas += " OR ";
			}
			filtroAlertas += "alertas like '%" + cbVagas + "%'";
		}

		if (cbFaltas != null && !cbFaltas.isEmpty())
		{
			if (filtroAlertas != null && !filtroAlertas.isEmpty())
			{
				filtroAlertas += " OR ";
			}
			filtroAlertas += "alertas like '%" + cbFaltas + "%'";
		}

		if (cbDemit != null && !cbDemit.isEmpty())
		{
			if (filtroAlertas != null && !filtroAlertas.isEmpty())
			{
				filtroAlertas += " OR ";
			}
			filtroAlertas += "alertas like  '%" + cbDemit + "%'";
		}

		if (cbOK != null && !cbOK.isEmpty())
		{
			if (filtroAlertas != null && !filtroAlertas.isEmpty())
			{
				filtroAlertas += " OR ";
			}
			filtroAlertas += "alertas like '%" + cbOK + "%' or alertas = '' ";
		}

		if (filtroAlertas != null && !filtroAlertas.isEmpty())
		{
			filtroAlertas = filtroAlertasInicioFim + filtroAlertas;
			filtroAlertas += " )";
		}

		QLRawFilter qlSql = new QLRawFilter(filtroAlertas);
		if (filtroAlertas != null && !filtroAlertas.equals(""))
			groupFilter.addFilter(qlSql);

		List<NeoObject> objsMedicoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter, -1, -1, "cliente ASC, numOfi ASC");
		List<MedicaoVO> medicoes = new ArrayList<MedicaoVO>();
		String clienteAntigo = "";
		String clienteNovo = "";
		String numCtrOriAnt = "";
		String numCtrOriNovo = "";

		for (NeoObject objMedicao : objsMedicoes)
		{

			EntityWrapper wrapper = new EntityWrapper(objMedicao);
			MedicaoVO medicao = new MedicaoVO();
			medicao.setCliente(String.valueOf(wrapper.findField("cliente").getValue()));
			clienteNovo = medicao.getCliente();
			long contrato = (long) wrapper.findField("contrato").getValue();
			medicao.setContrato(contrato);

			medicao.setNumOfi(String.valueOf(wrapper.findField("numOfi").getValue()));
			numCtrOriNovo = medicao.getNumOfi();

			long empresaContratada = (long) wrapper.findField("codEmpresaContratada").getValue();
			medicao.setCodEmpresaContratada(empresaContratada);

			medicao.setEmpresaContratada(String.valueOf(wrapper.findField("empresaContratada").getValue()));
			medicao.setResponsavel(String.valueOf(wrapper.findField("responsavel").getValue()));
			medicao.setPeriodo(String.valueOf(wrapper.findField("periodo").getValue()));

			long codRegional = (long) wrapper.findField("codRegional").getValue();
			medicao.setCodRegional(codRegional);

			medicao.setRegional(String.valueOf(wrapper.findField("regional").getValue()));
			medicao.setAlertas(String.valueOf(wrapper.findField("alertas").getValue()));

			boolean gerCms = (boolean) wrapper.findField("gerCms").getValue();
			medicao.setGerCms(gerCms);

			String filtroLink = medicao.getCliente();
			String link = "<a title='Lista de postos' style='cursor:pointer' onclick='javascript:listaDePostos(" + "\"" + filtroLink + "\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\", \"" + medicao.getNumOfi() + "\")'><img src='imagens/lupa_medicao.png'></a>";
			medicao.setLinkVisualizar(link);
			if (!clienteAntigo.equals(clienteNovo) || !numCtrOriAnt.equals(numCtrOriNovo))
			{
				String novoAlertaClientes = montaAlertasCliente(clienteNovo, competencia, numCtrOriNovo);
				medicao.setAlertas(novoAlertaClientes);
				medicao.setImgAlertas(retornaImagensAlertas(novoAlertaClientes));
				medicoes.add(medicao);
				clienteAntigo = clienteNovo;
				numCtrOriAnt = numCtrOriNovo;
			}
		}
		return medicoes;
	}

	public static String montaAlertasCliente(String cliente, String competencia, String numCtrOriNovo)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		if (cliente != null && !cliente.isEmpty())
		{
			QLRawFilter qlCliente = new QLRawFilter("cliente like '%" + cliente + "%'");
			groupFilter.addFilter(qlCliente);
		}

		if (competencia != null && !competencia.isEmpty())
		{
			QLRawFilter qlCompetencia = new QLRawFilter("periodo = '" + competencia + "'");
			groupFilter.addFilter(qlCompetencia);
		}

		if (numCtrOriNovo != null && !numCtrOriNovo.isEmpty())
		{
			QLRawFilter qlNumCtrOriNovo = new QLRawFilter("numOfi = '" + numCtrOriNovo + "'");
			groupFilter.addFilter(qlNumCtrOriNovo);
		}

		List<NeoObject> objsMedicoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter, -1, -1, "cliente ASC");

		String novaImg = "";
		for (NeoObject objM : objsMedicoes)
		{
			EntityWrapper wMedicao = new EntityWrapper(objM);
			List<NeoObject> postos = (List) wMedicao.findField("listaPostos").getValues();

			novaImg += validaImagAlertasMedicao(novaImg, postos);
			if (novaImg.contains("falta") || novaImg.contains("Divergentes"))
			{
				novaImg.replace(";ok", "");
				novaImg.replace("ok", "");
			}
		}

		for (NeoObject objM : objsMedicoes)
		{
			EntityWrapper wMedicao = new EntityWrapper(objM);
			wMedicao.findField("alertas").setValue(novaImg);
			PersistEngine.persist(objM);
		}
		return novaImg;
	}

	public static List<ConsideracoesMedicaoVO> listaObservacoes(String competencia, String numcad, String nomFun)
	{

		String sql = "";
		sql += "SELECT CN.descricao, CN.isFalta, CN.neoId ";
		sql += "  FROM D_MEDMedicaoDeContratos";
		sql += "  M ";
		sql += "  INNER JOIN D_MEDMedicaoDeContratos_listaPostos ML ON ML.D_MEDMEDICAODECONTRATOS_NEOID = M.neoId";
		sql += "  INNER JOIN D_MEDPOSTO P ON P.NEOID = ML.listaPostos_neoId ";
		sql += "  INNER JOIN D_MEDPOSTO_listaColaboradores MC ON	MC.D_MEDPOSTO_neoId = P.neoId";
		sql += "  INNER JOIN D_MEDColaboradores C ON 	C.NEOID = MC.listaColaboradores_neoId   ";
		sql += "  INNER JOIN D_MEDColaboradores_listaConsideracoes CCN ON  	CCN.D_MEDColaboradores_neoId = MC.listaColaboradores_neoId";
		sql += "  INNER JOIN D_MEDConsideracoes CN ON CN.neoId = 	CCN.listaConsideracoes_neoId   ";
		sql += "  where c.numCadColaborador = " + numcad + " and nomeColaborador = '" + nomFun + "' and M.periodo = '" + competencia + "'";
		Connection connFusion = null;
		ResultSet rsObservacoes = null;
		PreparedStatement stObservacoes = null;
		List<ConsideracoesMedicaoVO> consideracoesVO = new ArrayList<ConsideracoesMedicaoVO>();
		try
		{
			connFusion = PersistEngine.getConnection("");
			stObservacoes = connFusion.prepareStatement(sql.toString());
			rsObservacoes = stObservacoes.executeQuery();
			while (rsObservacoes.next())
			{
				ConsideracoesMedicaoVO observMedicaoVO = new ConsideracoesMedicaoVO();
				observMedicaoVO.setDescricao(rsObservacoes.getString("descricao"));
				observMedicaoVO.setFalta(rsObservacoes.getBoolean("isFalta"));
				if (observMedicaoVO.isFalta)
				{
					observMedicaoVO.setIsfaltaString("Sim");
				}
				else
				{
					observMedicaoVO.setIsfaltaString("Não");
				}
				observMedicaoVO.setNeoId(rsObservacoes.getLong("neoId"));
				observMedicaoVO.setLinkExcluir("<a title='Remover' style='cursor:pointer' onclick='javascript: if (confirm(\"Tem Certeza?\") == true) { removerObservacao(" + observMedicaoVO.getNeoId() + ")}'><img src='imagens/delete_16x16-trans.png'></a>");
				observMedicaoVO.setLinkEdirar("<a title='Editar' style='cursor:pointer' onclick='javascript:editarObs(" + observMedicaoVO.getNeoId() + ")'><img src='imagens/editar.png'></a>");
				consideracoesVO.add(observMedicaoVO);

			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connFusion, stObservacoes, rsObservacoes);
		}

		return consideracoesVO;
	}

	public static List<DiariaMedicaoVO> listaDeDiarias(String cliente, String periodo)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		List<DiariaMedicaoVO> diarias = new ArrayList<DiariaMedicaoVO>();

		QLRawFilter qlCliente = new QLRawFilter("cliente like '%" + cliente + "%'");
		groupFilter.addFilter(qlCliente);

		if (periodo != null && !periodo.isEmpty())
		{
			QLFilter qlCompetencia = new QLEqualsFilter("periodo", periodo);
			groupFilter.addFilter(qlCompetencia);
		}

		List<NeoObject> objsMedicoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter, -1, -1, "cliente ASC");
		for (NeoObject obj : objsMedicoes)
		{
			EntityWrapper wrapper = new EntityWrapper(obj);
			List<NeoObject> objDiarias = (List) wrapper.findField("listaDiarias").getValues();
			for (NeoObject diaria : objDiarias)
			{
				EntityWrapper wDiaria = new EntityWrapper(diaria);
				DiariaMedicaoVO diariaVO = new DiariaMedicaoVO();
				diariaVO.setObsDiaria(String.valueOf(wDiaria.findField("obsDiaria").getValue()));
				BigDecimal qtdDiarias = (BigDecimal) wDiaria.findField("qtdDiarias").getValue();
				diariaVO.setNeoId((long) wDiaria.findField("neoId").getValue());
				diariaVO.setQtdDiarias(qtdDiarias);
				diariaVO.setLinkExcluir("<a title='Remover' style='cursor:pointer' onclick='javascript: if (confirm(\"Tem Certeza?\") == true) { removerDiaria(" + diariaVO.getNeoId() + ")}'><img src='imagens/delete_16x16-trans.png'></a>");
				diariaVO.setLinkEditar("<a title='Editar' style='cursor:pointer' onclick='javascript:editarDiaria(" + diariaVO.getNeoId() + ")'><img src='imagens/editar.png'></a>");
				diarias.add(diariaVO);
			}
		}

		return diarias;
	}

	public static boolean salvarDiaria(String cliente, String competencia, String qtdDiarias, String obsDiaria, String neoIdDiaria)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		List<DiariaMedicaoVO> diarias = new ArrayList<DiariaMedicaoVO>();
		QLRawFilter qlCliente = new QLRawFilter("cliente like '%" + cliente + "%'");
		groupFilter.addFilter(qlCliente);
		if (competencia != null && !competencia.isEmpty())
		{
			QLFilter qlCompetencia = new QLEqualsFilter("periodo", competencia);
			groupFilter.addFilter(qlCompetencia);
		}

		List<NeoObject> objsMedicoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter, -1, -1, "cliente ASC");
		NeoObject objMed = objsMedicoes.get(0);
		EntityWrapper wCliente = new EntityWrapper(objMed);
		List<NeoObject> lDiaria = (List) wCliente.findField("listaDiarias").getValues();
		NeoObject objDiaria = null;
		if (neoIdDiaria == null || neoIdDiaria.isEmpty() || neoIdDiaria.equals("0"))
		{
			InstantiableEntityInfo insDiaria = AdapterUtils.getInstantiableEntityInfo("MEDDiarias");
			objDiaria = insDiaria.createNewInstance();
		}
		else
		{
			QLGroupFilter groupFilterDiaria = new QLGroupFilter("AND");
			long lNeoId = Long.parseLong(neoIdDiaria);
			QLFilter empresaQl = new QLEqualsFilter("neoId", lNeoId);
			groupFilterDiaria.addFilter(empresaQl);
			if (competencia != null && !competencia.isEmpty())
			{
				QLFilter qlCompetencia = new QLEqualsFilter("periodo", competencia);
				groupFilter.addFilter(qlCompetencia);
			}
			List<NeoObject> objsDiaria = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDDiarias"), groupFilterDiaria);
			objDiaria = objsDiaria.get(0);
		}

		EntityWrapper wDiaria = new EntityWrapper(objDiaria);
		BigDecimal qtdDiariasBig = new BigDecimal(qtdDiarias.toString());
		wDiaria.findField("obsDiaria").setValue(obsDiaria);
		wDiaria.findField("qtdDiarias").setValue(qtdDiariasBig);
		PersistEngine.persist(objDiaria);
		if (neoIdDiaria == null || neoIdDiaria.isEmpty() || neoIdDiaria.equals("0"))
		{
			lDiaria.add(objDiaria);
		}
		wCliente.setValue("listaDiarias", lDiaria);
		PersistEngine.persist(objMed);
		PersistEngine.commit(true);

		return true;
	}

	public static DiariaMedicaoVO editarDiarias(String neoId)

	{
		DiariaMedicaoVO diaria = new DiariaMedicaoVO();
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		if (neoId != null && !neoId.isEmpty())
		{
			Long neoIdQl = Long.parseLong(neoId);
			QLFilter qlNeoId = new QLEqualsFilter("neoId", neoIdQl);
			groupFilter.addFilter(qlNeoId);
		}

		List<NeoObject> objsObservacoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDDiarias"), groupFilter);
		if (objsObservacoes != null && objsObservacoes.size() > 0)
		{
			NeoObject object = objsObservacoes.get(0);
			EntityWrapper wrapper = new EntityWrapper(object);
			diaria.setObsDiaria(String.valueOf(wrapper.findField("obsDiaria").getValue()));
			diaria.setQtdDiarias(new BigDecimal(String.valueOf(wrapper.findField("qtdDiarias").getValue())));
			diaria.setNeoId((long) wrapper.findField("neoId").getValue());
		}
		return diaria;
	}

	public static List<CoberturaPostoMedicaoVO> listaCoberturasPosto(String competencia, String numPos)
	{

		QLGroupFilter groupFilter = new QLGroupFilter("AND");

		if (competencia != null && !competencia.isEmpty())
		{
			QLFilter qlCompetencia = new QLEqualsFilter("datCpt", competencia);
			groupFilter.addFilter(qlCompetencia);
		}

		if (numPos != null && !numPos.isEmpty())
		{
			QLFilter qlNumPos = new QLEqualsFilter("numPos", Long.parseLong(numPos));
			groupFilter.addFilter(qlNumPos);
		}

		List<NeoObject> objsCobertuas = PersistEngine.getObjects(AdapterUtils.getEntityClass("medCoberturasPosto"), groupFilter, -1, -1, "dataCoberturaPosto ASC");
		List<CoberturaPostoMedicaoVO> coberturas = new ArrayList<CoberturaPostoMedicaoVO>();
		for (NeoObject objCobertura : objsCobertuas)
		{
			EntityWrapper wrapper = new EntityWrapper(objCobertura);
			CoberturaPostoMedicaoVO coberturaVO = new CoberturaPostoMedicaoVO();
			coberturaVO.setDatCob((GregorianCalendar) wrapper.findField("dataCoberturaPosto").getValue());
			GregorianCalendar data = coberturaVO.getDatCob();
			coberturaVO.setDataCobertura(NeoDateUtils.safeDateFormat(data, "dd/MM/yyyy"));
			coberturaVO.setNomFun(String.valueOf(wrapper.findField("nomFun").getValue()));
			coberturaVO.setNumCad(Long.parseLong(String.valueOf(wrapper.findField("numCad").getValue())));
			coberturaVO.setNumEmp(Long.parseLong(String.valueOf(wrapper.findField("numEmp").getValue())));
			coberturaVO.setNumPos(Long.parseLong(String.valueOf(wrapper.findField("numPos").getValue())));
			coberturas.add(coberturaVO);
		}
		return coberturas;
	}

	public static List<ColaboradoresDemitidosPostosMedicaoVO> listaColaboradoresDemitidosDoPosto(String competencia, String numPos)
	{

		QLGroupFilter groupFilter = new QLGroupFilter("AND");

		if (competencia != null && !competencia.isEmpty())
		{
			String datCpf = "01/" + competencia;
			try
			{
				GregorianCalendar datIni = OrsegupsUtils.stringToGregorianCalendar(datCpf, "dd/MM/yyyy");
				GregorianCalendar datFim = (GregorianCalendar) datIni.clone();
				datFim.add(GregorianCalendar.MONTH, 1);
				datFim.add(GregorianCalendar.DATE, -1);
				QLOpFilter dataIniFilter = new QLOpFilter("dataDms", ">=", (GregorianCalendar) datIni);
				QLOpFilter dataFimFilter = new QLOpFilter("dataDms", "<=", (GregorianCalendar) datFim);
				groupFilter.addFilter(dataIniFilter);
				groupFilter.addFilter(dataFimFilter);
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}

		}

		if (numPos != null && !numPos.isEmpty())
		{
			QLFilter qlNumPos = new QLEqualsFilter("numpos", Long.parseLong(numPos));
			groupFilter.addFilter(qlNumPos);
		}

		List<NeoObject> objsColDem = PersistEngine.getObjects(AdapterUtils.getEntityClass("medColaboradoresDemitidosPorPosto"), groupFilter, -1, -1, "dataDms ASC");
		List<ColaboradoresDemitidosPostosMedicaoVO> colDems = new ArrayList<ColaboradoresDemitidosPostosMedicaoVO>();
		for (NeoObject objColDem : objsColDem)
		{
			EntityWrapper wrapper = new EntityWrapper(objColDem);
			ColaboradoresDemitidosPostosMedicaoVO colDem = new ColaboradoresDemitidosPostosMedicaoVO();
			colDem.setNumcad(Long.parseLong(String.valueOf(wrapper.findField("numcad").getValue())));
			colDem.setNumemp(Long.parseLong(String.valueOf(wrapper.findField("numemp").getValue())));
			colDem.setNomfun(String.valueOf(wrapper.findField("nomfun").getValue()));
			colDem.setDatDms((GregorianCalendar) wrapper.findField("dataDms").getValue());
			colDem.setDatDmsString(NeoDateUtils.safeDateFormat(colDem.getDatDms(), "dd/MM/yyyy"));
			colDem.setNumpos(Long.parseLong(String.valueOf(wrapper.findField("numpos").getValue())));

			colDems.add(colDem);
		}
		return colDems;
	}

	public static boolean salvarObservacaoPosto(String neoIdPosto, String obsPosto)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		if (neoIdPosto != null && !neoIdPosto.isEmpty())
		{
			QLFilter qlNeoId = new QLEqualsFilter("neoId", Long.parseLong(neoIdPosto));
			groupFilter.addFilter(qlNeoId);
		}

		List<NeoObject> objsColDem = PersistEngine.getObjects(AdapterUtils.getEntityClass("medPosto"), groupFilter);
		if (objsColDem != null && objsColDem.size() > 0)
		{
			EntityWrapper wrapper = new EntityWrapper(objsColDem.get(0));
			wrapper.setValue("obsPosto", obsPosto);
			PersistEngine.persist(objsColDem.get(0));
			return true;
		}
		return false;

	}

	public static boolean salvarObservacao(String competencia, String numcad, String nomfun, String observacao, String isFalta, String neoId, String horEx, String minEx, String horMed, String minMed, String descMed)
	{
		String sql = "";
		Long neoIdCol = 0L;
		sql += "  SELECT TOP 1 C.NEOID ";
		sql += "  FROM D_MEDMedicaoDeContratos";
		sql += "  M ";
		sql += "  INNER JOIN D_MEDMedicaoDeContratos_listaPostos ML ON ML.D_MEDMEDICAODECONTRATOS_NEOID = M.neoId";
		sql += "  INNER JOIN D_MEDPOSTO P ON P.NEOID = ML.listaPostos_neoId ";
		sql += "  INNER JOIN D_MEDPOSTO_listaColaboradores MC ON	MC.D_MEDPOSTO_neoId = P.neoId";
		sql += "  INNER JOIN D_MEDColaboradores C ON 	C.NEOID = MC.listaColaboradores_neoId   ";
		sql += "  where c.numCadColaborador = " + numcad + " and nomeColaborador = '" + nomfun + "' and M.periodo = '" + competencia + "'";

		Connection connFusion = null;
		ResultSet rsObservacoes = null;
		PreparedStatement stObservacoes = null;
		boolean retorno = false;

		try
		{
			connFusion = PersistEngine.getConnection("");
			stObservacoes = connFusion.prepareStatement(sql.toString());
			rsObservacoes = stObservacoes.executeQuery();

			while (rsObservacoes.next())
			{
				neoIdCol = rsObservacoes.getLong("neoId");
				break;
			}
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connFusion, stObservacoes, rsObservacoes);
		}

		if (neoId != null && !neoId.isEmpty())
		{
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			if (neoId != null && !neoId.isEmpty())
			{
				Long neoIdQl = Long.parseLong(neoId);
				QLFilter qlNeoId = new QLEqualsFilter("neoId", neoIdQl);
				groupFilter.addFilter(qlNeoId);
			}

			List<NeoObject> objsObservacoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDConsideracoes"), groupFilter);
			if (objsObservacoes != null && objsObservacoes.size() > 0)
			{
				NeoObject object = objsObservacoes.get(0);
				EntityWrapper wrapper = new EntityWrapper(object);
				wrapper.findField("descricao").setValue(observacao);
				if (isFalta.equals("Sim"))
				{
					wrapper.findField("isFalta").setValue(new Boolean(true));
				}
				else
				{
					wrapper.findField("isFalta").setValue(new Boolean(false));
				}

				QLGroupFilter groupFilter2 = new QLGroupFilter("AND");
				QLFilter qlNeoId = new QLEqualsFilter("neoId", neoIdCol);
				groupFilter2.addFilter(qlNeoId);

				List<NeoObject> objsColaborador = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDColaboradores"), groupFilter2, -1, -1, "nomeColaborador");
				NeoObject objColaborador = objsColaborador.get(0);
				EntityWrapper wColaborador = new EntityWrapper(objColaborador);
				wColaborador.findField("horEx").setValue(Long.parseLong(horEx));
				wColaborador.findField("minEx").setValue(Long.parseLong(minEx));
				wColaborador.findField("horMed").setValue(Long.parseLong(horMed));
				wColaborador.findField("minMed").setValue(Long.parseLong(minMed));
				wColaborador.findField("descMed").setValue(descMed);
				PersistEngine.persist(object);
				PersistEngine.persist(objColaborador);
				PersistEngine.commit(true);
				return true;
			}
		}
		else
		{

			if (neoIdCol > 0)
			{
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				QLFilter qlNeoId = new QLEqualsFilter("neoId", neoIdCol);
				groupFilter.addFilter(qlNeoId);

				List<NeoObject> objsColaborador = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDColaboradores"), groupFilter);
				NeoObject objColaborador = objsColaborador.get(0);
				EntityWrapper wColaborador = new EntityWrapper(objColaborador);

				List<NeoObject> listaObs = (List) wColaborador.findField("listaConsideracoes").getValues();
				InstantiableEntityInfo insObservacoes = AdapterUtils.getInstantiableEntityInfo("MEDConsideracoes");
				NeoObject objObservacoes = insObservacoes.createNewInstance();
				EntityWrapper wObservacoes = new EntityWrapper(objObservacoes);

				if (observacao != null && !observacao.isEmpty())
				{
					wObservacoes.findField("descricao").setValue(observacao);
					if (isFalta.equals("Sim"))
					{
						wObservacoes.findField("isFalta").setValue(Boolean.TRUE);
					}
					else
					{
						wObservacoes.findField("isFalta").setValue(Boolean.FALSE);
					}
					PersistEngine.persist(objObservacoes);
					listaObs.add(objObservacoes);
					for (NeoObject obj : listaObs)
					{
						PersistEngine.persist(obj);
					}
				}
				wColaborador.findField("descMed").setValue(descMed);
				wColaborador.findField("horEx").setValue(Long.parseLong(horEx));
				wColaborador.findField("minEx").setValue(Long.parseLong(minEx));
				wColaborador.findField("horMed").setValue(Long.parseLong(horMed));
				wColaborador.findField("minMed").setValue(Long.parseLong(minMed));
				PersistEngine.persist(objColaborador);
				retorno = true;
				PersistEngine.commit(true);
			}

			return retorno;
		}
		return false;
	}

	public static Boolean removerObservacao(String neoId)
	{
		Boolean retorno = false;
		Connection conVetorh = PersistEngine.getConnection("");
		PreparedStatement pst = null;
		try
		{
			String sql = "delete D_MEDColaboradores_listaConsideracoes where listaConsideracoes_neoId = ?;delete D_MEDConsideracoes where neoId = ?";
			pst = conVetorh.prepareStatement(sql);

			pst.setString(1, neoId);
			pst.setString(2, neoId);

			int r = pst.executeUpdate();
			System.out.println("dvinc->" + r);
			if (r > 0)
			{
				retorno = true;
			}

			return retorno;
		}
		catch (Exception e)
		{
			System.out.println("Erro ao remover Observação do colaborador");
			e.printStackTrace();
			return retorno;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conVetorh, pst, null);
		}
	}

	public static Boolean removerDiaria(String neoId)
	{
		Boolean retorno = false;
		Connection conVetorh = PersistEngine.getConnection("");
		PreparedStatement pst = null;
		try
		{
			String sql = "delete D_MEDMedicaoDeContratos_listaDiarias where listaDiarias_neoId = ? ; delete D_MEDDIARIAS where neoId = ?";
			pst = conVetorh.prepareStatement(sql);

			pst.setString(1, neoId);
			pst.setString(2, neoId);

			int r = pst.executeUpdate();
			System.out.println("dvinc->" + r);
			if (r > 0)
			{
				retorno = true;
			}

			return retorno;
		}
		catch (Exception e)
		{
			System.out.println("Erro ao remover Observação do colaborador");
			e.printStackTrace();
			return retorno;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conVetorh, pst, null);
		}
	}

	public static Boolean removerColaborador(String neoId)
	{
		Boolean retorno = false;
		Connection conVetorh = PersistEngine.getConnection("");
		PreparedStatement pst = null;
		try
		{
			String sql = "update D_medPosto set NColaboradores = NColaboradores - 1 where neoId = (select D_medPosto_neoId from D_medPosto_listaColaboradores where listaColaboradores_neoId = ?);" + "	  delete D_MEDConsideracoes where neoId in(select listaConsideracoes_neoId from D_MEDColaboradores_listaConsideracoes where D_MEDColaboradores_neoId = ?) ;" + "	  delete D_MEDColaboradores_listaConsideracoes where D_MEDColaboradores_neoId = ? ; "
					+ "   delete D_medPosto_listaColaboradores where listaColaboradores_neoId = ?;" + "   delete D_MEDColaboradores where neoId = ?";
			pst = conVetorh.prepareStatement(sql);

			pst.setString(1, neoId);
			pst.setString(2, neoId);
			pst.setString(3, neoId);
			pst.setString(4, neoId);
			pst.setString(5, neoId);

			int r = pst.executeUpdate();
			System.out.println("dvinc->" + r);
			if (r > 0)
			{
				retorno = true;
			}

			return retorno;
		}
		catch (Exception e)
		{
			System.out.println("Erro ao remover Colaborador do posto");
			e.printStackTrace();
			return retorno;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conVetorh, pst, null);
		}
	}

	public static String retornaImagensAlertas(String alertas)
	{
		if (alertas == null)
		{
			alertas = "";
		}
		String icone = "";
		boolean sitOK = true;

		if (alertas.contains("falta"))
		{
			icone = icone + "<img src='imagens/Alerta_Vagas.png'title='Contrato possui colaborador(es) com menos de 30 dias.'/>";
			sitOK = false;
		}

		if (alertas.contains("colaborador demitido"))
		{
			icone = icone + "<img src='imagens/novo_colaborador.png'title='O contrato possui um colaborador demitido no periodo.'/>";
			sitOK = false;
		}

		if (alertas.contains("nVagasDivergentes"))
		{
			icone = icone + "<img src='imagens/exclamacao2.png'title='O Posto possui divergência entre a quantidade de vagas e colaboradores.'/>";
			sitOK = false;
		}

		if (sitOK)
		{
			icone = "<img src='imagens/certo2.png' title='contrato sem alertas'/>";
		}

		return icone;
	}

	public static List<PostosMedicaoVO> listaPostos(String competencia, String cliente, String nomeColaborador, String numeroPosto, String cbTodos, String cbVagas, String cbFaltas, String cbDemit, String cbOK, String numOfi)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		boolean isPesquisa = false;

		if ((nomeColaborador != null && !nomeColaborador.isEmpty()) || (numeroPosto != null && !numeroPosto.isEmpty()))
		{
			isPesquisa = true;
		}

		if (competencia != null && !competencia.isEmpty())
		{
			QLFilter qlCompetencia = new QLEqualsFilter("periodo", competencia);
			groupFilter.addFilter(qlCompetencia);
		}

		if (cliente != null && !cliente.isEmpty())
		{
			QLFilter qlCliente = new QLEqualsFilter("cliente", cliente);
			groupFilter.addFilter(qlCliente);
		}

		if (numOfi != null && !numOfi.isEmpty())
		{
			QLFilter qlNumOfi = new QLEqualsFilter("numOfi", numOfi);
			groupFilter.addFilter(qlNumOfi);
		}

		List<NeoObject> objsMedicoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter, -1, -1, "cliente ASC");
		List<PostosMedicaoVO> postos = new ArrayList<PostosMedicaoVO>();
		List<NeoObject> postosMedicao = new ArrayList<NeoObject>();
		boolean pesquisa = true;
		String nomCli = "";
		String alertaColDem = "";
		for (NeoObject objMedicao : objsMedicoes)
		{
			EntityWrapper wrapper = new EntityWrapper(objMedicao);
			nomCli = String.valueOf(wrapper.findField("cliente").getValue());
			alertaColDem = String.valueOf(wrapper.findField("alertas").getValue());
			if (alertaColDem.contains("demitido"))
			{
				alertaColDem = ";colaborador demitido";
			}
			else
			{
				alertaColDem = "";
			}

			postosMedicao.addAll((List) wrapper.findField("listaPostos").getValues());
		}
		QLGroupFilter groupFilter2 = new QLGroupFilter("OR");
		for (NeoObject postoNeoId : postosMedicao)
		{
			EntityWrapper wPos = new EntityWrapper(postoNeoId);
			long neoId = (long) wPos.findField("neoId").getValue();
			QLFilter qlNeoId = new QLEqualsFilter("neoId", neoId);
			groupFilter2.addFilter(qlNeoId);
		}
		postosMedicao = new ArrayList<NeoObject>();
		postosMedicao = PersistEngine.getObjects(AdapterUtils.getEntityClass("medPosto"), groupFilter2, -1, -1, "codLoc DESC");

		for (NeoObject postoVO : postosMedicao)
		{
			pesquisa = true;
			boolean faltaPosto = false;
			EntityWrapper wrapperPosto = new EntityWrapper(postoVO);
			PostosMedicaoVO posto = new PostosMedicaoVO();
			long numPos = (long) wrapperPosto.findField("numPos").getValue();
			long numLoc = (long) wrapperPosto.findField("numLoc").getValue();
			long nVagas = (long) wrapperPosto.findField("nVagas").getValue();
			long neoId = (long) wrapperPosto.findField("neoId").getValue();
			long nColaboradores = (long) wrapperPosto.findField("nColaboradores").getValue();
			String codCcu = String.valueOf(wrapperPosto.findField("codCcu").getValue());
			String nomLoc = String.valueOf(wrapperPosto.findField("nomLoc").getValue());
			String codEsc = String.valueOf(wrapperPosto.getValue("codEsc"));
			posto.setNumPos(numPos);
			posto.setNomLoc(nomLoc);
			posto.setnVagas(nVagas);
			posto.setnColaboradores(nColaboradores);
			posto.setNeoId(neoId);
			posto.setCodCcu(codCcu);
			if (codEsc != null && !codEsc.equals(""))
			{
				posto.setCodEsc(codEsc);
			}
			else
			{
				posto.setCodEsc("");
			}

			if (wrapperPosto.findValue("obsPosto") != null)
			{
				posto.setObsPosto(String.valueOf(wrapperPosto.findValue("obsPosto")));
			}
			else
			{
				posto.setObsPosto("");
			}

			posto.setNumLoc(numLoc);
			posto.setLinkVisualizarObsPosto("<a title='Observações do posto' style='cursor:pointer' onclick='javascript:showObservacoesPosto(" + posto.getNeoId() + ",\"" + posto.getObsPosto() + "\")'><img src='imagens/inclusaoObs.png'></a>");
			posto.setLinkRemoverPosto("<a title='Remover Posto' style='cursor:pointer' onclick='javascript:if (confirm(\"Tem Certeza?\") == true) {removerPosto(\"" + posto.getNeoId() + "\")}'><img src='imagens/delete_16x16-trans.png'></a>");

			List<ColaboradorMedicaoVO> listaColab = new ArrayList<ColaboradorMedicaoVO>();
			QLGroupFilter groupFilter3 = new QLGroupFilter("OR");
			List<NeoObject> objCol = (List) wrapperPosto.findField("listaColaboradores").getValues();
			for (NeoObject colNeoId : objCol)
			{
				EntityWrapper wCol = new EntityWrapper(colNeoId);
				long neoIdCols = (long) wCol.findField("neoId").getValue();
				QLFilter qlNeoIdCol = new QLEqualsFilter("neoId", neoIdCols);
				groupFilter3.addFilter(qlNeoIdCol);
			}

			if (groupFilter3 != null && !groupFilter3.toString().equals(""))
			{
				objCol = (List) wrapperPosto.findField("listaColaboradores").getValues();
				objCol = PersistEngine.getObjects(AdapterUtils.getEntityClass("medColaboradores"), groupFilter3, -1, -1, "nomeColaborador DESC");
			}

			if (numeroPosto != null && !numeroPosto.isEmpty())
			{
				if (posto.getNumPos() == Long.parseLong(numeroPosto))
				{
					pesquisa = true;
				}
				else
				{
					pesquisa = false;
				}
			}

			if (pesquisa)
			{
				for (NeoObject colaborador : objCol)
				{
					EntityWrapper wrapperCol = new EntityWrapper(colaborador);
					ColaboradorMedicaoVO colVO = new ColaboradorMedicaoVO();
					colVO.setCargaHorPos(String.valueOf(wrapperCol.findField("cargaHorPos").getValue()));
					colVO.setCodCcu(String.valueOf(wrapperCol.findField("codCcu").getValue()));
					colVO.setDatAdm(String.valueOf(wrapperCol.findField("datAdm").getValue()));
					List<NeoObject> objsConsideracoes = (List<NeoObject>) wrapperCol.findValue("listaConsideracoes");
					colVO.setDescMed(String.valueOf(wrapperCol.findValue("descMed")));
					colVO.setFuncao(String.valueOf(wrapperCol.findField("funcao").getValue()));
					colVO.setGerCms((boolean) wrapperCol.findField("gerCms").getValue());
					colVO.setHorEx((long) wrapperCol.findField("horEx").getValue());
					colVO.setMinEx((long) wrapperCol.findField("minEx").getValue());
					colVO.setHorMed((long) wrapperCol.findField("horMed").getValue());
					colVO.setMinMed((long) wrapperCol.findField("minMed").getValue());
					colVO.setNumCadColaborador((long) wrapperCol.findField("numCadColaborador").getValue());
					colVO.setNomeColaborador(String.valueOf(wrapperCol.findField("nomeColaborador").getValue()));
					colVO.setNeoId((long) wrapperCol.findField("neoId").getValue());
					colVO.setLocal(String.valueOf(wrapperCol.findField("local").getValue()));
					colVO.setHorEx(Long.parseLong((String.valueOf(wrapperCol.findField("horEx").getValue()))));
					colVO.setMinEx(Long.parseLong((String.valueOf(wrapperCol.findField("minEx").getValue()))));
					colVO.setHorMed(Long.parseLong((String.valueOf(wrapperCol.findField("horMed").getValue()))));
					colVO.setMinMed(Long.parseLong((String.valueOf(wrapperCol.findField("minMed").getValue()))));
					if (wrapperCol.getValue("codEsc") != null)
					{
						colVO.setNomEsc((String.valueOf(wrapperCol.getValue("codEsc"))));
					}
					else
					{
						colVO.setNomEsc("");
					}
					String alertasColaborador = (String.valueOf(wrapperCol.findField("alertasColaborador").getValue()));
					List<NeoObject> consideracoes = (List) wrapperCol.findField("listaConsideracoes").getValues();

					String novoAlerta = validaImagAlertasColaborador(alertasColaborador, consideracoes);
					wrapperCol.findField("alertasColaborador").setValue(novoAlerta);
					if (novoAlerta.contains("falta"))
					{
						faltaPosto = true;
					}
					colVO.setImgAlertas(retornaImagensAlertas(novoAlerta));
					colVO.setLinkConsideracoes("<a title='Visualizar Observações' style='cursor:pointer' onclick='javascript:showObservacoes(" + colVO.getNumCadColaborador() + ",\"" + colVO.getNomeColaborador() + "\"," + posto.getNumPos() + ",\"" + colVO.getDescMed() + "\"," + posto.getNeoId() + "," + posto.numLoc + "," + colVO.getHorMed() + "," + colVO.getMinMed() + "," + colVO.getHorEx() + "," + colVO.getMinEx() + ")'><img src='imagens/lupa_medicao.png'></a>");
					colVO.setLinkRemoveColaborador("<a title='Remover colaborador' style='cursor:pointer' onclick='javascript:if (confirm(\"Tem Certeza?\") == true) {removerColaborador(" + colVO.getNeoId() + ",\"" + nomCli + "\")}'><img src='imagens/delete_16x16-trans.png'></a>");
					colVO.setSituacao(String.valueOf(wrapperCol.findField("situacao").getValue()));
					//Persistindo novo alerta;
					PersistEngine.persist(colaborador);
					if (nomeColaborador != null && !nomeColaborador.isEmpty())
					{
						Long numcad = 0L;
						try
						{
							numcad = Long.parseLong(nomeColaborador);
						}
						catch (NumberFormatException n)
						{
							//n.printStackTrace();
							System.out.println("Pesquisa não é pela matricula.");
						}

						if (numcad != 0)
						{
							if (colVO.getNumCadColaborador() == numcad)
							{
								listaColab.add(colVO);
							}
						}
						else
						{
							nomeColaborador = nomeColaborador.toUpperCase();
							if (colVO.getNomeColaborador().contains(nomeColaborador))
							{
								listaColab.add(colVO);
							}
						}
					}
					else
					{
						listaColab.add(colVO);
					}

				}
				posto.setListaColaborador(listaColab);
				posto.setLinkAddColaborador("<a title='Lotar Colaborador' style='cursor:pointer' onclick='javascript:cadastrarColaborador(" + posto.getNeoId() + ",\"" + nomCli + "\",\"" + posto.getCodCcu() + "\",\"" + posto.getNomLoc() + "\")'><img src='imagens/addColaborador.png'></a>");
				String alertas = wrapperPosto.findField("alertasPosto").getValueAsString();
				if (faltaPosto)
				{
					alertas = alertas.replace(";ok", "");
					alertas = alertas.replace("ok", "");
					if (!alertas.contains("falta"))
					{
						alertas = alertas + ";falta";
					}
				}
				else
				{
					if (alertas.contains("falta"))
					{
						alertas = alertas.replace(";falta", " ");
						alertas = alertas.replace("falta", " ");
					}
				}

				if (posto.getnVagas() != listaColab.size())
				{
					alertas = alertas.replace(";ok", "");
					alertas = alertas.replace("ok", "");
					if (!alertas.contains("nVagasDivergentes"))
					{
						alertas = alertas + ";nVagasDivergentes";
					}
				}
				else
				{
					alertas = alertas.replace(";nVagasDivergentes", " ");
					alertas = alertas.replace("nVagasDivergentes", " ");
				}
				alertas = alertas.trim();
				if (alertas.equals(""))
				{
					alertas = "ok";
				}
				if ((isPesquisa && listaColab != null && !listaColab.isEmpty()) || (!isPesquisa))
				{
					alertas += alertaColDem;
				}
				posto.setImgAlertas(retornaImagensAlertas(alertas));
				wrapperPosto.findField("alertasPosto").setValue(alertas);
				PersistEngine.persist(postoVO);
				if ((cbTodos != null && !cbTodos.isEmpty()) || (cbVagas != null && !cbVagas.isEmpty()) || (cbFaltas != null && !cbFaltas.isEmpty()) || (cbDemit != null && !cbDemit.isEmpty()) || (cbOK != null && !cbOK.isEmpty()))
				{
					if (cbTodos != null && !cbTodos.isEmpty() && alertas.contains(cbTodos))
					{
						postos.add(posto);
					}
					else if (cbVagas != null && !cbVagas.isEmpty() && alertas.contains(cbVagas))
					{

						postos.add(posto);
					}
					else if (cbFaltas != null && !cbFaltas.isEmpty() && alertas.contains(cbFaltas))
					{

						postos.add(posto);
					}
					else if (cbDemit != null && !cbDemit.isEmpty() && alertas.contains(cbDemit))
					{
						postos.add(posto);
					}
					else if (cbOK != null && !cbOK.isEmpty() && alertas.contains(cbOK))
					{
						postos.add(posto);
					}
				}
				else
				{
					if ((listaColab != null && listaColab.size() > 0) && !nomeColaborador.equals(""))
					{
						postos.add(posto);
					}
					else if (nomeColaborador.equals(""))
					{
						postos.add(posto);
					}
				}
			}
		}
		return postos;

	}

	public static String validaImagAlertasColaborador(String imgAlertas, List<NeoObject> consideracoes)
	{
		Boolean falta = false;
		for (NeoObject obj : consideracoes)
		{
			EntityWrapper wrapper = new EntityWrapper(obj);
			Boolean isFalta = (Boolean) wrapper.findField("isFalta").getValue();
			if (isFalta)
			{
				falta = true;
				break;
			}
		}

		if (falta)
		{
			if (!imgAlertas.contains("falta"))
			{
				imgAlertas = "falta";
			}
		}
		else
		{
			imgAlertas = "ok";
		}

		return imgAlertas;
	}

	public static String validaImagAlertasMedicao(String imgAlertasMedicao, List<NeoObject> postos)
	{
		Boolean falta = false;
		Boolean vgsDiv = false;
		for (NeoObject obj : postos)
		{
			EntityWrapper wrapper = new EntityWrapper(obj);
			String alertasPosto = String.valueOf(wrapper.findField("alertasPosto").getValue());
			Long nVagas = Long.parseLong(String.valueOf(wrapper.findField("nVagas").getValue()));
			Long nColab = Long.parseLong(String.valueOf(wrapper.findField("listaColaboradores").getValues().size()));

			if (!falta && alertasPosto.contains("falta"))
			{
				falta = true;
			}

			if (!vgsDiv && nVagas != nColab)
			{
				vgsDiv = true;
			}
		}

		if (falta || vgsDiv)
		{
			imgAlertasMedicao = "";
		}
		else
		{
			imgAlertasMedicao = imgAlertasMedicao.replace(";falta", "");
			imgAlertasMedicao = imgAlertasMedicao.replace(";nVagasDivergentes", "");
			imgAlertasMedicao = imgAlertasMedicao.replace("falta", "");
			imgAlertasMedicao = imgAlertasMedicao.replace("nVagasDivergentes", "");
		}

		if (falta)
		{
			if (!imgAlertasMedicao.contains("falta"))
			{
				imgAlertasMedicao = imgAlertasMedicao + "falta";
			}
		}

		if (vgsDiv)
		{
			if (!imgAlertasMedicao.contains("nVagasDivergentes"))
			{
				imgAlertasMedicao = imgAlertasMedicao + ";nVagasDivergentes";
			}
		}

		return imgAlertasMedicao;
	}

	public static ConsideracoesMedicaoVO editarObservacao(String neoId)

	{
		ConsideracoesMedicaoVO consideracao = new ConsideracoesMedicaoVO();
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		if (neoId != null && !neoId.isEmpty())
		{
			Long neoIdQl = Long.parseLong(neoId);
			QLFilter qlNeoId = new QLEqualsFilter("neoId", neoIdQl);
			groupFilter.addFilter(qlNeoId);
		}

		List<NeoObject> objsObservacoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDConsideracoes"), groupFilter);
		if (objsObservacoes != null && objsObservacoes.size() > 0)
		{
			NeoObject object = objsObservacoes.get(0);
			EntityWrapper wrapper = new EntityWrapper(object);
			consideracao.setDescricao(String.valueOf(wrapper.findField("descricao").getValue()));
			consideracao.setFalta((boolean) wrapper.findField("isFalta").getValue());
			if (consideracao.isFalta)
			{
				consideracao.setIsfaltaString("Sim");
			}
			else
			{
				consideracao.setIsfaltaString("Nao");
			}

		}
		return consideracao;
	}

	public static boolean salvarColaborador(String campoCargaHorPos, String campoFuncao, String campoMatricula, String campoNome, String campoCentroCusto, String campoDatAdm, String campoDescMed, String campoSituacao, String campoHorTra, String campoMinTra, String campoHorEx, String campoMinEx, String neoIdPosto, String campoLocal) throws ParseException
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		if (neoIdPosto != null && !neoIdPosto.isEmpty())
		{
			Long neoIdQl = Long.parseLong(neoIdPosto);
			QLFilter qlNeoId = new QLEqualsFilter("neoId", neoIdQl);
			groupFilter.addFilter(qlNeoId);

			List<NeoObject> objsObservacoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDPosto"), groupFilter);
			NeoObject obj = objsObservacoes.get(0);
			EntityWrapper wrapper = new EntityWrapper(obj);
			List<NeoObject> listaColaboradores = (List) wrapper.findField("listaColaboradores").getValue();

			InstantiableEntityInfo InsColaborador = AdapterUtils.getInstantiableEntityInfo("MEDColaboradores");
			NeoObject neoColaborador = InsColaborador.createNewInstance();
			EntityWrapper wColaborador = new EntityWrapper(neoColaborador);
			wColaborador.findField("cargaHorPos").setValue(campoCargaHorPos);
			wColaborador.findField("funcao").setValue(campoFuncao);
			wColaborador.findField("local").setValue(campoLocal);
			wColaborador.findField("numCadColaborador").setValue(Long.parseLong(campoMatricula));
			wColaborador.findField("nomeColaborador").setValue(campoNome);
			wColaborador.findField("codCcu").setValue(campoCentroCusto);
			String datAdmCon = NeoDateUtils.safeDateFormat(OrsegupsUtils.stringToGregorianCalendar(campoDatAdm, "yyyy-MM-dd"), "dd/MM/yyyy");
			wColaborador.findField("datAdm").setValue(datAdmCon);
			wColaborador.findField("descMed").setValue(campoDescMed);
			wColaborador.findField("situacao").setValue(campoSituacao);
			wColaborador.findField("horMed").setValue(Long.parseLong(campoHorTra));
			wColaborador.findField("minMed").setValue(Long.parseLong(campoMinTra));
			wColaborador.findField("horEx").setValue(Long.parseLong(campoHorEx));
			wColaborador.findField("minEx").setValue(Long.parseLong(campoMinEx));

			PersistEngine.persist(neoColaborador);
			listaColaboradores.add(neoColaborador);
			for (NeoObject objCol : listaColaboradores)
			{
				PersistEngine.persist(objCol);
			}

			Long nColaborador = (Long) wrapper.getValue("nColaboradores");
			nColaborador = nColaborador + 1;
			wrapper.setValue("nColaboradores", nColaborador);
			PersistEngine.persist(obj);
		}

		return false;
	}

	public static long retornaContrato(long numpos, long numloc)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsContrato = null;
		PreparedStatement stContrato = null;
		long numCtr = 0;

		StringBuilder queryContrato = new StringBuilder();
		try
		{

			queryContrato.append("SELECT USU_NUMCTR FROM USU_T038CVS WHERE USU_NUMPOS = ? AND USU_NUMLOC = ?");
			stContrato = connVetorh.prepareStatement(queryContrato.toString());
			stContrato.setLong(1, numpos);
			stContrato.setLong(2, numloc);

			rsContrato = stContrato.executeQuery();

			if (rsContrato.next())
			{
				numCtr = rsContrato.getLong("USU_NUMCTR");
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stContrato, rsContrato);
		}
		return numCtr;
	}

	public static void inserePostosSemColaborador(String datCpt)
	{
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT c.usu_numofi,EMP.nomemp, REG.USU_NomReg, usu_apecli, SUBSTRING(hie.CodLoc, 1, 12) AS NumLocCli, orn.numloc, hie.codloc, SUBSTRING(hie.CodLoc, 1, 24) AS CodPai, cvs.usu_nomloc, cvs.usu_desser, pos.usu_codser, cvs.usu_numctr, cvs.usu_numpos, orn.usu_lotorn, ");
		sql.append(" cvs.usu_codreg, orn.usu_cliorn, cvs.usu_sitcvs, cvs.usu_qtdcon, cvs.usu_numloc, cvs.usu_codccu, cvs.usu_codemp, cvs.usu_codfil, cvs.usu_endloc,  ");
		sql.append(" cid.NomCid, cid.EstCid, cvs.usu_endcep, orn.usu_cplctr,orn.usu_nomcid, cvs.usu_preuni * cvs.usu_qtdcvs AS valor, cvs.usu_datini, cvs.usu_datfim, cvs.usu_fimvig, ");
		sql.append(" cvs.usu_nomloc AS cvs_nomloc, cvs.usu_codccu AS cvs_codccu, cvs.usu_numctr AS cvs_numctr, cvs.usu_numpos AS cvs_numpos, pos.usu_peresc, mot.codmot,  mot.desmot ");
		sql.append(" FROM R016ORN orn WITH (NOLOCK)  ");
		sql.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
		sql.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc)  ");
		sql.append(" INNER JOIN USU_T200REG REG ON REG.USU_CodReg = cvs.usu_codreg ");
		sql.append(" INNER JOIN r030emp EMP ON EMP.numemp = cvs.usu_codemp ");
		sql.append(" INNER JOIN Sapiens.dbo.usu_t160ctr C ON C.usu_numctr = cvs.usu_numctr AND C.usu_codemp = cvs.usu_codemp AND c.usu_codfil = cvs.usu_codfil ");
		sql.append(" LEFT JOIN  Sapiens.dbo.USU_T160CVS pos ON pos.usu_codemp = cvs.usu_codemp AND pos.usu_codfil = cvs.usu_codfil AND pos.usu_numctr = cvs.usu_numctr AND pos.usu_numpos = cvs.usu_numpos  ");
		sql.append(" LEFT JOIN R074CID cid WITH (NOLOCK) ON cid.CodCid = orn.usu_codcid  ");
		sql.append(" LEFT JOIN Sapiens.dbo.E021MOT mot ON mot.codmot = pos.usu_codmot ");
		sql.append(" LEFT JOIN Sapiens.dbo.E085CLI cli ON cli.codcli = c.usu_codcli  ");
		sql.append(" WHERE (cvs.usu_taborg = 203)  ");
		sql.append(" AND (LEN(hie.CodLoc) = 27) ");
		sql.append(" AND (cvs.USU_SITCVS = 'S' OR DATEADD(MINUTE, 1439, cvs.usu_datfim) >= ? OR cvs.usu_datfim = '1900-12-31' OR EXISTS (SELECT * FROM R034FUN fun WHERE fun.TIPCOL = 1 AND cvs.usu_numloc = fun.numloc AND cvs.usu_taborg = fun.taborg AND (fun.sitafa <> 7 OR (fun.sitafa = 7 AND fun.datafa > ?)))) ");
		sql.append(" AND cvs.usu_codreg = 9 ");
		sql.append(" AND (hie.codloc LIKE '1.5%' OR hie.codloc LIKE '1.6%') ");
		sql.append(" AND cvs.usu_codemp IN (2, 6, 7, 8) ");
		sql.append(" AND cli.tipemc = 2 and not exists( select 1 from r034fun fun where fun.sitafa <> 7 and fun.numloc = orn.numloc and fun.taborg = orn.taborg) and cvs.usu_qtdcon > 0");
		sql.append(" ORDER BY orn.usu_cliorn, orn.usu_ordexi, hie.codloc ");

		GregorianCalendar datRef = new GregorianCalendar();
		String datCptCop = datCpt;
		datCpt = "01/" + datCpt;
		try
		{
			datRef = OrsegupsUtils.stringToGregorianCalendar(datCpt, "dd/MM/yyyy");
		}
		catch (ParseException e1)
		{
			e1.printStackTrace();
		}

		datRef.set(GregorianCalendar.MONTH, 1);
		datRef.set(GregorianCalendar.DATE, 1);
		datRef.add(GregorianCalendar.DATE, -1);
		datRef.set(GregorianCalendar.HOUR_OF_DAY, 23);
		datRef.set(GregorianCalendar.MINUTE, 59);
		datRef.set(GregorianCalendar.SECOND, 59);

		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsContrato = null;
		PreparedStatement stContrato = null;
		try
		{
			stContrato = connVetorh.prepareStatement(sql.toString());
			stContrato.setTimestamp(1, new Timestamp(datRef.getTimeInMillis()));
			stContrato.setTimestamp(2, new Timestamp(datRef.getTimeInMillis()));
			stContrato.setTimestamp(3, new Timestamp(datRef.getTimeInMillis()));

			rsContrato = stContrato.executeQuery();

			while (rsContrato.next())
			{
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				long numCtr = rsContrato.getLong("usu_numctr");
				QLFilter qlNumCtr = new QLEqualsFilter("contrato", numCtr);

				QLFilter qlDatCpt = new QLEqualsFilter("periodo", datCptCop);

				groupFilter.addFilter(qlNumCtr);
				groupFilter.addFilter(qlDatCpt);

				List<NeoObject> objsContratos = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter);
				EntityWrapper wContrato = null;

				if (objsContratos != null && objsContratos.size() > 0)
				{
					NeoObject obj = objsContratos.get(0);
					wContrato = new EntityWrapper(obj);
					List<NeoObject> postos = new ArrayList<NeoObject>();
					postos = (List) wContrato.findValue("listaPostos");

					//new Posto
					InstantiableEntityInfo insPos = AdapterUtils.getInstantiableEntityInfo("MEDPosto");
					NeoObject objPos = insPos.createNewInstance();
					EntityWrapper wPosto = new EntityWrapper(objPos);
					wPosto.setValue("nomLoc", rsContrato.getString("usu_lotorn") + " - " + rsContrato.getString("usu_nomLoc"));
					wPosto.setValue("nVagas", rsContrato.getLong("usu_qtdcon"));
					wPosto.setValue("nColaboradores", 0L);
					wPosto.setValue("numLoc", rsContrato.getLong("usu_numloc"));
					wPosto.setValue("numPos", rsContrato.getLong("usu_numpos"));
					wPosto.setValue("alertasPosto", "");
					wPosto.setValue("codLoc", rsContrato.getString("codloc"));
					wPosto.setValue("codCcu", rsContrato.getString("usu_codccu"));

					PersistEngine.persist(objPos);
					postos.add(objPos);
					wContrato.setValue("listaPostos", postos);
					PersistEngine.persist(obj);
				}
				else
				{

					InstantiableEntityInfo insMed = AdapterUtils.getInstantiableEntityInfo("MEDMedicaoDeContratos");
					NeoObject objMed = insMed.createNewInstance();
					wContrato = new EntityWrapper(objMed);
					wContrato.setValue("cliente", rsContrato.getString("usu_apecli"));
					wContrato.setValue("contrato", rsContrato.getLong("usu_numctr"));
					wContrato.setValue("codEmpresaContratada", rsContrato.getLong("usu_codemp"));
					wContrato.setValue("empresaContratada", rsContrato.getString("nomemp"));
					wContrato.setValue("responsavel", "Sra. Inês");
					wContrato.setValue("periodo", datCpt);
					wContrato.setValue("codRegional", rsContrato.getLong("usu_codreg"));
					wContrato.setValue("regional", rsContrato.getString("USU_NomReg"));
					wContrato.setValue("numOfi", rsContrato.getString("usu_numofi"));

					//new Posto
					InstantiableEntityInfo insPos = AdapterUtils.getInstantiableEntityInfo("MEDPosto");
					NeoObject objPos = insPos.createNewInstance();
					EntityWrapper wPosto = new EntityWrapper(objPos);
					wPosto.setValue("nomLoc", rsContrato.getString("usu_lotorn") + " - " + rsContrato.getString("usu_nomLoc"));
					wPosto.setValue("nVagas", rsContrato.getLong("usu_qtdcon"));
					wPosto.setValue("nColaboradores", 0L);
					wPosto.setValue("numLoc", rsContrato.getLong("usu_numloc"));
					wPosto.setValue("numPos", rsContrato.getLong("usu_numpos"));
					wPosto.setValue("alertasPosto", "");
					wPosto.setValue("codLoc", rsContrato.getString("codloc"));
					wPosto.setValue("codCcu", rsContrato.getString("usu_codccu"));
					List<NeoObject> listaPostos = new ArrayList<NeoObject>();
					PersistEngine.persist(objPos);
					listaPostos.add(objPos);
					wContrato.setValue("listaPostos", listaPostos);
					PersistEngine.persist(objMed);
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stContrato, rsContrato);
		}

	}

	public static String cadastrarPosto(String codCcu, String periodo, String cliente)
	{

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("select * from D_MEDMedicaoDeContratos med");
		sqlBuilder.append("	inner join D_MEDMedicaoDeContratos_listaPostos medPos on medPos.D_MEDMedicaoDeContratos_neoId = med.neoId");
		sqlBuilder.append("	inner join D_medPosto pos on pos.neoId = medPos.listaPostos_neoId");
		sqlBuilder.append("	where med.periodo = ? and pos.codCcu = ?");

		Connection connVetorh = PersistEngine.getConnection("");
		ResultSet rsContrato = null;
		PreparedStatement stContrato = null;
		try
		{
			stContrato = connVetorh.prepareStatement(sqlBuilder.toString());
			stContrato.setString(1, periodo);
			stContrato.setString(2, codCcu);

			rsContrato = stContrato.executeQuery();

			if (rsContrato.next())
			{
				return "Centro de custo já esta cadastrado na medição de contratos";
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stContrato, rsContrato);
		}

		StringBuilder sql = new StringBuilder();
		Long numctr = 0L;
		Long numpos = 0L;
		Long qtdVagas = 0L;
		Long qtdColab = 0L;
		String alerta = ";vagasDivergentes";
		String nomLoc = "";
		Long numLoc = 0L;
		String codLoc = "";
		cliente = cliente.trim();

		sql.append(" SELECT Top 1 cvs.usu_numctr,cvs.usu_numpos, cvs.usu_numloc, (orn.usu_lotorn+' - '+cvs.usu_nomloc) nomLoc, cvs.usu_qtdcon FROM USU_T038CVS cvs ");
		sql.append(" inner join r016orn orn on orn.numloc = cvs.usu_numloc and orn.taborg = cvs.usu_taborg ");
		sql.append(" WHERE cvs.USU_CODCCU = ? and orn.usu_apecli like '%" + cliente + "%' ORDER BY usu_seqalt desc  ");

		Connection connFusion = null;
		ResultSet rsPosto = null;
		PreparedStatement stPosto = null;

		try
		{
			connFusion = PersistEngine.getConnection("VETORH");
			stPosto = connFusion.prepareStatement(sql.toString());
			stPosto.setString(1, codCcu);
			rsPosto = stPosto.executeQuery();

			while (rsPosto.next())
			{
				numctr = rsPosto.getLong("usu_numctr");
				numpos = rsPosto.getLong("usu_numpos");
				qtdVagas = rsPosto.getLong("usu_qtdcon");
				numLoc = rsPosto.getLong("usu_numloc");
				nomLoc = rsPosto.getString("nomLoc");
				codLoc = retornaCodLoc(numLoc);
			}
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connFusion, stPosto, rsPosto);
		}

		if (numctr > 0)
		{
			NeoObject posto = AdapterUtils.createNewEntityInstance("medPosto");
			EntityWrapper wPostos = new EntityWrapper(posto);
			wPostos.setValue("nomLoc", nomLoc);
			wPostos.setValue("numPos", numpos);
			wPostos.setValue("numLoc", numLoc);
			wPostos.setValue("nColaboradores", qtdColab);
			wPostos.setValue("nVagas", qtdVagas);
			wPostos.setValue("alertasPosto", alerta);
			wPostos.setValue("codLoc", codLoc);
			wPostos.setValue("codCcu", codCcu);
			EscalaPostoVO escalas = null;
			String datRef = "01/" + periodo;
			try
			{
				GregorianCalendar dataCorte = OrsegupsUtils.stringToGregorianCalendar(datRef, "dd/MM/yyyy");
				dataCorte.add(GregorianCalendar.MONTH, -1);
				escalas = QLPresencaUtils.getEscalaPosto(numLoc, 203L, dataCorte);
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			wPostos.setValue("codEsc", escalas.toString());
			PersistEngine.persist(posto);

			QLFilter qlContrato = new QLEqualsFilter("contrato", numctr);
			QLFilter qlPeriodo = new QLEqualsFilter("periodo", periodo);
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(qlContrato);
			groupFilter.addFilter(qlPeriodo);

			List<NeoObject> objsObservacoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter);
			if (objsObservacoes != null && objsObservacoes.size() > 0)
			{
				NeoObject obj = objsObservacoes.get(0);
				EntityWrapper wrapper = new EntityWrapper(obj);
				List<NeoObject> listaPostos = (List) wrapper.findField("listaPostos").getValue();

				wrapper.setValue("listaPostos", listaPostos);
				PersistEngine.persist(obj);
				List<NeoObject> listaColaboradores = retornaListaColaboradoresDoPosto(String.valueOf(numpos), periodo);
				wPostos.setValue("listaColaboradores", listaColaboradores);
				listaPostos.add(posto);
				return "Posto cadastrado com sucesso!";
			}
			else
			{
				return "Contrato " + numctr + " não foi identificado na medição de contratos";
			}
		}
		else
		{
			return "Centro de custo não vinculado com o cliente " + cliente;
		}
	}

	public static String retornaCodLoc(Long numLoc)
	{
		Connection connFusion = null;
		ResultSet rsPosto = null;
		PreparedStatement stPosto = null;
		StringBuilder sql = new StringBuilder();
		String retorno = "";

		sql.append(" SELECT CodLoc FROM R016HIE WHERE NumLoc = ? ");

		try
		{
			connFusion = PersistEngine.getConnection("VETORH");
			stPosto = connFusion.prepareStatement(sql.toString());
			stPosto.setLong(1, numLoc);
			rsPosto = stPosto.executeQuery();

			while (rsPosto.next())
			{
				retorno = rsPosto.getString("CodLoc");
			}
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connFusion, stPosto, rsPosto);
		}
		return retorno;

	}

	public static Boolean removerPosto(String neoId)
	{
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM D_medPosto_listaColaboradores WHERE D_medPosto_neoId = ?  ");

		Connection connFusion = null;
		ResultSet rsPosto = null;
		PreparedStatement stPosto = null;

		try
		{
			connFusion = PersistEngine.getConnection("");
			stPosto = connFusion.prepareStatement(sql.toString());
			stPosto.setString(1, neoId);
			rsPosto = stPosto.executeQuery();

			while (rsPosto.next())
			{
				return false;
			}
		}

		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connFusion, stPosto, rsPosto);
		}

		sql = new StringBuilder();

		Boolean retorno = false;
		Connection conVetorh = PersistEngine.getConnection("");
		PreparedStatement pst = null;
		try
		{
			sql.append("delete D_MEDMedicaoDeContratos_listaPostos where listaPostos_neoId = ?; delete D_medPosto where neoid = ?");
			pst = conVetorh.prepareStatement(sql.toString());

			pst.setString(1, neoId);
			pst.setString(2, neoId);

			int r = pst.executeUpdate();
			System.out.println("dvinc->" + r);
			if (r > 0)
			{
				retorno = true;
			}

			return retorno;
		}
		catch (Exception e)
		{
			System.out.println("Erro ao remover Posto");
			e.printStackTrace();
			return retorno;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conVetorh, pst, null);
		}
	}

	public static List<NeoObject> retornaListaColaboradoresDoPosto(String numPos, String periodo)
	{

		GregorianCalendar datRef = null;
		List<NeoObject> listaColaboradores = new ArrayList<NeoObject>();
		periodo = "01/" + periodo;
		try
		{
			datRef = OrsegupsUtils.stringToGregorianCalendar(periodo, "dd/MM/yyyy");
		}
		catch (ParseException e1)
		{
			e1.printStackTrace();
		}
		datRef.set(GregorianCalendar.DATE, 1);
		datRef.add(GregorianCalendar.MONTH, 1);
		datRef.add(GregorianCalendar.DATE, -1);
		datRef.set(GregorianCalendar.HOUR_OF_DAY, 23);
		datRef.set(GregorianCalendar.MINUTE, 59);
		datRef.set(GregorianCalendar.SECOND, 59);
		System.out.println(NeoDateUtils.safeDateFormat(datRef, "dd/MM/yyyy"));

		StringBuilder queryMedicao = new StringBuilder();
		Connection connVetorh = null;
		ResultSet rsMedicao = null;
		PreparedStatement stMedicao = null;
		try
		{
			queryMedicao.append(" SELECT cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.nomfun,fun.numemp,fun.tipcol,fun.datadm ,isnull(sit.dessit, 'trabalhando') AS DesSit," + "orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp," + "orn.numloc,orn.usu_numemp,isnull(ctr.usu_numofi,'') as usu_numofi, cvs.usu_codccu,cvs.usu_qtdcon");
			queryMedicao.append(" ,hie.CodLoc , ESC.NomEsc FROM R034FUN fun   ");
			queryMedicao.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = ");
			queryMedicao.append(" (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= ?)  ");
			queryMedicao.append(" INNER JOIN R038HCA HCA (NOLOCK) ON HCA.CodCar = FUN.codcar AND HCA.NumCad = FUN.NUMCAD AND HCA.TipCol = FUN.TIPCOL AND HCA.NUMEMP = FUN.numemp AND HCA.DatAlt = ");
			queryMedicao.append(" (SELECT MAX(DATALT) FROM R038HCA HCA2 WHERE HCA2.NUMCAD = HCA.NUMCAD AND ");
			queryMedicao.append("										    HCA2.NUMEMP = HCA.NUMEMP AND ");
			queryMedicao.append("											HCA2.TIPCOL = HCA.TIPCOL AND ");
			queryMedicao.append("											HCA2.DatAlt <= ?)");
			queryMedicao.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = hlo.taborg AND hlo.numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = hie.taborg AND h2.numloc = hie.NumLoc) ");
			queryMedicao.append(" INNER JOIN R024CAR CAR (NOLOCK) ON CAR.CODCAR = HCA.CodCar and car.estcar = 1");
			queryMedicao.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = 203  ");
			queryMedicao.append(" INNER JOIN R038HES HES ON HES.NumCad = FUN.numcad AND HES.NumEmp = FUN.numemp AND HES.TipCol = FUN.tipcol AND HES.DatAlt =   ");
			queryMedicao.append(" (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = HES.NUMEMP AND TABELA001.TIPCOL = HES.TIPCOL AND TABELA001.NUMCAD = HES.NUMCAD AND TABELA001.DATALT <= ?)  ");
			queryMedicao.append(" INNER JOIN R006ESC ESC ON ESC.CodEsc = HES.CodEsc   ");
			queryMedicao.append(" INNER JOIN USU_T038CVS  cvs WITH (NOLOCK) ON cvs.USU_NumLoc = orn.NumLoc AND cvs.USU_TabOrg = orn.TabOrg AND cvs.USU_SeqAlt = (Select MAX(c2.usu_seqalt) FROM USU_T038CVS c2 Where ");
			queryMedicao.append(" c2.usu_taborg = cvs.usu_taborg And ");
			queryMedicao.append(" c2.usu_numloc = cvs.usu_numloc And ");
			queryMedicao.append(" c2.usu_datalt <= ?)  ");
			queryMedicao.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres  ");
			queryMedicao.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND DATEADD(minute,afa.HORAFA,afa.DATAFA) = ");
			queryMedicao.append(" (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 ");
			queryMedicao.append(" WHERE TABELA001.NUMEMP = afa.NUMEMP AND ");
			queryMedicao.append(" TABELA001.TIPCOL = afa.TIPCOL AND ");
			queryMedicao.append(" TABELA001.NUMCAD = afa.NUMCAD AND ");
			queryMedicao.append(" TABELA001.DATAFA <= ? AND ");
			queryMedicao.append(" (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime))))  LEFT JOIN R038AFA afaDem WITH (NOLOCK) ON ");
			queryMedicao.append(" afaDem.NumEmp = fun.NumEmp AND afaDem.TipCol = fun.TipCol AND afaDem.NumCad = fun.NumCad AND afaDem.sitAfa = 7 AND DATEADD(minute,afaDem.HORAFA,afaDem.DATAFA) = ");
			queryMedicao.append(" (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afaDem.NUMEMP AND TABELA001.TIPCOL = afaDem.TIPCOL AND TABELA001.NUMCAD = afaDem.NUMCAD)  ");
			queryMedicao.append(" LEFT JOIN R010SIT SIT ON SIT.CODSIT = AFA.SitAfa ");
			queryMedicao.append(" INNER JOIN USU_T200REG reg on reg.USU_CodReg = orn.usu_codreg");
			queryMedicao.append(" INNER JOIN r030emp emp on emp.numemp = orn.usu_numemp");
			queryMedicao.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.E085CLI CLI ON CLI.CODCLI = ORN.usu_codclisap");
			queryMedicao.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160CTR CTR ON CTR.usu_numctr = cvs.usu_numctr");
			queryMedicao.append(" WHERE fun.DatAdm <= ?  AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(? as float)) as datetime)))  ");
			queryMedicao.append(" AND fun.TipCol = 1  AND orn.usu_numemp IN (2,6,7,8) and orn.usu_codreg in(1,9) and cvs.usu_sitcvs = 'S' and CLI.TipEmc = 2 and cvs.usu_numpos = ?");
			queryMedicao.append(" GROUP BY cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.nomfun,fun.numemp,");
			queryMedicao.append(" fun.tipcol,fun.datadm,DesSit,orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp,orn.numloc,orn.usu_numemp,ctr.usu_numofi,");
			queryMedicao.append(" cvs.usu_codccu,cvs.usu_qtdcon ,hie.CodLoc , ESC.NomEsc");
			queryMedicao.append(" ORDER BY hie.CodLoc,fun.nomfun,cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.numemp, ");
			queryMedicao.append(" fun.tipcol,fun.datadm,DesSit,orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp,orn.numloc,orn.usu_numemp,ctr.usu_numofi, ");
			queryMedicao.append(" cvs.usu_codccu,cvs.usu_qtdcon , ESC.NomEsc");

			connVetorh = PersistEngine.getConnection("VETORH");
			stMedicao = connVetorh.prepareStatement(queryMedicao.toString());
			stMedicao.setTimestamp(1, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(2, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(3, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(4, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(5, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(6, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(7, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(8, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setLong(9, Long.parseLong(numPos));
			rsMedicao = stMedicao.executeQuery();
			String alertaCtr = "ok";
			int inss = 0;

			while (rsMedicao.next())
			{
				int desconto = OrsegupsMedicaoUtilsMensal.retornaInicioDesconto(rsMedicao.getString("datAdm"), datRef);
				inss = OrsegupsMedicaoUtilsMensal.verificaRetornoDoInss(rsMedicao.getLong("numcad"), rsMedicao.getLong("numcad"), 1L, datRef);
				desconto = desconto - inss;
				List<NeoObject> listConsideracoes = OrsegupsMedicaoUtilsMensal.validaAfastamento(rsMedicao.getLong("numemp"), rsMedicao.getLong("numcad"), 1L, datRef, rsMedicao.getLong("NumLoc"));
				NeoObject neoColaborador = AdapterUtils.createNewEntityInstance("MEDColaboradores");
				EntityWrapper wColaborador = new EntityWrapper(neoColaborador);
				if (listConsideracoes != null && listConsideracoes.size() >= 1)
				{
					wColaborador.findField("listaConsideracoes").setValue(listConsideracoes);
					for (int i = 0; i < listConsideracoes.size(); i++)
					{
						NeoObject wFalta = listConsideracoes.get(i);
						EntityWrapper ew = new EntityWrapper(wFalta);
						boolean isFalta = (boolean) ew.findField("isFalta").getValue();
						if (isFalta)
						{
							desconto--;
						}
					}

				}

				Long cargaHorPos = OrsegupsMedicaoUtilsMensal.cargaHorariaDoPosto(rsMedicao.getLong("usu_codemp"), rsMedicao.getLong("usu_codfil"), rsMedicao.getLong("usu_numctr"), rsMedicao.getLong("usu_numpos"));
				if (cargaHorPos != 0L)
				{
					wColaborador.setValue("cargaHorPos", cargaHorPos + " horas");
				}
				else
				{
					wColaborador.setValue("cargaHorPos", "Carga horaria não cadastrada!");
				}

				wColaborador.setValue("funcao", rsMedicao.getString("titred"));
				wColaborador.setValue("local", rsMedicao.getString("usu_lotorn"));
				wColaborador.setValue("numCadColaborador", rsMedicao.getLong("numcad"));
				wColaborador.setValue("nomeColaborador", rsMedicao.getString("nomfun"));
				wColaborador.setValue("datAdm", NeoUtils.safeDateFormat(rsMedicao.getDate("datadm"), "dd/MM/yyyy") + "");
				wColaborador.setValue("codCcu", rsMedicao.getString("usu_codccu"));

				String desMed = desconto + " Dia(s)" + " ";
				wColaborador.setValue("descMed", desMed);
				wColaborador.setValue("situacao", rsMedicao.getString("DesSit"));
				wColaborador.setValue("horMed", 0L);
				wColaborador.setValue("minMed", 0L);
				wColaborador.setValue("horEx", 0L);
				wColaborador.setValue("minEx", 0L);
				String alertaColaborador = "ok";
				if (!desMed.contains("30"))
				{
					alertaColaborador = "falta";
				}

				if (inss > 0)
				{
					if (!alertaColaborador.contains("ok"))
					{
						alertaColaborador += ";retornoINSS";
					}
					else
					{
						alertaColaborador = ";retornoINSS";
					}
				}
				wColaborador.setValue("alertasColaborador", alertaColaborador);
				wColaborador.setValue("codEsc", rsMedicao.getString("NomEsc"));
				PersistEngine.persist(neoColaborador);
				listaColaboradores.add(neoColaborador);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stMedicao, rsMedicao);
		}
		return listaColaboradores;
	}
}
