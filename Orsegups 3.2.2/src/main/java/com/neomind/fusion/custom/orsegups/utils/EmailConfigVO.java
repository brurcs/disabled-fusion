package com.neomind.fusion.custom.orsegups.utils;

public class EmailConfigVO
{
	private String userName;
	private String password;
	private String smtpport;
	private String protocol;
	private String host;

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getSmtpport()
	{
		return smtpport;
	}

	public void setSmtpport(String smtpport)
	{
		this.smtpport = smtpport;
	}

	public String getProtocol()
	{
		return protocol;
	}

	public void setProtocol(String protocol)
	{
		this.protocol = protocol;
	}

	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

}
