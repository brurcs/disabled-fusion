package com.neomind.fusion.custom.orsegups.presenca.vo;

public class InspecaoVO {
	
	private String neoId;
	private String code;
	private String startDate;
	private String situacao;
	private String inspetor;
	private String linkTarefa;
	private String nomePesquisa;
	
	public String getNeoId() {
		return neoId;
	}
	public void setNeoId(String neoId) {
		this.neoId = neoId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getInspetor() {
		return inspetor;
	}
	public void setInspetor(String inspetor) {
		this.inspetor = inspetor;
	}
	public String getLinkTarefa() {
		return linkTarefa;
	}
	public void setLinkTarefa(String linkTarefa) {
		this.linkTarefa = linkTarefa;
	}
	public String getNomePesquisa() {
		return nomePesquisa;
	}
	public void setNomePesquisa(String nomePesquisa) {
		this.nomePesquisa = nomePesquisa;
	}
	@Override
	public String toString() {
		return "InspecaoVO [neoId=" + neoId + ", code=" + code + ", startDate="
				+ startDate + ", situacao=" + situacao + ", inspetor="
				+ inspetor + ", linkTarefa=" + linkTarefa + ", nomePesquisa="
				+ nomePesquisa + "]";
	}
	
	
	
	

}
