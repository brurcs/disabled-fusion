/**
 * ModeloPlanoGerarContasInGridCta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano;

public class ModeloPlanoGerarContasInGridCta  implements java.io.Serializable {
    private java.lang.String abrCta;

    private java.lang.String anaSin;

    private java.lang.String claCta;

    private java.lang.String codCcu;

    private java.lang.Integer codMpc;

    private java.lang.Integer ctaCtb;

    private java.lang.Integer ctaRed;

    private java.lang.String desCta;

    private java.lang.String exiAux;

    private java.lang.String exiRat;

    private java.lang.Integer forRat;

    private java.lang.Integer metCon;

    private java.lang.Integer modCtb;

    private java.lang.String natCta;

    private java.lang.Integer tipCcu;

    private java.lang.String valFin;

    private java.lang.String valIni;

    public ModeloPlanoGerarContasInGridCta() {
    }

    public ModeloPlanoGerarContasInGridCta(
           java.lang.String abrCta,
           java.lang.String anaSin,
           java.lang.String claCta,
           java.lang.String codCcu,
           java.lang.Integer codMpc,
           java.lang.Integer ctaCtb,
           java.lang.Integer ctaRed,
           java.lang.String desCta,
           java.lang.String exiAux,
           java.lang.String exiRat,
           java.lang.Integer forRat,
           java.lang.Integer metCon,
           java.lang.Integer modCtb,
           java.lang.String natCta,
           java.lang.Integer tipCcu,
           java.lang.String valFin,
           java.lang.String valIni) {
           this.abrCta = abrCta;
           this.anaSin = anaSin;
           this.claCta = claCta;
           this.codCcu = codCcu;
           this.codMpc = codMpc;
           this.ctaCtb = ctaCtb;
           this.ctaRed = ctaRed;
           this.desCta = desCta;
           this.exiAux = exiAux;
           this.exiRat = exiRat;
           this.forRat = forRat;
           this.metCon = metCon;
           this.modCtb = modCtb;
           this.natCta = natCta;
           this.tipCcu = tipCcu;
           this.valFin = valFin;
           this.valIni = valIni;
    }


    /**
     * Gets the abrCta value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return abrCta
     */
    public java.lang.String getAbrCta() {
        return abrCta;
    }


    /**
     * Sets the abrCta value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param abrCta
     */
    public void setAbrCta(java.lang.String abrCta) {
        this.abrCta = abrCta;
    }


    /**
     * Gets the anaSin value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return anaSin
     */
    public java.lang.String getAnaSin() {
        return anaSin;
    }


    /**
     * Sets the anaSin value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param anaSin
     */
    public void setAnaSin(java.lang.String anaSin) {
        this.anaSin = anaSin;
    }


    /**
     * Gets the claCta value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return claCta
     */
    public java.lang.String getClaCta() {
        return claCta;
    }


    /**
     * Sets the claCta value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param claCta
     */
    public void setClaCta(java.lang.String claCta) {
        this.claCta = claCta;
    }


    /**
     * Gets the codCcu value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codMpc value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return codMpc
     */
    public java.lang.Integer getCodMpc() {
        return codMpc;
    }


    /**
     * Sets the codMpc value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param codMpc
     */
    public void setCodMpc(java.lang.Integer codMpc) {
        this.codMpc = codMpc;
    }


    /**
     * Gets the ctaCtb value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return ctaCtb
     */
    public java.lang.Integer getCtaCtb() {
        return ctaCtb;
    }


    /**
     * Sets the ctaCtb value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param ctaCtb
     */
    public void setCtaCtb(java.lang.Integer ctaCtb) {
        this.ctaCtb = ctaCtb;
    }


    /**
     * Gets the ctaRed value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the desCta value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return desCta
     */
    public java.lang.String getDesCta() {
        return desCta;
    }


    /**
     * Sets the desCta value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param desCta
     */
    public void setDesCta(java.lang.String desCta) {
        this.desCta = desCta;
    }


    /**
     * Gets the exiAux value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return exiAux
     */
    public java.lang.String getExiAux() {
        return exiAux;
    }


    /**
     * Sets the exiAux value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param exiAux
     */
    public void setExiAux(java.lang.String exiAux) {
        this.exiAux = exiAux;
    }


    /**
     * Gets the exiRat value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return exiRat
     */
    public java.lang.String getExiRat() {
        return exiRat;
    }


    /**
     * Sets the exiRat value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param exiRat
     */
    public void setExiRat(java.lang.String exiRat) {
        this.exiRat = exiRat;
    }


    /**
     * Gets the forRat value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return forRat
     */
    public java.lang.Integer getForRat() {
        return forRat;
    }


    /**
     * Sets the forRat value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param forRat
     */
    public void setForRat(java.lang.Integer forRat) {
        this.forRat = forRat;
    }


    /**
     * Gets the metCon value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return metCon
     */
    public java.lang.Integer getMetCon() {
        return metCon;
    }


    /**
     * Sets the metCon value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param metCon
     */
    public void setMetCon(java.lang.Integer metCon) {
        this.metCon = metCon;
    }


    /**
     * Gets the modCtb value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return modCtb
     */
    public java.lang.Integer getModCtb() {
        return modCtb;
    }


    /**
     * Sets the modCtb value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param modCtb
     */
    public void setModCtb(java.lang.Integer modCtb) {
        this.modCtb = modCtb;
    }


    /**
     * Gets the natCta value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return natCta
     */
    public java.lang.String getNatCta() {
        return natCta;
    }


    /**
     * Sets the natCta value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param natCta
     */
    public void setNatCta(java.lang.String natCta) {
        this.natCta = natCta;
    }


    /**
     * Gets the tipCcu value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return tipCcu
     */
    public java.lang.Integer getTipCcu() {
        return tipCcu;
    }


    /**
     * Sets the tipCcu value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param tipCcu
     */
    public void setTipCcu(java.lang.Integer tipCcu) {
        this.tipCcu = tipCcu;
    }


    /**
     * Gets the valFin value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return valFin
     */
    public java.lang.String getValFin() {
        return valFin;
    }


    /**
     * Sets the valFin value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param valFin
     */
    public void setValFin(java.lang.String valFin) {
        this.valFin = valFin;
    }


    /**
     * Gets the valIni value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @return valIni
     */
    public java.lang.String getValIni() {
        return valIni;
    }


    /**
     * Sets the valIni value for this ModeloPlanoGerarContasInGridCta.
     * 
     * @param valIni
     */
    public void setValIni(java.lang.String valIni) {
        this.valIni = valIni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModeloPlanoGerarContasInGridCta)) return false;
        ModeloPlanoGerarContasInGridCta other = (ModeloPlanoGerarContasInGridCta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.abrCta==null && other.getAbrCta()==null) || 
             (this.abrCta!=null &&
              this.abrCta.equals(other.getAbrCta()))) &&
            ((this.anaSin==null && other.getAnaSin()==null) || 
             (this.anaSin!=null &&
              this.anaSin.equals(other.getAnaSin()))) &&
            ((this.claCta==null && other.getClaCta()==null) || 
             (this.claCta!=null &&
              this.claCta.equals(other.getClaCta()))) &&
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codMpc==null && other.getCodMpc()==null) || 
             (this.codMpc!=null &&
              this.codMpc.equals(other.getCodMpc()))) &&
            ((this.ctaCtb==null && other.getCtaCtb()==null) || 
             (this.ctaCtb!=null &&
              this.ctaCtb.equals(other.getCtaCtb()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.desCta==null && other.getDesCta()==null) || 
             (this.desCta!=null &&
              this.desCta.equals(other.getDesCta()))) &&
            ((this.exiAux==null && other.getExiAux()==null) || 
             (this.exiAux!=null &&
              this.exiAux.equals(other.getExiAux()))) &&
            ((this.exiRat==null && other.getExiRat()==null) || 
             (this.exiRat!=null &&
              this.exiRat.equals(other.getExiRat()))) &&
            ((this.forRat==null && other.getForRat()==null) || 
             (this.forRat!=null &&
              this.forRat.equals(other.getForRat()))) &&
            ((this.metCon==null && other.getMetCon()==null) || 
             (this.metCon!=null &&
              this.metCon.equals(other.getMetCon()))) &&
            ((this.modCtb==null && other.getModCtb()==null) || 
             (this.modCtb!=null &&
              this.modCtb.equals(other.getModCtb()))) &&
            ((this.natCta==null && other.getNatCta()==null) || 
             (this.natCta!=null &&
              this.natCta.equals(other.getNatCta()))) &&
            ((this.tipCcu==null && other.getTipCcu()==null) || 
             (this.tipCcu!=null &&
              this.tipCcu.equals(other.getTipCcu()))) &&
            ((this.valFin==null && other.getValFin()==null) || 
             (this.valFin!=null &&
              this.valFin.equals(other.getValFin()))) &&
            ((this.valIni==null && other.getValIni()==null) || 
             (this.valIni!=null &&
              this.valIni.equals(other.getValIni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAbrCta() != null) {
            _hashCode += getAbrCta().hashCode();
        }
        if (getAnaSin() != null) {
            _hashCode += getAnaSin().hashCode();
        }
        if (getClaCta() != null) {
            _hashCode += getClaCta().hashCode();
        }
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodMpc() != null) {
            _hashCode += getCodMpc().hashCode();
        }
        if (getCtaCtb() != null) {
            _hashCode += getCtaCtb().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getDesCta() != null) {
            _hashCode += getDesCta().hashCode();
        }
        if (getExiAux() != null) {
            _hashCode += getExiAux().hashCode();
        }
        if (getExiRat() != null) {
            _hashCode += getExiRat().hashCode();
        }
        if (getForRat() != null) {
            _hashCode += getForRat().hashCode();
        }
        if (getMetCon() != null) {
            _hashCode += getMetCon().hashCode();
        }
        if (getModCtb() != null) {
            _hashCode += getModCtb().hashCode();
        }
        if (getNatCta() != null) {
            _hashCode += getNatCta().hashCode();
        }
        if (getTipCcu() != null) {
            _hashCode += getTipCcu().hashCode();
        }
        if (getValFin() != null) {
            _hashCode += getValFin().hashCode();
        }
        if (getValIni() != null) {
            _hashCode += getValIni().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModeloPlanoGerarContasInGridCta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "modeloPlanoGerarContasInGridCta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anaSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "anaSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMpc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMpc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaCtb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaCtb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exiAux");
        elemField.setXmlName(new javax.xml.namespace.QName("", "exiAux"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exiRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "exiRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modCtb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modCtb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
