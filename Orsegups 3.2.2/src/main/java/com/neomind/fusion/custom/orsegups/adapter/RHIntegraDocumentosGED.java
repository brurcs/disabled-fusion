package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.e2doc.E2docIndexacaoBean;
import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * Efetua a integração no GED dos documentos anexados no Fluxo de Admissão
 * 
 * @author herisson.ferreira
 *
 */
public class RHIntegraDocumentosGED implements AdapterInterface{
	
	private static final Log log = LogFactory.getLog(RHIntegraDocumentosGED.class);
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
			
		try {
			
			List<NeoObject> listaArquivos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RH01ParametrizacaoGED"));
			
			for(NeoObject arquivo : listaArquivos) {
				
				try {
					EntityWrapper ewArquivo = new EntityWrapper(arquivo);
					
					String campo = (String) ewArquivo.findGenericValue("codigo");
					String tipoArquivo = (String) ewArquivo.findGenericValue("tipoDocumento");
					String classificacao = (String) ewArquivo.findGenericValue("classe");
					
					NeoFile arquivoAdmissao = NeoUtils.safeNeoFile((NeoFile) processEntity.findGenericValue(campo));
					
					indexarDocumentos(processEntity, arquivoAdmissao, tipoArquivo, classificacao);
					
				} catch (Exception e) {
					e.printStackTrace();
					log.error(this.getClass().getSimpleName() + " (RH01ParametrizacaoGED) - Erro na integração dos arquivos do GED. ");
				}
				
			}
			
			List<NeoObject> listaDependentes = processEntity.findGenericValue("listaDepend");
			
			if(NeoUtils.safeIsNotNull(listaDependentes)) {
				insereDocumentosDependentes(processEntity, listaDependentes);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" #### ERRO ao integrar os documentos no GED ("+this.getClass().getSimpleName()+" - "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss")+")");
			throw new WorkflowException("Erro ao integrar os documentos no GED");
		}
		
	}
	
	private void insereDocumentosDependentes(EntityWrapper processEntity, List<NeoObject> listaDependentes) {
		
		NeoFile cpfDependente = null;
		NeoFile certidaoCasamento = null;
		NeoFile certidaodependente = null;
		NeoFile documentoVacinacao = null;
		
		for (int i = 0; i < listaDependentes.size(); i++)
		{
			try {
				
				cpfDependente = new EntityWrapper(listaDependentes.get(i)).findGenericValue("documentoCPF");
				certidaodependente = new EntityWrapper(listaDependentes.get(i)).findGenericValue("certidaodependente");
				certidaoCasamento = new EntityWrapper(listaDependentes.get(i)).findGenericValue("certidaoCasamento");
				documentoVacinacao = new EntityWrapper(listaDependentes.get(i)).findGenericValue("docvacinacao");
				

				if(NeoUtils.safeIsNotNull(cpfDependente)) {
					indexarDocumentos(processEntity, cpfDependente, "CERTIDOES E CERTIFICADOS", "CPF - DEPENDENTE");
				}
				
				if(NeoUtils.safeIsNotNull(certidaodependente)) {
					indexarDocumentos(processEntity, certidaodependente, "CERTIDOES E CERTIFICADOS", "CERTIDÃO DE NASCIMENTO - DEPENDENTE");
				}
				
				if(NeoUtils.safeIsNotNull(documentoVacinacao)) {
					indexarDocumentos(processEntity, documentoVacinacao, "CERTIDOES E CERTIFICADOS", "CARTEIRA DE VACINAÇÃO/TIPAGEM SANGUÍNEA - DEPENDENTE");
				}
				
				if(NeoUtils.safeIsNotNull(certidaoCasamento)) {
					indexarDocumentos(processEntity, certidaoCasamento, "CERTIDOES E CERTIFICADOS", "CASAMENTO");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(" #### ERRO ao integrar os documentos de dependente no GED ("+this.getClass().getSimpleName()+" - "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss")+")");
			}
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

	public void indexarDocumentos(EntityWrapper processEntity, NeoFile arquivo, String tipoDocumento, String classificacao) throws Exception {
		
		if (NeoUtils.safeIsNotNull(arquivo) && NeoUtils.safeIsNotNull(tipoDocumento) && NeoUtils.safeIsNotNull(classificacao)) {
			
			E2docIndexacaoBean e2docIndexacaoBean = new E2docIndexacaoBean();
			GregorianCalendar dataAtual = new GregorianCalendar();
			
			Long matricula = NeoUtils.safeOutputLong((Long) processEntity.findGenericValue("matricula"));
			String empresa = NeoUtils.safeOutputString((String) processEntity.findGenericValue("emp.nomemp"));
			String cpf = NeoUtils.safeOutputString((String) processEntity.findGenericValue("numcpf"));
			String colaborador = NeoUtils.safeOutputString((String) processEntity.findGenericValue("nomCompletoFC"));
			
			cpf = cpf.replaceAll("\\D", "");
			
			e2docIndexacaoBean.setEmpresa(empresa);
			e2docIndexacaoBean.setMatricula(matricula.toString());
			e2docIndexacaoBean.setColaborador(colaborador);
			e2docIndexacaoBean.setCpf(Long.parseLong(cpf));
			e2docIndexacaoBean.setTipoDocumento(tipoDocumento);
			e2docIndexacaoBean.setClasse(classificacao);
			e2docIndexacaoBean.setData(dataAtual.getTime());
			e2docIndexacaoBean.setObservacao(String.valueOf(Calendar.getInstance().getTimeInMillis()));
			e2docIndexacaoBean.setTipo("i");
			e2docIndexacaoBean.setModelo("documentação rh");
			e2docIndexacaoBean.setFormato(arquivo.getSufix());
			e2docIndexacaoBean.setDocumento(arquivo.getBytes());

			E2docUtils.indexarDocumento(e2docIndexacaoBean);
			
		}

	}
	
}
