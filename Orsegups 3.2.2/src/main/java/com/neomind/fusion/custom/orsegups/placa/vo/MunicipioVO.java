package com.neomind.fusion.custom.orsegups.placa.vo;

public class MunicipioVO {

    Integer cdMunicipio;
    String nmMunicipio;
    
    public Integer getCdMunicipio() {
        return cdMunicipio;
    }
    public void setCdMunicipio(Integer cdMunicipio) {
        this.cdMunicipio = cdMunicipio;
    }
    public String getNmMunicipio() {
        return nmMunicipio;
    }
    public void setNmMunicipio(String nmMunicipio) {
        this.nmMunicipio = nmMunicipio;
    }
}
