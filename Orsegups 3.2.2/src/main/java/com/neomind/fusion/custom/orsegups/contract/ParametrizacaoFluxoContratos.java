package com.neomind.fusion.custom.orsegups.contract;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

/**
 * Usa um eform para consultar paramtros diversos que hoje estao fixos no código. Tudo que for paramtetro do fluxo de contratos deve ser passado para esse eform.
 * Desta maneira, não será necessário reiniciar o fusion se algum parametro precisar ser alterado.
 * @author danilo.silva
 *
 */
public class ParametrizacaoFluxoContratos {

	static String eformParams = "FGCParametrizacao";
	
	/**
	 * Consulta um parametro e retorna null caso o mesmo não seja econtrato ou ocorra algum erro de acesso ao eform
	 * @param parameter
	 * @return
	 */
	public static String findParameter(String parameter){
		String retorno = "";
		try{
			QLGroupFilter gp = new QLGroupFilter("AND");	
			gp.addFilter( new QLEqualsFilter("parametro", parameter ) );
			NeoObject oControleParametreizacao = PersistEngine.getObject(AdapterUtils.getEntityClass(eformParams), gp );
			EntityWrapper wControleParametreizacao = new EntityWrapper(oControleParametreizacao);
			return (String) wControleParametreizacao.findValue("valor");
		}catch(Exception e){
			return null;
		}
	}

}
