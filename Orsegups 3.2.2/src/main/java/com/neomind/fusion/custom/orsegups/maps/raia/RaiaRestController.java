package com.neomind.fusion.custom.orsegups.maps.raia;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/raia")
public class RaiaRestController {
	private RaiaRestEngine raiaEngine = new RaiaRestEngine();
	@GET
    @Path("/getCentrais")
    @Produces("application/json")
	public List<RaiaVO> getCentrais() {
		return raiaEngine.getCentrais();
	}
	@GET
    @Path("/totalPatrimonios")
    @Produces("application/json")
	public Integer getTotalPatrimonios() {
		return raiaEngine.countTotalPatrimonios();
	}
	
	@GET
    @Path("/totalParticoes")
    @Produces("application/json")
	public Integer getTotalParticoes() {
		return raiaEngine.countTotalParticoes();
	}
	
	@GET
    @Path("/totalArmado")
    @Produces("application/json")
	public Integer getTotalArmado() {
		return raiaEngine.countTotalArmado();
	}
	
	@GET
    @Path("/totalArmadoParcial")
    @Produces("application/json")
	public Integer getTotalArmadoParcial() {
		return raiaEngine.countTotalArmadoParcial();
	}
	
	@GET
    @Path("/totalDesarmado")
    @Produces("application/json")
	public Integer getTotalDesarmado() {
		return raiaEngine.countTotalDesarmado();
	}
	@GET
    @Path("/totalCftv")
    @Produces("application/json")
	public Integer getTotalCftv() {
		return raiaEngine.countTotalCftv();
	}
	@GET
    @Path("/totalDesabilitado")
    @Produces("application/json")
	public Integer getTotalDesabilitado() {
		return raiaEngine.countTotalDesabilitado();
	}
	
	@GET
    @Path("/totalAlarmeEmAndamento")
    @Produces("application/json")
	public Integer getTotalAlarmeEmAndamento() {
		return raiaEngine.findAllAlarmeEmAndamento();
	}
	
	@GET
    @Path("/totalIntrusao")
    @Produces("application/json")
	public List<RaiaVO> getTotalIntrusao() {
		return raiaEngine.findAllIntrusao();
	}
	@GET
    @Path("/totalDesarmadoForaHorario")
    @Produces("application/json")
	public List<RaiaVO> getTotalDesarmadoForaHorario() {
		return raiaEngine.findAllDesarmadoForaHorario();
	}
	@GET
    @Path("/totalNaoArmado")
    @Produces("application/json")
	public List<RaiaVO> getTotalNaoArmado() {
		return raiaEngine.findAllNaoArmado();
	}
	@GET
    @Path("/totalCftvOffline")
    @Produces("application/json")
	public List<RaiaVO> getTotalCftvOffline() {
		return raiaEngine.findAllCftvOffline();
	}
	@GET
    @Path("/totalDeslocamento")
    @Produces("application/json")
	public List<RaiaVO> getTotalDeslocamento() {
		return raiaEngine.findAllDeslocamento();
	}
	@GET
    @Path("/totalBateriaBaixa")
    @Produces("application/json")
	public List<RaiaVO> getTotalBateriaBaixa() {
		return raiaEngine.findAllBateriaBaixa();
	}
	@GET
    @Path("/totalPanico")
    @Produces("application/json")
	public List<RaiaVO> getTotalPanico() {
		return raiaEngine.findAllPanico();
	}
	
	@GET
    @Path("/totalNoLocal")
    @Produces("application/json")
	public List<RaiaVO> getTotalNoLocal() {
		return raiaEngine.findAllNoLocal();
	}
	
	@GET
    @Path("/totalFalhaComunicacao")
    @Produces("application/json")
	public List<RaiaVO> getTotalFalhaComunicacao() {
		return raiaEngine.findAllFalhaComunicacao();
	}
	@GET
    @Path("/totalMonitoramentoImagens")
    @Produces("application/json")
	public List<RaiaVO> getTotalMonitoramentoImagens() {
		return raiaEngine.findAllMonitoramentoImagens();
	}
	
	@GET
    @Path("/totalFaltaEnergia")
    @Produces("application/json")
	public List<RaiaVO> getTotalFaltaEnergia() {
		return raiaEngine.findAllFaltaEnergia();
	}
	@GET
    @Path("/totalManutencaoAlarme")
    @Produces("application/json")
	public List<RaiaVO> getTotalManutencaoAlarme() {
		return raiaEngine.findAllManutencaoAlarme();
	}
	@GET
    @Path("/totalManutencaoCftv")
    @Produces("application/json")
	public List<RaiaVO> getTotalManutencaoCftv() {
		return raiaEngine.findAllManutencaoCftv();
	}
	
}
