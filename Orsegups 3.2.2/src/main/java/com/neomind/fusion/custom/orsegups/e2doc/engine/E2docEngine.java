package com.neomind.fusion.custom.orsegups.e2doc.engine;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.custom.orsegups.e2doc.vo.E2docVO;
import com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pasta;
import com.neomind.fusion.custom.orsegups.e2doc.xml.privado.Pastas;
import com.neomind.fusion.custom.orsegups.e2doc.xml.rh.Doc;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.DocumentoTreeVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QlPresencaArquivoUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

/**
 * Classe feita para trabalhar com WEBSERVICE E2DOC
 * 
 * Vamos utilizar apenas requisições GET e POST
 * 
 * Metodos existentes para validação de usuário e tempo de token para pesquisa e
 * download
 * 
 * 
 * @author William Bastos
 */

public class E2docEngine {
    private static final Log log = LogFactory.getLog(E2docEngine.class);
    private static final int TAM_MAX_BUFFER = 10240;

    public E2docEngine() {

    }


    private String requestSOAP(String urlWebService, String soapAction, String requestXML) {
	String resultado = "";
	HttpURLConnection urlConnection = null;
	OutputStream out = null;
	InputStream in = null;
	BufferedReader reader = null;

	try {
	    URL url = new URL(urlWebService);

	    urlConnection = (HttpURLConnection) url.openConnection();
	    urlConnection.setDoOutput(true);
	    urlConnection.setRequestMethod("POST");
	    urlConnection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
	    urlConnection.setRequestProperty("SOAPAction", soapAction);

	    String request = requestXML;

	    out = urlConnection.getOutputStream();
	    out.write(request.getBytes());
	    out.flush();

	    // Verifica se a resposta está ok antes de solicitar os dados
	    if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
		in = new BufferedInputStream(urlConnection.getInputStream());

		reader = new BufferedReader(new InputStreamReader(in, "UTF-8"), TAM_MAX_BUFFER);

		StringBuilder builder = new StringBuilder();

		for (String line = null; (line = reader.readLine()) != null;) {
		    builder.append(line).append("\n");
		}

		resultado = builder.toString();
	    } else {
		System.out.println("WebService ResponseCode: " + urlConnection.getResponseMessage());
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("WebService" + e.toString());
	} finally {
	    try {
		if (out != null) {

		    out.close();

		}
		if (in != null) {

		    in.close();

		}
		if (urlConnection != null) {
		    urlConnection.disconnect();
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	return resultado;
    }

    /**
     * POST /e2web.service/servicos.asmx HTTP/1.1 Host: 192.168.20.89
     * Content-Type: text/xml; charset=utf-8 Content-Length: length SOAPAction:
     * "localhost/e2web.service/servicos.asmx/AutenticarUsuario" <?xml
     * version="1.0" encoding="utf-8"?> <soap:Envelope
     * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     * xmlns:xsd="http://www.w3.org/2001/XMLSchema"
     * xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> <soap:Body>
     * <AutenticarUsuario xmlns="localhost/e2web.service/servicos.asmx">
     * <s_usuario>string</s_usuario> <s_senha>string</s_senha>
     * <s_base>string</s_base> </AutenticarUsuario> </soap:Body>
     * </soap:Envelope>
     * 
     * @return
     */

    @SuppressWarnings("unused")
    private String autenticarUsuarioWebServiceDocSOAP(String base) {
	String retorno = null;
	try {
	    E2docVO e2docVO = (E2docVO) E2docUtils.getConfigE2DOC();
	    if (e2docVO != null && base != null && !base.isEmpty()) {
		String urlWebService = e2docVO.getIp() + "/e2web.service2/servicos.asmx";
		String soapAction = "localhost/e2web.service2/servicos.asmx/AutenticarUsuario";
		String requestXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			+ "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "<soap:Body>"
			+ "<AutenticarUsuario xmlns=\"localhost/e2web.service/servicos.asmx\">" + "<s_usuario>" + e2docVO.getUsuario() + "</s_usuario>" + "<s_senha>" + e2docVO.getSenha() + "</s_senha>" + "<s_base>"
			+ base + "</s_base>" + "</AutenticarUsuario>" + "</soap:Body>" + "</soap:Envelope>";

		retorno = requestSOAP(urlWebService, soapAction, requestXML);

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO autenticarUsuarioWebServiceDocSOAP: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

	}
	return retorno;
    }

    /**
     * POST /e2web.service/servicos.asmx HTTP/1.1 Host: 192.168.20.89
     * Content-Type: text/xml; charset=utf-8 Content-Length: length SOAPAction:
     * "localhost/e2web.service/servicos.asmx/Pesquisar"
     * 
     * <?xml version="1.0" encoding="utf-8"?> <soap:Envelope
     * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     * xmlns:xsd="http://www.w3.org/2001/XMLSchema"
     * xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> <soap:Body>
     * <Pesquisar xmlns="localhost/e2web.service/servicos.asmx">
     * <key>string</key> <pesquisa>string</pesquisa> </Pesquisar> </soap:Body>
     * </soap:Envelope>
     */

    @SuppressWarnings("unused")
    private String pesquisarUsuarioWebServiceDocSOAP(String pesquisa, String base) {
	String retorno = null;
	try {
	    E2docVO e2docVO = (E2docVO) E2docUtils.getConfigE2DOC();
	    if (e2docVO != null && pesquisa != null && !pesquisa.isEmpty()) {
		String urlWebService = e2docVO.getIp() + "/e2web.service2/servicos.asmx";
		String soapAction = "localhost/e2web.service2/servicos.asmx/Pesquisar";

		String requestXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			+ "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "<soap:Body>"
			+ "<Pesquisar xmlns=\"localhost/e2web.service/servicos.asmx\">" + "<key>" + e2docVO.getKey() + "</key>" + "<pesquisa>" + pesquisa + "</pesquisa>" + "</Pesquisar>" + "</soap:Body>"
			+ "</soap:Envelope>";

		retorno = requestSOAP(urlWebService, soapAction, requestXML);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO pesquisarUsuarioWebServiceDocPost: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
	return retorno;
    }


    /**
     * GET /e2web.service/servicos.asmx/Pesquisar?key=string&pesquisa=string
     * HTTP/1.1 Host: 192.168.20.89 HTTP/1.1 200 OK Content-Type: text/xml;
     * charset=utf-8 Content-Length: length <?xml version="1.0"
     * encoding="utf-8"?> <string
     * xmlns="localhost/e2web.service/servicos.asmx">string</string>
     * 
     * @param pesquisa
     * @param base
     * @return
     */

    public static String pesquisarUsuarioWebServiceDocGET2(String pesquisa, String base) {
	String retorno = null;
	try {
	    E2docVO e2docVO = (E2docVO) E2docUtils.getConfigE2DOC();
	    if (e2docVO != null && pesquisa != null && !pesquisa.isEmpty()) {
	    	String url = e2docVO.getIp() + "/e2web.service2/servicos.asmx/Pesquisar?key=" + e2docVO.getKey();
            String params = "&pesquisa=<![CDATA[" + URLEncoder.encode(pesquisa, "UTF-8") + "]]>";
            System.out.println(url + params);
            String urlWebService = url + params;

            retorno = E2docUtils.requestGET(urlWebService);

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO pesquisarUsuarioWebServiceDocGET: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
	return retorno;
    }

    private String pesquisaDocumentoWebServiceDocGET(String id) {
	String retorno = null;
	try {
	    E2docVO e2docVO = (E2docVO) E2docUtils.getConfigE2DOC();
	    if (e2docVO != null && id != null && !id.isEmpty()) {
		String urlWebService = e2docVO.getIp() + "/e2web.service2/servicos.asmx/LinkImgSeguranca?key=" + e2docVO.getKey() + "&id=" + URLEncoder.encode(id, "UTF-8") + "&seguranca=false";

		retorno = E2docUtils.requestGET(urlWebService);

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO pesquisarUsuarioWebServiceDocPost: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
	return retorno;
    }

    private String pesquisaDocumentoWebServiceDocGET2(String id) {
	String retorno = null;
	try {
	    E2docVO e2docVO = (E2docVO) E2docUtils.getConfigE2DOC();
	    if (e2docVO != null && id != null && !id.isEmpty()) {
		String urlWebService = e2docVO.getIp() + "/e2web.service2/servicos.asmx/LinkImgSeguranca?key=" + e2docVO.getKey() + "&id=" + URLEncoder.encode(id, "UTF-8") + "&seguranca=false";

		System.out.println("URL ARQUIVO: " + urlWebService);

		retorno = E2docUtils.requestGET(urlWebService);

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO pesquisarUsuarioWebServiceDocPost: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
	return retorno;
    }

    public File retornaLinkImagemDocumento(String id) {
	String link = E2docUtils.trataRetorno(pesquisaDocumentoWebServiceDocGET(id), "link");

	URL url = null;
	File file = null;
	try {
	    url = new URL("http://" + link);

	    System.out.println("URL is: " + url.toString());

	    file = new File("doc");

	    FileUtils.copyURLToFile(url, file);

	} catch (MalformedURLException e) {

	    e.printStackTrace();
	} catch (IOException e) {

	    e.printStackTrace();
	}

	return file;
    }

//    public File retornaLinkImagemDocumento2(String id) {
//	String link = E2docUtils.trataRetorno(pesquisaDocumentoWebServiceDocGET2(id), "link");
//
//	URL url = null;
//	File file = null;
//	try {
//	    url = new URL("http://" + link);
//
//	    System.out.println("URL is: " + url.toString());
//
//	    file = new File("doc");
//
//	    FileUtils.copyURLToFile(url, file);
//
//	} catch (MalformedURLException e) {
//
//	    e.printStackTrace();
//	} catch (IOException e) {
//
//	    e.printStackTrace();
//	}
//
//	return file;
//    }
    
    
    public HashMap<String, Object> retornaLinkImagemDocumento2(String id) {
	String link = E2docUtils.trataRetorno(pesquisaDocumentoWebServiceDocGET2(id), "link");
	String[] linkBroke = link.split("[.]");
	String ext = linkBroke[linkBroke.length-1];
	URL url = null;
	File file = null;
	try {
	    url = new URL("http://" + link);

	    System.out.println("URL is: " + url.toString());

	    file = new File("doc");

	    FileUtils.copyURLToFile(url, file);

	} catch (MalformedURLException e) {

	    e.printStackTrace();
	} catch (IOException e) {

	    e.printStackTrace();
	}
	HashMap<String, Object> retorno = new HashMap<String, Object>();
	retorno.put("ext", ext);
	retorno.put("file", file);
	return retorno;
    }

    public static void inserirNovaKey(String base) {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("usuario", "administrador"));

	    List<NeoObject> keyList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("E2DOCConfiguracoes"), filter);
	    PersistEngine.getEntityManager().flush();

	    if (keyList != null && !keyList.isEmpty()) {
		NeoObject neoObject = (NeoObject) keyList.get(0);

		EntityWrapper keyWrapper = new EntityWrapper(neoObject);

		String key = E2docUtils.trataRetorno(E2docUtils.autenticarUsuarioWebServiceDocGET(base), "string");

		keyWrapper.findField("key").setValue(key);

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.MINUTE, +14);

		keyWrapper.findField("dateKey").setValue(calendar);

		PersistEngine.persist(neoObject);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO inserirNovaKey: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}

    }

    public static void verificaValidadeKey(String base) {

	try {
	    QLOpFilter datefilterfim = new QLOpFilter("dateKey", "<", new GregorianCalendar());

	    QLGroupFilter filtroMnc = new QLGroupFilter("AND");
	    filtroMnc.addFilter(datefilterfim);
	    List<NeoObject> sessions = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("E2DOCConfiguracoes"), filtroMnc);

	    if (sessions != null && !sessions.isEmpty()) {
		inserirNovaKey("RH");
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO inserirNovaKey: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
    }

    private synchronized List<DocumentoTreeVO> trataRetornoPesquisa2(String retorno) {
	/**
	 * NOVOS OBJETOS
	 */
	List<DocumentoTreeVO> listaDocumentos = new ArrayList<>();

	Map<String, DocumentoTreeVO> mapaDocs = new HashMap<>();
	Map<DocumentoTreeVO, ArrayList<String>> filhosTemp = new HashMap<>();	// map de pastas com uma lista para controle das subpastas para cada pasta
	/**
	 * FIM NOVOS OBJETOS
	 */
	com.neomind.fusion.custom.orsegups.e2doc.xml.rh.Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(com.neomind.fusion.custom.orsegups.e2doc.xml.rh.Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<com.neomind.fusion.custom.orsegups.e2doc.xml.rh.Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, com.neomind.fusion.custom.orsegups.e2doc.xml.rh.Pastas.class);

	    pastas = (com.neomind.fusion.custom.orsegups.e2doc.xml.rh.Pastas) je.getValue();

	    List<com.neomind.fusion.custom.orsegups.e2doc.xml.rh.Pasta> listaPasta = pastas.getPasta();
	    
	    long id = 0;
	    
	    for (com.neomind.fusion.custom.orsegups.e2doc.xml.rh.Pasta pasta : listaPasta) {

		String tipo = OrsegupsUtils.retiraCaracteresAcentuados(pasta.getIndices().getI4().toUpperCase());

		List<Doc> docs = pasta.getDoc();
				
		for (Doc doc : docs) {
		    if (doc != null && doc.getDes() != null){
			DocumentoTreeVO pai = null;
			if (!mapaDocs.containsKey(tipo)) {
			    pai = new DocumentoTreeVO();
			    pai.setNome("[E2DOC/RH] "+tipo);
			    pai.setChildren(new ArrayList<DocumentoTreeVO>());
			    pai.setState("closed");
			    pai.setId(++id);
			    mapaDocs.put(tipo, pai);
			    filhosTemp.put(pai, new ArrayList<String>());
			} else {
			    pai = mapaDocs.get(tipo);
			}
			
			String des = doc.getDes().toUpperCase(); 
			
			if (des.equals("OUTROS")){
			    
			    DocumentoTreeVO novoDoc = new DocumentoTreeVO();
			    
			    String competencia = "";
			    GregorianCalendar gData = new GregorianCalendar();
			    
			    //BLOCO DE TRATATIVA DE DATA
			    String comp = pasta.getIndices().getI3();
			    comp = comp.trim();
			    if (comp == null || comp.isEmpty() || comp.equals(" ")) {
				comp = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy");
			    }
			    if (comp != null && !comp.isEmpty() && comp.contains("/")) {
				String compArray[] = comp.split("/");
				Long mes = 0L;
				Long ano = 0L;
				if (compArray.length >= 3) {
				    mes = Long.parseLong(compArray[1].trim());
				    ano = Long.parseLong(compArray[2].trim());
				} else {
				    mes = Long.parseLong(compArray[0].trim());
				    ano = Long.parseLong(compArray[1].trim());
				}
				
				if (mes != null && mes != 0 && ano != null && ano != 0) {
				    competencia = comp;
				    gData = new GregorianCalendar(ano.intValue(), mes.intValue() - 1, 1);
				} else {
				    gData = new GregorianCalendar(1900, 0, 1);
				}
			    }
			    //FIM BLOCO DATA 
			    novoDoc.setNome(tipo);	    
			    novoDoc.setData(competencia);
			    novoDoc.setgData(gData);
			    novoDoc.setId(++id);
			    
			    String link = "<a href=\"https://intranet.orsegups.com.br/fusion/services/documentos/downloadImagem2/" + URLEncoder.encode(doc.getId().trim(), "UTF-8") + "\" target=\"_blank\" class=\"easyui-linkbutton\" data-options=\"iconCls:'icon-search'\" style=\"width:80px\"><img src='imagens/icones_final/document_search_16x16-trans.png' align='absmiddle'></a>";
			    novoDoc.setLink(link);
			    
			    
			    pai.getChildren().add(novoDoc);
			}else{
			    
			    DocumentoTreeVO novoFilho = null; 
			    
			    if (!pai.getChildren().isEmpty()){
				List<DocumentoTreeVO> paiTemp = new ArrayList<DocumentoTreeVO>(); // Lista temporária para gravar as novas subpastas que serão adicionadas na pasta pai
				ArrayList<String> listaFilhos = filhosTemp.get(pai); // recupera a lista de subpastas ja adicionadas
				for (DocumentoTreeVO filho : pai.getChildren()) { // for para verificar se subpasta atual ja esta adicionada na pasta atual
				    if (filho.getNome().equals(des)) {
					if (filho.getChildren() != null){
					    novoFilho = filho;
					}else{
					    if (!listaFilhos.contains(des)){
						novoFilho = new DocumentoTreeVO();
						novoFilho.setNome(des);
						novoFilho.setChildren(new ArrayList<DocumentoTreeVO>());
						novoFilho.setState("closed");
						novoFilho.setId(++id);					
						paiTemp.add(novoFilho);
						listaFilhos.add(des);
					    }
					}
				    } else {
					if (!listaFilhos.contains(des)){
					    novoFilho = new DocumentoTreeVO();
					    novoFilho.setNome(des);
					    novoFilho.setChildren(new ArrayList<DocumentoTreeVO>());
					    novoFilho.setState("closed");
					    novoFilho.setId(++id);					
					    paiTemp.add(novoFilho);
					    listaFilhos.add(des);
					}
				    }
				}
				pai.getChildren().addAll(paiTemp); // adiciona as subpastas ainda nao adicionadas na pasta pai
			    }else{
				novoFilho = new DocumentoTreeVO();
				novoFilho.setNome(des);
				novoFilho.setChildren(new ArrayList<DocumentoTreeVO>());
				novoFilho.setState("closed");
				novoFilho.setId(++id);
				pai.getChildren().add(novoFilho);
				ArrayList<String> listaFilhos = filhosTemp.get(pai);
				listaFilhos.add(des);
			    }
			    
			    
			    
			    DocumentoTreeVO novoDoc = new DocumentoTreeVO();
			    
			    String competencia = "";
			    GregorianCalendar gData = new GregorianCalendar();
			    
			    //BLOCO DE TRATATIVA DE DATA
			    String comp = pasta.getIndices().getI3();
			    comp = comp.trim();
			    if (comp == null || comp.isEmpty() || comp.equals(" ")) {
				comp = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy");
			    }
			    if (comp != null && !comp.isEmpty() && comp.contains("/")) {
				String compArray[] = comp.split("/");
				Long mes = 0L;
				Long ano = 0L;
				if (compArray.length >= 3) {
				    mes = Long.parseLong(compArray[1].trim());
				    ano = Long.parseLong(compArray[2].trim());
				} else {
				    mes = Long.parseLong(compArray[0].trim());
				    ano = Long.parseLong(compArray[1].trim());
				}
				
				if (mes != null && mes != 0 && ano != null && ano != 0) {
				    competencia = comp;
				    gData = new GregorianCalendar(ano.intValue(), mes.intValue() - 1, 1);
				} else {
				    gData = new GregorianCalendar(1900, 0, 1);
				}
			    }
			    //FIM BLOCO DATA 
			    novoDoc.setNome(des);	    
			    novoDoc.setData(competencia);
			    novoDoc.setgData(gData);
			    novoDoc.setId(++id);
			    
			    String link = "<a href=\"https://intranet.orsegups.com.br/fusion/services/documentos/downloadImagem2/" + URLEncoder.encode(doc.getId().trim(), "UTF-8") + "\" target=\"_blank\" class=\"easyui-linkbutton\" data-options=\"iconCls:'icon-search'\" style=\"width:80px\"><img src='imagens/icones_final/document_search_16x16-trans.png' align='absmiddle'></a>";
			    
			    novoDoc.setLink(link);
			    
			    novoFilho.getChildren().add(novoDoc);
			}
		    }
		    
		    
		    
		}
	
	    }
	    
	    listaDocumentos.addAll(mapaDocs.values());
	    
	    System.out.println(pastas);

	} catch (JAXBException e) {
	    e.printStackTrace();
	} catch (UnsupportedEncodingException e) {
	    e.printStackTrace();
	}

	return listaDocumentos;
    }

    public List<DocumentoTreeVO> pesquisaDocumentosColaborador(String cpf) {
	List<DocumentoTreeVO> documentos = new ArrayList<>();

	try {
	    List<String> pesquisa = new ArrayList<String>();
	    pesquisa.add("<modelo>documentação rh</modelo><indice0>CPF</indice0><valor0>" + cpf + "</valor0>");

	    if (pesquisa != null && !pesquisa.isEmpty()) {

		for (String value : pesquisa) {
		    verificaValidadeKey("RH");

		    String retorno = pesquisarUsuarioWebServiceDocGET2(value.trim(), "RH");

		    documentos = trataRetornoPesquisa2(retorno);
		}

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO pesquisaDocumentosColaborador: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
	
	removeDocumentosTemporarios();;
	
	return documentos;
    }

    public synchronized Pastas pesquisaContratosPrivados(HashMap<String, String> atributosPesquisa) {

	String pesquisa = "<modelo>comercial privado</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	verificaValidadeKey("RH");

	String retorno = pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");
	
	retorno = retorno.replaceAll("&", " ");

	Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosPrivados(retorno);
	}
	
	removeDocumentosTemporarios();;
	
	return pastas;
    }

    public Map<String, List<Pasta>> getImoveisPorTipo(List<Pasta> pastas) {

	Map<String, List<Pasta>> resultado = new HashMap<>();

	if (pastas != null) {
	    for (Pasta pasta : pastas) {

		String tipo = pasta.getIndices().getI4();

		if (resultado.containsKey(tipo)) {
		    List<Pasta> manipula = resultado.get(tipo);
		    manipula.add(pasta);
		} else {
		    List<Pasta> novaLista = new ArrayList<>();
		    novaLista.add(pasta);
		    resultado.put(tipo, novaLista);
		}

	    }
	}

	return resultado;

    }

    public Map<String, Map<String, List<Pasta>>> getImoveisPorTipo(Map<String, List<Pasta>> listaCliente) {

	Map<String, Map<String, List<Pasta>>> resultado = new HashMap<>();

	if (listaCliente != null) {
	    Set<String> keys = listaCliente.keySet();
	    if (keys != null) {
		for (String key : keys) {

		    List<Pasta> pastas = listaCliente.get(key);
		    if (pastas != null) {
			resultado.put(key, getImoveisPorTipo(pastas));
		    }

		}
	    }
	}

	return resultado;

    }
    
    public Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>> getImoveisLocadosPorTipo(List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta> pastas) {

	Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>> resultado = new HashMap<>();

	if (pastas != null) {
	    for (com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta pasta : pastas) {

		String tipo = pasta.getIndices().getI0();

		if (resultado.containsKey(tipo)) {
		    List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta> manipula = resultado.get(tipo);
		    manipula.add(pasta);
		} else {
		    List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta> novaLista = new ArrayList<>();
		    novaLista.add(pasta);
		    resultado.put(tipo, novaLista);
		}

	    }
	}

	return resultado;

    }

    public Map<String, Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>>> getImoveisLocadosPorTipo(Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>> listaCliente) {

	Map<String, Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>>> resultado = new HashMap<>();

	if (listaCliente != null) {
	    Set<String> keys = listaCliente.keySet();
	    if (keys != null) {
		for (String key : keys) {

		    List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta> pastas = listaCliente.get(key);
		    if (pastas != null) {
			resultado.put(key, getImoveisLocadosPorTipo(pastas));
		    }

		}
	    }
	}

	return resultado;

    }

    public Map<String, Map<String, List<Pasta>>> getImoveisPorCliente(com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas pastas) {
	Map<String, List<Pasta>> resultado = new HashMap<>();
	if (pastas != null) {
	    for (Pasta pasta : pastas.getPasta()) {

		String cliente = pasta.getIndices().getI0() + " / " + pasta.getIndices().getI3();

		if (resultado.containsKey(cliente)) {
		    List<Pasta> manipula = resultado.get(cliente);
		    manipula.add(pasta);
		} else {
		    List<Pasta> novaLista = new ArrayList<>();
		    novaLista.add(pasta);
		    resultado.put(cliente, novaLista);
		}
	    }
	}
	return getImoveisPorTipo(resultado); 
    }
    
    public Map<String, Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>>> getImoveisLocadosPorCliente(com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas pastas) {
	Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>> resultado = new HashMap<>();
	if (pastas != null) {
	    for (com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta pasta : pastas.getPasta()) {

		String cliente = pasta.getIndices().getI8();

		if (resultado.containsKey(cliente)) {
		    List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta> manipula = resultado.get(cliente);
		    manipula.add(pasta);
		} else {
		    List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta> novaLista = new ArrayList<>();
		    novaLista.add(pasta);
		    resultado.put(cliente, novaLista);
		}
	    }
	}
	return getImoveisLocadosPorTipo(resultado); 
    }
    
    public synchronized com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas pesquisaImoveisProprios(HashMap<String, String> atributosPesquisa, String modelo) {

	String pesquisa = "<modelo>"+modelo+"</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	verificaValidadeKey("RH");

	String retorno = pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

	retorno = retorno.replaceAll("&", " ");

	com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosImoveis(retorno);
	}
	
	removeDocumentosTemporarios();;

	return pastas;
    }
    
    public synchronized com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas pesquisaImoveisLocados(HashMap<String, String> atributosPesquisa, String modelo) {

	String pesquisa = "<modelo>"+modelo+"</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	verificaValidadeKey("RH");

	String retorno = pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

	retorno = retorno.replaceAll("&", " ");

	com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosImoveisLocados(retorno);
	}
	
	removeDocumentosTemporarios();;

	return pastas;
    }

    public Map<String, List<String>> getImoveisComboData() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	String sqlMunicipio = " SELECT DISTINCT s_indice37 FROM pasta WITH(NOLOCK) WHERE LEN(s_indice37) > 0 ";

	String sqlTipo = " SELECT DISTINCT s_indice36 FROM pasta WITH(NOLOCK) WHERE LEN(s_indice36) > 0 ";

	List<String> municipios = new ArrayList<>();
	List<String> tipo = new ArrayList<>();

	try {
	    conn = PersistEngine.getConnection("E2DOC");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sqlMunicipio);

	    while (rs.next()) {
		municipios.add(rs.getString("s_indice37"));
	    }

	    rs.close();
	    stmt.close();

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sqlTipo);

	    while (rs.next()) {
		tipo.add(rs.getString("s_indice36"));
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	Map<String, List<String>> resultado = new HashMap<>();

	resultado.put("municipio", municipios);
	resultado.put("tipoImovel", tipo);

	return resultado;
    }
    
    public Map<String, List<String>> getImoveisComboDataLocados() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	String sqlMunicipio = " SELECT DISTINCT s_indice53 FROM pasta WITH(NOLOCK) WHERE LEN(s_indice53) > 0 ORDER BY s_indice53 ";

	String sqlTipo = " SELECT DISTINCT s_indice36 FROM pasta WITH(NOLOCK) WHERE LEN(s_indice36) > 0 ";

	List<String> municipios = new ArrayList<>();
	List<String> tipo = new ArrayList<>();

	try {
	    conn = PersistEngine.getConnection("E2DOC");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sqlMunicipio);

	    while (rs.next()) {
		municipios.add(rs.getString("s_indice53"));
	    }

	    rs.close();
	    stmt.close();

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sqlTipo);

	    while (rs.next()) {
		tipo.add(rs.getString("s_indice36"));
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	Map<String, List<String>> resultado = new HashMap<>();

	resultado.put("municipio", municipios);
	resultado.put("tipoImovel", tipo);

	return resultado;
    }

    private Pastas tratarRetornoDocumentosPrivados(String retorno) {

	Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, Pastas.class);

	    pastas = (Pastas) je.getValue();

	} catch (JAXBException e) {
	    e.printStackTrace();
	}

	return pastas;

    }

    private com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas tratarRetornoDocumentosImoveis(String retorno) {

	com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas.class);

	    pastas = (com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pastas) je.getValue();
	    
	} catch (JAXBException e) {
	    e.printStackTrace();
	}
	
	return pastas;

    }
    
    private com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas tratarRetornoDocumentosImoveisLocados(String retorno) {

	com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas.class);

	    pastas = (com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pastas) je.getValue();
	    
	} catch (JAXBException e) {
	    e.printStackTrace();
	}
	
	return pastas;

    }

    public Map<String, List<Pasta>> ordernarMapImoveis(Map<String, List<Pasta>> map){
	Map<String,Integer> ordenacao = new LinkedHashMap<String, Integer>();
    		ordenacao.put("MATRÍCULA"	, 1);
    		ordenacao.put("CONTRATOS"	, 2);
    		ordenacao.put("ESCRITURA"	, 3);
    		ordenacao.put("HABITA - SE"	, 4);
    		ordenacao.put("HABITE - SE"	, 4);
    		ordenacao.put("ALVARÁS"		, 5);
    		ordenacao.put("IPTU/RIP/ITR"	, 6);
    		ordenacao.put("PLANTA"		, 7);
    		ordenacao.put("DIVERSOS"	, 8);
	Map<String, List<Pasta>> retorno = new LinkedHashMap<String, List<Pasta>>();
	for (String key : ordenacao.keySet()){
	    List<Pasta> lista = map.get(key);
	    if (lista != null){
		retorno.put(key, map.get(key));
	    }
	}
	return retorno;
    }
    
    public Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>> ordernarMapImoveisLocados(Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>> map){
	Map<String,Integer> ordenacao = new LinkedHashMap<String, Integer>();
    		ordenacao.put("MATRÍCULA"	, 1);
    		ordenacao.put("CONTRATOS"	, 2);
    		ordenacao.put("ESCRITURA"	, 3);
    		ordenacao.put("HABITA - SE"	, 4);
    		ordenacao.put("HABITE - SE"	, 4);
    		ordenacao.put("ALVARÁS"		, 5);
    		ordenacao.put("IPTU/RIP/ITR"	, 6);
    		ordenacao.put("PLANTA"		, 7);
    		ordenacao.put("DIVERSOS"	, 8);
	Map<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>> retorno = new LinkedHashMap<String, List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta>>();
	for (String key : ordenacao.keySet()){
	    List<com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisLocados.Pasta> lista = map.get(key);
	    if (lista != null){
		retorno.put(key, map.get(key));
	    }
	}
	return retorno;
    }

    public static void removeDocumentosTemporarios() {
	try {

	    String path = "'";

	    if (path != null) {
		File folder = new File(path);

		if (!folder.exists()) {
		    System.out.print("Não existe arquivo! ");
		} else if (folder.isDirectory()) {

		    File[] arquivos = folder.listFiles();

		    if (arquivos != null) {
			for (File file : arquivos) {
			    file.delete();
			}
		    }
		} else {
		    System.out.print("Não existe o diretório! ");
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public synchronized ColaboradorVO pesquisaColaborador(String numemp, String numcad, String cpf) {

	ColaboradorVO colaborador = null;
	try {

	    Long numempL = Long.parseLong(numemp);
	    Long numcadL = Long.parseLong(numcad);
	    Long numCpf = Long.parseLong(cpf);

	    colaborador = new ColaboradorVO();
	    colaborador = QlPresencaArquivoUtils.buscaFichaColaboradorArquivo(numempL, numcadL, numCpf);

	    String cpfColaborador = String.format("%011d", colaborador.getCpf());
	    String sexo = "";

	    cpfColaborador = OrsegupsUtils.formatarString(cpfColaborador, "###.###.###-##");

	    if (colaborador.getSexo().equals("M")) {
		sexo = "Masculino";

	    } else {
		sexo = "Feminino";
	    }
	    colaborador.setSexo(sexo);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return colaborador;
    }

    public List<ColaboradorVO> getListaColaboradoresArquivo(Long cpf, String colab, Long matr, Long empr) {

	Connection connVetorh = null;
	PreparedStatement stColaborador = null;
	ResultSet rsColaborador = null;
	List<ColaboradorVO> listaColaboradores = new ArrayList<ColaboradorVO>();
	ColaboradorVO colaborador = new ColaboradorVO();

	boolean passou = false;
	try {
	    connVetorh = PersistEngine.getConnection("VETORH");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    /*** Recupera Colaboradores ***/

	    StringBuffer queryColaborador = new StringBuffer();

	    if (colab != null && colab.equals("") || cpf != null && !cpf.equals("") || matr != null && !matr.equals("") && empr != null && empr > 0) {
		queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, ");
		queryColaborador.append("        fun.USU_MatRes ");
		queryColaborador.append(" FROM R034FUN fun  ");
		queryColaborador.append(" WHERE ");
	    }

	    if (cpf > 0) {
		queryColaborador.append(" fun.NumCpf = " + cpf);
		passou = true;
	    }

	    if (!colab.equals("")) {
		if (passou) {
		    queryColaborador.append(" AND fun.nomfun like '%" + colab + "%'");
		} else {
		    queryColaborador.append(" fun.nomfun like '%" + colab + "%'");
		    passou = true;
		}
	    }

	    if (empr != null && empr > 0) {
		if (matr != null && matr > 0) {
		    if (passou) {
			queryColaborador.append(" AND fun.numcad = " + matr);
			queryColaborador.append(" AND fun.numemp = " + empr);
		    } else {
			queryColaborador.append(" fun.numcad = " + matr);
			queryColaborador.append(" AND fun.numemp = " + empr);
		    }
		}
	    }

	    // Asseio

	    stColaborador = connVetorh.prepareStatement(queryColaborador.toString());

	    rsColaborador = stColaborador.executeQuery();

	    log.warn("SQL - getListaColaboradores - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rsColaborador.next()) {
		colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsColaborador.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsColaborador.getLong("TipCol"));
		colaborador.setNumeroCadastro(rsColaborador.getLong("NumCad"));
		colaborador.setNomeColaborador(rsColaborador.getString("NomFun"));
		colaborador.setCpf(rsColaborador.getLong("NumCpf"));
		colaborador.setSexo(rsColaborador.getString("TipSex"));
		colaborador.setDataNascimento(rsColaborador.getDate("DatNas"));
		colaborador.setDataAdmissao(rsColaborador.getDate("datAdm"));
		colaborador.setMatrizResponsabilidade(rsColaborador.getString("USU_MatRes"));

		// Adicionando a lista
		listaColaboradores.add(colaborador);
		break;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
	}

	return listaColaboradores;
    }
    
    public synchronized com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas pesquisaLivrosOcorrencia(HashMap<String, String> atributosPesquisa) {

	String pesquisa = "<modelo>vigilância humana</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	verificaValidadeKey("RH");

	String retorno = pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

	retorno = retorno.replaceAll("&", " ");

	com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosOcorrencia(retorno);
	}
	
	removeDocumentosTemporarios();;
	
	return pastas;
    }
    
    private com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas tratarRetornoDocumentosOcorrencia(String retorno) {

	com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas.class);

	    pastas = (com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia.Pastas) je.getValue();

	} catch (JAXBException e) {
	    e.printStackTrace();
	}

	return pastas;

    }
    
    public synchronized com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas pesquisaRessarcimentos(HashMap<String, String> atributosPesquisa, String modelo) {

	String pesquisa = "<modelo>"+modelo+"</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	verificaValidadeKey("RH");

	String retorno = pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

	retorno = retorno.replaceAll("&", " ");

	com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosRessarcimento(retorno);
	}
	
	removeDocumentosTemporarios();;
	
	return pastas;
    }
    
    private com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas tratarRetornoDocumentosRessarcimento(String retorno) {

	com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas.class);

	    pastas = (com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento.Pastas) je.getValue();

	} catch (JAXBException e) {
	    e.printStackTrace();
	}

	return pastas;

    }
    
    public synchronized com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas pesquisaCompras(HashMap<String, String> atributosPesquisa) {

	String pesquisa = "<modelo>Compras</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	verificaValidadeKey("RH");

	String retorno = pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

	retorno = retorno.replaceAll("&", " ");

	com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosCompras(retorno);
	}
	
	removeDocumentosTemporarios();;
	
	return pastas;
    }
    
    private com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas tratarRetornoDocumentosCompras(String retorno) {

	com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas.class);

	    pastas = (com.neomind.fusion.custom.orsegups.e2doc.xml.compras.Pastas) je.getValue();

	} catch (JAXBException e) {
	    e.printStackTrace();
	}

	return pastas;

    }
    
    public List<String> getTiposDocumentosCompras(){
    	
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet rs = null;

    	StringBuilder sql = new StringBuilder();

    	sql.append("SELECT DISTINCT s_indice47 FROM PASTA WHERE LEN(s_indice47)>0");
    	
    	List<String> retorno = new ArrayList<String>();
    	
    	try {
    	    conn = PersistEngine.getConnection("E2DOC");
    	    //conn = PersistEngine.getConnection("FUSIONPROD");
    	    stmt = conn.createStatement();
    	    rs = stmt.executeQuery(sql.toString());

    	    while (rs.next()) {
    		retorno.add(rs.getString(1));
    	    }

    	} catch (SQLException e) {
    	    e.printStackTrace();
    	} finally {
    	    OrsegupsUtils.closeConnection(conn, stmt, rs);
    	}
    	
    	
    	return retorno;
        }
    
    public synchronized com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas pesquisaDocumentacaoLegal(HashMap<String, String> atributosPesquisa) {

    	String pesquisa = "<modelo>documentos legais</modelo>";

    	int numIndice = 0;
    	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

    		String nomeAtributo = entry.getKey();
    		String valorAtributo = entry.getValue();

    		pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + ">"
    				+ "<valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
    		numIndice++;

    	}

    	verificaValidadeKey("RH");

    	String retorno = pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

    	retorno = retorno.replaceAll("&", " ");

    	com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas pastas = null;

    	if (!retorno.contains("erro")) {
    		pastas = this.tratarRetornoDocumentacaoLegal(retorno);
    	}

    	removeDocumentosTemporarios();

    	return pastas;
    }

    private com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas tratarRetornoDocumentacaoLegal(String retorno) {

    	com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas pastas = null;

    	try {
    		JAXBContext jaxbContext = JAXBContext.newInstance(com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas.class);
    		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    		StreamSource streamSource = new StreamSource(new StringReader(retorno));
    		JAXBElement<com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas.class);

    		pastas = (com.neomind.fusion.custom.orsegups.e2doc.xml.documentacaoLegal.Pastas) je.getValue();

    	} catch (JAXBException e) {
    		e.printStackTrace();
    	}

    	return pastas;

    }

    public List<String> getTiposDocumentacaoLegal(){

    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet rs = null;

    	StringBuilder sql = new StringBuilder();

    	sql.append("SELECT DISTINCT s_indice100 FROM PASTA WHERE LEN(s_indice100)>0"); 

    	List<String> retorno = new ArrayList<String>();

    	try {
    		conn = PersistEngine.getConnection("E2DOC");
    		stmt = conn.createStatement();
    		rs = stmt.executeQuery(sql.toString());

    		while (rs.next()) {
    			retorno.add(rs.getString(1));
    		}

    	} catch (SQLException e) {
    		e.printStackTrace();
    	} finally {
    		OrsegupsUtils.closeConnection(conn, stmt, rs);
    	}


    	return retorno;
    }
    
    public Map<String, List<String>> getTiposContratosPrivados() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	String sql = "SELECT DISTINCT s_indice71 FROM PASTA WHERE LEN(s_indice71) > 0";

	List<String> documentos = new ArrayList<>();

	try {
	    conn = PersistEngine.getConnection("E2DOC");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    while (rs.next()) {
	    documentos.add(rs.getString("s_indice71"));
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	Map<String, List<String>> resultado = new HashMap<>();

	resultado.put("tipoDocumentos", documentos);

	return resultado;
    }
    
}
