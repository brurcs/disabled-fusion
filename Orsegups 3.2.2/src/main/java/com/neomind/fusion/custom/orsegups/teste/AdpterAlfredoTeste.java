package com.neomind.fusion.custom.orsegups.teste;

import java.sql.Time;
import java.util.Calendar;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class AdpterAlfredoTeste implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Calendar dataParaSolicitacao = (Calendar) processEntity.getValue("dataSolicitacao");
		Calendar dataAtual = Calendar.getInstance();
		Time horaI = (java.sql.Time) processEntity.findValue("horaIsolicitacao");
		Time horaF = (java.sql.Time) processEntity.findValue("horaFsolicitacao");

		if (dataAtual.after(dataParaSolicitacao))
		{
			throw new WorkflowException("Data permanencia menor que data atual.");
		}

		if (horaI.after(horaF))
		{
			throw new WorkflowException("Hora final menor que inicial.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}

}
