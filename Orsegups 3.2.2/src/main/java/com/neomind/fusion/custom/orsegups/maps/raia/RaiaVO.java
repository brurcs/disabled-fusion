package com.neomind.fusion.custom.orsegups.maps.raia;

import java.util.List;

public class RaiaVO {
	private String conta;
	private String cdCliente;
	private boolean status;
	private String uf;
	private String descricao;
	private String cidade;
	private String bairro;
	private String log;
	private List<String> eventosAtivos;
	
	public String getConta() {
		return conta;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public List<String> getEventosAtivos() {
		return eventosAtivos;
	}
	public void setEventosAtivos(List<String> eventosAtivos) {
		this.eventosAtivos = eventosAtivos;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
}
