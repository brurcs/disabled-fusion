package com.neomind.fusion.custom.orsegups.contract.converter;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;

public class RazaoSocialConverter  extends StringConverter 
{
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		String html = super.getHTMLInput(field, origin);
		
		String script = "";
		script += "<script>"
				+ "if ($('#var_dadosGeraisContrato__responsavelContrato__').val() == ''){"
				+ "    $('#var_dadosGeraisContrato__responsavelContrato__').val($('#var_novoCliente__razaoSocial__').val());"
				+ "}"
				+ "$('#var_novoCliente__razaoSocial__').keyup(function(){"
				+ "    $('#var_novoCliente__nomeFantasia__').val($('#var_novoCliente__razaoSocial__').val());"
				+ "    $('#var_novoCliente__nomeContato__').val($('#var_novoCliente__razaoSocial__').val());"
				+ "    $('#var_dadosGeraisContrato__responsavelContrato__').val($('#var_novoCliente__razaoSocial__').val());"
				+ "});"
				+ "$('#var_novoCliente__telefone1__').keyup(function(){"
				+ "    $('#var_novoCliente__telefone2__').val($('#var_novoCliente__telefone1__').val());"
				+ "});"
				+ "$('#var_novoCliente__email__').keyup(function(){"
				+ "    $('#var_dadosGeraisContrato__emailContato__').val($('#var_novoCliente__email__').val());"
				+ "});"
				+ ""
				+ "</script>";
		
		
		return html + script;
	}
}
