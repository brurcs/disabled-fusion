package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class JACancelaTarefasEmDuplicidade implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {

	GregorianCalendar dataInicio1 = (GregorianCalendar) processEntity.getValue("dataIniAfa");;
	GregorianCalendar dataFim1 = (GregorianCalendar) processEntity.getValue("dataFimAfa");;

	String tarefaAtual = activity.getCode();

	NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");

	EntityWrapper wColaborador = new EntityWrapper(colaborador);

	String numcad = NeoUtils.safeOutputString(wColaborador.getValue("numcad"));

	long tipoDocumento = (long) processEntity.findValue("tipoDocumento.idDocumento");

	long idColaborador = (long) processEntity.findValue("colaborador.neoId");

	if (tipoDocumento == 1) {
	    boolean atestadoIgualAfa = (boolean) processEntity.findValue("isDataAtestadoIgualAfa");

	    if (!atestadoIgualAfa) {

		dataInicio1 = (GregorianCalendar) processEntity.findValue("datIniAtestado");
		dataFim1 = (GregorianCalendar) processEntity.findValue("dataFimAtestado");
		
	    }
	} else if (tipoDocumento == 2) {
	    long punicao = (long) processEntity.findValue("listaPunicoes.id");

	    if (punicao == 2) {
		boolean punicaoIgualAfa = (boolean) processEntity.findValue("isMesmaDataAfa");

		if (!punicaoIgualAfa) {
		    dataInicio1 = (GregorianCalendar) processEntity.findValue("perIniSuspensao");
		    dataFim1 = (GregorianCalendar) processEntity.findValue("perFimSuspensao");
		
		}

	    }
	}
	
	    System.out.println("JACancelaTarefasEmDuplicidade SQL: ANTES DO SELECT - " + numcad);	

	    String sql = " SELECT P.code,M.name FROM D_jaJustificativaAfastamento AF WITH(NOLOCK) " 
		    + " INNER JOIN WFProcess P WITH(NOLOCK)ON P.neoId = AF.wfprocess_neoId "
		    + " INNER JOIN ProcessModel M WITH(NOLOCK) ON M.neoId = P.model_neoId " 
		    + " WHERE ((AF.dataIniAfa BETWEEN ? AND ? ) OR (AF.dataFimAfa BETWEEN ? AND ? )) "
		    + " AND AF.colaborador_neoId= ? "
		    + " AND P.code != ? " 
		    + " AND P.processState=0 ";

	    System.out.println("JACancelaTarefasEmDuplicidade SQL: " + numcad + " - " + sql);
	    
	    dataFim1 = this.normalizaData(dataFim1);

	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    try {
		conn = PersistEngine.getConnection("");

		pstm = conn.prepareStatement(sql);

		pstm.setDate(1, new Date(dataInicio1.getTimeInMillis()));
		pstm.setDate(2, new Date(dataFim1.getTimeInMillis()));
		pstm.setDate(3, new Date(dataInicio1.getTimeInMillis()));
		pstm.setDate(4, new Date(dataFim1.getTimeInMillis()));
		pstm.setLong(5, idColaborador);
		pstm.setLong(6, Long.valueOf(tarefaAtual));

		rs = pstm.executeQuery();
		
		System.out.println("JACancelaTarefasEmDuplicidade SQL: " + numcad + " - FINALIZOU");

		while (rs.next()) {
		    String code = rs.getString("code");
		    String model = rs.getString("name");
		    String motivo = "Cancelamento devido a falta estar sendo tratado na tarefa " + activity.getCode() + ".";
		    this.cancelaProcessos(code, model, motivo);
		}

	    } catch (Exception e) {
		e.printStackTrace();
		throw new WorkflowException("Erro ao cancelar processos do periodo do afastamento!");
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }

	

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

    }

    @SuppressWarnings("unchecked")
    public String cancelaProcessos(String codes, String model, String motivo) {

	String strCodes = "'000000'";
	Long code = Long.parseLong(codes);
	if (code < 10) {
	    codes = "00000" + codes;
	} else if (code < 100) {
	    codes = "0000" + codes;
	} else if (code < 1000) {
	    codes = "000" + codes;
	} else if (code < 10000) {
	    codes = "00" + codes;
	} else if (code < 100000) {
	    codes = "0" + codes;
	}
	strCodes = codes;

	String query = "";

	query = " select neoId from wfprocess   where model_neoid in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.name = '" + model
		+ "' ) ) and code in  " + "(" + strCodes + ")";

	EntityManager entity = PersistEngine.getEntityManager();
	Integer cont = 1;
	if (entity != null) {
	    Query q = entity.createNativeQuery(query);
	    List<BigInteger> result = (List<BigInteger>) q.getResultList();

	    for (BigInteger i : result) {
		Long id = i.longValue();

		WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));

		if (proc != null) {
		    RuntimeEngine.getProcessService().cancel(proc, motivo);
		    System.out.println("[Cancelamento da tarefa referente ao fluxo de afastamento] - " + cont + " : Processo " + proc.getCode());
		} else {
		    System.out.println("[Cancelamento da tarefa referente ao fluxo de afastamento] - " + cont + " : ERRO: Processo não localizado!");
		}
		cont++;
	    }
	    return "Cancelamento Processado com Sucesso!";
	} else {
	    return "Nenhuma tarefa encontrada para cancelamento";
	}
    }

    private GregorianCalendar normalizaData(GregorianCalendar data) {

	data.set(Calendar.HOUR_OF_DAY, 23);
	data.set(Calendar.MINUTE, 59);
	data.set(Calendar.SECOND, 59);

	return data;
    }

}
