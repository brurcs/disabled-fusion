package com.neomind.fusion.custom.orsegups.fap.dto;

import java.util.List;

import com.neomind.fusion.common.NeoObject;

public class AplicacaoTecnicoTaticoDTO {
    NeoObject fornecedor = null;
    Long codTecnico = null;
    List<NeoObject> listaCcu = null;
    List<HistoricoFapDTO> historicoFAP = null;
    List<NeoObject> historicoContasSigma = null;
    
}
