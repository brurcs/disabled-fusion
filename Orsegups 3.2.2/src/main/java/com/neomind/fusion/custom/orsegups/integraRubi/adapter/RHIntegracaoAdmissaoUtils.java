package com.neomind.fusion.custom.orsegups.integraRubi.adapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.com.senior.services.sm.asoExterno.AsoexternoASOExterno2In;
import br.com.senior.services.sm.asoExterno.AsoexternoASOExternoIn;
import br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados3In;
import br.com.senior.services.vetorh.dependentes.DependentesDependente2In;
import br.com.senior.services.vetorh.dependentes.DependentesNascimentoIn;
import br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica5In;
import br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar7In;
import br.com.senior.services.vetorh.historico.HistoricosAdicional2In;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.adapter.integraRubi.ParametrizacaoAdmissao;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RHIntegracaoAdmissaoUtils
{
	private static Log log = LogFactory.getLog(RHIntegracaoAdmissaoUtils.class);

	public String atualizaDadosTabela(EntityWrapper wrapper, Long nrEmpresa, Long matricula, String codUsuario)
	{
		if (matricula == null)
			return "Número do cadastro não encontrado";

		wrapper.setValue("matricula", matricula);
		Long tipsal = wrapper.findGenericValue("tpSalario.codigo"); //Tipo de Salário
		Long tipCol = wrapper.findGenericValue("tpColaborador.codigo");

		if (tipsal == 5)
		{
			Boolean validaEventoHorista = RHIntegracaoBanco.insereEventoHorista(matricula, nrEmpresa);

			if (NeoUtils.safeBoolean(validaEventoHorista))
				log.info("EVENTOS DE HORISTA LANÇADOS COM SUCESSO - " + matricula + " - " + nrEmpresa);
			else
				return " ERRO NA INSERÇÃO DOS EVENTOS DE HORISTA!";
		}

		Long tipcon = wrapper.findGenericValue("tpContrato.codigo"); //Tipo de Contrato

		if (tipcon == 5l)
		{
			Boolean insereDadosEstagio = RHIntegracaoBanco.insereDadosEstagio(wrapper, matricula, nrEmpresa);
			if (!insereDadosEstagio)
				return " ERRO NA INSERÇÃO DOS DADOS DO ESTAGIÁRIO";
		}

		Long apupon = wrapper.findGenericValue("tpApuracao.codigo"); //Tipo de Apuração

		if (apupon != 1l || tipsal == 5l)
		{
			Boolean insereHistoricoDispensaPonto = RHIntegracaoBanco.insereHistoricoDispensaPonto(wrapper, matricula, nrEmpresa);
			if (!insereHistoricoDispensaPonto)
				return " ERRO NA INSERÇÃO DO HISTÓRICO DE DISPENSA DE PONTO";
		}

		Integer fichaMedica = RHIntegracaoBanco.getFichaMedica(matricula, nrEmpresa);
		if (fichaMedica != null && fichaMedica > 0)
			wrapper.findField("documentacao.fichamedica").setValue(fichaMedica.toString());

		PersistEngine.getEntityManager().flush();

		log.warn("FICHA BÁSICA GERADA COM SUCESSO!");
		log.warn("FICHA MÉDICA GERADA: " + fichaMedica);

		log.info("matricula resgatado: " + matricula);

		String dtAdmissaoS = new SimpleDateFormat("yyyy-MM-dd").format(((GregorianCalendar) wrapper.findGenericValue("dtAdmissao")).getTime());

		Boolean validaHistoricoCracha = RHIntegracaoBanco.insereHistoricoCracha(matricula, nrEmpresa, dtAdmissaoS);

		if (validaHistoricoCracha != null && !validaHistoricoCracha)
			return "ERRO NA INSERÇÃO DO HISTÓRICO DO CRACHÁ";
		else
		{
			log.info(matricula + " - " + nrEmpresa + "HISTÓRICO DO CRACHÁ INSERIDO COM SUCESSO!");
			wrapper.findField("validaHistoricoCracha").setValue(Boolean.TRUE);
		}

		Boolean validaPersonalizados = insereDadosPersonalizados(wrapper, matricula);

		if (validaPersonalizados != null && !validaPersonalizados)
			return "ERRO NA INSERÇÃO DOS CAMPOS PERSONALIZADOS";
		else
			log.warn("CAMPOS PERSONALIZADOS INSERIDOS COM SUCESSO!");

		String errroDadosVigilante = atualizaDadosVigilante(wrapper, nrEmpresa, matricula);

		if (ocorreuErro(errroDadosVigilante))
			return errroDadosVigilante;

		String erroFoto = atualizaFotoColaborador(wrapper, nrEmpresa, matricula);

		if (ocorreuErro(erroFoto))
			return erroFoto;

		Boolean validaSolicitante = RHIntegracaoBanco.resgataSolicitante(codUsuario, matricula, nrEmpresa, tipCol);

		if (validaSolicitante != null && !validaSolicitante)
			return " - ERRO NA INSERÇÃO DO USUÁRIO SOLICITANTE";

		if (NeoUtils.safeBoolean(wrapper.findGenericValue("existeSelecao")))
		{
			Long tipcol = wrapper.findGenericValue("tpColaborador.codigo");
			Boolean insereSelecao = RHIntegracaoBanco.insereSelecao(matricula, nrEmpresa, tipcol);

			if (insereSelecao != null && !insereSelecao)
				return "ERRO NA INSERÇÃO DA SELEÇÃO";

			wrapper.setValue("validaFoto", insereSelecao);
		}

		log.info("ANTES ENTRAR NA LISTA VT");
		if (!atualizaValeTransporte(wrapper, matricula, nrEmpresa))
			return "ERRO NA INSERÇÃO DOS CARTÕES DE VT DO COLABORADOR DE MATRICULA: " + matricula;

		return null;
	}

	private String atualizaDadosVigilante(EntityWrapper wrapper, Long nrEmpresa, Long matricula)
	{
		Boolean validaDadosVigilantes = null;

		log.info("vigilante: " + wrapper.findGenericValue("dadosVigilancia.aitVigilante"));

		Boolean aitVigilante = wrapper.findGenericValue("dadosVigilancia.aitVigilante");

		String cargoDescricao = wrapper.findGenericValue("cargos.titcar");
		String tipcar = wrapper.findGenericValue("cargos.usu_tipcar");
		if ((tipcar != null && tipcar.equals("VIG") && cargoDescricao.contains("VIGILANTE")) || (tipcar != null && tipcar.equals("VIG") && aitVigilante))
		{
			GregorianCalendar gDtformacao = wrapper.findGenericValue("dadosVigilancia.dtFormacCNV");
			SimpleDateFormat SDFVig = new SimpleDateFormat("yyyy-MM-dd");
			String dtformacao = null;

			if (gDtformacao != null)
				dtformacao = SDFVig.format(gDtformacao.getTime());
			else
				dtformacao = "1900-12-31";

			GregorianCalendar gDtreciclagem = wrapper.findGenericValue("dadosVigilancia.dtReciclagemCNV");
			Boolean cadastroCNV = wrapper.findGenericValue("dadosVigilancia.cadastroCNV");
			String dtreciclagem = null;

			if (gDtreciclagem != null)
				dtreciclagem = SDFVig.format(gDtreciclagem.getTime());
			else
				dtreciclagem = "1900-12-31";

			String orgaovig = wrapper.findGenericValue("dadosVigilancia.orgaoExpCNV");
			String numdrt = wrapper.findGenericValue("dadosVigilancia.numDRT");
			GregorianCalendar gDtvalidadecnv = wrapper.findGenericValue("dadosVigilancia.dtvalidadeCNV");

			log.info("Data Validade CNV: " + gDtvalidadecnv + " - " + nrEmpresa + " - " + matricula);

			String dtvalidadecnv = null;
			if (gDtvalidadecnv != null)
				dtvalidadecnv = SDFVig.format(gDtvalidadecnv.getTime());
			else
				dtvalidadecnv = "1900-12-31";

			String numcnv = wrapper.findGenericValue("dadosVigilancia.numCNV");
			Boolean extraval = wrapper.findGenericValue("dadosVigilancia.extTranspVal");
			Boolean extsegpes = wrapper.findGenericValue("dadosVigilancia.extSeguPesso");
			Long codoem = wrapper.findGenericValue("dadosVigilancia.escolaVig.codoem");

			validaDadosVigilantes = RHIntegracaoBanco.insereDadosVigilante(matricula, nrEmpresa, dtformacao, dtreciclagem, orgaovig, numdrt, dtvalidadecnv, numcnv, extraval, extsegpes, cadastroCNV, codoem);

			if (!validaDadosVigilantes)
				return " - ERRO NA INSERÇÃO DOS DADOS DO VIGILANTE";
			else
				log.info("DADOS DO VIGILANTE INSERIDOS COM SUCESSO - " + matricula);
		}

		return null;
	}

	private Boolean insereDadosPersonalizados(EntityWrapper wrapper, Long matricula)
	{
		String colabsede = wrapper.findGenericValue("colabSede.codigo");
		String nomcra = wrapper.findGenericValue("nomeCracha"); //Nome do crachá
		Long fisres = wrapper.findGenericValue("fiscRespon.usu_fisres");
		String valcom = wrapper.findGenericValue("auxVC.codigo");
		Long numEmp = wrapper.findGenericValue("emp.numemp");
		Long tipCol = wrapper.findGenericValue("tpColaborador.codigo");

		Long tipadmcolab = wrapper.findGenericValue("tpAdmColab.codigo");
		Long depemp = null;

		if (tipadmcolab == 2)
			depemp = 1l;
		else
			depemp = wrapper.findGenericValue("departamento.codigo");

		Boolean validaPersonalizados = RHIntegracaoBanco.inserePersonalizados(matricula, tipadmcolab, colabsede.toString(), "N", ((valcom == null) ? "" : valcom.toString()), depemp, fisres, nomcra, numEmp, tipCol);

		return validaPersonalizados;
	}

	public static String atualizaHistoricoCracha(EntityWrapper wrapper, Long nrEmpresa, Long matricula)
	{
		Boolean existeHistoricoCracha = wrapper.findGenericValue("validaHistoricoCracha");
		GregorianCalendar datadmCalendar = wrapper.findGenericValue("dtAdmissao");

		if (!existeHistoricoCracha)
		{
			SimpleDateFormat SDFHis = new SimpleDateFormat("yyyy-MM-dd");
			String dtAdmissaoS = SDFHis.format(datadmCalendar.getTime());
			Boolean validaHistoricoCracha = RHIntegracaoBanco.insereHistoricoCracha(matricula, nrEmpresa, dtAdmissaoS);

			if (validaHistoricoCracha != null && !validaHistoricoCracha)
			{
				log.info(matricula + " - " + nrEmpresa + "ERRO NO HISTÓRICO DO CRACHÁ!");
				return "ERRO NA INSERÇÃO DO HISTÓRICO DO CRACHÁ";
			}
			else
			{
				log.info(matricula + " - " + nrEmpresa + "HISTÓRICO DO CRACHÁ INSERIDO COM SUCESSO!");
				wrapper.setValue("validaHistoricoCracha", Boolean.TRUE);
			}
		}

		return null;
	}

	public static String atualizaFotoColaborador(EntityWrapper wrapper, Long nrEmpresa, Long matricula)
	{
		Boolean validaFoto = wrapper.findGenericValue("validaFoto");

		if (!validaFoto)
		{
			NeoFile fotoColabNF = wrapper.findGenericValue("fotocracha");
			if (fotoColabNF != null)
			{
				String caminhoPadrao = getCaminhoPadrao();
				log.info("fotoColabNF: " + fotoColabNF);

				GregorianCalendar agora = new GregorianCalendar();
				SimpleDateFormat sdfatual = new SimpleDateFormat("yyyyMMddHHmmss");
				String caminho = sdfatual.format(agora.getTime());

				log.info("caminho: " + caminho);

				try
				{
					File diretorio = new File(caminhoPadrao + caminho);
					log.info("diretorio: " + diretorio);
					diretorio.mkdir();
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}

				File tempDir = new File(caminhoPadrao + caminho);
				File tempFile = new File(tempDir + "\\" + fotoColabNF.getName());

				log.info("tempDir: " + tempDir);
				log.info("tempFile: " + tempFile);

				try
				{
					NeoStorage.copy(fotoColabNF.getInputStream(), new FileOutputStream(tempFile));
					log.info("fotoColabNF: " + fotoColabNF);
				}
				catch (FileNotFoundException e)
				{
					e.printStackTrace();
				}

				//CRIAR INSERT NO BANCO DO IMAGEM_HEX

				Boolean validaInsertFotoCracha = RHIntegracaoBanco.insereFotoCracha(matricula, nrEmpresa, tempFile.getAbsolutePath());

				log.info("validaInsertFotoCracha: " + validaInsertFotoCracha);

				tempFile.delete();
				tempDir.delete();

				//if (validaInsertFotoCracha != null && !validaInsertFotoCracha)
				//return " - ERRO NA INSERÇÃO DA FOTO DO USUÁRIO";

			}
		}

		return null;
	}

	public boolean atualizaValeTransporte(EntityWrapper wrapper, Long matricula, Long nrEmpresa)
	{
		List<NeoObject> listaVT = wrapper.findGenericValue("listaVT");
		if (listaVT.size() > 0)
		{
			log.info("ENTROU NA LISTA VT");

			for (NeoObject itemVT : listaVT)
			{
				EntityWrapper wItemVT = new EntityWrapper(itemVT);

				Long empresaVT = wItemVT.findGenericValue("empresaVT.codigo");
				Long numeroVT = wItemVT.findGenericValue("numVT");
				Long tipCol = wrapper.findGenericValue("tpColaborador.codigo");

				Long empVT = null;

				if (empresaVT != null)
					empVT = empresaVT;

				log.info("LISTA VT - NUMEROVT - " + numeroVT);
				log.info("LISTA VT - EMPRESAVT - " + empresaVT);

				Double numVT = null;

				if (numeroVT != null)
					numVT = numeroVT.doubleValue();

				if (empresaVT != null && numeroVT != null)
				{
					Boolean validaInsertVT = RHIntegracaoBanco.insereListaVT(matricula, nrEmpresa, tipCol, empVT, numVT);

					log.info("VALIDAINSERTVT - " + validaInsertVT);

					if (!validaInsertVT)
						return false;
				}
			}
		}

		return true;
	}

	public static String getCaminhoPadrao()
	{
		String caminhoPadrao = null;
		//$%
		List<NeoObject> listaCaminhoPadrao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhDiretorioDocs"), new QLRawFilter("tipoDoc like 'padrao'"));

		if (listaCaminhoPadrao.size() > 0)
		{
			for (NeoObject dir : listaCaminhoPadrao)
			{
				EntityWrapper wDir = new EntityWrapper(dir);

				caminhoPadrao = wDir.findGenericValue("diretorio");
			}

		}

		return caminhoPadrao;
	}

	public static HistoricosAdicional2In getObjetoIntegracaoAdcional(EntityWrapper wrapper, Long matricula)
	{
		HistoricosAdicional2In integrarAdiIn = new HistoricosAdicional2In();
		integrarAdiIn.setNumCad(matricula.intValue());
		log.info("Integração Adicional - Matricula: " + matricula);

		BigDecimal perinss = (BigDecimal) (NeoUtils.safeBoolean(wrapper.findGenericValue("exisAdicional")) ? wrapper.findGenericValue("perInsalubridade") : null);
		Long numemp = wrapper.findGenericValue("emp.numemp"); //Número Empresa
		Long tipcol = wrapper.findGenericValue("tpColaborador.codigo"); //Tipo de Colaborador

		BigDecimal perper = (BigDecimal) (NeoUtils.safeBoolean(wrapper.findGenericValue("exisAdicional")) ? wrapper.findGenericValue("perPericulosidade") : null);

		if (NeoUtils.safeBoolean(wrapper.findGenericValue("existeSelecao")))
			perinss = wrapper.findGenericValue("perInsalubridadeSele");

		integrarAdiIn.setNumEmp(numemp.intValue());
		log.info("Integração Adicional - numEmp: " + numemp);
		integrarAdiIn.setTipCol(tipcol.intValue());
		log.info("Integração Adicional - tipcol: " + tipcol);
		integrarAdiIn.setTipOpe("I");
		String dtadmissao = NeoUtils.safeDateFormat((GregorianCalendar) wrapper.findGenericValue("dtAdmissao"), "dd/MM/yyyy");

		integrarAdiIn.setDatAlt(dtadmissao);
		log.info("Integração Adicional - datAlt: " + dtadmissao);
		log.info("Integração Adicional - perper: " + (perper != null ? perper.doubleValue() : "null"));
		log.info("Integração Adicional - perinss: " + (perinss != null ? perinss.doubleValue() : "null"));

		if (perper != null)
			integrarAdiIn.setPerPer(perper.doubleValue());
		else
			integrarAdiIn.setPerPer(null);

		if (perinss != null)
			integrarAdiIn.setPerIns(perinss.doubleValue());
		else
			integrarAdiIn.setPerIns(null);

		return integrarAdiIn;
	}

	public static void adicionaRegistroIntegracao(final String registro)
	{
		NeoRunnable newEntityManager = new NeoRunnable()
		{

			@Override
			public void run() throws Exception
			{
				if (registro == null)
				{
					log.warn("REGISTRO DE INTEGRAÇÃO VEIO VAZIO");
					log.info("REGISTRO DE INTEGRAÇÃO VEIO VAZIO");
				}
				else
				{
					NeoObject oRegistrosIntegracao = AdapterUtils.createNewEntityInstance("rhRegistroIntegracao");

					EntityWrapper wRegistrosIntegracao = new EntityWrapper(oRegistrosIntegracao);
					wRegistrosIntegracao.setValue("registro", registro);
					log.info("REGISTRO DE INTEGRAÇÃO CORRETO: " + registro);
					PersistEngine.persist(oRegistrosIntegracao);
				}
			}
		};

		try
		{
			PersistEngine.managedRun(newEntityManager);
		}
		catch (Exception e)
		{
			log.error("ERRO ", e);
		}

		log.info("pasta criada com sucesso");

	}

	public static DependentesNascimentoIn getObjetoIntegracaoDependentesNasc(Long nrEmpresa, Long tipcol, Long matricula, Integer coddep, EntityWrapper wDependente)
	{
		String datnasDep = null;

		Long grapar = wDependente.findGenericValue("graParentesco.codigo");
		if (grapar == 3)
			datnasDep = wDependente.findGenericValue("dtNasciParentPaiMae");
		else
		{
			GregorianCalendar GCdatnas = wDependente.findGenericValue("dtNascDepend");
			SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

			datnasDep = SDF.format(GCdatnas.getTime());
		}

		DependentesNascimentoIn integrarDepNasIn = new DependentesNascimentoIn();

		integrarDepNasIn.setNumEmp(nrEmpresa.intValue());
		integrarDepNasIn.setTipCol(tipcol.intValue());
		integrarDepNasIn.setNumCad(matricula.intValue());
		integrarDepNasIn.setCodDep(coddep.intValue());
		if (grapar == 1)
		{
			GregorianCalendar GCentcer = wDependente.findGenericValue("dtEntregaCertidao");
			String entcer = new SimpleDateFormat("dd/MM/YYY hh:mm:ss").format(GCentcer.getTime());
			integrarDepNasIn.setEntCer(entcer);
		}
		else
			integrarDepNasIn.setEntCer(null);

		integrarDepNasIn.setNumEmp(nrEmpresa.intValue());
		integrarDepNasIn.setTipCol(tipcol.intValue());
		integrarDepNasIn.setNumCad(matricula.intValue());
		integrarDepNasIn.setCodDep(coddep.intValue());

		return integrarDepNasIn;
	}

	public static DependentesDependente2In getObjetoIntegracaoDependentes(Long nrEmpresa, Long tipcol, Long matricula, Integer coddep, EntityWrapper wDependente)
	{
		String nomdep = wDependente.findGenericValue("nomDependente");
		String datnasDep = null;
		Long grapar = wDependente.findGenericValue("graParentesco.codigo");
		if (grapar == 3)
			datnasDep = wDependente.findGenericValue("dtNasciParentPaiMae");
		else
		{
			GregorianCalendar GCdatnas = wDependente.findGenericValue("dtNascDepend");
			SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

			datnasDep = SDF.format(GCdatnas.getTime());
		}

		String tipsexDep = wDependente.findGenericValue("sexoDependente.sigla");
		String nomcomDep = wDependente.findGenericValue("nomCompletoDepend");
		Long tipdep = wDependente.findGenericValue("tpDependESoci.codigo"); //não mapeado na webservice
		Long estcivDep = wDependente.findGenericValue("estCivilDependente.codigo");
		String numcpfDep = wDependente.findGenericValue("cpfDependente");

		DependentesDependente2In integrarDepIn = new DependentesDependente2In();

		if (grapar == 1)
			integrarDepIn.setAviImp("N");
		else
			integrarDepIn.setAviImp(null);

		integrarDepIn.setTipOpe("I");
		integrarDepIn.setNumEmp(nrEmpresa.intValue());
		integrarDepIn.setTipCol(tipcol.intValue());
		integrarDepIn.setNumCad(matricula.intValue());
		integrarDepIn.setCodDep(coddep.intValue());
		integrarDepIn.setNomDep(nomdep);
		integrarDepIn.setDatNas(datnasDep);
		integrarDepIn.setGraPar(grapar.intValue());
		integrarDepIn.setTipSex(tipsexDep);
		integrarDepIn.setNomCom(nomcomDep);
		integrarDepIn.setTipDep(tipdep.intValue());
		integrarDepIn.setEstCiv(estcivDep.intValue());
		integrarDepIn.setNumCpf(numcpfDep);
		integrarDepIn.setPenJud("N");

		return integrarDepIn;
	}

	public static String getApFun(String chaveCodigo)
	{
		return "#I" + chaveCodigo;
	}

	public static FichaComplementarFichaComplementar7In integrarFichaComplementar(EntityWrapper wrapper, Long matricula)
	{
		FichaComplementarFichaComplementar7In integrarFComIn = new FichaComplementarFichaComplementar7In();
		String nomcom = wrapper.findGenericValue("nomCompletoFC"); //Nome Completo
		//numemp já tem na FBas
		//tipope já tem na FBas
		//tipcol já tem na FBas
		//matricula gerado no ato da FBas
		Long numemp = wrapper.findGenericValue("emp.numemp"); //Número Empresa
		Long tipcol = wrapper.findGenericValue("tpColaborador.codigo"); //Tipo de Colaborador
		integrarFComIn.setNumCad(matricula.intValue());
		integrarFComIn.setFicReg(matricula.intValue());

		SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
		String tiplgr = null;
		String endrua = null;
		Long endnum = null;
		String endcpl = null;
		Long codpai = 1l;
		String codest = null;
		Long codcid = null;
		Long codbai = null;
		Long endcep = wrapper.findGenericValue("cep");
		Boolean endManual = wrapper.findGenericValue("informaendereco");

		if (!endManual)
		{//AUTOMÁTICO
			tiplgr = wrapper.findGenericValue("tpLogradouro.codigo");
			endrua = wrapper.findGenericValue("ruacolab.nomrua");
			endnum = wrapper.findGenericValue("endnum");
			endcpl = wrapper.findGenericValue("complEnd");
			codest = wrapper.findGenericValue("estado.codest");
			codcid = wrapper.findGenericValue("cidcolab.codcid");
			codbai = wrapper.findGenericValue("bairro.codbai");
		}
		else
		{//MANUAL
			tiplgr = wrapper.findGenericValue("logracolab.codigo");
			endrua = wrapper.findGenericValue("ruacolabo");
			endnum = wrapper.findGenericValue("endnum");
			endcpl = wrapper.findGenericValue("complEnd");
			codest = wrapper.findGenericValue("estadocolab.codest");
			codcid = wrapper.findGenericValue("cidade.codcid");
			codbai = wrapper.findGenericValue("bairrocolab.codbai");
		}

		Long dditel = 55l;
		Long numddi2 = 55l;
		String numtel = wrapper.findGenericValue("telefone");
		Integer dddtel = Integer.parseInt(numtel.substring(1, 3));
		String primDig01 = numtel.substring(5, 6);
		String numtel2 = null;
		Integer numddd2 = null;
		String primDig02 = null;

		if (primDig01.equals("9") && numtel != null)
			numtel = numtel.substring(4, 15).replaceAll("-", "").trim();
		else if (!primDig01.equals("9") && numtel != null)
		{
			numtel = numtel.substring(4, 14).replaceAll("-", "").trim();
			numtel2 = wrapper.findGenericValue("telefone2");

			if (numtel2 != null && !numtel2.equals(""))
			{
				numddd2 = Integer.parseInt(numtel2.substring(1, 3));

				primDig02 = numtel2.substring(5, 6);

				if (primDig02.equals("9") && numtel2 != null)
					numtel2 = numtel2.substring(4, 15).replaceAll("-", "").trim();
				else if (!primDig02.equals("9") && numtel2 != null)
					numtel2 = numtel2.substring(4, 14).replaceAll("-", "").trim();
			}
		}

		Long painas = 1l;
		String estnas = wrapper.findGenericValue("estNasc.codest");
		Long ccinas = wrapper.findGenericValue("cidNasc.codcid");
		String numele = "" + wrapper.findGenericValue("numEleitorS");
		String zonele = wrapper.findGenericValue("zonaEleitorS");
		String secele = wrapper.findGenericValue("sessaoEleitorS");

		//VALIDAÇÃO CNH
		String exigeCNH = wrapper.findGenericValue("exigCNH.codigo");
		String numcnh = null;
		String orgcnh = null;
		String estcnh = null;
		String catcnh = null;

		GregorianCalendar GCdatcnh = null;
		String datcnh = null;
		GregorianCalendar GCvencnh = null;
		String vencnh = null;

		if (exigeCNH != null && exigeCNH.equals("S"))
		{
			Long numcnhL = wrapper.findGenericValue("numHabilitacao");
			numcnh = String.valueOf(numcnhL);
			orgcnh = wrapper.findGenericValue("orgHabilitacao.codigo");
			estcnh = wrapper.findGenericValue("estCNH.codest");
			catcnh = wrapper.findGenericValue("catHabilitacao.codigo");
			GCdatcnh = wrapper.findGenericValue("dtExpedicao");
			if (GCdatcnh != null)
				datcnh = SDF.format(GCdatcnh.getTime());
			GCvencnh = wrapper.findGenericValue("dtValidHabilit");
			if (GCvencnh != null)
				vencnh = SDF.format(GCvencnh.getTime());
		}

		String numres = wrapper.findGenericValue("numres");
		String catres = wrapper.findGenericValue("catres");
		Long durcon = wrapper.findGenericValue("duracaoContrato");
		Long procon = wrapper.findGenericValue("prorrogContrato");
		GregorianCalendar GCultexm = wrapper.findGenericValue("ultExameMedic");
		String ultexm = SDF.format(GCultexm.getTime());

		//GregorianCalendar gDexCid = wrapper.findGenericValue("dtExp");

		//String dexCid = SDF.format(gDexCid.getTime());

		//String estCid = wrapper.findGenericValue("estExp.codest");

		//String emiCid = wrapper.findGenericValue("orgExp.descricao");

		String numCid = wrapper.findGenericValue("numIdentRG");

		//INÍCIO In
		integrarFComIn.setNumEmp(numemp.intValue());
		integrarFComIn.setTipOpe("I");
		integrarFComIn.setTipCol(tipcol.intValue());
		integrarFComIn.setEndCep(endcep.intValue());
		integrarFComIn.setTipLgr(tiplgr);
		integrarFComIn.setEndRua(endrua);
		integrarFComIn.setEndNum(endnum.intValue());
		integrarFComIn.setEndRua(endrua);
		integrarFComIn.setEndCpl(endcpl);
		integrarFComIn.setCodPai(codpai.intValue());
		integrarFComIn.setCodEst(codest);
		integrarFComIn.setCodCid(codcid.intValue());
		integrarFComIn.setCodBai(codbai.intValue());
		integrarFComIn.setDdiTel(dditel.intValue());
		integrarFComIn.setDddTel(dddtel);
		integrarFComIn.setNumTel(numtel);

		//RG
		integrarFComIn.setNumCid(numCid);
		//integrarFComIn.setEmiCid(emiCid);
		//integrarFComIn.setEstCid(estCid);
		//integrarFComIn.setDexCid(dexCid);

		integrarFComIn.setNumDdi2(numddi2.intValue());
		integrarFComIn.setNumDdd2(numddd2);
		integrarFComIn.setNumTel2(numtel2);
		integrarFComIn.setPaiNas(painas.intValue());
		integrarFComIn.setEstNas(estnas);
		integrarFComIn.setCciNas(ccinas.intValue());
		integrarFComIn.setNumEle(numele);
		integrarFComIn.setZonEle(zonele);
		integrarFComIn.setSecEle(secele);
		integrarFComIn.setNumCnh(numcnh);
		integrarFComIn.setOrgCnh(orgcnh);
		integrarFComIn.setEstCnh(estcnh);
		integrarFComIn.setCatCnh(catcnh);
		integrarFComIn.setDatCnh(datcnh);
		integrarFComIn.setVenCnh(vencnh);
		integrarFComIn.setNumRes(numres);
		integrarFComIn.setCatRes(catres);
		integrarFComIn.setDurCon(durcon.intValue());
		integrarFComIn.setProCon(procon.intValue());
		integrarFComIn.setUltExm(ultexm);
		integrarFComIn.setNomCom(nomcom);
		//integrarFComIn.setNifExt("1");
		integrarFComIn.setClaAss("N");

		return integrarFComIn;
	}

	public static FichaBasicaFichaBasica5In defineFichaBasica(String chaveCodigo, EntityWrapper wrapper)
	{
		Long tipadmcolab = wrapper.findGenericValue("tpAdmColab.codigo");
		Long depemp = null;

		if (tipadmcolab == 2)
			depemp = 1l;
		else
			depemp = wrapper.findGenericValue("departamento.codigo");

		String valcom = wrapper.findGenericValue("auxVC.codigo");
		String numcpf = wrapper.findGenericValue("numcpf"); //Número do CPF
		Long estciv = wrapper.findGenericValue("estadoCivil.codigo"); //Estado Civil

		String colabsede = wrapper.findGenericValue("colabSede.codigo");
		String horinc = "534";
		String apefun = "#I" + chaveCodigo;

		Long numemp = wrapper.findGenericValue("emp.numemp"); //Número Empresa
		Long tipcol = wrapper.findGenericValue("tpColaborador.codigo"); //Tipo de Colaborador

		SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

		//Início Customizados
		tipadmcolab = wrapper.findGenericValue("tpAdmColab.codigo");

		if (tipadmcolab == 2)
			depemp = 1l;
		else
			depemp = wrapper.findGenericValue("departamento.codigo");

		colabsede = wrapper.findGenericValue("colabSede.codigo");

		valcom = wrapper.findGenericValue("auxVC.codigo");
		//Fim Customizados

		numemp = wrapper.findGenericValue("emp.numemp"); //Número Empresa

		String nomfun = wrapper.findGenericValue("colaborador");

		GregorianCalendar datadmCalendar = wrapper.findGenericValue("dtAdmissao");

		String dtFimEvt = "31/12/1900";
		String dtadmissao = SDF.format(datadmCalendar.getTime()); //Data de Admissão

		Long tipadm = wrapper.findGenericValue("tpAdmissao.codigo"); //Tipo de Admissão
		Long apupon = wrapper.findGenericValue("tpApuracao.codigo"); //Tipo de Apuração
		Long codvinhvi = wrapper.findGenericValue("tpVinculo.codvin"); //Tipo de Vínculo

		//Regra do sindicato para colaboradores administrativos ou operacionais
		Long codsin = null;
		Long codfil = null;
		String numloc = null;

		if (tipadmcolab == 1)
		{
			if (depemp == 2)
			{
				codsin = wrapper.findGenericValue("sindiAdmSede.sindicato.codsin"); //Código do Sindicato
				codfil = wrapper.findGenericValue("codfil.codfil"); //Código da Filial
				numloc = wrapper.findGenericValue("localadm.codloc");
			}
			else if (depemp > 0 && depemp < 31)
			{
				codsin = wrapper.findGenericValue("sindicatoAdm.sindicato.codsin"); //Código do Sindicato
				codfil = wrapper.findGenericValue("codfil.codfil"); //Código da Filial
				numloc = wrapper.findGenericValue("localadm.codloc");
			}
			else
			{
				codsin = wrapper.findGenericValue("sindicato.sindicato.codsin"); //Código do Sindicato
				codfil = wrapper.findGenericValue("codfil.codfil"); //Código da Filial
				numloc = wrapper.findGenericValue("localadm.codloc");
			}
		}
		else if (tipadmcolab == 2)
		{
			codsin = wrapper.findGenericValue("sindicato.sindicato.codsin"); //Código do Sindicato
			if (codsin == null)
			{
				codsin = wrapper.findGenericValue("sindicatoAdm.sindicato.codsin");
				if (codsin == null)
				{
					codsin = wrapper.findGenericValue("sindicato1.sindicato.codsin");

					if (codsin == null)
						codsin = wrapper.findGenericValue("sindicatoAdm1.sindicato.codsin");
				}
			}
			codfil = wrapper.findGenericValue("codfilOperacional.codfil"); //Código da Filial
			numloc = wrapper.findGenericValue("loca.codloc");
		}

		if (codfil == null && numloc != null)
			numloc.trim();

		Long sitafa = wrapper.findGenericValue("situacao"); //Situação
		String codcar = wrapper.findGenericValue("cargos.codcar"); //Código do Cargo
		Long tipcon = wrapper.findGenericValue("tpContrato.codigo"); //Tipo de Contrato
		Long tipsal = wrapper.findGenericValue("tpSalario.codigo"); //Tipo de Salário
		Boolean editarSalario = wrapper.findGenericValue("editsalario");
		BigDecimal valsal = wrapper.findGenericValue("valorSalario.valor"); //Valor do Salário

		if (valsal == null || editarSalario)
			valsal = wrapper.findGenericValue("valorSalarioPerso"); //Valor do Salário

		Long indadm = wrapper.findGenericValue("indAdmissao"); //Indicador da Admissão
		Long admeso = wrapper.findGenericValue("tpAdmeSocial"); //Tipo de Admissão eSocial
		Long cateso = wrapper.findGenericValue("cateSocial.codigo"); //Categoria do eSocial 

		String codccu = null;

		//busca de centro de custo depende do tipo de admissão do colaborador
		if (tipadmcolab == 1)
			codccu = wrapper.findGenericValue("ccuAdm.codccu");
		else if (tipadmcolab == 2)
			codccu = wrapper.findGenericValue("ccuOpe.codccu");

		Long numctp = wrapper.findGenericValue("numCartTrabalho"); //Número da Carteira de Trabalho
		String digcar = wrapper.findGenericValue("digCartTrabalho"); //Número da Carteira de Trabalho
		GregorianCalendar gcDtCartTrabalho = wrapper.findGenericValue("dtCartTrabalho");
		String dexctp = SDF.format(gcDtCartTrabalho.getTime()); //Data da Carteira de Trabalho
		String pagsin = wrapper.findGenericValue("contribSindical.codigo"); //Contribuição Sindical
		String serctp = wrapper.findGenericValue("serCartTrabalho"); //Série da Carteira de Trabalho
		Long codmothca = wrapper.findGenericValue("motivo.codmot"); //Código do Motivo de Alteração
		String modpag = wrapper.findGenericValue("modPagamento.codigo"); //Modo de Pagamento

		Long codage = null;
		Long codban = null;
		Long conban = null;
		String digconS = null;

		Long tpctba = null;

		if (modpag.equals("R"))
		{
			codage = wrapper.findGenericValue("agenciaConta.codage");
			codban = wrapper.findGenericValue("bancoConta.codban");
			conban = wrapper.findGenericValue("conban");
			digconS = wrapper.findGenericValue("digcontaS");
			tpctba = wrapper.findGenericValue("tpConta.codigo");

		}

		String socsinhsi = wrapper.findGenericValue("sindicalizados.codigo"); //Sindicalizados
		String estctp = wrapper.findGenericValue("ufCarteTrabalho.codest"); //UF da carteira de trabalho

		numcpf = numcpf.replace(".", "");
		numcpf = numcpf.replace("-", "");

		String tipsex = wrapper.findGenericValue("sexo.sigla"); //Sexo
		Long numpis = wrapper.findGenericValue("pis"); //Número do PIS
		Long grains = wrapper.findGenericValue("grauInstrucao.grains"); //Grau de Instrução
		String tipopc = wrapper.findGenericValue("optFGTS.codigo"); //Optante FGTS
		GregorianCalendar datnasCalendar = wrapper.findGenericValue("dtNascimento");
		String datnas = SDF.format(datnasCalendar.getTime()); //Data de Nascimento
		String perpag = wrapper.findGenericValue("periodoPgto.cd"); //Período de Pagamento
		Long codnac = 10l; //Código da Nacionalidade
		String recadi = wrapper.findGenericValue("recebAdtoSal.codigo"); //Recebe Adiantamento do Salário
		String rec13s = wrapper.findGenericValue("receb13Sal.codigo"); //Recebe 13º
		String lisrai = wrapper.findGenericValue("constaRais.codigo"); //Consta na RAIS
		Long codesc = wrapper.findGenericValue("escalaTrabalho.codesc"); //Código da Escala
		String emicar = wrapper.findGenericValue("emitirPonto.codigo"); //Emitir Cartão Ponto
		String deffis = wrapper.findGenericValue("deficiente.codigo"); //Deficiente Físico
		String benrea = wrapper.findGenericValue("benrea.codigo"); //Beneficiente Reabilitado
		Long raccor = wrapper.findGenericValue("racaCor.codigo"); //Raça Cor
		Long catsef = wrapper.findGenericValue("catSefip.codigo"); //Categoria do Sefip
		Long coddef = null;
		Boolean usavt = wrapper.findGenericValue("usaVT");
		Long escvtr = null;
		String deficiente = wrapper.findGenericValue("deficiente.codigo");

		if (deficiente != null && deficiente.equals("S"))
			coddef = wrapper.findGenericValue("deficiencia.codigo");

		Long fisres = wrapper.findGenericValue("fiscRespon.usu_fisres");

		if (valcom != null && valcom.equals("S"))
			escvtr = wrapper.findGenericValue("valauxVT.escvtr"); //Valor para Auxílio Combustível
		else if (usavt)
			escvtr = wrapper.findGenericValue("listaVT.escalaEVT.escvtr"); //Escala do Vale Transporte

		//INÍCIO DO IN			
		FichaBasicaFichaBasica5In integrarFBasIn = new FichaBasicaFichaBasica5In();
		integrarFBasIn.setTipOpe("I");
		integrarFBasIn.setNumEmp(numemp.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - numEmp: " + numemp.intValue());
		integrarFBasIn.setAciTraAfa(null);
		integrarFBasIn.setAdmAnt(null);
		integrarFBasIn.setAdmeSo(admeso.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - admeso: " + admeso.intValue());
		integrarFBasIn.setApeFun(apefun.toString());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - apefun: " + apefun.toString());
		integrarFBasIn.setTipCol(tipcol.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - tipcol: " + tipcol.intValue());
		integrarFBasIn.setApuPon(apupon.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - apupon: " + apupon.intValue());
		integrarFBasIn.setApuPonApu(apupon.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - numEmp: " + numemp.intValue());
		integrarFBasIn.setAssPpr(null);
		integrarFBasIn.setBenRea(benrea);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - benrea: " + benrea);
		integrarFBasIn.setBusHor(null);
		integrarFBasIn.setCadFol(null);
		integrarFBasIn.setCarVagHca(null);
		integrarFBasIn.setCatSef(catsef.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - catsef: " + catsef.intValue());
		integrarFBasIn.setCateSo(cateso.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - cateso: " + cateso.intValue());
		integrarFBasIn.setCauDemAfa(null);
		integrarFBasIn.setClaSalHsa(null);
		integrarFBasIn.setCnpjAn(null);

		if (modpag.equals("R"))
		{
			integrarFBasIn.setCodAge(codage.intValue());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codage: " + codage.intValue());
			integrarFBasIn.setCodBan(codban.intValue());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codban: " + codban.intValue());
			integrarFBasIn.setConBan(conban.doubleValue());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - conban: " + conban.intValue());
			integrarFBasIn.setTpCtBa(tpctba.intValue());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - tpctba: " + tpctba.intValue());
			integrarFBasIn.setDigBan(digconS);
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - digconS: " + digconS);
		}
		else
		{
			integrarFBasIn.setCodAge(null);
			integrarFBasIn.setCodBan(null);
			integrarFBasIn.setTpCtBa(null);
			integrarFBasIn.setConBan(null);
			integrarFBasIn.setDigBan(null);
		}

		integrarFBasIn.setCodAteAfa(null);
		integrarFBasIn.setCodCar(codcar);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codcar: " + codcar);
		integrarFBasIn.setCodCat(null);
		integrarFBasIn.setCodCcu(codccu);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codccu: " + codccu);
		integrarFBasIn.setCodCha(null);

		integrarFBasIn.setCodDoeAfa(null);
		//integrarFBasIn.setCodEqp(null);
		integrarFBasIn.setCodEsc(codesc.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codesc: " + codesc.intValue());
		//integrarFBasIn.setCodEstHsa(1);
		//integrarFBasIn.setCodEtb(null);					
		integrarFBasIn.setCodFicFmd(null);
		integrarFBasIn.setCodFil(codfil.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codfil: " + numemp.intValue());
		integrarFBasIn.setCodFor(null);
		integrarFBasIn.setCodIdn(null);
		integrarFBasIn.setCodMotHca((codmothca != null) ? codmothca.intValue() : null);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codmothca: " + codmothca.intValue());
		integrarFBasIn.setCodMotHsa((codmothca != null) ? codmothca.intValue() : null);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codmothca: " + codmothca.intValue());
		integrarFBasIn.setCodNac(codnac.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codnac: " + codnac.intValue());
		integrarFBasIn.setCodPro(null);

		if (codsin == null)
			new WorkflowException("Código do Sindicato Nulo!");
		else
		{
			integrarFBasIn.setCodSin(codsin.intValue());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codsin: " + codsin.intValue());
			integrarFBasIn.setCodSinHsi(codsin.intValue());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codsin: " + codsin.intValue());
		}

		integrarFBasIn.setCodTap(null);
		if (tipsal == 2l)
		{
			integrarFBasIn.setCodTma(null);
			integrarFBasIn.setCodTmaHes(null);
		} else
		{
			integrarFBasIn.setCodTma(1);
			integrarFBasIn.setCodTmaHes(1);
		}		
		integrarFBasIn.setCodVinHvi(codvinhvi.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - codvinhvi: " + codvinhvi.intValue());
		integrarFBasIn.setConFgt(null);
		integrarFBasIn.setConFinCcu(null);
		//integrarFBasIn.setConRho(conrho);
		integrarFBasIn.setConTosHlo(null);
		integrarFBasIn.setConTovAfa(null);
		integrarFBasIn.setConTovHfi(null);
		integrarFBasIn.setConTovHlo(null);
		integrarFBasIn.setCotDef(deffis);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - deffis: " + deffis);
		integrarFBasIn.setCplEstHsa(null);
		integrarFBasIn.setCplSalHsa(null);
		integrarFBasIn.setDatAdm(dtadmissao);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - dtadmissao: " + dtadmissao);
		integrarFBasIn.setDatAfaAfa(null);
		integrarFBasIn.setDatAltCcu(null);
		integrarFBasIn.setDatAltHca(null);
		integrarFBasIn.setDatAltHcs(null);
		integrarFBasIn.setDatAltHes(null);
		integrarFBasIn.setDatAltHfi(null);
		integrarFBasIn.setDatAltHlo(null);
		integrarFBasIn.setDatAltHor(null);
		integrarFBasIn.setDatApo(null);
		integrarFBasIn.setDatInc(null);
		integrarFBasIn.setDatNas(datnas);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - datnas: " + datnas);
		integrarFBasIn.setDatOpc(null);
		integrarFBasIn.setDatTerAfa(null);
		integrarFBasIn.setDcdPis(null);
		integrarFBasIn.setDepIrf(null);
		integrarFBasIn.setDepSaf(null);
		integrarFBasIn.setDexCtp(dexctp.toString());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - dexctp: " + dexctp.toString());
		integrarFBasIn.setDiaJusAfa(null);

		if (digcar == null)
			integrarFBasIn.setDigCar(null);
		else
		{
			integrarFBasIn.setDigCar(digcar.toString());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - digcar: " + digcar.toString());
		}

		integrarFBasIn.setDocEst(null);
		integrarFBasIn.setEmiCar(emicar);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - emicar: " + emicar);

		if (escvtr != null)
		{
			integrarFBasIn.setEscVtr(escvtr.intValue());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - escvtr: " + escvtr.intValue());
		}
		else
			integrarFBasIn.setEscVtr(null);

		integrarFBasIn.setEstCiv((estciv != null) ? estciv.intValue() : null);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - estciv: " + ((estciv != null) ? estciv.intValue() : null));
		integrarFBasIn.setEstConAfa(null);
		integrarFBasIn.setEstCtp(estctp);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - estctp: " + estctp);
		integrarFBasIn.setExmRetAfa(null);
		integrarFBasIn.setFimEtbHeb(null);
		integrarFBasIn.setFimEvt(dtFimEvt);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - dtFimEvt: " + dtFimEvt);
		integrarFBasIn.setFlowInstanceID(null);
		integrarFBasIn.setFlowName(null);
		integrarFBasIn.setGraIns(grains.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - grains: " + grains.intValue());
		integrarFBasIn.setHorAfaAfa(null);
		integrarFBasIn.setHorBas(null);
		integrarFBasIn.setHorDsrHor(null);
		integrarFBasIn.setHorInc(horinc);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - horinc: " + horinc);
		integrarFBasIn.setHorSabHor(null);
		integrarFBasIn.setHorSemHor(null);
		integrarFBasIn.setHorTerAfa(null);
		integrarFBasIn.setIndAdm((indadm != null) ? indadm.intValue() : null);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - indadm: " + ((indadm != null) ? indadm.intValue() : null));
		integrarFBasIn.setIniAtu(null);
		integrarFBasIn.setIniEtbHeb(null);
		integrarFBasIn.setIniEvt(null);
		integrarFBasIn.setInsCur(null);
		integrarFBasIn.setIrrIse(null);
		integrarFBasIn.setLisRai(lisrai);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - lisrai: " + lisrai);
		integrarFBasIn.setLocTraHlo(null);
		integrarFBasIn.setMatAnt(null);
		integrarFBasIn.setModPag(modpag);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - modpag: " + modpag);
		integrarFBasIn.setMoeEstHsa(null);
		integrarFBasIn.setMotPos(null);
		integrarFBasIn.setMovSef(null);
		integrarFBasIn.setNatDesHna(null);
		integrarFBasIn.setNivSalHsa(null);
		integrarFBasIn.setNomAteAfa(null);
		integrarFBasIn.setNomFun(nomfun.toString());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - nomfun: " + nomfun.toString());
		integrarFBasIn.setNumCad(0);
		integrarFBasIn.setNumCpf(numcpf);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - numcpf: " + numcpf);
		integrarFBasIn.setNumCtp(numctp.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - numctp: " + numctp.intValue());
		integrarFBasIn.setNumLoc(numloc);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - numloc: " + numloc);
		integrarFBasIn.setNumPis(numpis.doubleValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - numpis: " + numpis.doubleValue());
		integrarFBasIn.setObsAfaAfa(null);
		integrarFBasIn.setObsFimHeb(null);
		integrarFBasIn.setObsIniHeb(null);
		integrarFBasIn.setOnuSce(null);
		integrarFBasIn.setOpcCes(null);
		integrarFBasIn.setOrgClaAfa(null);
		integrarFBasIn.setPagSin(pagsin);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - pagsin: " + pagsin);
		integrarFBasIn.setPerJur(null);
		integrarFBasIn.setPerPag(perpag);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - perpag: " + perpag);
		integrarFBasIn.setPonEmb(null);
		integrarFBasIn.setPosObs(null);
		integrarFBasIn.setPosTra(null);
		integrarFBasIn.setPrvTerAfa(null);
		integrarFBasIn.setQhrAfaAfa(null);
		integrarFBasIn.setRacCor(raccor.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - raccor: " + raccor.intValue());
		integrarFBasIn.setRatEve("S");
		integrarFBasIn.setReaRes(null);
		integrarFBasIn.setRec13S(rec13s);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - rec13s: " + rec13s);
		integrarFBasIn.setRecAdi(recadi);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - recadi: " + recadi);
		integrarFBasIn.setRecGra(null);
		integrarFBasIn.setRegConAfa(null);
		integrarFBasIn.setResOnu(null);
		integrarFBasIn.setSalEstHsa(null);
		integrarFBasIn.setSerCtp(serctp);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - serctp: " + serctp);
		integrarFBasIn.setSisCes(null);
		integrarFBasIn.setSitAfa(sitafa.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - sitafa: " + sitafa.intValue());
		integrarFBasIn.setSocSinHsi(socsinhsi);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - socsinhsi: " + socsinhsi);
		integrarFBasIn.setTipAdmHfi(tipadm.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - tipadm: " + tipadm.intValue());
		//integrarFBasIn.setTipAdm(tipadm);
		integrarFBasIn.setTipApo(0);
		integrarFBasIn.setTipCon(tipcon.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - tipcon: " + tipcon.intValue());
		integrarFBasIn.setTipOpc(tipopc);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - tipopc: " + tipopc);
		//integrarFBasIn.setTipSal(tipsal.intValue());
		integrarFBasIn.setTipSalHsa(tipsal.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - tipsal: " + tipsal.intValue());
		integrarFBasIn.setTipSex(tipsex);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - tipsex: " + tipsex);
		integrarFBasIn.setTurInt(null);
		integrarFBasIn.setValSalHsa(valsal.doubleValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - valsal: " + valsal.doubleValue());

		integrarFBasIn.setVerInt(null);

		if (deficiente != null && deficiente.equals("S"))
		{
			integrarFBasIn.setCodDef(coddef.intValue());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - coddef: " + coddef.intValue());
			integrarFBasIn.setDefFis(deffis.toString());
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - deffis: " + deffis.toString());
		}
		else
		{
			integrarFBasIn.setCodDef(null);
			integrarFBasIn.setDefFis((deffis != null) ? deffis.toString() : null);
			log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - deffis: " + ((deffis != null) ? deffis.toString() : null));
		}

		//Início customizados
		integrarFBasIn.setUSU_ColSde(colabsede.toString());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - colabsede: " + colabsede.toString());
		integrarFBasIn.setUSU_CsgBrc("N");//Evandro mencionou que na contratação sempre é "N"
		integrarFBasIn.setUSU_DepEmp(depemp.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - depemp: " + depemp.intValue());
		integrarFBasIn.setUSU_TipAdm(tipadmcolab.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - tipadmcolab: " + tipadmcolab.intValue());
		integrarFBasIn.setUSU_ValCom((valcom != null) ? valcom.toString() : null);
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - valcom: " + ((valcom != null) ? valcom.toString() : null));
		integrarFBasIn.setUSU_FisRes(fisres.intValue());
		log.info("Ficha Básica - Tarefa: " + chaveCodigo + " - fisres: " + fisres.intValue());
		//codusu deverá ser inserido um usuário específico pra classificar como integração
		//fim customizados

		return integrarFBasIn;
	}

	public static String efetivarCadastroComplementar(EntityWrapper wrapper, String chaveCodigo, Long matricula)
	{
		List<NeoObject> registroFCom = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + getChaveFCom(chaveCodigo) + "'"));
		if (registroFCom.size() == 0)
		{
			String erroComp = RHIntegracaoServico.integrarVetorFichaComplementar(wrapper, chaveCodigo, matricula);
			log.info("Matrícula :" + matricula + " - Retorno cadastro complementar: " + erroComp);

			if (ocorreuErro(erroComp) && !erroComp.contains("O campo \"DENifExt1\" não está disponível na tela \"FR034CPL\""))
			{
				wrapper.setValue("errosIntegracao", erroComp);
				wrapper.setValue("contErros", 1l);
				wrapper.setValue("validaIntegracao", false);

				log.warn("Retorno Ficha Complementar - chave: " + chaveCodigo + ": " + erroComp);
			}
			else
			{
				log.info("FICHA COMPLEMENTAR GERADA COM SUCESSO!");

				adicionaRegistroIntegracao(getChaveFCom(chaveCodigo));
			}

			return erroComp;
		}
		else
			log.info("FICHA COMPLEMENTAR JÁ FOI GERADA!");

		return null;
	}

	public static String efetivaCadastroBase(EntityWrapper wrapper, String codUsuario, String chaveCodigo, Long nrEmpresa)
	{
		//INTEGRAÇÃO FBas

		Long matricula = null;

		if (!cadastroJaRegistrado(getChaveFBas(chaveCodigo)))
		{
			String erroCadastro = RHIntegracaoServico.integrarVetorInicio(wrapper, chaveCodigo);

			//Erro na Integração
			if (erroCadastro != null && !erroCadastro.contains("ficha médica") && !erroCadastro.contains("Deseja inserir demais informações do estagiário"))
				return erroCadastro;
			else if (erroCadastro == null || erroCadastro.contains("ficha médica") || erroCadastro.contains("Deseja inserir demais informações do estagiário"))
			{
				adicionaRegistroIntegracao(getChaveFBas(chaveCodigo));

				matricula = RHIntegracaoBanco.resgataNumCad(getApFun(chaveCodigo), nrEmpresa);
			}
			else
				return erroCadastro;
		}
		else
		{
			matricula = wrapper.findGenericValue("matricula");
			String apelidoFuncionario = wrapper.findGenericValue("apefun");

			if (matricula == null)
				matricula = RHIntegracaoBanco.resgataNumCad(apelidoFuncionario, nrEmpresa);

			log.info("Resgate passagem Matrícula: " + matricula);

			String erroCracha = atualizaHistoricoCracha(wrapper, nrEmpresa, matricula);
			if (erroCracha != null)
				return erroCracha;

			if (!NeoUtils.safeBoolean(wrapper.findGenericValue("validaFoto")))
			{
				/////////////////////////////////////////////////////////////////////////////////////////////
				/////INSERIDO O CÓDIGO DE INSERÇÃO DA FOTO FORA DA FICHA BÁSICA PARA ACUSAR O ERRO MESMO APÓS
				/////JÁ GERADA A FB.

				String erroFotoColab = atualizaFotoColaborador(wrapper, nrEmpresa, matricula);
				if (erroFotoColab != null)
					return erroFotoColab;
			}
			log.info("FICHA BÁSICA JÁ FOI GERADA");
		}

		String erro = new RHIntegracaoAdmissaoUtils().atualizaDadosTabela(wrapper, nrEmpresa, matricula, codUsuario);

		if (erro != null)
			return erro;

		return null;
	}

	public static String getchaveResultExam(String chaveCodigo)
	{//chave do Resultado do Exame
		String chaveResultExam = "ResultExam" + chaveCodigo;
		return chaveResultExam;
	}

	public static String getchaveASO(String chaveCodigo)
	{//chave da ASO
		String chaveASO = "ASO" + chaveCodigo;
		return chaveASO;
	}

	public static String getchaveDNas(String chaveCodigo)
	{ //chave do Dependentes Nascimento
		String chaveDNas = "DNas" + chaveCodigo;
		return chaveDNas;
	}

	public static String getchaveDepe(String chaveCodigo)
	{ //chave do Dependentes
		String chaveDepe = "Depe" + chaveCodigo;
		return chaveDepe;
	}

	public static String getchaveAdic(String chaveCodigo)
	{//chave do Adicionais
		String chaveAdic = "Adic" + chaveCodigo;
		return chaveAdic;
	}

	public static String getChaveFCom(String chaveCodigo)
	{//chave da Ficha Complementar
		String chaveFCom = "FCom" + chaveCodigo;
		return chaveFCom;
	}

	public static String getChaveFBas(String chaveCodigo)
	{//chave da Ficha Básica
		String chaveFBas = "FBas" + chaveCodigo;
		return chaveFBas;
	}

	public static String efetivarCadastroDependentes(EntityWrapper wrapper, String chaveCodigo, Long nrEmpresa, Long tipoColaborador, Long matricula)
	{
		//INTEGRAÇÃO Dep e DepNas
		List<NeoObject> listaDependentes = wrapper.findGenericValue("listaDepend");
		List<NeoObject> registroFPDepe = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + getchaveDepe(chaveCodigo) + "'"));

		if (registroFPDepe.size() == 0 && NeoUtils.safeBoolean(wrapper.findGenericValue("possuiDepend")))
		{
			Integer coddep = 1;

			for (NeoObject dependente : listaDependentes)
			{
				List<NeoObject> registroFDep = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + getchaveDepe(chaveCodigo) + dependente.getNeoId() + "'"));

				if (registroFDep.size() == 0)
				{
					EntityWrapper wDependente = new EntityWrapper(dependente);

					DependentesDependente2In integrarDepIn = getObjetoIntegracaoDependentes(nrEmpresa, tipoColaborador, matricula, coddep, wDependente);
					String erroDep = RHIntegracaoServico.integrarDependente(chaveCodigo, coddep, integrarDepIn);

					if (ocorreuErro(erroDep) && !erroDep.contains("Registro já existe."))
					{
						wrapper.setValue("errosIntegracao", erroDep);
						wrapper.setValue("contErros", 1l);
						wrapper.setValue("validaIntegracao", false);

						return erroDep;
					}
					else
					{
						adicionaRegistroIntegracao(getchaveDepe(chaveCodigo));
						adicionaRegistroIntegracao(getchaveDepe(chaveCodigo) + coddep);

						List<NeoObject> registrogetchaveDNas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + getchaveDNas(chaveCodigo) + dependente.getNeoId() + "'"));

						if (registrogetchaveDNas == null || registrogetchaveDNas.isEmpty())
						{
							DependentesNascimentoIn integrarDepNasIn = getObjetoIntegracaoDependentesNasc(nrEmpresa, tipoColaborador, matricula, coddep, wDependente);
							String erroDepNas = RHIntegracaoServico.integraDependenteNascimento(chaveCodigo, coddep, integrarDepNasIn);

							if (erroDepNas != null && erroDepNas.contains("erro"))
							{
								wrapper.setValue("errosIntegracao", erroDep);
								wrapper.setValue("contErros", 1l);
								wrapper.setValue("validaIntegracao", false);

								return erroDep;
							}
							else
							{
								log.info("NASCIMENTO DO DEPENDENTE INSERIDO COM SUCESSO!");

								adicionaRegistroIntegracao(getchaveDNas(chaveCodigo) + dependente.getNeoId());
							}
						}
					}
				}
				coddep++;
			}
		}

		return null;
	}

	public static String validarNegocio(EntityWrapper wrapper)
	{
		return null;
	}

	public static Boolean isSede(EntityWrapper wrapper)
	{
		Boolean eSede = false;

		Long codRegional = wrapper.findGenericValue("regionalColab.usu_codreg");

		if (codRegional != null && (codRegional == 0l || codRegional == 1l || codRegional == 9l))
			eSede = true;

		return eSede;
	}

	public static boolean cadastroJaRegistrado(String chave)
	{
		List<NeoObject> registroASO = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chave + "'"));
		return registroASO != null && !registroASO.isEmpty();
	}

	public static ResultadosExamesResultados3In getObjetoResultadoExame(EntityWrapper wrapper, Long matricula, Long nrEmpresa)
	{
		ResultadosExamesResultados3In integrarResultExamIn = new ResultadosExamesResultados3In();

		String StcodFic = wrapper.findGenericValue("documentacao.fichamedica");
		if (StcodFic == null)
		{
			Integer fichaMedica = RHIntegracaoBanco.getFichaMedica(matricula, nrEmpresa);
			if (fichaMedica != null && fichaMedica > 0)
			{
				wrapper.findField("documentacao.fichamedica").setValue(fichaMedica.toString());
				;
				StcodFic = fichaMedica.toString();
			}
		}

		Integer codFic = Integer.parseInt(StcodFic);

		// INÍCIO INTEGRAÇÃO RESULTADO DO EXAME
		GregorianCalendar GCultexm = wrapper.findGenericValue("ultExameMedic");
		String ultexm = new SimpleDateFormat("dd/MM/YYY hh:mm:ss").format(GCultexm.getTime());
		//IN RESULTADO EXAMES
		log.info("INTEGRACAO ResultadoExame : codFic=" + codFic);

		integrarResultExamIn.setNumEmp(nrEmpresa.intValue());
		log.info("INTEGRACAO ResultadoExame : numemp=" + nrEmpresa.intValue());

		integrarResultExamIn.setCodFic(codFic);
		log.info("INTEGRACAO ResultadoExame : codFic=" + codFic);

		integrarResultExamIn.setCodExa(2);
		log.info("INTEGRACAO ResultadoExame : codExa=2");

		integrarResultExamIn.setDatSol(ultexm);
		log.info("INTEGRACAO ResultadoExame : datSol=" + ultexm);

		integrarResultExamIn.setTipOpe("i");
		log.info("INTEGRACAO ResultadoExame : tipOpe=i");

		integrarResultExamIn.setOriExa(1);
		log.info("INTEGRACAO ResultadoExame : oriExa=1");

		integrarResultExamIn.setCodAte(132); //código da Thays fixamente
		log.info("INTEGRACAO ResultadoExame : codAte=132");

		integrarResultExamIn.setConPPP("S");
		log.info("INTEGRACAO ResultadoExame : conPPP=S");

		integrarResultExamIn.setSitExa("R");
		log.info("INTEGRACAO ResultadoExame : SitExa=R");

		integrarResultExamIn.setTipExa("R");
		log.info("INTEGRACAO ResultadoExame : TipExa=R");

		integrarResultExamIn.setCodPar(40);
		log.info("INTEGRACAO ResultadoExame : codPar=40");

		integrarResultExamIn.setNorAlt("N");
		log.info("INTEGRACAO ResultadoExame : norAlt=N");

		integrarResultExamIn.setEvoRes("E");
		log.info("INTEGRACAO ResultadoExame : evoRes=E");

		integrarResultExamIn.setSitRes("O");
		log.info("INTEGRACAO ResultadoExame : sitRes=O");
		return integrarResultExamIn;
	}

	public static String efetivarIntegracaoAdicional(EntityWrapper wrapper, String chaveCodigo, Long matricula)
	{
		if (!cadastroJaRegistrado(getchaveAdic(chaveCodigo)))
		{
			String erro = RHIntegracaoServico.integrarVetorFichaAdicional(chaveCodigo, matricula, wrapper);

			if (ocorreuErro(erro))
			{
				wrapper.setValue("errosIntegracao", erro);
				wrapper.setValue("contErros", 1l);
				wrapper.setValue("validaIntegracao", false);

				log.warn("Retorno Integracao Adicional - chave: " + chaveCodigo + ": " + erro);

				return erro;
			}
			else
			{
				log.info("ADICIONAIS GERADOS COM SUCESSO!");

				adicionaRegistroIntegracao(getchaveAdic(chaveCodigo));
			}
		}

		return null;
	}

	public static AsoexternoASOExterno2In getObjetoIntegraASO(EntityWrapper wrapper, Long numemp, String ultexm, String StcodFic)
	{
		Integer codFic;
		AsoexternoASOExterno2In integrarASOIn = new AsoexternoASOExterno2In();

		codFic = Integer.parseInt(StcodFic);
		log.info("INTEGRACAO AsoExterna : codFic=" + codFic);

		Long codMed = wrapper.findGenericValue("medico.codate");
		integrarASOIn.setTipOpe("i"); //FIXO COMO INCLUSÃO
		log.info("INTEGRACAO AsoExterna : tipOpe=" + integrarASOIn.getTipOpe());

		integrarASOIn.setNumEmp(numemp.intValue());
		log.info("INTEGRACAO AsoExterna : numemp=" + integrarASOIn.getNumEmp());

		integrarASOIn.setCodAte(codMed.intValue()); //criar campo
		log.info("INTEGRACAO AsoExterna : codMed=" + integrarASOIn.getCodAte());

		integrarASOIn.setNumAte(RHIntegracaoBanco.numAte(numemp, codMed));
		log.info("INTEGRACAO AsoExterna : numAte=" + integrarASOIn.getNumAte());

		integrarASOIn.setDatAte(ultexm);
		log.info("INTEGRACAO AsoExterna : ultexm=" + integrarASOIn.getDatAte());

		integrarASOIn.setTipAso(1);
		log.info("INTEGRACAO AsoExterna : tipAso=" + integrarASOIn.getTipAso());

		integrarASOIn.setCodFic(codFic);
		log.info("INTEGRACAO AsoExterna : codFic=" + integrarASOIn.getCodFic());

		integrarASOIn.setCodPar(40);
		log.info("INTEGRACAO AsoExterna : codPar=" + integrarASOIn.getCodPar());
		return integrarASOIn;
	}

	public static String cadastraExame(EntityWrapper wrapper, String chaveCodigo, Long numemp)
	{
		if (ParametrizacaoAdmissao.findParameter("efetuaIntegracaoResultadoExame") != null && !ParametrizacaoAdmissao.findParameter("efetuaIntegracaoResultadoExame").equals("0"))
		{
			if (!cadastroJaRegistrado(getchaveResultExam(chaveCodigo)))
			{
				Long matricula = wrapper.findGenericValue("matricula");
				if (matricula == null)
					matricula = new Long(RHIntegracaoBanco.resgataNumCad(chaveCodigo, numemp));

				ResultadosExamesResultados3In integrarResultExamIn = getObjetoResultadoExame(wrapper, matricula, numemp);

				String erroResultExam = RHIntegracaoServico.integraExames(chaveCodigo, integrarResultExamIn);

				if ((erroResultExam != null && erroResultExam.contains("erro")) && !erroResultExam.contains("Registro já existe"))
				{
					wrapper.setValue("errosIntegracao", erroResultExam);
					wrapper.setValue("contErros", 1l);
					wrapper.setValue("validaIntegracao", false);

					return "ERRO NA CRIAÇÃO DO RESULTADO DO EXAME DA ASO - " + erroResultExam;
				}
				else
				{
					log.info("Registro do Resultado do Exame inserido com sucesso!");

					adicionaRegistroIntegracao(getchaveResultExam(chaveCodigo));
				}
			}
		}

		return null;
	}

	//Verifica se a mensagem de erro existe
	public static boolean ocorreuErro(String erro)
	{
		return erro != null && erro.trim().length() > 0;
	}

	public static String cadastrarASO(EntityWrapper wrapper, String chaveCodigo, Long numemp)
	{
		if (ParametrizacaoAdmissao.findParameter("efetuaIntegracaoASO") != null && !ParametrizacaoAdmissao.findParameter("efetuaIntegracaoASO").equals("0"))
		{
			String erroASO = null;
			if (!cadastroJaRegistrado(getchaveASO(chaveCodigo)))
			{
				Long matricula = wrapper.findGenericValue("matricula");
				if (matricula == null)
					matricula = new Long(RHIntegracaoBanco.resgataNumCad(chaveCodigo, numemp));

				AsoexternoASOExternoIn integrarASOIn = getObjetoIntegraASO(wrapper, matricula, numemp);

				erroASO = RHIntegracaoServico.integraASO(chaveCodigo, integrarASOIn);
			}

			if ((erroASO != null && erroASO.contains("erro")) && !erroASO.contains("Registro já existe"))
			{
				wrapper.setValue("errosIntegracao", erroASO);
				wrapper.setValue("contErros", 1l);
				wrapper.setValue("validaIntegracao", false);

				return "ERRO NA CRIAÇÃO DA ASO EXTERNA  " + erroASO;
			}
			else
			{
				log.info("Registro da ASO inserido com sucesso!");

				adicionaRegistroIntegracao(getchaveASO(chaveCodigo));
			}
		}

		return null;
	}

	public static AsoexternoASOExternoIn getObjetoIntegraASO(EntityWrapper wrapper, Long matricula, Long nrEmpresa)
	{
		String StcodFic = wrapper.findGenericValue("documentacao.fichamedica");
		if (StcodFic == null)
		{
			Integer fichaMedica = RHIntegracaoBanco.getFichaMedica(matricula, nrEmpresa);
			if (fichaMedica != null && fichaMedica > 0)
			{
				wrapper.findField("documentacao.fichamedica").setValue(fichaMedica.toString());
				;
				StcodFic = fichaMedica.toString();
			}
		}

		Integer codFic = Integer.parseInt(StcodFic);
		AsoexternoASOExternoIn integrarASOIn = new AsoexternoASOExternoIn();
		//BUSCA CAMPOS ASO E RESULTADOS
		GregorianCalendar GCultexm = wrapper.findGenericValue("ultExameMedic");
		String ultexm = new SimpleDateFormat("dd/MM/YYY hh:mm:ss").format(GCultexm.getTime());
		log.info("INTEGRACAO AsoExterna : codFic=" + codFic);

		Long codMed = wrapper.findGenericValue("medico.codate");
		integrarASOIn.setTipOpe("i"); //FIXO COMO INCLUSÃO
		log.info("INTEGRACAO AsoExterna : tipOpe=" + integrarASOIn.getTipOpe());

		integrarASOIn.setNumEmp(nrEmpresa.intValue());
		log.info("INTEGRACAO AsoExterna : numemp=" + integrarASOIn.getNumEmp());

		integrarASOIn.setCodAte(codMed.intValue()); //criar campo
		log.info("INTEGRACAO AsoExterna : codMed=" + integrarASOIn.getCodAte());

		integrarASOIn.setNumAte(RHIntegracaoBanco.numAte(nrEmpresa, codMed));
		log.info("INTEGRACAO AsoExterna : numAte=" + integrarASOIn.getNumAte());

		integrarASOIn.setDatAte(ultexm);
		log.info("INTEGRACAO AsoExterna : ultexm=" + integrarASOIn.getDatAte());

		integrarASOIn.setTipAso(1);
		log.info("INTEGRACAO AsoExterna : tipAso=" + integrarASOIn.getTipAso());

		integrarASOIn.setCodFic(codFic);
		log.info("INTEGRACAO AsoExterna : codFic=" + integrarASOIn.getCodFic());

		integrarASOIn.setCodPar(40);
		log.info("INTEGRACAO AsoExterna : codPar=" + integrarASOIn.getCodPar());

		return integrarASOIn;
	}
}
