package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class CDAAtualizaE085HCL implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try {
			String nomeFonteDados = "SAPIENS";
	
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			
			// valores a serem atualizados no update
			String portador = (String) processEntity.findValue("portador");;
			String banco = (String) processEntity.findValue("banco");;
			String agencia = (String) processEntity.findValue("agencia");;
			String contaCorrente = (String) processEntity.findValue("contaCorrente");;
			String identificacaoBanco  = (String) processEntity.findValue("identificacaoBanco");;
			
			// valores utilizados na condicao where
			Long empresa = (Long) processEntity.findValue("contrato.usu_codemp");
			Long filial = (Long) processEntity.findValue("contrato.usu_codfil");
			Long cliente = (Long) processEntity.findValue("contrato.codcli");
	
			StringBuffer sql = new StringBuffer();
			sql.append("UPDATE E085HCL ");
			sql.append("SET porsi1 = ?, ");
			sql.append("codban = ?, ");
			sql.append("codage = ?, ");
			sql.append("ccbcli = ?, ");
			sql.append("usu_cliban = ? ");
			sql.append("WHERE CODEMP = ? ");
			sql.append("AND CODFIL = ? ");
			sql.append("AND CODCLI = ? ");
			
			try
			{
				PreparedStatement st = connection.prepareStatement(sql.toString());
				//UPDATE
				st.setString(1, portador);
				st.setString(2, banco);
				st.setString(3, agencia);
				st.setString(4, contaCorrente);
				st.setString(5, identificacaoBanco);
				//WHERE
				st.setLong(6, empresa);
				st.setLong(7, filial);
				st.setLong(8, cliente);
	
				st.executeUpdate();
			}
			catch (WorkflowException ex)
			{
				System.out.println("Erro na classe CDAAtualizaE085HCL - WorkflowException");
				ex.printStackTrace();
				throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
			}
			catch (SQLException e)
			{
				System.out.println("Erro na classe CDAAtualizaE085HCL - SQLException");
				e.printStackTrace();
				throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
			}
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe CDAAtualizaE085HCL - SQLException");
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
