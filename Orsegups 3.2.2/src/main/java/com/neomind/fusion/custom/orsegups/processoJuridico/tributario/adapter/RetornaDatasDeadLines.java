package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.RetornaDatasDeadLines

public class RetornaDatasDeadLines implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		GregorianCalendar deadLineDtAud = null;
		GregorianCalendar deadLineDtProAud = null;
		NeoUser resposanvel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));
		@SuppressWarnings("unchecked")
		List<NeoObject> registroAtividade = (List<NeoObject>) processEntity.getValue("j002RegistroHistoricoAtividade");
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002RegistroHistoricoAtividade");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
		wRegAti.setValue("responsavel", resposanvel.getCode());
		wRegAti.setValue("dataAcao", new GregorianCalendar());

		if (processEntity.getValue("deadLineDatAud") == null)
		{
			GregorianCalendar dtAud = (GregorianCalendar) processEntity.getValue("datAud");

			deadLineDtAud = (GregorianCalendar) dtAud.clone();
			deadLineDtAud.add(GregorianCalendar.DATE, -1);
			deadLineDtAud.add(GregorianCalendar.HOUR_OF_DAY, 23);
			deadLineDtAud.add(GregorianCalendar.MINUTE, 59);
			deadLineDtAud.add(GregorianCalendar.SECOND, 59);
			System.out.println("data deadLineDtAud : " + NeoDateUtils.safeDateFormat(deadLineDtAud, "dd/MM/yyyy"));
			processEntity.setValue("deadLineDatAud", deadLineDtAud);
			wRegAti.setValue("descricao", "Aguardando data da Audiência dia " + NeoDateUtils.safeDateFormat(dtAud, "dd/MM/yyyy"));
			wRegAti.setValue("atividade", "Aguardando data da Audiência");
		}

		if (processEntity.getValue("datProAud") != null)
		{
			GregorianCalendar dtProAud = (GregorianCalendar) processEntity.getValue("datProAud");
			deadLineDtProAud = (GregorianCalendar) dtProAud.clone();
			deadLineDtProAud.add(GregorianCalendar.DATE, -1);
			deadLineDtProAud.add(GregorianCalendar.HOUR_OF_DAY, 23);
			deadLineDtProAud.add(GregorianCalendar.MINUTE, 59);
			deadLineDtProAud.add(GregorianCalendar.SECOND, 59);
			System.out.println("data deadLineDtProAud : " + NeoDateUtils.safeDateFormat(deadLineDtProAud, "dd/MM/yyyy"));
			processEntity.setValue("deadLineDtProximaAud", deadLineDtProAud);
			wRegAti.setValue("descricao", "Aguardando data da Próxima Audiência dia " + NeoDateUtils.safeDateFormat(dtProAud, "dd/MM/yyyy"));
			wRegAti.setValue("atividade", "Aguardando data da Próxima Audiência");
		}

		PersistEngine.persist(objRegAti);
		registroAtividade.add(objRegAti);
		processEntity.setValue("j002RegistroHistoricoAtividade", registroAtividade);
	    }catch(Exception e){
		System.out.println("Erro na classe RetornaDatasDeadLines do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
