package com.neomind.fusion.custom.casvig;

import javax.persistence.PersistenceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.engine.custom.CustomNeoRunnable;

/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */

public class ThreadSigmaConnect extends CustomNeoRunnable
{

	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(ThreadSigmaConnect.class);

	private boolean close = false;

	private long sleep = 86400000;

	private String name = new String("Sigma Connect");

	@Override
	public String getLogName()
	{
		return name;
	}

	@Override
	public boolean isClosed()
	{
		return close;
	}

	@Override
	public long sleepThread()
	{
		return sleep;
	}

	@Override
	public void run()
	{
		CasvigPersistSigma casvigPersist = new CasvigPersistSigma();
		if (casvigPersist.connectSigma())
			throw new PersistenceException();
	}
}
