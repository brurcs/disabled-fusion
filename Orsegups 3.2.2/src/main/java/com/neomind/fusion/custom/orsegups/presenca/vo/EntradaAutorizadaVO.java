package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;

public class EntradaAutorizadaVO
{
	private ColaboradorVO colaborador;
	private ColaboradorVO colaboradorSubstituido;
	private PostoVO posto;
	private String descricaoCobertura;
	private GregorianCalendar dataInicial;
	private GregorianCalendar dataFinal;
	private Long horaInicial;
	private String observacao;
	private String excluir;
	private GregorianCalendar dataIniRet;
	private GregorianCalendar dataFimRet;

	public String getObservacao()
	{
		return this.observacao;
	}

	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}

	public GregorianCalendar getDataInicial()
	{
		return this.dataInicial;
	}

	public void setDataInicial(GregorianCalendar dataInicial)
	{
		this.dataInicial = dataInicial;
	}

	public GregorianCalendar getDataFinal()
	{
		return this.dataFinal;
	}

	public void setDataFinal(GregorianCalendar dataFinal)
	{
		this.dataFinal = dataFinal;
	}

	public ColaboradorVO getColaboradorSubstituido()
	{
		return colaboradorSubstituido;
	}

	public void setColaboradorSubstituido(ColaboradorVO colaboradorSubstituido)
	{
		this.colaboradorSubstituido = colaboradorSubstituido;
	}
	
	public String getBtnDelete()
	{
		return QLPresencaUtils.getLinkBtnDelete(this);
	}

	public Long getHoraInicial()
	{
		return horaInicial;
	}

	public void setHoraInicial(Long horaInicial)
	{
		this.horaInicial = horaInicial;
	}

	public String getDescricaoCobertura()
	{
		return descricaoCobertura;
	}

	public void setDescricaoCobertura(String descricaoCobertura)
	{
		this.descricaoCobertura = descricaoCobertura;
	}

	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}

	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}

	public PostoVO getPosto()
	{
		return posto;
	}

	public void setPosto(PostoVO posto)
	{
		this.posto = posto;
	}

	public String getExcluir()
	{
		return excluir;
	}

	public void setExcluir(String excluir)
	{
		this.excluir = excluir;
	}

	public GregorianCalendar getDataIniRet() {
	    return dataIniRet;
	}

	public void setDataIniRet(GregorianCalendar dataIniRet) {
	    this.dataIniRet = dataIniRet;
	}

	public GregorianCalendar getDataFimRet() {
	    return dataFimRet;
	}

	public void setDataFimRet(GregorianCalendar dataFimRet) {
	    this.dataFimRet = dataFimRet;
	}
	
	
}