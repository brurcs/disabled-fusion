package com.neomind.fusion.custom.orsegups.fap.slip.job;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class FAPSlipPagamentosComissaoJob implements CustomJobAdapter
{
	Boolean printLog = false;
	
	public String iniciarPagamento(Long codRep, Long codCal)
	{
		String codRepNotIn = "";
		String paramertoCodRepNotIn = buscarParametro("codRepNotIn");
		if (NeoUtils.safeIsNotNull(paramertoCodRepNotIn) && !paramertoCodRepNotIn.equals("0"))
			codRepNotIn = paramertoCodRepNotIn;
		
		QLGroupFilter filtroRep = new QLGroupFilter("AND");
		filtroRep.addFilter(new QLEqualsFilter("sitrep", "A"));
		filtroRep.addFilter(new QLRawFilter("codrep NOT IN (" + codRepNotIn + ")"));
		filtroRep.addFilter(new QLEqualsFilter("codrep", codRep));		
		NeoObject noRep = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSEREP"), filtroRep);
		
		return iniciarPagamento(noRep, codCal);
	}
	
	public String iniciarPagamento(NeoObject noRep, Long codCal)
	{
		try
		{
			EntityWrapper wRep = new EntityWrapper(noRep);
			
			Long codContrato = 0L;
			String codContratoStr = wRep.findGenericValue("usu_ctrfap");
			if (NeoUtils.safeIsNotNull(codContratoStr) && !codContratoStr.trim().isEmpty())
				codContrato = new Long(codContratoStr);
			
			NeoObject noCal = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUCAL"), new QLEqualsFilter("usu_codcal", codCal));
			if (NeoUtils.safeIsNotNull(noCal))
			{
				EntityWrapper wCal = new EntityWrapper(noCal);
				
				GregorianCalendar gDataCompetencia = wCal.findGenericValue("usu_cptcal");
				GregorianCalendar gDataIni = (GregorianCalendar) gDataCompetencia.clone();
				gDataIni.add(Calendar.DAY_OF_MONTH, -1);
				GregorianCalendar gDataFim = (GregorianCalendar) gDataCompetencia.clone();
				gDataFim.add(Calendar.DAY_OF_MONTH, 1);
				
				List<NeoObject> listPagtos = FAPSlipUtils.buscarProcessos(codContrato, gDataIni, gDataFim, false);
				if (NeoUtils.safeIsNotNull(listPagtos))
				{
					for (NeoObject noPagto : listPagtos)
					{
						WFProcess process = new EntityWrapper(noPagto).findGenericValue("wfprocess");
						if (process.getProcessState().equals(ProcessState.running))
						{
							return process.getCode();
						}
					}
				}
				
				Long codRep = wRep.findGenericValue("codrep");
				String codigoUsuario = wRep.findGenericValue("usu_usufus");
				
				QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("codContrato", codContrato), new QLEqualsFilter("ativo", true));
				NeoObject noContrato = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipCadastroContrato"), qlGroup);
				if (NeoUtils.safeIsNull(noContrato))
				{
					System.out.println("FAPSlipPagamentosComissaoJob:..: Contrato " + codContrato + " do representante " + codRep +" não encontrado.");
					throw new WorkflowException("Campo usu_ctrfap da tela Cadastros - Representantes não preenchido");
				}			
				
				BigDecimal totalComissoes = new BigDecimal(0);
				BigDecimal totalComissoesSemDesconto = new BigDecimal(0);
				
				QLGroupFilter qlGroupComissoes = new QLGroupFilter("AND", new QLEqualsFilter("usu_codrep", codRep), new QLEqualsFilter("usu_codcal", codCal));
				List<NeoObject> listComissoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUT240CVL"), qlGroupComissoes);
				List<NeoObject> listComissoesManuais = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUT240CVM"), qlGroupComissoes);
				
				Map<BigDecimal, BigDecimal> comissoes = calcularComissoes(listComissoes);
				for (Entry<BigDecimal, BigDecimal> comissao : comissoes.entrySet())
				{
					totalComissoesSemDesconto = totalComissoesSemDesconto.add(comissao.getKey());
					totalComissoes = totalComissoes.add(comissao.getValue());
				}
				
				comissoes = calcularComissoes(listComissoesManuais);
				for (Entry<BigDecimal, BigDecimal> comissao : comissoes.entrySet())
				{
					totalComissoesSemDesconto = totalComissoesSemDesconto.add(comissao.getKey());
					totalComissoes = totalComissoes.add(comissao.getValue());
				}
				
				NeoObject noTipoRateio = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipTipoRateio"), new QLEqualsFilter("codigo", 2L));
				
				NeoObject noCentrosCusto = AdapterUtils.createNewEntityInstance("FAPSlipCentroCusto");
				EntityWrapper wCentrosCusto = new EntityWrapper(noCentrosCusto);
				
				wCentrosCusto.setValue("tipoRateio", noTipoRateio);
				
				Long codEmp = new EntityWrapper(noContrato).findGenericValue("empresa.codemp");
				gerarRateios(listComissoes, wCentrosCusto, codEmp, codRep, totalComissoesSemDesconto, totalComissoes);
				gerarRateios(listComissoesManuais, wCentrosCusto, codEmp, codRep, totalComissoesSemDesconto, totalComissoes);
				
				BigDecimal totalRateios = new BigDecimal(0);
				List<NeoObject> listRateios = wCentrosCusto.findGenericValue("rateio");
				for (NeoObject noRateio : listRateios)
				{
					if (noRateio != null)
					{
						EntityWrapper wRateio = new EntityWrapper(noRateio);
						
						BigDecimal valorRateio = wRateio.findGenericValue("valor");
						if (valorRateio != null)
						{
							totalRateios = totalRateios.add(valorRateio);
						}
					}
				}
				
				totalComissoes = totalComissoes.setScale(2, BigDecimal.ROUND_HALF_UP);
				totalRateios = totalRateios.setScale(2, BigDecimal.ROUND_HALF_UP);
				
				BigDecimal diferenca = totalComissoes.subtract(totalRateios);
				if (!diferenca.equals(new BigDecimal(0)))
				{					
					for (NeoObject noRateio : listRateios)
					{
						if (noRateio != null)
						{
							EntityWrapper wRateio = new EntityWrapper(noRateio);
							BigDecimal valorRateio2 = wRateio.findGenericValue("valor");
							valorRateio2 = valorRateio2.add(diferenca);
							
							wRateio.setValue("valor", valorRateio2);
							
							PersistEngine.persist(noRateio);
							break;
						}
					}
				}
				
				NeoObject noOperacao = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipOperacao"), new QLEqualsFilter("codigo", 1L));
				NeoObject noTipoPagamento = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipTipoPagamento"), new QLEqualsFilter("codigo", 1L));
				NeoObject noModalidade = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipModalidadeContrato"), new QLEqualsFilter("codigo", 1L));
				NeoUser nuSolicitante = new EntityWrapper(noContrato).findGenericValue("responsavelContrato");
				NeoUser nuExecutor = PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", codigoUsuario));
				
				if (NeoUtils.safeIsNull(nuExecutor)) {
					System.out.println("FAPSlipPagamentosComissaoJob:..: Não encontrado o executor do representante " + codRep);
					throw new WorkflowException("Não encontrado o executor no cadastro desse representante");
				}
				
				GregorianCalendar competencia = new GregorianCalendar();
				competencia.set(Calendar.DAY_OF_MONTH, 1);
				competencia.set(Calendar.MONTH, gDataCompetencia.get(Calendar.MONTH));
				competencia.set(Calendar.HOUR, 0);
				competencia.set(Calendar.MINUTE, 0);
				
				NeoObject noFAP = AdapterUtils.createNewEntityInstance("FAPSlip");
				EntityWrapper wFAP = new EntityWrapper(noFAP);
				
				wFAP.setValue("operacao", noOperacao);
				wFAP.setValue("tipoPagamento", noTipoPagamento);
				wFAP.setValue("dadosContrato", noContrato);
				wFAP.setValue("modalidadeContrato", noModalidade);
				wFAP.setValue("solicitante", nuExecutor);
				PersistEngine.persist(noFAP);
				
				ProcessModel pm = PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "F002 - FAP Slip"));
				WFProcess processo = pm.startProcess(noFAP, false, null, null, null, null, nuSolicitante);
				
				Activity atividade = processo.getOpenActivities().get(0);
				TaskInstance taskInstance = atividade.getInstance();
				Task tarefa = atividade.getActivity().getTaskAssigner().assign((UserActivity) atividade, (NeoUser) nuExecutor, true);
				RuntimeEngine.getTaskService().complete(taskInstance, 4);
				
				wFAP.setValue("solicitante", nuExecutor);
				wFAP.setValue("competencia", competencia);
				wFAP.setValue("centrosCusto", noCentrosCusto);
				wFAP.setValue("valorPagamento", totalComissoes);
				wFAP.setValue("observacoes", "Pagamento Comissão");
				
				QLGroupFilter qlGroupTaskInstance = new QLGroupFilter("AND");
				qlGroupTaskInstance.addFilter(new QLEqualsFilter("processName", "F002 - FAP Slip"));
				qlGroupTaskInstance.addFilter(new QLEqualsFilter("taskName", "Inserir Dados Pagamento"));
				qlGroupTaskInstance.addFilter(new QLEqualsFilter("code", processo.getCode()));
				taskInstance = PersistEngine.getObject(TaskInstance.class, qlGroupTaskInstance);
				taskInstance.setOwner(nuExecutor);
				tarefa.setUser(nuExecutor);
				PersistEngine.persist(taskInstance);
				PersistEngine.persist(tarefa);
				
				return processo.getCode();
			}
			else
			{
				throw new WorkflowException("Não foi encontrado no SAPIENS o calendário solicitado");
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
	
	private void gerarRateios(List<NeoObject> listComissoes, EntityWrapper wCentrosCusto, Long codEmp, Long codRep, BigDecimal totalComissoesSemDesconto, BigDecimal totalComissoes)
	{
		for (NeoObject noComissao : listComissoes)
		{
			wCentrosCusto.findField("rateio").addValue(gerarRateio(codEmp, codRep, noComissao, totalComissoesSemDesconto, totalComissoes));
		}
	}
	
	private NeoObject gerarRateio(Long codEmp, Long codRep, NeoObject noComissao, BigDecimal totalComissoesSemDesconto, BigDecimal totalComissoes)
	{
		EntityWrapper wComissao = new EntityWrapper(noComissao);
		
		BigDecimal comissao = wComissao.findGenericValue("usu_vlrcom");
		if (NeoUtils.safeIsNotNull(comissao) && comissao.compareTo(new BigDecimal(0)) > 0)
		{
			comissao = comissao.divide(totalComissoesSemDesconto, 5, RoundingMode.HALF_UP).multiply(totalComissoes);
			
			NeoObject noCCU = null;
			String codCCU = wComissao.findGenericValue("usu_codccu");
			if (NeoUtils.safeIsNull(codCCU))
			{
				System.out.println("FAPSlipPagamentosComissaoJob:..:O representante " + codRep + " não possui centro de custo cadastrado em uma das suas comissões.");
				throw new WorkflowException("Sem CCU");
			}
			else
			{
				QLGroupFilter qlGroupCCU = new QLGroupFilter("AND", new QLEqualsFilter("codccu", codCCU), new QLEqualsFilter("nivccu", 8L), new QLEqualsFilter("acerat", "S"), new QLEqualsFilter("codemp", codEmp));
				noCCU = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), qlGroupCCU);
				if (NeoUtils.safeIsNull(noCCU))
				{
					System.out.println("FAPSlipPagamentosComissaoJob:..: Centro de custo " + codCCU + " do representante " + codRep + " não foi encontrado");
					throw new WorkflowException("CCU " + codCCU + " não foi encontrado");
				}
			}
			
			NeoObject noRateio = AdapterUtils.createNewEntityInstance("FAPSlipRateio");
			EntityWrapper wRateio = new EntityWrapper(noRateio);
			
			wRateio.setValue("valor", comissao);
			wRateio.setValue("centroCusto", noCCU);
			
			return noRateio;
		}
		
		return null;
	}
	
	
	
	private Map<BigDecimal, BigDecimal> calcularComissoes(Long codRep, Long codCal, String tabela)
	{
		QLGroupFilter qlGroupComissoes = new QLGroupFilter("AND", new QLEqualsFilter("usu_codrep", codRep), new QLEqualsFilter("usu_codcal", codCal));		
		List<NeoObject> listComissoes = PersistEngine.getObjects(AdapterUtils.getEntityClass(tabela), qlGroupComissoes);			
		
		return calcularComissoes(listComissoes);
	}
	
	private Map<BigDecimal, BigDecimal> calcularComissoes(List<NeoObject> listComissoes)
	{
		Map<BigDecimal, BigDecimal> map = new HashMap<BigDecimal, BigDecimal>();
		
		BigDecimal totalComissoes = new BigDecimal(0);
		BigDecimal totalComissoesSemDesconto = new BigDecimal(0);
		for (NeoObject noComissao : listComissoes)
		{
			EntityWrapper wComissao = new EntityWrapper(noComissao);
			
			BigDecimal comissao = wComissao.findGenericValue("usu_vlrcom");
			if (NeoUtils.safeIsNotNull(comissao))
			{
				totalComissoes = totalComissoes.add(comissao);
				if (comissao.compareTo(new BigDecimal(0)) > 0)
					totalComissoesSemDesconto = totalComissoesSemDesconto.add(comissao);
			}
		}
		
		map.put(totalComissoesSemDesconto, totalComissoes);
		
		return map;
	}
	
	@Override
	public void execute(CustomJobContext arg0)
	{
		Long codRep = 0l;
		String codRepNotIn = "";
		
		String paramertoCodRep = buscarParametro("codRepPagtoComissao");
		if (NeoUtils.safeIsNotNull(paramertoCodRep) && !paramertoCodRep.equals("0"))
			codRep = new Long(paramertoCodRep);
		
		String paramertoCodRepNotIn = buscarParametro("codRepNotIn");
		if (NeoUtils.safeIsNotNull(paramertoCodRepNotIn) && !paramertoCodRepNotIn.equals("0"))
			codRepNotIn = paramertoCodRepNotIn;
		
		QLGroupFilter filtroRep = new QLGroupFilter("AND");
		filtroRep.addFilter(new QLEqualsFilter("sitrep", "A"));
		filtroRep.addFilter(new QLRawFilter("codrep NOT IN (" + codRepNotIn + ")"));
		if (codRep != 0L)
			filtroRep.addFilter(new QLEqualsFilter("codrep", codRep));
		
		List<NeoObject> listRep = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEREP"),filtroRep,-1, -1,"usu_codreg, nomrep");
		for (NeoObject noRep : listRep)
		{
			iniciarPagamento(noRep, null);
		}
	}
		
	private String buscarParametro(String parametro)
	{
		NeoObject noParametrizacao = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipParametrizacao"), new QLEqualsFilter("parametro", parametro));
		if (NeoUtils.safeIsNotNull(noParametrizacao))
		{
			EntityWrapper wParametrizacao = new EntityWrapper(noParametrizacao);
			String valor = wParametrizacao.findGenericValue("valor");
			if (NeoUtils.safeIsNotNull(valor))
				return valor;
		}
		
		return "0";
	}
}
