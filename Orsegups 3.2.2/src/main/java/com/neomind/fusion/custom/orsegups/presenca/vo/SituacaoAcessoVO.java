package com.neomind.fusion.custom.orsegups.presenca.vo;

public class SituacaoAcessoVO
{
	private Long codigoSituacao;
	private String descricaoSituacao;
	private Long qdeHorasSituacao;
	private String horasSituacao;
	private String observacaoSituacao;
	private String situacaoExcecao;
	private String linkIntrajornada;
	
	private ApuracaoVO apuracao;
	
	public Long getCodigoSituacao()
	{
		return codigoSituacao;
	}
	public void setCodigoSituacao(Long codigoSituacao)
	{
		this.codigoSituacao = codigoSituacao;
	}
	public String getDescricaoSituacao()
	{
		return descricaoSituacao;
	}
	public void setDescricaoSituacao(String descricaoSituacao)
	{
		this.descricaoSituacao = descricaoSituacao;
	}
	public Long getQdeHorasSituacao()
	{
		return qdeHorasSituacao;
	}
	public void setQdeHorasSituacao(Long qdeHorasSituacao)
	{
		this.qdeHorasSituacao = qdeHorasSituacao;
	}
	public String getObservacaoSituacao()
	{
		return observacaoSituacao;
	}
	public void setObservacaoSituacao(String observacaoSituacao)
	{
		this.observacaoSituacao = observacaoSituacao;
	}
	public String getHorasSituacao()
	{
		return horasSituacao;
	}
	public void setHorasSituacao(String horasSituacao)
	{
		this.horasSituacao = horasSituacao;
	}
	public ApuracaoVO getApuracao()
	{
		return apuracao;
	}
	public void setApuracao(ApuracaoVO apuracao)
	{
		this.apuracao = apuracao;
	}
	public String getSituacaoExcecao()
	{
		return situacaoExcecao;
	}
	public void setSituacaoExcecao(String situacaoExcecao)
	{
		this.situacaoExcecao = situacaoExcecao;
	}
	public String getLinkIntrajornada()
	{
		return linkIntrajornada;
	}
	public void setLinkIntrajornada(String linkIntrajornada)
	{
		this.linkIntrajornada = linkIntrajornada;
	}
}
