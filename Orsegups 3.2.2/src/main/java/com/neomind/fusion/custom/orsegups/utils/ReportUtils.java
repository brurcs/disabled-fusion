package com.neomind.fusion.custom.orsegups.utils;



import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.sql.Connection;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import com.neomind.fusion.doc.NeoStorage;

public final class ReportUtils {

private static final Locale LOCALE_BRAZIL;
	
	static {
		LOCALE_BRAZIL = new Locale("pt", "BR");
	}
	
	public static java.io.InputStream getReportJasper(String reportName) {
		String pathReport = File.separator + "reports" + File.separator + reportName + ".jasper";
		System.out.println("===>>>>>> "+pathReport);
		ClassLoader classLoader = ReportUtils.class.getClassLoader();		
		InputStream inputStream = classLoader.getResourceAsStream(pathReport);
		
		return inputStream;
	}
	
	public static java.io.InputStream getImage(String image) {
		String pathImage = File.separator + "reports" + File.separator + "img" + File.separator + image;
		
		ClassLoader classLoader = ReportUtils.class.getClassLoader();		
		InputStream inputStream = classLoader.getResourceAsStream(pathImage);
		
		return inputStream;
	}
	
	
	
	public static String formatBrazilCurrency(BigDecimal valor){
		BigDecimal bigDecValue = (BigDecimal)valor;
        bigDecValue = bigDecValue.setScale(2, BigDecimal.ROUND_HALF_UP);
        NumberFormat formatter = DecimalFormat.getCurrencyInstance(LOCALE_BRAZIL);
        formatter.setMinimumFractionDigits(2);
        return formatter.format(bigDecValue.doubleValue());
	}
	
	public static File getJasper(String reportName, Connection fusion) {
	    
	    File file = null;
	    
	    try {
	    
	    InputStream is = null;
	    file = File.createTempFile(reportName, ".pdf");
	    file.deleteOnExit();
	    String caminho = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + reportName +".jasper";
	    
	    HashMap<String, Object> parametros = new HashMap<String, Object>();
	    parametros.put("sql", "teste");

	    is = new BufferedInputStream(new FileInputStream(caminho));
	    	    
	    JasperPrint impressao = JasperFillManager.fillReport(is, parametros, fusion);
		if (impressao != null && file != null)
		{
			JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
			return file;
		}
	    
	    } catch (Exception e) {
		System.err.println(e);
	    }
	    
	    return file;
	}
	
}