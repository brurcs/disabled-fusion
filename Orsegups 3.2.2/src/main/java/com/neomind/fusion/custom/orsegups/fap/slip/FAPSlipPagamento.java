package com.neomind.fusion.custom.orsegups.fap.slip;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.text.StrSubstitutor;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipOrcamentoUtils;
import com.neomind.fusion.custom.orsegups.fap.slip.utils.FAPSlipUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsGroupLevels;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class FAPSlipPagamento
{
	private EntityWrapper wrapper = null;
	private Task task = null;
	private Map<String, String> parametros = new HashMap<String, String>();
	
	public FAPSlipPagamento(EntityWrapper wrapper, Task task)
	{
		this.wrapper = wrapper;
		this.task = task;
	}
	
	public void iniciarPagamentoContrato()
	{
		Boolean usuarioPermitido = false;
		List<NeoObject> listUsuariosPermitidos = null;
		
		Long codModalidadePagto = wrapper.findGenericValue("modalidadeContrato.codigo");
		if (codModalidadePagto != null)
		{
			if (codModalidadePagto == 1l)
			{
				Long codModalidadeContrato = wrapper.findGenericValue("dadosContrato.modalidadeContrato.codigo");
				if (!codModalidadeContrato.equals(codModalidadePagto))
					throw new WorkflowException("A modalidade de pagamento selecionada não corresponde ao contrato informado.");
				
				criarHistoricoPagamentos(wrapper);
				listUsuariosPermitidos = wrapper.findGenericValue("dadosContrato.listaPermissao");
				Long codTipoOrcamento = wrapper.findGenericValue("dadosContrato.tipoOrcamento.codigo");
				Long codCriterioAprovacao = wrapper.findGenericValue("dadosContrato.criterioAprovacao.codigo");
				if ((codCriterioAprovacao != 5 && codCriterioAprovacao != 6 && codTipoOrcamento != null && codTipoOrcamento == 2) || (codCriterioAprovacao == 6))
				{
					List<NeoObject> listUsuariosPermitidosOrcamento = wrapper.findGenericValue("dadosContrato.orcamentoCadastrado.listaPermissao");
					usuarioPermitido = FAPSlipUtils.validarPermissaoUsuario(listUsuariosPermitidosOrcamento, PortalUtil.getCurrentUser());
					if (!usuarioPermitido)
						throw new WorkflowException("Você não ter permissão para usar o orçamento deste contrato");
				}
				
				List<SecurityEntity> listPermissao = wrapper.findGenericValue("dadosContrato.listaPermissao");				
				WFProcess processo = wrapper.findGenericValue("wfprocess");							
				FAPSlipUtils.adicionarVisualizadoresProcesso(processo, listPermissao);
				
				wrapper.setValue("tituloTarefa", "Contrato");
			}
			else if (codModalidadePagto == 2l)
			{
				//criarHistoricoContrato(wrapper);
				
				listUsuariosPermitidos = wrapper.findGenericValue("orcamento.listaPermissao");
				
				List<SecurityEntity> listPermissao = wrapper.findGenericValue("orcamento.listaPermissao");				
				WFProcess processo = wrapper.findGenericValue("wfprocess");							
				FAPSlipUtils.adicionarVisualizadoresProcesso(processo, listPermissao);
				
				wrapper.setValue("tituloTarefa", "Orçamento");
			}
		}
		else
			listUsuariosPermitidos = wrapper.findGenericValue("dadosContrato.listaPermissao");
		
		usuarioPermitido = FAPSlipUtils.validarPermissaoUsuario(listUsuariosPermitidos, PortalUtil.getCurrentUser());
		
		if (!usuarioPermitido)
			throw new WorkflowException("Você não ter permissão para iniciar esse contrato.");
		
		preencherDadosContrato();
		
		String operacao = wrapper.findGenericValue("operacao.descricao");
		String descricaoHistorico = "Processo Iniciado. Operação: " + operacao;
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	public void inserirDadosPagamento()
	{
		String descricaoHistorico = wrapper.findGenericValue("observacoes");
		
		GregorianCalendar competencia = wrapper.findGenericValue("competencia");
		competencia.set(Calendar.DAY_OF_MONTH, 1);
		competencia.set(Calendar.HOUR, 0);
		competencia.set(Calendar.MINUTE, 0);
		
		validarDadosPagamento();
		
		Long codEmp = wrapper.findGenericValue("empresa.codemp");
		NeoPaper npPapelValidarPagamento = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "FAPSlipValidarPgtoEmpresa" + codEmp));
		if (npPapelValidarPagamento == null)
			throw new WorkflowException("O papel de validação de pagamentos da empresa " + codEmp + " não existe. Contate a TI.");
		else
			wrapper.setValue("executorPagamento", npPapelValidarPagamento);
		
		NeoUser nuResponsavel = null;
		Long tipoPagamento = wrapper.findGenericValue("tipoPagamento.codigo");
		if (tipoPagamento == 1l)
		{
			Long codModalidade = wrapper.findGenericValue("modalidadeContrato.codigo");
			if (codModalidade == null || codModalidade == 1l)
			{
				nuResponsavel = wrapper.findGenericValue("dadosContrato.responsavelContrato");
				NeoObject noUsuarioResponsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), new QLEqualsFilter("nomusu", nuResponsavel.getCode()));
				if (noUsuarioResponsavel == null)
					throw new WorkflowException("Não encontrado o Código do usuário Responsável no SAPIENS!");
				
				GregorianCalendar dataFimVigencia = wrapper.findGenericValue("dadosContrato.fimVigencia");
				if (dataFimVigencia != null)
				{
					dataFimVigencia.set(Calendar.HOUR, 0);
					dataFimVigencia.set(Calendar.MINUTE, 0);
					
					if (dataFimVigencia.before(competencia))
						throw new WorkflowException("A vigência deste contrato expirou. Não é possível realizar pagamentos.");
				}
				
				wrapper.setValue("obsGTCGLN", wrapper.findGenericValue("dadosContrato.historicoSlip"));
					
				Boolean demandaAprovacao = analisarCriteriosAprovacao();
				/*if (demandaAprovacao && !confirmaPgamento)
					throw new WorkflowException("O pagamento não atigiu os critérios para pré-aprovação. Por favor, confirme os dados de pagamento.");*/
				
				List<NeoObject> listAprovadoresContrato = wrapper.findGenericValue("dadosContrato.aprovadores");
				List<NeoObject> listAprovadores = FAPSlipUtils.findAprovadoresPorNivel(listAprovadoresContrato, 1l);
				wrapper.setValue("listaAprovadores", listAprovadores);
			}
			else if (codModalidade != null && codModalidade == 2l)
			{
				Set<NeoObject> setProcessosPagamentos = new HashSet();
				WFProcess wfProcess = wrapper.findGenericValue("wfprocess");
				BigDecimal valorPgto = wrapper.findGenericValue("valorPagamento");
				GregorianCalendar dataPagamento = wrapper.findGenericValue("dataVencimento");
				
				wrapper.setValue("demandaAprovacao", true);
				wrapper.setValue("demandouAprovacao", true);
				wrapper.setValue("aprovacaoManual", true);
				wrapper.setValue("aprovacaoNiveis", false);	
				wrapper.setValue("aprovacaoOrcamento", false);	
				
				Boolean dataValida = validarDataVencto(wrapper);
				if (!dataValida)
				{
					String descricaoExtraPrazo = "<span style=\"color: red\"><b>Extra-Prazo</b></span>";
					descricaoExtraPrazo += "<br><span><b>Justificativa Extra-Prazo</b>: " + wrapper.findGenericValue("justificativaExtraPrazo") + "</span>";
					FAPSlipUtils.registrarHistorico(wrapper, "Sistema: Inserir Dados Pagamento", descricaoExtraPrazo, task.returnResponsible());
				}
				
				Boolean definirAprovadores = wrapper.findGenericValue("orcamento.definirAprovadores");
				if (definirAprovadores == null || !definirAprovadores)
				{
					NeoUser nuAprovadorAvulso = wrapper.findGenericValue("aprovadorAvulso");
					if (nuAprovadorAvulso == null)
					{
						nuAprovadorAvulso = wrapper.findGenericValue("solicitante");
						if (nuAprovadorAvulso.getGroup().getGroupLevel() >= OrsegupsGroupLevels.GERENCIA || PortalUtil.getCurrentUser().getGroup().getGroupLevel() >= OrsegupsGroupLevels.GERENCIA)
						{
							wrapper.setValue("demandaAprovacao", false);
							wrapper.setValue("demandouAprovacao", false);
							wrapper.setValue("aprovacaoManual", false);
							wrapper.setValue("aprovacaoNiveis", true);	
							wrapper.setValue("extraPrazo", false);	
						}
						else
						{
							nuAprovadorAvulso = FAPSlipUtils.findAprovadorAvulso(wrapper, nuAprovadorAvulso, OrsegupsGroupLevels.GERENCIA);
							
							if (nuAprovadorAvulso.equals(PortalUtil.getCurrentUser()))
							{
								nuAprovadorAvulso = FAPSlipUtils.findAprovadorAvulso(wrapper, nuAprovadorAvulso, OrsegupsGroupLevels.GERENCIA);
								
								if (nuAprovadorAvulso == null || nuAprovadorAvulso.getGroup().getGroupLevel() >= OrsegupsGroupLevels.GERENCIA)
								{
									wrapper.setValue("demandaAprovacao", false);
									wrapper.setValue("demandouAprovacao", false);
									wrapper.setValue("aprovacaoManual", false);
									wrapper.setValue("aprovacaoNiveis", true);
									wrapper.setValue("extraPrazo", false);	
								}
							}
						}
					}
					
					List<NeoObject> listAprovador = new ArrayList<>();
					listAprovador.add(nuAprovadorAvulso);
					
					wrapper.findField("listaAprovadores").setValue(listAprovador);
				}
				else
				{
					List<NeoObject> listAprovadoresOrcamento = wrapper.findGenericValue("orcamento.aprovadores");
					List<NeoObject> listAprovadores = FAPSlipUtils.findAprovadoresPorNivel(listAprovadoresOrcamento, 1l);
					wrapper.setValue("listaAprovadores", listAprovadores);
				}
				
				NeoObject noFAP = wrapper.getObject();
				FAPSlipOrcamentoUtils.validarPermissaoOrcamento(noFAP);
				
				NeoObject noContratoOrcamento = wrapper.findGenericValue("orcamento");
				List<NeoObject> listOrcamentosNaCompetencia = FAPSlipOrcamentoUtils.findOrcamentoPorCompetencia(noContratoOrcamento, competencia);
				if (listOrcamentosNaCompetencia == null || listOrcamentosNaCompetencia.isEmpty())
					throw new WorkflowException("Não existem orçamentos para a competência informada.");
				
				BigDecimal saldoDisponivel = FAPSlipOrcamentoUtils.consultarSaldoDisponivel(noContratoOrcamento, competencia);
				BigDecimal totalOrcamento = FAPSlipOrcamentoUtils.consultarOrcamentoPorCompetencia(noContratoOrcamento, competencia);
				
				if (saldoDisponivel.compareTo(new BigDecimal(0)) < 0 || totalOrcamento.compareTo(new BigDecimal(0)) <= 0)
					throw new WorkflowException("Contrato sem Orçamento Disponível. Você deve realizar ajuste no orçamento. Orçamento disponível: " + FAPSlipUtils.parseMonetario(saldoDisponivel));
				
				descricaoHistorico += "<li><b>Orçamento Total</b>: " + FAPSlipUtils.parseMonetario(totalOrcamento) + "</li>";
				descricaoHistorico += "<li><b>Saldo Anterior</b>: " + FAPSlipUtils.parseMonetario(saldoDisponivel.add(valorPgto)) + "</li>";
				descricaoHistorico += "<li><b>Valor desta Despesa</b>: " + FAPSlipUtils.parseMonetario(valorPgto) + "</li>";
				
				wrapper.setValue("aprovacaoOrcamento", saldoDisponivel.compareTo(new BigDecimal(0)) < 0 || totalOrcamento.compareTo(new BigDecimal(0)) <= 0);
			}
		}
		else if (tipoPagamento == 2l)
		{
			nuResponsavel = wrapper.findGenericValue("solicitante");
			NeoObject noUsuarioResponsavel = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), new QLEqualsFilter("nomusu", nuResponsavel.getCode()));
			if (noUsuarioResponsavel == null)
				throw new WorkflowException("Não encontrado o código do usuário responsável no SAPIENS!");
			
			Boolean dataValida = validarDataVencto(wrapper);
			
			if (!dataValida)
			{
				String descricaoExtraPrazo = "<span style=\"color: red\"><b>Extra-Prazo</b></span>";
				descricaoExtraPrazo += "<br><span><b>Justificativa Extra-Prazo</b>: " + wrapper.findGenericValue("justificativaExtraPrazo") + "</span>";
				FAPSlipUtils.registrarHistorico(wrapper, "Sistema: Inserir Dados Pagamento", descricaoExtraPrazo, task.returnResponsible());
			}
			
			wrapper.setValue("demandaAprovacao", true);
			wrapper.setValue("demandouAprovacao", true);
			wrapper.setValue("aprovacaoManual", true);
			wrapper.setValue("aprovacaoNiveis", false);	
			wrapper.setValue("aprovacaoOrcamento", false);	
			
			NeoUser nuAprovadorAvulso = wrapper.findGenericValue("aprovadorAvulso");
			if (nuAprovadorAvulso == null)
			{
				nuAprovadorAvulso = wrapper.findGenericValue("solicitante");
				nuAprovadorAvulso = FAPSlipUtils.findAprovadorAvulso(wrapper, nuAprovadorAvulso);
				
				if (nuAprovadorAvulso.equals(PortalUtil.getCurrentUser()))
					nuAprovadorAvulso = FAPSlipUtils.findAprovadorAvulso(wrapper, nuAprovadorAvulso);
			}
			
			List<NeoObject> listAprovador = new ArrayList<>();
			listAprovador.add(nuAprovadorAvulso);
			
			wrapper.findField("listaAprovadores").setValue(listAprovador);
		}
		else if (tipoPagamento == 3l)
		{						
			wrapper.setValue("demandaAprovacao", true);
			wrapper.setValue("demandouAprovacao", true);
			wrapper.setValue("aprovacaoManual", true);
		}
		else if (tipoPagamento == 4l)
		{
			
		}
		
		criarHistoricoFornecedor(wrapper);
		
		if (tipoPagamento == 1l || tipoPagamento == 2l)
		{
			String fornecedor = wrapper.findGenericValue("fornecedor.apefor");
			if (fornecedor == null || fornecedor.trim().length() == 0 || fornecedor.contains("#I"))
				fornecedor = wrapper.findGenericValue("fornecedor.nomfor");
				
			BigDecimal valorPagamento = wrapper.findGenericValue("valorPagamento");
			
			NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
			
			Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
			String tituloTarefa = "";
			if (tipoPagamento == 1l)
			{
				Long codModalidadePagto = wrapper.findGenericValue("modalidadeContrato.codigo");
				if (codModalidadePagto == 1l)
					tituloTarefa = "Contrato " + codContrato;
				else if (codModalidadePagto == 2l)
					tituloTarefa = "Orçamento";
			}
			else if (tipoPagamento == 2l)
				tituloTarefa = "Avulso";
			
			tituloTarefa += " - " + monetaryFormat.format(valorPagamento) + " - " + fornecedor;
			
			wrapper.setValue("tituloTarefa", tituloTarefa);
			
			List<NeoObject> listRateios = wrapper.findGenericValue("centrosCusto.rateio");
			for (NeoObject noRateio : listRateios)
			{
				EntityWrapper wRateio = new EntityWrapper(noRateio);
				
				BigDecimal valor = wRateio.findGenericValue("valor");
				if (valor.signum() == -1)
					throw new WorkflowException("O valor do rateio não pode ser negativo.");
			}
			
			Boolean pgtoParcelado = wrapper.findGenericValue("pgtoParcelado");
			if (pgtoParcelado != null && pgtoParcelado)
			{
				List<NeoObject> listParcelas = wrapper.findGenericValue("parcelasGLN");
				if (listParcelas != null)
				{
					BigDecimal totalParcelas = new BigDecimal(0);
					for (NeoObject noParcela : listParcelas)
					{
						EntityWrapper wParcela = new EntityWrapper(noParcela);
						
						BigDecimal valorParcela = wParcela.findGenericValue("valorParcela");
						totalParcelas = totalParcelas.add(valorParcela);
					}
					
					if (totalParcelas.compareTo(valorPagamento) != 0)
					{
						throw new WorkflowException("O valor total das parcelas deve ser igual ao valor do pagamento.");
					}
				}
			}
		}
		
		GregorianCalendar dataVencimento = wrapper.findGenericValue("dataVencimento");
		GregorianCalendar prazoTarefa = (GregorianCalendar) dataVencimento.clone();
		for (int i = 0; i < 4; i++)
		{
			prazoTarefa = OrsegupsUtils.getPrevWorkDay(prazoTarefa); 
		}
		
		wrapper.setValue("prazoTarefa", prazoTarefa);

		if (descricaoHistorico.equals(""))
			descricaoHistorico = "Mensagem Automática: Dados de pagamento inseridos";
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	public void validarPagamento(String hierarquia)
	{
		String descricaoHistorico = wrapper.findGenericValue("observacoes");
		
		Boolean aprovacaoControladoria = wrapper.findGenericValue("aprovacaoControladoria");
		if (aprovacaoControladoria)
		{						
			if (descricaoHistorico.equals(""))
				descricaoHistorico = "Mensagem Automática: Validado pela Controladoria";
			
			wrapper.setValue("executorControladoria", PortalUtil.getCurrentUser());
			
			NeoObject noContaFinanceira = wrapper.findGenericValue("contaFinanceira");
			NeoObject noContaContabil = wrapper.findGenericValue("contaContabil");
			List<NeoObject> listRateios = wrapper.findGenericValue("centrosCusto.rateio");
			for (NeoObject noRateio : listRateios)
			{
				EntityWrapper wRateio = new EntityWrapper(noRateio);
				
				NeoObject noContaFinanceiraRateio = wRateio.findGenericValue("contaFinanceira");
				if (noContaFinanceiraRateio == null && noContaFinanceira == null)
					throw new WorkflowException("A Conta Financeira do rateio deve ser preenchida.");
				else if (noContaFinanceiraRateio == null && noContaFinanceira != null)
					wRateio.setValue("contaFinanceira", noContaFinanceira);
				
				NeoObject noContaContabilRateio = wRateio.findGenericValue("contaContabil");
				if (noContaContabilRateio == null && noContaContabil == null)
					throw new WorkflowException("A Conta Contábil do rateio deve ser preenchida.");
				else if (noContaContabilRateio == null && noContaContabil != null)
					wRateio.setValue("contaContabil", noContaContabil);
				
			}
			
			if (hierarquia.equals(""))
			{
				NeoPaper npGerenciaControladoria = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "Responsável Controladoria"));
				wrapper.setValue("responsavelControladoria", npGerenciaControladoria);
			}
			else if (hierarquia.equals("Gerência"))
			{
				Long tipoLancamento = wrapper.findGenericValue("lancarNotaOuTitulo.codigoLancamento");
				if (tipoLancamento == 2l)
					FAPSlipUtils.preencherGTC(wrapper);
				
				wrapper.setValue("exigirAprovacao", null);
			}
		}
		else
		{
			Boolean exigirAprovacao = wrapper.findGenericValue("exigirAprovacao");
			if (exigirAprovacao != null && exigirAprovacao)
				descricaoHistorico += "<br>" + PortalUtil.getCurrentUser().getFullName() + " exigiu que esse pagamento passe por aprovação.";
		}
		
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	public void aprovarPagamento()
	{
		String descricaoHistorico = wrapper.findGenericValue("observacoes");
		
		Boolean aprovacaoPagamento = wrapper.findGenericValue("aprovarPagamento");
		if (aprovacaoPagamento)
		{
			if (descricaoHistorico.equals(""))
				descricaoHistorico = "Mensagem Automática: Pagamento Aprovado";
			
			Long codModalidadePagto = wrapper.findGenericValue("modalidadeContrato.codigo");
			Long tipoPagamento = wrapper.findGenericValue("tipoPagamento.codigo");
			if (tipoPagamento == 1l && codModalidadePagto == 1l)
			{
				List<NeoObject> listAprovadoresAtuais = wrapper.findGenericValue("listaAprovadores");
				List<NeoObject> listAprovadoresContrato = wrapper.findGenericValue("dadosContrato.aprovadores");					
				List<NeoObject> listAprovadoresNivelSuperior = FAPSlipUtils.findAprovadoresPorNivelSuperior(listAprovadoresAtuais, listAprovadoresContrato);
				if (!listAprovadoresNivelSuperior.isEmpty())
				{
					wrapper.setValue("aprovacaoNiveis", false);
				}
				else
				{
					wrapper.setValue("aprovacaoNiveis", true);
				}
				
				wrapper.setValue("listaAprovadores", listAprovadoresNivelSuperior);
			}
			else if (tipoPagamento == 2l || codModalidadePagto == 2l)
			{
				Boolean definirAprovadores = wrapper.findGenericValue("orcamento.definirAprovadores");
				if (definirAprovadores == null || !definirAprovadores)
				{
					NeoUser nuAprovadorAtual = (NeoUser) ((List)wrapper.findGenericValue("listaAprovadores")).get(0);
					NeoUser nuAprovador = null;
					if (codModalidadePagto == 2l)
						nuAprovador = FAPSlipUtils.findAprovadorAvulso(wrapper, nuAprovadorAtual, OrsegupsGroupLevels.GERENCIA);
					else
						nuAprovador = FAPSlipUtils.findAprovadorAvulso(wrapper, nuAprovadorAtual);
					
					if (nuAprovador != null)
					{
						List<NeoObject> listAprovador = new ArrayList<>();
						listAprovador.add(nuAprovador);
						wrapper.findField("listaAprovadores").setValue(listAprovador);
						wrapper.setValue("aprovacaoNiveis", false);
					}
					else
					{
						wrapper.setValue("aprovacaoNiveis", true);
					}	
				}
				else
				{
					List<NeoObject> listAprovadoresAtuais = wrapper.findGenericValue("listaAprovadores");					
					List<NeoObject> listAprovadoresContrato = wrapper.findGenericValue("orcamento.aprovadores");					
					List<NeoObject> listAprovadoresNivelSuperior = FAPSlipUtils.findAprovadoresPorNivelSuperior(listAprovadoresAtuais, listAprovadoresContrato);
					if (!listAprovadoresNivelSuperior.isEmpty())
					{
						wrapper.setValue("aprovacaoNiveis", false);
					}
					else
					{
						wrapper.setValue("aprovacaoNiveis", true);
					}
					
					wrapper.setValue("listaAprovadores", listAprovadoresNivelSuperior);
				}
			}
		}
		else
		{
			if (descricaoHistorico.equals(""))
				descricaoHistorico = "Mensagem Automática: Pagamento Reprovado";
		}
		
		if (task.returnResponsible().contains("em nome de Dilmo Wanderley Berger"))
			wrapper.setValue("tomarCienciaPresidente", true);
		
		FAPSlipUtils.registrarHistorico(wrapper, task.getActivityName(), descricaoHistorico, task.returnResponsible());
	}
	
	public void preencherDadosContrato()
	{
		NeoObject noContrato = wrapper.findGenericValue("dadosContrato");
		if (noContrato == null)
			return;
		EntityWrapper wContrato = new EntityWrapper(noContrato);

		Long diaProvavelPagto = wContrato.findGenericValue("diaProvavelPagto");
		Long formaPagamento = wContrato.findGenericValue("formaPagamento.codigoFormaPagamento");

		GregorianCalendar today = new GregorianCalendar();
		today.set(Calendar.DAY_OF_MONTH, (diaProvavelPagto != null ? diaProvavelPagto.intValue() : today.get(Calendar.DAY_OF_MONTH)));

		BigDecimal valorPagamento = wrapper.findGenericValue("valorPagamento"); 
		if (valorPagamento == null)
			wrapper.setValue("valorPagamento", wContrato.findGenericValue("valorBase")); 
		
		wrapper.setValue("codContrato", wContrato.findGenericValue("codContrato"));
		wrapper.setValue("formaPagamento", wContrato.findGenericValue("formaPagamento"));
		wrapper.setValue("dataVencimento", today);
		wrapper.setValue("fornecedor", wContrato.findGenericValue("fornecedor"));
		wrapper.setValue("empresa", wContrato.findGenericValue("empresa"));
		wrapper.setValue("filial", wContrato.findGenericValue("filial"));
		wrapper.setValue("contaContabil", wContrato.findGenericValue("contaContabil"));
		wrapper.setValue("contaFinanceira", wContrato.findGenericValue("contaFinanceira"));
		if (formaPagamento == 2l)
		{
			wrapper.setValue("banco", wContrato.findGenericValue("banco"));
			wrapper.setValue("agencia", wContrato.findGenericValue("agencia"));
			wrapper.setValue("conta", wContrato.findGenericValue("conta"));
		}
		
		NeoObject noCCU = EntityCloner.cloneNeoObject((NeoObject) wContrato.findGenericValue("centroCusto"));
		
		wrapper.setValue("centrosCusto", noCCU);

		List<NeoObject> listAprovadores = wContrato.findGenericValue("aprovadores");
		for (NeoObject noAprovadorContrato : listAprovadores)
		{
			EntityWrapper wAprovadorContrato = new EntityWrapper(noAprovadorContrato);
			NeoObject noAprovador = wAprovadorContrato.findGenericValue("aprovador");
			
			Long nivel = wAprovadorContrato.findGenericValue("nivel");
			if (nivel == 1l)
			{
				wrapper.findField("listaAprovadores").addValue(noAprovador);	
			}
		}
	}
	
	private void validarDadosPagamento()
	{
		Long codTipoRateio = wrapper.findGenericValue("centrosCusto.tipoRateio.codigo");
		Long codModalidadePagto = wrapper.findGenericValue("modalidadeContrato.codigo");
		BigDecimal totalRateio = wrapper.findGenericValue("centrosCusto.total");
		BigDecimal valorPagamento = wrapper.findGenericValue("valorPagamento");
		
		if (valorPagamento.equals(new BigDecimal("0")))
			throw new WorkflowException("Insira um valor para o pagamento");
		
		if (codTipoRateio == 1l && totalRateio.compareTo(new BigDecimal(100)) != 0)
			throw new WorkflowException("A porcentagem total de rateios deve chegar à 100. ");
		else if (codTipoRateio == 2l && totalRateio.compareTo(valorPagamento) != 0)
			throw new WorkflowException("O valor total do rateio deve ser igual ao valor do pagamento.");
		else if (codTipoRateio == 0l || codTipoRateio == null)
			throw new WorkflowException("É necessário escolher um tipo de rateio.");
		
		List<String> listCentroCustoInvalidos = new ArrayList<String>();
		
		totalRateio = new BigDecimal(0);
		List<NeoObject> listRateios = wrapper.findGenericValue("centrosCusto.rateio");
		for (NeoObject noRateio : listRateios)
		{
			EntityWrapper wRateio = new EntityWrapper(noRateio);
			
			NeoObject noCCU = wRateio.findGenericValue("centroCusto");
			if (noCCU == null)
			{
				String codCCU = wRateio.findGenericValue("codigoCentroCusto");
				if (codCCU != null && codCCU.trim().length() != 0)
				{
					Long codEmpresa = wrapper.findGenericValue("empresa.codemp");					
					NeoObject externoCentroCusto = FAPSlipUtils.buscarCCU(codEmpresa, codCCU);
					
					if (externoCentroCusto == null)
						listCentroCustoInvalidos.add(codCCU);
					else
						wRateio.setValue("centroCusto", externoCentroCusto);
				}
				else
					throw new WorkflowException("Existem rateios sem centro de centro de custo. Verifique-os e tente novamente.");
			}
			else
			{
				EntityWrapper wCCU = new EntityWrapper(noCCU);
				
				String aceitaRateio = wCCU.findGenericValue("acerat");
				if (!aceitaRateio.equals("S"))
				{
					listCentroCustoInvalidos.add((String) wCCU.findGenericValue("codccu"));
				}
			}
			
			BigDecimal valorRateio = wRateio.findGenericValue("valor");
			
			if (valorRateio != null)
				totalRateio = totalRateio.add(valorRateio);
		}
		
		if (!listCentroCustoInvalidos.isEmpty())
		{
			String erroCCU = "Centro de Custo(s) não localizado(s) ou não aceitam rateio! Por favor, verifique se o(s) Centro de Custo " + listCentroCustoInvalidos + " existem na Empresa de lançamento ou não aceitam rateio!";
			erroCCU = erroCCU.replaceAll("\\[|\\]|", "");
			throw new WorkflowException(erroCCU);
		}
		
		if (codTipoRateio == 1l && totalRateio.compareTo(new BigDecimal(100)) != 0)
			throw new WorkflowException("A porcentagem total de rateios deve chegar à 100. ");
		else if (codTipoRateio == 2l && totalRateio.compareTo(valorPagamento) != 0)
			throw new WorkflowException("O valor total do rateio deve ser igual ao valor do pagamento.");
		
		WFProcess processAtual = wrapper.findGenericValue("wfprocess");
		Long tipoPagamento = wrapper.findGenericValue("tipoPagamento.codigo");
		
		String nrDocumento = wrapper.findGenericValue("nrDocumento");
		GregorianCalendar competencia = wrapper.findGenericValue("competencia");
		NeoObject noFornecedor = wrapper.findGenericValue("fornecedor");
		
		if (tipoPagamento == 1l)
		{
			if (codModalidadePagto == 1l)
			{
				Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
				Long codCriterioAtual = wrapper.findGenericValue("dadosContrato.criterioAprovacao.codigo");
				QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("dadosContrato.codContrato", codContrato), new QLEqualsFilter("competencia", competencia));
				List<NeoObject> listForms = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup);
				if (!listForms.isEmpty())
				{
					for (NeoObject noForm : listForms)
					{
						EntityWrapper wForm = new EntityWrapper(noForm);
						
						//Long codCriterioAprovacao = wForm.findGenericValue("dadosContrato.criterioAprovacao.codigo");
						
						if ((codCriterioAtual != null && codCriterioAtual != 5 && codCriterioAtual != 6))
						{
							WFProcess process = wForm.findGenericValue("wfprocess");
							
							if (process != null && !process.getProcessState().equals(ProcessState.canceled) && !processAtual.getCode().equals(process.getCode()))
								throw new WorkflowException("Já existe um pagamento com essa competência para o contrato informado. Tarefa: " + process.getCode());
						}
					}
				}
				
				GregorianCalendar competenciaContrato = wrapper.findGenericValue("dadosContrato.competencia");
				if (competenciaContrato != null)
				{
					int mesCompetencia = competencia.get(Calendar.MONTH);
					int anoCompetencia = competencia.get(Calendar.YEAR);
					
					int mesCompetenciaContrato = competenciaContrato.get(Calendar.MONTH);
					int anoCompetenciaContrato = competenciaContrato.get(Calendar.YEAR);
					
					if ((anoCompetencia < anoCompetenciaContrato) || ((anoCompetencia == anoCompetenciaContrato) && (mesCompetencia < mesCompetenciaContrato)))
						throw new WorkflowException("Não é possível inserir um pagamento deste contrato com essa competência. A competência mínima é: " + NeoDateUtils.safeDateFormat(competenciaContrato, "MM/yyyy"));
				}
			}
			else if (codModalidadePagto == 2l)
			{
				
			}
		}
		else if (tipoPagamento == 2l)
		{
			QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("nrDocumento", nrDocumento), new QLEqualsFilter("competencia", competencia), new QLEqualsFilter("fornecedor", noFornecedor));		
			List<NeoObject> listForms = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup);
			if (!listForms.isEmpty())
			{
				for (NeoObject noForm : listForms)
				{
					WFProcess process = new EntityWrapper(noForm).findGenericValue("wfprocess");
					
					if (process != null && !process.getProcessState().equals(ProcessState.canceled) && !processAtual.getCode().equals(process.getCode()))
						throw new WorkflowException("Já existe um pagamento para a competência e documento informados. Tarefa: " + process.getCode());
				}
			}
			
			String obs = wrapper.findGenericValue("observacoes");
			if (obs == null || obs.isEmpty())
			{
				throw new WorkflowException("Para pagamento Avulso é obrigatório inserir uma observação.");
			}
		}		
		
		Long codFormaPagamento = wrapper.findGenericValue("formaPagamento.codigoFormaPagamento");
		if (codFormaPagamento == 2l)
		{
			String codBanco = wrapper.findGenericValue("banco.codban");
			String agencia = wrapper.findGenericValue("agencia");
			if (codBanco.equals("001"))
			{
				if (!agencia.contains("-"))
					throw new WorkflowException("Para pagamentos ao Banco do Brasil, é obrigatório informar o dígito da agência.");
			}
			else 
			{
				if (agencia.length() < 4)
					throw new WorkflowException("A agência deve conter pelo menos 4 números.");
			}
		}
		
		NeoUser nuSolicitante = wrapper.findGenericValue("solicitante");
		if (!nuSolicitante.equals(PortalUtil.getCurrentUser()))
		{
			if ((!PortalUtil.getCurrentUser().getGroup().getResponsible().contains(PortalUtil.getCurrentUser())
					&& nuSolicitante.getGroup().getResponsible().contains(nuSolicitante)) ||						
						(PortalUtil.getCurrentUser().getGroup().getResponsible().contains(PortalUtil.getCurrentUser())
							&& nuSolicitante.getGroup().getResponsible().contains(nuSolicitante)
							&& nuSolicitante.getGroup().getGroupLevel() > PortalUtil.getCurrentUser().getGroup().getGroupLevel())
				)
			{
				throw new WorkflowException("Não é possível selecionar esse solicitante.");
			}
		}
	}
	
	private Boolean analisarCriteriosAprovacao()
	{
		BigDecimal valorPgto = wrapper.findGenericValue("valorPagamento");
		Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
		Long codCriterio = wrapper.findGenericValue("dadosContrato.criterioAprovacao.codigo");
		Long codTipoContrato = wrapper.findGenericValue("dadosContrato.tipoDoContrato.codigo");
		Long codTipoOrcamento = wrapper.findGenericValue("dadosContrato.tipoOrcamento.codigo");
		GregorianCalendar competencia = wrapper.findGenericValue("competencia");
		competencia.set(Calendar.DAY_OF_MONTH, 1);
		competencia.set(Calendar.HOUR_OF_DAY, 0);
		competencia.set(Calendar.MINUTE, 0);
		competencia.set(Calendar.SECOND, 0);
		WFProcess wfProcess = wrapper.findGenericValue("wfprocess");
		
		parametros = new HashMap<String, String>();
		inserirMotivo("");
		
		Boolean demandaAprovacao = false;
		
		String descricaoHistorico = "${motivo} Cálculo para Aprovação: <ul>";
		
		if (codCriterio == 1l)
		{
			demandaAprovacao = true;
			inserirMotivo("Contrato sem pré-aprovação.<br>");
		}
		else if (codCriterio == 2l || codCriterio == 4l)
		{			
			BigDecimal valorCalculo = new BigDecimal(0);
			
			if (codCriterio == 2l)
			{
				Long mesesCompetencia = wrapper.findGenericValue("dadosContrato.periodoMedia");
				mesesCompetencia *= -1l;
				
				GregorianCalendar inicioCompetencia = new GregorianCalendar();
				inicioCompetencia.set(Calendar.DAY_OF_MONTH, 1);
				inicioCompetencia.add(Calendar.MONTH, mesesCompetencia.intValue());
				inicioCompetencia.set(Calendar.HOUR_OF_DAY, 0);
				inicioCompetencia.set(Calendar.MINUTE, 0);
				
				GregorianCalendar fimCompetencia = new GregorianCalendar();
				fimCompetencia.add(Calendar.DAY_OF_MONTH, 1);
				
				BigDecimal media = getTotalPagamentos(codContrato, inicioCompetencia, fimCompetencia, "media");
				parametros.put("media", media.toString());
				if (media.compareTo(new BigDecimal(0)) != 0)
				{					
					valorCalculo = media;
				}
				
				mesesCompetencia *= -1l;
				descricaoHistorico += "<li>Média (" + mesesCompetencia + " meses): ${media}</li>";
			} else if (codCriterio == 4l) 
			{
				valorCalculo = wrapper.findGenericValue("dadosContrato.valorBase");
				descricaoHistorico += "<li>Valor Base: " + FAPSlipUtils.parseMonetario(valorCalculo) + "</li>";
			}
			
			demandaAprovacao = calcularTolerancia(valorCalculo, valorPgto, wrapper);
			if (demandaAprovacao)
				inserirMotivo("Valor do pagamento ultrapassou a tolerância permitida.<br>");
			
			descricaoHistorico += "<li>Valor Max: ${valorMaxTolerancia} </li>";
			descricaoHistorico += "<li>Valor Min: ${valorMinTolerancia} </li>";
			
		} else if (codCriterio == 3l)
		{
			BigDecimal valorFixo = wrapper.findGenericValue("dadosContrato.valorBase");
			
			demandaAprovacao = valorPgto.compareTo(valorFixo) != 0;
			if (demandaAprovacao)
				inserirMotivo("Valor do pagamento ultrapassou o valor fixado para esse contrato.<br>");
			
			descricaoHistorico += "<li>Valor Fixo: " + FAPSlipUtils.parseMonetario(valorFixo) + "</li>";
		}
		
		if (codCriterio == 5l || (codCriterio != 5l && codCriterio != 6l && codTipoOrcamento!= null && codTipoOrcamento == 1l))
		{
			Set<NeoObject> setProcessosPagamentos = new HashSet();
			
			List<NeoObject> listOrcamento = null;
			
			if (codCriterio == 5l || (codCriterio != 5 && codCriterio != 6 && codTipoOrcamento == 1l))
				listOrcamento = wrapper.findGenericValue("dadosContrato.orcamento");
			
			BigDecimal totalOrcamento = new BigDecimal(0);
			totalOrcamento = FAPSlipOrcamentoUtils.consultarOrcamentoPorCompetencia(listOrcamento, competencia);
			
			listOrcamento = FAPSlipOrcamentoUtils.findOrcamentoPorCompetencia(listOrcamento, competencia);
			if (listOrcamento == null || listOrcamento.isEmpty())
				throw new WorkflowException("Não existem orçamentos para a competência informada.");
			
			if (!listOrcamento.isEmpty())
			{
				BigDecimal totalPago = new BigDecimal(0);
				
				for (NeoObject noOrcamento : listOrcamento)
				{
					EntityWrapper wOrcamento = new EntityWrapper(noOrcamento);
					
					GregorianCalendar inicioVigencia = wOrcamento.findGenericValue("inicioVigencia");
					inicioVigencia.set(Calendar.DAY_OF_MONTH, 1);
					inicioVigencia.set(Calendar.HOUR_OF_DAY, 0);
					inicioVigencia.set(Calendar.MINUTE, 0);
					inicioVigencia.set(Calendar.SECOND, 0);
					
					GregorianCalendar fimVigencia = wOrcamento.findGenericValue("fimVigencia");					
					fimVigencia = fimVigencia == null ? new GregorianCalendar() : fimVigencia;
					
					setProcessosPagamentos.addAll(FAPSlipUtils.buscarProcessos(codContrato, inicioVigencia, fimVigencia, false));
				}
				
				for (NeoObject noPagamento : setProcessosPagamentos)
				{
					EntityWrapper wPagamento = new EntityWrapper(noPagamento);
					
					WFProcess wfProcessPagamento = wPagamento.findGenericValue("wfprocess");
					if (wfProcessPagamento != null && !wfProcess.getCode().equals(wfProcessPagamento.getCode()))
					{
						BigDecimal valorPagamento = wPagamento.findGenericValue("valorPagamento");
						valorPagamento = valorPagamento != null ? valorPagamento : new BigDecimal(0);
						
						totalPago = totalPago.add(valorPagamento);
					}
				}
				
				BigDecimal valorDisponivel = totalOrcamento.subtract(totalPago);
				descricaoHistorico += "<li><b>Orçamento Total</b>: " + FAPSlipUtils.parseMonetario(totalOrcamento) + "</li>";
				descricaoHistorico += "<li><b>Saldo Anterior</b>: " + FAPSlipUtils.parseMonetario(valorDisponivel) + "</li>";
				descricaoHistorico += "<li><b>Valor desta Despesa</b>: " + FAPSlipUtils.parseMonetario(valorPgto) + "</li>";
				
				totalPago = totalPago.add(valorPgto);
				
				if (totalPago.compareTo(totalOrcamento) > 0)
					throw new WorkflowException("Contrato sem Orçamento Disponível. Você deve realizar ajuste no orçamento do contrato. Orçamento disponível: " + FAPSlipUtils.parseMonetario(valorDisponivel));
				
				valorDisponivel = valorDisponivel.subtract(valorPgto);
				descricaoHistorico += "<li><b>Orçamento Disponível Futuro</b>: " + FAPSlipUtils.parseMonetario(valorDisponivel) + "</li>";
				
				if (!demandaAprovacao)
					demandaAprovacao = totalPago.compareTo(totalOrcamento) > 0;
					
				if (totalPago.compareTo(totalOrcamento) > 0)
					inserirMotivo("<br>O pagamento ultrapassou o Orçamento informado.");
				
				wrapper.setValue("aprovacaoOrcamento", totalPago.compareTo(totalOrcamento) > 0);
			}
		}
		else if (codCriterio == 6l || (codCriterio != 5 && codCriterio != 6 && codTipoOrcamento != null && codTipoOrcamento == 2l))
		{
			NeoObject noContratoOrcamento = wrapper.findGenericValue("dadosContrato.orcamentoCadastrado");
			List<NeoObject> listOrcamentosNaCompetencia = FAPSlipOrcamentoUtils.findOrcamentoPorCompetencia(noContratoOrcamento, competencia);
			if (listOrcamentosNaCompetencia == null || listOrcamentosNaCompetencia.isEmpty())
				throw new WorkflowException("Não existem orçamentos para a competência informada.");
			
			BigDecimal saldoDisponivel = FAPSlipOrcamentoUtils.consultarSaldoDisponivel(noContratoOrcamento, competencia);
			BigDecimal totalOrcamento = FAPSlipOrcamentoUtils.consultarOrcamentoPorCompetencia(noContratoOrcamento, competencia);
			
			if (saldoDisponivel.compareTo(new BigDecimal(0)) < 0)
				throw new WorkflowException("Contrato sem Orçamento Disponível. Você deve realizar ajuste no orçamento. Orçamento disponível: " + FAPSlipUtils.parseMonetario(saldoDisponivel));
			
			descricaoHistorico += "<li><b>Orçamento Total</b>: " + FAPSlipUtils.parseMonetario(totalOrcamento) + "</li>";
			descricaoHistorico += "<li><b>Saldo Anterior</b>: " + FAPSlipUtils.parseMonetario(saldoDisponivel.add(valorPgto)) + "</li>";
			descricaoHistorico += "<li><b>Valor desta Despesa</b>: " + FAPSlipUtils.parseMonetario(valorPgto) + "</li>";
		}
		
		descricaoHistorico += "</ul>${statusPagamento}";
		
		if (demandaAprovacao)
			parametros.put("statusPagamento", "<span style=\"color: red\"><b>Valor</b>: Não Aprovado</span>");
		else
			parametros.put("statusPagamento", "<span style=\"color: green\"><b>Valor</b>: OK</span>");
		
		if (!validarDataVencto(wrapper))
		{
			descricaoHistorico += "<br><span style=\"color: red\"><b>Prazo</b>: Extra-Prazo</span>";
			descricaoHistorico += "<br><span style=\"color: red\"><b>Justificativa Extra-Prazo</b>: " + wrapper.findGenericValue("justificativaExtraPrazo") + "</span>";
			demandaAprovacao = true;
		}
		else 
		{
			descricaoHistorico += "<br><span style=\"color: green\"><b>Prazo</b>: No prazo</span>";
		}
		
		wrapper.setValue("demandaAprovacao", demandaAprovacao);
		wrapper.setValue("demandouAprovacao", demandaAprovacao);
		wrapper.setValue("aprovacaoManual", demandaAprovacao);
			
		StrSubstitutor sub = new StrSubstitutor(parametros);
		FAPSlipUtils.registrarHistorico(wrapper, "Sistema: Inserir Dados Pagamento", sub.replace(descricaoHistorico), task.returnResponsible());
		
		return demandaAprovacao;
	}
	
	private Boolean validarDataVencto(EntityWrapper wrapper)
	{
		Boolean dataValida = true;
		GregorianCalendar dataVencimento = wrapper.findGenericValue("dataVencimento");
		
		if (getDiferencaDiasUteis(new GregorianCalendar(), dataVencimento) <= 4)
		{
			Boolean valido = false;
			NeoUser nuUsuario = PortalUtil.getCurrentUser();
			
 			/*
 			String[] cargos = "Coordenação|coordenação|Coordenacao|coordenacao|Gerente|gerente|Gerência|gerência|Gerencia|gerencia|Diretoria|diretoria|Presidente|presidente|Nexti|nexti".split("\\|");
 			
 			 
 			for (String cargo : cargos)
			{
 				if (nuUsuario.getGroup().getName().contains(cargo))
 	 			{
 					valido = true;
 					break;
 	 			}
			}*/
			
			NeoPaper npResponsavel = nuUsuario.getGroup().getResponsible();
			if ((npResponsavel.getAllUsers().contains(nuUsuario) && nuUsuario.getGroup().getGroupLevel() >= OrsegupsGroupLevels.COORDENACAO) || nuUsuario.isAdm())
				valido = true;
 			 
 			if (!valido)
 			{
 				throw new WorkflowException("Pagamento classificado como extra-prazo. Lançamento só será possível por usuários de nível Coordenador ou superior.");
 			}
 			
 			String justificativaExtraPrazo = wrapper.findGenericValue("justificativaExtraPrazo");
 			if (justificativaExtraPrazo == null || justificativaExtraPrazo.isEmpty())
 				throw new WorkflowException("É necessário informar o motivo para extra-prazo.");
 			
 			wrapper.setValue("extraPrazo", true);
 			dataValida = false;
		}
		else 
		{
			wrapper.setValue("extraPrazo", false);
		}
		
		dataVencimento.set(Calendar.HOUR_OF_DAY, 23);
		dataVencimento.set(Calendar.MINUTE, 59);
		if (dataVencimento.before(new GregorianCalendar()))
			throw new WorkflowException("A data de vencimento não pode ser menor que a data atual");
		
		if (!OrsegupsUtils.isWorkDay(dataVencimento))
		{
			dataVencimento = OrsegupsUtils.getNextWorkDay(dataVencimento);
			if (dataVencimento != null)
				wrapper.setValue("dataVencimento", dataVencimento);
			
			throw new WorkflowException("A data de vencimento deve ser um dia útil.");
		}
		
		return dataValida;
	}
	
	private Boolean calcularTolerancia(BigDecimal valorBase, BigDecimal valorPagamento, EntityWrapper wrapper)
	{
		BigDecimal toleranciaAcima = wrapper.findGenericValue("dadosContrato.toleranciaAcima");
		BigDecimal toleranciaAbaixo = wrapper.findGenericValue("dadosContrato.toleranciaAbaixo");
		toleranciaAcima = toleranciaAcima != null ? toleranciaAcima : new BigDecimal(0);
		toleranciaAbaixo = toleranciaAbaixo != null ? toleranciaAbaixo : new BigDecimal(0);

		Long codTipoTolerancia = wrapper.findGenericValue("dadosContrato.tipoTolerancia.codigo");
		
		BigDecimal valorAcima = new BigDecimal(0);
		BigDecimal valorAbaixo = new BigDecimal(0);
		if (codTipoTolerancia == 1l)
		{
			valorAcima = toleranciaAcima.multiply(valorBase).divide(new BigDecimal(100), BigDecimal.ROUND_UP);
			valorAbaixo = toleranciaAbaixo.multiply(valorBase).divide(new BigDecimal(100), BigDecimal.ROUND_UP);
		} else if (codTipoTolerancia == 2l)
		{
			valorAcima = toleranciaAcima;
			valorAbaixo = toleranciaAbaixo;
		}
		
		BigDecimal valorMaxTolerancia = valorBase.add(valorAcima);
		BigDecimal valorMinTolerancia = valorBase.subtract(valorAbaixo);
		
		parametros.put("valorMaxTolerancia", FAPSlipUtils.parseMonetario(valorMaxTolerancia));
		parametros.put("valorMinTolerancia", FAPSlipUtils.parseMonetario(valorMinTolerancia));
		
		if (valorPagamento.compareTo(valorMaxTolerancia) > 0 || valorPagamento.compareTo(valorMinTolerancia) < 0)
			return true;
		else
			return false;
	}
	
	private void criarHistoricoContrato(EntityWrapper wrapper)
	{
		Long codContrato = wrapper.findGenericValue("dadosContrato.codContrato");
		
		List<NeoObject> listHistoricoContrato = FAPSlipUtils.buscarProcessos(codContrato, null, null);
		
		for (NeoObject noEntity : listHistoricoContrato)
		{
			wrapper.findField("historicoContrato").addValue(noEntity);
		}
	}
	
	private void criarHistoricoFornecedor(EntityWrapper wrapper)
	{
		Long codFornecedor = wrapper.findGenericValue("fornecedor.codfor");
		
		List<NeoObject> listHistoricoFornecedor = buscarPagamentosFornecedor(codFornecedor);
		
		wrapper.findField("historicoFornecedor").setValue(listHistoricoFornecedor);
	}
	
	private void criarHistoricoPagamentos(EntityWrapper wrapper) 
	{
		criarHistoricoFornecedor(wrapper);
		criarHistoricoContrato(wrapper);
	}
	
	private List<NeoObject> buscarPagamentosFornecedor(Long codFornecedor)
	{		
		QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("codfor", codFornecedor), new QLEqualsFilter("sittit", "LQ"), new QLRawFilter("codemp not in (3, 4, 6)"));
		List<NeoObject> listTitulos = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE501TCP"), qlGroup, 0, 10, "datppt desc");
		
		return listTitulos;
	}
	
	private BigDecimal getTotalPagamentos(Long codContrato, GregorianCalendar inicioCompetencia, GregorianCalendar fimCompetencia, String operacao)
	{
		BigDecimal totalPagamentos = new BigDecimal(0);
		
		List<NeoObject> listProcessos = FAPSlipUtils.buscarProcessos(codContrato, inicioCompetencia, fimCompetencia);
		if (!listProcessos.isEmpty())
		{
			for (NeoObject processo : listProcessos)
			{
				EntityWrapper wForm = new EntityWrapper(processo);
				
				BigDecimal valorPagamento = wForm.findGenericValue("valorPagamento");
				valorPagamento = valorPagamento != null ? valorPagamento : new BigDecimal(0);
				
				totalPagamentos = totalPagamentos.add(valorPagamento);
			}
		}
		
		if (operacao.equals("media"))
		{
			if (listProcessos.isEmpty())
				return new BigDecimal(0);
			else 
				return totalPagamentos.divide(new BigDecimal(listProcessos.size()), BigDecimal.ROUND_UP);
		}
		else 
			return totalPagamentos;
	}
	
	private Integer getDiferencaDiasUteis(GregorianCalendar inicio, GregorianCalendar fim)
	{
		inicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
		inicio.set(GregorianCalendar.MINUTE, 0);
		inicio.set(GregorianCalendar.SECOND, 0);
		inicio.set(GregorianCalendar.MILLISECOND, 0);
		
		fim.set(GregorianCalendar.HOUR_OF_DAY, 0);
		fim.set(GregorianCalendar.MINUTE, 0);
		fim.set(GregorianCalendar.SECOND, 0);
		fim.set(GregorianCalendar.MILLISECOND, 0);
		
		Integer diasDiferenca = 0;
		while (inicio.getTimeInMillis() < fim.getTimeInMillis())
		{
			inicio.add(Calendar.DAY_OF_MONTH, 1);

			if (OrsegupsUtils.isWorkDay(inicio))
				diasDiferenca++;
		}
		
		
		return diasDiferenca;
	}
	
	private void inserirMotivo(String motivo)
	{
		String motivoAtual = parametros.get("motivo");
		if (motivoAtual != null && motivoAtual.length() != 0)
			motivoAtual += motivo;
		else 
			motivoAtual = motivo;
		
		parametros.put("motivo", motivoAtual);
	}
}
