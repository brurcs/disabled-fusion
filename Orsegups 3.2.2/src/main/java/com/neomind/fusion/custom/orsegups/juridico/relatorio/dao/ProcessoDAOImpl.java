package com.neomind.fusion.custom.orsegups.juridico.relatorio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;

import com.neomind.fusion.custom.orsegups.juridico.relatorio.db.ConexaoUtils;
import com.neomind.fusion.custom.orsegups.juridico.relatorio.dto.ProcessoDTO;
import com.neomind.fusion.persist.PersistEngine;

public class ProcessoDAOImpl implements ProcessoDAO {
	
	@Override
	public List<ProcessoDTO> listaTudoAberto(String add) throws Exception {
		List<ProcessoDTO> retorno = new ArrayList<ProcessoDTO>();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		sql.append("select tabela1.centro, ");
		sql.append("       case when (tabela2.acerat = 'S') then 'A' else 'I' end as centroSit, ");
		sql.append("       tabela1.CCU, ");
		sql.append("       tabela1.CCUSit, ");
		sql.append("       tabela1.Autor, ");
		sql.append("       tabela1.Processo, ");
		sql.append("       tabela1.datCit, ");
		sql.append("       tabela1.datGer, ");
		sql.append("       tabela1.meses, ");
		sql.append("       tabela1.escritorio, ");
		sql.append("       tabela1.tarefa ");
		sql.append("  from (select coalesce(v034.codccu,'') as centro, ");
		sql.append("                    j002.codccuInformado as CCU, ");
		sql.append("		            case when ((j002.codccuInformado is not null) and (j002.codccuInformado <> v034.codccu)) ");
		sql.append("		                 then 'A' ");
		sql.append("		                 else '' end as CCUSit, ");
		sql.append("		            case j002.isColaborador when 1 then v034.nomfun else autor.nomAut end as Autor, ");
		sql.append("					j002.numPro as Processo, ");
		sql.append("					j002.dataCitacao as 'datCit', ");
		sql.append("					GETDATE() as 'datGer', ");
		sql.append("					esc.nome as escritorio, ");
		sql.append("					cast(DATEDIFF(month, j002.dataCitacao, GETDATE()) as nvarchar)+' meses' as meses, ");
		sql.append("					wf.code as tarefa ");
		sql.append("			   from D_j002Principal j002 with(nolock) ");
		sql.append("		 inner join WFProcess wf with(nolock) on wf.neoId = j002.wfprocess_neoId ");
		sql.append("		  left join D_j002EscAdvPro esc on j002.escRecPro_neoId = esc.neoId ");
		sql.append("		  left join D_j002Autor autor with(nolock) on autor.neoId = j002.autorNaoCol_neoId ");
		sql.append("		  left join X_VETORHUSUFUNFUSION fun with(nolock) on fun.neoId = j002.colaborador_neoId ");
		sql.append("		  left join [FSOODB04\\sql02].vetorh.dbo.USU_V034FUSION v034 with(nolock) on v034.pk = fun.pk ");
		sql.append("		 	  where wf.processState = 0 ");
		sql.append("		 	    and numPro is not null ");
		sql.append(add);
		sql.append("		 	    and numPro <> 'TESTE DA TI' ");
		sql.append("		 	    and numPro not in ('456456456456','9999999999') ");
		sql.append("		) as tabela1 ");
		sql.append("  left join (select distinct codccu,acerat,claccu,codemp ");
		sql.append("               from [FSOODB04\\SQL02].Sapiens.dbo.E044CCU b) as tabela2 ");
		sql.append("         on tabela2.codccu = tabela1.centro ");		
		sql.append("        and tabela2.codemp = SUBSTRING(tabela2.claccu,3,2) ");
		sql.append("   order by 2 desc, case when (tabela1.centro = '') then 1 end desc, 5");
		
		//[cachoeira\\sql01].vetorhTST.dbo.USU_V034FUSION   <-- Homologação  
		//[FSOODB04\\sql02].vetorh.dbo.USU_V034FUSION       <-- Produção
		//System.out.println(sql.toString());
		try {
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = PersistEngine.getConnection("");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next()) {
				ProcessoDTO process = new ProcessoDTO();
				process.setAutor(WordUtils.capitalizeFully(rs.getString("Autor")));
				if(rs.wasNull()) {
					process.setAutor("");
					}
				process.setCentroDeCusto(rs.getString("centro"));
				if(rs.wasNull()) {
					process.setCentroDeCusto("");
					}
				process.setCentroDeCustoSit(rs.getString("centroSit"));
				if(rs.wasNull()) {
					process.setCentroDeCustoSit("");
					}
				process.setCentroDeCustoManual(rs.getString("CCU"));
				if(rs.wasNull()) {
					process.setCentroDeCustoManual("");
					}
				process.setCentroDeCustoManualSit(rs.getString("CCUSit"));
				if(rs.wasNull()) {
					process.setCentroDeCustoManualSit("");
					}
				process.setNroProcesso(rs.getString("Processo"));
				if(rs.wasNull()) {
					process.setNroProcesso("");
					}				
				process.setDataCitacao(rs.getDate("datCit") != null ? df.format(rs.getDate("datCit")) : "");
				process.setDataFinalizacao(df.format(rs.getDate("datGer")));
				if(rs.wasNull()) {
					process.setDataFinalizacao("");
					}
				process.setMesesDiff(rs.getString("meses") != null ? rs.getString("meses") : "");
				process.setEscritorio(rs.getString("escritorio"));
				if(rs.wasNull()) {
					process.setEscritorio("");
					}
				process.setNroTarefa(rs.getString("tarefa"));
				if(rs.wasNull()) {
					process.setNroTarefa("");
					}

				retorno.add(process);

			}
			return retorno;
		} catch (Exception e) {
			return null;
		} finally {
			ConexaoUtils.closeConnection(conn, pstm, rs);
		}

	}
	@Override
	public List<ProcessoDTO> listaTudoFinalizado(String add) throws Exception {
		List<ProcessoDTO> retorno = new ArrayList<ProcessoDTO>();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		sql.append("select tabela1.centro, ");
		sql.append("       case when (tabela2.acerat = 'S') then 'A' else 'I' end as centroSit, ");
		sql.append("       tabela1.CCU, ");
		sql.append("       tabela1.CCUSit, ");
		sql.append("       tabela1.Autor, ");
		sql.append("       tabela1.Processo, ");
		sql.append("       tabela1.datCit, ");
		sql.append("       tabela1.datFil, ");
		sql.append("       tabela1.meses, ");
		sql.append("       tabela1.escritorio, ");
		sql.append("       tabela1.tarefa ");
		sql.append("  from (select coalesce(v034.codccu,'') as centro, ");
		sql.append("                    j002.codccuInformado as CCU, ");
		sql.append("		            case when ((j002.codccuInformado is not null) and (j002.codccuInformado <> v034.codccu)) ");
		sql.append("		                 then 'A' ");
		sql.append("		                 else '' end as CCUSit, ");
		sql.append("		            case j002.isColaborador when 1 then v034.nomfun else autor.nomAut end as Autor, ");
		sql.append("					j002.numPro as Processo, ");
		sql.append("					j002.dataCitacao as 'datCit', ");
		sql.append("					wf.finishDate as 'datFil', ");
		sql.append("					esc.nome as escritorio, ");
		sql.append("					cast(DATEDIFF(month, j002.dataCitacao, wf.finishDate) as nvarchar)+' meses' as meses, ");
		sql.append("					wf.code as tarefa ");
		sql.append("			   from D_j002Principal j002 with(nolock) ");
		sql.append("		 inner join WFProcess wf with(nolock) on wf.neoId = j002.wfprocess_neoId ");
		sql.append("		  left join D_j002EscAdvPro esc on j002.escRecPro_neoId = esc.neoId ");
		sql.append("		  left join D_j002Autor autor with(nolock) on autor.neoId = j002.autorNaoCol_neoId ");
		sql.append("		  left join X_VETORHUSUFUNFUSION fun with(nolock) on fun.neoId = j002.colaborador_neoId ");
		sql.append("		  left join [FSOODB04\\sql02].vetorh.dbo.USU_V034FUSION v034 with(nolock) on v034.pk = fun.pk ");
		sql.append("		 	  where wf.processState = 1 ");
		sql.append("		 	    and numPro is not null ");
		sql.append(add);
		sql.append("		 	    and numPro <> 'TESTE DA TI' ");
		sql.append("		 	    and numPro not in ('456456456456','9999999999') ");
		sql.append("		) as tabela1 ");
		sql.append("  left join (select distinct codccu,acerat,claccu,codemp ");
		sql.append("               from [FSOODB04\\SQL02].Sapiens.dbo.E044CCU b) as tabela2 ");
		sql.append("         on tabela2.codccu = tabela1.centro ");
		sql.append("        and tabela2.codemp = SUBSTRING(tabela2.claccu,3,2) ");
		
		
		sql.append("   order by 2 desc, case when (tabela1.centro = '') then 1 end desc, 5");
		
		//[cachoeira\\sql01].vetorhTST.dbo.USU_V034FUSION   <-- Homologação  
		//[FSOODB04\\sql02].vetorh.dbo.USU_V034FUSION       <-- Produção
		try {
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = PersistEngine.getConnection("");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next()) {
				ProcessoDTO process = new ProcessoDTO();
				process.setAutor(WordUtils.capitalizeFully(rs.getString("Autor")));
				if(rs.wasNull()) {
					process.setAutor("");
					}
				process.setCentroDeCusto(rs.getString("centro"));
				if(rs.wasNull()) {
					process.setCentroDeCusto("");
					}
				process.setCentroDeCustoSit(rs.getString("centroSit"));
				if(rs.wasNull()) {
					process.setCentroDeCustoSit("");
					}
				process.setCentroDeCustoManual(rs.getString("CCU"));
				if(rs.wasNull()) {
					process.setCentroDeCustoManual("");
					}
				process.setCentroDeCustoManualSit(rs.getString("CCUSit"));
				if(rs.wasNull()) {
					process.setCentroDeCustoManualSit("");
					}
				process.setNroProcesso(rs.getString("Processo"));
				if(rs.wasNull()) {
					process.setNroProcesso("");
					}
				process.setEscritorio(rs.getString("escritorio"));
				if(rs.wasNull()) {
					process.setEscritorio("");
					}
				process.setDataCitacao(rs.getDate("datCit") != null ? df.format(rs.getDate("datCit")) : "");
				process.setDataFinalizacao(df.format(rs.getDate("datFil")));
				if(rs.wasNull()) {
					process.setDataFinalizacao("");
					}
				process.setMesesDiff(rs.getString("meses") != null ? rs.getString("meses") : "");

				process.setNroTarefa(rs.getString("tarefa"));
				if(rs.wasNull()) {
					process.setNroTarefa("");
					}

				retorno.add(process);

			}
			return retorno;
		} catch (Exception e) {
			return null;
		} finally {
			ConexaoUtils.closeConnection(conn, pstm, rs);
		}
	}
	
	

}
