package com.neomind.fusion.custom.orsegups.rsc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.FornecedorVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;

public class RSCListaContratosConverter extends StringConverter
{
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		Long empresa = null;
		Long filial = null;
		Long numctr = null;
		
		StringBuilder textoTable = new StringBuilder();
		
		Connection conn = null;
		StringBuilder sqlFapPorFornecedor = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		Long idPai = field.getForm().getObjectId();
		NeoObject tarefa = PersistEngine.getNeoObject(idPai);
		EntityWrapper wrapper = new EntityWrapper(tarefa);
		
		NeoObject rscRegistroSolicitacaoCliente = (NeoObject) wrapper.findValue("RscRelatorioSolicitacaoCliente");
		EntityWrapper rscWrapper = new EntityWrapper(rscRegistroSolicitacaoCliente);
		
		/*Utilizados para o SubFluxo de RSC de Cancelamento*/
		Long codcli = (Long) rscWrapper.findValue("clienteSapiens.codcli");
		
		/*Utilizados para o SubFluxo de RSC de Orçamento*/
		if(wrapper.findValue("wfprocess.model.title").equals("C027.002 - RSC - Categoria Orçamento"))
		{		
			empresa = (Long) wrapper.findValue("contratoSapiens.usu_codemp");
			filial = (Long) wrapper.findValue("contratoSapiens.usu_codfil");
			numctr = (Long) wrapper.findValue("contratoSapiens.usu_numctr");
		}
		
		try
		{
			conn = PersistEngine.getConnection("SAPIENS");

			List<FornecedorVO> lstFornecedor = new ArrayList<FornecedorVO>();
			sqlFapPorFornecedor.append(" select ctr.usu_codemp, ctr.usu_codfil, ctr.usu_numctr, CONVERT(VARCHAR(10),ctr.usu_datini,103) as dtFatCtr, ");
			sqlFapPorFornecedor.append(" cvs.usu_numpos, CONVERT(VARCHAR(10),cvs.usu_datini,103) as dtFatPos, cvs.usu_codser, cvs.usu_cplcvs,        ");      
			sqlFapPorFornecedor.append(" cast((cvs.usu_qtdcvs * cvs.usu_preuni) as decimal (15,2)) as vlrUniSer, cvs.usu_endctr + ' - ' +            ");
			sqlFapPorFornecedor.append(" usu_cplctr + ' - ' + usu_baictr + ' - ' + usu_cidctr + ' - ' + usu_ufsctr as endpos, cvs.usu_sitcvs         ");
			sqlFapPorFornecedor.append(" from usu_t160ctr ctr																						 ");
			sqlFapPorFornecedor.append(" inner join usu_t160cvs cvs on ctr.usu_codemp = cvs.usu_codemp and                                           ");
			sqlFapPorFornecedor.append(" 							   ctr.usu_codfil = cvs.usu_codfil and                                           "); 
			sqlFapPorFornecedor.append(" 							   ctr.usu_numctr = cvs.usu_numctr                                               ");
			sqlFapPorFornecedor.append(" where ((ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= GETDATE())) and                "); 
			sqlFapPorFornecedor.append(" ((cvs.usu_sitcvs = 'A') or (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= GETDATE())) and                      ");
			if(empresa != null && filial != null && numctr != null)
			{
				sqlFapPorFornecedor.append(" ctr.usu_codemp = " + empresa + " and 																	 ");
				sqlFapPorFornecedor.append(" ctr.usu_codfil = " + filial + " and                                                                     ");
				sqlFapPorFornecedor.append(" ctr.usu_numctr = " + numctr + "                                                                         ");
			}
			else
			{
				sqlFapPorFornecedor.append(" ctr.usu_codcli = " + codcli                                                                              );
			}			
			sqlFapPorFornecedor.append(" ORDER BY ctr.usu_codemp, ctr.usu_codfil, ctr.usu_numctr, cvs.usu_numpos                                     ");

			pstm = conn.prepareStatement(sqlFapPorFornecedor.toString());
			rs = pstm.executeQuery();
		
			textoTable.append("<br><fieldset class=\"field_group\">");
			textoTable.append("<legend>Lista de Contratos</legend>");
			textoTable.append("<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			textoTable.append("			<tr style=\"cursor: auto\">");
			textoTable.append("				<th style=\"cursor: auto\">Empresa</th>");
			textoTable.append("             <th style=\"cursor: auto\">Filial</th>");
			textoTable.append("             <th style=\"cursor: auto\">Contrato</th>");
			textoTable.append("             <th style=\"cursor: auto\">Ini. Fat. Contrato</th>");
			textoTable.append("             <th style=\"cursor: auto\">Posto</th>");
			textoTable.append("             <th style=\"cursor: auto\">Ini. Fat. Posto</th>");
			textoTable.append("             <th style=\"cursor: auto\">Serviço</th>");
			textoTable.append("             <th style=\"cursor: auto\">Descrição do Serviço</th>");
			textoTable.append("             <th style=\"cursor: auto\">Vlr do Serviço</th>");
			textoTable.append("             <th style=\"cursor: auto\">End. Posto</th>");
			textoTable.append("             <th style=\"cursor: auto\">Ativo/Inativo</th>");
			textoTable.append("			</tr>");
			textoTable.append("			<tbody>");	
			
			while (rs.next())
			{
				textoTable.append("		<tr>");
				textoTable.append("			<td>" + rs.getLong("usu_codemp") + "</td>");
				textoTable.append("			<td>" + rs.getLong("usu_codfil") + "</td>");
				textoTable.append("			<td>" + rs.getLong("usu_numctr") + "</td>");
				textoTable.append("			<td>" + rs.getString("dtFatCtr") + "</td>");
				textoTable.append("			<td>" + rs.getLong("usu_numpos") + "</td>");
				textoTable.append("			<td>" + rs.getString("dtFatPos") + "</td>");
				textoTable.append("			<td>" + rs.getString("usu_codser") + "</td>");
				textoTable.append("			<td>" + rs.getString("usu_cplcvs") + "</td>");
				textoTable.append("			<td>" + rs.getBigDecimal("vlrUniSer") + "</td>");
				textoTable.append("			<td>" + rs.getString("endpos") + "</td>");
				textoTable.append("			<td>" + rs.getString("usu_sitcvs") + "</td>");
				textoTable.append("		</tr>");
			}
				
			textoTable.append("			</tbody>");
			textoTable.append("		</table>");
			textoTable.append("</fieldset>");
			
			return textoTable.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return textoTable.toString();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
