package com.neomind.fusion.custom.orsegups.autocargo.engine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.autocargo.beans.client.ACClient;
import com.neomind.fusion.custom.orsegups.autocargo.beans.client.ACClients;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class ACEngineIntegracao {

    private int totalPageNumber = 9999;

    private List<ACClient> result = new ArrayList<ACClient>();

    public String runIntegration() {
	
	for (int i = 0; i < totalPageNumber; i++) {

	    this.getResult(i);

	    try {
		Thread.sleep(1000);
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    }
	}
	
	this.addToDB(result);

	return "OK";

    }

    private void addToDB(List<ACClient> list) {

	Connection conn = null;
	PreparedStatement pstm = null;


	    
	    try {
		
		String sql = "INSERT INTO CELTECClients (idLegacy, operatorName, name, addressCode, addressCode, addressStreet, addressNumber, addressComplement, addressDistrict, addressCity) "
			+ " VALUES (?,?,?,?,?,?,?,?,?)";
		
		conn = PersistEngine.getConnection("IMPORTACOES");
		
		pstm = conn.prepareStatement(sql);
		
		for (ACClient client : list) {
		    
		    int idLegacy = client.getId();
		    String operatorName = client.getOperator_name();
		    String name = client.getName();
		    String addressCode = client.getAddress_code();
		    String addressStreet = client.getAddress_street();
		    String addressNumber = client.getAddress_number();
		    String addressComplement = client.getAddress_complement();
		    String addressDistrict = client.getAddress_district();
		    String addressCity = client.getAddress_city();
		    
		    pstm.setInt(1, idLegacy);
		    pstm.setString(2, operatorName);
		    pstm.setString(3, name);
		    pstm.setString(4, addressCode);
		    pstm.setString(5, addressStreet);
		    pstm.setString(6, addressNumber);
		    pstm.setString(7, addressComplement);
		    pstm.setString(8, addressDistrict);
		    pstm.setString(9, addressCity);
		    
		    int rs = pstm.executeUpdate();
		    
		    if (rs < 1){
			this.logError("Erro ao inserir cliente: "+idLegacy+" "+operatorName+" "+name+" "+addressCode+" "+addressStreet+" "+addressNumber+" "+addressComplement+" "+addressDistrict+" "+addressCity+" ");
		    }
		    
		    pstm.clearParameters();
		    
		    
		}
		
	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, null);
	    }
	

    }

    private void logError(String error) {

	String fileName = "C:\\Importacao\\erros.txt";

	try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {

	    bw.write(error);

	    // no need to close it.
	    // bw.close();

	    System.out.println("Done");

	} catch (IOException e) {

	    e.printStackTrace();

	}

    }

    private void getResult(int pageNumber) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://www2.autocargo.com.br/api/integration/v1/clients?page=" + pageNumber);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Authorization", "Token token=\"695688a822fd6ff8c1f5244be889b16e259c3ed604a9bdb7e937937ce7f612df\"");
	    conn.setRequestProperty("Content-Type", "application/json");
	    conn.setRequestProperty("Accept", "application/json");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
	    }

	    String totalPorPagina = conn.getHeaderField("X-Autocargo-Per-Page");
	    String totalResultados = conn.getHeaderField("X-Autocargo-Total");

	    this.totalPageNumber = Integer.parseInt(totalResultados) / Integer.parseInt(totalPorPagina);

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()),"UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {

	    e.printStackTrace();

	} catch (IOException e) {

	    e.printStackTrace();

	}

	System.out.println(retorno.toString());
	Gson gson = new Gson();
	ACClients resultList = gson.fromJson(retorno.toString(), ACClients.class);

	this.result.addAll(resultList.getClients());

    }

}
