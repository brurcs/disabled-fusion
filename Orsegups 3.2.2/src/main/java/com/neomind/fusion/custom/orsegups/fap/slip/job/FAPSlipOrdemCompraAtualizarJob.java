package com.neomind.fusion.custom.orsegups.fap.slip.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLInFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

import br.com.senior.services.fap.G5SeniorServicesLocator;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4In;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out;
import br.com.senior.services.fap.OrdemcomprabuscarPendentes4OutOrdemCompra;
import br.com.senior.services.vetorh.constants.WSConfigs;

public class FAPSlipOrdemCompraAtualizarJob implements CustomJobAdapter
{
	Boolean printLog = Boolean.valueOf(false);

	public void execute(CustomJobContext arg0)
	{
		NeoObject noParametrizacao = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipParametrizacao"), new QLEqualsFilter("parametro", "debug"));
		EntityWrapper wParametrizacao = null;
		if (noParametrizacao != null)
		{
			wParametrizacao = new EntityWrapper(noParametrizacao);
			String parametroDebug = wParametrizacao.findGenericValue("valor");
			if (parametroDebug.equals("1"))
				printLog = true;
			else
				printLog = false;
		}
		
		printLog("FAPSlipOrdemCompraAtualizarJob Iniciado");
		
		cancelarOrdensAprovadas();
		
		WSConfigs config = new WSConfigs();

		OrdemcomprabuscarPendentes4In buscaPendentesIn = new OrdemcomprabuscarPendentes4In();
		buscaPendentesIn.setCodUsu(Integer.valueOf(50));

		G5SeniorServicesLocator locator = new G5SeniorServicesLocator();
		try
		{
			OrdemcomprabuscarPendentes4Out buscaPendentesOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().buscarPendentes_4(config.getUserService(), config.getPasswordService(), 0, buscaPendentesIn);

			if ((buscaPendentesOut.getErroExecucao() == null) || (buscaPendentesOut.getErroExecucao().trim().length() == 0))
			{
				OrdemcomprabuscarPendentes4OutOrdemCompra[] solicitacoes = buscaPendentesOut.getOrdemCompra();
				for (OrdemcomprabuscarPendentes4OutOrdemCompra solicitacao : solicitacoes)
				{
					if ((solicitacao.getCodigoEmpresa().equals(1)) || (solicitacao.getCodigoEmpresa().equals(30)))
					{
						Long codOrdemCompra = solicitacao.getNumero().longValue();
						Long codEmpresa = solicitacao.getCodigoEmpresa().longValue();

						QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("ordemCompra.ordemCompra", codOrdemCompra),new  QLEqualsFilter("ordemCompra.empresa.codemp", codEmpresa));
						List<NeoObject> listOrdensCompraCadastradas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup, "neoId desc");

						printLog("FAPSlipOrdemCompraAtualizarJob: Atualizando OC: " + codOrdemCompra);

						WFProcess process = null;
						NeoObject noOrdemCompra = null;

						if (!listOrdensCompraCadastradas.isEmpty())
						{
							for (NeoObject noOrdemCompraCadastrada : listOrdensCompraCadastradas)
							{
								noOrdemCompra = (NeoObject) new EntityWrapper(noOrdemCompraCadastrada).findGenericValue("ordemCompra");

								process = (WFProcess) new EntityWrapper(noOrdemCompraCadastrada).findGenericValue("wfprocess");
								if ((process != null) && (process.isOpen()))
								{
									QLGroupFilter qlGroupOC = new QLGroupFilter("AND", new QLEqualsFilter("numocp", codOrdemCompra), new QLEqualsFilter("codemp", codEmpresa));
									NeoObject noOrdemCompraSapiens = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE420OCP"), qlGroupOC);
									if (noOrdemCompraSapiens == null)
									{
										break;
									}

								}

							}

							preencherOrdemCompra(codOrdemCompra, noOrdemCompra, codEmpresa);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void cancelarOrdensAprovadas()
	{
		List<NeoObject> listOrdens = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), new QLFilterIsNotNull("ordemCompra"));
		for (NeoObject noFAPSlip : listOrdens)
		{
			EntityWrapper wFAPSlip = new EntityWrapper(noFAPSlip);

			WFProcess process = (WFProcess) wFAPSlip.findGenericValue("wfprocess");

			Long codOrdemCompra = (Long) wFAPSlip.findGenericValue("ordemCompra.ordemCompra");
			Long codEmp = (Long) wFAPSlip.findGenericValue("empresa.codemp");
			QLGroupFilter qlGroupOC = new QLGroupFilter("AND", new QLEqualsFilter("numocp", codOrdemCompra), new QLEqualsFilter("codemp", codEmp));
			NeoObject noOrdemCompraSapiens = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE420OCP"), qlGroupOC);
			if (noOrdemCompraSapiens == null)
			{
				if ((process != null) && (process.isOpen()))
				{
					RuntimeEngine.getProcessService().cancel(process, "Ordem de Compra não existe no SAPIENS");
				}
			}
			else
			{
				EntityWrapper wOrdemCompraSapiens = new EntityWrapper(noOrdemCompraSapiens);

				String sitApr = (String) wOrdemCompraSapiens.findGenericValue("sitapr");
				if ((sitApr != null) && (sitApr.equals("APR")))
				{
					if ((process != null) && (process.isOpen()))
					{
						RuntimeEngine.getProcessService().cancel(process, "Ordem de Compra já aprovada no SAPIENS");
					}
				}
			}
		}
	}

	public NeoObject preencherOrdemCompra(final Long codOrdemCompra, NeoObject noOrdemCompra, final Long codEmp, Boolean refresh)
	{
		try
		{
			QLGroupFilter qlGroupOC = new QLGroupFilter("AND", new QLEqualsFilter("numocp", codOrdemCompra), new QLEqualsFilter("codemp", codEmp));
			NeoObject noOrdemCompraSapiens = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE420OCP"), qlGroupOC);
			if (noOrdemCompraSapiens == null)
			{
				return null;
			}
			
			EntityWrapper wOrdemCompraSapiens = new EntityWrapper(noOrdemCompraSapiens);
			Long codFornecedor = (Long) wOrdemCompraSapiens.findGenericValue("codfor");
			String obs = (String) wOrdemCompraSapiens.findGenericValue("obsocp");
			String transacaoOrdemCompra = wOrdemCompraSapiens.findGenericValue("tnspro").equals("90400") ? "Manual" : "Automatico";
			GregorianCalendar dataOC = (GregorianCalendar) wOrdemCompraSapiens.findGenericValue("datger");

			NeoObject noFornecedor = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("ETABFOR"), new QLEqualsFilter("codfor", codFornecedor));
			NeoObject noEmpresa = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("EEMP"), new QLEqualsFilter("codemp", codEmp));

			if (noOrdemCompra == null)
				noOrdemCompra = AdapterUtils.createNewEntityInstance("FAPSlipOrdemCompra");
			
			EntityWrapper wSolicitacaoCompra = new EntityWrapper(noOrdemCompra);

			String solicitanteOC = (String) wSolicitacaoCompra.findGenericValue("solicitante");
			if (solicitanteOC == null)
			{
				Long usuGer = (Long) wOrdemCompraSapiens.findGenericValue("usuger");
				NeoObject noSolicitante = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSUSUARIOS"), new QLEqualsFilter("codent", usuGer));
				if (noSolicitante != null)
				{
					solicitanteOC = (String) new EntityWrapper(noSolicitante).findGenericValue("nomcom");
					wSolicitacaoCompra.setValue("solicitante", solicitanteOC);
				}
			}

			wSolicitacaoCompra.setValue("ordemCompra", codOrdemCompra);
			wSolicitacaoCompra.setValue("fornecedor", noFornecedor);
			wSolicitacaoCompra.setValue("empresa", noEmpresa);
			wSolicitacaoCompra.setValue("obs", obs.length() >= 2000 ? obs.substring(0, 1999) : obs);
			wSolicitacaoCompra.setValue("dataOC", NeoDateUtils.safeDateFormat(dataOC, "dd/MM/yyyy"));
			wSolicitacaoCompra.setValue("tipoOC", transacaoOrdemCompra);

			List<NeoObject> listItensCadastrar = new ArrayList();
			List<NeoObject> listItensCadastrados = (List) wSolicitacaoCompra.findGenericValue("itensCompra");

			QLGroupFilter qlGroupItens = new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmp), new QLEqualsFilter("numocp", codOrdemCompra));
			List<NeoObject> listItensCompra = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE420IPO"), qlGroupItens);
			for (NeoObject noItemCompra : listItensCompra)
			{
				EntityWrapper wItemCompra = new EntityWrapper(noItemCompra);
				final String codProduto = (String) wItemCompra.findGenericValue("codpro");
				final String codDer = (String) wItemCompra.findGenericValue("codder");
				String transacao = "";
				String codCCU = (String) wItemCompra.findGenericValue("codccu");
				Long seqIpo = (Long) wItemCompra.findGenericValue("seqipo");

				String cliNom = "";
				String lotNom = "";
				String descCCU = "";

				QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("codccu", codCCU), new QLEqualsFilter("codemp", codEmp));
				NeoObject noCCU = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), qlGroup);
				if (noCCU != null)
				{
					EntityWrapper wCCU = new EntityWrapper(noCCU);
					cliNom = (String) wCCU.findGenericValue("usu_clinom");
					lotNom = (String) wCCU.findGenericValue("usu_lotnom");
					descCCU = (String) wCCU.findGenericValue("desccu");
				}

				String nrSolicitacao = "";
				String responsavelAberturaSolicitacao = "";
				String responsavelAprovSolicitacao = "";
				String obsSolicitacao = "";
				String aberturaSolicitacao = "";
				String aprovacaoSolicitacao = "";
				String nrRequisicao = "";
				String responsavelAberturaRequisicao = "";
				String responsavelAprovRequisicao = "";
				String obsRequisicao = "";
				String aberturaRequisicao = "";
				String aprovacaoRequisicao = "";

				String motivo = "";

				QLGroupFilter qlGroupLCO = new QLGroupFilter("AND", new QLEqualsFilter("numocp", codOrdemCompra), new QLEqualsFilter("codemp", codEmp), new QLEqualsFilter("seqipo", seqIpo));
				NeoObject noLigacaoOrdemCompra = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE410LCO"), qlGroupLCO);
				QLGroupFilter qlGroupSC;
				if (noLigacaoOrdemCompra != null)
				{
					EntityWrapper wLigacaoOrdemCompra = new EntityWrapper(noLigacaoOrdemCompra);

					Long numCot = (Long) wLigacaoOrdemCompra.findGenericValue("numcot");
					qlGroupSC = new QLGroupFilter("AND", new QLEqualsFilter("numcot", numCot), new QLEqualsFilter("codemp", codEmp));
					NeoObject noSolicitacaoCompraExt = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSSOLICITACAOCOMPRAEPI"), qlGroupSC);
					if (noSolicitacaoCompraExt != null)
					{
						EntityWrapper wSolicitacaoCompraExt = new EntityWrapper(noSolicitacaoCompraExt);

						motivo = (String) wSolicitacaoCompraExt.findGenericValue("obssol");
						transacao = transacao.equals("91402") ? "Automatico (RMC)" : wSolicitacaoCompraExt.findGenericValue("codtns").equals("91404") ? "Automatico via Requisição" : "Manual";
						aberturaSolicitacao = NeoDateUtils.safeDateFormat((GregorianCalendar) wSolicitacaoCompraExt.findGenericValue("datsol"), "dd/MM/yyyy");
						aprovacaoSolicitacao = NeoDateUtils.safeDateFormat((GregorianCalendar) wSolicitacaoCompraExt.findGenericValue("datapr"), "dd/MM/yyyy");
						obsSolicitacao = (String) wSolicitacaoCompraExt.findGenericValue("obssol");
						responsavelAberturaSolicitacao = wSolicitacaoCompraExt.findGenericValue("codusu").toString();
						nrSolicitacao = wSolicitacaoCompraExt.findGenericValue("numsol").toString();

						Long numApr = (Long) wSolicitacaoCompraExt.findGenericValue("numapr");
						Long rotNap = (Long) wSolicitacaoCompraExt.findGenericValue("rotnap");
						QLGroupFilter qlGroupAprovadorSolicitacao = new QLGroupFilter("AND", new QLEqualsFilter("numapr", numApr), new QLEqualsFilter("rotnap", rotNap));
						NeoObject noE614USU = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSE614USU"), qlGroupAprovadorSolicitacao);
						if (noE614USU != null)
						{
							EntityWrapper wE614USU = new EntityWrapper(noE614USU);
							responsavelAprovSolicitacao = wE614USU.findGenericValue("usuapr") == null ? "" : wE614USU.findGenericValue("usuapr").toString();
							if ((responsavelAprovSolicitacao != null) && (!responsavelAprovSolicitacao.trim().isEmpty()))
							{
								NeoObject noRespAprovSolicitacao = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSUSUARIOS"), new QLEqualsFilter("codent", new Long(responsavelAprovSolicitacao)));
								if (noRespAprovSolicitacao != null)
								{
									responsavelAprovSolicitacao = (String) new EntityWrapper(noRespAprovSolicitacao).findGenericValue("nomcom");
									responsavelAprovSolicitacao = responsavelAprovSolicitacao + " Depto: " + new EntityWrapper(noRespAprovSolicitacao) .findGenericValue("desusu");
								}
							}
						}

						NeoObject noRespSolicitante = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSUSUARIOS"), new QLEqualsFilter("codent", new Long(responsavelAberturaSolicitacao)));
						if (noRespSolicitante != null)
						{
							responsavelAberturaSolicitacao = (String) new EntityWrapper(noRespSolicitante).findGenericValue("nomcom");
							responsavelAberturaSolicitacao = responsavelAberturaSolicitacao + " Depto: " + new EntityWrapper(noRespSolicitante).findGenericValue("desusu");
						}

						Long numEme = (Long) wSolicitacaoCompraExt.findGenericValue("numeme");
						Long seqEme = (Long) wSolicitacaoCompraExt.findGenericValue("seqeme");
						QLGroupFilter qlGroupEME = new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmp), new QLEqualsFilter("numeme", numEme), new QLEqualsFilter("seqeme", seqEme));
						NeoObject noEME = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSLANCAMENTOSEPI"), qlGroupEME);
						if (noEME != null)
						{
							EntityWrapper wEME = new EntityWrapper(noEME);
							motivo = (String) wEME.findGenericValue("obseme");
							aberturaRequisicao = NeoDateUtils.safeDateFormat((GregorianCalendar) wEME.findGenericValue("datger"), "dd/MM/yyyy");
							aprovacaoRequisicao = NeoDateUtils.safeDateFormat((GregorianCalendar) wEME.findGenericValue("datapr"), "dd/MM/yyyy");
							obsRequisicao = motivo;

							responsavelAberturaRequisicao = wEME.findGenericValue("ususol").toString();
							responsavelAprovRequisicao = wEME.findGenericValue("usuapr").toString();
							nrRequisicao = numEme.toString();

							NeoObject noRespRequisicao = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSUSUARIOS"), new QLEqualsFilter("codent", new Long(responsavelAberturaRequisicao)));
							if (noRespRequisicao != null)
							{
								responsavelAberturaRequisicao = (String) new EntityWrapper(noRespRequisicao).findGenericValue("nomcom");
								responsavelAberturaRequisicao = responsavelAberturaRequisicao + " Depto: " + new EntityWrapper(noRespRequisicao).findGenericValue("desusu");
							}

							NeoObject noAprovRequisicao = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSUSUARIOS"), new QLEqualsFilter("codent", new Long(responsavelAprovRequisicao)));
							if (noAprovRequisicao != null)
							{
								responsavelAprovRequisicao = (String) new EntityWrapper(noAprovRequisicao).findGenericValue("nomcom");
								responsavelAprovRequisicao = responsavelAprovRequisicao + " Depto: " + new EntityWrapper(noAprovRequisicao).findGenericValue("desusu");
							}
						}
					}
				}

				NeoObject noItem = null;
				for (NeoObject noItemCadastrado : listItensCadastrados)
				{
					EntityWrapper wItemCadastrado = new EntityWrapper(noItemCadastrado);
					String codProdutoCadastrado = (String) wItemCadastrado.findGenericValue("codProduto");
					Long seqIpoCadastrado = (Long) wItemCadastrado.findGenericValue("seqipo");

					if ((codProdutoCadastrado.equals(codProduto)) && (seqIpoCadastrado.equals(seqIpo)))
					{
						noItem = noItemCadastrado;
						break;
					}
				}
				
				if (noItem == null)
					noItem = AdapterUtils.createNewEntityInstance("FAPSlipItensCompra");
				EntityWrapper wItem = new EntityWrapper(noItem);

				wItem.setValue("tipoOC", transacao);
				wItem.setValue("codProduto", codProduto);
				wItem.setValue("codder", codDer);
				wItem.setValue("codemp", codEmp);
				wItem.setValue("codccu", codCCU);
				wItem.setValue("seqipo", seqIpo);
				wItem.setValue("descricao", wItemCompra.findGenericValue("cplipo"));
				wItem.setValue("quantidade", ((BigDecimal) wItemCompra.findGenericValue("qtdped")).longValue());
				wItem.setValue("valorUnitario", wItemCompra.findGenericValue("preuni"));
				wItem.setValue("valorTotal", wItemCompra.findGenericValue("vlrbru"));
				wItem.setValue("dataEntrega", wItemCompra.findGenericValue("datent"));
				wItem.setValue("centroCusto", cliNom + " " + " > " + " " + lotNom + " " + " > " + " " + codCCU + " " + descCCU);
				wItem.setValue("motivo", motivo);
				wItem.setValue("nrRequisicao", nrRequisicao);
				wItem.setValue("responsavelSolicitacaoRequisicao", responsavelAberturaRequisicao);
				wItem.setValue("responsavelAprovacaoRequisicao", responsavelAprovRequisicao);
				wItem.setValue("obsRequisicao", obsRequisicao);
				wItem.setValue("abertRequisicao", aberturaRequisicao);
				wItem.setValue("aprovRequisicao", aprovacaoRequisicao);
				wItem.setValue("nrSolicitacao", nrSolicitacao);
				wItem.setValue("responsavelSolicitacao", responsavelAberturaSolicitacao);
				wItem.setValue("responsavelAprovacaoSolicitacao", responsavelAprovSolicitacao);
				wItem.setValue("obsSolicitacao", obsSolicitacao);
				wItem.setValue("abertSolicitacao", aberturaSolicitacao);
				wItem.setValue("aprovSolicitacao", aprovacaoSolicitacao);

				if ((refresh == null) || (!refresh.booleanValue()))
				{
					NeoObject noHistoricoItem = AdapterUtils.createNewEntityInstance("FAPSlipHistoricoItem");
					EntityWrapper wHistoricoItem = new EntityWrapper(noHistoricoItem);

					wHistoricoItem.setValue("codProduto", codProduto);
					wHistoricoItem.setValue("codOrdemCompra", codOrdemCompra);
					wHistoricoItem.setValue("codEmpresa", codEmp);
					wHistoricoItem.setValue("codDeri", codDer);
					wHistoricoItem.setValue("historicoItem", getHistoricoItem(codProduto, codOrdemCompra, codEmp));

					wItem.setValue("historicoItem", noHistoricoItem);
				}
				
				PersistEngine.persist(noItem);
				
				final Long idItem = noItem.getNeoId();
				
				NeoRunnable newEntityManager = new NeoRunnable()
				{
					@Override
					public void run() throws Exception
					{
						try
						{
							printLog("FAPSlipOrdemCompraAtualizarJob: Atualizando Estoque Produto: " + codProduto);
							Map<String, Long> mapDadosEstoque = getEstoqueItem(codProduto, codEmp, codDer);
							
							NeoObject noItemAtt = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPSlipItensCompra"), idItem);
							EntityWrapper wItemAtt = new EntityWrapper(noItemAtt);
							
							wItemAtt.setValue("regionais", mapDadosEstoque.get("regionais"));
							wItemAtt.setValue("cdo", mapDadosEstoque.get("CDO"));
							wItemAtt.setValue("lab01", mapDadosEstoque.get("LAB01"));
							wItemAtt.setValue("geral", mapDadosEstoque.get("geral"));
							wItemAtt.setValue("estoqueMin", mapDadosEstoque.get("estoqueMinimo"));
							wItemAtt.setValue("estoqueMax", mapDadosEstoque.get("estoqueMaximo"));
							wItemAtt.setValue("itensReservados", mapDadosEstoque.get("itensReservados"));
							
							PersistEngine.persist(noItemAtt);
							System.out.println(NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss") + " - " + "FAPSlipOrdemCompraAtualizarJob: Atualizando Estoque OC: Produto " + codProduto + " da OC " + codOrdemCompra + " atualizado com sucesso");
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				};

				try
				{
					PersistEngine.backgroundManagedRun(newEntityManager);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}

				listItensCadastrar.add(noItem);
			}

			wSolicitacaoCompra.setValue("itensCompra", listItensCadastrar);

			PersistEngine.persist(noOrdemCompra);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}

		return noOrdemCompra;
	}

	private List<NeoObject> getHistoricoItem(String codProduto, Long codOrdemCompra, Long codEmp)
	{
		List<NeoObject> listHistoricoItens = new ArrayList();

		QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("codemp", codEmp), new QLEqualsFilter("codpro", codProduto), new QLRawFilter("numocp <> " + codOrdemCompra));
		List<NeoObject> listItensCompra = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE420IPO"), qlGroup, 0, 3, "numocp desc");
		for (NeoObject noItemCompra : listItensCompra)
		{
			EntityWrapper wItemCompra = new EntityWrapper(noItemCompra);

			NeoObject noHistoricoItem = AdapterUtils.createNewEntityInstance("FAPSlipItensCompra");
			EntityWrapper wHistoricoItem = new EntityWrapper(noHistoricoItem);

			wHistoricoItem.setValue("ordemCompra", wItemCompra.findGenericValue("numocp"));
			wHistoricoItem.setValue("codemp", codEmp);
			wHistoricoItem.setValue("codProduto", codProduto);
			wHistoricoItem.setValue("codder", wItemCompra.findGenericValue("codder"));
			wHistoricoItem.setValue("quantidade", Long.valueOf(((BigDecimal) wItemCompra.findGenericValue("qtdped")).longValue()));
			wHistoricoItem.setValue("valorUnitario", wItemCompra.findGenericValue("preuni"));
			wHistoricoItem.setValue("valorTotal", wItemCompra.findGenericValue("vlrbru"));
			wHistoricoItem.setValue("dataEntrega", wItemCompra.findGenericValue("datent"));
			wHistoricoItem.setValue("dataCompra", wItemCompra.findGenericValue("datger"));

			PersistEngine.persist(noHistoricoItem);

			listHistoricoItens.add(noHistoricoItem);
		}

		return listHistoricoItens;
	}

	private Map<String, Long> getEstoqueItem(String codProduto, Long codEmp, String codDer)
	{
		Map<String, Long> dadosEstoque = new HashMap();

		QLGroupFilter qlGroupFilter = new QLGroupFilter("AND");
		qlGroupFilter.addFilter(new QLEqualsFilter("codpro", codProduto));
		qlGroupFilter.addFilter(new QLEqualsFilter("codemp", codEmp));
		if ((codDer != null) && (!codDer.trim().isEmpty()))
		{
			qlGroupFilter.addFilter(new QLEqualsFilter("codder", codDer.trim()));
		}

		BigDecimal totalEstoqueMin = new BigDecimal(0);
		BigDecimal totalEstoqueMaximo = new BigDecimal(0);
		BigDecimal totalItensReservados = new BigDecimal(0);
		List<NeoObject> listEstoqueMaximo = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE210EST"), qlGroupFilter);
		BigDecimal qtdRes;
		for (NeoObject noEstoqueItem : listEstoqueMaximo)
		{
			EntityWrapper wEstoqueItem = new EntityWrapper(noEstoqueItem);

			BigDecimal estMin = (BigDecimal) wEstoqueItem.findGenericValue("estmin");
			totalEstoqueMin = totalEstoqueMin.add(estMin != null ? estMin : new BigDecimal(0));

			BigDecimal estMax = (BigDecimal) wEstoqueItem.findGenericValue("estmax");
			totalEstoqueMaximo = totalEstoqueMaximo.add(estMax != null ? estMax : new BigDecimal(0));

			qtdRes = (BigDecimal) wEstoqueItem.findGenericValue("qtdres");
			totalItensReservados = totalItensReservados.add(qtdRes != null ? qtdRes : new BigDecimal(0));
		}

		List<String> listDer = new ArrayList();
		listDer.add("BNU01");
		listDer.add("CCO01");
		listDer.add("CSC01");
		listDer.add("CTA01");
		listDer.add("CUA01");
		listDer.add("IAI01");
		listDer.add("JGS01");
		listDer.add("JLE01");
		listDer.add("LGS01");
		listDer.add("ROP01");
		listDer.add("RSF01");
		listDer.add("RSL01");
		listDer.add("SOO01");
		listDer.add("TRO01");
		listDer.add("BQE01");
		listDer.add("NHO01");
		listDer.add("TRI01");
		listDer.add("GNA01");
		listDer.add("PMJ01");
		listDer.add("SRR01");
		listDer.add("XLN01");
		listDer.add("CAS01");
		QLGroupFilter qlGroupRegionais = new QLGroupFilter("AND");
		qlGroupRegionais.addFilter(qlGroupFilter);
		qlGroupRegionais.addFilter(new QLInFilter("_vo.__neoPK.coddep", listDer));

		BigDecimal totalRegionais = new BigDecimal(0);
		List<NeoObject> listEstoqueRegionais = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE210EST"), qlGroupRegionais);
		for (NeoObject noEstoqueItem : listEstoqueRegionais)
		{
			EntityWrapper wEstoqueItem = new EntityWrapper(noEstoqueItem);

			BigDecimal qtdEst = (BigDecimal) wEstoqueItem.findGenericValue("qtdest");

			totalRegionais = totalRegionais.add(qtdEst != null ? qtdEst : new BigDecimal(0));
		}
		dadosEstoque.put("regionais", Long.valueOf(totalRegionais.longValue()));

		QLGroupFilter qlGroupCDO = new QLGroupFilter("AND");
		qlGroupCDO.addFilter(qlGroupFilter);
		qlGroupCDO.addFilter(new QLEqualsFilter("coddep", "SOO01"));

		BigDecimal totalCDO = new BigDecimal(0);
		List<NeoObject> listEstoqueCDO = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE210EST"), qlGroupCDO);
		for (NeoObject noEstoqueItem : listEstoqueCDO)
		{
			EntityWrapper wEstoqueItem = new EntityWrapper(noEstoqueItem);

			BigDecimal qtdEst = (BigDecimal) wEstoqueItem.findGenericValue("qtdest");

			totalCDO = totalCDO.add(qtdEst != null ? qtdEst : new BigDecimal(0));
		}
		dadosEstoque.put("CDO", Long.valueOf(totalCDO.longValue()));

		QLGroupFilter qlGroupLAB01 = new QLGroupFilter("AND");
		qlGroupLAB01.addFilter(qlGroupFilter);
		qlGroupLAB01.addFilter(new QLEqualsFilter("coddep", "LAB01"));

		BigDecimal totalLAB01 = new BigDecimal(0);
		List<NeoObject> listEstoqueLAB01 = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE210EST"), qlGroupLAB01);
		for (NeoObject noEstoqueItem : listEstoqueLAB01)
		{
			EntityWrapper wEstoqueItem = new EntityWrapper(noEstoqueItem);

			BigDecimal qtdEst = (BigDecimal) wEstoqueItem.findGenericValue("qtdest");

			totalLAB01 = totalLAB01.add(qtdEst != null ? qtdEst : new BigDecimal(0));
		}
		dadosEstoque.put("LAB01", Long.valueOf(totalLAB01.longValue()));
		dadosEstoque.put("geral", Long.valueOf(totalRegionais.longValue() + totalCDO.longValue() + totalLAB01.longValue()));

		dadosEstoque.put("itensReservados", Long.valueOf(totalItensReservados.longValue()));
		dadosEstoque.put("estoqueMaximo", Long.valueOf(totalEstoqueMaximo.longValue()));
		dadosEstoque.put("estoqueMinimo", Long.valueOf(totalEstoqueMin.longValue()));

		return dadosEstoque;
	}

	private NeoObject preencherOrdemCompra(Long codOrdemCompra, NeoObject noOrdemCompra, Long codEmp)
	{
		return preencherOrdemCompra(codOrdemCompra, noOrdemCompra, codEmp, null);
	}

	private void printLog(String log)
	{
		if (printLog)
			System.out.println(NeoDateUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss") + " - " + log);
	}
}