package com.neomind.fusion.custom.orsegups.contract;

import static us.monoid.web.Resty.data;
import static us.monoid.web.Resty.form;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.jfree.util.Log;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import us.monoid.web.mime.MultipartContent;

/**
 * 
 * @author fernando.rebelo
 * 
 *         Classe responsável por fazer as chamadas ao WebService do SIGMA
 */
public class ContratoContasSIGMAWebService
{
	private static String WEB_SERVICE_SIGMA = "http://192.168.20.218:8080/SigmaWebServices/";

	/**
	 * @param map - Mapa contendo <"nomeCampoWebService", "valor">
	 * @return sucesso ou exception
	 * 
	 *         Metodo que chama o webservice para atualizar uma conta, usando o ID da conta
	 */
	public String atualizaConta(Map<String, String> map)
	{
		String param = montaParametroString(map);
		map.put("controlPartition", "1"); 
		String URL = (WEB_SERVICE_SIGMA + "ClientUpdate?" + param);
		System.out.println("[FLUXO CONTRATOS]-" + WEB_SERVICE_SIGMA + "ClientUpdate?" + param);
		try
		{
			Resty r = new Resty();
			JSONResource response = r.json(URL);
			System.out.println("[FLUXO CONTRATOS]-Resposta atualizaConta sigma: " + response.get("status") );
			if (response.get("status").equals("success"))
			{
				//sucesso
				return "ok";
			}
			else
			{
				String excessao = tratarInconsistencia(response, "ClientUpdate", param);
				return excessao;
			}

		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("ERRO - " + e.getMessage());
		}
	}

	/**
	 * 
	 * @param map - Mapa contendo <"nomeCampoWebService", "valor">
	 * @return id da conta do sigma
	 */
	public String insereNovaConta(Map<String, String> map)
	{
		String param = montaParametroString(map);
		System.out.println("[FLUXO CONTRATOS]-"+WEB_SERVICE_SIGMA + "ClientInsert?" + param);

		String URL = (WEB_SERVICE_SIGMA + "ClientInsert?" + param);
		try
		{
			Resty r = new Resty();
			JSONResource response = r.json(URL);
			if (response.get("status").equals("success"))
			{
				//sucesso - retorna numero da conta SIGMA
				return NeoUtils.safeOutputString(response.get("entityId"));
			}
			else
			{
				System.out.println("[FLUXO CONTRATOS]- T0");
				String excessao = tratarInconsistencia(response, "ClientInsert", param);
				if (excessao.equals("account_already_exists")){
					//return "account_already_exists";
					System.out.println("[FLUXO CONTRATOS]- T1");
					throw new WorkflowException("A conta do cliente já existe");
				}
				throw new WorkflowException(excessao);
			}

		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.out.println("[FLUXO CONTRATOS]- T2");
			throw new RuntimeException(e);
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			System.out.println("[FLUXO CONTRATOS]- T3");
			throw e;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("[FLUXO CONTRATOS]- T4");
			throw new WorkflowException(e.getMessage());
		}
	}

	public static String abrerturaOSSigma(Map<String, String> map) throws Exception
	{
		String param2 = montaParametroOSString(map);
		try
		{
			String urlParameters = param2;
			String request = WEB_SERVICE_SIGMA + "ReceptorOSWebService";
			System.out.println("[FLUXO CONTRATOS]-"+request + "/"+ urlParameters );
			URL url = new URL(request);
			URLConnection conn = url.openConnection();

			conn.setDoOutput(true);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(urlParameters);
			writer.flush();

			String line;
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			while ((line = reader.readLine()) != null)
			{
				JSONObject json = new JSONObject(line);
				if (json.getString("status").equals("success"))
				{
					return NeoUtils.safeOutputString(json.getString("entityId"));
				}
				else
				{
					String excessao = tratarInconsistencia(json, "ReceptorOSWebService", param2);
					System.out.println("[FLUXO CONTRATOS]-Exceção abertura OS->"+excessao);
				}
			}
			writer.close();
			reader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao abrir OS no SIGMA, favor informar a TI. " + e.getMessage());
		}

		return "";
	}

	/**
	 * 
	 * @param obj - objeto JSONResource que ocasionou o erra na chamada ao webservice
	 * @param funcaoWebService - funcao que foi chamada e que gerou erro
	 * @param param - String contendo os parametros usados na funcao
	 * @return WorkflowException contendo mensagem amigável
	 */

	public static String tratarInconsistencia(JSONResource obj, String funcaoWebService, String param)
	{
		String excessao = "Erro ao realizar uma consulta ao WEBSERVICE do sigma. Mensagem de retorno : ";
		try
		{
			String retorno = NeoUtils.safeOutputString(obj.get("status"));
			excessao += retorno + "<br>";

			JSONObject jsonObject = (JSONObject) obj.get("errors");
			JSONArray array = jsonObject.names();
			if (array.length() > 0)
			{
				excessao += "Motivo(s) do(s) erros(s):<br>";
			}

			for (int i = 0; i < array.length(); i++)
			{
				String nome = NeoUtils.safeOutputString(array.get(i));
				excessao += "Campo " + nome + ". Erro: " + jsonObject.get(nome) + "<br>";

				if (jsonObject.get(nome).equals("account_already_exists"))
				{
					return "account_already_exists";
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			excessao = "Erro ao tratar Inconsistencia - 1";
			Log.error("1 - tratarInconsistencia("+obj+", "+funcaoWebService+", "+param+") -" +e.getMessage());
		}

		return excessao;
	}
	
	public static String tratarInconsistencia(JSONObject obj, String funcaoWebService, String param)
	{
		String excessao = "Erro ao realizar uma consulta ao WEBSERVICE do sigma. Mensagem de retorno : ";
		try
		{
			String retorno = NeoUtils.safeOutputString(obj.get("status"));
			excessao += retorno + "<br>";

			JSONArray array = (JSONArray) obj.get("errors");

			if (array.length() > 0)
			{
				excessao += "Motivo(s) do(s) erros(s):<br>";
			}

			for (int i = 0; i < array.length(); i++)
			{
				JSONObject erro = (JSONObject) array.get(i);
				String mensagemErro = NeoUtils.safeOutputString(erro.getString("stateId"));
				excessao += mensagemErro + "<br>";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			excessao = "Erro ao tratar Inconsistencia - 2";
			Log.error("2 - tratarInconsistencia("+obj+", "+funcaoWebService+", "+param+") -" +e.getMessage());
		}

		return excessao;
	}

	/*public String buscaListaCompania()
	{
		try
		{
			//CompanyList
			JSONObject response = consultaWebService("CompanyList", "");
			if (response.get("status").equals("success"))
			{

			}

		}
		catch (Exception e)
		{

		}
		return "";
	}
	 */
	/*
	public String buscaRamoAtividade()
	{
		//BranchActivityList
		return "";
	}

	public String buscaListaColaboradores(String companyId)
	{
		//CollaboratorList?companyId=?
		return "";
	}

	public String buscaListaGrupos()
	{
		//GroupList
		return "";
	}*/

	/**
	 * 
	 * 
	 * @param companyId - Id da companhia
	 * @param nome - Nome do colaborador no Sigma, sem a filial. Ex. TERC - BARBARA
	 * @param regional - Sigla da Regional
	 * @return id do colaborador
	 * @throws Exception 
	 */
	public String buscaListaInstaladores(String companyId, String nome, String regional) throws Exception
	{
		//InstallerList?companyId=?
		JSONObject result = consultaWebService("InstallerList", "companyId=" + companyId);
		String idRetorno = "";
		try
		{
			if (result.get("status").equals("success"))
			{
				JSONArray array = result.getJSONArray("result");
				for (int i = 0; i < array.length(); i++)
				{
					JSONObject colaborador = (JSONObject) array.get(i);
					String colaboradorSigma = colaborador.getString("name");
					if (colaboradorSigma.endsWith(nome))
					{
						idRetorno = colaborador.getString("id");
						break;
					}else
					{
						String retorno_sem_acento = retiraCaracteresAcentuados(colaboradorSigma);
						String colaborador_sem_acento = retiraCaracteresAcentuados(nome);
						if(retorno_sem_acento.endsWith(colaborador_sem_acento))
						{
							idRetorno = colaborador.getString("id");
							break;
						}
					}
				}
			}
			else
			{

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return idRetorno;
	}

	public String buscaListaResponsaveisTecnicos(String companyId)
	{
		//ResponsibleTechnicianList?companyId=?
		return "";
	}

	/**
	 * Prefira consultar a rota via banco de dados usando o método {@link buscaRota(String rota)} por questões de desempenho do webservice
	 * @param rota - Nome da Rota
	 * @return id da Rota no sigma
	 */
	@Deprecated
	public String buscaListaRotas(String rota)
	{
		//RouteList
		String idRetorno = "";
		try
		{
			JSONObject result = consultaWebService("RouteList", "");
			if (result.get("status").equals("success"))
			{
				JSONArray array = result.getJSONArray("result");
				for (int i = 0; i < array.length(); i++)
				{
					JSONObject colaborador = (JSONObject) array.get(i);
					String colaboradorSigma = colaborador.getString("name");
					if (colaboradorSigma.equalsIgnoreCase(rota))
					{
						idRetorno = colaborador.getString("id");
						break;
					}else
					{
						String retorno_sem_acento = retiraCaracteresAcentuados(colaboradorSigma);
						String colaborador_sem_acento = retiraCaracteresAcentuados(rota);
						if(retorno_sem_acento.equalsIgnoreCase(colaborador_sem_acento))
						{
							idRetorno = colaborador.getString("id");
							break;
						}
					}
				}
			}
			else
			{

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return idRetorno;
	}

	/**
	 * 
	 * @param companyId - id da companhia
	 * @param vendedor - nome do vendedor
	 * @return id do vendedor no sigma
	 */
	public String buscaListaVendedores(String companyId, String vendedor)
	{
		//SellerList?companyId=?
		String idRetorno = "";
		try
		{
			JSONObject result = consultaWebService("SellerList", "companyId=" + companyId);
			if (result.get("status").equals("success"))
			{
				JSONArray array = result.getJSONArray("result");
				for (int i = 0; i < array.length(); i++)
				{
					JSONObject vendedorSigma = (JSONObject) array.get(i);
					String vendedorSigmaString = vendedorSigma.getString("name");
					if (vendedorSigmaString.equalsIgnoreCase(vendedor))
					{
						idRetorno = vendedorSigma.getString("id");
						break;
					}else
					{
						String retorno_sem_acento = retiraCaracteresAcentuados(vendedorSigmaString);
						String vendedor_sem_acento = retiraCaracteresAcentuados(vendedor);
						if(retorno_sem_acento.equalsIgnoreCase(vendedor_sem_acento))
						{
							idRetorno = vendedorSigma.getString("id");
							break;
						}
					}
				}
			}
			else
			{

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return idRetorno;
	}

	/**
	 * 
	 * @param cityId - id da cidade no sigma
	 * @param bairro - nome do bairro
	 * @return id do bairro no sigma
	 */
	public String buscaListaDistritos(String cityId, String bairro)
	{
		//DistrictList?cityId=?
		//bairro = "ANITA GARIBALDI";
		String idRetorno = "";
		try
		{
			JSONObject result = consultaWebService("DistrictList", "cityId=" + cityId);
			if (result.get("status").equals("success"))
			{
				JSONArray array = result.getJSONArray("result");
				for (int i = 0; i < array.length(); i++)
				{
					JSONObject bairroSigma = (JSONObject) array.get(i);
					String bairroSigmaString = bairroSigma.getString("name");
					if (bairroSigmaString.equalsIgnoreCase(bairro))
					{
						idRetorno = bairroSigma.getString("id");
						break;
					}else
					{
						String retorno_sem_acento = retiraCaracteresAcentuados(bairroSigmaString);
						String distrito_sem_acento = retiraCaracteresAcentuados(bairro);
						if(retorno_sem_acento.equalsIgnoreCase(distrito_sem_acento))
						{
							idRetorno = bairroSigma.getString("id");
							break;
						}
					}
				}
			}
			else
			{

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.error("buscaListaDistritos("+cityId+","+bairro+") ",e);
		}

		return idRetorno;
	}

	/**
	 * 
	 * @param stateId - id do estado. Ex. SC, SP, PR...
	 * @param cidade - nome da cidade com ou sem acento, no sigma isso difere no id
	 * @return id da cidade no sigma
	 */
	public String buscaListaCidades(String stateId, String cidade)
	{
		String idRetorno = "";
		try
		{
			JSONObject result = consultaWebService("CityList", "stateId=" + stateId);
			if (result.get("status").equals("success"))
			{
				JSONArray array = result.getJSONArray("result");
				for (int i = 0; i < array.length(); i++)
				{
					JSONObject cidadeSigma = array.getJSONObject(i);

					String retorno = cidadeSigma.getString("name");
					if (retorno.equalsIgnoreCase(cidade))
					{
						idRetorno = cidadeSigma.getString("id");
						break;
					}else 
					{
						String retorno_sem_acento = retiraCaracteresAcentuados(retorno);
						String cidade_sem_acento = retiraCaracteresAcentuados(cidade);
						if(retorno_sem_acento.equalsIgnoreCase(cidade_sem_acento))
						{
							idRetorno = cidadeSigma.getString("id");
							break;
						}
					}
				}
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return idRetorno;
	}

	/**
	 * Consome urls que retornam strings em formato json.
	 * 
	 * @param _url - caminho completo com parametros. <br>
	 *            Ex.: http://servidor:8080/SigmaWebServices/CityList?stateId=SC
	 * @return Objeto Json contendo o retorno da url
	 */
	
	public String getStringUTF8(String s)
	{
		String x = new String("");

		try
		{
			//aqui estou garantindo a formatação UTF-8  
			x = new String(s.getBytes("UTF-8"));
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block  
			e.printStackTrace();
		}

		return x;
	}

	public String buscaListaEstados()
	{
		//StateList
		return "";
	}

	public String buscaListaRegioes(String districtId)
	{
		//RegionList?districtId=?
		return "";
	}

	public static String retiraCaracteresAcentuados(String stringFonte)
	{
		String passa = stringFonte;
		passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
		passa = passa.replaceAll("[âãàáä]", "a");
		passa = passa.replaceAll("[ÊÈÉË]", "E");
		passa = passa.replaceAll("[êèéë]", "e");
		passa = passa.replaceAll("[ÎÍÌÏ]", "I");
		passa = passa.replaceAll("[îíìï]", "i");
		passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
		passa = passa.replaceAll("[ôõòóö]", "o");
		passa = passa.replaceAll("[ÛÙÚÜ]", "U");
		passa = passa.replaceAll("[ûúùü]", "u");
		passa = passa.replaceAll("Ç", "C");
		passa = passa.replaceAll("ç", "c");
		passa = passa.replaceAll("[ýÿ]", "y");
		passa = passa.replaceAll("[&]", "e");
		passa = passa.replaceAll("Ý", "Y");
		passa = passa.replaceAll("ñ", "n");
		passa = passa.replaceAll("Ñ", "N");
		passa = passa.replaceAll("[-+=*%$#@!_]", "");
		passa = passa.replaceAll("['\"]", "");
		passa = passa.replaceAll("[<>()\\{\\}]", "");
		passa = passa.replaceAll("['\\\\.()|/]", "");
		passa = passa.replaceAll("[^!-ÿ]{1}[^ -ÿ]{0,}[^!-ÿ]{1}|[^!-ÿ]{1}", " ");
		passa = passa.replaceAll("[',]", " ");
		return passa;
	}

	/**
	 * 
	 * @param funcao - função a ser consultado no webservice
	 * @param param - parametros da função, que muda de acordo com cada função
	 * @return
	 * @throws Exception 
	 */
	public static JSONObject consultaWebService(String funcao, String param) throws Exception
	{
		//String param = montaParametroString(map);
		JSONResource response = null;
		String URL = (WEB_SERVICE_SIGMA + funcao + "?" + param);
		String excessao = "";
		try
		{
			System.out.println(URL);
			Resty r = new Resty();
			response = r.json(URL);
			JSONObject j = consomePaginaJson(URL);
			
			if (j.get("status").equals("success"))
			{
				//sucesso
				return j;
			}
			else
			{
				excessao = tratarInconsistencia(response, funcao, param);
				throw new Exception(excessao);
			}

		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new Exception(excessao);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("ERRO - " + e.getMessage());
		}
	}
	
	/**
	 * Verifica se a conta sigma já existe para o posto do contrato passados.
	 * @param contrato - numero do contrato sapiens
	 * @param numeroPosto - numro do posto sapiens
	 * @return - null se não encontrar uma conta.<BR>-CD_Cliente se encontrar a conta 
	 * @throws Exception 
	 */
	public static String buscaCDContaSigma(String contrato, String numeroPosto) throws Exception{
		if ( (numeroPosto == null || (numeroPosto != null && numeroPosto.trim().equals(""))) || (contrato == null || (contrato != null && contrato.trim().equals("")))){
			throw new Exception("Erro ao consultar a conta sigma do posto antigo. Contrato: " + contrato + ", posto:"+numeroPosto);
		}
		String retorno = null;
		String query = "select CD_CLIENTE from dbCENTRAL where REPLACE(REPLACE(REPLACE(CONTRATO,'.',''),' ',''),'-','') = '"+contrato+"' and REPLACE(REPLACE(REPLACE(COMPLEMENTO,'.',''),' ',''),'-','') = '"+numeroPosto+"'";

		Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
		
		PreparedStatement st = null;
		try	{	
			st = connection.prepareStatement(query);
			ResultSet rs = st.executeQuery();
			if (rs.next()){
				retorno = rs.getString(1);
			}
			rs.close();
			rs = null;
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro ao validar a conta sigma."+e.getMessage());
		}
		finally	{
			if (st != null){
				try{
					st.close();
					connection.close();
				}
				catch (SQLException e){
					e.printStackTrace();
				}
			}
		}
		
		if(retorno == null)
		{
			query = "select usu_codcli from usu_t160sig where usu_numctr = "+contrato+" and usu_numpos = " + numeroPosto;
			
			connection = OrsegupsUtils.getSqlConnection("SAPIENS");
			st = null;
			try	{	
				st = connection.prepareStatement(query);
				ResultSet rs = st.executeQuery();
				if (rs.next()){
					retorno = rs.getString(1);
				}
				rs.close();
				rs = null;
			}

			catch (Exception e) {
				e.printStackTrace();
				throw new WorkflowException("Erro ao validar a conta sigma."+e.getMessage());
			}
			finally	{
				if (st != null){
					try{
						st.close();
						connection.close();
					}
					catch (SQLException e){
						e.printStackTrace();
					}
				}
			}
		}
		
		return retorno;
		
	}
	
	
	/**
	 * Busca a conta sigma via cdCliente na tabela dbCentral. Esse método é utilizado em alterações de contrato 
	 * @param contrato - numero do contrato sapiens
	 * @param numeroPosto - numro do posto sapiens
	 * @return - null se não encontrar uma conta.<BR>-CD_Cliente se encontrar a conta 
	 * @throws Exception 
	 */
	public static String buscaCDContaSigmaByCdCliente(String cdCliente) throws Exception{
		if ( cdCliente != null && !cdCliente.trim().equals("") ){
			throw new WorkflowException("Erro ao consultar a conta sigma pelo CD_CLIENTE => " + cdCliente);
		}
		
		String retorno = null;
		String query = "select CD_CLIENTE from dbCENTRAL where CD_CLIENTE = " + cdCliente;

		Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
		
		PreparedStatement st = null;
		try	{	
			st = connection.prepareStatement(query);
			ResultSet rs = st.executeQuery();
			if (rs.next()){
				retorno = rs.getString(1);
			}
			rs.close();
			rs = null;
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erro ao validar a conta sigma."+e.getMessage());
		}
		finally	{
			if (st != null){
				try{
					st.close();
					connection.close();
				}
				catch (SQLException e){
					e.printStackTrace();
				}
			}
		}
		return retorno;
		
	}
	
	public static JSONObject consomePaginaJson(String _url)
	{
		BufferedReader reader = null;
		URL url;
		try
		{
			url = new URL(_url);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			if (reader != null)
			{
				reader.close();
			}
			System.out.println("[FLUXO CONTRATOS]-"+buffer.toString());
			JSONObject jo = new JSONObject(buffer.toString());
			return jo;
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			return (JSONObject) null;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return (JSONObject) null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return (JSONObject) null;
		}

	}

	public static MultipartContent montaParametroOSFormData(Map<String, String> map)
	{
		MultipartContent formData = null;
		formData = form(data("clientId", "110774"), data("partition", "500"), data("companyId", "10004"), data("centralId", "B409"), data("description", "TESTE TI - EXCLUIR"), data("requester", "10030"), data("defect", "184"), data("responsibleTechnician", ""));

		/*
		 * Teste
		 * ID_Central = B409
		 * partição= 500
		 * cd_cliente = 110774
		 * id_empresa=10004
		 * requester = 10030
		 * String clientId = "110774";
		 * String partition = "500";
		 * String companyId = "1004";
		 * String centralId = "B409";
		 * String requester = "10030";
		 * String defect = "184";
		 */

		return formData;
	}

	public static String montaParametroOSString(Map<String, String> map)
	{
		String parametro = "";

		/*
		 * clientId=?*
		 * &centralId=?*
		 * &partition=?*
		 * &companyId=?*
		 * &responsibleTechnician=?*
		 * &defect=?*
		 * &requester=?*
		 * &description=?
		 * &estimatedTime=?
		 * &scheduledTime=?
		 */
		if (map.get("clientId") != null && !map.get("clientId").equals(""))
		{
			parametro += "clientId" + "=" + map.get("clientId");
		}

		if (map.get("centralId") != null && !map.get("centralId").equals(""))
		{
			parametro += "&" + "centralId" + "=" + map.get("centralId");
		}

		if (map.get("partition") != null && !map.get("partition").equals(""))
		{
			parametro += "&" + "partition" + "=" + map.get("partition");
		}

		if (map.get("companyId") != null && !map.get("companyId").equals(""))
		{
			parametro += "&" + "companyId" + "=" + map.get("companyId");
		}

		if (map.get("responsibleTechnician") != null && !map.get("responsibleTechnician").equals(""))
		{
			parametro += "&" + "responsibleTechnician" + "=" + map.get("responsibleTechnician");
		}

		if (map.get("defect") != null && !map.get("defect").equals(""))
		{
			parametro += "&" + "defect" + "=" + map.get("defect");
		}

		if (map.get("requester") != null && !map.get("requester").equals(""))
		{
			parametro += "&" + "requester" + "=" + map.get("requester");
		}

		if (map.get("description") != null && !map.get("description").equals(""))
		{
			parametro += "&" + "description" + map.get("description");
		}

		if (map.get("estimatedTime") != null && !map.get("estimatedTime").equals(""))
		{
			parametro += "&" + "estimatedTime" + "=" + map.get("estimatedTime");
		}

		if (map.get("scheduledTime") != null && !map.get("scheduledTime").equals(""))
		{
			parametro += "&" + "scheduledTime" + "=" + map.get("scheduledTime");
		}

		parametro = URLEncoder.encode(parametro).replace("%3D", "=").replace("%26", "&");
		return parametro;
	}

	/**
	 * 
	 * @param map - Mapa contendo <"nomeCampoWebService", "valor">
	 * @return String contendo os parametros ja formatados para ser enviado para o WEBSERVICE
	 */
	public String montaParametroString(Map<String, String> map)
	{
		String parametro = "";
		if (map.get("companyId") != null && !map.get("companyId").equals(""))
		{
			parametro += "companyId" + "=" + map.get("companyId");
		}

		if (map.get("id") != null && !map.get("id").equals(""))
		{
			parametro += "&" + "id" + "=" + map.get("id");
		}

		if (map.get("account") != null && !map.get("account").equals(""))
		{
			parametro += "&" + "account" + "=" + map.get("account");
		}

		if (map.get("corporationName") != null && !map.get("corporationName").equals(""))
		{
			parametro += "&" + "corporationName" + "=" + retiraCaracteresAcentuados(map.get("corporationName"));
		}

		if (map.get("tradeName") != null && !map.get("tradeName").equals(""))
		{
			parametro += "&" + "tradeName" + "=" + retiraCaracteresAcentuados(map.get("tradeName"));
		}

		if (map.get("stateId") != null && !map.get("stateId").equals(""))
		{
			parametro += "&" + "stateId" + "=" + map.get("stateId");
		}

		if (map.get("cityId") != null && !map.get("cityId").equals(""))
		{
			parametro += "&" + "cityId" + "=" + map.get("cityId");
		}

		if (map.get("districtId") != null && !map.get("districtId").equals(""))
		{
			parametro += "&" + "districtId" + "=" + map.get("districtId");
		}

		if (map.get("route1Id") != null && !map.get("route1Id").equals(""))
		{
			parametro += "&" + "route1Id" + "=" + map.get("route1Id");
		}

		if (map.get("installerId") != null && !map.get("installerId").equals(""))
		{
			parametro += "&" + "installerId" + "=" + map.get("installerId");
		}

		if (map.get("branchActivityId") != null && !map.get("branchActivityId").equals(""))
		{
			parametro += "&" + "branchActivityId" + "=" + map.get("branchActivityId");
		}

		if (map.get("sellerId") != null && !map.get("sellerId").equals(""))
		{
			parametro += "&" + "sellerId" + "=" + map.get("sellerId");
		}

		if (map.get("observations") != null && !map.get("observations").equals(""))
		{
			parametro += "&" + "observations" + "=" + map.get("observations");
		}

		if (map.get("active") != null && !map.get("active").equals(""))
		{
			parametro += "&" + "active" + "=" + map.get("active");
		}

		if (map.get("contract") != null && !map.get("contract").equals(""))
		{
			parametro += "&" + "contract" + "=" + map.get("contract");
		}

		if (map.get("complement") != null && !map.get("complement").equals(""))
		{
			parametro += "&" + "complement" + "=" + map.get("complement");
		}

		if (map.get("controlPartition") != null && !map.get("controlPartition").equals(""))
		{
			parametro += "&" + "controlPartition" + "=" + map.get("controlPartition");
		}

		if (map.get("unifyPartition") != null && !map.get("unifyPartition").equals(""))
		{
			parametro += "&" + "unifyPartition" + "=" + map.get("unifyPartition");
		}

		if (map.get("partition") != null && !map.get("partition").equals(""))
		{
			parametro += "&" + "partition" + "=" + map.get("partition");
		}

		if (map.get("groupId") != null && !map.get("groupId").equals(""))
		{
			parametro += "&" + "groupId" + "=" + map.get("groupId");
		}

		if (map.get("personType") != null && !map.get("personType").equals(""))
		{
			parametro += "&" + "personType" + "=" + map.get("personType");
		}

		if (map.get("taxpayerId") != null && !map.get("taxpayerId").equals(""))
		{
			parametro += "&" + "taxpayerId" + "=" + map.get("taxpayerId");
		}

		if (map.get("responsible") != null && !map.get("responsible").equals(""))
		{
			parametro += "&" + "responsible" + "=" + map.get("responsible");
		}

		if (map.get("email") != null && !map.get("email").equals(""))
		{
			parametro += "&" + "email" + "=" + truncaCampoEmail(map.get("email"),50);
		}

		if (map.get("phone1") != null && !map.get("phone1").equals(""))
		{
			parametro += "&" + "phone1" + "=" + map.get("phone1");
		}

		if (map.get("phone2") != null && !map.get("phone2").equals(""))
		{
			parametro += "&" + "phone2" + "=" + map.get("phone2");
		}

		if (map.get("nextel") != null && !map.get("nextel").equals(""))
		{
			parametro += "&" + "nextel" + "=" + map.get("nextel");
		}

		if (map.get("zipCode") != null && !map.get("zipCode").equals(""))
		{
			parametro += "&" + "zipCode" + "=" + map.get("zipCode");
		}

		if (map.get("address") != null && !map.get("address").equals(""))
		{
			parametro += "&" + "address" + "=" + map.get("address");
		}

		if (map.get("regionId") != null && !map.get("regionId").equals(""))
		{
			parametro += "&" + "regionId" + "=" + map.get("regionId");
		}

		if (map.get("peopleInPlace") != null && !map.get("peopleInPlace").equals(""))
		{
			parametro += "&" + "peopleInPlace" + "=" + map.get("peopleInPlace");
		}

		if (map.get("dogInPlace") != null && !map.get("dogInPlace").equals(""))
		{
			parametro += "&" + "dogInPlace" + "=" + map.get("dogInPlace");
		}

		if (map.get("gunInPlace") != null && !map.get("gunInPlace").equals(""))
		{
			parametro += "&" + "gunInPlace" + "=" + map.get("gunInPlace");
		}

		if (map.get("levelRisk") != null && !map.get("levelRisk").equals(""))
		{
			parametro += "&" + "levelRisk" + "=" + map.get("levelRisk");
		}

		if (map.get("ownsEquipment") != null && !map.get("ownsEquipment").equals(""))
		{
			parametro += "&" + "ownsEquipment" + "=" + map.get("ownsEquipment");
		}

		if (map.get("keyOfPlace") != null && !map.get("keyOfPlace").equals(""))
		{
			parametro += "&" + "keyOfPlace" + "=" + map.get("keyOfPlace");
		}

		if (map.get("keyChain") != null && !map.get("keyChain").equals(""))
		{
			parametro += "&" + "keyChain" + "=" + map.get("keyChain");
		}

		if (map.get("latitude") != null && !map.get("latitude").equals(""))
		{
			parametro += "&" + "latitude" + "=" + map.get("latitude");
		}

		if (map.get("longitude") != null && !map.get("longitude").equals(""))
		{
			parametro += "&" + "longitude" + "=" + map.get("longitude");
		}

		if (map.get("route2Id") != null && !map.get("route2Id").equals(""))
		{
			parametro += "&" + "route2Id" + "=" + map.get("route2Id");
		}

		if (map.get("forceOS") != null && !map.get("forceOS").equals(""))
		{
			parametro += "&" + "forceOS" + "=" + map.get("forceOS");
		}

		if (map.get("responsibleTechnicianId") != null && !map.get("responsibleTechnicianId").equals(""))
		{
			parametro += "&" + "responsibleTechnicianId" + "=" + map.get("responsibleTechnicianId");
		}

		if (map.get("tracker") != null && !map.get("tracker").equals(""))
		{
			parametro += "&" + "tracker" + "=" + map.get("tracker");
		}

		if (map.get("references") != null && !map.get("references").equals(""))
		{
			parametro += "&" + "references" + "=" + map.get("references");
		}

		parametro = URLEncoder.encode(parametro).replace("%3D", "=").replace("%26", "&");
		return parametro;
	}

	/**
	 * 
	 * @param map - Mapa contendo <"nomeCampoWebService", "valor">
	 * @return objeto JSON contendo os parametros
	 */
	public JSONObject montaParametros(Map<String, String> map)
	{
		JSONObject parametro = new JSONObject();
		try
		{
			if (map.get("companyId") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("companyId", map.get("companyId"));
			}

			if (map.get("account") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("account", map.get("account"));
			}

			if (map.get("corporationName") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("corporationName", map.get("corporationName"));
			}

			if (map.get("tradeName") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("tradeName", map.get("tradeName"));
			}

			if (map.get("stateId") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("stateId", map.get("stateId"));
			}

			if (map.get("cityId") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("cityId", map.get("cityId"));
			}

			if (map.get("districtId") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("districtId", map.get("districtId"));
			}

			if (map.get("route1Id") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("route1Id", map.get("route1Id"));
			}

			if (map.get("installerId") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("installerId", map.get("installerId"));
			}

			if (map.get("branchActivityId") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("branchActivityId", map.get("branchActivityId"));
			}

			if (map.get("sellerId") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("sellerId", map.get("sellerId"));
			}

			if (map.get("observations") != null && !map.get("XXXXXXX").equals(""))
			{
				parametro.put("observations", map.get("observations"));
			}

			{
				//opcionais
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return parametro;
	}
	
	
	/**
	 * Retorna lista de postos do sigma
	 * @param contractId - id do contrato no fusion
	 * @return
	 * @throws Exception 
	 */
	public static JSONArray buscaListaPostosSigmaPorContrato(Integer contractId) throws Exception
	{
		/*
		 * campos retornados:
		 	status	 				String	Yes	 	 success, validation_error, unexpected_error	 The status of the request
			errors	 				Map	 	No	 -	 The validation errors
			result	 				List	No	 -	 The clients of the search result
			result.id	 			Integer	No	 -	 Id of the client
			result.account	 		String	No	 -	 Account of the client
			result.company	 		String	No	 -	 Company name of the client
			result.tradeName	 	String	No	 -	 Trade name of the client
			result.corporateName	String	No	 -	 Corporate name of the client
			result.address	 		String	No	 -	 Address of the client
			result.district	 		String	No	 -	 District name of the client
			result.city	 			String	No	 -	 City name of the client
			result.state	 		String	No	 -	 State name of the client
		 */

		// ClientSearch?searchBy=0&searchType=0&description=1&companyId=10017&showDisabled=1
		try
		{
			String params = "searchBy=10&searchType=2&description="+contractId+"&showDisabled=1";
			JSONObject result = consultaWebService("ClientSearch", params);
			String idRetorno = "";
			
			if (result.get("status").equals("success"))
			{
				JSONArray array = result.getJSONArray("result");
				
				return array;
			}
			else
			{
				return (JSONArray) null;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("ERRO - buscaListaPostosSigmaPorContrato("+contractId+") " + e.getMessage());
		}
		
	}
	
	/**
	 * Verifica se o contrato já existe.
	 * @param CD_CLIENTE
	 * @param nomeContato
	 * @return
	 */
	public static boolean contatoJaExiste(Long CD_CLIENTE, String nomeContato){
		boolean retorno = false;
		String query = "select * from dbProvidencia where  CD_CLIENTE = "+CD_CLIENTE+" and NOME = '"+nomeContato+"' ";
		Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
		
		PreparedStatement st = null;
		try
		{	
			st = connection.prepareStatement(query);
			ResultSet rs = st.executeQuery();
			if (rs.next()){
				retorno = true;
			}
			rs.close();
			rs = null;
		}

		catch (Exception e) {
			retorno = false;
			e.printStackTrace();
			throw new WorkflowException("Erro ao verificar contato no sigma."+e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
		
		
		return retorno;
	}
	
	
	
	
	public static void inserirContatoSigma(Long CD_CLIENTE                
									,String NOME                      
									,String FONE1                     
									,String FONE2                     
									,String EMAIL
									,int PRIORIDADE
									  
		){
		Integer CD_PROVIDENCIA = 0;
		Integer ID_FUNCAO = 10001;
		String NU_NEXTEL_PROVIDENCIA = "";
		int VIOLACAO =1;                  
		int PANICO =1;                    
		int MEDICA =1;                    
		int INCENDIO =1;
		int NU_PRIORIDADE =0;             
		int NU_PRIORIDADE_NIVEL2 =1;      
		int FG_REGISTRO_ALTERADO =0;    
		int FG_REGISTRO_INCLUSO =1;      
		int FG_ERRO_WEBALARME =0;          
		int FG_EFETUADA_LIGACAO_FONE1 =0; 
		int FG_EFETUADA_LIGACAO_FONE2 =0; 
		int NU_TENTATIVA_LIGACAO_FONE1 =0;
		int NU_TENTATIVA_LIGACAO_FONE2 =0;
		int FG_RECEBER_LIGACOES_URA =0;
		
		if(PRIORIDADE != 0){
			NU_PRIORIDADE_NIVEL2 = PRIORIDADE;
		}
		
		Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
		
		String queryCD_PROVIDENCIA = "select MAX(isnull( CD_PROVIDENCIA,0))+1 from dbPROVIDENCIA";
		
		StringBuilder query = new StringBuilder();
		/*                                     
		 CD_PROVIDENCIA, int                  - max (cd_providencia+1) from dbPROVIDENCIA
		 CD_CLIENTE, int,                     - vem da dbcentral
		,ID_FUNCAO, int,                      - 10001 (Nao Definido)
		,NOME, varchar(50),                   - 
		,FONE1, varchar(14),                  - 
		,FONE2, varchar(14),                  - 
		,VIOLACAO, bit,                       - 1
		,PANICO, bit,                         - 1
		,MEDICA, bit,                         - 1
		,INCENDIO, bit,                       - 1
		,EMAIL, varchar(50),                  - 
		,NU_NEXTEL_PROVIDENCIA, varchar(20),  - null
		,NU_PRIORIDADE, smallint,             - 1
		,NU_PRIORIDADE_NIVEL2, smallint,      - 1
		,FG_REGISTRO_ALTERADO, bit,           - 1
		,FG_REGISTRO_INCLUSO, bit,            - 1
		,FG_ERRO_WEBALARME, bit,              - 0
		,FG_EFETUADA_LIGACAO_FONE1, bit,      - 0
		,FG_EFETUADA_LIGACAO_FONE2, bit,      - 0
		,NU_TENTATIVA_LIGACAO_FONE1, tinyint, - 0
		,NU_TENTATIVA_LIGACAO_FONE2, tinyint, - 0
		,FG_RECEBER_LIGACOES_URA, bit,        - 0
		*/



		query.append("INSERT INTO dbPROVIDENCIA (");
		//query.append("	    CD_PROVIDENCIA             ");  // 
		query.append("	   CD_CLIENTE                 ");  // CD_CLIENTE, int,
		query.append("     ,ID_FUNCAO                  ");  // ID_FUNCAO, int,
		query.append("     ,NOME                       ");  // NOME, varchar(50),
		query.append("     ,FONE1                      ");  // FONE1, varchar(14),
		query.append("     ,FONE2                      ");  // FONE2, varchar(14),
		query.append("     ,VIOLACAO                   ");  // VIOLACAO, bit,
		query.append("     ,PANICO                     ");  // PANICO, bit,
		query.append("     ,MEDICA                     ");  // MEDICA, bit,
		query.append("     ,INCENDIO                   ");  // INCENDIO, bit,
		query.append("     ,EMAIL                      ");  // EMAIL, varchar(50),
		//query.append("     ,NU_NEXTEL_PROVIDENCIA      ");  // NU_NEXTEL_PROVIDENCIA, varchar(20),
		query.append("     ,NU_PRIORIDADE              ");  // NU_PRIORIDADE, smallint,
		query.append("     ,NU_PRIORIDADE_NIVEL2       ");  // NU_PRIORIDADE_NIVEL2, smallint,
		query.append("     ,FG_REGISTRO_ALTERADO       ");  // FG_REGISTRO_ALTERADO, bit,
		query.append("     ,FG_REGISTRO_INCLUSO        ");  // FG_REGISTRO_INCLUSO, bit,
		query.append("     ,FG_ERRO_WEBALARME          ");  // FG_ERRO_WEBALARME, bit,
		query.append("     ,FG_EFETUADA_LIGACAO_FONE1  ");  // FG_EFETUADA_LIGACAO_FONE1, bit,
		query.append("     ,FG_EFETUADA_LIGACAO_FONE2  ");  // FG_EFETUADA_LIGACAO_FONE2, bit,
		query.append("     ,NU_TENTATIVA_LIGACAO_FONE1 ");  // NU_TENTATIVA_LIGACAO_FONE1, tinyint,
		query.append("     ,NU_TENTATIVA_LIGACAO_FONE2 ");  // NU_TENTATIVA_LIGACAO_FONE2, tinyint,
		query.append("     ,FG_RECEBER_LIGACOES_URA    ");  // FG_RECEBER_LIGACOES_URA, bit,)
		query.append(" )VALUES(                          ");
		//query.append("	   ( ? ");  							  // CD_PROVIDENCIA, int,
		query.append("	   "+CD_CLIENTE                  + " ");  // CD_CLIENTE, int,
		query.append("     ,"+ID_FUNCAO                  + " ");  // ID_FUNCAO, int,
		query.append("     ,'"+NOME                      + "' ");  // NOME, varchar(50),
		query.append("     ,'"+FONE1                     + "' ");  // FONE1, varchar(14),
		query.append("     ,'"+FONE2                     + "' ");  // FONE2, varchar(14),
		query.append("     ,"+VIOLACAO                   + " ");  // VIOLACAO, bit,
		query.append("     ,"+PANICO                     + " ");  // PANICO, bit,
		query.append("     ,"+MEDICA                     + " ");  // MEDICA, bit,
		query.append("     ,"+INCENDIO                   + " ");  // INCENDIO, bit,
		query.append("     ,'"+EMAIL                     + "' ");  // EMAIL, varchar(50),
		//query.append("     ,'"+NU_NEXTEL_PROVIDENCIA   + "' ");  // NU_NEXTEL_PROVIDENCIA, varchar(20),
		query.append("     ,"+NU_PRIORIDADE              + " ");  // NU_PRIORIDADE, smallint,
		query.append("     ,"+NU_PRIORIDADE_NIVEL2       + " ");  // NU_PRIORIDADE_NIVEL2, smallint,
		query.append("     ,"+FG_REGISTRO_ALTERADO       + " ");  // FG_REGISTRO_ALTERADO, bit,
		query.append("     ,"+FG_REGISTRO_INCLUSO        + " ");  // FG_REGISTRO_INCLUSO, bit,
		query.append("     ,"+FG_ERRO_WEBALARME          + " ");  // FG_ERRO_WEBALARME, bit,
		query.append("     ,"+FG_EFETUADA_LIGACAO_FONE1  + " ");  // FG_EFETUADA_LIGACAO_FONE1, bit,
		query.append("     ,"+FG_EFETUADA_LIGACAO_FONE2  + " ");  // FG_EFETUADA_LIGACAO_FONE2, bit,
		query.append("     ,"+NU_TENTATIVA_LIGACAO_FONE1 + " ");  // NU_TENTATIVA_LIGACAO_FONE1, tinyint,
		query.append("     ,"+NU_TENTATIVA_LIGACAO_FONE2 + " ");  // NU_TENTATIVA_LIGACAO_FONE2, tinyint,
		query.append("     ,"+FG_RECEBER_LIGACOES_URA    + " ");  // FG_RECEBER_LIGACOES_URA, bit,)
		query.append("	   )");
		
		
		PreparedStatement st = null;
		try
		{	
			System.out.println("[FLUXO CONTRATOS]-Query contato sigma :" + query.toString());
			st = connection.prepareStatement(query.toString());
			//st2.setLong(1, CD_PROVIDENCIA);
			st.executeUpdate();
			
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro ao cadastrar contato no sigma."+e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
		
	}

	/**
	 * Renomeia razao social apenas, na conta sigma.
	 * @param cgcCpf - cpf do cliente
	 * @param razaoOld - razão antiga
	 * @param razaoNew - razão nova
	 */
	public static void renomeiaRazaoClienteSigma(String cgcCpf,	String razaoOld, String razaoNew) {
		String query = null;
		if ( cgcCpf != null && !cgcCpf.trim().equals("") && !!cgcCpf.trim().equalsIgnoreCase("NULL") ){
			query = " update dbCENTRAL set razao = REPLACE(razao, '"+razaoOld+"','"+razaoNew+"' ) where (CGCCPF = CAST(CAST('"+cgcCpf+"' AS numeric(20,0)) as VARCHAR(20)) or CGCCPF = '"+cgcCpf+"' ) ";
		}
		
		Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
		
		PreparedStatement st = null;
		try
		{	
			System.out.println("[FLUXO CONTRATOS]-Query, altera razao social no sigma :" + query.toString());
			st = connection.prepareStatement(query.toString());
			//FIXME SÓ EXECUTAR EM PRODUCAO
			//st.executeUpdate();
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro ao alterar razao social no sigma."+e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public static Integer buscaRota(String rota){
		Integer retorno = -1;
		String query = "select * from ROTA where  NM_ROTA = '"+rota+"' ";
		Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
		
		PreparedStatement st = null;
		try
		{	
			st = connection.prepareStatement(query);
			ResultSet rs = st.executeQuery();
			if (rs.next()){
				retorno = rs.getInt("CD_ROTA");
			}
			rs.close();
			rs = null;
		}

		catch (Exception e) {
			retorno = -1;
			e.printStackTrace();
			throw new WorkflowException("Erro ao consultar rota no sigma."+e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
		
		
		return retorno;
	}
	
	public static Integer findRota(String rota){
		Integer retorno = -1;
		String query = "select * from ROTA where  NM_ROTA like '%"+rota+"%'";
		Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
		
		PreparedStatement st = null;
		try
		{	
			st = connection.prepareStatement(query);
			ResultSet rs = st.executeQuery();
			if (rs.next()){
				retorno = rs.getInt("CD_ROTA");
			}
			rs.close();
			rs = null;
		}

		catch (Exception e) {
			retorno = -1;
			e.printStackTrace();
			throw new WorkflowException("Erro ao consultar rota no sigma."+e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
		
		
		return retorno;
	}
	
	/**
	 * Trunca campo de e-mail 
	 * @param campo
	 * @param maxSize
	 * @return
	 */
	public static String truncaCampoEmail(String campo, int maxSize) {
		String retorno = null;
		if (campo != null) {
			if (campo.length() > maxSize) {
				int pos = -1;
				if ((pos = campo.indexOf(";")) > -1) {
					retorno = campo.substring(0, pos);
				} else if ((pos = campo.indexOf(",")) > -1) {
					retorno = campo.substring(0, pos);
				}
			} else {
				retorno = campo;
			}

		}
		return retorno;
	}
	
	/*public static void main(String args[]){
		System.out.println(retiraCaracteresAcentuados("K & bosta sa"));
	}*/
	
}
