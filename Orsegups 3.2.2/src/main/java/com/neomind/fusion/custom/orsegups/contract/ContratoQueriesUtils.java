package com.neomind.fusion.custom.orsegups.contract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.persist.PersistEngine;

//com.neomind.fusion.custom.orsegups.contract-ContratoQueriesUtils
public class ContratoQueriesUtils {

	public static ArrayList<String[]> listaCadastrosPorTipoLancamento(String datIni, String datFim){
		
		ArrayList<String[]> retorno = new ArrayList<String[]>();
		
		String sql = " select mov.descricaoTipo, count(*) as Contratos " +
		"from wfProcess wf " +
		"join D_FGCprincipal fp on (wf.neoId = fp.wfprocess_neoId  ) " +
		"join D_FCGContratoDadosGeraisSapiens fdg on (fp.dadosGeraisContrato_neoId = fdg.neoID) " +
		"join NeoUser nu on (nu.neoId = wf.requester_neoId) " +
		"left join x_sapienscontrato xcon on (xcon.neoId = fp.numContrato_neoID) " +
		"join D_FGCTipoContratoNovo mov on (fp.movimentoContratoNovo_neoID = mov.neoId) " +
		"where 1=1 " + 
		//"--and fdg.numeroContratoSapiens is not null
		" and wf.startDate between cast( '"+datIni+" 00:00:00.000' as Datetime) and cast( '"+datFim+" 00:00:00.000' as Datetime) " +
		//"and nu.fullname <> 'Danilo da Silva' and nu.fullname <> 'Suporte Neomind' " +
		"group by mov.descricaoTipo ";
		System.out.println(sql);
		//sql = "select mov.descricaoTipo, count(*) as Contratos from wfProcess wf join D_FGCprincipal fp on (wf.neoId = fp.wfprocess_neoId  ) join D_FCGContratoDadosGeraisSapiens fdg on (fp.dadosGeraisContrato_neoId = fdg.neoID) join NeoUser nu on (nu.neoId = wf.requester_neoId) left join x_sapienscontrato xcon on (xcon.neoId = fp.numContrato_neoID) join D_FGCTipoContratoNovo mov on (fp.movimentoContratoNovo_neoID = mov.neoId) where 1=1  and wf.startDate between cast( '2014-12-07 00:00:00.000' as Datetime) and cast( '2014-25-10 00:00:00.000' as Datetime) group by mov.descricaoTipo ";
		Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		
		if (resultList != null && !resultList.isEmpty())
		{
			String keys[] = new String[resultList.size()];
			String values[] = new String[resultList.size()];
			Iterator<Object> ite = resultList.iterator();
			
			for (int i =0; i < resultList.size(); i++)
			{
				Object result = ite.next();
				if (result != null)
				{
					Object[] obj = (Object[]) result;
					keys[i] = String.valueOf(obj[0]);
					values[i] = String.valueOf(obj[1]);		
					System.out.println(String.valueOf(obj[0])  +"-"+ String.valueOf(obj[1]) );
				}
			}
			retorno.add(keys);
			retorno.add(values);
		}
		
		return retorno;
	}
	
public static ArrayList<String[]> listaCadastrosPorDia(String datIni, String datFim){
		
		ArrayList<String[]> retorno = new ArrayList<String[]>();
		try{
			
			String sql = " select CONVERT(VARCHAR(12),wf.startDate, 12) as Dia, CONVERT(VARCHAR(12),wf.startDate, 3), count(*) as Contratos "
					+"from wfProcess wf "
					+"join D_FGCprincipal fp on (wf.neoId = fp.wfprocess_neoId  ) "
					+"join D_FCGContratoDadosGeraisSapiens fdg on (fp.dadosGeraisContrato_neoId = fdg.neoID) "
					+"join NeoUser nu on (nu.neoId = wf.requester_neoId) "
					+"left join x_sapienscontrato xcon on (xcon.neoId = fp.numContrato_neoID) "
					+"join D_FGCTipoContratoNovo mov on (fp.movimentoContratoNovo_neoID = mov.neoId) "
					+"where 1=1 "
					//+"--and fdg.numeroContratoSapiens is not null "
					+" and wf.startDate between cast( '"+datIni+" 00:00:00.000' as Datetime) and cast( '"+datFim+" 00:00:00.000' as Datetime) " 
					//+"and nu.fullname <> 'Danilo da Silva' and nu.fullname <> 'Suporte Neomind' "
					+"group by CONVERT(VARCHAR(12),wf.startDate, 12), CONVERT(VARCHAR(12),wf.startDate, 3) "
					+"order by CONVERT(VARCHAR(12),wf.startDate, 12) ";
			System.out.println(sql);
			
			Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();
			
			if (resultList != null && !resultList.isEmpty())
			{
				String keys[] = new String[resultList.size()];
				String values[] = new String[resultList.size()];
				Iterator<Object> ite = resultList.iterator();
				
				for (int i =0; i < resultList.size(); i++)
				{
					Object result = ite.next();
					if (result != null)
					{
						Object[] obj = (Object[]) result;
						keys[i] = String.valueOf(obj[1]);
						values[i] = String.valueOf(obj[2]);		
						System.out.println(String.valueOf(obj[1])  +"-"+ String.valueOf(obj[2]) );
					}
				}
				retorno.add(keys);
				retorno.add(values);
			}
			
			return retorno;
		}catch(Exception e){
			e.printStackTrace();
			return retorno;
		}
	}

public static ArrayList<String[]> listaCadastrosPorPessoa(String datIni, String datFim){
	
	ArrayList<String[]> retorno = new ArrayList<String[]>();
	try{
		
		String sql = "select nu.fullname, count(*) " 
					+"from wfProcess wf "
					+"join D_FGCprincipal fp on (wf.neoId = fp.wfprocess_neoId  ) "
					+"join D_FCGContratoDadosGeraisSapiens fdg on (fp.dadosGeraisContrato_neoId = fdg.neoID) "
					+"join NeoUser nu on (nu.neoId = wf.requester_neoId) "
					+"left join x_sapienscontrato xcon on (xcon.neoId = fp.numContrato_neoID) "
					+"join D_FGCTipoContratoNovo mov on (fp.movimentoContratoNovo_neoID = mov.neoId) "
					+"where 1=1 " 
					//+"--and fdg.numeroContratoSapiens is not null "
					+"and wf.startDate between cast( '"+datIni+" 00:00:00.000' as Datetime) and cast( '"+datFim+" 00:00:00.000' as Datetime) " 
					//+"and nu.fullname <> 'Danilo da Silva' and nu.fullname <> 'Suporte Neomind' "
					+"group by fullName ";
		System.out.println(sql);
		Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		
		if (resultList != null && !resultList.isEmpty())
		{
			String keys[] = new String[resultList.size()];
			String values[] = new String[resultList.size()];
			Iterator<Object> ite = resultList.iterator();
			
			for (int i =0; i < resultList.size(); i++)
			{
				Object result = ite.next();
				if (result != null)
				{
					Object[] obj = (Object[]) result;
					keys[i] = String.valueOf(obj[0]);
					values[i] = String.valueOf(obj[1]);		
					System.out.println(String.valueOf(obj[0])  +"-"+ String.valueOf(obj[1]) );
				}
			}
			retorno.add(keys);
			retorno.add(values);
		}
		
		return retorno;
	}catch(Exception e){
		e.printStackTrace();
		return retorno;
	}
}

	
public static ArrayList<String[]> listaCadastrosPorMes(String datIni, String datFim){
	
	ArrayList<String[]> retorno = new ArrayList<String[]>();
	try{
		
		String sql = "select datepart(month,wf.startDate) as mes,  count(*) as Contratos " 
					+"from wfProcess wf "
					+"join D_FGCprincipal fp on (wf.neoId = fp.wfprocess_neoId  ) "
					+"join D_FCGContratoDadosGeraisSapiens fdg on (fp.dadosGeraisContrato_neoId = fdg.neoID) "
					+"join NeoUser nu on (nu.neoId = wf.requester_neoId) "
					+"left join x_sapienscontrato xcon on (xcon.neoId = fp.numContrato_neoID) "
					+"join D_FGCTipoContratoNovo mov on (fp.movimentoContratoNovo_neoID = mov.neoId) "
					+"where 1=1 " 
					//+"--and fdg.numeroContratoSapiens is not null "
					+"and wf.startDate between cast( '"+datIni+" 00:00:00.000' as Datetime) and cast( '"+datFim+" 00:00:00.000' as Datetime) " 
					//+"and nu.fullname <> 'Danilo da Silva' and nu.fullname <> 'Suporte Neomind' "
					+"group by datepart(month,wf.startDate) ";
		System.out.println(sql);
		Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		
		if (resultList != null && !resultList.isEmpty())
		{
			String keys[] = new String[resultList.size()];
			String values[] = new String[resultList.size()];
			Iterator<Object> ite = resultList.iterator();
			
			for (int i =0; i < resultList.size(); i++)
			{
				Object result = ite.next();
				if (result != null)
				{
					Object[] obj = (Object[]) result;
					keys[i] = String.valueOf(obj[0]);
					values[i] = String.valueOf(obj[1]);		
					System.out.println(String.valueOf(obj[0])  +"-"+ String.valueOf(obj[1]) );
				}
			}
			retorno.add(keys);
			retorno.add(values);
		}
		
		return retorno;
	}catch(Exception e){
		e.printStackTrace();
		return retorno;
	}
}
	
	
	public static String arrayToStringString(String arr[]){
		String retorno = "";
		if (arr!= null && arr.length > 0){
			for (String a : arr){
				retorno += "'"+a+"',";
			}
			return retorno.substring(0,retorno.length()-1);
		}else{
			return retorno;
		}
	}
	
	public static String arrayToStringInt(String arr[]){
		String retorno = "";
		if (arr!= null && arr.length > 0){
			for (String a : arr){
				retorno += ""+a+",";
			}
			return retorno.substring(0,retorno.length()-1);
		}else{
			return retorno;
		}
	}
	
	public static String arrayMerged(String keys[] , String values[]){
		String retorno = "";
		int cont =0;
		if (keys!= null && keys.length > 0){
			for (String a : keys){
				retorno += "['"+a+"',"+values[cont]+"],";
				cont++;
			}
			return retorno.substring(0,retorno.length()-1);
		}else{
			return retorno;
		}
	}
	
	
	
	public static void main(String args[]){
		//ContratoQueriesUtils.listaCadastrosPorTipoLancamento();
	}
	
	
}
