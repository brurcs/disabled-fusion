package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.antlr.grammar.v3.ANTLRv3Parser.throwsSpec_return;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.presenca.vo.SaldoHorasVO;
import com.neomind.fusion.custom.orsegups.site.SiteUtils;
import com.neomind.fusion.custom.orsegups.site.vo.HorasVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;


//com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaR022HoraExtraExcessiva
public class RotinaAbreTarefaR022HoraExtraExcessiva2 implements CustomJobAdapter
{
	
	static Long numcad; 	 
	static Long numemp; 	 
	static Long tipcol; 	 
	static String nomfun; 	 
	static Long horas; 		 
	static Long codRegional; 
	static String lot; 	

	@Override
	public void execute(CustomJobContext ctx)
	{
		System.out.println("[HORA EXTRA] - #### Iniciando Processo de Abertura de Tarefas de Hora Extra Excessivas ####");
		GregorianCalendar datIni = new GregorianCalendar();
		datIni.set(GregorianCalendar.DATE, 1);
		datIni.add(GregorianCalendar.MONTH, -1);
		datIni.add(GregorianCalendar.MONTH, -1);
		
		GregorianCalendar datFim = new GregorianCalendar();
		datFim.set(GregorianCalendar.DATE, 1);
		datFim.add(GregorianCalendar.DATE, -1);
		datFim.add(GregorianCalendar.MONTH, -1);
		
		
		System.out.println(NeoUtils.safeDateFormat(datIni, "dd/MM/yyyy HH:mm"));
		System.out.println(NeoUtils.safeDateFormat(datFim, "dd/MM/yyyy HH:mm"));
		
		StringBuilder sql = new StringBuilder();
		
//		sql.append(" select fun.numcad, fun.nomfun, sum(horas.horas-horas.horasDeb) horasExtras, orn.USU_CodReg, orn.NomLoc, orn.USU_LotOrn, esc.HorMes, fun.numemp, fun.tipcol  from r034fun fun  ");
//		sql.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON (fun.numloc = orn.numloc) ");
//		sql.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
//		sql.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= GetDate()) "); 
//		sql.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc , ");
//		sql.append(" ( ");
//		sql.append("         ( ");
//		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  isnull(sum(sit.QtdHor),0) as horas, 0 as horasDeb  from dbo.R066sit sit ");
//		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp and tipcal = 11) ");
//		sql.append("                 where cal.perref = ? ");
//		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu "); 
//		sql.append("                 and sit.codsit in (451,452) ");
//		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
//		sql.append("         ) "); 
//		sql.append("         union ");
//		sql.append("         ( ");
//		sql.append("                 select sit.numcad, sit.numemp, sit.tipcol,  0 as horas,  isnull(sum(sit.QtdHor),0) as horasDeb  from dbo.R066sit sit ");
//		sql.append("                 inner join dbo.R044CAL cal on (cal.numemp = sit.numemp and tipcal = 11) ");
//		sql.append("                 where cal.perref = ? ");
//		sql.append("                 and datApu >= cal.iniapu   and datApu <= cal.fimapu "); 
//		sql.append("                 and sit.codsit in (450) ");
//		sql.append("                 group by sit.numcad, sit.numemp, sit.tipcol ");
//		sql.append("         ) "); 
//		sql.append(" ) as horas "); 
//		sql.append(" where  1=1 ");
//		//sql.append(" and fun.numcad = ? and fun.numemp = ? and fun.tipcol = ? ");
//		sql.append(" and ( horas.numcad = fun.numcad   and horas.numemp = fun.numemp and horas.tipcol = fun.tipcol ) and fun.numemp = ? and fun.numcad = ?");
//		//sql.append(" and fun.numcad in (768) ");//44125 , 44131,10127,10084,828,876,887, 44662,1180,935,1210
//		//sql.append(" and fun.numcad in (54,752,771,797,875,1077,15138,9694,15806,15763) ");//44125 , 44131,10127,10084,828,876,887, 44662,1180,935,1210
//		sql.append(" group by fun.numcad, fun.nomfun, orn.USU_CodReg, orn.NomLoc, orn.USU_LotOrn, esc.HorMes,fun.numemp, fun.tipcol ");
//		//sql.append(" having sum(horas.horas-horas.horasDeb) >= (esc.HorMes*0.20) "); // 20% do total que ele deve fazer no mês.
//		//sql.append(" having sum(horas.horas-horas.horasDeb) >= 300 ");
		sql.append(" declare @Datini datetime ");
		sql.append(" declare @Datfim datetime ");
		sql.append(" select @Datini = ? ");
		sql.append(" select @Datfim = ? ");
		sql.append(" select fun.numcad, fun.nomfun, sum(horas.horas-horas.horasDeb) horasExtras, orn.USU_CodReg, orn.NomLoc, orn.USU_LotOrn, esc.HorMes, fun.numemp, fun.tipcol  from r034fun fun  ");
		sql.append(" 		 INNER JOIN R016ORN orn WITH (NOLOCK) ON (fun.numloc = orn.numloc) ");
		sql.append(" 		 INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
		sql.append(" 		 INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= GetDate())  ");
		sql.append(" 		 INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc , ");
		sql.append(" 		 ( ");
		sql.append(" 		         ( ");
		sql.append(" 		                 select sit.numcad, sit.numemp, sit.tipcol,  isnull(sum(sit.QtdHor),0) as horas, 0 as horasDeb  from dbo.R066sit sit ");
		sql.append(" 		                 where  datApu >= '2016-06-01'   and datApu <= '2016-06-30' ");
		sql.append(" 		                 and sit.codsit in (451,452) ");
		sql.append(" 		                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append(" 		         )  ");
		sql.append(" 		         union ");
		sql.append(" 		         ( ");
		sql.append(" 		                 select sit.numcad, sit.numemp, sit.tipcol,  0 as horas,  isnull(sum(sit.QtdHor),0) as horasDeb  from dbo.R066sit sit ");
		sql.append(" 		                 where datApu >= '2016-06-01'   and datApu <= '2016-06-30' ");
		sql.append(" 		                 and sit.codsit in (450) ");
		sql.append(" 		                 group by sit.numcad, sit.numemp, sit.tipcol ");
		sql.append(" 		         )  ");
		sql.append(" 		 ) as horas  ");
		sql.append(" 		 where  1=1 ");
		sql.append(" 		 and ( horas.numcad = fun.numcad   and horas.numemp = fun.numemp and horas.tipcol = fun.tipcol )");
		sql.append(" 		 group by fun.numcad, fun.nomfun, orn.USU_CodReg, orn.NomLoc, orn.USU_LotOrn, esc.HorMes,fun.numemp, fun.tipcol ");
		sql.append("         HAVING sum(horas.horas-horas.horasDeb) >= 300 ");                                                                   

		
		Connection connection = OrsegupsUtils.getSqlConnection("VETORH") ;
		ResultSet rs = null;
		try
		{
			PreparedStatement pst = connection.prepareStatement(sql.toString());
			boolean abrirTarefa = false;
			
			pst.setDate(1, new Date(datIni.getTimeInMillis()));
			pst.setDate(2, new Date(datFim.getTimeInMillis()));
			rs = pst.executeQuery();
			Long saldoMesAnt = 0L;
			Long aPagar = 0L;
			
			while (rs.next()){
				Long he = rs.getLong("horasExtras");
				abrirTarefa = false;
					
				Long codReg = rs.getLong("USU_CodReg");
				String lotacao = "";
				
				if (codReg == 0 && rs.getString("USU_LotOrn").equals("Coordenadoria Indenização Contratual"))
				{
					lotacao = rs.getString("NomLoc");
				}
				else if (codReg == 1L)
				{
					lotacao = rs.getString("NomLoc");
				}
				else if (codReg == 13L)
				{
					lotacao = rs.getString("NomLoc");
				}
				else
				{
					lotacao = rs.getString("USU_LotOrn");
				}
				System.out.println("[HORA EXTRA] -  Abrir tarefa -> Funcionario:  " + rs.getLong("numcad") + " - " + rs.getString("nomfun") + " codReg="+codReg+", lotacao"+lotacao );
				numcad 	 = rs.getLong("numcad"); 
				numemp 	 = rs.getLong("numemp"); 
				tipcol 	 = rs.getLong("tipcol"); 
				nomfun 	 = rs.getString("nomfun");
				horas 		 = he;
				codRegional = codReg;
				lot 		 = lotacao;
				
				//Nova validação para banco de horas
				HorasVO horasAnt = new HorasVO();
				HorasVO horasA = new HorasVO();
				SaldoHorasVO saldos = new SaldoHorasVO();
				GregorianCalendar gcb = new GregorianCalendar();
				gcb.add(GregorianCalendar.MONTH, -1);
				GregorianCalendar gca = new GregorianCalendar(); 
				gca.add(GregorianCalendar.DATE, -1);
				
				gcb.set(GregorianCalendar.DATE, 1);
				gcb.add(GregorianCalendar.DATE, -1);
				System.out.println(""+NeoDateUtils.safeDateFormat(gcb, "dd/MM/yyyy"));
				System.out.println(""+NeoDateUtils.safeDateFormat(gca, "dd/MM/yyyy"));
				
				horasAnt = SiteUtils.retornaSaldoHoras(numcad, numemp, 1L, gcb , -1, true);
				horasA =   SiteUtils.retornaSaldoHoras(numcad, numemp, 1L, gca , -1, false);
				saldos = SiteUtils.calculaSaldoFinal(horasAnt,horasA);
				horas = saldos.getSaldoVencer();
				
				if(horas >= 300){
					abrirTarefa = true;
				}
				
				if(abrirTarefa){
					final NeoRunnable work = new NeoRunnable(){
						public void run() throws Exception{
							//abrirTarefaR022(rs.getLong("numcad"), rs.getLong("numemp"), rs.getLong("tipcol"), rs.getString("nomfun"),he,codReg,lotacao);
							abrirTarefaR022(numcad, numemp,tipcol, nomfun,horas,codRegional,lot);
						}
					};
				
					try{
						PersistEngine.managedRun(work);
					}
					catch (final Exception e){
						System.out.println(e.getMessage() + e);
						e.printStackTrace();
						throw new Exception("Erro ao fazer managedRun");
					}
				}
			}
			rs.close();
			System.out.println("[HORA EXTRA] - #### Finalizando Processo de Abertura de Tarefas de Hora Extra Excessivas ####");
		}
		catch (Exception e)
		{
			Long key = GregorianCalendar.getInstance().getTimeInMillis();
			System.out.println("["+key+"] Erro ao processa Job RotinaAbreTarefaR022HoraExtraExcessiva  " + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processar Job Verifique no log por ["+key+"] para maiores detalhes");
		}
	}
	public static void abrirTarefaR022(Long numcad, Long numemp, Long tipcol,  String nomfun, Long horasExtras, Long codReg, String lotacao ){
		GregorianCalendar cpt = new GregorianCalendar();
		String msg = "<p>Prezado Gestor, o colaborador <b>"+numcad+"-"+nomfun+"</b> executou um saldo de "+getHorario(horasExtras)+" horas extras na competência, "+NeoUtils.safeDateFormat(cpt,"MM/yyyy")+".<br/> Por qual motivo?</p>";
		//System.out.println(msg);
		NeoObject oDHEXPrecessoHe = AdapterUtils.createNewEntityInstance("HEXProcessoHE");
		
		EntityWrapper wDHEXPrecessoHe = new EntityWrapper(oDHEXPrecessoHe);
		
		NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "dilmoberger" ) );
		NeoUser usuarioResponsavel = null; //PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
		
		NeoPaper papel = getPapelJustificarR022(codReg, lotacao);
		
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuarioResponsavel = user;
				break;
			}
		}
		
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("codigo",numemp));
		NeoObject oEmpresa = PersistEngine.getObject(AdapterUtils.getEntityClass("GCEmpresa"), gp);
		wDHEXPrecessoHe.setValue("empresa", oEmpresa);
		
		
		QLGroupFilter gp2 = new QLGroupFilter("AND");
		gp2.addFilter(new QLEqualsFilter("numemp",numemp));
		List<NeoObject> oListaCalculos = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHRCAL"), gp2, -1,-1, "codcal desc");
		for (NeoObject oCalculo: oListaCalculos ){
			EntityWrapper wCalculo = new EntityWrapper(oCalculo);
			
			GregorianCalendar dataAtual = new GregorianCalendar();
			GregorianCalendar inicmp = (GregorianCalendar) wCalculo.findValue("inicmp");
			GregorianCalendar fimcmp = (GregorianCalendar) wCalculo.findValue("fimcmp");
			
			if (dataAtual.after(inicmp) && dataAtual.before(fimcmp)){
				wDHEXPrecessoHe.setValue("calculo", oCalculo);
				System.out.println("Periodo Referencia: " + NeoUtils.safeDateFormat((GregorianCalendar) wCalculo.findValue("perref")));
				break;
			}
		}

		
		NeoObject oHEXRegistroHE = AdapterUtils.createNewEntityInstance("HEXRegistroHE");
		EntityWrapper wHEXRegistroHE = new EntityWrapper(oHEXRegistroHE);
		
		QLGroupFilter gp3 = new QLGroupFilter("AND");
		gp3.addFilter(new QLEqualsFilter("numcad",numcad));
		gp3.addFilter(new QLEqualsFilter("numemp",numemp));
		gp3.addFilter(new QLEqualsFilter("tipcol",tipcol));
		//NeoObject oColaborador = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), gp3);
		List<NeoObject> listColaborador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), gp3);
		NeoObject oColaboradorHe = null;
		if (listColaborador != null && !listColaborador.isEmpty()){
			for(NeoObject oColaborador: listColaborador){
				oColaboradorHe = oColaborador;
				wHEXRegistroHE.setValue("colaborador", oColaborador);
			}
		}
		
		if (horasExtras > 2400){ // horas 100%
			wHEXRegistroHE.setValue("he50",getHorario(2400L));    
			wHEXRegistroHE.setValue("he100",getHorario(horasExtras - 2400));
		}else{ // horas 50%
			wHEXRegistroHE.setValue("he50",getHorario(horasExtras));    
			wHEXRegistroHE.setValue("he100","00:00");
		}
		    
		wHEXRegistroHE.setValue("vaExtra",0L);    
		wHEXRegistroHE.setValue("motHex","Apuração automática via presença.");    
		    
		
		PersistEngine.persist(oHEXRegistroHE);
		wDHEXPrecessoHe.setValue("viaProcessoAutomatico", true);
		wDHEXPrecessoHe.setValue("sitApr",true); // aprovado por padrão, pois via processo automatico o aprovador não vai poder enviar a tarefa para o colaborador.
		wDHEXPrecessoHe.setValue("txtobs","Por qual motivo este funcionário fez horas extras?");
		wDHEXPrecessoHe.getField("listaRegistro").addValue(oHEXRegistroHE);//  setValue("listaRegistro", oHEXRegistroHE);
		
		//wDHEXPrecessoHe.setValue("colaboradorHE", oColaboradorHe);
		wDHEXPrecessoHe.setValue("solicitante", usuarioSolicitante);
		//wDHEXPrecessoHe.setValue("usuarioAprovador",usuarioResponsavel);
		wDHEXPrecessoHe.setValue("usuarioAprovador",usuarioResponsavel);
		
		PersistEngine.persist(oDHEXPrecessoHe);
		
		
		
		iniciaProcessoTarefaR022(oDHEXPrecessoHe, usuarioSolicitante, usuarioResponsavel);
		
		
	}
	
	public static Boolean iniciaProcessoTarefaR022(NeoObject eformProcesso, NeoUser requester, NeoUser responsavel)
	{
		Boolean result = false;

		QLEqualsFilter equal = new QLEqualsFilter("Name", "R022 - Informar Horas Extras");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
		System.out.println("requester:"+requester.getCode());
		WFProcess proc = processModel.startProcess(eformProcesso, false, null, null, null, null, requester);
		proc.setRequester(requester);
		System.out.println("[HORA EXTRA] - Abriu Tarefa R022n. " + proc.getCode() + " para o responsável " + responsavel.getCode() );

		proc.setSaved(true);

		// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
		PersistEngine.persist(proc);
		PersistEngine.commit(true);

		/* Finaliza a primeira tarefa */
		/*Task task = null;

		final List<Activity> acts = proc.getOpenActivities();
		System.out.println("acts: " + acts);
		if (acts != null)
		{
			System.out.println("acts.size(): " + acts.size());
		}
		final Activity activity1 = (Activity) acts.get(0);
		if (activity1 instanceof UserActivity)
		{
			try
			{
				if (((UserActivity) activity1).getTaskList().size() <= 0)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, responsavel, new HashSet<Task>());
					task.finish();
				}
				else
				{
					task = ((UserActivity) activity1).getTaskList().get(0);
					task.finish();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.err.println("Erro ao iniciar a primeira atividade do processo.");
			}
		}*/
		return result;
	}
	
	
	
	
	public static void abrirTarefaSimples(Long numcad, String nomfun, Long horasExtras, Long codReg, String lotacao ){
		
		
		GregorianCalendar cpt = new GregorianCalendar();

				
		
		String msg = "<p>Prezado Gestor, o colaborador <b>"+numcad+"-"+nomfun+"</b> executou um saldo de "+getHorario(horasExtras)+" horas extras na competência, "+NeoUtils.safeDateFormat(cpt,"MM/yyyy")+".<br/> Por qual motivo?</p>";
		
		
		
		NeoObject oTarefa = AdapterUtils.createNewEntityInstance("Tarefa");
		
		final EntityWrapper wTarefa= new EntityWrapper(oTarefa);
		
		NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "dilmoberger" ) );
		NeoUser usuarioResponsavel = null;//PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
		
		
		NeoPaper papel = OrsegupsUtils.getPapelJustificarAfastamento(codReg, lotacao);

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuarioResponsavel = user;
				break;
			}
		}
		
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 2);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 23);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		
		
		wTarefa.findField("Solicitante").setValue(usuarioSolicitante);
		wTarefa.findField("Executor").setValue(usuarioResponsavel);
		wTarefa.findField("Titulo").setValue("Hora Extra em Excesso colaborador " + nomfun);
		wTarefa.findField("DescricaoSolicitacao").setValue(msg);
		wTarefa.findField("Prazo").setValue(OrsegupsUtils.getNextWorkDay(prazo));
		wTarefa.findField("dataSolicitacao").setValue(new GregorianCalendar());
		
		PersistEngine.persist(oTarefa);
		
		iniciaProcessoTarefaSimples(oTarefa, usuarioSolicitante, usuarioResponsavel);
	}

	public static Boolean iniciaProcessoTarefaSimples(NeoObject eformProcesso, NeoUser requester, NeoUser responsavel)
	{
		Boolean result = false;
	
		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
		
		WFProcess proc = processModel.startProcess(eformProcesso, false, null, null, null, null, requester);
		proc.setRequester(requester);
		System.out.println("[HORA EXTRA] - Abriu Tarefa simples n. " + proc.getCode());
	
		proc.setSaved(true);
	
		// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
		PersistEngine.persist(proc);
		PersistEngine.commit(true);
	
		/* Finaliza a primeira tarefa */
		Task task = null;
	
		final List<Activity> acts = proc.getOpenActivities();
		System.out.println("acts: " + acts);
		if (acts != null)
		{
			System.out.println("acts.size(): " + acts.size());
		}
		final Activity activity1 = (Activity) acts.get(0);
		if (activity1 instanceof UserActivity)
		{
			try
			{
				if (((UserActivity) activity1).getTaskList().size() <= 0)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, responsavel);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
				else
				{
					task = ((UserActivity) activity1).getTaskList().get(0);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.err.println("[HORA EXTRA] - Erro ao iniciar a primeira atividade do processo.");
			}
		}
		return result;
	}

	public static String getHorario(Long horario)
	{
		String horarioString = "";
		
		int hora = horario.intValue() / 60;
		int minuto = horario.intValue() % 60;
		
		if (minuto < 0){
			minuto = minuto * (-1);
			if (minuto > 0){
				hora = hora * (-1);
			}
		}
		
		String horaString = String.format ("%02d",hora);
		String minutoString = String.format ("%02d",minuto);
		
		
		horarioString = horaString + ":" + minutoString;
		return horarioString;
	}



public static NeoPaper getPapelJustificarR022(Long codreg, String lotacao)
{
	String nomePapel = "";
	
	switch (codreg.intValue())
	{
		case 0:
			if (lotacao != null && lotacao.contains("Coordenadoria NAC"))
			{
				nomePapel = "Coordenador CEREC";
			}
			else if (lotacao != null && lotacao.contains("Assistente Indenização Contratual"))
			{
				nomePapel = "Gerente Comercial Mercado Público"; // o correto seria samara.fontoura, mas como está afastada 
			}
			else if (lotacao != null && lotacao.contains("Auxiliar Administrativo - Jurídico"))
			{
				nomePapel = "Assessor Jurídico";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria de Controladoria"))
			{
				nomePapel = "Gerente Controladoria";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria Comercial Administrativa"))
			{
				nomePapel = "Coordenador CEREC";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria Comercial Publico"))
			{
				nomePapel = "Gerente Comercial Mercado Público";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria da Qualidade"))
			{
				nomePapel = "Superintendente Operacional";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria Financeira"))
			{
				nomePapel = "Gerente Financeiro";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria de Compras"))
			{
				nomePapel = "Gerente Administrativo";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria Administrativa"))
			{
				nomePapel = "Gerente Administrativo";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria Almoxarifado de Eletrônica"))
			{
				nomePapel = "Gerente Administrativo";
			}
			else if (lotacao != null && lotacao.contains("Comunicação e Marketing"))
			{
				nomePapel = "Responsável Marketing";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria de Sistemas da Informaçao"))
			{
				nomePapel = "Coordenador de Sistemas";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria de Infraestrutura"))
			{
				nomePapel = "Supervisor de Infraestrutura de TI";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria de Monitoramento"))
			{
				nomePapel = "Gerente de Monitoramento";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria de Recursos Humanos"))
			{
				nomePapel = "Gerente de RH";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria de Departamento Pessoal"))
			{
				nomePapel = "Gerente de RH";
			}
			else if (lotacao != null && lotacao.contains("Coordenadoria de RH Estratégico"))
			{
				nomePapel = "Gerente RH Estratégico";
			}
			
			else if (lotacao != null && lotacao.contains("Coordenadoria de RH RSA"))
			{
				nomePapel = "Gerente RH Estratégico";
			}else if (lotacao != null && lotacao.contains("Assistente Assessoria Técnica")){
				nomePapel = "Assessor Técnico";
			}
			
			break;
			
		case 1:
			if (lotacao != null && lotacao.startsWith("Técnico de Eletrônica") || 
				lotacao.startsWith("Supervisor Técnico de Eletrônica") || 
				lotacao.startsWith("Técnicos de Eletrônica") || 
				lotacao.startsWith("Agente de Inspeção Técnica") || 
				lotacao != null && lotacao.startsWith("Auxiliar Técnico de Eletrônica") || 
				lotacao != null && lotacao.startsWith("Cobertura de Férias - Segurança Eletrônica") || 
				lotacao != null && lotacao.startsWith("Auxiliar Administrativo Eletrônica") || 
				lotacao != null && lotacao.startsWith("Supervisor de Segurança Eletrônica") || 
				lotacao != null && lotacao.startsWith("Aviso Prévio - Segurança Eletrônica"))
			{
				nomePapel = "Gerente de Vigilância Eletrônica";
			}
			else
			{
				nomePapel = "Gerente de Segurança";
			}
			break;
		case 2:
			nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
			break;
		case 3:
			nomePapel = "Gerente Escritorio Regional Brusque";
			break;
		case 4:
			nomePapel = "Gerente Escritorio Regional Blumenal";
			break;
		case 5:
			nomePapel = "Gerente Escritorio Regional Joinville";
			break;
		case 6:
			nomePapel = "Gerente Escritorio Regional Lages";
			break;
		case 7:
			nomePapel = "Gerente Escritório Regional Tubarão";
			break;
		case 8:
			nomePapel = "Gerente Escritorio Regional Gaspar";
			break;
		case 9:
			nomePapel = "Gerente de Asseio Conserv. e Limpeza";
			break;
		case 10:
			nomePapel = "Gerente Escritorio Regional Chapeco";
			break;
		case 11:
			nomePapel = "Gerente Escritório Regional Rio do Sul";
			break;
		case 12:
			nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
			break;
		case 13:
			//Técnicos de eletrônica CTA
			if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
			{
				nomePapel = "Gerente de Segurança Eletrônica CTA";
			}
			//Táticos CTA
			else if (lotacao.startsWith("ASE") && lotacao.contains("ROTA") && lotacao.endsWith("CTA"))
			{
				nomePapel = "Gerente de Segurança Eletrônica CTA";
			}
			else
			//Outros
			{
				nomePapel = "Gerente Escritório Regional Curitiba";
			}
			break;
		case 14://cascavel
			nomePapel = "Gerente Escritorio Regional Cascavel";
			break;
		case 15:
			nomePapel = "Gerente Escritório Regional Curitiba";
			break;
		case 16:
			nomePapel = "Gerente Escritório Regional Tubarão";
			break;

	}
	
	if (nomePapel.equals("")){ // vai para o juliano tratar
		nomePapel = "Responsável Justificar Afastamento Qualidade";
	}
	
	NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",nomePapel));
	return obj;
}
public static Long retornaSaldoHoras(Long numcad, Long numemp, long tipcol, GregorianCalendar mesRef, Integer offset)
{
	Long retorno = 0L;
	GregorianCalendar datini = new GregorianCalendar();
	GregorianCalendar datfim = new GregorianCalendar();
	String datIniFor = "";
	String datFimFor = "";
	
	if (offset != null){
		mesRef.add(GregorianCalendar.MONTH, offset);
		datini = (GregorianCalendar) mesRef.clone();
		
		datini.set(GregorianCalendar.DAY_OF_MONTH, 1);
		datfim = (GregorianCalendar) datini.clone();
		datfim.add(GregorianCalendar.MONTH, 1);
		datIniFor = retornaDataFormatoSapiensEVetorh(datini);
		datFimFor = retornaDataFormatoSapiensEVetorh(datfim);
		
	}
	

	StringBuilder sql = new StringBuilder();
	
	sql.append(" SELECT (SELECT isnull(SUM(QTDHOR),0) FROM R011LAN WHERE SINLAN = '+' AND ORILAN = 'A' AND NUMEMP = ? AND NUMCAD = ? AND TIPCOL = ? AND DATLAN >= ? AND DATLAN < ?) ");
	sql.append(" - ");
	sql.append("       (SELECT isnull(SUM(QTDHOR),0) FROM R011LAN WHERE SINLAN = '-' AND ORILAN = 'A' AND NUMEMP = ? AND NUMCAD = ? AND TIPCOL = ? AND DATLAN >= ? AND DATLAN < ?) ");
	sql.append(" AS SALDO ");
	Connection connection = OrsegupsUtils.getSqlConnection("VETORH");
	ResultSet rs = null;
	try
	{
		//System.out.println(sql.toString());
		PreparedStatement pst = connection.prepareStatement(sql.toString());

		pst.setLong(1, numemp);
		pst.setLong(2, numcad);
		pst.setLong(3, tipcol);
		pst.setString(4, datIniFor);
		pst.setString(5, datFimFor);
		pst.setLong(6, numemp);
		pst.setLong(7, numcad);
		pst.setLong(8, tipcol);
		pst.setString(9, datIniFor);
		pst.setString(10, datFimFor);

		rs = pst.executeQuery();

		if (rs.next())
		{
			retorno = rs.getLong("SALDO");
		}
		rs.close();
		connection.close();
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}

	return retorno;
}
public static String retornaDataFormatoSapiensEVetorh(GregorianCalendar data)
{
	String novaData = "";

	if (data == null)
	{
		/*
		 * 31/12/1900 o sapiens\Vetorh entende essa data como vazia
		 */
		data = new GregorianCalendar();
		data.set(data.YEAR, 1900);
		data.set(data.MONTH, 11);
		data.set(data.DAY_OF_MONTH, 31);
		data.set(Calendar.HOUR_OF_DAY, 00);
		data.set(Calendar.MINUTE, 00);
		data.set(Calendar.SECOND, 00);
	}

	if (data != null)
	{
		novaData = NeoUtils.safeDateFormat(data, "yyyy-MM-dd");
		novaData = novaData + " 00:00:00";
	}

	return novaData;
}
	public static Long calculaSaldoFinal(Long saldoMA, Long saldoA){
		Long aPagar = 0L;
		//Calcular saldo mês atual e mês anterior
		if((saldoMA <= 0 && saldoA <= 0) || (saldoMA >= 0 && saldoA >= 0)){
			aPagar = saldoMA;
		}else if (saldoA < 0 && saldoMA >= 0L){
			if(saldoA + saldoMA == 0){
				aPagar = 0L;
			}else{
				if(saldoA + saldoMA > 0){
					aPagar = saldoA + saldoMA;
				}else{
					aPagar =  0L;
				}
			}
		}else{
			if(saldoMA + saldoA == 0){
				aPagar = 0L;
			}else{
				if(saldoMA + saldoA > 0){
					aPagar = 0L;
				}else{
					aPagar = saldoA + saldoMA;
				}
			}
		}
	
		return aPagar;
	}	
}