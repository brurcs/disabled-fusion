package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.processoJuridico.adapter.RetornaAcaoParaNull;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.PostoSemReceitaVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesCustoMaiorReceita1 implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(AbreTarefaSimplesCustoMaiorReceita1.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		NeoPaper papelSolicitante = OrsegupsUtils.getPaper("SolicitanteTarefaCustoMaiorReceita");
		NeoPaper papelExecutor = OrsegupsUtils.getPaper("ExecutorTarefaCustoMaiorReceita");
		
		NeoUser usuarioSolicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelSolicitante);
		NeoUser usuarioExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);
		
		String solicitante = usuarioSolicitante.getCode();
		String executor = usuarioExecutor.getCode();
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 10L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		int regionais[] = {
				0, //ADMINISTRAÇÃO - SEDE
				1, //CAPITAL SEG
				2, //ITAJAÍ
				3, //BRUSQUE
				4, //BLUMENAU
				5, //JOINVILLE
				6, //LAGES
				7, //CRICIÚMA
				8, //GASPAR
				9, //CAPITAL ACL
				10, //CHAPECÓ
				11, //RIO DO SUL
				12, //JARAGUÁ DO SUL
				13, //CURITIBA NORTE
				14, //CASCAVEL
				15, //CURITIBA SUL
				16, //TUBARAO
				17, //PORTO ALEGRE
				18, //TRAMANDAI
				19, //CAMPINAS
				20, //GOIANIA
				21, //PALMAS
				22, //SÃO JOSÉ DO RIO PRETO
				23, //XANGRI-LA
				999, //SEM REGIÃO
		};
		
		for (int regional : regionais)
		{
			Connection conn = PersistEngine.getConnection("DATAWAREHOUSE");
			StringBuilder sql = new StringBuilder();
			PreparedStatement pstm = null;
			ResultSet rs = null;
			final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesCustoMaiorReceita1");
			log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

			try
			{
				//Custo representando 90% ou mais da receita
				List<PostoSemReceitaVO> postosSemReceita = new ArrayList<PostoSemReceitaVO>();

				sql.append(" SELECT Top 10 cpt.NomeCompetencia, tm.NomeTipoMercado, emp.NomeEmpresa, reg.CodRegional, reg.NomeRegional, cli.NomeCliente, pos.NomePosto,                     ");
				sql.append(" cast(ROUND(SUM(r.ValorReceita),2) AS decimal (15,2)) ValorReceita, cast(ROUND(SUM(r.ValorCusto),2) AS decimal (15,2)) ValorCusto, pos.idposto, r.idNegocio     ");
				sql.append(" FROM Fact.Resultado r                                                                                                                                          ");
				sql.append(" INNER JOIN Dim.Competencia cpt ON cpt.IdCompetencia = r.idCompetencia                                                                                          ");
				sql.append(" INNER JOIN Dim.Cliente cli ON cli.IdCliente = r.idCliente                                                                                                      ");
				sql.append(" INNER JOIN Dim.Regional reg ON reg.IdRegional = r.idRegional                                                                                                   ");
				sql.append(" INNER JOIN Dim.Posto pos ON pos.IdPosto = r.idPosto                                                                                                            ");
				sql.append(" INNER JOIN Dim.Empresa emp ON emp.IdEmpresa = r.idEmpresa                                                                                                      ");
				sql.append(" INNER JOIN Dim.TipoMercado tm ON tm.IdTipoMercado = r.idMercado                                                                                                ");
				sql.append(" WHERE r.idNegocio IN (2, 3) AND reg.codRegional = ?                                                                                                            ");
				sql.append(" AND r.idMercado = 1 AND emp.IdEmpresa not in (2)                                                                                                               ");
				sql.append(" AND cpt.NomeCompetencia = RIGHT(CONVERT(VARCHAR(10), DATEADD(MONTH, -2, GETDATE()), 103), 7)                                                                   ");
				//Classificaçõs de serviço extra para não abrir tarefa
				sql.append(" AND pos.codPosto not in( '16150000100201000101','16150000100201000102','16150000100201000103','16150000100201000104',   										");
				sql.append("                          '16150000100201000105','16150000100201000106','16150000100201000107','16150000100201000108',                                          ");
				sql.append("		                  '16150000100201000109','16150000100201000110','16150000100201000113','16150000100201000118',                                          ");
				sql.append("                          '16150000100201000119')																												");
				sql.append(" GROUP BY cpt.NomeCompetencia, tm.NomeTipoMercado, emp.NomeEmpresa, reg.CodRegional, reg.NomeRegional, cli.NomeCliente, pos.NomePosto, pos.idposto, r.idNegocio ");
				sql.append(" HAVING (SUM(r.ValorCusto)/nullif(SUM(r.ValorReceita),0)) >= 0.9																									");
				sql.append(" ORDER BY cpt.NomeCompetencia, tm.NomeTipoMercado, emp.NomeEmpresa, reg.NomeRegional, cli.NomeCliente 															");

				pstm = conn.prepareStatement(sql.toString());
				pstm.setInt(1, regional);
				rs = pstm.executeQuery();

				while (rs.next())
				{
					PostoSemReceitaVO posto = new PostoSemReceitaVO();

					posto.setCompetencia(rs.getString("NomeCompetencia"));
					posto.setTipoMercado(rs.getString("NomeTipoMercado"));
					posto.setNomeEmpresa(rs.getString("NomeEmpresa"));
					posto.setNomeRegional(rs.getString("NomeRegional"));
					posto.setNomeCliente(rs.getString("NomeCliente"));
					posto.setNomePosto(rs.getString("NomePosto"));
					posto.setRegionalPosto(rs.getLong("CodRegional"));
					posto.setValorCusto(rs.getBigDecimal("ValorCusto"));
					posto.setValorReceita(rs.getBigDecimal("ValorReceita"));
					posto.setCodigoNegocio(rs.getLong("idNegocio"));

					postosSemReceita.add(posto);
				}

				IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
				if (postosSemReceita != null && !postosSemReceita.isEmpty())
				{
					
					executor = retornaGerenteResponsavel(regional).getCode(); 
					
					ListIterator<PostoSemReceitaVO> i = postosSemReceita.listIterator();
					List<PostoSemReceitaVO> listaPostosCliente = new ArrayList<PostoSemReceitaVO>();
					while (i.hasNext())
					{
						PostoSemReceitaVO atual = i.next();
						PostoSemReceitaVO proximo = null;
						listaPostosCliente.add(atual);

						if (i.hasNext())
						{
							proximo = i.next();
							i.previous();

							if (!atual.getNomeCliente().equals(proximo.getNomeCliente()))
							{				
								String descricao = montaDescricaoTarefaSimples(listaPostosCliente);
								String titulo = "Analisar Cliente com custo maior que 90% da receita - " + atual.getNomeCliente();
								String codigoTarefa = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

								log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " - Aberto a Tarefa Simples: " + codigoTarefa);

								listaPostosCliente.clear();
							}
						}
						else
						{

							String descricao = montaDescricaoTarefaSimples(listaPostosCliente);
							String titulo = "Analisar Cliente com custo maior que 90% da receita - " + atual.getNomeCliente();
							String codigoTarefa = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

							log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + " - Aberto a Tarefa Simples: " + codigoTarefa);
						}
					}
				}
				else
				{
					log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy") + "A consulta não retornou resultado.");
				}
			}
			catch (Exception e)
			{
				log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita");
				e.printStackTrace();
			}
			finally
			{
				try
				{
					rs.close();
					pstm.close();
					conn.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}

				log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Postos Sem Receita - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
		}
	}

	private NeoUser retornaGerenteResponsavel(int regional) {
		
 		EntityWrapper ewFormulario = null;
		NeoObject noFormulario = null;
		NeoPaper papelResponsavel = null;
		NeoUser user = null;
		
		try {
			
			noFormulario = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("ResponsaveisCustoMaiorReceita"), new QLEqualsFilter("_vo.regional.codigo", regional)); 
			
			if(NeoUtils.safeIsNotNull(noFormulario)) {
				ewFormulario = new EntityWrapper(noFormulario);
	    	    papelResponsavel = ewFormulario.findGenericValue("responsavel");
	    	    user = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelResponsavel);
			}
		
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### "+this.getClass().getSimpleName()+" - Erro ao retornar o gerente responsável pela regional "+regional);
		}
		
		return user;
	}

	public String montaDescricaoTarefaSimples(List<PostoSemReceitaVO> postos)
	{
		String descricao = "";

		String nomeCliente = postos.get(0).getNomeCliente();
		descricao = "Foi identificado que o cliente " + nomeCliente + " teve custo apropriado em pelo menos 90% da receita.<br/><br/>";

		descricao = descricao + "<table class=\"gridbox\" cellpadding=\"0\" cellspacing=\"0\" style=\"empty-cells: show\">";
		descricao = descricao + "<thead>";
		descricao = descricao + "<tr><th colspan=7>Tabela de Postos Sem Receita</th></tr>";
		descricao = descricao + "<tr>";
		descricao = descricao + "<th>Competência</th>";
		descricao = descricao + "<th>Mercado</th>";
		descricao = descricao + "<th>Regional</th>";
		descricao = descricao + "<th>Cliente</th>";
		descricao = descricao + "<th>Posto</th>";
		/*descricao = descricao + "<th>Vlr. Resultado</th>";*/
		descricao = descricao + "<th>Vlr. Receita</th>";
		descricao = descricao + "<th>Vlr. Custo</th>";
		descricao = descricao + "</tr>";
		descricao = descricao + "</thead>";
		descricao = descricao + "<tbody>";

		for (PostoSemReceitaVO posto : postos)
		{
			descricao = descricao + "<tr>";
			descricao = descricao + "<td>" + posto.getCompetencia() + "</td>";
			descricao = descricao + "<td>" + posto.getTipoMercado() + "</td>";
			descricao = descricao + "<td>" + posto.getNomeRegional() + "</td>";
			descricao = descricao + "<td>" + posto.getNomeCliente() + "</td>";
			descricao = descricao + "<td>" + posto.getNomePosto() + "</td>";
			descricao = descricao + "<td> R$ " + posto.getValorReceita() + "</td>";
			descricao = descricao + "<td> R$ " + posto.getValorCusto() + "</td>";
			descricao = descricao + "</tr>";
		}

		descricao = descricao + "</tbody>";
		descricao = descricao + "</table>";

		return descricao;
	}
}
