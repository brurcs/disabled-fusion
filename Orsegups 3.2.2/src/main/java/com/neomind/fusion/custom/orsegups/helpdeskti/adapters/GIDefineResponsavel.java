package com.neomind.fusion.custom.orsegups.helpdeskti.adapters;

import java.util.Set;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task; 
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoUtils;

/*
 * @author fernando.rebelo
 * 
 * Adapter entre as atividades de Atender Chamado e Realizar Triagem
 * Valida informações e retorna o usuário/papel que irá receber a tarefa 
 */
public class GIDefineResponsavel implements AdapterInterface{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		boolean transferirAtendimento = NeoUtils.safeBoolean(processEntity.findValue("transferirAtendimento"));
		/*
		 * Verifica quem Originou a tarefa que avançou para este adapter
		 * 
		 * quando for solicitado aprovação, a partir da tarefa 'Atender Chamado', 
		 * deve retornar para o usuário que finalizou o atendimento
		 * 
		 * Essa lógica é utilizada no caso de, na tarefa de 'Realizar Triagem',
		 * ser escolhido um papel/grupo como responsável
		 * pois se definir um usuário específico como responsável, essa lógica não é necessária
		 * 
		 * se for uma transferencia, desconsiderar
		 * pois o responsável vai ser setado abaixo
		 */		
		if(!transferirAtendimento)
		{		
			Set<Activity> actsOrigin = origin.getActivity().getIncoming();
			
			for(Activity origemAlvo : actsOrigin)
			{
				if(origemAlvo.getCode().equals("Atender Chamado"))
				{
					TaskInstance instance = origemAlvo.getInstance();
					
					if(instance != null)
						processEntity.findField("responsavel").setValue(instance.getOwner());
				}
			}
		}
		/*
		 * para o caso de ter sido solicitado aprovação na triagem
		 * setamos para false aqui para que, se necessário, enviar a aprovação novamente na atividade seguinte
		 * seja feito o controle correto
		 */
		processEntity.findField("retornaTriagem").setValue(false);
		 
		/*
		//evitar que na atividade seguinte o usuário avance com aprovação, sem querer
		if(NeoUtils.safeBoolean(processEntity.findField("aprovacao").getValue()))
		{
			processEntity.findField("aprovacao").setValue(false);
		}
		*/
		
		/*
		 * se optou por transferir a atividade para outro(s) responsável(eis)
		 */
		
		if(transferirAtendimento && processEntity.findValue("novoResponsavel") != null)
		{
			processEntity.findField("responsavel").setValue(processEntity.findValue("novoResponsavel"));
			//para evitar que o usuário transfira sem intenção
			processEntity.findField("transferirAtendimento").setValue(false);
		}
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
