package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCValidaDiasUteis implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazoAtendimento").getValue();
		GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
		if (!OrsegupsUtils.isWorkDay(prazo))
        {
			throw new WorkflowException("Prazo informado deve ser um dia útil!");
        }
		else if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
		{
			throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
		}
	}
}