package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaHoraExtra implements CustomJobAdapter {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(RotinaHoraExtra.class);

    @Override
    public void execute(CustomJobContext arg0) {
	
	Connection conn = PersistEngine.getConnection("FUSIONPROD"); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaHoraExtra");
	log.warn("##### INICIO AGENDADOR DE TAREFA: RotinaHoraExtra - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: RotinaHoraExtra - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss")); 
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try
	{	    
		StringBuffer  varname1 = new StringBuffer();
		
		varname1.append("      select distinct ");
		varname1.append("             left(REPLICATE('0',12-len(fun.numcpf))+cast(fun.numcpf as varchar),3)+'.'+right(left(REPLICATE('0',12-len(fun.numcpf))+cast(fun.numcpf as varchar),6),3)+'.'+right(left(REPLICATE('0',12-len(fun.numcpf))+cast(fun.numcpf as varchar),9),3)+'-'+right(left(fun.numcpf,12),2) as cpfFormatado, ");
		varname1.append("		      fun.nomfun, ");
		varname1.append("		      fun.numemp, ");
		varname1.append("		      fun.numcad, ");
		varname1.append("		      fun.numcpf, ");
		varname1.append("		      Convert(varchar(10), sit.datApu,103) as DataHoraExtra, ");
		varname1.append("		      sit.datApu, ");
		varname1.append("		      orn.USU_CodReg, ");
		varname1.append("		      orn.USU_LotOrn, ");
		varname1.append("		      orn.NomLoc, ");
		varname1.append("		      car.titcar ");
		varname1.append("        from [FSOODB04\\SQL02].VETORH.dbo.R066SIT sit ");
		varname1.append("  inner join [FSOODB04\\SQL02].VETORH.dbo.R034FUN fun on sit.NumEmp = fun.NumEmp ");
		varname1.append("				           							  and sit.TipCol = fun.TipCol ");
		varname1.append("				           							  and sit.NumCad = fun.NumCad ");
		varname1.append("  inner join [FSOODB04\\SQL02].VETORH.dbo.R016ORN orn on fun.numloc = orn.numloc ");
		varname1.append("  inner join [FSOODB04\\SQL02].VETORH.dbo.R038HES hes on sit.numEmp = hes.numEmp ");
		varname1.append("				    								  and sit.numCad = hes.numCad ");
		varname1.append("				    								  and sit.tipCol = hes.tipCol ");
		varname1.append("				    								  and hes.DatAlt = (SELECT MAX (DATALT) ");
		varname1.append("				                    								      FROM [FSOODB04\\SQL02].VETORH.dbo.R038HES TABELA001 ");
		varname1.append("				                    								     WHERE TABELA001.NUMEMP = hes.NUMEMP ");
		varname1.append("				                    								       AND TABELA001.TIPCOL = hes.TIPCOL ");
		varname1.append("				                    								       AND TABELA001.NUMCAD = hes.NUMCAD ");
		varname1.append("				                    								       AND TABELA001.DATALT <= GetDate()) ");
		varname1.append("  inner join [FSOODB04\\SQL02].VETORH.dbo.R024CAR car on fun.codcar = car.codcar ");
		varname1.append("  													  and fun.estcar = car.estcar ");
		varname1.append("       where fun.usu_ColSde = 'S' ");
		varname1.append("         and sit.datApu > DATEADD(Day,-2, getDate()) ");
		varname1.append("         and sit.datApu < DATEADD(Day,-1, getDate()) ");
		varname1.append("   and sit.codSit in (311,312,321,322,616,260,267,265,266,309,254,44,46,257,310,301,253,42,40,43,41,264,302,256,263,303,255,304, ");
		varname1.append("		                           258,249,252,261,262,451,452) ");		
		varname1.append("         and fun.tipCon <> 5 ");
		varname1.append("         and hes.codesc <> 3228 "); 
		varname1.append("	      and fun.usu_tipAdm = 1 ");
		varname1.append("    order by fun.nomfun");
	    
	    pstm = conn.prepareStatement(varname1.toString()); 
	    rs = pstm.executeQuery();	    

	    while (rs.next()) {
	    	
	    	String cargo = rs.getString("titcar");
    		String nomFun = rs.getString("nomfun");
    		Integer numemp = rs.getInt("numemp");
    		Integer numcad = rs.getInt("numcad");
    		String numcpf = rs.getString("numcpf");
    		String dataHoraExtra = rs.getString("DataHoraExtra");

    		String tituloTarefa = "Hora Extra - " + numemp + "/" + numcad + " - " + nomFun;
    		String descricaoTarefa = "<p>Prezado Gestor, justifique o motivo da hora extra do colaborador <b>" + nomFun + "</b>, "+cargo+" em <b>" + dataHoraExtra + "</b>. </p>";
    		descricaoTarefa += "Departamento: "+ rs.getString("USU_LotOrn");
    		descricaoTarefa += retornaHoras(numcpf);
    		descricaoTarefa += retornaHistorico("%Hora Extra -%"+nomFun+"%");
    		
    		Long codReg = rs.getLong("USU_CodReg");
    		String lotacao = rs.getString("USU_LotOrn");
    		
    		abrirTarefa(rs.getInt("numemp"), rs.getInt("numcad"), nomFun,  tituloTarefa, descricaoTarefa, codReg, lotacao);
	    	
	    }
	    
	}catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1"); 
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally {
	    try {
		rs.close();
		pstm.close();
		conn.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }

	    log.warn("##### FIM AGENDADOR DE TAREFA: RotinaHoraExtra - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    System.out.println("##### FIM AGENDADOR DE TAREFA: RotinaHoraExtra - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }
    
	private String retornaHoras(String _numcpf) {

		String descricao = "";

		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try {
			StringBuffer  varname1 = new StringBuffer();
			
			varname1.append("select ('Total: <b>'+CAST(sit.qtdHor as varchar)+' minuto(s) </b> - '+(select a.desSit from [FSOODB04\\SQL02].VETORH.dbo.R010SIT a where a.codSit = sit.codSit)) as desHora ");
			varname1.append("        from [FSOODB04\\SQL02].VETORH.dbo.R066SIT sit ");
			varname1.append("  inner join [FSOODB04\\SQL02].VETORH.dbo.R034FUN fun on sit.NumEmp = fun.NumEmp ");
			varname1.append("				           							and sit.TipCol = fun.TipCol ");
			varname1.append("				           							and sit.NumCad = fun.NumCad ");
			varname1.append("  inner join [FSOODB04\\SQL02].VETORH.dbo.R016ORN orn on fun.numloc = orn.numloc ");
			varname1.append("  inner join [FSOODB04\\SQL02].VETORH.dbo.R038HES hes on sit.numEmp = hes.numEmp ");
			varname1.append("				    								and sit.numCad = hes.numCad ");
			varname1.append("				    								and sit.tipCol = hes.tipCol ");
			varname1.append("				    								and hes.DatAlt = (SELECT MAX (DATALT) ");
			varname1.append("				                    								    FROM [FSOODB04\\SQL02].VETORH.dbo.R038HES TABELA001 ");
			varname1.append("				                    								   WHERE TABELA001.NUMEMP = hes.NUMEMP ");
			varname1.append("				                    								     AND TABELA001.TIPCOL = hes.TIPCOL ");
			varname1.append("				                    								     AND TABELA001.NUMCAD = hes.NUMCAD ");
			varname1.append("				                    								     AND TABELA001.DATALT <= GetDate()) ");
			varname1.append(" where fun.usu_ColSde = 'S' ");
			varname1.append("   and sit.datApu > DATEADD(Day,-2, getDate()) ");
			varname1.append("   and sit.datApu < DATEADD(Day,-1, getDate()) ");
			varname1.append("   and sit.codSit in (311,312,321,322,616,260,267,265,266,309,254,44,46,257,310,301,253,42,40,43,41,264,302,256,263,303,255,304, ");
			varname1.append("		                           258,249,252,261,262,451,452) ");
			varname1.append("   and fun.tipCon <> 5 ");
			varname1.append("         and hes.codesc <> 3228 ");
			varname1.append("	and fun.usu_tipAdm = 1 ");
			varname1.append("   and fun.numcpf = ");
			varname1.append(_numcpf);
			varname1.append("  order by fun.nomfun"); 

			pstm = conn.prepareStatement(varname1.toString());
			rs = pstm.executeQuery();
			
			while (rs.next()) {
				descricao += "<p>" + rs.getString("desHora");					
			}
			
			if (!descricao.contains("%"))
			{
				descricao += " %";
			}
			
			return descricao;
			
		} catch (Exception e) {
			return "</br></br>Não foi possível recuperar o registro de hora";
		}
	}
	
	private String retornaHistorico(String string) {

		String descricao = "";

		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try {			
			
			StringBuffer  varname1 = new StringBuffer();
			
			varname1.append("select top(5) p.code, ");
			varname1.append("			       Convert(varchar(10), p.startDate,103) as dataAbertura, t.neoId ");
			varname1.append("			  from d_tarefa t ");
			varname1.append("			 inner join WFProcess p on t.wfprocess_neoId = p.neoId ");
			varname1.append(" where t.titulo like '");
			varname1.append(string);
			varname1.append("' ");
			varname1.append("			   and t.neoId > 1072220268 ");
			varname1.append("			   and t.solicitante_neoId = 125951 "); // neoId do usuario do sistema do sr.Dilmo, utilizado para não trazer outras tarefas de Hora Extra
			varname1.append("			   and p.processState != 2	");
			varname1.append("			  order by t.neoId desc");

			pstm = conn.prepareStatement(varname1.toString());
			rs = pstm.executeQuery();

			descricao += "</br></br>Histórico de Tarefas Simples relacionadas à Hora Extra:</br><table>"
					+ "<tr>"
					+ "<td></td>"
					+ "<td>Cód.Tarefa</td>"
					+ "<td>Data</td>"
					+ "<td>Descrição</td>"
					+ "<td>Primeira Resposta</td>"
					+ "</tr>";
			
			int contador = 0;
			
			while (rs.next()) {		
				Long historicoId = rs.getLong("neoId");
				
				NeoObject noHistorico = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("Tarefa"), historicoId);
				EntityWrapper wHistorico = new EntityWrapper(noHistorico);
				
				String primeiraResposta = "";
				String descricaoSolicitacao = "";
				List<NeoObject> listRegistroAtividades = wHistorico.findGenericValue("registroAtividades");
				
				sortId(listRegistroAtividades);
				
				for (int i = 0; i < listRegistroAtividades.size(); i++)
				{
					if (i == 0)
					{
						descricaoSolicitacao = new EntityWrapper(listRegistroAtividades.get(i)).findGenericValue("descricao");
						descricaoSolicitacao = descricaoSolicitacao.substring(descricaoSolicitacao.indexOf("<p>Prezado"), descricaoSolicitacao.indexOf("%")+1);
					}
					else if (i == 1)
					{
						primeiraResposta = new EntityWrapper(listRegistroAtividades.get(i)).findGenericValue("descricao");
						break;
					}
				}
				
				descricao += "<tr>"
						+ "<td><a href=\"javascript:ellist_tarefasRelacionadas__.viewItem(" + 
				          rs.getString("neoId") + ");\" style=\"margin-right:1px;\"><img class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a></td>"
						+ "<td>" + rs.getInt("code")
						+ "</td>"
						+ "<td>" + rs.getString("dataAbertura")
						+ "</td>"
						+ "<td>" + descricaoSolicitacao + "</td>"
						+ "<td>" + primeiraResposta + "</td>"
						+ "</tr>";		
				contador++;
			}
			descricao += "</table>";
			
			if (contador == 0)
				descricao = "</br></br>Não há registros anteriores de hora extra para este colaborador.";
			
			return descricao;
			
		} catch (Exception e) {
			return "</br></br>Não foi possível recuperar o Histórico";
		}
	}

	private void abrirTarefa(Integer _numEmp, Integer _numCad, String _nomFun, String _titulo, String _descricaoTarefa, Long _regional, String _lotacao) throws Exception {
	
    	NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "dilmoberger" ) );
		NeoUser usuarioResponsavel = null; //PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
		
		NeoPaper papel = null;
		try {
			papel = getPapelJustificarR022(_regional, _lotacao);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuarioResponsavel = user;
				break;
			}
		}
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		if (usuarioResponsavel == null){
			
			usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "diogo.silva" ) );	
			usuarioResponsavel = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "fernanda.martins" ) );
			_titulo = "Corrigir Gestor. "+_titulo;
		    _descricaoTarefa = "Solicitamos informação para ajustar Setor do Colaborador: </br>Colaborador: "+_numEmp+"/"+_numCad+" - "+_nomFun+"</br>Regional: "+_regional+"</br>Lotação: "+_lotacao +"</br>Quem é seu Gestor?";		    
		}
		
		System.out.println(usuarioSolicitante.getCode()+";"+usuarioResponsavel.getCode()+";"+_nomFun+";"+_titulo+";"+_descricaoTarefa+";");
		iniciarTarefaSimples.abrirTarefa(usuarioSolicitante.getCode(), usuarioResponsavel.getCode(), _titulo, _descricaoTarefa, "1", "hadouken", prazo); 
    }
    
    public static NeoPaper getPapelJustificarR022(Long codreg, String lotacao) {
    	EntityWrapper ewResponsavelRegional = null;
    	NeoPaper papelResponsavel = null;
    	NeoObject responsavelRegional = null;
    	NeoUser user = null;
    	
    	QLGroupFilter groupFilter = new QLGroupFilter("AND");
    	groupFilter.addFilter(new QLEqualsFilter("codreg", codreg));
    	groupFilter.addFilter(new QLEqualsFilter("nomloc", lotacao));
    	responsavelRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("responsaveisRegionais"), groupFilter);

    	if (responsavelRegional == null) {
    	    responsavelRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("responsaveisRegionais"), new QLEqualsFilter("codreg", codreg));
    	}

    	if (NeoUtils.safeIsNotNull(responsavelRegional)) {

    	    ewResponsavelRegional = new EntityWrapper(responsavelRegional);
    	    papelResponsavel = ewResponsavelRegional.findGenericValue("responsavelR022");
    	    user = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelResponsavel);
    	    
    	} 

    	return papelResponsavel;

    }
    
    public void sortId(List<NeoObject> list)
    {
    	//ordena os registros pelo neoId para buscar o primeiro registro da atividade
		Collections.sort(list, new Comparator<NeoObject>()
		{
			@Override
			public int compare(NeoObject o1, NeoObject o2)
			{
				return o1.getNeoId() < o2.getNeoId() ? -1 : 1;
			}
		});
    }
}
