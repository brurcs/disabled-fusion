package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ChipGPRSVO
{
	private String numerotelefone;
	private String iccId;
	private String codigoOperadora;
	
	public String getNumerotelefone()
	{
		return numerotelefone;
	}
	public void setNumerotelefone(String numerotelefone)
	{
		this.numerotelefone = numerotelefone;
	}
	public String getIccId()
	{
		return iccId;
	}
	public void setIccId(String iccId)
	{
		this.iccId = iccId;
	}
	public String getCodigoOperadora()
	{
		return codigoOperadora;
	}
	public void setCodigoOperadora(String codigoOperadora)
	{
		this.codigoOperadora = codigoOperadora;
	}
}
