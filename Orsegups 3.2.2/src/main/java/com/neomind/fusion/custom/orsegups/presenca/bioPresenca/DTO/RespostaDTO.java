package com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO;

import java.util.List;

/**
 * 
 * @author lucas.alison
 *
 */

public class RespostaDTO {
    private Integer status;
    private String msg;
    private List<ErroDTOPai> erros;
    
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public List<ErroDTOPai> getErros() {
        return erros;
    }
    public void setErros(List<ErroDTOPai> erros) {
        this.erros = erros;
    }
    
    
}
