package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class J001ValidaDiaUtil implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
		GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("prazoDocReg").getValue();
		int diaSemana = 0;

		diaSemana = prazo.get(7);

		if ((diaSemana == 1) || (diaSemana == 7))
		{
			throw new WorkflowException("Prazo informado deve ser um dia útil!");
		}

		if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
		{
			throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
		}
	}
}