package com.neomind.fusion.custom.orsegups.scheduler;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class TesteAendadorMinutos implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext ctx)
	{
		InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("registroRotinaMinutos");
		NeoObject emailHis = infoHis.createNewInstance();
		PersistEngine.persist(emailHis);
		PersistEngine.commit(true);
	
	}

}
