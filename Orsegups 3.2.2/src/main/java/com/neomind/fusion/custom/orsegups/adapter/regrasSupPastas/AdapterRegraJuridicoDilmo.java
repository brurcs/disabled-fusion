package com.neomind.fusion.custom.orsegups.adapter.regrasSupPastas;

import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskSummary;
import com.neomind.fusion.workflow.task.rule.TaskRuleAdapter;
import com.neomind.fusion.workflow.task.rule.TaskRuleContext;

public class AdapterRegraJuridicoDilmo implements TaskRuleAdapter{

	@Override
	public boolean isTriggered(TaskRuleContext context){
	    	
	    boolean retorno = false;
	    
	    Task t = null;
	    TaskSummary	ts = null;
	    try{
		
		if(context.getTask() instanceof  Task){
		    t = (Task) context.getTask();
		}else{
		    ts = (TaskSummary) context.getTask();
		}

		retorno = ((t != null && t.getProcessName().equals("J002 - Processo Juridico")) || (ts != null && ts.getProcessName().equals("J002 - Processo Juridico")));

	    }catch(Exception e){
		System.out.println("Erro ao direcionar tarefa do Jurídico para subpastas. Mensagem: "+e.getMessage());
		e.printStackTrace();
	    }

	    return retorno;

	}
}

