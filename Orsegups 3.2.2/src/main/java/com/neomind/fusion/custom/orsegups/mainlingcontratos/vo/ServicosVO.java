package com.neomind.fusion.custom.orsegups.mainlingcontratos.vo;

public class ServicosVO
{

	private int codigoServico;
	private String descricaoServico;
	private String abreviatura;

	public String getAbreviatura()
	{
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura)
	{
		this.abreviatura = abreviatura;
	}

	public int getCodigoServico()
	{
		return codigoServico;
	}

	public void setCodigoServico(int codigoServico)
	{
		this.codigoServico = codigoServico;
	}

	public String getDescricaoServico()
	{
		return descricaoServico;
	}

	public void setDescricaoServico(String descricaoServico)
	{
		this.descricaoServico = descricaoServico;
	}

}
