package com.neomind.fusion.custom.orsegups.servlets.rh01admissao;

import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;

@WebServlet(name = "rh01VisualizarArquivo", urlPatterns = {"/servlet/rh01visualizar"})
public class rh01VisualizarArquivo extends HttpServlet
{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		//super.doGet(request, response);
		provessaImagem(request, response);
	}
	
	private void provessaImagem(HttpServletRequest request, HttpServletResponse response)
		throws IOException
	{
		Long fileNeoId = NeoUtils.safeLong(request.getParameter("id"));
		if (fileNeoId != null)
		{
			NeoFile neoFile = PersistEngine.getObject(NeoFile.class, fileNeoId);
			if (neoFile != null)
			{
				String extensaoArquivo = FilenameUtils.getExtension(neoFile.getName());
				
				if(extensaoArquivo.equalsIgnoreCase("pdf"))
				{
					OutputStream outStream = response.getOutputStream();
					File file = neoFile.getAsFile();
					FileInputStream inStream = new FileInputStream(file);
					String contentDisposition = "inline; filename=" + neoFile.getName();
					
					response.setContentType(neoFile.getContentType());
					response.setHeader("Content-Disposition", contentDisposition);
					response.setContentLength((int) file.length());
					
					byte[] buffer = new byte[4096];
					int bytesRead = -1;
					
					while ((bytesRead = inStream.read(buffer)) != -1) {
						outStream.write(buffer, 0, bytesRead);
					}
					
					inStream.close();
					outStream.close();
				}
				else
				{
					String imgSrc = PortalUtil.getBaseURL() + "file/download/" + neoFile.getNeoId();
					response.getWriter().print("<html><body><img src=\"" + imgSrc + "\" /></body></html>");
				}
			}
			else
			{
				//TODO: Retornar mensagem de erro (Arquivo para o id informado não foi encontrado)
			}
		}
		else
		{
			//TODO: Retornar mensagem de erro (Id não informado ou inválido)
		}
	}
}
