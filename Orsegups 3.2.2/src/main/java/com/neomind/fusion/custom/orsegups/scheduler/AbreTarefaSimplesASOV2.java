package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesASOV2 implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesASOV2");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO V2 - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		GregorianCalendar dataCorte = new GregorianCalendar();
		dataCorte = OrsegupsUtils.getSpecificWorkDay(dataCorte, -30L);
		dataCorte.set(Calendar.HOUR_OF_DAY, 0);
		dataCorte.set(Calendar.MINUTE, 0);
		dataCorte.set(Calendar.SECOND, 0);
		String dataCorteString = NeoUtils.safeDateFormat(dataCorte, "yyyy-MM-dd HH:mm:ss");

		try
		{

			sql.append("         Select fun.numemp, ");
			sql.append("                fun.tipcol, ");
			sql.append("                fun.numcad, ");
			sql.append("                fun.nomfun, ");
			sql.append("                fun.datadm, ");
			sql.append("                fun.numcpf, ");
			sql.append("                DATEDIFF(year , fun.datnas , getdate()) as idade, ");
			sql.append("                reg.USU_CodReg, ");
			sql.append("                reg.USU_NomReg, ");			
			sql.append("			  	cpl.ultexm, ");
			sql.append("			  	DATEADD(year,1,cpl.ultexm) as proexm, ");
			sql.append("			  	DATEADD(year,2,cpl.ultexm) as proexm2, ");
			sql.append("			  	car.TitRed, ");
			sql.append("			  	orn.USU_LotOrn, ");
			sql.append("			  	orn.NomLoc ");
			sql.append("		   From [FSOODB04\\SQL02].VETORH.DBO.R034FUN fun ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.R034CPL cpl On cpl.numemp = fun.numemp ");
			sql.append("	                                                   And cpl.tipcol = fun.tipcol ");
			sql.append("	                                                   And cpl.numcad = fun.numcad ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.R024CAR car On car.EstCar = fun.EstCar ");
			sql.append("	                                                   And car.CodCar = fun.CodCar ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.R016ORN orn On orn.numloc = fun.numloc ");
			sql.append("	                                                   And orn.taborg = fun.taborg ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg ");
			sql.append("		  Where fun.tipcol = 1 ");
			sql.append("		    And fun.sitafa in (1,2) ");
			sql.append("		    And cpl.ultexm <> '1900-12-31' ");
			sql.append("		    And Case When DATEDIFF(year , fun.datnas , getdate()) > 45 ");
			sql.append("			 		 Then DATEADD(month,11,cpl.ultexm) ");
			sql.append("			 		 Else DATEADD(month,23,cpl.ultexm) ");
			sql.append("			          End < getdate() ");
			sql.append("			And Not Exists (Select 1 ");
			sql.append("			                  From [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaASO tas WITH (NOLOCK) ");
			sql.append("			                 WHERE tas.numcpf = fun.numcpf ");
			sql.append("						       And CAST(floor(CAST(tas.proexm As FLOAT)) As datetime) > CAST(floor(CAST(cpl.ultexm AS FLOAT)) As datetime)) ");
			sql.append(" UNION ");
			sql.append("	     Select fun.numemp, ");
			sql.append("	            fun.tipcol, ");
			sql.append("	            fun.numcad, ");
			sql.append("	            fun.nomfun, ");
			sql.append("	            fun.datadm, ");
			sql.append("	            fun.numcpf, ");
			sql.append("	            DATEDIFF(year , fun.datnas , getdate()) as idade, ");
			sql.append("	            reg.USU_CodReg, ");
			sql.append("                reg.USU_NomReg, ");
			sql.append("			 	cpl.ultexm, ");
			sql.append("			 	DATEADD(year,1,cpl.ultexm) as proexm, ");
			sql.append("			 	DATEADD(year,2,cpl.ultexm) as proexm2, ");
			sql.append("			 	car.TitRed, ");
			sql.append("			 	orn.USU_LotOrn , ");
			sql.append("			 	orn.NomLoc ");
			sql.append("		   From [FSOODB04\\SQL02].VETORH.DBO.R034FUN fun ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.R034CPL cpl On cpl.numemp = fun.numemp ");
			sql.append("	                                                   And cpl.tipcol = fun.tipcol ");
			sql.append("	                                                   And cpl.numcad = fun.numcad ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.R024CAR car On car.EstCar = fun.EstCar ");
			sql.append("	                                                   And car.CodCar = fun.CodCar ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.USU_T038COBFUN cob On fun.numemp = cob.usu_numemp ");
			sql.append("	                                                          and fun.tipcol = cob.usu_tipcol ");
			sql.append("	                                                          and fun.numcad = cob.usu_numcad ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.R016ORN orn On orn.numloc = cob.usu_numloc ");
			sql.append("	                                                   And orn.taborg = cob.usu_taborg ");
			sql.append("	 Inner Join [FSOODB04\\SQL02].VETORH.DBO.USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg ");
			sql.append("		  where fun.tipcol = 1 ");
			sql.append("		    And fun.sitafa in (1,2) ");
			sql.append("		    And cpl.ultexm <> '1900-12-31' ");
			sql.append("			And Case ");
			sql.append("			 	When DATEDIFF(year , fun.datnas , getdate()) > 45 ");
			sql.append("			 	Then DATEADD(month,11,cpl.ultexm) ");
			sql.append("			 	Else DATEADD(month,23,cpl.ultexm) ");
			sql.append("			     End < getdate() ");
			sql.append("			And Not Exists (Select 1 ");
			sql.append("			                  From [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaASO tas WITH (NOLOCK) ");
			sql.append("			                 WHERE tas.numcpf = fun.numcpf ");
			sql.append("			And CAST(floor(CAST(tas.proexm As FLOAT)) As datetime) > CAST(floor(CAST(cpl.ultexm AS FLOAT)) As datetime)) ");
			sql.append("			And ((cob.usu_datfim > DATEADD(DAY,-30,GETDATE())) or (usu_datinccob > DATEADD(DAY,-30,GETDATE()))) ");
			sql.append("	   group By fun.numemp, ");
			sql.append("	            fun.tipcol, ");
			sql.append("	            fun.numcad, ");
			sql.append("	            fun.nomfun, ");
			sql.append("	            fun.datadm, ");
			sql.append("	            fun.numcpf, ");
			sql.append("	            DATEDIFF(year , fun.datnas , getdate()), ");
			sql.append("	            reg.USU_CodReg, ");
			sql.append("	            reg.USU_NomReg, ");
			sql.append("	            cpl.ultexm, ");
			sql.append("	            car.TitRed, ");
			sql.append("	            orn.USU_LotOrn, ");
			sql.append("	            orn.NomLoc ");
			sql.append("	   Order By fun.nomfun, ");
			sql.append("	            cpl.ultexm DESC");
			
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			
			String sNomFun = "";

			while (rs.next())
			{
				String nomFum = ""+rs.getString("NomFun");
				
				if (!nomFum.equals(sNomFun)) {
					
					String ultimoExame = NeoDateUtils.safeDateFormat(rs.getDate("ultexm"), "dd/MM/yyyy");
					String proximoExame = NeoDateUtils.safeDateFormat(rs.getDate("proexm"), "dd/MM/yyyy");
					String proximoExame2 = NeoDateUtils.safeDateFormat(rs.getDate("proexm2"), "dd/MM/yyyy");
					
					NeoPaper papelSolicitante = OrsegupsUtils.getPaper("SolicitanteTarefaSimplesAso");
					NeoUser usuarioSolicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelSolicitante);
					
					NeoPaper papelExecutor = OrsegupsUtils.getPaper("ExecutorTarefaSimplesAso");
					NeoUser usuarioExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);
					
					String regional = rs.getString("USU_NomReg");
					
					String titulo = "Agendar o próximo ASO - " + regional + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun");
					
					Integer idade = 0;
					idade = rs.getInt("idade");
					
					String descricao = "";
					descricao = descricao + "<strong>CPF:</strong> " + rs.getString("numcpf") + "<br>";
					descricao = descricao + "<strong>Colaborador:</strong> " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun") + "<br>";
					descricao = descricao + "<strong>Regional:</strong> "+ regional;
					descricao = descricao + "<strong>Idade:</strong> " + idade + "<br>";
					descricao = descricao + " <strong>Cargo:</strong> " + rs.getString("TitRed") + "<br>";
					descricao = descricao + " <strong>Último Exame:</strong> " + ultimoExame + "<br>";
					
					if (idade > 45) {
						descricao = descricao + " <strong>Próximo Exame:</strong> " + proximoExame + "<br>";
					} else {
						descricao = descricao + " <strong>Próximo Exame:</strong> " + proximoExame2 + "<br>";
					}
					
					GregorianCalendar prazo = new GregorianCalendar();
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 20L);
					prazo.set(Calendar.HOUR_OF_DAY, 23);
					prazo.set(Calendar.MINUTE, 59);
					prazo.set(Calendar.SECOND, 59);
					
					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					String tarefa = iniciarTarefaSimples.abrirTarefa(usuarioSolicitante.getCode(), usuarioExecutor.getCode(), titulo, descricao, "1", "sim", prazo);
					
					InstantiableEntityInfo tarefaAS = AdapterUtils.getInstantiableEntityInfo("tarefaASO");
					NeoObject no = tarefaAS.createNewInstance();
					EntityWrapper wrapper = new EntityWrapper(no);

					GregorianCalendar proexm = new GregorianCalendar();
					proexm.setTime(rs.getDate("proexm"));
					
					GregorianCalendar proexm2 = new GregorianCalendar();
					proexm2.setTime(rs.getDate("proexm2"));

					wrapper.findField("numemp").setValue(rs.getLong("numemp"));
					wrapper.findField("tipcol").setValue(rs.getLong("tipcol"));
					wrapper.findField("numcad").setValue(rs.getLong("numcad"));
					wrapper.findField("numcpf").setValue(rs.getLong("numcpf"));
					if (idade > 45) {
						wrapper.findField("proexm").setValue(proexm);
					} else {
						wrapper.findField("proexm").setValue(proexm2);
					}
					wrapper.findField("tarefa").setValue(tarefa);
					PersistEngine.persist(no);

					log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO V2 - Responsavel " + usuarioExecutor.getCode() + " Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoUtils.safeDateFormat(rs.getDate("DatAdm"), "dd/MM/yyyy"));
					
					Thread.sleep(1000);
					
					sNomFun = nomFum;
				} 
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples ASO V2");
			System.out.println("["+key+"] ##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples ASO V2");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO V2 - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
