package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class AbrirOSEmLote implements CustomJobAdapter {
    
    @Override
    public void execute(CustomJobContext ctx) {
	
    	ResultSet rs = null;
    	PreparedStatement pstm = null;
    	Connection conn = PersistEngine.getConnection("SIGMA90"); 
    	System.out.println("##### INICIO AGENDADOR DE TAREFA: AbrirOSEmLote - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
    	try {    		
    		StringBuffer  varname1 = new StringBuffer();
    		
    		varname1.append("select CD_CLIENTE, ");
    		varname1.append("       CD_TECNICO_RESPONSAVEL ");
    		varname1.append("  from dbCentral ");
    		varname1.append(" where CD_CLIENTE in ");
    		varname1.append("(241193,262236,263283,229517,228402,261933,257467,217860,223706,255872,200053,229730,229775,269622,217789,269632,223576,249521,223436,250037,259240,256191,199744,250103,263034,279557,242411,186608,195722,195709,194912,199716,195107,199394,199464,199387,186644,186648,186658,186684,199465,186701,199173,195304,195871,186711,186724,186744,195761,198927,193900,186772,196092,188203,188223,188224,189573,193832,193841,195405,193845,193852,196835,197698,188485,198756,193866,193893,195187,194967,193901,193904,193907,193927,193932,195807,195796,193947,196968,193959,193968,196969,193971,193976,193988,196989,194000,194007,194009,194010,194011,194012,195855,194026,194032,217693,229373,195939,198949,194526,194056,194082,194070,194080,194087,194101,194657,195557,195568,194123,195652,195213,194139,187893,187957,198889,188029,188033,194160,195877,194166,194167,195879,194194,195882,195910,217669,194214,194640,197206,194723,195951,194656,197250,194516,194270,194285,194286,199535,194407,194475,194437,194523,197702,195990,197764,196553,196720,196718,198082,197958,198766,198825,199403,199255,199263,199321,199337,199365,199541,199857,199402,199424,196056,196037,196228,196887,196439,223201,268979,198866,217767,280201,269176,263655,261508,270415,229879)");
	    
    		pstm = conn.prepareStatement(varname1.toString());
    		rs = pstm.executeQuery();
    		
    		Integer contador = 1;
    		String texto = "Realizar troca de numero telefonico da recetora, inserir numero 63 3215-6582 no campo primário e segundário e excluir qualquer outro numero que esteja programado.ATENÇÃO:_ Conferir se a(s) central(is) deste cliente estão com a programação correta de envio de RESTAURO no ato do fechamento da zona. Dúvidas entrar em contato com a Assessoria Técnica.";

    		while (rs.next()) {
    			Integer cdCliente = rs.getInt("CD_CLIENTE");
    			Integer cdColaborador = rs.getInt("CD_TECNICO_RESPONSAVEL");		
    			abrirOrdemServico(cdCliente, cdColaborador, texto);
    			System.out.println(contador+";"+cdCliente+";"+cdColaborador);
    		}
    	} catch (Exception e) {
    		System.out.println(e);
    	}
    }
    
    private void abrirOrdemServico(int cdCliente, int cdTecnico, String texto) {

    	Connection conn = null;
    	PreparedStatement pstm = null;
    	ResultSet rs = null;

    	StringBuilder sql = new StringBuilder();
    	sql.append(" INSERT INTO dbORDEM ");
    	sql.append(" (CD_CLIENTE, ID_INSTALADOR, ABERTURA,  DEFEITO, OPABRIU, IDOSDEFEITO, FG_EMAIL_ENVIADO, CD_OS_SOLICITANTE, FG_TODAS_PARTICOES_EM_MANUTENCAO,  EXECUTADO,	TEMPOEXECUCAOPREVISTO )");
    	sql.append(" VALUES(?,          ?,        GETDATE(), ?,       ?,       ?,           ?,                ?,                 ?,					?,		1 )");

    	try {
    		conn = PersistEngine.getConnection("SIGMA90");
    		pstm = conn.prepareStatement(sql.toString());
    		pstm.setInt(1, cdCliente);
    		pstm.setInt(2, cdTecnico);
    		pstm.setString(3, texto);
    		pstm.setInt(4, 11010); //FUSION
    		pstm.setInt(5, 1019);
    		pstm.setInt(6, 0); //FALSO	    
    		pstm.setInt(7, 10011); //5-DEMAIS ORDENS DE SERVIÇO
    		pstm.setInt(8, 0); //FALSO
    		pstm.setString(9, "");

    		pstm.executeUpdate();

    	} catch (SQLException e) {
    		e.printStackTrace();
    	} finally {
    		OrsegupsUtils.closeConnection(conn, pstm, rs);
    	}
    }    
}