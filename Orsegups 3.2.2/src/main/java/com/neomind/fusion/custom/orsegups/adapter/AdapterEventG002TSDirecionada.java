package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.util.NeoDateUtils;

/**
 * Concatenação do tipo da solicitação no título da tarefa
 * 
 * @author herisson.ferreira
 *
 */
public class AdapterEventG002TSDirecionada implements TaskFinishEventListener {
	
	private static final Log log = LogFactory.getLog(AdapterEventG002TSDirecionada.class);
	
	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
		
		System.out.println("#### INICIO DO ADAPTER DE EVENTOS: ADAPTEREVENTOG002TSDIRECIONADA DATA: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		
		InstantiableEntityInfo formularioTarefa = AdapterUtils.getInstantiableEntityInfo("tarefa");
		NeoObject objetoTarefa = formularioTarefa.createNewInstance();
		EntityWrapper wEvento = new EntityWrapper(event.getProcess().getEntity());
		
		String tituloAuxiliar = null;
		String tipoSolicitacao = null;
		String titulo = null;
		
		try {
			tituloAuxiliar = (String) wEvento.findValue("tarefaSimples.Titulo");
			tipoSolicitacao = (String) wEvento.findValue("tipo.descricao");
			titulo = tipoSolicitacao + " - " + tituloAuxiliar;
			
			wEvento.findField("tarefaSimples.Titulo").setValue(titulo);
			
			PersistEngine.persist(objetoTarefa);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("#### ERRO NO ADAPTER DE EVENTOS: ADAPTEREVENTOG002TSDIRECIONADA DATA: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		} finally {
			System.out.println("#### FIM DO ADAPTER DE EVENTOS: ADAPTEREVENTOG002TSDIRECIONADA DATA: "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		
	}

}
