package com.neomind.fusion.custom.orsegups.contract;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.NumberWordsOrsegups;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;


public class MinutaContrato
{
	private static final Log log = LogFactory.getLog(MinutaContrato.class);
	
	public static File geraPDFCroqui()
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		InputStream is = null;
		String dir_Croqui = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "croqui.jasper";
		String dir_image = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "croqui.png";
		try {
			paramMap.put("image", dir_image);
			
			is = new BufferedInputStream(new FileInputStream(dir_Croqui));
			if (paramMap != null) {

				File file = File.createTempFile("Croqui", ".pdf");
				file.deleteOnExit();
				JasperPrint impressao = null;
				impressao = JasperFillManager.fillReport(is, paramMap);
				
				
				//JasperPrint impressao = JasperFillManager.fillReport(is, paramMap, jrds);
				if (impressao != null && file != null)
				{
					JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
					return file;
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Gera o PDF do relatório, utilizando JasperReports
	 */
	public static File geraPDF(String isContrato) 
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		InputStream is = null;
		String dir_Minuta = "";
		
		try {
			//NeoObject contrato = (NeoObject) PersistEngine.getNeoObject(NeoUtils.safeLong(isContrato));
			NeoObject contrato = null;
			Collection<NeoObject> objContrato = (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCPrincipal"), new QLEqualsFilter("neoId", NeoUtils.safeLong(isContrato))); 
			if(objContrato != null && !objContrato.isEmpty()){
				for (NeoObject neoObject : objContrato)
				{
					contrato = neoObject;
					break;
				}
				
			}
			
			EntityWrapper wContrato = new EntityWrapper(contrato);
			
			long tipoMovimento = (Long) wContrato.findValue("movimentoContratoNovo.codTipo");
			
			long codEmp = (Long) wContrato.findValue("empresa.codemp");
			long codFil = (Long) wContrato.findValue("empresa.codfil");
			
			//002 = CFTV
			long modeloContrato = 0;
			if(wContrato.findValue("modelo") != null)
			{
				modeloContrato = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("modelo.codigo")));
			}
			long tipoAlteracao = 0;
			if(tipoMovimento == 5)
				tipoAlteracao = (Long) wContrato.findValue("movimentoAlteracaoContrato.codTipo");
			
			//contrato novo
			if(tipoMovimento <= 4)
			{
				boolean isOnDemand = NeoUtils.safeBoolean(wContrato.findValue("modelo.isOnDemand"));
				if(isOnDemand)
				{
					//o codigo do ON DEMAND será o mesmo dos contratos de eletronica, com um campo a mais
					//mensal = 1 e anual = 2
					String tipoOnDemand = NeoUtils.safeOutputString(wContrato.findValue("tipoOnDemand.tipo"));
					if(tipoOnDemand.equals("Mensal"))
					{
						dir_Minuta 				= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "contratoOnDemandMensal.jasper";
					}else
					{
						dir_Minuta 				= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "contratoOnDemandAnual.jasper";
					}
				
					paramMap = populaValoresOnDemand(contrato);
				}else{
					if(modeloContrato == 1)
					{
						//normal
						dir_Minuta 				= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "contratoEletronicaOrsegups.jasper";
						if (codEmp == 18 && codFil == 2) {
							dir_Minuta 				= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "contratoEletronicaOrsegups182.jasper";
						} else if (codEmp == 18 && codFil == 6) {
							dir_Minuta = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "contratoEletronicaOrsegups186.jasper";
						}
						paramMap = populaValores(contrato, false);
					}else
					{
						
						//CFTV
						dir_Minuta 				= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "contratoEletronicaOrsegupsCFTV.jasper";
						paramMap = populaValores(contrato, true);
						paramMap.put("Clausula12_1",retornaClausula12_1(contrato) );
					}
				}
			}else if(tipoMovimento == 6)
			{
				
			}else
			{
				/*
				 * 	1	ALTERAÇÃO DE CNPJ	
					2	ALTERAÇÃO DE RAZÃO SOCIAL	
					3	AMPLIAÇÃO ELETRÔNICA E CFTV	
					4	REDUÇÃO DE EQUIP. ELETRONICA E CFTV	
					5	AMPLIAÇÃO DE HUMANAS E ASSEIO	
					6	REDUÇÃO VALOR CONTRATO ELET/CFTV/VIG.HUMNAS/ASSEIO	
					7	SERVIÇO EXTRA VIG. HUMANA/ASSEIO	
					8	SERVIÇO EXTRA TEMPORARIO DE VIG. HUMANA/ASSEIO	
					9	REINSTALAÇÃO ELETRÔNICA/CFTV	
					10	VENDA ELETRÔNICA/CFTV
				 */
				if(tipoAlteracao == 1)
				{
					dir_Minuta 				= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "termoAditivoCessaoDireito.jasper";
					paramMap = populaValoresCessaoDireito(contrato);
				}else
				{
					dir_Minuta 				= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "termoAditivo.jasper";
					paramMap = populaValoresAditivo(contrato, tipoAlteracao);
				}
			}

			//System.out.println("Diretório da Minuta: ( "+dir_Minuta+" )");
			is = new BufferedInputStream(new FileInputStream(dir_Minuta));
			if (paramMap != null) {

				File file = File.createTempFile("Minuta_Contrato", ".pdf");
				file.deleteOnExit();
				JasperPrint impressao = null;
				if(tipoMovimento <= 4)
				{
					if(modeloContrato == 1)
					{
						JRDataSource jrdts = new JRBeanCollectionDataSource(preencheParametros(contrato, false));
						impressao = JasperFillManager.fillReport(is, paramMap, jrdts);
					}else
					{
						JRDataSource jrdts = new JRBeanCollectionDataSource(preencheParametros(contrato, true));
						impressao = JasperFillManager.fillReport(is, paramMap, jrdts);
					}
				}else if(tipoMovimento == 6)
				{
					//ON DEMAND não possui fields, apenas parâmetros
					impressao = JasperFillManager.fillReport(is, paramMap);
				}else
				{
					if(tipoAlteracao == 1)
					{
						//alteração de CNPJ não possui fields, apenas parâmetros
						impressao = JasperFillManager.fillReport(is, paramMap);
					}else
					{
						JRDataSource jrdts = new JRBeanCollectionDataSource(preencheParametrosAditivo(contrato));
						impressao = JasperFillManager.fillReport(is, paramMap, jrdts);
					}
				}
				
				//JasperPrint impressao = JasperFillManager.fillReport(is, paramMap, jrds);
				if (impressao != null && file != null)
				{
					JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
					return file;
				}
			}

		} catch (Exception e) {
			log.error("Erro ao gerar o PDF do Relatório de Prestação de Contas!! ", e);
			e.printStackTrace();
		}
		return null;
	}
	
	public static Map<String, Object> populaValoresCessaoDireito(NeoObject contrato)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		EntityWrapper wContrato = new EntityWrapper(contrato);
		/*
		 * cliente antigo está em buscaNomeCliente
		 * O cliente novo está em novoCliente
		 */
		
		String cnpjCliente = "";
		try
		{
			cnpjCliente = ContratoUtils.corrigeCpFCnpj(NeoUtils.safeOutputString(wContrato.findValue("empresa.numcgc")),"J");//OrsegupsUtils.formatarString(NeoUtils.safeOutputString(wContrato.findValue("empresa.numcgc")), "##.###.###/####-##");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		String contratada = NeoUtils.safeOutputString(wContrato.findValue("empresa.nomfil"));
		contratada += ", inscrita no CNPJ " + cnpjCliente;
		contratada += " com sede à " + NeoUtils.safeOutputString(wContrato.findValue("empresa.endfil"));
		contratada += ", " + NeoUtils.safeOutputString(wContrato.findValue("empresa.baifil"));
		contratada += ", " + NeoUtils.safeOutputString(wContrato.findValue("empresa.cidfil"));
		contratada += "/" + NeoUtils.safeOutputString(wContrato.findValue("empresa.sigufs"));
		paramMap.put("nomeContratada", contratada);
		
		/*
		 * hoje o layout suporta apenas 3 responsáveis
		 */
		String nomeResponsavel1 = "";
		String nomeResponsavel2 = "";
		String nomeResponsavel3 = "";
		String cargoResponsavel1 = "";
		String cargoResponsavel2 = "";
		String cargoResponsavel3 = "";
		String telefone1Responsavel1 = "";
		String telefone1Responsavel2 = "";
		String telefone1Responsavel3 = "";
		String telefone2Responsavel1 = "";
		String telefone2Responsavel2 = "";
		String telefone2Responsavel3 = "";
		
		
		List<NeoObject> listaResponsaveis = (List<NeoObject>) wContrato.findField("postosContrato.vigilanciaEletronica.listaContatosPosto").getValues();
		int count = 0;
		for(NeoObject resp : listaResponsaveis)
		{
			count++;
			EntityWrapper wResp = new EntityWrapper(resp);
			
			if(count == 1)
			{
				nomeResponsavel1 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
				cargoResponsavel1 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
				telefone1Responsavel1 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
				telefone2Responsavel1 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
			}
			
			if(count == 2)
			{
				nomeResponsavel2 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
				cargoResponsavel2 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
				telefone1Responsavel2 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
				telefone2Responsavel2 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
			}
			
			if(count == 3)
			{
				nomeResponsavel3 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
				cargoResponsavel3 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
				telefone1Responsavel3 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
				telefone2Responsavel3 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
			}
		}
		paramMap.put("nomeResponsavel1", nomeResponsavel1);
		paramMap.put("nomeResponsavel2", nomeResponsavel2);
		paramMap.put("nomeResponsavel3", nomeResponsavel3);
		paramMap.put("cargoResponsavel1", cargoResponsavel1);
		paramMap.put("cargoResponsavel2", cargoResponsavel2);
		paramMap.put("cargoResponsavel3", cargoResponsavel3);
		paramMap.put("telefone1Responsavel1", telefone1Responsavel1);
		paramMap.put("telefone1Responsavel2", telefone1Responsavel2);
		paramMap.put("telefone1Responsavel3", telefone1Responsavel3);
		paramMap.put("telefone2Responsavel1", telefone2Responsavel1);
		paramMap.put("telefone2Responsavel2", telefone2Responsavel2);
		paramMap.put("telefone2Responsavel3", telefone2Responsavel3);
		
		//CONTRATANTE CEDENTE > Cliente Antigo
		String ctRazao 			= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.nomcli"));
		String ctFantasia 		= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.apecli"));
		String ctCnpj 			= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.cgccpf"));
		String ctEndereco 		= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.endcli"));
		String ctBairro 		= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.baicli"));
		String ctEmail 			= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.intnet"));
		String ctCidade 		= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.cidcli"));
		String ctInscricao 		= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.insest"));
		String ctNumero 		= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.nencli"));
		String ctComplemento 	= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.cplend"));
		String ctUF 			= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.sigufs"));
		String ctTelefone 		= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.foncli"));
		String ctCep 			= NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.cepcli"));
		String ctDtNascimento 	= "";
		String ctResponsavel 	= "";
		String ctCpfResp 		= "";
		
		String ctNumeroContrato = NeoUtils.safeOutputString(wContrato.findValue("numContrato.usu_numctr"));
		String resp = NeoUtils.safeOutputString(wContrato.findValue("numContrato.usu_clicon"));
		if(resp.contains("-"))
		{
			String respCpf[] = NeoUtils.safeOutputString(wContrato.findValue("numContrato.usu_clicon")).split("-");
			ctResponsavel 	= respCpf[0].trim();
			ctCpfResp 		= respCpf[1].trim();
		}else
			ctResponsavel = resp;
		
		paramMap.put("ctRazao", ctRazao);
		paramMap.put("ctFantasia", ctFantasia);
		paramMap.put("ctCnpj", ctCnpj);
		paramMap.put("ctEndereco", ctEndereco);
		paramMap.put("ctBairro", ctBairro);
		paramMap.put("ctEmail", ctEmail);
		paramMap.put("ctCidade", ctCidade);
		paramMap.put("ctInscricao", ctInscricao);
		paramMap.put("ctNumero", ctNumero);
		paramMap.put("ctComplemento", ctComplemento);
		paramMap.put("ctUF", ctUF);
		paramMap.put("ctTelefone", ctTelefone);
		paramMap.put("ctCep", ctCep);
		paramMap.put("ctDtNascimento", ctDtNascimento);
		paramMap.put("ctResponsavel", ctResponsavel);
		paramMap.put("ctCpfResp", ctCpfResp);
		paramMap.put("ctNumeroContrato", ctNumeroContrato);
		
		//CONTRATANTE CESSIONARIA > Cliente Novo
		String csRazao 			= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.razaoSocial"));
		String csFantasia 		= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.nomeFantasia"));
		String csCnpj 			= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.cnpj"));
		if(csCnpj.equals(""))
			csCnpj 				= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.cpf"));
		String csEndereco 		= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.enderecoCliente.endereco"));
		String csBairro 		= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.enderecoCliente.bairro"));
		String csEmail 			= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.email"));
		String csCidade 		= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.enderecoCliente.estadoCidade.cidade.nomcid"));
		String csInscricao 		= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.inscricaoEstadual"));
		String csNumero 		= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.enderecoCliente.numero"));
		String csComplemento 	= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.enderecoCliente.complemento"));
		String csUF 			= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.enderecoCliente.estadoCidade.cidade.sigufs"));
		String csTelefone 		= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.telefone1"));
		String csCep 			= NeoUtils.safeOutputString(wContrato.findValue("novoCliente.enderecoCliente.cep"));
		String csDtNascimento 	= NeoCalendarUtils.dateToString((GregorianCalendar)wContrato.findValue("dadosGeraisContrato.dtNascimento"));
		String csResponsavel 	= NeoUtils.safeOutputString(wContrato.findValue("dadosGeraisContrato.responsavelContrato"));
		String csCpfResp 		= NeoUtils.safeOutputString(wContrato.findValue("dadosGeraisContrato.cpfResponsavelContrato"));
		String csNumeroContrato = NeoUtils.safeOutputString(wContrato.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		paramMap.put("csRazao", csRazao.isEmpty() ? ctRazao : csRazao);
		paramMap.put("csFantasia", csFantasia.isEmpty() ? ctFantasia : csFantasia);
		paramMap.put("csCnpj", csCnpj.isEmpty() ? ctCnpj : csCnpj);
		paramMap.put("csEndereco", csEndereco.isEmpty() ? ctEndereco : csEndereco);
		paramMap.put("csBairro", csBairro);
		paramMap.put("csEmail", csEmail);
		paramMap.put("csCidade", csCidade);
		paramMap.put("csInscricao", csInscricao);
		paramMap.put("csNumero", csNumero);
		paramMap.put("csComplemento", csComplemento);
		paramMap.put("csUF", csUF);
		paramMap.put("csTelefone", csTelefone);
		paramMap.put("csCep", csCep);
		paramMap.put("csDtNascimento", csDtNascimento);
		paramMap.put("csResponsavel", csResponsavel);
		paramMap.put("csCpfResp", csCpfResp);
		paramMap.put("csNumeroContrato", csNumeroContrato.isEmpty()? ctNumeroContrato : csNumeroContrato);
		
		SimpleDateFormat sdf = new SimpleDateFormat("d 'de' MMMM 'de' yyyy");  
		String dataAss = sdf.format(new Date());
		
		Long usuCodReg  = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("postosContrato.regionalPosto.usu_codreg")));
		String local =  "São José"; //retornaCidadeRegional(usuCodReg); //agora vai ser fixo por causa da solicitação 561210.
		paramMap.put("dataAss", local + ", " + dataAss);
		String dir_logo = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "imagens" + File.separator + "orsegups.jpg";
		paramMap.put("dir_logo", dir_logo);
		
		
		return paramMap;
	}
	
	private static String retornaCidadeRegional(Long usuCodReg) {
		String query = " select usu_cidreg from USU_T200REG where usu_codreg =  " + usuCodReg;
		String retorno = null;
		ResultSet rs = null;
		Long codRep = 0L;
		try{
			rs = OrsegupsContratoUtils.selectTable(query, PersistEngine.getConnection("SAPIENS"));
			if (rs!= null){
				retorno = rs.getString(1);
			}
			rs.close();
			return retorno;
		}catch(Exception e){
			ContratoLogUtils.logInfo("Erro ao buscar cidade da regional query->"+query );
			return null;
		}
		
		
	}

	public static Map<String, Object> populaValoresAditivo(NeoObject contrato, long tipoAlteracao)
	{
		EntityWrapper wrapper = new EntityWrapper(contrato);
		/*
		 * 1 e 2 	= contrato e cliente novo
		 * 3 e 4 	= contrato novo apenas
		 * 5 		= alteração
		 */
		String tipPessoa = NeoUtils.safeOutputString(wrapper.findValue("empresa.tipemp"));
		long movimento = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		String dir_termoAditivoPostos 			= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "termoAditivoPostos.jasper";
		
		
		Collection<NeoObject> postos =  wrapper.findField("postosContrato").getValues();
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
		
			String contaSIGMA = NeoUtils.safeOutputString(wPosto.findValue("contaSIGMA"));
			
			String num_pos = null;
			if (wPosto.findValue("numPostoAntigo") == null){
				num_pos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
			}else{
				num_pos = NeoUtils.safeOutputString(wPosto.findValue("numPostoAntigo"));
			}
			if("".equals(contaSIGMA))
			{
				String numctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr")); // pegar sempre o contrato antigo
				try{
					contaSIGMA = ContratoContasSIGMAWebService.buscaCDContaSigma(numctr, num_pos);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			paramMap.put("cdContaSigma", contaSIGMA);
		}
		
		String nrContrato = null;
		if (wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens") == null){
			nrContrato = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		}else{
			nrContrato = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		}
		
		//nome da filial + cnpj + endereço
		String cnpjCliente = "";
		try
		{
			while(cnpjCliente.length() < 11)
				cnpjCliente = "0" + cnpjCliente;
			cnpjCliente = ContratoUtils.corrigeCpFCnpj(NeoUtils.safeOutputString(wrapper.findValue("empresa.numcgc")), "J"); //OrsegupsUtils.formatarString(NeoUtils.safeOutputString(wrapper.findValue("empresa.numcgc")), "##.###.###/####-##");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		String telefone = "";
		String inscricaoEstadual = "";
		String cnpjCPF = "";
		String responsavelContrato = "";
		String cpfResponsavel = "";
		String dataNascimentoResponsavel = "";
		String razaoSocial = "";
		String nomeFantasia = "";
		
		String aAplica = "";
		String aNaoAplica = "";
		if(tipoAlteracao == 2)
		{
			//alteração de razão social e nome fantasia
			//termo A Aplicado
			razaoSocial = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
			nomeFantasia = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.nomeFantasia"));
			paramMap.put("razaoSocial", razaoSocial);
			paramMap.put("nomeFantasia", nomeFantasia);
			/*
			 * TODO aqui pegaremos do cliente selecionado ou do novo?
			 */
			telefone = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
			inscricaoEstadual = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.insest"));
			cnpjCPF = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cgccpf"));
			try{
				while(cnpjCPF.length() < 11) cnpjCPF = "0" + cnpjCPF;
				
				cnpjCPF = OrsegupsUtils.formatarString(cnpjCPF, "###.###.###-##");
			}catch(Exception e){}
			responsavelContrato = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.usu_pescon"));
			cpfResponsavel = NeoUtils.safeOutputString("");
			dataNascimentoResponsavel = NeoUtils.safeOutputString("");
			
			aAplica = "X";
		}else
		{
			aNaoAplica = "X";
		}
		
		
		
		/*
		 * termos B e C no momento não serão populados
		 * 
		 * preencher apenas
		 * 
		 * contratante
		 * contratada
		 * data da assinatura
		 * cpf contratante
		 */
		
		//outras alterações
		
		String contratada = NeoUtils.safeOutputString(wrapper.findValue("empresa.nomfil"));
		contratada += ", inscrita no CNPJ " + cnpjCliente;
		contratada += " com sede à " + NeoUtils.safeOutputString(wrapper.findValue("empresa.endfil"));
		contratada += ", " + NeoUtils.safeOutputString(wrapper.findValue("empresa.baifil"));
		contratada += ", " + NeoUtils.safeOutputString(wrapper.findValue("empresa.cidfil"));
		contratada += "/" + NeoUtils.safeOutputString(wrapper.findValue("empresa.sigufs"));
		
		String contratante = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.nomcli"));
		//String cpfCliente = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cgccpf"));
		SimpleDateFormat sdf = new SimpleDateFormat("d 'de' MMMM 'de' yyyy");
		String dataAss = sdf.format(new Date());
		
		
		Long usuCodReg  = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("postosContrato.regionalPosto.usu_codreg")));
		String local =  "São José"; //retornaCidadeRegional(usuCodReg); //agora vai ser fixo por causa da solicitação 561210.
		paramMap.put("dataAss", local + ", " + dataAss);
		
		/*cnpjCPF = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cgccpf"));
		try
		{
			while(cnpjCPF.length() < 11)
				cnpjCPF = "0" + cnpjCPF;
			cnpjCPF = OrsegupsUtils.formatarString(cnpjCPF, "###.###.###-##");
		}catch(Exception e){}*/
		
		
		/*
		 * no momento estes 2 blocos ficam em branco
		 */
		String enderecoContratante = "";
		String numeroContratante = "";
		String bairroContratante = "";
		String cidadeEstadoContratante= "";
		String telefoneContratante = "";
		String bAplica = "";
		String bNaoAplica = "X";
		if(NeoUtils.safeBoolean(wrapper.findValue("houveAlteracaoDadosCliente")))
		{
			boolean alterou = false;
			Collection<NeoObject> listaAlteracoesCliente = wrapper.findField("listaAlteracoesCliente").getValues();
			for(NeoObject alt : listaAlteracoesCliente)
			{
				EntityWrapper w = new EntityWrapper(alt);
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço do Cliente - endereco"))
				{
					alterou = true;
					enderecoContratante = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.endereco"));
				}
				
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço do Cliente - numero"))
				{
					alterou = true;
					numeroContratante = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.numero"));
				}
				
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço do Cliente - bairro"))
				{
					alterou = true;
					bairroContratante = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.bairro"));
				}
				
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço do Cliente - cidade") || 
				   NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço do Cliente - estado"))
				{
					alterou = true;
					cidadeEstadoContratante = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.estadoCidade.cidade.sigufs"));
				}
				
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço do Cliente - telefone1"))
				{
					alterou = true;
					telefoneContratante = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
				}
				
				if(alterou)
				{
					bAplica = "X";
					bNaoAplica = "";
				}
			}
		}
		
		
		
		String enderecoCobranca = "";
		String numeroCobranca = "";
		String bairroCobranca = "";
		String cidadeEstadoCobranca= "";
		String cepCobranca = "";
		String cAplica = "";
		String cNaoAplica = "X";
		if(NeoUtils.safeBoolean(wrapper.findValue("houveAlteracaoDadosCliente")))
		{
			boolean alterou = false;
			Collection<NeoObject> listaAlteracoesCliente = wrapper.findField("listaAlteracoesCliente").getValues();
			for(NeoObject alt : listaAlteracoesCliente)
			{
				EntityWrapper w = new EntityWrapper(alt);
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço de Cobrança - endereco"))
				{
					alterou = true;
					enderecoCobranca = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.endereco"));
				}
				
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço de Cobrança - numero"))
				{
					alterou = true;
					numeroCobranca = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.numero"));
				}
				
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço de Cobrança - bairro"))
				{
					alterou = true;
					bairroCobranca = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.bairro"));
				}
				
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço de Cobrança - cidade") || 
				   NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço de Cobrança - estado"))
				{
					alterou = true;
					cidadeEstadoCobranca = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.estadoCidade.cidade.sigufs"));
				}
				
				if(NeoUtils.safeOutputString(w.findValue("campo")).equals("Endereço de Cobrança - cep"))
				{
					alterou = true;
					cepCobranca = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.cep"));
				}
				
				if(alterou)
				{
					cAplica = "X";
					cNaoAplica = "";
				}
			}
		}
		
		
		Long prazoMaximoVigencia = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.peridoMaximoVigencia2.meses")));
		String strPrazoMaximo = "";
		String strPrazoMaximo2 = "";
		if (prazoMaximoVigencia != null){
			if (prazoMaximoVigencia.equals(0L)){
				strPrazoMaximo = "por prazo indeterminado, contado";
				strPrazoMaximo2 = "";
			}else if (prazoMaximoVigencia.equals(12L)){
				strPrazoMaximo = "pelo prazo de 12 (doze) meses, contados";
				strPrazoMaximo2 = "de 12 (doze) meses,";
			}else if (prazoMaximoVigencia.equals(24L)){
				strPrazoMaximo = "pelo prazo de 24 (vinte e quatro) meses, contados";
				strPrazoMaximo2 = "de 24 (vinte e quatro) meses,";
			}else if (prazoMaximoVigencia.equals(36L)){
				strPrazoMaximo = "pelo prazo de 36 (trinta e seis) meses, contados";
				strPrazoMaximo2 = "de 36 (trinta e seis) meses,";
			}else if (prazoMaximoVigencia.equals(48L)){
				strPrazoMaximo = "pelo prazo de 48 (quarenta e oito) meses, contados";
				strPrazoMaximo2 = "de 48 (quarenta e oito) meses,";
			}else if(prazoMaximoVigencia.equals(60L)) {
				strPrazoMaximo = "pelo prazo de 60 (sessenta) meses, contados";
				strPrazoMaximo2 = "de 60 (sessenta) meses,";
			}else{
				strPrazoMaximo = "por prazo indeterminado, contado";
				strPrazoMaximo2 = "";
			}
		}else{
			strPrazoMaximo = "por prazo indeterminado, contado";
			strPrazoMaximo2 = "";
		}
		
		String txt = "O presente contrato vigorará "+strPrazoMaximo+" a partir da data de implantação do sistema de alarme" ;
		String multa = ". Por qualquer motivo que a CONTRATANTE venha a rescindir o presente contrato, antes do prazo de vigência aqui estipulado, esta pagará, à CONTRATADA, o valor total correspondente a 50% (cinqüenta por cento) do valor das parcelas vincendas até completar o prazo de vigência acima estipulado a título de multa contratual.";
		String daRecisao = "";
		
		
		if (!strPrazoMaximo2.equals("")){
			
			daRecisao = txt + multa + "Transcorrido o período inicial, "+strPrazoMaximo2+" este contrato se renovará por prazo indeterminado, podendo ser rescindido, mediante aviso prévio de 30 (trinta) dias, por qualquer das partes, sem nenhum ônus. A rescisão do presente contrato, deverá ser feita, obrigatoriamente, por carta devidamente assinada e com reconhecimento de firma."; 
 
		}else{
			daRecisao = txt + ", podendo ser rescindido, mediante aviso prévio de 30 (trinta) dias, por qualquer das partes, sem nenhum ônus. A rescisão do presente contrato, deverá ser feita, obrigatoriamente, por carta devidamente assinada e com reconhecimento de firma.";
		}
		
		ContratoLogUtils.logInfo("prazo maximo viegencia = " + strPrazoMaximo);
		paramMap.put("prazoMaximo", strPrazoMaximo);
		paramMap.put("daRecisao", daRecisao);
		
		paramMap.put("nrContrato", nrContrato);
		paramMap.put("dir_termoAditivoPostos", dir_termoAditivoPostos);
		paramMap.put("aAplica", aAplica);
		paramMap.put("aNaoAplica", aNaoAplica);
		paramMap.put("telefone", telefone);
		paramMap.put("inscricaoEstadual", inscricaoEstadual);
		paramMap.put("cnpjCPF", cnpjCPF);
		paramMap.put("responsavelContrato", responsavelContrato);
		paramMap.put("cpfResponsavel", cpfResponsavel);
		paramMap.put("dataNascimentoResponsavel", dataNascimentoResponsavel);
		paramMap.put("contratada", contratada);
		paramMap.put("contratante", contratante);
		paramMap.put("dataAss", local + ", " +dataAss);
		paramMap.put("nomeContratante", contratante);
		paramMap.put("cpfContratante", cnpjCPF);
		paramMap.put("enderecoContratante", enderecoContratante);
		paramMap.put("numeroContratante", numeroContratante);
		paramMap.put("bairroContratante", bairroContratante);
		paramMap.put("cidadeEstadoContratante", cidadeEstadoContratante);
		paramMap.put("telefoneContratante", telefoneContratante);
		paramMap.put("bAplica", bAplica);
		paramMap.put("bNaoAplica", bNaoAplica);
		paramMap.put("enderecoCobranca", enderecoCobranca);
		paramMap.put("numeroCobranca", numeroCobranca);
		paramMap.put("bairroCobranca", bairroCobranca);
		paramMap.put("estadoCidadeCobranca", cidadeEstadoCobranca);
		paramMap.put("cepCobranca", cepCobranca);
		paramMap.put("cAplica", cAplica);
		paramMap.put("cNaoAplica", cNaoAplica);
		
		return paramMap;
	}
	
	public static Map<String, Object> populaValoresOnDemand(NeoObject contrato)
	{
		EntityWrapper wrapper = new EntityWrapper(contrato);
		/*
		 * 1 e 2 	= contrato e cliente novo
		 * 3 e 4 	= contrato novo apenas
		 * 5 		= alteração
		 * 6		= On Demand
		 */
		long movimento = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		//estes dois ficam em branco no momento
		String nrContrato = "";
		String codSigma = "";
		
		String nrContratoSapiens = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		//nome da filial + cnpj + endereço
		String cnpjCliente = "";
		try
		{
			cnpjCliente = ContratoUtils.corrigeCpFCnpj(NeoUtils.safeOutputString(wrapper.findValue("empresa.numcgc")),"J");//OrsegupsUtils.formatarString(NeoUtils.safeOutputString(wrapper.findValue("empresa.numcgc")), "##.###.###/####-##");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		String contratada = NeoUtils.safeOutputString(wrapper.findValue("empresa.nomfil"));
		contratada += ", inscrita no CNPJ " + cnpjCliente;
		contratada += " com sede à " + NeoUtils.safeOutputString(wrapper.findValue("empresa.endfil"));
		contratada += ", " + NeoUtils.safeOutputString(wrapper.findValue("empresa.baifil"));
		contratada += ", " + NeoUtils.safeOutputString(wrapper.findValue("empresa.cidfil"));
		contratada += "/" + NeoUtils.safeOutputString(wrapper.findValue("empresa.sigufs"));
		
		//razão social
		String contratante = "";
		if(movimento == 3 || movimento == 4)
		{
			contratante = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		}else
		{
			contratante = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.nomcli"));
		}
		
		String cidadeEstadoCobranca = "";
		String telCobranca = "";
		String emailCobranca = "";
		String enderecoCobranca = "";
		String nrCobranca = "";
		String cepCobranca = "";
		String bairroCobranca = "";
		
		String cidadeEstado = "";
		String telefone = "";
		String email = "";
		String endereco = "";
		String nr = "";
		String cep = "";
		String bairro = "";
		String inscricaoEstadual = "";
		/*
		 * sempre pega do novoCliente
		 * pois em qualquer tipo ele pode alterar os dados do cliente
		 * 
		 */
		cidadeEstadoCobranca 	= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.estadoCidade.cidade.sigufs"));
		telCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
		emailCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.email"));
		enderecoCobranca 		= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.endereco"));
		nrCobranca 				= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.numero"));
		cepCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.cep"));
		bairroCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.bairro"));

		cidadeEstado 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.estadoCidade.cidade.sigufs"));
		telefone 				= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
		email 					= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.email"));
		endereco 				= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.endereco"));
		nr 						= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.numero"));
		cep 					= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.cep"));
		bairro 					= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.bairro"));
		
		inscricaoEstadual		= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.inscricaoEstadual"));
		
		
		String responsavelContrato 				= NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.responsavelContrato"));
		String cpfResponsavelContrato 			= NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.cpfResponsavelContrato"));
		String dtNascimentoResponsavelContrato 	= NeoCalendarUtils.dateToString((GregorianCalendar)wrapper.findValue("dadosGeraisContrato.dtNascimento"));
		
		String tipoCliente = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipocliente.tipo"));
		String cnpj = "";
		if("F".equals(tipoCliente))
			cnpj = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.cpf"));
		else
			cnpj = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.cnpj"));
		
		String nomeFantasia = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.nomeFantasia"));
		String telefoneContratada = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
		
		if(movimento != 3 && movimento != 4)
		{
			tipoCliente = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipcli"));
			cnpj = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cgccpf"));
			nomeFantasia = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.apecli"));
			telefoneContratada = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.foncli"));
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("d 'de' MMMM 'de' yyyy");  
		String dataAss = sdf.format(new Date());
		
		Long usuCodReg  = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("postosContrato.regionalPosto.usu_codreg")));
		String local =  "São José"; //retornaCidadeRegional(usuCodReg); //agora vai ser fixo por causa da solicitação 561210.
		
		
		
		String nomeResponsavel1 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String nomeResponsavel2 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String nomeResponsavel3 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String telefone1Responsavel1 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String telefone1Responsavel2 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String telefone1Responsavel3 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String telefone2Responsavel1 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String telefone2Responsavel2 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String telefone2Responsavel3 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String emailResponsavel1 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String emailResponsavel2 = NeoUtils.safeOutputString(wrapper.findValue(""));
		String emailResponsavel3 = NeoUtils.safeOutputString(wrapper.findValue(""));
		
		Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
		/*
		 * no ON DEMAND só haverá um posto, logo pegar qq um da lista
		 */
		NeoObject posto = postos.iterator().next();
		EntityWrapper wPosto = new EntityWrapper(posto);
		Collection<NeoObject> listaResponsaveis = wPosto.findField("vigilanciaEletronica.listaContatosPosto").getValues();
		int count = 0;
		for(NeoObject resp : listaResponsaveis)
		{
			count++;
			EntityWrapper wResp = new EntityWrapper(resp);
			
			if(count == 1)
			{
				nomeResponsavel1 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
				emailResponsavel1 = NeoUtils.safeOutputString(wResp.findValue("email"));	
				telefone1Responsavel1 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
				telefone2Responsavel1 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
			}
			
			if(count == 2)
			{
				nomeResponsavel2 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
				emailResponsavel2 = NeoUtils.safeOutputString(wResp.findValue("email"));	
				telefone1Responsavel2 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
				telefone2Responsavel2 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
			}
			
			if(count == 3)
			{
				nomeResponsavel3 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
				emailResponsavel3 = NeoUtils.safeOutputString(wResp.findValue("email"));	
				telefone1Responsavel3 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
				telefone2Responsavel3 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
			}
		}
		
		String enderecoInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.endereco"));
		String numeroInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.numero"));
		String bairroInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.bairro"));
		String estadoCidadeInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.sigufs"));
		String referenciaInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.referencia"));
		String cepInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.cep"));
		String telefoneInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.telefoneRef"));
		
		paramMap.put("enderecoInstalacao", enderecoInstalacao);
		paramMap.put("numeroInstalacao", numeroInstalacao);
		paramMap.put("bairroInstalacao", bairroInstalacao);
		paramMap.put("estadoCidadeInstalacao", estadoCidadeInstalacao);
		paramMap.put("referenciaInstalacao", referenciaInstalacao);
		paramMap.put("cepInstalacao", cepInstalacao);
		paramMap.put("telefoneInstalacao", telefoneInstalacao);
		
		paramMap.put("nomeResponsavel1", nomeResponsavel1);
		paramMap.put("nomeResponsavel2", nomeResponsavel2);
		paramMap.put("nomeResponsavel3", nomeResponsavel3);
		paramMap.put("emailResponsavel1", emailResponsavel1);
		paramMap.put("emailResponsavel2", emailResponsavel2);
		paramMap.put("emailResponsavel3", emailResponsavel3);
		paramMap.put("telefone1Responsavel1", telefone1Responsavel1);
		paramMap.put("telefone1Responsavel2", telefone1Responsavel2);
		paramMap.put("telefone1Responsavel3", telefone1Responsavel3);
		paramMap.put("telefone2Responsavel1", telefone2Responsavel1);
		paramMap.put("telefone2Responsavel2", telefone2Responsavel2);
		paramMap.put("telefone2Responsavel3", telefone2Responsavel3);
		paramMap.put("inscricaoEstadual", inscricaoEstadual);
		paramMap.put("nrContrato", nrContrato);
		paramMap.put("codSigma", codSigma);
		paramMap.put("nrContratoSapiens", nrContratoSapiens);
		paramMap.put("contratada", contratada);
		paramMap.put("contratante", contratante);
		paramMap.put("cidadeEstadoCobranca", cidadeEstadoCobranca);
		paramMap.put("telCobranca", telCobranca);
		paramMap.put("emailCobranca", emailCobranca);
		paramMap.put("enderecoCobranca", enderecoCobranca);
		paramMap.put("nrCobranca", nrCobranca);
		paramMap.put("cepCobranca", cepCobranca);
		paramMap.put("bairroCobranca", bairroCobranca);
		paramMap.put("endereco", endereco);
		paramMap.put("telefone", telefone);
		paramMap.put("nr", nr);
		paramMap.put("bairro", bairro);
		paramMap.put("cep", cep);
		paramMap.put("email", email);
		paramMap.put("cidadeEstado", cidadeEstado);
		paramMap.put("responsavelContrato", responsavelContrato);
		paramMap.put("cpfResponsavelContrato", cpfResponsavelContrato);
		paramMap.put("dtNascimentoResponsavelContrato", dtNascimentoResponsavelContrato);
		paramMap.put("cnpj", ContratoUtils.corrigeCpFCnpj(cnpj,tipoCliente) );
		paramMap.put("nomeFantasia", nomeFantasia);
		paramMap.put("telefoneContratada", telefoneContratada);
		paramMap.put("dataAss", local + ", " + dataAss);
		
		return paramMap;
	}
	
	public static Map<String, Object> populaValores(NeoObject contrato, boolean CFTV)
	{
		/*
		 * aqui deve-se popular todos os parametros
		 * 
		 * contrato novo
		 */
		
		EntityWrapper wrapper = new EntityWrapper(contrato);
		
		long codEmp = (Long) wrapper.findValue("empresa.codemp");
		long codFil = (Long) wrapper.findValue("empresa.codfil");
		/*
		 * 1 e 2 	= contrato e cliente novo
		 * 3 e 4 	= contrato novo apenas
		 * 5 		= alteração
		 */
		long movimento = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		
		String dir_listaAnexos 			= "";
		
		if(!CFTV)
		{
			dir_listaAnexos 			= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "AnexosPosto.jasper";
			if (codEmp == 18 && codFil == 2){
				dir_listaAnexos 			= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "AnexosPosto182.jasper";
			}
			
		}else
		{
			dir_listaAnexos 			= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "AnexosPostoCFTV.jasper";
		}
		
		//estes dois ficam em branco no momento
		String nrContrato = "";
		String codSigma = "";
		
		String nrContratoSapiens = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		//nome da filial + cnpj + endereço
		String cnpjCliente = "";
		try
		{
			cnpjCliente = ContratoUtils.corrigeCpFCnpj(NeoUtils.safeOutputString(wrapper.findValue("empresa.numcgc")),"J"); //OrsegupsUtils.formatarString(NeoUtils.safeOutputString(wrapper.findValue("empresa.numcgc")), "##.###.###/####-##");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		String contratada = NeoUtils.safeOutputString(wrapper.findValue("empresa.nomfil"));
		contratada += ", inscrita no CNPJ " + cnpjCliente;
		contratada += " com sede à " + NeoUtils.safeOutputString(wrapper.findValue("empresa.endfil"));
		contratada += ", " + NeoUtils.safeOutputString(wrapper.findValue("empresa.baifil"));
		contratada += ", " + NeoUtils.safeOutputString(wrapper.findValue("empresa.cidfil"));
		contratada += "/" + NeoUtils.safeOutputString(wrapper.findValue("empresa.sigufs"));
		
		//razão social
		String contratante = "";
		if(movimento == 3 || movimento == 4)
		{
			contratante = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		}else
		{
			contratante = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.nomcli"));
		}
		Boolean adereProtecaoG = false;
		Long prazoMaximoVigencia = 0L; 
		String cidadeEstadoCobranca = "";
		String telCobranca = "";
		String emailCobranca = "";
		String enderecoCobranca = "";
		String nrCobranca = "";
		String cepCobranca = "";
		String bairroCobranca = "";
		
		String cidadeEstado = "";
		String telefone = "";
		String email = "";
		String endereco = "";
		String nr = "";
		String cep = "";
		String bairro = "";
		String inscricaoEstadual = "";
		
		
		/*
		 * sempre pega do novoCliente
		 * pois em qualquer tipo ele pode alterar os dados do cliente
		 * 
		 */
		//if(movimento == 3 || movimento == 4)
		//{
			boolean endCobrancaIgual = NeoUtils.safeBoolean(wrapper.findValue("novoCliente.endCobrancaIgualCliente"));
			
			cidadeEstadoCobranca 	= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.estadoCidade.cidade.sigufs"));
			telCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
			emailCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.email"));
			enderecoCobranca 		= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.endereco"));
			nrCobranca 				= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.numero"));
			cepCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.cep"));
			bairroCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.endCobranca.bairro"));

			cidadeEstado 			= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.estadoCidade.cidade.sigufs"));
			telefone 				= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
			email 					= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.email"));
			endereco 				= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.endereco"));
			nr 						= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.numero"));
			cep 					= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.cep"));
			bairro 					= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.enderecoCliente.bairro"));
			
			if (endCobrancaIgual){
				cidadeEstadoCobranca 	= cidadeEstado;
				telCobranca 			= telefone;
				emailCobranca 			= email;
				enderecoCobranca 		= endereco;
				nrCobranca 				= nr;
				cepCobranca 			= cep;
				bairroCobranca			= bairro;	
			}
			
			inscricaoEstadual		= NeoUtils.safeOutputString(wrapper.findValue("novoCliente.inscricaoEstadual"));
		/*}else
		{
			cidadeEstadoCobranca 	= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cidcob")) + " / " + NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.estcob"));
			telCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.foncli"));
			emailCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.intnet"));
			String enderecoNumeroCob = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.endcob"));
			if(enderecoNumeroCob.indexOf(",") != -1)
			{
				enderecoCobranca 			= enderecoNumeroCob.split(",")[0];
				nrCobranca					= enderecoNumeroCob.split(",")[1];
			}else
				enderecoCobranca			= enderecoNumeroCob;
			
			cepCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cepcob"));
			bairroCobranca 			= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.baicob"));

			cidadeEstado 			= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cidcli")) + " / " + NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.sigufs"));
			telefone 				= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.foncli"));
			email 					= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.intnet"));
			String enderecoNumero	= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.endcli"));
			
			inscricaoEstadual		= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.insest"));
			if(enderecoNumero.indexOf(",") != -1)
			{
				endereco 			= enderecoNumero.split(",")[0];
				nr					= enderecoNumero.split(",")[1];
			}else
				endereco			= enderecoNumero;
			cep 					= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cepcli"));
			bairro 					= NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.baicli"));
		}*/
		
		
		
		String responsavelContrato 				= NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.responsavelContrato"));
		String cpfResponsavelContrato 			= NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.cpfResponsavelContrato"));
		String dtNascimentoResponsavelContrato 	= NeoCalendarUtils.dateToString((GregorianCalendar)wrapper.findValue("dadosGeraisContrato.dtNascimento"));
		
		String tipoCliente = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipocliente.tipo"));
		String cnpj = "";
		if("F".equals(tipoCliente))
			cnpj = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.cpf"));
		else
			cnpj = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.cnpj"));
		
		String nomeFantasia = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.nomeFantasia"));
		String telefoneContratada = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.telefone1"));
		
		if(movimento != 3 && movimento != 4)
		{
			tipoCliente = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipcli"));
			cnpj = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cgccpf"));
			nomeFantasia = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.apecli"));
			telefoneContratada = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.foncli"));
		}
		
		String termo11a = "";
		String termo11b = "";
		String termo11c = "";
		
		long tipoContrato = 0;
		if(wrapper.findValue("dadosGeraisContrato.tipoDoContrato") != null)
		{
			tipoContrato = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.tipoDoContrato.codigoProduto")));
		}
		/*
		 * Monitoramento = 1.1 a
		 * Locação + Monitoramento = 1.1 c
		 * Locação Parcial+ Monitoramento = 1.1 b
		 */
		if(tipoContrato == 1)
		{
			//Monitoramento
			termo11a = "SIM";
			termo11b = "NÃO";
			termo11c = "NÃO";
		}else if(tipoContrato == 2)
		{
			//Locação + Monitoramento
			termo11a = "NÃO";
			termo11b = "NÃO";
			termo11c = "SIM";
		}else
		{
			//Locação Parcial+ Monitoramento
			termo11a = "NÃO";
			termo11b = "SIM";
			termo11c = "NÃO";
		}
		
		if(!CFTV)
		{
			adereProtecaoG = NeoUtils.safeBoolean(wrapper.findValue("adereProtecaoGarantida"));
			ContratoLogUtils.logInfo("AdereProteção Garantida? " + adereProtecaoG);
			String termo12a = "";
			String termo12b = "";
			String termo12c = "";
			boolean termo12aContrato = NeoUtils.safeBoolean(wrapper.findValue("dadosGeraisContrato.termo12a"), false);
			boolean termo12bContrato = NeoUtils.safeBoolean(wrapper.findValue("dadosGeraisContrato.termo12b"), false);
			boolean termo12cContrato = NeoUtils.safeBoolean(wrapper.findValue("dadosGeraisContrato.termo12c"), false);
			
			if(termo12aContrato)
				termo12a = "SIM";
			else
				termo12a = "NÃO";
			
			if(termo12bContrato)
				termo12b = "SIM";
			else
				termo12b = "NÃO";
			
			if(termo12cContrato)
				termo12c = "SIM";
			else
				termo12c = "NÃO";
			
			paramMap.put("termo12a", termo12a);
			paramMap.put("termo12b", termo12b);
			paramMap.put("termo12c", termo12c);
		}else
		{
			//Aqui é CFTV
			String termo12a = "";
			String termo13a = "";
			/*
			 * validar a automização destas clausulas, como na época não havia definição, por padrao será SIM
			 */
			boolean termo12aCFTV = NeoUtils.safeBoolean(wrapper.findValue("dadosGeraisContrato.termo12CFTV"), false);
			if(termo12aCFTV)
				termo12a = "SIM";
			else
				termo12a = "NÃO";
			
			//termo 13a sempre sim
			termo13a = "SIM";
			paramMap.put("termo12a", termo12a);
			paramMap.put("termo13a", termo13a);
		}
		
		prazoMaximoVigencia = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.peridoMaximoVigencia2.meses")));

		String strPrazoMaximo = "";
		String strPrazoMaximo2 = "";
		if (prazoMaximoVigencia != null){
			if (prazoMaximoVigencia.equals(0L)){
				strPrazoMaximo = "por prazo indeterminado, contado";
				strPrazoMaximo2 = "";
			}else if (prazoMaximoVigencia.equals(12L)){
				strPrazoMaximo = "pelo prazo de 12 (doze) meses, contados";
				strPrazoMaximo2 = "de 12 (doze) meses,";
			}else if (prazoMaximoVigencia.equals(24L)){
				strPrazoMaximo = "pelo prazo de 24 (vinte e quatro) meses, contados";
				strPrazoMaximo2 = "de 24 (vinte e quatro) meses,";
			}else if (prazoMaximoVigencia.equals(36L)){
				strPrazoMaximo = "pelo prazo de 36 (trinta e seis) meses, contados";
				strPrazoMaximo2 = "de 36 (trinta e seis) meses,";
			}else if (prazoMaximoVigencia.equals(48L)){
				strPrazoMaximo = "pelo prazo de 48 (quarenta e oito) meses, contados";
				strPrazoMaximo2 = "de 48 (quarenta e oito) meses,";
			}else if(prazoMaximoVigencia.equals(60L)) {
				strPrazoMaximo = "pelo prazo de 60 (sessenta) meses, contados";
				strPrazoMaximo2 = "de 60 (sessenta) meses,";
			}else{
				strPrazoMaximo = "por prazo indeterminado, contado";
				strPrazoMaximo2 = "";
			}
		}else{
			strPrazoMaximo = "por prazo indeterminado, contado";
			strPrazoMaximo2 = "";
		}
		
		String tipoSistema = "";
		if (CFTV){
			tipoSistema = "CFTV";
		}else{
			tipoSistema = "alarme";
		}
		
		String txt = "O presente contrato vigorará "+strPrazoMaximo+" a partir da data de implantação do sistema de "+ tipoSistema ;
		String multa = ". Por qualquer motivo que a CONTRATANTE venha a rescindir o presente contrato, antes do prazo de vigência aqui estipulado, esta pagará, à CONTRATADA, o valor total correspondente a 50% (cinqüenta por cento) do valor das parcelas vincendas até completar o prazo de vigência acima estipulado a título de multa contratual.";
		String daRecisao = "";
		
		
		
		if (!strPrazoMaximo2.equals("")){
			
			daRecisao = txt + multa + "Transcorrido o período inicial, "+strPrazoMaximo2+" este contrato se renovará por prazo indeterminado, podendo ser rescindido, mediante aviso prévio de 30 (trinta) dias, por qualquer das partes, sem nenhum ônus. A rescisão do presente contrato, deverá ser feita, obrigatoriamente, por carta devidamente assinada e com reconhecimento de firma."; 
 
		}else{
			daRecisao = txt + ", podendo ser rescindido, mediante aviso prévio de 30 (trinta) dias, por qualquer das partes, sem nenhum ônus. A rescisão do presente contrato, deverá ser feita, obrigatoriamente, por carta devidamente assinada e com reconhecimento de firma.";
		}
		
		Long diaFixoVencimento = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.diaFixoVencimento.diaVencimento")));
		System.out.println("[FLUXO CONTRATOS]-Minuta diaFixoVencimento: "+ diaFixoVencimento);
		NeoObject oDiaVencimento = PersistEngine.getObject(AdapterUtils.getEntityClass("FGCDiaFixoVencimento"), new QLEqualsFilter("diaVencimento", (Long)diaFixoVencimento));
		
		Long diaVencimento = 3L;
		String descricaoVencimento = "Três";
		
		if (oDiaVencimento != null){
			EntityWrapper wDiaVencimento= new EntityWrapper(oDiaVencimento);
			diaVencimento = (Long) wDiaVencimento.findValue("diaVencimento");
			descricaoVencimento = NeoUtils.safeString( (String) wDiaVencimento.findValue("descricaoVencimento"));
			System.out.print(wDiaVencimento.findValue("descricaoVencimento"));
			System.out.print(wDiaVencimento.findValue("diaVencimento"));
		}
		
		BigDecimal totalContrato = new BigDecimal(0);
		Collection<NeoObject> postos = (Collection<NeoObject>) wrapper.findField("postosContrato").getValues();
		for(NeoObject posto: postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			BigDecimal valorTotalPosto = (BigDecimal) wPosto.findValue("formacaoPreco.valorMontanteB"); //(BigDecimal) wPosto.findValue("valorTotalPosto");
			if (valorTotalPosto == null){
				valorTotalPosto = new BigDecimal("0");
			}
			totalContrato = totalContrato.add(valorTotalPosto);
		}
		
		String valorMensal = NeoUtils.safeOutputString(totalContrato).replace(".", ",");
		
		
		
		
		String valorMensalTxt = NumberWordsOrsegups.convertMoeda(totalContrato);
		paramMap.put("tipoSistema", tipoSistema);
		paramMap.put("adereProtecaoG", adereProtecaoG);
		paramMap.put("prazoMaximo", strPrazoMaximo);
		paramMap.put("daRecisao", daRecisao);
		
		paramMap.put("inscricaoEstadual", inscricaoEstadual);
		paramMap.put("valorMensal", valorMensal);
		paramMap.put("valorMensalTxt", valorMensalTxt);
		paramMap.put("termo11a", termo11a);
		paramMap.put("termo11b", termo11b);
		paramMap.put("termo11c", termo11c);
		paramMap.put("diaVcto", String.valueOf(diaVencimento));
		paramMap.put("descVcto", descricaoVencimento);
		
		//paramMap.put("dir_listaEquipamentos", dir_listaEquipamentos);
		paramMap.put("dir_listaAnexos", dir_listaAnexos);
		paramMap.put("nrContrato", nrContrato);
		paramMap.put("codSigma", codSigma);
		paramMap.put("nrContratoSapiens", nrContratoSapiens);
		paramMap.put("contratada", contratada);
		paramMap.put("contratante", contratante);
		paramMap.put("cidadeEstadoCobranca", cidadeEstadoCobranca);
		paramMap.put("telCobranca", telCobranca);
		paramMap.put("emailCobranca", emailCobranca);
		paramMap.put("enderecoCobranca", enderecoCobranca);
		paramMap.put("nrCobranca", nrCobranca);
		paramMap.put("cepCobranca", cepCobranca);
		paramMap.put("bairroCobranca", bairroCobranca);
		paramMap.put("endereco", endereco);
		paramMap.put("telefone", telefone);
		paramMap.put("nr", nr);
		paramMap.put("bairro", bairro);
		paramMap.put("cep", cep);
		paramMap.put("email", email);
		paramMap.put("cidadeEstado", cidadeEstado);
		paramMap.put("responsavelContrato", responsavelContrato);
		paramMap.put("cpfResponsavelContrato", cpfResponsavelContrato);
		paramMap.put("dtNascimentoResponsavelContrato", dtNascimentoResponsavelContrato);
		paramMap.put("cnpj", ContratoUtils.corrigeCpFCnpj(cnpj,tipoCliente) );
		paramMap.put("nomeFantasia", nomeFantasia);
		paramMap.put("telefoneContratada", telefoneContratada);
		
		SimpleDateFormat sdf = new SimpleDateFormat("d 'de' MMMM 'de' yyyy");  
		String dataAss = sdf.format(new Date());
		Long usuCodReg  = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("postosContrato.regionalPosto.usu_codreg")));
		String local = null;
		if (codEmp == 18 && codFil == 6) {
			local = "Goiânia/GO";
		} else {
			local = "São José"; //retornaCidadeRegional(usuCodReg); //agora vai ser fixo por causa da solicitação 561210.
		}
				
		paramMap.put("dataAss", local+ ", "  + dataAss);
		
		return paramMap;
	}
	
	public static String getValorTotalContrato(EntityWrapper w)
	{
		Collection<NeoObject> postos = w.findField("postosContrato").getValues();
		BigDecimal valorTotalContrato = new BigDecimal(0);
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			
			BigDecimal valorTotalPosto = new BigDecimal(0);
			if(wPosto.findValue("valorTotalPosto") != null)
				valorTotalPosto = (BigDecimal) wPosto.findValue("valorTotalPosto");
			
			valorTotalContrato.add(valorTotalPosto).setScale(2);
		}
		
		return NeoUtils.safeOutputString(valorTotalContrato);
	}
	
	public static Collection<MinutaTermoAditivoDataSource> preencheParametrosAditivo(NeoObject pedido)
	{
 		Collection<MinutaTermoAditivoDataSource> listaMinutas = new ArrayList<MinutaTermoAditivoDataSource>();
		Collection<TermoAditivoPostosDataSource> postosAditivoDS = new ArrayList<TermoAditivoPostosDataSource>();
		
		EntityWrapper wrapper = new EntityWrapper(pedido);
		Collection<NeoObject> postos = (Collection<NeoObject>)wrapper.findField("postosContrato").getValues(); 
		
		long tipoAlteracao = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");
		
		/*
		 * 1 - Alteração de CNPJ
		 * 2 - Alteração de Razão Social
		 * 3 - Ampliação Eletrônica
		 * 4 - Redução de Equipamentos
		 * 5 - HUMANAS
		 * 6 - Redução de Valores de Contrato
		 * 7 - HUMANAS
		 * 8 - HUMANAS
		 * 9 - Reinstalação Eletronica
		 * 10 - Venda Eletronica/CFTV
		 */
		BigDecimal totalContrato = new BigDecimal(0);
		
		for(NeoObject posto: postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			BigDecimal valorTotalPosto = (BigDecimal) wPosto.findValue("valorTotalPosto");
			
			totalContrato = totalContrato.add(valorTotalPosto);
		}
		
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			boolean sapiens = NeoUtils.safeBoolean(wPosto.findValue("origemSapiens"));
			boolean houveAlteracao = NeoUtils.safeBoolean(wPosto.findValue("houveAlteracaoPosto"));
			
			
			/*
			 * cada posto irá gerar uma minuta com pelo menos 3 peginas
			 * 1 sendo do contrato
			 * e para cada posto será uma com os dados do posto e uma folha para os anexos
			 */
			if(!sapiens || houveAlteracao)
			{
				TermoAditivoPostosDataSource termoAditivoPostos = new TermoAditivoPostosDataSource();
				
				String valorFinal = "";
				String valorFinalTxt = "";
				String dAplica = "";
				String dNaoAplica = "";
				String eAplica = "";
				String eNaoAplica = "";
				String valorAlt = "";
				String valorAltTxt = "";
				String dir_listaEquipamentos = "";
				String enderecoInstalacao = "";
				String estadoCidadeInstalacao = "";
				String referenciaInstalacao = "";
				String telefoneInstalacao1 = "";
				String telefoneInstalacao2 = "";
				String telefoneInstalacao3 = "";
				String cpfInstalacao = "";
				String bairroInstalacao = "";
				String numeroInstalacao = "";
				String fAplica = "";
				String fNaoAplica = "";
				String responsavelLocalInstalacao = "";
				String prazoMaximo = "";
				
				Long prazoMaximoVigencia = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.peridoMaximoVigencia2.meses")));
				String strPrazoMaximo = "";
				if (prazoMaximoVigencia != null){
					if (prazoMaximoVigencia.equals(0L)){
						strPrazoMaximo = "por prazo indeterminado, contado";
					}else if (prazoMaximoVigencia.equals(12L)){
						strPrazoMaximo = "pelo prazo de 12 (doze) meses, contados";
					}else if (prazoMaximoVigencia.equals(24L)){
						strPrazoMaximo = "pelo prazo de 24 (vinte e quatro) meses, contados";
					}else if (prazoMaximoVigencia.equals(36L)){
						strPrazoMaximo = "pelo prazo de 36 (trinta e seis) meses, contados";
					}else if (prazoMaximoVigencia.equals(48L)){
						strPrazoMaximo = "pelo prazo de 48 (quarenta e oito) meses, contados";
					}else if(prazoMaximoVigencia.equals(60L)) {
						strPrazoMaximo = "pelo prazo de 60 (sessenta) meses, contados";
					}else{
						strPrazoMaximo = "por prazo indeterminado, contado";
					}
				}else{
					strPrazoMaximo = "por prazo indeterminado, contado";
				}
				
				termoAditivoPostos.setPrazoMaximo(strPrazoMaximo);

				
				
				dir_listaEquipamentos = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "listaEquipamentosAditivo.jasper";
				
				String numeroPosto = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
				String numeroContrato = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
				
				Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos1 = new ArrayList<ListaEquipamentosMinutaDataSource>();
				Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos2 = new ArrayList<ListaEquipamentosMinutaDataSource>();
				
				String valorInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.valorInstalacao"));
				String valorInstalacaoTxt = NumberWordsOrsegups.convert(valorInstalacao.replace(".", ","));
				String numeroPontos = "";
				String numeroPontosTxt = "";
				String observacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.observacao"));
				
				
				String nomeExecutivo = NeoUtils.safeOutputString(wPosto.findValue("representante.nomrep"));
				
				Collection<NeoObject> listaEquipamentos = wPosto.findField("equipamentosItens.equipamentosPosto").getValues();
				
				if(listaEquipamentos.size() > 0)
				{
					dAplica = "X";
					
					Collection<ListaEquipamentosMinutaDataSource> listaEquips = getListaEquipamentosAditivo(wrapper, listaEquipamentos, tipoAlteracao);
					int count = 1;
					for(ListaEquipamentosMinutaDataSource equip : listaEquips)
					{
						if(count <= 12)
							listaEquipamentos1.add(equip);
						else
							listaEquipamentos2.add(equip);
						count++;
					}
				}else
					dNaoAplica = "X";
				
				long numeroPontosT = 0;
				for(NeoObject equip : listaEquipamentos)
				{
					EntityWrapper w = new EntityWrapper(equip);
					
					long qtd = 0;
					if(w.findValue("quantidade") != null)
						qtd = (Long) w.findValue("quantidade");
					
					long ponto = 0;
					if(w.findValue("produto.usu_ponpro") != null)
						ponto = (Long) w.findValue("produto.usu_ponpro");
					
					String tipoInst = NeoUtils.safeOutputString(w.findValue("tipoInstalacao.codigo"));
					if(tipoInst.equals("N") || tipoInst.equals("Y") || tipoInst.equals("R") ||
							tipoInst.equals("G") || tipoInst.equals("F") || tipoInst.equals("D"))
					{
						numeroPontosT += (qtd * ponto);
					}
					
					//numeroPontos += (qtd * ponto);
				}
				
				numeroPontos = NeoUtils.safeOutputString(numeroPontosT);
				numeroPontosTxt = NumberWordsOrsegups.convert(numeroPontos);
				
				BigDecimal valorTotalPosto = (BigDecimal) wPosto.findValue("valorTotalPosto");
				ContratoLogUtils.logInfo("valor total contrato - " + valorTotalPosto);
				if(valorTotalPosto.compareTo(new BigDecimal(0)) != 0)
				{
					eAplica = "X";
					BigDecimal valorTotalPostoAntigo = getValorTotalPostoBackup(wPosto, wrapper);
					
					
					valorAlt = NeoUtils.safeOutputString(valorTotalPostoAntigo);
					valorAltTxt = NumberWordsOrsegups.convert(valorAlt.replace(".", ","));
					
					
					valorFinal = NeoUtils.safeOutputString(valorTotalPosto);
					valorFinalTxt = NumberWordsOrsegups.convert(valorFinal.replace(".", ","));
				}else
				{
					eNaoAplica = "X";
				}
				
				boolean enderecoAlterado = verificaAlteracaoEnderecoPosto(wPosto);
				String telefone1Responsavel1 = "";
				String telefone1Responsavel2 = "";
				String telefone1Responsavel3 = "";
				String telefone2Responsavel1 = "";
				String telefone2Responsavel2 = "";
				String telefone2Responsavel3 = "";
				
				String cargoResponsavel1 = "";
				String cargoResponsavel2 = "";
				String cargoResponsavel3 = "";
				String nomeResponsavel1 = "";
				String nomeResponsavel2 = "";
				String nomeResponsavel3 = "";
				String nomeCliente = "";
				String cepReferencia = "";
				String cepInstalacao = "";
				
				if(enderecoAlterado || !sapiens)
				{
					fAplica = "X";
					
					nomeCliente = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.nomcli"));
					telefoneInstalacao1 = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.foncli"));
					telefoneInstalacao2 = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.telefoneRef"));
					enderecoInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.endereco"));
					numeroInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.numero"));
					bairroInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.bairro"));
					estadoCidadeInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.sigufs"));
					referenciaInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.referencia"));
					cepInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.cep"));
					cpfInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.cpfResponsavel"));
					responsavelLocalInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.responsavel"));
					cepReferencia = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.cepReferencia"));
					
					Collection<NeoObject> listaResponsaveis = wPosto.findField("vigilanciaEletronica.listaContatosPosto").getValues();
					int count = 0;
					for(NeoObject resp : listaResponsaveis)
					{
						count++;
						EntityWrapper wResp = new EntityWrapper(resp);
						
						if(count == 1)
						{
							nomeResponsavel1 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
							cargoResponsavel1 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
							telefone1Responsavel1 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
							telefone2Responsavel1 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
						}
						
						if(count == 2)
						{
							nomeResponsavel2 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
							cargoResponsavel2 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
							telefone1Responsavel2 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
							telefone2Responsavel2 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
						}
						
						if(count == 3)
						{
							nomeResponsavel3 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
							cargoResponsavel3 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
							telefone1Responsavel3 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
							telefone2Responsavel3 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
						}
					}
				}else
					fNaoAplica = "X";
				
				
				if(!valorFinal.equals(""))
				{
					termoAditivoPostos.setValorFinal("R$ " + valorFinal);
					termoAditivoPostos.setValorFinalTxt(valorFinalTxt + " reais");
				}else
				{
					termoAditivoPostos.setValorFinal(valorFinal);
					termoAditivoPostos.setValorFinalTxt(valorFinalTxt);
				}
					
				
				termoAditivoPostos.setCepReferencia(cepReferencia);
				termoAditivoPostos.setCepInstalacao(cepInstalacao);
				termoAditivoPostos.setdAplica(dAplica);
				termoAditivoPostos.setdNaoAplica(dNaoAplica);
				termoAditivoPostos.seteAplica(eAplica);
				termoAditivoPostos.seteNaoAplica(eNaoAplica);
				if(valorAlt.equals(""))
				{
					termoAditivoPostos.setValorAlt(valorAlt);
					termoAditivoPostos.setValorAltTxt(valorAltTxt);
				}else
				{
					termoAditivoPostos.setValorAlt("R$ " + valorAlt);
					termoAditivoPostos.setValorAltTxt(valorAltTxt + " reais");
				}
				
				if(!sapiens)
					termoAditivoPostos.setNovoPosto("X");
				else
					termoAditivoPostos.setAlteracaoPosto("X");
				
				termoAditivoPostos.setDir_listaEquipamentos(dir_listaEquipamentos);
				termoAditivoPostos.setEnderecoInstalacao(enderecoInstalacao);
				termoAditivoPostos.setEstadoCidadeInstalacao(estadoCidadeInstalacao);
				termoAditivoPostos.setReferenciaInstalacao(referenciaInstalacao);
				termoAditivoPostos.setTelefoneInstalacao2(telefoneInstalacao2);
				termoAditivoPostos.setTelefoneInstalacao1(telefoneInstalacao1);
				termoAditivoPostos.setTelefoneInstalacao3(telefoneInstalacao3);
				termoAditivoPostos.setCpfInstalacao(cpfInstalacao);
				termoAditivoPostos.setBairroInstalacao(bairroInstalacao);
				termoAditivoPostos.setNumeroInstalacao(numeroInstalacao);
				termoAditivoPostos.setfAplica(fAplica);
				termoAditivoPostos.setfNaoAplica(fNaoAplica);
				termoAditivoPostos.setResponsavelLocalInstalacao(responsavelLocalInstalacao);
				termoAditivoPostos.setTelefone1Responsavel1(telefone1Responsavel1);
				termoAditivoPostos.setTelefone1Responsavel2(telefone1Responsavel2);
				termoAditivoPostos.setTelefone1Responsavel3(telefone1Responsavel3);
				termoAditivoPostos.setTelefone2Responsavel1(telefone2Responsavel1);
				termoAditivoPostos.setTelefone2Responsavel2(telefone2Responsavel2);
				termoAditivoPostos.setTelefone2Responsavel3(telefone2Responsavel3);
				termoAditivoPostos.setNomeCliente(nomeCliente);
				termoAditivoPostos.setCargoResponsavel1(cargoResponsavel1);
				termoAditivoPostos.setCargoResponsavel2(cargoResponsavel2);
				termoAditivoPostos.setCargoResponsavel3(cargoResponsavel3);
				termoAditivoPostos.setNomeResponsavel1(nomeResponsavel1);
				termoAditivoPostos.setNomeResponsavel2(nomeResponsavel2);
				termoAditivoPostos.setNomeResponsavel3(nomeResponsavel3);
				termoAditivoPostos.setNumeroPosto(numeroPosto);
				termoAditivoPostos.setNumeroContrato(numeroContrato);
				termoAditivoPostos.setListaEquipamentos1(listaEquipamentos1);
				termoAditivoPostos.setListaEquipamentos2(listaEquipamentos2);
				//termoAditivoPostos.setListaAnexos(listaAnexos);
				if(valorInstalacao.equals(""))
				{
					termoAditivoPostos.setValorInstalacao(valorInstalacao);
					termoAditivoPostos.setValorInstalacaoTxt(valorInstalacaoTxt);
				}else
				{
					termoAditivoPostos.setValorInstalacao("R$ " + valorInstalacao);
					termoAditivoPostos.setValorInstalacaoTxt(valorInstalacaoTxt + " reais");
				}
				
				termoAditivoPostos.setNumeroPontos(numeroPontos);
				termoAditivoPostos.setNumeroPontosTxt(numeroPontosTxt);
				termoAditivoPostos.setObservacao(observacao);
				termoAditivoPostos.setNomeExecutivo(nomeExecutivo);
				postosAditivoDS.add(termoAditivoPostos);
			}
		}
		MinutaTermoAditivoDataSource minuta = new MinutaTermoAditivoDataSource();
		minuta.setListaAditivosPosto(postosAditivoDS);
		listaMinutas.add(minuta);
		return listaMinutas;
	}
	
	public static boolean verificaAlteracaoEnderecoPosto(EntityWrapper wPosto)
	{
		Collection<NeoObject> listaAlteracoes = wPosto.findField("listaAlteracoes").getValues();
		for(NeoObject alteracao : listaAlteracoes)
		{
			EntityWrapper wAlt = new EntityWrapper(alteracao);
			
			if(NeoUtils.safeOutputString(wAlt.findValue("campo")).equals("Endereço do Posto no Contrato"))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/*
	 * retorna a diferença do posto atual com o posto antigo
	 */
	public static BigDecimal getValorTotalPostoBackup(EntityWrapper wPosto, EntityWrapper wContrato)
	{
		BigDecimal valorTotal = new BigDecimal(0);
		BigDecimal valorTotalPosto = (BigDecimal) wPosto.findValue("valorTotalPosto");
		
		if(NeoUtils.safeIsNull(wPosto.findValue("numPosto")))
		{
			//valorTotal = valorTotalPosto;
			
		}else
		{
			long numeroPosto = (Long) wPosto.findValue("numPosto");
			
			Collection<NeoObject> postosBackup = wContrato.findField("postosContratoBackup").getValues();
			NeoObject postoAlvo = null;
			for(NeoObject posto : postosBackup)
			{
				EntityWrapper wPostoBackup = new EntityWrapper(posto);
				
				long numeroPostoBackup = (Long) wPostoBackup.findValue("numPosto");
				if(numeroPostoBackup == numeroPosto)
					postoAlvo = posto;
			}
			
			if(postoAlvo == null)
			{
				//valorTotal = new BigDecimal(0);
			}else
			{
				EntityWrapper wPostoBackup = new EntityWrapper(postoAlvo);
				BigDecimal valorPostoBackup = (BigDecimal) wPostoBackup.findValue("valorTotalPosto");
				
				if(valorPostoBackup.compareTo(valorTotalPosto) == -1)
				{
					valorTotal = valorTotalPosto.subtract(valorPostoBackup);
				}else if(valorPostoBackup.compareTo(valorTotalPosto) == 1)
				{
					//valorTotal = valorPostoBackup.subtract(valorTotalPosto);
					valorTotal = valorTotalPosto.subtract(valorPostoBackup); // 21/05/2014 - feito assim pois reclamaram que o executivo sempre põe a sinal - quando é uma redução de valor do posto
				}
			}
		}
			
		return valorTotal;
	}

	/**
	 * Preenche o mapa de parâmetros enviados ao relatório.
	 */
	public static Collection<MinutaDataSource> preencheParametros(NeoObject pedido, boolean CFTV)
	{
		/*
		 * aqui popula os fields
		 */
		
		Collection<MinutaDataSource> minutas = new ArrayList<MinutaDataSource>();

		try
		{
			EntityWrapper wrapper = new EntityWrapper(pedido);
			MinutaDataSource minuta = new MinutaDataSource();
			
			Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
			Collection<NeoObject> equipamentos = new ArrayList<NeoObject>();
			
			for(NeoObject posto : postos)
			{
				EntityWrapper wPosto = new EntityWrapper(posto);
				
				//adiciona os equipamentos
				if (wPosto.findValue("equipamentosItens.equipamentosPosto")!= null){
					equipamentos.addAll(wPosto.findField("equipamentosItens.equipamentosPosto").getValues());
				}
				
			}
			
			//TODO e se houverem mais de 32 itens??
			
			//lista de anexos
			if(CFTV)
			{
				Collection<ListaAnexosMinutaDataSource> listaAnexos = getListaAnexos(wrapper, postos, true);
				minuta.setListaAnexos(listaAnexos);
			}else
			{
				Collection<ListaAnexosMinutaDataSource> listaAnexos = getListaAnexos(wrapper, postos, false);
				minuta.setListaAnexos(listaAnexos);
			}
			
			
			minutas.add(minuta);
		}
		catch (Exception e)
		{
			log.error("Erro ao preencher o mapa de parâmetros da Impressão do Relatório", e);
			e.printStackTrace();
		}
		return minutas;
	}
	
	
	public static Collection<ListaAnexosMinutaDataSource> getListaAnexos(EntityWrapper wContrato, Collection<NeoObject> postos, boolean CFTV)
	{
		/*
		 * para cada posto deve ser gerado um obeto do tipo ListaAnexosMinutaDataSource
		 */
		
		long codEmp = (Long) wContrato.findValue("empresa.codemp");
		long codFil = (Long) wContrato.findValue("empresa.codfil");
		
		ArrayList<ListaAnexosMinutaDataSource> listaAnexos = new ArrayList<ListaAnexosMinutaDataSource>();
		long tipoMov = (Long) wContrato.findValue("movimentoContratoNovo.codTipo");
		
		ArrayList<NeoObject> postosList = new ArrayList<NeoObject>(postos); // copia dos postos para poder pegar pelo indice na validacao do PGO
		
		int i = 0;
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			
			if ( NeoUtils.safeOutputString(wPosto.findValue("tipoPosto.codigo")).equals("1") ){ // só processa posto normal
				ListaAnexosMinutaDataSource anexo = new ListaAnexosMinutaDataSource();
				
				SimpleDateFormat sdf = new SimpleDateFormat("EEEE, d 'de' MMMM 'de' yyyy");  
				String dataAss = sdf.format(new Date());
				
				String nomeCliente = "";
				String telefoneInstalacao1 = "";
				String contratanteCPF = "";
				String tipoCliente = null;
				if(tipoMov == 3 || tipoMov == 4)
				{
					nomeCliente = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.razaoSocial"));
					telefoneInstalacao1 = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.telefone1"));
					tipoCliente = NeoUtils.safeOutputString(wContrato.findValue("tipocliente.tipo"));
					if("F".equals(tipoCliente))
						contratanteCPF = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.cpf"));
					else
						contratanteCPF = NeoUtils.safeOutputString(wContrato.findValue("novoCliente.cnpj"));
				}else
				{
					tipoCliente = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.tipcli"));
					nomeCliente = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.nomcli"));
					telefoneInstalacao1 = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.foncli"));
					contratanteCPF = NeoUtils.safeOutputString(wContrato.findValue("buscaNomeCliente.cgccpf"));
				}
				
				String responsavelLocal = "";
				String cpfInstalacao = "";
				String telefoneInstalacao2 = "";
				
				/*
				 * hoje o layout suporta apenas 3 responsáveis
				 */
				String nomeResponsavel1 = "";
				String nomeResponsavel2 = "";
				String nomeResponsavel3 = "";
				String cargoResponsavel1 = "";
				String cargoResponsavel2 = "";
				String cargoResponsavel3 = "";
				String telefone1Responsavel1 = "";
				String telefone1Responsavel2 = "";
				String telefone1Responsavel3 = "";
				String telefone2Responsavel1 = "";
				String telefone2Responsavel2 = "";
				String telefone2Responsavel3 = "";
				
				List<NeoObject> listaResponsaveis = (List<NeoObject>) wPosto.findField("vigilanciaEletronica.listaContatosPosto").getValues();
				int count = 0;
				for(NeoObject resp : listaResponsaveis)
				{
					count++;
					EntityWrapper wResp = new EntityWrapper(resp);
					
					if(count == 1)
					{
						/*
						 * o primeiro da lista é o principal responsável, que vai para a minuta
						 */
						responsavelLocal = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.responsavel"));
						cpfInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.cpfResponsavel"));
						telefoneInstalacao2 = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.telefoneRef"));
						
						nomeResponsavel1 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
						cargoResponsavel1 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
						telefone1Responsavel1 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
						telefone2Responsavel1 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
					}
					
					if(count == 2)
					{
						nomeResponsavel2 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
						cargoResponsavel2 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
						telefone1Responsavel2 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
						telefone2Responsavel2 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
					}
					
					if(count == 3)
					{
						nomeResponsavel3 = NeoUtils.safeOutputString(wResp.findValue("nomecontato"));
						cargoResponsavel3 = NeoUtils.safeOutputString(wResp.findValue("funcao"));	
						telefone1Responsavel3 = NeoUtils.safeOutputString(wResp.findValue("telefone1"));
						telefone2Responsavel3 = NeoUtils.safeOutputString(wResp.findValue("telefone2"));
					}
				}
				
				
				String enderecoInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.endereco"));
				String numeroInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.numero"));;
				String bairroInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.bairro"));;
				String estadoCidadeInstalacao = NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.nomcid")) + " / " + NeoUtils.safeOutputString(wPosto.findValue("enderecoPosto.estadoCidade.cidade.sigufs"));
				
				String referenciaInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.referencia"));
				String telefoneInstalacao3 = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.telefoneRef"));
				
				
				String valorInstalacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.valorInstalacao"));
				String observacao = NeoUtils.safeOutputString(wPosto.findValue("vigilanciaEletronica.observacao"));
				
				BigDecimal valorPosto = (BigDecimal) wPosto.findValue("formacaoPreco.valorMontanteB");//(BigDecimal) wPosto.findValue("valorTotalPosto");
				if (valorPosto == null){
					valorPosto = new BigDecimal("0");
				}
				
				String strValorPosto = NeoUtils.safeOutputString(valorPosto);
				String strValorPostoExtenso = NumberWordsOrsegups.convertMoeda(valorPosto);
				
				if (strValorPostoExtenso != null){ // tratamento feito para remover a palavra inteiro(s) por "" sem comprometer a compatibilidade da lib com o resto do sistema.
					strValorPostoExtenso = strValorPostoExtenso.replace("inteiros", "")
					.replace("inteiro", "");
				}
				
				/*observacao PGO*/
				
				NeoObject postoPgo = null;

				if (i+1 < postosList.size()){
					postoPgo = postosList.get(i+1); // o posto PGO por convenção sempre será a proximo em relação ao posto normal. Se não for, o usuário cadastrou incorretamente.
				}
				
				String obsPgo = "";
				if (postoPgo != null){
					EntityWrapper wPostoPgo = new EntityWrapper(postoPgo);
					if ( NeoUtils.safeOutputString(wPostoPgo.findValue("tipoPosto.codigo")).equals("2") ){
						obsPgo += " / ";
						
						BigDecimal valorPostoPGO = (BigDecimal) wPostoPgo.findValue("formacaoPreco.valorMontanteB");//(BigDecimal) wPostoPgo.findValue("valorTotalPosto");
						if (valorPostoPGO == null){
							valorPostoPGO = new BigDecimal("0");
						}
						
						String strValorPostoPGO = NeoUtils.safeOutputString(valorPostoPGO);
						String strValorPostoPGOExtenso = NumberWordsOrsegups.convertMoeda(valorPostoPGO);
						
						
						if (strValorPostoPGOExtenso != null){ // tratamento feito para remover a palavra inteiro(s) por "" sem comprometer a compatibilidade da lib com o resto do sistema.
							strValorPostoPGOExtenso = strValorPostoPGOExtenso.replace("inteiros", "")
							.replace("inteiro", "");
						}
						
						obsPgo += "Valor do Seguro PGO: " + strValorPostoPGO + " " + strValorPostoPGOExtenso;
						
					}
				}
				/*Fim observação PGO*/
				
				observacao = "Valor do Posto: " + strValorPosto + " " + strValorPostoExtenso + " " + obsPgo + " -  " + observacao;
				
				
				long numeroPontos = 0;
				Collection<NeoObject> listaEquip = null;
				
				if (wPosto.findValue("equipamentosItens") != null){
					listaEquip = wPosto.findField("equipamentosItens.equipamentosPosto").getValues();
				}else{
					listaEquip = new ArrayList<NeoObject>();
					
				}
				
				for(NeoObject equip : listaEquip)
				{
					EntityWrapper w = new EntityWrapper(equip);
					
					long qtd = 0;
					if(w.findValue("quantidade") != null)
						qtd = (Long) w.findValue("quantidade");
					
					long ponto = 0;
					if(w.findValue("produto.usu_ponpro") != null)
						ponto = (Long) w.findValue("produto.usu_ponpro");
					
					/*
					 * somente calcular os pontos de equipamentos com letras: N, Y, R, F e D
					 * R - Reinstalação de Equipamentos Orsegups
					 * N - Equipamento Novo (Estoque)
					 * F - Equipamento Faturado Direto para o Cliente (GSN)
					 * D - Equipamento Compra Direta (GSN)
					 * Y - Reinstalação Equipamentos do Cliente
					 */
					String tipoInst = NeoUtils.safeOutputString(w.findValue("tipoInstalacao.codigo"));
					if(tipoInst.equals("N") || tipoInst.equals("Y") || tipoInst.equals("R") ||
							tipoInst.equals("G") || tipoInst.equals("F") || tipoInst.equals("D"))
					{
						numeroPontos += (qtd * ponto);
					}
				}
				
				//deve retornar uma lista com 32 elementos, mesmo que seja completado com elementos que tenham campo vazio
				Collection<ListaEquipamentosMinutaDataSource> listaEquipamento = getListaEquipamentos(wContrato, listaEquip);
				
				//cada lista deve conter exatamente 16 registros
				Collection<ListaEquipamentosMinutaDataSource> listaEquipamento1 = new ArrayList<ListaEquipamentosMinutaDataSource>();
				Collection<ListaEquipamentosMinutaDataSource> listaEquipamento2 = new ArrayList<ListaEquipamentosMinutaDataSource>();
				count = 0;
				for(ListaEquipamentosMinutaDataSource a : listaEquipamento)
				{
					count++;
					if(count > 16)
					{
						listaEquipamento2.add(a);
					}else
					{
						listaEquipamento1.add(a);
					}
				}
				
				/*
				 * Inicio checklist na lista de equipamentos
				 */
				Collection<NeoObject> listaCheckList = null;
				
				if (wPosto.findValue("equipamentosItens") != null){
					listaCheckList = wPosto.findField("equipamentosItens.itensChecklist").getValues();
				}else{
					listaCheckList = new ArrayList<NeoObject>();
					
				}
				
				Collection<ListaEquipamentosMinutaDataSource> listaCheckListAsEquipamento = getListaCheckListAsEquipamentos(wContrato, listaCheckList);
				for (ListaEquipamentosMinutaDataSource a : listaCheckListAsEquipamento)
				if ( listaEquipamento1.size() > 16 ){
					listaEquipamento2.add(a);
				}else{
					listaEquipamento1.add(a);
				}
				
				
				/*
				 * garantir que a lista de retono tenha exatamente 16 elementos
				 * se for necessário completar com elementos com campo vazio
				 */
				while(listaEquipamento1.size() < 16)
				{
					ListaEquipamentosMinutaDataSource equip = new ListaEquipamentosMinutaDataSource();
					equip.setItem(" ");
					equip.setQtd(" ");
					equip.setLocado(" ");
					//PersistEngine.persist(equip);
					listaEquipamento1.add(equip);
				}
				while(listaEquipamento2.size() < 16)
				{
					ListaEquipamentosMinutaDataSource equip = new ListaEquipamentosMinutaDataSource();
					equip.setItem(" ");
					equip.setQtd(" ");
					equip.setLocado(" ");
					//PersistEngine.persist(equip);
					listaEquipamento2.add(equip);
				}
				
				/*
				 * Fim checklist na lista de equipamentos
				 */
				
				
				
				String dir_listaEquipamentos 	= NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "listaEquipamentos.jasper";
				anexo.setDir_listaEquipamentos(dir_listaEquipamentos);
				// os primeiros 16 vão nesta lista
				anexo.setListaEquipamentos1(listaEquipamento1);
				
				//os demais devem ir nesta lista
				anexo.setListaEquipamentos2(listaEquipamento2);
				
				String nomeExecutivo = NeoUtils.safeOutputString(wPosto.findValue("representante.nomrep"));
				anexo.setNomeExecutivo(nomeExecutivo);
				
				if(CFTV)
				{
					anexo.setNomeCliente(nomeCliente);
					anexo.setResponsavelLocal(responsavelLocal);
					anexo.setEnderecoInstalacao(enderecoInstalacao);
					anexo.setReferenciaInstalacao(referenciaInstalacao);
					anexo.setNumeroInstalacao(numeroInstalacao);
					anexo.setCpfInstalacao(cpfInstalacao);
					anexo.setBairroInstalacao(bairroInstalacao);
					anexo.setTelefoneInstalacao1(telefoneInstalacao1);
					anexo.setTelefoneInstalacao2(telefoneInstalacao2);
					anexo.setTelefoneInstalacao3(telefoneInstalacao3);
					anexo.setEstadoCidadeInstalacao(estadoCidadeInstalacao);
					
					anexo.setNumeroPontos(NeoUtils.safeOutputString(numeroPontos));
					anexo.setNumeroPontosTxt(NumberWordsOrsegups.convert(NeoUtils.safeOutputString(numeroPontos)));
					anexo.setValorInstalacao(valorInstalacao);
					anexo.setValorInstalacaoTxt(NumberWordsOrsegups.convert(NeoUtils.safeOutputString(valorInstalacao)));
					anexo.setObservacao(observacao);
					
					
				}else
				{
					boolean tel = NeoUtils.safeBoolean(wContrato.findValue("dadosGeraisContrato.termo12a"));
					boolean gprs = NeoUtils.safeBoolean(wContrato.findValue("dadosGeraisContrato.termo12b"));
					boolean ethernet = NeoUtils.safeBoolean(wContrato.findValue("dadosGeraisContrato.termo12c"));
					
					boolean segundaViaMonitoramento = gprs || ethernet;
					
					/*
					 * Agora a regra vale para as duas empresas.
					 * 
					 * */
					if (tel && segundaViaMonitoramento){ 
						anexo.setPrintAnexo2("NÃO");
						anexo.setPrintAnexo3("NÃO");
					}else if (tel && !segundaViaMonitoramento){// mostra só anexo II
						anexo.setPrintAnexo2("SIM");
						anexo.setPrintAnexo3("NÃO");
					}else if (!tel && segundaViaMonitoramento){  // mostra só anexo III
						anexo.setPrintAnexo2("NÃO");
						anexo.setPrintAnexo3("SIM");
					}else if (!tel && !segundaViaMonitoramento){
						anexo.setPrintAnexo2("NÃO");
						anexo.setPrintAnexo3("NÃO");
					}else{
						anexo.setPrintAnexo2("NÃO");
						anexo.setPrintAnexo3("NÃO");
					}
					
					
					
					/*if(!monitoramento)
				{
					//setar flag no relatório para esconder termo de responsabilidade
					anexo.setPrintAnexo2("SIM");
				}else{
					anexo.setPrintAnexo2("NÃO");
				}
				if(!gprs)
				{
					//setar flag no relatório para esconder termo de responsabilidade
					anexo.setPrintAnexo3("SIM");
				}else{
					anexo.setPrintAnexo3("NÃO");
				}*/
					anexo.setDataAss(dataAss);
					anexo.setNomeCliente(nomeCliente);
					anexo.setResponsavelLocal(responsavelLocal);
					anexo.setEnderecoInstalacao(enderecoInstalacao);
					anexo.setReferenciaInstalacao(referenciaInstalacao);
					anexo.setNumeroInstalacao(numeroInstalacao);
					anexo.setCpfInstalacao(cpfInstalacao);
					anexo.setBairroInstalacao(bairroInstalacao);
					anexo.setTelefoneInstalacao1(telefoneInstalacao1);
					anexo.setTelefoneInstalacao2(telefoneInstalacao2);
					anexo.setTelefoneInstalacao3(telefoneInstalacao3);
					anexo.setEstadoCidadeInstalacao(estadoCidadeInstalacao);
					anexo.setNomeResponsavel1(nomeResponsavel1);
					anexo.setNomeResponsavel2(nomeResponsavel2);
					anexo.setNomeResponsavel3(nomeResponsavel3);
					anexo.setCargoResponsavel1(cargoResponsavel1);
					anexo.setCargoResponsavel2(cargoResponsavel2);
					anexo.setCargoResponsavel3(cargoResponsavel3);
					anexo.setTelefone1Responsavel1(telefone1Responsavel1);
					anexo.setTelefone1Responsavel2(telefone1Responsavel2);
					anexo.setTelefone1Responsavel3(telefone1Responsavel3);
					anexo.setTelefone2Responsavel1(telefone2Responsavel1);
					anexo.setTelefone2Responsavel2(telefone2Responsavel2);
					anexo.setTelefone2Responsavel3(telefone2Responsavel3);
					anexo.setNumeroPontos(NeoUtils.safeOutputString(numeroPontos));
					anexo.setValorInstalacao(valorInstalacao);
					anexo.setObservacao(observacao);
					anexo.setContratanteCPF( ContratoUtils.corrigeCpFCnpj(contratanteCPF, tipoCliente ));
				}
				
				
				
				listaAnexos.add(anexo);
				
			}
			
			i++;
		}
		
		return listaAnexos;
	}
	
	public static Collection<ListaEquipamentosMinutaDataSource> getListaEquipamentosAditivo(EntityWrapper wContrato, Collection<NeoObject> equipamentos, long tipoAlteracao)
	{
		Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos = new ArrayList<ListaEquipamentosMinutaDataSource>();
		
		for(NeoObject equipamento : equipamentos)
		{
			EntityWrapper w = new EntityWrapper(equipamento);
			
			String qtd = NeoUtils.safeOutputString(w.findValue("quantidade"));
			String item = NeoUtils.safeOutputString(w.findValue("produto.despro"));
			String tipoInstalacao = NeoUtils.safeOutputString(w.findValue("tipoInstalacao.codigo"));
			String locado = "";
			String IX = "";
			/*
			 * TODO quais tipos de instalação define um equipamento locado??
			 */
			if("N".equals(tipoInstalacao) || "E".equals(tipoInstalacao) || "R".equals(tipoInstalacao))
			{
				locado = "SIM";
			}else 
			{
				//empresa.codfil
				long codemp = (Long)wContrato.findValue("empresa.codemp");
				long codfil = (Long)wContrato.findValue("empresa.codfil");
				if(/*codemp == 18 && codfil == 2 &&*/ "D".equals(tipoInstalacao)) // alterado para todas empresas conforme solicitado na tarefa 691776 
				{
					locado = "SIM";
				}else{
					locado = "NÃO";
				}
			}
			
			if(tipoInstalacao.equals("X"))
				IX = "X";
			else
				IX = "I";
				
			
			ListaEquipamentosMinutaDataSource equip = new ListaEquipamentosMinutaDataSource();
			equip.setItem(item);
			equip.setQtd(qtd);
			equip.setLocado(locado);
			equip.setIX(IX);
			//PersistEngine.persist(equip);
			listaEquipamentos.add(equip);
		}
		
		/*
		 * garantir que a lista de retono tenha exatamente 32 elementos
		 * se for necessário completar com elementos com campo vazio
		 */
		while(listaEquipamentos.size() < 24)
		{
			ListaEquipamentosMinutaDataSource equip = new ListaEquipamentosMinutaDataSource();
			equip.setItem(" ");
			equip.setQtd(" ");
			equip.setLocado(" ");
			equip.setIX(" ");
			//PersistEngine.persist(equip);
			listaEquipamentos.add(equip);
		}
		
		return listaEquipamentos;
	}
	
	public static Collection<ListaEquipamentosMinutaDataSource> getListaEquipamentos(EntityWrapper wContrato, Collection<NeoObject> equipamentos)
	{
		Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos = new ArrayList<ListaEquipamentosMinutaDataSource>();
		
		for(NeoObject equipamento : equipamentos)
		{
			EntityWrapper w = new EntityWrapper(equipamento);
			
			String tipoInst = NeoUtils.safeOutputString(w.findValue("tipoInstalacao.codigo"));
			
			String qtd = NeoUtils.safeOutputString(w.findValue("quantidade"));
			String item = NeoUtils.safeOutputString(w.findValue("produto.despro"));
			String tipoInstalacao = NeoUtils.safeOutputString(w.findValue("tipoInstalacao.codigo"));
			String locado = "";
			/*
			 * TODO quais tipos de instalação define um equipamento locado??
			 */
			if("N".equals(tipoInstalacao) || "E".equals(tipoInstalacao) || "R".equals(tipoInstalacao))
			{
				locado = "SIM";
			}else 
			{
				//empresa.codfil
				long codemp = (Long)wContrato.findValue("empresa.codemp");
				long codfil = (Long)wContrato.findValue("empresa.codfil");
				if(/*codemp == 18 && codfil == 2 &&*/ "D".equals(tipoInstalacao)) // alterado para todas empresas conforme solicitado na tarefa 691776 
				{
					locado = "SIM";
				}else{
					locado = "NÃO";
				}
			}
			
			if ("X".equals(tipoInst)){
				ContratoLogUtils.logInfo("[EQP1] "+item+" - Equipamento desconsiderado na minuta pois tem tipo de instalação X");
			}else{
				ContratoLogUtils.logInfo("[EQP1] "+item+" - Adicionado na minuta OK");
				ListaEquipamentosMinutaDataSource equip = new ListaEquipamentosMinutaDataSource();
				equip.setItem(item);
				equip.setQtd(qtd);
				equip.setLocado(locado);
				//PersistEngine.persist(equip);
				listaEquipamentos.add(equip);
			}
		}
		
		/*
		 * garantir que a lista de retono tenha exatamente 32 elementos
		 * se for necessário completar com elementos com campo vazio
		 */
		/*while(listaEquipamentos.size() < 32)
		{
			ListaEquipamentosMinutaDataSource equip = new ListaEquipamentosMinutaDataSource();
			equip.setItem(" ");
			equip.setQtd(" ");
			equip.setLocado(" ");
			//PersistEngine.persist(equip);
			listaEquipamentos.add(equip);
		}*/
		
		return listaEquipamentos;
	}
	
	public static Collection<ListaEquipamentosMinutaDataSource> getListaCheckListAsEquipamentos(EntityWrapper wContrato, Collection<NeoObject> equipamentos)
	{
		Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos = new ArrayList<ListaEquipamentosMinutaDataSource>();
		
		for(NeoObject equipamento : equipamentos)
		{
			EntityWrapper w = new EntityWrapper(equipamento);
			
			
			String qtd = NeoUtils.safeOutputString(w.findValue("quantidade"));
			NeoObject itm = (NeoObject) w.findValue("equipamentosItens.itemChecklist");
			EntityWrapper wItm = new EntityWrapper(itm);
			
			String item = NeoUtils.safeOutputString(wItm.findValue("usu_desitm"));
			String locado = "";
			long codemp = (Long)wContrato.findValue("empresa.codemp");
			long codfil = (Long)wContrato.findValue("empresa.codfil");
			
							
			locado = " - ";
				
			if (item != null && item.contains("GPRS")){
				ContratoLogUtils.logInfo("[CHK1] "+item+" - Checklist Adicionado na minuta OK");
				ListaEquipamentosMinutaDataSource equip = new ListaEquipamentosMinutaDataSource();
				equip.setItem(item);
				equip.setQtd(qtd);
				equip.setLocado(locado);
				//PersistEngine.persist(equip);
				listaEquipamentos.add(equip);
			}
			
		}
		
		/*
		 * garantir que a lista de retono tenha exatamente 32 elementos
		 * se for necessário completar com elementos com campo vazio
		 
		while(listaEquipamentos.size() < 32)
		{
			ListaEquipamentosMinutaDataSource equip = new ListaEquipamentosMinutaDataSource();
			equip.setItem(" ");
			equip.setQtd(" ");
			equip.setLocado(" ");
			//PersistEngine.persist(equip);
			listaEquipamentos.add(equip);
		}*/
		
		return listaEquipamentos;
	}
	
	public static void autoGeracaoMinuta(EntityWrapper wrapper){
		String idProcessoContrato = NeoUtils.safeOutputString( wrapper.findValue("neoId") );
		System.out.println("idProcesso: " + idProcessoContrato);
		
		try
		{
			if (idProcessoContrato != null && !idProcessoContrato.isEmpty()) 
			{
				File fileProposta = null;
				{
					fileProposta = MinutaContrato.geraPDF(idProcessoContrato);
				}
				
				if(fileProposta != null )
				{
					NeoFile minuta = NeoStorage.getDefault().copy(fileProposta);
					
					NeoObject historicoMinuta = AdapterUtils.createNewEntityInstance("FGCHistoricoMinuta");
					EntityWrapper wHistoricoMinuta = new EntityWrapper(historicoMinuta);
					wHistoricoMinuta.setValue("minuta", minuta);
					PersistEngine.persist(historicoMinuta);
					
					wrapper.findField("historicoMinuta").addValue(historicoMinuta);
				}
				
			}
		}
		catch (Exception e)
		{
			ContratoLogUtils.logInfo("Erro ao gerar automaticamente a minuta. msg:" + e);
			e.printStackTrace();
		}
		
	}
	
	
	
	
	public static void main(String args[]){
			String cgccpf = "8491597000126"; //
			//cgccpf = "04086517922"; 
			System.out.println(ContratoUtils.corrigeCpFCnpj(cgccpf,"J"));
	}
	
	public static String retornaClausula12_1(NeoObject contrato){
		Long codReg = 0L;
		String nomCid = "";
		String clausula = "";
		EntityWrapper wContrato = new EntityWrapper(contrato);
		List<NeoObject> listaPostosFusion = (List<NeoObject>) wContrato.findField("postosContrato").getValue();
		for (NeoObject posto : listaPostosFusion)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			
			NeoObject oTipoPosto = (NeoObject) wPosto.findValue("tipoPosto");
			EntityWrapper wTipoPosto = new EntityWrapper(oTipoPosto);
			
			if (!NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("2")) 
			{ 
				codReg = NeoUtils.safeLong(NeoUtils.safeOutputString(wPosto.findValue("regionalPosto.usu_codreg")));
				break;
			}
		}
		
		if(codReg == 13L || codReg == 14L || codReg == 15L)
		{
			nomCid = "Curitiba";
		}else if (codReg == 1L){
			nomCid = "São José";
		}else if (codReg == 2L){
			nomCid = "Itajaí";
		}else if (codReg == 3L){
			nomCid = "Brusque";
		}else if (codReg == 4L){
			nomCid = "Blumenau";
		}else if (codReg == 5L){
			nomCid = "Joinville";
		}else if (codReg == 6L){
			nomCid = "Lages";
		}else if (codReg == 7L){
			nomCid = "Criciúma";
		}else if (codReg == 10L){
			nomCid = "Chapecó";
		}else if (codReg == 11L){
			nomCid = "Rio do Sul";
		}else if (codReg == 12L){
			nomCid = "Jaraguá do Sul";
		}else if (codReg == 16L){
			nomCid = "Tubarão";
		}else{
			nomCid = "Itajaí";
		}
		
		
		clausula = "12.1 Para dirimir qualquer conflito relativo à interpretação e/ou execução deste instrumento, "
				+ "fica desde já eleito, com expressa exclusão de qualquer outro, por mais privilegiado que seja, "
				+ "o Foro da Comarca de "+nomCid+", situada no Estado de Santa Catarina, podendo ainda a CONTRATADA, "
				+ "a seu exclusivo critério, optar pelo foro da sede da CONTRATANTE. Este contrato é celebrado em 02 (duas) vias "
				+ "de igual teor e forma, para um só efeito, sendo subscrito por 02 (duas) testemunhas.";
		
		return clausula;
	}
	
}
