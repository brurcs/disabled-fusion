package com.neomind.fusion.custom.orsegups.contract;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.eform.converter.template.MaskTemplate;
import com.neomind.fusion.eform.mask.MaskFactory;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldInfo;
import com.neomind.fusion.entity.FieldInfoMaskTemplate;
import com.neomind.fusion.entity.FieldInfoText;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.portal.VelocityUtils;
import com.neomind.util.CustomFlags;
import com.neomind.util.NeoUtils;

public class ContractSerasaConverter extends StringConverter{
	
	private static final Log log = LogFactory.getLog(ContractCEPConverter.class);
	
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		final FieldInfo fieldInfo = field.getFieldInfo();
		VelocityContext context = new VelocityContext();
		if (fieldInfo instanceof FieldInfoText)
		{
			FieldInfoText fit = (FieldInfoText) fieldInfo;
			NeoObject inf = PersistEngine.reload(fit);
			if (inf != null)
			{
				fit = (FieldInfoText) inf;
			}

			final String id = getFieldId(field);

			context.put("info", fit);
			context.put("field", field);
			context.put("id", id);
			context.put("baseURL", PortalUtil.getBaseURL());
			context.put("entity", field.getForm().getObject());
			context.put("showRemainingCharacters", fit.isShowRemainingCharacters());

			if (fit.isTextArea())
			{
				String mask = null;
				if (!CustomFlags.FBB && field.getFieldInfo() instanceof FieldInfoMaskTemplate)
				{
					FieldInfoMaskTemplate fimt = (FieldInfoMaskTemplate) field.getFieldInfo();
					MaskTemplate typeMask = fimt.getTypeMask();
					mask = typeMask.getJSMask();
				}
				if (NeoUtils.safeString(mask) == null)
					mask = MaskFactory.getMask(field); // Reinaldo

				context.put("id", id);

				// RTE
				if (fit.isRichText())
				{
					context.put("value", ((String) (field.getValue() != null ? field.getValue() : ""))
							.replaceAll("\n", " ").replaceAll("\r", " "));
				}
				else
				{
					context.put("value", (field.getValue() != null ? field.getValue() : ""));
				}
				
				context.put("mandatory", isFieldMandatory(field));
				context.put("mask", mask);
				context.put("maxLength", fit.getInputMaxLength());
				context.put("maskControl", MaskFactory.getMaskHTMLControl(field));
				context.put("rte", fit.isRichText());
				String rtrn = VelocityUtils.runTemplate("textField.vm", context);
				
				// busca eformPai aos dados gerais
				Long eform = field.getForm().getCaller().getObjectId();
				
				String botao = "<input type=\"button\" class=\"input_button\" id=\"bt_import\" value=\"Consultar\" onClick = \"javascript:consultaSerasa('"+eform+"');\"/>";
				
				return rtrn + botao;
			}
			
		}
		
		return super.getHTMLInput(field, origin);
	}

}
