package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class CopyOfOrsegupsMedicaoUtilsMensal5
{

	public static void inserePostosSemColaborador(String datCpt)
	{
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT c.usu_numofi,EMP.nomemp, REG.USU_NomReg, usu_apecli, SUBSTRING(hie.CodLoc, 1, 12) AS NumLocCli, orn.numloc, hie.codloc, SUBSTRING(hie.CodLoc, 1, 24) AS CodPai, cvs.usu_nomloc, cvs.usu_desser, pos.usu_codser, cvs.usu_numctr, cvs.usu_numpos, orn.usu_lotorn, ");
		sql.append(" cvs.usu_codreg, orn.usu_cliorn, cvs.usu_sitcvs, cvs.usu_qtdcon, cvs.usu_numloc, cvs.usu_codccu, cvs.usu_codemp, cvs.usu_codfil, cvs.usu_endloc,  ");
		sql.append(" cid.NomCid, cid.EstCid, cvs.usu_endcep, orn.usu_cplctr,orn.usu_nomcid, cvs.usu_preuni * cvs.usu_qtdcvs AS valor, cvs.usu_datini, cvs.usu_datfim, cvs.usu_fimvig, ");
		sql.append(" cvs.usu_nomloc AS cvs_nomloc, cvs.usu_codccu AS cvs_codccu, cvs.usu_numctr AS cvs_numctr, cvs.usu_numpos AS cvs_numpos, pos.usu_peresc, mot.codmot,  mot.desmot ");
		sql.append(" FROM R016ORN orn WITH (NOLOCK)  ");
		sql.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
		sql.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc)  ");
		sql.append(" INNER JOIN USU_T200REG REG ON REG.USU_CodReg = cvs.usu_codreg ");
		sql.append(" INNER JOIN r030emp EMP ON EMP.numemp = cvs.usu_codemp ");
		sql.append(" INNER JOIN Sapiens.dbo.usu_t160ctr C ON C.usu_numctr = cvs.usu_numctr AND C.usu_codemp = cvs.usu_codemp AND c.usu_codfil = cvs.usu_codfil ");
		sql.append(" LEFT JOIN  Sapiens.dbo.USU_T160CVS pos ON pos.usu_codemp = cvs.usu_codemp AND pos.usu_codfil = cvs.usu_codfil AND pos.usu_numctr = cvs.usu_numctr AND pos.usu_numpos = cvs.usu_numpos  ");
		sql.append(" LEFT JOIN R074CID cid WITH (NOLOCK) ON cid.CodCid = orn.usu_codcid  ");
		sql.append(" LEFT JOIN Sapiens.dbo.E021MOT mot ON mot.codmot = pos.usu_codmot ");
		sql.append(" LEFT JOIN Sapiens.dbo.E085CLI cli ON cli.codcli = c.usu_codcli  ");
		sql.append(" WHERE (cvs.usu_taborg = 203)  ");
		sql.append(" AND (LEN(hie.CodLoc) = 27) ");
		sql.append(" AND (cvs.USU_SITCVS = 'S' OR DATEADD(MINUTE, 1439, cvs.usu_datfim) >= ? OR cvs.usu_datfim = '1900-12-31' OR EXISTS (SELECT * FROM R034FUN fun WHERE fun.TIPCOL = 1 AND cvs.usu_numloc = fun.numloc AND cvs.usu_taborg = fun.taborg AND (fun.sitafa <> 7 OR (fun.sitafa = 7 AND fun.datafa > ?)))) ");
		sql.append(" AND cvs.usu_codreg = 9 ");
		sql.append(" AND (hie.codloc LIKE '1.5%' OR hie.codloc LIKE '1.6%') and cvs.usu_sitcvs <> 'N' and usu_apecli like '%UDESC%'");
		sql.append(" AND cvs.usu_codemp IN (2, 6, 7, 8) ");
		sql.append(" AND cli.tipemc = 2 and not exists( select 1 from r034fun fun where fun.sitafa <> 7 and fun.numloc = orn.numloc and fun.taborg = orn.taborg) and cvs.usu_qtdcon > 0");
		sql.append(" ORDER BY orn.usu_cliorn, orn.usu_ordexi, hie.codloc ");

		GregorianCalendar datRef = new GregorianCalendar();
		String datCptCop = datCpt;
		datCpt = "01/" + datCpt;
		try
		{
			datRef = OrsegupsUtils.stringToGregorianCalendar(datCpt, "dd/MM/yyyy");
		}
		catch (ParseException e1)
		{
			e1.printStackTrace();
		}

		datRef.set(GregorianCalendar.MONTH, 1);
		datRef.set(GregorianCalendar.DATE, 1);
		datRef.add(GregorianCalendar.DATE, -1);
		datRef.set(GregorianCalendar.HOUR_OF_DAY, 23);
		datRef.set(GregorianCalendar.MINUTE, 59);
		datRef.set(GregorianCalendar.SECOND, 59);

		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsContrato = null;
		PreparedStatement stContrato = null;
		try
		{
			stContrato = connVetorh.prepareStatement(sql.toString());
			stContrato.setTimestamp(1, new Timestamp(datRef.getTimeInMillis()));
			stContrato.setTimestamp(2, new Timestamp(datRef.getTimeInMillis()));
			stContrato.setTimestamp(3, new Timestamp(datRef.getTimeInMillis()));

			rsContrato = stContrato.executeQuery();

			while (rsContrato.next())
			{
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				long numCtr = rsContrato.getLong("usu_numctr");
				QLFilter qlNumCtr = new QLEqualsFilter("contrato", numCtr);

				QLFilter qlDatCpt = new QLEqualsFilter("periodo", datCptCop);

				groupFilter.addFilter(qlNumCtr);
				groupFilter.addFilter(qlDatCpt);

				List<NeoObject> objsContratos = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter);
				EntityWrapper wContrato = null;

				if (objsContratos != null && objsContratos.size() > 0)
				{
					NeoObject obj = objsContratos.get(0);
					wContrato = new EntityWrapper(obj);
					List<NeoObject> postos = new ArrayList<NeoObject>();
					postos = (List) wContrato.findValue("listaPostos");

					//new Posto
					InstantiableEntityInfo insPos = AdapterUtils.getInstantiableEntityInfo("MEDPosto");
					NeoObject objPos = insPos.createNewInstance();
					EntityWrapper wPosto = new EntityWrapper(objPos);
					wPosto.setValue("nomLoc", rsContrato.getString("usu_lotorn") + " - " + rsContrato.getString("usu_nomLoc"));
					wPosto.setValue("nVagas", rsContrato.getLong("usu_qtdcon"));
					wPosto.setValue("nColaboradores", 0L);
					wPosto.setValue("numLoc", rsContrato.getLong("usu_numloc"));
					wPosto.setValue("numPos", rsContrato.getLong("usu_numpos"));
					wPosto.setValue("alertasPosto", "");
					wPosto.setValue("codLoc", rsContrato.getString("codloc"));
					wPosto.setValue("codCcu", rsContrato.getString("usu_codccu"));
					
					PersistEngine.persist(objPos);
					postos.add(objPos);
					wContrato.setValue("listaPostos", postos);
					PersistEngine.persist(obj);
					PersistEngine.commit(true);
				}
				else
				{

					InstantiableEntityInfo insMed = AdapterUtils.getInstantiableEntityInfo("MEDMedicaoDeContratos");
					NeoObject objMed = insMed.createNewInstance();
					wContrato = new EntityWrapper(objMed);
					wContrato.setValue("cliente", rsContrato.getString("usu_apecli"));
					wContrato.setValue("contrato", rsContrato.getLong("usu_numctr"));
					wContrato.setValue("codEmpresaContratada", rsContrato.getLong("usu_codemp"));
					wContrato.setValue("empresaContratada", rsContrato.getString("nomemp"));
					wContrato.setValue("responsavel", "Sra. Inês");
					wContrato.setValue("periodo", datCptCop);
					wContrato.setValue("codRegional", rsContrato.getLong("usu_codreg"));
					wContrato.setValue("regional", rsContrato.getString("USU_NomReg"));
					wContrato.setValue("numOfi", rsContrato.getString("usu_numofi"));

					//new Posto
					InstantiableEntityInfo insPos = AdapterUtils.getInstantiableEntityInfo("MEDPosto");
					NeoObject objPos = insPos.createNewInstance();
					EntityWrapper wPosto = new EntityWrapper(objPos);
					wPosto.setValue("nomLoc", rsContrato.getString("usu_lotorn") + " - " + rsContrato.getString("usu_nomLoc"));
					wPosto.setValue("nVagas", rsContrato.getLong("usu_qtdcon"));
					wPosto.setValue("nColaboradores", 0L);
					wPosto.setValue("numLoc", rsContrato.getLong("usu_numloc"));
					wPosto.setValue("numPos", rsContrato.getLong("usu_numpos"));
					wPosto.setValue("alertasPosto", "");
					wPosto.setValue("codLoc", rsContrato.getString("codloc"));
					wPosto.setValue("codCcu", rsContrato.getString("usu_codccu"));
					List<NeoObject> listaPostos = new ArrayList<NeoObject>();
					PersistEngine.persist(objPos);
					listaPostos.add(objPos);
					wContrato.setValue("listaPostos", listaPostos);
					PersistEngine.persist(objMed);
					PersistEngine.commit(true);
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stContrato, rsContrato);
		}

	}
}
