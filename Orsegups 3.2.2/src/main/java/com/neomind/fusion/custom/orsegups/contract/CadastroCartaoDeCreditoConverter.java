package com.neomind.fusion.custom.orsegups.contract;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;

public class CadastroCartaoDeCreditoConverter extends StringConverter {

    @Override
    protected String getHTMLInput(EFormField field, OriginEnum origin) {

	StringBuilder textoTable = new StringBuilder();
	Long codigoCliente = null;
	EntityWrapper processEntityWrapper = new EntityWrapper(field.getForm().getCaller().getObject());
	codigoCliente = processEntityWrapper.findGenericValue("buscaNomeCliente.codcli");
	
	
	
	textoTable.append("<script>                                                                                  ");
	textoTable.append("function callRegisterForm() {     "
			+ "var parametro = $('#hid_root').val();                                                         ");
	textoTable.append("var url = '" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/contratos/CreditCardRegisterForm.jsp?codigoCliente="+codigoCliente+"';");
	textoTable.append("var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,450,560,'');             ");
	textoTable.append("var win = window.name;                                                                   ");
	textoTable.append("NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win);              ");
	textoTable.append("}                                                                                         ");
	textoTable.append("</script> ");
	textoTable.append("<p><img class=\"tableIcon\" onClick=\"callRegisterForm()\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></p>");
	
	
	return textoTable.toString();
    }

    @Override
    protected String getHTMLView(EFormField field, OriginEnum origin) {
	return getHTMLInput(field, origin);
    }
}
