package com.neomind.fusion.custom.orsegups.adapter;

import java.util.HashMap;
import java.util.Map;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoUtils;

public class RSCEnviaEmail implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		String origem = processEntity.findField("origem").getValueAsString();
		String code = origin.getCode();
		String categoriasRsc = processEntity.findField("categoriasRsc.descricao").getValueAsString();
		String descricao = processEntity.findField("descricao").getValueAsString();
		String email = processEntity.findField("email").getValueAsString();		
		String contato = processEntity.findField("contato").getValueAsString();
		String telefone = processEntity.findField("telefones").getValueAsString();
		String emailExecutivo = processEntity.findField("executivoResponsavel.email").getValueAsString();
		
		String cliente = "";
		if(NeoUtils.safeIsNotNull(processEntity.findField("clienteSapiens.nomcli"))) {
			cliente = processEntity.findField("clienteSapiens.nomcli").getValueAsString();
		} else {
			cliente = processEntity.findField("lotacao").getValueAsString();
		}

		if(origin.getActivityName().equalsIgnoreCase("Registrar Solicitação de Cliente")){
			if((!origem.equalsIgnoreCase("Site Orsegups")) && (NeoUtils.safeIsNotNull(email))) {
				Map<String, Object> paramMap = new HashMap<String, Object>();
				
				paramMap.put("tarefa", code);
				paramMap.put("nome", contato);
				paramMap.put("tipo", categoriasRsc);
				paramMap.put("mensagem", descricao);
				
				FusionRuntime.getInstance().getMailEngine().sendEMail(email, "/portal_orsegups/emailAberturaRSC.jsp", paramMap);
			}
		}

		if(categoriasRsc.equalsIgnoreCase("Orçamento")){
			Map<String, Object> paramMap = new HashMap<String, Object>();
			StringBuilder noUserMsg = new StringBuilder();
			
			noUserMsg.append("<strong>Cliente:</strong> " + cliente + "<br>");
			noUserMsg.append("<strong>Contato:</strong> " + contato + "<br>");
			noUserMsg.append("<strong>Telefone:</strong> " + telefone + "<br>");
			noUserMsg.append("<strong>Email:</strong> " + email + "<br>");
			noUserMsg.append("<strong>Mensagem:</strong> " + descricao + "<br>");
			
			paramMap.put("tarefa", code);
			paramMap.put("tipo", categoriasRsc);
			paramMap.put("mensagem", noUserMsg.toString());
			
			FusionRuntime.getInstance().getMailEngine().sendEMail(emailExecutivo, "/portal_orsegups/emailExecutivoRSC.jsp", paramMap);
		}
	}
}