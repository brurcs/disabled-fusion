package com.neomind.fusion.custom.orsegups.maps.call.engine;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.HttpResponse;

import com.neomind.fusion.custom.orsegups.maps.call.vo.CallAlertAitVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;

@Path(value = "mapsAlertAitTest")
public class CallAlertAitHandler 
{
	@Context HttpResponse response;
   
	@GET
	@Path("alertAit/{numeroInterno}/{numeroExterno}/{codigoViatura}/{statusAtendimento}")
	public String alertAit(@PathParam("numeroInterno") String numeroInterno, @PathParam("numeroExterno") String numeroExterno,  @PathParam("codigoViatura") String codigoViatura,  @PathParam("statusAtendimento") String statusAtendimento)
	{
		
		String retorno = null;
		int minutesSinceLastCallAnt = 0;
		String codVtr = codigoViatura;
		try
		{
		
//		EntityWrapper ewUser = new EntityWrapper(PortalUtil.getCurrentUser());
//        numeroInterno = "0000";
//             
//        if ((String)ewUser.findValue("ramal") != null) {
//           numeroInterno = (String)ewUser.findValue("ramal");
//        }	
			
		String viaturaInfoWindow = "<div><a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 0);\" class=\"myButton\" >Sucesso</a>" +
						"&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 5);\" class=\"myButton\"  >Ocupado</a>" +
						"&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 1);\" class=\"myButton\" >Não atende</a>" +
						"&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 4);\" class=\"myButton\"  >Caixa postal</a>" +
						"&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 6);\" class=\"myButton\"  >Não Existe</a>"+
					    "&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 7);\" class=\"myButton\"  >30</a>"+
					    "&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 8);\" class=\"myButton\"  >Aguardar 30</a>"+					    
					    "</div>";	
		
		
		
		CallAlertAitVO alertVOs = new CallAlertAitVO(); 
		ViaturaVO viaturaVO = new ViaturaVO();
		viaturaVO.setInfoWindowContent(viaturaInfoWindow);
		viaturaVO.setTitulo(statusAtendimento);
		viaturaVO.setCodigoViatura(codigoViatura);
		viaturaVO.setTelefone(numeroExterno);
		System.out.println(codigoViatura);
		Thread.sleep(50);
		
		EventoVO eventoVO = new EventoVO();
		eventoVO = (EventoVO) OrsegupsAlertAitEngine.getEvento(codigoViatura,statusAtendimento);
		 
		if(eventoVO != null && eventoVO.getInfoWindowContent() != null && !eventoVO.getInfoWindowContent().isEmpty()){
			alertVOs.setCallEventoVO(eventoVO);
			alertVOs.setCallViaturaVO(viaturaVO);
			retorno = OrsegupsAlertAitEngine.getInstance().alert(alertVOs, numeroInterno, numeroExterno, statusAtendimento);
		}else if(eventoVO == null && OrsegupsAlertAitEngine.waitingSnepResponseMap.containsKey(numeroExterno)){
			OrsegupsAlertAitEngine.waitingSnepResponseMap.remove(numeroExterno);
		}
		
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.setStatus(Response.Status.CREATED.getStatusCode());
		return retorno;
	}
	
	@GET
	@Path("receiveCall")
	public void receiveCall()
	{
		OrsegupsAlertAitEngine.getInstance().receiveCall("6635", "4197334645", null, 27L, null);
	}
	
	@GET
	@Path("clearCache")
	public void clearCache()
	{
		OrsegupsAlertAitEngine.getInstance().clearCache();
	}
	
	
	@GET
	@Path("getLigacaoPendente/{numeroInterno}")
	public void getLigacaoPendente(@PathParam("numeroInterno") String numeroInterno)
	{
		OrsegupsAlertAitEngine.getInstance().verificarLigacaoPendente(numeroInterno);
	}
	
	@GET
	@Path("alertAitClose/{numeroInterno}/{numeroExterno}/{callStatus}")
	public void alertAitClose(@PathParam("numeroInterno") String numeroInterno, @PathParam("numeroExterno") String numeroExterno, @PathParam("callStatus") String callStatus)
	{		CallAlertAitStatus status = null;
			if (callStatus.equals("0"))
			{
				status = CallAlertAitStatus.ATENDIDA;
			}
			else if (callStatus.equals("1"))
			{
				status = CallAlertAitStatus.NAO_ATENDIDA;
			}
			else if (callStatus.equals("2"))
			{
				status = CallAlertAitStatus.ATENDIDA_DESLIGADA;
			}
			else if (callStatus.equals("3"))
			{
				status = CallAlertAitStatus.PROXIMA_LIGACAO;
			}
			else if (callStatus.equals("4"))
			{
				status = CallAlertAitStatus.CAIXA_POSTAL;
			}
			else if (callStatus.equals("5"))
			{
				status = CallAlertAitStatus.OCUPADO;
			}
			else if (callStatus.equals("6"))
			{
				status = CallAlertAitStatus.NAO_EXISTE;
			}
			else if (callStatus.equals("7"))
			{
				status = CallAlertAitStatus.EM_ESPERA;
			}else if (callStatus.equals("8")) {
				status = CallAlertAitStatus.AGUARDAR_30;
			}
			OrsegupsAlertAitEngine.getInstance().alertAitClose(numeroInterno, numeroExterno, status);
			
	}
	
	@GET
	@Path("getCache")
	public void getCache()
	{
		OrsegupsAlertAitEngine.getInstance().getCache();
	}
	
	public static void alertsAit(String numeroInterno, String numeroExterno,  String codigoViatura,  String statusAtendimento)
	{
		
		String retorno = null;
		try
		{
			EntityWrapper ewUser = new EntityWrapper(PortalUtil.getCurrentUser());
            numeroInterno = "0000";
             
            if ((String)ewUser.findValue("ramal") != null) {
            	numeroInterno = (String)ewUser.findValue("ramal");
            }
			String viaturaInfoWindow = "<div><a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 0);\" class=\"myButton\" >Sucesso</a>" +
							"&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 5);\" class=\"myButton\"  >Ocupado</a>" +
							"&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 1);\" class=\"myButton\" >Não atende</a>" +
							"&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 4);\" class=\"myButton\"  >Caixa postal</a>" +
						    "&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 6);\" class=\"myButton\"  >Não Existe</a>"+
						    "&nbsp;<a href=\"javascript:eventCallHandlerClose("+numeroInterno+", "+numeroExterno+", 8);\" class=\"myButton\"  >Aguardar 30</a>"+						    
						    "</div>";		
			CallAlertAitVO alertVOs = new CallAlertAitVO(); 
			ViaturaVO viaturaVO = new ViaturaVO();
			viaturaVO.setInfoWindowContent(viaturaInfoWindow);
			viaturaVO.setTitulo(statusAtendimento);
		
			Thread.sleep(50);
			
			EventoVO eventoVO = new EventoVO();
			eventoVO = (EventoVO) OrsegupsAlertAitEngine.getEvento(codigoViatura,statusAtendimento);
			
			if(eventoVO != null && eventoVO.getInfoWindowContent() != null && !eventoVO.getInfoWindowContent().isEmpty()){
				alertVOs.setCallEventoVO(eventoVO);
				alertVOs.setCallViaturaVO(viaturaVO);
			retorno = OrsegupsAlertAitEngine.getInstance().alert(alertVOs, numeroInterno, numeroExterno, statusAtendimento);
		}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@GET
	@Path("verificaViaturas/{regional}/{ramalOrigem}/{aplicacao}/{emUso}/{x8IgnoreTags}/{filtrosConta}/{filtrosNotConta}/{stausVtr}/{ativaNaFrotaVtr}")
	public void verificaViaturas(@PathParam("regional") String regional,@PathParam("ramalOrigem") String ramalOrigem,@PathParam("aplicacao") String aplicacao,@PathParam("emUso") String emUso,@PathParam("x8IgnoreTags") String x8IgnoreTags,@PathParam("filtrosConta") String filtrosConta,@PathParam("filtrosNotConta") String filtrosNotConta,@PathParam("stausVtr") String stausVtr,@PathParam("ativaNaFrotaVtr") String ativaNaFrota)
	{
		//&regional="+regional+"&ramalOrigem="+ramalOrigem+"&aplicacao="+aplicacao+"&emUso="+emUso+"&x8IgnoreTags="+x8IgnoreTags+"&filtrosConta="+filtrosConta+"&filtrosNotConta="+filtrosNotConta+"&statusVtr="+statusVtr+"&ativaNaFrota="+ativaNaFrotaVtr
		//filtrosNotConta = "*:AAAA,*:AAA1,*:AAA2,*:AAA3,*:AAA4,*:AAA5,*:AAA6";
		OrsegupsAlertAitEngine.getInstance().getViaturas(regional, x8IgnoreTags, ramalOrigem, aplicacao, emUso, filtrosConta, filtrosNotConta, stausVtr, ativaNaFrota);
		
	}
	
	@GET
	@Path("verificaViaturass/{teste}")
	public void verificaViaturas(@PathParam("teste") String regional)
	{
		
		//OrsegupsAlertAitEngine.getInstance().getViaturas(regional, null, null, null, null, null, null, null, null, null, null, null, null);
	}
	
	@GET
	@Path("removeRamal/{ramal}")
	public void removeRamal(@PathParam("regional") String ramal)
	{
		if(OrsegupsAlertAitEngine.getInstance().successCallMap.containsKey(ramal))
			OrsegupsAlertAitEngine.getInstance().successCallMap.remove(ramal);
	}
}
