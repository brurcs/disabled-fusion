package com.neomind.fusion.custom.orsegups.adapter.q004;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RegistraHistorico implements TaskFinishEventListener, TaskAssignerEventListener
{
	private static final Log log = LogFactory.getLog(RegistraHistorico.class);

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException
	{
		// origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		String erro = "Por favor, contatar o administrador do sistema!";
		try
		{
			Task tarefa = event.getTask();
			EntityWrapper wrapper = new EntityWrapper(event.getProcess().getEntity());
			
			Boolean aprovado = wrapper.findGenericValue("aprovado");
			
			NeoObject registro = AdapterUtils.createNewEntityInstance("TarefaRegistroAtividades");
			EntityWrapper wRegistro = new EntityWrapper(registro);

			wRegistro.setValue("responsavel", tarefa.getUser());
			wRegistro.setValue("dataInicial", tarefa.getStartDate());
			wRegistro.setValue("dataFinal", tarefa.getFinishDate());			
			wRegistro.setValue("descricao", wrapper.findGenericValue("observacoes"));
			
			if (!tarefa.getActivityName().equals("Solicitar tarefa") && aprovado != null)
				wRegistro.setValue("atividade", tarefa.getActivityName() + (": " + (aprovado ? "Aprovada" : "Não aprovada")));
			else
				wRegistro.setValue("atividade", tarefa.getActivityName());

			event.getWrapper().findField("registroAtividades").addValue(registro);
			
			
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}

	}

	@Override
	public void onAssign(TaskAssignerEvent event) throws TaskException
	{
		EntityWrapper wrapper = new EntityWrapper(event.getProcess().getEntity());
		
		wrapper.setValue("aprovado", null);
		wrapper.setValue("observacoes", "");
	}
}