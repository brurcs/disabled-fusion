package com.neomind.fusion.custom.orsegups.ged.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.doc.pdf.PDFManager;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.exception.NeoConvertException;
import com.neomind.fusion.persist.PersistEngine;

@WebServlet(name="ShowAsPdfServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.ged.servlet.ShowAsPdfServlet"})
public class ShowAsPdfServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException 
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException 
	{
		this.doRequest(req, resp);
	}
	
	private void doRequest(HttpServletRequest req, HttpServletResponse resp)
	{
		String neoId = req.getParameter("neoId");
		
		if(neoId != null)
		{
			NeoFile file = PersistEngine.getObject(NeoFile.class, Long.valueOf(neoId));
			
			Map<String, Object> pdfOptions = new HashMap<String, Object>();
			
			pdfOptions.put("EncryptFile", Boolean.FALSE);
			pdfOptions.put("DocumentOpenPassword", "FUSION");
			pdfOptions.put("RestrictPermissions", Boolean.TRUE);
	
			// 0: printing not permitted
			// 1: permit low resolution printing
			// 2: permit high resolution printing
			pdfOptions.put("Printing", new Integer(0));
	
			// 0: not changes permitted
			// 1: inserting, deleting, rotating pages permitted
			// 2: filling in form fields permitted
			// 3: commenting permitted
			// 4: any changes permitted, except extraction of content
			pdfOptions.put("Changes", new Integer(0));

			pdfOptions.put("EnableCopyingOfContent", Boolean.FALSE);
			pdfOptions.put("EnableTextAccessForAccessibilityTools", Boolean.FALSE);

			NeoStorage n = NeoStorage.getDefault();
			pdfOptions.put("path", n.getPath());

			pdfOptions.put("pages", null); // para que nao caia na logica da copia controlada
			pdfOptions.put("footer", null); // para que nao caia na logica da copia controlada
			
			pdfOptions.put("newFileName", "showAsPdf.pdf");
			
			NeoFile pdfFile = null;
			
			//FIXME NEOMIND esse método não existe mais
//			try 
//			{
//				pdfFile = PDFManager.convertToPdfNeo(file, pdfOptions);
//			} 
//			catch (NeoConvertException e)
//			{
//				e.printStackTrace();
//				return;
//			}
			
			/**
			 * @author orsegups danilo.silva - Alterada a chamada visto que foi comentado anteriormente pela neomind como método inexistente.
			 * @date 08/09/2015
			 */
			NeoFile filePdfTemp = null;
			try 
			{
				//System.out.println("Original ID:" + file.getNeoId());
				filePdfTemp = convertToPdfNeo(file, pdfOptions);
				//System.out.println("Final ID:" + filePdfTemp.getNeoId());
			} 
			catch (Exception e)
			{
				e.printStackTrace();
				//return;
			}
			filePdfTemp.setName(file.getName());
			long mask = filePdfTemp.getNeoId();
			mask *= 32;
			mask += 4; // canPrint
			mask ^= 0x4242;
			
			try 
			{
				final PrintWriter out = resp.getWriter();
				out.print(mask);
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @author orsegups danilo.silva - Adaptado método da versão antiga da biblioteca neoConvert para funcionar no nosso caso .
	 * @date 09/09/2015
	 */
	public static NeoFile convertToPdfNeo(NeoFile file, Map pdfOptions) throws Exception
	{
		File pdfFile = PDFManager.convertDocumentToPdf(file.getAsFile(), file.getName() , pdfOptions, file.getNeoId(), FusionRuntime.getInstance().getTempDir().getAbsolutePath());
		if (pdfFile == null)
			return null;
		return (NeoFile) NeoStorage.getDefault().copy(pdfFile);
	}
	
	
}
