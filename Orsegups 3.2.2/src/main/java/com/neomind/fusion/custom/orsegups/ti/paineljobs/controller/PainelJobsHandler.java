package com.neomind.fusion.custom.orsegups.ti.paineljobs.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ti.paineljobs.bean.PainelJobBean;
import com.neomind.fusion.custom.orsegups.ti.paineljobs.bean.PainelJobErrosBean;
import com.neomind.fusion.custom.orsegups.ti.paineljobs.engine.PainelJobsEngine;

@Path(value = "jobs")
public class PainelJobsHandler
{
	private static final Log log = LogFactory.getLog(PainelJobsHandler.class);
	
	private PainelJobsEngine painelJobsEngine = new PainelJobsEngine();

	@GET
	@Path("buscarJobs")
	@Produces("application/json")
	public List<PainelJobBean> buscarJobs() {
	        	   
	    List<PainelJobBean> jobsFusion  = painelJobsEngine.buscarJobsFusion();
	    List<PainelJobBean> jobsVetorh  = painelJobsEngine.buscarJobsVetorh();
	    List<PainelJobBean> jobsSapiens = painelJobsEngine.buscarJobsSapiens();
	    
	    List<PainelJobBean> jobs = new ArrayList<PainelJobBean>();
	    jobs.addAll(jobsFusion );
	    jobs.addAll(jobsVetorh );
	    jobs.addAll(jobsSapiens);
	    return jobs;

	}
	
	@GET
	@Path("buscarTarefasJob/{idJob}/{sistema}")
	@Produces("application/json")
	public List<String> buscarTarefasJobs(@PathParam("idJob") Long idJob, @PathParam("sistema") Long sistema) {
	        	   
	    List<String> jobs = painelJobsEngine.buscarTarefasJob(idJob, sistema);
	        
	    return jobs;

	}
	
	@GET
	@Path("buscarJobsComErro/")
	@Produces("application/json")
	public List<PainelJobErrosBean> buscarJobsComErro() {
	        	   
	    List<PainelJobErrosBean> jobs = painelJobsEngine.buscarErrosAtuais();
	        
	    return jobs;

	}
	
	@GET
	@Path("atualizarErros/")
	public void AtualizarErros() {
	        	   
	    painelJobsEngine.atualizarErros();

	}
	
	@GET
	@Path("buscarTarefasAgendador/")
	@Produces("application/json")
	public List<String> buscarTarefasAgendador() {
	        	   
	    List<String> jobs = painelJobsEngine.buscarTarefasAgendador();
	        
	    return jobs;

	}
	
	@GET
	@Path("gravarVerificacaoErro/{idJob}/{sistema}")
	@Produces("application/json")
	public void gravarVerificacaoErro(@PathParam("idJob") Long idJob, @PathParam("sistema") Long sistema) {
	        	   
	    painelJobsEngine.gravarVerificacaoErro(idJob, sistema);
	        
	}
	
	@GET
	@Path("buscarDadosDiarioGraficoJobs/{tipos}")
	@Produces("application/json")
	public List<HashMap<String, Object>> buscarDadosDiarioGraficoJobs(@PathParam("tipos") String tipos) {
	        	   
	    List<HashMap<String, Object>> retorno = painelJobsEngine.buscarDadosDiarioGraficoJobs(tipos);
	        
	    return retorno;

	}
	
	@GET
	@Path("buscarDadosMensalGraficoJobs/")
	@Produces("application/json")
	public List<HashMap<String, Object>> buscarDadosMensalGraficoJobs() {
	        	   
	    List<HashMap<String, Object>> retorno = painelJobsEngine.buscarDadosMensalGraficoJobs();
	        
	    return retorno;

	}
	
	@GET
	@Path("buscarDadosHorarioGraficoJobs/")
	@Produces("application/json")
	public List<HashMap<String, Object>> buscarDadosHorarioGraficoJobs() {
	        	   
	    List<HashMap<String, Object>> retorno = painelJobsEngine.buscarDadosHorarioGraficoJobs();
	        
	    return retorno;

	}
	
}