package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;



public class RotinaUsuarioSapiensOcioso implements CustomJobAdapter {

	@Override
	public void execute(CustomJobContext arg0) {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		StringBuilder corpoEmail = new StringBuilder();
		try {
			conn = PersistEngine.getConnection("SAPIENS");

			sql.append(" SELECT r910usu.CodEnt, r910usu.NomCom, R910ENT.NomExb, r910usu.DesUsu, ");
			sql.append("         DATEADD(minute , r910usu.HorLog, r910usu.DatLog) DataEntrada, ");
			sql.append(" RIGHT('00' + CONVERT(VARCHAR, ABS(DATEDIFF(SECOND, DATEADD(minute , r910usu.HorLog, r910usu.DatLog), GETDATE()) / 60 / 60 / 24)),2) ");
			sql.append("                    + ' dias ' + RIGHT('00' + CONVERT(VARCHAR, ABS(((DATEDIFF(SECOND, DATEADD(minute , r910usu.HorLog, r910usu.DatLog), GETDATE()) / 60) / 60) % 24)), 2) ");
			sql.append("                    + ':' + RIGHT('00' + CONVERT(VARCHAR, ABS((DATEDIFF(SECOND, DATEADD(minute , r910usu.HorLog, r910usu.DatLog), GETDATE()) / 60) % 60)), 2) + 'h' TempoConectado, ");              
			sql.append("          (SELECT COUNT(*) ");
			sql.append("    FROM R911sec  ");
			sql.append("            WHERE R911sec.AppNam = 'SAPIENS' ");
			sql.append("              AND R911sec.AppUsr = R910ENT.NomExb) AS QTDCONEXAO ");
			sql.append("    FROM r910usu, R910ENT "); 
			sql.append("   WHERE R910USU.CodEnt = R910ENT.CodEnt ");
			sql.append(" GROUP BY r910usu.CodEnt, r910usu.NomCom, r910usu.DesUsu, ");
			sql.append("         r910usu.DatLog, r910usu.HorLog, R910ENT.NomExb ");
			sql.append("  HAVING (SELECT COUNT(*) ");
			sql.append("            FROM R911sec ");
			sql.append("           WHERE R911sec.AppNam = 'SAPIENS' ");
			sql.append("             AND R911sec.AppUsr = R910ENT.NomExb) > 0  ");
			sql.append(" UNION ");
			sql.append("  SELECT '', '', '', '', 0,'Total', COUNT(*) ");
			sql.append("    FROM r910usu, R910ENT, R911sec ");
			sql.append("   WHERE R910USU.CodEnt = R910ENT.CodEnt ");
			sql.append("     AND R911sec.AppNam = 'SAPIENS' ");
			sql.append("     AND R911sec.AppUsr = R910ENT.NomExb ");
			sql.append(" ORDER BY 6 desc ,4 ");


			pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			rs = pstm.executeQuery();

			if (rs.next()) {			
				int qtdConexoes = rs.getInt("QTDCONEXAO");
				if(qtdConexoes >= 1) {			
					try	{
						corpoEmail.append("<table style=\"border: 1px solid black \">");					
						corpoEmail.append("<thead style=\"text-align: center\">");
						corpoEmail.append("<tr>");
						corpoEmail.append("<th style=\"border-bottom: 1px solid black;border-right: 1px solid black\"> NOME </th>");
						corpoEmail.append("<th style=\"border-bottom: 1px solid black;border-right: 1px solid black\"> DEPARTAMENTO </th>");
						corpoEmail.append("<th style=\"border-bottom: 1px solid black;border-right: 1px solid black\"> DATA DE ENTRADA </th>");
						corpoEmail.append("<th style=\"border-bottom: 1px solid black;border-right: 1px solid black\"> TEMPO CONECTADO </th>");
						corpoEmail.append("<th style=\"border-bottom: 1px solid black;text-align: center\"> QTD. CONEXÕES ATIVAS </th>");
						corpoEmail.append("</tr>");	
						corpoEmail.append("</thead>");
						corpoEmail.append("<tbody style=\"text-align: left\">");

						while(rs.next()) {
							corpoEmail.append("<tr>");
							corpoEmail.append("<td style=\"border-bottom: 1px solid black;border-right: 1px solid black\">" + rs.getString("NomCom") + "</td>");
							corpoEmail.append("<td style=\"border-bottom: 1px solid black;border-right: 1px solid black\">" + rs.getString("DesUsu") + "</td>");
							corpoEmail.append("<td style=\"border-bottom: 1px solid black;border-right: 1px solid black\">" + rs.getString("DataEntrada") + "</td>");

							String tempoConectado = rs.getString("TempoConectado");
							System.out.println(tempoConectado);

							if(rs.getString("TempoConectado").contains("00 dias")) {
								tempoConectado = rs.getString("TempoConectado").substring(8, 14);
							}

							corpoEmail.append("<td style=\"border-bottom: 1px solid black;border-right: 1px solid black; text-align: center\">" + tempoConectado + "</td>");


							corpoEmail.append("<td style=\"border-bottom: 1px solid black;text-align: center\">" + rs.getString("QTDCONEXAO") + "</td>");
							corpoEmail.append("</tr>");	
						}

						corpoEmail.append("</tbody>");
						corpoEmail.append("</table>");

						enviarEmail(corpoEmail, qtdConexoes);
					} catch (Exception e){
						e.printStackTrace();
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}

	public void enviarEmail(StringBuilder corpoEmail, int qtdConexoes) {
		MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
		MultiPartEmail email = new HtmlEmail();
		StringBuilder eMail = new StringBuilder();

		eMail.append("Prezados(as) Gestores, \n");
		if(qtdConexoes == 152) {
			eMail.append("Alertamos que neste momento todas as licenças do sistema Sapiens estão sendo utilizadas. \n");
		} else {
			eMail.append("Alertamos que estamos próximos de atingir o número máximo de licenças do sistema Sapiens. \n");		
		}
		eMail.append("Solicitamos racionalidade no uso dos recursos. \n");
		eMail.append("Abaixo segue a listagem de usuários conectados: \n");
		eMail.append("\n" + corpoEmail);
		try	{
			email.setSubject("USUÁRIOS SAPIENS - LIMITE EXCEDIDO");
			email.addTo("gerentes.coordenadores.regionais@orsegups.com.br");
			email.addTo("gerentes.coordenadores.sede@orsegups.com.br");
			email.addTo("diretores@orsegups.com.br");
			email.addBcc("suportesistemas@orsegups.com.br");
			email.setFrom(settings.getFromEMail(), settings.getFromName());
			email.setSmtpPort(settings.getPort());
			email.setHostName(settings.getSmtpServer());
			email.setMsg(eMail.toString());
			email.send();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}


