package com.neomind.fusion.custom.orsegups.scheduler;

import com.neomind.fusion.custom.orsegups.utils.ContratoVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

public class AbreTarefaEncerramentoContrato implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {
        try {
            LinkedList<ContratoVO> listaContratos = retornaContratosEncerrados();

            if (NeoUtils.safeIsNotNull(listaContratos)) {
                abreTarefaContratos(listaContratos);
            }

        } catch (Exception e) {
            System.out.println(String.format("#### ERRO %s - %s", NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"), this.getClass().getSimpleName()));
            e.printStackTrace();
        }
    }


    private void abreTarefaContratos(LinkedList<ContratoVO> listaContratos) {
        IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

        GregorianCalendar prazo = new GregorianCalendar();

        prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);

        try {
            NeoPaper solicitantePapel = OrsegupsUtils.getPaper("SolicitanteTarefaEncerramentoContrato");
            NeoUser solicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(solicitantePapel);
            NeoPaper executorPadraoPapel = OrsegupsUtils.getPaper("ExecutorPadraoTarefaEncerramentoContrato");
            NeoUser executorPadrao = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(executorPadraoPapel);

            for (ContratoVO contratoVO : listaContratos) {
                try {
                    NeoUser executor = executorPadrao;
                    if (contratoVO.getUsuarioFusionRepresentante() != null &&
                            contratoVO.getUsuarioFusionRepresentante().length() > 0) {
                        QLFilter filtroExecutor = new QLEqualsFilter("code", contratoVO.getUsuarioFusionRepresentante());
                        executor = PersistEngine.getNeoObject(NeoUser.class, filtroExecutor);
                    }

                    String titulo = "Contrato Encerrado (Desistência) - " + contratoVO.getNomeCliente();
                    String descricao = retornaDescricao(contratoVO, executor != null);

                    iniciarTarefaSimples.abrirTarefa(solicitante.getCode(), executor.getCode(), titulo, descricao, "1", "sim", prazo);
                } catch (Exception e) {
                    System.out.println(String.format("#### ERRO %s - %s", NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"), this.getClass().getSimpleName()));
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            System.out.println(String.format("#### ERRO %s - %s", NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"), this.getClass().getSimpleName()));
            e.printStackTrace();
        }
    }


    private String retornaDescricao(ContratoVO contratoVO, boolean executorEncontrado) {
        GregorianCalendar gcInicio = new GregorianCalendar();
        gcInicio.setTime(contratoVO.getDataInicioVigencia());

        GregorianCalendar gcFim = new GregorianCalendar();
        gcFim.setTime(contratoVO.getDataFimVigencia());

        String inicioDaVigencia = NeoDateUtils.safeDateFormat(gcInicio, "dd/MM/yyyy");
        String fimDaVigencia = NeoDateUtils.safeDateFormat(gcFim, "dd/MM/yyyy");

        NumberFormat nf = NumberFormat.getCurrencyInstance();
        String valorContrato = nf.format(contratoVO.getValorPosto().toString());

        Map<String, String> params = new LinkedHashMap<>();
        params.put("EMPRESA/FILIAL", contratoVO.getEmpresa() + " - " + contratoVO.getFilial());
        params.put("NOME DO CLIENTE", contratoVO.getNomeCliente());
        params.put("NÚMERO DO CONTRATO", NeoUtils.safeOutputString(contratoVO.getNumeroContrato()));
        params.put("VALOR DO CONTRATO", valorContrato);
        params.put("INÍCIO DA VIGÊNCIA", inicioDaVigencia);
        params.put("FIM DA VIGÊNCIA", fimDaVigencia);
        params.put("NOME EXECUTIVO", contratoVO.getNomeRepresentante());
        params.put("SERVIÇO", contratoVO.getServico());
        params.put("OBSERVAÇÃO SERVIÇO", contratoVO.getObservacaoServico());
        params.put("MOTIVO", contratoVO.getMotivo());
        params.put("OBSERVAÇÃO MOTIVO", contratoVO.getObservacaoMotivo());

        StringBuilder corpoTarefa = new StringBuilder();

        corpoTarefa.append(" <html>");
        corpoTarefa.append("    <head>");
        corpoTarefa.append("        <meta charset='utf-8'>");
        corpoTarefa.append("    </head>");
        corpoTarefa.append("      <body>");

        if (!executorEncontrado) {
            corpoTarefa.append("        <p>O cadastro do representante no sapiens não possui o usuário do fusion</p>");
        }

        corpoTarefa.append("        <p>Favor justificar encerramento do contrato conforme descritivo abaixo</p>");

        for (Map.Entry<String, String> entry : params.entrySet()) {
            corpoTarefa.append(String.format("        <span><b>%s:</b> %s</span><br />", entry.getKey(), entry.getValue()));
        }

        corpoTarefa.append("      </body>");
        corpoTarefa.append(" </html>");

        return corpoTarefa.toString();
    }


    public LinkedList<ContratoVO> retornaContratosEncerrados() {

        LinkedList<ContratoVO> listaContratos = new LinkedList<>();

        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();

        try {

            sql.append(" SELECT CTR.usu_codemp EMPRESA, ");
            sql.append("         CTR.usu_codfil FILIAL, ");
            sql.append("         CTR.usu_numctr CONTRATO, ");
            sql.append("         CVS.usu_numpos POSTO, ");
            sql.append("         CTR.usu_codcli CLIENTE, ");
            sql.append("         CLI.NomCli     NOMECLIENTE, ");
            sql.append("         CLI.CgcCpf     CGCCPF, ");
            sql.append("         CTR.usu_inivig INICIOVIGENCIA, ");
            sql.append("         CTR.usu_fimvig FINALVIGENCIA, ");
            sql.append("         CVS.usu_preuni VALORPOSTO, ");
            sql.append("         REP.nomrep     NOMEREPRESENTANTE, ");
            sql.append("         REP.usu_usufus USUARIOFUSIONREPRESENTANTE, ");
            sql.append("         SER.DesSer SERVICO, ");
            sql.append("         CTR.usu_obscom OBSERVACAOSERVICO, ");
            sql.append("         CVS.usu_codmot MOTIVO, ");
            sql.append("         CVS.usu_obsmot OBSERVACAOMOTIVO, ");
            sql.append("         CTR.USU_DATMOT DATAENCERRAMENTO ");
            sql.append("  FROM usu_t160ctr CTR, ");
            sql.append("       usu_t160cvs CVS, ");
            sql.append("       E085CLI CLI, ");
            sql.append("       E090REP REP, ");
            sql.append("       E080SER SER ");
            sql.append("  WHERE CTR.usu_codemp = CVS.usu_codemp ");
            sql.append("    AND CTR.usu_codfil = CVS.usu_codfil ");
            sql.append("    AND CTR.usu_numctr = CVS.usu_numctr ");
            sql.append("    AND CTR.usu_codcli = CLI.CodCli ");
            sql.append("    AND CTR.usu_codrep = REP.codrep ");
            sql.append("    AND SER.CodEmp = CVS.usu_codemp ");
            sql.append("    AND SER.CodSer = CVS.usu_codser ");
            sql.append("    AND cast(CTR.usu_datmot as date) = cast(getdate() -1 as date) ");
            sql.append("    AND CTR.usu_codmot = 160 ");

            conn = PersistEngine.getConnection("SAPIENS");

            pstm = conn.prepareStatement(sql.toString());

            rs = pstm.executeQuery();

            while (rs.next()) {

                ContratoVO contratoVO = new ContratoVO();

                contratoVO.setEmpresa(rs.getLong("EMPRESA"));
                contratoVO.setFilial(rs.getLong("FILIAL"));
                contratoVO.setNomeCliente(rs.getString("NOMECLIENTE"));
                contratoVO.setNumeroContrato(rs.getLong("CONTRATO"));
                contratoVO.setDataInicioVigencia(rs.getTimestamp("INICIOVIGENCIA"));
                contratoVO.setDataFimVigencia(rs.getTimestamp("FINALVIGENCIA"));
                contratoVO.setValorPosto(rs.getBigDecimal("VALORPOSTO"));
                contratoVO.setNomeRepresentante(rs.getString("NOMEREPRESENTANTE"));
                contratoVO.setUsuarioFusionRepresentante(rs.getString("USUARIOFUSIONREPRESENTANTE"));
                contratoVO.setServico(rs.getString("SERVICO"));
                contratoVO.setObservacaoServico(rs.getString("OBSERVACAOSERVICO"));
                contratoVO.setMotivo(rs.getString("MOTIVO"));
                contratoVO.setObservacaoMotivo(rs.getString("OBSERVACAOMOTIVO"));
                contratoVO.setDataEncerramento(rs.getTimestamp("DATAENCERRAMENTO"));

                listaContratos.add(contratoVO);
            }

        } catch (SQLException e) {
            System.out.println(String.format("#### ERRO %s - %s", NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"), this.getClass().getSimpleName()));
            e.printStackTrace();
        } finally {
            OrsegupsUtils.closeConnection(conn, pstm, rs);
        }

        return listaContratos;
    }
}
