package com.neomind.fusion.custom.orsegups.contract;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class ContratoPrazoValidacao implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		try
		{

			GregorianCalendar dataPrazo = new GregorianCalendar();
//
//			//dataPrazo.add(GregorianCalendar.DATE,1); //+24 horas - Solicitação via tarefa 880307 - Alterado de 3 dias para 24 horas.
//
//			if (!OrsegupsUtils.isWorkDay(dataPrazo))
//			{
//				dataPrazo = OrsegupsUtils.getNextWorkDay(dataPrazo);
//			}
//
//
//			GregorianCalendar dataNova = OrsegupsUtils.getNextWorkDay(dataPrazo);
			
			GregorianCalendar dataNova = OrsegupsUtils.getSpecificWorkDay24Hr(dataPrazo, 1L);
			
			wrapper.setValue("prazoValidacao", dataNova);

			System.out.println("###C001 - Tarefa " + activity.getCode() + " - Atualizando prazo - prazoValidacao = " + NeoDateUtils.safeDateFormat(dataNova, "dd/MM/yyyy")); 
			ContratoLogUtils.logInfo("Tarefa " + origin.getCode() + " Prazo Validação " + NeoUtils.safeDateFormat(OrsegupsUtils.getNextWorkDay(dataPrazo)));
			
			NeoObject noEndereco = wrapper.findGenericValue("novoCliente.enderecoCliente");
			List<NeoObject> listPostos =  wrapper.findGenericValue("postosContrato");
			for (NeoObject posto : listPostos)
			{
				EntityWrapper wPosto = new EntityWrapper(posto);
				
				if (wPosto.findGenericValue("enderecoPosto") == null)
					wPosto.setValue("enderecoPosto", noEndereco);
			}

		}
		catch (Exception e)
		{
			ContratoLogUtils.logInfo("Tarefa " + origin.getCode() + ", Erro ao setar o prazo ");
			e.printStackTrace();
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
