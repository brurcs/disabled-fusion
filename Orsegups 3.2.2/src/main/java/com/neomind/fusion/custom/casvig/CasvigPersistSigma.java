package com.neomind.fusion.custom.casvig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.util.CustomFlags;

/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public class CasvigPersistSigma
{
	private static final Log log = LogFactory.getLog(CasvigPersistSigma.class);

	private static final String[] arraySigmaCentral = new String[] { "ID_CENTRAL", "CONTRATO", "CGCCPF", "RAZAO", "FANTASIA", "ENDERECO",
			"ID_ESTADO", "CIDADE", "BAIRRO", "CEP", "FONE1", "FONE2", "REFERENCIA", "OBSERVACAO", "RESPONSAVEL" };
	private static final String[] arraySigmaInstalador = new String[] { "ID_INSTALADOR", "NOME" };
	private static final String[] arraySigmaOperador = new String[] { "ID_OPERADOR", "NOME" };

	private static final String[] arrayFusionCentral = new String[] { "id_central", "contrato", "CGCCPF", "razao", "fantasia", "endereco",
			"id_estado", "nome_cidade", "nome_bairro", "CEP", "fone1", "fone2", "referencia", "observacao", "responsavel" };
	private static final String[] arrayFusionInstalador = new String[] { "id_instalador", "nome" };
	private static final String[] arrayFusionOperador = new String[] { "id_operador", "nome" };

	public boolean connectSigma()
	{
		boolean error = false;
		if (CustomFlags.CASVIG)
		{
			Connection con = null;
			Statement stm = null;

			try
			{
				// Este é um dos meios para registrar um driver
				Class.forName("net.sourceforge.jtds.jdbc.Driver");

				// Registrado o driver, vamos estabelecer uma conexão

				con = (Connection) DriverManager.getConnection("jdbc:jtds:sqlserver://192.168.20.41/SIGMA", "neomind", "cas19149mind");

				// Após estabelecermos a conexão com o banco de dados
				// Utilizamos o método createStatement de con para criar o
				// Statement
				stm = (Statement) con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

				selectSigmaCentral(con, stm);
				selectSigmaInstalador(con, stm);
				selectSigmaOperador(con, stm);

			}
			catch (SQLException se)
			{
				// se houve algum erro, uma exceção é gerada para informar o
				// erro
				se.printStackTrace(); // vejamos que erro foi gerado e quem o
				// gerou
				error = true;
			}
			catch (ClassNotFoundException cnfe)
			{
				// se houve algum erro, uma exceção é gerada para informar o
				// erro
				cnfe.printStackTrace(); // vejamos que erro foi gerado e quem o
				// gerou
				error = true;
			}
			finally
			{
				try
				{
					if (stm != null)
						stm.close();
					if (con != null)
						con.close();
				}
				catch (SQLException onConClose)
				{
					onConClose.printStackTrace();
					error = true;
				}
			} // fim do bloco try-catch-finally
		}
		return error;
	}

	private void selectSigmaCentral(Connection con, Statement stm) throws SQLException, ClassNotFoundException
	{
		ResultSet rs = null;
		try
		{
			// Vamos executar o seguinte comando SQL:
			String SQLCliente = " SELECT ce.ID_CENTRAL, ce.CONTRATO, ce.CGCCPF, ce.RAZAO, "
					+ "ce.FANTASIA, ce.ENDERECO, ce.ID_ESTADO, ci.NOME CIDADE, ba.NOME BAIRRO, "
					+ "CEP, FONE1, FONE2, REFERENCIA, OBSERVACAO, RESPONSAVEL "
					+ "FROM SIGMA.dbo.dbCENTRAL ce, SIGMA.dbo.dbCIDADE ci, SIGMA.dbo.dbBAIRRO ba "
					+ "WHERE ci.ID_CIDADE = ce.ID_CIDADE AND ce.ID_BAIRRO = ba.ID_BAIRRO ";

			// Definido o Statement, executamos a query no banco de dados
			rs = stm.executeQuery(SQLCliente);

			InstantiableEntityInfo entityInfo = (InstantiableEntityInfo) EntityRegister.getCacheInstance().getByType("GC_SIGMA_Central");

			int numPersist = persistEForm(rs, entityInfo, arraySigmaCentral, arrayFusionCentral);

			if (numPersist != 0)
				log.info("Persist " + numPersist + " new data for E-Form GC_SIGMA_Central");
			else
				log.info("No have new data for E-Form GC_SIGMA_Central");
		}
		finally
		{
			if (rs != null)
				rs.close();
		}
	}

	private void selectSigmaInstalador(Connection con, Statement stm) throws SQLException, ClassNotFoundException
	{
		ResultSet rs = null;
		try
		{
			// Vamos executar o seguinte comando SQL:
			String SQLCliente = " SELECT i.ID_INSTALADOR, i.NOME FROM SIGMA.dbo.dbINSTALADOR i ";

			// Definido o Statement, executamos a query no banco de dados
			rs = stm.executeQuery(SQLCliente);

			InstantiableEntityInfo entityInfo = (InstantiableEntityInfo) EntityRegister.getCacheInstance().getByType("GC_SIGMA_Instalador");

			int numPersist = persistEForm(rs, entityInfo, arraySigmaInstalador, arrayFusionInstalador);

			if (numPersist != 0)
				log.info("Persist " + numPersist + " new data for E-Form GC_SIGMA_Instalador");
			else
				log.info("No have new data for E-Form GC_SIGMA_Instalador");
		}
		finally
		{
			if (rs != null)
				rs.close();
		}
	}

	private void selectSigmaOperador(Connection con, Statement stm) throws SQLException, ClassNotFoundException
	{
		ResultSet rs = null;
		try
		{
			// Vamos executar o seguinte comando SQL:
			String SQLCliente = " SELECT o.ID_OPERADOR, o.NOME FROM SIGMA.dbo.dbOPERADOR o ";

			// Definido o Statement, executamos a query no banco de dados
			rs = stm.executeQuery(SQLCliente);

			InstantiableEntityInfo entityInfo = (InstantiableEntityInfo) EntityRegister.getCacheInstance().getByType("GC_SIGMA_Operador");

			int numPersist = persistEForm(rs, entityInfo, arraySigmaOperador, arrayFusionOperador);

			if (numPersist != 0)
				log.info("Persist " + numPersist + " new data for E-Form GC_SIGMA_Operador");
			else
				log.info("No have new data for E-Form GC_SIGMA_Operador");
		}
		finally
		{
			if (rs != null)
				rs.close();
		}
	}

	private int persistEForm(ResultSet rs, InstantiableEntityInfo entityInfo, String[] arraySigma, String[] arrayFusion) throws SQLException,
			ClassNotFoundException
	{
		int cont = 0;
		for (int numId = 0; rs.next(); numId++)
		{
			try
			{
				// armazena os resultados de cada linha do resultSet
				String[] arrayResult = new String[arraySigma.length];

				// captura os resultados
				for (int i = 0; i < arraySigma.length; i++)
				{
					String result = rs.getString(arraySigma[i]);
					arrayResult[i] = new String((result == null) ? "" : result);
				}

				NeoObject newObj = entityInfo.createNewInstance();
				EntityWrapper wrapper = new EntityWrapper(newObj);
				QLGroupFilter groupFilter = new QLGroupFilter("AND");

				for (int i = 0; i < arrayFusion.length; i++)
				{
					wrapper.setValue(arrayFusion[i], arrayResult[i]);

					// FIXME arruma para campos que possuem um tamanho muito
					// grande
					// esta dando problema com um campo com um tamanho de 5685
					if (arrayResult[i].length() < 4000 && !arrayResult[i].trim().equals(""))
						groupFilter.addFilter(QLEqualsFilter.buildFromObject(arrayFusion[i], arrayResult[i]));
				}

				NeoObject auxNeoObj = (NeoObject) PersistEngine.getObject(entityInfo.getEntityClass(), groupFilter);
				if (auxNeoObj == null)
				{
					PersistEngine.persist(newObj);
					cont++;
				}
			}
			catch (Exception e)
			{
				log.error("Persist error" + numId);
			}
		}
		return cont;
	}
}
