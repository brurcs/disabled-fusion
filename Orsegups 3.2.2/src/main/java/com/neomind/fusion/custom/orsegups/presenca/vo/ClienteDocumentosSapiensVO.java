package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ClienteDocumentosSapiensVO {
	
	private Long usuCodDoc;
	private Long usuCodEmp;
	private Long usuCodFil;
	
	
	public Long getUsuCodDoc() {
		return usuCodDoc;
	}
	public void setUsuCodDoc(Long usuCodDoc) {
		this.usuCodDoc = usuCodDoc;
	}
	public Long getUsuCodEmp() {
		return usuCodEmp;
	}
	public void setUsuCodEmp(Long usuCodEmp) {
		this.usuCodEmp = usuCodEmp;
	}
	public Long getUsuCodFil() {
		return usuCodFil;
	}
	public void setUsuCodFil(Long usuCodFil) {
		this.usuCodFil = usuCodFil;
	}
	
	@Override
	public String toString() {
		return "ClienteDocumentosSapiensVO [usuCodDoc=" + usuCodDoc
				+ ", usuCodEmp=" + usuCodEmp + ", usuCodFil=" + usuCodFil + "]";
	}
	
	
	
	
	
}
