package com.neomind.fusion.custom.orsegups.seventh.dto;

public class CFTVConfigDTO {
    
    private int idEmpresa;
    private String codigoCentral;
    private String servidorCFTV;
    private String usuarioCFTV;
    private String senhaCFTV;
    
    public int getIdEmpresa() {
        return idEmpresa;
    }
    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    public String getCodigoCentral() {
        return codigoCentral;
    }
    public void setCodigoCentral(String codigoCentral) {
        this.codigoCentral = codigoCentral;
    }
    public String getServidorCFTV() {
        return servidorCFTV;
    }
    public void setServidorCFTV(String servidorCFTV) {
        this.servidorCFTV = servidorCFTV;
    }
    public String getUsuarioCFTV() {
        return usuarioCFTV;
    }
    public void setUsuarioCFTV(String usuarioCFTV) {
        this.usuarioCFTV = usuarioCFTV;
    }
    public String getSenhaCFTV() {
        return senhaCFTV;
    }
    public void setSenhaCFTV(String senhaCFTV) {
        this.senhaCFTV = senhaCFTV;
    }
    
    

}
