package com.neomind.fusion.custom.orsegups.serasa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Conjunto de metodos estaticos para manipulacao do webservice de consulta ao serasa.
 * @author danilo.silva
 *
 */
public class SerasaUtils {

	/**
	 * Preenche a string com "+" à direita
	 * @param campo
	 * @param tamanho
	 * @return
	 */
	public static String preencheAlfaToSend(String campo, int tamanho){
		String retorno = "";
		if (campo== null){
			campo="";
		}else{
			campo = campo.trim();
		}
		int campoSize = campo.length();
		
		for (int i = 0; i < (tamanho - campoSize); i++){
			retorno +="+";
		}
		return campo + retorno;
		 
	}
	
	/**
	 * Preenche a string com "0" à esquerda
	 * @param campo
	 * @param tamanho
	 * @return
	 */
	public static String preencheNumeroToSend(String campo, int tamanho){
		String retorno = "";
		if (campo== null){
			campo="";
		}else{
			campo = campo.trim();
		}
		int campoSize = campo.length();
		
		for (int i = 0; i < (tamanho - campoSize); i++){
			retorno +="0";
		}
		return retorno + campo  ;

		 
	}
	public static String fillSpacesTiSend(String str){
		return str.replaceAll(" ", "+");
		
	}
	
	public static String enviaConsultaSerasa(String login, String senha, String parametros, boolean emProducao){
		String content = "?p=" + login + SerasaUtils.preencheAlfaToSend(senha, 8) + "++++++++" + parametros;
		
		try {
			//System.out.println("Request = " + parametros);
			String str ="";
			URL url = null;
			if(emProducao)
			{
				str = "https://sitenet43.serasa.com.br/Prod/consultahttps";
			}else
			{
				str = "https://mqlinuxext.serasa.com.br/Homologa/consultahttps";
			}
			System.out.println("[FLUXO CONTRATOS]-Serasa: "+str+content);
			url = new URL(str+content);
			
			  
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();  
			conn.setRequestMethod("POST");  
			conn.setDoOutput(true);  
			conn.setInstanceFollowRedirects(false);  
			conn.connect();
			
			InputStream is = conn.getInputStream();   
			BufferedReader bufr = new BufferedReader( new InputStreamReader( is ) );   
			String linha = null;   
			StringBuffer content2 = new StringBuffer();  
	
			while( ( linha = bufr.readLine() ) != null )   
				content2.append( linha + "\n");
			
			//System.out.print("Response= " + content2.toString() );
			return content2.toString();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	public static void main(String args[]){
		String login = "57201123";
		String senha = "d06m07";
		
		
		RegistroSerasa regB49C;
		try
		{
			regB49C = RegistroSerasaFactory.getReg("B49C");
			regB49C.setupValores("B49C","","00958730903","F","C","FI","","","S","99","S","INI","A","N","","","","","","N","","","");
			RegistroSerasa regP002 = RegistroSerasaFactory.getReg("P002"); 
			regP002.setupValores("P002","RSPU","","","","","","","","");
			RegistroSerasa  regI001 = RegistroSerasaFactory.getReg("I001");
			regI001.setupValores("I001","00","D","S","N","N","N","N","N","N","N","N","N","N","N","N","","","","","","","N","N","N","","","");
			RegistroSerasa regT999 = RegistroSerasaFactory.getReg("T999");
			regT999.setupValores("T999","","","");
			
			String content2 = SerasaUtils.enviaConsultaSerasa(login, senha, regB49C.parseToString()
					+regP002.parseToString()
					+regI001.parseToString()
					+regT999.parseToString()
					,true);
			System.out.println(content2);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
