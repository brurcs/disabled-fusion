package com.neomind.fusion.custom.orsegups.maps.call.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.callcenter.OrsegupsIncommingCallServlet;
import com.neomind.fusion.custom.orsegups.callcenter.SnepVO;
import com.neomind.fusion.custom.orsegups.maps.call.engine.CallAlertAitStatus;
import com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertAitEngine;

@WebServlet(name="OrsegupsAlertAitServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertAitServlet"})
public class OrsegupsAlertAitServlet extends HttpServlet
{
	
	private static final Log log = LogFactory.getLog(OrsegupsAlertAitServlet.class);
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException
	{
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("ISO-8859-1");

		Gson gson = new Gson();
		String externalNumber = null;
		String extensionNumber = null;
		String callStatus = null;
		String time = null;
		Long timeL = 0L;
		CallAlertAitStatus callAlertAitStatus = null;

		// busca os dados da chamada feita pelo snep que imprime o json no corpo da requisicao
		BufferedReader inputStream = new BufferedReader(new InputStreamReader(req.getInputStream()));
		String line = null;
		StringBuilder responseData = new StringBuilder();
		while ((line = inputStream.readLine()) != null)
		{
			responseData.append(line);
		}
		String json = responseData.toString();

		if (json != null && !json.isEmpty())
		{
			SnepVO snepVO = gson.fromJson(json, SnepVO.class);

			log.debug("Dados recebidos SNEP: " + responseData.toString());

			externalNumber = snepVO.getCallerid();
			extensionNumber = snepVO.getExten();
		}
		else
		{
			externalNumber = req.getParameter("externalNumber");
			extensionNumber = req.getParameter("extensionNumber");
			callStatus = req.getParameter("callStatus");
			time = req.getParameter("time");
			
			log.debug("Dados recebidos chamanda manual GET: externalNumber=" + externalNumber + " - extensionNumber=" + extensionNumber);
		}
			timeL = Long.parseLong(time);
		
		// TODO verifica se o retorno o snep vai ser desta maneira mesmo 
		// e realizar a chamada ao serviço de alertas passando os parametros
		if(CallAlertAitStatus.ATENDIDA.name().equals(callStatus))
			callAlertAitStatus = CallAlertAitStatus.ATENDIDA;
		if(CallAlertAitStatus.ATENDIDA_DESLIGADA.name().equals(callStatus))
			callAlertAitStatus = CallAlertAitStatus.ATENDIDA_DESLIGADA;
		if(CallAlertAitStatus.NAO_ATENDIDA.name().equals(callStatus))
			callAlertAitStatus = CallAlertAitStatus.NAO_ATENDIDA;
			
		
	//	OrsegupsAlertAitEngine.getInstance().receiveCall(extensionNumber, externalNumber, callAlertAitStatus, timeL,null);
	}
}
