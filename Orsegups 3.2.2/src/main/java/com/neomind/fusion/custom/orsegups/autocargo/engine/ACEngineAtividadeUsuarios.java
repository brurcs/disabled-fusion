package com.neomind.fusion.custom.orsegups.autocargo.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.autocargo.beans.user.User;
import com.neomind.fusion.custom.orsegups.autocargo.beans.user.Users;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;

public class ACEngineAtividadeUsuarios { // FalhaComunicacaoVeiculos

    private final Log log = LogFactory.getLog(ACEngineAtividadeUsuarios.class);

    private Long key = GregorianCalendar.getInstance().getTimeInMillis();

    private SIGMAUtilsEngine engine = new SIGMAUtilsEngine();

    /**
     * Veiculos com falha de comunicação de pelo 12 horas na data de hoje.
     * <tt>Rastreador</tt> é a chave do mapa
     */
    private Map<String, User> activities = new HashMap<String, User>();

    /**
     * Contas <tt>rastreadores</tt> que possuem eventos R003 abertos
     * <br> Chave: nome Fantasia
     */
    private Map<String, EventoVO> activitiesOpenEvents = new HashMap<String, EventoVO>();

    /**
     * Número total de páginas recebidos da API de consulta de falhas de eventos
     */
    private int totalPageNumber = 1;

    /**
     * Inconsistencias na inserção de eventos
     */
    private List<String> inconsistencias = new ArrayList<String>();

    public String runMiscommunication() {

	for (int i = 0; i < totalPageNumber; i++) {

	    try {
		this.getActivities(i);
	    } catch (RuntimeException e) {
		this.logError(e.getMessage(), "Erro ao acessar API", "Orsegups");
		log.error("Erro ao acessar a API Autocargo ACEngineAtividadeUsuarios [" + key + "] :");
		e.printStackTrace();
		throw new JobException("Erro ao acessar API de consulta de atividade de usuários Autocargo! Procurar no log por [" + key + "]");
	    }

	    try {
		Thread.sleep(1000);
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    }
	}

	boolean getEvents = this.getActivitiesOpenEvents();

	int countTry = 0;

	if (!getEvents && countTry < 2) {
	    getEvents = this.getActivitiesOpenEvents();
	    countTry++;
	}

	if (!this.activities.isEmpty()) {
	    for (String key : activities.keySet()) {

		if (activitiesOpenEvents.containsKey(key)) {
		    /* Já possui evento aberto, não faço nada */
		    activitiesOpenEvents.remove(key);
		} else {
		    String retorno = engine.receberEventoByIdentificadorCliente(key, "R003");

		    if (!retorno.equals("ACK")) {
			User u = activities.get(key);
			String cliente = "Cliente: " + u.getClient_name();
			this.logError("R003", retorno, cliente);
			this.inconsistencias.add(retorno + ";" + cliente);
		    }

		}
	    }
	}

	this.sendEmail();

	return "OK";

    }

    /**
     * Popula o set {@link #activitiesOpenEvents}
     * 
     * @return <tt>true</tt> se executou com sucesso <tt>false</tt> caso
     *         contrário
     */
    private boolean getActivitiesOpenEvents() {

	boolean executouComSucesso = false;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT C.NM_RASTREADOR, C.ID_CENTRAL, C.PARTICAO, C.FANTASIA FROM HISTORICO H WITH(NOLOCK) ");
	sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
	sql.append(" WHERE H.CD_EVENTO = 'R003' ");
	sql.append(" ORDER BY DT_PROCESSADO DESC ");

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	try {

	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {

		EventoVO novoEvento = new EventoVO();

		novoEvento.setCodigoCentral(rs.getString(2));
		novoEvento.setParticao(rs.getString(3));
		novoEvento.setFantasia(rs.getString(4));

		this.activitiesOpenEvents.put(rs.getString(4), novoEvento);
	    }

	    executouComSucesso = true;

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return executouComSucesso;

    }

    /**
     * Popula o mapa {@link #miscommunication}
     * 
     * @param pageNumber
     *            {@link #totalPageNumber} Não é necessário modificar
     */
    private void getActivities(int pageNumber) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://www2.autocargo.com.br/api/integration/v1/user_activities?days_ago=45");
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Authorization", "Token token=\"695688a822fd6ff8c1f5244be889b16e259c3ed604a9bdb7e937937ce7f612df\"");
	    conn.setRequestProperty("Content-Type", "application/json");
	    conn.setRequestProperty("Accept", "application/json");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException(String.valueOf(conn.getResponseCode()));
	    }

	    String totalPorPagina = conn.getHeaderField("X-Autocargo-Per-Page");
	    String totalResultados = conn.getHeaderField("X-Autocargo-Total");

	    int total = Integer.parseInt(totalPorPagina);

	    int result = this.roundUp(Integer.parseInt(totalResultados), total);

	    this.totalPageNumber = result / total;

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {

	    e.printStackTrace();

	} catch (IOException e) {

	    e.printStackTrace();

	}

	System.out.println(retorno.toString());
	Gson gson = new Gson();
	Users resultList = gson.fromJson(retorno.toString(), Users.class);

	for (User u : resultList.getUsers()) {
	    activities.put(u.getClient_name(), u);
	}

    }

    private int roundUp(int toRound, int roundTo) {

	if (toRound % roundTo == 0) {
	    return toRound;
	} else {
	    return (roundTo - toRound % roundTo) + toRound;
	}

    }

    private void logError(String codigoEvento, String erro, String cliente) {

	Connection conn = null;
	PreparedStatement pstm = null;

	String sql = "INSERT INTO AtividadeUsuariosLogs (CODIGO_EVENTO, ERRO, CLIENTE, DATA_ERRO) VALUES (?,?,?,?)";

	try {
	    conn = PersistEngine.getConnection("TIDB");

	    pstm = conn.prepareStatement(sql);

	    pstm.setString(1, codigoEvento);
	    pstm.setString(2, erro);
	    pstm.setString(3, cliente);
	    pstm.setTimestamp(4, new Timestamp(Calendar.getInstance().getTimeInMillis()));

	    pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

    }

    /**
     * Encaminha e-mail de informe de inconsistências
     */
    private void sendEmail() {

	if (!this.inconsistencias.isEmpty()) {

	    StringBuilder dados = new StringBuilder();

	    int cont = 0;

	    for (String i : this.inconsistencias) {

		String a[] = i.split(";");

		int resto = (cont) % 2;
		if (resto == 0) {
		    dados.append("<tr style=\"background-color: #d6e9f9\" ><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
		} else {
		    dados.append("<tr><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
		}

		cont++;
	    }

	    StringBuilder corpoEmail = new StringBuilder();

	    corpoEmail.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
	    corpoEmail.append("<html>");
	    corpoEmail.append("<head>");
	    corpoEmail.append("<meta charset=\"ISO-8859-1\">");
	    corpoEmail.append("<title>Relatório de inconsistências</title>");
	    corpoEmail.append("<style>");
	    corpoEmail.append("h2{");
	    corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-p{");
	    corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info{");
	    corpoEmail.append("table-layout: fixed;");
	    corpoEmail.append("text-align:left;");
	    corpoEmail.append("font-family: Verdana;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-info td{");
	    corpoEmail.append("overflow-x: hidden;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info thead{");
	    corpoEmail.append("background-color:#303090;");
	    corpoEmail.append("color: white;");
	    corpoEmail.append("font-weight:bold;");
	    corpoEmail.append("	}");
	    corpoEmail.append("</style>");
	    corpoEmail.append("</head>");
	    corpoEmail.append("<body>");
	    corpoEmail.append("<div>");
	    corpoEmail.append("<table width=\"600\" align=\"center\">");
	    corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<td><h2>Inconsistências de falha de comunicação</h2></td>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>As seguintes inconsistências ocorreram durante a inserção de eventos de falha de comunicação:</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
	    corpoEmail.append("<thead>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td>Inconsistência</td>");
	    corpoEmail.append("<td>Cliente</td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</thead>");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<!-- Recebe dados do método -->");
	    corpoEmail.append(dados);
	    corpoEmail.append("<br>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<br>");
	    corpoEmail.append("<!-- Rodapé do e-mail -->");
	    corpoEmail.append("<div class=\"rodape\">");
	    corpoEmail.append("<table width=\"600\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td width=\"360\" valign=\"bottom\"><img src=\"https://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-1.jpg\"></td>");
	    corpoEmail
		    .append("<td><a href=\"http://www.orsegups.com.br\" target=\"_blank\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-2.jpg\" width=\"260\" height=\"120\" alt=\"\"/></a></td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</body>");
	    corpoEmail.append("</html>");

	    String[] addTo = new String[] { "gustavo.freitas@orsegups.com.br" };
	    String[] comCopia = new String[] { "emailautomatico@orsegups.com.br" };
	    String from = "fusion@orsegups.com.br";
	    String subject = "Relatório de Cobrança de OS";
	    String html = corpoEmail.toString();

	    OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);

	}

    }

}
