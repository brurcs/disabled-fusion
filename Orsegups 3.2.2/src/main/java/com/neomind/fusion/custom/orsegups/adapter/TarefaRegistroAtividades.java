package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class TarefaRegistroAtividades implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(TarefaRegistroAtividades.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity) throws TaskException
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		String erro = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("TarefaRegistroAtividades");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);
			Long codigo = 0L;
			String txtDescricao = "";
			Long acaoTarefa = 0L;

			if (NeoUtils.safeIsNull(origin))
			{
				NeoPaper papel = new NeoPaper();
				papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
				NeoUser usuarioResponsavel = new NeoUser();
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						usuarioResponsavel = user;
						break;
					}
				}
				wRegistro.findField("responsavel").setValue(usuarioResponsavel);
				wRegistro.findField("dataInicial").setValue(new GregorianCalendar());
				wRegistro.findField("dataFinal").setValue(new GregorianCalendar());
				wRegistro.findField("atividade").setValue("Tarefa escalada em pool");
				wRegistro.findField("prazo").setValue(wrapper.findValue("Prazo"));
			}
			else
			{
				//valores padrão, existentes em toda tarefa de usuário
				wRegistro.findField("responsavel").setValue(origin.getUser());
				wRegistro.findField("dataInicial").setValue(origin.getStartDate());
				wRegistro.findField("dataFinal").setValue(origin.getFinishDate());

				if (origin.getActivityName().equalsIgnoreCase("Solicitar tarefa"))
				{
					wRegistro.findField("atividade").setValue("Abriu tarefa");
				}
				else if (origin.getActivityName().equalsIgnoreCase("Realizar tarefa"))
				{
					NeoObject rAcao = (NeoObject) wrapper.findValue("acaoTarefa");
					if (NeoUtils.safeIsNotNull(rAcao))
					{
						EntityWrapper wAcao = new EntityWrapper(rAcao);
						codigo = (Long) wAcao.findValue("codigo");
					}
					if (codigo.equals(3L))
					{
						wRegistro.findField("atividade").setValue("Solicitou ajuste");
					}
					else if (codigo.equals(1L))
					{
						wRegistro.findField("atividade").setValue("Realizou tarefa");
					}
					else
					{
						wRegistro.findField("atividade").setValue("Tarefa Escalada");
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Reavaliar Solicitação"))
				{
					if (wrapper.getValue("aceitaProrrogacaoPrazo").equals(Boolean.TRUE))
					{
						wRegistro.findField("atividade").setValue("Ajustou tarefa");
					}
					else
					{
						wRegistro.findField("atividade").setValue("Desaprovou ajuste");
						wrapper.setValue("aceitaProrrogacaoPrazo", null);
						GregorianCalendar dtPrazo = (GregorianCalendar) wrapper.getValue("Prazo");
						GregorianCalendar data = new GregorianCalendar();
						data.set(Calendar.HOUR_OF_DAY, 23);
						data.set(Calendar.MINUTE, 59);
						data.set(Calendar.SECOND, 59);
						data.add(Calendar.DAY_OF_MONTH, -1);
						if (dtPrazo != null && dtPrazo.before(data))
						{
							wrapper.setValue("Prazo", data);
						}
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa"))
				{
					if (wrapper.getValue("AprovacaoSolicitante").equals(Boolean.TRUE))
					{
						wRegistro.findField("atividade").setValue("Aprovou tarefa");
					}
					else
					{
						wRegistro.findField("atividade").setValue("Desaprovou tarefa");

					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada") || origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada Diretoria") || origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada - Último Nível"))
				{
					wRegistro.findField("atividade").setValue("Realizou tarefa - Escalada");
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa Escalada") || origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa Escalada Diretoria") || origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa - Escalada Último Nível"))
				{
					if (wrapper.getValue("AprovacaoSolicitante").equals(Boolean.TRUE))
					{
						wRegistro.findField("atividade").setValue("Aprovou tarefa - Escalada");
					}
					else
					{
						wRegistro.findField("atividade").setValue("Desaprovou tarefa - Escalada");
					}
				}

				if (origin.getActivityName().equalsIgnoreCase("Solicitar tarefa") || origin.getActivityName().equalsIgnoreCase("Reavaliar Solicitação") || origin.getProcessName().equals("F002 - FAP Slip"))
				{
					txtDescricao = (String) wrapper.findValue("DescricaoSolicitacao");
					acaoTarefa = (Long) wrapper.findValue("acaoTarefa.codigo");
					wRegistro.findField("descricao").setValue(txtDescricao);
					wrapper.setValue("DescricaoSolicitacao", null);
					wRegistro.findField("anexo").setValue(wrapper.findValue("AnexoSolicitante"));
					wrapper.setValue("AnexoSolicitante", null);
					wRegistro.findField("prazo").setValue(wrapper.findValue("Prazo"));
					wrapper.setValue("acaoTarefa", null);
				}

				if (origin.getActivityName().equalsIgnoreCase("Realizar tarefa") || origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada") || origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada Diretoria"))
				{
					txtDescricao = (String) wrapper.findValue("DescricaoExecucao");
					acaoTarefa = (Long) wrapper.findValue("acaoTarefa.codigo");
					wRegistro.findField("descricao").setValue(txtDescricao);
					wrapper.setValue("DescricaoExecucao", null);
					wRegistro.findField("anexo").setValue(wrapper.findValue("Anexo"));
					wrapper.setValue("Anexo", null);
					wRegistro.findField("prazo").setValue(wrapper.findValue("Prazo"));
					wrapper.setValue("acaoTarefa", null);
				}

				NeoUser userExec = origin.getInstance().getOwner();

				Boolean eDiretor = Boolean.FALSE;

				Set<NeoPaper> papeisUserExec = userExec.getPapers();

				for (NeoPaper papelUserExec : papeisUserExec)
				{

					String paperName = papelUserExec.getName();

					if (paperName.contains("Diretor ") || paperName.contains("Gerente "))
						eDiretor = Boolean.TRUE;

				}

				if (origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada - Último Nível") || origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada Diretoria") || (origin.getActivityName().contains("Realizar tarefa - Escalada") && eDiretor))
				{
					Boolean reenviarTarefa = (Boolean) wrapper.findValue("reenviarTarefa");
					acaoTarefa = (Long) wrapper.findValue("acaoTarefa.codigo");
					wRegistro.findField("anexo").setValue(wrapper.findValue("Anexo"));
					wrapper.setValue("reenviarTarefa", false);
					wrapper.setValue("Anexo", null);
					wRegistro.findField("prazo").setValue(wrapper.findValue("Prazo"));
					wrapper.setValue("acaoTarefa", null);

					if (NeoUtils.safeIsNotNull(reenviarTarefa) && reenviarTarefa)
					{
						if (txtDescricao == null || txtDescricao.isEmpty())
							txtDescricao = (String) wrapper.findValue("descricaoReenviar");
						wRegistro.findField("descricao").setValue(txtDescricao);
						wrapper.setValue("descricaoReenviar", null);
					}
					else
					{
						if (txtDescricao == null || txtDescricao.isEmpty())
							txtDescricao = (String) wrapper.findValue("DescricaoExecucao");
						wRegistro.findField("descricao").setValue(txtDescricao);
						wrapper.setValue("DescricaoExecucao", null);
					}
				}

				if (origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa") || origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa Escalada") || origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa Escalada Diretoria") || origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa - Escalada Último Nível"))
				{
					if ((Boolean) wrapper.findValue("AprovacaoSolicitante") == Boolean.TRUE)
					{
						txtDescricao = (String) wrapper.findValue("ObservacaoSolicitante");
						wRegistro.findField("descricao").setValue(txtDescricao);
						wrapper.setValue("ObservacaoSolicitante", null);
						wRegistro.findField("prazo").setValue(wrapper.findValue("Prazo"));
					}
					else
					{
						if (NeoUtils.safeIsNull((String) wrapper.findValue("justificativaSolicitante")) || (NeoUtils.safeIsNotNull((String) wrapper.findValue("justificativaSolicitante")) && wrapper.findValue("justificativaSolicitante").toString().trim().isEmpty()))
						{
							erro = "Campo Justificativa do Solicitante Requerido!";
							throw new WorkflowException(erro);
						}
						else
						{
							txtDescricao = (String) wrapper.findValue("justificativaSolicitante");
							wRegistro.findField("descricao").setValue(txtDescricao);
							wrapper.setValue("justificativaSolicitante", null);
							wRegistro.findField("prazo").setValue(wrapper.findValue("Prazo"));
						}
					}
				}
				//Tarefa de Falta de Efetivo
				String titulo = (String) wrapper.findValue("Titulo");

				if (titulo.startsWith("#Informar falta de efetivo no posto "))
				{
					String txtLog = "";

					if ((origin.getActivityName().equalsIgnoreCase("Realizar tarefa") || origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada") || origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada Diretoria") || origin.getActivityName().equalsIgnoreCase("Realizar tarefa - Escalada - Último Nível")) && acaoTarefa == 1L)
					{
						txtDescricao = txtDescricao.replaceAll("<.*?>", "");
						txtLog = "Resposta informada na tarefa de falta de efetivo " + activity.getProcess().getCode() + ": " + txtDescricao;
					}

					if ((origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa") || origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa Escalada") || origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa Escalada Diretoria") || origin.getActivityName().equalsIgnoreCase("Aprovar Tarefa - Escalada Último Nível")) && wrapper.findValue("AprovacaoSolicitante") == Boolean.FALSE)
					{
						txtDescricao = txtDescricao.replaceAll("<.*?>", "");
						txtLog = "Não aprovação da tarefa de falta de efetivo " + activity.getProcess().getCode() + ": " + txtDescricao;
					}

					if (!txtLog.equals(""))
					{
						String centroCusto = titulo.substring(36, titulo.length());
						QLPresencaUtils.saveLog(centroCusto, txtLog);
					}
				}
			}
			PersistEngine.persist(registro);
			wrapper.findField("registroAtividades").addValue(registro);
			System.out.println("  ");

		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
	}

	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}