package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;

public class AdapterReavaliarSolicitacaoG001 implements TaskFinishEventListener {

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
		
		EntityWrapper eForm = new EntityWrapper(event.getProcess().getProcessEntity());
		boolean aceitaProrrogacao = eForm.findGenericValue("aceitaProrrogacaoPrazo");
		
		if(!aceitaProrrogacao) {
			Task taskOrigin = OrsegupsWorkflowHelper.getTaskOrigin(event.getActivity());
			eForm.findField("Prazo").setValue(taskOrigin.getDueDate());
		}		
	}
}
