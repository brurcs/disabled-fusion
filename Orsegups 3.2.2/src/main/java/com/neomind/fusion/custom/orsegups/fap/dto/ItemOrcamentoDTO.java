package com.neomind.fusion.custom.orsegups.fap.dto;

import java.math.BigDecimal;

public class ItemOrcamentoDTO {
    String tipo = null;
    String descricao = null;
    BigDecimal valor = null;
    
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public BigDecimal getValor() {
        return valor;
    }
    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
    
    
}
