package com.neomind.fusion.custom.orsegups.contract;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoRegistraRevisao implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		boolean inviabilizarContrato = NeoUtils.safeBoolean(wrapper.findValue("inviabilizarContrato"));
		try{
			boolean aprovadoAnalise = NeoUtils.safeBoolean(wrapper.findValue("aprovadoAnalise"));
			boolean necessarioAprovacaoValores = NeoUtils.safeBoolean(wrapper.findValue("necessarioAprovacaoValores"));
			
			
			
			
			
			NeoObject tramite = AdapterUtils.createNewEntityInstance("FGChistoricoTramite");
			EntityWrapper wTramite = new EntityWrapper(tramite);
			wTramite.setValue("usuario", PortalUtil.getCurrentUser().getCode() );
			wTramite.setValue("obs", NeoUtils.safeOutputString(wrapper.findValue("obsTramite")) );
			
			if (inviabilizarContrato){
				wTramite.setValue("acao", "Inviabilizou o contrato." );
			}else if (!inviabilizarContrato){
				if (!aprovadoAnalise && necessarioAprovacaoValores){
					wTramite.setValue("acao", "Revisou Aprovação de Valores." );
				}else if (!aprovadoAnalise && !necessarioAprovacaoValores){
					wTramite.setValue("acao", "Revisou Cadastro" );
				}
			}else{
				
			}
			
			PersistEngine.persist(tramite);
			
			wrapper.findField("listaTramites").addValue(tramite);
			wrapper.setValue("motivo","");
			wrapper.setValue("obsTramite","");
		}catch(Exception e){
			e.printStackTrace();
			if (inviabilizarContrato){
				throw new WorkflowException("Erro inviabilizar. msg: "+e.getMessage());
			}else{
				throw new WorkflowException("Erro revisar. msg: "+e.getMessage());
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
		
	}


}
