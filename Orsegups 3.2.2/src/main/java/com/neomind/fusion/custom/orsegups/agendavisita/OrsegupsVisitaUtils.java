package com.neomind.fusion.custom.orsegups.agendavisita;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.custom.orsegups.mobile.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.security.NeoUser;
 
public class OrsegupsVisitaUtils
{
	

	public static String classificacaoCentroCusto(String codccu) 
	{
		String claccu = "";
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();
		Connection conn = OrsegupsUtils.getConnection("SAPIENS");
		ResultSet rs = null;

		try
		{
			sql.append("SELECT claccu FROM E044CCU WHERE CODCCU = " + codccu);
			pst = conn.prepareStatement(sql.toString());
			rs = pst.executeQuery();
			if (rs != null)
			{
				claccu = rs.getString("claccu");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return claccu;
	}

	public static int codigoCentroDeCusto(String Claccu, String nivCcu) 
	{
		int codccu = 0;
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();
		Connection conn = OrsegupsUtils.getConnection("SAPIENS");
		ResultSet rs = null;

		try
		{
			sql.append("SELECT codccu FROM E044CCU WHERE CLACCU  like '" + Claccu + "%' and nivccu = "+nivCcu+" and codemp = 1");
			pst = conn.prepareStatement(sql.toString());
			rs = pst.executeQuery();
			while(rs.next())
			{
				codccu = rs.getInt("codccu");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return codccu;
	}

	public static Long buscaCodigoCliente(int codCcuNiv8) 
	{
		PreparedStatement pst = null;
		Long codcli = 0L;
		
		
		Connection conn = OrsegupsUtils.getConnection("SAPIENS");
		StringBuilder sql = new StringBuilder();
		ResultSet rs = null;
		try
		{
			sql.append("SELECT top 1 codcli");
			sql.append("	FROM E044CCU ccu");
			sql.append("	Inner join usu_t160cvs p on p.usu_codccu = ccu.codccu and ccu.codemp = p.usu_codemp");
			sql.append("	Inner join usu_t160ctr c on c.usu_numctr = p.usu_numctr and c.usu_codemp = p.usu_codemp and c.usu_codfil = c.usu_codfil");
			sql.append("	Inner join e085cli cli on cli.codcli = c.usu_codcli ");
			sql.append("	Where ccu.codccu = "+ codCcuNiv8);
			pst = conn.prepareStatement(sql.toString());
			rs = pst.executeQuery();
			while(rs.next())
			{
				codcli = rs.getLong("codcli");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return codcli;
	}

}
