package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class RotinaAbreTarefaSimplesEventosFalhaTransmissao implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesEventosFalhaTransmissao.class);

    @Override
    public void execute(CustomJobContext arg0) {
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
	System.out.println("RotinaAbreTarefaSimplesEventosFalhaTransmissao Inicio execução em: " + dateFormat.format(new GregorianCalendar().getTime()));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Connection conn = PersistEngine.getConnection("SIGMA90");
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;

	String ultimaExecucaoRotina = OrsegupsEmailUtils.ultimaExecucaoRotinaEmail("RotinaAbreTarefaSimplesEventosFalhaTransmissao");

	try {

	    strSigma.append(" 	SELECT H.CD_HISTORICO, H.CD_EVENTO, H.CD_CODE, H.DT_RECEBIDO, C.ID_CENTRAL, C.PARTICAO, C.ID_EMPRESA, C.CD_CLIENTE, C.FANTASIA, C.RAZAO ");
	    strSigma.append(" FROM VIEW_HISTORICO H WITH(NOLOCK) ");
	    strSigma.append(" INNER JOIN DBCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
	    strSigma.append(" WHERE H.DT_RECEBIDO > '" + ultimaExecucaoRotina + "' ");
	    strSigma.append(" AND H.CD_EVENTO IN ('E911','E916') ");

	    pstm = conn.prepareStatement(strSigma.toString());
	    
	    boolean gravou = OrsegupsEmailUtils.inserirFimRotinaEmail("RotinaAbreTarefaSimplesEventosFalhaTransmissao");
	    
	    System.out.println("RotinaAbreTarefaSimplesEventosFalhaTransmissao sucesso ao inserir fim da rotina: "+gravou);
	    
	    ResultSet rs = pstm.executeQuery();

	    while (rs.next()) {
		
		String cdEvento = rs.getString("CD_EVENTO");
		String cdCode = rs.getString("CD_CODE");
		GregorianCalendar dtRecebido = new GregorianCalendar();
		dtRecebido.setTimeInMillis(rs.getTimestamp("DT_RECEBIDO").getTime());
		String idCentral = rs.getString("ID_CENTRAL");
		String particao = rs.getString("PARTICAO");
		String idEmpresa = rs.getString("ID_EMPRESA");
		String cdCliente = rs.getString("CD_CLIENTE");
		String fantasia = rs.getString("FANTASIA");
		String razao = rs.getString("RAZAO");
		
		String historico = rs.getString("CD_HISTORICO");
		
		String descricao = "";
		descricao = "<strong>DETALHES DO EVENTO:</strong><br>";
		descricao = descricao + "<strong>Evento:</strong> " + cdEvento+ "<br>";
		descricao = descricao + "<strong>CUC:</strong> " + cdCode+ "<br>";
		descricao = descricao + "<strong>Conta:</strong> " + idCentral + "<br>";
		descricao = descricao + "<strong>Partição:</strong> " + particao + "<br>";
		descricao = descricao + "<strong>Empresa:</strong> " + idEmpresa + "<br>";
		descricao = descricao + "<strong>Cliente:</strong> " + cdCliente + "<br>";
		descricao = descricao + "<strong>Fantasia:</strong> " + fantasia + "<br>";
		descricao = descricao + "<strong>Razão:</strong> " + razao + "<br>";
		descricao = descricao + "<strong>Recebido:</strong> " + dateFormat.format(dtRecebido.getTime()) + "<br>";
		
		String titulo = "Verificar evento "+cdEvento+" recebido em "+dateFormat.format(dtRecebido.getTime());
		
		String retorno = abrirTarefa(descricao, titulo);
		if (retorno != null && !retorno.isEmpty() && (!retorno.trim().contains("Erro"))) {
		    salvarHistoricoTarefa(Long.valueOf(cdCliente), retorno, historico);
		}
	    }
	    System.out.println("RotinaAbreTarefaSimplesEventosFalhaTransmissao Fim execução em: " + dateFormat.format(new GregorianCalendar().getTime()));
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println("[" + key + "] RotinaAbreTarefaSimplesEventosFalhaTransmissao erro no processamento." + e.getMessage());
	    log.error("RotinaAbreTarefaSimplesEventosFalhaTransmissao erro no processamento:");
	    throw new JobException("Erro no processamento. Procurar no log por [" + key + "] " + e.getMessage());
	} finally {
	    try {
		if (conn != null) {
		    conn.close();
		}
		if (pstm != null) {
		    pstm.close();
		}
	    } catch (Exception e2) {
		log.error("RotinaAbreTarefaSimplesEventosFalhaTransmissao erro fechamento de recursos cadastro AIT:" + e2.getMessage());
	    }

	}

    }
    
    private static String abrirTarefa(String descricao, String titulo) throws Exception {
	String tarefa = "";

	String solicitante = "edson.heinz";
	String executor = "laercio.leite";

	GregorianCalendar prazo = new GregorianCalendar();
	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

	tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);

	return tarefa;
    }
    
    private static void salvarHistoricoTarefa(Long cdCliente, String tarefa, String historico){
	Connection conn = PersistEngine.getConnection("TIDB");
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;

	try {

	    strSigma.append(" 	 INSERT INTO HISTORICO_TAREFA_EVENTOS_FALHA_COMUNICACAO (CD_CLIENTE, TAREFA, HISTORICO) VALUES ("+cdCliente+",'"+tarefa+"','"+historico+"')  ");
	    pstm = conn.prepareStatement(strSigma.toString());
	    pstm.executeUpdate();
	  
	    System.out.println("RotinaAbreTarefaSimplesEventosFalhaTransmissao Fim execução do fechamento do evento em: ");
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("RotinaAbreTarefaSimplesEventosFalhaTransmissao erro no processamento da gravacao do historico:");
	    throw new JobException("Erro no processamento. gravacao do historico " + e.getMessage());
	} finally {
	    try {
		if (conn != null) {
		    conn.close();
		}
		if (pstm != null) {
		    pstm.close();
		}
	    } catch (Exception e2) {
		log.error("RotinaAbreTarefaSimplesEventosFalhaTransmissao gravacao do historico:" + e2.getMessage());
	    }

	}
    }
    
}