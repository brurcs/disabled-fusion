package com.neomind.fusion.custom.orsegups.e2doc.xml.livroFinanceiro;

public class Campos {
    
    private String i92;
    private String i94;
    private String i95;
    private String i96;
    private String i97;
    
    public String getI92() {
        return i92;
    }
    public void setI92(String i92) {
        this.i92 = i92;
    }
    public String getI94() {
        return i94;
    }
    public void setI94(String i94) {
        this.i94 = i94;
    }
    public String getI95() {
        return i95;
    }
    public void setI95(String i95) {
        this.i95 = i95;
    }
    public String getI96() {
        return i96;
    }
    public void setI96(String i96) {
        this.i96 = i96;
    }
    public String getI97() {
        return i97;
    }
    public void setI97(String i97) {
        this.i97 = i97;
    }    
}
