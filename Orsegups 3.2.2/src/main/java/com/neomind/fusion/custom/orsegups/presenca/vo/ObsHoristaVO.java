package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ObsHoristaVO
{
	String dataRegistro;
	Long horaRegistro;
	Long numCadHor;
	Long numEmpHor;
	Long tipCol;
	String observacao;
	String neoId;
	String linkExcluir;
	String numcpf;

	public String getNumcpf()
	{
		return numcpf;
	}

	public void setNumcpf(String numcpf)
	{
		this.numcpf = numcpf;
	}

	public String getLinkExcluir()
	{
		return linkExcluir;
	}

	public void setLinkExcluir(String linkExcluir)
	{
		this.linkExcluir = linkExcluir;
	}

	public String getDataRegistro()
	{
		return dataRegistro;
	}

	public void setDataRegistro(String dataRegistro)
	{
		this.dataRegistro = dataRegistro;
	}

	public Long getHoraRegistro()
	{
		return horaRegistro;
	}

	public void setHoraRegistro(Long horaRegistro)
	{
		this.horaRegistro = horaRegistro;
	}

	public Long getNumCadHor()
	{
		return numCadHor;
	}

	public void setNumCadHor(Long numCadHor)
	{
		this.numCadHor = numCadHor;
	}

	public Long getNumEmpHor()
	{
		return numEmpHor;
	}

	public void setNumEmpHor(Long numEmpHor)
	{
		this.numEmpHor = numEmpHor;
	}

	public Long getTipCol()
	{
		return tipCol;
	}

	public void setTipCol(Long tipCol)
	{
		this.tipCol = tipCol;
	}

	public String getObservacao()
	{
		return observacao;
	}

	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}

	public String getNeoId()
	{
		return neoId;
	}

	public void setNeoId(String neoId)
	{
		this.neoId = neoId;
	}
}
