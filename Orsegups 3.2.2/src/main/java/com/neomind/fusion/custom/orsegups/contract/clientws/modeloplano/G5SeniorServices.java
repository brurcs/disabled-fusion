/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano;

public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPortAddress();

    public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort() throws javax.xml.rpc.ServiceException;

    public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
