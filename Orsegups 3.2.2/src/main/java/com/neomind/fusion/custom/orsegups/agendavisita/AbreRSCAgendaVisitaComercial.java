package com.neomind.fusion.custom.orsegups.agendavisita;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;

//TODO RSC - Se algum dia este fluxo for reativado, utilizar a classe RSCHelper para abrir as solicitacoes 
/**
 * Fluxo fora de uso por ordem do Sr. Gilson.
 * @author danilo.silva
 *
 */
@Deprecated
public class AbreRSCAgendaVisitaComercial implements AdapterInterface
{
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{

		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		Boolean isEletronica = NeoUtils.safeBoolean(wrapper.findValue("isEletronica")); 

		String claccu = null;
		
		if (isEletronica){
			claccu = NeoUtils.safeOutputString(wrapper.findValue("clienteLotacaoNiveisEletronica.nivel4.claccu"));
		}else{
			claccu = NeoUtils.safeOutputString(wrapper.findValue("clienteLotacaoNiveis.nivel4.codnivel"));
			if (claccu != null){
				claccu = claccu.replace(".", "");
			}
		}
		
		Long codcli = 0L;
		int codccu = 0;
		String categoriaRSC = "";//vincular e-form principal
		
		//claccu = OrsegupsVisitaUtils.classificacaoCentroCusto(ccuLvl4);
		codccu = OrsegupsVisitaUtils.codigoCentroDeCusto(claccu,"8");
		codcli = OrsegupsVisitaUtils.buscaCodigoCliente(codccu);
		//informações do cliente
		ExternalEntityInfo infoClienteSapiens = (ExternalEntityInfo)EntityRegister.getInstance().getCache().getByString("SAPIENS_Clientes");
		QLEqualsFilter filterCodCli = new QLEqualsFilter("codcli", codcli);
		NeoObject noClienteSapiens = (NeoObject) PersistEngine.getObject(infoClienteSapiens.getEntityClass(), filterCodCli);
		
		//Origem RSC
		InstantiableEntityInfo infoOrigem = AdapterUtils.getInstantiableEntityInfo("RRCOrigem");
		QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", "15");
		
		//Categoria RSC - 04 = Diversos
		InstantiableEntityInfo infoCategoria = AdapterUtils.getInstantiableEntityInfo("RSCCategorias");
		QLEqualsFilter filterCategoria = new QLEqualsFilter("codigo", "04");
		
		//Cidade Cliente Sapiens
		EntityWrapper clienteSapiensWrapper = new EntityWrapper(noClienteSapiens);
		
		//Descrição do processo
		InstantiableEntityInfo infoRSC = AdapterUtils.getInstantiableEntityInfo("RSCRelatorioSolicitacaoClienteNovo");
		NeoObject noRSC = infoRSC.createNewInstance();
		EntityWrapper rscWrapper = new EntityWrapper(noRSC);
		
		
		StringBuilder descricaoRSC = new StringBuilder();
		
		descricaoRSC.append(" Aberto via processo C036 - Relatar Visita Cliente, tarefa : "+origin.getCode()+" 	");
		descricaoRSC.append(" Usuario da visita : "+  NeoUtils.safeOutputString(wrapper.findValue("usuarioGerenteComercial")) +" - ");
		descricaoRSC.append("" + NeoUtils.safeBoolean(wrapper.findValue("clienteAlarme")) + "");
		descricaoRSC.append(" É Cliente de: [ ");
		descricaoRSC.append(", CFTV? " + (NeoUtils.safeBoolean(wrapper.findValue("clienteCFTV")) ? "Sim": "Não") + "</li>") ;
		descricaoRSC.append(", Rastreamento? " + (NeoUtils.safeBoolean(wrapper.findValue("clienteRastreamento")) ? "Sim": "Não") + "</li>");
		descricaoRSC.append(", Portaria? " + (NeoUtils.safeBoolean(wrapper.findValue("clientePortaria")) ? "Sim": "Não") + "</li>");
		descricaoRSC.append(", Vigilância? " + (NeoUtils.safeBoolean(wrapper.findValue("clienteVigilancia")) ? "Sim": "Não") + "</li>");
		descricaoRSC.append(", Asseio? " + (NeoUtils.safeBoolean(wrapper.findValue("clienteAsseio")) ? "Sim": "Não") + "</li>");
		descricaoRSC.append("] ");
		descricaoRSC.append(" Relato Geral Visita: " + wrapper.findValue("relatoGeral"));
		descricaoRSC.append(" Descrição RSC: " + NeoUtils.safeOutputString(wrapper.findValue("descricao")) );
		 
		GregorianCalendar dataAtual = new GregorianCalendar();
		rscWrapper.findField("descricao").setValue(descricaoRSC.toString());
		rscWrapper.findField("clienteSapiens").setValue(noClienteSapiens);
		//rscWrapper.findField("cidade").setValue((String) clienteSapiensWrapper.findValue("cidcli"));
		rscWrapper.findField("contato").setValue(wrapper.findValue("contatoCliente"));
		rscWrapper.findField("email").setValue(wrapper.findValue("email"));
		rscWrapper.findField("telefones").setValue(wrapper.findValue("telefone"));
		rscWrapper.findField("origem").setValue((NeoObject) PersistEngine.getObject(infoOrigem.getEntityClass(), filterOrigem));
		//rscWrapper.findField("categoriasRsc").setValue((NeoObject) PersistEngine.getObject(infoCategoria.getEntityClass(), filterCategoria));
		//rscWrapper.findField("responsavelRegistro").setValue(origin.getUser());
		
		//rscWrapper.findField("dataRegistro").setValue(dataAtual);
		PersistEngine.persist(noRSC);

		QLEqualsFilter equal = new QLEqualsFilter("Name", "C027 - RSC - Relatório de Solicitação de Cliente");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "gilsoncesar"));
		
		//Busca o executor
		NeoPaper papel = new NeoPaper();
		String executor = "";
		NeoUser neoExecutor = null;
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "SITERESPONSAVELRSC"));
		papel = (NeoPaper) obj;

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				neoExecutor = user;
				executor = user.getCode();
				break;
			}

		}
		
		
		final WFProcess processo = WorkflowService.startProcess(processModel, noRSC, false, solicitante);
		
		System.out.println("Abertura de RSC via C036 -> RSC: " + processo.getCode());
		try
		{
			new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(neoExecutor,false);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
		
		
		
		
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
