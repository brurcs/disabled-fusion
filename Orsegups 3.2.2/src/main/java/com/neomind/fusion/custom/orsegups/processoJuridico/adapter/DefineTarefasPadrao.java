package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoRole;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.DefineTarefasPadrao

public class DefineTarefasPadrao implements AdapterInterface {
    @Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try {
		    
            	    NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
            	    if (tipoProcesso != null) {
            		EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
            		System.out.println(wTipoProcesso.getValue("codigo"));
            		
            		switch(Integer.parseInt(wTipoProcesso.getValue("codigo").toString())) {
            		
            		case 1:
            		    processoTrabalhista(origin, processEntity, activity);
            		    break;
            		case 2:
            		    processoCivel(origin, processEntity, activity);
            		    break;
            		case 3:
            		    processoTributario(origin, processEntity, activity);	
            		    break;
            		}
            		
            		if (Integer.parseInt(wTipoProcesso.getValue("codigo").toString()) == 2) {
            
            		}
            	    } else {
            		processoTrabalhista(origin, processEntity, activity);
            	    }
		
        	    	
        		
		}catch (Exception e){
		    System.out.println("Erro na classe DefineTarefasPadrao do fluxo J002.");
		    e.printStackTrace();
		    throw new WorkflowException(e.getMessage());
		}
	}

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

    }
    
    public void processoTrabalhista(Task origin, EntityWrapper processEntity, Activity activity) throws Exception{
	
	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
        
	List<NeoObject> lisPed = (List<NeoObject>) processEntity.findValue("lisPed");
	String tarefaProcesso = "";
	String codCcu = "";
	tarefaProcesso = validaNumeroProcesso(String.valueOf(processEntity.getValue("numPro")), new Integer("1"),activity);

	if (!tarefaProcesso.equals(""))
	{
		throw new WorkflowException("O processo " + processEntity.getValue("numPro") + " já esta cadastrado na tarefa " + tarefaProcesso);
	}

	for (NeoObject pedido : lisPed)
	{
		EntityWrapper wPedido = new EntityWrapper(pedido);
		List<NeoObject> lisTar = new ArrayList<NeoObject>();
		NeoObject tipPed = (NeoObject) wPedido.findValue("tipoPedido");
		EntityWrapper tipoPedido = new EntityWrapper(tipPed);
		String descricaoTipo = String.valueOf(tipoPedido.findValue("descricao"));

		for (int i = 0; i < 5; i++)
		{
			InstantiableEntityInfo insTarefa = AdapterUtils.getInstantiableEntityInfo("j002LisTar");
			NeoObject objTarefa = insTarefa.createNewInstance();
			EntityWrapper wTarefa = new EntityWrapper(objTarefa);
			NeoObject colaborador = null;
			NeoObject autorNaoColaborador = null;
			String cpfAut = "";
			String nomAut = "";
			

			if (processEntity.getValue("colaborador") != null)
			{
				colaborador = (NeoObject) processEntity.getValue("colaborador");
				EntityWrapper wColaborador = new EntityWrapper(colaborador);
				colaborador = (NeoObject) processEntity.getValue("colaborador");
				wTarefa.setValue("descricao", "");
				wTarefa.setValue("titulo", "J002 - " + activity.getCode() + " - " + wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun") + " - " + descricaoTipo);
				codCcu = String.valueOf(wColaborador.getValue("codccu"));        					
			}
			else
			{
				autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
				EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);
				cpfAut = String.valueOf(wAutor.getValue("cpfAut"));
				nomAut = String.valueOf(wAutor.getValue("nomAut"));
				wTarefa.setValue("descricao", cpfAut + " / " + nomAut);
				wTarefa.setValue("titulo", descricaoTipo);
				codCcu = String.valueOf(wAutor.getValue("codCcu"));

			}
			
			
			PersistEngine.persist(objTarefa);
			lisTar.add(objTarefa);
		}
		wPedido.setValue("lisTar", lisTar);
	}
	if(!codCcu.equals("")) {
	    processEntity.setValue("codccuInformado", Long.parseLong(codCcu));
	}
	processEntity.setValue("grauProcesso", "1º Grau"); 

	//Criando pedido Obrigatório.
	InstantiableEntityInfo insPedObr = AdapterUtils.getInstantiableEntityInfo("j002LisPed");
	NeoObject objPedObr = insPedObr.createNewInstance();
	EntityWrapper wPedObr = new EntityWrapper(objPedObr);
	//Criando tarefa para o pedido Obr.
	InstantiableEntityInfo insTarefa = AdapterUtils.getInstantiableEntityInfo("j002LisTar");
	NeoObject objTarefaObr = insTarefa.createNewInstance();
	EntityWrapper wTarefa = new EntityWrapper(objTarefaObr);
	//variaveis para o titulo.
	NeoObject colaborador = null;
	NeoObject autorNaoColaborador = null;
	//Montando o titulo.
	wTarefa.setValue("descricao", "Providenciar os seguintes Documentos Essenciais: ADMISSÃO/DEMISSÃO/CARTÃO PONTO/FICHA FINANCEIRA/FÉRIAS/PUNIÇÕES");

	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	QLFilter tipoPedido = new QLEqualsFilter("descricao", "Documentos Essenciais");
	groupFilter.addFilter(tipoPedido);
	List<NeoObject> objsTipoPedido = PersistEngine.getObjects(AdapterUtils.getEntityClass("j002TipoPedido"), groupFilter);
	NeoObject tipoPedidoObr = null;
	EntityWrapper wTipoPedidoObr = null;
	if (objsTipoPedido != null && objsTipoPedido.size() > 0)
	{
		tipoPedidoObr = objsTipoPedido.get(0);
		wTipoPedidoObr = new EntityWrapper(tipoPedidoObr);
	}
	else
	{
		InstantiableEntityInfo insTipoPedido = AdapterUtils.getInstantiableEntityInfo("j002TipoPedido");
		tipoPedidoObr = insTipoPedido.createNewInstance();
		wTipoPedidoObr = new EntityWrapper(tipoPedidoObr);
		wTipoPedidoObr.setValue("descricao", "Documentos Essenciais");
		PersistEngine.persist(tipoPedidoObr);
	}
	wPedObr.setValue("tipoPedido", wTipoPedidoObr.getObject());

	if (processEntity.getValue("colaborador") != null)
	{
		colaborador = (NeoObject) processEntity.getValue("colaborador");
		EntityWrapper wColaborador = new EntityWrapper(colaborador);
		colaborador = (NeoObject) processEntity.getValue("colaborador");
		wTarefa.setValue("titulo", "J002 - " + activity.getCode() + " - " + wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun") + " - Documentos Essenciais - ADM/DEM/CARÃO PONTO/FICHA FINANCEIRA/RESCISÃO/PUNIÇÕES.");
	}

	else
	{
		autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
		EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);
		String cpfAut = "";
		String nomAut = "";
		cpfAut = String.valueOf(wAutor.getValue("cpfAut"));
		nomAut = String.valueOf(wAutor.getValue("nomAut"));
		wTarefa.setValue("titulo", cpfAut + " - " + nomAut + " - Documentos Obrigatórios.");

	}
	List<NeoObject> lisTarObr = new ArrayList<NeoObject>();
	PersistEngine.persist(objTarefaObr);
	lisTarObr.add(objTarefaObr);
	//Add tarefa no pedido Obr.
	wPedObr.setValue("lisTar", lisTarObr);

	//Add pedido Obr na lista de pedidos.
	PersistEngine.persist(objPedObr);
	lisPed.add(objPedObr);
	//Atualizando Lista de pedidos do e-form principal
	processEntity.setValue("lisPed", lisPed);

	//Montando titulo da tarefa
	String titulo = "";
	if (processEntity.getValue("isColaborador").equals(Boolean.TRUE))
	{
		colaborador = null;
		colaborador = (NeoObject) processEntity.getValue("colaborador");
		EntityWrapper wColaborador = new EntityWrapper(colaborador);
		colaborador = (NeoObject) processEntity.getValue("colaborador");
		titulo = wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun");
	}
	else
	{
		autorNaoColaborador = null;
		autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
		EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);

		titulo = wAutor.getValue("nomAut") + " - " + wAutor.getValue("cpfAut") + " - " + wAutor.getValue("rgAut");
	}

	processEntity.setValue("tituloTarefa", titulo);

	//Historico de arquivos
	List<NeoObject> arquivos = (List<NeoObject>) processEntity.getValue("j002LisArq");
	List<NeoObject> registroAtividadeArquivo = (List<NeoObject>) processEntity.getValue("j002RegistroArquivo");
	if (arquivos != null)
	{
		for (NeoObject arquivo : arquivos)
		{
			EntityWrapper wArquivo = new EntityWrapper(arquivo);
			InstantiableEntityInfo insRegArq = AdapterUtils.getInstantiableEntityInfo("j002RegistroArquivos");
			NeoObject objReArq = insRegArq.createNewInstance();
			EntityWrapper wRegArq = new EntityWrapper(objReArq);

			wRegArq.setValue("dtAnexo", new GregorianCalendar());
			wRegArq.setValue("titulo", String.valueOf(wArquivo.getValue("titulo")));
			wRegArq.setValue("respAnexo", origin.returnResponsible());
			wRegArq.setValue("anexo", (NeoFile) wArquivo.getValue("arquivo"));

			PersistEngine.persist(objReArq);

			registroAtividadeArquivo.add(objReArq);
		}
	}
	processEntity.setValue("j002RegistroArquivo", registroAtividadeArquivo);
	processEntity.setValue("j002LisArq", null);
	
	
	
	Connection conn = PersistEngine.getConnection("SAPIENS");
	StringBuilder sql = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try{
	    String codccu = "";
	    if(colaborador != null || processEntity.getValue("autorNaoCol") != null){
		EntityWrapper wColaborador = null;
		if(colaborador != null){
		    wColaborador = new EntityWrapper(colaborador);				    
		    codccu = String.valueOf(wColaborador.getValue("codccu"));
		}else{
		    wColaborador = new EntityWrapper((NeoObject)processEntity.getValue("autorNaoCol"));
		    if(wColaborador.getValue("codCcu") != null){
			codccu = String.valueOf(wColaborador.getValue("codCcu"));
		    }				    
		}
		if(codccu != null && !codccu.trim().equals("")){
		    sql.append(" select top 1 cc.usu_tipneg from E044CCU cc where cc.CodCcu = ? ");
		    pstm = conn.prepareStatement(sql.toString());
		    
		    pstm.setString(1, codccu);
		    rs = pstm.executeQuery();
		    HashMap<Long, String> map = new HashMap<>();
		    map.put(1l, "Eletrônica");
		    map.put(2l, "Humana");
		    map.put(3l, "Asseio");
		    map.put(4l, "Gerais");
		    map.put(5l, "Humana Asseio");
		    map.put(9l, "Desconsiderar");
		    while (rs.next()){
			Long tipNeg = rs.getLong("usu_tipneg");
			if (tipNeg != null){
			    processEntity.setValue("negocio", map.get(tipNeg));
			}
		    }
		}
	    }
	}catch (Exception e){
	    e.printStackTrace();
	    throw new WorkflowException(e.getMessage());
	}finally{
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
    }
    
    public void processoCivel(Task origin, EntityWrapper processEntity, Activity activity) throws Exception{
	
    }
    
    public void processoTributario(Task origin, EntityWrapper processEntity, Activity activity) throws Exception{

	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
        
	List<NeoObject> lisPed = (List<NeoObject>) processEntity.findValue("lisPed");
	String tarefaProcesso = "";
	String codCcu = "";
	tarefaProcesso = validaNumeroProcesso(String.valueOf(processEntity.getValue("numPro")), new Integer("3"),activity);

	if (!tarefaProcesso.equals(""))
	{
		throw new WorkflowException("O processo " + processEntity.getValue("numPro") + " já esta cadastrado na tarefa " + tarefaProcesso);
	}

	for (NeoObject pedido : lisPed)
	{
		EntityWrapper wPedido = new EntityWrapper(pedido);
		List<NeoObject> lisTar = new ArrayList<NeoObject>();
		NeoObject tipPed = (NeoObject) wPedido.findValue("tipoPedido");
		EntityWrapper tipoPedido = new EntityWrapper(tipPed);
		String descricaoTipo = String.valueOf(tipoPedido.findValue("descricao"));

		for (int i = 0; i < 5; i++)
		{
			InstantiableEntityInfo insTarefa = AdapterUtils.getInstantiableEntityInfo("j002LisTar");
			NeoObject objTarefa = insTarefa.createNewInstance();
			EntityWrapper wTarefa = new EntityWrapper(objTarefa);
			
			// Empresas Autoras
			//if (((Long) processEntity.getValue("nossasEmpresasAutoras")) == 1L) { 
			    
   		        
			// Empresas Rés
			//} else { 
			    
			    
			//}		
			
			PersistEngine.persist(objTarefa);
			lisTar.add(objTarefa);
		}
		wPedido.setValue("lisTar", lisTar);
	}
	if(!codCcu.equals("")) {
	    processEntity.setValue("codccuInformado", Long.parseLong(codCcu));
	}
	processEntity.setValue("grauProcesso", "1º Grau"); 
	
	boolean isTributario = false;		
	NeoObject tipoProcesso = (NeoObject) processEntity.findGenericValue("tipoProcesso");
	if (tipoProcesso != null) {
	    EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
	    isTributario = (boolean) (Integer.parseInt(wTipoProcesso.getValue("codigo").toString()) == 3) ? true : false;
	    if (isTributario) { 
		NeoRole papelEscritorioCivel = (NeoRole) PersistEngine.getObject(NeoRole.class, new QLEqualsFilter("code", "j002Civel"));
		NeoRole j002GuedesPintoConAssCivel = (NeoRole) PersistEngine.getObject(NeoRole.class, new QLEqualsFilter("code", "j002GuedesPintoConAssCivel "));
		processEntity.setValue("papelRaiaEscRes", papelEscritorioCivel);
		processEntity.setValue("papelRaiaPoolJurEsc", j002GuedesPintoConAssCivel);
		processEntity.setValue("temPgo", true);
	    }
	}

	//Criando pedido Obrigatório.
	InstantiableEntityInfo insPedObr = AdapterUtils.getInstantiableEntityInfo("j002LisPed");
	NeoObject objPedObr = insPedObr.createNewInstance();
	EntityWrapper wPedObr = new EntityWrapper(objPedObr);
	//Criando tarefa para o pedido Obr.
	InstantiableEntityInfo insTarefa = AdapterUtils.getInstantiableEntityInfo("j002LisTar");
	NeoObject objTarefaObr = insTarefa.createNewInstance();
	EntityWrapper wTarefa = new EntityWrapper(objTarefaObr);
	//variaveis para o titulo.
	
	//Montando o titulo.
	wTarefa.setValue("descricao", "Providenciar os seguintes Documentos Essenciais: VERIFICAR QUAIS SÃO ESTES DOCUMENTOS PARA O TRIBUTARIO");

	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	QLFilter tipoPedido = new QLEqualsFilter("descricao", "Documentos Essenciais");
	groupFilter.addFilter(tipoPedido);
	List<NeoObject> objsTipoPedido = PersistEngine.getObjects(AdapterUtils.getEntityClass("j002TipoPedido"), groupFilter);
	NeoObject tipoPedidoObr = null;
	EntityWrapper wTipoPedidoObr = null;
	if (objsTipoPedido != null && objsTipoPedido.size() > 0)
	{
		tipoPedidoObr = objsTipoPedido.get(0);
		wTipoPedidoObr = new EntityWrapper(tipoPedidoObr);
	}
	else
	{
		InstantiableEntityInfo insTipoPedido = AdapterUtils.getInstantiableEntityInfo("j002TipoPedido");
		tipoPedidoObr = insTipoPedido.createNewInstance();
		wTipoPedidoObr = new EntityWrapper(tipoPedidoObr);
		wTipoPedidoObr.setValue("descricao", "Documentos Essenciais");
		PersistEngine.persist(tipoPedidoObr);
	}
	wPedObr.setValue("tipoPedido", wTipoPedidoObr.getObject());

	
	List<NeoObject> lisTarObr = new ArrayList<NeoObject>();
	PersistEngine.persist(objTarefaObr);
	lisTarObr.add(objTarefaObr);
	//Add tarefa no pedido Obr.
	wPedObr.setValue("lisTar", lisTarObr);

	//Add pedido Obr na lista de pedidos.
	PersistEngine.persist(objPedObr);
	lisPed.add(objPedObr);
	//Atualizando Lista de pedidos do e-form principal
	processEntity.setValue("lisPed", lisPed);
	
    }

    public String validaNumeroProcesso(String numPro, Integer tipoProcesso , Activity activity) throws Exception {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	conn = PersistEngine.getConnection("");
	StringBuilder sql = new StringBuilder();
	String retorno = "";
	try {
	    sql.append(" Select top 1 w.code from d_j002Principal pr ");
	    sql.append(" inner join WFProcess w on w.neoId = pr.wfprocess_neoId ");
	    sql.append(" inner join d_j002TipoProcesso tp on tp.neoId = pr.tipoProcesso_neoId ");
	    sql.append(" where ((pr.numPro = ? and w.processState <> 2 and w.code <> ? and (tp.codigo = ? or pr.tipoProcesso_neoId is null))) ");
	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setString(1, numPro);
	    pstm.setString(2, activity.getCode());
	    pstm.setInt(3,tipoProcesso);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		retorno = rs.getString("code");
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    throw new Exception(e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return retorno;
    }
}
