package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ChipGPRSNivelSuperior implements AdapterInterface {
	
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		NeoPaper responsavelExecutor = null;
		
		if (origin.getActivityName().equalsIgnoreCase("Solicitar Chip GPRS")) {
			SecurityEntity executor = (SecurityEntity) processEntity.findField("executor").getValue();
			
			if(executor instanceof NeoUser){
				String usuario = executor.getCode();
				NeoUser objExecutor = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code",usuario));
				NeoPaper papelGroup = objExecutor.getGroup().getResponsible(); 
				
				if (objExecutor.getPapers().contains(papelGroup))
				{
					responsavelExecutor = objExecutor.getGroup().getUpperLevel().getResponsible();
				}
				else
				{
					responsavelExecutor = objExecutor.getGroup().getResponsible();
				}
			}
			else if(executor instanceof NeoPaper){
				if (executor.getCode().startsWith("GPRS")) {
					NeoPaper objPapel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",executor.getCode()));
					for (NeoUser user : objPapel.getUsers() )
					{
						if(NeoUtils.safeIsNotNull(user))
						{
							NeoPaper papelGroup = user.getGroup().getResponsible(); 
							if (user.getPapers().contains(papelGroup))
							{
								responsavelExecutor = user.getGroup().getUpperLevel().getResponsible();
							}
							else
							{
								responsavelExecutor = user.getGroup().getResponsible();
							}
							break;
						}
						else 
						{
							String erro = "Papel "+ objPapel.getName() +" sem usuário";
							throw new WorkflowException(erro);
						}
					}
				} 
				else 
				{
					throw new WorkflowException("Selecione o papel correto para o fluxo.");
				}
				
			}
			else {
				throw new WorkflowException("Selecione um usuário ou papel como executor.");
			}
		} 
		else if (origin.getActivityName().equalsIgnoreCase("Cadastrar Chip GPRS - Tarefa Escalada")) {
			NeoPaper resplExecutor = (NeoPaper) processEntity.findField("responsavelExecutor").getValue();
			NeoPaper objPapel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", resplExecutor.getCode()));
			for (NeoUser user : objPapel.getUsers() )
			{
				if(NeoUtils.safeIsNotNull(user))
				{  
					responsavelExecutor = user.getGroup().getUpperLevel().getResponsible();
					break;
				}
				else 
				{
					String erro = "Papel "+ objPapel.getName() +" sem usuário";
					throw new WorkflowException(erro);
				}
			}
		}
		
		processEntity.findField("responsavelExecutor").setValue(responsavelExecutor);
	}
	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}
