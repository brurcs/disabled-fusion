package com.neomind.fusion.custom.orsegups.mobile.vo2;

public class PerguntaVO {
	
	private String neoId;
	private String txtPergunta;

	public String getTxtPergunta() {
		return txtPergunta;
	}

	public void setTxtPergunta(String txtPergunta) {
		this.txtPergunta = txtPergunta;
	}

	public String getNeoId() {
		return neoId;
	}

	public void setNeoId(String neoId) {
		this.neoId = neoId;
	}
	
	
}
