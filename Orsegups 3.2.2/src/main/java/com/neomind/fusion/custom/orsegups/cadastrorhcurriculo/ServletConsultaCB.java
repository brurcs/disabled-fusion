package com.neomind.fusion.custom.orsegups.cadastrorhcurriculo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;

@WebServlet(name="ServletConsultaCB", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.cadastrorhcurriculo.ServletConsultaCB"})
public class ServletConsultaCB extends HttpServlet
{
	private static final Log log = LogFactory.getLog(ServletConsultaCB.class);
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		final PrintWriter out = response.getWriter();
		// dados do formulário
		String type = (String) request.getParameter("type");

		String neoId = (String) request.getParameter("value");

		if (neoId != null)
		{
			if (type.equalsIgnoreCase("cidade"))
			{
				InstantiableEntityInfo infoCidade = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("VETORH_R074CID");
				NeoObject objCidade = (NeoObject) PersistEngine.getObject(infoCidade.getEntityClass(), new QLEqualsFilter("neoId", new Long(neoId)));
				if (objCidade != null)
				{
					out.print("true");
					return;
				}
				else
				{
					out.print("false");
					return;
				}
			}
			else
			{
				InstantiableEntityInfo infoBairro = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("VETORHRBAI");
				NeoObject objBairro = (NeoObject) PersistEngine.getObject(infoBairro.getEntityClass(), new QLEqualsFilter("neoId", new Long(neoId)));
				if (objBairro != null)
				{
					out.print("true");
					return;
				}
				else
				{
					out.print("false");
					return;
				}
			}
		}
		else
		{
			out.print("false");
			return;
		}
	}
}