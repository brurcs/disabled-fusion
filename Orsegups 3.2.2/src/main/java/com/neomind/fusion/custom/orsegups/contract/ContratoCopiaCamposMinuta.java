package com.neomind.fusion.custom.orsegups.contract;

import java.nio.charset.CodingErrorAction;
import java.util.Collection;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.CadastraCentroCusto;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoCopiaCamposMinuta implements AdapterInterface
{
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		
		long movimentacao 	= (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		long alteracao 		= 0;
		if(movimentacao == 5)
			alteracao 		= (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");
		
		String numeroctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		if(numeroctr == null || numeroctr.equals("") || alteracao == 1)
			numeroctr = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		boolean adereProtecaoG = NeoUtils.safeBoolean(wrapper.findValue("adereProtecaoGarantida"));
		NeoObject postoNormal = null;
		
		// fernanda martins solicitou que fosse exigido pelo menos 2 contatos no posto
		Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			
			
			
			/*//preenche os vinculos de possíveis vinculos de conta sigma. Isso foi feito para conciliar partições 001 e 099 habilitando o controle de partição
			String codcliSapiens = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.codigoCliente"));
			if (codcliSapiens.equals("")){
				codcliSapiens = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codcli"));
			}
			String vinculos = ContratoSigmaUtils.retornaCdsClientLigacaoSapiens(codcliSapiens);
			ContratoLogUtils.logInfo("Codigo Cliente - " + codcliSapiens + ", vinculos -> " + vinculos);
			if (vinculos != null && !vinculos.equals("")){
				ContratoLogUtils.logInfo("vinculado");
				wPosto.setValue("vinculoContaSigmaFiltro",vinculos);
			}*/
			NeoObject oTipoPosto = (NeoObject) wPosto.findValue("tipoPosto");
			EntityWrapper wTipoPosto = new EntityWrapper(oTipoPosto);
			if ( !NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("2") ){ // posto normal
				boolean origemSapiens = NeoUtils.safeBoolean(wPosto.findValue("origemSapiens"));
				if(!origemSapiens && (alteracao == 4 || alteracao == 9 || alteracao == 6))
				{
					/*
					 * se for uma alteração de redução de equipamentos 
					 * ou uma REDUÇÃO VALOR CONTRATO
					 * ou uma REINSTALAÇÃO ELETRÔNICA/CFTV
					 * 
					 * não pode existir postos novos
					 */
					throw new WorkflowException("Não é permitido a criação de novos postos, para o tipo de alteração de contrato selecionado.");
				}
				
				if(origemSapiens && (alteracao == 4 || alteracao == 9))
				{
					Collection<NeoObject> equipamentos = wPosto.findField("equipamentosItens.equipamentosPosto").getValues();
					for(NeoObject equip : equipamentos)
					{
						EntityWrapper wEquip = new EntityWrapper(equip);
						String codigoInstalacao = NeoUtils.safeOutputString(wEquip.findValue("tipoInstalacao.codigo"));
						
						if(alteracao == 4 && !"X".equalsIgnoreCase(codigoInstalacao))
						{
							throw new WorkflowException("Na alteração de contratos REDUÇÃO DE EQUIP. ELETRONICA E CFTV, só é permitido insersão de quipamentos com o tipo de Instalação X - Devolução de Equipamento para Estoque.");
						}
						/* desligado conforme solicitado do Sr. Giliard em reunião no dia 22/05/2014
						if(alteracao == 9 && !"Y".equalsIgnoreCase(codigoInstalacao) && !"R".equalsIgnoreCase(codigoInstalacao))
						{
							throw new WorkflowException("Na alteração de contratos REINSTALAÇÃO ELETRÔNICA/CFTV, só é permitido insersão de quipamentos com os tipos de Instalações Y - Reinstalação Equipamentos do Cliente ou R - Reinstalação de Equipamentos Orsegups.");
						}
						*/
					}
				}
				
				//Collection<NeoObject> equipamentos = wPosto.findField("equipamentosItens.equipamentosPosto").getValues();
				String obsOs = "";
				if (wPosto.findValue("equipamentosItens.equipamentosPosto") != null){
					Collection<NeoObject> equipamentos = (Collection<NeoObject>) wPosto.findValue("equipamentosItens.equipamentosPosto");
					obsOs = geraDescricaoOS(numeroctr, movimentacao, alteracao, equipamentos);
					System.out.println("[FLUXO CONTRATOS]-observação da OS gerada: " +obsOs);
				}
				
				String placa = wPosto.findGenericValue("placa");
				placa = placa == null ? "" : placa;
				obsOs += " - " + placa + " INSTALAÇÃO POR CONTA";
				
				ContratoLogUtils.logInfo("[OBSOS] Contrato: " + numeroctr + ", OS: " + obsOs);
				wPosto.setValue("observacaoOS", obsOs);

				
				Collection<NeoObject> contatos = null;
				if (wPosto.findValue("vigilanciaEletronica.listaContatosPosto") != null){
					contatos = wPosto.findField("vigilanciaEletronica.listaContatosPosto").getValues();
				}
				
				if ((contatos == null) || (contatos != null && contatos.size() < 2) ){
					throw new WorkflowException("Erro: Para cada posto, deve haver pelo menos 2 contatos.");
				}
			}else{ // posto de proteção garantida
				// ainda não há validações para proteção garantida
			}
			
				
			// copia os dados do posto normal para o posto de proteção garantida
			if (NeoUtils.safeLong(NeoUtils.safeOutputString(wPosto.findValue("tipoPosto.codigo"))).equals(1L) ){
				postoNormal = posto;
			}else if (NeoUtils.safeLong(NeoUtils.safeOutputString(wPosto.findValue("tipoPosto.codigo"))).equals(2L) ){
				if (postoNormal != null){
					ProtecaoGarantidaUtils.atualizaPostoProtecaoGarantida(postoNormal, posto);
				}
				
			}else{
				postoNormal = posto;
			}
			
		}
		
		if(movimentacao != 5)
		{
			//popula os CC`s dos postos novos
			populaCCPostos(wrapper, false);
			
		}else
		{
			//alteração, mas podem existir postos novos
			populaCCPostos(wrapper, true);
		}
		
		setaContaFinanceiraPadrao(wrapper); // seta a conta financeira como sendo a 130
		setaContaContabilPadrao(wrapper); // seta a conta contabil como sendo a 4060
		preencheRegionalContrato(wrapper); // preenche a regional do contrato com a regional do representante
		setaInstaladorPadrao(wrapper);// seta instalador padrao para as listas de kit e equipamentos
		
		/*
		NeoObject cliente = (NeoObject) wrapper.findValue("buscaNomeCliente");
		EntityWrapper wCliente = new EntityWrapper(cliente);
		
		String cep =  NeoUtils.safeOutputString(wCliente.findValue("cepcli"));
		String endereco =  NeoUtils.safeOutputString(wCliente.findValue("endcli"));
		String complemento =  NeoUtils.safeOutputString(wCliente.findValue("cplend"));
		String bairro =  NeoUtils.safeOutputString(wCliente.findValue("baicli"));
		String cidade =  NeoUtils.safeOutputString(wCliente.findValue("cidcli"));
		String estado =  NeoUtils.safeOutputString(wCliente.findValue("sigufs"));
		NeoObject enderecoCliente = getEndereco(cep, endereco, complemento, bairro, cidade, estado);
		
		//pegar o tipo de movimentação
		
		if(movimentacao <= 4)
		{
			//contrato novo
			if(movimentacao == 1 || movimentacao == 3)
			{
				//eletronica
				NeoObject contratoNovoEletronica = AdapterUtils.createNewEntityInstance("FGCcontrato");
				EntityWrapper wContratoNovoEletronica = new EntityWrapper(contratoNovoEletronica);
				
				Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
				Collection<NeoObject> equipamentosContratoNovo = new ArrayList<NeoObject>();
				NeoObject enderecoPosto = null;
				int numPostos = 0;
				Collection<NeoObject> listaContatosPosto = new ArrayList<NeoObject>();
				for(NeoObject posto : postos)
				{
					EntityWrapper wPosto = new EntityWrapper(posto);
					
					wPosto.setValue("qtdeDeFuncionariosPrevistos", wPosto.findValue("qtdeDeFuncionarios"));
					wPosto.setValue("precoUnitarioPostoConferencia", wPosto.findValue("precoUnitarioPosto"));
					if(!NeoUtils.safeBoolean("origemSapiens"))
					{
						String code = PortalUtil.getCurrentUser().getCode();
						QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", code);
						@SuppressWarnings("unchecked")
						ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), loginFilter);
						if(sapiensUsers.size() > 0)
							wPosto.setValue("representante", sapiensUsers.get(0));
					}
					
					if(wPosto.findValue("enderecoEfetivo") != null)
						enderecoPosto = (NeoObject) wPosto.findValue("enderecoEfetivo");
					if(wPosto.findValue("equipamentosItens") != null)
					{
						Collection<NeoObject> equipamentos = wPosto.findField("equipamentosItens.equipamentosPosto").getValues();
						for(NeoObject equipamento : equipamentos)
						{
							EntityWrapper wEquipamento = new EntityWrapper(equipamento);
							NeoObject produto = (NeoObject) wEquipamento.findValue("produto");
							long qtd = (Long) wEquipamento.findValue("quantidade");
							
							NeoObject equipamentoContratoNovo = getEquipamentoContratoNovo(produto, NeoUtils.safeOutputString(qtd));
							equipamentosContratoNovo.add(equipamentoContratoNovo);
						}
					}
					
					listaContatosPosto.addAll(wPosto.findField("vigilanciaEletronica.listaContatosPosto").getValues());
					
					//pega todos os postos que não foram desativados
					if(!NeoUtils.safeBoolean(wPosto.findValue("duplicarDesativar")))
					{
						numPostos++;
					}
				}
				
				wContratoNovoEletronica.setValue("clausulaSegundaLista", equipamentosContratoNovo);
				
				NeoObject anexoI = AdapterUtils.createNewEntityInstance("FGCAnexoI");
				EntityWrapper wAnexo = new EntityWrapper(anexoI);
				wAnexo.setValue("enderecoInstalacao", enderecoPosto);
				wAnexo.setValue("nomeCliente", NeoUtils.safeOutputString(wCliente.findValue("nomcli")));
				wAnexo.setValue("numeroPontos", numPostos);
				
				
				InstantiableEntityInfo responsaveisMonitorado = AdapterUtils.getInstantiableEntityInfo("GEDResponsaveisMonitorado");
				for(NeoObject contatoPosto : listaContatosPosto)
				{
					EntityWrapper wContato = new EntityWrapper(contatoPosto);
					
					NeoObject responsavel = responsaveisMonitorado.createNewInstance();
					EntityWrapper wResponsavel = new EntityWrapper(responsavel);
					
					wResponsavel.setValue("responsavel", NeoUtils.safeOutputString(wContato.findValue("nomecontato")));
					wResponsavel.setValue("cargoFuncao", NeoUtils.safeOutputString(wContato.findValue("funcao")));
					wResponsavel.setValue("telefone1", NeoUtils.safeOutputString(wContato.findValue("telefone1")));
					wResponsavel.setValue("celular", NeoUtils.safeOutputString(wContato.findValue("telefone2")));
					
					PersistEngine.persist(responsavel);
					
					wAnexo.findField("responsavelLocalMonitorado").addValue(responsavel);
				}
				PersistEngine.persist(anexoI);
				
				wContratoNovoEletronica.setValue("anexoI", anexoI);
				PersistEngine.persist(contratoNovoEletronica);
				
				wrapper.setValue("contratoNovoEletronica", contratoNovoEletronica);
				
			}else
			{
				//humanas
			}
		}else{
			//alteração
			long codAlteracao = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");
			if(codAlteracao == 1)
			{
				//Cessão de Direito
			}else if(codAlteracao == 5 || codAlteracao == 6 || codAlteracao == 7 || codAlteracao == 8)
			{
				//Humanas
			}else
			{
				//Eletronica
				
				
				/*
				 *  a) Razão Social/CNPJ da Contratante - Dados Iniciais
				 *  taEletronica.alteracoes.dadosContratante.dadosIniciais.razaoSocial
				 *//*
				NeoObject taEletronica = AdapterUtils.createNewEntityInstance("FGCTermoAditMonitEletronicoOutrasAven");
				EntityWrapper wTaEletronica = new EntityWrapper(taEletronica);
				
				NeoObject alteracoes = AdapterUtils.createNewEntityInstance("FGCTermoAditivoAlteracoes");
				EntityWrapper wAlteracoes = new EntityWrapper(alteracoes);
				
				NeoObject dadosContratante = AdapterUtils.createNewEntityInstance("FGCCedente");
				EntityWrapper wDadosContratante = new EntityWrapper(dadosContratante);
				
				NeoObject dadosIniciais = AdapterUtils.createNewEntityInstance("FGCContratoIniciais");
				EntityWrapper wDadosIniciais = new EntityWrapper(dadosIniciais);
				wDadosIniciais.setValue("razaoSocial", NeoUtils.safeOutputString(wCliente.findValue("nomcli")));
				wDadosIniciais.setValue("nomeFantasia", NeoUtils.safeOutputString(wCliente.findValue("apecli")));
				wDadosIniciais.setValue("cpfcnpj", NeoUtils.safeOutputString(wCliente.findValue("cgccpf")));
				wDadosIniciais.setValue("InscricaoEstadualRG", NeoUtils.safeOutputString(wCliente.findValue("insest")));
				PersistEngine.persist(dadosIniciais);
				wDadosContratante.setValue("dadosIniciais", dadosIniciais);
				
				wDadosContratante.setValue("enderecoCedente", enderecoCliente);
				
				NeoObject responsavelContrato = AdapterUtils.createNewEntityInstance("FGCResponsavelContrato");
				EntityWrapper wResponsavelContrato = new EntityWrapper(responsavelContrato);
				wResponsavelContrato.setValue("responsavel", NeoUtils.safeOutputString(wCliente.findValue("nomcli")));
				wResponsavelContrato.setValue("cpf", NeoUtils.safeOutputString(wCliente.findValue("cgccpf")));
				//validar com o cliente
				//wResponsavelContrato.setValue("dataNascimento", value);
				PersistEngine.persist(responsavelContrato);
				wDadosContratante.setValue("responsavelContrato", responsavelContrato);
				
				wAlteracoes.setValue("dadosContratante", dadosContratante);
				wAlteracoes.setValue("enderecoContratante", enderecoCliente);
				wAlteracoes.setValue("enderecoCobranca", enderecoCliente);
				
				Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
				Collection<NeoObject> equipamentosMinuta = new ArrayList<NeoObject>();
				long valorPosto = 0L;
				long valorContrato = 0L;
				int numPostos = 0;
				for(NeoObject posto : postos)
				{
					EntityWrapper wPosto = new EntityWrapper(posto);
					
					if(wPosto.findValue("equipamentosItens") != null)
					{
						Collection<NeoObject> equipamentos = wPosto.findField("equipamentosItens.equipamentosPosto").getValues();
						for(NeoObject equipamento : equipamentos)
						{
							EntityWrapper wEquipamento = new EntityWrapper(equipamento);
							NeoObject produto = (NeoObject) wEquipamento.findValue("produto");
							long qtd = (Long) wEquipamento.findValue("quantidade");
							boolean locado = false;
							boolean inclusao = false;
							
							NeoObject equipamentoMinuta = getEquipamentoMinuta(produto, NeoUtils.safeOutputString(qtd), locado, inclusao);
							equipamentosMinuta.add(equipamentoMinuta);
						}
					}
					
					//pega somente os postos novos, ou seja, as alterações
					if(!NeoUtils.safeBoolean(wPosto.findValue("origemSapiens")))
					{
						BigDecimal valor = (BigDecimal) wPosto.findValue("valorTotalPosto");
						valorPosto += valor.longValue();
					}
					
					//pega todos os postos que não foram desativados
					if(!NeoUtils.safeBoolean(wPosto.findValue("duplicarDesativar")))
					{
						BigDecimal valor = (BigDecimal) wPosto.findValue("valorTotalPosto");
						valorContrato += valor.longValue();
						numPostos++;
					}
				}
				
				
				wAlteracoes.setValue("equipamentos", equipamentosMinuta);
				
				NeoObject valoresContrato = AdapterUtils.createNewEntityInstance("FGCAlteracaoValoresContrato");
				EntityWrapper wValores = new EntityWrapper(valoresContrato);
				wValores.setValue("valorAlteracao", valorPosto);
				wValores.setValue("valorFinal", valorContrato);
				PersistEngine.persist(valoresContrato);
				
				wAlteracoes.setValue("doValorMensal", valoresContrato);
				
				NeoObject enderecoInstalacao = AdapterUtils.createNewEntityInstance("FGCEnderecoInstalacao");
				EntityWrapper wEnderecoInstalacao = new EntityWrapper(enderecoInstalacao);
				wEnderecoInstalacao.setValue("nomeCliente", NeoUtils.safeOutputString(wCliente.findValue("nomcli")));
				wEnderecoInstalacao.setValue("telefoneCliente", NeoUtils.safeOutputString(wCliente.findValue("foncli")));
				wEnderecoInstalacao.setValue("responsavelPeloLocal", NeoUtils.safeOutputString(wCliente.findValue("nomcli")));
				wEnderecoInstalacao.setValue("cpfResponsavelLocal",  NeoUtils.safeOutputString(wCliente.findValue("cgccpf")));
				wEnderecoInstalacao.setValue("telefoneResponsavelLocal",  NeoUtils.safeOutputString(wCliente.findValue("foncli")));
				wEnderecoInstalacao.setValue("endereco", enderecoCliente);
				wEnderecoInstalacao.setValue("referenciaLocalizacao", "");
				wEnderecoInstalacao.setValue("cep", cep);
				PersistEngine.persist(enderecoInstalacao);
				
				wAlteracoes.setValue("enderecoInstalacao", enderecoInstalacao);
				PersistEngine.persist(alteracoes);
				
				wTaEletronica.setValue("enderecoContratada", enderecoCliente);
				wTaEletronica.setValue("alteracoes", alteracoes);
				
				NeoObject anexo = AdapterUtils.createNewEntityInstance("FGCAnexocroqui");
				EntityWrapper wAnexo = new EntityWrapper(anexo);
				wAnexo.setValue("numpontos", numPostos);
				PersistEngine.persist(anexo);
				
				wTaEletronica.setValue("anexo", anexo);
				PersistEngine.persist(taEletronica);
				
				wrapper.setValue("taEletronica", taEletronica);
			}
		}*/
	}
	
public void setaInstaladorPadrao(EntityWrapper wrapper){
	
	List<NeoObject> listaPostosFusion = (List<NeoObject>) wrapper.findField("postosContrato").getValue();
	for (NeoObject postoFusion : listaPostosFusion)
	{
		EntityWrapper wPostoFusion = new EntityWrapper(postoFusion);
		if(wPostoFusion.findField("equipamentosItens").getValue() != null)
		{
			
			//SAPIENSUSUT190UNS //SAPIENSUSUTINS producao
			NeoObject oInstalador = null;
			try{
				oInstalador = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUTINS"),	new QLEqualsFilter("usu_codins",   201050L ));
			}catch(Exception e){
				ContratoLogUtils.logInfo("SAPIENSUSUTINS não encontrado, tentando usar SAPIENSUSUT190UNS... ");
				oInstalador = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUT190UNS"),	new QLEqualsFilter("usu_codins",   201050L ));
			}
			
			
			List<NeoObject> listaKits = (List<NeoObject>) wPostoFusion.findField("equipamentosItens.kitsPosto").getValue();
			
			if(!listaKits.isEmpty())
			{
				for (NeoObject kit : listaKits)
				{
					EntityWrapper wKit = new EntityWrapper(kit);
					//NeoObject obj  = EntityCloner.cloneNeoObject(oInstalador);
					wKit.findField("instaladores").setValue(oInstalador);
				}
			}
			
			List<NeoObject> listaEquipamentos = (List<NeoObject>) wPostoFusion.findField("equipamentosItens.equipamentosPosto").getValue();
			if(!listaEquipamentos.isEmpty())
			{
				for (NeoObject equipamento : listaEquipamentos)
				{
					EntityWrapper wEquip = new EntityWrapper(equipamento);
					//NeoObject obj  = EntityCloner.cloneNeoObject( oInstalador );
					wEquip.findField("instaladores").setValue(oInstalador);
					
				}
			}
			
		}
	}
	
	
	
}
	
	public void preencheRegionalContrato(EntityWrapper wrapper){
		Long codMov = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
		//Long codReg = (Long) wrapper.findValue("dadosGeraisContrato.representante.usu_codreg");
		Long codReg = (Long) wrapper.findValue("dadosGeraisContrato.regional.usu_codreg");
		
		/*
		 * Quando é termo aditivo, pegar regional do posto como sendo a regional do contrato. Ajuste feito sob solicitação 347530
		 */
		if ( codMov != null && ( codMov ==5 ) ){ // alteração para quando for alteração de contrato pegar a regional do posto antigo. 
			
			Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
			for(NeoObject posto : postos)
			{
				EntityWrapper wPosto = new EntityWrapper(posto);
				codReg = NeoUtils.safeLong( String.valueOf(wPosto.findValue("regionalPosto.usu_codreg")) );
				break;
			}
			
		}
		
		if (codReg != null){
			NeoObject oRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUREG"),	new QLEqualsFilter("usu_codreg", codReg));
			if (oRegional != null){
				wrapper.findField("dadosGeraisContrato.regional").setValue(oRegional);
			}
			
		}
	}
	
	private void setaContaContabilPadrao(EntityWrapper wrapper)
	{

		Long codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
		if (codEmpresa != null){
			
			Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
			for(NeoObject posto : postos)
			{
				EntityWrapper w = new EntityWrapper(posto);
				//SAPIENSE091PLF    postosContrato.contaFinanceira
				QLGroupFilter qf = new QLGroupFilter("AND");
				
				/**
				 * @author orsegups lucas.avila - case sensitive.
				 * @date 08/07/2015
				 */
				qf.addFilter(new QLEqualsFilter("ctared", 33625L));
				qf.addFilter(new QLEqualsFilter("codemp", codEmpresa));
				
				List<NeoObject> oEstado = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"),qf);
				w.findField("contaContabil").setValue(oEstado.get(0));
				
			}
		}
	}

	private void setaContaFinanceiraPadrao(EntityWrapper wrapper)
	{
		Long codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
		if (codEmpresa != null){
			
			Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
			for(NeoObject posto : postos)
			{
				EntityWrapper w = new EntityWrapper(posto);
				
				//SAPIENSE091PLF    postosContrato.contaFinanceira
				QLGroupFilter qf = new QLGroupFilter("AND");
				qf.addFilter(new QLEqualsFilter("ctafin", 130L));
				qf.addFilter(new QLEqualsFilter("codemp", codEmpresa));
				
				List<NeoObject> oEstado = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"),qf);
				w.findField("contaFinanceira").setValue(oEstado.get(0));
				
			}
			
		}
		
	}

	public static NeoObject getEquipamentoContratoNovo(NeoObject produto, String qtd)
	{
		NeoObject clausulaSegunda = AdapterUtils.createNewEntityInstance("FGCClausulaSegunda");
		EntityWrapper wClausulaSegunda = new EntityWrapper(clausulaSegunda);
		
		NeoObject equipamento = AdapterUtils.createNewEntityInstance("GEDClausulaSegudaItem");
		EntityWrapper wEquipamento = new EntityWrapper(equipamento);
		wEquipamento.setValue("equipamento", produto);
		wEquipamento.setValue("quantidade", qtd);
		PersistEngine.persist(equipamento);
		
		wClausulaSegunda.setValue("itensClausulaSegunda", equipamento);
		PersistEngine.persist(clausulaSegunda);
		
		return clausulaSegunda;
	}
	
	public static NeoObject getEquipamentoMinuta(NeoObject produto, String qtd, boolean locado, boolean inclusao)
	{
		NeoObject equipamento = AdapterUtils.createNewEntityInstance("GEDCVEAditivoEquipamentoItem");
		EntityWrapper wEquipamento = new EntityWrapper(equipamento);
		wEquipamento.setValue("equipamento", produto);
		wEquipamento.setValue("quantidade", qtd);
		wEquipamento.setValue("locado", locado);
		
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("identificador", "2"));
		if(inclusao)
			gp.addFilter(new QLEqualsFilter("titulo", "Inclusão"));
		else
			gp.addFilter(new QLEqualsFilter("titulo", "Exclusão"));
		NeoObject exclusaoInclusao = PersistEngine.getObject(AdapterUtils.getEntityClass("opcoesCombobox"), gp);
		
		wEquipamento.setValue("inclusaoExclusao", exclusaoInclusao);
		PersistEngine.persist(equipamento);
		
		return equipamento;
	}
	
	public static NeoObject getEndereco(String cep, String endereco, String complemento, String bairro, String cidade, String estado)
	{
		NeoObject enderecoCliente = AdapterUtils.createNewEntityInstance("GEDEndereco");
		EntityWrapper wEnderecoCliente = new EntityWrapper(enderecoCliente);
		wEnderecoCliente.setValue("cep", cep);
		wEnderecoCliente.setValue("endereco", endereco);
		wEnderecoCliente.setValue("complemento", complemento);
		wEnderecoCliente.setValue("bairro", bairro);
		String cidadeCliente = NeoUtils.safeOutputString(cidade);
		String estadoCliente = NeoUtils.safeOutputString(estado);
		List<NeoObject> estado2 = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHREST"), new QLEqualsFilter("codest", estadoCliente));
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("estcid", estadoCliente));
		gp.addFilter(new QLEqualsFilter("nomcid", cidadeCliente));
		List<NeoObject> cidade2 = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_R074CID"), gp);
		NeoObject estadoCidade = AdapterUtils.createNewEntityInstance("GEDEstadoCidade");
		EntityWrapper wEstadoCidade = new EntityWrapper(estadoCidade);
		wEstadoCidade.setValue("estado", estado2.get(0));
		wEstadoCidade.setValue("cidade", cidade2.get(0));
		PersistEngine.persist(estadoCidade);
		
		
		wEnderecoCliente.setValue("estadoCidade", estadoCidade);
		PersistEngine.persist(enderecoCliente);
		
		return enderecoCliente;
	}
	
	public void populaCCPostos(EntityWrapper wrapper, boolean alteracao)
	{
		Long movimentoContrato = (Long) wrapper.findField("movimentoContratoNovo.codTipo").getValue();
		Long tipoAlteracao = 0L;
		if(movimentoContrato == 5)
		{
			tipoAlteracao = (Long) wrapper.findField("movimentoAlteracaoContrato.codTipo").getValue();
		}
		Long codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
		NeoObject representante = null;
		if(wrapper.findValue("dadosGeraisContrato.representante") != null)
			representante = (NeoObject) wrapper.findValue("dadosGeraisContrato.representante");
		
		NeoObject transacao = null;
		if(wrapper.findValue("dadosGeraisContrato.transacao") != null && codEmpresa == 19)
			transacao = (NeoObject) wrapper.findValue("dadosGeraisContrato.transacao");
		
		NeoObject oDadosGeraisContrato = (NeoObject) wrapper.findField("dadosGeraisContrato").getValue();
		EntityWrapper wDadosContrato = new EntityWrapper(oDadosGeraisContrato);
		
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codCli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codcli"));
		
		//numContrato.usu_numctr
		String numeroContrato = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		if(numeroContrato == null || numeroContrato.equals("") || tipoAlteracao == 1)
			numeroContrato = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		String classificacaoCentroCusto = CadastraCentroCusto.getClassificacaoCentroCusto(NeoUtils.safeOutputString(codemp), codCli, 6, numeroContrato);
		if(classificacaoCentroCusto.equals(""))
			classificacaoCentroCusto = NeoUtils.safeOutputString(wrapper.findValue("aClaCta6"));
		String centroCustoContrato = CadastraCentroCusto.getCentroCusto(classificacaoCentroCusto, 6, NeoUtils.safeOutputString(codemp));
		if(centroCustoContrato.equals(""))
		{
			/*
			 * se estiver vazio o cliente tem contrato mas nao 
			 * possui posto cadastrado ainda, logo e um contrato novo
			 */
			centroCustoContrato = NeoUtils.safeOutputString(wrapper.findValue("centroCusto6"));
		}
		
		/**
		 * @author orsegups lucas.avila - case sensitive e ValueOf para Long.
		 * @date 08/07/2015
		 */
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("codccu", centroCustoContrato));
		gp.addFilter(new QLEqualsFilter("codemp", Long.valueOf(codemp)));
		gp.addFilter(new QLEqualsFilter("nivccu", 6));
		List<NeoObject> centrosCustos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), gp);
		
		if(centrosCustos.size() > 0)
		{
			NeoObject centroCusto = centrosCustos.get(0);
			
			Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
			for(NeoObject posto : postos)
			{
				EntityWrapper w = new EntityWrapper(posto);
				
				boolean novoPosto = !NeoUtils.safeBoolean(w.findValue("origemSapiens"));
				if(!novoPosto && tipoAlteracao != 1)
					continue;
				
				
				Boolean copiarEndereco = (Boolean) w.findField("endEfetivoIgualPosto").getValue();
				if (copiarEndereco)
				{
					NeoObject oEnderecoPosto = (NeoObject) w.findField("enderecoPosto").getValue();
					EntityWrapper wEnderecoPosto = new EntityWrapper(oEnderecoPosto);
					
					String cepCli = NeoUtils.safeOutputString(wEnderecoPosto.findValue("cep"));
					String endCli = NeoUtils.safeOutputString(wEnderecoPosto.findValue("endereco"));  
					String numEndCli = NeoUtils.safeOutputString(wEnderecoPosto.findValue("numero"));    
					String compCli = NeoUtils.safeOutputString(wEnderecoPosto.findValue("complemento"));    
					String bairroCli = NeoUtils.safeOutputString(wEnderecoPosto.findValue("bairro"));    

					NeoObject oEnderecoEfetivo = AdapterUtils.createNewEntityInstance("FGCEndereco");
					EntityWrapper wEnderecoEfetivo = new EntityWrapper(oEnderecoEfetivo);
					
					wEnderecoEfetivo.findField("cep").setValue( cepCli);
					
					wEnderecoEfetivo.findField("endereco").setValue(endCli);
					wEnderecoEfetivo.findField("numero").setValue(numEndCli);
					wEnderecoEfetivo.findField("complemento").setValue(compCli);
					wEnderecoEfetivo.findField("bairro").setValue(bairroCli);
					
					//ENDERECO DO CLIENTE
					NeoObject oEstadoCidadePosto = (NeoObject) wEnderecoPosto.findField("estadoCidade").getValue();
					EntityWrapper wEstadoCidade = new EntityWrapper(oEstadoCidadePosto);
					String usu_cidctr = NeoUtils.safeOutputString(wEstadoCidade.findValue("cidade.nomcid"));
					String usu_ufsctr = NeoUtils.safeOutputString(wEstadoCidade.findValue("estado.sigufs"));
					
					String usu_cepctr = cepCli.replaceAll("-", "");
					
					//ESTADO CIDADE
					NeoObject oEstadoCidadeEfetivo = AdapterUtils.createNewEntityInstance("FGCEstadoCidade");
					if(oEstadoCidadeEfetivo != null)
					{
						EntityWrapper wEstadoCidadeEfetivo = new EntityWrapper(oEstadoCidadeEfetivo);

						//uf
						NeoObject estado = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE007UFS"),
								new QLEqualsFilter("sigufs", usu_ufsctr));
						if(estado != null)
							wEstadoCidadeEfetivo.findField("estado").setValue(estado);

						//cidade
						NeoObject cidade = retornaCidadeSapiens8Cep(usu_ufsctr, usu_cidctr, Long.parseLong(usu_cepctr) );
						if(cidade != null)
							wEstadoCidadeEfetivo.findField("cidade").setValue(cidade);

						//Seta no eform estadocidade no endereco
						PersistEngine.persist(oEstadoCidadeEfetivo);
						wEnderecoEfetivo.findField("estadoCidade").setValue(oEstadoCidadeEfetivo);
					}
										
					PersistEngine.persist(oEnderecoEfetivo);
					w.findField("enderecoEfetivo").setValue(oEnderecoEfetivo);
					
				}

				/*
				 * se for um posto novo, ou
				 * alteração de CNPJ
				 * 
				 * os centros de custo criados anteriormente devem ser vinculados a todos os postos
				 */
				
				if(!alteracao || (movimentoContrato == 5 && tipoAlteracao == 1))
				{
					w.setValue("centroCusto", centroCusto);
					w.setValue("representante", representante);
				}else
				{
					boolean agrupadorDiferente = NeoUtils.safeBoolean(wrapper.findValue("dadosGeraisContrato.utilizarCodigoExistente"));
					if(agrupadorDiferente)
					{
						NeoObject agrupador = (NeoObject) wrapper.findValue("dadosGeraisContrato.codigoAgrupador");
						EntityWrapper wAgr = new EntityWrapper(agrupador);
						numeroContrato = NeoUtils.safeOutputString(wAgr.findValue("usu_ctragr")).replace("AGR", "");
						String classificacaoQuintoNivel = CadastraCentroCusto.getClassificacaoCentroCusto(codemp, codCli, 5, numeroContrato);
						String centroCustoQuintoNivel = CadastraCentroCusto.getCentroCusto(classificacaoQuintoNivel, 5, codemp);
						gp.clearFilterList();
						gp.addFilter(new QLEqualsFilter("codccu", centroCustoQuintoNivel));
						gp.addFilter(new QLEqualsFilter("codemp", NeoUtils.safeOutputString(codemp)));
						gp.addFilter(new QLEqualsFilter("nivccu", 5));
						centrosCustos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), gp);
						w.setValue("centroCusto", centrosCustos.get(0));
					}else
					{
						w.setValue("centroCusto", centroCusto);
					}
				}
				if(tipoAlteracao != 1 && transacao != null)
					w.setValue("transacao", transacao);
			}
		}
	}
	
	/**
	 * SAPIENSE008CEP
	 * @param uf
	 * @param cidade
	 * @return NeoObject SAPIENSE008CEP
	 */
	private static NeoObject retornaCidadeSapiens8Cep(String uf, String cidade, Long cep)
	{
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("sigufs", uf));
		gp.addFilter(new QLEqualsFilter("nomcid", cidade));
		Collection<NeoObject> objCidade = (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE008CEP"), gp);
		NeoObject cidadeAlvo = null;
		for(NeoObject obj : objCidade)
		{
			EntityWrapper w = new EntityWrapper(obj);
			String cepini = NeoUtils.safeOutputString(w.findValue("cepini"));
			String cepfim = NeoUtils.safeOutputString(w.findValue("cepfim"));

			if((cep >= NeoUtils.safeLong(cepini)) &&  cep <= NeoUtils.safeLong(cepfim))
			{
				cidadeAlvo = obj;
			}
		}
		return cidadeAlvo;
	}
	
	public void back(EntityWrapper processEntity, Activity activity)
	{
		System.out.println("[FLUXO CONTRATOS]-Recusou atividade Minuta");
	}
	
	/**
	 * Gera Observação da OS sigma para poupar tempo do operador do cadastro.
	 * @param numeroContrato
	 * @param movimentoContrato
	 * @param movimentoAlteracao
	 * @param equipamentos
	 * @return
	 */
	public static String geraDescricaoOS(String numeroContrato, Long movimentoContrato, Long movimentoAlteracao,  Collection<NeoObject> equipamentos){
		
		StringBuilder obsBuff = new StringBuilder();
		int instalar = 0;
		int remover  = 0;
		int reinstalar = 0;
		
		long quantidadePontos = 0L;
		
		// verificar quantidade de pontos antes de mais nada, pois existem regras que dependem disso.
		for(NeoObject equip : equipamentos)
		{
			
			EntityWrapper wEquip = new EntityWrapper(equip);
			String codigoInstalacao = NeoUtils.safeOutputString(wEquip.findValue("tipoInstalacao.codigo"));
			NeoObject oProd = (NeoObject) wEquip.findValue("produto");
			EntityWrapper wProd = new EntityWrapper(oProd);
			if (wProd.findValue("usu_ponpro") != null && ("N".equals(codigoInstalacao) || "Y".equals(codigoInstalacao) || "R".equals(codigoInstalacao) || "F".equals(codigoInstalacao) || "D".equals(codigoInstalacao) ) ){
				Long qtdeEquipamentos = NeoUtils.safeLong( NeoUtils.safeOutputString(wEquip.findValue("quantidade")) );
				if (qtdeEquipamentos == null) qtdeEquipamentos = 1L;
				quantidadePontos += qtdeEquipamentos * NeoUtils.safeLong( NeoUtils.safeOutputString(wProd.findValue("usu_ponpro") ) );
				ContratoLogUtils.logInfo("Contrato: " + numeroContrato + " Produto:" + wProd.findValue("codpro") + " Pontos Individuais: " + quantidadePontos );
			}
		}
		
		ContratoLogUtils.logInfo("Contrato " + numeroContrato + ",Quantidade de Pontos " + quantidadePontos + ",quantidade Equipamentos= " + equipamentos.size());
		
		String strTipoContrato = "";
		
		if (movimentoContrato !=5 ){
			strTipoContrato = "CONTRATO NOVO - ";
		}else{
			strTipoContrato = "TERMO ADITIVO - ";
			switch (movimentoAlteracao.intValue()) {
			case 1:
				strTipoContrato += "ALTERAÇÃO DE CNPJ ";
				break;
			case 2:
				strTipoContrato += "ALTERAÇÃO DE RAZÃO SOCIAL ";
				break;
			case 3:
				strTipoContrato += "AMPLIAÇÃO DE ELETRÔNICA E CFTV ";
				break;
			case 4:
				strTipoContrato += "REDUÇÃO DE ELETRÕNICA E CFTV ";
				break;
			case 5:
				strTipoContrato += "AMPLIAÇÃO DE HUMANA E ASSEIO";
				break;
			case 6:
				strTipoContrato += "REDUÇÃO DE VALOR ";
				break;
			case 7:
				strTipoContrato += "SERVIÇO EXTRA DE VIGILANCIA HUMANA ";
				break;
			case 8:
				strTipoContrato += "SERVIÇO EXTRA TEMPORÁRIO DE VIGILANCIA HUMANA ";
				break;
			case 9:
				strTipoContrato += "REINSTALAÇÃO ";
				break;
			case 10:
				strTipoContrato += "VENDA - ELETRÔNICA E CFTV ";
				break;				
			default:
				strTipoContrato += "";
				break;
			}
		}
		
		
		String obs[] = {"","","","","","","","",""}; // acumula os codigos dos equiamentos para montar a lista de descrições
		
		/*
		 *  CONTRATO NOVO TP 14 INSTALAR EQUIPAMENTOS E HABILITAR MONITORAMENTO CONFORME RMC 342748
		 */
		
		for(NeoObject equip : equipamentos)
		{
			ContratoLogUtils.logInfo("Contrato: " + numeroContrato + "Montando Observação da, OS Entrando na lista.");
			EntityWrapper wEquip = new EntityWrapper(equip);
			NeoObject oProd = (NeoObject) wEquip.findValue("produto");
			EntityWrapper wProd = new EntityWrapper(oProd);
			String strQdt = NeoUtils.safeOutputString(wEquip.findValue("quantidade"));
			String  produtoDesc = NeoUtils.safeOutputString(strQdt+"UN. "+wProd.findValue("codpro"))+"-"+truncarNomeProduto( NeoUtils.safeOutputString(wProd.findValue("despro")),30) ; // resumindo nome do equipamento para caber mais equipamentos
			ContratoLogUtils.logInfo("Contrato: " + numeroContrato + "DescProd: " + produtoDesc);
			
			
			String codigoInstalacao = NeoUtils.safeOutputString(wEquip.findValue("tipoInstalacao.codigo"));
			if ("N".equalsIgnoreCase(codigoInstalacao)){
				obs[0] += produtoDesc + ", ";
				instalar++;
			}else if ("R".equalsIgnoreCase(codigoInstalacao)){
				obs[1] += produtoDesc + ", ";
				reinstalar++;
			}else if ("Y".equalsIgnoreCase(codigoInstalacao)){
				obs[2] += produtoDesc + ", ";
				reinstalar++;
			}else if ("E".equalsIgnoreCase(codigoInstalacao)){
				obs[3] += produtoDesc + ", ";
			}else if ("D".equalsIgnoreCase(codigoInstalacao)){
				obs[4] += produtoDesc + ", ";
				instalar++;
			}else if ("C".equalsIgnoreCase(codigoInstalacao)){
				obs[5] += produtoDesc + ", ";
			}else if ("X".equalsIgnoreCase(codigoInstalacao)){
				obs[6] += produtoDesc + ", ";
				remover++;
			}else if ("F".equalsIgnoreCase(codigoInstalacao)){
				obs[7] += produtoDesc + ", ";
				instalar++;
			}else if ("G".equalsIgnoreCase(codigoInstalacao)){
				obs[8] += produtoDesc + ", ";
				instalar++;
			}
			
		}
		if (instalar > 0 || reinstalar > 0 || remover >0 ){
			
			obsBuff.append(strTipoContrato);
			
			if (instalar > 0){
				obsBuff.append("INSTALAR EQUIPAMENTO(S) ");
				if (/*quantidadePontos > 0 &&*/ quantidadePontos >= 0){
					obsBuff.append("TP "+quantidadePontos+" ");
					if (obs[0].length() > 0 ) obsBuff.append( obs[0].substring(0,obs[0].lastIndexOf(",")) ); // N
					if (obs[7].length() > 0 ) obsBuff.append( obs[7].substring(0,obs[7].lastIndexOf(",")) ); // F
					if (obs[4].length() > 0 ) obsBuff.append( obs[4].substring(0,obs[4].lastIndexOf(",")) ); // D
				}
				obsBuff.append(".\n");
			}
			
			if (reinstalar > 0){
				obsBuff.append("REINSTALAR EQUIPAMENTO(S) ");
				if (/*quantidadePontos > 0 &&*/ quantidadePontos >= 0 ){ // solicitado Pro Fernanda.Martins e aprovado pelo Giliardi que até 2 pontos seja colocado código e descrição do produto na OS
					obsBuff.append("TP "+quantidadePontos+" ");
					if (obs[1].length() > 0 ) obsBuff.append( obs[1].substring(0,obs[1].lastIndexOf(",")) ); // R
					if (obs[2].length() > 0 ) obsBuff.append( obs[2].substring(0,obs[2].lastIndexOf(",")) ); // Y
				}
				obsBuff.append(".\n");
			}
			
			if (remover > 0){
				obsBuff.append("REMOVER EQUIPAMENTO(S) ");
				if (/*quantidadePontos > 0 &&*/ quantidadePontos >= 0 ){ // solicitado Pro Fernanda.Martins e aprovado pelo Giliardi que até 2 pontos seja colocado código e descrição do produto na OS
					obsBuff.append("TP "+quantidadePontos+" ");
					if (obs[6].length() > 0 ) obsBuff.append( obs[6].substring(0,obs[6].lastIndexOf(",")) ); // X
				}
				obsBuff.append(".\n");
			}
			
			obsBuff.append("CONFORME RMC: " +numeroContrato);
		}
		return obsBuff.toString();
	}
	
	public static String truncarNomeProduto(String nomeProd, int qtdeChars){
		String retorno ="";
		
		if (nomeProd != null && nomeProd.length() > 0 ){
			if (qtdeChars >= nomeProd.length()){
				retorno = nomeProd;
			}else{
				retorno = nomeProd.substring(0,qtdeChars);
			}
		}
		
		return retorno;
	}
	
	
	
}
