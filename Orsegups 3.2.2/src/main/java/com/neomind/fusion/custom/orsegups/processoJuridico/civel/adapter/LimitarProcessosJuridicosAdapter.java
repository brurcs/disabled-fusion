package com.neomind.fusion.custom.orsegups.processoJuridico.civel.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLDateBetweenFilter;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class LimitarProcessosJuridicosAdapter implements TaskFinishEventListener
{
	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException
	{
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);

		GregorianCalendar gc2 = new GregorianCalendar();
		gc2.set(Calendar.HOUR_OF_DAY, 24);
		gc2.set(Calendar.MINUTE, 59);

		QLGroupFilter qlGroup = new QLGroupFilter("AND");
		qlGroup.addFilter(new QLDateBetweenFilter("startDate", gc, gc2));
		qlGroup.addFilter(new QLEqualsFilter("model.title", "J004 - Processo Cível"));

		List<WFProcess> listProcessos = PersistEngine.getObjects(WFProcess.class, qlGroup);

		if (listProcessos.size() >= 10)
			throw new WorkflowException(
					"Não foi possível iniciar o processo. O limite é de 10 processos por dia.");
	}
}
