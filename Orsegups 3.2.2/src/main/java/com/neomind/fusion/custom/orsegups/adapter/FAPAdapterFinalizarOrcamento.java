package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dto.ItemOrcamentoDTO;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityFinishEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;

/**
 * Adapter de Eventos executado na atividade Finalizar Orçamento da FAP
 * 
 * @author herisson.ferreira
 *
 */
public class FAPAdapterFinalizarOrcamento implements TaskFinishEventListener {
	
	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
		
		EntityWrapper processEntity = event.getWrapper();
		
		String aplicacoesValidasTarefasNAC[] =  FAPParametrizacao.findParameter("aplicacoesTarefasNAC").split(",");
		Long aplicacaoFap = (Long) processEntity.findGenericValue("aplicacaoPagamento.codigo");
		boolean aplicacaoValidaNAC = validaAplicacao(aplicacoesValidasTarefasNAC, aplicacaoFap); 
		
		if(aplicacaoValidaNAC) {
			geraTarefaEquipamentosFap(processEntity);			
		}
	
	}

	/**
	 * Verifica se a aplicação atual da FAP é valida para geração da tarefa
	 * 
	 * @param aplicacoesValidasTarefasNAC
	 * @param aplicacaoFap
	 * @return Se a aplicação é valida ou não
	 */
	private boolean validaAplicacao(String[] aplicacoesValidasTarefasNAC, Long aplicacaoFap) {
		
		Boolean valido = false;
		
		for(int i = 0; i < aplicacoesValidasTarefasNAC.length ; i++) {
			
			if(!valido) {
				valido = aplicacoesValidasTarefasNAC[i].equals(aplicacaoFap.toString()) ? true : false;;				
			}
		}
		
		return valido;
	}

	/**
	 * Valida se a aplicação possui um item de orçamento do tipo Produto ou o campo possuiEquipamento está preenchido como sim para assim efetuar a abertura da tarefa
	 * 
	 * @param processEntity
	 */
	private void geraTarefaEquipamentosFap(EntityWrapper processEntity) {
		
		Boolean possuiProduto = false;
		Boolean possuiEquipamento = false;
		
		List<ItemOrcamentoDTO> listaItemOrcamento = new ArrayList<>();
		
		try {
			
			List<NeoObject> itensOrcamento = (List<NeoObject>) processEntity.findValue("itemOrcamento");

			possuiEquipamento = (Boolean) processEntity.findGenericValue("possuiEquipamento");
			
			if(itensOrcamento != null && !itensOrcamento.isEmpty()) {
				for(NeoObject itemOrcamento : itensOrcamento) {
					
					ItemOrcamentoDTO itemOrcamentoDTO = new ItemOrcamentoDTO();
					
					EntityWrapper wrpItem = new EntityWrapper(itemOrcamento);
					String descricao = (String) wrpItem.findValue("descricao");
					String tipoDoItem = (String) wrpItem.findValue("tipoItem.descricao");
					BigDecimal valor = (BigDecimal) wrpItem.findValue("valor");
					
					if(tipoDoItem.equalsIgnoreCase("PRODUTO")) {
						
						possuiProduto = true;
						
						itemOrcamentoDTO.setDescricao(descricao);
						itemOrcamentoDTO.setTipo(tipoDoItem);
						itemOrcamentoDTO.setValor(valor);
						
						listaItemOrcamento.add(itemOrcamentoDTO);
					}	
					
				}	
			}
			
			if(possuiProduto || possuiEquipamento) {
				abreTarefa(processEntity, listaItemOrcamento);
			}
			
		} catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException("Não foi possível abrir a tarefa de Equipamentos. Por gentileza entre em contato com o Departamento de T.I.");
		}
		
	}

	/**
	 * Efetua a abertura da tarefa simples
	 * 
	 * @param processEntity
	 * @throws WorkflowException
	 */
	private void abreTarefa(EntityWrapper processEntity, List<ItemOrcamentoDTO> listaItemOrcamento) throws WorkflowException {
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		GregorianCalendar prazo = new GregorianCalendar();

		NeoUser solicitante = null;
		NeoUser executor = null;

		String titulo = null;
		String descricao = null;

		String codigoFap = processEntity.findGenericValue("wfprocess.code");
		String razaoCliente = processEntity.findGenericValue("contaSigma.razao");

		titulo = "Equipamento FAP: "+codigoFap+" - Cliente: "+razaoCliente;

		solicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(OrsegupsUtils.getPaper("solicitanteTarefaEquipamentoFap"));
		executor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(OrsegupsUtils.getPaper("executorTarefaEquipamentoFap"));

		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);

		descricao = montaDescricao(processEntity, listaItemOrcamento);

		iniciarTarefaSimples.abrirTarefa(solicitante.getCode(), executor.getCode(), titulo, descricao, "1", "sim", prazo);

	}

	/**
	 * Monta a descrição da tarefa com os parâmetros presentes na tarefa de FAP
	 * 
	 * @param processEntity
	 * @return Descrição da tarefa
	 * @throws WorkflowException
	 */
	private String montaDescricao(EntityWrapper processEntity, List<ItemOrcamentoDTO> listaItemOrcamento) throws WorkflowException {
		
		StringBuilder descricao = new StringBuilder();
		
		String codigoFap = processEntity.findGenericValue("wfprocess.code");
		String codigoContaSigma = processEntity.findGenericValue("contaSigma.id_central");
		String razaoCliente = processEntity.findGenericValue("contaSigma.razao");
		String laudoOS = processEntity.findGenericValue("laudoEletronica.laudo");
		String relatoOS = processEntity.findGenericValue("laudoEletronica.relato");
		Long numeroOrdemServico = 0L;
		
		List<NeoObject> listaOs = processEntity.findGenericValue("listaOrdemDeServico");			
		NeoObject objetoOrdemServico = listaOs.isEmpty() == true ? null : listaOs.get(0);
		
		if(objetoOrdemServico != null) {
			EntityWrapper ewOrdemServico = new EntityWrapper(objetoOrdemServico);
			numeroOrdemServico = (Long) ewOrdemServico.findGenericValue("numeroDaOs") != null ? (Long) ewOrdemServico.findGenericValue("numeroDaOs") : 0L;
		}
		
		descricao.append("<strong>BA:</strong> " + codigoContaSigma + " - " + razaoCliente + "</br>");
		descricao.append("<strong>FAP:</strong> " + codigoFap + "</br>");
		descricao.append("<strong>Número OS:</strong> " + numeroOrdemServico.toString() + "</br>");
		descricao.append("<strong>Relato OS:</strong> " + relatoOS + "</br>");
		descricao.append("<strong>Laudo OS:</strong> " + laudoOS + "</br>");
		descricao.append("Analisar solicitação e providenciar o cadastro e/ou envio dos equipamentos.");
		
		descricao.append(listaItemOrcamento.size() > 0 ? retornaListaItensOrcamentoDescricao(listaItemOrcamento) : "");
		
		return descricao.toString();
	}

	private String retornaListaItensOrcamentoDescricao(List<ItemOrcamentoDTO> listaItemOrcamento) {
		
		StringBuilder descricao = new StringBuilder();
		
		descricao.append(" <html>                                                  				   												 ");
		descricao.append("    <head>                                              				   												 ");
		descricao.append("        <meta charset='utf-8'>                           				   												 ");
		descricao.append("    </head>                                              				   												 ");
		descricao.append("      <body>                                             				   												 ");
		descricao.append("          <table border='2'>                             				   												 ");
		descricao.append("              <tr>                                       				   												 ");
		descricao.append("                  <th>Tipo</th>                      				   										 			 ");
		descricao.append("                  <th>Descrição</th>      		      			 												     ");
		descricao.append("                  <th>Valor</th>      		      														             ");
		descricao.append("              </tr>                                       				   								 		     ");
		
		for(ItemOrcamentoDTO itemOrcamento : listaItemOrcamento) {
			
			descricao.append("              <tr>                                       				   												 ");
			descricao.append("                <td>&nbsp; "+itemOrcamento.getTipo()+" </td>           												 ");
			descricao.append("                <td>&nbsp; "+itemOrcamento.getDescricao()+" </td>           											 ");
			descricao.append("                <td>&nbsp; "+itemOrcamento.getValor()+" </td>           												 ");
			descricao.append("              </tr>                                      				   												 ");
			
		}
		
		descricao.append("      </table>                                             				   											 ");
		descricao.append("      </body>                                             				   											 ");
		descricao.append(" </html>                                                  				   											 ");
		
		
		return descricao.toString();
	}
	
}
