/**
 * ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

public class ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente  implements java.io.Serializable {
    private java.lang.String acePar;

    private java.lang.Integer acrDia;

    private java.lang.String antDsc;

    private java.lang.String avaAti;

    private java.lang.Integer avaMot;

    private java.lang.String avaObs;

    private java.lang.Double avaVlr;

    private java.lang.Double avaVls;

    private java.lang.Double avaVlu;

    private java.lang.Integer catCli;

    private java.lang.String ccbCli;

    private java.lang.String cifFob;

    private java.lang.String cnpjFilial;

    private java.lang.String codAge;

    private java.lang.String codBan;

    private java.lang.String codCpg;

    private java.lang.String codCrp;

    private java.lang.String codCrt;

    private java.lang.Integer codEmp;

    private java.lang.String codFcr;

    private java.lang.Integer codFil;

    private java.lang.Integer codFpg;

    private java.lang.String codFrj;

    private java.lang.String codIn1;

    private java.lang.String codIn2;

    private java.lang.String codMar;

    private java.lang.Integer codRed;

    private java.lang.Integer codRep;

    private java.lang.String codRve;

    private java.lang.String codTab;

    private java.lang.String codTic;

    private java.lang.String codTpr;

    private java.lang.Integer codTra;

    private java.lang.String codTrd;

    private java.lang.Integer codVen;

    private java.lang.String criEdv;

    private java.lang.Integer ctaAad;

    private java.lang.Integer ctaAux;

    private java.lang.Integer ctaFcr;

    private java.lang.Integer ctaFdv;

    private java.lang.Integer ctaRcr;

    private java.lang.Integer ctaRed;

    private java.lang.Integer ctrPad;

    private java.lang.String datLim;

    private java.lang.String datPmr;

    private java.lang.String diaEsp;

    private java.lang.Integer diaMe1;

    private java.lang.Integer diaMe2;

    private java.lang.Integer diaMe3;

    private java.lang.String dscPrd;

    private java.lang.String ecpCnp;

    private java.lang.String exiAge;

    private java.lang.String exiLcp;

    private java.lang.String fveCpg;

    private java.lang.String fveFpg;

    private java.lang.String fveTns;

    private java.lang.String gerTcc;

    private java.lang.String indOrf;

    private java.lang.String limApr;

    private java.lang.Integer perAqa;

    private java.lang.String perCcr;

    private java.lang.Integer perCom;

    private java.lang.Double perDs1;

    private java.lang.Double perDs2;

    private java.lang.Double perDs3;

    private java.lang.Double perDs4;

    private java.lang.Double perDs5;

    private java.lang.Double perDsc;

    private java.lang.Double perEmb;

    private java.lang.Double perEnc;

    private java.lang.Double perFre;

    private java.lang.Double perIss;

    private java.lang.Integer perOf1;

    private java.lang.Integer perOf2;

    private java.lang.Double perOut;

    private java.lang.Double perSeg;

    private java.lang.String porNa1;

    private java.lang.String porNa2;

    private java.lang.String porSi1;

    private java.lang.String porSi2;

    private java.lang.String prdDsc;

    private java.lang.Integer qtdChs;

    private java.lang.Integer qtdDcv;

    private java.lang.Integer qtdPrt;

    private java.lang.Integer recDtj;

    private java.lang.Integer recDtm;

    private java.lang.Double recJmm;

    private java.lang.Double recMul;

    private java.lang.String recTjr;

    private java.lang.Integer seqCob;

    private java.lang.Integer seqEnt;

    private java.lang.Integer tolDsc;

    private java.lang.Double vlrAcr;

    private java.lang.Double vlrLim;

    private java.lang.Double vlrPrt;

    public ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente() {
    }

    public ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente(
           java.lang.String acePar,
           java.lang.Integer acrDia,
           java.lang.String antDsc,
           java.lang.String avaAti,
           java.lang.Integer avaMot,
           java.lang.String avaObs,
           java.lang.Double avaVlr,
           java.lang.Double avaVls,
           java.lang.Double avaVlu,
           java.lang.Integer catCli,
           java.lang.String ccbCli,
           java.lang.String cifFob,
           java.lang.String cnpjFilial,
           java.lang.String codAge,
           java.lang.String codBan,
           java.lang.String codCpg,
           java.lang.String codCrp,
           java.lang.String codCrt,
           java.lang.Integer codEmp,
           java.lang.String codFcr,
           java.lang.Integer codFil,
           java.lang.Integer codFpg,
           java.lang.String codFrj,
           java.lang.String codIn1,
           java.lang.String codIn2,
           java.lang.String codMar,
           java.lang.Integer codRed,
           java.lang.Integer codRep,
           java.lang.String codRve,
           java.lang.String codTab,
           java.lang.String codTic,
           java.lang.String codTpr,
           java.lang.Integer codTra,
           java.lang.String codTrd,
           java.lang.Integer codVen,
           java.lang.String criEdv,
           java.lang.Integer ctaAad,
           java.lang.Integer ctaAux,
           java.lang.Integer ctaFcr,
           java.lang.Integer ctaFdv,
           java.lang.Integer ctaRcr,
           java.lang.Integer ctaRed,
           java.lang.Integer ctrPad,
           java.lang.String datLim,
           java.lang.String datPmr,
           java.lang.String diaEsp,
           java.lang.Integer diaMe1,
           java.lang.Integer diaMe2,
           java.lang.Integer diaMe3,
           java.lang.String dscPrd,
           java.lang.String ecpCnp,
           java.lang.String exiAge,
           java.lang.String exiLcp,
           java.lang.String fveCpg,
           java.lang.String fveFpg,
           java.lang.String fveTns,
           java.lang.String gerTcc,
           java.lang.String indOrf,
           java.lang.String limApr,
           java.lang.Integer perAqa,
           java.lang.String perCcr,
           java.lang.Integer perCom,
           java.lang.Double perDs1,
           java.lang.Double perDs2,
           java.lang.Double perDs3,
           java.lang.Double perDs4,
           java.lang.Double perDs5,
           java.lang.Double perDsc,
           java.lang.Double perEmb,
           java.lang.Double perEnc,
           java.lang.Double perFre,
           java.lang.Double perIss,
           java.lang.Integer perOf1,
           java.lang.Integer perOf2,
           java.lang.Double perOut,
           java.lang.Double perSeg,
           java.lang.String porNa1,
           java.lang.String porNa2,
           java.lang.String porSi1,
           java.lang.String porSi2,
           java.lang.String prdDsc,
           java.lang.Integer qtdChs,
           java.lang.Integer qtdDcv,
           java.lang.Integer qtdPrt,
           java.lang.Integer recDtj,
           java.lang.Integer recDtm,
           java.lang.Double recJmm,
           java.lang.Double recMul,
           java.lang.String recTjr,
           java.lang.Integer seqCob,
           java.lang.Integer seqEnt,
           java.lang.Integer tolDsc,
           java.lang.Double vlrAcr,
           java.lang.Double vlrLim,
           java.lang.Double vlrPrt) {
           this.acePar = acePar;
           this.acrDia = acrDia;
           this.antDsc = antDsc;
           this.avaAti = avaAti;
           this.avaMot = avaMot;
           this.avaObs = avaObs;
           this.avaVlr = avaVlr;
           this.avaVls = avaVls;
           this.avaVlu = avaVlu;
           this.catCli = catCli;
           this.ccbCli = ccbCli;
           this.cifFob = cifFob;
           this.cnpjFilial = cnpjFilial;
           this.codAge = codAge;
           this.codBan = codBan;
           this.codCpg = codCpg;
           this.codCrp = codCrp;
           this.codCrt = codCrt;
           this.codEmp = codEmp;
           this.codFcr = codFcr;
           this.codFil = codFil;
           this.codFpg = codFpg;
           this.codFrj = codFrj;
           this.codIn1 = codIn1;
           this.codIn2 = codIn2;
           this.codMar = codMar;
           this.codRed = codRed;
           this.codRep = codRep;
           this.codRve = codRve;
           this.codTab = codTab;
           this.codTic = codTic;
           this.codTpr = codTpr;
           this.codTra = codTra;
           this.codTrd = codTrd;
           this.codVen = codVen;
           this.criEdv = criEdv;
           this.ctaAad = ctaAad;
           this.ctaAux = ctaAux;
           this.ctaFcr = ctaFcr;
           this.ctaFdv = ctaFdv;
           this.ctaRcr = ctaRcr;
           this.ctaRed = ctaRed;
           this.ctrPad = ctrPad;
           this.datLim = datLim;
           this.datPmr = datPmr;
           this.diaEsp = diaEsp;
           this.diaMe1 = diaMe1;
           this.diaMe2 = diaMe2;
           this.diaMe3 = diaMe3;
           this.dscPrd = dscPrd;
           this.ecpCnp = ecpCnp;
           this.exiAge = exiAge;
           this.exiLcp = exiLcp;
           this.fveCpg = fveCpg;
           this.fveFpg = fveFpg;
           this.fveTns = fveTns;
           this.gerTcc = gerTcc;
           this.indOrf = indOrf;
           this.limApr = limApr;
           this.perAqa = perAqa;
           this.perCcr = perCcr;
           this.perCom = perCom;
           this.perDs1 = perDs1;
           this.perDs2 = perDs2;
           this.perDs3 = perDs3;
           this.perDs4 = perDs4;
           this.perDs5 = perDs5;
           this.perDsc = perDsc;
           this.perEmb = perEmb;
           this.perEnc = perEnc;
           this.perFre = perFre;
           this.perIss = perIss;
           this.perOf1 = perOf1;
           this.perOf2 = perOf2;
           this.perOut = perOut;
           this.perSeg = perSeg;
           this.porNa1 = porNa1;
           this.porNa2 = porNa2;
           this.porSi1 = porSi1;
           this.porSi2 = porSi2;
           this.prdDsc = prdDsc;
           this.qtdChs = qtdChs;
           this.qtdDcv = qtdDcv;
           this.qtdPrt = qtdPrt;
           this.recDtj = recDtj;
           this.recDtm = recDtm;
           this.recJmm = recJmm;
           this.recMul = recMul;
           this.recTjr = recTjr;
           this.seqCob = seqCob;
           this.seqEnt = seqEnt;
           this.tolDsc = tolDsc;
           this.vlrAcr = vlrAcr;
           this.vlrLim = vlrLim;
           this.vlrPrt = vlrPrt;
    }


    /**
     * Gets the acePar value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return acePar
     */
    public java.lang.String getAcePar() {
        return acePar;
    }


    /**
     * Sets the acePar value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param acePar
     */
    public void setAcePar(java.lang.String acePar) {
        this.acePar = acePar;
    }


    /**
     * Gets the acrDia value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return acrDia
     */
    public java.lang.Integer getAcrDia() {
        return acrDia;
    }


    /**
     * Sets the acrDia value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param acrDia
     */
    public void setAcrDia(java.lang.Integer acrDia) {
        this.acrDia = acrDia;
    }


    /**
     * Gets the antDsc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return antDsc
     */
    public java.lang.String getAntDsc() {
        return antDsc;
    }


    /**
     * Sets the antDsc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param antDsc
     */
    public void setAntDsc(java.lang.String antDsc) {
        this.antDsc = antDsc;
    }


    /**
     * Gets the avaAti value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return avaAti
     */
    public java.lang.String getAvaAti() {
        return avaAti;
    }


    /**
     * Sets the avaAti value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param avaAti
     */
    public void setAvaAti(java.lang.String avaAti) {
        this.avaAti = avaAti;
    }


    /**
     * Gets the avaMot value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return avaMot
     */
    public java.lang.Integer getAvaMot() {
        return avaMot;
    }


    /**
     * Sets the avaMot value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param avaMot
     */
    public void setAvaMot(java.lang.Integer avaMot) {
        this.avaMot = avaMot;
    }


    /**
     * Gets the avaObs value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return avaObs
     */
    public java.lang.String getAvaObs() {
        return avaObs;
    }


    /**
     * Sets the avaObs value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param avaObs
     */
    public void setAvaObs(java.lang.String avaObs) {
        this.avaObs = avaObs;
    }


    /**
     * Gets the avaVlr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return avaVlr
     */
    public java.lang.Double getAvaVlr() {
        return avaVlr;
    }


    /**
     * Sets the avaVlr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param avaVlr
     */
    public void setAvaVlr(java.lang.Double avaVlr) {
        this.avaVlr = avaVlr;
    }


    /**
     * Gets the avaVls value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return avaVls
     */
    public java.lang.Double getAvaVls() {
        return avaVls;
    }


    /**
     * Sets the avaVls value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param avaVls
     */
    public void setAvaVls(java.lang.Double avaVls) {
        this.avaVls = avaVls;
    }


    /**
     * Gets the avaVlu value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return avaVlu
     */
    public java.lang.Double getAvaVlu() {
        return avaVlu;
    }


    /**
     * Sets the avaVlu value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param avaVlu
     */
    public void setAvaVlu(java.lang.Double avaVlu) {
        this.avaVlu = avaVlu;
    }


    /**
     * Gets the catCli value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return catCli
     */
    public java.lang.Integer getCatCli() {
        return catCli;
    }


    /**
     * Sets the catCli value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param catCli
     */
    public void setCatCli(java.lang.Integer catCli) {
        this.catCli = catCli;
    }


    /**
     * Gets the ccbCli value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ccbCli
     */
    public java.lang.String getCcbCli() {
        return ccbCli;
    }


    /**
     * Sets the ccbCli value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ccbCli
     */
    public void setCcbCli(java.lang.String ccbCli) {
        this.ccbCli = ccbCli;
    }


    /**
     * Gets the cifFob value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return cifFob
     */
    public java.lang.String getCifFob() {
        return cifFob;
    }


    /**
     * Sets the cifFob value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param cifFob
     */
    public void setCifFob(java.lang.String cifFob) {
        this.cifFob = cifFob;
    }


    /**
     * Gets the cnpjFilial value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return cnpjFilial
     */
    public java.lang.String getCnpjFilial() {
        return cnpjFilial;
    }


    /**
     * Sets the cnpjFilial value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param cnpjFilial
     */
    public void setCnpjFilial(java.lang.String cnpjFilial) {
        this.cnpjFilial = cnpjFilial;
    }


    /**
     * Gets the codAge value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codAge
     */
    public java.lang.String getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.String codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codCpg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codCpg
     */
    public java.lang.String getCodCpg() {
        return codCpg;
    }


    /**
     * Sets the codCpg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codCpg
     */
    public void setCodCpg(java.lang.String codCpg) {
        this.codCpg = codCpg;
    }


    /**
     * Gets the codCrp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codCrp
     */
    public java.lang.String getCodCrp() {
        return codCrp;
    }


    /**
     * Sets the codCrp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codCrp
     */
    public void setCodCrp(java.lang.String codCrp) {
        this.codCrp = codCrp;
    }


    /**
     * Gets the codCrt value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codEmp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codFcr
     */
    public java.lang.String getCodFcr() {
        return codFcr;
    }


    /**
     * Sets the codFcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codFcr
     */
    public void setCodFcr(java.lang.String codFcr) {
        this.codFcr = codFcr;
    }


    /**
     * Gets the codFil value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFpg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFrj value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codFrj
     */
    public java.lang.String getCodFrj() {
        return codFrj;
    }


    /**
     * Sets the codFrj value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codFrj
     */
    public void setCodFrj(java.lang.String codFrj) {
        this.codFrj = codFrj;
    }


    /**
     * Gets the codIn1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codIn1
     */
    public java.lang.String getCodIn1() {
        return codIn1;
    }


    /**
     * Sets the codIn1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codIn1
     */
    public void setCodIn1(java.lang.String codIn1) {
        this.codIn1 = codIn1;
    }


    /**
     * Gets the codIn2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codIn2
     */
    public java.lang.String getCodIn2() {
        return codIn2;
    }


    /**
     * Sets the codIn2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codIn2
     */
    public void setCodIn2(java.lang.String codIn2) {
        this.codIn2 = codIn2;
    }


    /**
     * Gets the codMar value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codMar
     */
    public java.lang.String getCodMar() {
        return codMar;
    }


    /**
     * Sets the codMar value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codMar
     */
    public void setCodMar(java.lang.String codMar) {
        this.codMar = codMar;
    }


    /**
     * Gets the codRed value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codRed
     */
    public java.lang.Integer getCodRed() {
        return codRed;
    }


    /**
     * Sets the codRed value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codRed
     */
    public void setCodRed(java.lang.Integer codRed) {
        this.codRed = codRed;
    }


    /**
     * Gets the codRep value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codRep
     */
    public java.lang.Integer getCodRep() {
        return codRep;
    }


    /**
     * Sets the codRep value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codRep
     */
    public void setCodRep(java.lang.Integer codRep) {
        this.codRep = codRep;
    }


    /**
     * Gets the codRve value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codRve
     */
    public java.lang.String getCodRve() {
        return codRve;
    }


    /**
     * Sets the codRve value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codRve
     */
    public void setCodRve(java.lang.String codRve) {
        this.codRve = codRve;
    }


    /**
     * Gets the codTab value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codTab
     */
    public java.lang.String getCodTab() {
        return codTab;
    }


    /**
     * Sets the codTab value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codTab
     */
    public void setCodTab(java.lang.String codTab) {
        this.codTab = codTab;
    }


    /**
     * Gets the codTic value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codTic
     */
    public java.lang.String getCodTic() {
        return codTic;
    }


    /**
     * Sets the codTic value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codTic
     */
    public void setCodTic(java.lang.String codTic) {
        this.codTic = codTic;
    }


    /**
     * Gets the codTpr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codTpr
     */
    public java.lang.String getCodTpr() {
        return codTpr;
    }


    /**
     * Sets the codTpr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codTpr
     */
    public void setCodTpr(java.lang.String codTpr) {
        this.codTpr = codTpr;
    }


    /**
     * Gets the codTra value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codTra
     */
    public java.lang.Integer getCodTra() {
        return codTra;
    }


    /**
     * Sets the codTra value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codTra
     */
    public void setCodTra(java.lang.Integer codTra) {
        this.codTra = codTra;
    }


    /**
     * Gets the codTrd value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codTrd
     */
    public java.lang.String getCodTrd() {
        return codTrd;
    }


    /**
     * Sets the codTrd value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codTrd
     */
    public void setCodTrd(java.lang.String codTrd) {
        this.codTrd = codTrd;
    }


    /**
     * Gets the codVen value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return codVen
     */
    public java.lang.Integer getCodVen() {
        return codVen;
    }


    /**
     * Sets the codVen value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param codVen
     */
    public void setCodVen(java.lang.Integer codVen) {
        this.codVen = codVen;
    }


    /**
     * Gets the criEdv value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return criEdv
     */
    public java.lang.String getCriEdv() {
        return criEdv;
    }


    /**
     * Sets the criEdv value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param criEdv
     */
    public void setCriEdv(java.lang.String criEdv) {
        this.criEdv = criEdv;
    }


    /**
     * Gets the ctaAad value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ctaAad
     */
    public java.lang.Integer getCtaAad() {
        return ctaAad;
    }


    /**
     * Sets the ctaAad value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ctaAad
     */
    public void setCtaAad(java.lang.Integer ctaAad) {
        this.ctaAad = ctaAad;
    }


    /**
     * Gets the ctaAux value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ctaAux
     */
    public java.lang.Integer getCtaAux() {
        return ctaAux;
    }


    /**
     * Sets the ctaAux value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ctaAux
     */
    public void setCtaAux(java.lang.Integer ctaAux) {
        this.ctaAux = ctaAux;
    }


    /**
     * Gets the ctaFcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ctaFcr
     */
    public java.lang.Integer getCtaFcr() {
        return ctaFcr;
    }


    /**
     * Sets the ctaFcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ctaFcr
     */
    public void setCtaFcr(java.lang.Integer ctaFcr) {
        this.ctaFcr = ctaFcr;
    }


    /**
     * Gets the ctaFdv value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ctaFdv
     */
    public java.lang.Integer getCtaFdv() {
        return ctaFdv;
    }


    /**
     * Sets the ctaFdv value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ctaFdv
     */
    public void setCtaFdv(java.lang.Integer ctaFdv) {
        this.ctaFdv = ctaFdv;
    }


    /**
     * Gets the ctaRcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ctaRcr
     */
    public java.lang.Integer getCtaRcr() {
        return ctaRcr;
    }


    /**
     * Sets the ctaRcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ctaRcr
     */
    public void setCtaRcr(java.lang.Integer ctaRcr) {
        this.ctaRcr = ctaRcr;
    }


    /**
     * Gets the ctaRed value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the ctrPad value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ctrPad
     */
    public java.lang.Integer getCtrPad() {
        return ctrPad;
    }


    /**
     * Sets the ctrPad value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ctrPad
     */
    public void setCtrPad(java.lang.Integer ctrPad) {
        this.ctrPad = ctrPad;
    }


    /**
     * Gets the datLim value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return datLim
     */
    public java.lang.String getDatLim() {
        return datLim;
    }


    /**
     * Sets the datLim value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param datLim
     */
    public void setDatLim(java.lang.String datLim) {
        this.datLim = datLim;
    }


    /**
     * Gets the datPmr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return datPmr
     */
    public java.lang.String getDatPmr() {
        return datPmr;
    }


    /**
     * Sets the datPmr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param datPmr
     */
    public void setDatPmr(java.lang.String datPmr) {
        this.datPmr = datPmr;
    }


    /**
     * Gets the diaEsp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return diaEsp
     */
    public java.lang.String getDiaEsp() {
        return diaEsp;
    }


    /**
     * Sets the diaEsp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param diaEsp
     */
    public void setDiaEsp(java.lang.String diaEsp) {
        this.diaEsp = diaEsp;
    }


    /**
     * Gets the diaMe1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return diaMe1
     */
    public java.lang.Integer getDiaMe1() {
        return diaMe1;
    }


    /**
     * Sets the diaMe1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param diaMe1
     */
    public void setDiaMe1(java.lang.Integer diaMe1) {
        this.diaMe1 = diaMe1;
    }


    /**
     * Gets the diaMe2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return diaMe2
     */
    public java.lang.Integer getDiaMe2() {
        return diaMe2;
    }


    /**
     * Sets the diaMe2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param diaMe2
     */
    public void setDiaMe2(java.lang.Integer diaMe2) {
        this.diaMe2 = diaMe2;
    }


    /**
     * Gets the diaMe3 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return diaMe3
     */
    public java.lang.Integer getDiaMe3() {
        return diaMe3;
    }


    /**
     * Sets the diaMe3 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param diaMe3
     */
    public void setDiaMe3(java.lang.Integer diaMe3) {
        this.diaMe3 = diaMe3;
    }


    /**
     * Gets the dscPrd value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return dscPrd
     */
    public java.lang.String getDscPrd() {
        return dscPrd;
    }


    /**
     * Sets the dscPrd value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param dscPrd
     */
    public void setDscPrd(java.lang.String dscPrd) {
        this.dscPrd = dscPrd;
    }


    /**
     * Gets the ecpCnp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return ecpCnp
     */
    public java.lang.String getEcpCnp() {
        return ecpCnp;
    }


    /**
     * Sets the ecpCnp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param ecpCnp
     */
    public void setEcpCnp(java.lang.String ecpCnp) {
        this.ecpCnp = ecpCnp;
    }


    /**
     * Gets the exiAge value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return exiAge
     */
    public java.lang.String getExiAge() {
        return exiAge;
    }


    /**
     * Sets the exiAge value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param exiAge
     */
    public void setExiAge(java.lang.String exiAge) {
        this.exiAge = exiAge;
    }


    /**
     * Gets the exiLcp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return exiLcp
     */
    public java.lang.String getExiLcp() {
        return exiLcp;
    }


    /**
     * Sets the exiLcp value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param exiLcp
     */
    public void setExiLcp(java.lang.String exiLcp) {
        this.exiLcp = exiLcp;
    }


    /**
     * Gets the fveCpg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return fveCpg
     */
    public java.lang.String getFveCpg() {
        return fveCpg;
    }


    /**
     * Sets the fveCpg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param fveCpg
     */
    public void setFveCpg(java.lang.String fveCpg) {
        this.fveCpg = fveCpg;
    }


    /**
     * Gets the fveFpg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return fveFpg
     */
    public java.lang.String getFveFpg() {
        return fveFpg;
    }


    /**
     * Sets the fveFpg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param fveFpg
     */
    public void setFveFpg(java.lang.String fveFpg) {
        this.fveFpg = fveFpg;
    }


    /**
     * Gets the fveTns value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return fveTns
     */
    public java.lang.String getFveTns() {
        return fveTns;
    }


    /**
     * Sets the fveTns value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param fveTns
     */
    public void setFveTns(java.lang.String fveTns) {
        this.fveTns = fveTns;
    }


    /**
     * Gets the gerTcc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return gerTcc
     */
    public java.lang.String getGerTcc() {
        return gerTcc;
    }


    /**
     * Sets the gerTcc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param gerTcc
     */
    public void setGerTcc(java.lang.String gerTcc) {
        this.gerTcc = gerTcc;
    }


    /**
     * Gets the indOrf value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return indOrf
     */
    public java.lang.String getIndOrf() {
        return indOrf;
    }


    /**
     * Sets the indOrf value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param indOrf
     */
    public void setIndOrf(java.lang.String indOrf) {
        this.indOrf = indOrf;
    }


    /**
     * Gets the limApr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return limApr
     */
    public java.lang.String getLimApr() {
        return limApr;
    }


    /**
     * Sets the limApr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param limApr
     */
    public void setLimApr(java.lang.String limApr) {
        this.limApr = limApr;
    }


    /**
     * Gets the perAqa value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perAqa
     */
    public java.lang.Integer getPerAqa() {
        return perAqa;
    }


    /**
     * Sets the perAqa value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perAqa
     */
    public void setPerAqa(java.lang.Integer perAqa) {
        this.perAqa = perAqa;
    }


    /**
     * Gets the perCcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perCcr
     */
    public java.lang.String getPerCcr() {
        return perCcr;
    }


    /**
     * Sets the perCcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perCcr
     */
    public void setPerCcr(java.lang.String perCcr) {
        this.perCcr = perCcr;
    }


    /**
     * Gets the perCom value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perCom
     */
    public java.lang.Integer getPerCom() {
        return perCom;
    }


    /**
     * Sets the perCom value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perCom
     */
    public void setPerCom(java.lang.Integer perCom) {
        this.perCom = perCom;
    }


    /**
     * Gets the perDs1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perDs1
     */
    public java.lang.Double getPerDs1() {
        return perDs1;
    }


    /**
     * Sets the perDs1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perDs1
     */
    public void setPerDs1(java.lang.Double perDs1) {
        this.perDs1 = perDs1;
    }


    /**
     * Gets the perDs2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perDs2
     */
    public java.lang.Double getPerDs2() {
        return perDs2;
    }


    /**
     * Sets the perDs2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perDs2
     */
    public void setPerDs2(java.lang.Double perDs2) {
        this.perDs2 = perDs2;
    }


    /**
     * Gets the perDs3 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perDs3
     */
    public java.lang.Double getPerDs3() {
        return perDs3;
    }


    /**
     * Sets the perDs3 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perDs3
     */
    public void setPerDs3(java.lang.Double perDs3) {
        this.perDs3 = perDs3;
    }


    /**
     * Gets the perDs4 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perDs4
     */
    public java.lang.Double getPerDs4() {
        return perDs4;
    }


    /**
     * Sets the perDs4 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perDs4
     */
    public void setPerDs4(java.lang.Double perDs4) {
        this.perDs4 = perDs4;
    }


    /**
     * Gets the perDs5 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perDs5
     */
    public java.lang.Double getPerDs5() {
        return perDs5;
    }


    /**
     * Sets the perDs5 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perDs5
     */
    public void setPerDs5(java.lang.Double perDs5) {
        this.perDs5 = perDs5;
    }


    /**
     * Gets the perDsc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perDsc
     */
    public java.lang.Double getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Double perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perEmb value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perEmb
     */
    public java.lang.Double getPerEmb() {
        return perEmb;
    }


    /**
     * Sets the perEmb value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perEmb
     */
    public void setPerEmb(java.lang.Double perEmb) {
        this.perEmb = perEmb;
    }


    /**
     * Gets the perEnc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perEnc
     */
    public java.lang.Double getPerEnc() {
        return perEnc;
    }


    /**
     * Sets the perEnc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perEnc
     */
    public void setPerEnc(java.lang.Double perEnc) {
        this.perEnc = perEnc;
    }


    /**
     * Gets the perFre value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perFre
     */
    public java.lang.Double getPerFre() {
        return perFre;
    }


    /**
     * Sets the perFre value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perFre
     */
    public void setPerFre(java.lang.Double perFre) {
        this.perFre = perFre;
    }


    /**
     * Gets the perIss value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perIss
     */
    public java.lang.Double getPerIss() {
        return perIss;
    }


    /**
     * Sets the perIss value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perIss
     */
    public void setPerIss(java.lang.Double perIss) {
        this.perIss = perIss;
    }


    /**
     * Gets the perOf1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perOf1
     */
    public java.lang.Integer getPerOf1() {
        return perOf1;
    }


    /**
     * Sets the perOf1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perOf1
     */
    public void setPerOf1(java.lang.Integer perOf1) {
        this.perOf1 = perOf1;
    }


    /**
     * Gets the perOf2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perOf2
     */
    public java.lang.Integer getPerOf2() {
        return perOf2;
    }


    /**
     * Sets the perOf2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perOf2
     */
    public void setPerOf2(java.lang.Integer perOf2) {
        this.perOf2 = perOf2;
    }


    /**
     * Gets the perOut value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perOut
     */
    public java.lang.Double getPerOut() {
        return perOut;
    }


    /**
     * Sets the perOut value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perOut
     */
    public void setPerOut(java.lang.Double perOut) {
        this.perOut = perOut;
    }


    /**
     * Gets the perSeg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return perSeg
     */
    public java.lang.Double getPerSeg() {
        return perSeg;
    }


    /**
     * Sets the perSeg value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param perSeg
     */
    public void setPerSeg(java.lang.Double perSeg) {
        this.perSeg = perSeg;
    }


    /**
     * Gets the porNa1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return porNa1
     */
    public java.lang.String getPorNa1() {
        return porNa1;
    }


    /**
     * Sets the porNa1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param porNa1
     */
    public void setPorNa1(java.lang.String porNa1) {
        this.porNa1 = porNa1;
    }


    /**
     * Gets the porNa2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return porNa2
     */
    public java.lang.String getPorNa2() {
        return porNa2;
    }


    /**
     * Sets the porNa2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param porNa2
     */
    public void setPorNa2(java.lang.String porNa2) {
        this.porNa2 = porNa2;
    }


    /**
     * Gets the porSi1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return porSi1
     */
    public java.lang.String getPorSi1() {
        return porSi1;
    }


    /**
     * Sets the porSi1 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param porSi1
     */
    public void setPorSi1(java.lang.String porSi1) {
        this.porSi1 = porSi1;
    }


    /**
     * Gets the porSi2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return porSi2
     */
    public java.lang.String getPorSi2() {
        return porSi2;
    }


    /**
     * Sets the porSi2 value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param porSi2
     */
    public void setPorSi2(java.lang.String porSi2) {
        this.porSi2 = porSi2;
    }


    /**
     * Gets the prdDsc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return prdDsc
     */
    public java.lang.String getPrdDsc() {
        return prdDsc;
    }


    /**
     * Sets the prdDsc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param prdDsc
     */
    public void setPrdDsc(java.lang.String prdDsc) {
        this.prdDsc = prdDsc;
    }


    /**
     * Gets the qtdChs value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return qtdChs
     */
    public java.lang.Integer getQtdChs() {
        return qtdChs;
    }


    /**
     * Sets the qtdChs value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param qtdChs
     */
    public void setQtdChs(java.lang.Integer qtdChs) {
        this.qtdChs = qtdChs;
    }


    /**
     * Gets the qtdDcv value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return qtdDcv
     */
    public java.lang.Integer getQtdDcv() {
        return qtdDcv;
    }


    /**
     * Sets the qtdDcv value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param qtdDcv
     */
    public void setQtdDcv(java.lang.Integer qtdDcv) {
        this.qtdDcv = qtdDcv;
    }


    /**
     * Gets the qtdPrt value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return qtdPrt
     */
    public java.lang.Integer getQtdPrt() {
        return qtdPrt;
    }


    /**
     * Sets the qtdPrt value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param qtdPrt
     */
    public void setQtdPrt(java.lang.Integer qtdPrt) {
        this.qtdPrt = qtdPrt;
    }


    /**
     * Gets the recDtj value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return recDtj
     */
    public java.lang.Integer getRecDtj() {
        return recDtj;
    }


    /**
     * Sets the recDtj value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param recDtj
     */
    public void setRecDtj(java.lang.Integer recDtj) {
        this.recDtj = recDtj;
    }


    /**
     * Gets the recDtm value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return recDtm
     */
    public java.lang.Integer getRecDtm() {
        return recDtm;
    }


    /**
     * Sets the recDtm value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param recDtm
     */
    public void setRecDtm(java.lang.Integer recDtm) {
        this.recDtm = recDtm;
    }


    /**
     * Gets the recJmm value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return recJmm
     */
    public java.lang.Double getRecJmm() {
        return recJmm;
    }


    /**
     * Sets the recJmm value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param recJmm
     */
    public void setRecJmm(java.lang.Double recJmm) {
        this.recJmm = recJmm;
    }


    /**
     * Gets the recMul value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return recMul
     */
    public java.lang.Double getRecMul() {
        return recMul;
    }


    /**
     * Sets the recMul value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param recMul
     */
    public void setRecMul(java.lang.Double recMul) {
        this.recMul = recMul;
    }


    /**
     * Gets the recTjr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return recTjr
     */
    public java.lang.String getRecTjr() {
        return recTjr;
    }


    /**
     * Sets the recTjr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param recTjr
     */
    public void setRecTjr(java.lang.String recTjr) {
        this.recTjr = recTjr;
    }


    /**
     * Gets the seqCob value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return seqCob
     */
    public java.lang.Integer getSeqCob() {
        return seqCob;
    }


    /**
     * Sets the seqCob value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param seqCob
     */
    public void setSeqCob(java.lang.Integer seqCob) {
        this.seqCob = seqCob;
    }


    /**
     * Gets the seqEnt value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return seqEnt
     */
    public java.lang.Integer getSeqEnt() {
        return seqEnt;
    }


    /**
     * Sets the seqEnt value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param seqEnt
     */
    public void setSeqEnt(java.lang.Integer seqEnt) {
        this.seqEnt = seqEnt;
    }


    /**
     * Gets the tolDsc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return tolDsc
     */
    public java.lang.Integer getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Integer tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the vlrAcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return vlrAcr
     */
    public java.lang.Double getVlrAcr() {
        return vlrAcr;
    }


    /**
     * Sets the vlrAcr value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param vlrAcr
     */
    public void setVlrAcr(java.lang.Double vlrAcr) {
        this.vlrAcr = vlrAcr;
    }


    /**
     * Gets the vlrLim value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return vlrLim
     */
    public java.lang.Double getVlrLim() {
        return vlrLim;
    }


    /**
     * Sets the vlrLim value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param vlrLim
     */
    public void setVlrLim(java.lang.Double vlrLim) {
        this.vlrLim = vlrLim;
    }


    /**
     * Gets the vlrPrt value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @return vlrPrt
     */
    public java.lang.Double getVlrPrt() {
        return vlrPrt;
    }


    /**
     * Sets the vlrPrt value for this ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.
     * 
     * @param vlrPrt
     */
    public void setVlrPrt(java.lang.Double vlrPrt) {
        this.vlrPrt = vlrPrt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente)) return false;
        ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente other = (ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.acePar==null && other.getAcePar()==null) || 
             (this.acePar!=null &&
              this.acePar.equals(other.getAcePar()))) &&
            ((this.acrDia==null && other.getAcrDia()==null) || 
             (this.acrDia!=null &&
              this.acrDia.equals(other.getAcrDia()))) &&
            ((this.antDsc==null && other.getAntDsc()==null) || 
             (this.antDsc!=null &&
              this.antDsc.equals(other.getAntDsc()))) &&
            ((this.avaAti==null && other.getAvaAti()==null) || 
             (this.avaAti!=null &&
              this.avaAti.equals(other.getAvaAti()))) &&
            ((this.avaMot==null && other.getAvaMot()==null) || 
             (this.avaMot!=null &&
              this.avaMot.equals(other.getAvaMot()))) &&
            ((this.avaObs==null && other.getAvaObs()==null) || 
             (this.avaObs!=null &&
              this.avaObs.equals(other.getAvaObs()))) &&
            ((this.avaVlr==null && other.getAvaVlr()==null) || 
             (this.avaVlr!=null &&
              this.avaVlr.equals(other.getAvaVlr()))) &&
            ((this.avaVls==null && other.getAvaVls()==null) || 
             (this.avaVls!=null &&
              this.avaVls.equals(other.getAvaVls()))) &&
            ((this.avaVlu==null && other.getAvaVlu()==null) || 
             (this.avaVlu!=null &&
              this.avaVlu.equals(other.getAvaVlu()))) &&
            ((this.catCli==null && other.getCatCli()==null) || 
             (this.catCli!=null &&
              this.catCli.equals(other.getCatCli()))) &&
            ((this.ccbCli==null && other.getCcbCli()==null) || 
             (this.ccbCli!=null &&
              this.ccbCli.equals(other.getCcbCli()))) &&
            ((this.cifFob==null && other.getCifFob()==null) || 
             (this.cifFob!=null &&
              this.cifFob.equals(other.getCifFob()))) &&
            ((this.cnpjFilial==null && other.getCnpjFilial()==null) || 
             (this.cnpjFilial!=null &&
              this.cnpjFilial.equals(other.getCnpjFilial()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codCpg==null && other.getCodCpg()==null) || 
             (this.codCpg!=null &&
              this.codCpg.equals(other.getCodCpg()))) &&
            ((this.codCrp==null && other.getCodCrp()==null) || 
             (this.codCrp!=null &&
              this.codCrp.equals(other.getCodCrp()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFcr==null && other.getCodFcr()==null) || 
             (this.codFcr!=null &&
              this.codFcr.equals(other.getCodFcr()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFrj==null && other.getCodFrj()==null) || 
             (this.codFrj!=null &&
              this.codFrj.equals(other.getCodFrj()))) &&
            ((this.codIn1==null && other.getCodIn1()==null) || 
             (this.codIn1!=null &&
              this.codIn1.equals(other.getCodIn1()))) &&
            ((this.codIn2==null && other.getCodIn2()==null) || 
             (this.codIn2!=null &&
              this.codIn2.equals(other.getCodIn2()))) &&
            ((this.codMar==null && other.getCodMar()==null) || 
             (this.codMar!=null &&
              this.codMar.equals(other.getCodMar()))) &&
            ((this.codRed==null && other.getCodRed()==null) || 
             (this.codRed!=null &&
              this.codRed.equals(other.getCodRed()))) &&
            ((this.codRep==null && other.getCodRep()==null) || 
             (this.codRep!=null &&
              this.codRep.equals(other.getCodRep()))) &&
            ((this.codRve==null && other.getCodRve()==null) || 
             (this.codRve!=null &&
              this.codRve.equals(other.getCodRve()))) &&
            ((this.codTab==null && other.getCodTab()==null) || 
             (this.codTab!=null &&
              this.codTab.equals(other.getCodTab()))) &&
            ((this.codTic==null && other.getCodTic()==null) || 
             (this.codTic!=null &&
              this.codTic.equals(other.getCodTic()))) &&
            ((this.codTpr==null && other.getCodTpr()==null) || 
             (this.codTpr!=null &&
              this.codTpr.equals(other.getCodTpr()))) &&
            ((this.codTra==null && other.getCodTra()==null) || 
             (this.codTra!=null &&
              this.codTra.equals(other.getCodTra()))) &&
            ((this.codTrd==null && other.getCodTrd()==null) || 
             (this.codTrd!=null &&
              this.codTrd.equals(other.getCodTrd()))) &&
            ((this.codVen==null && other.getCodVen()==null) || 
             (this.codVen!=null &&
              this.codVen.equals(other.getCodVen()))) &&
            ((this.criEdv==null && other.getCriEdv()==null) || 
             (this.criEdv!=null &&
              this.criEdv.equals(other.getCriEdv()))) &&
            ((this.ctaAad==null && other.getCtaAad()==null) || 
             (this.ctaAad!=null &&
              this.ctaAad.equals(other.getCtaAad()))) &&
            ((this.ctaAux==null && other.getCtaAux()==null) || 
             (this.ctaAux!=null &&
              this.ctaAux.equals(other.getCtaAux()))) &&
            ((this.ctaFcr==null && other.getCtaFcr()==null) || 
             (this.ctaFcr!=null &&
              this.ctaFcr.equals(other.getCtaFcr()))) &&
            ((this.ctaFdv==null && other.getCtaFdv()==null) || 
             (this.ctaFdv!=null &&
              this.ctaFdv.equals(other.getCtaFdv()))) &&
            ((this.ctaRcr==null && other.getCtaRcr()==null) || 
             (this.ctaRcr!=null &&
              this.ctaRcr.equals(other.getCtaRcr()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.ctrPad==null && other.getCtrPad()==null) || 
             (this.ctrPad!=null &&
              this.ctrPad.equals(other.getCtrPad()))) &&
            ((this.datLim==null && other.getDatLim()==null) || 
             (this.datLim!=null &&
              this.datLim.equals(other.getDatLim()))) &&
            ((this.datPmr==null && other.getDatPmr()==null) || 
             (this.datPmr!=null &&
              this.datPmr.equals(other.getDatPmr()))) &&
            ((this.diaEsp==null && other.getDiaEsp()==null) || 
             (this.diaEsp!=null &&
              this.diaEsp.equals(other.getDiaEsp()))) &&
            ((this.diaMe1==null && other.getDiaMe1()==null) || 
             (this.diaMe1!=null &&
              this.diaMe1.equals(other.getDiaMe1()))) &&
            ((this.diaMe2==null && other.getDiaMe2()==null) || 
             (this.diaMe2!=null &&
              this.diaMe2.equals(other.getDiaMe2()))) &&
            ((this.diaMe3==null && other.getDiaMe3()==null) || 
             (this.diaMe3!=null &&
              this.diaMe3.equals(other.getDiaMe3()))) &&
            ((this.dscPrd==null && other.getDscPrd()==null) || 
             (this.dscPrd!=null &&
              this.dscPrd.equals(other.getDscPrd()))) &&
            ((this.ecpCnp==null && other.getEcpCnp()==null) || 
             (this.ecpCnp!=null &&
              this.ecpCnp.equals(other.getEcpCnp()))) &&
            ((this.exiAge==null && other.getExiAge()==null) || 
             (this.exiAge!=null &&
              this.exiAge.equals(other.getExiAge()))) &&
            ((this.exiLcp==null && other.getExiLcp()==null) || 
             (this.exiLcp!=null &&
              this.exiLcp.equals(other.getExiLcp()))) &&
            ((this.fveCpg==null && other.getFveCpg()==null) || 
             (this.fveCpg!=null &&
              this.fveCpg.equals(other.getFveCpg()))) &&
            ((this.fveFpg==null && other.getFveFpg()==null) || 
             (this.fveFpg!=null &&
              this.fveFpg.equals(other.getFveFpg()))) &&
            ((this.fveTns==null && other.getFveTns()==null) || 
             (this.fveTns!=null &&
              this.fveTns.equals(other.getFveTns()))) &&
            ((this.gerTcc==null && other.getGerTcc()==null) || 
             (this.gerTcc!=null &&
              this.gerTcc.equals(other.getGerTcc()))) &&
            ((this.indOrf==null && other.getIndOrf()==null) || 
             (this.indOrf!=null &&
              this.indOrf.equals(other.getIndOrf()))) &&
            ((this.limApr==null && other.getLimApr()==null) || 
             (this.limApr!=null &&
              this.limApr.equals(other.getLimApr()))) &&
            ((this.perAqa==null && other.getPerAqa()==null) || 
             (this.perAqa!=null &&
              this.perAqa.equals(other.getPerAqa()))) &&
            ((this.perCcr==null && other.getPerCcr()==null) || 
             (this.perCcr!=null &&
              this.perCcr.equals(other.getPerCcr()))) &&
            ((this.perCom==null && other.getPerCom()==null) || 
             (this.perCom!=null &&
              this.perCom.equals(other.getPerCom()))) &&
            ((this.perDs1==null && other.getPerDs1()==null) || 
             (this.perDs1!=null &&
              this.perDs1.equals(other.getPerDs1()))) &&
            ((this.perDs2==null && other.getPerDs2()==null) || 
             (this.perDs2!=null &&
              this.perDs2.equals(other.getPerDs2()))) &&
            ((this.perDs3==null && other.getPerDs3()==null) || 
             (this.perDs3!=null &&
              this.perDs3.equals(other.getPerDs3()))) &&
            ((this.perDs4==null && other.getPerDs4()==null) || 
             (this.perDs4!=null &&
              this.perDs4.equals(other.getPerDs4()))) &&
            ((this.perDs5==null && other.getPerDs5()==null) || 
             (this.perDs5!=null &&
              this.perDs5.equals(other.getPerDs5()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perEmb==null && other.getPerEmb()==null) || 
             (this.perEmb!=null &&
              this.perEmb.equals(other.getPerEmb()))) &&
            ((this.perEnc==null && other.getPerEnc()==null) || 
             (this.perEnc!=null &&
              this.perEnc.equals(other.getPerEnc()))) &&
            ((this.perFre==null && other.getPerFre()==null) || 
             (this.perFre!=null &&
              this.perFre.equals(other.getPerFre()))) &&
            ((this.perIss==null && other.getPerIss()==null) || 
             (this.perIss!=null &&
              this.perIss.equals(other.getPerIss()))) &&
            ((this.perOf1==null && other.getPerOf1()==null) || 
             (this.perOf1!=null &&
              this.perOf1.equals(other.getPerOf1()))) &&
            ((this.perOf2==null && other.getPerOf2()==null) || 
             (this.perOf2!=null &&
              this.perOf2.equals(other.getPerOf2()))) &&
            ((this.perOut==null && other.getPerOut()==null) || 
             (this.perOut!=null &&
              this.perOut.equals(other.getPerOut()))) &&
            ((this.perSeg==null && other.getPerSeg()==null) || 
             (this.perSeg!=null &&
              this.perSeg.equals(other.getPerSeg()))) &&
            ((this.porNa1==null && other.getPorNa1()==null) || 
             (this.porNa1!=null &&
              this.porNa1.equals(other.getPorNa1()))) &&
            ((this.porNa2==null && other.getPorNa2()==null) || 
             (this.porNa2!=null &&
              this.porNa2.equals(other.getPorNa2()))) &&
            ((this.porSi1==null && other.getPorSi1()==null) || 
             (this.porSi1!=null &&
              this.porSi1.equals(other.getPorSi1()))) &&
            ((this.porSi2==null && other.getPorSi2()==null) || 
             (this.porSi2!=null &&
              this.porSi2.equals(other.getPorSi2()))) &&
            ((this.prdDsc==null && other.getPrdDsc()==null) || 
             (this.prdDsc!=null &&
              this.prdDsc.equals(other.getPrdDsc()))) &&
            ((this.qtdChs==null && other.getQtdChs()==null) || 
             (this.qtdChs!=null &&
              this.qtdChs.equals(other.getQtdChs()))) &&
            ((this.qtdDcv==null && other.getQtdDcv()==null) || 
             (this.qtdDcv!=null &&
              this.qtdDcv.equals(other.getQtdDcv()))) &&
            ((this.qtdPrt==null && other.getQtdPrt()==null) || 
             (this.qtdPrt!=null &&
              this.qtdPrt.equals(other.getQtdPrt()))) &&
            ((this.recDtj==null && other.getRecDtj()==null) || 
             (this.recDtj!=null &&
              this.recDtj.equals(other.getRecDtj()))) &&
            ((this.recDtm==null && other.getRecDtm()==null) || 
             (this.recDtm!=null &&
              this.recDtm.equals(other.getRecDtm()))) &&
            ((this.recJmm==null && other.getRecJmm()==null) || 
             (this.recJmm!=null &&
              this.recJmm.equals(other.getRecJmm()))) &&
            ((this.recMul==null && other.getRecMul()==null) || 
             (this.recMul!=null &&
              this.recMul.equals(other.getRecMul()))) &&
            ((this.recTjr==null && other.getRecTjr()==null) || 
             (this.recTjr!=null &&
              this.recTjr.equals(other.getRecTjr()))) &&
            ((this.seqCob==null && other.getSeqCob()==null) || 
             (this.seqCob!=null &&
              this.seqCob.equals(other.getSeqCob()))) &&
            ((this.seqEnt==null && other.getSeqEnt()==null) || 
             (this.seqEnt!=null &&
              this.seqEnt.equals(other.getSeqEnt()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.vlrAcr==null && other.getVlrAcr()==null) || 
             (this.vlrAcr!=null &&
              this.vlrAcr.equals(other.getVlrAcr()))) &&
            ((this.vlrLim==null && other.getVlrLim()==null) || 
             (this.vlrLim!=null &&
              this.vlrLim.equals(other.getVlrLim()))) &&
            ((this.vlrPrt==null && other.getVlrPrt()==null) || 
             (this.vlrPrt!=null &&
              this.vlrPrt.equals(other.getVlrPrt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAcePar() != null) {
            _hashCode += getAcePar().hashCode();
        }
        if (getAcrDia() != null) {
            _hashCode += getAcrDia().hashCode();
        }
        if (getAntDsc() != null) {
            _hashCode += getAntDsc().hashCode();
        }
        if (getAvaAti() != null) {
            _hashCode += getAvaAti().hashCode();
        }
        if (getAvaMot() != null) {
            _hashCode += getAvaMot().hashCode();
        }
        if (getAvaObs() != null) {
            _hashCode += getAvaObs().hashCode();
        }
        if (getAvaVlr() != null) {
            _hashCode += getAvaVlr().hashCode();
        }
        if (getAvaVls() != null) {
            _hashCode += getAvaVls().hashCode();
        }
        if (getAvaVlu() != null) {
            _hashCode += getAvaVlu().hashCode();
        }
        if (getCatCli() != null) {
            _hashCode += getCatCli().hashCode();
        }
        if (getCcbCli() != null) {
            _hashCode += getCcbCli().hashCode();
        }
        if (getCifFob() != null) {
            _hashCode += getCifFob().hashCode();
        }
        if (getCnpjFilial() != null) {
            _hashCode += getCnpjFilial().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodCpg() != null) {
            _hashCode += getCodCpg().hashCode();
        }
        if (getCodCrp() != null) {
            _hashCode += getCodCrp().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFcr() != null) {
            _hashCode += getCodFcr().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFrj() != null) {
            _hashCode += getCodFrj().hashCode();
        }
        if (getCodIn1() != null) {
            _hashCode += getCodIn1().hashCode();
        }
        if (getCodIn2() != null) {
            _hashCode += getCodIn2().hashCode();
        }
        if (getCodMar() != null) {
            _hashCode += getCodMar().hashCode();
        }
        if (getCodRed() != null) {
            _hashCode += getCodRed().hashCode();
        }
        if (getCodRep() != null) {
            _hashCode += getCodRep().hashCode();
        }
        if (getCodRve() != null) {
            _hashCode += getCodRve().hashCode();
        }
        if (getCodTab() != null) {
            _hashCode += getCodTab().hashCode();
        }
        if (getCodTic() != null) {
            _hashCode += getCodTic().hashCode();
        }
        if (getCodTpr() != null) {
            _hashCode += getCodTpr().hashCode();
        }
        if (getCodTra() != null) {
            _hashCode += getCodTra().hashCode();
        }
        if (getCodTrd() != null) {
            _hashCode += getCodTrd().hashCode();
        }
        if (getCodVen() != null) {
            _hashCode += getCodVen().hashCode();
        }
        if (getCriEdv() != null) {
            _hashCode += getCriEdv().hashCode();
        }
        if (getCtaAad() != null) {
            _hashCode += getCtaAad().hashCode();
        }
        if (getCtaAux() != null) {
            _hashCode += getCtaAux().hashCode();
        }
        if (getCtaFcr() != null) {
            _hashCode += getCtaFcr().hashCode();
        }
        if (getCtaFdv() != null) {
            _hashCode += getCtaFdv().hashCode();
        }
        if (getCtaRcr() != null) {
            _hashCode += getCtaRcr().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getCtrPad() != null) {
            _hashCode += getCtrPad().hashCode();
        }
        if (getDatLim() != null) {
            _hashCode += getDatLim().hashCode();
        }
        if (getDatPmr() != null) {
            _hashCode += getDatPmr().hashCode();
        }
        if (getDiaEsp() != null) {
            _hashCode += getDiaEsp().hashCode();
        }
        if (getDiaMe1() != null) {
            _hashCode += getDiaMe1().hashCode();
        }
        if (getDiaMe2() != null) {
            _hashCode += getDiaMe2().hashCode();
        }
        if (getDiaMe3() != null) {
            _hashCode += getDiaMe3().hashCode();
        }
        if (getDscPrd() != null) {
            _hashCode += getDscPrd().hashCode();
        }
        if (getEcpCnp() != null) {
            _hashCode += getEcpCnp().hashCode();
        }
        if (getExiAge() != null) {
            _hashCode += getExiAge().hashCode();
        }
        if (getExiLcp() != null) {
            _hashCode += getExiLcp().hashCode();
        }
        if (getFveCpg() != null) {
            _hashCode += getFveCpg().hashCode();
        }
        if (getFveFpg() != null) {
            _hashCode += getFveFpg().hashCode();
        }
        if (getFveTns() != null) {
            _hashCode += getFveTns().hashCode();
        }
        if (getGerTcc() != null) {
            _hashCode += getGerTcc().hashCode();
        }
        if (getIndOrf() != null) {
            _hashCode += getIndOrf().hashCode();
        }
        if (getLimApr() != null) {
            _hashCode += getLimApr().hashCode();
        }
        if (getPerAqa() != null) {
            _hashCode += getPerAqa().hashCode();
        }
        if (getPerCcr() != null) {
            _hashCode += getPerCcr().hashCode();
        }
        if (getPerCom() != null) {
            _hashCode += getPerCom().hashCode();
        }
        if (getPerDs1() != null) {
            _hashCode += getPerDs1().hashCode();
        }
        if (getPerDs2() != null) {
            _hashCode += getPerDs2().hashCode();
        }
        if (getPerDs3() != null) {
            _hashCode += getPerDs3().hashCode();
        }
        if (getPerDs4() != null) {
            _hashCode += getPerDs4().hashCode();
        }
        if (getPerDs5() != null) {
            _hashCode += getPerDs5().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerEmb() != null) {
            _hashCode += getPerEmb().hashCode();
        }
        if (getPerEnc() != null) {
            _hashCode += getPerEnc().hashCode();
        }
        if (getPerFre() != null) {
            _hashCode += getPerFre().hashCode();
        }
        if (getPerIss() != null) {
            _hashCode += getPerIss().hashCode();
        }
        if (getPerOf1() != null) {
            _hashCode += getPerOf1().hashCode();
        }
        if (getPerOf2() != null) {
            _hashCode += getPerOf2().hashCode();
        }
        if (getPerOut() != null) {
            _hashCode += getPerOut().hashCode();
        }
        if (getPerSeg() != null) {
            _hashCode += getPerSeg().hashCode();
        }
        if (getPorNa1() != null) {
            _hashCode += getPorNa1().hashCode();
        }
        if (getPorNa2() != null) {
            _hashCode += getPorNa2().hashCode();
        }
        if (getPorSi1() != null) {
            _hashCode += getPorSi1().hashCode();
        }
        if (getPorSi2() != null) {
            _hashCode += getPorSi2().hashCode();
        }
        if (getPrdDsc() != null) {
            _hashCode += getPrdDsc().hashCode();
        }
        if (getQtdChs() != null) {
            _hashCode += getQtdChs().hashCode();
        }
        if (getQtdDcv() != null) {
            _hashCode += getQtdDcv().hashCode();
        }
        if (getQtdPrt() != null) {
            _hashCode += getQtdPrt().hashCode();
        }
        if (getRecDtj() != null) {
            _hashCode += getRecDtj().hashCode();
        }
        if (getRecDtm() != null) {
            _hashCode += getRecDtm().hashCode();
        }
        if (getRecJmm() != null) {
            _hashCode += getRecJmm().hashCode();
        }
        if (getRecMul() != null) {
            _hashCode += getRecMul().hashCode();
        }
        if (getRecTjr() != null) {
            _hashCode += getRecTjr().hashCode();
        }
        if (getSeqCob() != null) {
            _hashCode += getSeqCob().hashCode();
        }
        if (getSeqEnt() != null) {
            _hashCode += getSeqEnt().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getVlrAcr() != null) {
            _hashCode += getVlrAcr().hashCode();
        }
        if (getVlrLim() != null) {
            _hashCode += getVlrLim().hashCode();
        }
        if (getVlrPrt() != null) {
            _hashCode += getVlrPrt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesGravarClientesInDadosGeraisClienteDefinicoesCliente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesInDadosGeraisClienteDefinicoesCliente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acePar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acePar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acrDia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acrDia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "antDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaAti");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaAti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaVlr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaVlr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaVls");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaVls"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaVlu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaVlu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccbCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ccbCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cifFob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cifFob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnpjFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cnpjFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codIn1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codIn1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codIn2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codIn2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRve");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRve"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTab");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTab"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTrd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTrd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codVen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codVen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criEdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "criEdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaAad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaAad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaAux");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaAux"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctrPad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctrPad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datLim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datLim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPmr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPmr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaEsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaEsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaMe1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaMe1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaMe2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaMe2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaMe3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaMe3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscPrd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscPrd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ecpCnp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ecpCnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exiAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "exiAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exiLcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "exiLcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fveCpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fveCpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fveFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fveFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fveTns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fveTns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gerTcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gerTcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indOrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indOrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perAqa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perAqa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perEmb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perEmb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perOf1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perOf1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perOf2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perOf2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perSeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perSeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porNa1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porNa1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porNa2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porNa2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porSi1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porSi1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porSi2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porSi2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prdDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prdDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdChs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdChs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdDcv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdDcv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdPrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdPrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDtj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDtj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDtm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDtm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recJmm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recJmm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recTjr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recTjr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrLim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrLim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrPrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrPrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
