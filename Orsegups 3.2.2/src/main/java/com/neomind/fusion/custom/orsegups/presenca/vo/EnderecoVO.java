package com.neomind.fusion.custom.orsegups.presenca.vo;


public class EnderecoVO
{
	private String logradouro;
	private String numero;
	private String pais;
	private String cidade;
	private String uf;
	private String bairro;
	private String cep;
	private String complemento;
	
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	@Override
	public String toString()
	{
		StringBuilder end = new StringBuilder();
		end.append(logradouro.toUpperCase());
		
		if (numero != null &&!numero.equals("") && !numero.equals(" "))
		{
			end.append(" - Nº ");
			end.append(numero.toUpperCase());
		}
		
		if (complemento != null && !complemento.equals("")  && !complemento.equals(" "))
		{
			end.append(" - ");
			end.append(complemento.toUpperCase());
		}
		return end.toString();
	}
}