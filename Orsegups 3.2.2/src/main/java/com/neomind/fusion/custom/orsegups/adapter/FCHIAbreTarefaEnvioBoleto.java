package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCHIAbreTarefaEnvioBoleto implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{		
		try
		{
			 	Long codCli = (Long) processEntity.findValue("contratoSapiens.codcli");
			 	String nomeCliente = (String) processEntity.findValue("contratoSapiens.nomcli");
			 	Long cgcCpf = (Long) processEntity.findValue("contratoSapiens.cgccpf");
			 	Long numCotrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");
			 	
				String solicitante = "fernanda.maciel";
				String titulo = "Enviar Boleto Cliente";
				String descricao = "Mediante tarefa "+activity.getCode()+" do processo Cobrança Humana Inadimplência, enviar o boleto para o seguinte cliente: \n" + 
								   "Código Cliente: "+codCli+"\n Nome Cliente: "+nomeCliente+"\n CGCCPF: "+cgcCpf+"\n Contrato: "+numCotrato;
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				NeoPaper papel = new NeoPaper();
				String executor = "";
				
				papel = papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","Responsavel Envio Boleto"));
				
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						break;
					}
				}
				else
				{
					System.out.println("Papel " + papel.getCode() + " sem usuário definido!");
					throw new WorkflowException("Papel " + papel.getCode() + " sem usuário definido!");
				}

				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro ao Abrir tarefa Simples para Envio de Boleto!");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
}

