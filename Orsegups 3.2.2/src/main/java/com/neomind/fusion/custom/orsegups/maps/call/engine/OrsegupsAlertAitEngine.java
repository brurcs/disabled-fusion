package com.neomind.fusion.custom.orsegups.maps.call.engine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.jms.NeoJMSPublisher;
import com.neomind.fusion.custom.orsegups.maps.call.vo.CallAlertAitVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLNotInFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class OrsegupsAlertAitEngine
{
	private static final Log log = LogFactory.getLog(OrsegupsAlertAitEngine.class);

	private static OrsegupsAlertAitEngine instance;

	public static Map<String, CallAlertAitVO> waitingSnepResponseMap;

	public static Map<String, CallAlertAitVO> successCallMap;

	public static List<CallAlertAitVO> listaAitVOs;

	public static TreeMap<Integer, CallAlertAitVO> treeMap;

	private static Boolean verificandoViaturas = Boolean.FALSE;

	private static Boolean verificaViaturas = Boolean.FALSE;

	private static String comboMotivoAlarme;

	private OrsegupsAlertAitEngine()
	{
		OrsegupsAlertAitEngine.waitingSnepResponseMap = new HashMap<String, CallAlertAitVO>();
		OrsegupsAlertAitEngine.successCallMap = new HashMap<String, CallAlertAitVO>();
		OrsegupsAlertAitEngine.listaAitVOs = new ArrayList<CallAlertAitVO>();
		OrsegupsAlertAitEngine.treeMap = new TreeMap<Integer, CallAlertAitVO>();
	}

	public static synchronized OrsegupsAlertAitEngine getInstance()
	{
		if (OrsegupsAlertAitEngine.instance == null)
		{
			OrsegupsAlertAitEngine.instance = new OrsegupsAlertAitEngine();
		}

		return OrsegupsAlertAitEngine.instance;
	}

	public static String alert(CallAlertAitVO alertVOs, String extensionNumber, String externalNumber, String status)
	{
		// realizar a chamada deste metodo quando os alertas ocorrerem.
		//Implementado em OrsegupsMapsServlets
		// quando for alerta que não tenha um evento envolvido passar o parametro eventoVO como null
		String retorno = null;
		try
		{

			if (!OrsegupsAlertAitEngine.verificandoViaturas)
			{
				OrsegupsAlertAitEngine.verificandoViaturas = Boolean.TRUE;

				ViaturaVO viaturaVO = new ViaturaVO();
				viaturaVO = (ViaturaVO) getViatura(alertVOs.getCallViaturaVO().getCodigoViatura(), null, null, null, null, null, null, null, null, null);
				StringBuilder buttonInfoWindow = new StringBuilder();
				buttonInfoWindow.append("<div>");
				buttonInfoWindow.append("<a href=\"javascript:eventCallHandlerClose(" + extensionNumber + ", " + externalNumber + ", 0,'" + alertVOs.getCallViaturaVO().getCodigoViatura() + "');\" class=\"myButton\" >Sucesso</a>");
				buttonInfoWindow.append("&nbsp;<a href=\"javascript:eventCallHandlerClose(" + extensionNumber + ", " + externalNumber + ", 5,'" + alertVOs.getCallViaturaVO().getCodigoViatura() + "');\" class=\"myButton\"  >Ocupado</a>");
				buttonInfoWindow.append("&nbsp;<a href=\"javascript:eventCallHandlerClose(" + extensionNumber + ", " + externalNumber + ", 1,'" + alertVOs.getCallViaturaVO().getCodigoViatura() + "');\" class=\"myButton\" >Não atende</a>");
				buttonInfoWindow.append("&nbsp;<a href=\"javascript:eventCallHandlerClose(" + extensionNumber + ", " + externalNumber + ", 4,'" + alertVOs.getCallViaturaVO().getCodigoViatura() + "');\" class=\"myButton\"  >Caixa postal</a>");
				if (viaturaVO != null && viaturaVO.getCodigoStatus() == 0L)
					buttonInfoWindow.append("&nbsp;<a href=\"javascript:eventCallHandlerClose(" + extensionNumber + ", " + externalNumber + ", 7,'" + alertVOs.getCallViaturaVO().getCodigoViatura() + "');\" class=\"myButton\"  >30 min</a>");
				else
					buttonInfoWindow.append("&nbsp;<a href=\"javascript:eventCallHandlerClose(" + extensionNumber + ", " + externalNumber + ", 6,'" + alertVOs.getCallViaturaVO().getCodigoViatura() + "');\" class=\"myButton\"  >Não Existe</a>");
				buttonInfoWindow.append("<a href=\"javascript:eventCallHandlerClose(" + extensionNumber + ", " + externalNumber + ", 8,'" + alertVOs.getCallViaturaVO().getCodigoViatura() + "');\" class=\"myButton\" >Aguardar 30</a>");
				
				buttonInfoWindow.append("</div><br>");
				if(status != null && !status.isEmpty())
				viaturaVO.setStatusVtr(status);
				//viaturaInfoWindow += "";
				CallAlertAitVO alertVOAit = new CallAlertAitVO();

				alertVOAit.setCallViaturaVO(viaturaVO);
				alertVOAit.setCallEventoVO(alertVOs.getCallEventoVO());
				alertVOAit.setButtonDiv(buttonInfoWindow.toString());
				alertVOAit.setLastCallCalendar(alertVOs.getLastCallCalendar());

				if (extensionNumber != null && externalNumber != null && OrsegupsAlertAitEngine.successCallMap != null && !OrsegupsAlertAitEngine.successCallMap.containsKey(extensionNumber))
				{
					//valida se pode realizar a ligação para o AIT

					if (hasToCall(externalNumber, alertVOAit))//!OrsegupsAlertAitEngine.waitingSnepResponseMap.containsKey(externalNumber)
					{
						CallAlertAitVO alertVO = new CallAlertAitVO();
						if (OrsegupsAlertAitEngine.waitingSnepResponseMap != null && OrsegupsAlertAitEngine.waitingSnepResponseMap.containsKey(externalNumber))
						{
							alertVO = OrsegupsAlertAitEngine.waitingSnepResponseMap.get(externalNumber);
							alertVO.setCallViaturaVO(alertVOAit.getCallViaturaVO());
							alertVO.setCallEventoVO(alertVOAit.getCallEventoVO());
							alertVO.setButtonDiv(alertVOAit.getButtonDiv());
						}
						else
						{
							alertVO = alertVOAit;
						}
						EventoVO eventoVO = null;
						if (alertVO.getCallEventoVO().getCodigoHistorico() != null)
							eventoVO = (EventoVO) OrsegupsAlertAitEngine.getEventoHistorico(alertVO.getCallEventoVO().getCodigoHistorico());
						alertVO.setLastCallCalendar(new GregorianCalendar());
						boolean flag = true;
						for (CallAlertAitVO value : OrsegupsAlertAitEngine.successCallMap.values())
						{
							if (value.getCallEventoVO().getCodigoHistorico().equals(alertVO.getCallEventoVO().getCodigoHistorico()))
							{
								flag = false;
							}
						}
						if (externalNumber != null && !externalNumber.isEmpty() && alertVO != null && eventoVO != null && flag)
						{
							OrsegupsAlertAitEngine.waitingSnepResponseMap.put(externalNumber, alertVO);
							receiveCall(extensionNumber, externalNumber, alertVO, 0l, alertVO);
							callAit(extensionNumber, externalNumber, alertVO.getCallViaturaVO().getCodigoViatura());
							OrsegupsAlertAitEngine.successCallMap.put(extensionNumber, alertVO);
							log.debug("Ligação do ramal " + extensionNumber + " para o telefone " + externalNumber + " evento: " + alertVOAit.getCallEventoVO().getCodigoHistorico());

							retorno = "Ok";
						}
						else
						{
							if (OrsegupsAlertAitEngine.waitingSnepResponseMap.containsKey(externalNumber))
								OrsegupsAlertAitEngine.waitingSnepResponseMap.remove(externalNumber);
							if (OrsegupsAlertAitEngine.successCallMap.containsKey(extensionNumber))
								OrsegupsAlertAitEngine.successCallMap.remove(extensionNumber);
						}
					}
				}

			}
		}
		catch (Exception e)
		{
			retorno = "Erro";
			e.printStackTrace();
			log.error("Erro ao executar procedimento padrão AIT." + e);

		}
		finally
		{
			if (OrsegupsAlertAitEngine.verificandoViaturas)
				OrsegupsAlertAitEngine.verificandoViaturas = Boolean.FALSE;
		}
		if (retorno == null)
		    retorno = "Vazio";
		return retorno;

	}

	private static void callAit(String extensionNumber, String externalNumber, String codigoViatura)
	{

		try
		{

			String message = "";
	
			String ramalOrigem = extensionNumber;
			String ramalDestino = externalNumber;

			String url = "http://192.168.20.241/snep/services/?service=Dial&ramal=" + ramalOrigem + "&exten=" + ramalDestino;

			if (ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty())
			{

				if (ramalDestino.equals(ramalOrigem))
				{
					message = "Erro ao efetuar chamada.\nRamal de origem igual ao ramal de destino.";
				}
				else
				{
					StringBuilder buffer = callUrl(url);

					String result = buffer.toString();
					result = result.replaceAll("\\{", "");
					result = result.replaceAll("\\}", "");
					String[] params = result.split(",");

					if (params[0] != null && params[0].contains("ok"))
					{
						message = "Chamada efetuada a partir do ramal " + ramalOrigem + " para o telefone " + ramalDestino + ".";

					}
					else
					{
						String erro[] = params[1].split(":");

						message = "Erro ao efetuar chamada.\n" + erro[1];
					}
				}
				addAitLog(codigoViatura, "Ligando para AIT devido ao alerta de deslocamento " + message);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao efetuar a ligação!" + e);
		}
	}

	public static void receiveCall(String extensionNumber, String externalNumber, CallAlertAitVO callStatus, Long time, CallAlertAitVO alertVOS)
	{
		try
		{
			//busca o alerta referente a ligação que está sendo entregue ao operador
			CallAlertAitVO alertVO = OrsegupsAlertAitEngine.waitingSnepResponseMap.get(externalNumber);
			if (alertVO != null && extensionNumber != null && !extensionNumber.isEmpty() && !OrsegupsAlertAitEngine.successCallMap.containsKey(extensionNumber))
			{
				Gson gson = new Gson();
				String alertVoJSON = null;
				if (alertVOS.getCallViaturaVO() != null)
					alertVO.setCallViaturaVO(alertVOS.getCallViaturaVO());
				if (alertVOS.getCallEventoVO() != null)
					alertVO.setCallEventoVO(alertVOS.getCallEventoVO());
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.add(Calendar.MINUTE, +3);
				String dataDiscagem = NeoDateUtils.safeDateFormat(calendar, "dd/MM/yyyy HH:mm:ss");
				System.out.println(dataDiscagem);
				alertVO.setDiscCallCalendar(dataDiscagem);
				alertVoJSON = gson.toJson(alertVO);
				log.debug("Antes de executar receiveCall AIT evento " + alertVO.getCallEventoVO().getCodigoHistorico() + " ramal: " + extensionNumber);
				System.out.println("Antes de executar receiveCall AIT evento " + alertVO.getCallEventoVO().getCodigoHistorico() + " ramal: " + extensionNumber);
				NeoJMSPublisher.getInstance().sendAlertAitMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), extensionNumber);
				log.debug("Depois de executar receiveCall AIT   ");
				System.out.println("Depois de executar receiveCall AIT   ");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro de executar receiveCall AIT  " + e);
		}
	}

	public static void alertAitClose(String ramal, String telefone, CallAlertAitStatus callStatus)
	{

		try
		{

			if (NeoUtils.safeIsNotNull(ramal) && NeoUtils.safeIsNotNull(telefone) && OrsegupsAlertAitEngine.successCallMap.containsKey(ramal))
			{
				CallAlertAitVO successAlertVO = OrsegupsAlertAitEngine.successCallMap.get(ramal);
				GregorianCalendar gregorianCalendar = new GregorianCalendar();
				if (NeoUtils.safeIsNotNull(successAlertVO))
				{
					String codigoViatura = successAlertVO.getCallViaturaVO().getCodigoViatura();

					if (callStatus.equals(CallAlertAitStatus.ATENDIDA))
					{
						addAitLog(codigoViatura, " Ligação atendida pelo AIT.");
					}
					else
					{
						gregorianCalendar.setTime(new GregorianCalendar().getTime());
						gregorianCalendar.add(Calendar.MINUTE, -1);
					}
					if (callStatus.equals(CallAlertAitStatus.NAO_ATENDIDA))
					{
						addAitLog(codigoViatura, " Ligação não atendida pelo AIT.");

					}
					else if (callStatus.equals(CallAlertAitStatus.ATENDIDA_DESLIGADA))
					{
						addAitLog(codigoViatura, " Ligação foi atendida e desligada pelo AIT ente de falar com operador.");
					}
					else if (callStatus.equals(CallAlertAitStatus.PROXIMA_LIGACAO))
					{
						addAitLog(codigoViatura, "Efeturar próxima ligação, varias tentativas sem sucesso!");
					}
					else if (callStatus.equals(CallAlertAitStatus.OCUPADO))
					{
						addAitLog(codigoViatura, "Falha ao efetuar a ligação, canal ocupado!");
					}
					else if (callStatus.equals(CallAlertAitStatus.CAIXA_POSTAL))
					{
						addAitLog(codigoViatura, "Falha ao efetuar a ligação, caixa postal!");
					}
					else if (callStatus.equals(CallAlertAitStatus.NAO_EXISTE))
					{
						addAitLog(codigoViatura, "Falha ao efetuar a ligação, numero não existe!");
					}
					else if (callStatus.equals(CallAlertAitStatus.EM_ESPERA))
					{
						addAitLog(codigoViatura, "Alerta pausado por 30 min em virtude de problema com rastreador!");
						gregorianCalendar.setTime(new GregorianCalendar().getTime());
						gregorianCalendar.add(Calendar.MINUTE, 30);
					}else if (callStatus.equals(CallAlertAitStatus.AGUARDAR_30)) {
						addAitLog(codigoViatura, "Aguardar 30 minutos devido arrombamento e/ou espera do cliente!");
						gregorianCalendar.setTime(new GregorianCalendar().getTime());
						gregorianCalendar.add(Calendar.MINUTE, 30);
					}
					successAlertVO.setLastCallCalendar(gregorianCalendar);
					if (successAlertVO.getCallViaturaVO() != null && successAlertVO.getCallViaturaVO().getCodigoViatura() != null)
						saveLogFim("Ligando para AIT devido ao alerta de deslocamento", successAlertVO.getCallViaturaVO().getCodigoViatura());
					if (OrsegupsAlertAitEngine.successCallMap != null && OrsegupsAlertAitEngine.successCallMap.containsKey(ramal))
						OrsegupsAlertAitEngine.successCallMap.remove(ramal);

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao fechar atendimento AIT " + e);
		}
	}

	public static void addAitLog(String placa, String message)
	{
		// logica para persistir a mensagem no log da viatura
		try
		{
			saveLog(message, placa);
			log.debug("Adicionando ao log da viatura! ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.debug("Erro  ao adicionar log da viatura! " + e);
		}

	}

	//testa para verificar se o alerta não está esperando retorno do Snep
	public static Boolean hasToCall(String aitTelephoNumber, CallAlertAitVO alertVOAit)
	{
		Boolean call = Boolean.FALSE;
		int minutesSinceLastCall = -1;
		int secondsSinceLastCall = -1;
		try
		{

			//verifica se ja passou 3 minutos desde a ultima ligação para este AIT
			CallAlertAitVO successAlertVO = OrsegupsAlertAitEngine.waitingSnepResponseMap.get(aitTelephoNumber);
			if (successAlertVO != null)
			{
				minutesSinceLastCall = Minutes.minutesBetween(new DateTime(successAlertVO.getLastCallCalendar()), new DateTime(new GregorianCalendar())).getMinutes();
				secondsSinceLastCall = Seconds.secondsBetween(new DateTime(successAlertVO.getLastCallCalendar()), new DateTime(new GregorianCalendar())).getSeconds();

				log.debug("Ultima ligação realizada a " + minutesSinceLastCall + ":" + secondsSinceLastCall % 60 + " minutos. Para o evento: " + successAlertVO.getCallEventoVO().getCodigoHistorico());

				if (minutesSinceLastCall > 2)
				{
					call = Boolean.TRUE;
				}

			}
			else
			{
				minutesSinceLastCall = Minutes.minutesBetween(new DateTime(alertVOAit.getLastCallCalendar()), new DateTime(new GregorianCalendar())).getMinutes();
				secondsSinceLastCall = Seconds.secondsBetween(new DateTime(alertVOAit.getLastCallCalendar()), new DateTime(new GregorianCalendar())).getSeconds();

				if (minutesSinceLastCall > 2 || minutesSinceLastCall == 0)
				{
					call = Boolean.TRUE;
				}
			}

			log.debug("Realizar ligação AIT: " + call);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao realizar ligação AIT: " + call + " " + e);
		}
		return call;

	}

	public void clearCache()
	{
		try
		{

			OrsegupsAlertAitEngine.waitingSnepResponseMap.clear();
			OrsegupsAlertAitEngine.successCallMap.clear();
			OrsegupsAlertAitEngine.listaAitVOs.clear();
			OrsegupsAlertAitEngine.treeMap.clear();
			OrsegupsAlertAitEngine.verificandoViaturas = false;
			OrsegupsAlertAitEngine.verificaViaturas = false;
			OrsegupsAlertAitEngine.instance = null;

			log.debug("Executar limpeza das listas procedimento AIT");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar limpeza das listas procedimento AIT ");
		}
	}

	@SuppressWarnings("unchecked")
	private static void saveLog(String texto, String placa)
	{
		try
		{
			if (placa != null && !placa.isEmpty())
			{
				String userLog = "";
				if (texto != null && !texto.contains("Sistema Fusion") && PortalUtil.getCurrentUser() != null && PortalUtil.getCurrentUser().getFullName() != null)
					userLog = " - Operador(a) " + PortalUtil.getCurrentUser().getFullName() + " : ";

				QLGroupFilter filterAnd = new QLGroupFilter("AND");
				filterAnd.addFilter(new QLEqualsFilter("cd_viatura", Long.parseLong(placa)));
				@SuppressWarnings("deprecation")
				ExternalEntityInfo infoVTR = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SIGMA90VIATURA");
				List<NeoObject> listaVTR = (List<NeoObject>) PersistEngine.getObjects(infoVTR.getEntityClass(), filterAnd);

				if (listaVTR != null && !listaVTR.isEmpty())
				{
					NeoObject viaturaObject = (NeoObject) listaVTR.get(0);
					EntityWrapper placaEntityWrapper = new EntityWrapper(viaturaObject);
					String placaStr = (String) placaEntityWrapper.findValue("nm_placa");
					NeoObject logViatura = AdapterUtils.createNewEntityInstance("SIGMALogViatura");

					if (logViatura != null)
					{
						EntityWrapper viaturaWrapper = new EntityWrapper(logViatura);
						viaturaWrapper.setValue("viatura", viaturaObject);
						viaturaWrapper.setValue("dataLog", new GregorianCalendar());
						viaturaWrapper.setValue("textoLog", userLog + " " + texto);
						viaturaWrapper.setValue("usuario", PortalUtil.getCurrentUser());
						viaturaWrapper.setValue("placaViatura", placaStr);

						PersistEngine.persist(logViatura);

						log.debug("Log salvo com sucesso.");
						return;
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao salvar Log." + e);
		}

	}

	@SuppressWarnings("unchecked")
	private static void saveLogFim(String texto, String placa)
	{
		try
		{
			if (placa != null && !placa.isEmpty())
			{

				QLGroupFilter filterAnd = new QLGroupFilter("AND");
				filterAnd.addFilter(new QLEqualsFilter("cd_viatura", Long.parseLong(placa)));
				@SuppressWarnings("deprecation")
				ExternalEntityInfo infoVTR = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SIGMA90VIATURA");
				List<NeoObject> listaVTR = (List<NeoObject>) PersistEngine.getObjects(infoVTR.getEntityClass(), filterAnd);

				if (listaVTR != null && !listaVTR.isEmpty())
				{
					NeoObject viaturaObject = (NeoObject) listaVTR.get(0);
					EntityWrapper placaEntityWrapper = new EntityWrapper(viaturaObject);
					String placaStr = (String) placaEntityWrapper.findValue("nm_placa");
					//NeoObject logViatura = AdapterUtils.createNewEntityInstance("SIGMALogViatura");
					QLGroupFilter groupFilter = new QLGroupFilter("AND");
					groupFilter.addFilter(new QLEqualsFilter("placaViatura", placaStr));
					groupFilter.addFilter(new QLOpFilter("textoLog", "LIKE", "%" + texto + "%"));
					List<NeoObject> neoObject = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMALogViatura"), groupFilter, -1, -1, "dataLog desc");

					if (NeoUtils.safeIsNotNull(neoObject) && !neoObject.isEmpty())
					{
						EntityWrapper viaturaWrapper = new EntityWrapper(neoObject.get(0));
						viaturaWrapper.setValue("dataLogFim", new GregorianCalendar());

						PersistEngine.persist(neoObject.get(0));

						log.debug("Log salvo com sucesso.");
						return;
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao salvar Log." + e);
		}

	}

	private static StringBuilder callUrl(String url)
	{
		StringBuilder inputLine = new StringBuilder();

		try
		{
			URL murl = new URL(url);
			URLConnection murlc = murl.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));

			char letter;

			while ((letter = (char) in.read()) != '\uFFFF')
			{
				inputLine.append(letter);
			}

			in.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar callUrl." + e);
		}
		return inputLine;
	}

//	private static String getCodigoViatura(String telefone)
//	{
//		String codigoViatura = null;
//		try
//		{
//			if (NeoUtils.safeIsNotNull(telefone))
//			{
//				QLGroupFilter groupFilter = new QLGroupFilter("AND");
//				QLEqualsFilter equalsFilter = new QLEqualsFilter("telefone", telefone);
//				groupFilter.addFilter(equalsFilter);
//				NeoObject neoObject = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("OTSViatura"), groupFilter);
//				if (NeoUtils.safeIsNotNull(neoObject))
//				{
//					EntityWrapper entityWrapper = new EntityWrapper(neoObject);
//					codigoViatura = (String) entityWrapper.getValue("codigoViatura");
//				}
//
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			log.error("Erro ao executar getPlacaViatura." + e);
//		}
//		return codigoViatura;
//	}
//

//	private String getLogViatura(NeoObject otsViatura)
//	{
//		String resultLog = "<b>Log:</b></br>";
//		try
//		{
//
//			EntityWrapper entityWrapper = new EntityWrapper(otsViatura);
//
//			QLEqualsFilter viaturaFilter = new QLEqualsFilter("viatura", otsViatura);
//			GregorianCalendar calendar = new GregorianCalendar();
//			calendar.add(Calendar.DATE, -3);
//			//QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) entityWrapper.findValue("dataMovimentacao")); 
//			QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) calendar);
//			QLGroupFilter groupFilter = new QLGroupFilter("AND");
//			groupFilter.addFilter(viaturaFilter);
//			groupFilter.addFilter(datefilter);
//
//			@SuppressWarnings("rawtypes")
//			Class clazz = AdapterUtils.getEntityClass("OTSLogViatura");
//
//			@SuppressWarnings("unchecked")
//			Collection<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilter, -1, -1, "dataLog desc");
//
//			if (logs != null && logs.size() > 0)
//			{
//				String linha = "";
//				for (NeoObject log : logs)
//				{
//					EntityWrapper logWrapper = new EntityWrapper(log);
//
//					String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
//					String texto = (String) logWrapper.findValue("textoLog");
//					linha = "<div style=\"overflow-y: scroll;\">";
//					if (texto != null && !texto.isEmpty())
//					{
//						linha = "<li class=\"gm-addr\">" + formatedDate + " - " + texto + "</li>";
//						resultLog += linha;
//					}
//				}
//				linha = "</div>";
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			log.error("Erro ao executar getPlacaViatura." + e);
//		}
//
//		return resultLog.replaceAll("\"", "'");
//	}

//	@SuppressWarnings("unused")
//	private String verificarPrefixo(String destino)
//	{
//
//		List<String> prefix_list = new ArrayList<String>();
//		String[] arr = { "3276", "3274", "3256", "3272", "3243", "3285", "3264", "3025", "3028", "3216", "3217", "3221", "3222", "3223", "3224", "3225", "3226", "3228", "3229", "3231", "3232", "3233", "3234", "3235", "3236", "3237", "3238", "3239", "3240", "3244", "3248", "3249", "3251", "3261", "3266", "3269", "3281", "3282", "3284", "3322", "3323", "3324", "3325", "3326", "3329", "3331", "3333", "3334", "3335", "3337", "3338", "3348", "3368", "3369", "3422", "3451", "3700", "3254", "3354",
//				"3262", "3268", "3273", "3267", "3033", "3242", "3283", "3286", "3341", "3342", "3344", "3253", "3275", "3292", "3245", "3265", "3241", "3246", "3247", "3257", "3258", "3259", "3271", "3278", "3343", "3346", "3357", "3381", "3972", "3277", "3378", "3263", "3279", "3206" };
//		for (String string : arr)
//		{
//			prefix_list.add(string);
//		}
//		if (destino != null && !destino.isEmpty())
//			log.error("Telefone destino nulo ou vazio!");
//
//		if (destino.length() == 10 && destino.substring(0, 2) == "48")
//			destino = destino.substring(-8);
//		else if (destino.length() == 10)
//			destino = "0" + destino;
//
//		String prefix = null;
//		prefix = destino.substring(3, 4);
//
//		if (prefix_list.contains(prefix) && destino.substring(1, 2) == "48")
//			destino = destino.substring(-8);
//
//		return destino;
//	}


	public static EventoVO getEvento(String cdViatura, String statusAtendimento)
	{

		String nomeFonteDados = "SIGMA90";
		List<EventoVO> eventoVOs = null;
		try
		{
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO, h.NU_PRIORIDADE, c.CD_CLIENTE, v.NM_VIATURA, h.FG_STATUS, fe.NM_FRASE_EVENTO, ");
			sql.append(" v.CD_VIATURA, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, c.ID_CENTRAL, c.PARTICAO, ");
			sql.append(" c.FANTASIA, c.RAZAO, c.ENDERECO, cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, TX_OBSERVACAO_FECHAMENTO, ");
			sql.append(" TX_OBSERVACAO_GERENTE, r.TELEFONE, c.NU_LATITUDE, c.NU_LONGITUDE, v.NM_PLACA, h.CD_USUARIO_VIATURA_NO_LOCAL, ");
			sql.append(" h.DT_VIATURA_DESLOCAMENTO, c.COMPLEMENTO, e.NOME AS NOMEESTADO, ColDsl.NM_COLABORADOR AS NOMECOLABORADORFDSL , ");
			sql.append(" ColIni.NM_COLABORADOR AS NOMECOLABORADORINI, ColLoc.NM_COLABORADOR AS NOMECOLABORADORLOC,  r.NM_ROTA, ");
			sql.append(" c.ID_EMPRESA, h.DT_EXIBIDO_VIATURA, c.CHAVE, c.CACHORRO, c.ARMA FROM HISTORICO h WITH (NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE LEFT OUTER JOIN VIATURA v WITH (NOLOCK) ON  v.CD_VIATURA = h.CD_VIATURA ");
			sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK) ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO ");
			sql.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO ");
			sql.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			sql.append(" INNER JOIN dbESTADO e WITH (NOLOCK)  ON e.ID_ESTADO = cid.ID_ESTADO ");
			sql.append(" LEFT JOIN USUARIO UsuDsl WITH (NOLOCK) on UsuDsl.CD_USUARIO = h.CD_USUARIO_ESPERA_DESLOCAMENTO	 ");
			sql.append(" LEFT JOIN COLABORADOR ColDsl WITH (NOLOCK) on ColDsl.CD_COLABORADOR = UsuDsl.CD_COLABORADOR ");
			sql.append(" LEFT JOIN USUARIO UsuIni WITH (NOLOCK) on UsuIni.CD_USUARIO = h.CD_USUARIO_VIATURA_DESLOCAMENTO ");
			sql.append(" LEFT JOIN COLABORADOR ColIni WITH (NOLOCK) on ColIni.CD_COLABORADOR = UsuIni.CD_COLABORADOR ");
			sql.append(" LEFT JOIN USUARIO UsuLoc WITH (NOLOCK) on UsuLoc.CD_USUARIO = h.CD_USUARIO_VIATURA_NO_LOCAL	 ");
			sql.append(" LEFT JOIN COLABORADOR ColLoc WITH (NOLOCK) on ColLoc.CD_COLABORADOR = UsuLoc.CD_COLABORADOR ");
			sql.append(" WHERE h.FG_STATUS =:FG_STATUS AND c.NU_LATITUDE is not null AND c.NU_LONGITUDE is not null AND h.CD_HISTORICO_PAI IS NULL AND h.CD_VIATURA  =:CD_VIATURA  ");

			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
			query.setParameter("FG_STATUS", statusAtendimento);
			query.setParameter("CD_VIATURA", cdViatura);
			@SuppressWarnings("unchecked")
			Collection<Object> resultList = query.getResultList();
			PersistEngine.getEntityManager().flush();

			eventoVOs = new ArrayList<EventoVO>();
			comboMotivoAlarme = "";

			if (resultList != null)
			{
				comboMotivoAlarme = buscaMotivoAlarme();
				for (Object result : resultList)
				{
					if (result != null)
					{
						EventoVO eventoVO = composeEvento((Object[]) result);
						if (eventoVO != null)
						{
							eventoVOs.add(eventoVO);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar getEvento." + e);

		}

		EventoVO eventoVO = null;
		if (eventoVOs != null && !eventoVOs.isEmpty())
		{
		    eventoVO = (EventoVO) eventoVOs.get(0);
		}
		
		return eventoVO;
	}


	public static EventoVO getEventoHistorico(String cdHistorico)
	{

		String nomeFonteDados = "SIGMA90";
		List<EventoVO> eventoVOs = null;
		try
		{
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO, h.NU_PRIORIDADE, c.CD_CLIENTE, v.NM_VIATURA, h.FG_STATUS, fe.NM_FRASE_EVENTO, ");
			sql.append(" v.CD_VIATURA, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, c.ID_CENTRAL, c.PARTICAO, ");
			sql.append(" c.FANTASIA, c.RAZAO, c.ENDERECO, cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, TX_OBSERVACAO_FECHAMENTO, ");
			sql.append(" TX_OBSERVACAO_GERENTE, r.TELEFONE, c.NU_LATITUDE, c.NU_LONGITUDE, v.NM_PLACA, h.CD_USUARIO_VIATURA_NO_LOCAL, ");
			sql.append(" h.DT_VIATURA_DESLOCAMENTO, c.COMPLEMENTO, e.NOME AS NOMEESTADO, ColDsl.NM_COLABORADOR AS NOMECOLABORADORFDSL , ");
			sql.append(" ColIni.NM_COLABORADOR AS NOMECOLABORADORINI, ColLoc.NM_COLABORADOR AS NOMECOLABORADORLOC,  r.NM_ROTA, ");
			sql.append(" c.ID_EMPRESA, h.DT_EXIBIDO_VIATURA, c.CHAVE, c.CACHORRO, c.ARMA FROM HISTORICO h WITH (NOLOCK) ");
			sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE LEFT OUTER JOIN VIATURA v WITH (NOLOCK) ON  v.CD_VIATURA = h.CD_VIATURA ");
			sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK) ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO ");
			sql.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO ");
			sql.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			sql.append(" INNER JOIN dbESTADO e WITH (NOLOCK)  ON e.ID_ESTADO = cid.ID_ESTADO ");
			sql.append(" LEFT JOIN USUARIO UsuDsl WITH (NOLOCK) on UsuDsl.CD_USUARIO = h.CD_USUARIO_ESPERA_DESLOCAMENTO	 ");
			sql.append(" LEFT JOIN COLABORADOR ColDsl WITH (NOLOCK) on ColDsl.CD_COLABORADOR = UsuDsl.CD_COLABORADOR ");
			sql.append(" LEFT JOIN USUARIO UsuIni WITH (NOLOCK) on UsuIni.CD_USUARIO = h.CD_USUARIO_VIATURA_DESLOCAMENTO ");
			sql.append(" LEFT JOIN COLABORADOR ColIni WITH (NOLOCK) on ColIni.CD_COLABORADOR = UsuIni.CD_COLABORADOR ");
			sql.append(" LEFT JOIN USUARIO UsuLoc WITH (NOLOCK) on UsuLoc.CD_USUARIO = h.CD_USUARIO_VIATURA_NO_LOCAL	 ");
			sql.append(" LEFT JOIN COLABORADOR ColLoc WITH (NOLOCK) on ColLoc.CD_COLABORADOR = UsuLoc.CD_COLABORADOR ");
			sql.append(" WHERE  c.NU_LATITUDE is not null AND c.NU_LONGITUDE is not null AND h.CD_HISTORICO_PAI IS NULL AND h.CD_HISTORICO  =:CD_HISTORICO  ");

			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
			query.setParameter("CD_HISTORICO", cdHistorico);
			@SuppressWarnings("unchecked")
			Collection<Object> resultList = query.getResultList();

			eventoVOs = new ArrayList<EventoVO>();
			comboMotivoAlarme = "";

			if (resultList != null)
			{
				comboMotivoAlarme = buscaMotivoAlarme();
				for (Object result : resultList)
				{
					if (result != null)
					{
						EventoVO eventoVO = composeEvento((Object[]) result);
						if (eventoVO != null)
						{
							eventoVOs.add(eventoVO);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar getEventoHistorico." + e);
		}

		EventoVO eventoVO = null;
		if (eventoVOs != null && !eventoVOs.isEmpty())
		{
		    eventoVO = (EventoVO) eventoVOs.get(0);
		}
		
		return eventoVO;
	}

	public static EventoVO composeEvento(Object[] resultSet)
	{
		EventoVO eventoVO = null;

		try
		{
			Integer codigoHistorico = (Integer) resultSet[0]; //resultSet.getString("CD_HISTORICO");
			String codigoEvento = (String) resultSet[1]; //resultSet.getString("CD_EVENTO");
			Short prioridade = (Short) resultSet[2]; //resultSet.getLong("NU_PRIORIDADE");
			Integer codigoCliente = (Integer) resultSet[3]; //resultSet.getString("CD_CLIENTE");
			String nomeViatura = (String) resultSet[4]; //resultSet.getString("NM_VIATURA");
			Short status = (Short) resultSet[5]; //resultSet.getInt("FG_STATUS");
			String nomeEvento = (String) resultSet[6]; //resultSet.getString("NM_FRASE_EVENTO");
			Integer codigoViatura = (Integer) resultSet[7]; //resultSet.getString("CD_VIATURA");
			Timestamp dataRecebimento = (Timestamp) resultSet[8]; //resultSet.getTimestamp("DT_RECEBIDO");
			Timestamp dataDeslocamentoViatura = (Timestamp) resultSet[9]; //resultSet.getTimestamp("DT_ESPERA_DESLOCAMENTO");
			Timestamp dataViaturaNoLocal = (Timestamp) resultSet[10]; //resultSet.getTimestamp("DT_VIATURA_NO_LOCAL");
			String codigoCentral = (String) resultSet[11]; //resultSet.getString("ID_CENTRAL");
			String particao = (String) resultSet[12]; //resultSet.getString("PARTICAO");
			String fantasia = (String) resultSet[13]; //resultSet.getString("FANTASIA");
			String razao = (String) resultSet[14]; //resultSet.getString("RAZAO");
			String endereco = (String) resultSet[15]; //resultSet.getString("ENDERECO");
			String nomeCidade = (String) resultSet[16]; //resultSet.getString("NOMECIDADE");
			String bairro = (String) resultSet[17]; //resultSet.getString("NOMEBAIRRO");
			String observacaoFechamento = (String) resultSet[18]; //resultSet.getString("TX_OBSERVACAO_FECHAMENTO"); // log
			String observavaoGerente = (String) resultSet[19]; //resultSet.getString("TX_OBSERVACAO_GERENTE"); // log gerencia
			String telefone = (String) resultSet[20]; //resultSet.getString("TELEFONE");
			String latitude = (String) resultSet[21]; //resultSet.getFloat("NU_LATITUDE");
			String longitude = (String) resultSet[22]; //resultSet.getFloat("NU_LONGITUDE");
			String placa = (String) resultSet[23]; //resultSet.getString("NM_PLACA");
			Integer origemNoLocal = (Integer) resultSet[24]; //CD_USUARIO_VIATURA_NO_LOCAL
			Timestamp deslocamentoIniciado = (Timestamp) resultSet[25]; //resultSet.getTimestamp("DT_VIATURA_DESLOCAMENTO");
			String complemento = (String) resultSet[26]; //resultSet.getString("COMPLEMENTO");
			String nomeEstado = (String) resultSet[27]; //resultSet.getString("NOMEESTADO");
			String nomeColaborador = (String) resultSet[28]; //resultSet.getString("NOMECOLABORADORFDSL");
			String nomeColaboradorIniciado = (String) resultSet[29]; //resultSet.getString("NOMECOLABORADORINI");
			String nomeColaboradorLocal = (String) resultSet[30]; //resultSet.getString("NOMECOLABORADORLOC");
			String nomeRota = (String) resultSet[31]; //resultSet.getString("nm_rota");
			String empresa = String.valueOf(resultSet[32]); //resultSet.getString("ID_EMPRESA");
			Timestamp dataExibido = (Timestamp) resultSet[33]; //resultSet.getTimestamp("DT_EXIBIDO_VIATURA");
			Boolean chave = (Boolean) resultSet[34]; //resultSet.getString("CHAVE");
			Boolean cachorro = (Boolean) resultSet[35]; //resultSet.getString("CACHORRO");
			Boolean arma = (Boolean) resultSet[36]; //resultSet.getString("ARMA");
			if (NeoUtils.safeIsNotNull(nomeColaborador))
				nomeColaborador = "(" + nomeColaborador + ")";
			if (NeoUtils.safeIsNotNull(nomeColaboradorIniciado))
				nomeColaboradorIniciado = "(" + nomeColaboradorIniciado + ")";
			if (NeoUtils.safeIsNotNull(nomeColaboradorLocal))
				nomeColaboradorLocal = "(" + nomeColaboradorLocal + ")";

			eventoVO = new EventoVO();
			eventoVO.setCodigoHistorico((codigoHistorico != null) ? codigoHistorico.toString() : "");
			eventoVO.setCodigoEvento(codigoEvento);
			eventoVO.setPrioridade((prioridade != null) ? prioridade.longValue() : null);
			eventoVO.setCodigoCliente((codigoCliente != null) ? codigoCliente.toString() : "");
			eventoVO.setNomeViatura(nomeViatura);
			eventoVO.setStatus(status.intValue());
			eventoVO.setNomeEvento(nomeEvento);
			eventoVO.setCodigoViatura((codigoViatura != null) ? codigoViatura.toString() : "");
			eventoVO.setEmpresa(empresa);

			String dataRecebimentoStr = NeoDateUtils.safeDateFormat(dataRecebimento, "dd/MM/yyyy HH:mm:ss");
			eventoVO.setDataRecebimento(dataRecebimentoStr);
			String dataDeslocamentoViaturaStr = " ";
			if (NeoUtils.safeIsNotNull(dataDeslocamentoViatura))
			{
				dataDeslocamentoViaturaStr = NeoDateUtils.safeDateFormat(dataDeslocamentoViatura, "dd/MM/yyyy HH:mm:ss");
				eventoVO.setDataDeslocamentoViatura(dataDeslocamentoViaturaStr);
			}
			String dataViaturaNoLocalStr = " ";
			if (NeoUtils.safeIsNotNull(dataDeslocamentoViatura))
			{
				dataViaturaNoLocalStr = NeoDateUtils.safeDateFormat(dataViaturaNoLocal, "dd/MM/yyyy HH:mm:ss");
				eventoVO.setDataViaturaNoLocal(dataViaturaNoLocalStr);

			}
			String dataDeslocamentoIniciadoStr = " ";
			if (NeoUtils.safeIsNotNull(deslocamentoIniciado))
				dataDeslocamentoIniciadoStr = NeoDateUtils.safeDateFormat(deslocamentoIniciado, "dd/MM/yyyy HH:mm:ss");
			if (NeoUtils.safeIsNull(complemento) || complemento.isEmpty())
				complemento = " ";
			String dataExibidoVtr = " ";
			if (NeoUtils.safeIsNotNull(dataExibido))
				dataExibidoVtr = NeoDateUtils.safeDateFormat(dataExibido, "dd/MM/yyyy HH:mm:ss");
			eventoVO.setCodigoCentral(codigoCentral);
			eventoVO.setParticao(particao);
			eventoVO.setFantasia(retiraCaracteresAcentuados(fantasia));
			eventoVO.setRazao(razao);
			eventoVO.setEndereco(endereco);
			eventoVO.setNomeCidade(nomeCidade);
			eventoVO.setBairro(bairro);
			eventoVO.setObservacaoFechamento(observacaoFechamento);
			eventoVO.setObservavaoGerente(observavaoGerente);
			eventoVO.setTelefone(telefone);
			eventoVO.setLatitude(Float.valueOf(latitude));
			eventoVO.setLongitude(Float.valueOf(longitude));
			if (placa == null || placa.isEmpty())
				placa = verificaCor(String.valueOf(codigoHistorico));
			else
				placa += ":0";
			eventoVO.setPlaca(placa);

			NeoObject corPrioridadeEventoObj = getCorPrioridadeEvento(codigoEvento);
			//se nao encontrar pelo codigo do evento busca pelo codigo da prioridade
			if (corPrioridadeEventoObj == null && prioridade != null)
			{
				corPrioridadeEventoObj = getCorPrioridadeEvento(prioridade.toString());
			}

			if (corPrioridadeEventoObj != null)
			{
				EntityWrapper wrapper = new EntityWrapper(corPrioridadeEventoObj);

				String corFundo = (String) wrapper.findValue("corFundo");
				String corTexto = (String) wrapper.findValue("corTexto");

				if (corFundo != null)
				{
					eventoVO.setCorPrioridade(corFundo);
				}

				if (corTexto != null)
				{
					eventoVO.setCorTexto(corTexto);
				}
			}

			String deslocamentoViatura = " ";
			if (NeoUtils.safeIsNotNull(dataDeslocamentoViaturaStr))
			{
				deslocamentoViatura = dataDeslocamentoViaturaStr;
			}
			if (NeoUtils.safeIsNull(dataDeslocamentoViaturaStr) || dataDeslocamentoIniciadoStr.equals(deslocamentoViatura) || (status != null && status.intValue() == 2))
			{
				dataDeslocamentoIniciadoStr = " ";
				nomeColaboradorIniciado = " ";
			}
			String viaturaNoLocal = " ";

			if (dataViaturaNoLocalStr != null)
			{
				viaturaNoLocal = dataViaturaNoLocalStr;
			}
			if (NeoUtils.safeIsNull(nomeColaboradorLocal))
			{
				nomeColaboradorLocal = " ";
			}
			if (observacaoFechamento == null || observacaoFechamento.isEmpty())
			{
				observacaoFechamento = "Vazio";
			}
			else
			{
				observacaoFechamento = "<li>" + observacaoFechamento;
				observacaoFechamento = observacaoFechamento.replace("\n", "<br><li>");
			}
			if (observavaoGerente == null || observavaoGerente.isEmpty())
			{
				observavaoGerente = "Vazio";
			}
			else
			{
				observavaoGerente = "<li>" + observavaoGerente;
				observavaoGerente = observavaoGerente.replace("\n", "<br><li>");
			}
			String chaveStr = "";
			String cachorroStr = "";
			String armaStr = "";
			if (chave != null && chave)
				chaveStr = "<img src=\"images/key.png\"  />";
			if (cachorro != null && cachorro)
				cachorroStr = "<img src=\"images/dog.png\"  />";
			if (arma != null && arma)
				armaStr = "<img src=\"images/gun.png\"  />";

			if (NeoUtils.safeIsNull(deslocamentoViatura))
				deslocamentoViatura = " ";

			if (NeoUtils.safeIsNull(nomeColaborador))
				nomeColaborador = " ";
			else if (nomeColaborador.contains("CM"))
				nomeColaborador = "Operação realizada pela CM, operador: " + nomeColaborador;
			else if (nomeColaborador.contains("VTR"))
				nomeColaborador = "Operação realizada em campo, agente: : " + nomeColaborador;
			else if (nomeColaborador.contains("fusion"))
				nomeColaborador = "Operação realizada pelo Fusion, usuário padrão: COLABORADOR_PADRÃO (Código 9999)";

			if (NeoUtils.safeIsNull(dataDeslocamentoIniciadoStr))
				dataDeslocamentoIniciadoStr = " ";

			if (NeoUtils.safeIsNull(nomeColaboradorIniciado))
				nomeColaboradorIniciado = " ";
			else if (nomeColaboradorIniciado.contains("CM"))
				nomeColaboradorIniciado = "Operação realizada pela CM, operador: " + nomeColaboradorIniciado;
			else if (nomeColaboradorIniciado.contains("VTR"))
				nomeColaboradorIniciado = "Operação realizada em campo, agente: " + nomeColaboradorIniciado;
			else if (nomeColaboradorIniciado.contains("fusion"))
				nomeColaboradorIniciado = "Operação realizada pelo Fusion, usuário padrão: COLABORADOR_PADRÃO (Código 9999)";

			if (NeoUtils.safeIsNull(viaturaNoLocal))
				viaturaNoLocal = " ";

			if (NeoUtils.safeIsNull(nomeColaboradorLocal))
				nomeColaboradorLocal = " ";
			else if (nomeColaboradorLocal.contains("CM"))
				nomeColaboradorLocal = "Operação realizada pela CM, operador: " + nomeColaboradorLocal;
			else if (nomeColaboradorLocal.contains("VTR"))
				nomeColaboradorLocal = "Operação realizada em campo, agente: " + nomeColaboradorLocal;
			else if (nomeColaboradorLocal.contains("fusion"))
				nomeColaboradorLocal = "Operação realizada pelo Fusion, usuário padrão: COLABORADOR_PADRÃO (Código 9999)";

			StringBuilder contentString = new StringBuilder();
			contentString.append("<div id='content' style=\"width:550px; height:280px\" >");
			contentString.append("<div id='bodyContent'><span class=\"gm-style-iw\"><b>Cliente:</b> </span> <span class=\"gm-addr\">");
			contentString.append(codigoCentral + " [" + particao + "] " + fantasia + "&nbsp &nbsp" + chaveStr + "&nbsp" + cachorroStr + "&nbsp" + armaStr + "</span>");
			contentString.append("&nbsp&nbsp<a href=\"javascript:atualizaContatos(" + eventoVO.getCodigoCliente() + ", " + eventoVO.getCodigoHistorico() + ");\" title='Informações do cliente.' ><img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/home_16x16.png\"  /></a>");
			contentString.append("<br> <span class=\"gm-style-iw\"><b>Razão Social:</b> </span> <span class=\"gm-addr\">" + razao + "</span>" + "<br>");
			contentString.append("<span class=\"gm-style-iw\"><b>Endereço:</b> </span> <span class=\"gm-addr\">" + endereco);
			contentString.append("  " + complemento + "</span><br><span class=\"gm-style-iw\"><b>Bairro/Cidade:</b> </span> <span class=\"gm-addr\">");
			contentString.append(bairro + " " + nomeCidade + " " + nomeEstado + "</span><br><span class=\"gm-style-iw\"><b>Evento:</b> </span> <span class=\"gm-addr\">");
			contentString.append(codigoEvento + " - " + nomeEvento + "</span><br><span class=\"gm-style-iw\"><b>Rota:</b> </span> <span class=\"gm-addr\">" + nomeRota + "</span><br>");
			contentString.append("<span class=\"gm-style-iw\"><b>Recebido:</b> <span class=\"gm-addr\">" + dataRecebimentoStr + "</span><br><span class=\"gm-style-iw\"><b>Deslocado:</b> </span> <span class=\"gm-addr\">" + deslocamentoViatura);

			if (NeoUtils.safeIsNotNull(nomeColaborador) && nomeColaborador.contains("CM") && (!deslocamentoViatura.isEmpty()))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaborador + "\"> <img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/home_16x16.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaborador) && nomeColaborador.contains("VTR") && (!deslocamentoViatura.isEmpty()))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaborador + "\"> <img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/papel_16x16-trans.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaborador) && (!deslocamentoViatura.isEmpty()))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaborador + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/process_running.png\"  /></a></span>");
			contentString.append("<br><span class=\"gm-style-iw\"><b>Exibido:</b> </span> <span class=\"gm-addr\">" + dataExibidoVtr + "<br><span class=\"gm-style-iw\"><b>Iniciado:</b> <span class=\"gm-addr\">" + dataDeslocamentoIniciadoStr);

			if (NeoUtils.safeIsNotNull(nomeColaboradorIniciado) && nomeColaboradorIniciado.contains("CM") && (dataDeslocamentoIniciadoStr != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaboradorIniciado + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/home_16x16.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaboradorIniciado) && nomeColaboradorIniciado.contains("VTR") && (dataDeslocamentoIniciadoStr != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaboradorIniciado + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/papel_16x16-trans.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaboradorIniciado) && (dataDeslocamentoIniciadoStr != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaboradorIniciado + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/process_running.png\"  /></a></span>");
			contentString.append("<br><span class=\"gm-style-iw\"><b>No Local:</b> </span> <span class=\"gm-addr\">" + viaturaNoLocal);

			if (NeoUtils.safeIsNotNull(nomeColaboradorLocal) && nomeColaboradorLocal.contains("CM") && (viaturaNoLocal != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaboradorLocal + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/custom/home_16x16.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaboradorLocal) && nomeColaboradorLocal.contains("VTR") && (viaturaNoLocal != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaboradorLocal + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/papel_16x16-trans.png\"  /></a></span>");
			else if (NeoUtils.safeIsNotNull(nomeColaboradorLocal) && (viaturaNoLocal != null || !nomeColaborador.equals(" ")))
				contentString.append("&nbsp &nbsp <a title=\"Responsável: " + nomeColaboradorLocal + "\"><img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/process_running.png\"  /></a></span>");
			if (origemNoLocal != null && origemNoLocal == 9999)
			{
				contentString.append("&nbsp &nbsp<img style=\"border : none\" src=\"images/peloAA.png\"/>");
			}
			contentString.append("<br>");
			if (codigoHistorico != null)
				contentString.append(listaPosicoes(String.valueOf(codigoHistorico)));
			contentString.append("&nbsp &nbsp</span> <a  href=\"javascript:salvaLogEvento(" + codigoHistorico + ")\" tooltip=\"<b>Log:</b></br>" + observacaoFechamento + "\">");
			contentString.append("<img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");
			contentString.append("&nbsp &nbsp<a href=\"javascript:salvaLogEventoGerencia(" + codigoHistorico + ")\" tooltip=\"<b>Log Gerência:</b></br>" + observavaoGerente + "\">");
			contentString.append("<img src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/doc_link.png\"/></a>");

			if (placa != null && !placa.isEmpty())
			{

				contentString.append("&nbsp &nbsp");
				contentString.append("<a  tooltip=\"<b>Atualizar Posição</b>\" href=\"javascript:atualizaPosicaoCliente(" + codigoCliente + ",'" + placa + "')\"><img style=\"border : none\" src=\"images/atualizarPosicao.jpg\"/></a>");

			}

			if (status.intValue() == 9)
			{
				contentString.append("&nbsp&nbsp<a href=\"javascript:viaturaNoLocal(" + codigoHistorico + ");\"  title='Chegada ao local!' ><img style=\"border : none\" src=\"images/local.png\"/></a>");
			}

			if (status.intValue() == 2)
			{
				contentString.append("&nbsp &nbsp<a href=\"javascript:deslocarEventoViaturaIniciado(" + codigoHistorico + ");\"  title='Iniciar o deslocamento!' ><img style=\"border : none\" src=\"images/settings.png\"/></a>");
			}

			if (status.intValue() != 1)
			{
				contentString.append("&nbsp&nbsp<a href=\"javascript:cancelarEventoIniciado(" + codigoHistorico + ");\"  title='Cancelar deslocamento!' ><img style=\"border : none\" src=\"images/remove.png\"/></a>");

			}

			if (status.intValue() == 1)
			{
				String nmRotaStr = "";
				if (nomeRota.length() > 8 && nomeRota.substring(8, 8).contains("-"))
					nmRotaStr = nomeRota.substring(0, 7);
				else if (nomeRota.length() > 8)
					nmRotaStr = nomeRota.substring(0, 8);

				String cbVtrs = "<select name='cbViatura' id='cbViatura' style='width:400px;'>";
				String regionalStr = (String) nomeRota.subSequence(0, 3);
				StringBuilder builder = new StringBuilder(getMotoritas(regionalStr));
				String[] combobox = builder.toString().split("::");
				Collection<String> listaCombo = new ArrayList<String>();
				for (String stringCombo : combobox)
				{
					if (stringCombo.contains(nmRotaStr) && !listaCombo.contains(stringCombo))
					{
						listaCombo.add(stringCombo);
					}
				}

				if (listaCombo.isEmpty())
					cbVtrs += "<option value='0'  selected='selected' disabled='disabled' style='display: none'>Selecione...</option>";
				nmRotaStr = nomeRota.substring(0, 3);
				for (String stringCombo : combobox)
				{
					if (stringCombo.contains(nmRotaStr) && !listaCombo.contains(stringCombo))
					{
						listaCombo.add(stringCombo);
					}
				}
				for (String resultCombo : listaCombo)
				{
					cbVtrs += resultCombo;
				}

				cbVtrs += "</select>";
				contentString.append("<br><span class=\"gm-style-iw\"><b>Deslocar  -VTR:</b> </span> " + cbVtrs + "&nbsp &nbsp");
				String nomeCliente = eventoVO.getCodigoCentral() + "[" + eventoVO.getParticao() + "] " + eventoVO.getFantasia();

				contentString.append("<a title='Deslocar evento para fila!'   href=\"javascript:deslocarEventoFila(" + codigoHistorico + ",'" + retiraCaracteresAcentuados(nomeCliente) + "')\"><img style=\"border : none\" src=\"images/plus.png\"/></a>");
				contentString.append("&nbsp&nbsp<a href=\"javascript:deslocarEventoViatura(" + codigoHistorico + ",'" + retiraCaracteresAcentuados(nomeCliente) + "');\"  title='Deslocar evento para viatura!' ><img style=\"border : none\" src=\"images/play.png\"/></a>");

			}

			String cbTipoFechamento = "<br><span class=\"gm-style-iw\"><b>Fechar Evento:</b> </span>  <select name='cbTipoFechamento' id='cbTipoFechamento' style='width:400px;'>";
			cbTipoFechamento += comboMotivoAlarme;
			cbTipoFechamento += "</select>";
			contentString.append(cbTipoFechamento);
			contentString.append("&nbsp &nbsp<a href=\"javascript:fecharEventoNoLocal(" + codigoHistorico + ");\"  title='Fechar evento!' ><img style=\"border : none\" src=\"images/delete.png\"/></a>");
			contentString.append("&nbsp &nbsp<a href=\"javascript:removerEventoEmEspera(" + codigoHistorico + ");\"  title='Remover evento fila!' ><img style=\"border : none\" src=\"images/remove.png\"/></a>");
			contentString.append("</div></div>");
			eventoVO.setInfoWindowContent(contentString.toString());

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar composeEvento AIT " + e);
		}

		return eventoVO;
	}

	public static StringBuilder getMotoritas(String regional)
	{
		List<NeoObject> motoristas = null;
		StringBuilder builder = null;
		try
		{

			if (NeoUtils.safeIsNotNull(regional))
			{
				QLGroupFilter filterAnd = new QLGroupFilter("AND");

				filterAnd.addFilter(new QLOpFilter("motorista", "LIKE", regional + "%"));
				filterAnd.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
				motoristas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filterAnd);
				PersistEngine.getEntityManager().flush();
				if (NeoUtils.safeIsNotNull(motoristas))
				{
					builder = new StringBuilder();
					for (NeoObject neoObject : motoristas)
					{
						NeoObject object = (NeoObject) neoObject;
						EntityWrapper entityWrapper = new EntityWrapper(object);
						String motorista = (String) entityWrapper.getValue("motorista");
						String codigoViatura = (String) entityWrapper.getValue("codigoViatura");
						if (!motorista.contains("- TEC") && !motorista.contains("-TEC") && !motorista.contains("-INSP") && (!motorista.contains("CTA-SUP") && (!motorista.contains("HN") || !motorista.contains("HD"))) && !motorista.contains("-MOTO") && !motorista.contains("-ADM") && !motorista.contains("OFFICE") && !motorista.contains(" ENG"))
							builder.append("<option value='" + codigoViatura + "'>" + motorista + "</option>::");
					}
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar getMotoritas AIT " + e);
		}
		
		return builder;
	}

	public static String retiraCaracteresAcentuados(String stringFonte)
	{
		String passa = stringFonte;
		passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
		passa = passa.replaceAll("[âãàáä]", "a");
		passa = passa.replaceAll("[ÊÈÉË]", "E");
		passa = passa.replaceAll("[êèéë]", "e");
		passa = passa.replaceAll("ÎÍÌÏ", "I");
		passa = passa.replaceAll("îíìï", "i");
		passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
		passa = passa.replaceAll("[ôõòóö]", "o");
		passa = passa.replaceAll("[ÛÙÚÜ]", "U");
		passa = passa.replaceAll("[ûúùü]", "u");
		passa = passa.replaceAll("Ç", "C");
		passa = passa.replaceAll("ç", "c");
		passa = passa.replaceAll("[ýÿ]", "y");
		passa = passa.replaceAll("Ý", "Y");
		passa = passa.replaceAll("ñ", "n");
		passa = passa.replaceAll("Ñ", "N");
		passa = passa.replaceAll("[-+=*&amp;%$#@!_]", "");
		passa = passa.replaceAll("['\"]", "");
		passa = passa.replaceAll("[<>()\\{\\}]", "");
		passa = passa.replaceAll("['\\\\.,()|/]", "");
		passa = passa.replaceAll("[^!-ÿ]{1}[^ -ÿ]{0,}[^!-ÿ]{1}|[^!-ÿ]{1}", " ");
		return passa;
	}


	@SuppressWarnings("unchecked")
	public static String listaPosicoes(String codHistorico)
	{

		NeoObject eventosObj = null;
		int i = 1;
		StringBuilder stringBuilder = new StringBuilder();

		try
		{

			if (NeoUtils.safeIsNotNull(codHistorico))
			{
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("codHistorico", codHistorico));
				eventosObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), groupFilter);

				if (eventosObj != null)
				{

					NeoObject noPS = (NeoObject) eventosObj;

					EntityWrapper psWrapper = new EntityWrapper(noPS);
					String cdViaturaSigma = (String) psWrapper.getValue("cdViaturaSigma");
					QLGroupFilter filterAndVtr = new QLGroupFilter("AND");
					filterAndVtr.addFilter(new QLEqualsFilter("cdViaturaSigma", cdViaturaSigma));
					Collection<NeoObject> eventosList = null;
					eventosList = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAndVtr, -1, -1, "dataEspera ASC");
					if (eventosList != null && !eventosList.isEmpty())
					{
						stringBuilder.append("<span class=\"gm-style-iw\"><b> Posição: </b> </span> <select id='cbPosicao'  ");
						stringBuilder.append("panelHeight=\"auto\" editable=\"false\" style=\"width: 40px\" >");
						
						for (int y=0; y<eventosList.size(); y ++){
						    stringBuilder.append("<option value=" + i + ">" + i + "</option>");
						    i++;
						    
						}

						stringBuilder.append("</select>&nbsp<a href=\"javascript:atualizaPosicaoEventoFila(" + codHistorico + ");\" title='Confirmar!' ><img style=\"border : none\" src=\"images/accept.png\"/></a>");
						PersistEngine.getEntityManager().flush();
					}
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsMapsServlet listaPosicoes : " + e.getMessage());
		}

		if (i == 2)
		{
		    stringBuilder = new StringBuilder();
		    stringBuilder.append("");
		}
		
		return stringBuilder.toString();

	}

	private static String buscaMotivoAlarme()
	{
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		ResultSet rsH = null;
		String retornoLista = "";
		try
		{

			conn = PersistEngine.getConnection("SIGMA90");
			String selecHtSQL = " SELECT CD_MOTIVO_ALARME,DS_MOTIVO_ALARME FROM MOTIVO_ALARME WITH(NOLOCK) where FG_ATIVO = 1 ";
			preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());
			rsH = preparedStatementHSelect.executeQuery();
			retornoLista += "<option value='0'  selected='selected' disabled='disabled' style='display: none'>Selecione...</option>";
			while (rsH.next())
			{
				retornoLista += "<option value=" + rsH.getString("CD_MOTIVO_ALARME") + " > " + rsH.getString("DS_MOTIVO_ALARME") + "</option>";

			}

		}
		catch (Exception e)
		{

			e.printStackTrace();
			log.error("##### ERRO SALVAR LOG EVENT ALERT fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
		}
		return retornoLista;
	}

	public static NeoObject getCorPrioridadeEvento(String codigoPrioridade)
	{
		NeoObject object = null;

		try
		{
			Class<NeoObject> clazz = AdapterUtils.getEntityClass("OTSCorPrioridadeEvento");

			QLEqualsFilter equalsFilter = new QLEqualsFilter("codigo", codigoPrioridade);

			object = PersistEngine.getObject(clazz, equalsFilter);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar getCorPrioridadeEvento AIT " + e);
		}

		return object;
	}

	@SuppressWarnings({ "unchecked" })
	public static String verificaCor(String codHistorico)
	{

		NeoObject eventosObj = null;
		String placa = "";
		int i = 1;

		try
		{

			if (NeoUtils.safeIsNotNull(codHistorico))
			{
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("codHistorico", codHistorico));
				eventosObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), groupFilter);

				if (eventosObj != null)
				{

					NeoObject noPS = (NeoObject) eventosObj;

					EntityWrapper psWrapper = new EntityWrapper(noPS);
					String cdViaturaSigma = (String) psWrapper.getValue("cdViaturaSigma");
					QLGroupFilter filterAndVtr = new QLGroupFilter("AND");
					filterAndVtr.addFilter(new QLEqualsFilter("cdViaturaSigma", cdViaturaSigma));
					Collection<NeoObject> eventosList = null;
					eventosList = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAndVtr, -1, -1, "dataEspera ASC");
					if (eventosList != null && !eventosList.isEmpty())
					{

						for (NeoObject neoObject : eventosList)
						{
							NeoObject neoObjs = (NeoObject) neoObject;

							EntityWrapper psWrapperObjs = new EntityWrapper(neoObjs);
							String hist = (String) psWrapperObjs.getValue("codHistorico");
							if (hist.equals(codHistorico))
								break;
							i++;
						}

						QLGroupFilter groupFilters = new QLGroupFilter("AND");
						groupFilters.addFilter(new QLEqualsFilter("codigoViatura", cdViaturaSigma));
						groupFilters.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
						NeoObject objOTSViatura = PersistEngine.getObject(AdapterUtils.getEntityClass("OTSViatura"), groupFilters);

						NeoObject neoObj = (NeoObject) objOTSViatura;

						EntityWrapper psWrappers = new EntityWrapper(neoObj);

						placa = (String) psWrappers.getValue("placa");
						placa += ":" + i;
						PersistEngine.getEntityManager().flush();
					}
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO OrsegupsMapsServlet ao verificar a cor prioridade : " + e.getMessage());
		}
		return placa;

	}

	public void verificarLigacaoPendente(String ramal)
	{

		try
		{
			if (ramal != null && !ramal.isEmpty() && OrsegupsAlertAitEngine.successCallMap.values() != null && !OrsegupsAlertAitEngine.successCallMap.isEmpty() && OrsegupsAlertAitEngine.successCallMap.containsKey(ramal))
			{
				CallAlertAitVO alertVO = OrsegupsAlertAitEngine.successCallMap.get(ramal);
				if (alertVO != null && alertVO.getCallEventoVO() != null)
				{

					if (alertVO.getCallEventoVO().getCodigoHistorico() != null)
					{
						EventoVO eventoVO = alertVO.getCallEventoVO();
						eventoVO = (EventoVO) OrsegupsAlertAitEngine.getEventoHistorico(alertVO.getCallEventoVO().getCodigoHistorico());
						ViaturaVO viaturaVO = getViatura(alertVO.getCallViaturaVO().getCodigoViatura(), null, null, null, null, null, null, null, null, null);

						if (eventoVO != null)
							alertVO.setCallEventoVO(eventoVO);
						if (viaturaVO != null)
						{
							viaturaVO.setStatusVtr(alertVO.getCallViaturaVO().getStatusVtr());
							alertVO.setCallViaturaVO(viaturaVO);
						}
					}
					Gson gson = new Gson();
					String alertVoJSON = "";
					alertVoJSON = gson.toJson(alertVO);

					log.debug("Antes de executar verificarLigacaoPendente AIT evento : " + alertVO.getCallEventoVO().getCodigoHistorico() + " ramal: " + ramal);
					NeoJMSPublisher.getInstance().sendAlertAitMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);
					log.debug("Depois de executar verificarLigacaoPendente AIT ");
				}
			}
			else if (ramal != null && !ramal.isEmpty())
			{
				Gson gson = new Gson();
				String alertVoJSON = gson.toJson("");
				
				NeoJMSPublisher.getInstance().sendAlertAitMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar verificarLigacaoPendente AIT " + e);
		}

	}

	public void getCache()
	{

		try
		{

			Set<String> chaves = OrsegupsAlertAitEngine.successCallMap.keySet();
			for (String chave : chaves)
			{
				if (chave != null)
				{
					int minutesSinceLastCall = 0;
					if (OrsegupsAlertAitEngine.successCallMap.get(chave) != null && OrsegupsAlertAitEngine.successCallMap.get(chave).getCallEventoVO() != null && OrsegupsAlertAitEngine.successCallMap.get(chave).getCallEventoVO().getFantasia() != null)
						minutesSinceLastCall = Minutes.minutesBetween(new DateTime(OrsegupsAlertAitEngine.successCallMap.get(chave).getLastCallCalendar()), new DateTime(new GregorianCalendar())).getMinutes();
					log.debug("--------------------------------------------------------------------------------------------------------------- ");
					log.debug("Chave: " + chave + " minutos : " + minutesSinceLastCall);
					log.debug("--------------------------------------------------------------------------------------------------------------- ");
				}
			}

			Set<String> chavess = OrsegupsAlertAitEngine.waitingSnepResponseMap.keySet();
			for (String chave : chavess)
			{
				if (chave != null)
				{
					int minutesSinceLastCall = 0;
					if (OrsegupsAlertAitEngine.waitingSnepResponseMap.get(chave) != null && OrsegupsAlertAitEngine.waitingSnepResponseMap.get(chave).getCallEventoVO() != null && OrsegupsAlertAitEngine.waitingSnepResponseMap.get(chave).getCallEventoVO().getFantasia() != null)
						minutesSinceLastCall = Minutes.minutesBetween(new DateTime(OrsegupsAlertAitEngine.waitingSnepResponseMap.get(chave).getLastCallCalendar()), new DateTime(new GregorianCalendar())).getMinutes();

					log.debug("--------------------------------------------------------------------------------------------------------------- ");
					log.debug("Chave: " + chave + " minutos : " + minutesSinceLastCall);
					log.debug("--------------------------------------------------------------------------------------------------------------- ");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar getCache AIT " + e);
		}
	}

	private String retornaLogParaCadastro(CallAlertAitVO aitVO)
	{

		String texto = "";
		try
		{
			texto = "Sistema Fusion. ";

			texto += "Viatura em alerta, devido a ";
			if (aitVO.getCallViaturaVO().getAlertaNoLocal() != null && aitVO.getCallViaturaVO().getAlertaNoLocal())
				texto += "excesso de tempo no local.";
			else if (aitVO.getCallViaturaVO().getAtrasoDeslocamento() != null && aitVO.getCallViaturaVO().getAtrasoDeslocamento())
				texto += "atraso após inicio do deslocamento.";
			else if (aitVO.getCallViaturaVO().getDeslocamentoLento() != null && aitVO.getCallViaturaVO().getDeslocamentoLento())
				texto += "deslocamento com atraso, não iniciado.";
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return texto;

	}

	@SuppressWarnings("unchecked")
	public void getViaturas(String regional, String x8IgnoreTags, String ramalOrigem, String aplicacao, String strEmUso, String filtrosConta, String filtrosNotConta, String stausVtr, String ativaNaFrotaVtr)
	{

		//regional, x8IgnoreTags, ramalOrigem, aplicacao, emUso, null, filtrosConta, filtrosNotConta, stausVtr, ativaNaFrota

		try
		{
			if (!OrsegupsAlertAitEngine.verificaViaturas)
			{
				OrsegupsAlertAitEngine.verificaViaturas = Boolean.TRUE;

				Class<?> viaturaClazz = null;
				viaturaClazz = AdapterUtils.getEntityClass("OTSViatura");
				String tipo = "viaturasCM";
				Boolean emUso = Boolean.TRUE;
				Boolean ativaNaFrota = Boolean.TRUE;
				String codigoTecnico = "";

				if (strEmUso != null && strEmUso.equals("nao"))
				{
					emUso = Boolean.FALSE;
				}
				else if (strEmUso != null && strEmUso.equals("na"))
				{
					emUso = null;
				}

				if (ativaNaFrotaVtr != null && ativaNaFrotaVtr.equals("nao"))
				{
					ativaNaFrota = Boolean.FALSE;
				}
				else if (ativaNaFrotaVtr != null && ativaNaFrotaVtr.equals("na"))
				{
					ativaNaFrota = null;
				}

				QLFilterIsNotNull latitudeIsNotNull = new QLFilterIsNotNull("latitude");
				QLFilterIsNotNull longitudeIsNotNull = new QLFilterIsNotNull("longitude");

				QLEqualsFilter emUsoFilter = new QLEqualsFilter("emUso", emUso);

				QLEqualsFilter ativaNaFrotaFilter = new QLEqualsFilter("ativaNaFrota", ativaNaFrota);

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(latitudeIsNotNull);
				groupFilter.addFilter(longitudeIsNotNull);
				if (emUso != null)
				{
					groupFilter.addFilter(emUsoFilter);
				}

				if (ativaNaFrota != null)
				{
					groupFilter.addFilter(ativaNaFrotaFilter);
				}

				// somente filtra pela regional da viatura se a regional for passada como parametro (lista), caso contrario traz as viaturas de todas as regionais
				//			if (regional != null && !regional.isEmpty() && !regional.equalsIgnoreCase("null"))
				//			{
				//				QLGroupFilter listaRegionalFilter = new QLGroupFilter("OR");
				//				StringTokenizer st = new StringTokenizer(regional, ",");
				//
				//				while (st.hasMoreElements())
				//				{
				//					String siglaRegional = (String) st.nextElement();
				//					QLEqualsFilter regionalFilter = new QLEqualsFilter("codigoRegional", siglaRegional);
				//					listaRegionalFilter.addFilter(regionalFilter);
				//				}
				//				groupFilter.addFilter(listaRegionalFilter);
				//			}

				if (tipo != null && tipo.equalsIgnoreCase("viaturasCM"))
				{

					// filtra viaturas ligadas a eventos X8 com tags de exclusao
					String nomeFonteDados = "SIGMA90";
					StringBuilder sql = new StringBuilder();
					sql.append(" SELECT v.NM_PLACA FROM HISTORICO h WITH (NOLOCK) INNER JOIN VIATURA v WITH (NOLOCK) ON  v.CD_VIATURA = h.CD_VIATURA ");
					sql.append(" WHERE h.FG_STATUS IN (2, 3, 9) AND h.CD_HISTORICO_PAI IS NULL ");

					String sqlFiltroTag = "";
					if (x8IgnoreTags != null)
					{
						StringTokenizer stTag = new StringTokenizer(x8IgnoreTags, ",");
						int countToken = 0;
						while (stTag.hasMoreElements())
						{
							countToken++;
							String tag = (String) stTag.nextElement();

							if (countToken == 1)
							{
								sqlFiltroTag = sqlFiltroTag + " AND (  ";
								sqlFiltroTag = sqlFiltroTag + "        ( h.TX_OBSERVACAO_GERENTE LIKE '%#" + tag + "%' ) ";
							}
							else
							{
								sqlFiltroTag = sqlFiltroTag + "        OR (h.TX_OBSERVACAO_GERENTE LIKE '%#" + tag + "%' ) ";
							}

						}
						if (countToken > 0)
						{
							sqlFiltroTag = sqlFiltroTag + " ) ";
						}
					}
					sql.append(sqlFiltroTag);

					Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
					Collection<Object> resultList = query.getResultList();

					//Statement statement = null;
					Collection<String> listaNotPlaca = new ArrayList<String>();

					if (resultList != null)
					{
						for (Object result : resultList)
						{
							if (result != null)
							{
								String placa = ((String) result);
								if (placa != null)
								{
									listaNotPlaca.add(placa);
								}
							}
						}
					}

					// cria o filtro das viaturas tagadas
					QLNotInFilter listaExclusaoViaturasFilter = new QLNotInFilter("placa", listaNotPlaca);
					groupFilter.addFilter(listaExclusaoViaturasFilter);

					// somente filtra pela aplicacao da viatura se a aplicacao for passada como parametro (lista), caso contrario traz somente as viaturas da aplicacao TAT
					if (aplicacao != null && !aplicacao.isEmpty() && !aplicacao.equalsIgnoreCase("null"))
					{
						QLGroupFilter listaAplicacaoFilter = new QLGroupFilter("OR");
						StringTokenizer st = new StringTokenizer(aplicacao, ",");

						while (st.hasMoreElements())
						{
							String siglaAplicacao = (String) st.nextElement();
							QLEqualsFilter viaturaCMFilter = new QLEqualsFilter("aplicacaoViatura.sigla", siglaAplicacao);
							listaAplicacaoFilter.addFilter(viaturaCMFilter);
						}
						groupFilter.addFilter(listaAplicacaoFilter);
					}
					else
					{
						QLEqualsFilter viaturaCMFilter = new QLEqualsFilter("aplicacaoViatura.sigla", "TAT");
						groupFilter.addFilter(viaturaCMFilter);
					}

				}
				else if (tipo != null && tipo.equalsIgnoreCase("viaturasTecnicas"))
				{
					QLEqualsFilter viaturaTecnicaFilter = new QLEqualsFilter("aplicacaoViatura.sigla", "TEC");
					groupFilter.addFilter(viaturaTecnicaFilter);

					if (codigoTecnico != null)
					{
						QLGroupFilter listaTecnicosFilter = new QLGroupFilter("OR");
						StringTokenizer st = new StringTokenizer(codigoTecnico, ",");

						while (st.hasMoreElements())
						{
							String codTec = (String) st.nextElement();
							QLEqualsFilter tecnicoFilter = new QLEqualsFilter("codigoDoTecnico", codTec);
							listaTecnicosFilter.addFilter(tecnicoFilter);
						}
						groupFilter.addFilter(listaTecnicosFilter);

					}

				}

				Collection<NeoObject> ostViaturesObj = (Collection<NeoObject>) PersistEngine.getObjects(viaturaClazz, groupFilter);

				Collection<ViaturaVO> viaturaVOs = new ArrayList<ViaturaVO>();

				

				TreeMap<Integer, ViaturaVO> treeMapVtr = new TreeMap<Integer, ViaturaVO>();

				for (NeoObject obj : ostViaturesObj)
				{
					ViaturaVO viaturaVO = composeViaturaVO(obj, ramalOrigem);

					if (viaturaVO != null)
					{
						viaturaVOs.add(viaturaVO);
						if (viaturaVO.getCodigoViatura() != null && !viaturaVO.getCodigoViatura().isEmpty() && viaturaVO.getCodigoViatura().length() <= 6)
							treeMapVtr.put(Integer.parseInt(viaturaVO.getCodigoViatura()), viaturaVO);

					}

				}

				//				if ((OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty()) && (OrsegupsAlertAitEngine.successCallMap.values() == null || OrsegupsAlertAitEngine.successCallMap.isEmpty()) && (OrsegupsAlertAitEngine.waitingSnepResponseMap.values() == null || OrsegupsAlertAitEngine.waitingSnepResponseMap.isEmpty()))
				//				{
				//
				//					if (OrsegupsAlertAitEngine.instance != null)
				//					{
				//						TreeMap<Integer, CallAlertAitVO> treeMapCom = (TreeMap<Integer, CallAlertAitVO>) treeMap;
				//						for (Entry<Integer, CallAlertAitVO> entry : treeMapCom.entrySet())
				//						{
				//
				//							if (entry != null)
				//							{
				//								CallAlertAitVO aitVO = entry.getValue();
				//
				//								String texto = retornaLogParaCadastro(aitVO);
				//
				//								if (NeoUtils.safeIsNotNull(entry.getKey()))
				//									saveLogFim(texto, String.valueOf(entry.getKey()));
				//
				//								if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty() && treeMap.containsKey(Integer.parseInt(aitVO.getCallViaturaVO().getCodigoViatura())))
				//									OrsegupsAlertAitEngine.treeMap.remove(Integer.parseInt(aitVO.getCallViaturaVO().getCodigoViatura()));
				//
				//							}
				//						}
				//					}
				//				}

				if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty() && viaturaVOs != null && !viaturaVOs.isEmpty())
				{
					for (Entry<Integer, ViaturaVO> entry : treeMapVtr.entrySet())
					{

						if (entry != null && entry.getKey() != null)
						{

							if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty() && OrsegupsAlertAitEngine.treeMap.containsKey(entry.getKey()))
							{
								ViaturaVO vtr = treeMapVtr.get(entry.getKey());
								CallAlertAitVO aitVO = OrsegupsAlertAitEngine.treeMap.get(Integer.parseInt(vtr.getCodigoViatura()));
								EventoVO eventoVO = new EventoVO();
								eventoVO = (EventoVO) OrsegupsAlertAitEngine.getEventoHistorico(aitVO.getCallEventoVO().getCodigoHistorico());
								if ((eventoVO == null || (aitVO.getCallViaturaVO() != null && !eventoVO.getCodigoViatura().equals(aitVO.getCallViaturaVO().getCodigoViatura()))) || ((!vtr.getAlertaNoLocal() && !vtr.getAtrasoDeslocamento() && !vtr.getDeslocamentoLento())))
								{
									String texto = retornaLogParaCadastro(aitVO);

									if (NeoUtils.safeIsNotNull(aitVO.getCallViaturaVO().getCodigoViatura()) && texto != null)
										saveLogFim(texto, aitVO.getCallViaturaVO().getCodigoViatura());

									if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty() && OrsegupsAlertAitEngine.treeMap.containsKey(Integer.parseInt(vtr.getCodigoViatura())))
										OrsegupsAlertAitEngine.treeMap.remove(Integer.parseInt(vtr.getCodigoViatura()));
									if (OrsegupsAlertAitEngine.waitingSnepResponseMap != null && !OrsegupsAlertAitEngine.waitingSnepResponseMap.isEmpty() && OrsegupsAlertAitEngine.waitingSnepResponseMap.containsKey(aitVO.getCallViaturaVO().getTelefone()))
										OrsegupsAlertAitEngine.waitingSnepResponseMap.remove(aitVO.getCallViaturaVO().getTelefone());

									log.debug("Removeu ait :" + vtr.getNomeMotorista() + " " + vtr.getAlertaNoLocal() + " " + vtr.getAtrasoDeslocamento() + " " + vtr.getDeslocamentoLento() + " evento " + eventoVO);

								}

							}
						}
					}

					if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty())
					{
						TreeMap<Integer, CallAlertAitVO> treeMapAux = (TreeMap<Integer, CallAlertAitVO>) treeMap.clone();
						for (Entry<Integer, CallAlertAitVO> entry : treeMapAux.entrySet())
						{

							if (entry != null)
							{
								if (!treeMapVtr.containsKey(entry.getKey()))
								{
									CallAlertAitVO aitVO = entry.getValue();

									String texto = retornaLogParaCadastro(aitVO);

									if (NeoUtils.safeIsNotNull(entry.getKey()))
										saveLogFim(texto, String.valueOf(entry.getKey()));

									if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty() && treeMap.containsKey(entry.getKey()))
										OrsegupsAlertAitEngine.treeMap.remove(entry.getKey());
									if (OrsegupsAlertAitEngine.waitingSnepResponseMap != null && !OrsegupsAlertAitEngine.waitingSnepResponseMap.isEmpty() && OrsegupsAlertAitEngine.waitingSnepResponseMap.containsKey(aitVO.getCallViaturaVO().getTelefone()))
										OrsegupsAlertAitEngine.waitingSnepResponseMap.remove(aitVO.getCallViaturaVO().getTelefone());

								}
							}
						}
					}
				}

				TreeMap<Integer, CallAlertAitVO> treeMapAux = new TreeMap<Integer, CallAlertAitVO>();
				if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty())
				{
					treeMapAux = (TreeMap<Integer, CallAlertAitVO>) OrsegupsAlertAitEngine.treeMap;

				}

				HashSet<Integer> cdViaturaListRemove = new HashSet<Integer>();

				for (Entry<Integer, ViaturaVO> entry : treeMapVtr.entrySet())
				{
					if (entry != null)
					{

						ViaturaVO viaturaVO = treeMapVtr.get(entry.getKey());

						int statusAtendimento = 0;
						if (viaturaVO.getDeslocamentoLento() != null && viaturaVO.getDeslocamentoLento())
							statusAtendimento = 2;
						else if (viaturaVO.getAtrasoDeslocamento() != null && viaturaVO.getAtrasoDeslocamento())
							statusAtendimento = 9;
						else if (viaturaVO.getAlertaNoLocal() != null && viaturaVO.getAlertaNoLocal())
							statusAtendimento = 3;

						if (statusAtendimento > 0)
						{
							CallAlertAitVO aitVO = new CallAlertAitVO();
							viaturaVO.setStatusVtr(String.valueOf(statusAtendimento));
							aitVO.setCallViaturaVO(viaturaVO);
							EventoVO eventoVO = new EventoVO();
							eventoVO = (EventoVO) OrsegupsAlertAitEngine.getEvento(viaturaVO.getCodigoViatura(), String.valueOf(statusAtendimento));

							if (eventoVO != null && OrsegupsAlertAitEngine.treeMap != null && OrsegupsAlertAitEngine.waitingSnepResponseMap != null)
							{

								if (aitVO != null && aitVO.getCallViaturaVO() != null && aitVO.getCallViaturaVO().getCodigoViatura() != null && !treeMap.containsKey(Integer.parseInt(aitVO.getCallViaturaVO().getCodigoViatura())))
								{
									aitVO.setCallEventoVO(eventoVO);

									if (aitVO.getCallEventoVO() != null && aitVO.getLastCallCalendar() == null && aitVO.getCallViaturaVO().getDeslocamentoLento() != null && aitVO.getCallViaturaVO().getDeslocamentoLento())
									{
										GregorianCalendar calendar = new GregorianCalendar();
										calendar.add(Calendar.MINUTE, -2);
										aitVO.setLastCallCalendar(calendar);
									}

									if (aitVO.getCallEventoVO() != null && aitVO.getListCallCalendar() == null)
									{
										aitVO.setListCallCalendar(new GregorianCalendar());
										//log.debug("Lista é vazia? " + OrsegupsAlertAitEngine.treeMap.isEmpty());

									}
									if (aitVO != null
											&& aitVO.getCallEventoVO() != null
											&& (aitVO.getCallEventoVO().getCodigoEvento().equals("XXX8") && (!aitVO.getCallEventoVO().getCodigoCentral().contains("AAAA") && !aitVO.getCallEventoVO().getCodigoCentral().contains("AAA1") && !aitVO.getCallEventoVO().getCodigoCentral().contains("AAA2") && !aitVO.getCallEventoVO().getCodigoCentral().contains("AAA3") && !aitVO.getCallEventoVO().getCodigoCentral().contains("AAA4") && !aitVO.getCallEventoVO().getCodigoCentral().contains("AAA5") && !aitVO
													.getCallEventoVO().getCodigoCentral().contains("AAA6"))) || !aitVO.getCallEventoVO().getCodigoEvento().equals("XXX8"))
									{

										log.debug("Adicionou " + aitVO.getCallViaturaVO().getNomeMotorista());
										OrsegupsAlertAitEngine.treeMap.put(Integer.parseInt(aitVO.getCallViaturaVO().getCodigoViatura()), aitVO);

										String texto = retornaLogParaCadastro(aitVO);

										if (aitVO.getCallEventoVO().getCodigoCentral() != null && aitVO.getCallEventoVO().getParticao() != null && aitVO.getCallEventoVO().getFantasia() != null)
											texto += " Conta: " + aitVO.getCallEventoVO().getCodigoCentral() + "[" + aitVO.getCallEventoVO().getParticao() + "] " + aitVO.getCallEventoVO().getFantasia();

										if (NeoUtils.safeIsNotNull(aitVO.getCallViaturaVO().getCodigoViatura()) && texto != null)
										{
											saveLog(texto, aitVO.getCallViaturaVO().getCodigoViatura());
										}
									}
								}
							}
							else if (eventoVO == null)
							{
								for (Entry<Integer, CallAlertAitVO> entryCom : treeMapAux.entrySet())
								{

									CallAlertAitVO listAitVO = (CallAlertAitVO) treeMapAux.get(entryCom.getKey());

									if (listAitVO != null && listAitVO.getCallViaturaVO().getCodigoViatura().equalsIgnoreCase(aitVO.getCallViaturaVO().getCodigoViatura()))
									{

										String texto = retornaLogParaCadastro(aitVO);

										if (NeoUtils.safeIsNotNull(listAitVO.getCallViaturaVO().getCodigoViatura()))
											saveLogFim(texto, listAitVO.getCallViaturaVO().getCodigoViatura());

										if (OrsegupsAlertAitEngine.waitingSnepResponseMap != null && !OrsegupsAlertAitEngine.waitingSnepResponseMap.isEmpty() && OrsegupsAlertAitEngine.waitingSnepResponseMap.containsKey(listAitVO.getCallViaturaVO().getTelefone()))
										{
											OrsegupsAlertAitEngine.waitingSnepResponseMap.remove(listAitVO.getCallViaturaVO().getTelefone());
											cdViaturaListRemove.add(Integer.parseInt(listAitVO.getCallViaturaVO().getCodigoViatura()));

										}

									}
								}
							}
						}
					}
				}

				if (cdViaturaListRemove != null && !cdViaturaListRemove.isEmpty())
				{
					for (Integer integer : cdViaturaListRemove)
					{
						if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty() && OrsegupsAlertAitEngine.treeMap.containsKey(integer))
							OrsegupsAlertAitEngine.treeMap.remove(integer);
					}

				}

				if (OrsegupsAlertAitEngine.treeMap != null && !OrsegupsAlertAitEngine.treeMap.isEmpty())
				{

					for (Entry<Integer, CallAlertAitVO> entryCom : treeMap.entrySet())
					{
						if (OrsegupsAlertAitEngine.waitingSnepResponseMap != null && !OrsegupsAlertAitEngine.waitingSnepResponseMap.isEmpty() && OrsegupsAlertAitEngine.waitingSnepResponseMap.containsKey(entryCom.getValue().getCallViaturaVO().getTelefone()))
						{
							CallAlertAitVO aitVOWait = OrsegupsAlertAitEngine.waitingSnepResponseMap.get(entryCom.getValue().getCallViaturaVO().getTelefone());
							entryCom.getValue().setLastCallCalendar(aitVOWait.getLastCallCalendar());
						}

					}

					List<CallAlertAitVO> listaOrdenada = new ArrayList<CallAlertAitVO>();
					listaOrdenada.addAll(treeMap.values());
					Collections.sort(listaOrdenada,new CallAlertAitVO());

					for (CallAlertAitVO aitAlertAitVO : listaOrdenada)
					{

						if ((regional != null && !regional.equals("null") && !regional.isEmpty() && aitAlertAitVO != null && aitAlertAitVO.getCallViaturaVO() != null && aitAlertAitVO.getCallViaturaVO().getNomeMotorista() != null && regional.length() > 3 && regional.contains(aitAlertAitVO.getCallViaturaVO().getNomeMotorista().substring(0, 3))) || (regional == null || regional.isEmpty() || regional.equals("null")))
						{
							int minutesSinceLastCall = Minutes.minutesBetween(new DateTime(aitAlertAitVO.getLastCallCalendar()), new DateTime(new GregorianCalendar())).getMinutes();
							int secondsSinceLastCall = Seconds.secondsBetween(new DateTime(aitAlertAitVO.getLastCallCalendar()), new DateTime(new GregorianCalendar())).getSeconds();

							log.debug("Ultima ligação realizada a " + minutesSinceLastCall + ":" + secondsSinceLastCall % 60 + " minutos. Para o evento: " + aitAlertAitVO.getCallEventoVO().getCodigoHistorico());
							String status = "0";
							if(aitAlertAitVO.getCallViaturaVO().getStatusVtr() != null && !aitAlertAitVO.getCallViaturaVO().getStatusVtr().isEmpty())
								status = aitAlertAitVO.getCallViaturaVO().getStatusVtr();
							alert(aitAlertAitVO, ramalOrigem, aitAlertAitVO.getCallViaturaVO().getTelefone(), status);
						}
					}
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar OrsegupsAlertAitEngine  " + e);

		}
		finally
		{
			if (OrsegupsAlertAitEngine.verificaViaturas)
				OrsegupsAlertAitEngine.verificaViaturas = Boolean.FALSE;

		}

	}

	private static ViaturaVO composeViaturaVO(NeoObject ostViaturaObject, String ramalOrigem)
	{
		EntityWrapper viaturaWrapper = new EntityWrapper(ostViaturaObject);

		String dataRecepcaoStr = "";
		GregorianCalendar calendar = (GregorianCalendar) viaturaWrapper.findValue("ultimaAtualizacao");
		if (calendar != null)
		{
			dataRecepcaoStr = NeoDateUtils.safeDateFormat(calendar);
		}
		String dataMovimentacaoStr = "";
		GregorianCalendar calendarMov = (GregorianCalendar) viaturaWrapper.findValue("dataMovimentacao");
		if (calendarMov != null)
		{
			dataMovimentacaoStr = NeoDateUtils.safeDateFormat(calendarMov);
		}
		String dataUltimoDeslocamentoStr = "";
		GregorianCalendar calendarUltimoDeslocamento = (GregorianCalendar) viaturaWrapper.findValue("ultimoStatusDeslocamento");
		if (calendarUltimoDeslocamento != null)
		{
			dataUltimoDeslocamentoStr = NeoDateUtils.safeDateFormat(calendarUltimoDeslocamento);
		}
		Float latitudeFloat = 0F;
		BigDecimal latitude = (BigDecimal) viaturaWrapper.findValue("latitude");
		if (latitude != null)
		{
			latitudeFloat = latitude.floatValue();
		}

		Float longitudeFloat = 0F;
		BigDecimal longitude = (BigDecimal) viaturaWrapper.findValue("longitude");
		if (longitude != null)
		{
			longitudeFloat = longitude.floatValue();
		}

		String nomeImagem = "carro.png";
		if (viaturaWrapper.findValue("tipoViatura.nomeImagem") != null)
		{
			nomeImagem = (String) viaturaWrapper.findValue("tipoViatura.nomeImagem");
		}

		ViaturaVO viaturaVO = new ViaturaVO();
		viaturaVO.setDataRecepcao(dataRecepcaoStr);
		viaturaVO.setLatitude(latitudeFloat);
		viaturaVO.setLongitude(longitudeFloat);
		viaturaVO.setVelocidade((Long) viaturaWrapper.findValue("velocidade"));
		viaturaVO.setTitulo((String) viaturaWrapper.findValue("titulo"));
		viaturaVO.setAltitude((Long) viaturaWrapper.findValue("altitude"));
		viaturaVO.setDirecao((Long) viaturaWrapper.findValue("direcao"));
		viaturaVO.setIgnicao((Boolean) viaturaWrapper.findValue("ignicao"));
		viaturaVO.setSatelites((Long) viaturaWrapper.findValue("satelites"));
		viaturaVO.setPlaca((String) viaturaWrapper.findValue("placa"));
		viaturaVO.setNomeMotorista((String) viaturaWrapper.findValue("motorista"));
		String moduloRastreador = (String) viaturaWrapper.findValue("rastreador.id") + " - " + (String) viaturaWrapper.findValue("rastreador.modeloEquipamento.nomeModelo");
		viaturaVO.setNomeImagem(nomeImagem);
		viaturaVO.setDeslocamentoLento(false);
		viaturaVO.setCodigoViatura((String) viaturaWrapper.findValue("codigoViatura"));
		if (viaturaWrapper.findValue("corLigacao") != null)
		{

			viaturaVO.setCorLigacao((String) viaturaWrapper.findValue("corLigacao.cor"));
			viaturaVO.setSufixoLigacao((String) viaturaWrapper.findValue("corLigacao.sufixoImagem"));

		}
		else
		{
			viaturaVO.setSufixoLigacao("");
		}
		viaturaVO.setAtrasoDeslocamento((Boolean) viaturaWrapper.findValue("atrasoDeslocamento"));
		viaturaVO.setDeslocamentoLento((Boolean) viaturaWrapper.findValue("deslocamentoNaoIniciado"));

		viaturaVO.setAlertaNoLocal((Boolean) viaturaWrapper.findValue("excessoDeTempoNoLocal"));

		//busca e valida os ramais/numeros para o snap discar
		String numeroDestino = (String) viaturaWrapper.findValue("telefone");
		viaturaVO.setTelefone(numeroDestino);
		if ((ramalOrigem == null || ramalOrigem.isEmpty() || ramalOrigem.equalsIgnoreCase("null")) && PortalUtil.getCurrentUser() != null)
		{
			EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
			ramalOrigem = (String) usuarioOrigemWrapper.findValue("ramal");
		}
		String link = OrsegupsUtils.getCallLink(ramalOrigem, numeroDestino, false);

		String imagemIgnicao = "images/led-red.gif";
		if (viaturaVO.getIgnicao())
		{
			imagemIgnicao = "images/led-green.gif";
		}
		StringBuilder contentString = new StringBuilder();
		contentString.append("<div id='content' style=\"width:500px; height:170px\"  ><div id='bodyContent'  >");
		contentString.append("<span class=\"gm-style-iw\"><b>Motorista:</b> </span><span class=\"gm-addr\">" + viaturaVO.getNomeMotorista());
		contentString.append("</span><br><span class=\"gm-style-iw\"><b>Placa:</b> </span><span class=\"gm-addr\">" + viaturaVO.getPlaca());
		contentString.append("</span><br><span class=\"gm-style-iw\"><b>Viatura em uso desde:</b> </span><span class=\"gm-addr\">" + dataMovimentacaoStr);
		contentString.append("</span><br><span class=\"gm-style-iw\"><b>Rastreador:</b> </span><span class=\"gm-addr\">" + moduloRastreador);
		contentString.append("</span><br><span class=\"gm-style-iw\"><b>Ignição:</b> </span><img src=\"" + imagemIgnicao + "\" />");
		contentString.append("<br><span class=\"gm-style-iw\"><b>Velocidade:</b> </span><span class=\"gm-addr\">" + viaturaVO.getVelocidade());
		contentString.append("Km/h</span>" + "<br>" + "<span class=\"gm-style-iw\"><b>Ultima Atualização:</b> </span><span class=\"gm-addr\">" + viaturaVO.getDataRecepcao() + "</span><br>");
		String dataStr = "";

		if (NeoUtils.safeIsNotNull(moduloRastreador) && moduloRastreador.contains("SEGWARE MOBILE"))
			dataStr = viaturaVO.getDataRecepcao().toString();
		else
			dataStr = dataUltimoDeslocamentoStr;

		contentString.append("<span class=\"gm-style-iw\"><b>Último Movimento:</b> </span><span class=\"gm-addr\">" + dataStr + "</span>");

		if (link != null)
		{
			contentString.append("<br><span class=\"gm-style-iw\"><b>Telefone:</b> <b>" + link + "</b></span>");
		}
		else if (numeroDestino != null && !numeroDestino.isEmpty())
		{
			contentString.append("<br><span class=\"gm-style-iw\">Telefone: <b>" + numeroDestino + "</b></span>");
		}

		//String textoLog = this.getLogViatura(ostViaturaObject);
		contentString.append("<br><a  id=\"div2\"  class=\"easyui-tooltip\" onmouseleave=\"javascript:removeTooltip('" + viaturaVO.getPlaca() + "')\" onmouseover=\"javascript:getLogViatura('" + viaturaVO.getPlaca() + "',this);\" ><img style=\"border : none\" src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");
		contentString.append("&nbsp<a  id=\"div2\"  class=\"easyui-tooltip\" onmouseleave=\"javascript:removeTooltip('" + viaturaVO.getPlaca() + "')\" onmouseover=\"javascript:getLogMotorista('" + viaturaVO.getPlaca() + "',this);\" ><img style=\"border : none\" src=\"" + PortalUtil.getBaseURL() + "imagens/icones_final/document_v_16x16-trans.png\"/></a>");

		contentString.append("</div> </div>");

		viaturaVO.setInfoWindowContent(contentString.toString());

		if (viaturaWrapper.findValue("statusViatura") != null)
		{
			GregorianCalendar calendarAtual = new GregorianCalendar();
			calendarAtual.add(Calendar.MINUTE, -20);

			Long status = (Long) viaturaWrapper.findValue("statusViatura.codigoStatus");
			viaturaVO.setCodigoStatus(status);
			if (status != null && status != 1L && calendar.before(calendarAtual))
			{
				NeoObject statusObj = getStatusViatura(0L);
				if (statusObj != null)
				{
					EntityWrapper entityWrapper = new EntityWrapper(statusObj);
					viaturaVO.setCorStatus((String) entityWrapper.findValue("Cor"));
					viaturaVO.setCorTexto((String) entityWrapper.findValue("corTexto"));
				}
			}
			else
			{
				if (viaturaWrapper.findValue("statusViatura.Cor") != null)
				{
					viaturaVO.setCorStatus((String) viaturaWrapper.findValue("statusViatura.Cor"));
				}

				if (viaturaWrapper.findValue("statusViatura.corTexto") != null)
				{
					viaturaVO.setCorTexto((String) viaturaWrapper.findValue("statusViatura.corTexto"));
				}
			}
		}

		return viaturaVO;
	}


//	private Collection<ViaturaVO> getDeslocamentoLento(List<DeslocamentoVO> listaDeslocamento)
//	{
//
//		Collection<ViaturaVO> lista = null;
//		String nomeFonteDados = "SIGMA90";
//		Connection conn = null;
//		PreparedStatement pstm = null;
//		ResultSet rs = null;
//		try
//		{
//
//			if (NeoUtils.safeIsNotNull(listaDeslocamento) && !listaDeslocamento.isEmpty())
//			{
//				lista = new ArrayList<ViaturaVO>();
//
//				conn = PersistEngine.getConnection(nomeFonteDados);
//
//				for (DeslocamentoVO deslocamentoVO : listaDeslocamento)
//				{
//
//					StringBuilder sql = new StringBuilder();
//					String placa = deslocamentoVO.getViaturaVO().getPlaca();
//					String regional = deslocamentoVO.getRegional();
//					String x8IgnoreTags = deslocamentoVO.getX8IgnoreTags();
//					String filtrosConta = deslocamentoVO.getFiltrosConta();
//					String filtrosNotConta = deslocamentoVO.getFiltrosNotConta();
//
//					String sqlEvento = "";
//					String sqlAnd = "";
//					String sqlAndH9 = "";
//
//					sql.append(" SELECT h2.CD_VIATURA, v.NM_PLACA ");
//					sql.append(" FROM HISTORICO h2 ");
//					sql.append(" INNER JOIN VIATURA v ON v.CD_VIATURA = h2.CD_VIATURA   ");
//					sql.append(" INNER JOIN DBCENTRAL c ON C.CD_CLIENTE = h2.CD_CLIENTE   ");
//					sql.append(" WHERE FG_STATUS = ? AND v.NM_PLACA = ? AND  h2.DT_VIATURA_DESLOCAMENTO <= DATEADD(mi,-1,GETDATE()) ");
//
//					if (regional != null && !regional.isEmpty() && !regional.equalsIgnoreCase("null"))
//					{
//						sqlAnd += "AND (  ";
//						StringTokenizer st = new StringTokenizer(regional, ",");
//						int count = 0;
//
//						while (st.hasMoreElements())
//						{
//							String siglaRegional = (String) st.nextElement();
//							if (count > 0)
//								sqlAnd += " OR ";
//							sqlAnd += " v.NM_VIATURA LIKE '" + siglaRegional + "%'  ";
//
//							count++;
//						}
//						sqlAnd += " ) ";
//
//					}
//
//					if (x8IgnoreTags != null && !x8IgnoreTags.isEmpty() && !x8IgnoreTags.equalsIgnoreCase("null"))
//					{
//						StringTokenizer stTag = new StringTokenizer(x8IgnoreTags, ",");
//						int countToken = 0;
//						while (stTag.hasMoreElements())
//						{
//							countToken++;
//							String tag = (String) stTag.nextElement();
//							if (countToken == 1)
//							{
//								sqlAnd += " AND (  ";
//								sqlAndH9 += " AND (  ";
//								sqlAnd += "  ISNULL (h2.TX_OBSERVACAO_GERENTE, '') NOT LIKE '%#" + tag + "%'  ";
//								sqlAndH9 += " ISNULL (h9.TX_OBSERVACAO_GERENTE, '') NOT LIKE '%#" + tag + "%'  ";
//							}
//							else
//							{
//								sqlAnd += "  OR ISNULL (h2.TX_OBSERVACAO_GERENTE, '') NOT LIKE '%#" + tag + "%'  ";
//								sqlAndH9 += "  OR ISNULL (h9.TX_OBSERVACAO_GERENTE, '') NOT LIKE '%#" + tag + "%' ";
//							}
//						}
//						if (countToken > 0)
//						{
//							sqlAnd += " ) ";
//							sqlAndH9 += " ) ";
//						}
//					}
//
//					Map<String, Collection<String>> filtroContaMap = new HashMap<String, Collection<String>>();
//					Map<String, Collection<String>> filtroNotContaMap = new HashMap<String, Collection<String>>();
//
//					if (filtrosConta != null && !filtrosConta.isEmpty())
//					{
//						String contasArray[] = filtrosConta.split(",");
//						for (String item : contasArray)
//						{
//							if (item != null)
//							{
//								String empresaConta[] = item.split(":");
//
//								if (empresaConta != null && empresaConta.length == 2)
//								{
//									String empresa = empresaConta[0];
//									String conta = empresaConta[1];
//
//									if (empresa != null && conta != null)
//									{
//										Collection<String> contas = filtroContaMap.get(empresa);
//
//										if (contas == null)
//										{
//											contas = new ArrayList<String>();
//										}
//
//										contas.add(conta);
//
//										filtroContaMap.put(empresa, contas);
//									}
//								}
//							}
//						}
//					}
//					if (filtrosNotConta != null && !filtrosNotConta.isEmpty())
//					{
//						String notContasArray[] = filtrosNotConta.split(",");
//						for (String itemNot : notContasArray)
//						{
//							if (itemNot != null)
//							{
//								String empresaNotConta[] = itemNot.split(":");
//
//								if (empresaNotConta != null && empresaNotConta.length == 2)
//								{
//									String notEmpresa = empresaNotConta[0];
//									String notConta = empresaNotConta[1];
//
//									if (notEmpresa != null && notConta != null)
//									{
//										Collection<String> notContas = filtroNotContaMap.get(notEmpresa);
//
//										if (notContas == null)
//										{
//											notContas = new ArrayList<String>();
//										}
//
//										notContas.add(notConta);
//
//										filtroNotContaMap.put(notEmpresa, notContas);
//									}
//								}
//							}
//						}
//					}
//
//					//filtra por conta e empresa
//					if (filtroContaMap != null && !filtroContaMap.isEmpty())
//					{
//						Boolean firstEmpresa = true;
//						Boolean needClose = false;
//
//						for (String empresa : filtroContaMap.keySet())
//						{
//							Collection<String> contas = filtroContaMap.get(empresa);
//
//							if (contas != null && !contas.isEmpty())
//							{
//								if (firstEmpresa)
//								{
//									sqlEvento += "AND (";
//									firstEmpresa = false;
//									needClose = true;
//								}
//								else
//								{
//									sqlEvento += "OR ";
//								}
//
//								sqlEvento += "( ";
//
//								if (!empresa.trim().equals("*"))
//								{
//									sqlEvento += " c.ID_EMPRESA = " + empresa + " AND ";
//								}
//
//								sqlEvento += " c.ID_CENTRAL IN (";
//								Boolean first = true;
//								for (String conta : contas)
//								{
//									if (first)
//									{
//										sqlEvento += "'" + conta + "'";
//										first = false;
//									}
//									else
//									{
//										sqlEvento += ", '" + conta + "'";
//									}
//								}
//								sqlEvento += ") ) ";
//							}
//						}
//
//						if (needClose)
//						{
//							sqlEvento += ") ";
//						}
//					}
//
//					//filtra (ELIMINA) contas (e empresa)
//					if (filtroNotContaMap != null && !filtroNotContaMap.isEmpty())
//					{
//						Boolean firstNotEmpresa = true;
//						Boolean needClose = false;
//
//						for (String notEmpresa : filtroNotContaMap.keySet())
//						{
//							Collection<String> notContas = filtroNotContaMap.get(notEmpresa);
//
//							if (notContas != null && !notContas.isEmpty())
//							{
//								if (firstNotEmpresa)
//								{
//									sqlEvento += "AND (";
//									firstNotEmpresa = false;
//									needClose = true;
//								}
//								else
//								{
//									sqlEvento += "OR ";
//								}
//
//								sqlEvento += "( ";
//
//								if (!notEmpresa.trim().equals("*"))
//								{
//									sqlEvento += " c.ID_EMPRESA <> " + notEmpresa + " AND ";
//								}
//
//								sqlEvento += " c.ID_CENTRAL NOT IN (";
//								Boolean first = true;
//								for (String notConta : notContas)
//								{
//									if (first)
//									{
//										sqlEvento += "'" + notConta + "'";
//										first = false;
//									}
//									else
//									{
//										sqlEvento += ", '" + notConta + "'";
//									}
//								}
//								sqlEvento += ") ) ";
//							}
//						}
//
//						if (needClose)
//						{
//							sqlEvento += ") ";
//						}
//					}
//
//					sql.append(sqlAnd.toString());
//					sql.append(sqlEvento.toString());
//					sql.append(" AND NOT EXISTS (SELECT CD_HISTORICO  ");
//					sql.append("  FROM HISTORICO h9 ");
//					sql.append(" INNER JOIN  DBCENTRAL c ON c.CD_CLIENTE = h9.CD_CLIENTE " + new StringBuilder(sqlEvento).toString() + " " + new StringBuilder(sqlAndH9).toString());
//					sql.append("  WHERE FG_STATUS IN (9,3)  ");
//					sql.append(" AND h2.CD_VIATURA = h9.CD_VIATURA) ");
//					pstm = conn.prepareStatement(sql.toString());
//					pstm.setString(1, "2");
//					pstm.setString(2, placa);
//					//					pstm.setString(3, "9");
//					//					pstm.setString(4, "3");
//					//					pstm.setString(5, "5");
//					rs = pstm.executeQuery();
//
//					if (rs.next())
//						deslocamentoVO.getViaturaVO().setDeslocamentoLento(Boolean.TRUE);
//					else
//						deslocamentoVO.getViaturaVO().setDeslocamentoLento(Boolean.FALSE);
//					lista.add(deslocamentoVO.getViaturaVO());
//				}
//			}
//		}
//		catch (Exception e)
//		{
//			log.error("##### getDeslocamentoLento ERRO ALTERAÇÃO STATUS VIATURA SEM DESLOCAMENTO - " + e.getMessage() + " Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
//			e.printStackTrace();
//		}
//		finally
//		{
//
//			try
//			{
//
//				OrsegupsUtils.closeConnection(conn, pstm, rs);
//			}
//			catch (Exception e)
//			{
//				log.error("#####  getDeslocamentoLento ERRO ALTERAÇÃO STATUS MAPAS : " + e.getMessage() + " Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
//				e.printStackTrace();
//			}
//			return lista;
//
//		}
//
//	}

//	private Boolean verificaAtrasoEvento(String cdViatura)
//	{
//		Connection conn = null;
//		PreparedStatement preparedStatementHSelect = null;
//		ResultSet rsH = null;
//		Boolean flag = Boolean.FALSE;
//		try
//		{
//			if (cdViatura != null && !cdViatura.isEmpty())
//			{
//				int diferencaMinutos = -10;
//				conn = PersistEngine.getConnection("SIGMA90");
//
//				StringBuilder sqlSigma = new StringBuilder();
//				sqlSigma.append(" SELECT h.CD_VIATURA ");
//				sqlSigma.append(" FROM HISTORICO h WITH (NOLOCK)  ");
//				sqlSigma.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON v.CD_VIATURA = h.CD_VIATURA ");
//				sqlSigma.append(" WHERE h.FG_STATUS IN (3) 	 ");
//				sqlSigma.append(" AND h.CD_EVENTO != 'XXX8'  ");
//				sqlSigma.append(" AND h.DT_VIATURA_NO_LOCAL < DATEADD(MINUTE, " + diferencaMinutos + ", GETDATE()) ");
//				sqlSigma.append(" AND h.CD_HISTORICO_PAI IS NULL  ");
//				sqlSigma.append(" AND v.NM_PLACA IS NOT NULL ");
//				sqlSigma.append(" AND v.NM_PLACA NOT LIKE '' ");
//				sqlSigma.append(" AND h.CD_VIATURA = ? ");
//
//				sqlSigma.append(" ORDER BY h.DT_VIATURA_DESLOCAMENTO DESC ");
//
//				preparedStatementHSelect = conn.prepareStatement(sqlSigma.toString());
//				preparedStatementHSelect.setString(1, cdViatura);
//				rsH = preparedStatementHSelect.executeQuery();
//				if (rsH.next())
//				{
//					flag = Boolean.TRUE;
//				}
//
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			log.error("##### ERRO verificaAtrasoEvento: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
//		}
//		finally
//		{
//			OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
//			return flag;
//		}
//
//	}

	private static NeoObject getStatusViatura(Long status)
	{
		NeoObject statusViatura = null;

		try
		{
			Class<NeoObject> statusViaturaClazz = AdapterUtils.getEntityClass("OTSStatusViatura");

			QLEqualsFilter equalsFilter = new QLEqualsFilter("codigoStatus", status);

			statusViatura = PersistEngine.getObject(statusViaturaClazz, equalsFilter);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return statusViatura;
	}
//
//	private Collection<EventoEsperaVO> buscarEventoEmEspera(String codigoViatura, String rota, String tipo)
//	{
//		Collection<EventoEsperaVO> eventosVOList = null;
//
//		EventoEsperaVO eventoEsperaVO = null;
//		try
//		{
//
//			QLGroupFilter filterAnd = new QLGroupFilter("AND");
//			filterAnd.addFilter(new QLEqualsFilter("cdViaturaSigma", codigoViatura));
//			Collection<NeoObject> eventosList = null;
//			eventosList = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAnd, -1, -1, "dataEspera ASC");
//			eventosVOList = new ArrayList<EventoEsperaVO>();
//			if (eventosList != null && !eventosList.isEmpty())
//			{
//				List<String> listaEventos = verificaEventoExisteHistorico();
//				for (NeoObject object : eventosList)
//				{
//					NeoObject neoObject = (NeoObject) object;
//					EntityWrapper eventoEsperaVOWrapper = new EntityWrapper(neoObject);
//					eventoEsperaVO = new EventoEsperaVO();
//
//					String cdHistorico = (String) eventoEsperaVOWrapper.getValue("codHistorico");
//
//					if (cdHistorico != null && listaEventos != null && !listaEventos.isEmpty() && listaEventos.contains(cdHistorico))
//					{
//
//						String cdRota = (String) eventoEsperaVOWrapper.getValue("codRota");
//						GregorianCalendar dataRetorno = (GregorianCalendar) eventoEsperaVOWrapper.getValue("dataEspera");
//						String codigo = (String) eventoEsperaVOWrapper.getValue("codigo");
//						String cdCliente = (String) eventoEsperaVOWrapper.getValue("cdCliente");
//						String cdViatura = (String) eventoEsperaVOWrapper.getValue("cdViaturaSigma");
//
//						eventoEsperaVO.setCodHistorico(cdHistorico);
//						eventoEsperaVO.setCodRota(cdRota);
//						eventoEsperaVO.setDataEspera(NeoDateUtils.safeDateFormat(dataRetorno, "dd/MM/yyyy HH:mm:ss"));
//						eventoEsperaVO.setCliente(cdCliente);
//						eventoEsperaVO.setCodigoViatura(cdViatura);
//
//						eventosVOList.add(eventoEsperaVO);
//
//					}
//					else if (neoObject != null)
//					{
//
//						PersistEngine.remove(neoObject);
//					}
//				}
//
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			log.error("ERRO OrsegupsMapsServlet AO ADD removerEventoEmEspera : " + e.getMessage());
//
//		}
//		finally
//		{
//			return eventosVOList;
//		}
//
//	}

//
//	private List<String> verificaEventoExisteHistorico()
//	{
//		Connection connection = null;
//		PreparedStatement preparedStatement = null;
//		ResultSet resultSet = null;
//		List<String> listaEvento = null;
//		try
//		{
//			connection = PersistEngine.getConnection("SIGMA90");
//			StringBuilder builder = new StringBuilder();
//			builder.append(" SELECT CD_HISTORICO FROM HISTORICO WITH (NOLOCK) WHERE  FG_STATUS = 1  ");
//			preparedStatement = connection.prepareStatement(builder.toString());
//			resultSet = preparedStatement.executeQuery();
//			listaEvento = new ArrayList<String>();
//			while (resultSet.next())
//			{
//				String historico = resultSet.getString("CD_HISTORICO");
//				listaEvento.add(historico);
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			log.error("Erro ao executar verificaEventoExisteHistorico AIT " + e);
//		}
//		finally
//		{
//			OrsegupsUtils.closeConnection(connection, preparedStatement, resultSet);
//			return listaEvento;
//		}
//	}

	@SuppressWarnings("unchecked")
	public Boolean verificaLog(String cdViatura, String texto)
	{
		try
		{
			GregorianCalendar date = new GregorianCalendar();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date.getTime());
			int minutos = -10;
			cal.add(Calendar.MINUTE, minutos);
			date.setTime(cal.getTime());

			QLGroupFilter filterAnd = new QLGroupFilter("AND");
			filterAnd.addFilter(new QLEqualsFilter("cd_viatura", Long.parseLong(cdViatura)));
			@SuppressWarnings("deprecation")
			ExternalEntityInfo infoVTR = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SIGMA90VIATURA");
			List<NeoObject> listaVTR = (List<NeoObject>) PersistEngine.getObjects(infoVTR.getEntityClass(), filterAnd);

			if (listaVTR != null && !listaVTR.isEmpty())
			{
				NeoObject viaturaObject = (NeoObject) listaVTR.get(0);
				EntityWrapper placaEntityWrapper = new EntityWrapper(viaturaObject);
				String placaStr = (String) placaEntityWrapper.findValue("nm_placa");

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) date);
				groupFilter.addFilter(new QLEqualsFilter("placaViatura", placaStr));
				groupFilter.addFilter(new QLOpFilter("textoLog", "LIKE", "%" + texto + "%"));
				groupFilter.addFilter(datefilter);
				List<NeoObject> neoObject = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMALogViatura"), groupFilter, -1, -1, "dataLog desc");

				if (neoObject != null && !neoObject.isEmpty())
					return Boolean.TRUE;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return Boolean.FALSE;
	}

	@SuppressWarnings("unchecked")
	public static ViaturaVO getViatura(String cdViatura, String regional, String x8IgnoreTags, String ramalOrigem, String aplicacao, String strEmUso, String filtrosConta, String filtrosNotConta, String stausVtr, String ativaNaFrotaVtr)
	{
		//regional, x8IgnoreTags, ramalOrigem, aplicacao, emUso, null, filtrosConta, filtrosNotConta, stausVtr, ativaNaFrota
		ViaturaVO viaturaVO = null;
		try
		{
			Class<?> viaturaClazz = null;
			viaturaClazz = AdapterUtils.getEntityClass("OTSViatura");
			String tipo = "viaturasCM";
			Boolean emUso = Boolean.TRUE;
			Boolean ativaNaFrota = Boolean.TRUE;
			String codigoTecnico = "";
			x8IgnoreTags = "busy";
			filtrosNotConta = "*:AAAA,*:AAA1,*:AAA2,*:AAA3,*:AAA4,*:AAA5,*:AAA6";
			
			aplicacao = "TAT,TSH";
			

			if (strEmUso != null && strEmUso.equals("nao"))
			{
				emUso = Boolean.FALSE;
			}
			else if (strEmUso != null && strEmUso.equals("na"))
			{
				emUso = null;
			}

			if (ativaNaFrotaVtr != null && ativaNaFrotaVtr.equals("nao"))
			{
				ativaNaFrota = Boolean.FALSE;
			}
			else if (ativaNaFrotaVtr != null && ativaNaFrotaVtr.equals("na"))
			{
				ativaNaFrota = null;
			}

			QLFilterIsNotNull latitudeIsNotNull = new QLFilterIsNotNull("latitude");
			QLFilterIsNotNull longitudeIsNotNull = new QLFilterIsNotNull("longitude");

			QLEqualsFilter emUsoFilter = new QLEqualsFilter("emUso", emUso);

			QLEqualsFilter ativaNaFrotaFilter = new QLEqualsFilter("ativaNaFrota", ativaNaFrota);

			QLEqualsFilter cdViaturaFilter = new QLEqualsFilter("codigoViatura", cdViatura);

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(latitudeIsNotNull);
			groupFilter.addFilter(longitudeIsNotNull);
			groupFilter.addFilter(cdViaturaFilter);
			if (emUso != null)
			{
				groupFilter.addFilter(emUsoFilter);
			}

			if (ativaNaFrota != null)
			{
				groupFilter.addFilter(ativaNaFrotaFilter);
			}

			if (tipo != null && tipo.equalsIgnoreCase("viaturasCM"))
			{

				// filtra viaturas ligadas a eventos X8 com tags de exclusao
				String nomeFonteDados = "SIGMA90";
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT v.NM_PLACA FROM HISTORICO h WITH (NOLOCK) INNER JOIN VIATURA v WITH (NOLOCK) ON  v.CD_VIATURA = h.CD_VIATURA ");
				sql.append(" WHERE h.FG_STATUS IN (2, 3, 9) AND h.CD_HISTORICO_PAI IS NULL ");

				String sqlFiltroTag = "";
				if (x8IgnoreTags != null)
				{
					StringTokenizer stTag = new StringTokenizer(x8IgnoreTags, ",");
					int countToken = 0;
					while (stTag.hasMoreElements())
					{
						countToken++;
						String tag = (String) stTag.nextElement();

						if (countToken == 1)
						{
							sqlFiltroTag = sqlFiltroTag + " AND (  ";
							sqlFiltroTag = sqlFiltroTag + "        ( h.TX_OBSERVACAO_GERENTE LIKE '%#" + tag + "%' ) ";
						}
						else
						{
							sqlFiltroTag = sqlFiltroTag + "        OR (h.TX_OBSERVACAO_GERENTE LIKE '%#" + tag + "%' ) ";
						}

					}
					if (countToken > 0)
					{
						sqlFiltroTag = sqlFiltroTag + " ) ";
					}
				}
				sql.append(sqlFiltroTag);

				Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql.toString());
				Collection<Object> resultList = query.getResultList();

				//Statement statement = null;
				Collection<String> listaNotPlaca = new ArrayList<String>();

				if (resultList != null)
				{
					for (Object result : resultList)
					{
						if (result != null)
						{
							String placa = ((String) result);
							if (placa != null)
							{
								listaNotPlaca.add(placa);
							}
						}
					}
				}

				// cria o filtro das viaturas tagadas
				QLNotInFilter listaExclusaoViaturasFilter = new QLNotInFilter("placa", listaNotPlaca);
				groupFilter.addFilter(listaExclusaoViaturasFilter);

				// somente filtra pela aplicacao da viatura se a aplicacao for passada como parametro (lista), caso contrario traz somente as viaturas da aplicacao TAT
				if (aplicacao != null && !aplicacao.isEmpty() && !aplicacao.equalsIgnoreCase("null"))
				{
					QLGroupFilter listaAplicacaoFilter = new QLGroupFilter("OR");
					StringTokenizer st = new StringTokenizer(aplicacao, ",");

					while (st.hasMoreElements())
					{
						String siglaAplicacao = (String) st.nextElement();
						QLEqualsFilter viaturaCMFilter = new QLEqualsFilter("aplicacaoViatura.sigla", siglaAplicacao);
						listaAplicacaoFilter.addFilter(viaturaCMFilter);
					}
					groupFilter.addFilter(listaAplicacaoFilter);
				}
				else
				{
					QLEqualsFilter viaturaCMFilter = new QLEqualsFilter("aplicacaoViatura.sigla", "TAT");
					groupFilter.addFilter(viaturaCMFilter);
				}

			}
			else if (tipo != null && tipo.equalsIgnoreCase("viaturasTecnicas"))
			{
				QLEqualsFilter viaturaTecnicaFilter = new QLEqualsFilter("aplicacaoViatura.sigla", "TEC");
				groupFilter.addFilter(viaturaTecnicaFilter);

				if (codigoTecnico != null)
				{
					QLGroupFilter listaTecnicosFilter = new QLGroupFilter("OR");
					StringTokenizer st = new StringTokenizer(codigoTecnico, ",");

					while (st.hasMoreElements())
					{
						String codTec = (String) st.nextElement();
						QLEqualsFilter tecnicoFilter = new QLEqualsFilter("codigoDoTecnico", codTec);
						listaTecnicosFilter.addFilter(tecnicoFilter);
					}
					groupFilter.addFilter(listaTecnicosFilter);

				}

			}

			Collection<NeoObject> ostViaturesObj = (Collection<NeoObject>) PersistEngine.getObjects(viaturaClazz, groupFilter);

			Collection<ViaturaVO> viaturaVOs = new ArrayList<ViaturaVO>();

			for (NeoObject obj : ostViaturesObj)
			{
				viaturaVO = composeViaturaVO(obj, ramalOrigem);

				if (viaturaVO != null)
				{
					viaturaVOs.add(viaturaVO);
					break;
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return viaturaVO;
	}

//	private static List<NeoUser> neoUsers()
//	{
//
//		List<NeoUser> users = new ArrayList<NeoUser>();
//
//		DecimalFormat f = new DecimalFormat("000000000");
//		ArrayList<HttpSession> hashMap = new ArrayList<HttpSession>();
//		SortedMap<String, HttpSession> sortedUsers = new TreeMap<String, HttpSession>();
//		for (HttpSession sessao : hashMap)
//		{
//			try
//			{
//				final String user = (String) sessao.getAttribute("user");
//				long l = 999999999l - sessao.getLastAccessedTime();
//				sortedUsers.put(f.format(l) + user.toLowerCase(), sessao);
//			}
//			catch (Exception e)
//			{
//				// Eat it
//			}
//		}
//
//		for (Map.Entry<String, HttpSession> entry : sortedUsers.entrySet())
//		{
//			try
//			{
//				HttpSession sessao = entry.getValue();
//				final String user = (String) sessao.getAttribute("user");
//
//				NeoUser nuser = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", user));
//				users.add(nuser);
//
//			}
//			catch (IllegalStateException e)
//			{
//				e.printStackTrace();
//			}
//
//		}
//		return users;
//	}
}
