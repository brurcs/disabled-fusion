package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesRelatorioSimplesRsc implements CustomJobAdapter
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesRelatorioSimplesRsc.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try
		{
			conn = PersistEngine.getConnection("FUSIONPROD");

			GregorianCalendar dataRelatorioInicio = new GregorianCalendar();

			GregorianCalendar dataInicio = OrsegupsUtils.getSpecificWorkDay(dataRelatorioInicio, 2L);
			dataRelatorioInicio.set(Calendar.HOUR_OF_DAY, 23);
			dataRelatorioInicio.set(Calendar.MINUTE, 59);
			dataRelatorioInicio.set(Calendar.SECOND, 59);

			GregorianCalendar dataPrazo = (GregorianCalendar) dataInicio.clone();

			GregorianCalendar dataInicioRel = new GregorianCalendar();
			dataInicioRel.add(dataInicioRel.DAY_OF_MONTH, -7);
			GregorianCalendar dataInicioRelelatorio = (GregorianCalendar) dataInicioRel.clone();

			GregorianCalendar dataFimRel = new GregorianCalendar();
			dataFimRel.add(dataFimRel.DAY_OF_MONTH, -2);
			GregorianCalendar dataFimRelelatorio = (GregorianCalendar) dataFimRel.clone();

			StringBuilder sql = new StringBuilder();

			log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Relatório RSC - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			
			//TODO RSC - Essa consulta precisa ser revisada para ser adequada ao novo modelo de RSC
			sql.append(" select p.code as Tarefa, p.startDate as InicioTarefa, his.data as DataOcorrencias, res.fullName as Responsavel, gra.descricao as graoSatisfacao ");
			sql.append(" from dbo.D_RSCRelatorioSolicitacaoCliente rsc ");
			sql.append(" inner join dbo.D_RSCRelatorioSolicitacaoCliente_historicoOcorrencias rhi on rhi.D_RSCRelatorioSolicitacaoCliente_neoId = rsc.neoId ");
			sql.append(" inner join dbo.D_RRCHistoricoOcorrencia his on his.neoId = rhi.historicoOcorrencias_neoId ");
			sql.append(" inner join dbo.D_RRCGrauSatisfacaoCliente gra on gra.neoId = his.grauSatisfacao_neoId ");
			sql.append(" inner join dbo.NeoUser res on res.neoId = his.responsavel_neoId ");
			sql.append(" inner join dbo.WFProcess p on p.entity_neoId = rsc.neoid ");
			sql.append(" where his.data >=  cast( '" + NeoUtils.safeDateFormat(dataInicioRelelatorio, "yyyy-MM-dd") + "' as datetime) ");
			sql.append(" and his.data <=  cast( '" + NeoUtils.safeDateFormat(dataFimRelelatorio, "yyyy-MM-dd") + "' as datetime) ");
			sql.append(" and p.saved = 1 and p.processState <> 2 ");
			sql.append(" order by p.startDate, his.data ");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			StringBuilder textoFinal = new StringBuilder();
			textoFinal = new StringBuilder();

			textoFinal.append("Prezado(a), </br></br>");
			textoFinal.append("Segue em anexo Relatório RSC, Período " + NeoUtils.safeDateFormat(dataInicioRelelatorio, "dd/MM/yyyy") + " de " + NeoUtils.safeDateFormat(dataFimRelelatorio, "dd/MM/yyyy") + ". Este relatório não serve para contabilizar produtividade</br>");

			String filename = "C:/Fusion/temp/RelatorioRsc_" + NeoUtils.safeDateFormat(dataInicioRelelatorio, "dd.MM.yyyy") + "-" + NeoUtils.safeDateFormat(dataFimRelelatorio, "dd.MM.yyyy") + ".xls";
			//String filename = "C:/Temp/RelatorioRsc_" + NeoUtils.safeDateFormat(dataInicioRelelatorio, "dd.MM.yyyy") + "-" + NeoUtils.safeDateFormat(dataFimRelelatorio, "dd.MM.yyyy") + ".xls";
			HSSFWorkbook hwb = new HSSFWorkbook();
			HSSFSheet sheet = hwb.createSheet("RelatorioRSC - ddMMyyyy");

			HSSFCellStyle cellStyle = hwb.createCellStyle();

			cellStyle.setFillBackgroundColor(HSSFColor.BLACK.index);
			cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

			HSSFFont font = hwb.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.WHITE.index);

			cellStyle.setFont(font);

			HSSFRow rowhead = sheet.createRow(0);
			rowhead.createCell(0).setCellValue("Tarefa");
			rowhead.getCell(0).setCellStyle(cellStyle);

			rowhead.createCell(1).setCellValue("Data Ínicio");
			rowhead.getCell(1).setCellStyle(cellStyle);

			rowhead.createCell(2).setCellValue("Data Ocorrencia");
			rowhead.getCell(2).setCellStyle(cellStyle);

			rowhead.createCell(3).setCellValue("Responsavel");
			rowhead.getCell(3).setCellStyle(cellStyle);

			rowhead.createCell(4).setCellValue("Grau de Satisfação");
			rowhead.getCell(4).setCellStyle(cellStyle);

			sheet.setColumnWidth(2, 12000);
			sheet.setColumnWidth(3, 12000);
			sheet.setColumnWidth(4, 5000);

			int countRow = 1;

			while (rs.next())
			{
				Long tarefa = rs.getLong("Tarefa");

				GregorianCalendar inicioTarefa = new GregorianCalendar();
				inicioTarefa.setTime(rs.getDate("InicioTarefa"));

				GregorianCalendar dataOcorrencias = new GregorianCalendar();
				dataOcorrencias.setTime(rs.getDate("DataOcorrencias"));

				String responsavel = rs.getString("Responsavel");
				String grau = rs.getString("graoSatisfacao");

				HSSFRow row = sheet.createRow(countRow);
				row.createCell(0).setCellValue(tarefa);
				row.createCell(1).setCellValue(NeoUtils.safeDateFormat(inicioTarefa, "dd/MM/yyyy"));
				row.createCell(2).setCellValue(NeoUtils.safeDateFormat(dataOcorrencias, "dd/MM/yyyy"));
				row.createCell(3).setCellValue(responsavel);
				row.createCell(4).setCellValue(grau);

				countRow++;
			}

			FileOutputStream fileOut = new FileOutputStream(filename);
			hwb.write(fileOut);
			fileOut.close();

			File file = new File(filename);
			NeoFile newFile = new NeoFile();
			newFile.setName(file.getName());
			newFile.setStorage(NeoStorage.getDefault());
			PersistEngine.persist(newFile);
			//PersistEngine.evict(newFile);
			OutputStream outi = newFile.getOutputStream();
			InputStream in = null;

			in = new BufferedInputStream(new FileInputStream(file));
			NeoStorage.copy(in, outi);
			newFile.setOrigin(NeoFile.Origin.TEMPLATE);

			file.delete();

			String solicitante = "fernanda.maciel";
			String titulo = "Relatório RSC";

			dataPrazo = OrsegupsUtils.getNextWorkDay(dataPrazo);
			dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
			dataPrazo.set(Calendar.MINUTE, 59);
			dataPrazo.set(Calendar.SECOND, 59);

			String executor = "";

			NeoPaper responsavel = new NeoPaper();
			responsavel = OrsegupsUtils.getPaper("Supervisor CEREC");
			if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
			{
				for (NeoUser user : responsavel.getUsers())
				{
					executor = user.getCode();
					break;
				}
			}
			else
			{
				//log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Relatório RSC - Papel " + responsavel.getCode() + " sem usuário definido - Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoUtils.safeDateFormat(dataInicio, "dd/MM/yyyy"));
			}

			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, textoFinal.toString(), "1", "sim", dataPrazo, newFile);

			Thread.sleep(1000);
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Erro ao Abrir Tarefa Simples Relatório RSC");
			e.printStackTrace();
			System.out.println("GENDADOR DE TAREFA: Erro ao Abrir Tarefa Simples Relatório RSC na Classe AbreTarefaSimplesRelatorioSimplesRsc");
			System.out.println("[" + key + "] ##### AGENDADOR DE TAREFA: Erro ao Abrir Tarefa Simples Relatório RSC");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{

			try
			{

				if (rs != null)
					rs.close();
				if (pstm != null)
					pstm.close();
				if (conn != null)
					conn.close();
			}
			catch (SQLException e)
			{

			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Relatório RSC - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	public void exportExcel()
	{
		WritableWorkbook wworkbook;
		try
		{
			wworkbook = Workbook.createWorkbook(new File("output.xls"));
			WritableSheet wsheet = wworkbook.createSheet("Teste", 0);
			Label label = new Label(0, 2, "A label record");
			wsheet.addCell(label);
			Number number = new Number(3, 4, 3.1459);
			wsheet.addCell(number);
			wworkbook.write();
			wworkbook.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (RowsExceededException e)
		{
			e.printStackTrace();
		}
		catch (WriteException e)
		{
			e.printStackTrace();
		}
	}
}
