package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.text.ParseException;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterDescricaoAfastamento
public class ConverterDescricaoAfastamento extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		NeoObject objPrincipal = field.getForm().getObject();
		EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);
		NeoObject neoColaborador = null;
		Short sitAfa = 0;
		String desAfa = "";
		String datafa = "";

		if (wPrincipal.getValue("colaborador") != null)
		{
			neoColaborador = (NeoObject) wPrincipal.getValue("colaborador");
			EntityWrapper wColaborador = new EntityWrapper(neoColaborador);
			Long numcad = (Long) wColaborador.getValue("numcad");
			Long numemp = (Long) wColaborador.getValue("numemp");

			StringBuilder buscaAfastamento = new StringBuilder();
			buscaAfastamento.append(" SELECT AFA.SITAFA,SIT.DESSIT,convert(varchar,datafa,103) as datafa FROM R038AFA AFA INNER JOIN R010SIT SIT ON SIT.CODSIT = AFA.SITAFA ");
			buscaAfastamento.append(" WHERE AFA.NUMCAD = :numcad AND AFA.NUMEMP = :numemp AND (AFA.DatTer IS NULL OR AFA.DatTer = '1900-12-31') ORDER BY AFA.DATAFA DESC");

			EntityManager entityManager = PersistEngine.getEntityManager("VETORH");
			Query queryBuscaAfa = entityManager.createNativeQuery(buscaAfastamento.toString());
			queryBuscaAfa.setParameter("numcad", numcad);
			queryBuscaAfa.setParameter("numemp", numemp);
			queryBuscaAfa.setMaxResults(1);
			if (queryBuscaAfa.getResultList() != null && queryBuscaAfa.getResultList().size() > 0)
			{
				Object obj = queryBuscaAfa.getSingleResult();
				if (NeoUtils.safeIsNotNull(obj))
				{
					Object[] resultado = (Object[]) obj;
					sitAfa = (Short) resultado[0];
					desAfa = (String) resultado[1];
					datafa = (String) resultado[2];

				}
			}
			
			if(!datafa.isEmpty()){
				GregorianCalendar datAfastamento = null;
				try
				{
					datAfastamento = OrsegupsUtils.stringToGregorianCalendar(datafa, "dd/MM/yyyy");
				}
				catch (ParseException e)
				{
					e.printStackTrace();
				}
				wPrincipal.setValue("dtAfa", datAfastamento);
			}
			else
			{
				return "<i>Trabalhando</i>";
			}
		}
		if (sitAfa > 0)
		{
			return sitAfa + " - " + desAfa;
		}
		else
		{
			return "";
		}

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
