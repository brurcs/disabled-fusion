package com.neomind.fusion.custom.orsegups.site.vo;


public class SiteEmpresaColaboradorVO {

	private Long codEmp;
	private String nomEmp;
	private SiteFilialColaboradorVO filial;
	
	
	public Long getCodEmp() {
		return codEmp;
	}
	public void setCodEmp(Long codEmp) {
		this.codEmp = codEmp;
	}
	public String getNomEmp() {
		return nomEmp;
	}
	public void setNomEmp(String nomEmp) {
		this.nomEmp = nomEmp;
	}
	public SiteFilialColaboradorVO getFilial()
	{
		return filial;
	}
	public void setFilial(SiteFilialColaboradorVO filial)
	{
		this.filial = filial;
	}
	
	
	
}
