function cancelarTarefa(){
	
	var code = document.getElementById("tarefaCancelar").value;
	var motivo = document.getElementById("motivoCancelar").value;
	$.ajax({
		type:"post",
		url:'https://intranet.orsegups.com.br/fusion/services/CancelarTarefas/cancelarTarefas;code='+code+';motivo='+motivo,
		dataType:'json',
		success: function(result) {
			var retorno = result.retorno;
			$('#processing-modal').modal('hide');
			if (retorno.indexOf("Sucesso") !== -1){
				$('#success-alert').show();
				setTimeout(function(){
					$('#success-alert').hide();
				}, 3500);
				document.getElementById("tarefaCancelar").value = "";
				document.getElementById("motivoCancelar").value = "";
			}else{
				document.getElementById("error-alert").innerHTML = "<strong>Erro!</strong> "+retorno;
				$('#error-alert').show();
				setTimeout(function(){
					$('#error-alert').hide();
				}, 3500);
			}
		},  
		error: function(e){  
			$('#warning-alert').show();
			setTimeout(function(){
				$('#warning-alert').hide();
			}, 3500);
			console.log(e);
		}
	});	
}

function buscarTarefa(){
	
	var code = document.getElementById("tarefaCancelar").value;
	$.ajax({
		type:"post",
		url:'https://intranet.orsegups.com.br/fusion/services/CancelarTarefas/buscarTarefa;code='+code,
		dataType:'json',
		success: function(result) {
			var retorno = result;
			document.getElementById("cliente").value = retorno.cliente;
			console.log(retorno.cliente);
		},  
		error: function(e){  
			console.log(e);
		}
	});	
}
  
$(document).ready(function(e){
	

});
