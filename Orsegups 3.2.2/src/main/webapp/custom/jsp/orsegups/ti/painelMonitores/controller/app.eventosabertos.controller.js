app.controller('appEventosAbertos', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){

	console.log('Iniciou controller eventos abertos');

  var codigos = "'Q002','Q004','Q501','Q023','Q043'";

  $scope.eventos = [];
  $scope.iconRefresh = "pull-right fa fa-refresh";
	  
	$http.post('http://localhost:8080/fusion/services/SIGMAUtils/getEventosAbertos/96674/'+codigos).success(function(data) {
		$scope.eventos = data;
	}).error(function() {
		console.log("Erro ao buscar eventos!");
	});


	$scope.atualizarEventos = function(){
		
		$scope.iconRefresh = "pull-right fa fa-refresh fa-spin";
		
		
		
		$http.post('http://localhost:8080/fusion/services/SIGMAUtils/getEventosAbertos/96674/'+codigos).success(function(data) {
			$scope.eventos = data;
			
		}).error(function() {
			console.log("Erro ao buscar eventos!");
		});
		
		
		$timeout(function(){
			$scope.iconRefresh = "pull-right fa fa-refresh";
		},1000);
		
	};



}]);
