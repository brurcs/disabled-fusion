<%@ page import="com.neomind.fusion.custom.orsegups.ti.RelatorioServlet"%>
<%@ page import="com.neomind.fusion.custom.orsegups.mobile.vo.NeoUserVO"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="dao"
	class="com.neomind.fusion.custom.orsegups.ti.RelatorioServlet" />
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script
	src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="js/pesquisaTarefas.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<link rel="stylesheet" href="css/transfere-tarefa.css">
<link rel="stylesheet" href="css/typeahead.css">

<title>Relatório de Tarefas</title>
</head>
<body>
	<%
	    RequestDispatcher rd = request
						.getRequestDispatcher("RelatorioServlet");
	%>
	<div class="alert alert-warning collapse" id="warning-alert"
		role="alert">
		<strong>Atenção!</strong> Erro ao listar tarefas.
	</div>

	<div class="alert alert-success collapse" id="success-alert"
		role="alert">
		<strong>Sucesso!</strong> Tarefas listadas.
	</div>

	<div class="container">
		<div id="loginbox" style="margin-top: 50px;"
			class="mainbox col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">Relatório de Tarefas Fechadas</div>
				</div>

				<div style="padding-top: 30px" class="panel-body">

					<div style="display: none" id="login-alert"
						class="alert alert-danger col-sm-12"></div>

					<form>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"> <i
								class="glyphicon glyphicon-calendar"></i>
							</span> <input class="form-control" name="dataInicio" id="dataInicio"
								type="date" placeholder="Data Início" required="required">
							<span class="input-group-addon"> <i
								class="glyphicon glyphicon-calendar"></i>
							</span> <input class="form-control" name="dataFim" id="dataFim"
								type="date" placeholder="Data Fim" required="required">
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"> <i
								class="glyphicon glyphicon-user"> EXECUTORES</i>
							</span> <select id="usuario" name="usuario"
								onchange="listaUsuarios(this);" class="form-control"
								style="width: 260px; height: 205px" multiple>
								<option value="todos">Todos</option>
								<option value="645688264">Diogo Simão da Silva</option>
								<option value="796206584">Felipe Souza Stegel</option>
								<option value="714203615">Filipe Amaral Neis</option>
								<option value="651231111">Lucas Alison de Lima</option>
								<option value="785835352">Lucas Ribeiro</option>
								<option value="702407129">Luiz Henrique Dandolini dos
									Reis</option>
								<option value="598912158">Mateus Alfredo Batista</option>
								<option value="701051940">Matheus Rodrigo Santos da
									Silva</option>
								<option value="810501877">Rodrigo Joaquim Nunes</option>
								<option value="47133909">Thiago Coelho</option>
							</select> 
							<span class="input-group-addon"> <i
								class="glyphicon glyphicon-user"> SOLICITANTES</i>
							</span> <select id="solicitante" name="solicitante" onchange="listaSolicitantes(this);" class="form-control"
								style="width: 260px; height: 205px" multiple>
								
							</select>
						</div>
						<div style="margin-top: 10px" class="form-group">
							<!-- Button -->

							<div class="col-sm-12 controls">
								<a id="btn-login" onclick="pesquisaTarefa();"
									class="btn btn-success" data-toggle="modal"
									data-target="#processing-modal">Pesquisar </a>
								<button class="btn btn-default" type="reset">Limpar</button>
							</div>
						</div>
					</form>
					</br> </br>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover"
								id="tabela-resultado">
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal modal-static fade" id="processing-modal"
		name="processing-modal" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="text-center">
						<img src="img/loading.gif" style="height: 80px;" class="icon" />
						<h4>Pesquisando...</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- javascript -->
<script type="text/javascript">

$(document).ready(function(){
	
	$.ajax({ 
	    type: 'POST', 
	    url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ti.RelatorioServlet',
	    beforeSend: function(jqXHR) {
	          jqXHR.overrideMimeType("text/html;charset=iso-8859-1");
	        },
	    data: 'action=listaUsuarios',
	    dataType: 'json',
	    success: function(result) {
	    	        
	    	var retorno = "";
	    		    	 			
 			for(i =0; i < result.length; i++){
 				
 				//Selecionando o ID da linha onde irá começar a exibir o resultado
 				var valor = window.document.getElementById("solicitante");
 				//salvo em uma variável o retorno do Servlet
 				retorno += "<option value="+result[i].code+" >"+result[i].fullName+"</option>";
 				
 			}
 			valor.innerHTML = retorno;
		},
        error: function(e) {
		}
	});   
});
</script>
</body>
</html>