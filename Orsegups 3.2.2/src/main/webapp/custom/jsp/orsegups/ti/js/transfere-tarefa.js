
function enviar(){
	
		 var tarefas =  document.getElementById("txt_tarefas_transferir").value;
		 var modelo =  document.getElementById("cmb_modelo_transferir").value;
		 var usuario =  document.getElementById("typeahead").value;
		 $.ajax({
	           type:"post",
	           url:"https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.UtilidadesController",
	           data:'action=transferirTarefa&txt_tarefas_transferir='+tarefas+'&cmb_modelo_transferir='+modelo+'&cmb_usuario_destino='+usuario,
	           dataType:'text',
	           success: function(result) {
	        	   $('#processing-modal').modal('hide');
	        	   
		    		$('#success-alert').show();
	    			setTimeout(function(){
	    				$('#success-alert').hide();
	    			}, 3500);
	           },  
      			error: function(e){  
		    		$('#warning-alert').show();
	    			setTimeout(function(){
	    				$('#warning-alert').hide();
	    			}, 3500);
      				 console.log(e);
          	 }
	       });	
	 
}

  
$(document).ready(function(e){
	 
	var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};

		
		var states = new Array();

		$.ajax({
		    type:"post",
		    url:"https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.UtilidadesController",
		    data:'action=getListaUsuarios',
		    dataType:"json",
		    success: function(msg) {
		    	states = msg;	
		  
				$('#typeahead').typeahead({
					  hint: true,
					  highlight: true,
					  minLength: 1
					},
					{
					  name: 'states',
					  source: substringMatcher(states)
				});
				
				 	
		    },  
				error: function(e){  
					 console.log(e);
			 }
		});

});
