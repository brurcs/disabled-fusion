<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>


<portlet:defineObjects />
<form:defineObjects />
<head>
	<meta charset="UTF-8">
</head>
<portal:head title="Relat�rio Comercial Privado">

<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/spin.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/spin.min.js"></script>
<script language="JavaScript" type="text/javascript">

	
	function showPopUp() 
	{
		var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/relatorios/relComTer.jsp';	
	
		
		$('#modalComTer').window('refresh', caminho);
		$('#modalComTer').window('open');
		
	}
	</script>
<cw:main>

	<cw:header title="Relat�rios Comercial Privado" />
	
	<cw:body id="area_scroll">
	
	<div class="float_icos">
	<a href="javascript:nop();">
	
	 <a href="javascript:void(0)"  iconCls="icon-add" plain="true" onclick="newUser()">
	
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>UTILITARIOS T.I.</strong>
	</p>
	
	<p>Efetuar marca��o de ponto</p>
	</a>
	</a>
	</div>
	
    <div id="dlg" class="easyui-dialog" style="width:520px;height:260px;padding:5px 10px"
            closed="true" buttons="#dlg-buttons" data-options="iconCls:'icon-save',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fm" method="post" novalidate>
              <div class="fitem">
                <label style="width: 100px;font-size:12px;">Empresa :</label>
                <input id="cbEmpresa" name="cbEmpresa" class="easyui-combobox"  style="width: 300px;font-size:11px;" data-options="
                    url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaEmpresas',
                    valueField:'codemp',method:'get',required:true,
                    textField:'codemp', formatter: formatItemEmpresa">
            </div>
            
            <div class="fitem">
                <label style="width: 100px;font-size:12px;">Matr�cula :</label>
                    <input class="easyui-validatebox" id="inMatricula" name="inInstalador" data-options="required:true,validType:'length[3,5]'">
            </div>
            
             <div class="fitem">
                <label style="width: 100px;font-size:12px;">Telefone :</label>
                <input class="easyui-validatebox" id="inTelefone" name="inTelefone" data-options="required:true,validType:'length[4,10]'">
            </div>
            
            <div class="fitem">
                <label style="width: 100px;font-size:12px;">Entrada / Sa�da :</label>
                <select
				 id="cbEntradaSaida" name="cbEntradaSaida" class="easyui-combobox" required="true"
				style="width: 150px;font-size:11px;" data-options="panelHeight:'46', editable:false, required:true">
					<option value="login">Entrada</option>
					<option value="logoff">Sa�da</option>
			</select>
            </div>
         <div class="fitem">
                <label style="width: 100px;font-size:12px;">Empresa :</label>
                <input id="cbEform" name="cbEform" class="easyui-combobox"  style="width: 300px;font-size:11px;" data-options="
                    url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaEforms',
                    valueField:'typeName',method:'get',required:true,
                    textField:'typeName', formatter: formatItemEform">
            </div
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:saveUser()">Confirmar</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancelar</a>
    </div>
    
	<div class="modal"></div>			
   	<script type="text/javascript">
   	
        function newUser(){
            $('#dlg').dialog('open').dialog('setTitle','&nbspMarcar ponto');
            $('#fm').form('clear');
        }
        
		/*Retornos poss�veis:
			OK - Sem erro
			1 - Empresa inv�lida!
			2 - Matr�cula inv�lida!
			3 - Entrada duplicada!
			4 - Acesso n�o permitido!
			99 - Erro desconhecido. Entre em contato com seu supervisor.*/
			
        function saveUser(){
        	
        	var empresa = $('#cbEmpresa').combobox('getValue');
    		var matricula = $('#inMatricula').val();
    		var telefone = $('#inTelefone').val();
    		var entradaSaida = $('#cbEntradaSaida').combobox('getValue');
    		var urlPonto = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/presencaViaSNEP.jsp?numEmp='+empresa+'&numCad='+matricula+'&acao='+entradaSaida+'&fone='+telefone;
            $('#fm').form('submit',{
            	type : "POST",
                url: urlPonto,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress(0);
            		}
                    return isValid;
                },
                success: function(result){
                	alert(result);
                	
                    
                    if (result){
                    	progress(1);
                    	error();
                    	
                    } else {
                    	progress(1);
                    	result();
                        
                    }
                }
            });
        }
        
      
        function formatItemEmpresa(row){
            var s = '<span style="font-weight:bold">' + row.codemp + '</span><br/>' +
                    '<span style="color:#888">' + row.nomemp + '</span>';
            return s;
        }
        
        function formatItemEform(row){
            var s = '<span style="font-weight:bold">' + row.typeName + '</span><br/>' +
                    '<span style="color:#888">' + row.title + '</span>';
            return s;
        }
        
        function myformatter(date) {
    		var y = date.getFullYear();
    		var m = date.getMonth() + 1;
    		var d = date.getDate();
    		return (d < 10 ? ('0' + d) : d) + '/' + (m < 10 ? ('0' + m) : m) + '/'
    				+ y;

    	}
        
    	function myparser(s) {
    		if (!s)
    			return new Date();
    		var ss = (s.split('-'));
    		var y = parseInt(ss[0], 10);
    		var m = parseInt(ss[1], 10);
    		var d = parseInt(ss[2], 10);
    		if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
    			return new Date(d, m - 1, y);
    		} else {
    			return new Date();
    		}
    	}
    	
    	function alertaCampos(){
            $.messager.alert('Aten��o','Campos Obrigat�rios!','warning');
        }
    	
    	 function progress(x){
	            var win = $.messager.progress({
	                title:'Por favor, aguarde!',
	                msg:'',
	                text: 'Carregando...'
	            });
	            setTimeout(function(){
	                if(x != 1)
	            	$.messager.progress('bar');
	                else
	                $.messager.progress('close');	
	            });
	        }
    	 
    	 function result(){
    			$.messager.alert('Aten��o','Marca��o efetuada com sucesso!','info');
    			
    	    }
    		
    	function error(){
    			$.messager.alert('Aten��o','Erro ao efetuar marca��o!','error');
    			
    	    }
    	
    	function spin(){
    		var opts = {
    	            lines: 10, // The number of lines to draw
    	            length: 7, // The length of each line
    	            width: 4, // The line thickness
    	            radius: 10, // The radius of the inner circle
    	            corners: 1, // Corner roundness (0..1)
    	            rotate: 0, // The rotation offset
    	            color: '#000', // #rgb or #rrggbb
    	            speed: 1, // Rounds per second
    	            trail: 60, // Afterglow percentage
    	            shadow: false, // Whether to render a shadow
    	            hwaccel: false, // Whether to use hardware acceleration
    	            className: 'spinner', // The CSS class to assign to the spinner
    	            zIndex: 2e9, // The z-index (defaults to 2000000000)
    	            top: 25, // Top position relative to parent in px
    	            left: 25 // Left position relative to parent in px
    	        };
    	        var target = document.getElementById('fm');
    	        var spinner = new Spinner(opts).spin(target);
    	}
    	
    </script>
    <style type="text/css">
        #fm{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
        .modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/images/loading.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}

.invisible {
    display: none;
}

.visible {
    display:block;
}

.left{
    float: left;
    margin-right: 3px;
}

.fieldContainer {
	display: inline-block;
}

.spanValue {
	margin-top: 3px;
}
	
.ico {
	width: 12px
}
    </style>
	
	
	
	
	
<style type="text/css">
input,textarea {
	border: 1px solid #95B8E7;
	padding: 2px;
}

       
</style>
	</cw:body>
	
</cw:main>
</portal:head>
