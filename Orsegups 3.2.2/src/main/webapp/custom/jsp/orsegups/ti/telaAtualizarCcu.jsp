<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/atualizar-centrocusto.css">
<script src="js/atualizar-centrocusto.js"></script>
<script src="js/jquery.growl.js" type="text/javascript"></script>
<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" 
		href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
		<script type="text/javascript" 
		src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

<title>Atualizar Centro de Custo - C001</title>
</head>
<body>
<div class="container">    
	<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
    	<div class="panel panel-info" >
        	<div class="panel-heading" align="center">
                        <div class="panel-title">Atualizar Centro de Custo - C001</div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
	            <form id="loginform" method="post" class="form-horizontal" role="form">
	            	<div style="margin-bottom: 25px" class="input-group">
	                    <span class="input-group-addon"><i class="glyphicon glyphicon-indent-right"></i></span>
	                    <input id="codCcu" name="centro_de_ custo" class="form-control" autocomplete="off" type="text" placeholder="Centro de custo de nível 6">                                        
	                   </div> 
	                <div style="margin-bottom: 25px" class="input-group">
	                    <span class="input-group-addon"><i class="glyphicon glyphicon-tasks"></i></span>
	                    <input id="codTar" name="cmb_usuario_destino" class="form-control" autocomplete="off" type="text" placeholder="Código Tarefa C001">                                        
	                </div>
	                    <div style="margin-top:10px" class="form-group" align="center">
	                        <!-- Button -->
							<div class="col-sm-12 controls">
	                          <a id="btn-listar" onclick="listar()" class="btn btn-success" data-toggle="modal" data-target="#processing-modal">Listar</a>
	                          <a id="btn-atualizar" onclick="enviar()" class="btn btn-success" data-toggle="modal" data-target="#processing-modal">Atualizar</a>
	                        </div>
	                    </div>
                </form>
                <div>
	 		</div>   
        	</div>
        </div>
      </div> 
</div>
<div class="modal modal-static fade" id="processing-modal" name="processing-modal" role="dialog" aria-hidden="true" >
  <div class="modal-dialog"  >
      <div class="modal-content"   >
          <div class="modal-body"  >
              <div class="text-center"   >	               
			<img src="img/loading.gif" style="height: 80px;" class="icon" />
            			<h4>Processando... </h4>
                </div>
            </div>
        </div>
    </div>
</div>
<table id="listaEform" class="table table-condensed table-hover table-striped"  data-options="singleSelect:true,collapsible:true,url:'datagrid_data1.json',method:'get'">
    <thead>
        <tr>
			<th>aCtaRed</th>
			<th>centroCustoContrato</th>
			<th>centroCusto6</th>
			<th>aClaCta6</th>
			<th>aClaCta</th>
        </tr>
    </thead>
</table>
</body>
<script type="text/javascript">
$('#listaEform').dataTable({                              
	   "oLanguage": {
	      "sProcessing": "Aguarde enquanto os dados são carregados ...",
	      "sLengthMenu": "Mostrar _MENU_ registros por pagina",
	      "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
	      "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
	      "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
	      "sInfoFiltered": "",
	      "sSearch": "Procurar",
	      "oPaginate": {
	         "sFirst":    "Primeiro",
	         "sPrevious": "Anterior",
	         "sNext":     "Próximo",
	         "sLast":     "Último"
	      }
	   }                              
	  });
</script>
</html>