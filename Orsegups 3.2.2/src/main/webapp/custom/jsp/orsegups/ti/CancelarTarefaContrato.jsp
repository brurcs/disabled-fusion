<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/cancelar-tarefa.css">
<script src="js/cancelar-tarefa.js"></script>

<title>Cancelar Tarefa</title>
</head>
<body>
<div class="alert alert-warning collapse" id="warning-alert" role="alert">
 		<strong>Aten��o!</strong> Erro ao cancelar tarefas.
</div>
<div class="alert alert-danger collapse" id="error-alert" role="alert">
</div>
<div class="alert alert-success collapse" id="success-alert" role="alert">
  		<strong>Sucesso!</strong> Tarefas canceladas com sucesso.
</div>

<div class="container">    
	<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
    	<div class="panel panel-info" >
        	<div class="panel-heading">
                        <div class="panel-title">Cancelar Tarefa</div>
            </div>     

            <div class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
	                
	            <form id="loginform" method="post" class="form-horizontal" role="form">
	            
	                <div class="form-group col-md-12 col-sm-12">
						<label class="col-md-4 control-label" for="tarefaCancelar">Tarefas � cancelar</label>
					</div>
					<div class="form-group col-md-12 col-sm-12">
						<div class="col-md-12">
							<input id="tarefaCancelar" name="tarefaCancelar" type="text"
								placeholder="Tarefas � cancelar" class="form-control input-md" onblur="buscarTarefa();">
						</div>
					</div>	
					<div class="form-group col-md-12 col-sm-12">
						<label class="col-md-2 control-label" for="cliente">Cliente</label>
					</div>
					<div class="form-group col-md-12 col-sm-12">
						<div class="col-md-12">
							<input id="cliente" name="cliente" type="text"
								placeholder="Cliente" class="form-control input-md" disabled>
						</div>
					</div>	
					<div class="form-group col-md-12 col-sm-12">	
						<label class="col-md-5 control-label" for="motivoCancelar">Motivo do cancelamento</label>
					</div>
					<div class="form-group col-md-12 col-sm-12">
						<div class="col-md-12">
							<textarea id="motivoCancelar" name="motivoCancelar"
								placeholder="Motivo do cancelamento" class="form-control input-md" rows="5"></textarea>

						</div>
					</div>
	
                    <div style="margin-top:10px;rigth:0" class="form-group">
                        <div class="col-sm-12 controls">
                          <a id="btn-processar" onclick="cancelarTarefa()" class="btn btn-success" data-toggle="modal" data-target="#processing-modal">Processar Cancelamento </a>                     
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                            </div>
                        </div>
                    </div>    
                </form>     

        	</div>                     
		</div>  
	</div>
</div>
    
<div class="modal modal-static fade" id="processing-modal" name="processing-modal" role="dialog" aria-hidden="true" >
  <div class="modal-dialog"  >
      <div class="modal-content"   >
          <div class="modal-body"  >
              <div class="text-center"   >	               
			<img src="img/loading.gif" style="height: 80px;" class="icon" />
            			<h4>Processando... </h4>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>