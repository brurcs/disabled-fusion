

<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.custom.orsegups.ti.TarefasGrupoUsuarioVO"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.custom.orsegups.ti.TarefasUsuarioVO"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>

<%@page import="com.neomind.fusion.custom.orsegups.ti.TarefasVO"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.TarefasUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@taglib uri="/WEB-INF/form.tld" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="application/vnd.android.package-archive; charset=ISO-8859-1">

	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<portal:head title="Controle de tarefas">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap.min.css">
	
	<!-- Optional theme -->
	<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap-theme.min.css">
	
	<!-- Latest compiled and minified JavaScript 	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script> -->

	
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>	
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>

	<style type="text/css">
	
	   body {
	    margin: 5px;
	    background: #A6A6A6;
	    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	    font-size: 14px;
	  }

		.wrapper {    
		margin-top: 80px;
		margin-bottom: 20px;
		}
		
		.form-signin {
		  max-width: 420px;
		  padding: 30px 38px 66px;
		  margin: 0 auto;
		  background-color: #eee;
		  border: 3px dotted rgba(0,0,0,0.1);  
		  }
		
		.form-signin-heading {
		  text-align:center;
		  margin-bottom: 30px;
		}
		
		.form-control {
		  position: relative;
		  font-size: 16px;
		  height: auto;
		  padding: 10px;
		}
		
		input[type="text"] {
		  margin-bottom: 0px;
		  border-bottom-left-radius: 0;
		  border-bottom-right-radius: 0;
		}
		
		input[type="password"] {
		  margin-bottom: 20px;
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		}
		
		.colorgraph {
		  height: 7px;
		  border-top: 0;
		  background: #c4e17f;
		  border-radius: 5px;
		  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
		  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
		  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
		  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
		}

	</style>

	<%
	 if (PortalUtil.getCurrentUser() == null) {
			out.print("Erro - Acesso Negado");
			return;
	}
	NeoUser user = PortalUtil.getCurrentUser();
	EntityWrapper ewUser = new EntityWrapper(user);
	String nome = PortalUtil.getCurrentUser().getFullName();
	String neoId = String.valueOf(PortalUtil.getCurrentUser().getNeoId());
	String extensionNumber = (String)ewUser.findField("ramal").getValue();
	
	response.setContentType("text/html");
	response.setCharacterEncoding("ISO-8859-1");
	PrintWriter outPrintWriter;
	try
	{
		outPrintWriter = response.getWriter();
	
		String action = "";
	
		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
		}
	
		if (action.equalsIgnoreCase("alterarRamal"))
		{
			this.alterarRamal(request, response, outPrintWriter);
		}

		outPrintWriter.flush();
	
	}
	catch (Exception e)
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	%>
	
<%!

private void alterarRamal(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
{

	
	String neoIdParam = request.getParameter("neoIdParam");
	String ramalParam = request.getParameter("ramalParam");

	String retorno = "OK";
		try
		{
			if(neoIdParam != null && !neoIdParam.isEmpty() && ramalParam != null && !ramalParam.isEmpty())
			{
			
				NeoUser user = (NeoUser)PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", Long.parseLong(neoIdParam)));
				EntityWrapper ewUser = new EntityWrapper(user);
				ewUser.findField("ramal").setValue(ramalParam);
				PersistEngine.persist(user);
				
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "ERRO";
			
		}
		finally
		{
			out.print(retorno);
		}
	}
%>
	<script type="text/javascript">
	
	
	$(document).ready(function() {
		document.getElementById("Username").value = "<%=PortalUtil.getCurrentUser().getFullName()%>";
		
	});
	
	function alterarRamal(){
		
		var neoId = "<%=neoId%>";
			 var ramal =  document.getElementById("Password").value;
			 $.ajax({
		           type:"post",
		           url:"https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/ti/alterarRamalHelp.jsp",
		           data:'action=alterarRamal&neoIdParam='+neoId+'&ramalParam='+ramal,
		           dataType:'text',
		           success: function(result) {
		        	   alert("Ramal Alterado");
		           },  
	      			error: function(e){  
	      				 console.log(e);
	          	 }
		       });	
	}
	
	
   
	</script>
	

<cw:main>
<cw:header title="Alterar Ramal" />
<cw:body id="area_scroll">	

 <div class = "container">
	<div class="wrapper">
		<form action="" method="post" name="Login_Form" class="form-signin">       
		    <h3 class="form-signin-heading">Bem vindo de volta! Por favor, altere o seu ramal.</h3>
			  <hr class="colorgraph"><br>
			  
			  <input type="text" class="form-control" id="Username" name="Username" placeholder="Username" required="" autofocus="" disabled="disabled"/>
			  <input type="number" class="form-control" id="Password" name="Password" placeholder="Novo Ramal" required=""/>     		  
			 
			  <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" onclick="alterarRamal();" type="Submit">Confirmar</button>  			
		</form>			
	</div>
</div>
			
</cw:body>
</cw:main>
</portal:head>
</html>