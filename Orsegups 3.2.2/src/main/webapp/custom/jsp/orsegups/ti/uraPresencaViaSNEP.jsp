<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>


<%
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.uraPresencaViaSNEP");

	response.setContentType("text/html");
	response.setCharacterEncoding("ISO-8859-1");
	PrintWriter outPrintWriter;
	try
	{
		outPrintWriter = response.getWriter();
	
		String action = "";
	
		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
		}
	
		if (action.equalsIgnoreCase("buscarMensagens"))
		{
			this.getBuscaMensagens(request, response, outPrintWriter);
		}
	
		if (action.equalsIgnoreCase("salvarLogMensagem"))
		{
			this.salvarLogMensagem(request, response, outPrintWriter);
		}
		outPrintWriter.flush();
	
	}
	catch (Exception e)
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

%>

<%!
/*
 * Retornos poss�veis:
 * OK - Sem erro
 * 9-Usu�rio n�o possui mensagens.
 * 99-Verifique as informa��es para salvar o log.
 * 199-Mensagem n�o encontrada.
 * 299-Mensagem n�o pode ser salva.
 * 999-Erro desconhecido (Exception)
 */

private void salvarLogMensagem(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
{

	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.uraPresencaViaSNEP");
	String codigo = request.getParameter("userField");
	String telefone = request.getParameter("telefone");
	String idMensagem = request.getParameter("idMensagem");
	String conclusaoMensagem = request.getParameter("conclusaoMensagem");

	String retorno = "";
		try
		{
			if (codigo != null && !codigo.isEmpty() && !codigo.contains("null") && telefone != null && !telefone.isEmpty() && !telefone.contains("null") && idMensagem != null && !idMensagem.isEmpty() && !idMensagem.contains("null") && idMensagem.contains("_"))
			{
				String id[] = idMensagem.split("_");
				retorno = "{\"id\":\"" + idMensagem + "\", \"return\":\"OK\"}";
				Long neoId = 0L;
				
				if(idMensagem.contains("ANIVERSARIO"))
					neoId = Long.parseLong(id[2]);
				else if(idMensagem.contains("FERIAS"))
					neoId = Long.parseLong(id[3]);
				else if(idMensagem.contains("RECICLAGEM"))
					neoId = Long.parseLong(id[3]);
				else
					neoId = Long.parseLong(id[1]);
					
				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("neoId", neoId));
				List<NeoObject> mensagemList = null;
				mensagemList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMensagem"), filter);

				if (mensagemList != null && !mensagemList.isEmpty())
				{
					NeoObject mensagemObj = AdapterUtils.createNewEntityInstance("TILeituraMensagem");

					EntityWrapper mensagemWrapper = new EntityWrapper(mensagemObj);

					if (mensagemWrapper != null)
					{

						mensagemWrapper.findField("telefone").setValue(Long.parseLong(telefone));
						mensagemWrapper.findField("userField").setValue(codigo);
						mensagemWrapper.findField("dataHora").setValue(new GregorianCalendar());

						if (conclusaoMensagem != null && !conclusaoMensagem.isEmpty() && conclusaoMensagem.contains("0"))
							conclusaoMensagem = "Parcial";
						else if (conclusaoMensagem != null && !conclusaoMensagem.isEmpty() && conclusaoMensagem.contains("1"))
							conclusaoMensagem = "Total";

						mensagemWrapper.findField("conclusaoMensagem").setValue(conclusaoMensagem);

						EntityWrapper mensagemWrapperComp = new EntityWrapper(mensagemList.get(0));

						PersistEngine.persist(mensagemObj);
						QLGroupFilter groupFilter = new QLGroupFilter("AND");
						groupFilter.addFilter(new QLEqualsFilter("userField", codigo));
						List<NeoObject> leituraList = null;
						leituraList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TILeituraMensagem"), groupFilter);
						
						if (leituraList != null && !leituraList.isEmpty())
						{
							Long quant = (Long) mensagemWrapperComp.findValue("quantidadeExecutada");
							quant = quant + 1;
							mensagemWrapperComp.findField("quantidadeExecutada").setValue(quant);
							mensagemWrapperComp.findField("leituraMensagem").addValue(leituraList.get(0));
							PersistEngine.persist(mensagemList.get(0));
						}
						else
						{
							retorno = "{\"id\":\"" + idMensagem + "\", \"return\":\"299\"}";
							log.error("299-Mensagem n�o pode ser salva. Ramal - " + idMensagem + " C�digo - " + codigo + " uniqueid - " + telefone);
						}

					}
				}
				else
				{
					retorno = "{\"id\":\"" + idMensagem + "\", \"return\":\"199\"}";
					log.error("199-Mensagem n�o encontrada. Ramal - " + idMensagem + " C�digo - " + codigo + " uniqueid - " + telefone);
				}
			}
			else
			{
				retorno = "{\"id\":\"" + idMensagem + "\", \"return\":\"99\"}";
				log.error("99-Verifique as informa��es. Ramal - " + idMensagem + " C�digo - " + codigo + " uniqueid - " + telefone);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "{\"id\":\"" + idMensagem + "\", \"return\":\"999\"}";
			log.error("999-Erro desconhecido (Exception) Ramal - " + idMensagem + " C�digo - " + codigo + " uniqueid - " + telefone);
		}
		finally
		{
			out.print(retorno);
		}
	}
%>

<%!
public void getBuscaMensagens(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
{
		GregorianCalendar dataAtual = new GregorianCalendar();

		String codigoCadastro = request.getParameter("mat");
		String codigoEmpresa = request.getParameter("emp");
		Connection connVetorh = null;
		PreparedStatement stColaborador = null;
		ResultSet rsColaborador = null;
		String retorno = "";
		try
		{

			StringBuffer queryColaborador = new StringBuffer();
			
			queryColaborador.append("  SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.DatAdm, fun.NomFun, fun.SitAfa, fun.DatAfa, fun.emicar,fun.NumCpf");
			queryColaborador.append("  FROM R034FUN fun WITH(NOLOCK)  ");
			queryColaborador.append("  WHERE fun.DatAdm <= getdate() AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > getdate())) "); 
			queryColaborador.append("  AND fun.TipCol = 1 AND fun.NumCad = ? AND fun.NumEmp = ? ");
			
//			queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.DatAdm, fun.NomFun, fun.SitAfa, fun.DatAfa, fun.emicar, sit.usu_blopre, ");
//			queryColaborador.append("        fun.NumCpf, hes.CodEsc, hes.CodTma, orn.NumLoc, orn.TabOrg, orn.usu_codccu ");
//			queryColaborador.append(" FROM R034FUN fun ");
//			queryColaborador.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
//			queryColaborador.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (hlo2.DATALT) FROM R038HLO hlo2 WHERE hlo2.NUMEMP = hlo.NUMEMP AND hlo2.TIPCOL = hlo.TIPCOL AND hlo2.NUMCAD = hlo.NUMCAD AND hlo2.DATALT <= ?) ");
//			queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (hes2.DATALT) FROM R038HES hes2 WHERE hes2.NUMEMP = hes.NUMEMP AND hes2.TIPCOL = hes.TIPCOL AND hes2.NUMCAD = hes.NUMCAD AND hes2.DATALT <= ?) ");
//			queryColaborador.append(" INNER JOIN R010SIT sit ON sit.CodSit = fun.SitAfa ");
//			queryColaborador.append(" INNER JOIN R016ORN orn ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = hlo.TabOrg ");
//			queryColaborador.append(" WHERE fun.DatAdm <= ? AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > ?)) ");
//			queryColaborador.append(" AND fun.TipCol = 1 AND fun.NumCad = ? AND fun.NumEmp = ? ");

			connVetorh = PersistEngine.getConnection("VETORH");
			Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
			stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
// 			stColaborador.setTimestamp(1, dataAtualTS);
// 			stColaborador.setTimestamp(2, dataAtualTS);
// 			stColaborador.setTimestamp(3, dataAtualTS);
// 			stColaborador.setTimestamp(4, dataAtualTS);
// 			stColaborador.setTimestamp(5, dataAtualTS);
// 			stColaborador.setLong(6, Long.parseLong(codigoCadastro));
// 			stColaborador.setLong(7, Long.parseLong(codigoEmpresa));

			stColaborador.setLong(1, Long.parseLong(codigoCadastro));
 			stColaborador.setLong(2, Long.parseLong(codigoEmpresa));

			rsColaborador = stColaborador.executeQuery();
			Long cpf = 0L;
			if (rsColaborador.next())
			{
				cpf = rsColaborador.getLong("NumCpf");
			}

			if (cpf != 0L)
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("cpf", cpf));
				QLOpFilter datefilter = new QLOpFilter("quantidadeExecucoes", "<=", 5L);
				filter.addFilter(datefilter);
				List<NeoObject> mensagemList = null;
				mensagemList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMensagem"), filter);

				if (mensagemList != null && !mensagemList.isEmpty())
				{
					int count = 0;
					for (NeoObject neoObject : mensagemList)
					{
						NeoObject object = (NeoObject) neoObject;

						EntityWrapper entityWrapper = new EntityWrapper(object);

						if (count > 0)
							retorno += ";";

						Long neoId = (Long) entityWrapper.findValue("neoId");
						
						String mensagem = (String) entityWrapper.findValue("tipoMensagem.descricao");
						
							if(mensagem != null && !mensagem.isEmpty() && mensagem.equals("ANIVERSARIO"))
							{
								mensagem += "_" + getBuscaData(Long.parseLong(codigoCadastro), Long.parseLong(codigoEmpresa),"Anivers�rio");
							}
							else if(mensagem != null && !mensagem.isEmpty() && mensagem.equals("FERIAS"))
							{
								mensagem += "_" + getBuscaData(Long.parseLong(codigoCadastro), Long.parseLong(codigoEmpresa),"F�rias");
							}
					
						
						retorno += mensagem + "_" + neoId;

						count++;
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
			if (retorno != null && retorno.isEmpty())
				retorno = "you do not have message";
			out.print(retorno);
		}

}
%>
<%!
public static String getBuscaData(Long codigoCadastro, Long codigoEmpresa,String descricaoSituacao)
	{

		Connection connVetorh = null;
		PreparedStatement stColaborador = null;
		ResultSet rsColaborador = null;
		String retorno = "";
		try
		{

			StringBuffer queryColaborador = new StringBuffer();
			queryColaborador.append("   SELECT TOP 1 fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, afa.DatAfa, afa.HorAfa, afa.DatTer, afa.ObsAfa, sit.DesSit,fun.datnas  ");
			queryColaborador.append("   FROM  FROM R034FUN fun  WITH(NOLOCK)   ");
			queryColaborador.append("   LEFT JOIN R038AFA afa WITH(NOLOCK)   ON fun.NumEmp = afa.NumEmp AND afa.NumCad = fun.NumCad AND fun.TipCol = afa.TipCol   ");
			queryColaborador.append("   LEFT JOIN R010SIT sit  WITH(NOLOCK)   ON sit.CodSit = afa.SitAfa   ");
			queryColaborador.append("   WHERE fun.NumEmp = ? AND fun.TipCol = 1 AND fun.NumCad = ? ");
			if(descricaoSituacao != null && !descricaoSituacao.isEmpty() && descricaoSituacao.equals("F�rias"))
			queryColaborador.append("   and sit.DesSit like ? ");
			queryColaborador.append("   ORDER BY afa.datafa desc   ");

			connVetorh = PersistEngine.getConnection("VETORH");
			stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
			stColaborador.setLong(1, codigoCadastro);
			stColaborador.setLong(2, codigoEmpresa);
			if(descricaoSituacao != null && !descricaoSituacao.isEmpty() && descricaoSituacao.equals("F�rias"))
			stColaborador.setString(3, descricaoSituacao);

			rsColaborador = stColaborador.executeQuery();
			if (rsColaborador.next())
			{
				if(descricaoSituacao != null && !descricaoSituacao.isEmpty() && descricaoSituacao.equals("F�rias"))
				{
				Timestamp datAfa = rsColaborador.getTimestamp("DatAfa");
				Timestamp datTer = rsColaborador.getTimestamp("DatTer");

				if (datAfa != null && datTer != null)
					retorno = (datAfa.getTime() / 1000) + "_" + (datTer.getTime() / 1000 );
				}
				else if(descricaoSituacao != null && !descricaoSituacao.isEmpty() && descricaoSituacao.equals("Anivers�rio"))
				{
					Timestamp datNas = rsColaborador.getTimestamp("datNas");

					if (datNas != null && datNas != null)
						retorno = String.valueOf(datNas.getTime() / 1000);
				}
			}

			OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
		}

		return retorno;
	}
%>
