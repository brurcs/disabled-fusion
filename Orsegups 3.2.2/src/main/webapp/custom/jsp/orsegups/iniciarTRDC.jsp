<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.HashSet"%>
<%
	InstantiableEntityInfo infoContact = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByType("RDCReversaoDeCancelamento");

	NeoObject novoCadastro = infoContact.createNewInstance();
	EntityWrapper wrp = new EntityWrapper(novoCadastro);

	NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("solicitante")));
	InstantiableEntityInfo eOrigem = AdapterUtils.getInstantiableEntityInfo("origemTarefa");
	NeoObject origem = (NeoObject) PersistEngine.getObject(eOrigem.getEntityClass(), new QLEqualsFilter("codigo", Long.valueOf(request.getParameter("origem"))));
	
	String titulo = request.getParameter("titulo");
	String descricao = request.getParameter("descricao");
	String sPrazo = request.getParameter("prazo");
	GregorianCalendar prazo = null;
	prazo = AdapterUtils.getGregorianCalendar(sPrazo, "dd/MM/yyyy");

	wrp.findField("solicitante").setValue(solicitante);
	wrp.findField("titulo").setValue(titulo);
	wrp.findField("descricao2").setValue(descricao);
	wrp.findField("prazo").setValue(prazo);
	
	PersistEngine.persist(novoCadastro);

	ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("Name", "C032 - RDC - Revers�o de Cancelamento"));

	WFProcess proc = processModel.startProcess(novoCadastro, null, null);

	proc.setSaved(true);
	proc.setRequester(solicitante);

	// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
	PersistEngine.persist(proc);
	PersistEngine.commit(true);

	// Finaliza a primeira tarefa
	Task task = null;
	final List acts = proc.getOpenActivities();
	final Activity activity1 = (Activity) acts.get(0);

	if (activity1 instanceof UserActivity)
	{
		task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, solicitante, new HashSet());
		task.finish();
	}
	out.print(proc.getCode());
	
%>