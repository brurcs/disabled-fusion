<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.TaskInstanceHelper"%>
<%@page import="com.neomind.fusion.workflow.task.rule.TaskRuleEngine"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.TaskLog.TaskLogType"%>
<%@page import="com.neomind.fusion.workflow.TaskLog"%>
<%@page import="com.neomind.fusion.workflow.TaskInstanceActionType"%>
<%@page import="com.neomind.fusion.workflow.handler.HandlerFactory"%>
<%@page import="com.neomind.fusion.workflow.handler.TaskHandler"%>
<%@page import="com.neomind.fusion.workflow.exception.AssignmentException"%>
<%@page import="com.neomind.fusion.workflow.task.central.search.TaskCentralIndex"%>
<%@page import="com.neomind.fusion.workflow.TaskInstance"%>
<%@page import="com.neomind.fusion.engine.runtime.RuntimeEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="java.math.BigInteger"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.Collection"%>
<%@page import="javax.persistence.Query"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%!
public ArrayList<String[]> retornaModelos(){
	ArrayList<String[]> retorno = new ArrayList<String[]>();
	
	
	String sql = "select neoId, name from processmodel pm  where pm.neoId in (select max(neoid) from processmodel pm1 where pm1.name = pm.name)  order by name";
	
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql);
	Collection<Object[]> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		for (Object result[] : resultList){
			String ret[] = new String[2];
			ret[0] = NeoUtils.safeOutputString(result[0]);
			ret[1] = NeoUtils.safeOutputString(result[1]);
			retorno.add(ret);
		}
	}
	return retorno;
}

public ArrayList<String[]> retornaUsuariosAtivos(){
	ArrayList<String[]> retorno = new ArrayList<String[]>();
	
	
	String sql = "select nu.neoid, se.code from neouser nu join dbo.SecurityEntity se on (nu.neoId = se.neoId) where nu.fullname not like '%ZZZ%' and se.active = 1 order by se.code";
	
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql);
	Collection<Object[]> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		for (Object result[] : resultList){
			String ret[] = new String[2];
			ret[0] = NeoUtils.safeOutputString(result[0]);
			ret[1] = NeoUtils.safeOutputString(result[1]);
			retorno.add(ret);
		}
	}
	return retorno;
}


public String[] retornaWfProcess(String code, String model){
	String ret[] = new String[3];
	
	String sql = " select neoId, code, (select substring(neoType,2,LEN(neoType)) as tipo from  NeoObject nob join neoType nty on nob.neoObjectType_id = nty.id where neoId= entity_neoId) " +
			" from wfprocess where model_neoid in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.neoId = "+model+" ) )  and code = " + code;
	
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql);
	Collection<Object[]> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		for (Object result[] : resultList){
			ret[0] = NeoUtils.safeOutputString(result[0]);
			ret[1] = NeoUtils.safeOutputString(result[1]);
			ret[2] = NeoUtils.safeOutputString(result[2]);
		}
	}
	return ret;
}
public String transferirTarefa(String codes, String model, String userDestinoCode, String motivo){
	
	String xCodes[] = codes.split(",");
	String strCodes = "'000000'";
	
	for (String x: xCodes){
		strCodes += ",'"+x+"'";
	}
	
	if (model == null ){ return "Voc� precisa selecionar o modelo de processo da tarefa"; }
	String query = null;
	query = " select ti.id, (select code from dbo.SecurityEntity n where n.neoId = ti.owner_neoid) as actualUserCode from dbo.WFProcess p ";
	query += " inner join dbo.Activity a on a.process_neoId = p.neoid ";
	query += " inner join dbo.Task t on t.activity_neoId = a.neoId ";
	query += " inner join dbo.Task_deadLine td on td.Task_neoId = t.neoId ";
	query += " inner join dbo.DeadLine d on d.neoId = td.deadLine_neoId ";
	query += " inner join dbo.TaskInstance ti on ti.task_neoId = t.neoId ";
	query += " where p.code in ( "+strCodes+" ) ";
	query += " and p.model_neoId in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.neoId = "+model+" ) ) ";
	
	EntityManager entity = PersistEngine.getEntityManager();
	Integer cont = 1;
	if(entity != null)
	{
		Query q = entity.createNativeQuery(query);
		List<Object[]> result = (List<Object[]>) q.getResultList();

		for (Object i[] : result)
		{
			Long id = ((BigInteger) i[0]).longValue();
			String actualUserCode = NeoUtils.safeOutputString(i[1]);
			
			try
			{
				//RuntimeEngine.getTaskService().delegate(Long.valueOf(id), actualUserCode, userDestinoCode, motivo);
				String taskId = String.valueOf(id);
			    String userId = actualUserCode;
	            String targetUserId = userDestinoCode;
	            String reason = motivo;
	
	            if (NeoUtils.safeIsNotNull(taskId) && NeoUtils.safeIsNotNull(userId) && NeoUtils.safeIsNotNull(targetUserId) && NeoUtils.safeIsNotNull(reason))
	            {
		
			       TaskInstance taskInstance = PersistEngine.getObject(TaskInstance.class, new QLEqualsFilter("id", Long.valueOf(taskId)));
			       NeoUser user = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userId));
			       NeoUser targetUser = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", targetUserId));
			
			       delegate(taskInstance, user, targetUser, reason);
			       System.out.println("[ORSETOOL] - debug " + Long.valueOf(id) +" - "+ actualUserCode +" - "+ userDestinoCode +" - "+ motivo);
				   System.out.println(cont + " : Processo transferido" );
		
	            }
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
				
				System.out.println("[ORSETOOL] - " + cont + " : ERRO: Processo n�o localizado!");
			}
			
			/*
			WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));
			
			if (proc != null)
			{
				RuntimeEngine.getProcessService().cancel(proc, motivo);
				System.out.println(cont + " : Processo " + proc.getCode());
			}
			else
			{
				System.out.println(cont + " : ERRO: Processo n�o localizado!");
			} */
			cont++;
		}
		return "Transferencia Processada com Sucesso!";
	}else{
		return "Nenhuma tarefa encontrada para transferencia";
	}
	
	
	
}
public String cancelaProcessos(String codes, String model, String motivo){
	String xCodes[] = codes.split(",");
	String strCodes = "'000000'";
	
	for (String x: xCodes){
		strCodes += ",'"+x+"'";
	}
	
	if (model == null ){ return "Voc� precisa selecionar o modelo de processo da tarefa"; }
	
	String query = "";
	
	query = " select neoId from wfprocess   where model_neoid in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.neoId = "+model+" ) ) and code in  " 
	+ "("+strCodes+")";
	
	
	EntityManager entity = PersistEngine.getEntityManager();
	Integer cont = 1;
	if(entity != null)
	{
		Query q = entity.createNativeQuery(query);
		List<BigInteger> result = (List<BigInteger>) q.getResultList();

		for (BigInteger i : result)
		{
			Long id = i.longValue();
			
			/**
			 * FIXME Cancelar tarefas.
			 */
			WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));
			
			if (proc != null)
			{
				RuntimeEngine.getProcessService().cancel(proc, motivo);
				System.out.println("[ORSETOOL] - " + cont + " : Processo " + proc.getCode());
			}
			else
			{
				System.out.println("[ORSETOOL] - " + cont + " : ERRO: Processo n�o localizado!");
			}
			cont++;
		}
		return "Cancelamento Processado com Sucesso!";
	}else{
		return "Nenhuma tarefa encontrada para cancelamento";
	}
	
	
}



public ArrayList<String> retornaEformExternoRelacionado(String nomeTabela){
	ArrayList<String> retorno = new ArrayList<String>();
	
	String sql =" select ei.typeName	from entityInfo ei "+
			" join externalEntityinfo ee on (ee.neoID = ei.neoID ) "+
			" join neoExternalTable et on (et.neoID = ee.externalTable_neoid) "+ 
			" left join neoDataSource dt on (dt.neoID = et.dataSource_neoID  ) "+
			" where upper(et.name) like upper('"+nomeTabela+"%') ";
	
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql);
	Collection<Object> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		for (Object result : resultList){
			if (!NeoUtils.safeOutputString(result).trim().equals("")){
				retorno.add(NeoUtils.safeOutputString(result));
			}
		}
	}
	return retorno;
}

public ArrayList<String> retornaEformTabelaRelacionado(String nomeEformExterno){
	ArrayList<String> retorno = new ArrayList<String>();
	
	String sql ="select dt.name +'.'+ et.name	from entityInfo ei "+ 
			 " join externalEntityinfo ee on (ee.neoID = ei.neoID ) "+ 
			 " join neoExternalTable et on (et.neoID = ee.externalTable_neoid) "+  
			 " left join neoDataSource dt on (dt.neoID = et.dataSource_neoID  ) "+ 
			 " where upper(ei.typeName) like upper('"+nomeEformExterno+"%') ";
	
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql);
	Collection<Object> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		for (Object result : resultList){
			retorno.add(NeoUtils.safeOutputString(result));
		}
	}
	return retorno;
}

// retorna todas tabelas que referenciam uma tabela
public ArrayList<String[]> retornaReferenciasParaTabela(String nomeTabela){
	
	ArrayList<String[]> retorno = new ArrayList<String[]>();
	
	String sql ="select t.name as Tabela, c.name as Coluna "+ 
			" from sys.foreign_key_columns as fk "+
			" inner join sys.tables as t on fk.parent_object_id = t.object_id "+
			" inner join sys.columns as c on fk.parent_object_id = c.object_id and fk.parent_column_id = c.column_id "+
			" where fk.referenced_object_id = (select object_id from sys.tables where name = '"+nomeTabela+"') ";
			//" order by TableWithForeignKey, FK_PartNo";
	
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql);
	Collection<Object[]> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		for (Object result[] : resultList){
			String ret[] = new String[2];
			ret[0] = NeoUtils.safeOutputString(result[0]);
			ret[1] = NeoUtils.safeOutputString(result[1]);
			retorno.add(ret);
		}
	}
	return retorno;
}


public void delegate(TaskInstance taskInstance, NeoUser user, NeoUser targetUser, String reason)
{
	if (targetUser == null)
		throw new AssignmentException("");

	Task task = taskInstance.getTask();

	TaskCentralIndex.getInstance().delete(taskInstance, true);

	taskInstance.getTask().getLogs().add(createLog(TaskLogType.TRANSFER, reason, user, taskInstance.getOwner()));

	TaskHandler taskHandler = HandlerFactory.TASK.get(taskInstance.getTask());
	Task t = taskHandler.transfer(user, targetUser);

	taskInstance.setOwner(t.getUser());

	// Remove a caixa
	RuntimeEngine.getTaskService().setBox(taskInstance, null);

	taskInstance.setActionType(TaskInstanceActionType.DELEGATED);

	// Remove os marcadores
	taskInstance.getTags().clear();

	// Executa regras
	this.refresh(taskInstance);

	taskInstance.setTask(t);
}

private TaskLog createLog(TaskLogType type, String reason, NeoUser user, NeoUser owner)
{
	TaskLog log = new TaskLog();
	log.setAction(type);
	log.setDateLog(new GregorianCalendar());
	log.setDescription(reason);
	log.setUser(user);
	log.setOwner(owner);

	PersistEngine.persist(log);

	return log;
}

/**
 * M�todo para atualizar os dados da tarefa, e executar novamente seus gatilhos.
 * 
 * @param taskInstance
 */
public void refresh(TaskInstance taskInstance)
{
	TaskInstanceHelper.update(taskInstance);
	TaskRuleEngine.get().run(taskInstance);
}




%>
    
<%
if (PortalUtil.getCurrentUser() != null ){
	

ArrayList<String[]> listaModelos = retornaModelos();
ArrayList<String[]> listaUsuarios = retornaUsuariosAtivos();
String code = "";
String codes = "";
String model = "";
String motivo = "";
String userDestinoCode = "";

String neoObjectID = "";
String eformName = "";

String retornoCancelamento = "";
String retornoTransferencia = "";

if (request.getParameter("act") != null && request.getParameter("act").equals("proc")){
	code = request.getParameter("txt_tarefa");
	model = request.getParameter("cmb_modelo");
	String wf[] = retornaWfProcess(code, model);
	neoObjectID = wf[0];
	eformName = wf[2];
}else if (request.getParameter("act") != null && request.getParameter("act").equals("cancelProc")){
	codes = request.getParameter("txt_tarefas");
	motivo = request.getParameter("txt_motivo");
	model = request.getParameter("cmb_modelo_cancelar");
	retornoCancelamento = cancelaProcessos(codes, model, motivo);
}else if ( request.getParameter("act") != null && request.getParameter("act").equals("transferProc") ){
	codes = request.getParameter("txt_tarefas_transferir");
	model = request.getParameter("cmb_modelo_transferir");
	userDestinoCode = request.getParameter("cmb_usuario_destino");
	retornoTransferencia = transferirTarefa(codes, model, userDestinoCode,"Tarefa transferida via TI");
}


%>    
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tool da TI</title>
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
	
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>	
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
	function dumpar(val){
		window.open("<%=PortalUtil.getBaseURL()%>adm/dumpEFormTree.jsp?id="+val);
	}
	
	function listar(val){
		window.open("<%=PortalUtil.getBaseURL()%>portal/render/FormList?type="+val);
	}
	
	function verificarEform(){
		document.getElementById('act').value = 'eformx';
		document.getElementById('frm').submit();
	}
	
	function verificarEform2(){
		document.getElementById('act').value = 'eformx2';
		document.getElementById('frm').submit();
	}
	
	function verificarEform3(){
		document.getElementById('act').value = 'eformx3';
		document.getElementById('frm').submit();
	}
	
</script>
</head>
<body>
	<form id="frm" method="post">
		<input type="hidden" id="act" name="act" value="proc">

		<center>
			<h2>Utilidades de Sistema para Desenvolvimento e Manuten��o</h2>
		</center>

		<hr/>
		
		<table>
			<tr>
				<td>
					
					<b>An�lise de Tarefas</b>
					<br>
					<select id="cmb_modelo" name="cmb_modelo" >
						<%
						for (String obj[] : listaModelos){
						%>
							<option value ="<%= obj[0] %>" <%= obj[0].equals(model) ? "selected='selected'" : "" %> ><%= obj[1] %></option>
						<%
						}
						%>
					</select>
					<br>
					
				</td>
				<td style="width: 600px;">
					<br/>
					Tarefa: <input type="text" id="txt_tarefa" name="txt_tarefa" value="<%= ( request.getParameter("act") != null && request.getParameter("act").equals("proc") ) ? code : "" %>" />
					<input type="submit" value="Processar" onclick="javascript:document.getElementById('act').value = 'proc';">
					<br/>
				</td>
				<td>
					<%if (request.getParameter("act") != null && request.getParameter("act").equals("proc")){ %>
					NeoID do processo : <%= neoObjectID %><br/>
					<a href="<%=PortalUtil.getBaseURL()%>adm/dumpNeoObject.jsp?id=<%= neoObjectID %>" target="blank">DumpNeoObject (<%= code + " - " + eformName %>)</a><br>
					<a href="<%=PortalUtil.getBaseURL()%>adm/dumpEFormTree.jsp?id=<%= eformName %>" target="blank">DumEformTree (<%= eformName %>)</a><br>
					<a href="<%=PortalUtil.getBaseURL()%>portal/render/FormList?type=<%= eformName %>" target="blank">FormList Principal (<%= eformName %>)</a><br><br>
					<%}else{ %>
					
					NeoID do processo : <br/>
					<a href="#">DumpNeoObject </a><br>
					<a href="#">DumEformTree </a><br>
					<a href="#">FormList Principal </a><br><br>
					<% } %>
				</td>
				<td style="padding-left: 50px;">
					<textarea rows="5" cols="50" readonly="readonly">
						Ajuda: Selecione o fluxo abaixo, passe o n�mero da tarefa e aperte em processar para procurar informa��es sobre tarefas e fluxos
					</textarea>
					
				</td>
			</tr>
			<tr><td colspan="10"><hr/></td></tr>
			<tr>
				<td>
					
					<b>Cancelamento de Tarefas</b>
					<br>
					<select id="cmb_modelo_cancelar" name="cmb_modelo_cancelar" >
						<%
						for (String obj[] : listaModelos){
						%>
							<option value ="<%= obj[0] %>" <%= obj[0].equals(model) ? "selected='selected'" : "" %> ><%= obj[1] %></option>
						<%
						}
						%>
					</select>
					<br>
					
				</td>
				
				<td>
					<table>
						<tr>
							<td>Tarefas � cancelar: </td>
							<td><input type="text" id="txt_tarefas" name="txt_tarefas" value="<%= ( request.getParameter("act") != null && request.getParameter("act").equals("cancelProc") ) ? codes : "" %>" style="width: 350px;" /></td>
						</tr>
						<tr>
							<td>Motivo cancelamento:</td>
							<td><input type="text" id="txt_motivo" name="txt_motivo" value="<%= motivo %>" style="width: 300px;" /></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" value="Processar Cancelamento" onclick="javascript:document.getElementById('act').value = 'cancelProc';"></td>
						</tr>
					</table>
					
					 
					 
				</td>
				<td>
					<span>&nbsp;&nbsp;Retorno: <%= (!retornoCancelamento.equals("") ?  retornoCancelamento : "" ) %></span>
				</td>
				<td style="padding-left: 50px;">
					<textarea rows="5" cols="50" readonly="readonly">
						Ajuda: 
						*Selecione o fluxo. 
						*Colcar as tarefas separadas por virgula, Ex.: 006654,006689,...
						*Escreva o motivo 
						*Clique em processar cancelamento
					</textarea>
					
				</td>
				
			</tr>
			<tr><td colspan="10"><hr/></td></tr>
			<tr >
				<td>
					
					<b>Transferencia de Tarefas</b>
					<br>
					<select id="cmb_modelo_transferir" name="cmb_modelo_transferir" >
						<%
						for (String obj[] : listaModelos){
						%>
							<option value ="<%= obj[0] %>" <%= obj[0].equals(model) ? "selected='selected'" : "" %> ><%= obj[1] %></option>
						<%
						}
						%>
					</select>
					<br>
					
				</td>
				
				<td>
					<table>
						<tr>
							<td>Usuario de Destino: </td>
							<td>
								<select id="cmb_usuario_destino" name="cmb_usuario_destino" >
									<%
									for (String obj[] : listaUsuarios){
									%>
										<option value ="<%= obj[1] %>" <%= obj[1].equals(userDestinoCode) ? "selected='selected'" : "" %> ><%= obj[1] %></option>
									<%
									}
									%>
								</select>
							</td>
						</tr>
						<tr>
							<td>Tarefas � transferir:</td>
							<td>
								<input type="text" id="txt_tarefas_transferir" name="txt_tarefas_transferir" value="<%= ( request.getParameter("act") != null && request.getParameter("act").equals("transferProc") ) ? codes : "" %>" style="width: 350px;" />
							</td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" value="Processar Transferencia" onclick="javascript:document.getElementById('act').value = 'transferProc';"></td>
						</tr>
					</table>
					 
				</td>
				<td>
					<span>&nbsp;&nbsp;Retorno: <%= (!retornoTransferencia.equals("") ?  retornoTransferencia : "" ) %></span>
				</td>
				<td style="padding-left: 50px;">
					<textarea rows="5" cols="50" readonly="readonly">
						Ajuda: 
						*Selecione o fluxo. 
						*Colcar as tarefas separadas por virgula, Ex.: 006654,006689,...
						*Escolher o usu�rio de destino 
						*Clique em processar Transferencia
					</textarea>
					
				</td>
				
			</tr>
			<tr><td colspan="10"><hr/></td></tr>
		</table>
		<br>
		
		Eform Especifico: <input id="eform" value="">
		<input type="button" value="DumpEformTree" onclick="javascript: dumpar(document.getElementById('eform').value); return false;">
		<input type="button" value="FormList" onclick="javascript: listar(document.getElementById('eform').value); return false;"><br/><br/>
		
		<hr/>
		<a href="<%=PortalUtil.getBaseURL()%>adm/log.jsp" target="_blank" >Abrir Configura��o de Logging</a><br/>
		<a href="<%=PortalUtil.getBaseURL()%>adm/sql.jsp" target="_blank" >Abrir SQL web</a><br/>
		<a href="<%=PortalUtil.getBaseURL()%>adm/hql.jsp" target="_blank" >Abrir HQL web</a><br/>
		<a href="<%=PortalUtil.getBaseURL()%>adm/activeUsers.jsp" target="blank">Usu�rios Ativos</a><br>
		
		<br/>
		
		<hr/>
		
		<table>
			<tr>
				<td>Verificar se h� eform externo apontado apara a tabela:</td>
				<td><input type="text" id ="eformx" name="eformx" ></td>
				<td><input type="button" value="Verificar" onclick="verificarEform()" /></td>
				<td>
					<textarea rows="2" cols="50">
						<%
							String eformx = ""; 
							if (request.getParameter("act") != null && request.getParameter("act").equals("eformx")){
								ArrayList<String> lista = retornaEformExternoRelacionado(request.getParameter("eformx")); 
								for (String x: lista){
									eformx += "\r\n"+x;
								}
						%> 
							<%= eformx + "\r\n" %>
						<% }
						%>
					</textarea>
				</td>
			</tr>
			<tr>
				<td>Verificar se a tabela apontada pelo eform externo:</td>
				<td><input type="text" id ="eformx2" name="eformx2" ></td>
				<td><input type="button" value="Verificar" onclick="verificarEform2()" /></td>
				<td>
					<textarea rows="2" cols="50">
						<%
							String eformx2 = ""; 
							if (request.getParameter("act") != null && request.getParameter("act").equals("eformx2")){
								ArrayList<String> lista = retornaEformTabelaRelacionado(request.getParameter("eformx2")); 
								for (String x: lista){
									eformx += "\r\n"+x;
								}
						%> 
							<%= eformx + "\r\n" %>
						<% }
						%>
					</textarea>
				</td>
			</tr>
			
			<tr>
				<td>Listar Tabelas que FK para a tabela:</td>
				<td><input type="text" id ="eformx3" name="eformx3" ></td>
				<td><input type="button" value="Verificar" onclick="verificarEform3()" /></td>
				<td>
					<textarea rows="5" cols="120">
						<%
							String eformx3 = ""; 
							if (request.getParameter("act") != null && request.getParameter("act").equals("eformx3")){
								ArrayList<String[]> lista = retornaReferenciasParaTabela(request.getParameter("eformx3")); 
								for (String x[]: lista){
									eformx += "\r\n"+x[0]+"-"+x[1];
								}
						%> 
							<%= eformx + "\r\n" %>
						<% }
						%>
					</textarea>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<br />		
		<hr/>
		
		
		
		
		
		
	</form>
<%} %>
</body>
</html>