<%@page import="com.google.javascript.rhino.head.ast.ForInLoop"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="org.apache.poi.hssf.record.crypto.Biff8DecryptingStream"%>
<%@page import="com.neomind.util.NeoDateUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper"%>
<%@page import="com.neomind.fusion.workflow.simulation.WorkflowService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="java.io.File"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.StringTokenizer"%>

<body>
	<%
		BigDecimal valorTercerizado = new BigDecimal(0.0);
		Long regional = NeoUtils.safeLong(request.getParameter("regional"));
		String departamento = request.getParameter("departamento");
		String solicitante = request.getParameter("solicitante");
		String arquivo = request.getParameter("arquivo");
		String outrasCidades = request.getParameter("outrasCidades") != null ? request.getParameter("outrasCidades") : ""; 
		String nomeCliente = request.getParameter("nomeCliente");
		String numeroContrato = request.getParameter("numeroContrato");
		Long codigoEmpresa = NeoUtils.safeLong(request.getParameter("codigoEmpresa"));
		Long diaBase = NeoUtils.safeLong(request.getParameter("diaBase"));
		Long codigoFilial = NeoUtils.safeLong(request.getParameter("codigoFilial"));
		String numerosPostos = request.getParameter("numerosPostos");
		String strVerificarEstoque = request.getParameter("verificarEstoque");
		String strGerarPosVenda = request.getParameter("iniciarPosVenda");
		String tipIns = request.getParameter("tipIns");
		Long codigoCliente = NeoUtils.safeLong(request.getParameter("codigoCliente"));
		Long quantidadePontos = NeoUtils.safeLong(request.getParameter("quantidadePontos"));
		Long numeroOs = NeoUtils.safeLong(request.getParameter("numeroOs"));
		Long tipoTecnico = NeoUtils.safeLong(request.getParameter("tipoTecnico"));
		String numeroPosto = request.getParameter("numeroPosto");
		String placas = request.getParameter("listaPlaca");
		String postos = request.getParameter("listaPosto");
		String numeroRMC = request.getParameter("numeroRMC");
		String isRastreamento = request.getParameter("isRastreamento");
		String isCtrHumanaOuAsseio = request.getParameter("isCtrHumanaOuAsseio");
		String isAllCompraDireta = request.getParameter("allCompraDireta");
		String isEstoqueDisponivel = request.getParameter("estoqueDisponivel");
		List<NeoObject> listaRastreamento = new ArrayList();
		boolean isHumanaOuAsseio = false;
		boolean allCompraDireta = false;
		boolean estoqueDisponivel = false;
		
		if(isEstoqueDisponivel != null) {
			estoqueDisponivel = (isEstoqueDisponivel.equalsIgnoreCase("sim") ? true : false);
		}
		
		if(isAllCompraDireta != null) {
			allCompraDireta = (isAllCompraDireta.equalsIgnoreCase("sim") ? true : false);
		}
		
		if(isCtrHumanaOuAsseio != null) {
			isHumanaOuAsseio = (isCtrHumanaOuAsseio.equals("sim")? true : false);
		} 
		
		if (request.getParameter("listaPlaca") != null && !request.getParameter("listaPlaca").isEmpty())
		{
			String[] listaPlaca = placas.split(",");
			String[] listaPosto = postos.split(",");

			for (int i = 0; i < listaPlaca.length; i++)
			{
				QLGroupFilter filtroRastreamento = new QLGroupFilter("AND");
				filtroRastreamento.addFilter(new QLEqualsFilter("placa", listaPlaca[i]));
				filtroRastreamento.addFilter(new QLEqualsFilter("posto", listaPosto[i]));
				List<NeoObject> eRmcRastreamento = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RmcRastreamento"), filtroRastreamento);

				if (eRmcRastreamento == null || eRmcRastreamento.isEmpty())
				{
					NeoObject wkfRastreamwntoRMC = AdapterUtils.getInstantiableEntityInfo("RmcRastreamento").createNewInstance();
					EntityWrapper ewWkfRastreamwntoRMC = new EntityWrapper(wkfRastreamwntoRMC);
					ewWkfRastreamwntoRMC.findField("placa").setValue(listaPlaca[i]);
					ewWkfRastreamwntoRMC.findField("posto").setValue(listaPosto[i]);
					PersistEngine.persist(wkfRastreamwntoRMC);
					listaRastreamento.add(wkfRastreamwntoRMC);
				}
				else
				{
					listaRastreamento.add(eRmcRastreamento.get(0));
				}
			}
		}
		if (request.getParameter("valorTercerizado") != null && !request.getParameter("valorTercerizado").isEmpty())
		{
			valorTercerizado = new BigDecimal(request.getParameter("valorTercerizado"));
		}
		Long tipoCliente = NeoUtils.safeLong(request.getParameter("tipoCliente"));

		try
		{
			Long regionalContrato = null;
			if (request.getParameter("regionalContrato") != null && !request.getParameter("regionalContrato").isEmpty() && !request.getParameter("regionalContrato").equals("null"))
			{
				regionalContrato = NeoUtils.safeLong(request.getParameter("regionalContrato"));
			}

			String executivoRMC = request.getParameter("executivoRMC");

			Boolean verificarEstoque = false;
			if (strVerificarEstoque != null && strVerificarEstoque.toLowerCase().equals("sim"))
			{
				verificarEstoque = true;
			}

			Boolean gerarPosVenda = false;
			if (strGerarPosVenda != null && strGerarPosVenda.toLowerCase().equals("sim"))
			{
				gerarPosVenda = true;
			}

			//String diretorio = "\\\\lagoa\\rmc\\";
			String diretorio = "\\\\ssoovt09\\f$\\Sistemas\\Sapiens\\RMC\\";

			NeoUser uSolic = (NeoUser) PersistEngine.getObject(SecurityEntity.class, new QLEqualsFilter("code", solicitante));
			if (uSolic == null)
			{
				out.print("Erro #1 - Usu�rio Solicitante n�o encontrado (code #" + solicitante + ")");
				return;
			}

			InstantiableEntityInfo infoRegional = AdapterUtils.getInstantiableEntityInfo("GCEscritorioRegional");
			InstantiableEntityInfo infoEmpresa = AdapterUtils.getInstantiableEntityInfo("GCEmpresa");
			InstantiableEntityInfo infoDepartamento = AdapterUtils.getInstantiableEntityInfo("RMCDepartamento");
			InstantiableEntityInfo infoResponsaveis = AdapterUtils.getInstantiableEntityInfo("RMCResponsaveisRecebimento");
			InstantiableEntityInfo infoResponsaveLAlmoxarifado = AdapterUtils.getInstantiableEntityInfo("RMCResponsavelAlmoxarifadoRegional");

			NeoObject eRegional = (NeoObject) PersistEngine.getObject(infoRegional.getEntityClass(), new QLEqualsFilter("codigo", regional));
			if (eRegional == null)
			{
				out.print("Erro #2 - Regional n�o encontrada (c�digo #" + regional + ")");
				return;
			}

			NeoObject eRegionalContrato = null;
			if (regionalContrato != null)
			{
				eRegionalContrato = (NeoObject) PersistEngine.getObject(infoRegional.getEntityClass(), new QLEqualsFilter("codigo", regionalContrato));
				if (eRegionalContrato == null)
				{
					out.print("Erro #3 - Regional do Contrato n�o encontrada (c�digo #" + regional + ")");
					return;
				}
			}

			NeoObject eEmpresa = (NeoObject) PersistEngine.getObject(infoEmpresa.getEntityClass(), new QLEqualsFilter("codigo", codigoEmpresa));
			if (eEmpresa == null)
			{
				out.print("Erro #4 - Empresa n�o encontrada (c�digo #" + codigoEmpresa + ")");
				return;
			}

			if ((diaBase <= 0) || (diaBase > 31))
			{
				out.print("Erro #5 - Dia Base inv�lido (#" + diaBase + ")");
				return;
			}

			NeoObject eDepartamento = (NeoObject) PersistEngine.getObject(infoDepartamento.getEntityClass(), new QLEqualsFilter("sigla", departamento));
			if (eDepartamento == null)
			{
				out.print("Erro #6 - Departamento n�o encontrado (sigla #" + departamento + ")");
				return;
			}

			NeoObject papelAlmoxarifadoRegional = null;
			NeoObject papelCoordenadorAlmoxarifadoRegional = null;
			NeoObject papelGerenteAlmoxarifadoRegional = null;
			NeoObject papelCoordenacaoAdministrativa = null;
			NeoObject papelGerenciaAdministrativa = null;

			NeoObject papelResponsavelAlmoxarifadoSede = null;
			NeoObject papelResponsavelAgendamento = null;

			if (regionalContrato != null)
			{
				QLEqualsFilter efResponsavelAlmoxarifadoRegional = new QLEqualsFilter("codigoRegional", eRegionalContrato);
				NeoObject oResponsavelAlmoxarifadoRegional = (NeoObject) PersistEngine.getObject(infoResponsaveLAlmoxarifado.getEntityClass(), efResponsavelAlmoxarifadoRegional);
				if (oResponsavelAlmoxarifadoRegional == null)
				{
					out.print("Erro #7 - Respons�vel pelo Almoxarifado da Regional do Contrato " + regionalContrato + " n�o encontrado ");
					return;
				}
				else
				{
					EntityWrapper almoxarifadoRegionalWrapper = new EntityWrapper(oResponsavelAlmoxarifadoRegional);
					if (almoxarifadoRegionalWrapper != null)
					{
						papelAlmoxarifadoRegional = (NeoObject) almoxarifadoRegionalWrapper.findValue("responsavelAlmoxarifado");
						papelCoordenadorAlmoxarifadoRegional = (NeoObject) almoxarifadoRegionalWrapper.findValue("coordenadorAlmoxarifado");
						papelGerenteAlmoxarifadoRegional = (NeoObject) almoxarifadoRegionalWrapper.findValue("gerenciaAlmoxarifado");
					}
				}
			}

			if (((codigoEmpresa == 18L && codigoFilial == 2L) || codigoEmpresa == 21L || codigoEmpresa == 22L) && (tipIns != null && (tipIns.equals("D") || (tipIns.equals("F")))) && verificarEstoque)
			{
				papelAlmoxarifadoRegional = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Almoxarife - CTA [RMC]"));
				papelCoordenacaoAdministrativa = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Coordenador �rea T�cnica CTA"));
				papelGerenciaAdministrativa = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Gerente de Seguran�a Eletr�nica CTA"));
			}
			else if (((codigoEmpresa == 18L && codigoFilial == 2L) || codigoEmpresa == 21L || codigoEmpresa == 22L) && (tipIns != null && tipIns.equals("N")) && verificarEstoque)
			{
				papelResponsavelAlmoxarifadoSede = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Almoxarife - SOO [RMC]"));
				papelCoordenacaoAdministrativa = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Coordenador Almoxarifado de Eletr�nica Sede"));
				papelGerenciaAdministrativa = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Gerente Administrativo"));
			}
			else
			{
				if (tipIns != null && (tipIns.equals("N") || tipIns.equals("F")))
				{
					papelResponsavelAlmoxarifadoSede = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Almoxarife - SOO [RMC]"));
					papelCoordenacaoAdministrativa = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Coordenador Almoxarifado de Eletr�nica Sede"));
					papelGerenciaAdministrativa = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Gerente Administrativo"));
				}
				else
				{
					papelResponsavelAlmoxarifadoSede = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "AlmoxarifeSOOCompraDiretaRMC"));
					papelCoordenacaoAdministrativa = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Coordenador Almoxarifado de Eletr�nica Sede"));
					papelGerenciaAdministrativa = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "Gerente Administrativo"));
				}
			}

			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("regional", eRegional));
			gp.addFilter(new QLEqualsFilter("departamento", eDepartamento));
			NeoObject eResponsaveis = (NeoObject) PersistEngine.getObject(infoResponsaveis.getEntityClass(), gp);
			System.out.println("Dalvan" + eRegional + "DEP " + eDepartamento);
			if (eResponsaveis == null)
			{
				out.print("Erro #9 - Lista de respos�veis para a regional e departamento passados, n�o foi encontrada");
				return;
			}

			EntityWrapper ewResp = new EntityWrapper(eResponsaveis);
			Collection<NeoPaper> listaPapeisResp = (Collection<NeoPaper>) ewResp.findField("listaDestinatarios").getValues();
			if (listaPapeisResp == null || listaPapeisResp.size() <= 0)
			{
				out.print("Erro #10 - Lista de destinat�rios n�o existe ou est� vazia para Regional: " + regional + " e Departamento: " + departamento + ".");
				return;
			}

		
			
			Set<NeoUser> listaResp = new HashSet<NeoUser>();
			for (NeoPaper papel : listaPapeisResp)
			{
				listaResp.addAll(papel.getAllUsers());
			}
			
			/* Incluir na lista de respons�veis o departamento presen�a. Eles dever�o receber todas as rmc's de servi�os das fam�lias SER101 e SER103 */
			if(isHumanaOuAsseio){
				NeoPaper receberRmcDepartamentoPresenca = PersistEngine.getNeoObject(NeoPaper.class, new QLEqualsFilter("code", "RMCTomarCienciaDepartamentoPresenca"));			
				NeoUser usuarioPresenca = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(receberRmcDepartamentoPresenca);
				listaResp.add(usuarioPresenca);
			}

			QLEqualsFilter equal = new QLEqualsFilter("Name", "C005 - Distribui��o de RMC");
			ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
			if (pm == null)
			{
				out.print("Erro #11 - Modelo de processo n�o encontrado");
				return;
			}

			// Se verificar Estoque - fluxo almoxarifado - entao deve ser criada apenas uma tarefa, caso contr�rio uma para cada usuario do papel)
			if (verificarEstoque)
			{

				//File file = new File("S:\\RMC\\"+arquivo);
				File file = new File(diretorio + arquivo);
				NeoFile newFile = new NeoFile();
				newFile.setName(file.getName());
				newFile.setStorage(NeoStorage.getDefault());
				PersistEngine.persist(newFile);
				OutputStream outi = newFile.getOutputStream();
				InputStream in = null;

				try
				{
					in = new BufferedInputStream(new FileInputStream(file));
					NeoStorage.copy(in, outi);
					newFile.setOrigin(NeoFile.Origin.TEMPLATE);
				}
				catch (FileNotFoundException e)
				{
					e.printStackTrace();
					out.print("Erro #12 - Relat�rio RMC n�o encontrado #" + diretorio + arquivo);
					return;
				}

				NeoObject wkfDistribuicaoRMC = AdapterUtils.getInstantiableEntityInfo("RelatorioMovimentacaoContrato").createNewInstance();
				EntityWrapper ewWkfDistribuicaoRMC = new EntityWrapper(wkfDistribuicaoRMC);

				String anexos = request.getParameter("anexos");
				if ((anexos != null) && (anexos != ""))
				{
					StringTokenizer st = new StringTokenizer(anexos, ",");
					String diretorioAnexo = "\\\\lagoa\\arqcvs\\";
					List<NeoObject> listaAnexos = new ArrayList();
					InstantiableEntityInfo rmcAnexos = AdapterUtils.getInstantiableEntityInfo("RMCAnexos");
					while (st.hasMoreTokens())
					{
						String token = st.nextToken();
						File anexo = new File(diretorioAnexo + token);
						NeoFile newAnexo = new NeoFile();
						NeoObject objAnexos = rmcAnexos.createNewInstance();
						EntityWrapper wAnexos = new EntityWrapper(objAnexos);
						newAnexo.setName(anexo.getName());
						newAnexo.setStorage(NeoStorage.getDefault());
						PersistEngine.persist(newAnexo);
						PersistEngine.persist(objAnexos);
						OutputStream outAnexo = newAnexo.getOutputStream();
						InputStream inAnexo = null;
						inAnexo = new BufferedInputStream(new FileInputStream(anexo));
						NeoStorage.copy(inAnexo, outAnexo);
						newAnexo.setOrigin(NeoFile.Origin.TEMPLATE);
						wAnexos.findField("anexos").setValue(newAnexo);
						listaAnexos.add(objAnexos);
					}
					ewWkfDistribuicaoRMC.findField("anexos").setValue(listaAnexos);
				}

				/*IN�CIO - Regra Nova do Prazo da Atividade de RMC de 36hs*/

				GregorianCalendar prazoRMC = new GregorianCalendar();
				//prazoRMC.set(2017, 03, 27, 15, 45);

				Integer horaRMC = 24 - prazoRMC.get(GregorianCalendar.HOUR_OF_DAY);
				Integer horaRMCRes = 24 - horaRMC;

				Integer hora = prazoRMC.get(GregorianCalendar.HOUR_OF_DAY);
				Integer minuto = prazoRMC.get(GregorianCalendar.MINUTE);
				Integer segundo = prazoRMC.get(GregorianCalendar.SECOND);
				//System.out.println("Horas que est�o sobrando = " + horaRMCRes + " - " + "Horas que faltam para fechar 00:00 = " + horaRMC + " - " + "Hora/Minuto/Segunda atual = " + hora + ":" + minuto + ":" + segundo);

				prazoRMC.add(prazoRMC.HOUR, horaRMC);
				//System.out.println(NeoDateUtils.safeDateFormat(prazoRMC, "dd/MM/yyyy HH:mm:ss"));

				prazoRMC.set(GregorianCalendar.HOUR, 0);
				prazoRMC.set(GregorianCalendar.MINUTE, minuto);
				prazoRMC.set(GregorianCalendar.SECOND, segundo);
				prazoRMC.add(prazoRMC.HOUR, horaRMCRes);
				//System.out.println(NeoDateUtils.safeDateFormat(prazoRMC, "dd/MM/yyyy HH:mm:ss"));

				boolean verificaDiaUtil = OrsegupsUtils.isWorkDay(prazoRMC);
				if (!verificaDiaUtil)
				{
					prazoRMC = OrsegupsUtils.getSpecificWorkDay(prazoRMC, 1L);
					prazoRMC.add(prazoRMC.HOUR, horaRMCRes + 12);

					Integer horaFinal = prazoRMC.get(GregorianCalendar.HOUR_OF_DAY);
					prazoRMC.set(GregorianCalendar.HOUR, horaFinal);
					prazoRMC.set(GregorianCalendar.MINUTE, minuto);
					prazoRMC.set(GregorianCalendar.SECOND, segundo);
				}
				else
				{
					prazoRMC.add(prazoRMC.HOUR, 12);
					boolean ehDiaUtil = OrsegupsUtils.isWorkDay(prazoRMC);
					if (!ehDiaUtil)
					{
						Integer horaFinal = prazoRMC.get(GregorianCalendar.HOUR_OF_DAY);
						prazoRMC = OrsegupsUtils.getSpecificWorkDay(prazoRMC, 1L);

						prazoRMC.add(prazoRMC.HOUR, horaFinal);
						prazoRMC.set(GregorianCalendar.MINUTE, minuto);
						prazoRMC.set(GregorianCalendar.SECOND, segundo);
					}
				}
				//System.out.println(NeoDateUtils.safeDateFormat(prazoRMC, "dd/MM/yyyy HH:mm:ss"));				

				/*FIM - Regra Nova do Prazo da Atividade de RMC de 36hs*/

				if (isRastreamento != null && !isRastreamento.equals(""))
				{
					if (isRastreamento.equals("SIM"))
					{
						ewWkfDistribuicaoRMC.findField("isRastreamento").setValue(true);
						papelResponsavelAgendamento = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "CMAgendamentoInstalacaoRastreamento"));
					}
					else
					{
						ewWkfDistribuicaoRMC.findField("isRastreamento").setValue(false);
						papelResponsavelAgendamento = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "CM - Agendamento Instala��o [RMC]"));
					}

					if (numeroPosto != null && !numeroPosto.equals(""))
					{
						ewWkfDistribuicaoRMC.findField("numeroPosto").setValue(Long.parseLong(numeroPosto));
					}

					if (numeroRMC != null && !numeroRMC.equals(""))
					{
						ewWkfDistribuicaoRMC.findField("numeroRMC").setValue(Long.parseLong(numeroRMC));
					}
				}
				else
				{
					ewWkfDistribuicaoRMC.findField("isRastreamento").setValue(false);
					papelResponsavelAgendamento = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "CM - Agendamento Instala��o [RMC]"));
				}
				ewWkfDistribuicaoRMC.findField("arquivo").setValue(newFile);
				ewWkfDistribuicaoRMC.findField("solicitante").setValue(uSolic);
				ewWkfDistribuicaoRMC.findField("empresa").setValue(eEmpresa);
				ewWkfDistribuicaoRMC.findField("nomeCliente").setValue(nomeCliente);
				ewWkfDistribuicaoRMC.findField("numeroContrato").setValue(numeroContrato);
				ewWkfDistribuicaoRMC.findField("diaBase").setValue(diaBase);
				ewWkfDistribuicaoRMC.findField("verificarEstoque").setValue(verificarEstoque);
				ewWkfDistribuicaoRMC.findField("filial").setValue(codigoFilial);
				ewWkfDistribuicaoRMC.findField("numerosPostos").setValue(numerosPostos);
				ewWkfDistribuicaoRMC.findField("quantidadePontos").setValue(quantidadePontos);
				ewWkfDistribuicaoRMC.findField("NumeroOs").setValue(numeroOs);
				ewWkfDistribuicaoRMC.findField("tipoTecnico").setValue(tipoTecnico);
				ewWkfDistribuicaoRMC.findField("valorTercerizado").setValue(valorTercerizado);
				ewWkfDistribuicaoRMC.findField("tipIns").setValue(tipIns);
				ewWkfDistribuicaoRMC.findField("responsavelAlmoxarifadoSede").setValue(papelResponsavelAlmoxarifadoSede);
				ewWkfDistribuicaoRMC.findField("responsavelAlmoxarifadoRegional").setValue(papelAlmoxarifadoRegional);
				ewWkfDistribuicaoRMC.findField("responsavelAlmoxarifadoParana").setValue(papelAlmoxarifadoRegional);
				ewWkfDistribuicaoRMC.findField("coordenadorAlmoxarifado").setValue(papelCoordenadorAlmoxarifadoRegional);
				ewWkfDistribuicaoRMC.findField("gerenteAlmoxarifadoRegional").setValue(papelGerenteAlmoxarifadoRegional);
				ewWkfDistribuicaoRMC.findField("coordenadorAdministrativo").setValue(papelCoordenacaoAdministrativa);
				ewWkfDistribuicaoRMC.findField("gerenteAdministrativo").setValue(papelGerenciaAdministrativa);
				ewWkfDistribuicaoRMC.findField("responsavelAgendamento").setValue(papelResponsavelAgendamento);
				ewWkfDistribuicaoRMC.findField("executivoRMC").setValue(executivoRMC);
				ewWkfDistribuicaoRMC.findField("prazoRMC").setValue(prazoRMC);
				ewWkfDistribuicaoRMC.findField("regionalContrato").setValue(eRegionalContrato);
				ewWkfDistribuicaoRMC.findField("tipoCliente").setValue(tipoCliente);
				ewWkfDistribuicaoRMC.findField("rastreamento").setValue(listaRastreamento);  
				ewWkfDistribuicaoRMC.findField("outrasCidades").setValue(outrasCidades);
				ewWkfDistribuicaoRMC.findField("todosCompraDireta").setValue(allCompraDireta);
				ewWkfDistribuicaoRMC.findField("estoqueDisponivel").setValue(estoqueDisponivel);
				
				System.out.println(NeoDateUtils.safeDateFormat(prazoRMC, "dd/MM/yyyy"));

				GregorianCalendar prazoAgendarInstSemEquip = new GregorianCalendar();
				if (tipIns.equals("C") || tipIns.equals("E"))
				{
					GregorianCalendar prazoHabilitacao = new GregorianCalendar();
					prazoHabilitacao.set(Calendar.MINUTE, 30);

					GregorianCalendar prazoAgendamento = new GregorianCalendar();
					prazoAgendamento = OrsegupsUtils.getSpecificWorkDay(prazoAgendamento, 5L);

					ewWkfDistribuicaoRMC.findField("data").setValue(prazoHabilitacao);
					ewWkfDistribuicaoRMC.findField("dataAgendamento").setValue(prazoAgendamento);
				}
				else if (tipIns.equals("R") || tipIns.equals("Y"))
				{
					prazoAgendarInstSemEquip = OrsegupsUtils.getSpecificWorkDay(prazoAgendarInstSemEquip, 2L);
					prazoAgendarInstSemEquip.set(GregorianCalendar.HOUR, 23);
					prazoAgendarInstSemEquip.set(GregorianCalendar.MINUTE, 59);
					prazoAgendarInstSemEquip.set(GregorianCalendar.SECOND, 59);
					ewWkfDistribuicaoRMC.findField("prazoAgendarInstSemEquipamento").setValue(prazoAgendarInstSemEquip);
				}
				ewWkfDistribuicaoRMC.findField("codigoCliente").setValue(codigoCliente);
				ewWkfDistribuicaoRMC.findField("realizarPosVenda").setValue(gerarPosVenda);
				PersistEngine.persist(wkfDistribuicaoRMC);

				/**
				 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
				 * @date 11/03/2015
				 */

				//WFProcess processo = WorkflowService.startProcess(pm, wkfDistribuicaoRMC, false, null, null, null, null, uSolic, null, null, null);

				try
				{
					String code = OrsegupsWorkflowHelper.iniciaProcessoNeoIdCode(pm, wkfDistribuicaoRMC, false, uSolic, true);
					//EntityWrapper w = new EntityWrapper(wkfDistribuicaoRMC);
					//System.out.println(w.findGenericValue("verificarEstoque"));
					System.out.println(code);
					//new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(uSolic);
				}
				catch (Exception e)
				{
					System.out.println("Erro #13 - Erro ao iniciar a atividade para o respons�vel");
					e.printStackTrace();
					out.print("Erro #13 - Erro ao iniciar a atividade para o respons�vel");
					return;
				}

				/**
				 * FIM ALTERA��ES - NEOMIND
				 */
			}
			else
			{
				for (NeoUser user : listaResp)
				{
					//vefica��o feita com a variavel "verificar" para abrir apenas uma tarefa para os Executivos de Vendas;
					boolean verificar = true;
					System.out.println(departamento);
					if (departamento.equals("DEXV"))
					{
						System.out.println(executivoRMC);
						System.out.println(user.getCode());
						if (!user.getCode().equals(executivoRMC))
						{
							verificar = false;
						}
					}

					if (verificar == true)
					{
						if (user.isActive())
						{
							//File file = new File("S:\\RMC\\"+arquivo);
							File file = new File(diretorio + arquivo);
							NeoFile newFile = new NeoFile();
							newFile.setName(file.getName());
							newFile.setStorage(NeoStorage.getDefault());
							PersistEngine.persist(newFile);
							OutputStream outi = newFile.getOutputStream();
							InputStream in = null;
							
							try
							{
								in = new BufferedInputStream(new FileInputStream(file));
								NeoStorage.copy(in, outi);
								newFile.setOrigin(NeoFile.Origin.TEMPLATE);
							}
							catch (FileNotFoundException e)
							{
								e.printStackTrace();
								out.print("Erro #12 - Relat�rio RMC n�o encontrado #" + diretorio + arquivo);
								return;
							}

							NeoObject wkfDistribuicaoRMC = AdapterUtils.getInstantiableEntityInfo("RelatorioMovimentacaoContrato").createNewInstance();
							EntityWrapper ewWkfDistribuicaoRMC = new EntityWrapper(wkfDistribuicaoRMC);

							String anexos = request.getParameter("anexos");
							if ((anexos != null) && (anexos != ""))
							{
								StringTokenizer st = new StringTokenizer(anexos, ",");
								String diretorioAnexo = "\\\\lagoa\\arqcvs\\";
								List<NeoObject> listaAnexos = new ArrayList();
								InstantiableEntityInfo rmcAnexos = AdapterUtils.getInstantiableEntityInfo("RMCAnexos");
								while (st.hasMoreTokens())
								{
									String token = st.nextToken();
									File anexo = new File(diretorioAnexo + token);
									NeoFile newAnexo = new NeoFile();
									NeoObject objAnexos = rmcAnexos.createNewInstance();
									EntityWrapper wAnexos = new EntityWrapper(objAnexos);
									newAnexo.setName(anexo.getName());
									newAnexo.setStorage(NeoStorage.getDefault());
									PersistEngine.persist(newAnexo);
									PersistEngine.persist(objAnexos);
									OutputStream outAnexo = newAnexo.getOutputStream();
									InputStream inAnexo = null;
									inAnexo = new BufferedInputStream(new FileInputStream(anexo));
									NeoStorage.copy(inAnexo, outAnexo);
									newAnexo.setOrigin(NeoFile.Origin.TEMPLATE);
									wAnexos.findField("anexos").setValue(newAnexo);
									listaAnexos.add(objAnexos);
								}
								ewWkfDistribuicaoRMC.findField("anexos").setValue(listaAnexos);
							}

							GregorianCalendar prazoRMC = new GregorianCalendar();

							Integer hora = prazoRMC.get(GregorianCalendar.HOUR_OF_DAY);
							Integer minuto = prazoRMC.get(GregorianCalendar.MINUTE);
							Integer segundo = prazoRMC.get(GregorianCalendar.SECOND);

							prazoRMC = OrsegupsUtils.getSpecificWorkDay(prazoRMC, 1L);

							prazoRMC.set(GregorianCalendar.HOUR, hora);
							prazoRMC.set(GregorianCalendar.MINUTE, minuto);
							prazoRMC.set(GregorianCalendar.SECOND, segundo);

							GregorianCalendar prazoAgendarInstSemEquip = new GregorianCalendar();
							if (tipIns != null && tipIns.isEmpty())
							{
								if (tipIns.equals("R") || tipIns.equals("Y"))
								{
									prazoAgendarInstSemEquip = OrsegupsUtils.getSpecificWorkDay(prazoAgendarInstSemEquip, 2L);
									prazoAgendarInstSemEquip.set(GregorianCalendar.HOUR, 23);
									prazoAgendarInstSemEquip.set(GregorianCalendar.MINUTE, 59);
									prazoAgendarInstSemEquip.set(GregorianCalendar.SECOND, 59);
									ewWkfDistribuicaoRMC.findField("prazoAgendarInstSemEquipamento").setValue(prazoAgendarInstSemEquip);
								}
							}

							ewWkfDistribuicaoRMC.findField("arquivo").setValue(newFile);
							ewWkfDistribuicaoRMC.findField("solicitante").setValue(uSolic);
							ewWkfDistribuicaoRMC.findField("empresa").setValue(eEmpresa);
							ewWkfDistribuicaoRMC.findField("nomeCliente").setValue(nomeCliente);
							ewWkfDistribuicaoRMC.findField("numeroContrato").setValue(numeroContrato);
							ewWkfDistribuicaoRMC.findField("diaBase").setValue(diaBase);
							ewWkfDistribuicaoRMC.findField("gerente").setValue(user);
							ewWkfDistribuicaoRMC.findField("verificarEstoque").setValue(verificarEstoque);
							ewWkfDistribuicaoRMC.findField("filial").setValue(codigoFilial);
							ewWkfDistribuicaoRMC.findField("numerosPostos").setValue(numerosPostos);
							ewWkfDistribuicaoRMC.findField("tipIns").setValue(tipIns);
							ewWkfDistribuicaoRMC.findField("todosCompraDireta").setValue(allCompraDireta);
							PersistEngine.persist(wkfDistribuicaoRMC);

							/**
							 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
							 * @date 11/03/2015
							 */

							WFProcess processo = pm.startProcess(wkfDistribuicaoRMC, false, null);
							processo.setSaved(true);

							try
							{
								new OrsegupsWorkflowHelper(processo).avancaPrimeiraTarefa(uSolic);
							}
							catch (Exception e)
							{
								System.out.println("Erro #13 - Erro ao iniciar a atividade para o respons�vel");
								e.printStackTrace();
								out.print("Erro #13 - Erro ao iniciar a atividade para o respons�vel");
								return;
							}

							/**
							 * FIM ALTERA��ES - NEOMIND
							 */
						}
					}
				}
			}
			out.print("OK");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Teste do TI�O: " + e.getMessage());
			out.print("Erro #99 - Aprova��o de RMC com erros #" + e.getMessage());
			return;
		}
	%>
</body>


