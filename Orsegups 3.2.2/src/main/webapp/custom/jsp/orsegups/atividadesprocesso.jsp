<%@page import="com.neomind.fusion.security.NeoRole"%>
<%@page import="com.neomind.fusion.security.NeoGroup"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>

<%@page import="com.neomind.fusion.workflow.RoleAvailableActivityData"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.HashSet"%>
<body>
<%
	// pegando os parametros
	String code = request.getParameter("code");
	String name = request.getParameter("processo");
	out.print("Debug: " + name.toString() + "|" + code + "<br>");
	
	// filtro para pegar o objeto do processo
	QLGroupFilter g = new QLGroupFilter("AND");
	g.addFilter(new QLOpFilter("model.name","LIKE","%"+name+"%"));
	g.addFilter(new QLEqualsFilter("code",code));
	WFProcess w = (WFProcess) PersistEngine.getObject(WFProcess.class, g);
	
	out.print("Processo: " + w.getModel().getName() + "<br>");
	
	// filtro para pegar as atividades
	QLGroupFilter gAct = new QLGroupFilter("AND");
	gAct.addFilter(new QLFilterIsNotNull("startDate")));
	gAct.addFilter(new QLEqualsFilter("process",w));
	
	// atividades em aberto
	Collection<Activity> colAct = PersistEngine.getObjects(Activity.class,gAct);
	out.print("Total de atividades em aberto: " + colAct.size() + "<br>");
	
	// tarefas em aberto
	for(Activity a : colAct)
	{
		
		QLGroupFilter gTask = new QLGroupFilter("AND");
		gTask.addFilter(new QLFilterIsNull("finishDate"));
		gTask.addFilter(new QLFilterIsNotNull("startDate"));
		gTask.addFilter(new QLEqualsFilter("activity",a));

		
		Task task = (Task) PersistEngine.getObject(Task.class,gTask);
		
		// pode estar pool, dai nao tem Task ainda
		if(task != null)
		{
			out.print("Tarefa: " + task.getActivityName() + " para o usu�rio: " + task.getUser().getFullName() + "<br>");
		} else {
			out.print("Atividade: " + a.getActivityName() + " em POOL para: ");
			if (a instanceof UserActivity)
			{
				for (SecurityEntity sec : a.getInstance().getPotentialOwners())
				{
					if (sec instanceof NeoUser)
					{
						result = result + ", " + ((NeoUser) sec).getFullName();
					} 
					else if (sec instanceof NeoPaper)
					{
						result = result + ", " + ((NeoPaper) sec).getName();
					}
					else if (sec instanceof NeoGroup)
					{
						result = result + ", " + ((NeoGroup) sec).getName();
					}
					else if (sec instanceof NeoRole)
					{
						result = result + ", " + ((NeoRole) sec).getName();
					}
					
					if (a.getInstance().getPotentialOwners().size() > i)
					{
						result = result + ", " + ", ";
					}
				}
			}
		}
	}
	
%>
</body>
