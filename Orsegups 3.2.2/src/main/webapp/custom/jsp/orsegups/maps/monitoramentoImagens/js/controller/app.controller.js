app.directive('fallbackSrc', function ($filter) {
  var fallbackSrc = {
    link: function postLink(scope, iElement, iAttrs) {
      iElement.bind('error', function() {
        angular.element(this).attr("src", iAttrs.fallbackSrc);
        
        var id = angular.element(this).attr("id");
        
        console.log(scope.$parent.varreduras);
        
        if (scope.$parent.varreduras > 10){
        	scope.$parent.imageFail.push(id);        	
        }
        
                            
      });
    }
   }
   return fallbackSrc;
});


app.controller('appcontroller', ['$scope','$http','$interval','$filter','$location',function($scope, $http, $interval, $filter,$location){

	console.log('Iniciou controller');
	
	$scope.url = "inicio";
	
	$scope.cameras;
	
	$scope.imageUrlLarge = [];
	
	$scope.imageUrlSmall = [];
	
	$scope.imageFail = [];
		
	$scope.varreduras = 0;
		
	$interval(function(){
		$scope.refreshViaturas();
	},1000);
	
	$scope.refreshViaturas = function (){
		
		var newUrl = $location.url().replace("/","");
		
		if (newUrl != $scope.url){
			$scope.url = newUrl;
			
			var c = $scope.url - 20160819;
			
			$http({
				method: 'GET',
				url: 'https://intranet.orsegups.com.br/fusion/services/seventh/getCameraIDs/'+c
				}).then(function successCallback(response) {
					console.log(response.data);
					$scope.cameras = response.data;
					$scope.varreduras = 0;
					
				}, function errorCallback(response) {
					console.log('Erro ao carregar cameras!');
				});
			
		}
			
		if ($scope.cameras){
			$scope.imageUrlLarge = [];
			$scope.imageUrlSmall = [];
			
			var now = new Date();
			
			var count = 0;
			
			angular.forEach($scope.cameras.ids, function (value, key){
				
				//console.log(value);
				
				var obj = {};
				
				var s = 'https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/seventh/mjpegStreamDGuardCustom.jsp?camera='+value+'&servidor='+$scope.cameras.servidor+'&a='+new Date().getTime();
				
				if ($scope.varreduras > 10){
					var index = $scope.imageFail.indexOf(value);
					
					if(index > -1){
						s = 'https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/cm/imagens/camera-motion_not_found.png';
					}
					
				}
				
				obj.src= s;
				obj.id= value;
				
				if (count < 4){
					$scope.imageUrlLarge.push(obj);					
				}else{
					$scope.imageUrlSmall.push(obj);
				}
				
				count ++;
											
			});

		}
		
		$scope.varreduras ++;


	};
		
}]);

