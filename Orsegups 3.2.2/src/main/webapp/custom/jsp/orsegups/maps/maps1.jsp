<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0px;
	padding: 0px
}

#map_canvas {
	height: 100%
}

.labels {
     color: #6C6C6C;
     background-color: white;
     font-family: "Tahoma";
     font-size: 8px;
     font-weight: bold;
     text-align: center;
     width: 24px;
     height: 10px;
     border: 1px solid black;
     white-space: nowrap;
   }
   
.window_black_bold {
	font-family: "Tahoma";
	color: black;
	background-color: white;
	font-size: 12px;
	font-weight: bold
}

.window_black {
	font-family: "Tahoma";
	color: black;
	background-color: white;
	font-size: 12px;
}

</style>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>css/themes/neo.ui.css.jsp"></script>
	<!-- 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.js"></script>
	 -->
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<!-- 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.layout.js"></script>
	 -->
<script type="text/javascript"
	src="https://maps.google.com/maps/api/js?sensor=false&language=pt-BR&region=BR"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/markerwithlabel.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/keydragzoom.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.qtip-1.0.0-rc3.min.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/clock/jquery.jclock.js"></script>

<% 
	// seguranca: podem acessar usuarios logados e maquinas da rede Orsegups ou Apple devices com passagem de parametro secreto
	NeoUser user = PortalUtil.getCurrentUser();
	String remoteIP = request.getRemoteAddr();
	//System.out.println(remoteIP);
	// 127.0.0.1 e 0:0:0:0:0:0:0:1 (ipv6) - localhost
	boolean accessGranted = false;
	
	if ((user != null) || ( (remoteIP.matches("0:0:0:0:0:0:0:1")) ||
							(remoteIP.matches("127.0.0.1")) ||
							(remoteIP.matches("192\\.168\\.[1-5]\\d\\.\\d{1,3}")) ||
							(remoteIP.matches("201\\.47\\.43\\.1\\d{2}"))
		                  )
	   ) 
	{
		accessGranted = true;
	}
	
	if (!accessGranted) 
	{
		out.print("Acesso Negado. Rede n�o autorizada.");
		return;
	}
%>
		
<%
	String action = request.getParameter("action");
	String regional = request.getParameter("regional");
	String ramalOrigem = request.getParameter("ramalOrigem");
	String aplicacao = request.getParameter("aplicacao");
	String atualizaPosicao = request.getParameter("atualizaPosicao");
	if(atualizaPosicao != null)
	{
		atualizaPosicao = atualizaPosicao.toLowerCase();
	}
	else
	{
		atualizaPosicao = "";
	}
%>

<script type="text/javascript">
	
	var directionsService = new google.maps.DirectionsService();
	var mapsLayout;
	var map;
	var viaturasArray = [];
	var eventosArray = [];
	var infoWindowMouseOverControl = [];
	//var ligacaoEventoArray = [];
	//var ligacaoViaturaArray = [];
	var refreshing = true;
	var timeout;
	var timeoutOS;
	var timeoutViaturaTecnica;
	var timeoutEventosCM;
	var timeoutViaturasCM;
	var filter = "<%=action%>";
	var regional = "<%=regional%>";
	var ramalOrigem = "<%=ramalOrigem%>";
	var aplicacao = "<%=aplicacao%>";
	var atualizaPosicao = "<%=atualizaPosicao%>";
	var tecnicoSelecionado;
	var routeDistance = 0;
	var atendenteSelecionadoDiv;
	var atendenteSelecionadoText;

	
	$(document).ready(function () {
		initialize();
	});
	
	// Possiveis valores do atributo mapTypeId google.maps.MapTypeId
	//google.maps.MapTypeId.ROADMAP exibe as blocos 2D normais, padr�o, do Google Maps.
	//google.maps.MapTypeId.SATELLITE exibe blocos fotogr�ficos.
	//google.maps.MapTypeId.HYBRID exibe uma mistura entre blocos fotogr�ficos e uma camada de blocos com recursos importantes (estradas, nomes de cidade).
	//google.maps.MapTypeId.TERRAIN exibe blocos de relevo f�sico para exibi��o de recursos de eleva��o e �gua (montanhas, rios etc.).
	
	
	function initialize() {
		var latlng = new google.maps.LatLng(-27.59694, -48.54889);
		var myOptions = {
			zoom : 9,
			center : latlng,
			panControl: false,
			streetViewControl: false,
			zoomControl : true,
			mapTypeId : google.maps.MapTypeId.ROADMAP,
	      	streetViewControl: true,
	      	streetViewControlOptions: {
	          position: google.maps.ControlPosition.LEFT_TOP
	      	}
		};
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
		map.enableKeyDragZoom({
	          visualEnabled: true,
	          visualPosition: google.maps.ControlPosition.TOP_LEFT,
	          visualPositionOffset: new google.maps.Size(0, 5),
	          visualPositionIndex: null,
	          visualSprite: "images/dragzoom_btn.png",
	          visualSize: new google.maps.Size(20, 20),
	          visualTips: {
	            off: "Zoom Area",
	            on: "Zoom Area"
	          }
	        });
		
		// Cria o botao FitToWindow (enquadrar marcadores)
		fitToMarkersControl();
		
		if(filter == "viaturas")
		{
			loadViaturasCM();
		}
		else if(filter == "centralMonitoramento")
		{
				loadViaturasCM();
				loadEventosCM();
		}
		else if(filter == "areaTecnica")
		{
			//cria DIv onde ser'a mostrado o tecnico selecionado
			if(atendenteSelecionadoDiv == null)
			{
				atendenteSelecionadoDiv = document.createElement('DIV');
				atendenteSelecionadoDiv.index = 1;
				atendenteSelecionadoDiv.style.padding = '2px';
				atendenteSelecionadoDiv.style.height = '40px';
				
				// Set CSS for the control border
				var controlUI = document.createElement('DIV');
				controlUI.style.backgroundColor = 'white';
				controlUI.style.borderStyle = 'solid';
				controlUI.style.borderWidth = '2px';
				controlUI.style.cursor = 'pointer';
				controlUI.style.textAlign = 'center';
				atendenteSelecionadoDiv.appendChild(controlUI);
				
				// Set CSS for the control interior
				atendenteSelecionadoText = document.createElement('DIV');
				atendenteSelecionadoText.style.fontFamily = 'Tahoma';
				atendenteSelecionadoText.style.fontSize = '12px';
				atendenteSelecionadoText.style.paddingLeft = '4px';
				atendenteSelecionadoText.style.paddingRight = '4px';
				//atendenteSelecionadoText.style.fontWeight = 'bold';
				//controlText.style.height = '40px';
				//atendenteSelecionadoText.style.width = '80px';
				controlUI.appendChild(atendenteSelecionadoText);
				
				map.controls[google.maps.ControlPosition.TOP_CENTER].push(atendenteSelecionadoDiv);
			}
			
			loadAtendentes();
		}
		
		fitToMarkers();
		
		createClock();
	}
	
	function createClock()
	{
		var options = {
	       timeNotation: '24h',
	       am_pm: false,
	       fontFamily: 'Tahoma',
	       fontSize: '20px',
	       //foreground: 'yellow',
	       background: 'white'
		}; 
		$('.jclock').jclock(options);
		
		var clockDiv = document.getElementById('jclockId');
		
		map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(clockDiv);
	}
	
	function fitToMarkersControl() {

		var controlDiv = document.createElement('DIV');
		
		// Set CSS styles for the DIV containing the control
		// Setting padding to 5 px will offset the control
		// from the edge of the map
		controlDiv.style.padding = '5px';
		
		// Set CSS for the control border
		var controlUI = document.createElement('DIV');
		controlUI.style.backgroundColor = 'white';
		controlUI.style.borderStyle = 'solid';
		controlUI.style.borderWidth = '2px';
		controlUI.style.cursor = 'pointer';
		controlUI.style.textAlign = 'center';
		controlUI.title = 'Enquadrar Marcadores';
		controlDiv.appendChild(controlUI);
		
		// Set CSS for the control interior
		var controlText = document.createElement('DIV');
		controlText.style.fontFamily = 'Tahoma';
		controlText.style.fontSize = '12px';
		controlText.style.paddingLeft = '4px';
		controlText.style.paddingRight = '4px';
		controlText.innerHTML = 'Enquadrar Marcadores';
		controlUI.appendChild(controlText);
		
		// Setup the click event listeners: simply set the map to Chicago
		google.maps.event.addDomListener(controlUI, 'click', function() {
			fitToMarkers();
		});
		
		controlDiv.index = 1;
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(controlDiv);
	}
	
	function barraAtendentes(atendenteObj) {

		var barraAtendentesDiv = document.createElement('DIV');
		
		barraAtendentesDiv.index = 1;
		
		// Set CSS styles for the DIV containing the control
		// Setting padding to 5 px will offset the control
		// from the edge of the map
		barraAtendentesDiv.style.padding = '2px';
		barraAtendentesDiv.style.height = '40px';
		
		// Set CSS for the control border
		var controlUI = document.createElement('DIV');
		controlUI.style.backgroundColor = 'white';
		controlUI.style.borderStyle = 'solid';
		controlUI.style.borderWidth = '2px';
		controlUI.style.cursor = 'pointer';
		controlUI.style.textAlign = 'center';
		controlUI.title = atendenteObj.nomeAtendente;
		barraAtendentesDiv.appendChild(controlUI);
		
		// Set CSS for the control interior
		var controlText = document.createElement('DIV');
		controlText.style.fontFamily = 'Tahoma';
		controlText.style.fontSize = '10px';
		controlText.style.paddingLeft = '4px';
		controlText.style.paddingRight = '4px';
		//controlText.style.height = '40px';
		//controlText.style.width = '80px';
		controlText.innerHTML = atendenteObj.nomeAtendente;
		controlUI.appendChild(controlText);
		
		
		// Setup the click event listeners: simply set the map to Chicago
		google.maps.event.addDomListener(controlUI, 'click', function() {
			loadTecnico(atendenteObj.codigoAtendente);
			
			atendenteSelecionadoText.innerHTML = atendenteObj.nomeAtendente;
		});
		
		map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(barraAtendentesDiv);
	}
	
	function atualizaPosicaoCliente(codigoCliente, placa)
	{
		var r = confirm("Deseja atualizar a posi��o do Evento com a posi��o da Viatura?");
		if(r == true)
		{
			var viaturaItem = viaturasArray[placa];
			if(viaturaItem != null)
			{
				viaturaObj = viaturaItem["viaturaObj"];
				if(viaturaObj != null)
				{
					var latitude = viaturaObj.latitude;
					var longitude = viaturaObj.longitude;
					
					var dataObj = new Object();
					dataObj.codigoCliente = codigoCliente;
					dataObj.latitude = latitude;
					dataObj.longitude = longitude;
				
					var dataJson = JSON.stringify(dataObj);
					
					var result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=atualizaPosicaoEvento&data="+dataJson);
					
					if(result != null && result != "")
					{
						alert(result);
					}
					
					if(filter == "centralMonitoramento")
					{
						clearTimeout(timeoutEventosCM);
						loadEventosCM();
					}
					else
					{
						clearTimeout(timeoutOS);
						loadOS();
					}
				}
			}
		}
	}
	
	function loadEventosCM()
	{
		var eventosJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=eventosCM&regional="+regional+"&atualizaPosicao="+atualizaPosicao);
		
		parseEventos(eventosJson);
		
		if(refreshing)
		{
			timeoutEventosCM = setTimeout("loadEventosCM()",15000);
		}
	}
	
	function loadViaturasCM()
	{
		var viaturasJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=viaturasCM&regional="+regional+"&ramalOrigem="+ramalOrigem+"&aplicacao="+aplicacao);
		
		parseViaturas(viaturasJson);
		
		if(refreshing)
		{
			timeoutViaturasCM = setTimeout("loadViaturasCM()",15000);
		}
	}
	
	function loadTecnico(codigoTecnico)
	{
		if(codigoTecnico != null)
		{
			// remove os timeOuts que recaregam os dados a cada 15 segundos para nao sobrecarrega-los 
			clearTimeout(timeoutOS);
			clearTimeout(timeoutViaturaTecnica);
			
			//remove todos os itens do mapa pois ao chamar este metodo estaremos carregando viatura e os de outro tecnico
			for(e in eventosArray)
			{
				removeEvento(e);
			}
			for(v in viaturasArray)
			{
				removeViatura(v);
			}
			
			// atualiza quem � o tecnico atual para que o refresh saiba de quem carregar as OS
			tecnicoSelecionado = codigoTecnico; 
			
			// busca os novos dados
			loadViaturasTecnicas();
			loadOS();
		}
		fitToMarkers();
	}
	
	function loadOS()
	{
		var osJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=OS&codigoTecnico="+tecnicoSelecionado+"&atualizaPosicao="+atualizaPosicao);
		
		parseEventos(osJson);
		
		if(refreshing)
		{
			timeoutOS = setTimeout("loadOS()",15000);
		}
	}
	
	function loadViaturasTecnicas()
	{
		var viaturasJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=viaturasTecnicas&codigoTecnico="+tecnicoSelecionado+"&ramalOrigem="+ramalOrigem);
		
		parseViaturas(viaturasJson);
		
		if(refreshing)
		{
			timeoutViaturaTecnica = setTimeout("loadViaturasTecnicas()",15000);
		}
	}
	
	function loadAtendentes()
	{
		var atendentesJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=atendentes&regional="+regional);
		
		parseAtendentes(atendentesJson);
		
		if(refreshing)
		{
			//timeout = setTimeout("loadAtendentes()",15000);
		}
	}
	
	function parseAtendentes(atendentesJson)
	{
		if(atendentesJson != null && atendentesJson != "")
		{
			atendentes = JSON.parse(atendentesJson);
			
			for(i in atendentes) 
			{
				var atendenteObj = atendentes[i];
				
				barraAtendentes(atendenteObj);
			}
		}
	}
		
		
	function parseEventos(eventosJson)
	{
		if(eventosJson != null && eventosJson != "")
		{
			eventos = JSON.parse(eventosJson);
			
			for(i in eventos) 
			{
				var eventoObj = eventos[i];
				
				manageEvento(eventoObj);
			}
			
			for(k in eventosArray)
			{
				var exists = false;
				
				for(n in eventos) {
					if(eventos[n].codigoHistorico == k)
					{
						exists = true;
						break;
					}
				}
				
				if(!exists)
				{
					removeEvento(k);
				}
			}
		}
		
		if(refreshing)
		{
			timeout = setTimeout("parseEventos()",15000);
		}
	}
	
	function removeEvento(k)
	{
			if(eventosArray[k] != null)
			{
				if(eventosArray[k]["marker"] != null)
				{
					eventosArray[k]["marker"].setMap(null);
					eventosArray[k]["marker"] = null;
				}
				
				if(eventosArray[k]["info"] != null)
				{
					eventosArray[k]["info"].setMap(null);
					eventosArray[k]["info"] = null;
				}
				
				if(eventosArray[k]["line"] != null)
				{
					eventosArray[k]["line"].setMap(null);
					eventosArray[k]["line"] = null;
				}
				
				var placa = eventosArray[k]["eventoObj"].placa;
				if(placa != null && placa != "" && viaturasArray[placa] != null)
				{
					if(viaturasArray[placa]["line"] != null && viaturasArray[placa]["line"][k] != null)
					{
						viaturasArray[placa]["line"][k].setMap(null);
						viaturasArray[placa]["line"][k] = null;
					}
					
					if(viaturasArray[placa]["marker"] != null)
					{
						viaturasArray[placa]["marker"].setIcon("images/"+viaturasArray[placa]["viaturaObj"].nomeImagem+".png");
					}
					
				}
				
				eventosArray[k] = null;
			}
	}
	
	function parseViaturas(viaturasJson)
	{
		if(viaturasJson != null && viaturasJson != "")
		{
			var viaturas = JSON.parse(viaturasJson);
			
			for(i in viaturas) {
				var viaturaObj = viaturas[i];
				
				manageViatura(viaturaObj);
		    }
			
			for(k in viaturasArray)
			{
				var exists = false;
				
				for(n in viaturas) {
					if(viaturas[n].placa == k)
					{
						exists = true;
						break;
					}
				}
				
				if(!exists)
				{
					removeViatura(k);
				}
			}
		}
	}
	
	function removeViatura(k)
	{
		if(viaturasArray[k] != null)
		{
			if(viaturasArray[k]["marker"] != null)
			{
				viaturasArray[k]["marker"].setMap(null);
				viaturasArray[k]["marker"] = null;
			}
			
			if(viaturasArray[k]["info"] != null)
			{
				viaturasArray[k]["info"].setMap(null);
				viaturasArray[k]["info"] = null;
			}
			
			if(viaturasArray[k]["line"] != null)
			{
				lines = viaturasArray[k]["line"];
				
				for(i in lines)
				{
					if(lines[i] != null)
					{
						lines[i].setMap(null);
						lines[i] = null;
						
						if(eventosArray[i] != null && eventosArray[i]["marker"] != null)
						{
							eventosArray[i]["marker"].setIcon("images/"+eventosArray[i]["eventoObj"].nomeImagem+".png");
							if(eventosArray[i]["line"] != null)
							{
								eventosArray[i]["line"].setMap(null);
								eventosArray[i]["line"] = null;
							}
						}
					}
				}
			}
			viaturasArray[k] = null;
		}
	}
	
	function startRefresh()
	{
		if(!refreshing)
		{
			refreshing = true;
			
			loadViaturas();
		}
	}
	
	function stopRefresh()
	{
		if(refreshing)
		{
			refreshing = false;
			
			clearTimeout(timeout);
		}
	}
	
	function manageEvento(eventoObj)
	{
		var location = new google.maps.LatLng(eventoObj.latitude, eventoObj.longitude);
		var marker = null;
		var infowindow = null;
		var eventoItem = null;
		var eventoViaturaLine = null;
		var directionsDisplay = null;
		
		eventoItem = eventosArray[eventoObj.codigoHistorico];
		
		if(eventoItem != null)
		{
			marker = eventoItem["marker"];
			infowindow = eventoItem["info"];
			eventoViaturaLine = eventoItem["line"];
		}
		else
		{
			eventoItem = [];
		}
		
		eventoItem["eventoObj"] = eventoObj;
		
		//cria o marcador do evento
		if(marker != null)
		{
			marker.setPosition(location);
			marker.set("labelStyle", {opacity: 1, backgroundColor: eventoObj.corPrioridade, color: eventoObj.corTexto});
		}
		else
		{
			marker = new MarkerWithLabel({
				position: location,
				map: map,
				title : eventoObj.codigoEvento + ' - ' + eventoObj.nomeEvento,
				icon : "images/"+eventoObj.nomeImagem+".png",
				zIndex : 1,
				labelContent: eventoObj.codigoEvento,
		       	labelClass: "labels", // the CSS class for the label
		       	//labelAnchor: new google.maps.Point(-9, 20), label deslocada para direita
		       	labelAnchor: new google.maps.Point(13, 45),
		       	labelInBackground: true,
		       	labelStyle: {opacity: 1, backgroundColor: eventoObj.corPrioridade, color: eventoObj.corTexto}
			});
			
			if(eventoObj.type != null && eventoObj.type == "os")
			{
				marker.setTitle(eventoObj.nomeEvento);
				marker.set("labelContent", eventoObj.nomeEvento);
				//marker.set("labelVisible", false);
			}
			
			eventoItem["marker"] = marker;
		}

		var contentString = eventoObj.infoWindowContent;
		
		// se a janela ja existe atualiza o conteudo da janela, caso contrario cria ela
		if(infowindow != null)
		{
			infowindow.setContent(contentString);
		}
		else
		{
			infowindow = new google.maps.InfoWindow({
			    content: contentString
			});
			eventoItem["info"] = infowindow;
			
			//inicializa com true
			infoWindowMouseOverControl[eventoObj.codigoHistorico] = true;
			
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
				infoWindowMouseOverControl[eventoObj.codigoHistorico] = false;
			});
			
			google.maps.event.addListener(marker, 'dblclick', function() {
				infowindow.close();
				infoWindowMouseOverControl[eventoObj.codigoHistorico] = true;
			});
			
			google.maps.event.addListener(marker, 'mouseover', function() {
				if(infoWindowMouseOverControl[eventoObj.codigoHistorico])
				{
					infowindow.open(map,marker);
				}
			});
			
			google.maps.event.addListener(marker, 'mouseout', function() {
				if(infoWindowMouseOverControl[eventoObj.codigoHistorico])
				{
					infowindow.close();
				}
			});
			
			google.maps.event.addListener(infowindow, 'closeclick', function() {
				infoWindowMouseOverControl[eventoObj.codigoHistorico] = true;
			});
			
			google.maps.event.addListener(infowindow, 'domready', function() {
				if(!infoWindowMouseOverControl[eventoObj.codigoHistorico])
				{
					$('#content a[tooltip]').each(function()
					{
					   $(this).qtip({
					      content: $(this).attr('tooltip'), // Use the tooltip attribute of the element for the content
					      show: { 
					    	  when: { event: 'mouseenter' }, 
					    	  delay: 0,  
					    	  solo: true, 
					    	  effect: { length: 0,
					    		  		type: ""} 
					      }, 
					      hide: { 
					    	  when: { 
					    		  event: 'mouseleave', 
					    		  event: 'click', 
					    		  event: "unfocus", 
					    		  event: "mouseout" }, 
							  delay: 0,
							  effect: { length: 0,
				    		  			type: ""} 
					      },
					      style: { 
					    	  width: {
					    		  max : 800,
					    		  min : 100
					    	  },
					    	  border: {
					    	         width: 2
					    	  },
					    	  name: 'dark'
					      }
					   }); 
					});
				}
			});
		}
		
		// gerencia a ligacao entre o evento e a viatura
		if(eventoObj.placa != null && eventoObj.placa != '')
		{
			eventoLatLng = location;
			
			var viaturaItem = viaturasArray[eventoObj.placa];
			if(viaturaItem != null)
			{
				var viatura = viaturaItem["marker"]; 
				viaturaLatLng = viatura.getPosition();
				
				var eventoViaturaPath = [eventoLatLng, viaturaLatLng];
				
				//calcula distancia
				/*
				var request = {
						avoidHighways : false,
						avoidTolls : false,
						origin : viaturaLatLng,
						destination: eventoLatLng, 
					    travelMode: google.maps.DirectionsTravelMode.DRIVING,
					    provideRouteAlternatives : false
				};
				
				directionsService.route(request, function(response, status) {
					routeDistance = 0;
					if (status == google.maps.DirectionsStatus.OK) {
						
						var routes = response.routes;
						if(routes != null)
						{
							for(x in routes)
							{
								var route = routes[x];
								if(route != null)
								{
									var legs = route.legs;
									
									for(z in legs)
									{
										routeDistance += legs[z]["distance"]["value"];
									}
								}
							}
						}
						
						
						directionsDisplay = eventoItem["directionsDisplay"];
						
						if(directionsDisplay == null)
						{
							var directionsDisplay = new google.maps.DirectionsRenderer();
							directionsDisplay.setMap(map);
						}
						
						var options = {
							polylineOptions : {strokeColor: viaturaItem["viaturaObj"].corLigacao}
						};
						
						directionsDisplay.setDirections(response);
						directionsDisplay.setOptions(options);
						
						eventoItem["directionsDisplay"] = directionsDisplay;
						
					}
				});
				*/
				
				//alert(routeDistance);
				
				
				if(eventoViaturaLine != null)
				{
					eventoViaturaLine.setPath(eventoViaturaPath);
				}
				else
				{
					var eventoViaturaLine = new google.maps.Polyline({
						path: eventoViaturaPath,
						strokeColor: viaturaItem["viaturaObj"].corLigacao,
						strokeOpacity: 1.0,
						strokeWeight: 3
					});
					
					eventoViaturaLine.setMap(map);
					
					eventoItem["line"] = eventoViaturaLine;
					
					lines = viaturaItem["line"];
					if(lines == null)
					{
						lines = [];
						viaturaItem["line"] = lines;
					}
					lines[eventoObj.codigoHistorico] = eventoViaturaLine;
				}
				
				
				if(viaturaItem["viaturaObj"].sufixoLigacao != null)
				{
					marker.setIcon("images/"+eventoObj.nomeImagem+"_"+viaturaItem["viaturaObj"].sufixoLigacao+".png");
					viatura.setIcon("images/"+viaturaItem["viaturaObj"].nomeImagem+"_"+viaturaItem["viaturaObj"].sufixoLigacao+".png");
				}
			}
			viaturasArray[eventoObj.placa] = viaturaItem;
		}
		eventosArray[eventoObj.codigoHistorico] = eventoItem;
	}
	
	function manageViatura(viaturaObj)
	{
		var location = new google.maps.LatLng(viaturaObj.latitude, viaturaObj.longitude);
		var marker = null;
		var infowindow = null;
		var viaturaItem = null;
		var lines = null;
		
		viaturaItem = viaturasArray[viaturaObj.placa];
		
		if(viaturaItem != null)
		{
			marker = viaturaItem["marker"]; 
			infowindow = viaturaItem["info"];
			lines = viaturaItem["line"];
		}
		else
		{
			viaturaItem = [];
		}
		
		viaturaItem["viaturaObj"] = viaturaObj;
		
		if(marker != null)
		{
			marker.setPosition(location);
			marker.set("labelContent", viaturaObj.titulo);
			marker.set("labelStyle", {opacity: 1, backgroundColor: viaturaObj.corStatus, color: viaturaObj.corTexto});
		}
		else
		{
			marker = new MarkerWithLabel({
				position: location,
				map: map,
				title : viaturaObj.titulo,
				icon : "images/"+viaturaObj.nomeImagem+".png",
				zIndex : 100,
				labelContent: viaturaObj.titulo,
		       	labelClass: "labels", // the CSS class for the label
		       	//labelAnchor: new google.maps.Point(27, 20), deslocado para a esquerda
		       	labelAnchor: new google.maps.Point(13, 38),
		       	labelInBackground: false,
		       	labelStyle: {opacity: 1, backgroundColor: viaturaObj.corStatus, color: viaturaObj.corTexto}
			});
			
			viaturaItem["marker"] = marker;
		}
		
		var contentString = viaturaObj.infoWindowContent;
		
		if(infowindow != null)
		{
			infowindow.setContent(contentString);
		}
		else
		{
			infowindow = new google.maps.InfoWindow({
			    content: contentString
			});
			viaturaItem["info"] = infowindow;
			
			//inicializa com true
			infoWindowMouseOverControl[viaturaObj.placa] = true;
			
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
				infoWindowMouseOverControl[viaturaObj.placa] = false;
			});
			
			google.maps.event.addListener(marker, 'dblclick', function() {
				infowindow.close();
				infoWindowMouseOverControl[viaturaObj.placa] = true;
			});
			
			google.maps.event.addListener(marker, 'mouseover', function() {
				if(infoWindowMouseOverControl[viaturaObj.placa])
				{
					infowindow.open(map,marker);
				}
			});
			
			google.maps.event.addListener(marker, 'mouseout', function() {
				if(infoWindowMouseOverControl[viaturaObj.placa])
				{
					infowindow.close();
				}
			});
			
			google.maps.event.addListener(infowindow, 'closeclick', function() {
				infoWindowMouseOverControl[viaturaObj.placa] = true;
			});
		}
		
		// caso tenha uma linha associada a esta viatura atualiza o ponto
		if(lines != null)
		{
			for(i in lines)
			{
				var line = lines[i];
				if(line != null)
				{
					var path = line.getPath();
					path.removeAt(1);
					path.insertAt(1, location);
				}
			}
		}
			
		viaturasArray[viaturaObj.placa] = viaturaItem;
	}
	
	function fitToMarkers()
	{
		var latLngBounds = null;
		
		for(v in viaturasArray)
		{
			if(viaturasArray[v] != null && viaturasArray[v]["marker"] != null)
			{
				var location = viaturasArray[v]["marker"].getPosition();
				
				if(latLngBounds == null)
				{
					latLngBounds = new google.maps.LatLngBounds(location, location);
				}
				else
				{
					latLngBounds.extend(location);
				}
			}
		}
		
		for(e in eventosArray)
		{
			if(eventosArray[e] != null && eventosArray[e]["marker"] != null)
			{
				var location = eventosArray[e]["marker"].getPosition();
				
				if(latLngBounds == null)
				{
					latLngBounds = new google.maps.LatLngBounds(location, location);
				}
				else
				{
					latLngBounds.extend(location);
				}
			}
		}
		if(latLngBounds != null)
		{
			map.fitBounds(latLngBounds);
		}
	}
	
</script>
</head>
<body id="bodyId">
	<!-- 
	<div id="filter-canvas" style="width: 400px; height: 100%" class="ui-layout-west">
		<button onclick="fitToMarkers()" >Fit Markers</button>
	</div>
	 -->
	 
	<div id="map_canvas" style="width: 100%; height: 100%"></div>
	
	
	<div class="jclock" id="jclockId" ></div>
	
</body>
</html>
