<%@page
	import="com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.maps.vo.EventoEsperaVO"%>
<%@page
	import="br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy"%>
<%@page
	import="br.com.segware.sigmaWebServices.webServices.EventoRecebido"%>
<%@page import="java.text.ParseException"%>
<%@page import="javax.swing.text.MaskFormatter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="org.apache.commons.mail.HtmlEmail"%>
<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.mail.MailSettings"%>
<%@page
	import="br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServiceProxy"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.net.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEventoViaturaLivre");
	log.warn("##### INICIAR ROTINA DESLOCAMENTO PARA VIATURA LIVRE - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	List<NeoObject> eventosList = null;
	String returnFromAccess = "";
	List<EventoEsperaVO> list = null;
	String eventosAgrupados = "";
	String rotaStr = null;
	String regionalStr = null;

	try
	{
		String regional = "";
		String rota = "";
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		//groupFilter.addFilter(new QLOpFilter("motorista", "LIKE", "%SOO-AGENTE TESTE%"));
		//groupFilter.addFilter(new QLEqualsFilter("codigoRegional", regional));
		groupFilter.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
		groupFilter.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
		List<NeoObject> listOTSViatura = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), groupFilter);

		if (listOTSViatura != null && !listOTSViatura.isEmpty())
		{
			List<String> listaEventos = verificaEventoHistorico();
			for (NeoObject neoObject : listOTSViatura)
			{
				EntityWrapper entityWrapper = new EntityWrapper(neoObject);
				String motorista = (String) entityWrapper.getValue("motorista");
				String codigoViartura = (String) entityWrapper.getValue("codigoViatura");
				if (!motorista.contains("- TEC") && !motorista.contains("-TEC") && !motorista.contains("-INSP") && (!motorista.contains("CTA-SUP") && (!motorista.contains("HN") || !motorista.contains("HD"))) && !motorista.contains("-MOTO") && !motorista.contains("-ADM") && !motorista.contains("OFFICE") && !motorista.contains("ENG") && !codigoViartura.isEmpty() && codigoViartura.length() <= 6)
				{
					QLGroupFilter filterAnd = new QLGroupFilter("AND");
					filterAnd.addFilter(new QLEqualsFilter("cdViaturaSigma", codigoViartura));
					eventosList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAnd);
					list = new ArrayList<EventoEsperaVO>();
					EventoEsperaVO eventosObj = null;
					if (eventosList != null && eventosList.isEmpty())
					{
						if (motorista != null && !motorista.isEmpty())
						{
							motorista = verificaViatura(codigoViartura);
							int inicio = motorista.indexOf("-");
							int fim = motorista.lastIndexOf("-");
							String array[] = motorista.split("-");
							//String array[]  = motorista.split("-");
							int tamanho =  inicio + 1;
							if (array.length > 1)
							{
								if (motorista != null && fim >= tamanho)
								{
									rotaStr = motorista.substring(inicio + 1, fim);
									regionalStr = array[0];
								}
							}
						}

						Boolean existeEvento = listaEventos.contains(codigoViartura);
						if (existeEvento != null && !existeEvento && rotaStr != null && !rotaStr.isEmpty() && regionalStr != null && !regionalStr.isEmpty())
						{
							log.warn("##### EXECUTAR ROTINA DESLOCAMENTO PARA VIATURA LIVRE, VIATURA : " + codigoViartura + " Rota " + rotaStr + " Regional " + regionalStr + " -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

							conn = PersistEngine.getConnection("SIGMA90");

							StringBuilder sql = new StringBuilder();
							sql.append(" SELECT h.NU_AUXILIAR, h.CD_HISTORICO, h.CD_EVENTO, REPLACE(SUBSTRING(r.NM_ROTA, CHARINDEX('AA', r.NM_ROTA), 4), '-', '') AS ROTA, SUBSTRING(r.NM_ROTA, 1, 3) AS REG, ");
							sql.append(" c.ID_CENTRAL, c.PARTICAO, c.RAZAO, c.FANTASIA, c.CD_CLIENTE, c.ID_EMPRESA, c.STSERVIDORCFTV  ");
							sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
							sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
							sql.append(" INNER JOIN rota r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
							sql.append(" INNER JOIN dbCODE cuc WITH (NOLOCK) ON cuc.ID_CODE = h.CD_CODE ");
							sql.append(" WHERE h.FG_STATUS = 1 AND h.CD_HISTORICO_PAI IS NULL ");
							sql.append(" AND c.FANTASIA NOT LIKE('%DILMO%') AND c.RAZAO NOT LIKE ('%DILMO%') ");
							if (rotaStr != null && regionalStr != null)
							{
								sql.append(" AND r.NM_ROTA LIKE '%" + rotaStr + "%' ");

								sql.append(" AND (cuc.TIPO = 1 OR h.CD_EVENTO LIKE 'XXX7') ");
								sql.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) LIKE '%" + regionalStr + "%' ");
								sql.append(" ORDER BY h.DT_RECEBIDO ");
								pstm = conn.prepareStatement(sql.toString());
								rs = pstm.executeQuery();
								int contador = 0;
								while (rs.next())
								{
									log.warn("##### EXECUTAR ROTINA DESLOCAMENTO PARA VIATURA LIVRE, VIATURA : " + codigoViartura + " Rota " + rotaStr + " Regional " + regionalStr + " Evento : " + rs.getLong("CD_HISTORICO") + " -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
									log.warn(" SQL " + sql.toString());
									Long idEvento = (Long) rs.getLong("CD_HISTORICO");
									QLGroupFilter filterAndAux = new QLGroupFilter("AND");
									filterAndAux.addFilter(new QLEqualsFilter("codHistorico", String.valueOf(idEvento)));
									List<NeoObject> eventosListAux = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAndAux);
									if ((eventosListAux != null && eventosListAux.isEmpty()) || eventosListAux == null)
									{
										String central = rs.getString("ID_CENTRAL");
										String particao = rs.getString("PARTICAO");
										String razao = rs.getString("RAZAO");
										String fantasia = rs.getString("FANTASIA");
										String evento = rs.getString("CD_EVENTO");
										String cliente = rs.getString("CD_CLIENTE");
										String empresa = rs.getString("ID_EMPRESA");
										String stservcftv = rs.getString("STSERVIDORCFTV");

										Long idViatura = Long.parseLong(codigoViartura);

										returnFromAccess = executaServicoSegware(idViatura, idEvento);
										log.warn("##### EXECUTAR ROTINA DESLOCAMENTO PARA VIATURA LIVRE, VIATURA : " + codigoViartura + " Evento : " + idEvento + " -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

										if (returnFromAccess.equals("ACK"))
										{
											log.warn("##### EXECUTAR ROTINA DESLOCAMENTO PARA VIATURA LIVRE, VIATURA ACK : " + codigoViartura + " Evento : " + idEvento + " -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
										}
										else
										{
											log.warn("##### EXECUTAR ROTINA DESLOCAMENTO PARA VIATURA LIVRE, VIATURA RETORNO N�O ACK = " + returnFromAccess + " " + codigoViartura + " Evento : " + idEvento + " -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

										}

										if (returnFromAccess.equals("ACK") && stservcftv.equals("1") && !verificaEventoHistoricoView(cliente))
										{
											//returnFromAccess = executaServicoSegware(evento, cliente, empresa, central, particao);
										}
									}
									else
									{
										log.warn("##### EXECUTAR ROTINA DESLOCAMENTO PARA VIATURA LIVRE, VIATURA : " + codigoViartura + " Evento j� esta em espera para outro atendente : " + idEvento + " -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

									}

								}

							}
						}
						OrsegupsUtils.closeConnection(conn, pstm, rs);
					}
				}
			}
		}
	}
	catch (Exception e)
	{
		e.printStackTrace();
		log.error("##### ERRO ROTINA DESLOCAMENTO PARA VIATURA LIVRE -  Data: " + e.getStackTrace() + " " + e.getMessage() + " " + e.getCause() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	}
	finally
	{
		try
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			out.print("OK");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO ROTINA DESLOCAMENTO PARA VIATURA LIVRE -  Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		log.warn("##### FINALIZAR ROTINA DESLOCAMENTO PARA VIATURA LIVRE -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
%>


<%!private String verificaRetornoWebService(String retorno)
	{

		if (retorno.equals("VIATURA_NAO_ENCONTRADA"))
			retorno = "N�o � poss�vel identificar o ve�culo atrav�s do par�metro idRastreador.";
		else if (retorno.equals("EVENTO_NAO_ENCONTRADO"))
			retorno = "N�o � poss�vel identificar o evento atrav�s de par�metro idEvento.";
		else if (retorno.equals("CLIENTE_NAO_PERMITE_DESLOCAMENTO"))
			retorno = "O cliente pertence ao evento � configurado para n�o permitir deslocamentos carro.";
		else if (retorno.equals("CUC_NAO_PERMITE_DESLOCAMENTO"))
			retorno = "O CUC qual o evento pertence est� configurado para n�o permitir deslocamentos de carro.";
		else if (retorno.equals("CLIENTE_E_CUC_NAO_PERMITEM_DESLOCAMENTO"))
			retorno = "Tanto o cliente e o CUC que pertence ao evento s�o configurados para n�o permitir deslocamentos carro.";
		else if (retorno.equals("ERRO_ATUALIZAR_STATUS_EVENTO"))
			retorno = "Ocorreu um erro inesperado ao atualizar o status do evento";
		else if (retorno.equals("OPERACAO_ILEGAL"))
			retorno = "A mudan�a do status atual do evento para o estado entrou n�o � permitido. Um evento no estado - On Site - ou - servi�o feito - n�o pode ter seu status alterado para - Offset.";
		else if (retorno.equals("VIATURA_JA_DESLOCADA"))
			retorno = "O carro foi movido para outro evento. Esta mensagem s� ser� devolvido se o nas configura��es do desktop Sigma - Ativar compensado por um carro limita��o - est� habilitado.";
		else if (retorno.equals("DESLOCAMENTO_DE_EVENTO_FILHO_NAO_PERMITIDO"))
			retorno = "A mensagem retornada quando o agrupamento de eventos habilitado, eo evento a ser movido � um evento em cluster.";
		else if (retorno.equals("ACK"))
			retorno = "Opera��o bem-sucedida.";
		else
			retorno = "WebServiceSigma n�o retornou uma mensagem v�lida!. Mensagem de retorno: " + retorno;

		return retorno;
	}%>
<%!private String executaServicoSegware(Long idViatura, Long idEvento)
	{
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEventoViaturaRota");
		String returnFromAccess = "";
		try
		{
			DeslocarEventoWebServiceProxy webServiceProxy = new DeslocarEventoWebServiceProxy();

			returnFromAccess = webServiceProxy.deslocarEvento(idEvento, null, idViatura);

			log.warn("##### ROTINA DESLOCAMENTO PARA VIATURA LIVRE " + returnFromAccess + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
			log.error("##### ROTINA DESLOCAMENTO PARA VIATURA LIVRE " + returnFromAccess + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		finally
		{
			return returnFromAccess;
		}
	}%>
<%!private String executaServicoSegware(String evento, String cliente, String empresa, String idCentral, String particao)
	{
		String returnFromAccess = "";
		String eventoAux = "XVID";
		try
		{

			EventoRecebido eventoRecebido = new EventoRecebido();

			eventoRecebido.setCodigo(eventoAux);
			eventoRecebido.setData(new GregorianCalendar());
			eventoRecebido.setEmpresa(Long.parseLong(empresa));
			eventoRecebido.setIdCentral(idCentral);
			eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
			eventoRecebido.setParticao(particao);
			eventoRecebido.setProtocolo(Byte.parseByte("2"));

			ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();

			returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
		}
		finally
		{
			return returnFromAccess;
		}
	}%>



<%!private List<String> verificaEventoHistorico()
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Boolean flag = Boolean.FALSE;
		List<String> listaVerificaEvento = null;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder selecHtSQL = new StringBuilder();
			selecHtSQL.append(" SELECT H.CD_VIATURA ");
			selecHtSQL.append(" FROM HISTORICO H WITH (NOLOCK) ");
			selecHtSQL.append(" WHERE  H.FG_STATUS in (9,2) ");

			pstm = conn.prepareStatement(selecHtSQL.toString());
			//pstm.setString(1, idViatura);
			rs = pstm.executeQuery();
			int contador = 0;
			listaVerificaEvento = new ArrayList<String>();
			while (rs.next())
			{
				String his = rs.getString("CD_VIATURA");
				listaVerificaEvento.add(his);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			return listaVerificaEvento;
		}
	}%>

<%!
	private String verificaViatura(String cdViatura)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Boolean flag = Boolean.FALSE;
		String retorno = "";
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder selecHtSQL = new StringBuilder();
			selecHtSQL.append(" SELECT V.NM_VIATURA ");
			selecHtSQL.append(" FROM VIATURA V WITH (NOLOCK) ");
			selecHtSQL.append(" WHERE  V.CD_VIATURA = ? ");

			pstm = conn.prepareStatement(selecHtSQL.toString());
			pstm.setString(1, cdViatura);
			rs = pstm.executeQuery();
			int contador = 0;

			if (rs.next())
			{
				retorno = rs.getString("NM_VIATURA");

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			return retorno;
		}
	}
%>

<%!
private Boolean verificaEventoHistoricoView(String cdCliente)
{
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	Boolean flag = Boolean.FALSE;
	try
	{
		conn = PersistEngine.getConnection("SIGMA90");
		StringBuilder sql = new StringBuilder();
		sql.append("  SELECT TOP(1)V.CD_EVENTO,* FROM VIEW_HISTORICO V WITH (NOLOCK) WHERE V.CD_CLIENTE = ?  ORDER BY DT_RECEBIDO DESC ");
		st = conn.prepareStatement(sql.toString());
		st.setString(1, cdCliente);
		
		rs = st.executeQuery();

		while (rs.next()){
		System.out.println("ROTINA DESLOCAMENTO AUTOMATICO VIEW COD EVENTO : ");
			if(rs.getString("CD_EVENTO").contains("XVID"))
			flag = Boolean.TRUE;
		}

	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		OrsegupsUtils.closeConnection(conn, st, rs);
		return flag;
	}

}

%>



