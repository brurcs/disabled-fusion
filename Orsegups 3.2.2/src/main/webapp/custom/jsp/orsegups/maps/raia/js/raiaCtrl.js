var raiaApp = angular.module('raiaApp', ['infinite-scroll']);
angular.module('raiaApp', ['infinite-scroll'])
.controller('RaiaController', ['$scope', '$http', '$q', function RaiaController($scope, $http, $q) {
	console.log("loaded controller");
	$('[data-toggle="tooltip"]').tooltip();
	$scope.totalPatrimonios = 0;
	$scope.totalParticoes = 0;
	$scope.totalCftv = 0;
	$scope.totalArmado = 0;
	$scope.totalArmadoParcial = 0;
	$scope.totalDesarmado = 0;
	$scope.totalAlarmesAndamento = 0;
	$scope.listManutencaoAlarme = [];
	$scope.listManutencaoCftv = [];
	$scope.listIntrusao = [];
	$scope.listPanico = [];
	$scope.listDeslocamento = [];
	$scope.listFalhaComunicacao = [];
	$scope.listFaltaEnergia = [];
	$scope.listBateriaBaixa = [];
	$scope.listImagens = [];
	$scope.listNaoArmado = [];
	$scope.listDesarmadoForaHorario = []
	$scope.centrais = [];
	$scope.eventos = [];
	$scope.statusSelected = '';
	$scope.searchConta = "";
	$scope.searchUf = "";
	$scope.searchDescricao = "";
	$scope.searchCidade = "";
	$scope.searchBairro = "";
	$scope.currentLog = "";
	$scope.limit = 50;
	$scope.isLoading = false;
	
	$scope.listStatus = [
		{
			nome: "Todos",
			value: null,
		},
		{
			nome: "Sistema de Alarme Ativado",
			value: true,
		},
		{
			nome: "Sistema de Alarme Desativado",
			value: false
		}
	]
	$scope.statusSelected = $scope.listStatus[0];
	$scope.eventoSelected = {};	
	const ICONS = {
		INTRUSAO: "fa-user-ninja",
		PANICO: "fa-exclamation-triangle",
		LOCAL: "fa-user-shield",
		FALHA_COMUNICACAO: "fa-wifi",
		FALTA_ENERGIA: "fa-bolt",
		BATERIA_BAIXA: "fa-battery-quarter",
		IMAGENS: "fa-images",
		DESLOCAMENTO: "fa-motorcycle",
		MANUTENCAO_ALARME: "fa-wrench",
		MANUTENCAO_CFTV: "fa-video"
	}
	$scope.ICONS = ICONS;
	
	$scope.executeHttp = function(path, callback){
		$http.get(path).then(function successCallback(response) {
			if(callback){
				callback(response);
			}
		}, function errorCallback(response) {
			$scope.isLoading = false;
			console.log('Erro ao carregar total!');
		});
	}
	$scope.initializeIcons = function(){
		$scope.eventos = [
			{
				array: $scope.listDeslocamento,
				icon: ICONS.DESLOCAMENTO,
				nome: "Em Atendimento T�tico"
			},
			{
				array: $scope.listIntrusao,
				icon: ICONS.INTRUSAO,
				nome: "Alarmando"
			},
			{
				array: $scope.listNaoArmado,
				icon: ICONS.PANICO,
				nome: "N�o Armado"
			},
			{
				array: $scope.listDesarmadoForaHorario,
				icon: ICONS.PANICO,
				nome: "Desarmado fora do hor�rio"
			},
			{
				array: $scope.listFalhaComunicacao,
				icon: ICONS.FALHA_COMUNICACAO,
				nome: "Falha de Comunica��o"
			},
			{
				array: $scope.listFaltaEnergia,
				icon: ICONS.FALTA_ENERGIA,
				nome: "Falta de Energia"
			},
			{
				array: $scope.listBateriaBaixa,
				icon: ICONS.FALTA_ENERGIA,
				nome: "Bateria Baixa"
			},
			{
				array: $scope.listManutencaoAlarme,
				icon: ICONS.MANUTENCAO_ALARME,
				nome: "Sistema de Alarme em Manuten��o"
			},
			{
				array: $scope.listManutencaoCftv,
				icon: ICONS.MANUTENCAO_CFTV,
				nome: "Sistema de CFTV em Manuten��o"
			}
		]
	}
	
	$scope.getList = function(path, callback){
		  return $q(function(resolve, reject){
			  $http.get(path).then(function(response) {
				  if(callback){
					  callback(response);
				  }
				  resolve(response);
			  });
		  });
	}
	$scope.init = function(){
		$scope.totalPatrimonios = 0;
		$scope.totalParticoes = 0;
		$scope.totalCftv = 0;
		$scope.totalArmado = 0;
		$scope.totalArmadoParcial = 0;
		$scope.totalDesarmado = 0;
		$scope.totalAlarmesAndamento = 0;
		$scope.listManutencaoAlarme = [];
		$scope.listManutencaoCftv = [];
		$scope.listIntrusao = [];
		$scope.listPanico = [];
		$scope.listDeslocamento = [];
		$scope.listFalhaComunicacao = [];
		$scope.listFaltaEnergia = [];
		$scope.listBateriaBaixa = [];
		$scope.listImagens = [];
		$scope.listNaoArmado = [];
		$scope.listDesarmadoForaHorario = []
		$scope.centrais = [];
		$scope.eventos = [];
		$scope.statusSelected = '';
		$scope.searchConta = "";
		$scope.searchUf = "";
		$scope.searchDescricao = "";
		$scope.searchCidade = "";
		$scope.searchBairro = "";
		$scope.currentLog = "";
		$scope.limit = 50;
		$scope.isLoading = true;
		//$scope.executeHttp('/fusion/services/raia/totalPatrimonios', (response) => $scope.totalPatrimonios = response.data);
		//$scope.executeHttp('/fusion/services/raia/totalParticoes', (response) => $scope.totalParticoes = response.data);
		//$scope.executeHttp('/fusion/services/raia/totalArmadoParcial', (response) => $scope.totalArmadoParcial = response.data);
		$scope.executeHttp('/fusion/services/raia/totalCftv', (response) => $scope.totalCftv = response.data);
		$scope.executeHttp('/fusion/services/raia/totalArmado', (response) => $scope.totalArmado = response.data);
		$scope.executeHttp('/fusion/services/raia/totalDesarmado', (response) => $scope.totalDesarmado = response.data);
		//Eventos
		var requests = new Array();
		
		requests.push($scope.getList('/fusion/services/raia/totalManutencaoCftv', (response) => $scope.listManutencaoCftv = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalManutencaoAlarme', (response) => $scope.listManutencaoAlarme = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalIntrusao', (response) => $scope.listIntrusao = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalDeslocamento', (response) => $scope.listDeslocamento = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalPanico', (response) => $scope.listPanico = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalFalhaComunicacao', (response) => $scope.listFalhaComunicacao = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalFaltaEnergia', (response) => $scope.listFaltaEnergia = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalDesarmadoForaHorario', (response) => $scope.listDesarmadoForaHorario = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalNaoArmado', (response) => $scope.listNaoArmado = response.data));
		requests.push($scope.getList('/fusion/services/raia/totalBateriaBaixa', (response) => $scope.listBateriaBaixa = response.data));
		//tabela de centrais
		$q.all(requests).then(function (resolvers) {
			$scope.initializeIcons();
			$scope.executeHttp('/fusion/services/raia/getCentrais', (response) => {$scope.centrais = response.data; $scope.isLoading = false; });
		});
	}
	$scope.filterCentrais = function(item) {
		return item.conta.toLowerCase().includes($scope.searchConta.toLowerCase()) && 
		item.uf.toLowerCase().includes($scope.searchUf.toLowerCase()) &&
		item.descricao.toLowerCase().includes($scope.searchDescricao.toLowerCase()) &&
		item.bairro.toLowerCase().includes($scope.searchBairro.toLowerCase()) &&
		item.cidade.toLowerCase().includes($scope.searchCidade.toLowerCase()) &&
		$scope.checkStatusSelected(item) &&
		$scope.checkEventoSelected(item);
	};
	$scope.checkStatusSelected = function(item){
		return $scope.statusSelected.value == null || item.status == $scope.statusSelected.value;
	}
	$scope.checkEventoSelected = function(item){
		return $scope.eventoSelected && $scope.eventoSelected.nome? $scope.checkIfContains($scope.eventoSelected, item):true;
	}
	$scope.checkIfContains = function(item, object){
		return item.array.some(central => central.cdCliente === object.cdCliente);
	}
	 $scope.addLimit = function(){
        if((!$scope.eventoSelected || !$scope.eventoSelected.nome) && $scope.centrais.length > 0){
            $scope.limit += 50;
        }
    }
	$scope.openLog = function(item, central){
		for(itemSelect of item.array){
			if(central.cdCliente === itemSelect.cdCliente){
				$scope.currentLog = itemSelect.log && itemSelect.log != ''?itemSelect.log:'Nenhum log cadastrado at� o momento. Clique em atualizar se necess�rio.';
				$('#logModal').modal('show');
				break;
			}
		}
	}
	$scope.init();
}]);
