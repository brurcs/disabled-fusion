<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertEventEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.vo.DBOrdemVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.SigmaUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
	String codigoHistorico = request.getParameter("codigoHistorico");
%>

<html>
<body>


<div style="width:90%; padding:5px;height:auto">
		<form id="fmOrdem" method="post">
			<table style="width: 100%;">
				<tbody>
					<tr>
						<td class="flabel" class="flabel">Descrição:</td>
						<td class="fvalue">
						  <input class="easyui-textbox" id="taDescricao" name="taDescricao" data-options="multiline:true,   prompt: 'Insira a descrição aqui!'," rows="5" cols="50" style="width:300px;height:100px">

						</td>
					</tr>
				</tbody>
			</table>		
			<div align="center" style="width:90%; padding: 10px">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="abreOs()"  iconCls="icon-ok">Salvar</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#modaAbreOS').dialog('close')" iconCls="icon-cancel">Cancelar</a>
			</div>
		</form>	
</div>	
<script type="text/javascript">
	
	function abreOs()
	{	var descricao = $('#taDescricao').val();
		var codigoHistorico = '<%=codigoHistorico%>';
		if(codigoHistorico != null || codigoHistorico != 'null')
		{
			$.ajax({
	 			   type: "GET",
	 	 		   url: "<%=PortalUtil.getBaseURL()%>services/eventos/abreOs/"+codigoHistorico+"/"+descricao,
	 	 		   success: function(resp){  
	 	 		   if(resp != null)
	         	   result();
	 	 		   $('#modaAbreOS').dialog('close');
	         	   },  
	       		   error: function(e){  
	        	   error();
				  
	      		   }  
		 		   });
		}
	}
	
	function error(){
  		$.messager.alert('Atenção','Erro ao salvar informações!','error');
  		
 	}
  
    
    function result(){
        $.messager.show({
               title:'Atenção',
               msg:'<div class=\'messager-icon messager-info\'></div>Dados salvos com sucesso!',
               showType:'show'
               });
     }
  
</script>
</body>
</html>