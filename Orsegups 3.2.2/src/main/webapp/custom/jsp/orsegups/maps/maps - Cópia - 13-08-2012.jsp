<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0px;
	padding: 0px
}

#map_canvas {
	height: 100%
}

.labels {
     color: #6C6C6C;
     background-color: white;
     font-family: "Tahoma";
     font-size: 8px;
     font-weight: bold;
     text-align: center;
     width: 24px;
     height: 12px;
     border: 1px solid black;
     white-space: nowrap;
   }
   
.window_black_bold {
	font-family: "Tahoma";
	color: black;
	background-color: white;
	font-size: 12px;
	font-weight: bold
}

.window_black {
	font-family: "Tahoma";
	color: black;
	background-color: white;
	font-size: 12px;
}
</style>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>css/themes/neo.ui.css.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<!-- 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.layout.js"></script>
	 -->
<script type="text/javascript"
	src="https://maps.google.com/maps/api/js?sensor=false&language=pt-BR&region=BR"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/markerwithlabel.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/keydragzoom.js"></script>
	
<%
	String action = request.getParameter("action");
%>

<script type="text/javascript">
	
	var mapsLayout;
	var map;
	var viaturasArray = [];
	var eventosArray = [];
	var ligacaoEventoArray = [];
	var ligacaoViaturaArray = [];
	var refreshing = true;
	var timeout;
	var timeoutOS;
	var timeoutViaturaTecnica;
	var filter = "";
	var tecnicoSelecionado;

	filter = "<%=action%>";
	
	$(document).ready(function () {
		//mapsLayout = $('body').layout({ applyDefaultStyles : true });
		
		//mapsLayout.sizePane("west", 300);
		
		//mapsLayout.close("west");
		
		initialize();
	});
	
	// Possiveis valores do atributo mapTypeId google.maps.MapTypeId
	//google.maps.MapTypeId.ROADMAP exibe as blocos 2D normais, padr�o, do Google Maps.
	//google.maps.MapTypeId.SATELLITE exibe blocos fotogr�ficos.
	//google.maps.MapTypeId.HYBRID exibe uma mistura entre blocos fotogr�ficos e uma camada de blocos com recursos importantes (estradas, nomes de cidade).
	//google.maps.MapTypeId.TERRAIN exibe blocos de relevo f�sico para exibi��o de recursos de eleva��o e �gua (montanhas, rios etc.).
	
	
	function initialize() {
		var latlng = new google.maps.LatLng(-27.59694, -48.54889);
		var myOptions = {
			zoom : 9,
			center : latlng,
			panControl: false,
			streetViewControl: false,
			zoomControl : true,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
		map.enableKeyDragZoom({
	          visualEnabled: true,
	          visualPosition: google.maps.ControlPosition.TOP_LEFT,
	          visualPositionOffset: new google.maps.Size(0, 5),
	          visualPositionIndex: null,
	          visualSprite: "images/dragzoom_btn.png",
	          visualSize: new google.maps.Size(20, 20),
	          visualTips: {
	            off: "Zoom Area",
	            on: "Zoom Area"
	          }
	        });
		
		// Create the DIV to hold the control and call the HomeControl() constructor
		// passing in this DIV.
		var fitToMarkersControlDiv = document.createElement('DIV');
		var fitToMarkersControl = new FitToMarkersControl(fitToMarkersControlDiv, map);
		
		fitToMarkersControlDiv.index = 1;
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(fitToMarkersControlDiv);
		
		
		
		if(filter == "centralMonitoramento")
		{
			loadViaturasCM();
			loadEventosCM();
		}
		else if(filter == "areaTecnica")
		{
			loadAtendentes();
		}
		
		fitToMarkers();
	}
	
	function FitToMarkersControl(controlDiv, map) {

		  // Set CSS styles for the DIV containing the control
		  // Setting padding to 5 px will offset the control
		  // from the edge of the map
		  controlDiv.style.padding = '5px';

		  // Set CSS for the control border
		  var controlUI = document.createElement('DIV');
		  controlUI.style.backgroundColor = 'white';
		  controlUI.style.borderStyle = 'solid';
		  controlUI.style.borderWidth = '2px';
		  controlUI.style.cursor = 'pointer';
		  controlUI.style.textAlign = 'center';
		  controlUI.title = 'Enquadrar Marcadores';
		  controlDiv.appendChild(controlUI);

		  // Set CSS for the control interior
		  var controlText = document.createElement('DIV');
		  controlText.style.fontFamily = 'Tahoma';
		  controlText.style.fontSize = '12px';
		  controlText.style.paddingLeft = '4px';
		  controlText.style.paddingRight = '4px';
		  controlText.innerHTML = 'Enquadrar Marcadores';
		  controlUI.appendChild(controlText);

		  // Setup the click event listeners: simply set the map to Chicago
		  google.maps.event.addDomListener(controlUI, 'click', function() {
			  fitToMarkers();
		  });
	}
	
	function BarraAtendentes(map, atendenteObj) {

		var barraAtendentesDiv = document.createElement('DIV');
		
		barraAtendentesDiv.index = 1;
		
		// Set CSS styles for the DIV containing the control
		// Setting padding to 5 px will offset the control
		// from the edge of the map
		barraAtendentesDiv.style.padding = '2px';
		barraAtendentesDiv.style.height = '40px';
		
		// Set CSS for the control border
		var controlUI = document.createElement('DIV');
		controlUI.style.backgroundColor = 'white';
		controlUI.style.borderStyle = 'solid';
		controlUI.style.borderWidth = '2px';
		controlUI.style.cursor = 'pointer';
		controlUI.style.textAlign = 'center';
		controlUI.title = atendenteObj.nomeAtendente;
		barraAtendentesDiv.appendChild(controlUI);
		
		// Set CSS for the control interior
		var controlText = document.createElement('DIV');
		controlText.style.fontFamily = 'Tahoma';
		controlText.style.fontSize = '10px';
		controlText.style.paddingLeft = '4px';
		controlText.style.paddingRight = '4px';
		//controlText.style.height = '40px';
		//controlText.style.width = '80px';
		controlText.innerHTML = atendenteObj.nomeAtendente;
		controlUI.appendChild(controlText);
		
		
		// Setup the click event listeners: simply set the map to Chicago
		google.maps.event.addDomListener(controlUI, 'click', function() {
			loadTecnico(atendenteObj.codigoAtendente);
		});
		
		map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(barraAtendentesDiv);
	}
	
	function loadEventosCM()
	{
		var eventosJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=eventosCM");
		
		parseEventos(eventosJson);
		
		if(refreshing)
		{
			timeout = setTimeout("loadEventosCM()",15000);
		}
	}
	
	function loadViaturasCM()
	{
		var viaturasJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=viaturasCM");
		
		parseViaturas(viaturasJson);
		
		if(refreshing)
		{
			timeout = setTimeout("loadViaturasCM()",15000);
		}
	}
	
	function loadTecnico(codigoTecnico)
	{
		if(codigoTecnico != null)
		{
			// remove os timeOuts que recaregam os dados a cada 15 segundos para nao sobrecarrega-los 
			clearTimeout(timeoutOS);
			clearTimeout(timeoutViaturaTecnica);
			
			//remove todos os itens do mapa pois ao chamar este metodo estaremos carregando viatura e os de outro tecnico
			for(e in eventosArray)
			{
				removeEvento(e);
			}
			for(v in viaturasArray)
			{
				removeViatura(v);
			}
			
			// atualiza quem � o tecnico atual para que o refresh saiba de quem carregar as OS
			tecnicoSelecionado = codigoTecnico; 
			
			// busca os novos dados
			loadViaturasTecnicas()
			loadOS();
		}
		fitToMarkers();
	}
	
	function loadOS()
	{
		var osJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=OS&codigoTecnico="+tecnicoSelecionado);
		
		parseEventos(osJson);
		
		if(refreshing)
		{
			timeoutOS = setTimeout("loadOS()",15000);
		}
	}
	
	function loadViaturasTecnicas()
	{
		var viaturasJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=viaturasTecnicas&codigoTecnico="+tecnicoSelecionado);
		
		parseViaturas(viaturasJson);
		
		if(refreshing)
		{
			timeoutViaturaTecnica = setTimeout("loadViaturasTecnicas()",15000);
		}
	}
	
	function loadAtendentes()
	{
		var atendentesJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet?action=atendentes");
		
		parseAtendentes(atendentesJson);
		
		if(refreshing)
		{
			//timeout = setTimeout("loadAtendentes()",15000);
		}
	}
	
	function parseAtendentes(atendentesJson)
	{
		if(atendentesJson != null && atendentesJson != "")
		{
			atendentes = JSON.parse(atendentesJson);
			
			for(i in atendentes) 
			{
				var atendenteObj = atendentes[i];
				
				new BarraAtendentes(map, atendenteObj);
			}
		}
	}
		
		
	function parseEventos(eventosJson)
	{
		if(eventosJson != null && eventosJson != "")
		{
			eventos = JSON.parse(eventosJson);
			
			for(i in eventos) 
			{
				var eventoObj = eventos[i];
				
				manageEvento(eventoObj);
			}
			
			for(k in eventosArray)
			{
				var exists = false;
				
				for(n in eventos) {
					if(eventos[n].codigoHistorico == k)
					{
						exists = true;
						break;
					}
				}
				
				if(!exists)
				{
					removeEvento(k);
				}
			}
		}
		
		if(refreshing)
		{
			timeout = setTimeout("parseEventos()",15000);
		}
	}
	
	function removeEvento(k)
	{
			if(eventosArray[k] != null)
			{
				if(eventosArray[k]["marker"] != null)
				{
					eventosArray[k]["marker"].setMap(null);
					eventosArray[k]["marker"] = null;
				}
				
				if(eventosArray[k]["info"] != null)
				{
					eventosArray[k]["info"].setMap(null);
					eventosArray[k]["info"] = null;
				}
				
				if(eventosArray[k]["line"] != null)
				{
					eventosArray[k]["line"].setMap(null);
					eventosArray[k]["line"] = null;
				}
				
				var placa = eventosArray[k]["eventoObj"].placa;
				if(placa != null && placa != "" && viaturasArray[placa] != null)
				{
					if(viaturasArray[placa]["line"] != null && viaturasArray[placa]["line"][k] != null)
					{
						viaturasArray[placa]["line"][k].setMap(null);
						viaturasArray[placa]["line"][k] = null;
					}
					
					if(viaturasArray[placa]["marker"] != null)
					{
						viaturasArray[placa]["marker"].setIcon("images/"+viaturasArray[placa]["viaturaObj"].nomeImagem+".png");
					}
					
				}
				
				eventosArray[k] = null;
			}
	}
	
	function parseViaturas(viaturasJson)
	{
		if(viaturasJson != null && viaturasJson != "")
		{
			var viaturas = JSON.parse(viaturasJson);
			
			for(i in viaturas) {
				var viaturaObj = viaturas[i];
				
				manageViatura(viaturaObj);
		    }
			
			for(k in viaturasArray)
			{
				var exists = false;
				
				for(n in viaturas) {
					if(viaturas[n].placa == k)
					{
						exists = true;
						break;
					}
				}
				
				if(!exists)
				{
					removeViatura(k);
				}
			}
		}
	}
	
	function removeViatura(k)
	{
		if(viaturasArray[k] != null)
		{
			if(viaturasArray[k]["marker"] != null)
			{
				viaturasArray[k]["marker"].setMap(null);
				viaturasArray[k]["marker"] = null;
			}
			
			if(viaturasArray[k]["info"] != null)
			{
				viaturasArray[k]["info"].setMap(null);
				viaturasArray[k]["info"] = null;
			}
			
			if(viaturasArray[k]["line"] != null)
			{
				lines = viaturasArray[k]["line"];
				
				for(i in lines)
				{
					if(lines[i] != null)
					{
						lines[i].setMap(null);
						lines[i] = null;
						
						if(eventosArray[i] != null && eventosArray[i]["marker"] != null)
						{
							eventosArray[i]["marker"].setIcon("images/"+eventosArray[i]["eventoObj"].nomeImagem+".png");
							if(eventosArray[i]["line"] != null)
							{
								eventosArray[i]["line"].setMap(null);
								eventosArray[i]["line"] = null;
							}
						}
					}
				}
			}
			viaturasArray[k] = null;
		}
	}
	
	function startRefresh()
	{
		if(!refreshing)
		{
			refreshing = true;
			
			loadViaturas();
		}
	}
	
	function stopRefresh()
	{
		if(refreshing)
		{
			refreshing = false;
			
			clearTimeout(timeout);
		}
	}
	
	function manageEvento(eventoObj)
	{
		var location = new google.maps.LatLng(eventoObj.latitude, eventoObj.longitude);
		var marker = null;
		var infowindow = null;
		var eventoItem = null;
		var eventoViaturaLine = null;
		
		eventoItem = eventosArray[eventoObj.codigoHistorico];
		
		if(eventoItem != null)
		{
			marker = eventoItem["marker"];
			infowindow = eventoItem["info"];
			eventoViaturaLine = eventoItem["line"];
		}
		else
		{
			eventoItem = [];
		}
		
		eventoItem["eventoObj"] = eventoObj;
		
		//cria o marcador do evento
		if(marker == null)
		{
			marker = new MarkerWithLabel({
				position: location,
				map: map,
				title : eventoObj.codigoEvento + ' - ' + eventoObj.nomeEvento,
				icon : "images/"+eventoObj.nomeImagem+".png",
				zIndex : 100,
//				labelContent: " &nbsp; ",
				labelContent: eventoObj.codigoEvento,
		       	labelClass: "labels", // the CSS class for the label
		       	labelAnchor: new google.maps.Point(13, 47),
		       	labelStyle: {opacity: 1, backgroundColor: eventoObj.corPrioridade}
			});
			
			if(eventoObj.type != null && eventoObj.type == "os")
			{
				marker.setTitle("");
				marker.set("labelVisible", false);
			}
			
			eventoItem["marker"] = marker;
		}

		var contentString = eventoObj.infoWindowContent;
		
		// se a janela ja existe atualiza o conteudo da janela, caso contrario cria ela
		if(infowindow != null)
		{
			infowindow.setContent(contentString);
		}
		else
		{
			infowindow = new google.maps.InfoWindow({
			    content: contentString
			});
			eventoItem["info"] = infowindow;
			
			google.maps.event.addListener(marker, 'click', function() {
				  infowindow.open(map,marker);
				});
		}
		
		// gerencia a ligacao entre o evento e a viatura
		if(eventoObj.placa != null && eventoObj.placa != '')
		{
			eventoLatLng = location;
			
			viaturaItem = viaturasArray[eventoObj.placa];
			if(viaturaItem != null)
			{
				var viatura = viaturaItem["marker"]; 
				viaturaLatLng = viatura.getPosition();
				
				var eventoViaturaPath = [eventoLatLng, viaturaLatLng];
				
				if(eventoViaturaLine != null)
				{
					eventoViaturaLine.setPath(eventoViaturaPath);
				}
				else
				{
					var eventoViaturaLine = new google.maps.Polyline({
						path: eventoViaturaPath,
						strokeColor: viaturaItem["viaturaObj"].corLigacao,
						strokeOpacity: 1.0,
						strokeWeight: 2
					});
					
					eventoViaturaLine.setMap(map);
					
					eventoItem["line"] = eventoViaturaLine;
					
					lines = viaturaItem["line"];
					if(lines == null)
					{
						lines = [];
						viaturaItem["line"] = lines;
					}
					lines[eventoObj.codigoHistorico] = eventoViaturaLine;
				}
				
				if(viaturaItem["viaturaObj"].sufixoLigacao != null)
				{
					marker.setIcon("images/"+eventoObj.nomeImagem+"_"+viaturaItem["viaturaObj"].sufixoLigacao+".png");
					viatura.setIcon("images/"+viaturaItem["viaturaObj"].nomeImagem+"_"+viaturaItem["viaturaObj"].sufixoLigacao+".png");
				}
			}
			viaturasArray[eventoObj.placa] = viaturaItem;
		}
		eventosArray[eventoObj.codigoHistorico] = eventoItem;
	}
	
	function manageViatura(viaturaObj)
	{
		var location = new google.maps.LatLng(viaturaObj.latitude, viaturaObj.longitude);
		var marker = null;
		var infowindow = null;
		var viaturaItem = null;
		var lines = null;
		
		viaturaItem = viaturasArray[viaturaObj.placa];
		
		if(viaturaItem != null)
		{
			marker = viaturaItem["marker"]; 
			infowindow = viaturaItem["info"];
			lines = viaturaItem["line"];
		}
		else
		{
			viaturaItem = [];
		}
		
		viaturaItem["viaturaObj"] = viaturaObj;
		
		if(marker != null)
		{
			marker.setPosition(location);
			marker.set("labelContent", viaturaObj.titulo);
		}
		else
		{
			marker = new MarkerWithLabel({
				position: location,
				map: map,
				title : viaturaObj.titulo,
				icon : "images/"+viaturaObj.nomeImagem+".png",
				zIndex : 0,
				labelContent: viaturaObj.titulo,
		       	labelClass: "labels", // the CSS class for the label
		       	labelAnchor: new google.maps.Point(13, 47),
		       	labelStyle: {opacity: 0.75}
			});
			
			viaturaItem["marker"] = marker;
		}
		
		var contentString = viaturaObj.infoWindowContent;
		
		if(infowindow != null)
		{
			infowindow.setContent(contentString);
		}
		else
		{
			infowindow = new google.maps.InfoWindow({
			    content: contentString
			});
			viaturaItem["info"] = infowindow;
			
			google.maps.event.addListener(marker, 'click', function() {
				  infowindow.open(map,marker);
				});
		}
		
		// caso tenha uma linha associada a esta viatura atualiza o ponto
		if(lines != null)
		{
			for(i in lines)
			{
				var line = lines[i];
				if(line != null)
				{
					var path = line.getPath();
					path.removeAt(1);
					path.insertAt(1, location);
				}
			}
		}
			
		viaturasArray[viaturaObj.placa] = viaturaItem;
	}
	
	function fitToMarkers()
	{
		var latLngBounds = null;
		
		for(v in viaturasArray)
		{
			if(viaturasArray[v] != null && viaturasArray[v]["marker"] != null)
			{
				var location = viaturasArray[v]["marker"].getPosition();
				
				if(latLngBounds == null)
				{
					latLngBounds = new google.maps.LatLngBounds(location, location);
				}
				else
				{
					latLngBounds.extend(location);
				}
			}
		}
		
		for(e in eventosArray)
		{
			if(eventosArray[e] != null && eventosArray[e]["marker"] != null)
			{
				var location = eventosArray[e]["marker"].getPosition();
				
				if(latLngBounds == null)
				{
					latLngBounds = new google.maps.LatLngBounds(location, location);
				}
				else
				{
					latLngBounds.extend(location);
				}
			}
		}
		if(latLngBounds != null)
		{
			map.fitBounds(latLngBounds);
		}
	}
	
</script>
</head>
<body>
	<!-- 
	<div id="filter-canvas" style="width: 400px; height: 100%" class="ui-layout-west">
		<button onclick="fitToMarkers()" >Fit Markers</button>
	</div>
	 -->
	<div id="map_canvas" style="width: 100%; height: 100%" class="ui-layout-centert"></div>
</body>
</html>
