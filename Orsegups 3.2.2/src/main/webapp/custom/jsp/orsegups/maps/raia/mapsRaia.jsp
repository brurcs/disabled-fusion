<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<!DOCTYPE html>
<html ng-app="raiaApp">
	<head>
		<meta charset="ISO-8859-1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	 	<link rel="stylesheet" href="css/estilos.css">
		<title>Opera��o Raia Drogasil</title>
	</head>
	<body ng-controller="RaiaController" ng-cloak>
		<section id="main">
			<div class="container-fluid" align="center">
				<div>
					<div style="position: absolute; top: 0;">
						<img src="../images/drogaraia.png" style="height: 80px; float: left;">
						<img src="../images/Logo-Orsegups-WhiteBG.png" style="height: 45px; float: left; margin-top: 13px;">
					</div>
					<h1 style="font-size: 30px">
						Opera��o Raia Drogasil / Orsegups
					</h1>
				</div>
				<div class="row" align="center" width="98%">
					<div class="col-md-12">
						<div class="panel with-nav-tabs panel-primary">
							<div class="panel-heading">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab1primary" data-toggle="tab">Informa��es Gerais</a></li>
									<li><a href="#tab2primary" data-toggle="tab">Mapa Eventos</a></li>
									<li><a href="#tab3primary" data-toggle="tab">Mapa OS</a></li>
									<li style="float: right"><div style="text-align: right;"><button type="button" class="btn btn-primary" style="color: #fff;background-color: #8db1d0;border-color: #90afca;" ng-click="init()">Atualizar</button></div></li>
								</ul>
							</div>
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="tab1primary" infinite-scroll="addLimit()" infinite-scroll-distance="2">
										<div class="table-responsive">
											<table class="table table-bordered table-info">
											<tr class="tr-title">
													<td colspan="4">Informa��es</td>
												</tr>
												<tr>
													<td><i class="fas fa-map-marker-alt"></i> Contas de Alarme</td>
													<td class="total-number">{{totalArmado + totalDesarmado}}</td>
													<td><i class="fas fa-map-marker-alt"></i> Contas de CFTV</td>
													<td class="total-number">{{totalCftv}}</td>
													<!-- <td><i class="fas fa-broadcast-tower"></i> Total Parti��es</td>
													<td class="total-number">{{totalParticoes}}</td> -->
												</tr>
												<tr class="tr-title">
													<td colspan="4">Status dos Sistemas</td>
												</tr>
												<tr>
													<td><i class="fas fa-lock"></i> Sistema de Alarme Ativado</td>
													<td class="total-number">{{totalArmado}}</td>
													<td><i class="fas {{ICONS.IMAGENS}}"></i> Sistemas CFTV - Online</td>
													<td class="total-number">0</td>	
												</tr>
												<tr>
													<td><i class="fas fa-lock-open"></i> Sistema de Alarme Desativado</td>
													<td class="total-number">{{totalDesarmado}}</td>
													<td><i class="fas {{ICONS.IMAGENS}}"></i> Sistemas CFTV - Offline</td>
													<td class="total-number">0</td>
												</tr>
												<tr class="tr-title">
													<td colspan="4">Status da Opera��o</td>
												</tr>
												<tr>
													<td><i class="fas {{ICONS.PANICO}}"></i> Desvio de H�bito</td>
													<td class="total-number">{{listNaoArmado.length + listDesarmadoForaHorario.length}}</td>
													<td><i class="fas {{ICONS.INTRUSAO}}"></i> Alarmando</td>
													<td class="total-number">{{listIntrusao.length}}</td>
													<!-- <td><i class="fas fa-bell"></i> Alarme em Andamento</td>
													<td class="total-number">{{listIntrusao.length + listPanico.length + listDeslocamento.length  + listNoLocal.length}}</td> -->
												</tr>
												<tr>
													<td><i class="fas {{ICONS.FALTA_ENERGIA}}"></i> Falta de Energia</td>
													<td class="total-number">{{listFaltaEnergia.length}}</td>
													<td><i class="fas {{ICONS.FALHA_COMUNICACAO}}"></i> Falha de Comunica��o</td>
													<td class="total-number">{{listFalhaComunicacao.length }}</td>
												</tr>
												<tr>
													<td><i class="fas {{ICONS.BATERIA_BAIXA}}"></i> Bateria Baixa</td>
													<td class="total-number">{{listBateriaBaixa.length}}</td>
													<td><i class="fas {{ICONS.DESLOCAMENTO}}"></i> Em Atendimento T�tico</td>
													<td class="total-number">{{listDeslocamento.length }}</td>
												</tr>
												<tr class="tr-title">
													<td colspan="4">Status de Manuten��o</td>
												</tr>
												<tr>
													<td><i class="fas {{ICONS.MANUTENCAO_ALARME}}"></i> Sistema de Alarme em Manuten��o</td>
													<td class="total-number">{{listManutencaoAlarme.length}}</td>
													<td><i class="fas {{ICONS.MANUTENCAO_CFTV}}"></i> Sistema de CFTV em Manuten��o</td>
													<td class="total-number">{{listManutencaoCftv.length}}</td>
												</tr>
											</table>
										</div>
										<div class="table-responsive">
											<table class="table table-stripped" id="tableCentrais">
												<thead>
													<tr>
														<td>
															Status
															<div>
																<select ng-model="statusSelected"  ng-options="status.nome for status in listStatus track by status.value" style="width: 85%" class="form-control">
																</select>
															</div>
														</td>
														<td>
															Conta
															<div>
																<input ng-model="searchConta" style="width: 85%" type="text" class="form-control " placeholder="Pesquisar..">
															</div>
														</td>
														<td>
															Descri��o
															<div>
																<input ng-model="searchDescricao" style="width: 85%" type="text" class="form-control " placeholder="Pesquisar..">
															</div>
														</td>
														<td>
															Bairro
															<div>
																<input ng-model="searchBairro" style="width: 85%" type="text" class="form-control " placeholder="Pesquisar..">
															</div>
														</td>
														<td>
															Cidade
															<div>
																<input ng-model="searchCidade" style="width: 85%" type="text" class="form-control " placeholder="Pesquisar..">
															</div>
														</td>
														<td>
															UF
															<div>
																<input ng-model="searchUf" style="width: 85%" type="text" class="form-control " placeholder="Pesquisar..">
															</div>
														</td>
														<td>
															Eventos
															<select ng-model="eventoSelected"  ng-options="evento.nome for evento in eventos track by evento.nome" style="width: 85%" class="form-control">
																<option value="">Todos</option>
															</select>
														</td>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="central in centrais | filter : filterCentrais | limitTo: limit">
														<td><i ng-class="{'fas fa-lock icon-success' : central.status, 'fas fa-lock-open icon-warning' : !central.status}"></i></td>
														<td>{{central.conta}}</td>
														<td>{{central.descricao}}</td>
														<td>{{central.bairro}}</td>
														<td>{{central.cidade}}</td>
														<td>{{central.uf}}</td>
														<td style="text-align: right;">
															<span ng-repeat="item in eventos"><i class="fas {{item.icon}}" style="cursor: pointer;" onmouseenter="$(this).tooltip('show')" data-toggle="tooltip" data-placement="top" title="{{item.nome}}"  tooltip ng-if="checkIfContains(item, central)" ng-click="openLog(item, central)"></i></span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="tab-pane fade" id="tab2primary">
										<div class="embed-responsive embed-responsive-16by9">
		  									<iframe id="frame-map-eventos" name="frame-map-eventos" class="embed-responsive-item" src="https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/mapsCM.jsp?action=DASHBOARD&grupoCliente=292"></iframe>
										</div>
									</div>
									<div class="tab-pane fade" id="tab3primary">
										<div class="embed-responsive embed-responsive-16by9">
		  									<iframe id="frame-map-os" name="frame-map-os" class="embed-responsive-item" src="https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/mapsVtrMobile.jsp?action=DASHBOARD&grupoCliente=292"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="logModal" tabindex="-1" role="dialog" aria-labelledby="logModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="logModalLabel">Descri��o</h4>
		      </div>
		      <div class="modal-body">
		        {{currentLog}}
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
		
	<div class="splash-screen" ng-show="isLoading"><div class="loader"></div></div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>	
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  	<script src="https://kit.fontawesome.com/3c6868a037.js" crossorigin="anonymous"></script>
  	<script src="../../../../../custom/js/lib/angular.min.js"></script>
  	<script src="js/raiaCtrl.js"></script>
  	<script src="js/raiaService.js"></script>
  	<script src="js/ng-infinite-scroll.min.js"></script>
  	
</body>
</html>