<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.vo.DBOrdemVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.SigmaUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
	String regional = request.getParameter("regional");
%>

<html>
<body>
	<div style="width:99%; padding:2px;height:auto">
		<form id="fmAlertas" method="post">
			<table class="easyui-datagrid" id="dgHistorico" name="dgHistorico" title="Informações" style="width:100%;height:420px;font-family: Calibri,verdana,helvetica,arial,sans-serif;font-size:12px;z-index: 1100;"
            data-options="singleSelect: true,iconCls: 'icon-search',
            rowStyler: function(index,row){
                    if (row.emAtendimento){
                        return 'background-color:#FF0000;color:#fff;font-weight:bold;';
                    }
                }">
		        <thead>
		            <tr>
		                <th data-options="field:'posicao',width:20">Nº</th>
		                <th data-options="field:'ait',width:300">AIT</th>
		                <th data-options="field:'conta',width:60,align:'right'">Conta</th>
		                <th data-options="field:'fantasia',width:250,align:'right'">Fantasia</th>
		                <th data-options="field:'tempo',width:100,align:'center'">Tempo espera</th>
		                <th data-options="field:'tempoFila',width:100,align:'center'">Tempo em fila</th>
		                <th data-options="field:'log',width:30,align:'center'">Log</th>
		                <th data-options="field:'nomeAtendente',width:125,align:'center'">Usuário</th>
		            </tr>
		        </thead>
		    </table>		
		</form>	
	</div>	
	

</body>
<script type="text/javascript">
		
		var regional="<%=regional%>";
		$(document).ready(function ()
		{
			showFilaDeEventos();
		});
		
		function showFilaDeEventos()
	    {
	    	$('#dgHistorico').datagrid('loadData', new Array());
	               $.ajax({
	               	method: 'post',
	                   url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet2?action=getFilaEventos&regional='+regional,
	                   dataType: 'json',
	                   success: function(result){
	                   	$('#dgHistorico').datagrid('loadData', result);
	                    },  
	          			error: function(e){  
	         			}
	         		});
	      }
	</script>

</html>
