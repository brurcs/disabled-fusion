<%@page import="br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy"%>
<%@page import="br.com.segware.sigmaWebServices.webServices.EventoRecebido"%>
<%@page import="java.text.ParseException"%>
<%@page import="javax.swing.text.MaskFormatter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="org.apache.commons.mail.HtmlEmail"%>
<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.mail.MailSettings"%>
<%@page
	import="br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServiceProxy"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.net.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEventoViaturaRota");
	log.debug("##### INICIAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	String returnFromAccess = "";

	try
	{
		/**
		 * Com o uso do aplicativo VTR MOBILE, o trabalho do operador de deslocamento (CM) passou a
		 * ser a avalia��o
		 * e redistribui��o dos atendimentos (Evento x Viatura), conforme dist�ncia, idade e
		 * prioridade. Com isto,
		 * o papel de DESLOCAR passou a ser uma opera��o mec�nica. A a��o do operador � posterior ao
		 * deslocamento.
		 * Entretanto, como o SIGMA ainda n�o executa este papel automaticamente, o operador �
		 * onerado (em tempo)
		 * tendo que realizar este trabalho manualmente, para a viatura da rota do evento.
		 * O objetivo deste rob� � DESLOCAR um evento que esteja no status ESPERA, para a viatura da
		 * ROTA correspondente a sua conta.
		 * 
		 * Para tanto, o SIGMA disponibiliza um WEBSERVICE:Servi�o:
		 * http://www.segware.com.br/dev/SigmaWebService.html#Deslocar-Evento-WebService
		 * Ele espera dois par�metros:
		 * - C�digo do evento a ser deslocado
		 * - C�digo da Viatura a ser deslocada
		 */

		conn = PersistEngine.getConnection("SIGMA90");

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT h.NU_AUXILIAR, h.CD_HISTORICO, h.CD_EVENTO, REPLACE(SUBSTRING(r.NM_ROTA, CHARINDEX('AA', r.NM_ROTA), 4), '-', '') AS ROTA, SUBSTRING(r.NM_ROTA, 1, 3) AS REG, ");
		sql.append(" c.ID_CENTRAL, c.PARTICAO, c.RAZAO, c.FANTASIA, c.CD_CLIENTE, c.ID_EMPRESA, c.STSERVIDORCFTV  ");
		sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
		sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		sql.append(" INNER JOIN rota r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
		sql.append(" INNER JOIN dbCODE cuc WITH (NOLOCK) ON cuc.ID_CODE = h.CD_CODE ");
		sql.append(" WHERE h.FG_STATUS = 1 AND h.CD_HISTORICO_PAI IS NULL ");
		//sql.append(" AND h.CD_CLIENTE = 50931 ");
		sql.append(" AND r.NM_ROTA LIKE '%AA%' ");
		sql.append(" AND (cuc.TIPO = 1 OR h.CD_EVENTO LIKE 'XXX7') ");
		//sql.append("   AND (cuc.TIPO = 1 OR h.CD_EVENTO LIKE 'XXX7') ");
		sql.append(" AND h.CD_EVENTO NOT LIKE 'XXX8' ");
		//Temporario (Regionais que usam o VTR MOBILE) - Quando todas usarem, remover
		sql.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ('CTA', 'SOO', 'BQE', 'CUA', 'BNU','RSL','JGS','IAI','JLE','LGS','CCO','CSC') ");

		/////////////////// SOLICITA��O FEITA POR  Dirceu Henrique Borrajo Costa  - Gerente da Central de Monitoramento
		sql.append("  AND NOT EXISTS ( SELECT * FROM HISTORICO h WITH (NOLOCK)  ");
		sql.append("  INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		sql.append("  INNER JOIN rota r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA  ");
		sql.append("  INNER JOIN dbCODE cuc WITH (NOLOCK) ON cuc.ID_CODE = h.CD_CODE ");
		sql.append("  WHERE h.FG_STATUS = 1 AND h.CD_HISTORICO_PAI IS NULL ");
		sql.append("  AND r.NM_ROTA LIKE '%AA%'  ");
		sql.append("  AND c.ID_CENTRAL IN('8012','8002','8009','8004','8015','8018','8019','3186',  ");
		sql.append("  '5915','5371','5731','5735','0915','1261','7605','5849','7608','5876','7602','0916',  ");
		sql.append("  '7603','7604','7606','7607','5724','0434','8007','8008','8003','8013','8014','8010',  ");
		sql.append("  '8005','8011','8006','8016','8017','5283','5288','3916','5279','5280','5282','5285',  ");
		sql.append("  '5284','3918','0657','0659','0359','5286','0539','3917','0658','5290','5913','5293',  ");
		sql.append("  '5291','3913','3915','3920','0143','3912','5287','3949','8001','7601','7589') ");
		sql.append("   AND (cuc.TIPO = 1 OR h.CD_EVENTO LIKE 'XXX7') ");
		//sql.append("   AND (cuc.TIPO = 1 OR h.CD_EVENTO LIKE 'XXX7') ");
		sql.append("   AND h.CD_EVENTO NOT LIKE 'XXX8' ");
		sql.append("   AND SUBSTRING(r.NM_ROTA, 1, 3) IN ('CTA'))   ");
		////////////////////////////////////////////////////////////////////////////////////////		  

		sql.append(" ORDER BY h.DT_RECEBIDO ");

		pstm = conn.prepareStatement(sql.toString());
		rs = pstm.executeQuery();
		int contador = 0;
		while (rs.next())
		{
			String rota = rs.getString("ROTA");
			String regional = rs.getString("REG");
			String central = null;
			String particao = null;
			String razao = null;
			String fantasia = null;
			String retorno = null;
			

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLOpFilter("motorista", "LIKE", "" + regional + "%" + rota + "-%"));
			groupFilter.addFilter(new QLEqualsFilter("codigoRegional", regional));
			groupFilter.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
			List<NeoObject> listOTSViatura = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), groupFilter);

			central = rs.getString("ID_CENTRAL");
			particao = rs.getString("PARTICAO");
			razao = rs.getString("RAZAO");
			fantasia = rs.getString("FANTASIA");
			
			String evento = rs.getString("CD_EVENTO");
	        String cliente = rs.getString("CD_CLIENTE");
	        String empresa = rs.getString("ID_EMPRESA");
	        String stservcftv = rs.getString("STSERVIDORCFTV");
	        if(stservcftv.equals("1") && !verificaEventoHistorico(cliente) && !verificaEventoHistoricoView(cliente)){
	        	//returnFromAccess = executaServicoSegware(evento, cliente, empresa, central, particao);
	        }

			if (NeoUtils.safeIsNotNull(listOTSViatura) && listOTSViatura.size() == 1 && evento.equals("XXX7"))
			{
				NeoObject neoOts = listOTSViatura.get(0);
				EntityWrapper ewOts = new EntityWrapper(neoOts);
				String idViaturaStr = (String) ewOts.findField("codigoViatura").getValue();
				Long idEvento = (Long) rs.getLong("CD_HISTORICO");
				Long idViatura = Long.parseLong(idViaturaStr);
				returnFromAccess = executaServicoSegware(idViatura, idEvento);
				retorno = returnFromAccess;

			}
			else if (NeoUtils.safeIsNotNull(listOTSViatura) && (listOTSViatura.size() > 1 || listOTSViatura.isEmpty()) )
			{
				if (listOTSViatura.isEmpty())
					returnFromAccess = "<strong> N�o foram encontradas informa��es! </strong> <br> <strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";
				else if (listOTSViatura.size() > 1)
				{
					returnFromAccess += "<strong> Existem " + listOTSViatura.size() + " viaturas relacionadas favor verificar!. </strong> <br>";
					returnFromAccess += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";

					for (NeoObject neo : listOTSViatura)
					{
						EntityWrapper ew = new EntityWrapper(neo);
						returnFromAccess += "<strong> C�digo Viadura:  </strong>" + (String) ew.findField("codigoViatura").getValue() + " <br>";
						returnFromAccess += "<strong> C�digo Cliente:  </strong>" + (String) ew.findField("codigoCliente").getValue() + " <br>";
						returnFromAccess += "<strong> Regional - Rota  </strong>" + (String) ew.findField("codigoRegional").getValue() + " - " + rota + " <br>";
						returnFromAccess += "<strong> Placa: </strong>" + (String) ew.findField("placa").getValue() + " <br>";
						returnFromAccess += "<strong> T�tulo: </strong>" + (String) ew.findField("titulo").getValue() + " <br>";
						returnFromAccess += "<strong> Descri��o: </strong>" + (String) ew.findField("descricao").getValue() + " <br>";
						returnFromAccess += "<strong> Motorista: </strong>" + (String) ew.findField("motorista").getValue() + " <br>";
						String telefone = formatString((String) ew.findField("telefone").getValue(), "(##) ####-####");
						returnFromAccess += "<strong> Telefone:  </strong>" + telefone + " <br>";
						String utilizando = "N�o";
						Boolean emUso = (Boolean) ew.findField("emUso").getValue();
						if (emUso)
							utilizando = "Sim";
						returnFromAccess += "<strong> Em uso: </strong>" + utilizando + " <br>";
						returnFromAccess += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";

					}

				}

			}

			if (NeoUtils.safeIsNotNull(retorno) && !retorno.equals("ACK"))
			{
				log.debug("ERRO WEBSERVICE ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA: " + retorno);
				retorno = verificaRetornoWebService(retorno);
				returnFromAccess = "<strong> Erro: </strong>" + retorno + " <br>";
			}
			//else if ((NeoUtils.safeIsNotNull(listOTSViatura) && (listOTSViatura.size() > 1)) && (NeoUtils.safeIsNull(retorno) || !retorno.equals("ACK")))
			//{
				//enviaEmailRotina(central, particao, razao, fantasia, regional, rota, returnFromAccess);
			//}

			contador++;

		}
		log.debug("##### EXECUTAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - GERANDO UM TOTAL DE : " + contador + " EVENTOS EM ESPERA TRATADOS -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	}
	catch (Exception e)
	{
		e.printStackTrace();
		log.error("##### ERRO ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + e.getStackTrace() + " " + e.getMessage() + " " + e.getCause() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		//enviaEmailRotina(null, null, null, null, null, null, returnFromAccess);
	}
	finally
	{
		try
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		log.debug("##### FINALIZAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
%>


<%!private String verificaRetornoWebService(String retorno)
	{

		if (retorno.equals("VIATURA_NAO_ENCONTRADA"))
			retorno = "N�o � poss�vel identificar o ve�culo atrav�s do par�metro idRastreador.";
		else if (retorno.equals("EVENTO_NAO_ENCONTRADO"))
			retorno = "N�o � poss�vel identificar o evento atrav�s de par�metro idEvento.";
		else if (retorno.equals("CLIENTE_NAO_PERMITE_DESLOCAMENTO"))
			retorno = "O cliente pertence ao evento � configurado para n�o permitir deslocamentos carro.";
		else if (retorno.equals("CUC_NAO_PERMITE_DESLOCAMENTO"))
			retorno = "O CUC qual o evento pertence est� configurado para n�o permitir deslocamentos de carro.";
		else if (retorno.equals("CLIENTE_E_CUC_NAO_PERMITEM_DESLOCAMENTO"))
			retorno = "Tanto o cliente e o CUC que pertence ao evento s�o configurados para n�o permitir deslocamentos carro.";
		else if (retorno.equals("ERRO_ATUALIZAR_STATUS_EVENTO"))
			retorno = "Ocorreu um erro inesperado ao atualizar o status do evento";
		else if (retorno.equals("OPERACAO_ILEGAL"))
			retorno = "A mudan�a do status atual do evento para o estado entrou n�o � permitido. Um evento no estado - On Site - ou - servi�o feito - n�o pode ter seu status alterado para - Offset.";
		else if (retorno.equals("VIATURA_JA_DESLOCADA"))
			retorno = "O carro foi movido para outro evento. Esta mensagem s� ser� devolvido se o nas configura��es do desktop Sigma - Ativar compensado por um carro limita��o - est� habilitado.";
		else if (retorno.equals("DESLOCAMENTO_DE_EVENTO_FILHO_NAO_PERMITIDO"))
			retorno = "A mensagem retornada quando o agrupamento de eventos habilitado, eo evento a ser movido � um evento em cluster.";
		else if (retorno.equals("ACK"))
			retorno = "Opera��o bem-sucedida.";
		else
			retorno = "WebServiceSigma n�o retornou uma mensagem v�lida!. Mensagem de retorno: " + retorno;

		return retorno;
	}%>
<%!private String executaServicoSegware(Long idViatura, Long idEvento)
	{
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEventoViaturaRota");
		String returnFromAccess = "";
		try
		{
			DeslocarEventoWebServiceProxy webServiceProxy = new DeslocarEventoWebServiceProxy();

			returnFromAccess = webServiceProxy.deslocarEvento(idEvento, null, idViatura);

			log.debug("##### ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA EXECUTADA COM SUCESSO - RETORNOU " + returnFromAccess + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
			log.error("##### ERRO AO EXECUTAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - RETORNOU " + returnFromAccess + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		finally
		{
			return returnFromAccess;
		}
	}%>
<%!private void enviaEmailRotina(String central, String particao, String razao, String fantasia, String regional, String rota, String returnFromAccess)
	{
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEventoViaturaRota");
		try
		{

			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			mailClone.setFromEMail("nao_responda@orsegups.com.br");

			mailClone.setFromName("Orsegups Partipa��es S.A.");
			if (mailClone != null && mailClone.isEnabled())
			{

				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();

				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) <= 12)
				{
					saudacaoEMail = "Bom dia, ";
				}
				else if (saudacao.get(Calendar.HOUR_OF_DAY) > 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
				{
					saudacaoEMail = "Boa tarde, ";
				}
				else
				{
					saudacaoEMail = "Boa noite, ";
				}

				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("cm@orsegups.com.br");
				noUserEmail.addTo("emailautomatico@orsegups.com.br");

				noUserEmail.setSubject(" N�o Responda - Rotina deslocamento evento em espera para viatura respons�vel rota. ");

				noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				// 				noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				// 				noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				// 				noUserMsg.append("            </td>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail + "</strong><br>");
				noUserMsg.append("			<strong> Erro no deslocamento autom�tico:</strong><br>");
				if (NeoUtils.safeIsNotNull(central) && NeoUtils.safeIsNotNull(particao))
					noUserMsg.append("			<strong>Conta: </strong> " + central + "[" + particao + "]" + " <br>");
				if (NeoUtils.safeIsNotNull(razao))
					noUserMsg.append("			<strong>Raz�o: </strong> " + razao + " (" + fantasia + ") <br>");
				if (NeoUtils.safeIsNotNull(regional))
					noUserMsg.append("			<strong>Regional: </strong> " + regional + " <br>");
				if (NeoUtils.safeIsNotNull(rota))
					noUserMsg.append("			<strong>Rota: </strong> " + rota + " <br>");
				if (NeoUtils.safeIsNull(central) && NeoUtils.safeIsNull(particao))
					noUserMsg.append("			<strong>Erro: </strong> Por favor, verificar erro. <br>");
				if (NeoUtils.safeIsNotNull(returnFromAccess) && !returnFromAccess.isEmpty())
					noUserMsg.append(returnFromAccess.toString());
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");
				noUserEmail.setHtmlMsg(noUserMsg.toString());
				noUserEmail.setContent(noUserMsg.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO ENVIAR EMAIL ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}

	}%>

<%!public static String formatString(String value, String pattern)
	{
		MaskFormatter mf;
		try
		{
			mf = new MaskFormatter(pattern);
			mf.setValueContainsLiteralCharacters(false);
			return mf.valueToString(value);
		}
		catch (ParseException ex)
		{
			return value;
		}
	}%>
	
<%!
	private String executaServicoSegware(String evento, String cliente, String empresa, String idCentral, String particao)
	{
 		String returnFromAccess = "";
 		String eventoAux = "XVID";
  		try
 		 {
   		
    	EventoRecebido eventoRecebido = new EventoRecebido();
    
    	eventoRecebido.setCodigo(eventoAux);
    	eventoRecebido.setData(new GregorianCalendar());
    	eventoRecebido.setEmpresa(Long.parseLong(empresa));
    	eventoRecebido.setIdCentral(idCentral);
   	    eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
   		eventoRecebido.setParticao(particao);
    	eventoRecebido.setProtocolo(Byte.parseByte("2"));
    
    	ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();
    
   		 returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
  }
 	 catch (Exception e)
  {
    e.printStackTrace();
    returnFromAccess = e.getMessage();
  }
  	finally
  {
    return returnFromAccess;
  }
}
%>

<%!
private Boolean verificaEventoHistorico(String cdCliente)
{
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	Boolean flag = Boolean.FALSE;
	try
	{
		conn = PersistEngine.getConnection("SIGMA90");
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT H.CD_HISTORICO FROM HISTORICO H WITH (NOLOCK)  WHERE H.CD_CLIENTE = ? ");
		sql.append(" AND  h.FG_STATUS IN (0) AND H.CD_EVENTO LIKE 'XVID' ");
		st = conn.prepareStatement(sql.toString());
		st.setString(1, cdCliente);
		rs = st.executeQuery();

		if (rs.next())
			flag = Boolean.TRUE;

	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		OrsegupsUtils.closeConnection(conn, st, rs);
		return flag;
	}

}


%>

<%!
private Boolean verificaEventoHistoricoView(String cdCliente)
{
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	Boolean flag = Boolean.FALSE;
	try
	{
		conn = PersistEngine.getConnection("SIGMA90");
		StringBuilder sql = new StringBuilder();
		sql.append("  SELECT TOP(1)V.CD_EVENTO,* FROM VIEW_HISTORICO V WITH (NOLOCK) WHERE V.CD_CLIENTE = ?  ORDER BY DT_RECEBIDO DESC ");
		st = conn.prepareStatement(sql.toString());
		st.setString(1, cdCliente);
		
		rs = st.executeQuery();

		while (rs.next()){
		System.out.println("ROTINA DESLOCAMENTO AUTOMATICO VIEW COD EVENTO : ");
			if(rs.getString("CD_EVENTO").contains("XVID"))
			flag = Boolean.TRUE;
		}

	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		OrsegupsUtils.closeConnection(conn, st, rs);
		return flag;
	}

}

%>

 
