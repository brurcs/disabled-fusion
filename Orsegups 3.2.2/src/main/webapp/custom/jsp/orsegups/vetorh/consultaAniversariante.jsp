<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.regex.*"%>
<%@page import="java.util.List"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>

<%
	GregorianCalendar gc = new GregorianCalendar();
	Date dataHoje = gc.getTime();
	gc.setTime(dataHoje);

	java.lang.StringBuilder str = new StringBuilder();
	
	QLRawFilter gAct = new QLRawFilter(" month(bornDate) = " + String.valueOf(gc.get(Calendar.MONTH) + 1) + " and day(bornDate) = " + String.valueOf(gc.get(Calendar.DAY_OF_MONTH)) + " and active = 1");
	List<NeoUser> users = PersistEngine.getObjects(NeoUser.class, gAct);

	if (users != null && !users.isEmpty())
	{
		for (NeoUser auxUser : users)
		{
			EntityWrapper auxUser2 = new EntityWrapper(auxUser);
			String email = (String) auxUser2.findValue("email").toString();

			Pattern padrao = Pattern.compile(".+@.+\\.[a-z]+");
			Matcher m = padrao.matcher(email);

			Boolean flag = m.matches();
			if (flag)
			{
				str.append(auxUser2.findValue("email") + ";");
			}

		}
		if (str.length() == 0)
		{
			out.print("null;");
			return;
		}

		out.print(str.toString());
		return;
	}
	else
	{
		out.print("null;");
		return;
	}
%>
