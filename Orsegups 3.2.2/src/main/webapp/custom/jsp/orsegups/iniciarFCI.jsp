<%@page import="com.neomind.fusion.security.NeoRole"%>
<%@page import="com.neomind.fusion.security.NeoGroup"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper"%>
<%@page import="com.neomind.fusion.workflow.simulation.WorkflowService"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="javax.persistence.Query"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="java.lang.Object"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>


<body>
	<%
		// Definicoes Gerais
		String nomeFonteDados = "SAPIENS";
		String op = request.getParameter("op");
		if (op == null || op.trim().equals(""))
		{
			out.print("Erro #1 - Opera��o inv�lida (#" + request.getParameter("op") + ")");
			return;
		}

		String pk = request.getParameter("pk");
		if (pk == null)
		{
			out.print("Erro #2 - Erro ao gerar contrato chave do Sapiens (#" + request.getParameter("pk") + ")");
			return;
		}

		NeoObject contrato = null;
		ExternalEntityInfo eContrato = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("VFUSCTRSAP");
		List<NeoObject> listacontrato = (List<NeoObject>) PersistEngine.getObjects(eContrato.getEntityClass(), new QLEqualsFilter("pk", pk));
		if (listacontrato != null && listacontrato.size() > 0)
		{
			contrato = listacontrato.get(0);

		}
		else
		{
			out.print("Erro #3 - Erro ao localizar o contrato do Sapiens pela chave.");
			return;
		}

		if (op.equals("query"))
		{
			InstantiableEntityInfo eCancEletIna = AdapterUtils.getInstantiableEntityInfo("FCICobrancaInadimplencia");

			List<NeoObject> listaRegCanc = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna.getEntityClass(), new QLEqualsFilter("contratoSapiens", contrato));
			String result = "NA";
			if (listaRegCanc != null && listaRegCanc.size() > 0)
			{
				for (NeoObject noRegCanc : listaRegCanc)
				{
					EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
					WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();
					if (wfpCanc != null && wfpCanc.getProcessState().toString().equals("running"))
					{
						result = wfpCanc.getCode();
					}
				}
			}
			out.print(result);
		}
		else if (op.equals("queryDetails"))
		{
			//verifica se existe tarefa para o processo C026
			InstantiableEntityInfo eCancEletIna = AdapterUtils.getInstantiableEntityInfo("FCICobrancaInadimplencia");

			List<NeoObject> listaRegCanc = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna.getEntityClass(), new QLEqualsFilter("contratoSapiens", contrato));
			String result = "NA";
			if (listaRegCanc != null && listaRegCanc.size() > 0)
			{
				for (NeoObject noRegCanc : listaRegCanc)
				{
					EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
					WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();
					if (wfpCanc != null && wfpCanc.getProcessState().toString().equals("running"))
					{

						// filtro para pegar as atividades
						QLGroupFilter gAct = new QLGroupFilter("AND");
						gAct.addFilter(new QLFilterIsNotNull("startDate"));
						gAct.addFilter(new QLFilterIsNull("finishDate"));
						gAct.addFilter(new QLEqualsFilter("process", wfpCanc));

						// atividades em aberto
						Collection<Activity> colAct = PersistEngine.getObjects(Activity.class, gAct);
						result = wfpCanc.getCode() + "Tarefas/Atividades em aberto: <strong>" + colAct.size() + "</strong><br>";

						// tarefas em aberto
						for (Activity a : colAct)
						{
							QLGroupFilter gTask = new QLGroupFilter("AND");
							gTask.addFilter(new QLFilterIsNull("finishDate"));
							gTask.addFilter(new QLFilterIsNotNull("startDate"));
							gTask.addFilter(new QLEqualsFilter("activity", a));

							Task task = (Task) PersistEngine.getObject(Task.class, gTask);

							// pode estar pool, dai nao tem Task ainda
							if (task != null)
							{
								result = result + "Processo: C026 Tarefa: <strong>" + task.getActivityName() + "</strong> para o usu�rio: <strong>" + task.getUser().getFullName() + "</strong><br>";
							}
// 							else
// 							{
// 								if (a instanceof UserActivity)
// 								{
// 									result = result + "Processo: C026 Atividade: <strong>" + a.getActivityName() + "</strong> em POOL para: <strong>";
// 									int i = 1;
// 									for (SecurityEntity sec : a.getInstance().getPotentialOwners())
// 									{
// 										if (a.getInstance() != null)
// 										{
// 											if (sec instanceof NeoUser)
// 											{
// 												result = result + ", " + ((NeoUser) sec).getFullName();
// 											}
// 											else if (sec instanceof NeoPaper)
// 											{
// 												result = result + ", " + ((NeoPaper) sec).getName();
// 											}
// 											else if (sec instanceof NeoGroup)
// 											{
// 												result = result + ", " + ((NeoGroup) sec).getName();
// 											}
// 											else if (sec instanceof NeoRole)
// 											{
// 												result = result + ", " + ((NeoRole) sec).getName();
// 											}

// 											if (a.getInstance().getPotentialOwners().size() > i)
// 											{
// 												result = result + ", " + ", ";
// 											}
// 											i++;
// 										}
// 									}
// 								}
// 								result = result + "</strong><br>";
// 							}
						}
					}
				}
			}

			//Verifica se existe processo C026
			InstantiableEntityInfo eCancEletIna2 = AdapterUtils.getInstantiableEntityInfo("FCNcancelamentoEletInadimplenciaNew");

			List<NeoObject> listaRegCanc2 = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna2.getEntityClass(), new QLEqualsFilter("contratoSapiens", contrato));
			if (listaRegCanc2 != null && listaRegCanc2.size() > 0)
			{
				for (NeoObject noRegCanc : listaRegCanc2)
				{
					EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
					WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();
					if (wfpCanc != null && wfpCanc.getProcessState().toString().equals("running"))
					{

						// filtro para pegar as atividades
						QLGroupFilter gAct = new QLGroupFilter("AND");
						gAct.addFilter(new QLFilterIsNotNull("startDate"));
						gAct.addFilter(new QLEqualsFilter("process", wfpCanc));

						// atividades em aberto
						Collection<Activity> colAct = PersistEngine.getObjects(Activity.class, gAct);
						result = wfpCanc.getCode() + "Tarefas/Atividades em aberto: <strong>" + colAct.size() + "</strong><br>";

						// tarefas em aberto
						for (Activity a : colAct)
						{
							QLGroupFilter gTask = new QLGroupFilter("AND");
							gTask.addFilter(new QLFilterIsNull("finishDate"));
							gTask.addFilter(new QLFilterIsNotNull("startDate"));
							gTask.addFilter(new QLEqualsFilter("activity", a));

							Task task = (Task) PersistEngine.getObject(Task.class, gTask);

							// pode estar pool, dai nao tem Task ainda
							if (task != null)
							{
								result = result + "Processo: C025 Tarefa: <strong>" + task.getActivityName() + "</strong> para o usu�rio: <strong>" + task.getUser().getFullName() + "</strong><br>";
							}
							else
							{
								if (a instanceof UserActivity)
								{
									result = result + "Processo: C025 Atividade: <strong>" + a.getActivityName() + "</strong> em POOL para: <strong>";
									int i = 0;
									if (a.getInstance() != null)
									{
										for (SecurityEntity sec : a.getInstance().getPotentialOwners())
										{
											if (sec instanceof NeoUser)
											{
												result = result + ((NeoUser) sec).getFullName();
											}
											else if (sec instanceof NeoPaper)
											{
												result = result + ((NeoPaper) sec).getName();
											}
											else if (sec instanceof NeoGroup)
											{
												result = result + ((NeoGroup) sec).getName();
											}
											else if (sec instanceof NeoRole)
											{
												result = result + ((NeoRole) sec).getName();
											}

											if (a.getInstance().getPotentialOwners().size() > i)
											{
												result = result + ", ";
											}
											i++;
										}
									}
								}
								result = result + "</strong><br>";
							}
						}
					}
				}
			}

			out.print(result);

		}
		else if (op.equals("start"))
		{
			// Buscando login do Fusion do Representante do post mais recente do contrato
			Long indice = NeoUtils.safeLong(request.getParameter("indice"));

			EntityWrapper erContrato = new EntityWrapper(contrato);
			Long codEmp = (Long) erContrato.findValue("usu_codemp");
			Long codFil = (Long) erContrato.findValue("usu_codfil");
			Long numCtr = (Long) erContrato.findValue("usu_numctr");

			QLGroupFilter filterCVS = new QLGroupFilter("AND");
			filterCVS.addFilter(new QLEqualsFilter("usu_codemp", codEmp));
			filterCVS.addFilter(new QLEqualsFilter("usu_codfil", codFil));
			filterCVS.addFilter(new QLEqualsFilter("usu_numctr", numCtr));
			String loginFusionRepresentante = null;

			List<NeoObject> listaCVS = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTCVS"), filterCVS, 0, 1, " usu_sitcvs, usu_numpos DESC ");
			if (listaCVS != null && listaCVS.size() > 0)
			{
				EntityWrapper erCVS = new EntityWrapper(listaCVS.get(0));
				Long codRep = (Long) erCVS.findValue("usu_codrep");

				List<NeoObject> listaRep = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEREP"), new QLEqualsFilter("codrep", codRep));
				if (listaRep != null && listaRep.size() > 0)
				{
					EntityWrapper erRep = new EntityWrapper(listaRep.get(0));
					loginFusionRepresentante = NeoUtils.safeTrim((String) erRep.findValue("usu_usufus"));
				}
			}
			NeoUser userRepresentante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", loginFusionRepresentante));
			Boolean isEnviaExecutivo = userRepresentante != null;

			// solicitante
			NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("solicitante")));
			if (solicitante == null)
			{
				out.print("Erro #4 - Solicitante n�o encontrado (#" + request.getParameter("solicitante") + ")");
				return;
			}

			EntityWrapper contratoSapiensWrapper = new EntityWrapper(contrato);
			String codemp = contratoSapiensWrapper.getValue("usu_codemp").toString();
			String codfil = contratoSapiensWrapper.getValue("usu_codfil").toString();
			String numctr = contratoSapiensWrapper.getValue("usu_numctr").toString();

			String sql = "	SELECT E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT 			" + "  FROM E301TCR, E001TNS, E140NFV, E140ISV   										" + "  WHERE E140ISV.CODEMP = E140NFV.CODEMP     										" + "  AND E140ISV.CODFIL = E140NFV.CODFIL   										    " + "  AND E140ISV.CODSNF = E140NFV.CODSNF                                             " + "  AND E140ISV.NUMNFV = E140NFV.NUMNFV                                             "
					+ "  AND E140NFV.CODEMP = E301TCR.CODEMP                                             " + "  AND E140NFV.CODFIL = E301TCR.CODFIL                                             " + "  AND E140NFV.CODSNF = E301TCR.CODSNF                                             " + "  AND E140NFV.NUMNFV = E301TCR.NUMNFV                                             " + "  AND E001TNS.CODEMP = E301TCR.CODEMP                                             "
					+ "  AND E001TNS.CODTNS = E301TCR.CODTNS                                             " + "  AND E001TNS.LISMOD = 'CRE'                                                      " + "  AND E301TCR.VLRABE > 0                                                          " + "  AND E301TCR.VCTPRO <= (GETDATE() - 4)                                              " + "  AND ((E301TCR.SITTIT >= 'AA' AND E301TCR.SITTIT <= 'AV') OR (E301TCR.SITTIT = 'CE')) " + "  AND E140ISV.CodEmp = "
					+ codemp + "                                                         " + "  AND E140ISV.CodFil = " + codfil + "                                                          " + "  AND E140ISV.NumCtr = " + numctr + "                                                     " + "  GROUP BY E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.NUMTIT, E301TCR.CODTPT         " + "  ORDER BY E301TCR.NUMTIT                                                         ";

			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql);
			@SuppressWarnings("unchecked")
			Collection<Object> resultList = query.getResultList();

			// Lista de T�tulos
			Collection<NeoObject> titulosCollection = new ArrayList<NeoObject>();

			if (resultList != null)
			{
				for (Object resultSet : resultList)
				{
					if (resultSet != null)
					{
						Object[] result = (Object[]) resultSet;
						QLGroupFilter filterTituloSapiens = new QLGroupFilter("AND");
						filterTituloSapiens.addFilter(new QLEqualsFilter("codemp", (Short) result[0]));
						filterTituloSapiens.addFilter(new QLEqualsFilter("codfil", (Short) result[1]));
						filterTituloSapiens.addFilter(new QLEqualsFilter("codtpt", (String) result[2]));
						filterTituloSapiens.addFilter(new QLEqualsFilter("numtit", (String) result[3]));
						List<NeoObject> listaNoTituloSapiens = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSETCR")).getEntityClass(), filterTituloSapiens);
						if (listaNoTituloSapiens != null && listaNoTituloSapiens.size() > 0)
						{
							NeoObject noTituloSapiens = listaNoTituloSapiens.get(0);
							if (noTituloSapiens != null)
							{
								NeoObject noTitulo = AdapterUtils.createNewEntityInstance("FCITituloSapiens");
								EntityWrapper tituloFusionWrapper = new EntityWrapper(noTitulo);
								EntityWrapper tituloSapiensWrapper = new EntityWrapper(noTituloSapiens);
								tituloFusionWrapper.setValue("empresa", tituloSapiensWrapper.getValue("codemp"));
								tituloFusionWrapper.setValue("filial", tituloSapiensWrapper.getValue("codfil"));
								tituloFusionWrapper.setValue("numeroTitulo", tituloSapiensWrapper.getValue("numtit"));
								tituloFusionWrapper.setValue("tipoTitulo", tituloSapiensWrapper.getValue("codtpt"));
								tituloFusionWrapper.setValue("emissao", tituloSapiensWrapper.getValue("datemi"));
								tituloFusionWrapper.setValue("vencimentoOriginal", tituloSapiensWrapper.getValue("vctori"));
								tituloFusionWrapper.setValue("vctoProrrogado", tituloSapiensWrapper.getValue("vctpro"));
								tituloFusionWrapper.setValue("valorOriginal", tituloSapiensWrapper.getValue("vlrori"));
								tituloFusionWrapper.setValue("valorAberto", tituloSapiensWrapper.getValue("vlrabe"));
								Collection<NeoObject> listaObs = (Collection<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSEMOR")).getEntityClass(), filterTituloSapiens, -1, -1, " seqmov ASC");
								tituloFusionWrapper.setValue("listaObservacoes", listaObs);
								PersistEngine.persist(noTitulo);
								titulosCollection.add(noTitulo);
							}
						}
					}
				}
			}
			else
			{
				out.print("Erro #10 - Nenhum t�tulo localizado.");
				return;
			}

			final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("Name", "C026 - FCI - Cobran�a Inadimpl�ncia"));
			if (pm == null)
			{
				out.print("Erro #6 - Erro ao recuperar modelo do processo ");
				return;
			}

			/**
			 * @author orsegups lucas.avila - Alterado para utilizar a nova arquitetura de processos do Fusion
			 * @date 25/05/2015
			 */

			WFProcess processo = WorkflowService.startProcess(pm, false, solicitante);
			processo.setSaved(true);
			NeoObject wkfFCC = processo.getEntity();
			EntityWrapper ewWkfFCC = new EntityWrapper(wkfFCC);

			ewWkfFCC.findField("contratoSapiens").setValue(contrato);
			ewWkfFCC.findField("listaTitulos").setValue(titulosCollection);
			ewWkfFCC.findField("enviaExecutivo").setValue(isEnviaExecutivo);
			ewWkfFCC.findField("nomeExecutivo").setValue(userRepresentante);
			ewWkfFCC.findField("indice").setValue(indice);

			try
			{
				new OrsegupsWorkflowHelper(processo).avancaPrimeiraTarefa(solicitante);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				out.print("Erro #4 - Erro ao avan�ar a primeira tarefa");
				return;
			}

			out.print(processo.getCode());

			/**
			 * FIM ALTERA��ES - ORSEGUPS
			 */

		}
		else
		{
			out.print("Erro #9 - Opera��o inv�lida");
			return;
		}
	%>
</body>
