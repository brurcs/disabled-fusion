<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.engine.runtime.RuntimeEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cancela Processo</title>
</head>
<body>
<%
		String codigoProcesso = request.getParameter("codigoProcesso");
		String motivoCancelamento = request.getParameter("motivoCancelamento");
		
		/**
		 * FIXME Cancelar tarefas.
		 */
		WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", NeoUtils.safeLong(codigoProcesso)));
		
		if (proc != null)
		{
			RuntimeEngine.getProcessService().cancel(proc, motivoCancelamento);
		}
		
		out.print("OK");
%>
</body>
</html>