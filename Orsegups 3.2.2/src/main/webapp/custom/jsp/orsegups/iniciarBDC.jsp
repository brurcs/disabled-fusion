<%@page import="com.neomind.fusion.workflow.model.ProcessModelProxy"%>
<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.fusion.workflow.simulation.WorkflowService"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="java.io.File"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.StringTokenizer"%>
<body><%
		String op = request.getParameter("op");
		if (op == null || op.trim().equals(""))
		{
			out.print("Erro #1 - Opera��o inv�lida (#" + request.getParameter("op") + ")");
			return;
		}

		/******* Usu�rio Solictante ********/
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("NomUsu")));
		if (solicitante == null)
		{
			out.print("Erro #2 - Solicitante n�o encontrado (#" + request.getParameter("NomUsu") + ")");
			return;
		}
		ProcessModelProxy px = (ProcessModelProxy) PersistEngine.getObject(ProcessModelProxy.class, new QLEqualsFilter("name", "C023 - Solicita��o de Cancelamento de NF, Apontamentos e Descontos em NF"));
		ProcessModel pm = px.getVersionControl().getCurrent();
		if(pm == null)
		{
			out.print("Erro #6 - Erro ao recuperar modelo do processo ");
			return;
		}


		/**
		 * @author orsegups lucas.avila - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 15/12/2015
		 */
		
		//Cria Instancia do Eform Principal
		InstantiableEntityInfo infoTarefa = AdapterUtils.getInstantiableEntityInfo("ProcessoSolicitacaoCancelamentoNFBonificacao");
		NeoObject noTarefa = infoTarefa.createNewInstance();
		EntityWrapper tarefaWrapper = new EntityWrapper(noTarefa);

		tarefaWrapper.findField("dataSolicitacao").setValue(new GregorianCalendar());
		tarefaWrapper.findField("solicitante").setValue(solicitante);

		//	String motivo = request.getParameter("motivo");
		//	if(motivo == null || motivo.trim().equals(""))	{
		//		out.print("Erro #4 - Motivo em branco (#"+request.getParameter("motivo")+")");
		//		return;
		//	}
		//	ewWkfBDC.findField("motivoSolicitacao").setValue(motivo);

		/******* Tipo de Solicitacao 1=Bonificacao, 2=Desconto, 3=Cancelamento NF, 4=Cancelamento NF c/ Desconto ********/
		InstantiableEntityInfo infoTipoSolicitacao = AdapterUtils.getInstantiableEntityInfo("tipoSolicitacaoComercial");
		NeoObject tipoSolicitacao = (NeoObject) PersistEngine.getObject(infoTipoSolicitacao.getEntityClass(), new QLEqualsFilter("codigo", NeoUtils.safeLong(op)));
		if (tipoSolicitacao == null)
		{
			out.print("Erro #5 - Tipo de Solicita��o Comercial n�o encontrado (c�digo #" + op + ")");
			return;
		}
		tarefaWrapper.findField("tipoSolicitacao").setValue(tipoSolicitacao);

		/******* Empresa ********/
		String CodEmp = request.getParameter("CodEmp");
		ExternalEntityInfo infoEmpresa = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("EEMP");
		NeoObject empresa = (NeoObject) PersistEngine.getObject(infoEmpresa.getEntityClass(), new QLEqualsFilter("codemp", NeoUtils.safeLong(CodEmp)));
		if (empresa == null)
		{
			out.print("Erro #6 - Empresa n�o encontrada (c�digo #" + CodEmp + ")");
			return;
		}
		tarefaWrapper.findField("empresa").setValue(empresa);

		String tituloExtra;
		/***************************************************************/
		/********* BONIFICACAO, DESCONTO, APONTAMENTO e MEDI��O ********/
		/***************************************************************/
		try
		{
			if (op.equals("1") || op.equals("2") || op.equals("3") || op.equals("5") || op.equals("8"))
			{

				/******* Contrato ********/
				String CodFil = request.getParameter("CodFil");
				String NumCtr = request.getParameter("NumCtr");
				ExternalEntityInfo infoContrato = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("VFUSCTRSAP");
				QLGroupFilter gfCtr = new QLGroupFilter("AND");
				gfCtr.addFilter(new QLEqualsFilter("usu_codemp", NeoUtils.safeLong(CodEmp)));
				gfCtr.addFilter(new QLEqualsFilter("usu_codfil", NeoUtils.safeLong(CodFil)));
				gfCtr.addFilter(new QLEqualsFilter("usu_numctr", NeoUtils.safeLong(NumCtr)));
				List<NeoObject> listaContrato = (List<NeoObject>) PersistEngine.getObjects(infoContrato.getEntityClass(), gfCtr);
				NeoObject contrato = null;
				if (listaContrato != null && listaContrato.size() > 0)
				{
					contrato = listaContrato.get(0);
				}
				else
				{
					out.print("Erro #7 - Erro ao localizar o contrato do Sapiens pelo filtro ( #" + CodEmp + "|" + CodFil + "|" + NumCtr + ")");
					return;
				}
				tarefaWrapper.findField("contrato").setValue(contrato);
				EntityWrapper ewContrato = new EntityWrapper(contrato);
				tituloExtra = "Dia Base: " + ((Long) ewContrato.findField("usu_diabas").getValue()).toString();
				//		out.print(((Long)ewContrato.findField("usu_diabas").getValue()).toString());

				/******* Compet�ncias ********/
				String SeqCms = request.getParameter("SeqCms");
				StringTokenizer st = new StringTokenizer(SeqCms, ",");
				List<NeoObject> listaCpt = new ArrayList();
				ExternalEntityInfo infoCpt = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("SAPIENSUSUTCMS");
				while (st.hasMoreTokens())
				{
					String token = st.nextToken();
					QLGroupFilter filtroCms = new QLGroupFilter("AND");
					filtroCms.addFilter(new QLEqualsFilter("usu_codemp", NeoUtils.safeLong(CodEmp)));
					filtroCms.addFilter(new QLEqualsFilter("usu_codfil", NeoUtils.safeLong(CodFil)));
					filtroCms.addFilter(new QLEqualsFilter("usu_numctr", NeoUtils.safeLong(NumCtr)));
					filtroCms.addFilter(new QLEqualsFilter("usu_seqmov", NeoUtils.safeLong(token)));
					
					List<NeoObject> listaTemp = (List<NeoObject>) PersistEngine.getObjects(infoCpt.getEntityClass(), filtroCms);
					if (listaTemp != null && listaTemp.size() > 0)
					{
						EntityWrapper ewTemp = new EntityWrapper(listaTemp.get(0));
						//				out.print(ewTemp.findField("usu_numpos").getValue() + "|" + token + "<br>");
						listaCpt.add(ewTemp.getObject());
					}
					else
					{
						out.print("Erro #13 - N�o foi poss�vel localizar a compet�ncia informada ( " + token + " ).");
						return;
					}
				}
				tarefaWrapper.findField("movimentos").setValue(listaCpt);

				/******* Medi��es ********/
				String arquivo = request.getParameter("Arquivo");
				if (arquivo != null)
				{
					String diretorio = "\\\\FSOOFS01\\Sapiens\\Relatorios\\Mercado\\";
					File file = new File(diretorio + arquivo);
					NeoFile newFile = new NeoFile();
					newFile.setName(file.getName());
					newFile.setStorage(NeoStorage.getDefault());
					PersistEngine.persist(newFile);
					OutputStream outi = newFile.getOutputStream();
					InputStream in = null;
					try
					{
						in = new BufferedInputStream(new FileInputStream(file));
						NeoStorage.copy(in, outi);
						newFile.setOrigin(NeoFile.Origin.TEMPLATE);
					}
					catch (FileNotFoundException e)
					{
						e.printStackTrace();
						out.print("Relat�rio n�o encontrado: " + diretorio + arquivo);
						return;
					}
					tarefaWrapper.findField("arquivo").setValue(newFile);
				}

				/*************************************/
				/******* CNC + DSC e CNC + APO *******/
				/*************************************/
			}
			else if (op.equals("4") || op.equals("6"))
			{
				System.out.println("Regra BDC 1: ");
				/******* Contrato ********/
				String CodFil = request.getParameter("CodFil");
				String NumCtr = request.getParameter("NumCtr");
				ExternalEntityInfo infoContrato = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("VFUSCTRSAP");
				QLGroupFilter gfCtr = new QLGroupFilter("AND");
				gfCtr.addFilter(new QLEqualsFilter("usu_codemp", NeoUtils.safeLong(CodEmp)));
				gfCtr.addFilter(new QLEqualsFilter("usu_codfil", NeoUtils.safeLong(CodFil)));
				gfCtr.addFilter(new QLEqualsFilter("usu_numctr", NeoUtils.safeLong(NumCtr)));
				List<NeoObject> listaContrato = (List<NeoObject>) PersistEngine.getObjects(infoContrato.getEntityClass(), gfCtr);
				System.out.println("Regra BDC 2: ");
				NeoObject contrato = null;
				if (listaContrato != null && listaContrato.size() > 0)
				{
					contrato = listaContrato.get(0);
					System.out.println("Regra BDC 3: ");
				}
				else
				{
					out.print("Erro #7 - Erro ao localizar o contrato do Sapiens pelo filtro ( #" + CodEmp + "|" + CodFil + "|" + NumCtr + ")");
					System.out.println("Regra BDC 4: ");
					return;
				}
				tarefaWrapper.findField("contrato").setValue(contrato);
				EntityWrapper ewContrato = new EntityWrapper(contrato);
				tituloExtra = "Dia Base: " + ((Long) ewContrato.findField("usu_diabas").getValue()).toString();
				//		out.print(((Long)ewContrato.findField("usu_diabas").getValue()).toString());

				/******* Nota Fiscal ********/
				String NumNfv = request.getParameter("NumNfv");
				String CodSnf = request.getParameter("CodSnf");
				if (NumNfv == null || NumNfv.trim().equals(""))
				{
					out.print("Erro #11 - N�mero de Nota Fiscal inv�lido (#" + request.getParameter("NumNfv") + ")");
					System.out.println("Regra BDC 5: ");
					return;
				}
				if (CodSnf == null || CodSnf.trim().equals(""))
				{
					out.print("Erro #12 - C�digo de S�rie de Nota Fiscal inv�lido (#" + request.getParameter("CodSnf") + ")");
					System.out.println("Regra BDC 6: ");
					return;
				}
				String auxReemiteNF = request.getParameter("reemiteNF");
				if (auxReemiteNF == null || auxReemiteNF.trim().equals(""))
				{
					auxReemiteNF = "N";
				}
				Boolean reemiteNF = auxReemiteNF.trim().equals("S");
				tarefaWrapper.findField("reemiteNF").setValue(reemiteNF);

				System.out.println("Regra BDC 7: ");
				ExternalEntityInfo infoNF = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("USUVNFVBDC");
				QLGroupFilter gfNF = new QLGroupFilter("AND");
				gfNF.addFilter(new QLEqualsFilter("codemp", NeoUtils.safeLong(CodEmp)));
				gfNF.addFilter(new QLEqualsFilter("codfil", NeoUtils.safeLong(CodFil)));
				gfNF.addFilter(new QLEqualsFilter("codsnf", CodSnf));
				gfNF.addFilter(new QLEqualsFilter("numnfv", NeoUtils.safeLong(NumNfv)));
				List<NeoObject> listaNF = (List<NeoObject>) PersistEngine.getObjects(infoNF.getEntityClass(), gfNF);
				System.out.println("Regra BDC 8: ");
				NeoObject nf = null;
				if (listaNF != null && listaNF.size() > 0)
				{
					nf = listaNF.get(0);
					System.out.println("Regra BDC 9: ");
				}
				else
				{
					out.print("Erro #10 - Erro ao localizar a Nota Fiscal do Sapiens pelo filtro ( #" + CodEmp + "|" + CodFil + "|" + CodSnf + "|" + NumNfv + ")");
					System.out.println("Regra BDC 10: ");
					return;
				}
				tarefaWrapper.findField("nfSapiens").setValue(nf);
				//		EntityWrapper ewNF = new EntityWrapper(nf);
				//		out.print(ewNF.findField("numnfv").getValue());

				/******* Compet�ncias ********/
				String SeqCms = request.getParameter("SeqCms");
				StringTokenizer st = new StringTokenizer(SeqCms, ",");
				List<NeoObject> listaCpt = new ArrayList();
				ExternalEntityInfo infoCpt = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("SAPIENSUSUTCMS");
				System.out.println("Regra BDC 11: ");
				while (st.hasMoreTokens())
				{
					String token = st.nextToken();
					List<NeoObject> listaTemp = (List<NeoObject>) PersistEngine.getObjects(infoCpt.getEntityClass(), new QLEqualsFilter("usu_seqmov", NeoUtils.safeLong(token)));
					System.out.println("Regra BDC 12: ");
					if (listaTemp != null && listaTemp.size() > 0)
					{
						EntityWrapper ewTemp = new EntityWrapper(listaTemp.get(0));
						//				out.print(ewTemp.findField("usu_numpos").getValue() + "|" + token + "<br>");
						listaCpt.add(ewTemp.getObject());
						System.out.println("Regra BDC 13: ");
					}
					else
					{
						out.print("Erro #13 - N�o foi poss�vel localizar a compet�ncia informada ( " + token + " ).");
						System.out.println("Regra BDC 14: ");
						return;
					}
				}
				tarefaWrapper.findField("movimentos").setValue(listaCpt);
			}
			else
			{
				out.print("Erro #9 - Opera��o inv�lida");
				return;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			out.print("Erro #14");
			System.out.println("Erro #14");
			return;
		}
		/******* Disparando processo ********/
		tarefaWrapper.findField("tituloExtra").setValue(tituloExtra);
		PersistEngine.persist(noTarefa);		
		
		WFProcess process = pm.startProcess(noTarefa , false, null);
		process.setSaved(true);
		 
		try
		{
			new OrsegupsWorkflowHelper(process).avancaPrimeiraAtividade(solicitante);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return;
		}

		/**
		 * FIM ALTERA��ES - NEOMIND
		 */

		out.print("OK:" + process.getCode());
	%>
</body>