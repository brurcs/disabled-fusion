<!DOCTYPE html>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.custom.orsegups.medicaoContratos.OrsegupsMedicaoUtilsMensal"%>
<%@page import="java.util.ArrayList"%>
<html>
	<head>
	<%@page import="com.neomind.fusion.portal.PortalUtil"%>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="css/estilos.css">
 	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
	<title>Acompanhamento Medi��o</title>
	</head>
	<body>
	<% 
	List<String> retorno = new ArrayList<String>();
	retorno = OrsegupsMedicaoUtilsMensal.empresas();
	
	List<String> listCom = new ArrayList<String>();
	listCom = OrsegupsMedicaoUtilsMensal.competencias();
	
	String cgcCpf = "7551685936";
	if (cgcCpf.length() < 11)
	{
		int cont = 11 - cgcCpf.length();
		for (int i = 0; i < cont; i++)
		{
			cgcCpf = "0" + cgcCpf;
		}
	}
	System.out.print(cgcCpf);

	%>
	<section id=main>
		<div class="container-fluid" align="center">
			<div class="page-header">
				<h1>
					Acompanhamento Medi��o
				</h1>
			</div>
			<div class="row" align="center" width="98%">
				<div class="col-md-12">
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1primary" data-toggle="tab">Informa��es Gerais</a></li>
								<li><a href="#tab2primary" data-toggle="tab">Exce��o</a></li>
								<li><a href="#tab3primary" data-toggle="tab">Relat�rio</a></li>
								<li><a href="#tab4primary" data-toggle="tab">Relat�rio - Comercial</a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
										<div class="info">
											<h3>Ser� gerado a medi��o de contratos quando:</h3>
											<ul>
											    Ser� gerado relat�rio de medi��o de contratos quando:
                                                <ul>
                                                	<li>  Empresas: SLC, Proserv, Orsegups Servi�os e Profiser;</li>
                                                	<li>  Regionais: S�o Jos� / S�o Jos� Asseio;</li>
                                                	<li>  Tipo Mercado: Publico;</li>
                                                </ul>
                                                A medi��o � gerada utilizando como refer�ncia os registros de ponto/presta��o de servi�os realizada na compet�ncia anterior. Ou seja s�o considerados:
                                                <ul>
                                                	<li>  Marca��es do Ponto;</li>
                                                	<li>  Lan�amento de Afastamentos;</li>
                                                	<li>Essas informa��es s�o resgatadas automaticamente do sistema Vetorh-Rubi. Assim em caso de d�vida sobre qualquer apontamento deste relat�rio, dever� ser conferido os lan�amentos do Rubi/Presen�a.</li>
                                                </ul>
                                                No dia 01 de cada m�s as 23:30hs da noite � enviado e-mail autom�tico com relat�rio de m�di��o para: Gerente Asseio S�o Jos�, Diretoria e Presid�ncia.
												<br><br>
												<i><b><u>Informa��es T�cnicas para TI:</u></b></i><br>
                                                <li>No dia 01 de m�s as 05:00 da manh� o cadastro de exece��es � populado automaticamente</li><br>
                                                <b>Eforms de base para gera��o do relat�rio:</b>
                                                 <ul>
                                                 	<li>Eform MEDMedicaoDeContratos</li>
                                                	<li>Eform MEDColaboradores</li>
                                                	<li>Eform Medconsideracoes</li><br>
                                                </ul>
                                                <b>Eforms de BKP</b>
                                                <ul>
                                                	<li>Eform MEDMedicaoDeContratosBKP</li>
                                                	<li>Eform MEDColaboradoresBKP</li>
                                                	<li>Eform MedconsideracoesBKP</li><br>
                                                </ul>
                                                <b>Classes utilizadas:</b>
                                                <ul>
                                                	<li>GeraInformacoesParaMedicaoCtrJob</li>
                                                	<li>OrsegupsMedicaoUtilsMensal</li>
                                                </ul>
                             					<br>
											</ul>
										</div>
								</div>
								<div class="tab-pane fade" id="tab2primary" style="text-align: left;">
									<div class="form-inline"style="text-align: left;">
									<!-- E-form principal da medi��o -->
															<select
	                                         				id="competencias" name="competencias" class="form-control campoformulario" onchange="atualizaListaEformMedicao()" style="width: 100px;font-size:11px;">
	                                         					<% for(String obj : listCom){%>
	                                         					<option value ="<%= obj %>"><%= obj%></option>
	                                         					<%} %>
	                                         				</select>
	                                         				<input type="text" class="form-control campoformulario"  id="contrato" name="contrato" placeholder="Informe o contrato">
	                                         				<input type="text" class="form-control campoformulario"  id="cliente" name="cliente" placeholder="Informe o cliente">
	                                         				<input type="text" class="form-control campoformulario"  id="noContrato" name="noContrato" placeholder="Informe o N�. O Contrato">
	                                         				<br><input type="radio" name="status" value="" id="status" title= "Todos" onclick="radio(this.value)">
  																<img src="">
  															</input>Todos<br>
	                                         				<input type="radio" name="status" value="ok" id="status" onclick="radio(this.value)">
  																<img src="imagens/certo2.png" title= "Sem Alertas">
  															</input>
  															<input type="radio" name="status" value="nVagasDivergentes" id="status" onclick="radio(this.value)">
  																<img src="imagens/exclamacao2.png" title= "Vagas Divergentes">
  															</input>
  															<input type="radio" name="status" value="falta" id="status" onclick="radio(this.value)">
  																<img src="imagens/Alerta_Vagas.png" title= "Faltas">
  															</input>
  															<input type="radio" name="status" value="colaborador demitido" id="status" onclick="radio(this.value)">
  																<img src="imagens/novo_colaborador.png" title= "Colaborador demitido">
  															</input>
  															
  															
															<input type="hidden" id="result">
															<input type="button" class="btn btn-primary campoBotao" value="Pesquisar" id="pesquisar" name="pesquisar" onclick="atualizaListaEformMedicao()"> 
	                   					<div class="embed-responsive embed-responsive-16by9">
											<iframe id="fLista" name="fLista" class="embed-responsive-item" src="http://localhost/fusion/portal/render/FormList?type=medMedicaoDeContratos&showPages=true&edit=true&creatable=true&_fl_set_dyn_periodo='Selecione a compet�ncia...'"></iframe>
	  									</div>
	  								</div>
								  </div>
								<div class="tab-pane fade" id="tab3primary">
									<!-- E-form Exce��o Cliente -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<div id="dlgCon" class="tab-pane fade in active" style="text-align: left;"
												 closed="false" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:false">
                                                 <div class="ftitle"><font size=4><b>Preencha as informa��es a seguir para a gera��o do relat�rio</b></font></font></div><br>
                                                 <form id="forcom" method="post" novalidate>
                                                  		<div>
                                                  			<label >M�s refer�ncia: </label>
                                                  			<input type="month" class="form-control campoformulario"  id="periodo" name="periodo" required='true'>
                                                  		</div>
	                                         			<div><br>
	                                                 		<label>Empresas</label> <select
	                                         				id="empresas" name="empresas" class="form-control campoformulario" style="width: 100px;font-size:11px;">
	                                         					<% for(String obj : retorno){%>
	                                         					<option value ="<%= obj %>"><%= obj%></option>
	                                         					<%} %>
	                                         				</select><br>
	                                         			</div>                                                  		
                                                  		<div>
                                                  			<label>Contratos</label>
                                                	    	<input type="text" name='numctr' class="form-control campoformulario" id='numctr' required='false'/>
                                                	    	<label><font size=0,8> <i>Utilizar a virgula(,) para separar os contratos</i></font></label>
                                                     	</div><br>
                                                     	<div>
                                                  			<label>Cliente</label>
                                                	    	<input type="text" name='clienteRel' class="form-control campoformulario" id='clienteRel' required='false'/>
                                                	   </div><br>
	                                                 	<div>
	                                                 		<label>Formato</label> <select
	                                         				id="setfor" name="setfor" class="form-control campoformulario" style="width: 100px;font-size:11px;" required='true'>
	                                         					<option value="PDF">PDF</option>
	                                         					<option value="XLS">XLS</option>
	                                         				</select><br>
	                                         				
	                                         			</div>
	                                         		</div>
	                                         	</form>
                                                 <div style="text-align: left;">
                                                 <a href="javascript:void(0)" class="btn btn-primary campoBotao"  iconCls="icon-print"style="width: 100px;font-size:11px;" onclick="javascript:gerarRelatorioMedicaoContratos()">Imprimir</a>
                                               </div>
                                             </div>
                                          </div>
                                          <div class="tab-pane fade" id="tab4primary">
									<!-- E-form Exce��o Cliente -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<div id="dlgCon" class="tab-pane fade in active" style="text-align: left;"
												 closed="false" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:false">
                                                 <div class="ftitle"><font size=4><b>Preencha as informa��es a seguir para a gera��o do relat�rio</b></font></font></div><br>
                                                 <form id="forcom" method="post" novalidate>
                                                  		<div>
                                                  			<label >M�s refer�ncia: </label>
                                                  			<input type="month" class="form-control campoformulario"  id="periodoComercial" name="periodoComercial" required='true'>
                                                  		</div>
	                                         			<div><br>
	                                                 		<label>Empresas</label> <select
	                                         				id="empresasComercial" name="empresasComercial" class="form-control campoformulario" style="width: 100px;font-size:11px;">
	                                         					<% for(String obj : retorno){%>
	                                         					<option value ="<%= obj %>"><%= obj%></option>
	                                         					<%} %>
	                                         				</select><br>
	                                         			</div>                                                  		
                                                  		<div>
                                                  			<label>Contratos</label>
                                                	    	<input type="text" name='numctr' class="form-control campoformulario" id='numctrComercial' required='false'/>
                                                	    	<label><font size=0,8> <i>Utilizar a virgula(,) para separar os contratos</i></font></label>
                                                     	</div><br>
                                                     	<div>
                                                  			<label>Cliente</label>
                                                	    	<input type="text" name='clienteRel' class="form-control campoformulario" id='clienteRelComercial' required='false'/>
                                                	   </div><br>
	                                                 	<div>
	                                                 		<label>Formato</label> <select
	                                         				id="setforComercial" name="setforComercial" class="form-control campoformulario" style="width: 100px;font-size:11px;" required='true'>
	                                         					<option value="PDF">PDF</option>
	                                         					<option value="XLS">XLS</option>
	                                         				</select><br>
	                                         				
	                                         			</div>
	                                         		</div>
	                                         	</form>
                                                 <div style="text-align: left;">
                                                 <a href="javascript:void(0)" class="btn btn-primary campoBotao"  iconCls="icon-print"style="width: 100px;font-size:11px;" onclick="javascript:gerarRelatorioMedicaoContratosComercial()">Imprimir</a>
                                               </div>
                                             </div>
                                          </div>
                                          
                                             
	  									<script type="text/javascript">
                                               var url;
                                               function gerarRelatorioMedicaoContratos(){
                                               	
		                                       	var periodo = jQuery('#periodo').val();	
		                                       	var setfor  = jQuery('#setfor option:selected').text();
		                                       	var numctr = $('#numctr').val();
		                                       	var empresa = jQuery('#empresas option:selected').text();
		                                       	var clienteRel = $('#clienteRel').val();
		                                       	var isComercial = "NAO";
		                                       	var servlet = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=medicaoContratos&periodo='+periodo+'&setfor='+setfor+'&numctr='+numctr+'&empresa='+empresa+'&clienteRel='+clienteRel+'&isComercial='+isComercial;
		                                       	alert(''+clienteRel);
    	                                       	location.href=servlet;                                                   
                                               }
                                               
                                               function gerarRelatorioMedicaoContratosComercial(){
                                                  	
   		                                       	var periodo = jQuery('#periodoComercial').val();	
   		                                       	var setfor  = jQuery('#setforComercial option:selected').text();
   		                                       	var numctr = $('#numctrComercial').val();
   		                                       	var empresa = jQuery('#empresasComercial option:selected').text();
   		                                       	var clienteRel = $('#clienteRelComercial').val();
   		                                       	var isComercial = "SIM";
   		                                       	var servlet = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=medicaoContratos&periodo='+periodo+'&setfor='+setfor+'&numctr='+numctr+'&empresa='+empresa+'&clienteRel='+clienteRel+'&isComercial='+isComercial;
   		                                       	location.href=servlet;                                                   
                                                }
                                               
                                               function atualizaListaEformMedicao(){
                                            	   var url = 'http://localhost/fusion/portal/render/FormList?type=medMedicaoDeContratos&showPages=true&edit=true&creatable=true';
                                            	   var competencias = document.getElementById('competencias').value;
                                            	   var contrato = document.getElementById('contrato').value;
                                            	   var cliente = document.getElementById('cliente').value;
                                            	   var noContrato = document.getElementById('noContrato').value;
                                            	   if(document.getElementById("result").value != ''){
                                            		   url = url + "&_fl_set_dyn_alertas="+document.getElementById("result").value;
                                            	   }
                                            	   
                                            	   if(contrato != ''){
                                            		   url = url + "&_fl_set_dyn_contrato="+contrato;
                                            	   }
                                            	   
                                            	   if(cliente != ''){
                                            		   url = url + "&_fl_set_dyn_cliente="+cliente;
                                            	   }
                                            	   
                                            	   if(noContrato != ''){
                                            		   url = url + "&_fl_set_dyn_numOfi="+noContrato;
                                            	   }
                                            	 
                                            	   document.getElementById('fLista').src=url+"&_fl_set_dyn_periodo="+competencias;   
                                               }
                                               function radio(valor){
                                            	   document.getElementById("result").value = valor;
                                               }
                                         </script>
	  								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<style>
	.campoformulario{
		max-width: 300px;
		min-width: 300px;
	}
	.campoBotao{
		max-width: 300px;
		min-width: 300px;
		background-color:#6b7486;
	    
	}
	.tipoOk { background-image:url(imagens/certo2.png);   }
	.tipoFalta { background-image:url(imagens/exclamacao2.png); }
	</style>	
</body>
</html>