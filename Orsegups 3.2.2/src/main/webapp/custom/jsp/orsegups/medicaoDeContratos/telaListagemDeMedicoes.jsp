<!DOCTYPE html>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.custom.orsegups.medicaoContratos.OrsegupsMedicaoUtilsMensal"%>
<%@page import="java.util.ArrayList"%>
<html>
	<head>
	<%@page import="com.neomind.fusion.portal.PortalUtil"%>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="css/estilos.css">
	<title>Controle Medi��o de Contratos</title>
	 <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<link rel="stylesheet" 
		href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
		<script type="text/javascript" 
		src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" 
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
	<script src="js/jquery.growl.js" type="text/javascript"></script>
		<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />
	</head>
	<% 
	List<String> retorno = new ArrayList<String>();
	retorno = OrsegupsMedicaoUtilsMensal.empresas();
	
	List<String> listCom = new ArrayList<String>();
	listCom = OrsegupsMedicaoUtilsMensal.competencias();
	%>
	
	<script type="text/javascript">
		var nomFunGlo = "";
		var competencia ="";
		var numPosGlo = 0;
		var numLocGlo = 0;
		var numOfiCtr = 0;
		var regionaisPesquisa = "";
		
	</script>
	<section id=main>
		<div class="container-fluid" align="center">
			<div class="page-header">
				<h1>
					Acompanhamento Medi��o
				</h1>
			</div>
			<div class="row" align="center" width="98%">
				<div class="col-md-12">
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading" style="background-color:#6b7486;">
							<ul class="nav nav-tabs" style="background-color:#6b7486;">
								<li class="active"><a href="#tab1primary" onclick="escoderDiv()" data-toggle="tab">Informa��es Gerais</a></li>
								<li><a href="#tab2primary" onclick="mostrarDiv()" data-toggle="tab">Exce��o</a></li>
								<li><a href="#tab3primary" onclick="escoderDiv()" data-toggle="tab">Relat�rio</a></li>
								<li><a href="#tab4primary" onclick="escoderDiv()" data-toggle="tab">Relat�rio - Comercial</a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
										<div class="info">
											<h3>Ser� gerado a medi��o de contratos quando:</h3>
											<ul>
											    Ser� gerado relat�rio de medi��o de contratos quando:
                                                <ul>
                                                	<li>  Empresas: SLC, Proserv, Orsegups Servi�os e Profiser;</li>
                                                	<li>  Regionais: S�o Jos� / S�o Jos� Asseio;</li>
                                                	<li>  Tipo Mercado: Publico;</li>
                                                </ul>
                                                A medi��o � gerada utilizando como refer�ncia os registros de ponto/presta��o de servi�os realizada na compet�ncia anterior. Ou seja s�o considerados:
                                                <ul>
                                                	<li>  Marca��es do Ponto;</li>
                                                	<li>  Lan�amento de Afastamentos;</li>
                                                	<li>Essas informa��es s�o resgatadas automaticamente do sistema Vetorh-Rubi. Assim em caso de d�vida sobre qualquer apontamento deste relat�rio, dever� ser conferido os lan�amentos do Rubi/Presen�a.</li>
                                                </ul>
                                                No dia 01 de cada m�s as 23:30hs da noite � enviado e-mail autom�tico com relat�rio de m�di��o para: Gerente Asseio S�o Jos�, Diretoria e Presid�ncia.
												<br><br>
												<i><b><u>Informa��es T�cnicas para TI:</u></b></i><br>
                                                <li>No dia 01 de m�s as 05:00 da manh� o cadastro de exece��es � populado automaticamente</li><br>
                                                <b>Eforms de base para gera��o do relat�rio:</b>
                                                 <ul>
                                                 	<li>Eform MEDMedicaoDeContratos</li>
                                                	<li>Eform MEDColaboradores</li>
                                                	<li>Eform Medconsideracoes</li><br>
                                                </ul>
                                                <b>Eforms de BKP</b>
                                                <ul>
                                                	<li>Eform MEDMedicaoDeContratosBKP</li>
                                                	<li>Eform MEDColaboradoresBKP</li>
                                                	<li>Eform MedconsideracoesBKP</li><br>
                                                </ul>
                                                <b>Classes utilizadas:</b>
                                                <ul>
                                                	<li>GeraInformacoesParaMedicaoCtrJob</li>
                                                	<li>OrsegupsMedicaoUtilsMensal</li>
                                                </ul>
                             					<br>
											</ul>
										</div>
								</div>
								<div class="tab-pane fade in active" id="tab2primary">										
										<div class="overflow-x:auto;">
											<div class="form-inline"style="text-align: left;">
									<!-- E-form principal da medi��o -->
															<select
	                                         					id="competencias" name="competencias" class="form-control campoformulario" style="width: 100px;font-size:11px;">
	                                         						<% for(String obj : listCom){%>
	                                         						<option value ="<%= obj %>"><%= obj%></option>
	                                         						<%} %>
	                                         				</select>
	                                         				<input type="text" class="form-control campoformulario"  id="cliente" name="cliente" placeholder="Informe o cliente">
	                                         				<input type="text" class="form-control campoformulario"  id="noContrato" name="noContrato" placeholder="Informe o N�. O Contrato">
	                                         				<hr>
	                                         				<br><input type="checkbox" name="status" value="" id="idCbtodos" title= "Todos" onclick="cbTodos('todos')">
  															<img src="">
  															</input><label>Todos</label>
	                                         				<input type="checkbox" name="status" value="" id="idCbOk" onclick="cbOk('ok')">
  																<img src="imagens/certo2.png" title= "Sem Alertas">
  															</input>
  															<input type="checkbox" name="status" value="" id="idVagas" onclick="cbVagas('vagasDiv')">
  																<img src="imagens/exclamacao2.png" title= "Vagas Divergentes">
  															</input>
  															<input type="checkbox" name="status" value="" id="idFaltas" onclick="cbFaltas('falta')">
  																<img src="imagens/Alerta_Vagas.png" title= "Faltas">
  															</input>
  															<input type="checkbox" name="status" value="" id="idDemitidos" onclick="cbDemitidos('demiti')">
  																<img src="imagens/novo_colaborador.png" title= "Colaborador demitido">
  															</input>
  															<input type="hidden" id="result">
															<input type="button" class="btn btn-primary campoBotao" value="Pesquisar" id="pesquisar" name="pesquisar" onclick="pesquisarMedicoes()"></br></br></br> 
															
	                   							</div>
	                   						</div>
										     <div class="table-responsive">          
											  <table id="tabelaNova" class="table tableNova">
											    <thead>
											      <tr>
											        <th>Cliente</th>
											        <th>Empresa</th>
											        <th>Responsavel</th>
											        <th>N� Oficial</th>
											        <th>Regional</th>
											        <th>Alertas</th>
											        <th>Visualizar</th>
											      </tr>
											    </thead>
											  </table>
											  </div>
										     <!-- window -->
										     <div id="add" title="Rela��o de postos e colaboradores" class="easyui-dialog" data-options="modal:true,closed:true,iconCls:'icon-save'" style="width:1100px;height:520px;padding:10px;">
												<div id="tbAdd" class="thtd" style="padding:3px">
												  <div class="form-inline"style="text-align: left;">	
													<input type="text" class="form-control campoformulario"  id="pesquisaPosto" name="pesquisaPosto" placeholder="Informe o N�mero do Posto">
													<input type="text" class="form-control campoformulario"  id="pesquisaColaboradorNome" name="pesquisaColaboradorNome" placeholder="Informe o Colaborador">
													<br><hr><input type="checkbox" name="status" value="" id="idTodosPosto" title= "Todos" onclick="radioPosto(this.value)">
  															<img src="">
  															</input><label>Todos</label>
	                                         				<input type="checkbox" name="status" value="" id="idCbOkPosto" onclick="cbOkPosto('ok')">
  																<img src="imagens/certo2.png" title= "Sem Alertas">
  															</input>
  															<input type="checkbox" name="status" value="" id="idVagasPosto" onclick="cbVagasPosto('vagas')">
  																<img src="imagens/exclamacao2.png" title= "Vagas Divergentes">
  															</input>
  															<input type="checkbox" name="status" value="" id="idFaltasPosto" onclick="cbFaltasPosto('falta')">
  																<img src="imagens/Alerta_Vagas.png" title= "Faltas">
  															</input>
  															<input type="checkbox" name="status" value="" id="idDemitidosPosto" onclick="cbDemitidosPosto('demiti')">
  																<img src="imagens/novo_colaborador.png" title= "Colaborador demitido">
  															</input>  															
  													<input type="button" class="btn btn-primary campoBotao" value="Pesquisar" id="pesquisar" name="pesquisar" onclick="pesquisarColaboradorEPosto()">
													<input type="hidden" id="resultPosto"> 
													<hr></hr>
													<a title='Cadastrar Posto' style='cursor:pointer' ALIGN='RIGHT' onclick='cadastrarPosto()'><img src='imagens/CadastrarPosto.png'></a><br>
												 </div>
													<table id ="dtPostos" class="table">
														<thead>
															<tr class="thtd">
															</tr>
														</thead>
													</table>
													<input type="hidden" id="nomeCliente">
												</div>
											</div>
											<!-- Observacoes do posto -->
											<div id="idObservacoesPosto" title="Observa��es do Posto" class="easyui-dialog" data-options="modal:true,closed:true,iconCls:'icon-save'" style="width:800px;height:520px;padding:10px;">
												 	<div id="dvObservacaoPosto" class="tt-inner" style="padding:3px">
												 		<textarea id="labelObsPosto" value="" style="width:800px;height:300px"></textarea><br><br>
												 		<label id="idObsPosto" type="hidden" value=""></label>
												 		<input type="button" class="btn btn-primary campoBotao" value="Salvar" id="Salvar" name="Salvar" onclick="salvarObsPosto()"></button>
												 	</div>
											</div>
											<!-- window -->
										
											<div id="idObservacoes" title="Observa��es do colaborador" class="easyui-dialog" data-options="modal:true,closed:true,iconCls:'icon-save'" style="width:1100px;height:520px;padding:10px;">
											 <div id="tab" class="easyui-tabs" style="width: auto;height:630px" data-options="tabHeight:55">
												<div id="aba1"title="<span class='tt-inner'><img src='imagens/obsColaborador.png'/><br>Observa��es</span>" style="padding:10px">
											 		<div id="dvObservacao" class="tt-inner" style="padding:3px">
														<table id ="dtObservacoes" title="Observa��es" class="easyui-datagrid" rownumbers="true" singleSelect="true" nowrap="false">
															<thead>
																<tr>
																	<th field="descricao" width="700">Observa��es</th>
		           													<th field="isfaltaString" width="100">� falta</th>
		           													<th field="linkExcluir" width="40">Remover</th>
		           													<th field="linkEdirar" width="40">Editar</th>
		           												</tr>
															</thead>
														</table>
														<div>
															<div class="form-group">
														 				Observa��o:<td><textarea id="observacao" name="observacao" style="width:500px" class="form-control"></textarea>
														 				<br>
														 				� Falta:<select class="form-control" rows="3" style="width:500px" id="falta">
    																		<option value="Sim">Sim</option>
    																		<option value="Nao">N�o</option>
  																		</select><br><br>
  																		Desc.<input id="idDescMed" style="width:500px" class="form-control"></input>
  																		<div class="form-inline"style="text-align: left;">
  																			<div class="field_container">
  																				<form>
  																					<fieldset class="field_group">
    																					<legend>Horas&nbsp&nbsp
    																					</legend>
    																					<table>
    																						<div style="text-align: left;">
    																							<tr>
    																								<td style="width:420px">
  																										<label>Parcial</label>
  																										<img title='	*Parcial: Referisse � soma das horas parciais que o colaborador realizou no decorrer do m�s (Nos dias em que o colaborador n�o cumpriu sua carga horario total).' 
    																									src="imagens/help_medicao.png">
  																										
  																										<br>
    																										&nbsp&nbspHora/Min Trab:<input id="idHorTra" type="number" style="width:80px" class="form-control"></input>
  																											<input id="idMinTra" type="number" style="width:80px" class="form-control"></input>
  																									</td>
  																									<td style="width:420px">
	    																								<label>Extra</label>
	    																								<img title='*Extra: Referisse a soma das horas extras que o colaborador realizou no decorrer do m�s.' 
    																									src="imagens/help_medicao.png">
	    																								<br>
		    																							Hora/Min Ex:&nbsp<input id="idHorEx" type="number" style="width:80px" class="form-control"></input>
  																										<input id="idMinEx" type="number" style="width:80px" class="form-control"></input>
  																									</td>
  																								</tr>
  																							</div>
  																						</table>
    																				</fieldset>
																				</form>
																			</div>
  																		</div><br>
	  																	
  																<br><input type="button" class="btn btn-primary campoBotao" value="Salvar" id="Salvar" name="Salvar" onclick="salvarObservacao()">
  																	<input type="hidden" id="neoIdObs">
  																	<input type="hidden" id="idPosto">
  															</div>
														</div>
													</div>
												  </div>
												<div title="<span class='tt-inner'><img src='imagens/diarias.png'/><br>Diarias</span>" style="padding:10px">
											 		<div id="dvDiarias" class="thtd" style="padding:3px">
														<table id="dtDiarias" title="Diarias" class="easyui-datagrid" rownumbers="true" singleSelect="true" nowrap="false">
		        											<thead>
		            											<tr>
		                											<th field="qtdDiarias" width="70">Quantidade</th>
		                											<th field="obsDiaria" width="700">Observa��es</th>
		                											<th field="linkExcluir" width="60">Remover</th>
		                											<th field="linkEditar" width="60">Editar</th>
		                											<th 
		                										</tr>
		        											</thead>
		    											</table>
		    											<div>
		    												Observa��o:<input id="idObsDia" style="width:500px" class="form-control"></input>
		    												Quantidade:<input id="idQtdDia" style="width:500px" type="number" class="form-control"></input><br>
		    												<input type="button" class="btn btn-primary campoBotao" value="Salvar" id="Salvar" name="Salvar" onclick="salvarDiaria()">
		    												<input type="hidden" id="neoIdDiaria"></input>
		    											</div>
		    										</div>
												  </div>
												  <div title="<span class='tt-inner'><img src='imagens/demitido.png'/><br>Colab. Demitidos do M�s</span>" style="padding:10px">
											 	  <div id="dvColaboradoresDem" class="thtd" style="padding:3px">
														<table id ="dtColaboradoresDem" title="Colaboradores Demitidos" class="easyui-datagrid" rownumbers="true" singleSelect="true" nowrap="false">
															<thead>
																<tr>
																	<th field="numcad" width="70">Matricula</th>
		           													<th field="numemp" width="70">Empresa</th>
		           													<th field="nomfun" width="400">Nome</th>
		           													<th field="datDmsString" width="150">Data da Demiss�o</th>
		           													<th field="numpos" width="70">Posto</th>
		           												</tr>
															</thead>
														</table>
													</div>
												  </div>
											  <div title="<span class='tt-inner'><img src='imagens/cobertura_posto.png'/><br>Cobertura(s)">
													<table id="dgCobertura" title="COBERTURAS" class="easyui-datagrid" style="width:auto;height:auto"; rownumbers="true" singleSelect="true" nowrap="false">
		        										<thead>
		            										<tr>
		           												<th field="percob" width="100">Per�odo</th>
		           												<th field="tipcob" width="150">Cobertura</th>
		           												<th field="nomcob" width="150">Colaborador</th>
		           												<th field="nomsub" width="150">Substitu�do</th>
		                										<th field="obscob" width="210">Observa��o</th>
		            										</tr>
		        										</thead>
		    										</table>
												</div>
												  <div title="<span class='tt-inner'><img src='imagens/supervisao_moto.png'/><br>Supervis�o</span>" style="width:1200px;height:520px; padding:10px">
											 		<div id="dvColaboradoresDem" class="thtd" style="padding:3px">
														<table id="dgEventoX8" title="Eventos Supervis�o" class="easyui-datagrid" rownumbers="true" singleSelect="true" nowrap="false">
		        											<thead>
		            											<tr>
		                											<th field="centralParticao" width="70">Conta</th>
		                											<th field="dataRecebido" width="70">Recebido</th>
		                											<th field="dataDeslocamento" width="70">Deslocado</th>
		                											<th field="dataLocal" width="70">Local</th>
		                											<th field="dataFechamento" width="70">Fechado</th>
		                											<th field="nomeViatura" width="140">Colaborador</th>
		                											<th field="observacaoFechamento" width="260">Observa��o</th>
		            											</tr>
		        											</thead>
		    											</table>
													</div>
												  </div>
											 	</div>
										 	</div>
										
										  <!-- window cadastro colaborador -->
											<div id="addColaborador" title="Cadastrar colaborador" class="easyui-dialog" data-options="modal:true,closed:true,iconCls:'icon-save'" style="width:1100px;height:520px;padding:10px;">
												<div id="tbColaborador" class="thtd" style="padding:3px">
													<form role="form">
															<div class="form-group">
																<label>Carga Horaria:</label>
																<input id="campoCargaHorPos" class="form-control camposCadastroColaborador"></input><br>
																<label>Funcao:</label>
																<input id="campoFuncao" class="form-control camposCadastroColaborador" ></input><br>
																<input id="campoLocal" type="hidden" class="form-control camposCadastroColaborador" ></input><br>
																<label>Matricula Colaborador:</label>
																<input id="campoMatricula" type="number" class="form-control camposCadastroColaborador"></input><br>
																<label>Nome do Colaborador:</label>
																<input id="campoNome" class="form-control camposCadastroColaborador"></input><br>
																<input id="campoCentroCusto" type="hidden" class="form-control camposCadastroColaborador"></input><br>
																<label>Data de Admissao:</label>
																<input id="campoDatAdm" type="date" class="form-control camposCadastroColaborador"></input><br>
																<label>Descricao - Medicao:</label>
																<input id="campoDescMed" class="form-control camposCadastroColaborador"></input><br>
																<input id="campoSituacao" type="hidden" class="form-control camposCadastroColaborador"></input><br>
																<label>Horas(s) trabalhadas:</label>
																<input id="campoHorTra" class="form-control camposCadastroColaborador"></input><br>
																<label>Minuto(s) trabalhados:</label>
																<input id="campoMinTra" class="form-control camposCadastroColaborador"></input><br>
																<label>Hora(s) extra(s):</label>
																<input id="campoHorEx" class="form-control camposCadastroColaborador"></input><br>
																<label>Minuto(s) Extra(s):</label>
																<input id="campoMinEx" class="form-control camposCadastroColaborador"></input><br>	
															</div>
															<input type="button" class="btn btn-primary campoBotao" value="Salvar" id="SalvarColaborador" name="SalvarColaborador" onclick="salvarColaborador()">
															<input type="hidden" id="idDoPosto">	
													</form>
												</div>
											</div>
											<!-- window -->
									 </div>
									 <div class="tab-pane fade" id="tab3primary">
									<!-- E-form Exce��o Cliente -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<div id="dlgCon" class="tab-pane fade in active" style="text-align: left;"
												 closed="false" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:false">
                                                 <div class="ftitle"><font size=4><b>Preencha as informa��es a seguir para a gera��o do relat�rio</b></font></font></div><br>
                                                 <form id="forcom" method="post" novalidate>
                                                  		<div>
                                                  			<label >M�s refer�ncia: </label>
                                                  			<input type="month" class="form-control campoformulario"  id="periodo" name="periodo" required='true'>
                                                  		</div>
	                                         			<div><br>
	                                                 		<label>Empresas</label> <select
	                                         				id="empresas" name="empresas" class="form-control campoformulario" style="width: 100px;font-size:11px;">
	                                         					<% for(String obj : retorno){%>
	                                         					<option value ="<%= obj %>"><%= obj%></option>
	                                         					<%} %>
	                                         				</select><br>
	                                         			</div>                                                  		
                                                  		<div>
                                                  			<label>Contratos</label>
                                                	    	<input type="text" name='numctr' class="form-control campoformulario" id='numctr' required='false'/>
                                                	    	<label><font size=0,8> <i>Utilizar a virgula(,) para separar os contratos</i></font></label>
                                                     	</div><br>
                                                     	<div>
                                                  			<label>N� Contrato Oficial</label>
                                                	    	<input type="text" name='numCtrOfi' class="form-control campoformulario" id='numCtrOfi' required='false'/>
                                                	    	<label><font size=0,8></font></label>
                                                     	</div><br>
                                                     	<div>
                                                  			<label>Cliente</label>
                                                	    	<input type="text" name='clienteRel' class="form-control campoformulario" id='clienteRel' required='false'/>
                                                	   </div><br>
	                                                 	<div>
	                                                 		<label>Formato</label> <select
	                                         				id="setfor" name="setfor" class="form-control campoformulario" style="width: 100px;font-size:11px;" required='true'>
	                                         					<option value="PDF">PDF</option>
	                                         					<option value="XLS">XLS</option>
	                                         				</select><br>
	                                         				
	                                         			</div>
	                                         		</div>
	                                         	</form>
                                                 <div style="text-align: left;">
                                                 <a href="javascript:void(0)" class="btn btn-primary campoBotao"  iconCls="icon-print"style="width: 100px;font-size:11px;" onclick="javascript:gerarRelatorioMedicaoContratos()">Imprimir</a>
                                               </div>
                                             </div>
                                          </div>
                                          <div class="tab-pane fade" id="tab4primary">
									<!-- E-form Exce��o Cliente -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<div id="dlgCon" class="tab-pane fade in active" style="text-align: left;"
												 closed="false" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:false">
                                                 <div class="ftitle"><font size=4><b>Preencha as informa��es a seguir para a gera��o do relat�rio</b></font></font></div><br>
                                                 <form id="forcom" method="post" novalidate>
                                                  		<div>
                                                  			<label >M�s refer�ncia: </label>
                                                  			<input type="month" class="form-control campoformulario"  id="periodoComercial" name="periodoComercial" required='true'>
                                                  		</div>
	                                         			<div><br>
	                                                 		<label>Empresas</label> <select
	                                         				id="empresasComercial" name="empresasComercial" class="form-control campoformulario" style="width: 100px;font-size:11px;">
	                                         					<% for(String obj : retorno){%>
	                                         					<option value ="<%= obj %>"><%= obj%></option>
	                                         					<%} %>
	                                         				</select><br>
	                                         			</div>                                                  		
                                                  		<div>
                                                  			<label>Contratos</label>
                                                	    	<input type="text" name='numctr' class="form-control campoformulario" id='numctrComercial' required='false'/>
                                                	    	<label><font size=0,8> <i>Utilizar a virgula(,) para separar os contratos</i></font></label>
                                                     	</div><br>
                                                     	<div>
                                                  			<label>N� Contrato Oficial</label>
                                                	    	<input type="text" name='numCtrOfiC' class="form-control campoformulario" id='numCtrOfiC' required='false'/>
                                                	    	<label><font size=0,8> <i>Utilizar a virgula(,) para separar os contratos</i></font></label>
                                                     	</div><br>
                                                     	
                                                     	<div>
                                                  			<label>Cliente</label>
                                                	    	<input type="text" name='clienteRel' class="form-control campoformulario" id='clienteRelComercial' required='false'/>
                                                	   </div><br>
                                                	   Imprimir Observa��es:<select class="form-control campoformulario" rows="3" style="width:500px" id="imprimirObs">
    																		<option value="Sim">Sim</option>
    																		<option value="Nao">N�o</option>
  																		</select><br>
	                                                 	<div>
	                                                 		<label>Formato</label> <select
	                                         				id="setforComercial" name="setforComercial" class="form-control campoformulario" style="width: 100px;font-size:11px;" required='true'>
	                                         					<option value="PDF">PDF</option>
	                                         					<option value="XLS">XLS</option>
	                                         				</select><br>
	                                         				
	                                         			</div>
	                                         		</div>
	                                         	</form>
                                                 <div style="text-align: left;">
                                                 <a href="javascript:void(0)" class="btn btn-primary campoBotao"  iconCls="icon-print"style="width: 100px;font-size:11px;" onclick="javascript:gerarRelatorioMedicaoContratosComercial()">Imprimir</a>
                                               </div>
                                             </div>
                                          </div>
										</div>
										<div id="dlgLoader" title="Carregando..." class="easyui-window" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false,closable:false" style="width:220px;height:120px">  
											<div style="width:100%; text-align:center; margin-top: 10px">
												<img src="imagens/loader.gif">
											</div>	
										</div>
										<div id="dlNovoPosto" title="Informe o Centro de Custo" class="easyui-window" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false,closable:true" style="width:320px;height:200px">  
											<div style="width:100%; text-align:center; margin-top: 10px">
												<input type="text" class="form-control"  id="ccuNovoPosto" name="ccuNovoPosto" placeholder="Informe o Centro de custo"><br>
												<input type="button" class="btn btn-primary campoBotao" value="Salvar" id="SalvarPosto" name="SalvarPosto" onclick="salvarPosto()">
											</div>	
										</div>
										<script type="text/javascript">
										 	
										var url;
                                        function gerarRelatorioMedicaoContratos(){
                                        	
	                                       	var periodo = jQuery('#periodo').val();	
	                                       	var setfor  = jQuery('#setfor option:selected').text();
	                                       	var numctr = $('#numctr').val();
	                                       	var empresa = jQuery('#empresas option:selected').text();
	                                       	var clienteRel = $('#clienteRel').val();
	                                       	var isComercial = "NAO";
	                                       	var imprimirObs = "SIM";
	                                       	var numCtrOfi = jQuery('#numCtrOfi').val();	
	                                       	var servlet = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=medicaoContratos&periodo='+periodo+'&setfor='+setfor+'&numctr='+numctr+'&empresa='+empresa+'&clienteRel='+clienteRel+'&isComercial='+isComercial+'&imprimirObs='+imprimirObs+'&numCtrOfi='+numCtrOfi;
	                                       	location.href=servlet;                                                   
                                        }
                                        
                                        function gerarRelatorioMedicaoContratosComercial(){
                                           	
	                                       	var periodo = jQuery('#periodoComercial').val();	
	                                       	var setfor  = jQuery('#setforComercial option:selected').text();
	                                       	var numctr = $('#numctrComercial').val();
	                                       	var empresa = jQuery('#empresasComercial option:selected').text();
	                                       	var clienteRel = $('#clienteRelComercial').val();
	                                       	var isComercial = "SIM";
	                                       	var imprimirObs = jQuery('#imprimirObs').val();
	                                       	var numCtrOfi = jQuery('#numCtrOfiC').val();
	                                       	var servlet = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=medicaoContratos&periodo='+periodo+'&setfor='+setfor+'&numctr='+numctr+'&empresa='+empresa+'&clienteRel='+clienteRel+'&isComercial='+isComercial+'&imprimirObs='+imprimirObs+'&numCtrOfi='+numCtrOfi;
	                                       	location.href=servlet;                                                   
                                         }
											function pesquisarMedicoes(){
													competencia = document.getElementById('competencias').value;
    	                                        	var cliente = document.getElementById('cliente').value;
    	                                        	var noContrato = document.getElementById('noContrato').value;
    	                                        	var cbTodos = document.getElementById("idCbtodos").value;
    	                                        	var cbVagas = document.getElementById("idVagas").value;
    	                                        	var cbFalta = document.getElementById("idFaltas").value;
    	                                        	var cbDemit = document.getElementById("idDemitidos").value;
    	                                        	var cbOk = document.getElementById("idCbOk").value;
    	                                        	startProgress();
    	                                        	var data = "action=listaMedicoes&competencia="+competencia+"&cliente="+cliente+"&noContrato="+noContrato+"&cbTodos="+cbTodos;
    	                                        	data = data +"&cbVagas="+cbVagas+"&cbFalta="+cbFalta+"&cbDemit="+cbDemit+"&cbOk="+cbOk;
    	                                           	
    	                                        	jQuery.ajax({
    	                                        		method: 'post',
    	                                            	url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
    	                                            	dataType: 'json',	
    	                                            	data: data,
    	                                            	success: function(result){
    	                                            		$("#tabelaNova > tbody").empty();
    	                                            		for(i in result){
    	                                            			$('#tabelaNova').append("<tr><td>" + result[i].cliente + 
    	                                            					"</td><td>" +result[i].empresaContratada + "</td>"+
    	                                            					"</td><td>" + result[i].responsavel + "</td>"+
    	                                            					"</td><td>" + result[i].numOfi + "</td>"+
    	                                            					"</td><td>" + result[i].regional + "</td>"+
    	                                            					"</td><td>" + result[i].imgAlertas + "</td>"+
    	                                            					"</td><td>" + result[i].linkVisualizar + "</td></tr>");	
    	                                            		}
    	                                            		if(result == ""){
    	                                            			$.growl.warning({ title: "", message: "Nenhum resultado contrato!" });
    	                                            		}else{
    	                                            			$.growl({ title: "", message: "Dados carregados com sucesso!" });
    	                                            		}
	    	                                            	//$('#dt').datagrid('loadData',result);
    		                                              	//$('#dt').datagrid('load',result);
    		                                               	stopProgress();
    	        	                                    }
    	            	                           });
                                     	   }
										 	
											function showObservacoesPosto(neoId,obsPosto){
												document.getElementById("labelObsPosto").value = obsPosto;
												document.getElementById("idObsPosto").value = neoId;
												$("#idObservacoesPosto").dialog("open");
											}
											
										 	function salvarDiaria(){
										 		
										 		var obsDia = document.getElementById('idObsDia').value;
										 		var qtdDia = document.getElementById('idQtdDia').value;
										 		var cliente = document.getElementById('nomeCliente').value;
										 		var neoIdDiaria = document.getElementById('neoIdDiaria').value;
										 		startProgress();
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',	
	                                                data: "action=salvarDiaria&competencia="+competencia+"&cliente="+cliente+"&obsDia="+obsDia+"&qtdDia="+qtdDia+"&neoIdDiaria="+neoIdDiaria,
	                                                success: function(result){
	                                                	$('#dtDiarias').datagrid('loadData',result);
	                                                	$('#dtDiarias').datagrid('load',result);
	                                                	document.getElementById('neoIdDiaria').value = 0;
	                                                	document.getElementById('idObsDia').value = '';
	                                                	document.getElementById('idQtdDia').value = '';
	                                                	$.growl({ title: "", message: "Diaria Salva Com Sucesso!" });
	                                                }
	                                            });
										 		stopProgress();
										 	}
										 	
										 	function editarDiaria(neoId){
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',	
	                                                data: "action=editarDiaria&neoId="+neoId,
	                                                success: function(result){
	                                                	document.getElementById('idObsDia').value = result.obsDiaria;
	    										 		document.getElementById('idQtdDia').value = result.qtdDiarias;
	    										 		document.getElementById('neoIdDiaria').value = neoId;
	                                                }
	                                            });
										 		
										 		
										 	}
										 	
										 	function removerDiaria(neoId){
										 		startProgress();
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',	
	                                                data: "action=removerDiaria&competencia="+competencia+"&cliente="+cliente+"&neoId="+neoId,
	                                                success: function(result){
	                                                	$('#dtDiarias').datagrid('loadData',result);
	                                                	$('#dtDiarias').datagrid('load',result);
	                                                	$.growl({ title: "", message: "Diaria Removida Com Sucesso!" });
	                                                }
	                                            });
										 		stopProgress();
										 	}
										 	
										 	function pesquisarColaboradorEPosto(){
										 		var pesquisaColaboradorNome = document.getElementById('pesquisaColaboradorNome').value;
										 		var pesquisaPosto = document.getElementById('pesquisaPosto').value;
										 		var cliente = document.getElementById('nomeCliente').value;
										 		var cbTodos = document.getElementById("idTodosPosto").value;
	                                           	var cbVagas = document.getElementById("idVagasPosto").value;
	                                           	var cbFalta = document.getElementById("idFaltasPosto").value;
	                                           	var cbDemit = document.getElementById("idDemitidosPosto").value;
	                                           	var cbOk = document.getElementById("idCbOkPosto").value;
	                                           	listaDePostos(cliente,pesquisaColaboradorNome,pesquisaPosto,'',cbTodos,cbVagas,cbFalta,cbDemit,cbOk,numOfiCtr);
										 	}
										 	
										    function salvarColaborador(){
										    	var campoCargaHorPos = document.getElementById('campoCargaHorPos').value;
										    	var campoFuncao = document.getElementById('campoFuncao').value;
										    	var campoLocal =  document.getElementById('campoLocal').value;
										    	var campoMatricula = document.getElementById('campoMatricula').value;
										    	var campoNome = document.getElementById('campoNome').value;
										    	var campoCentroCusto = document.getElementById('campoCentroCusto').value;
										    	var campoDatAdm = document.getElementById('campoDatAdm').value;
										    	var campoDescMed = document.getElementById('campoDescMed').value;
										    	var campoSituacao = document.getElementById('campoSituacao').value;
										    	var campoHorTra = document.getElementById('campoHorTra').value;
										    	var campoMinTra = document.getElementById('campoMinTra').value;
										    	var campoHorEx = document.getElementById('campoHorEx').value;
										    	var campoMinEx = document.getElementById('campoMinEx').value;
										    	var neoIdPosto = document.getElementById('idDoPosto').value;
										    	
										    	var data = "action=salvarColaborador&campoCargaHorPos="+campoCargaHorPos+"&campoFuncao="+campoFuncao+"&campoMatricula="+campoMatricula+"&campoNome="+campoNome;
										    	data = data+"&campoCentroCusto="+campoCentroCusto+"&campoDatAdm="+campoDatAdm+"&campoDescMed="+campoDescMed+"&campoSituacao="+campoSituacao;
										    	data = data+"&campoHorTra="+campoHorTra+"&campoMinTra="+campoMinTra+"&campoHorEx="+campoHorEx+"&campoMinEx="+campoMinEx+"&neoIdPosto="+neoIdPosto+"&campoLocal="+campoLocal;
										    	var cliente = document.getElementById("nomeCliente").value;
										    	jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: data,
	                                                success: function(result){
	                                                	listaDePostos(cliente);
	                                                	$("#addColaborador").dialog("close");
	                                                	$.growl({ title: "", message: "Colaborador Salvo Com Sucesso!" });
	                                                }
	                                            });
										    	
										    }
										    
										    function salvarObsPosto(){
										    	var neoId = document.getElementById("idObsPosto").value;
										    	var observacao = document.getElementById("labelObsPosto").value;
										    	var cliente = document.getElementById("nomeCliente").value;
										    	startProgress();
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: "action=salvarObservacaoPosto&observacao="+observacao+"&neoId="+neoId,
	                                                success: function(result){
	                                                   	var janela = false;
	                                                	listaDePostos(cliente,'','',janela);
	                                                	$("#idObservacoesPosto").dialog("close");
	                                                	$.growl({ title: "", message: "Observa��o Posto Salvo Com Sucesso!" });
	                                                }
										 		});
										 		stopProgress();
										    }
										    
										    function salvarPosto(){
										    	var codCcu = document.getElementById("ccuNovoPosto").value;
										    	var periodo = document.getElementById('competencias').value;
										    	var cliente = document.getElementById("nomeCliente").value;
										    	codCcu = codCcu.trim();
										    	if(codCcu === ''){
										    		$.growl.warning({ title: "Aviso", message: 'O Centro de Custo precisa ser informado!'});
										    	}else{
										    		startProgress();
										 			jQuery.ajax({
	                                            		method: 'post',
	                                        	        url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                    	            dataType: 'json',
	                                	                data: "action=cadastrarPosto&codCcu="+codCcu+"&periodo="+periodo+"&cliente="+cliente,
	                            	                    success: function(result){
	                        	                           	var janela = false;
	                        	                           	if(result.mensagemErro != undefined){
	                        	                           		$.growl.error({title: "Erro", message: result.mensagemErro});
	                        	                           	}else{
	                        	                           		listaDePostos(cliente,'','',janela);
		                	                                	$("#dlNovoPosto").dialog("close");
		            	                                    	$.growl({ title: "", message: result.mensagemSucesso});	
	                        	                           	}
	                    	                            },
	    	                                            error: function(e){  
		                              		       			
		                              		       			$("#dlNovoPosto").dialog("close");
	                              		       				document.getElementById("ccuNovoPosto").value = '';
	                              		       				$("#dlNovoPosto").dialog("open");
	                              		          	}
										 			});
										 			stopProgress();
										    	}
										    }
										    
										    function cadastrarPosto(){
										    	document.getElementById("ccuNovoPosto").value = '';
										    	$("#dlNovoPosto").dialog("open");
										    }
										 	
										 	function salvarObservacao(){
										 		var isFalta = document.getElementById('falta').value;
										 		var observacao = jQuery('#observacao').val();
										 		var neoId = document.getElementById("neoIdObs").value;
										 		var neoIdPosto = document.getElementById("idPosto").value;
										 		var cliente = document.getElementById('nomeCliente').value;
										 		var horTra = document.getElementById('idHorTra').value;
										 		var minTra = document.getElementById('idMinTra').value;
										 		var horEx = document.getElementById('idHorEx').value;
										 		var minEx = document.getElementById('idMinEx').value;
										 		var descMed = document.getElementById("idDescMed").value;
										 		var observacaoValidacao = observacao.trim();
										 		 startProgress();
										 		
										 		 jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: "action=salvarObservacao&observacao="+observacao+"&isFalta="+isFalta+"&competencia="+competencia+"&numcad="+numCadGlo+"&nomfun="+nomFunGlo+"&neoId="+neoId+"&neoIdPosto="+neoIdPosto+"&descMed="+descMed
	                                                +"&horTra="+horTra+"&minTra="+minTra+"&horEx="+horEx+"&minEx="+minEx,
	                                                success: function(result){
	                                                	$('#dtObservacoes').datagrid('loadData',result);
	                                                	$('#dtObservacoes').datagrid('load',result);
	                                                	$('#dtObservacoes').datagrid('loadData',result);
	                                                	$('#dtObservacoes').datagrid('load',result);
	                                                	document.getElementById("observacao").value = '';
	                                                	document.getElementById("neoIdObs").value = '';
	                                                	var janela = false;
	                                                	listaDePostos(cliente,'','',janela);
	                                                	$("#idObservacoes").dialog("open");
	                                                	$.growl({ title: "", message: "Registro Salva Com Sucesso!" });	
	                                                	
	                                                }
										 		});
										 		stopProgress();
										 	}
										 	function removerObservacao(neoId){
										 		var isFalta = document.getElementById('falta').value;
										 		var observacao = jQuery('#observacao').val();
										 		var cliente = document.getElementById('nomeCliente').value;
										 		var janela = false;
										 		startProgress();
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: "action=removerObservacao&observacao="+observacao+"&isFalta="+isFalta+"&competencia="+competencia+"&numcad="+numCadGlo+"&nomfun="+nomFunGlo+"&neoId="+neoId,
	                                                success: function(result){
	                                                	listaDePostos(cliente,'','',janela);
	                                                	$('#dtObservacoes').datagrid('loadData',result);
	                                                	$('#dtObservacoes').datagrid('load',result);
	                                                	$.growl({ title: "", message: "Observa��o Removida Com Sucesso!" });
	                                                }
	                                            });
										 		stopProgress();
										 	}
										 	
										 	function removerPosto(neoId){
										 		var cliente = document.getElementById('nomeCliente').value;
										 		var janela = false;
										 		startProgress();
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: "action=removerPosto&neoId="+neoId,
	                                                success: function(result){
	                                                	listaDePostos(cliente,'','',janela);
	                                                	$.growl({ title: "", message: "Posto Removido Com Sucesso!" });
	                                                },
    	                                            error: function(e){  
	                              		       			$.growl.error({title: "Erro", message: 'Posto possui colaborador lotado'});
	                              		       }
	                                            });
										 		stopProgress();
										 		
										 	}
										 	
										 	function cadastrarColaborador(neoId,cliente,codccu,nomloc){
										 		limparCamposColaboradores();
										 		document.getElementById("nomeCliente").value = cliente;
										 		document.getElementById("idDoPosto").value = neoId;
										 		document.getElementById("campoLocal").value = nomloc;
										 		document.getElementById("campoCentroCusto").value = codccu;
										 		document.getElementById("campoDescMed").value = "30 Dia(s)";
										 		document.getElementById("campoSituacao").value = "Trabalhando";
										 		document.getElementById("campoHorTra").value = 0;
										 		document.getElementById("campoMinTra").value = 0;
										 		document.getElementById("campoHorEx").value = 0;
										 		document.getElementById("campoMinEx").value = 0;
										 		$("#addColaborador").dialog("open");
										 	}
										 	
										 	function removerColaborador(neoId,cliente){
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: "action=removerColaborador&competencia="+competencia+"&neoId="+neoId+"&cliente="+cliente,
	                                                success: function(result){
	                                                	listaDePostos(cliente);
	                                                	$("#add").dialog("close");
	                                                	$.growl({ title: "", message: "Colaborador Removida Com Sucesso!" });
	                                                }
	                                            });
										 	}
										 	
										 	
										 	function showObservacoes(numcad,nomfun,numpos,descMed,neoIdPosto,numloc, horTra, minTra, horEx, minEx){
										 		competencia = document.getElementById('competencias').value;
										 		startProgress();
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: "action=listaObservacoes&competencia="+competencia+"&numcad="+numcad+"&nomfun="+nomfun,
	                                                success: function(result){
	                                                	$('#dtObservacoes').datagrid('loadData',result);
	                                                	$('#dtObservacoes').datagrid('load',result);
	                                                	$('#tab').tabs('select',0);
	                                                	$("#idObservacoes").dialog("open");
	                                                	document.getElementById("idPosto").value = neoIdPosto;
	                                                	document.getElementById("idDescMed").value= descMed;
	                                                	document.getElementById('idHorTra').value = horTra;
	                                                	document.getElementById('idMinTra').value = minTra;
	                                                	document.getElementById('idHorEx').value  = horEx;
	                                                	document.getElementById('idMinEx').value  = minEx;
	                                                 	document.getElementById('observacao').value = '';
												 		document.getElementById("neoIdObs").value = '';
	                                                	numPosGlo = 0;
	                                                	numPosGlo = numpos;
	                                                	numCadGlo = numcad;
	                                                	nomFunGlo = nomfun;
	                                                	numLocGlo = numloc;
	                                                }
	                                            });
										 		stopProgress();
										 		
										 	}
										 	
										 	function editarObs(neoId){
										 		document.getElementById("neoIdObs").value = neoId;
										 		jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: "action=editarObservacao&neoId="+neoId,
	                                                success: function(result){
	                                                		var obs = result.descricao;
	                                                		document.getElementById("observacao").value = obs;
	                                                		var falta = result.isfaltaString;
	                                                		if(falta === "Sim"){
	                                                			document.getElementById("falta").selectedIndex = 0;	
	                                                		}else{
	                                                			document.getElementById("falta").selectedIndex = 1;
	                                                		}
	                                                }
	                                            });
										 	}
										 	
										 	function radio(valor){
                                         	   document.getElementById("result").value = valor;
                                            }
										 	
										 	function cbTodos(valor){
	                                         	var valorAnterior = document.getElementById("idCbtodos").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idCbtodos").value = '';
	                                         	}else{
	                                         		document.getElementById("idCbtodos").value = valor;
	                                         	}
										    }
										 	function cbVagas(valor){
	                                         	var valorAnterior = document.getElementById("idVagas").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idVagas").value = '';
	                                         	}else{
	                                         		document.getElementById("idVagas").value = valor;
	                                         	}
										    }
										 	function cbFaltas(valor){
	                                         	var valorAnterior = document.getElementById("idFaltas").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idFaltas").value = '';
	                                         	}else{
	                                         		document.getElementById("idFaltas").value = valor;
	                                         	}
										    }
										 	function cbDemitidos(valor){
	                                         	var valorAnterior = document.getElementById("idDemitidos").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idDemitidos").value = '';
	                                         	}else{
	                                         		document.getElementById("idDemitidos").value = valor;
	                                         	}
										    }
										 	function cbOk(valor){
	                                         	var valorAnterior = document.getElementById("idCbOk").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idCbOk").value = '';
	                                         	}else{
	                                         		document.getElementById("idCbOk").value = valor;
	                                         	}
										    }
										 	
										 	function cbTodosPosto(valor){
	                                         	var valorAnterior = document.getElementById("idCbtodosPosto").value;
	                                         	
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idCbtodosPosto").value = '';
	                                         	}else{
	                                         		document.getElementById("idCbtodosPosto").value = valor;
	                                         	}
										    }
										 	function cbVagasPosto(valor){
	                                         	var valorAnterior = document.getElementById("idVagasPosto").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idVagasPosto").value = '';
	                                         	}else{
	                                         		document.getElementById("idVagasPosto").value = valor;
	                                         	}
										    }
										 	function cbFaltasPosto(valor){
	                                         	var valorAnterior = document.getElementById("idFaltasPosto").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idFaltasPosto").value = '';
	                                         	}else{
	                                         		document.getElementById("idFaltasPosto").value = valor;
	                                         	}
										    }
										 	function cbDemitidosPosto(valor){
	                                         	var valorAnterior = document.getElementById("idDemitidosPosto").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idDemitidosPosto").value = '';
	                                         	}else{
	                                         		document.getElementById("idDemitidosPosto").value = valor;
	                                         	}
										    }
										 	function cbOkPosto(valor){
	                                         	var valorAnterior = document.getElementById("idCbOkPosto").value;
	                                         	if(valorAnterior == valor){
	                                         		document.getElementById("idCbOkPosto").value = '';
	                                         	}else{
	                                         		document.getElementById("idCbOkPosto").value = valor;
	                                         	}
										    }
										 	
										 	function radioPosto(valor){
	                                         	   document.getElementById("resultPosto").value = valor;
	                                        }
										 	
										 	
										 	function listaDePostos(cliente,nomeColaborador,numeroPosto,janela,cbTodos,cbVagas,cbFalta,cbDemit,cbOk,numOfi){
										 		startProgress();
										 		
										 		if(janela === ''){
										 			janela = true;
										 		}
										 		
										 		if(janela == undefined || janela === ''){
											 		$( '#idTodosPosto' ).prop( "checked" , false);
	                            	               	$( '#idVagasPosto' ).prop( "checked" , false);
	                        	                   	$( '#idFaltasPosto' ).prop( "checked" , false);
	                    	                       	$( '#idDemitidosPosto' ).prop( "checked" , false);
	                	                           	$( '#idCbOkPosto' ).prop( "checked" , false);
                	                        		document.getElementById("idTodosPosto").value = '';
	        	                                   	document.getElementById("idVagasPosto").value = '';
	    	                                       	document.getElementById("idFaltasPosto").value = '';
		                                           	document.getElementById("idDemitidosPosto").value = '';
		                                           	document.getElementById("idCbOkPosto").value = '';
		                                       }
										 		competencia = document.getElementById('competencias').value;
										 		 document.getElementById('nomeCliente').value = cliente;
										 		 if(numeroPosto == undefined){
										 			numeroPosto = "";
										 		 }
										 		 if(nomeColaborador == undefined){
										 			nomeColaborador = "";
										 		 }
										 		if(cbTodos == undefined){
										 			cbTodos = "";
										 		 }
										 		if(cbVagas == undefined){
										 			cbVagas = "";
										 		 }
										 		if(cbFalta == undefined){
										 			cbFalta = "";
										 		 }
										 		if(cbDemit == undefined){
										 			cbDemit = "";
										 		 }
										 		if(cbOk == undefined){
										 			cbOk = "";
										 		 }
										 		
										 		if(numOfi == undefined){
										 			numOfi = ""; 
										 		}
										 		numOfiCtr = numOfi;
										 		var data = "action=listaPostos&competencia="+competencia+"&cliente="+cliente+"&nomeColaborador="+nomeColaborador+"&numeroPosto="+numeroPosto;
										 		data = data + "&cbTodos="+cbTodos+"&cbVagas="+cbVagas+"&cbFalta="+cbFalta+"&cbDemit="+cbDemit+"&cbOk="+cbOk+"&numOfi="+numOfi;
										 		
										 		jQuery.ajax({
										 			method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
	                                                dataType: 'json',
	                                                data: data,
	                                                
                                                	success: function(result){
                                                		var x = document.getElementById("dtPostos").rows.length;
                                                		var i = 0;
                                                		for(i; i < x; x--){
                                                			if(x === 1){
                                                				break;	
                                                			}
                                                			document.getElementById("dtPostos").deleteRow(i);
                                                		}
                                                		var table = document.getElementById("dtPostos");
	                                                	var header = table.createTHead();
	                                                	var rowHeader = header.insertRow(0); 
	                                                	var cellHeader0 = rowHeader.insertCell(0);
	                                                	var cellHeader1 = rowHeader.insertCell(1);
	                                                	var cellHeader2 = rowHeader.insertCell(2);
	                                                	var cellHeader3 = rowHeader.insertCell(3);
	                                                	var cellHeader4 = rowHeader.insertCell(4);
	                                                	var cellHeader5 = rowHeader.insertCell(5);
	                                                	var cellHeader6 = rowHeader.insertCell(6);
	                                                	var cellHeader7 = rowHeader.insertCell(7);
	                                                	var cellHeader8 = rowHeader.insertCell(8);
	                                                	var cellHeader9 = rowHeader.insertCell(9);
	                                                	var cellHeader10 = rowHeader.insertCell(10);
	                                                	var cellHeader11 = rowHeader.insertCell(11);
	                                                	var cellHeader12 = rowHeader.insertCell(12);
	                                                	var cellHeader13 = rowHeader.insertCell(13);
	                                                	
	                                                	
	                                                	cellHeader0.innerHTML = "<b>N� Posto</b>";
	                                                	cellHeader1.innerHTML = "<b>Local</b>";
	                                                	cellHeader2.innerHTML = "<b>N� Local</b>";
	                                                	cellHeader3.innerHTML = "<b>Vagas</b>";
	                                                	cellHeader4.innerHTML = "<b>Colab.</b>";
	                                                	cellHeader5.innerHTML = "<b>Centro de custo</b>";
	                                                	cellHeader6.innerHTML = "";
	                                                	cellHeader7.innerHTML = "";
	                                                	cellHeader8.innerHTML = "";
	                                                	cellHeader9.innerHTML = "<b>Escala</b>";
	                                                	cellHeader10.innerHTML ="";
	                                                	cellHeader11.innerHTML ="<b>Alertas</b>";
	                                                	cellHeader12.innerHTML ="<b>A��es</b>";
	                                                	cellHeader13.innerHTML ="";
	                                                	for(var i in result){
	                                                    	var row = table.insertRow(1);
	                                                    	row.setAttribute("STYLE","color:white");;
	                                                    	row.setAttribute("bgcolor","#6b7486");
	                                                    	var cell1 = row.insertCell(0);
	                                                    	cell1.setAttribute("width","70");
	                                                    	var cell2 = row.insertCell(1);
	                                                    	cell2.setAttribute("width","320");
		                                                    var cell3 = row.insertCell(2);
		                                                    cell3.setAttribute("width","70");
		                                                    var cell4 = row.insertCell(3);
		                                                    cell4.setAttribute("width","50");
		                                                    var cell5 = row.insertCell(4);
		                                                    cell5.setAttribute("width","70");
		                                                    var cell6 = row.insertCell(5);
		                                                    cell6.setAttribute("width","70");
		                                                    var cell7 = row.insertCell(6);
		                                                    cell7.setAttribute("width","70");
		                                                    var cell8 = row.insertCell(7);
		                                                    cell8.setAttribute("width","70");
		                                                    var cell9 = row.insertCell(8);
		                                                    cell9.setAttribute("width","10");
		                                                    var cell10 = row.insertCell(9);
		                                                    cell10.setAttribute("width","180");
		                                                    var cell11 = row.insertCell(10);
		                                                    cell11.setAttribute("width","5");
		                                                    var cell12 = row.insertCell(11);
		                                                    cell12.setAttribute("width","80");
		                                                    var cell13 = row.insertCell(12);
		                                                    cell13.setAttribute("width","120");
		                                                    
		                                                    cell1.innerHTML = result[i].numPos;
	                                                    	cell2.innerHTML = result[i].nomLoc;
	                                                    	cell3.innerHTML = result[i].numLoc;
	                                                    	cell4.innerHTML = result[i].nVagas;
	                                                    	cell5.innerHTML = result[i].listaColaborador.length;
	                                                    	cell6.innerHTML = result[i].codCcu;
	                                                    	cell7.innerHTML = '';
	                                                    	cell8.innerHTML = '';
	                                                    	cell9.innerHTML = '';
	                                                    	cell10.innerHTML = result[i].codEsc;
	                                                    	cell11.innerHTML = '';
	                                                    	cell12.innerHTML = result[i].imgAlertas;
	                                                    	cell13.innerHTML = result[i].linkAddColaborador + " " +result[i].linkVisualizarObsPosto + " " + result[i].linkRemoverPosto;
	                                                    	
	                                                    	var objCol = result[i].listaColaborador;
	                                                    	var rowW = table.insertRow(2);
	                                                    	var cell11 = rowW.insertCell(0);
		                                                    var cell22 = rowW.insertCell(1);
		                                                    var cell33 = rowW.insertCell(2);
		                                                    var cell44 = rowW.insertCell(3);
		                                                    var cell55 = rowW.insertCell(4);
		                                                    var cell66 = rowW.insertCell(5);
		                                                    var cell77 = rowW.insertCell(6);
		                                                    var cell88 = rowW.insertCell(7);
		                                                    var cell99 = rowW.insertCell(8);
		                                                    var cellEsc = rowW.insertCell(9);
		                                                    var cellAlerta = rowW.insertCell(10);
		                                                    var cellAcao = rowW.insertCell(11);
		                                                    
		                                                    cell11.innerHTML = "<b>Matricula</b>";
		                                                    cell22.innerHTML = "<b>Nome</b>";
		                                                    cell33.innerHTML = "<b>C.H Posto</b>";
		                                                    cell44.innerHTML = "<b>Fun��o</b>";
		                                                    cell55.innerHTML = "<b>Data Admiss�o</b>";
		                                                    cell66.innerHTML = "<b>Situa��o</b>";
		                                                    cell77.innerHTML = "<b>Desc.</b>";
		                                                    cell88.innerHTML = "<b>Hora(s) trab.</b>";
		                                                    cell99.innerHTML = "<b>Hora(s) Ext.</b>";
		                                                    cellEsc.innerHTML = "";
		                                                    cellAlerta.innerHTML = "";
		                                                    cellAcao.innerHTML = "";
		                                                    
		                                                     for(var j in objCol){
		                                                    	var row3 = table.insertRow(3);
		                                                    	var cell100 = row3.insertCell(0);
			                                                    var cell200 = row3.insertCell(1);
			                                                    var cell300 = row3.insertCell(2);
			                                                    var cell400 = row3.insertCell(3);
			                                                    var cell500 = row3.insertCell(4);
			                                                    var cell600 = row3.insertCell(5);
			                                                    var cell700 = row3.insertCell(6);
			                                                    var cell800 = row3.insertCell(7);
			                                                    var cell900 = row3.insertCell(8);
			                                                    var cell1000 =row3.insertCell(9);
			                                                    var cell1001 =row3.insertCell(10);
			                                                    var cell1002 =row3.insertCell(11);
			                                                    var cell1003 =row3.insertCell(12);
			                                                    
			                                                    cell100.innerHTML = objCol[j].numCadColaborador;
		                                                    	cell200.innerHTML = objCol[j].nomeColaborador;
		                                                    	cell300.innerHTML = objCol[j].cargaHorPos;
		                                                    	cell400.innerHTML = objCol[j].funcao;
		                                                    	cell500.innerHTML = objCol[j].datAdm;
		                                                    	cell600.innerHTML = objCol[j].situacao;
		                                                    	cell700.innerHTML = objCol[j].descMed;
		                                                    	cell800.innerHTML = objCol[j].horMed+' : '+objCol[j].minMed;
		                                                    	cell900.innerHTML = objCol[j].horEx+' : '+objCol[j].minEx;
		                                                    	cell1000.innerHTML = objCol[j].nomEsc;
		                                                    	cell1001.innerHTML = '';
		                                                    	cell1002.innerHTML = objCol[j].imgAlertas;
		                                                    	cell1003.innerHTML = objCol[j].linkConsideracoes +' '+ objCol[j].linkRemoveColaborador;
		                                                    } 
		                                                    
		                                         	     }
	                                                	if(janela != false){
	                                                	   	$("#add").dialog("open");
	                                                	}
	                                                	stopProgress();
	                                                }
	                                            });
										 	}
										 	
										 	
										 	$('#tab').tabs({
												onSelect : function(title, index) {
													var id = "";
													var data = "";
													switch (index) {
														case 1:
															var clienteDiaria = document.getElementById('nomeCliente').value;
															id = "#dtDiarias";
															data = "action=listaDiarias&cliente="+"&competencia="+competencia;
															break;
															
														case 2:
															id = "#dtColaboradoresDem";
															data = "action=listaColaboradoresDemitidosDoPosto&numPos="+numPosGlo+"&competencia="+competencia;
															break;
													
														case 3:
															id = "#dgCobertura";
															data = "action=listaCoberturaPosto&numloc="+numLocGlo;
															break;
															
														case 4:
															id = "#dgEventoX8";
															data = "action=listaEventoX8&numloc="+numLocGlo+"&numpos="+numPosGlo;
															break;
														
														default:
															break;															
														
													}
													if(index > 0 && index < 5){
														startProgress();	
														jQuery.ajax({
			                                            	method: 'post',
			                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao',
			                                                dataType: 'json',
			                                                data: data,
			                                                success: function(result){
			                                                	$(id).datagrid('loadData',result);
			                                                	$(id).datagrid('load',result);
			                                                	stopProgress();
			                                                }
			                                            });
													  }
												}
											});
										 	
										 	function limparCamposColaboradores(){
										 		document.getElementById('campoCargaHorPos').value = '';
										    	document.getElementById('campoFuncao').value = '';
										    	document.getElementById('campoLocal').value = '';
										    	document.getElementById('campoMatricula').value = '';
										    	document.getElementById('campoNome').value = '';
										    	document.getElementById('campoCentroCusto').value = '';
										    	document.getElementById('campoDatAdm').value = '';
										    	document.getElementById('campoDescMed').value = '';
										    	document.getElementById('campoSituacao').value = '';
										    	document.getElementById('campoHorTra').value = '';
										    	document.getElementById('campoMinTra').value = '';
										    	document.getElementById('campoHorEx').value = '';
										    	document.getElementById('campoMinEx').value = '';
										    	document.getElementById('idDoPosto').value = '';
										 	}
										 	$(document).ready(function() {
										 		document.getElementById('tab2primary').style.display = "none";
										    });
	
										 	function mostrarDiv(){
										 		document.getElementById('tab2primary').style.display = "block";
										 	}
										 	
										 	function escoderDiv(){
										 		document.getElementById('tab2primary').style.display = "none";
										 	}
										 	
										 	function startProgress(){
									        	$('#dlgLoader').window('open');
									        }
									        
									        function stopProgress(){
									        	$('#dlgLoader').window('close');
									        }
										 	
										 </script>
									</div>
								</div>
							</div>
						</div>
					</div>
	</section>
	<style>
	.campoformulario{
		max-width: 220px;
		min-width: 220px;
	}
	
	.camposCadastroColaborador{
		max-width: 420px;
		min-width: 420px;
	}
	.campoBotao{
		max-width: 120px;
		min-width: 120px;
		background-color:#6b7486;
	    
	}
	.tipoOk { background-image:url(imagens/certo2.png);   }
	.tipoFalta { background-image:url(imagens/exclamacao2.png); }
	

	.table {
    	border-collapse: collapse;
    	width: 100%;
    	class: "";
   }

	.thtd {
    	text-align: left;
    	padding: 8px;
	}

	.tr:nth-child(even){background-color: #f2f2f2}

	.th {
    	background-color: #4CAF50;
    	color: white;
	}
	
	.tableNova {
    border-collapse: collapse;
    width: 100%;
	}

	th {
    background-color: #6b7486;
    color: white;
	}
	</style>	
</body>
</html>


