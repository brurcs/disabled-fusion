<%@page import="com.neomind.fusion.custom.orsegups.sigma.TaticoContaAnalitico"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.util.SortedMap"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>


<%
	final NeoUser currentUser = PortalUtil.getCurrentUser();
	final boolean isAdm = (currentUser!=null&&currentUser.isAdm())?true:false;
	final String none = "&nbsp;";

	DecimalFormat f = new DecimalFormat("000000000");

	DecimalFormat formatoReal = new DecimalFormat("#,##0.00");
	
	String nomTec = NeoUtils.safeString(request.getParameter("nomTec"));
	
	Integer nOrigem = NeoUtils.safeInteger(request.getParameter("nOrigem"));
	
	Integer i = nomTec.indexOf("%20-%20");
	
	i = i + 10;
	
	Integer j = nomTec.length();
	
	nomTec = nomTec.substring(i, j);
	
	nomTec = nomTec.replaceAll("%20", " "); 
	
	nomTec = nomTec.replace("[","[[]");
		
	String origem = "";
	
	System.out.println(nomTec);
	
	//1 - ALARME || 2 - CFTV
		
	if(nOrigem == 1)
	    origem = "(PARTICAO NOT LIKE '098' AND PARTICAO NOT LIKE '099')";
	else
	    origem = "(PARTICAO LIKE '098' OR PARTICAO LIKE '099')";
	    
		
%>

<portal:head title="Contas x T�tico">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css">
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/overlib/overlib.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<cw:main>
		<cw:header title="Anal�tico das Contas Por Rota" />
		<cw:body id="area_scroll">
		
			<%
				Connection conn = null;
				PreparedStatement pstm = null;
				ResultSet rs = null;
				
				String descricaoRota = "";
				
				String extraStyle = "style='background-color: #FFEEDD;'";
				try
				{

						conn = PersistEngine.getConnection("SIGMA90");

						StringBuilder sql = new StringBuilder();
						sql.append("SELECT NM_FANTASIA, ID_CENTRAL, RAZAO																																");
						sql.append("	FROM (SELECT DISTINCT t.CD_COLABORADOR, t.NM_COLABORADOR, c.ID_EMPRESA, c.ID_CENTRAL, c.RAZAO, emp.NM_FANTASIA, CASE WHEN PARTICAO LIKE '098' OR PARTICAO		");
						sql.append("		LIKE '099' THEN 'CFTV' ELSE 'ALARME' END Tipo, DFAP.valorCFTV AS valorCFTV, DFAP.valorAlarme AS valorAlarme, DFAPMO.codigo AS modalidadePag					");
						sql.append("		FROM dbCENTRAL c WITH (NOLOCK)																																");
						sql.append("			INNER JOIN EMPRESA emp WITH (NOLOCK) ON c.ID_EMPRESA = emp.CD_EMPRESA  																					");
						sql.append("			INNER JOIN COLABORADOR t WITH (NOLOCK) ON t.CD_COLABORADOR = c.CD_TECNICO_RESPONSAVEL  																	");									  								  				      
						sql.append("			INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.X_SIGMACOLABORADOR XCOL WITH (NOLOCK) ON T.CD_COLABORADOR = XCOL.CD_COLABORADOR							"); 							  								  				      
						sql.append("			INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_FAPAplicacaoTecnico DFAP WITH (NOLOCK) ON XCOL.NEOID = DFAP.TECNICO_NEOID								");
						sql.append("			INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_FAPTecnicoModalidadePagamento DFAPMO WITH (NOLOCK) ON DFAP.MODALIDADEPAG_NEOID = DFAPMO.NEOID 			");							  						  				      
						sql.append("		WHERE c.CTRL_CENTRAL = 1 AND t.FG_ATIVO_COLABORADOR = 1 AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' AND  								");							 						  				      
						sql.append("			c.ID_CENTRAL NOT LIKE 'FFFF' AND ID_CENTRAL NOT LIKE 'R%' AND NM_COLABORADOR LIKE '%" + nomTec + "%'													");						  						  				      
						sql.append("			AND " + origem																																	 		 );
						sql.append("			AND NOT EXISTS (select sig.usu_codcli  from [FSOODB04\\SQL02].sapiens.dbo.usu_t160ctr ctr  																");
						sql.append("			inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160cvs cvs on cvs.usu_codemp = ctr.usu_codemp 											          		");
						sql.append("				and cvs.usu_codfil = ctr.usu_codfil																													");
						sql.append("    			and cvs.usu_numctr = ctr.usu_numctr 																												");
						sql.append("        	inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160sig sig on sig.usu_codemp = ctr.usu_codemp 															");
						sql.append("		  		and sig.usu_codfil = ctr.usu_codfil																													");
						sql.append("				and sig.usu_numctr = ctr.usu_numctr																													");
						sql.append("				and sig.usu_numpos = cvs.usu_numpos																													");
						sql.append("				where ((ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate()))															");
						sql.append("		  			and ((cvs.usu_sitcvs = 'A') or (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= getdate())) 															");
						sql.append("					and sig.usu_codcli = CD_CLIENTE)) 																												");
						sql.append("					AS base GROUP BY NM_FANTASIA, ID_CENTRAL, RAZAO ORDER BY  1, 2 																					");
								
						pstm = conn.prepareStatement(sql.toString());
						
						//System.out.println("LOG ALEXANDRE: " + sql.toString());
						
						rs = pstm.executeQuery();
						
						List<TaticoContaAnalitico> taticoContasAnalitico= new ArrayList<TaticoContaAnalitico>();
						while (rs.next())
						{
							TaticoContaAnalitico taticoContaAnalitico = new TaticoContaAnalitico();
							
							taticoContaAnalitico.setNomeRota(rs.getString("NM_FANTASIA"));
							taticoContaAnalitico.setCtrlCentral(rs.getString("ID_CENTRAL"));
							taticoContaAnalitico.setRazaoSocial(rs.getString("RAZAO"));
							
							descricaoRota = rs.getString("NM_FANTASIA");
							
							taticoContasAnalitico.add(taticoContaAnalitico);
						 }				
						 %>
							<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
							<tr style="cursor: auto">
							<th style="cursor: auto; white-space: normal" width="10%">Empresa Sigma</th>
							<th style="cursor: auto; white-space: normal" width="5%">Conta do Cliente</th>
							<th style="cursor: auto; white-space: normal" width="5%">Raz�o Social do Cliente</th>
							</tr>
							<fieldset class="fieldGroup" >
							<legend class="legend" style="font-size: 1.2em">&nbsp;<%= descricaoRota %>&nbsp;
							</legend>
							<tbody>
							<%
							for (TaticoContaAnalitico taticoObj: taticoContasAnalitico){
							%>
								<tr>
									<td style="white-space: normal; text-align: left"><%=taticoObj.getNomeRota()%></td>
									<td style="white-space: normal; text-align: left"><%=taticoObj.getCtrlCentral()%></td>
									<td style="white-space: normal; text-align: left"><%=taticoObj.getRazaoSocial()%></td>
								</tr>
							<%
							}
							%>
					</tbody>
				</table>
			</fieldset>	
			<br />
		
				<%
					} catch (Exception e) {
						e.printStackTrace();
				%>
				<script type="text/javascript">
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
				<%
					}finally {
						try {
										
						OrsegupsUtils.closeConnection(conn, pstm, rs);
						} catch (Exception e) {
						e.printStackTrace();
				%>
				<script type="text/javascript">
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>", 'error');
				</script>
				<%
					}
					}
				%>
			

		</cw:body>
	</cw:main>
</portal:head>