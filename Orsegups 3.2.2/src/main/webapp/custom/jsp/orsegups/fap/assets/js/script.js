// #################################
// ########## Form Submit ##########
// #################################
$( "#formVotacaoProposicoes" ).submit(function( event ) {
	salvarProposicoes(true);
	event.preventDefault();
});

// ########################################
// ########## Eventos dos botões ##########
// ########################################
$('#btn-salvar').click(function() {
	salvarProposicoes(false);
});

$('.btn-a-favor').click(function() {
	updateSelectsNRadios("favor", 1, this);
	return false;
});

$('.btn-contra').click(function() {
	updateSelectsNRadios("contra", 2, this);
	return false;
});

$('.btn-ausente').click(function() {
	updateSelectsNRadios("ausente", "", this);
	return false;
});

$('.btn-limpar').click(function() {
	var btn = this;
	modalConfirm("Atenção",
				 "Tem certeza que deseja limpar as respostas dos vereadores?",
			 	 "Sim",
			 	 (function (){
			 		 updateSelectsNRadios(null, "", btn);
			 		 $('#modalWindow').modal('hide');
		 		 }), 
			 	 "Não", 
			 	 null
		 	 );
	return false;
});


function updateSelectsNRadios(radioValue, selectValue, button){
	var proposicaoNeoId = $(button).attr('proposicaoNeoId');
	var tipoProposicao = $(button).attr('neoTipoProposicao');
	
	var statusSelect = $('select[name="status' + proposicaoNeoId + '"]');
	var encaminharSelect = $('select[name="encaminhamento' + proposicaoNeoId + '"]');
	
	if(radioValue == null)
		$(button).parent().parent().parent().parent().find('input:radio').prop('checked', false);
	else
		$('input[neoVoto="' + radioValue + proposicaoNeoId + '"').prop('checked', true);
	

		if (tipoProposicao == "indicacao" || tipoProposicao == "requerimento"
			|| tipoProposicao == "mocao"
			|| tipoProposicao == "pedidoInformacao")
		$(statusSelect).find('option[value="' + selectValue  + '"]').attr('selected', 'selected');
}

function salvarProposicoes(avancaProcesso){
	modalInfo("Aguarde", "Salvando...")
	var returnArray = {};
	returnArray.dataSessao = $('span#dataSessao').html();
	returnArray.pequenoExpediente = preparaExpediente('pequenoExpediente'); 
	returnArray.grandeExpediente = preparaExpediente('grandeExpediente');

	var returnJsonString = JSON.stringify(returnArray);
	
	var url = $('form').attr('action');
	$.ajax({
		  type: "POST",
		  url: url,
		  data: {action: "votacaoProposicoesPlenario", expedientePlenario: returnJsonString, avancaProcesso: avancaProcesso},
		  success: function(data){
			  setTimeout(function(){ modalAlert(data, null); }, 3000);
		  },
		  error: function(xhr, ajaxOptions, thrownError) {
			  modalAlert("Erro ao salvar votações!", null);
		  }
		});
}

function preparaExpediente(tipoExpediente){
	var expediente = [];
	$("div." + tipoExpediente).each(function(iListaProposicoes, vListaProposicoes) {
		var proposicao = this;
		var tipoProposicao = $(proposicao).attr('id')
		var listaProposicoes = {};
		listaProposicoes.tipo = tipoProposicao;
		listaProposicoes.proposicoes = [];
	    $(proposicao).find('li').each(function(iProposicao, vProposicao){
	    	var proposicao = {};
	    	var proposicaoNeoId = $(this).find('.proposicaoId').html();
	    	proposicao.proposicaoNeoId = proposicaoNeoId;
	    	proposicao.encaminhamento = '';
	    	if($(this).find('select[name="encaminhamento'+proposicaoNeoId+'"]').val()){
	    		proposicao.encaminhamento = $(this).find('select[name="encaminhamento'+proposicaoNeoId+'"]').val()
	    	}
	    	proposicao.status = '';
	    	if($(this).find('select[name="status'+proposicaoNeoId+'"]').val()){
	    		proposicao.status = $(this).find('select[name="status'+proposicaoNeoId+'"]').val()
	    	}
	    	var votoVereadores = [];
	    	$(this).find('.vereadores tbody tr').each(function(iVereador, vVereador){
	    		var votoVereador = {};
	    		votoVereador.voto = '';
	    		if($(this).find('input:radio.votoVereador:checked').val()){
	    			votoVereador.voto = $(this).find('input:radio.votoVereador:checked').val();
	    		}
	    		votoVereador.vereadorNeoId = $(this).find('.vereadorId').val(); 
				votoVereadores[votoVereadores.length] = votoVereador;
			});
	    	proposicao.votoVereadores = votoVereadores
	    	listaProposicoes.proposicoes[listaProposicoes.proposicoes.length] = (proposicao);
	    });
	    expediente[expediente.length] = listaProposicoes;
	});
	return expediente;
}