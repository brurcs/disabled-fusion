function modalAlert(modalTitle, modalText){
	genericModalBox(modalTitle, modalText, 'OK', null, null, null, false, false);
}

function modalInfo(modalTitle, modalText){
	genericModalBox(modalTitle, modalText, null, null, null, null, true, true);
}

function modalConfirm(modalTitle, modalText, txtBtnOK, okAction, txtBtnCancelar, cancelarAction)
{
	genericModalBox(modalTitle, modalText, txtBtnOK, okAction, txtBtnCancelar, cancelarAction, false, true)
}

function genericModalBox(modalTitle, modalText, txtBtnOK, okAction, txtBtnCancelar, cancelarAction, info, small, exibeImgTitle){
	if(small){
		$('#modalWindow').find('.modal-dialog').addClass('modal-sm');
	}else{
		$('#modalWindow').find('.modal-dialog').removeClass('modal-sm');
	}
	
	if(exibeImgTitle){
		$('#modalWindow').find('#imgTitle').show();
	}else{
		$('#modalWindow').find('#imgTitle').hide();
	}
	
	if(info == null) { info = false; }
	if(txtBtnOK == null && !info) { txtBtnOK = 'OK'; }
	if(txtBtnCancelar == null && !info) { txtBtnCancelar = 'Cancelar'; }
	
	if(modalTitle != null){
		$('#modalWindow').find('#modalTitle').html(modalTitle);
		$('#modalWindow').find('.modal-header').show();
	}else{
		$('#modalWindow').find('.modal-header').hide();
	}
	if(modalText != null){
		$('#modalWindow').find('#modalText').html(modalText);
		$('#modalWindow').find('.modal-body').show();
	}else{
		$('#modalWindow').find('.modal-body').hide();
	}
	
	if(cancelarAction == null && okAction == null && txtBtnOK == null && txtBtnCancelar == null){
		$('#modalWindow').find('.modal-footer').hide();
	}else{
		if(cancelarAction == null){
			$('#modalWindow').find('#modalBtnFechar').html(txtBtnCancelar);
			$('#modalWindow').find('#modalBtnFechar').show();
			$('#modalWindow').find('#modalBtnCancelar').hide();
		}else{
			$('#modalWindow').find('#modalBtnCancelar').html(txtBtnCancelar);
			$('#modalWindow').find('#modalBtnFechar').hide();
			$('#modalWindow').find('#modalBtnCancelar').unbind('click');
			$('#modalWindow').find('#modalBtnCancelar').click(cancelarAction);
		}
		
		if(okAction != null){
			$('#modalWindow').find('#modalBtnOk').html(txtBtnOK);
			$('#modalWindow').find('#modalBtnOk').show();
			$('#modalWindow').find('#modalBtnOk').unbind('click');
			$('#modalWindow').find('#modalBtnOk').click(okAction);
		}else{
			$('#modalWindow').find('#modalBtnOk').hide();
			if(cancelarAction == null){
				$('#modalWindow').find('#modalBtnFechar').html(txtBtnOK);
				$('#modalWindow').find('#modalBtnFechar').show();
				$('#modalWindow').find('#modalBtnCancelar').hide();
			}else{
				$('#modalWindow').find('#modalBtnCancelar').html(txtBtnOK);
				$('#modalWindow').find('#modalBtnFechar').hide();
				$('#modalWindow').find('#modalBtnCancelar').show();
				$('#modalWindow').find('#modalBtnCancelar').unbind('click');
				$('#modalWindow').find('#modalBtnCancelar').click(cancelarAction);
			}
		}
		$('#modalWindow').find('.modal-footer').show();
	}
	
	$('#modalWindow').modal('show')
}