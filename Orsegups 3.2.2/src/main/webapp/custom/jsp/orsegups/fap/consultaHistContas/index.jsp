<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.neomind.fusion.portal.PortalUtil"%>

<%@page import="java.util.ArrayList"%>
<%@page import="javax.persistence.Query"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Consulta Histórico de Contas</title>

<!-- <link rel="stylesheet" href="../assets/css/bootstrap-fusion.css"> -->
<!-- <link rel="stylesheet" href="../assets/css/newTheme.css"> -->
<!-- <link rel="stylesheet" -->
<%-- 	href="<%=PortalUtil.getBaseURL()%>css/menu_icons.css"> --%>
<!-- <link rel="stylesheet" -->
<!-- 	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->

<!-- <script src="../assets/js/jquery.min.js"></script> -->
<%-- <script src="<%=PortalUtil.getBaseURL()%>js/jquery/jquery.js.jsp"></script> --%>
<%-- <script src="<%=PortalUtil.getBaseURL()%>js/jquery/jquery-ui.js.jsp"></script> --%>
<%-- <script src="<%=PortalUtil.getBaseURL()%>core/portalutil.js.jsp" /></script> --%>
<%-- <script src="<%=PortalUtil.getBaseURL()%>core/alljs-dynamic.jsp" /></script> --%>
<%-- <script src="<%=PortalUtil.getBaseURL()%>core/alljs-static.jsp" /></script> --%>
<!-- <script src="../assets/js/jquery.validate.min.js"></script> -->
<!-- <script src="../assets/js/jquery-ui.js"></script> -->
<!-- <script src="../assets/js/bootstrap.js"></script> -->
<!-- <script src="../assets/js/ie10-viewport-bug-workaround.js"></script> -->
<!-- <script src="../assets/js/modalBox.js"></script> -->

<link rel="stylesheet" href="../assets/css/bootstrap-fusion.css">
		<link rel="stylesheet" href="../assets/css/newTheme.css">
		<link rel="stylesheet" href="<%=PortalUtil.getBaseURL()%>css/menu_icons.css">
<!-- 		<link rel="stylesheet" href="../assets/css/jquery-ui.css">  -->
		<link rel="stylesheet" href="../assets/css/jquery.ui.autocomplete.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

		<script src="../assets/js/jquery.min.js"></script>
		<script src="<%=PortalUtil.getBaseURL()%>js/jquery/jquery.js.jsp"></script>
		<script src="<%=PortalUtil.getBaseURL()%>js/jquery/jquery-ui.js.jsp"></script>
		<script src="<%=PortalUtil.getBaseURL()%>js/modules/lib/jquery/ui/jquery.ui.autocomplete.js"></script>
		<script src="<%=PortalUtil.getBaseURL()%>core/portalutil.js.jsp" /></script>
		<script src="<%=PortalUtil.getBaseURL()%>core/alljs-dynamic.jsp" /></script>
		<script src="<%=PortalUtil.getBaseURL()%>core/alljs-static.jsp" /></script>
		<script src="../assets/js/jquery.validate.min.js"></script>
		<script src="../assets/js/jquery-ui.js"></script>
		<script src="../assets/js/bootstrap.js"></script>
		<script src="../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../assets/js/modalBox.js"></script>
		<script src="../assets/js/excellentexport.js"></script>
		<script src="../assets/js/autocomplete.js.jsp"></script>
		<script src="../assets/js/jquery.table2excel.min.js"></script>

<script>
			$(function()
			{
				$('#consulta').click(function()
				{
					searchAndRender();
				});
				
				$('#cliente').keyup(function()
				{
					var val = $(this).val();
					var isHashComplet = (val && val.length == 32);
					
					if (isHashComplet)
						searchAndRender(val);
				});
				
				$("#cliente").focus();
				
				$("#competenciaIni").datepicker(
						{
						    dateFormat: 'dd/mm/yy',
						    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
						    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
						    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab','Dom'],
						    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
						    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
						    nextText: 'Próximo',
						    prevText: 'Anterior'
				 		});
				
				$("#competenciaFin").datepicker(
						{
						    dateFormat: 'dd/mm/yy',
						    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
						    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
						    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab','Dom'],
						    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
						    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
						    nextText: 'Próximo',
						    prevText: 'Anterior'
				 		});
				
			});
			
			function searchAndRender()
			{
				var url = "<%=PortalUtil.getBaseURL()%>servlet/ConsultaHistoricoContas";
				
				var formObj = new Object();
				
				formObj.cliente = $("#cliente").val();
				formObj.nomnumrota = $("#nomnumrota").val();
				formObj.situacaoFap = $("#situacaoFap").val();
				formObj.central = $("#central").val();
				formObj.tipo = $("#tipo").val();
				formObj.codigo = $("#codigo").val();
				formObj.competenciaIni = $("#competenciaIni").val();
				formObj.competenciaFin = $("#competenciaFin").val();
				
 				var formData = formData = $("#form").serializeArray();
				
				for (var i = 0; i < formData.length; i++) 
 				{
				eval("formObj." + formData[i].name + " = '" + formData[i].value + "';");
				}
				
				console.log(formObj);
				
				$('#loading').fadeIn("200", function() 
				{
					$('#resultados').hide();
					$("#exportBtnContainer").hide();
					$("#page").css("height", "100%");
					
					selectedCount = 0;
					
					$("#exportCount").html("");
					
					$.ajax(
					{
						url: "<%=PortalUtil.getBaseURL()%>servlet/ConsultaHistoricoContas",        
						type: "GET",
						dataType: "json",      
						contentType: "application/json;charset=UTF-8", 
						data: 
						{
							form: JSON.stringify(formObj),
							type: "10"
							
						},       
						beforeSend:function(xhr)
						{            
							xhr.setRequestHeader("SOAPAction", url);
						},
						success:function(data, textStatus)
						{          
							console.log(data);

							if (data.rows.length > 0) 
							{
								render(data);
								
								$('#loading').fadeOut("300");
								$('#resultados').show();
								$("#exportBtnContainer").show();
								$("#page").css("height", "calc(100% - 35px)");
							} 
							else 
							{
								$('#loading').fadeOut("300");
								
								alert("Nenhum resultado encontrado para busca.");	
							}
											
							$("#page").animate({ scrollTop: $('#resultados')[0].offsetTop }, 300);
						},
						error:function(res, textStatus, errorThrown)
						{
							$('#loading').fadeOut("300");
							
							alert("Ocorreu um erro ao consultar: " + textStatus);
						},
						async: true
					});
				});
			}
			
			function render(data) 
			{
				$('#listaProposicoes').html("");
				
				var table = "<thead class='header'>";
				
				for (var i = 0; i < data.header.length; i++) 
				{
					if (data.header[i].name == "atividade")
						continue;
					
					table += "<th>" + data.header[i].title + "</th>";
				}
				
				table +="</thead><tbody>"
				
				$.each( data.rows, function(index, item) 
				{
					var padraoCVJneoId  = item["padraoCVJneoId"];
					var atividade  		= item["atividade"];
					
					var newRowItem = "<tr id='row-" + padraoCVJneoId + "'>";
		
					for (var i = 0; i < data.header.length; i++) 
					{
						var text = eval("item." + data.header[i].name) || "";
						
						var temp = document.createElement("div");
						
						temp.innerHTML = text;
						
						var sanitized = temp.textContent || temp.innerText;
						
						sanitized = sanitized.split("\n").join("<br>");
						
						newRowItem += "<td class='center-text'>" + sanitized + "</td>";
					}
					
					table += newRowItem + "</tr>";
				});
				
				table += "</tbody>";
				
				$('#listaProposicoes').html(table);
			}
			
		</script>
</head>

<body>
	<div id="loading">
		<div class="preloader">
			<div class="preloaderCircular align-loader-ie loader-size-md">
				<div class="spinner">
					<div class="left"></div>
					<div class="right"></div>
				</div>
			</div>
		</div>
	</div>

	<div div id="page">
		<div class="container-fuild no-padding">
			<ol class="breadcrumb">
				<li><a href="#"
					onclick="window.top.location.href='<%=PortalUtil.getBaseURL()%>';">Início</a></li>
				<li class="active">Consulta Histórico de Contas</li>
			</ol>
		</div>

		<div class="container">
			<div class="panel panel-default">
				<div class="panel-heading">Histórico de Contas</div>

				<div class="panel-body">
					<form class="form-horizontal">
						<div id="form">

							<div class="form-group">
								<label class="col-xs-3 control-label">Cliente:</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" id="cliente"
										name="cliente" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-xs-3 control-label">Nome/Número da
								Rota:</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" id="nomnumrota"
										name="nomnumrota" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-xs-3 control-label">Situação FAP:</label>
								<div class="col-xs-8">
									<select name="situacaoFap" id="situacaoFap" class="form-control">
										<option value="-1" selected>Todos</option>
										<option value="EM EXECU">Em Execução</option>
										<option value="FINALIZADO">Finalizado</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-xs-3 control-label">Central:</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" id="central"
										name="central" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-3 control-label">Tipo:</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" id="tipo"
										name="tipo" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-3 control-label">Código Tarefa:</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" id="codigo"
										name="codigo" />
								</div>
							</div>

							<div id="dataCompetencia" class="form-group">
								<label class="col-xs-3 control-label">Competência:</label>
								<div class="col-xs-4">
									<input type="text" class="form-control" id="competenciaIni" name="competenciaIni" placeholder="Data Início">
								</div>

								<div class="col-xs-4">
									<input type="text" class="form-control" id="competenciaFin" name="competenciaFin" placeholder="Data Fim">
								</div>
							</div>
						</div>
					</form>
				</div>

				<div class="panel-footer">
					<div style="text-align: right;">
						<button type='button' class="btn btn-primary" id="consulta">
							<span><i class="icon-search2"> </i> Consultar</span>
						</button>
					</div>
				</div>
			</div>
		</div>

		<div id="carregando" class="row" style="display: none;">
			<div class="col-md-2 col-md-offset-5">
				<center>
					<img width="100px" src="../assets/img/carregando.gif">
				</center>
			</div>
		</div>

		<div id="resultados" style="display: none;">
			<table id="listaProposicoes" class="table"></table>
		</div>
	</div>
</body>
</html>
