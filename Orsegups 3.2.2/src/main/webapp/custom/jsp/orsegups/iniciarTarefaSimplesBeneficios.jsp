<%@page import="com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="java.util.HashSet"%>
<%
	
	String solicitante = request.getParameter("solicitante");
	String avanca = request.getParameter("avanca");
	String sOrigem = request.getParameter("origem");
		
	InstantiableEntityInfo eBeneficios = AdapterUtils.getInstantiableEntityInfo("TipoBeneficios");	
	String tipoBeneficio = request.getParameter("tipo");
	if(tipoBeneficio == null) {
		out.print("Erro #2 - Tipo de benef�cio inv�lido (#"+tipoBeneficio+")");
		return;
	}
	NeoObject beneficio = (NeoObject) PersistEngine.getObject(eBeneficios.getEntityClass(), new QLEqualsFilter("codigo", Long.valueOf(tipoBeneficio)));
	if(beneficio == null) {
		out.print("Erro #3 - Tipo Beneficio para Tarefa n�o encontrado");
		return;
	}
	
	String executor = request.getParameter("executor");
	Long numemp = Long.parseLong(request.getParameter("numemp").trim());
	Long tipcol = Long.parseLong(request.getParameter("tipcol").trim());
	Long numcad = Long.parseLong(request.getParameter("numcad").trim());
	
	//verifica se existe tarefa simples para benef�cio
	InstantiableEntityInfo tarefaBeneficio = AdapterUtils.getInstantiableEntityInfo("TarefaBeneficios");
	// filtro para pegar as atividades
	QLGroupFilter qFiltro = new QLGroupFilter("AND");
	qFiltro.addFilter(new QLEqualsFilter("numemp",numemp));
	qFiltro.addFilter(new QLEqualsFilter("tipcol",tipcol));
	qFiltro.addFilter(new QLEqualsFilter("numcad",numcad));
	qFiltro.addFilter(new QLEqualsFilter("beneficio",tipoBeneficio));
	List<NeoObject> listaBeneficio = (List<NeoObject>) PersistEngine.getObjects(tarefaBeneficio.getEntityClass(), qFiltro);
	if(listaBeneficio.size() == 0) {
	
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String sPrazo = request.getParameter("prazo");
		String slPrazo = request.getParameter("prazoDias");
		
		Long lPrazo = 0L;
		
		if (NeoUtils.safeIsNotNull(slPrazo))
		{
			lPrazo = Long.parseLong(slPrazo);
		}
		
		GregorianCalendar prazo = null;	
		GregorianCalendar dataHoje = new GregorianCalendar();

		if (lPrazo != 0L)
		{
			prazo = OrsegupsUtils.getSpecificWorkDay(dataHoje, lPrazo);
		}
		else if (sPrazo != null)
		{
			prazo = AdapterUtils.getGregorianCalendar(sPrazo, "dd/MM/yyyy");
		}
		
		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
		String retorno = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, sOrigem, avanca, prazo);

		InstantiableEntityInfo tarefaTB = AdapterUtils.getInstantiableEntityInfo("TarefaBeneficios");
		NeoObject noTB = tarefaTB.createNewInstance();
		EntityWrapper ajWrapper = new EntityWrapper(noTB);

		ajWrapper.findField("numemp").setValue(numemp);
		ajWrapper.findField("tipcol").setValue(tipcol);
		ajWrapper.findField("numcad").setValue(numcad);
		ajWrapper.findField("beneficio").setValue(tipoBeneficio);
		ajWrapper.findField("tarefa").setValue(retorno);
		PersistEngine.persist(noTB);
	
		out.print(retorno);
	}
%>