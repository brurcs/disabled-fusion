<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.io.UnsupportedEncodingException"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.io.IOException"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.testeCotacao");


InstantiableEntityInfo infoURL = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByString("urlsSapiensCotacao");
List<NeoObject> obj = (List<NeoObject>) PersistEngine.getObjects(infoURL.getEntityClass());

if (infoURL != null)
{
	EntityWrapper wrpURL = new EntityWrapper(obj.get(0));
	String urlLogin = (String) wrpURL.getValue("login");
	String urlAlterar = (String) wrpURL.getValue("alterar");
	String urlGravacao = (String) wrpURL.getValue("atualiza");
	String urlAjuste = (String) wrpURL.getValue("ajusta");
	
	Boolean liberaCotacao =true ;
	
	if (liberaCotacao)
	{
		
		Long numsol =  4012L;
		Long seqsol = 1L;
		Long codfor = 1696L;
		Long numcot = 13428L;
		Long prazoEntrega = 20L;
					
		BigDecimal qtdCotada = new BigDecimal("1");
		BigDecimal precot = new BigDecimal("12.00");
		
		String obs = "teste de cotacao online";
		String code = "C";
	
	    String cp = "1530";
	
		//Encode de caracteres para executar uma URL
		try
		{
			obs = java.net.URLEncoder.encode(obs, "ISO-8859-1");
		}
		catch (UnsupportedEncodingException e1)
		{
			e1.printStackTrace();
		} 
		
		urlGravacao = urlGravacao.concat("&NumSol=" + numsol);
		urlGravacao = urlGravacao.concat("&SeqSol=" + seqsol);
		urlGravacao = urlGravacao.concat("&CodFor=" + codfor);
		urlGravacao = urlGravacao.concat("&QtdCot=" + qtdCotada);
		urlGravacao = urlGravacao.concat("&PreCot=" + precot);
		urlGravacao = urlGravacao.concat("&PrzEnt=" + prazoEntrega);
		urlGravacao = urlGravacao.concat("&TipRet=0");
		urlGravacao = urlGravacao.concat("&CodCpg=" + cp);
		urlGravacao = urlGravacao.concat("&ObsCot=" + obs);
		urlGravacao = urlGravacao.concat("&CifFob=" + code);
		urlGravacao = urlGravacao.concat("&USER=sapienssid");
		urlGravacao = urlGravacao.concat("&CONNECTION=");
	
		urlAjuste = urlAjuste.concat("&NumReg=170");
		urlAjuste = urlAjuste.concat("&NumCot=" + numcot);
		urlAjuste = urlAjuste.concat("&CodFor=" + codfor);
		urlAjuste = urlAjuste.concat("&USER=sapienssid");
		urlAjuste = urlAjuste.concat("&CONNECTION=");
	
		log.warn("URL de Login: " + urlLogin);
		log.warn("URL de altera: " + urlAlterar);
		log.warn("URL de grava��o: " + urlGravacao);
		log.warn("URL de Ajuste: " + urlAjuste);
		
		log.warn("Dados da cota��o: NumSol: "+numsol+" SeqSol: "+seqsol+" CodFor: "+codfor+" QtdCot: "+qtdCotada+" PreCot "+precot+ " PrzEnt: "+prazoEntrega+ " CodCpg " +cp+ "ObsCot "+obs+ "CifFob "+ code);
		
		try
		{
			log.warn("Iniciando nova cota��o: N�mero Cota��o #"+ numcot +" C�digo fornecedor #"+codfor);
			URL url = new URL(urlLogin);	
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String menssagemSapiensSID="",menssagemSapiensSID2="",menssagemSapiensSID3="",menssagemSapiensSID4="";
			Boolean efetivada=false;
			while ((menssagemSapiensSID = in.readLine()) != null)
			{
				if (!menssagemSapiensSID.equalsIgnoreCase(""))
				{
					String codeEntrada = menssagemSapiensSID;
					
					URL urlAlteracao = new URL(urlAlterar + codeEntrada);
						
					BufferedReader in2 = new BufferedReader(new InputStreamReader(urlAlteracao.openStream()));
					
					
					while ((menssagemSapiensSID2 = in2.readLine()) != null)
					{
						if (menssagemSapiensSID2.equalsIgnoreCase("OK"))
						{		
							log.warn("Passou gravar");
							URL urlAtualizacao = new URL(urlGravacao + codeEntrada);
							
							BufferedReader in3 = new BufferedReader(new InputStreamReader(urlAtualizacao.openStream()));
							
							
							while ((menssagemSapiensSID3 = in3.readLine()) != null)
							{
								if (menssagemSapiensSID3.equalsIgnoreCase("OK"))
								{											
									URL urlAjustes = new URL(urlAjuste + codeEntrada);										
									BufferedReader in4 = new BufferedReader(new InputStreamReader(urlAjustes.openStream()));
									
									log.warn("URL ajuste sapiens");
									
									while ((menssagemSapiensSID4 = in4.readLine()) != null)
									{
										if (menssagemSapiensSID4.equalsIgnoreCase("OK"))
										{
											log.warn("Cota��o realizada CONNECTION #"+ codeEntrada + "Numero Cota��o #"+ numcot +"Fornecedor c�digo #"+codfor);
                             				efetivada=true;
										}
										else
										{
											log.warn(menssagemSapiensSID4);
										}
									}
								}
							}
						}
						else
						{
							log.warn("Mensagem Sapiens ALTERA��O"+  menssagemSapiensSID2);
						}
						
					}
				}
				else
				{
					log.warn("Mensagem Sapiens LOGIN "+  menssagemSapiensSID);
				}
			}
			if(!efetivada)
			{
				log.warn("Mensagem Sapiens LOGIN "+  menssagemSapiensSID);
				log.warn("Mensagem Sapiens LOGIN "+  menssagemSapiensSID2);
				log.warn("Mensagem Sapiens LOGIN "+  menssagemSapiensSID3);
				log.warn("Mensagem Sapiens LOGIN "+  menssagemSapiensSID4);
				
			}
			in.close();
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}


%>
</body>
</html>