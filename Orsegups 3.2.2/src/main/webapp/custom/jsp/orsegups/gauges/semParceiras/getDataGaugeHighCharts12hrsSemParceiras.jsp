<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%
	// CONEXAO STAGINGSIGMA OU SIGMA90
	
    String conexaoSelecionada = "STAGINGSIGMA";	

	String raw = request.getParameter("raw");
	boolean rawData = raw != null && raw.trim().equals("yes");
	
	String kpi = request.getParameter("kpi");
	String regional = request.getParameter("regional");
	String regionalTitulo = regional;
	if(regional == null || regional.trim().equals("") || regional.trim().equals("null"))	{
	    regional = "'SOO', 'IAI', 'BQE', 'BNU', 'JLE', 'LGS', 'CUA', 'CCO', 'RSL', 'JGS', 'CTA', 'CSC', 'TRI', 'CAS', 'GNA', 'PMJ', 'SRR', 'XLN' ";
		regionalTitulo = "Geral";
 	} else {
		regional = "'"+regional+"'";
	}
	
	double kpi_value = 0;
	int kpi_qtd = 0;
	StringBuffer select_kpi = null;
 	if (kpi != null && kpi.equals("X406")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_DESARME h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'X406' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010) ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
		
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_DESARME h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'X406' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010) ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
		
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_DESARME h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'X406' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010) ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		
		}
		
		
	} else if (kpi != null && kpi.equals("X5")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX5' AND CD_CODE = 'DST' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		
	    }
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX5' AND CD_CODE = 'DST' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX5' AND CD_CODE = 'DST' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
	} else if (kpi != null && kpi.equals("X2")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX2' AND CD_CODE = 'NAR' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX2' AND CD_CODE = 'NAR' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX2' AND CD_CODE = 'NAR' ");
			select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
	} else if (kpi != null && kpi.equals("DSL")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))  ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))  ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))  ");
		}
	} else if (kpi != null && kpi.equals("ATD")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))  ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))  ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
	} else if (kpi != null && kpi.equals("ATD2")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 							  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND c.ID_RAMO NOT IN (10004,10011,10006,10007,10009,10010,10242,15004) ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 							  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND c.ID_RAMO NOT IN (10004,10011,10006,10007,10009,10010,10242,15004) ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");	
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 							  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
// 			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND c.ID_RAMO NOT IN (10004,10011,10006,10007,10009,10010,10242,15004) ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");	
		}
	} else if (kpi != null && kpi.equals("IDC")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'VID'  ");
			select_kpi.append(" AND h.CD_CODE <> 'PRE'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL5'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL6'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL7'  ");
			select_kpi.append(" AND (h.CD_CODE not in ('ALR','AL4') AND (H.TX_OBSERVACAO_FECHAMENTO not LIKE '%#MOTIVO%')) ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'VID'  ");
			select_kpi.append(" AND h.CD_CODE <> 'PRE'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL5'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL6'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL7'  ");
			select_kpi.append(" AND (h.CD_CODE not in ('ALR','AL4') AND (H.TX_OBSERVACAO_FECHAMENTO not LIKE '%#MOTIVO%')) ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'VID'  ");
			select_kpi.append(" AND h.CD_CODE <> 'PRE'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL5'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL6'  ");
			select_kpi.append(" AND h.CD_CODE <> 'AL7'  ");
			select_kpi.append(" AND (h.CD_CODE not in ('ALR','AL4') AND (H.TX_OBSERVACAO_FECHAMENTO not LIKE '%#MOTIVO%')) ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +")) ");
		}
	} else if (kpi != null && kpi.equals("CFTV")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('VID','PRE','XV4','XV5','XV6') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('VID','PRE','XV4','XV5','XV6') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('VID','PRE','XV4','XV5','XV6') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +")) ");
		}	
    } else if (kpi != null && kpi.equals("PREDITIVA")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('ALR','AL4') AND (H.TX_OBSERVACAO_FECHAMENTO LIKE '%#MOTIVO%') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('ALR','AL4') AND (H.TX_OBSERVACAO_FECHAMENTO LIKE '%#MOTIVO%') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('ALR','AL4') AND (H.TX_OBSERVACAO_FECHAMENTO LIKE '%#MOTIVO%') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +")) ");
		}	
	}else if (kpi != null && kpi.equals("GPRS")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('AL5','AL6','AL7') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('AL5','AL6','AL7') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND h.CD_CODE in ('AL5','AL6','AL7') ");  
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +")) ");
		}	
	}else if (kpi != null && kpi.equals("ESP")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) >= 0 																			  ");
// 			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) >= 0 																			  ");
// 			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) >= 0 																			  ");
// 			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND vtr.NM_VIATURA not like '%PARC%' ");
			select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
			select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +")) ");
		}
		
	}
   
%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%
	Connection connSigma = null;
	PreparedStatement stSeq1 = null;
	ResultSet rsSeq1 = null;
	String retorno = "";
	request.setCharacterEncoding("ISO-8859-1");  
	try {
		connSigma = PersistEngine.getConnection(conexaoSelecionada);		
		StringBuffer sqlSeq = new StringBuffer();
		String grafico = kpi;		
		// KPI
		if (select_kpi != null) {
			sqlSeq = select_kpi;
			stSeq1 = connSigma.prepareStatement(sqlSeq.toString());
			rsSeq1 = stSeq1.executeQuery();
			if (rsSeq1.next()) {
				kpi_value = rsSeq1.getDouble("KPI");
				kpi_qtd = rsSeq1.getInt("QTD");
				retorno +="[{ \"name\": \""+grafico+" - "+kpi_qtd+"\",  \"showInLegend\": \"+true+\", \"data\": ["+kpi_value+"] , \"tooltip\": \"valueSuffix: { "+grafico+" }\"}]"; 
            
			}  
		}
	} catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #5 - Erro ao realizar consulta no banco de dados do Sigma");
		return;    
	} finally{
		try
		{
		OrsegupsUtils.closeConnection(connSigma, stSeq1, rsSeq1);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	if (rawData) {
%><%= kpi_value +";"+kpi_qtd %><%
} else { 
%>
<%= retorno %>
<%
}
%>