<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<!DOCTYPE html>
<html>
<%
	String regional = request.getParameter("regional");
	String kpi = request.getParameter("kpi");
	String width = request.getParameter("width");
	if (width == null) width = "200";		
	String height = request.getParameter("height");
	if (height == null) height = "150";		
	String redFrom = request.getParameter("redFrom");
	if (redFrom == null) redFrom = "8";		
	String redTo = request.getParameter("redTo");
	if (redTo == null) redTo = "10";		
	String yellowFrom = request.getParameter("yellowFrom");
	if (yellowFrom == null) yellowFrom = "5";		
	String yellowTo = request.getParameter("yellowTo");
	if (yellowTo == null) yellowTo = "8";		
	String greenFrom = request.getParameter("greenFrom");
	if (greenFrom == null) greenFrom = "0";		
	String greenTo = request.getParameter("greenTo");
	if (greenTo == null) greenTo = "5";		
	String min = request.getParameter("min");
	if (min == null) min = "0";		
	String max = request.getParameter("max");
	if (max == null) max = "10";
	String showMajorTicks = request.getParameter("showMajorTicks");
	String majorTicks = request.getParameter("majorTicks");
	if (showMajorTicks == null || showMajorTicks.equals("yes")) 
	{
		if (majorTicks == null) 
		{
		  majorTicks = max;
		}

		int iMax = Integer.parseInt(majorTicks);
		majorTicks = "";
		for (int i=0;i<=iMax;i++) 
		{
		  if (i == 0)
		  {
		  majorTicks = ", majorTicks: ['" + i + "'";	  
		  } 
		  else 
		  {
		  majorTicks = majorTicks + ", '"+i+"'";	  
		  }
		  if (i == iMax)
		  {
			majorTicks = majorTicks + "]";
		  } 
		  
		}
	}
	else 
	{
		majorTicks = "";
	}
%>
	
  <head>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
  
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript"
         src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-3d.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script type="text/javascript">
   
	(function(b,a){
	if(!b){
	return
	}var c=b.Chart.prototype,d=b.Legend.prototype;b.extend(c,{legendSetVisibility:function(h){
	var i=this,k=i.legend,e,g,j,m=i.options.legend,f,l;
	if(m.enabled==h){return}m.enabled=h;
	if(!h){d.destroy.call(k);e=k.allItems;
	if(e){for(g=0,j=e.length;g<j;++g){e[g].legendItem=a}}k.group={}}c.render.call(i);
	if(!m.floating){f=i.scroller;
	if(f&&f.render){l=i.xAxis[0].getExtremes();f.render(l.min,l.max)}}},legendHide:function(){
	this.legendSetVisibility(false)},legendShow:function(){
	this.legendSetVisibility(true)},legendToggle:function(){
	this.legendSetVisibility(this.options.legend.enabled^true)}})}(Highcharts));
   
	$(function () {
		var jsonDataPiex = callSync("<%=PortalUtil.getBaseURL()%>/custom/jsp/orsegups/gauges/getDataPieHighCharts.jsp?kpi=<%= kpi %>&greenFrom=<%=greenFrom%>&greenTo=<%=greenTo%>&yellowFrom=<%=yellowFrom%>&yellowTo=<%=yellowTo%>&redFrom=<%=redFrom%>&width=<%=width%>&height=<%=height%>&regional=<%= regional %>");
		var obj3 = jQuery.parseJSON(jsonDataPiex);
		
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 0
            }
        },
        title: {
            text: ""
        },
		credits: {
			enabled: false,
			text: 'ORSEGUPS S.A.',
			href: 'http://www.orsegups.com.br'
		},
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> - Total: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                 dataLabels: {
                    enabled: true,
                    format: '<div style="width:80px;left;font-size: 10px;">{point.percentage:.0f}%</div>',
                    distance: -10,
                    color: "white"
                },
                    showInLegend: true
            }
        },
		
			legend: {
                enabled: false,
                layout: 'vertical',
                align: 'center',
                useHTML: false,
                verticalAlign: 'bottom',
				labelFormatter: function() {
                    return '<div style="width:80px;left;font-size: 10px;">' + this.name + '</div>';
				}
            },
        series: [{
            type: 'pie',
			name: 'Propor��o',
            data: obj3
        }]
    },
    function(chart) 
    {
        $('#toggleBtn').click(function () { chart.legendToggle(); });
    });
		var jsonData = callSync("<%=PortalUtil.getBaseURL()%>/custom/jsp/orsegups/gauges/getDataGaugeHighCharts.jsp?kpi=<%= kpi %>&greenFrom=<%=greenFrom%>&greenTo=<%=greenTo%>&yellowFrom=<%=yellowFrom%>&yellowTo=<%=yellowTo%>&redFrom=<%=redFrom%>&width=<%=width%>&height=<%=height%>&regional=<%= regional %>");

		jsonData = jQuery.parseJSON(jsonData);
		
	    $('#containerGauge').highcharts({
	        chart: {
	            type: 'gauge',
	            plotBackgroundColor: null,
	            plotBackgroundImage: null,
	            plotBorderWidth: 0,
	            plotShadow: false
	        },

	        title: {
	            text: ""
	        },
			credits: {
				enabled: false,
				text: 'ORSEGUPS S.A.',
				href: 'http://www.orsegups.com.br'
			},
	        pane: {
	            startAngle: -150,
	            endAngle: 150,
	            background: [{
	                backgroundColor: {
	                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	                    stops: [
	                        [0, '#FFF'],
	                        [1, '#333']
	                    ]
	                },
	                borderWidth: 0,
	                outerRadius: '109%'
	            }, {
	                backgroundColor: {
	                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	                    stops: [
	                        [0, '#333'],
	                        [1, '#FFF']
	                    ]
	                },
	                borderWidth: 1,
	                outerRadius: '107%'
	            }, {
	                // default background
	            }, {
	                backgroundColor: '#DDD',
	                borderWidth: 0,
	                outerRadius: '105%',
	                innerRadius: '103%'
	            }]
	        },

	        // the value axis
	        yAxis: {
	            min: <%=min%>,
	            max: <%=max%>,

	            minorTickInterval: 'auto',
	            minorTickWidth: 1,
	            minorTickLength: 10,
	            minorTickPosition: 'inside',
	            minorTickColor: '#666',

	            tickPixelInterval: 30,
	            tickWidth: 2,
	            tickPosition: 'inside',
	            tickLength: 10,
	            tickColor: '#666',
	            labels: {
	                step: 2,
	                rotation: 'auto'
	            },
	            title: {
	                text: ""
	            },
	            plotBands: [{
	                from: <%=greenFrom%>,
	                to: <%=greenTo%>,
	                color: '#55BF3B' // green
	            }, {
	                from: <%=yellowFrom%>,
	                to: <%=yellowTo%>,
	                color: '#DDDF0D' // yellow
	            }, {
	                from: <%=redFrom%>,
	                to: <%=redTo%>,
	                color: '#DF5353' // red
	            }]
	        },
			plotOptions: {
            gauge: {
                
                wrap: false
            }
        },
	        series: jsonData
		});
});
	
	
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
	
	
	<div id="containerGauge" style="width: 45%; min-height: 250px;height:50%; margin: 0 auto; float:left;position: solid;"></div>
	
	<div id="container" style="width: 55%; min-height: 220px;height:50%;  margin: 0 auto; float:left;position: solid;"></div>
	<input type="button" value="Legenda" id="toggleBtn" />
	
  </body>
</html>