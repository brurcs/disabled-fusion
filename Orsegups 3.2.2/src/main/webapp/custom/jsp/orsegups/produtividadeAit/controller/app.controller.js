app.controller('getListas', ['mainInfo', '$scope', '$http', '$timeout', function(mainInfo, $scope, $http, $timeout){	

	  $scope.states = []; 
	  
	  $scope.subjectsAux = [];
	  
	  $scope.setCompetenciasHistorico = function(){
		  var data = new Date();
		  
		  if (data.getDate() <= 15){
			  data.setMonth(data.getMonth()-1);
		  }
		  		  
		  var mes =  data.getMonth()+1;
		  
		  if (mes < 10){
			  mes = '0'+mes;
		  }
		  
		  var competencia = mes+'/'+data.getFullYear();
		  
		  $scope.subjectsAux.push(competencia);
		  
		  console.log(competencia);
		  
		  while (competencia != '07/2015'){
			  data.setMonth(data.getMonth()-1);
			  
			  mes =  data.getMonth()+1;
			  
			  if (mes < 10){
				  mes = '0'+mes;
			  }
			  
			  competencia = mes+'/'+data.getFullYear();
			  $scope.subjectsAux.push(competencia);

		  }
		  

	  };
	  
	  $scope.setCompetenciasHistorico();
	  
	  $scope.print = function(){
		  var divToPrint=document.getElementById("dadosProdutividade");
		   newWin= window.open("");
		   newWin.document.write(divToPrint.outerHTML);
		   newWin.print();
		   newWin.close();
	  };
	  
	  $http
      .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/ait")
      .success(function(data){
    	 $scope.selected = undefined;
 		 $scope.states = data;
      })
      .error(function(){
          alert("Não foi possível carregar os dados");
      });
	  
	  $http.get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/competencia")
	    .then(function (response) {
	    $scope.subjects = response.data;
	    $scope.selectedItem = response.data[0];
		    $scope.dropboxitemselected = function (item) {
		        $scope.selectedItem = item;
		    }
		    
	  });

	  $scope.atendimentos = []; 
	  $scope.confirmarContestacaoClass = "btn btn-primary pull-right";
	  $scope.exibirProcessoClass = "btn btn-default hide";
	  $scope.permissoes = [];
	  
	  $scope.numeroProcesso;
	  
	  $scope.sendGet = function() {
		
	     $http
	     .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/atendimentos/"+$scope.selected.cdViatura+"/"+$scope.selectedItem)
	     .success(function(data, status) {
	    	    $scope.orderByField = 'cliente';
	    	    $scope.reverseSort = false;
	    	    $scope.atendimentos = data;
	   	        $scope.getTotal($scope.atendimentos);
	       	    $scope.inactive = true;
	            console.log(data);
	            $(function() {
	             	$('#processing-modal').modal('hide');
	             });
	            
	            
	     })
	     .error(function(){
	          alert("Não foi possível carregar os dados");
	          $(function() {
	           	$('#processing-modal').modal('hide');
	           });
	     });

	     $http
   	     .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/obsevacaoProdutividade/"+$scope.selected.cdViatura+"/"+$scope.selectedItem)
   	     .success(function(data, status) {
   	           
   	            $scope.dadosProdutividade = data;
   	            $scope.txRelato = "";
   	            $scope.txAit = "";
   	            
			       $scope.processo = $scope.dadosProdutividade.wfProcess;
			       
			       $scope.numeroProcesso = $scope.processo;
   			       
   			       if ($scope.txRelato == null){
   			    	   $scope.txRelato = "";
   			       }
   			       $scope.txAit =  $scope.dadosProdutividade.ait;
   			       
   			       if($scope.processo != null){
   			    	  $scope.confirmarContestacaoClass = "btn btn-primary pull-right disabled"
   			    	  $scope.exibirProcessoClass = "btn btn-default";  
   			       }else{
   			    	 $scope.confirmarContestacaoClass = "btn btn-primary pull-right"
   			    	 $scope.exibirProcessoClass = "btn btn-default hide"; 
   			       }
   	 	             
   	            
   	            if($scope.dadosProdutividade.relato != null && $scope.dadosProdutividade.relato != "")
   	            {
	   	             $scope.txRelato =  $scope.dadosProdutividade.relato;
	 	             $scope.txAit =  $scope.dadosProdutividade.ait;
	   	             $(function() {
	   	            	 var botaoContestar = "<a class=\"btn btn-danger pull-right h2\" ng-click=\"sendGetDadosProdutividade()\" href=\"#\" "+
		 	   								  " data-toggle=\"modal\" "+
			 	   					          " data-target=\"#contestar-modal\"><span class=\"glyphicon glyphicon-flag\"></span> "+
			 	   					          " Contestação</a>";
	 	             	$('#btnContestar').html(botaoContestar);
	 	             });
   	            }
   	            else
   	            {
	   	             $scope.txRelato =  $scope.dadosProdutividade.relato;
	 	             $scope.txAit =  $scope.dadosProdutividade.ait;
   	            	$(function() {
	  	            	 var botaoContestar = "<a class=\"btn btn-primary pull-right h2\" ng-click=\"sendGetDadosProdutividade()\" href=\"#\" "+
		 	   								  " data-toggle=\"modal\" "+
			 	   					          " data-target=\"#contestar-modal\"><span class=\"glyphicon glyphicon-exclamation-sign\"></span> "+
			 	   					          " Contestar</a>";
		             	
	  	            	$('#btnContestar').html(botaoContestar);
	   	             });
   	            }
   	            
   	            console.log(data);
   	     })
   	     .error(function(data, status, header, config){
		   	 	 var responn = " Não foi possível carregar os dados "+
				  "<hr />Data: " + data +
		         "<hr />status: " + status +
		         "<hr />headers: " + header +
		         "<hr />config: " + config;
			        alert(responn);
   	     });
	     
	     $scope.dadosAit = null;
	     $http
	     .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/dadosAit/"+$scope.selected.cdViatura)
	     .success(function(data, status) {
	            $scope.dadosAit = data;
	            console.log(data);
	     })
	     .error(function(){
	          alert("Não foi possível carregar os dados");
	     });
	     

	     
	  }
	  
	  $scope.navbar = "navbar";
	  $scope.jumbo = "jumbo"; 
	  
	  $scope.dadosRankingAit;
	  
   	  mainInfo.getPermissoes.then(function(records) {
		  $scope.permissoes = records;
	  });
	  
	  $timeout(function () {
	     var tamanho = Object.keys($scope.permissoes).length
	
	  	  if (tamanho == 1){
	  		  $('button:first').click();
		  }
	  }, 1000);
	  
	  $scope.ranking = function(regional){
	
		  	  console.log(regional);
			  $http
			     .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/rankingProdutividade/"+regional)
			     .success(function(data, status) {
			    	 	$scope.jumbo = "jumbo-hidden";
			            $scope.dadosRankingAit = data;
			            console.log(data);
			            $('#processing-modal').modal('hide');
			     })
			     .error(function(){
			          alert("Não foi possível carregar os dados");
			     });
	  }
	  
	  $scope.detalhes = false;
	  $scope.detalhesClass = "glyphicon glyphicon-chevron-down";
	  
	  $scope.mostrarDetalhes = function(){
		  
		  if ($scope.detalhes){
			  $scope.detalhes = false;   
			  $scope.detalhesClass = "glyphicon glyphicon-chevron-down";
		  }else{
			  $scope.detalhes = true;
			  $scope.detalhesClass = "glyphicon glyphicon-chevron-up";
		  }
		  
	  }
	  
	  $scope.sendGetDadosProdutividade = function() {
		  $scope.txRelato = "";
		  $scope.txAit = "";
		  $http
		  .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/obsevacaoProdutividade/"+$scope.selected.cdViatura+"/"+$scope.selectedItem)
		  .success(function(data, status) {
		       $scope.dadosProdutividade = data;
		       $scope.txRelato =  $scope.dadosProdutividade.relato;      
		       console.log(data);
		  })
		  .error(function(data, status, header, config){
			 var responn = " Não foi possível carregar os dados "+
			  "<hr />Data: " + data +
              "<hr />status: " + status +
              "<hr />headers: " + header +
              "<hr />config: " + config;
		      alert(responn);
		   });
	  }	  //getPermissaoExibicao
	  

	  $scope.count = null;
	  $scope.totalValorDLS = null;
	  $scope.totalValorATD = null;
	  $scope.totalValorGeral = null;
	  
	  $scope.getTotal = function(atendimentoArray){
		  
		  $scope.count = 0.0;
		  $scope.totalValorDLS = 0.0;
		  $scope.totalValorATD = 0.0;
		  $scope.totalValorGeral = 0.0;
		    
		    if(atendimentoArray != null && atendimentoArray.length > 1)
		    {

			    for(var i = 0; i < atendimentoArray.length; i++){
			        var atend = atendimentoArray[i];
			        
			        var valorDLS = 0.0;
					var valorATD = 0.0;
			        
			        if(parseInt(atend.deslocamento) >= 0 && parseInt(atend.deslocamento) <= 10)
			        {
			        	valorDLS = 2.0;
			        }
			        else if(parseInt(atend.deslocamento) > 10 && parseInt(atend.deslocamento) <= 15)
			        {
			        	valorDLS = 1.0;
			        }
			        else if(parseInt(atend.deslocamento) > 15 && parseInt(atend.deslocamento) <= 20)
			        {
			        	valorDLS = 0.50;
			        }
			        
			        if(parseInt(atend.atendimento) >= 0 && parseInt(atend.atendimento) <= 15)
			        {
			        	valorATD = 1.0;
			        }
			        else if(parseInt(atend.atendimento) > 15 && parseInt(atend.atendimento) <= 20)
			        {
			        	valorATD = 0.50;
			        }
			        else if(parseInt(atend.atendimento) > 20 && parseInt(atend.atendimento) <= 25)
			        {
			        	valorATD = 0.25;
			        }
			        
			        $scope.totalValorDLS += valorDLS;
			        $scope.totalValorATD += valorATD;
			        $scope.count++;
			    }
			    $scope.totalValorGeral = $scope.totalValorDLS + $scope.totalValorATD;
		   }
      }
	  
	  $scope.foto = null; 
	  $scope.sendGetFoto = function(x) {
	     $http
	     .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/foto/"+$scope.selected.cdViatura)
	     .success(function(data, status) {
	            $scope.foto = data;
	            $scope.evento = x;
	            console.log(data);
	     })
	     .error(function(){
	          alert("Não foi possível carregar os dados");
	     });
	  } 
	  	  
	  $scope.retorno = null; 
	  $scope.sendSetRelato = function(x) {
		  
		 if($scope.txRelato == null || $scope.txRelato === "")
	     {
			 $scope.txRelato = "null";
			 $(function() {
  	            	 var botaoContestar = "<a class=\"btn btn-primary pull-right h2\" ng-click=\"sendGetDadosProdutividade()\" href=\"#\" "+
	 	   								  " data-toggle=\"modal\" "+
		 	   					          " data-target=\"#contestar-modal\"><span class=\"glyphicon glyphicon-exclamation-sign\"></span> "+
		 	   					          " Contestar</a>";
	             	$('#btnContestar').html(botaoContestar);
	             	
	         });
	     }
	     $http
	     .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/relatoAtendimento/"+$scope.selected.cdViatura+"/"+$scope.selectedItem+"/"+$scope.txRelato)
	     .success(function(data, status) {
	            $scope.retorno = data;
	            console.log(data);
	            $(function() {
		           	$('#contestar-modal').modal('hide');
		        });
	            
	            $http
	      	     .get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/obsevacaoProdutividade/"+$scope.selected.cdViatura+"/"+$scope.selectedItem)
	      	     .success(function(data, status) {
	      	           
	      	            $scope.dadosProdutividade = data;
	      	            if($scope.dadosProdutividade.relato != null && $scope.dadosProdutividade.relato != "")
	      	            {
	   	   	             $scope.txRelato =  $scope.dadosProdutividade.relato;   	   	             
	   	 	             $scope.txAit =  $scope.dadosProdutividade.ait;
	   	 	             
		   	 	         $(function() {
		   	            	 var botaoContestar = "<a class=\"btn btn-danger pull-right h2\" ng-click=\"sendGetDadosProdutividade()\" href=\"#\" "+
			 	   								  " data-toggle=\"modal\" "+
				 	   					          " data-target=\"#contestar-modal\"><span class=\"glyphicon glyphicon-flag\"></span> "+
				 	   					          " Contestar</a>";
		 	             	$('#btnContestar').html(botaoContestar);
		 	             });
	   	   	             
	      	            }
	      	            if($scope.txRelato == null || $scope.txRelato == "null"){
	      	            	$scope.txRelato = "";
	      	            }
	      	            console.log(data);
	      	          
	      	     })
	      	     .error(function(data, status, header, config){
	   		   	 	 var responn = " Não foi possível carregar os dados "+
	   				  "<hr />Data: " + data +
	   		         "<hr />status: " + status +
	   		         "<hr />headers: " + header +
	   		         "<hr />config: " + config;
	   			      alert(responn);
	      	     });
	   	     
	           
	            
	     })
	     .error(function(data, status, header, config){
	    	 var responn = " Não foi possível carregar os dados "+
			  "<br />Data: " + data +
	          "<br />status: " + status +
	          "<br />headers: " + header +
	          "<br />config: " + config;
		        alert(responn);
	         
	          $(function() {
		           	$('#contestar-modal').modal('hide');
		       });
	         
	     });
	  } 

	  $scope.loading = function ($scope) {
		  $scope.max = 200;

		  $scope.random = function() {
		    var value = Math.floor(Math.random() * 100 + 1);
		    var type;

		    if (value < 25) {
		      type = 'success';
		    } else if (value < 50) {
		      type = 'info';
		    } else if (value < 75) {
		      type = 'warning';
		    } else {
		      type = 'danger';
		    }

		    $scope.showWarning = type === 'danger' || type === 'warning';

		    $scope.dynamic = value;
		    $scope.type = type;
		  };

		  $scope.random();

		  $scope.randomStacked = function() {
		    $scope.stacked = [];
		    var types = ['success', 'info', 'warning', 'danger'];

		    for (var i = 0, n = Math.floor(Math.random() * 4 + 1); i < n; i++) {
		        var index = Math.floor(Math.random() * 4);
		        $scope.stacked.push({
		          value: Math.floor(Math.random() * 30 + 1),
		          type: types[index]
		        });
		    }
		  };
		  $scope.randomStacked();
	  }
}]);

app.controller('getListaAits',['$scope', 'consultas', function($scope, consultas){
    
	  $scope.states = []; 
	  $scope.selected = undefined;
	  $scope.states =  consultas.async();                       
}]);


app.controller('getListaAtendimentos', function ($scope, $http) {
	  $scope.atendimentos = []; 
	    $scope.sendGet = function() {
	        $http.get("https://intranet.orsegups.com.br/fusion/services/produtividadeAit/ait/"+$scope.selected.cdViatura+"/"+$scope.selectedItem).success(function(data, status) {
	            $scope.atendimentos = data;
	            $scope.inactive = true;
	            console.log(data);
	        })
	    }               
});






