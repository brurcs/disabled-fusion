<%@page import="java.util.TreeMap"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.util.SortedMap"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.TecnicoConta"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>




<%
	
	if (PortalUtil.getCurrentUser() == null) {
		out.print("Erro - Acesso Negado");
		return;
	}
	final NeoUser currentUser = PortalUtil.getCurrentUser();
	final boolean isAdm = (currentUser!=null&&currentUser.isAdm())?true:false;
	final String none = "&nbsp;";


	
%>

<portal:head title="Produtividade AA/SUP/FIS">
	
	
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>CRUD com Bootstrap 3</title>

 <link href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">

	<style type="text/css">
		
		    
	</style>
		<script type="text/javascript">
			
		
			
		</script>
	<cw:main>
		<cw:header title="Produtividade AA/SUP/FIS" />
		<cw:body id="area_scroll">
		
			<nav class="navbar navbar-inverse navbar-fixed-top">
			 <div class="container-fluid">
			  <div class="navbar-header">
			   <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			    <span class="sr-only">Toggle navigation</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			   </button>
			   <a class="navbar-brand" href="#">Web Dev Academy</a>
			  </div>
			  <div id="navbar" class="navbar-collapse collapse">
			   <ul class="nav navbar-nav navbar-right">
			    <li><a href="#">In�cio</a></li>
			    <li><a href="#">Op��es</a></li>
			    <li><a href="#">Perfil</a></li>
			    <li><a href="#">Ajuda</a></li>
			   </ul>
			  </div>
			 </div>
			</nav>
			<div id="main" class="container-fluid">
			 <h3 class="page-header">Template Inicial</h3>
			</div>
 
    <div class="col-md-3">
        <a href="add.html" class="btn btn-primary pull-right h2">Novo Item</a>
    </div>
</div> <!-- /#top -->
			
		<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.js"></script>
		</cw:body>
	</cw:main>
</portal:head>