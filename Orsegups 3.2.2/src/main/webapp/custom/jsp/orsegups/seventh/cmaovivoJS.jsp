<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%
	String camera = ((String)request.getParameter("camera")) == null ? "FFFF01" : (String)request.getParameter("camera");
	String cameraLabel = ((String)request.getParameter("cameraLabel")) == null ? "Indefinida" : (String)request.getParameter("cameraLabel");

	if (PortalUtil.getCurrentUser() == null) {
		out.print("Erro - Acesso Negado");
		return;
	}
%>
<!DOCTYPE html>
<html>
  <body>
	<h3>Central de Monitoramento 24h Orsegups<h3>
	<h3>C�mera <%= cameraLabel %><h3>
    <img src='<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/seventh/mjpegStreamDGuard.jsp?camera=<%=camera%>&a=<%= new Date().getTime() %>' id='test_canvas' width='640px' height='480px' style='border:1px solid #d3d3d3'/>
    <script language="JavaScript">
		var img = document.getElementById('test_canvas');
		var camera = "<%= camera %>";
		img.src = "<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/seventh/mjpegStreamDGuard.jsp?camera=" + camera + "&a=" + new Date().getTime();

		function refreshCanvas(){
			img.src = "<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/seventh/mjpegStreamDGuard.jsp?camera=" +camera+ "&a=" + new Date().getTime();
		};
		window.setInterval("refreshCanvas()", 1000);
    </script>
  </body>
</html>