<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.custom.orsegups.contract.ContratoLogUtils"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="javax.persistence.Query"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>C037 - Resultado da Avalia��o trimestral de t�cnicos terceirizados</title>
</head>
<body>

<%!


public static boolean validaOSFinalizada(String numeroOS)
{
	StringBuilder sql = new StringBuilder();
	sql.append("select "); 
	sql.append("os.ID_ORDEM ");

	sql.append("from dbordem os ");
	
	sql.append("where ");
	sql.append("os.FECHADO = 1 and ");
	sql.append("os.FECHAMENTO is not null and ");
	sql.append("os.ID_ORDEM = " + numeroOS);
	sql.append("order by os.ID_ORDEM ");
	
	Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
	Collection<Object> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		//se achou alguma coisa o chamado est� finalizado
		return true;
	}
	
	return false;
}


public static String retornaOsAtendidas(String cdColaborador, String datIni, String datFim){
	String retorno = "0";
	
	String sql = "select count(ord.id_ordem) from dbOrdem ord where ord.ID_INSTALADOR = "+ cdColaborador +
			" and ord.fechamento between cast('"+datIni+" 00:00:00.000' as Datetime) and cast('"+datFim+" 00:00:00.000' as Datetime)  and ord.FECHADO = 1  ";
		
	Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
	Object result = query.getSingleResult();
	if (result != null){
		retorno = NeoUtils.safeOutputString(result);
	}
	return retorno;
			
}

public static String retornaNomeInstalador(String cdColaborador){
	String retorno = "";
	
	String sql =" select nm_colaborador "+
				" from COLABORADOR xcolab " +
				" where xcolab.cd_colaborador = " + cdColaborador ; 
	//System.out.println(sql);
		
	Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
	Object result = query.getSingleResult();
	if (result != null){
		retorno = NeoUtils.safeOutputString(result);
	}
	return retorno;
			
}

public static ArrayList<String[]> retornarPontuacao(String trimestreNeoID, String datIni, String datFim)
{
	
	ArrayList<String[]> retorno = new ArrayList();
	
	
	String sql = " select x.cd_colaborador "+
			" , cast ( ("+
						"	sum(x.d1) +  sum(x.d2) + sum(x.d3) + sum(x.d4) +   sum(x.d5)  +   sum(x.d6) + sum(x.d7) + sum(x.d8) + sum(x.d9) + sum(x.d10) + "+
						"	sum(x.d11) +  sum(x.d12) + sum(x.d13) + sum(x.d14) +   sum(x.d15)  +   sum(x.d16) + sum(x.d17) + sum(x.d18) + sum(x.d19) + sum(x.d20) + "+
						"	sum(x.d21) +  sum(x.d22) + sum(x.d23) + sum(x.d24)  "+
						") as numeric)          "+
			" from                                                                                                                    "+
			" (                                                                                                               "+
			" select xcolab.cd_colaborador "+  
			"	,dac1.codigo \"d1\" ,dac2.codigo \"d2\" ,dac3.codigo \"d3\",dac4.codigo \"d4\" ,dac5.codigo \"d5\" ,dac6.codigo \"d6\" ,dac7.codigo \"d7\", dac8.codigo \"d8\", dac9.codigo \"d9\", dac10.codigo \"d10\"  "+
			"	,dac11.codigo \"d11\" ,dac12.codigo \"d12\" ,dac13.codigo \"d13\",dac14.codigo \"d14\" ,dac15.codigo \"d15\" ,dac16.codigo \"d16\" ,dac17.codigo \"d17\", dac18.codigo \"d18\", dac19.codigo \"d19\", dac20.codigo \"d20\"  "+
			"	,dac21.codigo \"d21\" ,dac22.codigo \"d22\" ,dac23.codigo \"d23\",dac24.codigo \"d24\"   "+
			" from wfProcess wf                                                                                                       "+
			" join D_ADTprincipal ap on (wf.neoId = ap.wfprocess_neoId and ap.trimestre_neoID = " + trimestreNeoID +"  ) "+                                                             
			" join X_SIGMACOLABORADOR xcolab	on (ap.tecnico_neoID = xcolab.neoID) "+  
			"         left join d_adtconceito dac1 on (ap.apresUniforme_neoID = dac1.neoID) "+                                             
			"         left join d_adtconceito dac2 on (ap.apresRelacionamento_neoID = dac2.neoID) "+                                                   
			"         left join d_adtconceito dac3 on (ap.pontualidade_neoID = dac3.neoID) "+                                                      
			"         left join d_adtconceito dac4 on (ap.respcompContasRota_neoID = dac4.neoID) "+                                                         
			"         left join d_adtconceito dac5 on (ap.respcompApoioFechamentoOSs_neoID = dac5.neoID) "+                                          
			"         left join d_adtconceito dac6 on (ap.respcompAuxilioAtendimento_neoID = dac6.neoID) "+                                                    
			"         left join d_adtconceito dac7 on (ap.atendimentoAgendado_neoID = dac7.neoID) "+
			"         left join d_adtconceito dac8 on (ap.atendimentoPrreventivo_neoID = dac8.neoID) "+
			"         left join d_adtconceito dac9 on (ap.limpezaDuranteEntrega_neoID = dac9.neoID) "+
			"         left join d_adtconceito dac10 on (ap.orientacoesClienteFuncionamentoSistema_neoID = dac10.neoID) "+
			"         left join d_adtconceito dac11 on (ap.placaOrsegupsClientesRotaAtendidos_neoID = dac11.neoID) "+
			"         left join d_adtconceito dac12 on (ap.carmotLimpoBomEstado_neoID = dac12.neoID) "+
			"         left join d_adtconceito dac13 on (ap.carmotLogadasParadasForaRota_neoID = dac13.neoID) "+
			"         left join d_adtconceito dac14 on (ap.servicoPrestadoCliente_neoID = dac14.neoID) "+
			"         left join d_adtconceito dac15 on (ap.retornoClientesSobreProfissional_neoID = dac15.neoID) "+
			"         left join d_adtconceito dac16 on (ap.participacaoCapacitacoes_neoID = dac16.neoID) "+
			"         left join d_adtconceito dac17 on (ap.ferramentasEquipamentosAdequados_neoID = dac17.neoID) "+
			"         left join d_adtconceito dac18 on (ap.kitTecnicoPecasComponentes_neoID = dac18.neoID) "+
			"         left join d_adtconceito dac19 on (ap.comprometimentoPadraoTecnicoOrsegups_neoID = dac19.neoID) "+
			"         left join d_adtconceito dac20 on (ap.eticaNosAtendimentos_neoID = dac20.neoID) "+
			"         left join d_adtconceito dac21 on (ap.atendimentoLigacoesCM_neoID = dac21.neoID) "+
			"         left join d_adtconceito dac22 on (ap.trabalhoEquipe_neoID = dac22.neoID) "+
			"         left join d_adtconceito dac23 on (ap.osFechamentoAdequado_neoID = dac23.neoID) "+
			"         left join d_adtconceito dac24 on (ap.fechamentoPontoComTermoResp_neoID = dac24.neoID) "+
			" where wf.processState <> 2                                                                                              "+
			" and trimestre_neoID = " + trimestreNeoID +
			" and wf.finishDate is not null                                                                                           "+
			" and (dac1.codigo > 0 and dac2.codigo > 0 and dac3.codigo > 0 and dac4.codigo > 0                                        "+
			" and dac5.codigo > 0 and dac6.codigo > 0 )                                                                               "+
			//" and wf.startDate between cast('2014-01-01 00:00:00.000' as Date) and cast('2014-09-30 00:00:00.000' as Date)   		  "+
			//" and dataAvaliacao between cast('"+datIni+" 00:00:00.000' as Date) and cast('"+datFim+" 00:00:00.000' as Date)   "+		
			" and wf.finishDate is not null 																						  "+
			" ) x                                                                                                                     "+
			" group by x.cd_colaborador                                                                                               "+
			" order by 2 desc";                                                                                                       
	//System.out.println(sql);
	
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
	Collection<Object[]> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		String cabecalho[] = {"Colaborador", "<center>Pontuacao </center>","Resultado Final"};
		retorno.add(cabecalho);
		for (Object[] rs : resultList){
			String linha[] = {
					String.valueOf(rs[0]),
					String.valueOf(rs[1]),
					NeoUtils.safeOutputString(  
							 	NeoUtils.safeDouble( String.valueOf(rs[1]) ) / NeoUtils.safeDouble(retornaOsAtendidas(String.valueOf(rs[0]), datIni, datFim))   
							)
					};
			retorno.add(linha);
		}
	}
	
	return retorno;
}

public String classificar(long valor, long maxOuro, long minOuro, long maxBronze, long minBronze){
	if( (valor >= minOuro) && (valor <= maxOuro) ){
		return "<center><span style='background-color: #D9D919 '>&nbsp;Ouro&nbsp;&nbsp;</span></center>";
	}else if( (valor >= minBronze) && (valor <= maxBronze) ){
		return "<center><span style='background-color: #DB9370 '>Bronze&nbsp;</span></center>";
	}else{
		return "<center><span style='background-color: #C0C0C0 '>&nbsp;Prata&nbsp;&nbsp;</span></center>";
	}
}

%>


<%
double fator = 0;
long maximoOuro = 0;
long minimoOuro = 0;
long maximoBronze = 0;
long minimoBronze = 99999;

String strTrimestre = request.getParameter("cmb_trimestre");
String strAno = request.getParameter("cmb_ano");
String trimestreNeoID = "0";
ArrayList<NeoObject> oTrimestres = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ADTTrimestre"));
GregorianCalendar gc = new GregorianCalendar();
int anoAtual = gc.get(GregorianCalendar.YEAR); 
int anoPassado = anoAtual -1;

GregorianCalendar datIni = new GregorianCalendar();
GregorianCalendar datFim = new GregorianCalendar();

if (strTrimestre != null){
	for (NeoObject trimestre : oTrimestres){
		EntityWrapper wT = new EntityWrapper(trimestre);
		if (NeoUtils.safeOutputString(wT.findValue("trimestre")).equals(strTrimestre)){
			System.out.println("trimestre..." + strTrimestre);
			trimestreNeoID = NeoUtils.safeOutputString(wT.findValue("neoId"));
			datIni.set(GregorianCalendar.MONTH, Integer.parseInt( NeoUtils.safeOutputString( wT.findValue("mesIni") ) )-1 );
			datFim.set(GregorianCalendar.MONTH, Integer.parseInt( NeoUtils.safeOutputString( wT.findValue("mesFim") ) )-1 );
			break;
		}
	}
}
if (strAno != null){
	datIni.set(GregorianCalendar.YEAR, Integer.parseInt( strAno ) );
	datFim.set(GregorianCalendar.YEAR, Integer.parseInt( strAno ) );
}
	datIni.set(GregorianCalendar.DATE, 1);
	datFim.set(GregorianCalendar.DATE, datFim.getActualMaximum(GregorianCalendar.DATE));
	System.out.println("datas" + NeoUtils.safeDateFormat(datIni.getTime(), "yyyy-MM-dd") + " - " + NeoUtils.safeDateFormat(datFim.getTime(), "yyyy-MM-dd") );

%>
	<form>
		<span>Trimestre</span>
		<select id="cmb_trimestre" name="cmb_trimestre">
			<%
			for (NeoObject trimestre : oTrimestres){
				EntityWrapper wTrim = new EntityWrapper(trimestre);
			%>
			<option value="<%=wTrim.findValue("trimestre")%>" <%= strTrimestre != null && strTrimestre.equals(NeoUtils.safeOutputString(wTrim.findValue("trimestre")) )? "selected='selected'" : "" %> ><%= wTrim.findValue("trimestre") %></option>
			<%
			}
			%>

		</select>
		&nbsp;
		<span>Ano</span>
		<select id="cmb_ano" name="cmb_ano">
			<option value="<%= anoAtual %>" selected="selected"><%= anoAtual %></option>
			<option value="<%= anoPassado %>" ><%= anoPassado %></option>
		</select>
		<input type="submit" value="Processar">
	</form>

<%

	DecimalFormat formatoReal = new DecimalFormat("#,##0.00");


	ArrayList<String[]> lista = retornarPontuacao(trimestreNeoID,NeoUtils.safeDateFormat(datIni.getTime(), "yyyy-MM-dd"), NeoUtils.safeDateFormat(datFim.getTime(), "yyyy-MM-dd"));
	
	int c = 0;
	for (String x[] : lista){
		if (c > 0){
			if (maximoOuro < Long.parseLong(x[1])){
				maximoOuro = Long.parseLong(x[1]);
			}
			if (minimoBronze > Long.parseLong(x[1])){
				minimoBronze = Long.parseLong(x[1]);
			}
		}
		c++;
	}
	fator = Math.round((maximoOuro - minimoBronze) * 0.3);
	
	minimoOuro = maximoOuro - (long) fator;
	maximoBronze = minimoBronze + (long) fator;
	
	c = 0;
	
	%>
	<table style="border: black solid 1px;">
		<tr align="center" style="background-color: silver;">
			<td colspan="5" style="border: black solid 1px;"><h3>Classifica��o de T�cnicos <%= strTrimestre != null ? (" - " + strTrimestre + "� trimestre de " + strAno) : "" %></h3></td>
		</tr>
	<%
	int total = lista.size();
	int faturados = 0;
	int pendentes = 0;
	int errados = 0;
	
	
	for(String linha[]: lista){
		String stylo = "";
		
		%>
		<tr  >
			<td style="border: black solid 1px;<%=  c%2 == 0 ? "background-color:silver;":"" %>" ><%= c > 0 ? c : ""  %></td> 
			<td style="border: black solid 1px;<%=  c%2 == 0 ? "background-color:silver;":"" %>" ><%= linha[0] + ( c>0? "-" +retornaNomeInstalador(linha[0]): "") %></td>
			<td style="border: black solid 1px;<%=  c%2 == 0 ? "background-color:silver;":"" %>" ><%= linha[1] %></td>
			<td style="border: black solid 1px;<%=  c == 0 ? "background-color:silver;":"" %>" > <%= c>0?  classificar(Long.parseLong(linha[1]), maximoOuro, minimoOuro, maximoBronze, minimoBronze) :  ""   %></td>
		</tr>
		<%
		c++;
	}
	%>
	</table>
		<br>
</body>
</html>