app.controller('appontrollerressarcimento', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.nomePosto;
	$scope.nomeCliente;
	$scope.cidadePosto;
	$scope.periodoInicial;
	$scope.periodoFinal;
	
//	$scope.livros;
	
	$scope.livros = [];
    $scope.livrosDisplay = [];
	
	$scope.opcoesAnoInicio =  getYears();
	$scope.opcoesAnoFim =  getYears();
	
	$scope.anoInicioSelecionado;
	$scope.anoFimSelecionado;
	
	var nomeDoPosto = "";
	var nomeDoCliente = "";
	var cidadeDoPosto = "";
	
	$scope.buscar = function(){
		
		if ($scope.nomeCliente){
			nomeDoCliente = $scope.nomeCliente;
			if (!$scope.anoInicioSelecionado){
				$scope.anoInicioSelecionado = "";
			}else{
				$scope.anoInicioSelecionado = "%%2F"+$scope.anoInicioSelecionado;
			}
			if (!$scope.anoFimSelecionado){
				$scope.anoFimSelecionado = "";
			}else{
				$scope.anoFimSelecionado = "%%2F"+$scope.anoFimSelecionado;
			}
			if ($scope.nomePosto){
				nomeDoPosto = $scope.nomePosto;
			}
			if ($scope.cidadePosto){
				cidadeDoPosto = $scope.cidadePosto;
			}
			$http.post('https://intranet.orsegups.com.br/fusion/services/documentos/pesquisarLivrosOcorrencia;nomePosto='+nomeDoPosto+";nomeCliente="+nomeDoCliente+
					";cidadePosto="+cidadeDoPosto+";periodoInicial="+$scope.anoInicioSelecionado+";periodoFinal="+$scope.anoFimSelecionado).success(function(data, status) {
						console.log(data);
						if (data){
							$.each(data.pasta, function(key, value){
								data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
							});
						}else{
							$('#warning-alert').show();
							setTimeout(function(){
								$('#warning-alert').hide();
							}, 5000);
						}
						$scope.livros = data.pasta;
						$scope.livrosDisplay = data.pasta;
						$('#processing-modal').modal('hide');
					}).error(function() {
						alert("Não foi possível carregar os dados");
					});
		}else{
			setTimeout(function(){
				$('#processing-modal').modal('hide');
				$('#danger-alert').show();
				setTimeout(function(){
					$('#danger-alert').hide();
					$scope.livros = [];
					$scope.livrosDisplay = [];
				}, 5000);				
			}, 1000);
		}
	}
	
	
	
	function getYears(){
		var d = new Date();
		var ano = d.getFullYear();
		var yearList = [];
		
		for (i= ano; i>=1990; i--){
			yearList.push(ano);
			ano--;
		}
		
		return yearList;
	}
	
}]);
