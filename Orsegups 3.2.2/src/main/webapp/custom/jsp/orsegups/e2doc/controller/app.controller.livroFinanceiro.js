app.controller('appcontrollerlivrofinanceiro', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.documentos = [];
	$scope.documentosView = [];
	
	$scope.tiposDocumentos = [];
	$scope.tipoDocumentoSel = '';
	
	$scope.tiposLivros = [];
	$scope.tipoLivroSel = '';
	
	$scope.empresas = [];
	$scope.empresaSel = '';
	$scope.competencia;
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentosLivroFinanceiro/getTiposLivros').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.tiposLivros = data;
		}else{
			alert('Não foi possível obter os tipos de livros!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentosLivroFinanceiro/getEmpresas').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.empresas = data;
		}else{
			alert('Não foi possível obter as empresas!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	
	$scope.pesquisar = function(){
		
		var comp = '';
		
		
		if(!$scope.tipoDocumentoSel){
			$scope.tipoDocumentoSel = '';
		}
		
		if(!$scope.tipoLivroSel){
			$scope.tipoLivroSel = '';
		}
		
		if(!$scope.empresaSel){
			$scope.empresaSel = '';
		}
		
		if(!$scope.numero){
			$scope.numero = '';
		}
		
		if(!$scope.ano){
			$scope.ano = '';
		}
		
		$http.post('https://intranet.orsegups.com.br/fusion/services/documentosLivroFinanceiro/pesquisaLivroFinanceiro;tiposDocumentos='+$scope.tipoDocumentoSel+';tiposLivros='+encodeURIComponent($scope.tipoLivroSel)+';empresa='+$scope.empresaSel+';numero='+$scope.numero+';ano='+$scope.ano).success(function(data, status) {
			
			if (data){
				$.each(data.pasta, function(key, value){
					data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
				});
			}else{
				$('#warning-alert').show();
				setTimeout(function(){
					$('#warning-alert').hide();
				}, 8*1000);
			}
			$scope.documentos = data.pasta;
			$scope.documentosView = data.pasta;
			
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	}
		
}]);
