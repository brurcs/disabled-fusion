app.controller('appcontrollerdocumentacaoLegal', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.dataEntrega = new Date();
	$scope.documentos = [];
    $scope.documentosLegaisDisplay = [];
    $scope.razaoSocial;
    $scope.cnpj;
    
    $scope.tiposDocumentos = [];
	$scope.tipoDocumentoSel = '';
	
	$http.post('/fusion/services/documentos/getTiposDocumentacaoLegal').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.tiposDocumentos = data;
		}else{
			alert('Não foi possível obter os tipos de documentos!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	$scope.buscar = function(){
		
		if(!$scope.razaoSocial){
			razaoSocial = $scope.razaoSocial;
		}
		
		if(!$scope.cnpj){
			cnpj = $scope.cnpj;
		}

		if(!$scope.tipoDocumentoSel){
			$scope.tipoDocumentoSel = '';
		}
		
		$http.post('/fusion/services/documentos/pesquisarDocumentacaoLegal;razaoSocial='+$scope.razaoSocial+';cnpj='+$scope.cnpj+';tipoDocumento='+$scope.tipoDocumentoSel).success(function(data, status) {
			if (data){
				$.each(data.pasta, function(key, value){
					data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
				});
			}else{
				$('#warning-alert').show();
				setTimeout(function(){
					$('#warning-alert').hide();
				}, 5000);
			}
			//$scope.documentos = data.pasta;
			$scope.documentosDisplay = data.pasta;
			let table = new Array();
			
			for(var i = 0; i < data.pasta.length ; i++){
				let pasta = data.pasta[i];
				for(var j = 0; j < data.pasta[i].doc.length ; j++){
					let obj = {i0:pasta.indices.i0, i1:pasta.indices.i1, i2:pasta.indices.i2, i3:pasta.indices.i3, i4:pasta.indices.i4, i5:pasta.indices.i5, i6:pasta.indices.i6, docDes:"", docId:""};
					let doc = data.pasta[i].doc[j];
					obj.docDes = doc.des;
					obj.docId = doc.id;
					table.push(obj);
				}
			}
			$scope.documentos = table;
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	} 	
}]); 
