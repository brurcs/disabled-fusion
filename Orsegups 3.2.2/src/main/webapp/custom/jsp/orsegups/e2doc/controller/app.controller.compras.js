app.controller('appcontrollercompras', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.dataEntrega = new Date();
	$scope.teste;
	$scope.compras = [];
    $scope.comprasDisplay = [];
    $scope.fornecedores = [];
    
    $scope.tiposDocumentos = [];
	$scope.tipoDocumentoSel = '';
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentos/getTiposDocumentos').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.tiposDocumentos = data;
		}else{
			alert('Não foi possível obter os tipos de documentos!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	$scope.buscar = function(){
		
		var data = "";
		
		if ($scope.fornecedor!= null) {
			data = '';
		} else {
		
			if ($scope.dataEntrega){
				data = new Date($scope.dataEntrega);
				var mes = data.getMonth()+1;
				var ano = data.getFullYear();
				data = (mes < 10 ? "0"+mes : mes)+"-"+ano
			}
		}
		
		if(!$scope.tipoDocumentoSel){
			$scope.tipoDocumentoSel = '';
		}
		
		$http.post('https://intranet.orsegups.com.br/fusion/services/documentos/pesquisarCompras;fornecedor='+$scope.fornecedor+';dataEntrega='+data+';tipoDocumento='+$scope.tipoDocumentoSel).success(function(data, status) {
			console.log(data);
			if (data){
				$.each(data.pasta, function(key, value){
					data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
				});
			}else{
				$('#warning-alert').show();
				setTimeout(function(){
					$('#warning-alert').hide();
				}, 5000);
			}
			$scope.compras = data.pasta;
			$scope.comprasDisplay = data.pasta;
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	}
	
	
	
	function getYears(){
		var d = new Date();
		var ano = d.getFullYear();
		var yearList = [];
		
		for (i= ano; i>=1990; i--){
			yearList.push(ano);
			ano--;
		}
		
		return yearList;
	}
	
}]);
