app.controller('appontrollerressarcimento', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.numeroConta;
	$scope.nomeCliente;
	$scope.endereco;
	$scope.municipio;
	$scope.modelo;
	
	$scope.ressarcimentos = [];
    $scope.ressarcimentosDisplay = [];
	
	
	$scope.buscar = function(){
		var numeroConta = "";
		var nomeDoCliente = "";
		var endereco = "";
		var municipio = "";
		if ($scope.nomeCliente){
			nomeDoCliente = $scope.nomeCliente;
		}
		if ($scope.numeroConta){
			numeroConta = $scope.numeroConta;
		}
		if ($scope.municipio){
			municipio = $scope.municipio;
		}
		if ($scope.endereco){
			endereco = $scope.endereco;
		}
		$http.post('https://intranet.orsegups.com.br/fusion/services/documentos/pesquisarRessarcimentos;numeroConta='+numeroConta+";nomeCliente="+nomeDoCliente+
				";endereco="+endereco+";municipio="+municipio+";modelo="+$scope.modelo).success(function(data, status) {
					console.log(data);
					if (data){
						$.each(data.pasta, function(key, value){
							if (value.doc instanceof Array){
								data.pasta[key].doc.id = encodeURIComponent(value.doc[0].id);
							}else{
								data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
							}
						});
					}else{
						$('#warning-alert').show();
						setTimeout(function(){
							$('#warning-alert').hide();
						}, 5000);
					}
					console.log(data.pasta);
					$scope.ressarcimentos = data.pasta;
					$scope.ressarcimentosDisplay = data.pasta;
					$('#processing-modal').modal('hide');
				}).error(function() {
					alert("Não foi possível carregar os dados");
				});
	}
	
}]);
