<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
 
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.net.URLEncoder"%>

<title>Ordens de Servi�o em Atendimento</title>
<base href="http://intranet.orsegups.com.br/fusion/" />
<style type="text/css" media="print">
@page
{
size: landscape;
margin: 1cm;
}
</style>

<style type="text/css">

.cor_os_preta {
  background-color: #ffffff;
  color: #0000;
}
.gridboxRoteiro{
	font-family: "Trebuchet MS", Tahoma, Verdana, Arial, Helvetica, sans-serif;
	background-color:#FFFFFF;
	border:1px solid #000000;
	width:95%;
	border-collapse: separate;
	clear:left;
	font-size: 10px;
}
.gridboxRoteiro tr *
{
}
.gridboxRoteiro th {
	border: 1px solid;
	border-color : white #9095A1 #9095A1 white;
	text-align:center;
	font-weight:normal;
	cursor:pointer;
	background:url(../imagens/core/table_hd_back.jpg) top center #E9F4FC repeat-x;
	padding:0.2em 0.5em;
	vertical-align:middle;
	white-space:nowrap;
	display:table-cel;
	}

.gridboxRoteiro th.min{width:1%;}
.gridboxRoteiro td.min{width:0%;}
.gridboxRoteiro td {
	border: 1px solid;
	border-color : #FFFFFF #9095A1 #9095A1 #FFFFFF;
	font-family: "Trebuchet MS", Tahoma, Verdana, Arial, Helvetica, sans-serif;
	padding:0.1em 0.5em;
	vertical-align:middle;
	white-space:normal;
	cursor:default;
}
.gridboxRoteiro td *{vertical-align:middle;}
.gridboxRoteiro td.viz{cursor:pointer;}
.gridboxRoteiro td img{
	vertical-align:middle;
}

.gridboxRoteiro tr.destaq td{
	background:url(../imagens/core/table_hd_over.jpg) top left repeat-x;
}

.tb_remove_itens{ background:#FFFFFF; width:96%; border:none;}
.tb_remove_itens td{
padding:0.5em 0.2em;border:none;border-bottom:#CCCCCC 1px dashed;
}
.tb_remove_itens td:hover{ background: url(../imagens/core/input_field_back_orange.gif) #fffcf6 top left repeat-x;}
.tb_remove_itens td.min{width:0%; text-align:right; white-space:nowrap;}

</style>
<body onload="window.print()">
<%		
	String regional = request.getParameter("regional");
	if(regional == null || regional.trim().equals(""))	{
		regional = "";
 	}
	String cd_tecnico = request.getParameter("cd_tecnico");
	if(cd_tecnico == null || cd_tecnico.trim().equals(""))	{
		cd_tecnico = "";
 	}

	String checkedFechadas = "";
	String checkedAbertas = "";
	String checkedAlertas = "";
	boolean booleanMostrarFechadas = false;
	String mostrarFechadas = request.getParameter("controle");
	if(mostrarFechadas != null && mostrarFechadas.trim().equals("on"))	{
		booleanMostrarFechadas = true;
		checkedFechadas = " checked ";
 	} 

	boolean booleanMostrarAbertas = false;
	String mostrarAbertas = request.getParameter("controle");
	if(mostrarAbertas != null && mostrarAbertas.trim().equals("on"))	{
		booleanMostrarAbertas = true;
		checkedAbertas = " checked ";
 	}	

	boolean booleanMostrarAlertas = false;
	String mostrarAlertas = request.getParameter("controle");
	if(mostrarAlertas != null && mostrarAlertas.trim().equals("on"))	{
		booleanMostrarAlertas = true;
		checkedAlertas = " checked ";
 	} 		

	String controle = request.getParameter("controle");
	//System.out.println(controle);
	if (controle == null)	{
		controle = "P";
 	} 
	if (controle.equals("T"))	{
		booleanMostrarFechadas = true;
		booleanMostrarAbertas = true;
		booleanMostrarAlertas = true;
		checkedFechadas	= " checked ";	
 	} 
	if (controle.equals("P"))	{
		booleanMostrarFechadas = false;
		booleanMostrarAbertas = true;
		booleanMostrarAlertas = true;
		checkedAbertas	= " checked ";	
 	} 
	if (controle.equals("A"))	{
		booleanMostrarFechadas = false;
		booleanMostrarAbertas = false;
		booleanMostrarAlertas = true;
		checkedAlertas	= " checked ";	
 	} 

%>
<%!
public static String getTimeSpentDays(long timeSpent) {

		// string de retorno
		String str = "";

		// quantidade de dias
		long days = timeSpent / 86400;

		// se houver dias
		if (days > 0) {

			// mostra quantos dias passaram
			str = str + days + "d";
		} else {
			str =  "1d";
		}

		return str;

	}
			
%>	
<%		 
    final String none = "&nbsp;";
    PreparedStatement st = null;
    PreparedStatement st3 = null;
    Connection conn = null;
    Connection connSapiens = null;
    ResultSet rs = null;
    ResultSet rsEquip = null;
    try {
		String nomeFonteDados = "SIGMA90";
		conn = PersistEngine.getConnection(nomeFonteDados);
		connSapiens = PersistEngine.getConnection("SAPIENS");

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH':'mm");
		Timestamp tsAuxAbertura = null;
		Timestamp tsAuxAgendada = null;
		Date dateAuxAgora = new Date();
		String auxTimeAbertura = "";
		String auxTimeAgendada = "";
		long longTempoMinutos = 0;

		// 0 - Aberta
		// 1 - Fechada
		// 2 - Execu��o
		// 3 - Pausa        
		
		StringBuffer sql1 = new StringBuffer();
		sql1.append("SELECT col.NM_COLABORADOR , def.DESCRICAODEFEITO, os.ID_ORDEM, os.ABERTURA, os.DATAAGENDADA, os.DEFEITO, os.ID_INSTALADOR,   		");
		sql1.append("c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO , c.CD_CLIENTE, c.FANTASIA, c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, 	");
		sql1.append("cid.NOME AS NOMECIDADE, os.FECHAMENTO, os.FECHADO, osh.DATAINICIOEXECUCAO, osh.DATAFIMEXECUCAO, os.EXECUTADO, sol.NM_DESCRICAO, CD_COLABORADOR       				");
		sql1.append("FROM dbORDEM os WITH (NOLOCK)																									");
		sql1.append("INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR 												");
		sql1.append("INNER JOIN OS_SOLICITANTE sol WITH (NOLOCK) ON sol.CD_OS_SOLICITANTE = os.CD_OS_SOLICITANTE 												");
		sql1.append("INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO														");
		sql1.append("INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE 														 	");
		sql1.append("INNER JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE  							");
		sql1.append("INNER JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE  										 					");
		sql1.append("LEFT JOIN OSHISTORICO osh WITH (NOLOCK) ON osh.ID_ORDEM = os.ID_ORDEM AND osh.IDOSHISTORICO = (SELECT MIN(IDOSHISTORICO) FROM OSHISTORICO WHERE ID_ORDEM = os.ID_ORDEM)	");														
		sql1.append("WHERE col.CD_COLABORADOR = "+cd_tecnico+" 																	");
		sql1.append("AND os.FECHADO IN (0, 2, 3)       																				");
		sql1.append(" ORDER BY col.NM_COLABORADOR, os.FECHAMENTO DESC, sol.NM_DESCRICAO, os.DATAAGENDADA, os.ABERTURA																			");
		
		st = conn.prepareStatement(sql1.toString());
		rs = st.executeQuery();
				
		String nm_tecnico_anterior = "primeira";
		String icone_tecnico = "<img src='imagens/custom/user-worker.png' alt='T�cnico'/>&nbsp;";
		String linha_rota = "";
		String class_cor_evento = "cor_os_preta";
		String nm_status = "";
		String nm_tecnico = ".";
		String nm_tecnico_display = "";
		int qtdAtendimentos = 0;
		int qtdAtendidos = 0;
		String horaLoginAA = "";
		Timestamp timeLoginAA = null;
		String sTempoAtendimento = "";
					
		while(rs.next()) {
			
			/* Trocou de TECNICO */
			nm_tecnico = rs.getString("NM_COLABORADOR");
			if (!nm_tecnico_anterior.equals(nm_tecnico)) {
						
				if(!nm_tecnico_anterior.equals("primeira")) {
					out.print("</tr class='linha_viatura'>");
					out.print("<td style='font-weight: normal' colspan='2'>&nbsp;</td>");
					out.print("<td style='font-weight: bold' colspan='2'> Em atendimento: "+ qtdAtendimentos +"</td>");
					out.print("<td style='font-weight: bold' colspan='3'>&nbsp;</td>");
					out.print("<td style='font-weight: normal' colspan='5'>&nbsp;</td>");
					out.print("</tr>");
				}
				String id_instalador = rs.getString("ID_INSTALADOR");
		
				nm_tecnico_anterior = nm_tecnico;
				out.print("<span style='font-weight: bold' colspan='10'>");				
				out.print(icone_tecnico+"&nbsp;"+ nm_tecnico);				
				out.print("</span>");				
			
				qtdAtendimentos = 0;
				%>
			
					<table class="gridboxRoteiro" cellpadding="0" cellspacing="0">
					<thead style="cursor: auto">
						<tr style="cursor: auto">
							<th style="cursor: auto">OS</th>
							<th style="cursor: auto">Status</th>
							<th style="cursor: auto; width: 80px">Abertura</th>
							<th style="cursor: auto; width: 80px">Agendada</th>
							<th style="cursor: auto"><img src="imagens/icones_final/clock.png" alt="Tempo de execu��o"/></th>
							<th style="cursor: auto">Prioridade</th>
							<th style="cursor: auto">Equip.</th>
							<th style="cursor: auto">Conta</th>
							<th style="cursor: auto">Cliente</th>
							<th style="cursor: auto" class="min">Endere�o</th>
							<th style="cursor: auto" class="min">Bairro</th>
							<th style="cursor: auto; width: 100px;" class="min">Descri��o Defeito</th>
						</tr>
					</thead>
					<tbody>	
				
				<%

			}	
			String cd_empresa = rs.getString("ID_EMPRESA");
			String id_central = rs.getString("ID_CENTRAL");
			String particao = rs.getString("PARTICAO");
			
			StringBuffer sqlEquip = new StringBuffer();
			sqlEquip.append(" SELECT cvs.USU_CodSer ");
			sqlEquip.append(" FROM USU_T160SIG sig ");
			sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
			sqlEquip.append(" WHERE sig.usu_sigemp = "+cd_empresa+" ");
			sqlEquip.append(" AND sig.usu_sigcon = '"+id_central+"' ");
			sqlEquip.append(" AND sig.usu_sigpar = '"+particao+"' ");
			sqlEquip.append(" AND cvs.usu_codser IN ('9002035', '9002011', '9002004', '9002005', '9002014') ");
			sqlEquip.append(" ORDER BY cvs.usu_sitcvs ");

			st3 = connSapiens.prepareStatement(sqlEquip.toString());
			rsEquip = st3.executeQuery();
			String flag_equip = "Cliente";
			if (rsEquip.next()) {
				// Equipamento Orsegups
				flag_equip = "Orsegups";
			}
			
			nm_status = "&nbsp;";
			String def = rs.getString("DESCRICAODEFEITO");
			if (rs.getInt("FECHADO") == 1) {
				// 1 - Fechada
				qtdAtendidos++;
				nm_status = "Fechada";
			
			} else if (rs.getInt("FECHADO") == 2) {
				// 2 - Execu��o
				qtdAtendimentos++;
				class_cor_evento = "cor_os_verde_execucao";			
				nm_status = "Execu��o";				
			} else if (rs.getInt("FECHADO") == 3) {
				// 3 - Pausa   			
				qtdAtendimentos++;
				class_cor_evento = "cor_os_amarela_pausada";			
				nm_status = "Pausada";				
			} else if (rs.getInt("FECHADO") == 0) {
				// 0 - Aberta
				qtdAtendimentos++;
				class_cor_evento = "cor_os_vermelha_aberta";			
				nm_status = "Aberta";				
			}

			tsAuxAbertura = rs.getTimestamp("ABERTURA");
			tsAuxAgendada = rs.getTimestamp("DATAAGENDADA");
			sTempoAtendimento = none;
			auxTimeAbertura = none;
			auxTimeAgendada = none;
	
			if (tsAuxAbertura != null) {
				auxTimeAbertura = formatter.format(tsAuxAbertura);
				sTempoAtendimento = getTimeSpentDays(Math.round((dateAuxAgora.getTime() - tsAuxAbertura.getTime()) / 1000));
			}
			
			if (tsAuxAgendada != null) {
				auxTimeAgendada= formatter.format(tsAuxAgendada);
			}
			
			out.println("<tr>");
			out.println("<td class='"+ class_cor_evento + "'> " +  rs.getString("ID_ORDEM") + "</td>");
			out.println("<td class='"+ class_cor_evento + "'> " +  nm_status + "</td>");
			out.println("<td align='center' class='"+ class_cor_evento + "'> " + auxTimeAbertura + "</td>");
			out.println("<td align='center' class='"+ class_cor_evento + "'> " + auxTimeAgendada + "</td>");
			out.println("<td align='center' class='"+ class_cor_evento + "'> " + sTempoAtendimento + "</td>");
			out.println("<td class='"+ class_cor_evento + "'>");	
			out.println(rs.getString("NM_DESCRICAO"));			
			out.println("</td>");			
			out.println("<td class='"+ class_cor_evento + "'>" + flag_equip + "</td>");			
			out.println("<td class='"+ class_cor_evento + "'>" + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]</td>");			
			out.println("<td class='"+ class_cor_evento + "'>" + rs.getString("FANTASIA") + "</td>");			
			out.println("<td class='"+ class_cor_evento + "'>" + rs.getString("ENDERECO") + "</td>");
			out.println("<td class='"+ class_cor_evento + "'>" + rs.getString("NOMEBAIRRO") + "</td>");
			out.println("<td class='"+ class_cor_evento + "'>" + rs.getString("DEFEITO") + "</td>");
			out.println("</tr>");			
					
		}
		out.print("<tr class='linha_viatura'>");
		out.print("<td style='font-weight: normal' colspan='2'>&nbsp;</td>");
		out.print("<td style='font-weight: bold' colspan='3'> Ordens de Servi�os: "+ qtdAtendimentos +"</td>");
		out.print("<td style='font-weight: bold' colspan='2'>&nbsp;</td>");
		out.print("<td style='font-weight: normal' colspan='4'>&nbsp;</td>");
		out.print("</tr>");
		
			
	} catch (Exception e) {
		e.printStackTrace();	
	}
    finally
    {
    	OrsegupsUtils.closeConnection(conn, st, rs);
    	OrsegupsUtils.closeConnection(connSapiens, st3, rsEquip);
    }
%>
	</tbody>
	</table>