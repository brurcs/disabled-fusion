<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>

<%
	String cd_central = request.getParameter("cd_central");
	if(cd_central == null || cd_central.trim().equals(""))	{
		out.print("Erro #1 - C�digo da Central n�o informado");
		return;
	}
	String cd_empresa = request.getParameter("cd_empresa");
	if(cd_empresa == null || cd_empresa.trim().equals(""))	{
		out.print("Erro #2 - Empresa n�o informada");
		return;
	}	
	String particao = request.getParameter("particao");
	if(particao == null || particao.trim().equals(""))	{
		out.print("Erro #3 - Parti��o n�o informada");
		return;
	}
	String nm_evento = request.getParameter("nm_evento");
	if(nm_evento == null || nm_evento.trim().equals(""))	{
		out.print("Erro #4 - Evento n�o informado");
		return;
	}
	String origem = request.getParameter("origem");
	if(origem == null || origem.trim().equals(""))	{
		origem = "Gera��o Fusion";
	}
	if(origem.length() > 20)	{
		origem = origem.substring(0, 20);
	}
	
    String url = "jdbc:jtds:sqlserver://FSOODB03:1434/SIGMA90;instanceName=SQL01";
    String username = "sigma90";
    String password = "crU3rExa";
    Connection conn = null;
    PreparedStatement st = null;

    try {
		Class.forName("net.sourceforge.jtds.jdbc.Driver");
		conn = DriverManager.getConnection(url, username, password);
		
		StringBuffer sql11 = new StringBuffer();
		sql11.append("INSERT INTO RECEPCAO ");
		sql11.append("(CD_CENTRAL, NM_EVENTO, NU_AUXILIAR, NU_PARTICAO, NU_PORTA, NU_RECEPTORA, NU_LINHA, NU_PROTOCOLO, NU_GRUPO_RECEPTORA, DT_RECEPCAO, TP_RECEPTORA, CD_EMPRESA, NM_RECEPTORA, TP_RECEPCAO, CD_VIATURA_EVENTO) ");
		sql11.append("SELECT ?, ?, '0', ?, '9', NULL, NULL, '2', NULL, GETDATE(), '1', ?, ?, NULL, NULL");
		st = conn.prepareStatement(sql11.toString());
		st.setString(1, cd_central);
		st.setString(2, nm_evento);
		st.setString(3, particao);
		st.setString(4, cd_empresa);
		st.setString(5, origem);
		st.executeUpdate();
		
		out.print("OK");		
    }  catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #5 - Erro ao inserir evento no buffer do Sigma");
		return;    
	} finally {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
		out.print("Erro #6 - Erro ao finalizar conex�o");
		return;    
      }
    }
%>
