<%@page import="com.neomind.fusion.custom.orsegups.sigma.TaticoContaAnalitico"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.util.SortedMap"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.TaticoContaAnalitico"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>


<%
	final NeoUser currentUser = PortalUtil.getCurrentUser();
	final boolean isAdm = (currentUser!=null&&currentUser.isAdm())?true:false;
	final String none = "&nbsp;";

	DecimalFormat f = new DecimalFormat("000000000");

	DecimalFormat formatoReal = new DecimalFormat("#,##0.00");
	
	Long codRota = NeoUtils.safeLong(request.getParameter("codRota"));
%>

<portal:head title="Contas x T�tico">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css">
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/overlib/overlib.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<cw:main>
		<cw:header title="Anal�tico das Contas Por Rota" />
		<cw:body id="area_scroll">
		
			<%
				Connection conn = null;
				PreparedStatement pstm = null;
				ResultSet rs = null;
				
				String descricaoRota = "";
				
				String extraStyle = "style='background-color: #FFEEDD;'";
				try
				{

						conn = PersistEngine.getConnection("SIGMA90");

						StringBuilder sql = new StringBuilder();
						sql.append("SELECT CTRL.ID_ROTA, RT.NM_ROTA, CTRL.ID_EMPRESA, EMP.NM_FANTASIA, CTRL.ID_CENTRAL, 																");
						sql.append("(SELECT TOP 1 RAZAO FROM DBCENTRAL WHERE ID_ROTA = CTRL.ID_ROTA AND ID_EMPRESA = CTRL.ID_EMPRESA AND ID_CENTRAL = CTRL.ID_CENTRAL) AS RAZAOCLIENTE	");
						sql.append("FROM DBCENTRAL CTRL																																	");
						sql.append("INNER JOIN EMPRESA EMP ON EMP.CD_EMPRESA = CTRL.ID_EMPRESA																							");
						sql.append("INNER JOIN ROTA RT ON RT.CD_ROTA = CTRL.ID_ROTA																										");
						sql.append("WHERE CTRL.FG_ATIVO = 1 																															");									  								  				      
						sql.append("AND CTRL.CTRL_CENTRAL = 1 																															"); 							  								  				      
						sql.append("AND CTRL.ID_CENTRAL NOT LIKE 'AAA%' 																												");							  						  				      
						sql.append("AND CTRL.ID_CENTRAL NOT LIKE 'DDDD' 																												");							 						  				      
						sql.append("AND CTRL.ID_CENTRAL NOT LIKE 'FFFF'																													");						  						  				      
						sql.append("AND CTRL.ID_CENTRAL NOT LIKE 'R%'																													");
						sql.append("AND CTRL.ID_ROTA = " + codRota   																													 );
						sql.append("AND EXISTS (select sig.usu_codcli  from [FSOODB04\\SQL02].sapiens.dbo.usu_t160ctr ctr												                ");
						sql.append("		  inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160cvs cvs on cvs.usu_codemp = ctr.usu_codemp												");
						sql.append("                                                                 and cvs.usu_codfil = ctr.usu_codfil 												");
						sql.append("                                                                 and cvs.usu_numctr = ctr.usu_numctr 												");
						sql.append("		  inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160sig sig on sig.usu_codemp = ctr.usu_codemp												");
						sql.append("																 and sig.usu_codfil = ctr.usu_codfil												");
						sql.append("																 and sig.usu_numctr = ctr.usu_numctr												");
						sql.append("																 and sig.usu_numpos = cvs.usu_numpos												");
						sql.append("		  where ((ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate()))													");
						sql.append("			and ((cvs.usu_sitcvs = 'A') or (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= getdate()))													");
						sql.append("			and sig.usu_codcli = CD_CLIENTE)																										");
						sql.append("GROUP BY CTRL.ID_ROTA, RT.NM_ROTA, CTRL.ID_EMPRESA, EMP.NM_FANTASIA, CTRL.ID_CENTRAL																");
						sql.append("ORDER BY CTRL.ID_CENTRAL																															");
								
						pstm = conn.prepareStatement(sql.toString());
						rs = pstm.executeQuery();
						
						List<TaticoContaAnalitico> taticoContasAnalitico= new ArrayList<TaticoContaAnalitico>();
						while (rs.next())
						{
							TaticoContaAnalitico taticoContaAnalitico = new TaticoContaAnalitico();
							
							taticoContaAnalitico.setNomeRota(rs.getString("NM_ROTA"));
							taticoContaAnalitico.setNomeFantasia(rs.getString("NM_FANTASIA"));
							taticoContaAnalitico.setCtrlCentral(rs.getString("ID_CENTRAL"));
							taticoContaAnalitico.setRazaoSocial(rs.getString("RAZAOCLIENTE"));
							
							descricaoRota = rs.getString("NM_ROTA");
							
							taticoContasAnalitico.add(taticoContaAnalitico);
						 }				
						 %>
							<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
							<tr style="cursor: auto">
							<th style="cursor: auto; white-space: normal" width="10%">Empresa Sigma</th>
							<th style="cursor: auto; white-space: normal" width="5%">Conta do Cliente</th>
							<th style="cursor: auto; white-space: normal" width="5%">Raz�o Social do Cliente</th>
							</tr>
							<fieldset class="fieldGroup" >
							<legend class="legend" style="font-size: 1.2em">&nbsp;<%= descricaoRota %>&nbsp;
							</legend>
							<tbody>
							<%
							for (TaticoContaAnalitico taticoObj: taticoContasAnalitico){
							%>

								<tr>
									<td style="white-space: normal; text-align: left"><%=taticoObj.getNomeFantasia()%></td>
									<td style="white-space: normal; text-align: left"><%=taticoObj.getCtrlCentral()%></td>
									<td style="white-space: normal; text-align: left"><%=taticoObj.getRazaoSocial()%></td>
								</tr>
							<%
							}
							%>
					</tbody>
				</table>
			</fieldset>	
			<br />
		
				<%
					} catch (Exception e) {
						e.printStackTrace();
				%>
				<script type="text/javascript">
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
				<%
					}finally {
						try {
										
						OrsegupsUtils.closeConnection(conn, pstm, rs);
						} catch (Exception e) {
						e.printStackTrace();
				%>
				<script type="text/javascript">
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>", 'error');
				</script>
				<%
					}
					}
				%>
			

		</cw:body>
	</cw:main>
</portal:head>