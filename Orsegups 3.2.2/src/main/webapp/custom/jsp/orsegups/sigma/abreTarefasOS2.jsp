<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.net.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.SimpleDateFormat"%>

<%!
public Calendar addBusinessDays(Calendar cal, int numBusinessDays) {
	int numNonBusinessDays = 0;

	for(int i = 0; i < numBusinessDays; i++) {
	cal.add(Calendar.DATE, 1);

	if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
	  numNonBusinessDays++;
	}
	}

	if(numNonBusinessDays > 0) {
		cal.add(Calendar.DATE, numNonBusinessDays);
	}

	if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
		cal.add(Calendar.DATE, 2);
	}
	if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
		cal.add(Calendar.DATE, 1);
	}

  return cal;
}
%>
<%
	String testMode = request.getParameter("testMode");
	String solicitante = request.getParameter("solicitante");
	if(solicitante == null || solicitante.trim().equals(""))	{
		out.print("Erro #1 - Solicitante n�o encontrado");
		return;
	}
	String executor = "";
	
	Calendar cal2 = Calendar.getInstance();
	Calendar cal4 = Calendar.getInstance();
	cal2 = addBusinessDays(cal2, 2);
	cal4 = addBusinessDays(cal4, 4);
	
	SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yyyy");
	String prazo2 = formatter.format(cal2.getTime());
	String prazo4 = formatter.format(cal4.getTime());
	boolean isTest = false;
	if (testMode != null && testMode.trim().equals("sim")) {
		isTest = true;
	}
	//out.println(prazo2);
	//out.println(prazo4);
    //if (1==1)return;
	PreparedStatement st = null;
    PreparedStatement st3 = null;
    
    Connection conn = PersistEngine.getConnection("SIGMA90");
	Connection connSapiens = PersistEngine.getConnection("SAPIENS");

	try {
		InstantiableEntityInfo osInfo = AdapterUtils.getInstantiableEntityInfo("SIGMAControleUltimaOS");
		
		StringBuffer sqlOS = new StringBuffer();
		sqlOS.append(" SELECT c.ID_CENTRAL, c.PARTICAO, os.ID_ORDEM, os.DEFEITO, def.DESCRICAODEFEITO, col.NM_COLABORADOR, c.FANTASIA, c.RAZAO, ");
		sqlOS.append(" p.TX_OBSERVACAO, mp.NM_DESCRICAO, p.DT_PAUSA, c.RESPONSAVEL, c.FONE1, c.FONE2, p.CD_MOTIVO_PAUSA, c.ID_EMPRESA, SUBSTRING(r.NM_ROTA,1,3) AS REG_ROTA");
		sqlOS.append(" FROM dbORdem os WITH (NOLOCK) ");
		sqlOS.append(" INNER JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
		sqlOS.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR 												");
		sqlOS.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO														");
		sqlOS.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE 														 	");
		sqlOS.append(" INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA																			");
		sqlOS.append(" INNER JOIN MOTIVO_PAUSA mp WITH (NOLOCK) ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA             ");
		sqlOS.append(" WHERE 1 = 1 /* col.NM_COLABORADOR NOT LIKE '%TERC%'    AND os.ID_ORDEM = 765446 	*/ ");
		sqlOS.append(" AND     (  						 ");
		sqlOS.append("          (os.FECHADO = 3 AND p.CD_MOTIVO_PAUSA IN (2, 41, 43))   	");
		sqlOS.append("           OR  						   ");
		sqlOS.append(" 		(os.FECHADO = 3 AND p.CD_MOTIVO_PAUSA = 1 AND DT_PAUSA < (CAST((STR( YEAR( GETDATE()-3 ) ) + '/' + STR( MONTH( GETDATE()-3 ) ) + '/' + STR( DAY( GETDATE()-3 ) ))AS DATETIME)))   ");
		sqlOS.append("           OR  						   ");
		sqlOS.append(" 		(os.FECHADO = 3 AND p.CD_MOTIVO_PAUSA = 9 AND DT_PAUSA < (CAST((STR( YEAR( GETDATE()-3 ) ) + '/' + STR( MONTH( GETDATE()-5 ) ) + '/' + STR( DAY( GETDATE()-5 ) ))AS DATETIME)))   ");
		sqlOS.append("         )  			 ");

		st = conn.prepareStatement(sqlOS.toString());
		ResultSet rs = st.executeQuery();

		while(rs.next()) {
			Long numeroOS = new Long(rs.getLong("ID_ORDEM"));
			Long cd_motivo = rs.getLong("CD_MOTIVO_PAUSA");
			//teste 43
			cd_motivo = 43L;
			
			String defeito = rs.getString("DESCRICAODEFEITO");
			String descricaoDefeito = rs.getString("DEFEITO");
			String tecnico = rs.getString("NM_COLABORADOR");
			String cd_empresa = rs.getString("ID_EMPRESA");
			String conta = rs.getString("ID_CENTRAL");
			String particao = rs.getString("PARTICAO");
			String motivoPausa = rs.getString("TX_OBSERVACAO");
			String tipoPausa = rs.getString("NM_DESCRICAO");
			String regionalRota = rs.getString("REG_ROTA");
			String dataPausa = formatter.format(rs.getDate("DT_PAUSA"));
			String responsavel = (rs.getString("RESPONSAVEL") != null ? rs.getString("RESPONSAVEL") : "");
			String fone1 = (rs.getString("FONE1") != null ? rs.getString("FONE1") : "");
			String fone2 = (rs.getString("FONE2") != null ? rs.getString("FONE2") : "");
			String motivoEspecifico = "";
			
			StringBuffer sqlEquip = new StringBuffer();
			sqlEquip.append(" SELECT cvs.USU_CodSer ");
			sqlEquip.append(" FROM USU_T160SIG sig ");
			sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
			sqlEquip.append(" WHERE sig.usu_sigemp = "+cd_empresa+" ");
			sqlEquip.append(" AND sig.usu_sigcon = '"+conta+"' ");
			sqlEquip.append(" AND sig.usu_sigpar = '"+particao+"' ");
			sqlEquip.append(" AND cvs.usu_codser IN ('9002011', '9002004', '9002005', '9002014') ");
			sqlEquip.append(" ORDER BY cvs.usu_sitcvs ");
			
			st3 = connSapiens.prepareStatement(sqlEquip.toString());
			ResultSet rsEquip = st3.executeQuery();
			String flag_equip = "<img src='imagens/icones/sphere_red_16x16-trans.png' alt='Equipamento Cliente'/> Cliente";
			if (rsEquip.next()) {
				// Equipamento Orsegups
				flag_equip = "<img src='imagens/icones/sphere_yellow_16x16-trans.png' alt='Equipamento Orsegups'/> Orsegups";
			}
			
			if (motivoPausa == null) {
				motivoPausa = "";
			}
			if (cd_motivo == 41) {
				motivoEspecifico = "Pend�ncia de agendamento";
			}
			if (cd_motivo == 42) {
				motivoEspecifico = "Habilita��o do Monitoramento/Faturamento";
			}
			if (cd_motivo == 43) {
				motivoEspecifico = "Pend�ncia de Negocia��o";
			}
			
			String cliente = rs.getString("FANTASIA") + " (" + rs.getString("RAZAO") + ")";
			out.println(numeroOS + "<br>");
			List listOSTarefa = PersistEngine.getObjects(osInfo.getEntityClass(), new QLEqualsFilter("numeroOS", numeroOS), 0, 1, "");
			if (listOSTarefa.isEmpty()) {
				String titulo = URLEncoder.encode("OS: " + numeroOS + " - " + rs.getString("FANTASIA"),"ISO-8859-1");
				String descricao = "";
				descricao = "<strong>DETALHES DA OS:</strong><br>";
				descricao = descricao + "<strong>Conta:</strong> " + conta + "[" + particao + "]" + "<br>";
				descricao = descricao + "<strong>Cliente:</strong> " + cliente + "<br>";
				descricao = descricao + "<strong>Equipamento:</strong> " + flag_equip + "<br>";
				descricao = descricao + "<strong>Respons�vel:</strong> " + responsavel + "<br>";
				descricao = descricao + "<strong>Fone 1:</strong> " + fone1 + "<br>";
				descricao = descricao + "<strong>Fone 2:</strong> " + fone2 + "<br>";
				descricao = descricao + "<strong>Defeito:</strong> " + defeito + "<br>";  
				descricao = descricao + "<strong>Descri��o:</strong> " + descricaoDefeito + "<br>";
				descricao = descricao + "<strong>Pausada em:</strong> " + dataPausa + "<br>";
				descricao = descricao + "<strong>Tipo da Pausa:</strong> " + tipoPausa + "<br>";
				descricao = descricao + "<strong>Motivo da Pausa:</strong> " + motivoPausa + "<br>";
				descricao = descricao + "<strong>T�cnico:</strong> " + tecnico + "<br>";  
				descricao = descricao + "<br><br>";  
				descricao = descricao + "<strong>Motivo de abertura: " + motivoEspecifico + "</strong>";  
				descricao = descricao.replaceAll("'", "");
				descricao = descricao.replaceAll("\"", "");
				descricao = URLEncoder.encode(descricao,"ISO-8859-1");

				String urlTarefa = "";
				if ((cd_motivo == 2) || (cd_motivo == 9)) {
					urlTarefa = PortalUtil.getBaseURL() + "custom/jsp/orsegups/iniciarTarefaSimples.jsp?avanca=nao&solicitante=" + solicitante + "&origem=3&descricao="+ descricao + "&titulo=" + titulo + "&prazo=" + prazo2;
				} else if ((cd_motivo == 1) || (cd_motivo == 41)) {
					if (("RSL".equals(regionalRota)) || ("TRO".equals(regionalRota)) || ("CUA".equals(regionalRota)) || ("BNU".equals(regionalRota)) || ("BQE".equals(regionalRota))  || ("GPR".equals(regionalRota))) {
						executor = "maira.rosa";
					}
					else if (("LGS".equals(regionalRota)) || ("CCO".equals(regionalRota)) || ("JGS".equals(regionalRota)) || ("JLE".equals(regionalRota))) {
							executor = "giani.perico";
					}
					else if ("IAI".equals(regionalRota)) {
						executor = "fabiane.matos";
					}
					else if ("SOO".equals(regionalRota)){
						executor = "dgecy.trupel";
					}
					else if ("CTA".equals(regionalRota)){
						//este caso problema fabio
						executor = "irene.xavier";
					}
					else {
						executor = "dayane.varella";
					}
					urlTarefa = PortalUtil.getBaseURL() + "custom/jsp/orsegups/iniciarTarefaSimples.jsp?avanca=sim&solicitante=" + solicitante + "&executor=" + executor + "&origem=3&descricao="+ descricao + "&titulo=" + titulo + "&prazo=" + prazo4;
				
				// Habilitacao (libera contrato para faturamento)
				} else if (cd_motivo == 42) {
					executor = "fernanda.martins";
					urlTarefa = PortalUtil.getBaseURL() + "custom/jsp/orsegups/iniciarTarefaSimples.jsp?avanca=sim&solicitante=" + solicitante + "&executor=" + executor + "&origem=3&descricao="+ descricao + "&titulo=" + titulo + "&prazo=" + prazo2;				
				}
				 else if (cd_motivo == 43) {
						executor = "alfredo.anjos";
						urlTarefa = PortalUtil.getBaseURL() + "custom/jsp/orsegups/iniciarTarefaReversaoDeCancelamento2.jsp?avanca=sim&solicitante=" + solicitante + "&executor=" + executor + "&origem=3&descricao="+ descricao + "&titulo=" + titulo + "&prazo=" + prazo2;
						System.out.println("URL: "+urlTarefa);
					}
				//out.println(urlTarefa);
				//out.println("<br>");
				if (!isTest) { 
					URL urlAux = new URL(urlTarefa);
					URLConnection uc = urlAux.openConnection();
					BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
					String retorno = in.readLine();
					//System.out.println(retorno);
					if (retorno != null && (!retorno.trim().contains("Erro"))) {
						NeoObject objOS = osInfo.createNewInstance();
						EntityWrapper wrpOS = new EntityWrapper(objOS);
						wrpOS.findField("numeroOS").setValue(numeroOS);
						wrpOS.findField("numeroTarefa").setValue(Long.parseLong(retorno));
						
					    PersistEngine.persist(objOS);	

						/*
						 *
						 * Realiza fechamento daOS
						 *
						 */
						//FECHADO=1, OPFECHOU, FECHAMENTO, IDOSCAUSADEFEITO, IDOSSOLUCAO
				    /*		String textoFechamento = "Fechamento autom�tico via Tarefa: " + retorno;
						StringBuffer sqlFechaOS = new StringBuffer();
			    		sqlFechaOS.append("UPDATE dbORDEM SET FECHADO = 1, OPFECHOU = 11010, FECHAMENTO = GETDATE(), IDOSCAUSADEFEITO = 1, IDOSSOLUCAO = 249, EXECUTADO = ? WHERE ID_ORDEM = ? ");
						PreparedStatement stFechaOS = conn.prepareStatement(sqlFechaOS.toString());
						stFechaOS.setString(1, textoFechamento);
						stFechaOS.setLong(2, numeroOS);
						stFechaOS.executeUpdate();
						stFechaOS.close();*/
			
					} else {
						out.print("Erro #7 - Erro ao criar tarefa");
					}
					
				}
			
			}
		}
		st.close();		
		
		out.print("OK");		
		
	} catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #8 - Erro ao gerar OS");
		return;
	} 
	finally
	{
		try
		{
			conn.close();
			connSapiens.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			out.print("Erro #9 - Erro ao finalizar conex�o");
		}
	}
%>
