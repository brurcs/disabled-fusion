<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>

<%
	Boolean requestPhone = false;
%>

<portal:head title="Gest�o de Entrada/Sa�da Colaboradores">
<cw:main>
	<cw:header title="Gest�o de Entrada/Sa�da Colaboradores" />
	<cw:body id="area_scroll">
	
	<FIELDSET class="fieldGroup">
	<LEGEND class="legend">&nbsp;Colaboradores&nbsp;</LEGEND>
		
<%
	
	String area = request.getParameter("area");
	if(area == null || area.trim().equals(""))	{
		area = "TAT";
 	}
	final String none = "&nbsp;";
	
	QLEqualsFilter filterAA = new QLEqualsFilter("id_ramo", 10058L);
	QLEqualsFilter filterHUM = new QLEqualsFilter("id_ramo", 10059L);
	QLEqualsFilter filterACL = new QLEqualsFilter("id_ramo", 10060L);
	QLEqualsFilter filterTEC = new QLEqualsFilter("id_ramo", 10061L);
	QLEqualsFilter filterADM = new QLEqualsFilter("id_ramo", 15021L);
	QLEqualsFilter filterPLC = new QLEqualsFilter("id_ramo", 15145L);
	
	QLGroupFilter filterRamo = new QLGroupFilter("OR");
	if (area.equals("TAT")) {
		filterRamo.addFilter(filterAA);
		filterRamo.addFilter(filterHUM);
		filterRamo.addFilter(filterACL);
		filterRamo.addFilter(filterPLC);
		requestPhone = true;
	} else if (area.equals("TEC")) {
		filterRamo.addFilter(filterTEC);
	} else if (area.equals("ADM")) {
		filterRamo.addFilter(filterADM);
	}
	

	QLGroupFilter filterAtivo = new QLGroupFilter("AND");
	filterAtivo.addFilter(new QLEqualsFilter("ctrl_central", new Boolean(true)));
	QLOpFilter datefilterini = new QLOpFilter("cd_cliente", "<>", 93587L);
	filterAtivo.addFilter(datefilterini);

	QLGroupFilter filterControle = new QLGroupFilter("AND");	
	filterControle.addFilter(filterRamo);
	filterControle.addFilter(filterAtivo);
	
	//System.out.println(filterControle);
	ExternalEntityInfo infoAA = (ExternalEntityInfo)EntityRegister.getInstance().getCache().getByString("SIGMAVCLIENTES");
	List<NeoObject> listaAA = (List<NeoObject>)PersistEngine.getObjects(infoAA.getEntityClass(), filterControle, -1, -1, "fantasia ASC, particao ASC");	
        //System.out.println(listaAA.size());
	if(listaAA.isEmpty()) {
		out.print("&nbsp;&nbsp;&nbsp;&nbsp;<i>Nenhum Colaborador encontrado!</i><br/><br/>");
	} else {
%>
	<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
	<thead style="cursor: auto">
		<tr style="cursor: auto">
			<th style="cursor: auto">Conta</th>
			<th style="cursor: auto">Part.</th>
			<th style="cursor: auto" class="min">Entrada</th>
			<th style="cursor: auto" class="min">Sa�da</th>
			<th style="cursor: auto" class="min">Troca Viatura</th>
			<th style="cursor: auto">Rota</th>
			<th style="cursor: auto">AA</th>
			<th style="cursor: auto">VTR SIGMA</th>
			<th style="cursor: auto">Matr�cula</th>
			<th style="cursor: auto">C�digo URA</th>
		</tr>
	</thead>
	<tbody>	

	<c:forEach items="<%= listaAA %>" var="itemAA">
<%
			try {
			EntityWrapper wrpAA = new EntityWrapper((NeoObject)itemAA); 
			String codigoAA = ((Long)(wrpAA.getValue("cd_cliente"))).toString();
			String nomeAA = (String)wrpAA.getValue("fantasia");
			String contaAA = (String)wrpAA.getValue("id_central");
			String particaoAA = (String)wrpAA.getValue("particao");
			String matriculaAA = (String)wrpAA.getValue("responsavel");
			String vtrAA = (String)wrpAA.getValue("cgccpf");
			String empresaAA = ((Long)(wrpAA.getValue("cd_empresa"))).toString();

%>			<tr>
			<td><%= contaAA %></td>
			<td><%= particaoAA %></td>
			<td align='center'> <a href='javascript:doAction("login", "<%= codigoAA %>");'><img src='imagens/icones_final/check_16x16-trans.png' align='absmiddle' /></a></td>
			<td align='center'> <a href='javascript:doAction("logoff", "<%= codigoAA %>");'><img src='imagens/icones_final/sphere_red_logout_16x16-trans.png' align='absmiddle' /></a></td>
			<td align='center'> <a href='javascript:doAction("chgvtr", "<%= codigoAA %>");'><img src='imagens/icones_final/reatribuir_16x16-trans.png' align='absmiddle' /></a></td>
			<td><%= nomeAA.substring(0, nomeAA.lastIndexOf('-')) %></td>
			<td><%= nomeAA.substring(nomeAA.lastIndexOf('-')+1) %></td>
			<td><%= vtrAA %></td>
			<td><%= matriculaAA %></td>
			<td><%= codigoAA %></td>
			</tr>
<%
			}
			catch(Exception e)
			{
			e.printStackTrace();
			System.out.println("Erro "+e.getMessage().toString());
			%>	
			<script type="text/javascript" >
			alert("Erro! "+<%=e.getMessage().toString()%>);
			</script>
			<%
			}
		%>
		</c:forEach>							
		<%
	}
%>
	</tbody>
	</table>
	[<%= listaAA.size() %>]
	<br>
	</fieldset>
	
	</cw:body>
</cw:main>
</portal:head>

<script language="JavaScript">

function doAction(acao, codigo) {

	var response = "";
	var phoneNumber = "";
	var requestPhone = <%= requestPhone %>;
	
	if ((acao == "login") || (acao == "chgvtr")) {

		var cd_placa = prompt("Insira o n�mero da Placa.", "0000");
		if (cd_placa) {
			if (requestPhone) {
			   phoneNumber = prompt("Insira o DDD+Celular (Ex.: 4891235713).", "");
			}
			if (cd_placa && ((!requestPhone) || (requestPhone && phoneNumber))) {
				response = callSync('<%= PortalUtil.getBaseURL() %>custom/jsp/orsegups/sigma/entradaAAviaSNEP.jsp?verboseError=yes&acao=' + acao + '&codigo='+ codigo +'&cd_placa='+ cd_placa +'&origem=fusion&fone='+phoneNumber+'&username=<%= PortalUtil.getCurrentUser().getCode() %>');

				if (response.indexOf("OK") != -1) {
					alert("OK - A��o realizada com sucesso.");
				} else {
					alert("ERRO: " + response);
				}
			}
		}
		
	} else {
		response = callSync('<%= PortalUtil.getBaseURL() %>custom/jsp/orsegups/sigma/entradaAAviaSNEP.jsp?verboseError=yes&acao=' + acao + '&codigo='+ codigo +'&origem=fusion&fone=0000000000&username=<%= PortalUtil.getCurrentUser().getCode() %>');	
		if (response.indexOf("OK") != -1) {
			alert("OK - A��o realizada com sucesso.");
		} else {
			alert("ERRO: " + response);
		}
	}
	
}

</script>