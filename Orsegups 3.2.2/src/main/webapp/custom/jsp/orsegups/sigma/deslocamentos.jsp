<%@page import="com.neomind.fusion.custom.orsegups.sigma.SigmaUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
 
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.util.NeoCalendarUtils"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>


<script type="text/javascript" 
src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	
<style type="text/css">
.evento_prioridade1 {
  background-color: #ffffff;
  color: #000000;
}
.evento_prioridade2 {
  background-color: #ffffff;
  color: #ff0000;
}
.evento_prioridade9 {
  background-color: #ffffff;
  color: #ff0000;
}
.evento_prioridade3 {
  background-color: #ffffff;
  color: #ff9a00;
}
.evento_prioridade10 {
  background-color: #ffffff;
  color: #959595;
}
.evento_prioridade4 {
  background-color: #ffffff;
  color: #ff9a00;
}
.evento_prioridade5 {
  background-color: #ffffff;
  color: #3934f7;
}
.evento_atendido {
  background-color: #ffffff;
  color: #959595;
}

</style>
<meta http-equiv="refresh" content="1800"/>
<%		
	String regional = request.getParameter("regional");
	String regionalTitulo = regional;
	if(regional == null || regional.trim().equals(""))	{
		regional = "";
		regionalTitulo = "Geral";
 	}
	String ramal = "";
	
	EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
	ramal = (String) usuarioOrigemWrapper.findValue("ramal");
	
	if(ramal == null || ramal.trim().equals(""))	{
		ramal = "";
 	}

	String ramos = request.getParameter("ramos"); 
	if(ramos == null || ramos.trim().equals("") || (ramos.trim().length() > 25 ))	{
		ramos = "10058,10059";
 	}
	String asseio = request.getParameter("asseio"); 
	if(asseio == null || asseio.trim().equals(""))	{
		asseio = "show"; // no-sem asseio // only-somente asseio // show-nao filtra
 	}
	String humana = request.getParameter("humana"); 
	if(humana == null || humana.trim().equals(""))	{
		humana = "show"; // no-sem humana // only-somente humana // show-nao filtra
 	}

	boolean booleanMostrarAtendidos = false;
	String mostrarAtendidos = request.getParameter("mostrarAtendidos");
	if(mostrarAtendidos != null && mostrarAtendidos.trim().equals("sim"))	{
		booleanMostrarAtendidos = true;
 	}

	//System.out.println(mostrarAtendidos);
	//System.out.println(booleanMostrarAtendidos);
	//System.out.println(regional);
%>

<portal:head title="<%= regionalTitulo %>">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

<cw:main>
	<cw:header title="<%= regionalTitulo %>" />
	<cw:body id="area_scroll">

	
	<input onClick="javascript:document.location.reload();" type="button" value="Atualizar"/>
<%
	if (booleanMostrarAtendidos) {
%>
		<input onClick="javascript:document.location.href='/fusion/custom/jsp/orsegups/sigma/deslocamentos.jsp?regional=<%=regional%>&asseio=<%=asseio%>&humana=<%=humana%>&mostrarAtendidos=nao'" type="button" value="Ocultar atendidos"/>
<%
	} else {
%>	
		<input onClick="javascript:document.location.href='/fusion/custom/jsp/orsegups/sigma/deslocamentos.jsp?regional=<%=regional%>&asseio=<%=asseio%>&humana=<%=humana%>&mostrarAtendidos=sim'" type="button" value="Mostrar atendidos"/>
<%
	} 
%>	
	<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
	<thead style="cursor: auto">
		<tr style="cursor: auto">
			<th style="cursor: auto" width="5%">Evento</th>
			<th style="cursor: auto" width="20%">Conta [Parti��o] Cliente</th>
			<th style="cursor: auto" width="5%">Logs</th>
			<th style="cursor: auto" width="1%">Sit.</th>
			<th style="cursor: auto" width="10%">Recebido</th>
			<th style="cursor: auto" width="2%"><img src="imagens/icones_final/clock.png" title="Tempo para a CM deslocar - IDC"/></th>
			<th style="cursor: auto" width="10%">Deslocado</th>
			<th style="cursor: auto" width="2%"><img src="imagens/icones_final/clock.png" title="Tempo para iniciar o deslocamento - TID"/></th>
			<th style="cursor: auto" width="10%">In�cio Desloc.</th>
			<th style="cursor: auto" width="2%"><img src="imagens/icones_final/clock.png" title="Tempo para chegada ao local - DSL"/></th>
			<th style="cursor: auto" width="10%">No Local</th>
			<th style="cursor: auto" width="2%"><img src="imagens/icones_final/clock.png" title="Tempo para atendimento ao cliente - ATD"/></th>
			<th style="cursor: auto" width="2%"><img src="imagens/icones_final/clock.png" title="Tempo at� o fechamento do evento"/></th>
			<th style="cursor: auto" width="20%">Endere�o</th>
		</tr>
	</thead>
	<tbody>	

<%!


    private String getLogViatura(String placa)
    {
    	QLGroupFilter viaturaFilter = new QLGroupFilter("AND");
    	viaturaFilter.addFilter(new QLEqualsFilter("placa", placa));
    	viaturaFilter.addFilter(new QLEqualsFilter("emUso", true));
   	NeoObject otsViatura = PersistEngine.getObject(AdapterUtils.getEntityClass("OTSViatura"), viaturaFilter);
    
	String resultLog = "";
	if (otsViatura != null) {
		
		EntityWrapper entityWrapper = new EntityWrapper(otsViatura);
		
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("viatura", otsViatura));
		groupFilter.addFilter(new QLOpFilter("dataLog", ">=", (GregorianCalendar) entityWrapper.findValue("dataMovimentacao")));
		
		Class clazz = AdapterUtils.getEntityClass("OTSLogViatura");
		
		Collection<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilter);
		
		if(logs != null && logs.size() > 0)
		{
			for(NeoObject log : logs)
			{
				EntityWrapper logWrapper = new EntityWrapper(log);
				
				String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
				String texto = (String) logWrapper.findValue("textoLog");
				
				if(texto != null && !texto.isEmpty())
				{
					String linha = "<li>"+formatedDate+" - "+texto+"</li>";
					resultLog += linha;
				}
			}
		}
	}
    	return resultLog.replaceAll("\"", "'");
    }

public static String getTimeSpent(long timeSpent) {

		// string de retorno
		String str = "";

		// quantidade de anos
		long years = timeSpent / 31536000;

		// se houver anos
		if (years >= 1) {
			// mostra quantos anos passaram
			str = years + "a";

			// retira do total, o tempo em segundos dos anos passados
			timeSpent = timeSpent - (years * 31536000);
		}

		// quantidade de meses (anos/12 e n�o dias*30)
		long months = timeSpent / 2628000;

		// se houver meses
		if (months >= 1) {
			str = str + months + "m";

			// retira do total, o tempo em segundos dos meses passados
			timeSpent = timeSpent - (months * 2628000);
		}

		// quantidade de dias
		long days = timeSpent / 86400;

		// se houver dias
		if (days >= 1) {

			// mostra quantos dias passaram
			str = str + days + "d";

			// retira do total, o tempo em segundos dos dias passados
			timeSpent = timeSpent - (days * 86400);
		}

		// quantidade de horas
		long hours = timeSpent / 3600;

		// se houver horas
		if (hours >= 1) {

			// mostra quantas horas passaram
			str = str + hours + "h";

			// retira do total, o tempo em segundos das horas passados
			timeSpent = timeSpent - (hours * 3600);
		}

		// quantidade de minutos
		long minutes = timeSpent / 60;

		// se houver minutos
		if (minutes >= 1) {

			// mostra quantos minutos passaram
			str = str + minutes + "m";

			// retira do total, o tempo em segundos dos minutos passados
			timeSpent = timeSpent - (minutes * 60);
		}

		// mostra quantos minutos passaram
		if (timeSpent > 0)
			str = str + timeSpent + "s";

		return str;

	}
	
	public static String encodeHTML(String s) {
		StringBuffer out = new StringBuffer();
		for(int i=0; i<s.length(); i++)
		{
			char c = s.charAt(i);
			if(c > 127 || c=='"' || c=='<' || c=='>')
			{
			   out.append("&#"+(int)c+";");
			}
			else
			{
				out.append(c);
			}
		}
		return out.toString();
	}	
%>		

<%		
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.sigma.entradaAAviaSNEP");
	final String none = "&nbsp;";

    PreparedStatement st = null;
    try {
		String nomeFonteDados = "SIGMA90";
		Connection conn = PersistEngine.getConnection(nomeFonteDados);

		SimpleDateFormat formatter = new SimpleDateFormat("HH':'mm':'ss");
		Timestamp tsAuxEvtRec = null;
		Timestamp tsAuxEvtDes = null;
		Timestamp tsAuxEvtIni = null;
		Timestamp tsAuxEvtLoc = null;
		Timestamp tsAuxEvtFec = null;
		Date dateAuxAgora = null;
		String auxTimeRec = "";
		String auxTimeDes = "";
		String auxTimeIni = "";
		String auxTimeLoc = "";
		long longTempoMinutos = 0;
		long tempoAtd = 0;
		String sTempoMinutosRec = "";
		String sTempoMinutosAtd = "";
		String sTempoMinutosDes = "";
		String sTempoMinutosIni = "";
		String sTempoMinutosLoc = "";
		String link_discagem = "javascript:unavaliable();";
		Long cd_livro_ata = 0L;
		/*
		 *
		 * Recuperando Livro Ata Aberto
		 *
		 */	 
		StringBuffer sqlLivroAta = new StringBuffer();
		sqlLivroAta.append(" SELECT TOP 1 CD_LIVRO_ATA  "); 
		sqlLivroAta.append(" FROM LIVRO_ATA WITH (NOLOCK) ");
		sqlLivroAta.append(" WHERE FG_PLANTAO = 0  ");
		sqlLivroAta.append(" ORDER BY CD_LIVRO_ATA DESC ");
		PreparedStatement stLivroAta = conn.prepareStatement(sqlLivroAta.toString());
		ResultSet rsLivroAta = stLivroAta.executeQuery();
		if (rsLivroAta.next()) {
			cd_livro_ata = rsLivroAta.getLong("CD_LIVRO_ATA");
		} else {
				out.print("ERRO: Livro Ata nao encontrado. Verifique com a CM.");
			log.warn("##### DSL> ERRO: Livro Ata nao encontrado. Verifique com a CM.");
			return;
		}
		rsLivroAta.close();
		stLivroAta.close();				
		
		// LOGADOS
		StringBuffer sqlLogados = new StringBuffer();
 		sqlLogados.append(" SELECT v.CD_VIATURA, v.NM_VIATURA, v.FG_CONECTADO, ISNULL(v.NM_PLACA, '') AS NM_PLACA, laf.DT_INICIO_USO_VIATURA, ISNULL(r.TELEFONE, '') AS TELEFONE, ISNULL(c.FANTASIA, '') AS FANTASIA, ISNULL(c.CD_CLIENTE, '0') AS CD_CLIENTE ");
 		sqlLogados.append(" FROM LIVRO_ATA_FROTA laf  ");
 		sqlLogados.append(" INNER JOIN VIATURA v ON v.CD_VIATURA = laf.CD_VIATURA ");
		sqlLogados.append(" LEFT OUTER JOIN dbCENTRAL c WITH (NOLOCK) ON CONVERT(VARCHAR, v.CD_VIATURA) = c.CGCCPF ");
 		sqlLogados.append(" LEFT OUTER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA 							 ");		  
  		sqlLogados.append(" WHERE laf.DT_FIM_USO_VIATURA IS NULL ");
 		sqlLogados.append(" AND laf.CD_LIVRO_ATA = " + cd_livro_ata + " ");
		sqlLogados.append(" AND v.NM_VIATURA LIKE '"+ regional +"%'                                                             	                          ");
		if (asseio.equals("only")) {
			sqlLogados.append("AND ( v.NM_VIATURA LIKE '%-AD-%' OR v.NM_VIATURA LIKE '%-AN-%' )                                                                              		        ");
		}
		if (asseio.equals("no")) {
			sqlLogados.append("AND v.NM_VIATURA NOT LIKE '%-AD-%' AND  v.NM_VIATURA NOT LIKE '%-AN-%'                                                                              		        ");
		}
		if (humana.equals("only")) {
			sqlLogados.append("AND (v.NM_VIATURA LIKE '%-CFD-%' OR v.NM_VIATURA LIKE '%-CFN-%' OR v.NM_VIATURA LIKE '%-HD-%' OR v.NM_VIATURA LIKE '%-HN-%' )                                                                              		        ");
		}
		if (humana.equals("no")) {
			sqlLogados.append("AND v.NM_VIATURA NOT LIKE '%-CFD-%' AND v.NM_VIATURA NOT LIKE '%-CFN-%' AND v.NM_VIATURA NOT LIKE '%-HD-%' AND  v.NM_VIATURA NOT LIKE '%-HN-%'                                                                              		        ");
		}
		sqlLogados.append("ORDER BY v.NM_VIATURA, laf.DT_INICIO_USO_VIATURA 												");

		// 0-N�o Atendido
		// 1-Espera
		// 2-Deslocamento
		// 3-No Local
		// 4-Fechado
		// 5-Observa��o
		// 6-URA
		// 7-Nao Usado
		// 8-Oculto do monitoramento (usado no tratamento autom�tico)
		// 9-Deslocamento Iniciado
		// 10 - Atendimento Concluido

		PreparedStatement stLogados = conn.prepareStatement(sqlLogados.toString());
		ResultSet rsLogados = stLogados.executeQuery();

		String nm_viatura_anterior = "primeira";
		String icone_vtr = "<img src='imagens/custom/police.png' title='Viatura'/>&nbsp;";
		String linha_rota = "";
		String class_cor_evento = "";
		String nm_status = "";
		String nm_viatura = ".";
		Long cd_viatura = 0L;
		String placa = "";
		String nm_viatura_display = "";
		int qtdAtendimentos = 0;
		int qtdAtendidos = 0;
					
		while(rsLogados.next()) {
			qtdAtendimentos = 0;
			qtdAtendidos = 0;
			
			cd_viatura = rsLogados.getLong("CD_VIATURA");
			placa = rsLogados.getString("NM_PLACA");
			Timestamp timeLoginAA = rsLogados.getTimestamp("DT_INICIO_USO_VIATURA");
			String horaLoginAA = formatter.format(timeLoginAA);
			nm_viatura_display = rsLogados.getString("FANTASIA");
			nm_viatura_display = nm_viatura_display.substring(nm_viatura_display.lastIndexOf('-')+1);	
			
			
			String telefone = "";
			
			if(cd_viatura != null)
			{
			QLGroupFilter groupFilters = new QLGroupFilter("AND");
			groupFilters.addFilter(new QLEqualsFilter("codigoViatura", String.valueOf(cd_viatura)));
			groupFilters.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
			NeoObject objOTSViatura = PersistEngine.getObject(AdapterUtils.getEntityClass("OTSViatura"), groupFilters);
			if(objOTSViatura != null)
			{
				NeoObject neoObj = (NeoObject) objOTSViatura;

				EntityWrapper psWrappers = new EntityWrapper(neoObj);
				
				telefone = (String) psWrappers.getValue("telefone");
			}
			else
			{
				telefone = rsLogados.getString("TELEFONE");
			}
			}
			if (!ramal.equals("")) {
				

				link_discagem = "javascript:dial(\""+ramal+"\", "+ "\"0" + telefone.replaceAll("\\D+","") + "\");";
			}
			nm_viatura_display = nm_viatura_display + " - " + rsLogados.getString("CD_CLIENTE")  + " - " + rsLogados.getString("NM_VIATURA") + " <a href='"+link_discagem+"'><img src='imagens/custom/phone.png' align='absMiddle' title='Discar para o telefone da rota'>&nbsp;" + telefone + "</a> <img src='imagens/custom/car.png' align='absMiddle' title='Viatura que est� utilizando'> " + placa;
			
			/////// LOG VIATURA ////////
			String logViatura = getLogViatura(placa);
			if (logViatura == null) {
				logViatura = "Vazio";
			} else {
				logViatura = "<li>" + logViatura;
				logViatura =  encodeHTML(logViatura.replace("\n", "<br><li>")); 
			}
			logViatura =  encodeHTML(logViatura.replace("\n", "<br>")); 
			
			%>
				<tr>
				  <td style='font-weight: bold' colspan='14'><img src='imagens/custom/police.png' title='Viatura'  align="absMiddle"/>&nbsp;<%= SigmaUtils.getLogViaturaLink(cd_viatura) %>&nbsp;<%=nm_viatura_display %>
				  <a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%=logViatura%> ', STICKY, CLOSETEXT, 'Fechar', CAPTION, 'Log da Viatura', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);" onmouseout="return nd();">&nbsp;<img src="imagens/icones_final/document_v_16x16-trans.png" align="absMiddle"/></a></td></tr>
				</tr>
		
			<%
			
			///////// EVENTOS FECHADOS /////////////
			if (booleanMostrarAtendidos) {
				StringBuffer sqlEventosFechados = new StringBuffer();
				sqlEventosFechados.append(" SELECT  h.CD_EVENTO, h.NU_PRIORIDADE, c.CD_CLIENTE, v.NM_VIATURA, h.FG_STATUS, fe.NM_FRASE_EVENTO, v.CD_VIATURA, ");		
				sqlEventosFechados.append(" h.DT_RECEBIDO, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, c.ENDERECO,   ");			
				sqlEventosFechados.append(" cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, h.DT_FECHAMENTO, TX_OBSERVACAO_FECHAMENTO, TX_OBSERVACAO_GERENTE, h.DT_ESPERA_DESLOCAMENTO "); 
				sqlEventosFechados.append(" FROM VIEW_HISTORICO h WITH (NOLOCK) ");
				sqlEventosFechados.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
				sqlEventosFechados.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON  v.CD_VIATURA = h.CD_VIATURA ");
				sqlEventosFechados.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK) ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
				sqlEventosFechados.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE ");
				sqlEventosFechados.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE ");
				sqlEventosFechados.append(" WHERE h.CD_VIATURA = ? AND h.DT_RECEBIDO >= DATEADD(HH,-14,GETDATE()) ");
				sqlEventosFechados.append(" AND DT_FECHAMENTO IS NOT NULL AND DT_VIATURA_DESLOCAMENTO IS NOT NULL AND v.NM_VIATURA LIKE '"+ regional +"%' ");
				sqlEventosFechados.append(" ORDER BY v.NM_VIATURA, DT_RECEBIDO ");
				PreparedStatement stEventosFechados = conn.prepareStatement(sqlEventosFechados.toString());
				stEventosFechados.setLong(1, cd_viatura);
// 				stEventosFechados.setTimestamp(2, timeLoginAA);
				ResultSet rsEventosFechados= stEventosFechados.executeQuery();
						
				while(rsEventosFechados.next()) {
				
					qtdAtendidos++;
					%>
					 <tr>
						<td class='evento_atendido'><%= rsEventosFechados.getString("CD_EVENTO") %><a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= rsEventosFechados.getString("CD_EVENTO") %> - <%= rsEventosFechados.getString("NM_FRASE_EVENTO") %>', CAPTION, 'Evento', TEXTSIZE, '10px', BORDER, 10);" onmouseout="return nd();"><img src="imagens/icones_final/information_16x16-trans.png"/></a>
						<td class='evento_atendido'><%= rsEventosFechados.getString("ID_CENTRAL") %>[<%= rsEventosFechados.getString("PARTICAO") %>]<%= rsEventosFechados.getString("FANTASIA") %>&nbsp;(&nbsp;<%= rsEventosFechados.getString("RAZAO") %>&nbsp;)</td>
					<%
					String logTexto = rsEventosFechados.getString("TX_OBSERVACAO_FECHAMENTO");
					if (logTexto == null) {
						logTexto = "Vazio";
					} else {
						logTexto = "<li>" + logTexto;
						logTexto =  encodeHTML(logTexto.replace("\n", "<br><li>")); 
					}
					%>
					   <td class='evento_atendido'>
					   <a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= logTexto %>', CAPTION, 'Log', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);" onmouseout="return nd();">&nbsp;<img src="imagens/icones_final/document_v_16x16-trans.png"/></a>
					<%
					logTexto = rsEventosFechados.getString("TX_OBSERVACAO_GERENTE");
					if (logTexto == null) {
						logTexto = "Vazio";
					} else {
						logTexto = "<li>" + logTexto;
						logTexto =  encodeHTML(logTexto.replace("\n", "<br><li>")); 
					}
					logTexto =  encodeHTML(logTexto.replace("\n", "<br>")); 
					%>
					  <a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= logTexto %>', CAPTION, 'Log Ger�ncia', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);" onmouseout="return nd();">&nbsp;<img src="imagens/icones_final/doc_link.png"/></a>
					 </td>
					 <td align='center' class='evento_atendido'><img src='imagens/icones_final/check_16x16-trans.png' alt='Finalizado'/></td>
					<%
					tsAuxEvtRec = rsEventosFechados.getTimestamp("DT_RECEBIDO");
					tsAuxEvtDes = rsEventosFechados.getTimestamp("DT_ESPERA_DESLOCAMENTO");
					tsAuxEvtIni = rsEventosFechados.getTimestamp("DT_VIATURA_DESLOCAMENTO");
					tsAuxEvtLoc = rsEventosFechados.getTimestamp("DT_VIATURA_NO_LOCAL");
					tsAuxEvtFec = rsEventosFechados.getTimestamp("DT_FECHAMENTO");
					dateAuxAgora = new Date();
					sTempoMinutosDes = none;
					sTempoMinutosIni = none;
					sTempoMinutosRec = none;
					sTempoMinutosAtd = none;
					sTempoMinutosLoc = none;
					auxTimeDes = none;
					auxTimeRec = none;
					auxTimeLoc = none;
					tempoAtd = 0;

					if (tsAuxEvtRec != null) {
						auxTimeRec = formatter.format(tsAuxEvtRec);
					}
					if (tsAuxEvtRec != null) {
						sTempoMinutosRec = getTimeSpent(Math.round((tsAuxEvtFec.getTime() - tsAuxEvtRec.getTime()) / 1000));
					}
					if (tsAuxEvtDes != null) {
						auxTimeDes = formatter.format(tsAuxEvtDes);
					}			
					if ((tsAuxEvtDes != null) && (tsAuxEvtIni != null)) {
						sTempoMinutosIni = getTimeSpent(Math.round((tsAuxEvtDes.getTime() - tsAuxEvtIni.getTime()) / 1000 ));
					}			
					if (tsAuxEvtIni != null) {
						auxTimeIni = formatter.format(tsAuxEvtIni);
					}			
					if ((tsAuxEvtDes != null) && (tsAuxEvtRec != null)) {
						sTempoMinutosDes = getTimeSpent(Math.round((tsAuxEvtDes.getTime() - tsAuxEvtRec.getTime()) / 1000 ));
						tempoAtd += Math.round((tsAuxEvtDes.getTime() - tsAuxEvtRec.getTime()) / 1000 );						
					}			
					if (tsAuxEvtLoc != null) {
						auxTimeLoc = formatter.format(tsAuxEvtLoc);
					}
					if ((tsAuxEvtLoc != null) && (tsAuxEvtDes != null)) {
						sTempoMinutosLoc = getTimeSpent(Math.round(tsAuxEvtLoc.getTime() - tsAuxEvtDes.getTime()) / 1000);
						tempoAtd += Math.round(tsAuxEvtLoc.getTime() - tsAuxEvtDes.getTime()) / 1000;
						sTempoMinutosAtd = getTimeSpent(tempoAtd);								
					} else if (tsAuxEvtDes != null) {
						sTempoMinutosLoc = "QTA";
						sTempoMinutosAtd = "QTA";
					} 
					%>
					<td align='center' class='evento_atendido'><%= auxTimeRec %></td>
					<td align='center' class='evento_atendido'><%= sTempoMinutosDes %></td>
					<td align='center' class='evento_atendido'><%= auxTimeDes %></td>
					<td align='center' class='evento_atendido'><%= sTempoMinutosLoc %></td>
					<td align='center' class='evento_atendido'><%= auxTimeLoc %></td>
					<td align='center' class='evento_atendido'><%= sTempoMinutosAtd %></td>
					<td align='center' class='evento_atendido'><%= sTempoMinutosRec %></td>
					<td class='"+ class_cor_evento + "'><%= rsEventosFechados.getString("ENDERECO") %>, <%= rsEventosFechados.getString("NOMEBAIRRO") %>, <%= rsEventosFechados.getString("NOMECIDADE") %></td>
					</tr>
					<%			
				}
				rsEventosFechados.close();
				stEventosFechados.close();	
			}
			
			
			
			///////// EVENTOS ABERTOS /////////////
			StringBuffer sqlEventosAbertos = new StringBuffer();
			sqlEventosAbertos.append(" SELECT  h.CD_EVENTO, h.NU_PRIORIDADE, c.CD_CLIENTE, v.NM_VIATURA, h.FG_STATUS, fe.NM_FRASE_EVENTO, v.CD_VIATURA,		   ");		
			sqlEventosAbertos.append(" h.DT_RECEBIDO, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, c.ENDERECO,   ");			
			sqlEventosAbertos.append(" cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, TX_OBSERVACAO_FECHAMENTO, TX_OBSERVACAO_GERENTE, r.TELEFONE, v.NM_PLACA, h.DT_ESPERA_DESLOCAMENTO			  	   "); 
			sqlEventosAbertos.append(" FROM HISTORICO h WITH (NOLOCK)														   ");
			sqlEventosAbertos.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE 									   ");
			sqlEventosAbertos.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON  v.CD_VIATURA = h.CD_VIATURA        								   ");
			sqlEventosAbertos.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK) ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  						   ");
			sqlEventosAbertos.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE 					   ");
			sqlEventosAbertos.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE 									   ");
			sqlEventosAbertos.append(" INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA 										   ");
			sqlEventosAbertos.append(" WHERE h.FG_STATUS IN (2, 3, 9, 10) 								   ");
			sqlEventosAbertos.append(" AND h.CD_VIATURA = " + cd_viatura +"                                                            	                          ");
			sqlEventosAbertos.append(" AND v.NM_VIATURA LIKE '"+ regional +"%'                                                             	                          ");
			PreparedStatement stEventosAbertos = conn.prepareStatement(sqlEventosAbertos.toString());
			ResultSet rsEventosAbertos = stEventosAbertos.executeQuery();
			
			while(rsEventosAbertos.next()) {
			
				class_cor_evento = "evento_prioridade" + rsEventosAbertos.getString("FG_STATUS");			
				qtdAtendimentos++;
				nm_status = "";
				if (rsEventosAbertos.getInt("FG_STATUS") == 2) {
					nm_status = "<img src='imagens/icones_final/arrow_pag_blue_next_16x16-trans.png' title='Em Deslocamento'/>";
				} else if (rsEventosAbertos.getInt("FG_STATUS") == 9) {
					nm_status = "<img src='imagens/custom/car-red-16x16.png' title='Deslocamento iniciado'/>";
				} else if (rsEventosAbertos.getInt("FG_STATUS") == 3) {
					nm_status = "<img src='imagens/icones_final/user_green_16x16-trans.png' title='No Local'/>";
				} else if (rsEventosAbertos.getInt("FG_STATUS") == 10) {
					nm_status = "<img src='imagens/icones_final/sphere_blue_ok_16x16-trans.png' title='No Local - Sem Altera��o'/>";
				}
				%>
				 <tr>
					<td class='<%= class_cor_evento %>'><%= rsEventosAbertos.getString("CD_EVENTO") %><a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= rsEventosAbertos.getString("CD_EVENTO") %> - <%= rsEventosAbertos.getString("NM_FRASE_EVENTO") %>', CAPTION, 'Evento', TEXTSIZE, '10px', BORDER, 10);" onmouseout="return nd();"><img src="imagens/icones_final/information_16x16-trans.png"/></a>
					<td class='<%= class_cor_evento %>'><%= rsEventosAbertos.getString("ID_CENTRAL") %>[<%= rsEventosAbertos.getString("PARTICAO") %>]<%= rsEventosAbertos.getString("FANTASIA") %>&nbsp;(&nbsp;<%= rsEventosAbertos.getString("RAZAO") %>&nbsp;)</td>
				<%

				String logTexto = rsEventosAbertos.getString("TX_OBSERVACAO_FECHAMENTO");
				if (logTexto == null) {
					logTexto = "Vazio";
				} else {
					logTexto = "<li>" + logTexto;
					logTexto =  encodeHTML(logTexto.replace("\n", "<br><li>")); 
				}
				%>
				   <td class='<%= class_cor_evento %>'>
				   <a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= logTexto %>', CAPTION, 'Log', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);" onmouseout="return nd();">&nbsp;<img src="imagens/icones_final/document_v_16x16-trans.png"/></a>
				<%
				logTexto = rsEventosAbertos.getString("TX_OBSERVACAO_GERENTE");
				if (logTexto == null) {
					logTexto = "Vazio";
				} else {
					logTexto = "<li>" + logTexto;
					logTexto =  encodeHTML(logTexto.replace("\n", "<br><li>")); 
				}
				logTexto =  encodeHTML(logTexto.replace("\n", "<br>")); 
				%>
				  <a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= logTexto %>', CAPTION, 'Log Ger�ncia', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);" onmouseout="return nd();">&nbsp;<img src="imagens/icones_final/doc_link.png"/></a>
				 </td>
				 <td align='center' class='<%= class_cor_evento %>'><%= nm_status %></td>
				<%
				tsAuxEvtRec = rsEventosAbertos.getTimestamp("DT_RECEBIDO");
				tsAuxEvtDes = rsEventosAbertos.getTimestamp("DT_ESPERA_DESLOCAMENTO");
				tsAuxEvtIni = rsEventosAbertos.getTimestamp("DT_VIATURA_DESLOCAMENTO");
				tsAuxEvtLoc = rsEventosAbertos.getTimestamp("DT_VIATURA_NO_LOCAL");
				dateAuxAgora = new Date();
				sTempoMinutosDes = none;
				sTempoMinutosIni = none;
				sTempoMinutosRec = none;
				sTempoMinutosLoc = none;
				sTempoMinutosAtd = none;
				auxTimeDes = none;
				auxTimeIni = none;
				auxTimeRec = none;
				auxTimeLoc = none;
				tempoAtd = 0;

				if (tsAuxEvtRec != null) {
					auxTimeRec = formatter.format(tsAuxEvtRec);
				}
				if (tsAuxEvtRec != null) {
					sTempoMinutosRec = getTimeSpent(Math.round((dateAuxAgora.getTime() - tsAuxEvtRec.getTime()) / 1000));
					sTempoMinutosRec = sTempoMinutosRec + " <img src='imagens/icones_final/arrow_mini_updown_7x7-trans copy.png' title='Tempo atual'/>";
				}
				if (tsAuxEvtDes != null) {
					auxTimeDes = formatter.format(tsAuxEvtDes);
				}			
				if ((tsAuxEvtDes != null) && (tsAuxEvtRec != null)) {
					sTempoMinutosDes = getTimeSpent(Math.round((tsAuxEvtDes.getTime() - tsAuxEvtRec.getTime()) / 1000));
					tempoAtd += Math.round((tsAuxEvtDes.getTime() - tsAuxEvtRec.getTime()) / 1000 );
				}			
				if (tsAuxEvtIni != null) {
					auxTimeIni = formatter.format(tsAuxEvtIni);
				}			
				if ((tsAuxEvtDes != null) && (tsAuxEvtIni != null)) {
					sTempoMinutosIni = getTimeSpent(Math.round((tsAuxEvtDes.getTime() - tsAuxEvtIni.getTime()) / 1000));
				}			
				if (tsAuxEvtLoc != null) {
					auxTimeLoc = formatter.format(tsAuxEvtLoc);
				}
				if ((tsAuxEvtLoc != null) && (tsAuxEvtDes != null)) {
					sTempoMinutosLoc = getTimeSpent(Math.round(tsAuxEvtLoc.getTime() - tsAuxEvtDes.getTime()) / 1000);
					tempoAtd += Math.round(tsAuxEvtLoc.getTime() - tsAuxEvtDes.getTime()) / 1000;
					sTempoMinutosAtd = getTimeSpent(tempoAtd);								
					
				} else if (tsAuxEvtDes != null) {
					sTempoMinutosLoc = getTimeSpent(Math.round(dateAuxAgora.getTime() - tsAuxEvtDes.getTime()) / 1000);
					sTempoMinutosLoc = sTempoMinutosLoc + " <img src='imagens/icones_final/arrow_mini_updown_7x7-trans copy.png' title='Tempo atual'/>";
					tempoAtd += Math.round(dateAuxAgora.getTime() - tsAuxEvtDes.getTime()) / 1000;
					sTempoMinutosAtd = getTimeSpent(Math.round((dateAuxAgora.getTime() - tsAuxEvtRec.getTime()) / 1000)) + " <img src='imagens/icones_final/arrow_mini_updown_7x7-trans copy.png' title='Tempo atual'/>";								
				}
				%>
				<td align='center' class='<%= class_cor_evento %>'><%= auxTimeRec %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosDes  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= auxTimeDes  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosIni  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= auxTimeIni  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosLoc  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= auxTimeLoc  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosAtd  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosRec  %></td>
				<td class='<%= class_cor_evento %>'><%= rsEventosAbertos.getString("ENDERECO") %>, <%= rsEventosAbertos.getString("NOMEBAIRRO") %>, <%= rsEventosAbertos.getString("NOMECIDADE") %></td>
				</tr>
				<%
			
			}
			rsEventosAbertos.close();
			stEventosAbertos.close();	
			%>
		
			<tr class='linha_viatura'>
				<td style='font-weight: normal' colspan='2'>&nbsp;</td>
				<td style='font-weight: bold' colspan='3'> Em atendimento: <%= qtdAtendimentos %></td>
				<td style='font-weight: bold' colspan='3'> Atendidos desde <%= horaLoginAA %>: <%= qtdAtendidos %></td>
				<td style='font-weight: normal' colspan='6'>&nbsp;</td>
			</tr>
			<%		
		}	
		rsLogados.close();
		stLogados.close();	


		///////////////// NAO LOGADOS ////////////////////
		StringBuffer sqlNaoLogados = new StringBuffer();
		sqlNaoLogados.append("SELECT  h.CD_EVENTO, h.NU_PRIORIDADE, c.CD_CLIENTE, v.NM_VIATURA, h.FG_STATUS, fe.NM_FRASE_EVENTO, v.CD_VIATURA,		   ");		
		sqlNaoLogados.append("h.DT_RECEBIDO, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, c.ENDERECO,   ");			
		sqlNaoLogados.append("cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, TX_OBSERVACAO_FECHAMENTO, TX_OBSERVACAO_GERENTE, r.TELEFONE, v.NM_PLACA, h.DT_ESPERA_DESLOCAMENTO			  	   "); 
		sqlNaoLogados.append("FROM HISTORICO h WITH (NOLOCK)														   ");
		sqlNaoLogados.append("INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE 									   ");
		sqlNaoLogados.append("INNER JOIN VIATURA v WITH (NOLOCK) ON  v.CD_VIATURA = h.CD_VIATURA        								   ");
		sqlNaoLogados.append("INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK) ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  						   ");
		sqlNaoLogados.append("LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE 					   ");
		sqlNaoLogados.append("LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE 									   ");
		sqlNaoLogados.append("INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA 										   ");
		sqlNaoLogados.append("WHERE h.FG_STATUS IN (2, 3, 9, 10) 												   ");
		sqlNaoLogados.append("AND h.CD_VIATURA NOT IN (SELECT CD_VIATURA FROM LIVRO_ATA_FROTA WHERE CD_LIVRO_ATA = " + cd_livro_ata + " AND DT_FIM_USO_VIATURA IS NULL) ");
		sqlNaoLogados.append("AND v.NM_VIATURA LIKE '"+ regional +"%'                                                             	                          ");		
		if (asseio.equals("only")) {
			sqlNaoLogados.append("AND ( v.NM_VIATURA LIKE '%-AD-%' OR v.NM_VIATURA LIKE '%-AN-%' )                                                                              		        ");
		}
		if (asseio.equals("no")) {
			sqlNaoLogados.append("AND v.NM_VIATURA NOT LIKE '%-AD-%' AND  v.NM_VIATURA NOT LIKE '%-AN-%'                                                                              		        ");
		}
		if (humana.equals("only")) {
			sqlNaoLogados.append("AND (v.NM_VIATURA LIKE '%-CFD-%' OR v.NM_VIATURA LIKE '%-CFN-%' OR v.NM_VIATURA LIKE '%-HD-%' OR v.NM_VIATURA LIKE '%-HN-%' )                                                                              		        ");
		}
		if (humana.equals("no")) {
			sqlNaoLogados.append("AND v.NM_VIATURA NOT LIKE '%-CFD-%' AND v.NM_VIATURA NOT LIKE '%-CFN-%' AND v.NM_VIATURA NOT LIKE '%-HD-%' AND  v.NM_VIATURA NOT LIKE '%-HN-%'                                                                              		        ");
		}
		sqlNaoLogados.append("ORDER BY v.NM_VIATURA, DT_RECEBIDO ");

		PreparedStatement stNaoLogados = conn.prepareStatement(sqlNaoLogados.toString());
		ResultSet rsNaoLogados = stNaoLogados.executeQuery();
		cd_viatura = 0L;
		Long cd_viatura_ant = -1L;
		qtdAtendimentos = 0;
		while(rsNaoLogados.next()) {
			cd_viatura = rsNaoLogados.getLong("CD_VIATURA");
			placa = rsNaoLogados.getString("NM_PLACA");
			nm_viatura_display = rsNaoLogados.getString("NM_VIATURA");
			class_cor_evento = "evento_prioridade" + rsNaoLogados.getString("FG_STATUS");			
			nm_status = "";
			
			if (!cd_viatura.equals(cd_viatura_ant)) {
				cd_viatura_ant = cd_viatura;

				/////// LOG VIATURA ////////
				String logViatura = getLogViatura(placa);
				if (logViatura == null) {
					logViatura = "Vazio";
				} else {
					logViatura = "<li>" + logViatura;
					logViatura =  encodeHTML(logViatura.replace("\n", "<br><li>")); 
				}
				logViatura =  encodeHTML(logViatura.replace("\n", "<br>")); 				
				%>
				<tr>
				  <td style='font-weight: bold' colspan='12'>
					  <img src='imagens/custom/police.png' title='Viatura'  align="absMiddle"/>&nbsp;<%= SigmaUtils.getLogViaturaLink(cd_viatura) %>&nbsp;<%=nm_viatura_display %> 
					  <a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%=logViatura%> ', STICKY, CLOSETEXT, 'Fechar', CAPTION, 'Log da Viatura', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);" onmouseout="return nd();">&nbsp;<img src="imagens/icones_final/document_v_16x16-trans.png" align="absMiddle"/></a>
					  &nbsp;<img src='imagens/icones_final/sphere_yellow_att_16x16-trans.png' title='Entrada n�o registrada'/> Entrada n�o registrada
				  </td>
				</tr>
				<%
			}

			if (rsNaoLogados.getInt("FG_STATUS") == 2) {
				nm_status = "<img src='imagens/icones_final/arrow_pag_blue_next_16x16-trans.png' title='Em Deslocamento'/>";
			} else if (rsNaoLogados.getInt("FG_STATUS") == 3) {
				nm_status = "<img src='imagens/icones_final/user_green_16x16-trans.png' title='No Local'/>";
			} else if (rsNaoLogados.getInt("FG_STATUS") == 10) {
				nm_status = "<img src='imagens/icones_final/sphere_blue_ok_16x16-trans.png' title='No Local - Sem Altera��o'/>";
			}
			%>
			 <tr>
				<td class='<%= class_cor_evento %>'><%= rsNaoLogados.getString("CD_EVENTO") %><a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= rsNaoLogados.getString("CD_EVENTO") %> - <%= rsNaoLogados.getString("NM_FRASE_EVENTO") %>', CAPTION, 'Evento', TEXTSIZE, '10px', BORDER, 10);" onmouseout="return nd();"><img src="imagens/icones_final/information_16x16-trans.png"/></a>
				<td class='<%= class_cor_evento %>'><%= rsNaoLogados.getString("ID_CENTRAL") %>[<%= rsNaoLogados.getString("PARTICAO") %>]<%= rsNaoLogados.getString("FANTASIA") %>&nbsp;(&nbsp;<%= rsNaoLogados.getString("RAZAO") %>&nbsp;)</td>
			<%

			String logTexto = rsNaoLogados.getString("TX_OBSERVACAO_FECHAMENTO");
			if (logTexto == null) {
				logTexto = "Vazio";
			} else {
				logTexto = "<li>" + logTexto;
				logTexto =  encodeHTML(logTexto.replace("\n", "<br><li>")); 
			}
			%>
			   <td class='<%= class_cor_evento %>'>
			   <a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= logTexto %>', CAPTION, 'Log', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);" onmouseout="return nd();">&nbsp;<img src="imagens/icones_final/document_v_16x16-trans.png"/></a>
			<%
			logTexto = rsNaoLogados.getString("TX_OBSERVACAO_GERENTE");
			if (logTexto == null) {
				logTexto = "Vazio";
			} else {
				logTexto = "<li>" + logTexto;
				logTexto =  encodeHTML(logTexto.replace("\n", "<br><li>")); 
			}
			logTexto =  encodeHTML(logTexto.replace("\n", "<br>")); 
			%>
			  <a tabIndex="-1" href="javascript:void(0);" onmouseover="return overlib('<%= logTexto %>', CAPTION, 'Log Ger�ncia', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);" onmouseout="return nd();">&nbsp;<img src="imagens/icones_final/doc_link.png"/></a>
			 </td>
			 <td align='center' class='<%= class_cor_evento %>'><%= nm_status %></td>
			<%
			tsAuxEvtRec = rsNaoLogados.getTimestamp("DT_RECEBIDO");
			tsAuxEvtDes = rsNaoLogados.getTimestamp("DT_ESPERA_DESLOCAMENTO");
			tsAuxEvtLoc = rsNaoLogados.getTimestamp("DT_VIATURA_NO_LOCAL");
			dateAuxAgora = new Date();
			sTempoMinutosDes = none;
			sTempoMinutosRec = none;
			sTempoMinutosLoc = none;
			sTempoMinutosAtd = none;
			auxTimeDes = none;
			auxTimeRec = none;
			auxTimeLoc = none;
			tempoAtd = 0;

			if (tsAuxEvtRec != null) {
				auxTimeRec = formatter.format(tsAuxEvtRec);
			}
			if (tsAuxEvtRec != null) {
				sTempoMinutosRec = getTimeSpent(Math.round((dateAuxAgora.getTime() - tsAuxEvtRec.getTime()) / 1000));
				sTempoMinutosRec = sTempoMinutosRec + " <img src='imagens/icones_final/arrow_mini_updown_7x7-trans copy.png' title='Tempo atual'/>";
			}
			if (tsAuxEvtDes != null) {
				auxTimeDes = formatter.format(tsAuxEvtDes);
			}			
			if ((tsAuxEvtDes != null) && (tsAuxEvtRec != null)) {
				sTempoMinutosDes = getTimeSpent(Math.round((tsAuxEvtDes.getTime() - tsAuxEvtRec.getTime()) / 1000));
				tempoAtd += Math.round((tsAuxEvtDes.getTime() - tsAuxEvtRec.getTime()) / 1000 );
			}			
			if (tsAuxEvtLoc != null) {
				auxTimeLoc = formatter.format(tsAuxEvtLoc);
			}
			if ((tsAuxEvtLoc != null) && (tsAuxEvtDes != null)) {
				sTempoMinutosLoc = getTimeSpent(Math.round(tsAuxEvtLoc.getTime() - tsAuxEvtDes.getTime()) / 1000);
				tempoAtd += Math.round(tsAuxEvtLoc.getTime() - tsAuxEvtDes.getTime()) / 1000;
				sTempoMinutosAtd = getTimeSpent(tempoAtd);								
				
			} else if (tsAuxEvtDes != null) {
				sTempoMinutosLoc = getTimeSpent(Math.round(dateAuxAgora.getTime() - tsAuxEvtDes.getTime()) / 1000);
				sTempoMinutosLoc = sTempoMinutosLoc + " <img src='imagens/icones_final/arrow_mini_updown_7x7-trans copy.png' title='Tempo atual'/>";
				tempoAtd += Math.round(dateAuxAgora.getTime() - tsAuxEvtDes.getTime()) / 1000;
				sTempoMinutosAtd = getTimeSpent(Math.round((dateAuxAgora.getTime() - tsAuxEvtRec.getTime()) / 1000)) + " <img src='imagens/icones_final/arrow_mini_updown_7x7-trans copy.png' title='Tempo atual'/>";								
			}
			%>
				<td align='center' class='<%= class_cor_evento %>'><%= auxTimeRec %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosDes  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= auxTimeDes  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosLoc  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= auxTimeLoc  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosAtd  %></td>
				<td align='center' class='<%= class_cor_evento %>'><%= sTempoMinutosRec  %></td>
				<td class='<%= class_cor_evento %>'><%= rsNaoLogados.getString("ENDERECO") %>, <%= rsNaoLogados.getString("NOMEBAIRRO") %>, <%= rsNaoLogados.getString("NOMECIDADE") %></td>
			</tr>
			<%				
		}	
		rsNaoLogados.close();
		stNaoLogados.close();			
		
		
		
	} catch (Exception e) {
		e.printStackTrace();
%>
				<script type="text/javascript" >
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
			<%			
	}
%>
	</tbody>
	</table>
	<input onClick="javascript:document.location.reload();" type="button" value="Atualizar"/></a>
	
	<br>
	
	</cw:body>

</cw:main>
</portal:head>



<script language="JavaScript">

function unavaliable() {
	alert('Discagem autom�tica dispon�vel apenas para a Central de Monitoramento.');
}

function dial(src, dst) {
	var result = callSync('<%= PortalUtil.getBaseURL() %>servlet/com.neomind.fusion.custom.orsegups.servlets.RamalServlet?ramaOrigem='+src+'&ramalDestino='+dst);

	if (result.indexOf("Chamada efetuada") == -1) {
		alert("N�o foi poss�vel discar: " + response);
	}
}

function saveInputLog(idEntidade, tipo)
{
	var label = "Informar Log do AA/FIS/SUP:";
	
	texto = prompt(label,"");
	
	if(texto != null && texto != "")
	{
		var	result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.sigma.servlet.SigmaServlet?action=saveLog&tipo="+tipo+"&idEntidade="+idEntidade+"&texto="+JSON.stringify(texto));
		
		alert(result);
	}
}



</script>