<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>



<portal:head title="Colaboradores SIGMA">
<cw:main>
	<cw:header title="Colaboradores / AAs SIGMA" />
	<cw:body id="area_scroll">
	
	<FIELDSET class="fieldGroup">
	<LEGEND class="legend">&nbsp;Colaboradores / AAs SIGMA&nbsp;</LEGEND>
		
<%
	final String none = "&nbsp;";
	
	QLEqualsFilter filterAtivo = new QLEqualsFilter("fg_ativo_colaborador", true);
	
	ExternalEntityInfo infoCol = (ExternalEntityInfo)EntityRegister.getInstance().getCache().getByString("SIGMACOLABORADOR");
	List<NeoObject> listaCol = (List<NeoObject>)PersistEngine.getObjects(infoCol.getEntityClass(), filterAtivo, -1, -1, " nm_colaborador ASC");	

	if(listaCol.isEmpty()) {
		out.print("&nbsp;&nbsp;&nbsp;&nbsp;<i>Nenhum Colaborador encontrado!</i><br/><br/>");
	} else {
%>
	<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
	<thead style="cursor: auto">
		<tr style="cursor: auto">
			<th style="cursor: auto">C�digo Colaborador SIGMA</th>
			<th style="cursor: auto">Nome</th>
			<th style="cursor: auto">RG</th>
		</tr>
	</thead>
	<tbody>	

<%
	for (NeoObject col : listaCol)
	{
		try {
			EntityWrapper wrpCol = new EntityWrapper((NeoObject)col); 
			String codigoCol = ((Long)(wrpCol.getValue("cd_colaborador"))).toString();
			String nomeCol = (String)wrpCol.getValue("nm_colaborador");
			String rgCol = (String)wrpCol.getValue("nu_registro_geral");
			if ( (rgCol == null) || (rgCol.equals(""))) {
				rgCol = "&nbsp;";
			}
			
			out.print("<tr>");
			out.print("<td> " +  codigoCol + "</td>");
			out.print("<td> " +  nomeCol + "</td>");
			out.print("<td> " +  rgCol + "</td>");
			out.print("</tr>");

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
%>
	</tbody>
	</table>
<%
	out.print("[" + listaCol.size() + "]");
%>
	<br>
	</fieldset>
	
	</cw:body>
</cw:main>
</portal:head>
