<%@page import="java.util.TreeMap"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.util.SortedMap"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.TecnicoConta"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>


<%
	
	
	final NeoUser currentUser = PortalUtil.getCurrentUser();
	final boolean isAdm = (currentUser!=null&&currentUser.isAdm())?true:false;
	final String none = "&nbsp;";

	DecimalFormat f = new DecimalFormat("000000000");

	DecimalFormat formatoReal = new DecimalFormat("#,##0.00");
	List<String> listReg = new ArrayList<String>();
	
	listReg.add("BNU");
	listReg.add("BQE");
	listReg.add("CCO");
	listReg.add("CSC");
	listReg.add("CTA");
	listReg.add("CUA");
	listReg.add("IAI");
	listReg.add("JLE");
	listReg.add("JGS");
	listReg.add("LGS");
	listReg.add("RSL");
	listReg.add("SOO");
	listReg.add("TRO");
	listReg.add("GNA");
	listReg.add("CAS");
	listReg.add("PMJ");
	listReg.add("NHO");
	listReg.add("SRR");
	listReg.add("TRI");
	listReg.add("XLN");
	listReg.add("REGIONAL INDEFINIDA");
	
%>

<portal:head title="Contas x T�cnico">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css">
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/overlib/overlib.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<cw:main>
		<cw:header title="Contas x T�cnico" />
		<cw:body id="area_scroll">
		
			<%
				Connection conn = null;
				PreparedStatement pstm = null;
				ResultSet rs = null;
				String extraStyle = "style='background-color: #FFEEDD;'";
				String returnFromAccess = "";
				String iconeRep = "<img src='imagens/custom/user-worker-boss.png' alt='Colaborador' align='absMiddle'/>";
				try
				{

				conn = PersistEngine.getConnection("SIGMA90");

						StringBuilder sql = new StringBuilder();
						sql.append(" SELECT CD_COLABORADOR, NM_COLABORADOR, Tipo, COUNT(*) AS TOTAL ");
						sql.append(" FROM ( ");
						sql.append(" SELECT DISTINCT t.CD_COLABORADOR, t.NM_COLABORADOR, c.ID_EMPRESA, c.ID_CENTRAL, CASE WHEN PARTICAO LIKE '098' OR PARTICAO LIKE '099' THEN 'CFTV' ELSE 'ALARME' END Tipo ");
						sql.append(" FROM dbCENTRAL c WITH (NOLOCK)  ");
						sql.append(" INNER JOIN COLABORADOR t WITH (NOLOCK) ON t.CD_COLABORADOR = c.CD_TECNICO_RESPONSAVEL ");
						sql.append(" WHERE c.CTRL_CENTRAL = 1 AND t.FG_ATIVO_COLABORADOR = 1  ");
						sql.append(" AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' 	AND c.ID_CENTRAL NOT LIKE 'FFFF' AND ID_CENTRAL NOT LIKE 'R%'");
						sql.append(" ) AS base GROUP BY CD_COLABORADOR, NM_COLABORADOR, Tipo ORDER BY  1, 2 ");
						
						pstm = conn.prepareStatement(sql.toString());
						rs = pstm.executeQuery();
						TecnicoConta tecnicoConta = null;
						List<TecnicoConta> tecnicoContas = new ArrayList<TecnicoConta>();
						int flag = 0;
						String colaborador = "";
						String totalAlr = "";
						String totalCFTV = "";
						String colaboradorAnt = "Org";
						String tipo = "";
						tecnicoConta = new TecnicoConta();
						while (rs.next())
						{
						
						colaborador = rs.getString("CD_COLABORADOR") + " - " + rs.getString("NM_COLABORADOR");
						if(flag == 0)
						{
							colaboradorAnt = colaborador;		
						}
						tipo = rs.getString("Tipo");
									 
						if(tipo.equals("ALARME"))
							totalAlr = rs.getString("TOTAL");
						if(tipo.equals("CFTV"))
							totalCFTV = rs.getString("TOTAL");	
							
						if(!colaboradorAnt.equals(colaborador)){
							
							tecnicoContas.add(tecnicoConta);
							
							colaboradorAnt = colaborador;	
							tecnicoConta = new TecnicoConta();
						}
						if(colaboradorAnt.equals(colaborador)){
							tecnicoConta.setNomeTecnico(colaboradorAnt);
							if(totalAlr == null || totalAlr.isEmpty())
								totalAlr = "0";
							if(tecnicoConta.getContaAlarme() == null || tipo.equals("ALARME"))
								tecnicoConta.setContaAlarme(totalAlr);
							if(colaboradorAnt.contains("DANIEL CLAUDEMIR RASPE"))
								System.out.println("AAAA");
							if(totalCFTV == null || totalCFTV.isEmpty())
								totalCFTV = "0";
							if(tecnicoConta.getContaCFTV() == null || tipo.equals("CFTV"))
								tecnicoConta.setContaCFTV(totalCFTV);
							int i = 0;
							for(String reg: listReg){
								if(colaborador.contains(reg+" - ")){
									tecnicoConta.setRegional(reg);
									i++;
									break;
								}
								
							}
							if(i == 0){
								tecnicoConta.setRegional("REGIONAL INDEFINIDA");
							}
							
							totalAlr = "";	
							totalCFTV = "";
												
						 }
						flag++;
						
						}
						
						tecnicoContas.add(tecnicoConta);
						
						for (String objReg : listReg) 
						{
									
						  	String nomeReg = (String) objReg;
							if(!nomeReg.contains("GPR")){
							Double vlrAlarme = 0.0;
							Double vlrCFTV = 0.0;
							Double subTotalAlarme = 0.0;
							Double subTotalCFTV = 0.0;
							Double totalAl= 0.0;
							Double totalCF = 0.0;
							int quantAl = 0;
							int quantCF = 0;
							%>
									
							<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
							<tr style="cursor: auto">
							<th style="cursor: auto; white-space: normal" width="10%">T�cnico</th>
							<th style="cursor: auto; white-space: normal" width="5%">Contas Alarme</th>
							<th style="cursor: auto; white-space: normal" width="5%">Valor Alarme</th>
							<th style="cursor: auto; white-space: normal" width="5%">Sub-total Alarme</th>
							<th style="cursor: auto; white-space: normal" width="5%">Contas CFTV</th>
							<th style="cursor: auto; white-space: normal" width="5%">Valor CFTV</th>
							<th style="cursor: auto; white-space: normal" width="5%">Sub-total CFTV</th>
							<th style="cursor: auto; white-space: normal" width="5%">Total Individual</th>
							</tr>
							<fieldset class="fieldGroup" >
							<legend class="legend" style="font-size: 1.2em">&nbsp;<%= nomeReg %>&nbsp;
							</legend>
							<tbody>
							<%
							List<TecnicoConta> tecnicoContasList = new ArrayList<TecnicoConta>();
							for(TecnicoConta tecnicoContaObj: tecnicoContas)
							{
								TecnicoConta conta = (TecnicoConta)tecnicoContaObj;			
							if( tecnicoContaObj.getRegional().contains(nomeReg))
							{
											
							if(!tecnicoContaObj.getNomeTecnico().contains("CFTV"))
							{
							vlrAlarme = 5.50;
							vlrCFTV = 11.00;
							}else if(tecnicoContaObj.getNomeTecnico().contains("CFTV")){
								vlrAlarme = 0.0;
								if(tecnicoContaObj.getNomeTecnico().indexOf("CTA") == 0)
									vlrAlarme = 5.50;
								vlrCFTV = 16.50;
							}
							if(!tecnicoContaObj.getNomeTecnico().contains("- TEC -") ){
							vlrAlarme = 0.0;
							vlrCFTV = 0.0;	
							}
							if(NeoUtils.safeIsNotNull(tecnicoContaObj.getContaAlarme()))
							subTotalAlarme = vlrAlarme * Double.parseDouble(tecnicoContaObj.getContaAlarme());
							if(NeoUtils.safeIsNotNull(tecnicoContaObj.getContaCFTV()))
							subTotalCFTV = vlrCFTV * Double.parseDouble(tecnicoContaObj.getContaCFTV());
											
							quantAl += Integer.parseInt(tecnicoContaObj.getContaAlarme());
							quantCF += Integer.parseInt(tecnicoContaObj.getContaCFTV());
							totalAl += subTotalAlarme;
						    totalCF += subTotalCFTV;
						    
						    conta.setValorAlarme(vlrAlarme);
						    conta.setValorCFTV(vlrCFTV);
						    conta.setSubTotalAlarme(subTotalAlarme);
						    conta.setSubTotalCFTV(subTotalCFTV);
						   
						    tecnicoContasList.add(conta);
							}
							}
							Collections.sort(tecnicoContasList);
							for(TecnicoConta tecConObj : tecnicoContasList){
							%>

						<tr>
							<td style="white-space: normal"><%=tecConObj.getNomeTecnico()%></td>
							<td style="white-space: normal; text-align: right"><%=tecConObj.getContaAlarme()%></td>
							<td style="white-space: normal; text-align: right"><%=NeoUtils.safeFormat(formatoReal, tecConObj.getValorAlarme())%></td>
							<td style="white-space: normal; text-align: right"><%=NeoUtils.safeFormat(formatoReal, tecConObj.getSubTotalAlarme())%></td>
							<td style="white-space: normal; text-align: right"><%=tecConObj.getContaCFTV()%></td>
							<td style="white-space: normal; text-align: right"><%=NeoUtils.safeFormat(formatoReal, tecConObj.getValorCFTV())%></td>
							<td style="white-space: normal; text-align: right"><%=NeoUtils.safeFormat(formatoReal, tecConObj.getSubTotalCFTV())%></td>
							<td style="white-space: normal; text-align: right"><%=NeoUtils.safeFormat(formatoReal, tecConObj.getSubTotalAlarme() +  tecConObj.getSubTotalCFTV())%></td>

						</tr>
						
						<%
							}
							%>
							<tr>
								<td style="white-space: normal" colspan="1">&nbsp;</td>
								<td style="white-space: normal; text-align: right;font-weight: bold;background-color: #FFEEDD;" colspan="1">Qtde alarme: <%=quantAl%> </td>
								<td style="white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;" colspan="2">Total alarme: <%=NeoUtils.safeFormat(formatoReal, totalAl)%> </td>
								<td style="white-space: normal; text-align: right;font-weight: bold;background-color: #FFEEDD;" colspan="1">Qtde CFTV: <%=quantCF%> </td>
								<td style="white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;" colspan="2">Total CFTV: <%=NeoUtils.safeFormat(formatoReal, totalCF)%></td>
								<td style="white-space: normal; text-align: right;font-weight: bold; background-color: #FFFFB9;" colspan="2">Total geral: <%=NeoUtils.safeFormat(formatoReal, totalAl + totalCF)%></td>
							</tr>
							<%
							}
							
							}
					
						%>
						
					</tbody>
				</table>
			</fieldset>	
			<br />
		
				<%
					} catch (Exception e) {
						e.printStackTrace();
				%>
				<script type="text/javascript">
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
				<%
					}finally {
						try {
										
						OrsegupsUtils.closeConnection(conn, pstm, rs);
						} catch (Exception e) {
						e.printStackTrace();
				%>
				<script type="text/javascript">
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>", 'error');
				</script>
				<%
					}
					}
				%>
			

		</cw:body>
	</cw:main>
</portal:head>