<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.fusion.custom.orsegups.maps.engine.MapsRESTEngine"%>
<%@page import="com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>

<%!

public static double distanceInMeters(double lat1, double lon1, double lat2, double lon2) {
	double theta = lon1 - lon2;
	double dist = Math.sin(lat1 * Math.PI / 180.0) * Math.sin(lat2 * Math.PI / 180.0) + Math.cos(lat1 * Math.PI / 180.0) * Math.cos(lat2 * Math.PI / 180.0) * Math.cos(theta * Math.PI / 180.0);
	dist = Math.acos(dist);
	dist = dist * 180.0 / Math.PI;
	dist = dist * 60 * 1.1515 * 1.609344 * 1000.0;
	return (dist);
}


public static void insertMove(NeoObject viatura, String codigoUsuario, String nomeUsuario, Long tipoMovimentacao) {
	InstantiableEntityInfo vtrInfo = AdapterUtils.getInstantiableEntityInfo("OTSMovimentacaoViatura");	
	NeoObject noTipoMovimentacao = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSTipoMovimentacaoViatura").getEntityClass(), new QLEqualsFilter("codigo", tipoMovimentacao));

	NeoObject objVtr = vtrInfo.createNewInstance();
	EntityWrapper wrpVtr = new EntityWrapper(objVtr);
	wrpVtr.findField("viatura").setValue(viatura);
	wrpVtr.findField("dataMovimento").setValue(new GregorianCalendar());
	wrpVtr.findField("codigoUsuario").setValue(codigoUsuario);
	wrpVtr.findField("nomeUsuario").setValue(nomeUsuario);
	wrpVtr.findField("tipoMovimento").setValue(noTipoMovimentacao);
	PersistEngine.persist(objVtr);

}

public static void verificaFilaEvento(String cdViatura)
{
	
	try
	{
		QLGroupFilter filterAnd = new QLGroupFilter("AND");
		filterAnd.addFilter(new QLEqualsFilter("cdViaturaSigma", cdViatura));
		List<NeoObject> eventosList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAnd);
		if(eventosList != null && !eventosList.isEmpty())
		{
			PersistEngine.removeObjects(eventosList);
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	

}
  			
 %>

<%
    final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.sigma.entradaAAviaSNEP");

	SIGMAUtilsEngine utils = new SIGMAUtilsEngine();
	MapsRESTEngine mapUtils = new MapsRESTEngine();

	String verboseError = request.getParameter("verboseError");
	boolean isVerboseError = false;
	if (verboseError != null && (verboseError.trim().equals("sim") || verboseError.trim().equals("1") || verboseError.trim().equals("yes")|| verboseError.trim().equals("on") )) {
		isVerboseError = true;
	}

	String username = request.getParameter("username");
	String testMode = request.getParameter("testMode");
	
	System.out.println("Query String "+request.getQueryString());
	
	String verificarSemAlteracao = request.getParameter("verificarSemAlteracao");
	
	boolean isVerificarSemAlteracao = false;
	
	if (verificarSemAlteracao != null){
		if (verificarSemAlteracao.equalsIgnoreCase("sim") || verificarSemAlteracao.equalsIgnoreCase("ok") || verificarSemAlteracao.equalsIgnoreCase("true") ){
			isVerificarSemAlteracao = true;
		}
	}
	String respostaEventoAgrupado = request.getParameter("respostas");
	
	
	boolean isTest = false;
	if (testMode != null && (testMode.trim().equals("sim") || testMode.trim().equals("1") || testMode.trim().equals("yes")|| testMode.trim().equals("on") )) {
		isTest = true;
	}
	String fone = request.getParameter("fone");
	String origem = request.getParameter("origem");
	
	if (fone == null) {
		out.print("Erro #14 - Fone n�o informado - necess�rio para obter o colaborador (VTR Mobile)");
		return;		
	} else if(origem != null && origem.trim().contains("fusion")) {
		origem = "FusionAA("+username+")";
	} else {
		origem = "SNEP-" + fone;
	}	
	//ERRO URA ENTRADA VIA SNEP
	//out.print(fone+"|"+origem);

	origem = URLEncoder.encode(origem,"ISO-8859-1");
	
	String cd_cliente = request.getParameter("codigo");
	if(cd_cliente == null || cd_cliente.trim().equals(""))	{
		//String tipoTituloAux = (String) processEntity.findValue("tipoTitulo");
		return;
	}
	String cd_placa = request.getParameter("cd_placa");
	
	String acao = request.getParameter("acao");
	
	System.out.print(acao);
	
	if(acao == null || (!acao.trim().equals("login") && !acao.trim().equals("logoff") && !acao.trim().equals("chgvtr") && !acao.trim().equals("local") && !acao.trim().equals("semalteracao")))	{
		out.print("Erro #2 - A��o inv�lida");
		return;
	} else {
		if (acao.equals("login") || acao.equals("chgvtr")) {		
			if ((cd_placa == null) || (cd_placa.length() != 4)) {
				out.print("Erro #3 - Placa n�o informada ou formato inv�lido (4 d�gitos)");
				return;
			}		
		}
	}
	boolean isDebug = true;
	
	String regional = "";
	String cd_central = "";
	String cd_empresa = "";
	String particao = "";
	String nm_evento = "";
	String cd_rota = "";
	Long cd_livro_ata = 0L;
	String cd_historico = "";
	String cd_viatura = "";
	String cd_colaborador = "";
	String cd_responsavel = "";
	String placa = "";
	String placa_atual = "";
	String sigla_rota = "";
	String fantasia = "";
	String aplicacao = "";
	String codigoTecnico = "";
	double margemDistancia = 200;  // em metros
	
	
	Connection conn = null;
	PreparedStatement stCentral = null;
	PreparedStatement stLogado = null;
	PreparedStatement stEvento = null;
	PreparedStatement stRota = null;
	PreparedStatement stLivroAta = null;
	PreparedStatement stColSigma = null;
	PreparedStatement stVtrSigma = null;
	PreparedStatement stUpdate = null;
	PreparedStatement stVtrAtual = null;
	PreparedStatement stUpdateFone = null;
	PreparedStatement stUpdateStatus = null;
	PreparedStatement stEventoAtual = null;
	
	PreparedStatement stEventoComFilhos = null;
	
	EntityWrapper ewViaturaAtual = null;
	EntityWrapper ewViaturaNova = null;
	
	/*
		Retornos poss�veis:
		OK - Sem erro
		1-C�digo inv�lido
		2-Usuario j� logado 
		2-Usuario n�o logado
		3-Rota j� ocupada
		4-Viatura inv�lida
		5-Viatura em uso
		6-Evento nao localizado
		7-Mais de um evento para o atendente
		8-Viatura com coordenadas inv�lidas
		10-AA fora da margem de seguran�a (dist�ncia)
		11-Livro ata nao aberto
		12-Colaborador invalido
		13-VTR Sigma invalida
		99-Erro desconhecido (Exception)
	*/
	try {
		String nomeFonteDados = "SIGMA90";
		conn = PersistEngine.getConnection(nomeFonteDados);
		
		StringBuffer sqlCentral = new StringBuffer();
		sqlCentral.append(" SELECT c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO, c.ID_ROTA, r.NM_ABREVIATURA, c.FANTASIA, c.CGCCPF, c.RESPONSAVEL, c.ID_RAMO, col.CD_COLABORADOR, SUBSTRING(r.NM_ROTA, 1, 3) AS REG  ");
		sqlCentral.append(" FROM dbCENTRAL c ");
		sqlCentral.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
		sqlCentral.append(" LEFT OUTER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = c.CD_TECNICO_RESPONSAVEL	");
		sqlCentral.append(" WHERE c.CD_CLIENTE = ? AND c.ID_RAMO IN (10058, 10059, 10060, 10061, 15021, 15145) AND c.CTRL_CENTRAL = 1 ");
		stCentral = conn.prepareStatement(sqlCentral.toString());
		stCentral.setString(1, cd_cliente);
		ResultSet rsCentral = stCentral.executeQuery();
		if (rsCentral.next()) {
			cd_central = rsCentral.getString("ID_CENTRAL");
			cd_empresa = rsCentral.getString("ID_EMPRESA");
			particao = rsCentral.getString("PARTICAO");
			cd_rota = rsCentral.getString("ID_ROTA");
			sigla_rota = rsCentral.getString("NM_ABREVIATURA");
			if (sigla_rota == null || (sigla_rota.trim().equals(""))) {
				sigla_rota = "---";	
			}			
			fantasia = rsCentral.getString("FANTASIA");
			cd_viatura = rsCentral.getString("CGCCPF");
			regional = rsCentral.getString("REG");
			aplicacao = rsCentral.getString("ID_RAMO");
			if (aplicacao == null || (aplicacao.trim().equals(""))) {
				aplicacao = "NDA";	
			} else if (aplicacao.equals("10058")) {
				aplicacao = "TAT";			
			} else if (aplicacao.equals("10059")) {
				aplicacao = "TSH";			
			} else if (aplicacao.equals("10060")) {
				aplicacao = "TSA";			
			} else if (aplicacao.equals("15021")) {
				aplicacao = "ADM";			
			} else if (aplicacao.equals("15145")) { // Placas
				aplicacao = "PLC";			
			} else if (aplicacao.equals("10061")) {
				aplicacao = "TEC";			
				codigoTecnico = rsCentral.getString("CD_COLABORADOR");
			}
			
		} else {
			// C�digo inv�lido
			if (isVerboseError) {
				out.print("1-C�digo inv�lido");
			} else {
				out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"1\"}");
			}
			if (isDebug) {
				log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> C�digo inv�lido");
			}
			return;
	
		}
		NeoObject aplicacaoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSAplicacaoViatura").getEntityClass(), new QLEqualsFilter("sigla", aplicacao));
		if(aplicacaoObj == null) {
			if (isDebug) {
				log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> N�o foi poss�vel definir Aplica��o da Viatura [" + aplicacao + "]");
			}
		}
		
		/*
		 *
		 * Recuperando Livro Ata Aberto
		 *
		 */	 
		StringBuffer sqlLivroAta = new StringBuffer();
		sqlLivroAta.append(" SELECT TOP 1 CD_LIVRO_ATA  "); 
		sqlLivroAta.append(" FROM LIVRO_ATA WITH (NOLOCK) ");
		sqlLivroAta.append(" WHERE FG_PLANTAO = 0  ");
		sqlLivroAta.append(" ORDER BY CD_LIVRO_ATA DESC ");
		stLivroAta = conn.prepareStatement(sqlLivroAta.toString());
		ResultSet rsLivroAta = stLivroAta.executeQuery();
		if (rsLivroAta.next()) {
			cd_livro_ata = rsLivroAta.getLong("CD_LIVRO_ATA");
		} else {
			// Livro Ata n�o encontrado
			if (isVerboseError) {
				out.print("11-Livro Ata nao encontrado");
			} else {
				out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"11\"}");
			}
			if (isDebug) {
				log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 11-Livro ata nao encontrado [" + aplicacao + "]");
			}
			return;
		}
		rsLivroAta.close();
		stLivroAta.close();		
					
		
		if (acao.equals("login")) {	
			if (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC")) {
				/*
				 *
				 * Valida��o 1: AA j� logado?
				 * 
				 */				 
				StringBuffer sqlLogado = new StringBuffer();
				sqlLogado.append("SELECT CD_VIATURA "); 
				sqlLogado.append("FROM LIVRO_ATA_FROTA WITH (NOLOCK) ");
				sqlLogado.append("WHERE DT_FIM_USO_VIATURA IS NULL   ");
				sqlLogado.append("AND CD_VIATURA = "+ cd_viatura +" AND CD_LIVRO_ATA = " + cd_livro_ata);
				stLogado = conn.prepareStatement(sqlLogado.toString());
				ResultSet rsLogado = stLogado.executeQuery();
				if (rsLogado.next()) {
					// Usuario j� logado
					if (isVerboseError) {
						out.print("2-Usu�rio j� logado");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"2\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 2-Usu�rio j� logado [" + aplicacao + "]");
					}
					return;
				}
				rsLogado.close();
				stLogado.close();	
				
				/*
				 *
				 * Valida��o 2: Colaborador SIGMA (VTR Mobile) valido
				 * 
				 */	 
				 
				String fone2 = fone;
				
				if (fone.length()>10){
				    String subFone = fone.substring(3);
				    
				    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
					fone2 = fone.substring(0, 2)+subFone;
				    }			    
				}
								 	
				StringBuffer sqlColSigma = new StringBuffer();
				sqlColSigma.append(" SELECT CD_COLABORADOR  "); 
				sqlColSigma.append(" FROM COLABORADOR WITH (NOLOCK) ");
				sqlColSigma.append(" WHERE ( NM_COLABORADOR LIKE 'VTR-MOBILE-%"+fone+"' OR NM_COLABORADOR LIKE 'VTR-MOBILE-%"+fone2+"' ) AND FG_ATIVO_COLABORADOR = 1 ");
				stColSigma = conn.prepareStatement(sqlColSigma.toString());
				ResultSet rsColSigma = stColSigma.executeQuery();
				if (rsColSigma.next()) {
					cd_colaborador = rsColSigma.getString("CD_COLABORADOR");
				} else {
					// Colaborador sigma n�o encontrado
					if (isVerboseError) {
						out.print("12-Colaborador SIGMA invalido");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"12\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 12-Colaborador SIGMA invalido [" + aplicacao + "]");
					}
					return;
				}
				rsColSigma.close();
				stColSigma.close();	
		
				/*
				 *
				 * Valida��o 4: VTR SIGMA valida
				 * 
				 */	 
				StringBuffer sqlVtrSigma = new StringBuffer();
				sqlVtrSigma.append(" SELECT COUNT(*) "); 
				sqlVtrSigma.append(" FROM VIATURA vtr WITH (NOLOCK) ");
				sqlVtrSigma.append(" WHERE CONVERT(VARCHAR, vtr.CD_VIATURA) = '"+ cd_viatura +"' AND FG_ATIVO = 1 ");
				stVtrSigma = conn.prepareStatement(sqlVtrSigma.toString());
				ResultSet rsVtrSigma = stVtrSigma.executeQuery();
				if (!rsVtrSigma.next()) {
					// Vtr sigma n�o encontrado
					if (isVerboseError) {
						out.print("13-Viatura SIGMA invalido");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"13\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 13-Viatura SIGMA invalido [" + aplicacao + "]");
					}
					return;
				}
				rsVtrSigma.close();
				stVtrSigma.close();	
				
				/*
				 *
				 * Valida��o 2: Rota (Colaborador/VTR Mobile) ocupado?
				 *
				 */
				StringBuffer sqlRotaOcupada = new StringBuffer();
				sqlRotaOcupada.append("SELECT CD_COLABORADOR "); 
				sqlRotaOcupada.append("FROM LIVRO_ATA_COLABORADOR WITH (NOLOCK) ");
				sqlRotaOcupada.append("WHERE DT_SAIDA_PLANTAO IS NULL  ");
				sqlRotaOcupada.append("AND CD_COLABORADOR = "+ cd_colaborador +" AND CD_LIVRO_ATA = " + cd_livro_ata);
				PreparedStatement stRotaOcupada = conn.prepareStatement(sqlRotaOcupada.toString());
				ResultSet rsRotaOcupada = stRotaOcupada.executeQuery();
				if (rsRotaOcupada.next()) {
					// Usuario j� logado
					if (isVerboseError) {
						out.print("3-Rota (VTR Mobile) ocupada");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"3\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"/"+cd_colaborador+"] via "+origem+"> 3-Rota (VTR Mobile) ocupada [" + aplicacao + "]");
					}
					return;
				}
				rsRotaOcupada.close();
				stRotaOcupada.close();					
			}
			
						
			
			/*
			 *
			 * Valida��o 3: Viatura FUSION (rastremento) valida?
			 *
			 */
			QLGroupFilter filterVtrRegional = new QLGroupFilter("AND");
			filterVtrRegional.addFilter(new QLOpFilter("placa", "LIKE" , "%"+cd_placa));
			filterVtrRegional.addFilter(new QLEqualsFilter("codigoRegional", regional));
			filterVtrRegional.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
			NeoObject viaturaNovaObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), filterVtrRegional);
			if(viaturaNovaObj == null) {
				// Viatura inv�lida
				if (isVerboseError) {
					out.print("4-Viatura inv�lida");
				} else {
					out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"4\"}");
				}
				if (isDebug) {
					log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 4-Viatura inv�lida [" + aplicacao + "]. Reg: " + regional + ". Placa: " + cd_placa);
				}
				return;
			} else {
				ewViaturaNova = new EntityWrapper(viaturaNovaObj);
				/*
				 *
				 * Validação 4: Viatura liberada?
				 *
				 */
				if (((Boolean)ewViaturaNova.findField("emUso").getValue()).booleanValue()) {
					// Viatura em uso
					if (isVerboseError) {
						out.print("5-Viatura em uso");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"5\"}");
					}
					if (isDebug) {
						log.warn("#####-- AA("+acao+") ["+cd_cliente+"] via "+origem+"> 5-Viatura em uso (" + ewViaturaNova.findField("placa").getValue() + ") [" + aplicacao + "]");
					}
					return;
				} else {
					placa = ewViaturaNova.findField("placa").getValue().toString();
					//out.print(placa);
				}
			}
		} else if (acao.equals("chgvtr")) {		
			if (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC")) {
				/*
				 *
				 *  Valida��o 1: AA n�o logado 
				 *  Recuperar placa atual
				 *
				 */
				StringBuffer sqlLogado = new StringBuffer();
				sqlLogado.append(" SELECT v.NM_PLACA ");
				sqlLogado.append(" FROM LIVRO_ATA_FROTA laf WITH (NOLOCK) ");
				sqlLogado.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON laf.CD_VIATURA = v.CD_VIATURA  ");
				sqlLogado.append(" WHERE laf.DT_FIM_USO_VIATURA IS NULL ");
				sqlLogado.append(" AND laf.CD_VIATURA = " + cd_viatura );
				sqlLogado.append(" AND laf.CD_LIVRO_ATA = " + cd_livro_ata);
				stLogado = conn.prepareStatement(sqlLogado.toString());
				ResultSet rsLogado = stLogado.executeQuery();
				if (rsLogado.next()) {
						placa_atual = rsLogado.getString("NM_PLACA");
				} else {
					// Usuario n�o logado
					if (isVerboseError) {
						out.print("2-Usu�rio n�o logado");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"2\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 2-Usu�rio n�o logado [" + aplicacao + "]");
					}
					return;
				}
				rsLogado.close();
				stLogado.close();		
			}
			if (aplicacao.equals("TEC") || aplicacao.equals("ADM")) {
				/*
				 *
				 *  Recuperar placa atual 
				 *  
				 *  
				 */
				QLGroupFilter filterVtrUso = new QLGroupFilter("AND");
				filterVtrUso.addFilter(new QLEqualsFilter("emUso", true));
				filterVtrUso.addFilter(new QLEqualsFilter("codigoDoTecnico", codigoTecnico));
				NeoObject viaturaemUsoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), filterVtrUso);
				
				if(viaturaemUsoObj == null) {
					// Usuario n�o logado
					if (isVerboseError) {
						out.print("2-Usu�rio n�o logado");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"2\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 2-Usu�rio n�o logado [" + aplicacao + "]");
					}
					return;
				} else {
					ewViaturaAtual = new EntityWrapper(viaturaemUsoObj);	
					placa_atual = ewViaturaAtual.findField("placa").getValue().toString();
				}

			}
						
			/*
			 *
			 * Valida��o 3: Viatura Fusion (rastreamento) valida?
			 *
			 */		 
			QLGroupFilter filterVtrRegional = new QLGroupFilter("AND");
			filterVtrRegional.addFilter(new QLOpFilter("placa", "LIKE" , "%"+cd_placa));
			filterVtrRegional.addFilter(new QLEqualsFilter("codigoRegional", regional));
			filterVtrRegional.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
			NeoObject viaturaNovaObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), filterVtrRegional);
			if(viaturaNovaObj == null) {
				// Viatura inv�lida
				if (isVerboseError) {
					out.print("4-Viatura inv�lida");
				} else {
					out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"4\"}");
				}
				if (isDebug) {
					log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 4-Viatura inv�lida [" + aplicacao + "]");
				}
				return;
			} else {
				ewViaturaNova = new EntityWrapper(viaturaNovaObj);
				/*
				 *
				 * Valida��o 4: Nova Viatura liberada?
				 *
				 */
				if (((Boolean)ewViaturaNova.findField("emUso").getValue()).booleanValue()) {
					// Viatura em uso
					if (isVerboseError) {
						out.print("5-Viatura em uso");
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"5\"}");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"5\"}");
					}
					if (isDebug) {
						log.warn("#####- AA("+acao+") ["+cd_cliente+"] via "+origem+"> 5-Viatura em uso (" + ewViaturaNova.findField("placa").getValue() + ") [" + aplicacao + "]");
					
					}
					return;
				} else {
					placa = ewViaturaNova.findField("placa").getValue().toString();
				}
			} 
	
		} else if (acao.equals("logoff")) {
			if (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC")) {
				/*
				 *
				 *  Valida��o 1: AA n�o logado 
				 *  Recuperar placa atual
				 *
				 */
				StringBuffer sqlLogado = new StringBuffer();
				sqlLogado.append(" SELECT v.NM_PLACA ");
				sqlLogado.append(" FROM LIVRO_ATA_FROTA laf WITH (NOLOCK) ");
				sqlLogado.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON laf.CD_VIATURA = v.CD_VIATURA  ");
				sqlLogado.append(" WHERE laf.DT_FIM_USO_VIATURA IS NULL ");
				sqlLogado.append(" AND laf.CD_VIATURA = " + cd_viatura);
				sqlLogado.append(" AND laf.CD_LIVRO_ATA = " + cd_livro_ata);
				stLogado = conn.prepareStatement(sqlLogado.toString());
				ResultSet rsLogado = stLogado.executeQuery();
				if (rsLogado.next()) {
						placa_atual = rsLogado.getString("NM_PLACA");
				} else {
					// Usuario n�o logado
					if (isVerboseError) {
						out.print("2-Usu�rio n�o logado");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"2\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 2-Usu�rio n�o logado [" + aplicacao + "]");
					}
					return;
				}
				rsLogado.close();
				stLogado.close();	
			} 
			if (aplicacao.equals("TEC")) {
				/*
				 *
				 *  Recuperar placa atual
				 *  
				 */
				QLGroupFilter filterVtrUso = new QLGroupFilter("AND");
				filterVtrUso.addFilter(new QLEqualsFilter("emUso", true));
				filterVtrUso.addFilter(new QLEqualsFilter("codigoDoTecnico", codigoTecnico));
				NeoObject viaturaemUsoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), filterVtrUso);
				
				if(viaturaemUsoObj == null) {
					// Usuario n�o logado
					if (isVerboseError) {
						out.print("2-Usu�rio n�o logado");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"2\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 2-Usu�rio n�o logado [" + aplicacao + "]");
					}
					return;
				} else {
					ewViaturaAtual = new EntityWrapper(viaturaemUsoObj);	
					placa_atual = ewViaturaAtual.findField("placa").getValue().toString();
				}			
			}
			if (aplicacao.equals("ADM")) {
				/*
				 *
				 *  Recuperar placa atual
				 *  
				 */
				QLGroupFilter filterVtrUso = new QLGroupFilter("AND");
				filterVtrUso.addFilter(new QLEqualsFilter("emUso", true));
				filterVtrUso.addFilter(new QLEqualsFilter("codigoCliente", cd_cliente));
				NeoObject viaturaemUsoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), filterVtrUso);
				
				if(viaturaemUsoObj == null) {
					// Usuario n�o logado
					if (isVerboseError) {
						out.print("2-Usu�rio n�o logado");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"2\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 2-Usu�rio n�o logado [" + aplicacao + "]");
					}
					return;
				} else {
					ewViaturaAtual = new EntityWrapper(viaturaemUsoObj);	
					placa_atual = ewViaturaAtual.findField("placa").getValue().toString();
				}			
			}			
		} else if (acao.equals("local")) {
			BigDecimal latitude_evento;
			BigDecimal longitude_evento;
			BigDecimal latitude_viatura;
			BigDecimal longitude_viatura;
			String cdEventoNoLocal = "";
			if (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC")) {
				/*
				 *
				 *  Valida��o 1: AA n�o logado 
				 *  Recuperar placa atual
				 *
				 */
				StringBuffer sqlLogado = new StringBuffer();
				sqlLogado.append(" SELECT v.NM_PLACA ");
				sqlLogado.append(" FROM LIVRO_ATA_FROTA laf WITH (NOLOCK) ");
				sqlLogado.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON laf.CD_VIATURA = v.CD_VIATURA  ");
				sqlLogado.append(" WHERE laf.DT_FIM_USO_VIATURA IS NULL ");
				sqlLogado.append(" AND laf.CD_VIATURA = " + cd_viatura);
				sqlLogado.append(" AND laf.CD_LIVRO_ATA = " + cd_livro_ata);
				stLogado = conn.prepareStatement(sqlLogado.toString());
				ResultSet rsLogado = stLogado.executeQuery();
				if (rsLogado.next()) {
					placa_atual = rsLogado.getString("NM_PLACA");
				} else {
					// Usuario n�o logado
					if (isVerboseError) {
						out.print("2-Usu�rio n�o logado");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"2\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 2-Usu�rio n�o logado [" + aplicacao + "]");
					}
					return;
				}
				rsLogado.close();
				stLogado.close();	

				/*
				 *
				 *  Valida��o 2: Somente 1 evento 
				 *  Recuperar evento deslocado e coordenadas da conta
				 *
				 */
				 
				 
				 
				StringBuffer sqlEvento = new StringBuffer();
				sqlEvento.append(" SELECT h.CD_HISTORICO, c.NU_LATITUDE, c.NU_LONGITUDE, h.CD_EVENTO, h.DT_RECEBIDO ");
				sqlEvento.append(" FROM HISTORICO h WITH (NOLOCK) ");
				sqlEvento.append(" INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = h.CD_CLIENTE ");
				sqlEvento.append(" WHERE h.FG_STATUS = 9 ");
				sqlEvento.append(" AND c.NU_LATITUDE IS NOT NULL ");
				sqlEvento.append(" AND c.NU_LONGITUDE IS NOT NULL ");				
				//sqlEvento.append(" AND h.CD_EVENTO != 'XXX8' ");
				sqlEvento.append(" AND h.CD_VIATURA = " + cd_viatura);
				sqlEvento.append(" AND h.CD_HISTORICO_PAI IS NULL  ");
				sqlEvento.append(" ORDER BY h.CD_EVENTO, h.DT_RECEBIDO ");
				stEvento = conn.prepareStatement(sqlEvento.toString());
				ResultSet rsEvento = stEvento.executeQuery();
				if (rsEvento.next()) {
					cd_historico = rsEvento.getString("CD_HISTORICO");
					latitude_evento = rsEvento.getBigDecimal("NU_LATITUDE");
					longitude_evento = rsEvento.getBigDecimal("NU_LONGITUDE");
					cdEventoNoLocal = rsEvento.getString("CD_EVENTO");
				} else {
					// Evento n�o localizado
					if (isVerboseError) {
						out.print("6-Nenhum evento para o atendente");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"6\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 6-Nenhum evento para o atendente [" + aplicacao + "]. CD_VIATURA:" + cd_viatura + ".");
					}
					return;
				}
				/*
				if (rsEvento.next()) {
					// Mais de um evento encontrado
					if (isVerboseError) {
						out.print("7-Mais de um evento para o atendente");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"7\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 7-Mais de um evento para o atendente [" + aplicacao + "]");
					}
					return;
				}
				*/				
				rsEvento.close();
				stEvento.close();	

				/*
				 *
				 *  Valida��o 3: Viatura com coordenadas? 
				 *
				 */				 
				
				//  BLOCO N�O DEVE SER EXECUTADO PARA EVENTOS DE PLACA
				
				if (!cdEventoNoLocal.equals("XPL1") && !cdEventoNoLocal.equals("XPL2") && !cdEventoNoLocal.equals("XPL3")){
				 
					QLGroupFilter filterVtrRegional = new QLGroupFilter("AND");
					filterVtrRegional.addFilter(new QLEqualsFilter("placa", placa_atual));
					filterVtrRegional.addFilter(new QLEqualsFilter("codigoRegional", regional));
					filterVtrRegional.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
					NeoObject viaturaAtualObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), filterVtrRegional);
					if(viaturaAtualObj != null) {
						ewViaturaAtual = new EntityWrapper(viaturaAtualObj);
						latitude_viatura = (BigDecimal)ewViaturaAtual.findField("latitude").getValue();
						longitude_viatura = (BigDecimal)ewViaturaAtual.findField("longitude").getValue();
						if (latitude_viatura == null || longitude_viatura == null ) {
							if (isVerboseError) {
								out.print("8-Viatura com coordenadas inv�lidas");
							} else {
								out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"8\"}");
							}
							if (isDebug) {
								log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 8-Viatura com coordenadas inv�lidas " + placa_atual + " [" + aplicacao + "]: " + latitude_viatura + "," +longitude_viatura );							}
							return;
						}
						if (isDebug) {
							log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> Coordenadas atuais da VTR " + placa_atual + " [" + aplicacao + "]: " + latitude_viatura + "," +longitude_viatura );
						}
					} else {
						
						if (isVerboseError) {
							out.print("4-Viatura inv�lida");
						} else {
							out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"4\"}");
						}
						if (isDebug) {
							log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 4-Viatura inv�lida [" + aplicacao + "]");
						}
						return;
					}				
					
					/*
					 *
					 *  Valida��o 3: Posicao x Dist�ncia
					 *
					 */
					double distancia = distanceInMeters(latitude_viatura.doubleValue(), longitude_viatura.doubleValue(), latitude_evento.doubleValue(), longitude_evento.doubleValue());
					if (distancia > margemDistancia) {
						// Evento n�o localizado
						if (isVerboseError) {
							out.print("10-AA fora da margem de seguran�a (dist�ncia)");
						} else {
							out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"10\"}");
						}
						if (isDebug) {
							log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 10-AA fora da margem de seguran�a (dist�ncia): " + distancia + " metros [" + aplicacao + "]");
						}
						return;
					} else {
						if (isDebug) {
							log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> Dist�ncia OK (Evento x AA): " + distancia + " metros [" + aplicacao + "]");
						}
					}
				}
				// TERMINA AQUI
				
				
			}
			
		} else if (acao.equals("semalteracao")) {
			if (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC")) {
			    
			    /**
				* Valida��o 13: Evento de PORTARIA REMOTA
				*/
			    
			    StringBuffer sqlEventoPROCA = new StringBuffer();
			    sqlEventoPROCA.append(" SELECT 1 FROM HISTORICO VH ");
			    sqlEventoPROCA.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = VH.CD_CLIENTE ");
			    sqlEventoPROCA.append(" WHERE VH.CD_VIATURA = "+ cd_viatura +" AND VH.FG_STATUS = 3 AND (C.ID_EMPRESA IN (10119,10120) OR C.CD_GRUPO_CLIENTE = 128) ");
			    PreparedStatement stEventoPROCA = conn.prepareStatement(sqlEventoPROCA.toString());
			    ResultSet rsEventoPROCA = stEventoPROCA.executeQuery();
			    if (rsEventoPROCA.next()){
					System.out.println("Erro 13 evento de portaria remota cd_viatura: "+cd_viatura);
					if (isVerboseError) {
						out.print("13-Evento de Portaria Remota e/ou Controle de Acesso");
					} else {
						out.print("{\"id\":\""+cd_viatura+"\", \"return\":\"13\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_viatura+"] via "+origem+"> 13-Evento de portaria remota [" + aplicacao + "]");
					}
					return;
			    }else{
					System.out.println("Erro 13 nao entrou evento de portaria remota cd_viatura: "+cd_viatura);
			    }
			    stEventoPROCA.close();
			    rsEventoPROCA.close();
				    			   
				    
			    /**
			    * Valida��o 11: Evento de alto risco
			    */
			    
			    StringBuffer sqlEventoRisco = new StringBuffer();
			    sqlEventoRisco.append(" SELECT H.CD_HISTORICO, H.TX_OBSERVACAO_FECHAMENTO FROM HISTORICO H  ");
			    sqlEventoRisco.append(" INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE ");
			    sqlEventoRisco.append(" INNER JOIN (SELECT count(distinct h2.NU_AUXILIAR) AS CONTADOR,c2.ID_CENTRAL, c2.ID_EMPRESA FROM HISTORICO H2 ");
			    sqlEventoRisco.append(" INNER JOIN dbCENTRAL C2 ON C2.CD_CLIENTE = H2.CD_CLIENTE ");
			    sqlEventoRisco.append(" WHERE H2.FG_STATUS in (1,2,3,8,9) ");
			    sqlEventoRisco.append(" AND c2.ID_CENTRAL NOT LIKE 'AAA%' AND c2.ID_CENTRAL NOT LIKE 'DDDD' AND c2.ID_CENTRAL NOT LIKE 'FFFF' AND c2.ID_CENTRAL NOT LIKE 'R%' ");
			    sqlEventoRisco.append(" group by c2.ID_CENTRAL, c2.ID_EMPRESA ");
			    sqlEventoRisco.append(" having count(distinct h2.NU_AUXILIAR) > 2) TABELA1 ON TABELA1.ID_CENTRAL = C.ID_CENTRAL AND TABELA1.ID_EMPRESA = C.ID_EMPRESA ");
			    sqlEventoRisco.append(" WHERE H.CD_VIATURA = "+ cd_viatura +" AND H.DT_VIATURA_NO_LOCAL IS NOT NULL AND H.FG_STATUS = 3 ");
			    sqlEventoRisco.append(" AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' 	AND c.ID_CENTRAL NOT LIKE 'FFFF' AND c.ID_CENTRAL NOT LIKE 'R%' ");
			    PreparedStatement stEventoRisco = conn.prepareStatement(sqlEventoRisco.toString());
			    ResultSet rsEventoRisco = stEventoRisco.executeQuery();
			    if (rsEventoRisco.next()){
					System.out.println("Erro 11 evento de alto risco cd_viatura: "+cd_viatura);
					Long tempHistorico = rsEventoRisco.getLong("CD_HISTORICO");
					String tempObsFechamento = rsEventoRisco.getString("TX_OBSERVACAO_FECHAMENTO");
					if (tempObsFechamento == null){
					    tempObsFechamento = "";
					}
					tempObsFechamento += "\n\n--- Evento de alto risco ---";
					//--------------------------
					StringBuffer sqlUpdateLogErro11 = new StringBuffer();
					sqlUpdateLogErro11.append(" UPDATE HISTORICO SET TX_OBSERVACAO_FECHAMENTO = ? WHERE CD_HISTORICO = ? ");
					PreparedStatement stLogErro11 = conn.prepareStatement(sqlUpdateLogErro11.toString());
					stLogErro11 = conn.prepareStatement(sqlUpdateLogErro11.toString());
					stLogErro11.setString(1, tempObsFechamento);
					stLogErro11.setLong(2, tempHistorico);
					stLogErro11.executeUpdate();
					stLogErro11.close();	
					//---------------------------
					if (isVerboseError) {
						out.print("11-Evento de alto risco");
					} else {
						out.print("{\"id\":\""+cd_viatura+"\", \"return\":\"11\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_viatura+"] via "+origem+"> 11-Evento de alto risco [" + aplicacao + "]");
					}
					return;
			    }else{
					System.out.println("Erro 11 nao entrou evento de alto risco cd_viatura: "+cd_viatura);
			    }
			    stEventoRisco.close();
			    rsEventoRisco.close();
			    
			    /**
			    * Valida��o 11: Evento de alto risco CFTV
			    */
			    
			    StringBuffer sqlEventoRiscoCFTV = new StringBuffer();
			    
			    sqlEventoRiscoCFTV.append(" SELECT H.CD_HISTORICO, H.TX_OBSERVACAO_FECHAMENTO ");
			    sqlEventoRiscoCFTV.append(" FROM HISTORICO H WITH(NOLOCK) ");
			    sqlEventoRiscoCFTV.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
			    sqlEventoRiscoCFTV.append(" WHERE H.CD_VIATURA = "+ cd_viatura +" AND H.DT_VIATURA_NO_LOCAL IS NOT NULL AND H.FG_STATUS = 3 "); 
			    sqlEventoRiscoCFTV.append(" AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' 	AND c.ID_CENTRAL NOT LIKE 'FFFF' AND c.ID_CENTRAL NOT LIKE 'R%' "); 
			    sqlEventoRiscoCFTV.append(" AND EXISTS (SELECT COUNT(DISTINCT H2.NU_AUXILIAR) FROM VIEW_HISTORICO H2 WITH(NOLOCK) ");
			    sqlEventoRiscoCFTV.append(" INNER JOIN dbCENTRAL C2 WITH(NOLOCK) ON C2.CD_CLIENTE = H2.CD_CLIENTE ");
			    sqlEventoRiscoCFTV.append(" WHERE C2.ID_CENTRAL = C.ID_CENTRAL AND C2.ID_EMPRESA = C.ID_EMPRESA AND H2.DT_RECEBIDO >= H.DT_RECEBIDO ");
			    sqlEventoRiscoCFTV.append(" HAVING COUNT(DISTINCT H2.NU_AUXILIAR) > 2) ");
			    
			    PreparedStatement stEventoRiscoCFTV = conn.prepareStatement(sqlEventoRiscoCFTV.toString());
			    ResultSet rsEventoRiscoCFTV = stEventoRiscoCFTV.executeQuery();
			    if (rsEventoRiscoCFTV.next()){
					System.out.println("Erro 11 evento de alto risco CFTV cd_viatura: "+cd_viatura);
					Long tempHistoricoCFTV = rsEventoRiscoCFTV.getLong("CD_HISTORICO");
					String tempObsFechamentoCFTV = rsEventoRiscoCFTV.getString("TX_OBSERVACAO_FECHAMENTO");
					if (tempObsFechamentoCFTV == null){
					    tempObsFechamentoCFTV = "";
					}
					tempObsFechamentoCFTV += "\n\n--- Evento de alto risco CFTV ---";
					//--------------------------
					StringBuffer sqlUpdateLogErro11CFTV = new StringBuffer();
					sqlUpdateLogErro11CFTV.append(" UPDATE HISTORICO SET TX_OBSERVACAO_FECHAMENTO = ? WHERE CD_HISTORICO = ? ");
					PreparedStatement stLogErro11CFTV = conn.prepareStatement(sqlUpdateLogErro11CFTV.toString());
					stLogErro11CFTV = conn.prepareStatement(sqlUpdateLogErro11CFTV.toString());
					stLogErro11CFTV.setString(1, tempObsFechamentoCFTV);
					stLogErro11CFTV.setLong(2, tempHistoricoCFTV);
					stLogErro11CFTV.executeUpdate();
					stLogErro11CFTV.close();	
					//---------------------------
					if (isVerboseError) {
						out.print("11-Evento de alto risco");
					} else {
						out.print("{\"id\":\""+cd_viatura+"\", \"return\":\"11\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_viatura+"] via "+origem+"> 11-Evento de alto risco [" + aplicacao + "]");
					}
					return;
			    }else{
					System.out.println("Erro 11 nao entrou evento de alto risco cd_viatura: "+cd_viatura);
			    }
			    stEventoRiscoCFTV.close();
			    rsEventoRiscoCFTV.close();
			    			   			   
			    
			    /**
			    * Valida��o 12: Cliente de alto risco
			    */
			    
			    StringBuffer sqlClienteRisco = new StringBuffer();
			    sqlClienteRisco.append("  SELECT H.CD_HISTORICO, H.TX_OBSERVACAO_FECHAMENTO, C.ID_CENTRAL, C.ID_EMPRESA FROM HISTORICO H  ");
			    sqlClienteRisco.append("  INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE ");
			    sqlClienteRisco.append("  WHERE H.CD_VIATURA = "+ cd_viatura +" AND H.DT_VIATURA_NO_LOCAL IS NOT NULL AND H.FG_STATUS = 3 "); 
			    sqlClienteRisco.append("  AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' AND c.ID_CENTRAL NOT LIKE 'FFFF' AND c.ID_CENTRAL NOT LIKE 'R%' AND H.CD_EVENTO NOT IN ('XXX7') "); 
			    sqlClienteRisco.append("  AND EXISTS (SELECT 1 FROM [FSOODB04\\SQL02].TIDB.DBO.ARROMBAMENTO_CONFIRMADO AC WHERE AC.ID_CENTRAL = C.ID_CENTRAL AND AC.ID_EMPRESA = C.ID_EMPRESA) ");
			    PreparedStatement stClienteRisco = conn.prepareStatement(sqlClienteRisco.toString());
			    ResultSet rsClienteRisco = stClienteRisco.executeQuery();
			    if (rsClienteRisco.next()){
					System.out.println("Erro 12 evento de alto risco cd_viatura: "+cd_viatura);
					Long tempHistorico = rsClienteRisco.getLong("CD_HISTORICO");
					String tempObsFechamento = rsClienteRisco.getString("TX_OBSERVACAO_FECHAMENTO");
					if (tempObsFechamento == null){
					    tempObsFechamento = "";
					}
					tempObsFechamento += "\n\n--- Cliente de alto risco ---";
					//--------------------------
					StringBuffer sqlUpdateLogErro12 = new StringBuffer();
					sqlUpdateLogErro12.append(" UPDATE HISTORICO SET TX_OBSERVACAO_FECHAMENTO = ? WHERE CD_HISTORICO = ? ");
					PreparedStatement stLogErro12 = conn.prepareStatement(sqlUpdateLogErro12.toString());
					stLogErro12 = conn.prepareStatement(sqlUpdateLogErro12.toString());
					stLogErro12.setString(1, tempObsFechamento);
					stLogErro12.setLong(2, tempHistorico);
					stLogErro12.executeUpdate();
					stLogErro12.close();	
					//---------------------------
					if (isVerboseError) {
						out.print("12-Cliente de alto risco");
					} else {
						out.print("{\"id\":\""+cd_viatura+"\", \"return\":\"12\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_viatura+"] via "+origem+"> 12-Cliente de alto risco [" + aplicacao + "]");
					}
					return;
			    }else{
					System.out.println("Erro 12 nao entrou evento de alto risco cd_viatura: "+cd_viatura);
			    }
			    stClienteRisco.close();
			    rsClienteRisco.close();  
			    
			    
			    /**
			    * Valida��o 14: Cliente com atualiza��o cadastral
			    */
			    
			    StringBuffer sqlClienteACRisco = new StringBuffer();
			    sqlClienteACRisco.append(" SELECT H.CD_HISTORICO, H.TX_OBSERVACAO_FECHAMENTO ");
			    sqlClienteACRisco.append(" FROM HISTORICO H ");
			    sqlClienteACRisco.append(" INNER JOIN DBCENTRAL C ON C.CD_CLIENTE = H.CD_CLIENTE ");
			    sqlClienteACRisco.append(" WHERE H.CD_VIATURA = "+ cd_viatura +" AND H.FG_STATUS = 3 AND H.DT_VIATURA_NO_LOCAL IS NOT NULL AND H.CD_CODE = 'ACC' ");
			    sqlClienteACRisco.append(" AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' 	AND c.ID_CENTRAL NOT LIKE 'FFFF' AND c.ID_CENTRAL NOT LIKE 'R%' ");
			    
			    PreparedStatement stClienteACRisco = conn.prepareStatement(sqlClienteACRisco.toString());
			    ResultSet rsClienteACRisco = stClienteACRisco.executeQuery();
			    if (rsClienteACRisco.next()){
					System.out.println("Erro 14 cliente atualizacao cadastral cd_viatura: "+cd_viatura);
					Long tempHistoricoAC = rsClienteACRisco.getLong("CD_HISTORICO");
					String tempObsFechamentoAC = rsClienteACRisco.getString("TX_OBSERVACAO_FECHAMENTO");
					if (tempObsFechamentoAC == null){
					    tempObsFechamentoAC = "";
					}
					tempObsFechamentoAC += "\n\n--- Cliente com Atualiza��o Cadastral ---";
					//--------------------------
					StringBuffer sqlUpdateLogErro14 = new StringBuffer();
					sqlUpdateLogErro14.append(" UPDATE HISTORICO SET TX_OBSERVACAO_FECHAMENTO = ? WHERE CD_HISTORICO = ? ");
					PreparedStatement stLogErro14 = conn.prepareStatement(sqlUpdateLogErro14.toString());
					stLogErro14 = conn.prepareStatement(sqlUpdateLogErro14.toString());
					stLogErro14.setString(1, tempObsFechamentoAC);
					stLogErro14.setLong(2, tempHistoricoAC);
					stLogErro14.executeUpdate();
					stLogErro14.close();	
					//---------------------------
					if (isVerboseError) {
						out.print("14-Cliente com Atualizacao Cadastral");
					} else {
						out.print("{\"id\":\""+cd_viatura+"\", \"return\":\"14\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_viatura+"] via "+origem+"> 14-Cliente com Atualizacao Cadastral [" + aplicacao + "]");
					}
					return;
			    }else{
					System.out.println("Erro 14 nao entrou cliente com atualizacao cadastral cd_viatura: "+cd_viatura);
			    }
			    stClienteACRisco.close();
			    rsClienteACRisco.close();  
			    
/*
				 *
				 *  Valida��o 1: AA n�o logado 
				 *  Recuperar placa atual
				 *
				 */
				StringBuffer sqlLogado = new StringBuffer();
				sqlLogado.append(" SELECT v.NM_PLACA ");
				sqlLogado.append(" FROM LIVRO_ATA_FROTA laf WITH (NOLOCK) ");
				sqlLogado.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON laf.CD_VIATURA = v.CD_VIATURA  ");
				sqlLogado.append(" WHERE laf.DT_FIM_USO_VIATURA IS NULL ");
				sqlLogado.append(" AND laf.CD_VIATURA = " + cd_viatura);
				sqlLogado.append(" AND laf.CD_LIVRO_ATA = " + cd_livro_ata);
				stLogado = conn.prepareStatement(sqlLogado.toString());
				ResultSet rsLogado = stLogado.executeQuery();
				if (rsLogado.next()) {
						placa_atual = rsLogado.getString("NM_PLACA");
				} else {
					// Usuario n�o logado
					if (isVerboseError) {
						out.print("2-Usu�rio n�o logado");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"2\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 2-Usu�rio n�o logado [" + aplicacao + "]");
					}
					return;
				}
				rsLogado.close();
				stLogado.close();	
				
				//BLOCO VERIFICACAO DE EVENTO COM FILHOS
				
				ResultSet rsEventoComFilhos = null;
				ResultSet rsEventoAtual = null;
				
				String evtAtual = "";
				String codigoClienteEvtAtual = "";
				
				StringBuffer sqlEvtAtual = new StringBuffer();
				sqlEvtAtual.append(" SELECT H.CD_EVENTO, H.CD_CLIENTE FROM HISTORICO H ");
				sqlEvtAtual.append(" WHERE H.FG_STATUS = 3 AND H.CD_VIATURA = " + cd_viatura);
				stEventoAtual = conn.prepareStatement(sqlEvtAtual.toString());
				rsEventoAtual = stEventoAtual.executeQuery();
				
				if (rsEventoAtual.next()) {
					evtAtual = rsEventoAtual.getString("CD_EVENTO");
					codigoClienteEvtAtual = rsEventoAtual.getString("CD_CLIENTE");
				}
				
				if (isVerificarSemAlteracao) {

					if (!evtAtual.equals("XDSE") && !evtAtual.equals("XDSC")) {
						
						StringBuilder checaChaveSql = new StringBuilder();
						checaChaveSql.append("select chave from dbCENTRAL d where d.CD_CLIENTE = " + codigoClienteEvtAtual);
						ResultSet rschave = conn.prepareStatement(checaChaveSql.toString()).executeQuery();
						
						while (rschave.next()) {
							String temChave = rschave.getString("chave");
							
							if (temChave.equals("1")) {
								out.print("{\"id\":\"" + cd_cliente + "\", \"return\":\"70\"}");
								return;								
							}
						}
						
						rschave.close();
					}	
					StringBuffer sqlEventoComFilhos = new StringBuffer();
					sqlEventoComFilhos.append(" SELECT H.CD_HISTORICO, H.CD_EVENTO FROM HISTORICO H WITH(NOLOCK) ");
					sqlEventoComFilhos.append(" WHERE CD_VIATURA= " + cd_viatura);
					sqlEventoComFilhos.append(
							" AND EXISTS (SELECT * FROM HISTORICO H2 WITH(NOLOCK) WHERE H2.CD_HISTORICO_PAI = H.CD_HISTORICO) ");

					stEventoComFilhos = conn.prepareStatement(sqlEventoComFilhos.toString());
					rsEventoComFilhos = stEventoComFilhos.executeQuery();
					if (rsEventoComFilhos.next()) {
						// VALIDACAO DE EVENTO XXX1

						String cdEvento = rsEventoComFilhos.getString("CD_EVENTO");

						if (cdEvento.equalsIgnoreCase("XXX1")) {
							out.print("{\"id\":\"" + cd_cliente + "\", \"return\":\"60\"}");
							return;
						}

						cd_historico = rsEventoComFilhos.getString("CD_HISTORICO");
						out.print("{\"id\":\"" + cd_cliente + "\", \"return\":\"50\"}");
						return;
					} else {
						System.out.print("N�o encontrou evento com filho");
						isVerificarSemAlteracao = false;
					}
				} else {
					System.out.print("N�o verificou eventos filhos");
					isVerificarSemAlteracao = false;
				}
				
				if (rsEventoComFilhos != null){
					rsEventoComFilhos.close();	
				}
				
				if (stEventoComFilhos != null){
					stEventoComFilhos.close();	
				}
				
				
				/*
				 *
				 *  Valida��o 2: Somente 1 evento no local
				 *  Recuperar evento com status no local
				 *
				 */				 
				StringBuffer sqlEvento = new StringBuffer();
				sqlEvento.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO ");
				sqlEvento.append(" FROM HISTORICO h WITH (NOLOCK) ");
				sqlEvento.append(" INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = h.CD_CLIENTE ");
				sqlEvento.append(" WHERE h.FG_STATUS = 3");
				sqlEvento.append(" AND h.CD_HISTORICO_PAI IS NULL  ");
				sqlEvento.append(" AND h.CD_VIATURA = " + cd_viatura);
				stEvento = conn.prepareStatement(sqlEvento.toString());
				ResultSet rsEvento = stEvento.executeQuery();
				if (rsEvento.next()) {
					cd_historico = rsEvento.getString("CD_HISTORICO");
				    //VALIDACAO DE EVENTO XXX1
				    
				    String cdEvento = rsEvento.getString("CD_EVENTO");
				    
				    if (cdEvento.equalsIgnoreCase("XXX1")){
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"60\"}");
						return;
				    }

				} else {
					// Evento n�o localizado
					if (isVerboseError) {
						out.print("6-Nenhum evento para o atendente");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"6\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 6-Nenhum evento para o atendente [" + aplicacao + "]. CD_VIATURA:" + cd_viatura + ".");
					}
					return;
				}
				
				if (rsEvento.next()) {
					// Mais de um evento encontrado
					if (isVerboseError) {
						out.print("7-Mais de um evento para o atendente");
					} else {
						out.print("{\"id\":\""+cd_cliente+"\", \"return\":\"7\"}");
					}
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> 7-Mais de um evento para o atendente [" + aplicacao + "]");
					}
					return;
				}			
				rsEvento.close();
				stEvento.close();	

			}
			
		}
							
		if (!isTest) {
		
			if (acao.equals("login") || acao.equals("chgvtr")) {
			
				/*
				 *
				 * 1. Atualiza Status da Nova Viatura no Fusion
				 *
				 */
				QLGroupFilter filterVtrRegional = new QLGroupFilter("AND");
				filterVtrRegional.addFilter(new QLOpFilter("placa", "LIKE" , "%"+cd_placa));
				filterVtrRegional.addFilter(new QLEqualsFilter("codigoRegional", regional));
				filterVtrRegional.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
				NeoObject viaturaNovaObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), filterVtrRegional);
				
				if (acao.equals("login")) {
					insertMove(viaturaNovaObj, cd_cliente, fantasia, 1L); 
				} else {
					insertMove(viaturaNovaObj, cd_cliente, fantasia, 3L); 
				}
				// Seta rastreador
				if (ewViaturaNova.getValue("rastreador") == null) {
					NeoObject vtrMobileRastreadorObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSRastreador").getEntityClass(), new QLEqualsFilter("id", fone));
					ewViaturaNova.setValue("rastreador", vtrMobileRastreadorObj);
				}
				ewViaturaNova.setValue("emUso", true);				
				ewViaturaNova.setValue("atrasoDeslocamento", false);
				ewViaturaNova.setValue("titulo", sigla_rota);
				ewViaturaNova.setValue("motorista", fantasia);
				ewViaturaNova.setValue("dataMovimentacao", new GregorianCalendar());
				ewViaturaNova.setValue("telefone", fone);
				//ewViaturaNova.setValue("codigoRegional", fantasia.substring(0,3));
				ewViaturaNova.setValue("codigoDoTecnico", codigoTecnico);
				ewViaturaNova.setValue("aplicacaoViatura", aplicacaoObj);
				ewViaturaNova.setValue("codigoCliente", cd_cliente);
				ewViaturaNova.setValue("codigoViatura", cd_viatura);
				PersistEngine.persist(viaturaNovaObj);
				PersistEngine.commit(true);
				
				boolean ativarRota = false;
				
				if(!fantasia.contains("- TEC") && !fantasia.contains("-TEC") && !fantasia.contains("-INSP") && !fantasia.contains("-MOTO") && !fantasia.contains("-ADM")  && !fantasia.contains("OFFICE") && !fantasia.contains("COORD") && !fantasia.contains("ENG") ){

				    if (fantasia.contains("SUP")){
						if (!fantasia.contains("SOO") && !fantasia.contains("CTA")){
						    ativarRota = true;
						}
				    }else{
						ativarRota = true;
				    }   
				   	    
				}
				
				if (ativarRota){
				    ewViaturaNova.setValue("controlarRota", true);
				    String viaturaSigma = utils.getNomeViaturaSigma(cd_viatura);
				    Map<String, String> regionalRota = utils.getRegionalRota(viaturaSigma);
				    mapUtils.setRotaEfetiva(Long.valueOf(cd_viatura),regionalRota.get(SIGMAUtilsEngine.REGIONAL)+"-"+regionalRota.get(SIGMAUtilsEngine.ROTA));
				}else{
				    ewViaturaNova.setValue("controlarRota", false); 
				}
								
				if (isDebug) {
					log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> Reservada VTR: " + ewViaturaNova.getValue("placa") + " [" + aplicacao + "]");
				}
				
				if (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC")) {
					
					/*
					 *
					 * 2. Seta Placa da Viatura no Sigma
					 *
					 */
					StringBuffer sqlUpdate = new StringBuffer();
					sqlUpdate.append("UPDATE VIATURA ");
					sqlUpdate.append("SET NM_PLACA = ? ");
					sqlUpdate.append("WHERE VIATURA.CD_VIATURA = ? ");
					stUpdate = conn.prepareStatement(sqlUpdate.toString());
					stUpdate.setString(1, placa);
					stUpdate.setString(2, cd_viatura);
					stUpdate.executeUpdate();
					stUpdate.close();	
					if (isDebug) {
						log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> Atualizada VTR no SIGMA:" + placa + " [" + aplicacao + "]");
					}
				}
			}
			
			if (acao.equals("login")) {
				if (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC")) {
					if ((fone != null) && (!fone.trim().equals(""))&& (!fone.trim().equals("fusion"))) {
						/*
						 *
						 * Atualiza telefone da rota no Sigma
						 *
						 */	 
						StringBuffer sqlUpdateFone = new StringBuffer();
						sqlUpdateFone.append("UPDATE ROTA ");
						sqlUpdateFone.append("SET TELEFONE = ? ");
						sqlUpdateFone.append("FROM dbCENTRAL ");
						sqlUpdateFone.append("WHERE (dbCENTRAL.ID_ROTA = ROTA.CD_ROTA OR dbCENTRAL.ID_ROTA2 = ROTA.CD_ROTA)  ");
						sqlUpdateFone.append("AND dbCENTRAL.CD_CLIENTE = ? ");
						stUpdateFone = conn.prepareStatement(sqlUpdateFone.toString());
						stUpdateFone.setString(1, fone);
						stUpdateFone.setString(2, cd_cliente);
						stUpdateFone.executeUpdate();
						stUpdateFone.close();	
						if (isDebug) {
							log.warn("##### AA("+acao+") ["+cd_cliente+"] via "+origem+"> Atualizado fone da Rota no SIGMA:" + fone + " [" + aplicacao + "]");
						}
					}
				}
			}			
			
			if (acao.equals("logoff")) {			
				/*
				 *
				 * 1. Libera Viatura Atual no Fusion
				 *
				 */
				NeoObject viaturaAtualObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), new QLEqualsFilter("placa", placa_atual));
				if(viaturaAtualObj != null) {
					ewViaturaAtual = new EntityWrapper(viaturaAtualObj);
					insertMove(viaturaAtualObj, cd_cliente, fantasia, 2L); 
					//cd_viatura = (String) ewViaturaAtual.findField("codigoViatura").getValue();
					// Limpa rastreador SEGWARE MOBILE
					if ((ewViaturaAtual.findValue("rastreador.modeloEquipamento.nomeModelo") != null) && (ewViaturaAtual.findValue("rastreador.modeloEquipamento.nomeModelo").toString().contains("SEGWARE"))) {
						ewViaturaAtual.setValue("rastreador", null);
					}
										    
					
			    String rota = (String) ewViaturaAtual.getValue("rotaEfetiva");

			    if (rota != null && !rota.isEmpty()) {
					mapUtils.removerRotaEfetiva(Long.valueOf(cd_viatura), rota);
			    }
				

			    
			    ewViaturaAtual.setValue("emUso", false);
			    ewViaturaAtual.setValue("rotaEfetiva", null);
			    ewViaturaAtual.setValue("listaRotasEfetivas", null);
			    ewViaturaAtual.setValue("atrasoDeslocamento", false);
			    ewViaturaAtual.setValue("titulo", "Liberada em " + NeoUtils.safeDateFormat(new GregorianCalendar()));
			    ewViaturaAtual.setValue("motorista", fantasia);
			    ewViaturaAtual.setValue("dataMovimentacao", new GregorianCalendar());
			    ewViaturaAtual.setValue("telefone", fone);
			    //ewViaturaAtual.setValue("codigoRegional", fantasia.substring(0,3));
			    ewViaturaAtual.setValue("codigoDoTecnico", "");
			    
			    String codigoViatura = ewViaturaAtual.findField("codigoViatura").getValueAsString();
			    
			    if (codigoViatura != null && !codigoViatura.isEmpty()){
					List<NeoObject> eventosList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), new QLEqualsFilter("cdViaturaSigma", codigoViatura));
					
					if (eventosList != null && !eventosList.isEmpty()){
					    for (NeoObject o : eventosList){
							PersistEngine.remove(o);
					    }
					}
					
			    }

			    if (isDebug) {
				log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Liberada VTR em uso no Fusion:" + placa_atual + " [" + aplicacao + "]");
			    }

			    if (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC")) {

				/*
				 *
				 * 99. Seta Placa em branco da Viatura no Sigma
				 *
				 */
				StringBuffer sqlUpdate = new StringBuffer();
				sqlUpdate.append("UPDATE VIATURA ");
				sqlUpdate.append("SET NM_PLACA = ? ");
				sqlUpdate.append("WHERE VIATURA.CD_VIATURA = ? ");
				stUpdate = conn.prepareStatement(sqlUpdate.toString());
				stUpdate.setString(1, "");
				stUpdate.setString(2, cd_viatura);
				stUpdate.executeUpdate();
				stUpdate.close();
				if (isDebug) {
				    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Zerada placa VTR no SIGMA:" + placa_atual + " [" + aplicacao + "]");
				}
				if (cd_viatura != null && !cd_viatura.isEmpty())
				    verificaFilaEvento(cd_viatura);
			    }
			}
		    }
		    if (acao.equals("local")) {
			/* 
			 *
			 * 1. Altera Status do Evento Deslocado para No Local
			 *
			 */
			StringBuffer sqlUpdateStatus = new StringBuffer();
			sqlUpdateStatus.append(" UPDATE HISTORICO ");
			sqlUpdateStatus.append(" SET FG_STATUS = 3, CD_USUARIO_VIATURA_NO_LOCAL = 9999, DT_VIATURA_NO_LOCAL = GETDATE(),  ");
			sqlUpdateStatus.append(" NU_TEMPO_VIATURA_DESLOCAMENTO = DATEDIFF(MILLISECOND, DT_VIATURA_DESLOCAMENTO, GETDATE()), CD_FALHA_OPERACIONAL_STATUS = NULL ");
			sqlUpdateStatus.append(" WHERE CD_HISTORICO = ? AND FG_STATUS IN (2, 9)  ");
			stUpdateStatus = conn.prepareStatement(sqlUpdateStatus.toString());
			stUpdateStatus.setString(1, cd_historico);
			int count = stUpdateStatus.executeUpdate();
			stUpdateStatus.close();
			if (isDebug) {
			    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Atualizado (" + count + ") Status do Evento (" + cd_historico + ") para No Local [" + aplicacao + "]");
			}
			/*
			 *
			 * 2. Altera Status dos Eventos filhos para No Local
			 *
			 */
			sqlUpdateStatus = new StringBuffer();
			sqlUpdateStatus.append(" UPDATE HISTORICO ");
			sqlUpdateStatus.append(" SET FG_STATUS = 3 ");
			sqlUpdateStatus.append(" WHERE CD_HISTORICO_PAI = ? AND FG_STATUS NOT IN (6,8)  ");
			stUpdateStatus = conn.prepareStatement(sqlUpdateStatus.toString());
			stUpdateStatus.setString(1, cd_historico);
			count = stUpdateStatus.executeUpdate();
			stUpdateStatus.close();
			if (isDebug) {
			    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Atualizado(s) (" + count + ") Status do(s) Evento(s) Filho(s) (" + cd_historico + ") para No Local [" + aplicacao
				    + "]");
			}

		    }
		    if (acao.equals("semalteracao") && !isVerificarSemAlteracao) {
				boolean possuiChave = false;
				if(respostaEventoAgrupado != null){
					String arrayMensagem[] = respostaEventoAgrupado.split(",");
					possuiChave = arrayMensagem.length == 4;
				}
			/*
			 *
			 * 1. Altera Status dos Eventos filhos para Fechado
			 *
			 */
				StringBuffer sqlUpdateStatus = new StringBuffer();
				sqlUpdateStatus.append(" UPDATE HISTORICO ");
				sqlUpdateStatus.append(" SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE(), CD_USUARIO_FECHAMENTO = 9999 ");
				sqlUpdateStatus.append(" WHERE CD_HISTORICO_PAI = ? AND FG_STATUS NOT IN (6)  ");
				stUpdateStatus = conn.prepareStatement(sqlUpdateStatus.toString());
				stUpdateStatus.setString(1, cd_historico);
				int count = stUpdateStatus.executeUpdate();
				stUpdateStatus.close();
				if (isDebug) {
				    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Atualizado(s) (" + count + ") Status do(s) Evento(s) Filho(s) (" + cd_historico + ") para Fechado [" + aplicacao
					    + "]");
				}
				/* 
				 *
				 * 2. Altera Status do Evento Deslocado para Fechado
				 *
				 */
				sqlUpdateStatus = new StringBuffer();
				if(possuiChave){
					sqlUpdateStatus.append(" UPDATE HISTORICO SET ");
					String sqlAux = "";
					boolean realizouInterna = false;
					if(respostaEventoAgrupado != null){
						String arrayMensagem[] = respostaEventoAgrupado.split(",");
					    if(arrayMensagem[3].equals("1")){
					    	realizouInterna = true;
					    	//realizou a vistoria interna
					    	sqlUpdateStatus.append(" FG_STATUS = 4 , DT_FECHAMENTO = GETDATE(), CD_USUARIO_FECHAMENTO = 9999, CD_MOTIVO_ALARME = 48 ");
					    }
				    	if(realizouInterna) sqlUpdateStatus.append(" , ");
				    	
				    	sqlAux = " TX_OBSERVACAO_FECHAMENTO = 'Voc� realizou a vistoria completa no patrim�nio do cliente? \r\n R:" + (arrayMensagem[0].equals("1") ? "Sim" : "N�o")
								+ "' + \r\n (CHAR(10) + CHAR(13)) + 'Voc� confirma que o patrim�nio est� sem altera��o? \r\n R:" + (arrayMensagem[1].equals("1") ? "Sim" : "N�o")
								+ "'  + \r\n (CHAR(10) + CHAR(13)) + 'Foi necess�rio acionar o cliente para acompanhar a vistoria? \r\n R:" + (arrayMensagem[2].equals("1") ? "Sim" : "N�o")
								+ "'  + \r\n (CHAR(10) + CHAR(13)) + 'Esse cliente possui chave de acesso, voc� realizou vistoria interna no patrim�nio? \r\n R:" + (arrayMensagem[3].equals("1") ? "Sim" : "N�o")
								+ "' \r\n +(CHAR(10) + CHAR(13))+ TX_OBSERVACAO_FECHAMENTO ";
					}
			
				
					if (!sqlAux.isEmpty()) {
					    sqlUpdateStatus.append(sqlAux);
					    System.out.println("entradaAAViaSnep Fechando evento "+cd_historico);
					}
		
					sqlUpdateStatus.append(" WHERE CD_HISTORICO = ? ");
					stUpdateStatus = conn.prepareStatement(sqlUpdateStatus.toString());
					stUpdateStatus.setString(1, cd_historico);
					count = stUpdateStatus.executeUpdate();
					stUpdateStatus.close();
					if(!realizouInterna){
						//nao realizou a vistoria interna
						out.print("{\"id\":\""+cd_viatura+"\", \"return\":\"15\"}");
						return;
					}
				 }else{
					String sqlAux = "";

					if (respostaEventoAgrupado != null) {
						String arrayMensagem[] = respostaEventoAgrupado.split(",");

						if (arrayMensagem[0] != null && arrayMensagem[1] != null && arrayMensagem[2] != null) {
						sqlAux = " ,TX_OBSERVACAO_FECHAMENTO = 'Voc� realizou a vistoria do par�metro completo? \r\n R:" + (arrayMensagem[0].equals("1") ? "Sim" : "N�o")
							+ "' + \r\n (CHAR(10) + CHAR(13)) + 'Voc� confirma que n�o h� ind�cios de viola��o no patrim�nio? \r\n R:" + (arrayMensagem[1].equals("1") ? "Sim" : "N�oo")
							+ "'  + \r\n (CHAR(10) + CHAR(13)) + 'O cliente acompanhou a vistoria? \r\n R:" + (arrayMensagem[2].equals("1") ? "Sim" : "N�o")
							+ "' \r\n +(CHAR(10) + CHAR(13))+ TX_OBSERVACAO_FECHAMENTO ";
						}
					}

					sqlUpdateStatus = new StringBuffer();
					sqlUpdateStatus.append(" UPDATE HISTORICO ");
					sqlUpdateStatus.append(" SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE(), CD_USUARIO_FECHAMENTO = 9999, CD_MOTIVO_ALARME = 48 ");

					if (!sqlAux.isEmpty()) {
						sqlUpdateStatus.append(sqlAux);
						System.out.println("entradaAAViaSnep Fechando evento "+cd_historico);
					}
		
					sqlUpdateStatus.append(" WHERE CD_HISTORICO = ? ");
					stUpdateStatus = conn.prepareStatement(sqlUpdateStatus.toString());
					stUpdateStatus.setString(1, cd_historico);
					count = stUpdateStatus.executeUpdate();
					stUpdateStatus.close();
					System.out.println("entradaAAViaSnep Fechou evento com sucesso"+cd_historico);
					if (isDebug) {
					    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Atualizado (" + count + ") Status do Evento (" + cd_historico + ") para Fechado[" + aplicacao + "]");
					}
					if (cd_viatura != null){
						sqlUpdateStatus = new StringBuffer();
						sqlUpdateStatus.append(" UPDATE HISTORICO SET DT_VIATURA_DESLOCAMENTO = GETDATE() WHERE CD_HISTORICO in (SELECT H.CD_HISTORICO FROM HISTORICO H ");
						sqlUpdateStatus.append(" INNER JOIN VIATURA V ON V.CD_VIATURA = H.CD_VIATURA ");
						sqlUpdateStatus.append(" where FG_STATUS = 2 and v.CD_VIATURA = ? and H.CD_HISTORICO <> ?) ");
			
					    stUpdateStatus = conn.prepareStatement(sqlUpdateStatus.toString());
						stUpdateStatus.setString(1, cd_viatura);
						stUpdateStatus.setString(2, cd_historico);
						count = stUpdateStatus.executeUpdate();
						stUpdateStatus.close();
						System.out.println("entradaAAViaSnep Atualizou a data de deslocamento com sucesso "+cd_historico);
						if (isDebug) {
						    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Atualizado s data de deslocamento (" + count + ") Status do Evento (" + cd_historico + ") para Fechado[" + aplicacao + "]");
						}
						System.out.println("entradaAAviaSNEP dados recebidos na finalizacao, viatura:"+cd_viatura+" - placa:"+cd_placa);
				    }
				}
		    }
		    if (acao.equals("chgvtr")) {
			/*
			 *
			 * 1. Libera Viatura Atual no Fusion
			 *
			 */
			if ((placa_atual != null) && (cd_placa != null) && (!placa_atual.contains(cd_placa))) {
				QLGroupFilter filterVtrRegional = new QLGroupFilter("AND");
				filterVtrRegional.addFilter(new QLOpFilter("placa", "LIKE" , "%"+cd_placa));
				filterVtrRegional.addFilter(new QLEqualsFilter("codigoRegional", regional));
				filterVtrRegional.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
				NeoObject viaturaAtualObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), filterVtrRegional);
			    if (viaturaAtualObj != null) {
				ewViaturaAtual = new EntityWrapper(viaturaAtualObj);
				insertMove(viaturaAtualObj, cd_cliente, fantasia, 4L);

				// Limpa rastreador SEGWARE MOBILE
				if ((ewViaturaNova.findValue("rastreador.modeloEquipamento.nomeModelo") != null) && (ewViaturaNova.findValue("rastreador.modeloEquipamento.nomeModelo").toString().contains("SEGWARE"))) {
				    ewViaturaNova.setValue("rastreador", null);
				}
				ewViaturaAtual.setValue("emUso", false);
				ewViaturaAtual.setValue("rotaEfetiva", null);
				ewViaturaAtual.setValue("listaRotasEfetivas", null);
				ewViaturaAtual.setValue("atrasoDeslocamento", false);
				ewViaturaAtual.setValue("titulo", "Liberada em " + NeoUtils.safeDateFormat(new GregorianCalendar()));
				ewViaturaAtual.setValue("motorista", fantasia);
				ewViaturaAtual.setValue("dataMovimentacao", new GregorianCalendar());
				ewViaturaAtual.setValue("telefone", fone);
				//ewViaturaAtual.setValue("codigoRegional", fantasia.substring(0,3));
				ewViaturaAtual.setValue("codigoDoTecnico", "");
				    
				String codigoViatura = ewViaturaAtual.findField("codigoViatura").getValueAsString();
				    
			    if (codigoViatura != null && !codigoViatura.isEmpty()){
					List<NeoObject> eventosList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), new QLEqualsFilter("cdViaturaSigma", codigoViatura));
					
					if (eventosList != null && !eventosList.isEmpty()){
					    for (NeoObject o : eventosList){
							PersistEngine.remove(o);
					    }
					}
					
			    }
			    
				if (isDebug) {
				    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Liberada VTR em uso no Fusion:" + placa_atual + " [" + aplicacao + "]");
				}
			    }
			}
		    }

		    if (acao.equals("login") && (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC"))) {
			/*
			 *
			 * Registra a entrada no livro ata do SIGMA
			 *
			 */
			StringBuffer sqlInsertEntrada = new StringBuffer();
			sqlInsertEntrada.append(" INSERT INTO LIVRO_ATA_COLABORADOR (CD_LIVRO_ATA, CD_COLABORADOR, DT_ENTRADA_PLANTAO, DT_SAIDA_PLANTAO, CD_USUARIO, DT_OCORRENCIA) ");
			sqlInsertEntrada.append(" VALUES (?, ?, GETDATE(), NULL, 11010, GETDATE()) ");
			PreparedStatement stInsertEntrada = conn.prepareStatement(sqlInsertEntrada.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			stInsertEntrada.setLong(1, cd_livro_ata);
			stInsertEntrada.setString(2, cd_colaborador);
			int count = stInsertEntrada.executeUpdate();
			ResultSet generatedKeys = stInsertEntrada.getGeneratedKeys();
			Long cd_livro_ata_colaborador = 0L;
			if (generatedKeys.next()) {
			    cd_livro_ata_colaborador = generatedKeys.getLong(1);
			} else {
				throw new SQLException("Erro registrando entrada. N�o foi poss�vel obter a chave do registro do colaborador no livro ata.");
			}
			stInsertEntrada.close();
			generatedKeys.close();
			if (isDebug) {
			    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Entrada registrada no Livro Ata [" + aplicacao + "]");
			}

			/*
			 *
			 * Registra a associacao do colaborador com VTR no livro ata do SIGMA
			 *
			 */
			StringBuffer sqlInsertAssociacao = new StringBuffer();
			sqlInsertAssociacao
				.append(" INSERT INTO LIVRO_ATA_FROTA (CD_LIVRO_ATA, CD_LIVRO_ATA_COLABORADOR, CD_VIATURA, DT_INICIO_USO_VIATURA, NU_KM_INICIAL, DT_FIM_USO_VIATURA, NU_KM_FINAL, CD_USUARIO, DT_OCORRENCIA, FG_ERRO_KM_INICIAL)  ");
			sqlInsertAssociacao.append(" VALUES (?, ?, ?, GETDATE(), 0, NULL, 0, 11010, GETDATE(), 0) ");
			PreparedStatement stInsertAssociacao = conn.prepareStatement(sqlInsertAssociacao.toString());
			stInsertAssociacao.setLong(1, cd_livro_ata);
			stInsertAssociacao.setLong(2, cd_livro_ata_colaborador);
			stInsertAssociacao.setString(3, cd_viatura);
			count = stInsertAssociacao.executeUpdate();
			stInsertAssociacao.close();
			if (isDebug) {
			    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Entrada registrada no Livro Ata [" + aplicacao + "]");
			}
		    }

		    if (acao.equals("logoff") && (aplicacao.equals("TAT") || aplicacao.equals("TSH") || aplicacao.equals("TSA") || aplicacao.equals("PLC"))) {
			/*
			 *
			 * Recupera codigo Colaborador SIGMA (VTR Mobile)
			 * 
			 */
			StringBuffer sqlColSigma = new StringBuffer();
			sqlColSigma.append(" SELECT lac.CD_COLABORADOR ");
			sqlColSigma.append(" FROM LIVRO_ATA_COLABORADOR lac WITH (NOLOCK) ");
			sqlColSigma.append(" INNER JOIN LIVRO_ATA_FROTA laf WITH (NOLOCK) ON lac.CD_LIVRO_ATA_COLABORADOR = laf.CD_LIVRO_ATA_COLABORADOR ");
			sqlColSigma.append(" WHERE lac.DT_SAIDA_PLANTAO IS NULL ");
			sqlColSigma.append(" AND laf.DT_FIM_USO_VIATURA IS NULL ");
			sqlColSigma.append(" AND lac.CD_LIVRO_ATA = " + cd_livro_ata);
			sqlColSigma.append(" AND laf.CD_VIATURA = " + cd_viatura);
			stColSigma = conn.prepareStatement(sqlColSigma.toString());
			ResultSet rsColSigma = stColSigma.executeQuery();
			if (rsColSigma.next()) {
			    cd_colaborador = rsColSigma.getString("CD_COLABORADOR");
			}
			rsColSigma.close();
			stColSigma.close();

			/*
			 *
			 * Desfaz a associacao do COLABORADOR com VTR no livro ata do SIGMA
			 *
			 */
			StringBuffer sqlUpdateAssociacao = new StringBuffer();
			sqlUpdateAssociacao.append(" UPDATE LIVRO_ATA_FROTA ");
			sqlUpdateAssociacao.append(" SET LIVRO_ATA_FROTA.DT_FIM_USO_VIATURA = GETDATE() ");
			sqlUpdateAssociacao.append(" FROM LIVRO_ATA_COLABORADOR ");
			sqlUpdateAssociacao.append(" WHERE LIVRO_ATA_FROTA.CD_LIVRO_ATA = ? ");
			sqlUpdateAssociacao.append(" AND LIVRO_ATA_COLABORADOR.CD_LIVRO_ATA_COLABORADOR = LIVRO_ATA_FROTA.CD_LIVRO_ATA_COLABORADOR ");
			sqlUpdateAssociacao.append(" AND LIVRO_ATA_COLABORADOR.DT_SAIDA_PLANTAO IS NULL  ");
			sqlUpdateAssociacao.append(" AND LIVRO_ATA_FROTA.DT_FIM_USO_VIATURA IS NULL ");
			sqlUpdateAssociacao.append(" AND CD_COLABORADOR = ? ");
			PreparedStatement stUpdateAssociacao = conn.prepareStatement(sqlUpdateAssociacao.toString());
			stUpdateAssociacao.setLong(1, cd_livro_ata);
			stUpdateAssociacao.setString(2, cd_colaborador);
			int count = stUpdateAssociacao.executeUpdate();
			stUpdateAssociacao.close();
			if (isDebug) {
			    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Viatura liberada no Livro Ata [" + aplicacao + "]");
			}

			/*
			 *
			 * Registra a SAIDA da VTR no livro ata do SIGMA
			 *
			 */
			StringBuffer sqlUpdateSaida = new StringBuffer();
			sqlUpdateSaida.append(" UPDATE LIVRO_ATA_COLABORADOR ");
			sqlUpdateSaida.append(" SET DT_SAIDA_PLANTAO = GETDATE() ");
			sqlUpdateSaida.append(" WHERE CD_LIVRO_ATA = ? ");
			sqlUpdateSaida.append(" AND DT_SAIDA_PLANTAO IS NULL  ");
			sqlUpdateSaida.append(" AND CD_COLABORADOR = ? ");
			PreparedStatement stUpdateSaida = conn.prepareStatement(sqlUpdateSaida.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			stUpdateSaida.setLong(1, cd_livro_ata);
			stUpdateSaida.setString(2, cd_colaborador);
			count = stUpdateSaida.executeUpdate();
			stUpdateSaida.close();
			if (isDebug) {
			    log.warn("##### AA(" + acao + ") [" + cd_cliente + "] via " + origem + "> Entrada registrada no Livro Ata [" + aplicacao + "]");
			}
			if (cd_viatura != null && !cd_viatura.isEmpty())
			    verificaFilaEvento(cd_viatura);
		    }
		}

		// entrada / troca / saida com sucesso
		if (isVerboseError) {
		    out.print("OK");
		} else {
		    out.print("{\"id\":\"" + cd_cliente + "\", \"return\":\"OK\"}");
		}

		rsCentral.close();
		stCentral.close();

    } catch (Exception e) {
		e.printStackTrace();
		System.out.println("entradaAAViaSNEP 99"+cd_historico);
		// erro desconhecido
		if (isVerboseError) {
		    out.print("99-Erro desconhecido");
		} else {
		    out.print("{\"id\":\"" + cd_cliente + "\", \"return\":\"99\"}");
		}
		return;
    } finally {
		try {
		    conn.close();
		} catch (SQLException e) {
		    e.printStackTrace();
		    out.print("Erro #6 - Erro ao finalizar conex�o");
		    return;
		}
    }
%>
