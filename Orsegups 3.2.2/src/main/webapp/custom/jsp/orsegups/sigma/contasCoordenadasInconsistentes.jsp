<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
 
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.net.URLEncoder"%>


<style type="text/css">
.cod_preta {
  background-color: #ffffff;
  color: #000000;
}
.cor_os_verde_execucao {
  background-color: #ffffff;
  color: #008000;
}
.cor_os_amarela_pausada{
  background-color: #ffffff;
  color: #ff9a00;
}
.cor_os_cinza_fechada {
  background-color: #ffffff;
  color: #959595;
}

</style>
 <!-- <meta http-equiv="refresh" content="180"/>-->
<%		
	String regional = request.getParameter("regional");
	if(regional == null || regional.trim().equals(""))	{
		regional = "";
 	}
	String cd_tecnico = request.getParameter("cd_tecnico");
	if(cd_tecnico == null || cd_tecnico.trim().equals(""))	{
		cd_tecnico = "";
 	}

	String checkedFechadas = "";
	String checkedAbertas = "";
	String checkedSomenteFechadas = "";
	String checkedAlertas = "";

	boolean booleanTerceiro = false;
	String mostrarTerceiro = request.getParameter("terceiro");
	if(mostrarTerceiro != null && mostrarTerceiro.trim().equals("sim"))	{
		booleanTerceiro = true;
 	} 


	boolean booleanMostrarFechadas = false;
	String mostrarFechadas = request.getParameter("controle");
	if(mostrarFechadas != null && mostrarFechadas.trim().equals("on"))	{
		booleanMostrarFechadas = true;
		checkedFechadas = " checked ";
 	} 

	boolean booleanMostrarAbertas = false;
	String mostrarAbertas = request.getParameter("controle");
	if(mostrarAbertas != null && mostrarAbertas.trim().equals("on"))	{
		booleanMostrarAbertas = true;
		checkedAbertas = " checked ";
 	}	

	boolean booleanMostrarAlertas = false;
	String mostrarAlertas = request.getParameter("controle");
	if(mostrarAlertas != null && mostrarAlertas.trim().equals("on"))	{
		booleanMostrarAlertas = true;
		checkedAlertas = " checked ";
 	}
	boolean booleanSomenteFechadas = false;
	String somenteFechadas = request.getParameter("controle");
	if(somenteFechadas != null && somenteFechadas.trim().equals("on"))	{
		booleanSomenteFechadas = true;
		checkedSomenteFechadas = " checked ";
 	}
	

	String controle = request.getParameter("controle");
	//System.out.println(controle);
	if (controle == null)	{
		controle = "P";
 	} 
	if (controle.equals("T"))	{
		booleanMostrarFechadas = true;
		booleanMostrarAbertas = true;
		booleanSomenteFechadas = false;
		booleanMostrarAlertas = true;
		checkedFechadas	= " checked ";	
 	} 
	if (controle.equals("P"))	{
		booleanMostrarFechadas = false;
		booleanMostrarAbertas = true;
		booleanMostrarAlertas = true;
		checkedAbertas	= " checked ";	
 	} 
	if (controle.equals("A"))	{
		booleanMostrarFechadas = false;
		booleanMostrarAbertas = false;
		booleanMostrarAlertas = true;
		checkedAlertas	= " checked ";	
 	} 
	if (controle.equals("F"))	{
		booleanMostrarFechadas = false;
		booleanMostrarAbertas = false;
		booleanMostrarAlertas = false;
		booleanSomenteFechadas = true;
		checkedSomenteFechadas	= " checked ";	
 	} 

%>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
	
<portal:head title="Contas com Coordenadas Inconsistentes">
<cw:main>
	<cw:header title="Contas com Coordenadas Inconsistentes" />
	<cw:body id="area_scroll">
		<!-- 
		<form name="formControles" action="/fusion/custom/jsp/orsegups/sigmaAtendimentoOS2.jsp?regional=<%=regional%>" method="POST">
			<input onClick="document.formControles.submit();" type="button" value="Atualizar"/>
			<input onClick="document.formControles.submit();" type="radio" <%= checkedFechadas %> name="controle" id="mostrarFechadas" value="T"/> <label for="mostrarFechadas">Todas</label>
			<input onClick="document.formControles.submit();" type="radio" <%= checkedSomenteFechadas %> name="controle" id="somenteFechadas" value="F"/> <label for="somenteFechadas">Fechadas</label>
			<input onClick="document.formControles.submit();" type="radio" <%= checkedAbertas %> name="controle" id="mostrarAbertas" value="P"/> <label for="mostrarAbertas">Abertas</label>
			<input onClick="document.formControles.submit();" type="radio" <%= checkedAlertas %> name="controle" id="mostrarAlertas" value="A"/> <label for="mostrarAlertas">Somente Alertas</label>
		</form>
		-->
	<input onClick="javascript:document.location.reload();" type="button" value="Atualizar"/></a>
	<input onClick="normalizarCoordenadasCentrais();" type="button" value="Atualizar todas as coordenadas poss�veis"/></a>

	<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
	<thead style="cursor: auto">
		<tr style="cursor: auto">
			<th style="cursor: auto">C�d.Cliente</th>
			<th style="cursor: auto">Empresa</th>
			<th style="cursor: auto">Conta [Parti��o]</th>
			<th style="cursor: auto">Latitude</th>
			<th style="cursor: auto">Longitude</th>
			<th style="cursor: auto">Status</th>
			<th style="cursor: auto">Endere�o</th>
			<th style="cursor: auto">Bairro</th>
			<th style="cursor: auto">Cidade</th>
			<th style="cursor: auto">UF</th>
			<th style="cursor: auto">Cliente</th>
		</tr>
	</thead>
	<tbody>	
	
<%		
    final String none = "&nbsp;";

    PreparedStatement st = null;
    PreparedStatement st2 = null;
    PreparedStatement st3 = null;
    try {
		String nomeFonteDados = "SIGMA90";
		Connection conn = PersistEngine.getConnection(nomeFonteDados);
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH':'mm");
		Date dataCadastro = null;
		int qtdContas = 0;
	
		StringBuffer sql1 = new StringBuffer();
		sql1.append(" SELECT CASE  ");
		sql1.append(" 		WHEN (c.NU_LATITUDE IS NULL OR c.NU_LONGITUDE IS NULL) THEN 1  ");
// 		sql1.append(" 		WHEN (c.NU_LATITUDE > -22.421185 OR c.NU_LATITUDE < -29.3986111) THEN 2"); 
// 		sql1.append(" 		WHEN (c.NU_LONGITUDE > -48.3269444 OR c.NU_LONGITUDE < -54.405945) THEN 3             ");
// 		sql1.append(" 		WHEN (cid.NU_RAIO * cid.NU_MARGEM < dbo.LatLonRadiusDistance(c.NU_LATITUDE, c.NU_LONGITUDE, cid.USU_NU_LATITUDE, cid.USU_NU_LONGITUDE)*1.6) THEN 4    ");
		sql1.append(" 	   END AS COD_INC,		                                                                                                                                             ");
		sql1.append(" 		 c.NU_LATITUDE, c.NU_LONGITUDE, cid.USU_NU_LATITUDE AS LATCID, cid.USU_NU_LONGITUDE AS LNGCID, cid.NU_RAIO * cid.NU_MARGEM AS RANGE, dbo.LatLonRadiusDistance(c.NU_LATITUDE, c.NU_LONGITUDE, cid.USU_NU_LATITUDE, cid.USU_NU_LONGITUDE)*1.6 AS DISTANCE,  ");
		sql1.append(" 		 c.DT_CADASTRO_CENTRAL, CD_CLIENTE, ID_CENTRAL, ID_EMPRESA, PARTICAO, RAZAO, FANTASIA, ENDERECO, REFERENCIA, bai.NOME AS NOMEBAIRRO, cid.NOME AS NOMECIDADE, cid.ID_ESTADO,    ");
		sql1.append(" CASE WHEN ID_EMPRESA IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140) THEN 'V' ELSE 'O' END AS EMP  ");
		sql1.append(" FROM dbCENTRAL c WITH (NOLOCK)                                                                                       ");
		sql1.append(" LEFT OUTER JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE                                                          ");
		sql1.append(" LEFT OUTER JOIN dbBAIRRO bai ON c.ID_BAIRRO = bai.ID_BAIRRO AND c.ID_CIDADE = bai.ID_CIDADE ");
		sql1.append(" WHERE c.CTRL_CENTRAL = 1 													    ");
		//sql1.append(" AND c.CD_GRUPO_CLIENTE <> 56 													    ");
		//sql1.append(" AND c.ID_RAMO NOT IN (10058, 10059, 10060, 10061)													    ");
		sql1.append(" AND c.ID_RAMO <> 10191 ");
		sql1.append(" AND (     														    ");
		//sql1.append(" 		cid.NU_RAIO * cid.NU_MARGEM < dbo.LatLonRadiusDistance(c.NU_LATITUDE, c.NU_LONGITUDE, cid.USU_NU_LATITUDE, cid.USU_NU_LONGITUDE)*1.6    ");
		sql1.append(" 		c.NU_LATITUDE IS NULL           ");
		sql1.append(" 		OR c.NU_LONGITUDE IS NULL   	 ");
		sql1.append(" 	) ");
		sql1.append("  AND c.ID_EMPRESA NOT IN (10127,10075,10129,10144) ");
		sql1.append(" ORDER BY cid.NOME																			");
		//out.println(sql1.toString());	
		
		st = conn.prepareStatement(sql1.toString());
		ResultSet rs = st.executeQuery();
						
		while(rs.next()) {
		
			String cd_cliente = rs.getString("CD_CLIENTE");
			String cd_empresa = rs.getString("ID_EMPRESA");
			String id_central = rs.getString("ID_CENTRAL");
			String particao = rs.getString("PARTICAO");
			dataCadastro = rs.getDate("DT_CADASTRO_CENTRAL");
			String endereco = rs.getString("ENDERECO");
			String nomebairro = rs.getString("NOMEBAIRRO");
			String nomecidade = rs.getString("NOMECIDADE");
			String uf = rs.getString("ID_ESTADO");
			String sLat = rs.getString("NU_LATITUDE");
			String sLng = rs.getString("NU_LONGITUDE");
			double lat = rs.getDouble("NU_LATITUDE");
			double lng = rs.getDouble("NU_LONGITUDE");
			String emp = rs.getString("EMP");
			
			int codInc = rs.getInt("COD_INC");
			// Determinando a inconsist�ncia
			String problem = "";
			if (codInc == 1) {
				problem = "N�o mapeado";
			} else if( codInc == 2 || codInc == 3) {
				problem = "<a href=javascript:openMaps('"+sLat+","+ sLng+"');><img src='imagens/custom/pin-blue1.png' border='0' alt='Ver no Google Maps pela Coordenada Cadastrada'/> Fora de SC/PR</a>";
			} else if( codInc == 4 ) {
				problem = "<a href=javascript:openMaps('"+sLat+","+ sLng+"');><img src='imagens/custom/pin.png' border='0' alt='Ver no Google Maps pela Coordenada Cadastrada'/> Fora de "+nomecidade+"</a>";
			} else {
				problem = "Erro";
			}

			if (emp.equals("V")){
			    out.print("<tr style=\"background-color: lightblue;\">");			    
			}else{
			    out.print("<tr>");
			}
			
			out.print("<td style='font-weight: normal'>"+ cd_cliente +"</td>");
			out.print("<td>" + cd_empresa + "</td>");			
			out.print("<td>" + rs.getString("ID_CENTRAL") + " [" + rs.getString("PARTICAO") + "]</td>");			
			out.print("<td>"+ sLat + "</td>");			
			out.print("<td>"+ sLng + "</td>");	
			out.print("<td>"+ problem + "</td>");
			
			if (endereco != null){
			    
			endereco = endereco.replaceAll("[�����]", "a")   
	                .replaceAll("[����]", "e")   
	                .replaceAll("[����]", "i")   
	                .replaceAll("[�����]", "o")   
	                .replaceAll("[����]", "u")   
	                .replaceAll("[�����]", "A")   
	                .replaceAll("[����]", "E")   
	                .replaceAll("[����]", "I")   
	                .replaceAll("[�����]", "O")   
	                .replaceAll("[����]", "U")   
	                .replace('�', 'c')   
	                .replace('�', 'C')   
	                .replace('�', 'n')   
	                .replace('�', 'N')
	                .replaceAll("!", "")	                
	                .replaceAll ("\\[\\�\\`\\?!\\@\\#\\$\\%\\�\\*"," ")
	                .replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]"," ")
	                .replaceAll("[\\.\\;\\-\\_\\+\\'\\�\\�\\:\\;\\/]"," ");
			}
			
			if (nomecidade != null){
				nomecidade = nomecidade.replaceAll("[�����]", "a")   
			                .replaceAll("[����]", "e")   
			                .replaceAll("[����]", "i")   
			                .replaceAll("[�����]", "o")   
			                .replaceAll("[����]", "u")   
			                .replaceAll("[�����]", "A")   
			                .replaceAll("[����]", "E")   
			                .replaceAll("[����]", "I")   
			                .replaceAll("[�����]", "O")   
			                .replaceAll("[����]", "U")   
			                .replace('�', 'c')   
			                .replace('�', 'C')   
			                .replace('�', 'n')   
			                .replace('�', 'N')
			                .replaceAll("!", "")	                
			                .replaceAll ("\\[\\�\\`\\?!\\@\\#\\$\\%\\�\\*"," ")
			                .replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]"," ")
			                .replaceAll("[\\.\\;\\-\\_\\+\\'\\�\\�\\:\\;\\/]"," ");
			}
			
			if (uf != null){
			    
				uf = uf.replaceAll("[�����]", "a")   
			                .replaceAll("[����]", "e")   
			                .replaceAll("[����]", "i")   
			                .replaceAll("[�����]", "o")   
			                .replaceAll("[����]", "u")   
			                .replaceAll("[�����]", "A")   
			                .replaceAll("[����]", "E")   
			                .replaceAll("[����]", "I")   
			                .replaceAll("[�����]", "O")   
			                .replaceAll("[����]", "U")   
			                .replace('�', 'c')   
			                .replace('�', 'C')   
			                .replace('�', 'n')   
			                .replace('�', 'N')
			                .replaceAll("!", "")	                
			                .replaceAll ("\\[\\�\\`\\?!\\@\\#\\$\\%\\�\\*"," ")
			                .replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]"," ")
			                .replaceAll("[\\.\\;\\-\\_\\+\\'\\�\\�\\:\\;\\/]"," ");		
			}
			
			
			String encodedMapURL = URLEncoder.encode(endereco + "," + nomecidade + "," + uf,"ISO8859-1");
			
			out.print("<td><a href=javascript:openMaps('"+encodedMapURL+"');><img src='imagens/custom/red-pin.png' border='0' alt='Ver no Google Maps pelo Endere�o'/></a>&nbsp;");
			out.print("<a href='javascript:updateLatLng("+cd_cliente+")';><img src='imagens/icones_final/process_open_16x16-trans.png' border='0' alt='Atualizar Coordenada'/></a>");
			out.print("&nbsp;" + endereco + "</td>");
			out.print("<td> " + nomebairro + "</td>");
			out.print("<td> " + nomecidade + "</td>");
			out.print("<td> " + uf + "</td>");
			out.print("<td>"+ rs.getString("FANTASIA") + " ( " + rs.getString("RAZAO") +" )</td>");			
			out.print("</tr>");
			qtdContas++;
		}
		out.print("<tr>");
		out.print("<td style='font-weight: normal' colspan='1'>&nbsp;</td>");
		out.print("<td style='font-weight: bold' colspan='3'> Total: "+ qtdContas +"</td>");					
		out.print("<td style='font-weight: normal' colspan='6'>&nbsp;</td>");
		out.print("</tr>");
		
		rs.close();
		st.close();		
	} catch (Exception e) {
		e.printStackTrace();	
	}
%>
	</tbody>
	</table>
	<input onClick="javascript:document.location.reload();" type="button" value="Atualizar"/></a>
	
	<br>

	
	</cw:body>
</cw:main>
</portal:head>


<script language="JavaScript">

function openMaps(param) {
	window.open('http://maps.google.com.br/maps?q=' + param,'','toolbar=yes,scrollbars=yes,width=1000,height=500')
}



function updateLatLng(cd_cliente) {
	var response = callSync('<%= PortalUtil.getBaseURL() %>custom/jsp/orsegups/sigma/atualizaCoordenadas.jsp?cd_cliente=' + cd_cliente);

	if (response == "OK") {
		alert("Coordenadas atualizadas com sucesso");
	} else {
		alert("N�o foi poss�vel atualizar as coordenadas. " + response);
	}
	
}


function normalizarCoordenadasCentrais(){
	
	alert('Processo iniciado! A finaliza��o pode levar mais de uma hora em fun��o da quantidade de registros!');
	
    $.ajax({url: 'https://intranet.orsegups.com.br/fusion/services/SIGMAUtils/normalizarCoordenadasCentrais', success: function(result){
        console.log(result);
    }});
}


</script>