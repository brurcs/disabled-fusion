<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>

<portlet:defineObjects />
<form:defineObjects />

<portal:head title="Relat�rio de Arrombamentos Confirmados">
<cw:main>
	<cw:header title="Relat�rio de Arrombamentos Confirmados" />
	
	<cw:body id="area_scroll">

				
   		<div id="dados">
		<fieldset class="fieldGroup">
		<legend class="legend" hasbox="2">Filtros</legend>
		<OL hasbox="2">
		<div class="fieldSet" hasbox="2">
		<LI>
			<label class="formFieldLabel">Regional:&nbsp;</label>&nbsp;
			<input type="text"class="input_text" name='txt_regional' id='idTxtRegional' required='false' /> 
			<input id='idRegional' name='idRegional' type='hidden'/>
			<script type="text/javascript">
				createAutoComplete(myId('idRegional'), 'idTxtRegional', 'GCEscritorioRegional', 'nome;siglaRegional', null);
			</script>
        </LI>
		<LI>
		<label class="formFieldLabel">Data Inicial:&nbsp;</label>&nbsp;
	         <input class="input_text" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)"name= 'var_dataInicial' errorSpan='err_var_dataInicial' id='var_dataInicial' required='false' onchange=''/><span id='err_var_dataInicial' style='margin-left:0px;'></span>&nbsp;<img src='imagens/icones_final/eraser_16x16-trans.png' class='ico' onclick="javascript: limpaCampo('var_dataInicial');">&nbsp;<img src='imagens/icones_final/date_16x16-trans.png' class='ico' id='btdataInicial' name='btdataInicial'  onclick="return showNeoCalendar('var_dataInicial', '%d/%m/%Y','btdataInicial', event);">
		     <br>
		     <span class="fieldHelp" >Utilize o formato DD/MM/YYYY</span>
		</LI>
		<LI>
		   <label class="formFieldLabel">Data Final:&nbsp;</label>&nbsp;
	         <input class="input_text" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)"name= 'var_dataFinal' errorSpan='err_var_dataFinal' id='var_dataFinal' required='false' onchange=''/><span id='err_var_dataFinal' style='margin-left:0px;'></span>&nbsp;<img src='imagens/icones_final/eraser_16x16-trans.png' class='ico' onclick="javascript: limpaCampo('var_dataFinal');">&nbsp;<img src='imagens/icones_final/date_16x16-trans.png' class='ico' id='btdataFinal' name='btdataFinal'  onclick="return showNeoCalendar('var_dataFinal', '%d/%m/%Y','btdataFinal', event);">
		     <br>
		     <span class="fieldHelp" >Utilize o formato DD/MM/YYYY</span>
		</LI>
		
		<LI>
			<label class="formFieldLabel">C�igo do Motivo:&nbsp;</label>&nbsp;
			<input type="text"class="input_text" name='txt_motivo' id='idTxtMotivo' required='false' />
			 
        </LI>
		
		<div align="center">
			<LI>
				<input type="button"class="input_button"id="botaoGerarRelatorio"value="Gerar Relat�rio">
			</LI>
		</div>
		</div>
	 	</OL>
		</fieldset>
	
	<script type="text/javascript">
		$(document).ready(function() 
			{
		 		$('#botaoGerarRelatorio').click(function() 
		   	 	{
		 			var regional = $('#idTxtRegional').val();
		 			var dataInicial = $('#var_dataInicial').val();
			    	var dataFinal  = $('#var_dataFinal').val();
			    	var motivo = $('#idTxtMotivo').val();
			    	
			   		  if(dataInicial == null || dataInicial == '' ){
			   			  alert('O campo Data Inicial: n�o foi preenchido.');
			   			  document.getElementById('var_dataInicial').focus();
			   			  return;
			   		  }
			   		  
			   		 if(dataFinal == null || dataFinal == ''){
			   			  alert('O campo Data Final: n�o foi preenchido.');
			   			  document.getElementById('var_dataFinal').focus();
			   			  return;
			   		  }
			    	
		    	  	var servlet = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/sigma/servletRelatorioArrombamentos.jsp?regional='+regional+'&dataInicial='+dataInicial+'&dataFinal='+dataFinal+"&motivo="+motivo;
		    	  	 window.open(servlet,'Relat�rio','status=0, resizable=0, scrollbars=NO, width=100, height=75', 'download');
				}
		 		);
			}
		);

	function limpaCampo(idCampo)
	{
		$('#'+idCampo).val("");
		$('#'+idCampo).removeAttr("selectedvalue");
	}
	</script>

	</cw:body>
	
</cw:main>
</portal:head>