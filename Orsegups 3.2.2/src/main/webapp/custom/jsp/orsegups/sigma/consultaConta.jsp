<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>

<%	

	Long cd_cliente = Long.valueOf(request.getParameter("cd_cliente"));
	String cgccpf = request.getParameter("cgccpf");
	
	if (NeoUtils.safeIsNull(cgccpf)) 
	{
		out.print("ERRO - CPF/CNPJ do cliente Sapiens n�o preenchido!");
		return;
	}
	
	Connection connSapiens = PersistEngine.getConnection("SIGMA90");

	try
	{
		StringBuffer sqlSigma = new StringBuffer();
		sqlSigma.append(" SELECT CGCCPF, ID_EMPRESA, ID_CENTRAL, PARTICAO FROM DBCENTRAL WHERE CD_CLIENTE = ? ");

		PreparedStatement stSigma = connSapiens.prepareStatement(sqlSigma.toString());
		stSigma.setLong(1, cd_cliente);
		ResultSet rsSigma = stSigma.executeQuery();

		//Cargos
		if (rsSigma.next())
		{
			String cpfSigma = rsSigma.getString("CGCCPF");
			String idCentral = rsSigma.getString("ID_CENTRAL");
			
			if (!idCentral.equals("AAA5") && !idCentral.equals("AAA6") && NeoUtils.safeIsNull(cpfSigma)) 
			{
				out.print("ERRO - CPF/CNPJ do cliente Sigma n�o preenchido!");
				return;
			}
			
			if (cpfSigma != null && cgccpf != null && !cpfSigma.equals("") && !cgccpf.equals("") && cpfSigma.equals(cgccpf)) 
			{
				String codigoIdCentral = rsSigma.getString("ID_CENTRAL");
				Long codigoEmpresa = rsSigma.getLong("ID_EMPRESA");
				String codigoParticao = rsSigma.getString("PARTICAO");
						
				String sURL = "conta:" + codigoIdCentral +  ";empresa:" + Long.valueOf(codigoEmpresa) +";particao:" + codigoParticao;
				out.print(sURL);
				return;
			} 
			else 
			{
				out.print("ERRO - CPF/CNPJ do cliente Sapiens diferente do SIGMA!");
				return;
			}
		}
	 	else 
	 	{
			out.print("ERRO - Conta "+cd_cliente+" inexistente no SIGMA!");
			return;
		}
	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		try
		{
			connSapiens.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}	
%>
