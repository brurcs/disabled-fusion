<%@page import="java.util.TreeMap"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.util.SortedMap"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.TaticoConta"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>


<%	
	
	final NeoUser currentUser = PortalUtil.getCurrentUser();
	final boolean isAdm = (currentUser!=null&&currentUser.isAdm())?true:false;
	final String none = "&nbsp;";

	DecimalFormat f = new DecimalFormat("000000000");

	DecimalFormat formatoReal = new DecimalFormat("#,##0.00");
	List<String> listReg = new ArrayList<String>();

	listReg.add("BNU");
	listReg.add("BQE");
	listReg.add("CCO");
	listReg.add("CSC");
	listReg.add("CTA");
	listReg.add("CUA");
	listReg.add("IAI");
	listReg.add("JLE");
	listReg.add("JGS");
	listReg.add("LGS");
	listReg.add("RSL");
	listReg.add("SOO");
	listReg.add("TRO");
	listReg.add("GNA");
	listReg.add("CAS");
	listReg.add("PMJ");
	listReg.add("NHO");
	listReg.add("PAE");
	listReg.add("SRR");
	listReg.add("TRI");
	listReg.add("XLN");
	listReg.add("REGIONAL INDEFINIDA");
	
%>

<script language="javascript" type="text/javascript">
        function OpenPopupCenter(pageURL, title, w, h) {
            var left = (screen.width - w) / 2;
            var top = (screen.height - h) / 4;  /* for 25% - devide by 4  |  for 33% - devide by 3 */
            var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        } 
 </script>

<portal:head title="Contas x T�tico">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css">
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/overlib/overlib.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<cw:main>
		<cw:header title="Contas x T�tico" />
		<cw:body id="area_scroll">
		
			<%
				Connection conn = null;
				PreparedStatement pstm = null;
				ResultSet rs = null;
				String extraStyle = "style='background-color: #FFEEDD;'";
				try
				{

						conn = PersistEngine.getConnection("SIGMA90");

						StringBuilder sql = new StringBuilder();
						sql.append("select  rt.CD_ROTA, rt.NM_ROTA, COUNT(*) as QtdeContas, FAPTAT.valorRota, MODPG.codigo, MODPG.Descricao, fpg.descricaoFormaPagamento,");              				                                   
						sql.append("CASE WHEN FAPTAT.ativo = 1 THEN 'ATIVO' ELSE 'INATIVO' END situacao													 				 ");
						sql.append("from ROTA rt              								  														                     ");
						sql.append("inner join (select ID_ROTA, id_empresa, ID_CENTRAL                                                                                   ");
						sql.append("			from (select CD_CLIENTE, ID_ROTA, id_empresa, ID_CENTRAL                                                                 ");
						sql.append("				  from dbCENTRAL 								  				                                                     ");
						sql.append("				  where FG_ATIVO = 1 									  								  				             ");      
						sql.append("                    AND CTRL_CENTRAL = 1  							  								  				                 ");  
						sql.append("                    AND ID_CENTRAL NOT LIKE 'AAA%' 							  						  				                 ");  
						sql.append("                    AND ID_CENTRAL NOT LIKE 'DDDD' 							 						  				                 ");  
						sql.append("                    AND ID_CENTRAL NOT LIKE 'FFFF'							  						  				                 ");  
						sql.append("                    AND ID_CENTRAL NOT LIKE 'R%'	                                                                                 ");      
						sql.append("                    AND NOT EXISTS (select sig.usu_codcli  from [FSOODB04\\SQL02].sapiens.dbo.usu_t160ctr ctr                        ");
						sql.append("						        inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160cvs cvs on cvs.usu_codemp = ctr.usu_codemp          ");   
						sql.append("                                                                                       and cvs.usu_codfil = ctr.usu_codfil           ");    
						sql.append("                                                                                       and cvs.usu_numctr = ctr.usu_numctr           ");    
						sql.append("						        inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160sig sig on sig.usu_codemp = ctr.usu_codemp          ");   
						sql.append("																				       and sig.usu_codfil = ctr.usu_codfil           ");    
						sql.append("																				       and sig.usu_numctr = ctr.usu_numctr           ");    
						sql.append("																				       and sig.usu_numpos = cvs.usu_numpos           ");    
						sql.append("						        where ((ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate()))             ");    
						sql.append("							      and ((cvs.usu_sitcvs = 'A') or (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= getdate()))             ");    
						sql.append("							      and sig.usu_codcli = CD_CLIENTE)  							  						  			 ");	       
						sql.append("                  group by CD_CLIENTE, ID_ROTA, ID_EMPRESA, ID_CENTRAL) as resulSapiens                                              ");
						sql.append("            group by ID_ROTA, id_empresa, ID_CENTRAL) rtempctrl ON rtempctrl.ID_ROTA = rt.CD_ROTA 				                     ");
						sql.append("LEFT JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.X_SIGMAROTA SIGRT ON SIGRT.cd_rota = RT.CD_ROTA                                        ");
						sql.append("LEFT JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPAplicacaoTatico FAPTAT ON FAPTAT.rotaSigma_neoid = SIGRT.neoid                    ");
						sql.append("LEFT JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPFormaPagamento FPG ON FPG.neoid = FAPTAT.formaPagamento_neoId                     ");
						sql.append("LEFT JOIN [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_FAPTaticoModalidadePgto MODPG ON MODPG.neoid = FAPTAT.modalidade_neoid               ");
						sql.append("WHERE NM_ROTA like '% - PARC%' 											  				                                             ");
						sql.append("group by rt.CD_ROTA, rt.NM_ROTA, FAPTAT.valorRota, MODPG.Descricao, fpg.descricaoFormaPagamento, MODPG.codigo,FAPTAT.ativo               ");
						sql.append("order by 3																															 ");
								
						pstm = conn.prepareStatement(sql.toString());
						rs = pstm.executeQuery();
						
						List<TaticoConta> taticoContas = new ArrayList<TaticoConta>();
						while (rs.next())
						{
							TaticoConta taticoConta = new TaticoConta();
							
							taticoConta.setCodRota(rs.getInt("CD_ROTA"));
							taticoConta.setNomeTatico(rs.getString("NM_ROTA"));
							taticoConta.setQtdContas(rs.getInt("QtdeContas"));
							taticoConta.setVlrRota(rs.getDouble("valorRota"));;
							taticoConta.setFormaPagamento(rs.getString("descricaoFormaPagamento"));
							taticoConta.setCodModalidade(rs.getInt("codigo"));
							taticoConta.setDescModalidade(rs.getString("Descricao"));
							taticoConta.setSituacao(rs.getString("situacao"));
							
							int i = 0;
							for(String reg: listReg)
							{
								if(rs.getString("NM_ROTA").contains(reg+" - ") || rs.getString("NM_ROTA").contains(reg+"-")){
									taticoConta.setRegional(reg);
									i++;
									break;
								}
							}
							if(i == 0){
								taticoConta.setRegional("REGIONAL INDEFINIDA");
							}
							
							taticoContas.add(taticoConta);
						 }				
						
						
						for (String objReg : listReg) 
						{
							String nomeReg = (String) objReg;
							
							Integer totalContasReg = 0; 
							Double valorTotalReg = 0.0;
							String descricaoReg = "";
							
							if(nomeReg.equals("BNU")){
								descricaoReg = "BNU - Blumenau";
							}
							else if(nomeReg.equals("BQE")){
								descricaoReg = "BQE - Brusque";
							}
							else if(nomeReg.equals("CCO")){
								descricaoReg = "CCO - Chapec�";
							}
							else if(nomeReg.equals("CSC")){
								descricaoReg = "CSC - Cascavel";
							}
							else if(nomeReg.equals("CTA")){
								descricaoReg = "CTA - Curitiba";
							}
							else if(nomeReg.equals("CUA")){
								descricaoReg = "CUA - Crici�ma";
							}
							else if(nomeReg.equals("IAI")){
								descricaoReg = "IAI - Itaja�";
							}
							else if(nomeReg.equals("JLE")){
								descricaoReg = "JLE - Joinville";
							}
							else if(nomeReg.equals("JGS")){
								descricaoReg = "JGS - Jaragu� do Sul";
							}
							else if(nomeReg.equals("LGS")){
								descricaoReg = "LGS - Lages";
							}
							else if(nomeReg.equals("RSL")){
								descricaoReg = "RSL - Rio do Sul";
							}
							else if(nomeReg.equals("SOO")){
								descricaoReg = "SOO - S�o Jos�";
							}
							else if(nomeReg.equals("TRO")){
								descricaoReg = "TRO - Tubar�o";
							}
							else if(nomeReg.equals("GNA")){
								descricaoReg = "GNA - Goi�nia";
							}
							else if(nomeReg.equals("CAS")){
								descricaoReg = "CAS - Campinas";
							}
							else if(nomeReg.equals("PMJ")){
								descricaoReg = "PMJ - Palmas";
							}
							else if(nomeReg.equals("NHO")){
								descricaoReg = "NHO - Novo Hamburgo";
							}
							else if(nomeReg.equals("SRR")){
								descricaoReg = "SRR - S�o Jos� do Rio Preto";
							}
							else if(nomeReg.equals("TRI")){
								descricaoReg = "TRI - Tramanda�";
							}
							else if(nomeReg.equals("XLN")){
								descricaoReg = "XLN - Xangri-l�";
							}
							else if(nomeReg.equals("REGIONAL INDEFINIDA")){
								descricaoReg = "REGIONAL INDEFINIDA";
							}
									
							%>							
									
							<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
							<tr style="cursor: auto">
							<th style="cursor: auto; white-space: normal" width="10%">T�tico</th>
							<th style="cursor: auto; white-space: normal" width="5%">Situa��o T�tico</th>
							<th style="cursor: auto; white-space: normal" width="5%">Forma de Pagamento</th>
							<th style="cursor: auto; white-space: normal" width="5%">Modalidade</th>
							<th style="cursor: auto; white-space: normal" width="5%">Qtde Contas</th>							
							<th style="cursor: auto; white-space: normal" width="5%">Valor</th>
							<th style="cursor: auto; white-space: normal" width="5%">Valor Total</th>							
							</tr>
							<fieldset class="fieldGroup" >
							<legend class="legend" style="font-size: 1.2em">&nbsp;<%= descricaoReg %>&nbsp;
							</legend>
							<tbody>
							<%
							List<TaticoConta> taticoList = new ArrayList<TaticoConta>();
							for(TaticoConta taticoObj: taticoContas)
							{
								TaticoConta conta = (TaticoConta)taticoObj;	
								if( taticoObj.getRegional().contains(nomeReg))
								{
								    taticoList.add(conta);
								}
							}
							Collections.sort(taticoList);
							for(TaticoConta taticoConObj : taticoList){
							%>
								<tr>
									<td style="white-space: normal"><%=taticoConObj.getCodRota() + " - " + taticoConObj.getNomeTatico()%></td>
									<td style="white-space: normal; text-align: right"><%=taticoConObj.getSituacao()%></td>
									<td style="white-space: normal; text-align: right"><%=taticoConObj.getFormaPagamento() == null ? "N/D" : taticoConObj.getFormaPagamento()%></td>
									<td style="white-space: normal; text-align: right"><%=taticoConObj.getDescModalidade() == null ? "N/D" : taticoConObj.getDescModalidade()%></td>
									<td style="white-space: normal; text-align: right"><%=taticoConObj.getQtdContas()%>&nbsp;<a onclick="OpenPopupCenter('https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/sigma/consTotalContasTipoTaticoAnaliticoSemConciliacao.jsp?codRota=<%=taticoConObj.getCodRota()%>', 'Pagina', 800, 600);"><img src="https://intranet.orsegups.com.br:443/fusion/imagens/icones_final/visualize_blue_16x16-trans.png"></a></td>
									<td style="white-space: normal; text-align: right"><%=taticoConObj.getCodModalidade() == 2L ? NeoUtils.safeFormat(formatoReal, taticoConObj.getVlrRota()) : "N/D"%></td>
									<td style="white-space: normal; text-align: right"><%=NeoUtils.safeFormat(formatoReal, taticoConObj.getCodModalidade() == 2L ? taticoConObj.getVlrRota() * taticoConObj.getQtdContas() : taticoConObj.getVlrRota())%></td>
									
								</tr>
								<% 
									totalContasReg = totalContasReg + taticoConObj.getQtdContas();
									valorTotalReg = valorTotalReg + (taticoConObj.getCodModalidade() == 2L ? taticoConObj.getVlrRota() * taticoConObj.getQtdContas() : taticoConObj.getVlrRota());
								%>
							<%
							}
							%>
							<tr>
								<td style="white-space: normal" colspan="1">&nbsp;</td>
								<td style="white-space: normal" colspan="1">&nbsp;</td>
								<td style="white-space: normal" colspan="1">&nbsp;</td>
								<td style="white-space: normal" colspan="1">&nbsp;</td>
								<td style="white-space: normal; text-align: right;font-weight: bold;background-color: #FFEEDD;" colspan="1">Total de Contas: <%=totalContasReg%></td>
								<td style="white-space: normal; text-align: right;font-weight: bold;background-color: #CAFFCA;" colspan="3">Valor Total: <%=NeoUtils.safeFormat(formatoReal, valorTotalReg)%></td>
							</tr>
							<%
							}
							%>
					</tbody>
				</table>
			</fieldset>	
			<br />
		
				<%
					} catch (Exception e) {
						e.printStackTrace();
				%>
				<script type="text/javascript">
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
				<%
					}finally {
						try {
										
						OrsegupsUtils.closeConnection(conn, pstm, rs);
						} catch (Exception e) {
						e.printStackTrace();
				%>
				<script type="text/javascript">
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>", 'error');
				</script>
				<%
					}
					}
				%>
			

		</cw:body>
	</cw:main>
</portal:head>