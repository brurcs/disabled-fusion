<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLInFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.sigma.ODMviaSNEP");

	String verboseMode = request.getParameter("verboseMode");
	boolean isVerboseMode = false;
	if (verboseMode != null && (verboseMode.trim().equals("sim") || verboseMode.trim().equals("1") || verboseMode.trim().equals("yes")|| verboseMode.trim().equals("on") )) {
		isVerboseMode = true;
	}


	String testMode = request.getParameter("testMode");
	boolean isTest = false;
	if (testMode != null && (testMode.trim().equals("sim") || testMode.trim().equals("1") || testMode.trim().equals("yes")|| testMode.trim().equals("on") )) {
		isTest = true;
	}

	String acao = request.getParameter("acao");
	if(acao == null) {
		out.print("Erro #2 - A��o n�o informada");
		return;
	} else if (!acao.trim().equals("identify") && !acao.trim().equals("validate") && !acao.trim().equals("generate"))  {
		out.print("Erro #2 - A��o inv�lida");
		return;
	}

	String fone = request.getParameter("fone");
	if(fone == null || fone.trim().equals("")) {
		out.print("Erro #1 - Telefone n�o informado");
		return;
	}	

	String fonex = request.getParameter("codigo");
	log.warn("##### ODM ("+acao+") ["+fone+":"+fonex+"]> TestMode = "+isTest);
	
	
	boolean isDebug = true;
	
	/*
		Retornos poss�veis:
		ID-cd_cliente - Sem erro
		1-Cadastro n�o localizado
		2-N�o foi poss�vel identificar univocamente a conta
		3-Codigo Invalido
		99-Erro desconhecido (Exception)
	*/
	try {

		if (acao.equals("identify")) {	

			QLGroupFilter contasFilter = new QLGroupFilter("AND");
			contasFilter.addFilter(new QLEqualsFilter("usu_numfne", fone));
			contasFilter.addFilter(new QLOpFilter("usu_cd_clie", "<>", "0"));
		
			List<NeoObject> listaContas = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) (EntityRegister.getCacheInstance().getByType("SAPIENSUSUT500FNE"))).getEntityClass(), contasFilter);
			
			if(listaContas != null && listaContas.size() == 0 ) {
					// Cadastro n�o localizado
					if (isVerboseMode) {
						out.print("1-Cadastro n�o localizado");
					} else {
						out.print("{\"id\":\""+fone+"\", \"return\":\"1\"}");
					}
					if (isDebug) {
						log.warn("##### ODM ("+acao+") ["+fone+"]> 1-Cadastro n�o localizado");
					}
					return;
			} else if(listaContas.size() > 1 ) {
			
					QLGroupFilter codigosRecuperadosFilter = new QLGroupFilter("OR");
					for(NeoObject noConta : listaContas) {
						EntityWrapper ewConta = new EntityWrapper(noConta);
						codigosRecuperadosFilter.addFilter(new QLEqualsFilter("cd_cliente", Long.parseLong((String) ewConta.findValue("usu_cd_clie"))));
					}
					QLGroupFilter onDemandFilter = new QLGroupFilter("AND");
					onDemandFilter.addFilter(new QLEqualsFilter("id_empresa", 10118L));
					onDemandFilter.addFilter(codigosRecuperadosFilter);
					List<NeoObject> listaContasOnDemand = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) (EntityRegister.getCacheInstance().getByType("SIGMACENTRAL"))).getEntityClass(), onDemandFilter);

					if(listaContasOnDemand.size() != 1 ) {
						// N�o foi poss�vel identificar univocamente a conta
						if (isVerboseMode) {
							out.print("2-N�o foi poss�vel identificar univocamente a conta");
						} else {
							out.print("{\"id\":\""+fone+"\", \"return\":\"2\"}");
						}
						if (isDebug) {
							log.warn("##### ODM ("+acao+") ["+fone+"]> 2-N�o foi poss�vel identificar univocamente a conta");
						}
						return;
					}
					// Conta on demand identificada
					EntityWrapper ewContaIdentificada = new EntityWrapper((NeoObject) listaContasOnDemand.get(0));
					Long cd_cliente_identificado = (Long) ewContaIdentificada.findValue("cd_cliente");
					// Identificado com sucesso
					if (isVerboseMode) {
						out.print("OK:" + cd_cliente_identificado);
					} else {
						out.print("{\"id\":\""+fone+"\", \"return\":\"OK-"+cd_cliente_identificado+"\"}");
					}
					return;
			}
			
			// Conta identificada
			EntityWrapper ewContaIdentificada = new EntityWrapper((NeoObject) listaContas.get(0));
			String cd_cliente_identificado = (String) ewContaIdentificada.findValue("usu_cd_clie");
			// Identificado com sucesso
			if (isVerboseMode) {
				out.print("OK:" + cd_cliente_identificado);
			} else {
				out.print("{\"id\":\""+fone+"\", \"return\":\"OK-"+cd_cliente_identificado+"\"}");
			}
			return;
			
		} else if (acao.equals("validate")) {		
			String cd_cliente = request.getParameter("codigo");
			if(cd_cliente == null || cd_cliente.trim().equals(""))	{
				out.print("Erro #2 - C�digo n�o informado");
				return;
			}
			cd_cliente = cd_cliente.replaceAll("\\*", "");
			
			QLGroupFilter onDemandFilter = new QLGroupFilter("AND");
			Long cd_cliente_long = Long.parseLong(cd_cliente);
			onDemandFilter.addFilter(new QLEqualsFilter("cd_cliente", cd_cliente_long));
			onDemandFilter.addFilter(new QLEqualsFilter("ctrl_central", Boolean.TRUE));
			onDemandFilter.addFilter(new QLEqualsFilter("id_empresa", 10118L));
		
			List<NeoObject> listaContasOnDemand = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) (EntityRegister.getCacheInstance().getByType("SIGMACENTRAL"))).getEntityClass(), onDemandFilter);
			
			if(listaContasOnDemand == null || listaContasOnDemand.size() != 1 ) {
				if (isVerboseMode) {
					out.print("3-Erro na validacao");
					return;
				} else {
					out.print("{\"id\":\""+fone+"\", \"return\":\"3\"}");
					return;
				}
			}			
			
			if (isVerboseMode) {
				out.print("OK:" + cd_cliente);
			} else {
				out.print("{\"id\":\""+fone+"\", \"return\":\"OK-"+cd_cliente+"\"}");
			}
			
		} else if (acao.equals("generate")) {		
			// Criacao do evento no Sigma
			String cd_central = "";
			String cd_empresa = "";
			String particao = "";
			String nm_evento = "EODM";
			String cd_cliente = request.getParameter("codigo");
			if(cd_cliente == null || cd_cliente.trim().equals(""))	{
				out.print("Erro #2 - C�digo n�o informado");
				return;
			}
			cd_cliente = cd_cliente.replaceAll("\\*", "");


			QLGroupFilter onDemandFilter = new QLGroupFilter("AND");
			Long cd_cliente_long = Long.parseLong(cd_cliente);
			onDemandFilter.addFilter(new QLEqualsFilter("cd_cliente", cd_cliente_long));
			onDemandFilter.addFilter(new QLEqualsFilter("ctrl_central", Boolean.TRUE));
			onDemandFilter.addFilter(new QLEqualsFilter("id_empresa", 10118L));
			List<NeoObject> listaContasOnDemand = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) (EntityRegister.getCacheInstance().getByType("SIGMACENTRAL"))).getEntityClass(), onDemandFilter);
			if(listaContasOnDemand == null || (listaContasOnDemand != null && listaContasOnDemand.size() != 1) ) {
				if (isVerboseMode) {
					out.print("4-Erro na geracao do evento");
					return;
				} else {
					out.print("{\"id\":\""+fone+"\", \"return\":\"4\"}");
					return;
				}
			} else {
				EntityWrapper ewContaIdentificada = new EntityWrapper((NeoObject) listaContasOnDemand.get(0));
				cd_central = (String) ewContaIdentificada.findValue("id_central");
				cd_empresa = ((Long) ewContaIdentificada.findValue("id_empresa")).toString();
				particao = (String) ewContaIdentificada.findValue("particao");
			}

			if (!isTest) {
				String urlEvento = PortalUtil.getBaseURL() + "custom/jsp/orsegups/sigma/criaEvento.jsp?cd_central=" + cd_central + "&cd_empresa="+ cd_empresa +"&particao="+ particao +"&nm_evento=" + nm_evento +"&origem=ODM:"+fone;
				URL urlAux = new URL(urlEvento);
				URLConnection uc = urlAux.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));	
				if (isVerboseMode) {
					out.print("OK:" + cd_cliente);
				} else {
					out.print("{\"id\":\""+fone+"\", \"return\":\"OK-"+cd_cliente+"\"}");
				}
			}
			if (isDebug) {
				log.warn("##### ODM ("+acao+") ["+fone+":"+cd_cliente+"]> Criado Evento no Sigma:" + nm_evento + " - TestMode = "+isTest);
			}
			
		}

		
    }  catch (Exception e) {
		e.printStackTrace();
		// erro desconhecido
		if (isVerboseMode) {
			out.print("99-Erro desconhecido");
		} else {
			out.print("{\"id\":\""+fone+"\", \"return\":\"99\"}");
		}
		return;    
	} 
%>