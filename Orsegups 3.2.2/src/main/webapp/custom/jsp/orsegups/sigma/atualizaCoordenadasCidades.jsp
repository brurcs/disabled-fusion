<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.net.*"%>
<%@page import="java.io.*"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="org.xml.sax.SAXException"%>
<%@page import="org.xml.sax.SAXParseException"%> 
<%@page import="org.w3c.dom.Document"%> 
<%@page import="org.w3c.dom.*"%> 

	<input onClick="javascript:document.location.reload();" type="button" value="Atualizar"/></a>
	
	<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
	<thead style="cursor: auto">
		<tr style="cursor: auto">
			<th style="cursor: auto">CIDADE</th>
			<th style="cursor: auto">UF</th>
			<th style="cursor: auto">LAT</th>
			<th style="cursor: auto">LONG</th>
		</tr>
	</thead>
	<tbody>	
<%!

	public final String getString(String tagName, Element element) {
		NodeList list = element.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			NodeList subList = list.item(0).getChildNodes();

			if (subList != null && subList.getLength() > 0) {
				return subList.item(0).getNodeValue();
			}
		}
		return null;
	}
%>
<%
	final String none = "&nbsp;";
	int count = 0;

	PreparedStatement st = null;
	try {
		String nomeFonteDados = "SIGMA90";
		Connection conn = PersistEngine.getConnection(nomeFonteDados);
 		
		StringBuffer sql1 = new StringBuffer();
		sql1.append("SELECT NOME, ID_ESTADO, ID_CIDADE, NU_LATITUDE, c.NU_LONGITUDE ");
		sql1.append("FROM dbCIDADE c ");
		sql1.append("WHERE ID_ESTADO = 'SC' AND (c.NU_LATITUDE IS NULL OR c.NU_LONGITUDE IS NULL) ");
		sql1.append("ORDER BY ID_CIDADE ");

		st = conn.prepareStatement(sql1.toString());
		ResultSet rs = st.executeQuery();
			
		while(rs.next()) {
			String lat = null;
			String lng = null;
			String sURL = rs.getString("NOME") + " - "+ rs.getString("ID_ESTADO");
			String encodedURL = URLEncoder.encode(sURL.toString(),"UTF-8");
			URL url = new URL("http://maps.google.com/maps/api/geocode/xml?sensor=false&address=" + encodedURL);
			URLConnection uc = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			String inputLine;
			//while ((inputLine = in.readLine()) != null) 
			//	System.out.println(inputLine);
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse (uc.getInputStream());			
			in.close();	
				
			Element rootElement = doc.getDocumentElement();
			lat = getString("lat", rootElement);
			lng = getString("lng", rootElement);

			out.print("<tr>");		
			out.print("<td> " + rs.getString("NOME") + "</td>");
			out.print("<td> " + rs.getString("ID_ESTADO") + "</td>");
			out.print("<td> " + lat + "</td>");
			out.print("<td> " + lng + "</td>");
			out.print("</tr>");

			if (lat != null) {
				count++;
			}
			StringBuffer sql2 = new StringBuffer();
			sql2.append("UPDATE dbCIDADE SET NU_LATITUDE = ?, NU_LONGITUDE = ? WHERE ID_CIDADE = ?   ");
			PreparedStatement st2 = conn.prepareStatement(sql2.toString());
			st2.setString(1, lat);
			st2.setString(2, lng);
			st2.setLong(3, rs.getLong("ID_CIDADE"));
			st2.executeUpdate();
			st2.close();
			
			System.out.println(count + ":" + rs.getString("ID_CIDADE") + " - " + rs.getString("NOME") + ": " + lat + "," + lng);
			
		}
		rs.close();
		st.close();		
	} catch (Exception e) {
		out.print("Erro ao tentar localizar coordenadas: " + e.getMessage());
		e.printStackTrace();	
	}
	if (count > 0) {
		out.print("OK");
	} else {
		out.print("Coordenadas n�o localizadas.");
	}

	
%>

	</tbody>
	</table>
	
	<br>


	