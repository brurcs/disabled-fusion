<%@page import="com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper"%>
<%@page import="com.neomind.fusion.workflow.simulation.WorkflowService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.HashSet"%>
<% 

	NeoObject novoCadastro = AdapterUtils.getInstantiableEntityInfo("RDCReversaoDeCancelamento").createNewInstance();
	EntityWrapper wrp = new EntityWrapper(novoCadastro);

	NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("solicitante")));
	InstantiableEntityInfo eOrigem = AdapterUtils.getInstantiableEntityInfo("origemTarefa");
	NeoObject origem = (NeoObject) PersistEngine.getObject(eOrigem.getEntityClass(), new QLEqualsFilter("codigo", Long.valueOf(request.getParameter("origem"))));
	
	String titulo = request.getParameter("titulo");
	String descricao = request.getParameter("descricao");
	Long idOrdem = Long.parseLong(request.getParameter("idOrdem"));
	
	wrp.findField("solicitante").setValue(solicitante);
	wrp.findField("titulo").setValue(titulo);
	wrp.findField("descricao2").setValue(descricao);
	wrp.findField("idordem").setValue(idOrdem);
	
	PersistEngine.persist(novoCadastro);
	
	ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("Name", "C032 - RDC - Revers�o de Cancelamento"));

	WFProcess processo = WorkflowService.startProcess(pm, novoCadastro, false, solicitante);
	processo.setSaved(true);
	
	new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante, true);
	 
	out.print(processo.getCode());
	
%>