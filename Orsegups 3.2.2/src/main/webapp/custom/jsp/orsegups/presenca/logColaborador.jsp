<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoCalendarUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	String numcpf = request.getParameter("numcpf");
	List<NeoObject> logs = QLPresencaUtils.getLogColaborador(numcpf);
	
%>
<html>
<%  
if (logs != null && !logs.isEmpty())
{
	for (NeoObject log: logs)
	{
		EntityWrapper logWrapper = new EntityWrapper(log);
		
		String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
		String texto = (String) logWrapper.findValue("textoLog");
		String usuario = (String) logWrapper.findValue("usuario.fullName");
		
		%>	
		<div style="background-color: #e2edff; border: 1px solid #fff;font-size: 10px">
			<span style="color: #082c5a; height: 20px"><b><%= formatedDate %></b> <%= ": " + texto %> <br/><b><%= usuario %></b></span>
		</div>
		<%	
	}
}
else
{
	%>	
	<div style="background-color: #e2edff; border: 1px solid #fff;font-size: 10px">
		<span style="color: #082c5a; height: 20px"><b>Nenhuma informa��o � listar</b></span>
	</div>
	<%
}
%>
</html>