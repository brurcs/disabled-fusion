<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@ page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta http-equiv="Pragma" content="no-cache">
 <meta http-equiv="Cache-Control" content="no-cache">
 <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<title>Teste SNEP</title>
</head>
<body>
</body>
</html>
	
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>

<portlet:defineObjects />
<form:defineObjects />

<portal:head title="Teste SNEP">
<cw:main>
	<cw:header title="Teste SNEP" />
	
	<cw:body id="area_scroll">

				
   		<div id="dados">
		<fieldset class="fieldGroup">
		<legend class="legend" hasbox="2">Teste SNEP</legend>
		<OL hasbox="2">
		<div class="fieldSet" hasbox="2">
		<table width="100%">
		 <tr>
		    <td>
		     <LI>
		      <label class="formFieldLabel">Dire��o:&nbsp;</label>&nbsp;
	          <input type="text"class="input_text" name='txt_direcao' id='idTxtDirecao' required='false' /> 
	          <input id='idDirecao' name='idDirecao' type='hidden'/>
             </LI>
		    </td>
		 </tr>
		 <tr>
		  <td>
		    <LI>
		      <label class="formFieldLabel">Empresa:&nbsp;</label>&nbsp;
	          <input type="text"class="input_text" name='txt_empresa' id='idTxtEmpresa' required='false' /> 
	          <input id='idEmpresa' name='idEmpresa' type='hidden'/>
		    </LI>
		  </td>
		</tr>
		<tr>
		  <td>
		  	<LI>
			   <label class="formFieldLabel">Matricula:&nbsp;</label>&nbsp;
		       <input type="text"class="input_text" name='txt_matricula' id='idTxtMatricula' required='false' /> 
		       <input id='idMatricula' name='idMatricula' type='hidden'/>
		    </LI>
		   </td>
		</tr>
		 <tr>
			<td>
			  <LI>
			    <label class="formFieldLabel">Data:&nbsp;</label>&nbsp;
	              <input id="dataString" class="input_text" style="height: 20px;width: 120px" name="dataString" value="<%=dataString%>" errorSpan="err_var_dataInicialDe" required="false" onchange="" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)"/><span id="err_var_dataInicialDe" style="margin-left:0px;"></span>&nbsp;<img src="imagens/icones_final/date_16x16-trans.png" class="ico" id="btdataInicialDe" name="btdataInicialDe"  onclick="return showNeoCalendar('dataString', '%d/%m/%Y %H:%M','btdataInicialDe', event);">&nbsp;<img src='imagens/icones_final/eraser_16x16-trans.png' class='ico' title="Buscar a data atual" onclick="javascript: limpaData();">&nbsp;&nbsp;&nbsp;
				  <span>Utilize o formato DD/MM/AAAA HH:MM</span>
		          <br>
		      </LI>
			</td>
		 </tr>
		</table>
		<LI>
			<input type="button"class="input_button"id="consulta"value="Consultar">
		</LI>
		</div>
	 			</OL>
		</fieldset>

	<div id=resultadoConsulta>

	</div>

	<script type="text/javascript">

		$(document).ready(function() 
			{
		 		$('#consulta').click(function() 
		   	 	{
		 		  var direcaoAcesso;	
		 			
		    	  var direcao = $('#idTxtDirecao').val();
		   		  var empresa = $('#idTxtEmpresa').val();
		   		  var matricula = $('#idTxtMatricula').val();
		   		  var data  = $('#var_data').val();
		   		  
		   		  if (direcao == 1){
		   			  direcaoAcesso = "login";
		   		  } else{
		   			  direcaoAcesso = 'logoff';
		   		  }
		   		  
		   		  if(data == null || data == '' ){
		   			  alert('O campo Data n�o foi preenchido.');
		   			  document.getElementById('var_data').focus();
		   			  return;
		   		  }

		   		  var result = '<%= PortalUtil.getBaseURL() %>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=consultaCobertura&colaborador='+colaborador+'&colaboradorSub='+colaboradorSub+'&posto='+posto+'&dataInicialDe='+dataInicialDe+'&dataInicialAte='+dataInicialAte+'&dataFinalDe='+dataFinalDe+'&dataFinalAte='+dataFinalAte;

		   		  	$.ajax({
			  		  url: result,
			  		  success: function( data ) {
			  			  $('#resultadoConsulta').html(data);

			  		  }
			  		});
		   		 }
		 		);
			}
		);
		
	function limpaCampo(idCampo)
	{
		$('#'+idCampo).val("");
		$('#'+idCampo).removeAttr("selectedvalue");
	}
	
	function deleteRegistroCobertura(numEmp, tipCol, numCad, dataInicial, horaInicial)
	{
		var isDelete = confirm("Deseja excluir o registro de cobertura?");

		if(isDelete)
		{
			var	result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=deleteRegistroCobertura&numEmp="+numEmp+"&tipCol="+tipCol+"&numCad="+numCad+"&dataInicial="+dataInicial+"&horaInicial="+horaInicial);
			if(result == 0){
			  alert('N�o foi poss�vel excluir o registro de cobertura.');
			}
			if(result > 0){
			  alert('Registro exclu�do com sucesso.');
			  document.getElementById("consulta").click();
			}
		}
	}

	</script>
	</cw:body>
	
</cw:main>
</portal:head>