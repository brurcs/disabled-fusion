<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.net.URLEncoder"%>



<portal:head title="Gest�o de Entrada/Sa�da Colaboradores">
<cw:main>
	<cw:header title="Gest�o de Entrada/Sa�da Colaboradores" />
	<cw:body id="area_scroll">
	
	<FIELDSET class="fieldGroup">
	<LEGEND class="legend">&nbsp;Colaboradores com Crach�&nbsp;</LEGEND>
		
	<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
	<thead style="cursor: auto">
		<tr style="cursor: auto">
			<th style="cursor: auto">Empresa</th>
			<th style="cursor: auto">Matr�cula</th>
			<th style="cursor: auto">Crach�</th>
			<th style="cursor: auto">Colaborador</th>
			<th style="cursor: auto" class="min">Entrada</th>
			<th style="cursor: auto" class="min">Sa�da</th>
		</tr>
	</thead>
	<tbody>	

<%
	try {
		Connection connVetorh = PersistEngine.getConnection("VETORH");

		/*** Recupera Colaboradores com crach� ***/
		StringBuffer queryCracha = new StringBuffer();
		queryCracha.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.NomFun ");
		queryCracha.append(" FROM R038HCH hch ");
		queryCracha.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.TipCol = hch.TipCol AND fun.NumCad = hch.NumCad ");
		queryCracha.append(" WHERE hch.NumCra <= ? 	 ");

		PreparedStatement stCracha = connVetorh.prepareStatement(queryCracha.toString());
		stCracha.setInt(1, 14);

		ResultSet rsCracha = stCracha.executeQuery();

		while(rsCracha.next()) {
			String empresa = rsCracha.getString("numemp");
			String matricula = rsCracha.getString("numcad");
			String cracha = String.format("%05d", rsCracha.getInt("numcra"));
			
			String colaborador = rsCracha.getString("nomfun");
			
			out.print("<tr>");
			out.print("<td> " +  empresa + "</td>");
			out.print("<td> " +  matricula + "</td>");
			out.print("<td> " +  cracha + "</td>");
			out.print("<td> " +  colaborador + "</td>");
			out.print("<td align='center'> <a href='javascript:doAction(\"login\", \""+ cracha + "\");'><img src='imagens/icones_final/check_16x16-trans.png' align='absmiddle' /></a></td>");
			out.print("<td align='center'> <a href='javascript:doAction(\"logoff\", \""+ cracha + "\");'><img src='imagens/icones_final/sphere_red_logout_16x16-trans.png' align='absmiddle' /></a></td>");
			out.print("</tr>");
		}
	}
	catch (IllegalStateException e)
	{
		
	}

%>
	</tbody>
	</table>
	<br>
	</fieldset>
	
	</cw:body>
</cw:main>
</portal:head>

<script language="JavaScript">

function doAction(acao, codigo) {

	var response = "";
	
	response = callSync('<%= PortalUtil.getBaseURL() %>custom/jsp/orsegups/sigmaEntradaAAviaSNEP2.jsp?verboseError=yes&acao=' + acao + '&codigo='+ codigo +'&cd_placa=0000');

	if (response.indexOf("OK") != -1) {
		alert("OK - A��o realizada com sucesso.");
	} else {
		alert("ERRO: " + response);
	}
		
	
}

</script>