<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoUtils"%>

<%!


public static String convertHoraSenior(Long horasSenior) {
	
	String horaConvertida = "";
	GregorianCalendar calUtil = new GregorianCalendar();
	Integer hora = 0;
	Integer minuto = 0;		

	if (horasSenior > 0) {
		hora = horasSenior.intValue() / 60;
		minuto = horasSenior.intValue() % 60;
	}
	
	calUtil.set(GregorianCalendar.HOUR_OF_DAY, hora);
	calUtil.set(GregorianCalendar.MINUTE, minuto);
	
	return NeoUtils.safeDateFormat(calUtil, "HH:mm");

}

public static Long getDiaPresenca(GregorianCalendar data) {
		
		Map<Integer,Long> diaSemana = new HashMap<Integer,Long>(); 
		diaSemana.put(GregorianCalendar.MONDAY, 1L);  
		diaSemana.put(GregorianCalendar.TUESDAY, 2L);  
		diaSemana.put(GregorianCalendar.WEDNESDAY, 3L);  
		diaSemana.put(GregorianCalendar.THURSDAY, 4L);  
		diaSemana.put(GregorianCalendar.FRIDAY, 5L);  
		diaSemana.put(GregorianCalendar.SATURDAY, 6L);  
		diaSemana.put(GregorianCalendar.SUNDAY, 7L);  
		
		Long dia = diaSemana.get(data.get(GregorianCalendar.DAY_OF_WEEK));
		
		// Checar Feriados 
		if (!OrsegupsUtils.isWorkDay(data)) {
			dia = 8L;
		}
		
		return dia;

}

public static String getDescricaoEscala(Long codigoEscala, GregorianCalendar data) {

	String descricao = "";
	
	QLGroupFilter horarioFilter = new QLGroupFilter("AND"); 
	horarioFilter.addFilter(new QLEqualsFilter("usu_codesc", codigoEscala));
	horarioFilter.addFilter(new QLEqualsFilter("usu_diasem", getDiaPresenca(data)));
	
	List<NeoObject> horarios = PersistEngine.getObjects(((ExternalEntityInfo)EntityRegister.getInstance().getCache().getByType("SAPIENSUSUTHOR")).getEntityClass(), horarioFilter, -1, -1, " usu_horini ");
	if(NeoUtils.safeIsNotNull(horarios) && horarios.size() > 0)
	{
		for(NeoObject horario : horarios){
			EntityWrapper wrpHorario = new EntityWrapper(horario);
			Long horIni = (Long)wrpHorario.findField("usu_horini").getValue();			
			Long horFim = (Long)wrpHorario.findField("usu_horfim").getValue();
			
			if (descricao.equals("")) {
				descricao = convertHoraSenior(horIni) + "-" + convertHoraSenior(horFim);
			} else {
				descricao = descricao + " / " + convertHoraSenior(horIni) + "-" + convertHoraSenior(horFim);				
			}
		}
	}
	if ("".equals(descricao)) {
		descricao = "Sem Escala";
	}
	return descricao;
}
  


public static String getDescricaoEscalaBarra(Long codigoEscala, GregorianCalendar data) {

	String descricao = "";
	
	QLGroupFilter horarioFilter = new QLGroupFilter("AND"); 
	horarioFilter.addFilter(new QLEqualsFilter("usu_codesc", codigoEscala));
	horarioFilter.addFilter(new QLEqualsFilter("usu_diasem", getDiaPresenca(data)));
	
	List<NeoObject> horarios = PersistEngine.getObjects(((ExternalEntityInfo)EntityRegister.getInstance().getCache().getByType("SAPIENSUSUTHOR")).getEntityClass(), horarioFilter, -1, -1, " usu_horini ");
	if(NeoUtils.safeIsNotNull(horarios) && horarios.size() > 0)
	{
		for(NeoObject horario : horarios){
			EntityWrapper wrpHorario = new EntityWrapper(horario);
			Long horIni = (Long)wrpHorario.findField("usu_horini").getValue();			
			Long horFim = (Long)wrpHorario.findField("usu_horfim").getValue();
			
			if (descricao.equals("")) {
				descricao = convertHoraSenior(horIni) + "-" + convertHoraSenior(horFim);
			} else {
				descricao = descricao + " / " + convertHoraSenior(horIni) + "-" + convertHoraSenior(horFim);				
			}
		}
	}
	if ("".equals(descricao)) {
		descricao = "N�o Dispon�vel";
	}
	return descricao;
}
 %>


<portal:head title="Quadro de Lota��o - Presen�a">
<cw:main>
	<cw:header title="Quadro de Lota��o - Presen�a" />
	<cw:body id="area_scroll">
	
<%

	String regional = request.getParameter("regional");
	if(regional == null || regional.trim().equals(""))	{
		out.print("Erro #2 - Regional n�o informada");
		return;
	}
	Long codigoRegional = NeoUtils.safeLong(regional);


	/*** Declaracoes globais ***/
	final String none = "&nbsp;";
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	Connection connSapiens = PersistEngine.getConnection("SAPIENS");
	String nomeCliente = "";
	String nomeClienteAnt = "x";
	String nomePosto = "";
	String servico = "";
	String situacao = "";
	String qtdVagas = "";
	String escalaHoje = "";
	String centroCusto = "";
	int codigoOrganograma = 203;
	int numeroLocal = 0;
	SimpleDateFormat formatadorDataHora = new SimpleDateFormat("dd/MM/yyyy HH':'mm");
	Collection<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
	String telefonePosto = "";
%>

<%
	/*** Recupera Postos ***/
	StringBuffer queryPostos = new StringBuffer();
	queryPostos.append(" SELECT nv8.codnivel, nv8.nomloc, nv8.usu_desser, nv8.usu_codreg, nv8.usu_cliorn, usu_sitati, usu_qtdcon , nv8.numloc, nv8.usu_codccu");
	queryPostos.append(" FROM USU_VORG203NV8 nv8 WITH (NOLOCK) ");
	queryPostos.append(" INNER JOIN R074CID cid WITH (NOLOCK) ON cid.CODCID = nv8.USU_CODCID ");
	queryPostos.append(" WHERE nv8.USU_CodReg = ? AND nv8.USU_SitAti = 'S' ");
	queryPostos.append(" AND nv8.TABORG = ?  ");
	queryPostos.append(" AND nv8.USU_NUMEMP NOT IN (5, 11) ");
//	queryPostos.append(" AND nv8.USU_Codccu IN ('180830', '5618071', '5618081') ");
//	queryPostos.append(" AND nv8.USU_Codccu IN ('225765','5618071', '5618081') ");
	queryPostos.append(" AND (  ");
	queryPostos.append("   (nv8.codloc LIKE '1.5%'  AND  nv8.USU_NUMEMP IN (1, 15, 17, 19) ) ");
//	queryPostos.append("   OR ");
//	queryPostos.append("   (nv8.codloc LIKE '1.1%') ");
	queryPostos.append(" )  ");
	queryPostos.append(" ORDER BY nv8.codloc ");

	PreparedStatement stPostos = connVetorh.prepareStatement(queryPostos.toString());
	stPostos.setLong(1, codigoRegional);
	stPostos.setInt(2, codigoOrganograma);
	ResultSet rsPostos = stPostos.executeQuery();

	try {
		Boolean fecharTabela = false;
		while(rsPostos.next()) {	
			telefonePosto = "";
			entradasAutorizadas.clear();
			nomeCliente = rsPostos.getString("usu_cliorn");
			nomePosto = rsPostos.getString("nomloc");
			centroCusto = rsPostos.getString("usu_codccu");
			servico = rsPostos.getString("usu_desser");
			situacao = (rsPostos.getString("usu_sitati").equals("S") ? "Ativo" : "Inativo");
			qtdVagas = rsPostos.getString("usu_qtdcon");
			numeroLocal = rsPostos.getInt("numloc");
			
			/*** Recupera Operacoes em Posto ***/
			StringBuffer queryOperacaoPosto = new StringBuffer();
			queryOperacaoPosto.append(" SELECT cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, cob.usu_datalt, cob.usu_datfim, fun.nomfun, car.titred ");
			queryOperacaoPosto.append(" FROM USU_T038COBFUN cob WITH (NOLOCK) ");
			queryOperacaoPosto.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad  ");
			queryOperacaoPosto.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
			queryOperacaoPosto.append(" WHERE cob.usu_taborgtra = ? AND cob.usu_numloctra = ? AND GETDATE() BETWEEN cob.USU_DatAlt AND cob.USU_DatFim");

			PreparedStatement stOperacaoPosto = connVetorh.prepareStatement(queryOperacaoPosto.toString());
			stOperacaoPosto.setLong(1, codigoOrganograma);
			stOperacaoPosto.setLong(2, numeroLocal);
			ResultSet rsOperacaoPosto = stOperacaoPosto.executeQuery();
			while(rsOperacaoPosto.next()) {
				EntradaAutorizadaVO entradaVO = new EntradaAutorizadaVO();
				entradaVO.setNumeroEmpresa(rsOperacaoPosto.getLong("usu_numemp"));
				entradaVO.setTipoColaborador(rsOperacaoPosto.getLong("usu_tipcol"));
				entradaVO.setNumeroCadastro(rsOperacaoPosto.getLong("usu_numcad"));
				entradaVO.setNomeColaborador(rsOperacaoPosto.getString("nomfun"));
				entradaVO.setNomeColaboradorSubtituido("N/A");
				String escalaAux = "";
				GregorianCalendar calUtil = new GregorianCalendar();
				calUtil.setTime(rsOperacaoPosto.getTimestamp("usu_datalt"));
				entradaVO.setDataInicial(calUtil);
				escalaAux = NeoUtils.safeDateFormat(calUtil, "HH:mm") + "-";
				calUtil = new GregorianCalendar();
				calUtil.setTime(rsOperacaoPosto.getTimestamp("usu_datfim"));
				entradaVO.setDataFinal(calUtil);
				escalaAux = escalaAux + NeoUtils.safeDateFormat(calUtil, "HH:mm");
				entradaVO.setEscala(escalaAux);
				entradaVO.setCargo(rsOperacaoPosto.getString("titred"));
				// Adicionando na lista geral de entradas autorizadas
				entradasAutorizadas.add(entradaVO);
			}
	
			/*** Recupera Escala Posto ***/
			StringBuffer queryEscalaPosto = new StringBuffer();
			queryEscalaPosto.append(" SELECT TOP 1 usu_codesc ");
			queryEscalaPosto.append(" FROM USU_T160CVS WITH (NOLOCK) ");
			queryEscalaPosto.append(" WHERE USU_SitCvs = 'A' AND USU_CodCcu = ? ");

			PreparedStatement stEscala = connSapiens.prepareStatement(queryEscalaPosto.toString());
			stEscala.setString(1, centroCusto);
			ResultSet rsEscala = stEscala.executeQuery();
			if(rsEscala.next()) {
				escalaHoje = getDescricaoEscala(rsEscala.getLong("usu_codesc"), new GregorianCalendar());
			} else {
				escalaHoje = "<img src='imagens/icones_final/sphere_yellow_att_16x16-trans.png' alt='Escala n�o cadastrada'/> Escala N�o Cadastrada";
			}

			/*** Recupera Telefone(s) do Posto ***/
			QLEqualsFilter centroCustoTelefoneFilter = new QLEqualsFilter("centrodecusto", centroCusto);
		
			List<NeoObject> listaTelefone = PersistEngine.getObjects(AdapterUtils.getEntityClass("TELECOMCelulares"), centroCustoTelefoneFilter);
			if(NeoUtils.safeIsNotNull(listaTelefone) && listaTelefone.size() > 0)
			{
				NeoObject noTelefone = (NeoObject)listaTelefone.get(0);
				EntityWrapper wrpTelefone = new EntityWrapper(noTelefone);
				telefonePosto = "<a href='javascript:void();'><img src='imagens/custom/phone.png' align='absMiddle' alt='Discar para o telefone do posto'>" + (String)wrpTelefone.findValue("ddd") + " " + (String)wrpTelefone.findValue("numero") + "</a>";
			} else {
				telefonePosto = "<img src='imagens/icones_final/sphere_yellow_att_16x16-trans.png' alt='Telefone n�o cadastrado'/> Telefone n�o cadastrado";
			}
			
			if (!nomeCliente.equals(nomeClienteAnt) && fecharTabela) {
				fecharTabela = false;
				%>
				</tbody>
				</table>
				</fieldset>
				<br/><br/>
			   <%
			}			

			if (!nomeCliente.equals(nomeClienteAnt) ) {
				nomeClienteAnt = nomeCliente;
				fecharTabela = true;
				%>
				<FIELDSET class="fieldGroup">
				<LEGEND class="legend">&nbsp;<%= nomeCliente %>&nbsp;</LEGEND>
				<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
				<tbody>						
				<%
			}
			%>
					<tr style="cursor: auto">
						<th style="cursor: auto">Nome do Posto</th>
						<th style="cursor: auto">Centro de Custo</th>
						<th style="cursor: auto">Servi�o</th>
						<th style="cursor: auto">Telefone</th>
						<th style="cursor: auto">Escala de Hoje</th>
						<th style="cursor: auto">Situa��o</th>
						<th style="cursor: auto">Vagas</th>
					</tr>
				<tr>
					<td style="font-weight: bold"><%=nomePosto%></td>
					<td style="font-weight: bold"><%=centroCusto%></td>
					<td style="font-weight: bold"><%=servico%></td>
					<td style="font-weight: bold"><%=telefonePosto %></td>
					<td style="font-weight: bold"><%=escalaHoje%></td>
					<td style="font-weight: bold"><%=situacao%></td>
					<td style="font-weight: bold"><%=qtdVagas%></td>
				</tr>
				<tr>
					<td colspan="7">
					<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
						<tr style="cursor: auto">
							<th style="cursor: auto" width="5%">Emp.</th>
							<th style="cursor: auto" width="5%">Matr�cula</th>
							<th style="cursor: auto" width="30%">Colaborador</th>
							<th style="cursor: auto" width="15%">Cargo</th>
							<th style="cursor: auto" width="15%">Escala</th>
							<th style="cursor: auto" width="10%">Situa��o</th>
							<th style="cursor: auto" width="8%">Entrada</th>
							<th style="cursor: auto" width="8%">Sa�da</th>
							<th style="cursor: auto">&nbsp;</th>
						</tr>
					<tbody>	
					<%					
					/*** Recupera Colaboradores ***/
					StringBuffer queryColaborador = new StringBuffer();
					queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.DatAdm, fun.NomFun, fun.SitAfa, fun.DatAfa, sit.DesSit, esc.NomEsc, car.TitRed, fun.NumCpf, hes.CodEsc, hes.CodTma ");
					queryColaborador.append(" FROM R034FUN fun  ");
					queryColaborador.append(" INNER JOIN R038HLO hlo ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= GETDATE()) ");
					queryColaborador.append(" INNER JOIN R038HES hes ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= GETDATE()) ");
					queryColaborador.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
					queryColaborador.append(" INNER JOIN R038HCA hca ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= GETDATE()) ");
					queryColaborador.append(" INNER JOIN R024CAR car ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");
					queryColaborador.append(" INNER JOIN R010SIT sit ON sit.CodSit = fun.SitAfa ");
					queryColaborador.append(" INNER JOIN R030ORG org ON org.NumEmp = fun.NumEmp AND org.DatAlt = (SELECT MAX (DATALT) FROM R030ORG TABELA001 WHERE TABELA001.NUMEMP = org.NUMEMP AND TABELA001.DATALT <= GETDATE()) ");
					queryColaborador.append(" WHERE fun.DatAdm <= GETDATE() ");
					queryColaborador.append(" AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > GETDATE())) ");
					queryColaborador.append(" AND fun.TipCol = 1 ");
					queryColaborador.append(" AND hlo.TabOrg = ? AND hlo.NumLoc = ? ");

					PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
					stColaborador.setInt(1, codigoOrganograma);
					stColaborador.setInt(2, numeroLocal);

					ResultSet rsColaborador = stColaborador.executeQuery();
					
					while(rsColaborador.next()) {

						/*** Recupera Operacoes de Cobertura por Colaborador ***/
						StringBuffer queryOperacaoPessoa = new StringBuffer();
						queryOperacaoPessoa.append(" SELECT cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, cob.usu_datalt, cob.usu_datfim, fun.nomfun, car.titred, fun2.NumCad AS NumCadCob, esc.NomEsc, fun2.NomFun AS NomFun2, fun2.CodTma ");
						queryOperacaoPessoa.append(" FROM USU_T038COBFUN cob WITH (NOLOCK) ");
						queryOperacaoPessoa.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad  ");
						queryOperacaoPessoa.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
						queryOperacaoPessoa.append(" INNER JOIN R034FUN fun2 WITH (NOLOCK) ON fun2.NumEmp = cob.USU_NumEmpCob AND fun2.TipCol = cob.USU_TipColCob AND fun2.NumCad = cob.USU_NumCadCob ");
						queryOperacaoPessoa.append(" INNER JOIN R006ESC esc WITH (NOLOCK) ON esc.CodEsc = fun2.CodEsc ");
						queryOperacaoPessoa.append(" WHERE cob.usu_numempcob = ? AND cob.usu_tipcolcob = ? AND cob.usu_numcadcob = ? AND GETDATE() BETWEEN cob.USU_DatAlt AND cob.USU_DatFim ");
						
						PreparedStatement stOperacaoPessoa = connVetorh.prepareStatement(queryOperacaoPessoa.toString());
						stOperacaoPessoa.setLong(1, rsColaborador.getLong("NumEmp"));
						stOperacaoPessoa.setLong(2, rsColaborador.getLong("TipCol"));
						stOperacaoPessoa.setLong(3, rsColaborador.getLong("NumCad"));
						ResultSet rsOperacaoPessoa = stOperacaoPessoa.executeQuery();
						while(rsOperacaoPessoa.next()) {
							EntradaAutorizadaVO entradaVO = new EntradaAutorizadaVO();
							entradaVO.setNumeroEmpresa(rsOperacaoPessoa.getLong("usu_numemp"));
							entradaVO.setTipoColaborador(rsOperacaoPessoa.getLong("usu_tipcol"));
							entradaVO.setNumeroCadastro(rsOperacaoPessoa.getLong("usu_numcad"));
							entradaVO.setNomeColaborador(rsOperacaoPessoa.getString("nomfun"));
							entradaVO.setCargo(rsOperacaoPessoa.getString("titred"));
							entradaVO.setEscala(rsOperacaoPessoa.getString("nomesc") + " / " + rsOperacaoPessoa.getInt("CodTma"));
							GregorianCalendar calUtil = new GregorianCalendar();
							calUtil.setTime(rsOperacaoPessoa.getTimestamp("usu_datalt"));
							entradaVO.setDataInicial(calUtil);
							calUtil = new GregorianCalendar();
							calUtil.setTime(rsOperacaoPessoa.getTimestamp("usu_datfim"));
							entradaVO.setDataFinal(calUtil);
							entradaVO.setNomeColaboradorSubtituido(rsOperacaoPessoa.getString("NomFun2"));
							// Adicionando na lista geral de entradas autorizadas
							entradasAutorizadas.add(entradaVO);
						}						
						%>
						<tr>
							<td><%=rsColaborador.getInt("NumEmp")%></td>
							<td><%=rsColaborador.getInt("NumCad")%></td>
							<td><%=rsColaborador.getString("NomFun")%></td>
							<td><%=rsColaborador.getString("TitRed")%></td>
							<td><%=rsColaborador.getString("NomEsc")%> / <%=rsColaborador.getInt("CodTma")%></td>
							<td><%=rsColaborador.getString("DesSit")%></td>
							<%
													
							
							/*** Recupera Presenca ***/
							StringBuffer queryPresenca = new StringBuffer();
							queryPresenca.append(" SELECT DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) AS DatAcc, acc.DirAcc, acc.NumCra ");
							queryPresenca.append(" FROM R070ACC acc, R038HCH hch, R034FUN fun ");
							queryPresenca.append(" WHERE hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= GETDATE()) ");
							queryPresenca.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = (SELECT MAX(DATEADD(MINUTE, acc2.HorAcc, acc2.DatAcc)) FROM R070ACC acc2 WHERE acc.NumCra = acc2.NumCra) ");
							queryPresenca.append(" AND acc.SeqAcc = (SELECT MAX(acc3.SeqAcc) FROM R070ACC acc3 WHERE acc.NumCra = acc3.NumCra AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = DATEADD(MINUTE, acc3.HorAcc, acc3.DatAcc)) ");
							queryPresenca.append(" AND acc.NumCra = hch.NumCra ");
							queryPresenca.append(" AND fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? ");

							PreparedStatement stPresenca = connVetorh.prepareStatement(queryPresenca.toString());
							stPresenca.setInt(1, rsColaborador.getInt("NumEmp"));
							stPresenca.setInt(2, rsColaborador.getInt("TipCol"));
							stPresenca.setInt(3, rsColaborador.getInt("NumCad"));
							//System.out.println(rsColaborador.getInt("NumCad"));
							ResultSet rsPresenca = stPresenca.executeQuery();
							
							if (rsPresenca.next()) {
								String direcaoAcesso = rsPresenca.getString("DirAcc");
								Timestamp dataAcesso = rsPresenca.getTimestamp("DatAcc");
								if (direcaoAcesso != null && direcaoAcesso.equals("E")) {
									%>
									<td><%=formatadorDataHora.format(dataAcesso)%></td>
									<td>&nbsp;</td>
									<td><img src="imagens/custom/online.png" alt="Presente"/></td>
									<%
								} else {
		
									/*** Recupera Ultima Entrada ***/
									StringBuffer queryPresencaEntrada = new StringBuffer();
									queryPresencaEntrada.append(" SELECT DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) AS DatAcc, acc.DirAcc, acc.NumCra ");
									queryPresencaEntrada.append(" FROM R070ACC acc, R038HCH hch, R034FUN fun ");
									queryPresencaEntrada.append(" WHERE hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= GETDATE()) ");
									queryPresencaEntrada.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = (SELECT MAX(DATEADD(MINUTE, acc2.HorAcc, acc2.DatAcc)) FROM R070ACC acc2 WHERE acc.NumCra = acc2.NumCra AND acc2.DirAcc = 'E') ");
									queryPresencaEntrada.append(" AND acc.SeqAcc = (SELECT MAX(acc3.SeqAcc) FROM R070ACC acc3 WHERE acc.NumCra = acc3.NumCra AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = DATEADD(MINUTE, acc3.HorAcc, acc3.DatAcc) AND acc3.DirAcc = 'E') ");
									queryPresencaEntrada.append(" AND acc.NumCra = hch.NumCra ");
									queryPresencaEntrada.append(" AND fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ?  ");

									PreparedStatement stPresencaEntrada = connVetorh.prepareStatement(queryPresencaEntrada.toString());
									stPresencaEntrada.setInt(1, rsColaborador.getInt("NumEmp"));
									stPresencaEntrada.setInt(2, rsColaborador.getInt("TipCol"));
									stPresencaEntrada.setInt(3, rsColaborador.getInt("NumCad"));
									ResultSet rsPresencaEntrada = stPresencaEntrada.executeQuery();
									
									if (rsPresencaEntrada.next()) {
										Timestamp dataEntrada = rsPresencaEntrada.getTimestamp("DatAcc");
										%>
										<td><%=formatadorDataHora.format(dataEntrada)%></td>
										<%
									} else {
										%>
										<td>&nbsp;</td>
										<%
									}							
									%>
									<td><%=formatadorDataHora.format(dataAcesso)%></td>
									<%
								
								%>
								<td><img src="imagens/custom/offline.png" alt="Ausente"/></td>
								<%
								} 
							} else {
								%>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								<td><img src="imagens/custom/na.png" alt="Indefinido"/></td>
								<%
							}	
															
							%>
						</tr>
						<%
					
					}	
					
					%>
					</tbody>
					</table>
					
					</td>
					<%
					if (!entradasAutorizadas.isEmpty()) {
					%>
					<tr>
						<td colspan="7">
							<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
							<tr style="cursor: auto" style="background-color: #F0E68C;">
								<th style="cursor: auto" width="5%"><img src="imagens/icones_final/star_16x16.png" alt="Autoriza��o"/>Emp.</th>
								<th style="cursor: auto" width="5%">Matr�cula</th>
								<th style="cursor: auto" width="30%">Colaborador</th>
								<th style="cursor: auto" width="15%">Cargo</th>
								<th style="cursor: auto" width="15%">Substituindo</th>
								<th style="cursor: auto" width="15%">Per�odo</th>
								<th style="cursor: auto" width="15%">Escala Autorizada</th>
								<th style="cursor: auto" width="8%">Entrada</th>
								<th style="cursor: auto" width="8%">Sa�da</th>
								<th style="cursor: auto">&nbsp;</th>
							</tr>
							<%
							for(EntradaAutorizadaVO entradaAutorizada : entradasAutorizadas){
							%>
								<tbody>	
								<tr style="background-color: #F0E68C;">
									<td><%= entradaAutorizada.getNumeroEmpresa() %></td>
									<td><%= entradaAutorizada.getNumeroCadastro() %></td>
									<td><%= entradaAutorizada.getNomeColaborador() %></td>
									<td><%= entradaAutorizada.getCargo() %></td>
									<td><%= entradaAutorizada.getNomeColaboradorSubtituido() %></td>
									<td><%= NeoUtils.safeDateFormat(entradaAutorizada.getDataInicial(), "dd/MM/yyyy") %> a <%= NeoUtils.safeDateFormat(entradaAutorizada.getDataFinal(), "dd/MM/yyyy") %></td>
									<td><%= entradaAutorizada.getEscala() %></td>
									<%
									/*** Recupera Presenca Autorizadas ***/
									StringBuffer queryPresencaAutorizada = new StringBuffer();
									queryPresencaAutorizada.append(" SELECT DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) AS DatAcc, acc.DirAcc, acc.NumCra ");
									queryPresencaAutorizada.append(" FROM R070ACC acc, R038HCH hch, R034FUN fun ");
									queryPresencaAutorizada.append(" WHERE hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= GETDATE()) ");
									queryPresencaAutorizada.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = (SELECT MAX(DATEADD(MINUTE, acc2.HorAcc, acc2.DatAcc)) FROM R070ACC acc2 WHERE acc.NumCra = acc2.NumCra) ");
									queryPresencaAutorizada.append(" AND acc.SeqAcc = (SELECT MAX(acc3.SeqAcc) FROM R070ACC acc3 WHERE acc.NumCra = acc3.NumCra AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = DATEADD(MINUTE, acc3.HorAcc, acc3.DatAcc)) ");
									queryPresencaAutorizada.append(" AND acc.NumCra = hch.NumCra ");
									queryPresencaAutorizada.append(" AND fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? ");
		
									PreparedStatement stPresencaAutorizada = connVetorh.prepareStatement(queryPresencaAutorizada.toString());
									stPresencaAutorizada.setLong(1, entradaAutorizada.getNumeroEmpresa());
									stPresencaAutorizada.setLong(2, entradaAutorizada.getTipoColaborador());
									stPresencaAutorizada.setLong(3, entradaAutorizada.getNumeroCadastro());
									//System.out.println(rsColaborador.getInt("NumCad"));
									ResultSet rsPresencaAutorizada = stPresencaAutorizada.executeQuery();
									
									if (rsPresencaAutorizada.next()) {
										String direcaoAcesso = rsPresencaAutorizada.getString("DirAcc");
										Timestamp dataAcesso = rsPresencaAutorizada.getTimestamp("DatAcc");
										if (direcaoAcesso != null && direcaoAcesso.equals("E")) {
											%>
											<td><%=formatadorDataHora.format(dataAcesso)%></td>
											<td>&nbsp;</td>
											<td><img src="imagens/custom/online.png" alt="Presente"/></td>
											<%
										} else {
				
											/*** Recupera Ultima Entrada Autorizada ***/
											StringBuffer queryPresencaEntradaAutorizada = new StringBuffer();
											queryPresencaEntradaAutorizada.append(" SELECT DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) AS DatAcc, acc.DirAcc, acc.NumCra ");
											queryPresencaEntradaAutorizada.append(" FROM R070ACC acc, R038HCH hch, R034FUN fun ");
											queryPresencaEntradaAutorizada.append(" WHERE hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= GETDATE()) ");
											queryPresencaEntradaAutorizada.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = (SELECT MAX(DATEADD(MINUTE, acc2.HorAcc, acc2.DatAcc)) FROM R070ACC acc2 WHERE acc.NumCra = acc2.NumCra AND acc2.DirAcc = 'E') ");
											queryPresencaEntradaAutorizada.append(" AND acc.SeqAcc = (SELECT MAX(acc3.SeqAcc) FROM R070ACC acc3 WHERE acc.NumCra = acc3.NumCra AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = DATEADD(MINUTE, acc3.HorAcc, acc3.DatAcc) AND acc3.DirAcc = 'E') ");
											queryPresencaEntradaAutorizada.append(" AND acc.NumCra = hch.NumCra ");
											queryPresencaEntradaAutorizada.append(" AND fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ?  ");
		
											PreparedStatement stPresencaEntradaAutorizada = connVetorh.prepareStatement(queryPresencaEntradaAutorizada.toString());
											stPresencaEntradaAutorizada.setLong(1, entradaAutorizada.getNumeroEmpresa());
											stPresencaEntradaAutorizada.setLong(2, entradaAutorizada.getTipoColaborador());
											stPresencaEntradaAutorizada.setLong(3, entradaAutorizada.getNumeroCadastro());
											ResultSet rsPresencaEntradaAutorizada = stPresencaEntradaAutorizada.executeQuery();
											
											if (rsPresencaEntradaAutorizada.next()) {
												Timestamp dataEntrada = rsPresencaEntradaAutorizada.getTimestamp("DatAcc");
												%>
												<td><%=formatadorDataHora.format(dataEntrada)%></td>
												<%
											} else {
												%>
												<td>&nbsp;</td>
												<%
											}							
											%>
											<td><%=formatadorDataHora.format(dataAcesso)%></td>
											<%
										
										%>
										<td><img src="imagens/custom/offline.png" alt="Ausente"/></td>
										<%
										} 
									} else {
										%>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										<td><img src="imagens/custom/na.png" alt="Indefinido"/></td>
										<%
									}	
																	
									%>
								</tr>
							<%
							}
							%>
							</tbody>
							</table>
							
						</td>
					</tr>					
					<%
					}
					%>
					
				</tr>
			<%
			
		}
	} catch (IllegalStateException e) {
			e.printStackTrace();
	}
%>
	
	</cw:body>
</cw:main>
</portal:head>


<script language="JavaScript">

function doAction(acao, empresa, matricula) {

	var response = "";
	
	response = callSync('<%= PortalUtil.getBaseURL() %>custom/jsp/orsegups/presenca/presencaviaSNEP.jsp?verboseError=yes&fone=fusion&acao=' + acao + '&numEmp='+ empresa + '&numCad='+ matricula);

	if (response.indexOf("OK") != -1) {
		alert("OK - A��o realizada com sucesso.");
	} else {
		alert("ERRO: " + response);
	}
		
	
}
</script>