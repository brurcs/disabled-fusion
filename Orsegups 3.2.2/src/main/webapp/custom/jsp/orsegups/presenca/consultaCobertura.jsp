<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@ page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta http-equiv="Pragma" content="no-cache">
 <meta http-equiv="Cache-Control" content="no-cache">
 <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<title>Consulta de Coberturas</title>
</head>
<body>
</body>
</html>
	
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>

<portlet:defineObjects />
<form:defineObjects />

<portal:head title="Consulta de Coberturas">
<cw:main>
	<cw:header title="Consulta de Coberturas" />
	
	<cw:body id="area_scroll">

				
   		<div id="dados">
		<fieldset class="fieldGroup">
		<legend class="legend" hasbox="2">Filtros</legend>
		<OL hasbox="2">
		<div class="fieldSet" hasbox="2">
		<table width="100%">
		 <tr>
		    <td>
		     <LI>
		      <label class="formFieldLabel">Colaborador:&nbsp;</label>&nbsp;
	          <input type="text"class="input_text" name='txt_colaborador' id='idTxtColaborador' required='false' /> 
	          <input id='idColaborador' name='idColaborador' type='hidden'/>
	          <script type="text/javascript">
                   createAutoComplete(myId('idColaborador'), 'idTxtColaborador', 'VETORHUSUFUNFUSION', 'numcad;nomfun', null);
              </script>
             </LI>
		    </td>
		 </tr>
		 <tr>
		  <td>
		    <LI>
		      <label class="formFieldLabel">Colaborador a Substituir:&nbsp;</label>&nbsp;
	          <input type="text"class="input_text" name='txt_colaboradorSub' id='idTxtColaboradorSub' required='false' /> 
	          <input id='idColaboradorSub' name='idColaboradorSub' type='hidden'/>
	          <script type="text/javascript">
                   createAutoComplete(myId('idColaboradorSub'), 'idTxtColaboradorSub', 'VETORHUSUFUNFUSION', 'numcad;nomfun', null);
              </script>
		    </LI>
		  </td>
		</tr>
		<tr>
		  <td>
		  	<LI>
		   <label class="formFieldLabel">Posto:&nbsp;</label>&nbsp;
	       <input type="text"class="input_text" name='txt_posto' id='idTxtPosto' required='false' /> 
	       <input id='idPosto' name='idPosto' type='hidden'/>
	       <script type="text/javascript">
                   createAutoComplete(myId('idPosto'), 'idTxtPosto', 'VETORH_USU_Vorg203nv8', 'usu_codccu;nomloc', null);
           </script>
		    </LI>
		   </td>
		</tr>
		 <tr>
			<td>
			  <LI>
			    <label class="formFieldLabel">Data Inicial de:&nbsp;</label>&nbsp;
	             <input class="input_text" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)"
	              name= 'var_dataInicialDe' errorSpan='err_var_dataInicialDe' id='var_dataInicialDe' 
	              required='false' onchange=''/><span id='err_var_dataInicialDe' style='margin-left:0px;'></span>&nbsp;<img src='imagens/icones_final/eraser_16x16-trans.png' class='ico' onclick="javascript: limpaCampo('var_dataInicialDe');">&nbsp;<img src='imagens/icones_final/date_16x16-trans.png' class='ico' id='btdataInicialDe' name='btdataInicialDe'  onclick="return showNeoCalendar('var_dataInicialDe', '%d/%m/%Y','btdataInicialDe', event);">
		          <br>
		          <span class="fieldHelp" >Utilize o formato DD/MM/YYYY</span>
		      </LI>
			</td>
			<td>
			  <label class="formFieldLabel">Data Inicial at�:&nbsp;</label>&nbsp;
	           <input class="input_text" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)"
	             name= 'var_dataInicialAte' errorSpan='err_var_dataInicialAte' id='var_dataInicialAte' 
	             required='false' onchange=''/><span id='err_var_dataInicialAte' style='margin-left:0px;'></span>&nbsp;<img src='imagens/icones_final/eraser_16x16-trans.png' class='ico' onclick="javascript: limpaCampo('var_dataInicialAte');">&nbsp;<img src='imagens/icones_final/date_16x16-trans.png' class='ico' id='btdataInicialAte' name='btdataInicialAte'  onclick="return showNeoCalendar('var_dataInicialAte', '%d/%m/%Y','btdataInicialAte', event);">
		         <br>
		         <span class="fieldHelp" >Utilize o formato DD/MM/YYYY</span>
			</td>
		 </tr>
		 <tr>
			<td>
			  <LI>
			    <label class="formFieldLabel">Data Final de:&nbsp;</label>&nbsp;
	             <input class="input_text" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)"
	              name= 'var_dataFinalDe' errorSpan='err_var_dataFinalDe' id='var_dataFinalDe' 
	              required='false' onchange=''/><span id='err_var_dataFinalDe' style='margin-left:0px;'></span>&nbsp;<img src='imagens/icones_final/eraser_16x16-trans.png' class='ico' onclick="javascript: limpaCampo('var_dataFinalDe');">&nbsp;<img src='imagens/icones_final/date_16x16-trans.png' class='ico' id='btdataFinalDe' name='btdataFinalDe'  onclick="return showNeoCalendar('var_dataFinalDe', '%d/%m/%Y','btdataFinalDe', event);">
		          <br>
		          <span class="fieldHelp" >Utilize o formato DD/MM/YYYY</span>
		      </LI>
			</td>
			<td>
			  <label class="formFieldLabel">Data Final at�:&nbsp;</label>&nbsp;
	           <input class="input_text" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)"
	             name= 'var_dataFinalAte' errorSpan='err_var_dataFinalAte' id='var_dataFinalAte' 
	             required='false' onchange=''/><span id='err_var_dataFinalAte' style='margin-left:0px;'></span>&nbsp;<img src='imagens/icones_final/eraser_16x16-trans.png' class='ico' onclick="javascript: limpaCampo('var_dataFinalAte');">&nbsp;<img src='imagens/icones_final/date_16x16-trans.png' class='ico' id='btdataFinalAte' name='btdataFinalAte'  onclick="return showNeoCalendar('var_dataFinalAte', '%d/%m/%Y','btdataFinalAte', event);">
		         <br>
		         <span class="fieldHelp" >Utilize o formato DD/MM/YYYY</span>
			</td>
		 </tr>
		</table>
		<LI>
			<input type="button"class="input_button"id="consulta"value="Consultar">
		</LI>
		</div>
	 			</OL>
		</fieldset>

	<div id=resultadoConsulta>

	</div>

	<script type="text/javascript">

		$(document).ready(function() 
			{
		 		$('#consulta').click(function() 
		   	 	{
		    	  var colaborador    = $('#idTxtColaborador').val();
		   		  var colaboradorSub = $('#idTxtColaboradorSub').val();
		   		  var posto          = $('#idTxtPosto').val();
		   		  var dataInicialDe  = $('#var_dataInicialDe').val();
		   		  var dataInicialAte = $('#var_dataInicialAte').val();
		   		  var dataFinalDe	 = $('#var_dataFinalDe').val();
		   		  var dataFinalAte	 = $('#var_dataFinalAte').val();
		   		  
		   		  if(dataInicialDe == null || dataInicialDe == '' ){
		   			  alert('O campo Data Inicial de: n�o foi preenchido.');
		   			  document.getElementById('var_dataInicialDe').focus();
		   			  return;
		   		  }
		   		  
		   		 if(dataFinalDe == null || dataFinalDe == ''){
		   			  alert('O campo Data Final de: n�o foi preenchido.');
		   			  document.getElementById('var_dataFinalDe').focus();
		   			  return;
		   		  }

		   		  var result = '<%= PortalUtil.getBaseURL() %>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=consultaCobertura&colaborador='+colaborador+'&colaboradorSub='+colaboradorSub+'&posto='+posto+'&dataInicialDe='+dataInicialDe+'&dataInicialAte='+dataInicialAte+'&dataFinalDe='+dataFinalDe+'&dataFinalAte='+dataFinalAte;

		   		  	$.ajax({
			  		  url: result,
			  		  success: function( data ) {
			  			  $('#resultadoConsulta').html(data);

			  		  }
			  		});
		   		 }
		 		);
			}
		);
		
	function limpaCampo(idCampo)
	{
		$('#'+idCampo).val("");
		$('#'+idCampo).removeAttr("selectedvalue");
	}
	
	function deleteRegistroCobertura(numEmp, tipCol, numCad, dataInicial, horaInicial)
	{
		var isDelete = confirm("Deseja excluir o registro de cobertura?");

		if(isDelete)
		{
			var	result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=deleteRegistroCobertura&numEmp="+numEmp+"&tipCol="+tipCol+"&numCad="+numCad+"&dataInicial="+dataInicial+"&horaInicial="+horaInicial);
			if(result == 0){
			  alert('N�o foi poss�vel excluir o registro de cobertura.');
			}
			if(result > 0){
			  alert('Registro exclu�do com sucesso.');
			  document.getElementById("consulta").click();
			}
		}
	}

	</script>
	</cw:body>
	
</cw:main>
</portal:head>