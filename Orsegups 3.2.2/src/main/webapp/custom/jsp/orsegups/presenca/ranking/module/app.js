(function (){
	
	
	var app = angular.module('rankingHoristas',[]);
	
	
	app.factory('mainInfo', function($http) { 
    
	    var permissoes = $http.get('https://intranet.orsegups.com.br/fusion/services/horistas/getPermissaoExibicao/').then(function(response){ 
	    	console.log(response.data);
	        return response.data;
	    });    

	    return {
	        getPermissoes: permissoes
	    };  
	       
	});
	
	

	app.controller('RankingController', function(mainInfo, $scope, $http, $timeout) {
		
		$scope.horistas = [];
		
		$scope.permissoes = [];
		
		$scope.jumbo = "jumbo"; 
		
		$scope.opcoesAnos =  getYears();
		
		$scope.anoSelecionado = $scope.opcoesAnos[0];
				
		$scope.opcoesMes =  getMonths();
		
		var d = new Date();
		var mes = d.getMonth();
		
		$scope.mesSelecionado = $scope.opcoesMes[mes];
		
		
		//AÇÕES A SEREM EXECUTADAS APÓS CARREGAR AS PERMISSOES
		mainInfo.getPermissoes.then(function(records) {
			$scope.permissoes = records;
			
		});
		
		
	    $timeout(function () {
			var tamanho = Object.keys($scope.permissoes).length
			
			if (tamanho == 1){
				$('button:first').click();
			}
	    }, 1000);
				
		$scope.ranking = function(regional) {

			$http.get("https://intranet.orsegups.com.br/fusion/services/horistas/ranking/"+ regional+"/"+$scope.mesSelecionado+"/"+$scope.anoSelecionado+"").success(function(data, status) {
				$scope.jumbo = "jumbo-hidden";
				$scope.horistas = data;
				console.log(data);
				$('#processing-modal').modal('hide');
			}).error(function() {
				alert("Não foi possível carregar os dados");
			});
			
		}
		
		
		$scope.showFichaColaborador = function(numemp, tipcol, numcad, dep, codccu) {
			var caminho = 'https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/presenca/ranking/fichaColaborador.jsp?numemp='+numemp+'&tipcol='+tipcol+'&numcad='+numcad+'&dep='+dep+'&codccu='+codccu;
			
			console.log(caminho);

			$('#modalFichaColaborador').window('open');
			$('#modalFichaColaborador').window('refresh', caminho);
		}
			
			
	});
	
	
	
	function getYears(){
		var d = new Date();
		var ano = d.getFullYear();
		
		var yearList = [];
		
		
		var anoCorrente = ano;
		for (i= 0; i<4; i++){
			yearList.push(anoCorrente);
			anoCorrente --;
		}
		
		return yearList;
	}
	
	function getMonths(){
				
		var monthList = [];
		
		for (var i= 1; i<13; i++){
			if (i < 10){
				monthList.push('0'+i);
			}else{
				monthList.push(i);
			}


		}
		
		return monthList;
	}
	
	

})();
