<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.PostoSupervisaoVO"%>
<%@page import="com.neomind.util.ReflectionUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.text.ParseException"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.AcessoVO"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page import="java.io.OutputStream"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   

<%

	GregorianCalendar dataFiltro = new GregorianCalendar();

	NeoUser user = PortalUtil.getCurrentUser();

	NeoPaper papelQlDiretoria = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Quadro de Lotação Presença - Diretoria"));
	NeoPaper papelQlValorPosto = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Quadro de Lotação Presença - Valor Posto"));
	NeoPaper papelPresencaSupervisao = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","presencaSupervisao"));
	NeoPaper papelPresencaVinculaContaAlarme = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","PresencaVinculaContaAlarme"));
	NeoPaper PapelBiPresenca = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","biPresenca"));
	NeoPaper vincularSupervisaoPosto = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","vincularSupervisaoPosto"));
	
	
	Boolean isQlDiretoria = Boolean.FALSE;
	Boolean isQlValorPosto = Boolean.FALSE;
	Boolean isPresencaSupervisao = Boolean.FALSE;
	Boolean isPresencaVinculaContaAlarme = Boolean.FALSE;
	Boolean isBiPresenca = Boolean.FALSE;
	Boolean isVincularSupervisaoPosto = Boolean.FALSE;
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelQlDiretoria)) {
		isQlDiretoria = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelQlValorPosto)) {
		isQlValorPosto = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelPresencaSupervisao)) {
		isPresencaSupervisao = Boolean.TRUE;
	}
	
	if (user != null && user.getPapers() != null && user.getPapers().contains(papelPresencaVinculaContaAlarme)){
		isPresencaVinculaContaAlarme = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(PapelBiPresenca)) {
		isBiPresenca = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(vincularSupervisaoPosto)) {
		isVincularSupervisaoPosto = Boolean.TRUE;
	}
	
	
	
	GregorianCalendar dataAtual = new GregorianCalendar(); 

	String numloc = request.getParameter("numloc");
	String taborg = request.getParameter("taborg");
	
	Long numlocL = Long.parseLong(numloc);
	Long taborgL = Long.parseLong(taborg);
	
	PostoVO posto = new PostoVO();
	posto = QLPresencaUtils.buscaFichaPosto(numlocL, taborgL, dataAtual);
	
	String valor = "";
	SimpleDateFormat ano = new SimpleDateFormat("yyyy");
	
	if (NeoUtils.safeIsNotNull(posto))
	{
		String formato = "R$ #,##0.00";
		DecimalFormat df = new DecimalFormat(formato);
		valor = df.format(posto.getValor());
	}
	
	String raizQL = request.getParameter("raizQL");
	
	boolean isPostoAdm = false; // verdadeiro se o posto for da raiz administrativa
	
	if ("".equals(raizQL) || "null".equals(raizQL) || raizQL == null){
		isPostoAdm = false;
	}else{
		isPostoAdm = true;
	}
	
	String obsComercial = NeoUtils.safeOutputString( QLPresencaUtils.retornaObservacoesComerciaisContrato(posto.getNumeroContrato(), posto.getNumemp()) ) ;
	
	String strTimeMillisDataAtual = request.getParameter("timeMillisDataAtual");
	if (strTimeMillisDataAtual != null){
		dataFiltro.setTimeInMillis(NeoUtils.safeLong(strTimeMillisDataAtual));
		System.out.println("fichaPosto->dataFiltro --> " + NeoUtils.safeDateFormat(dataFiltro));
	}
%>

<html>
<body>
	
    <div id="tabPosto" class="easyui-tabs" style="width: auto;height:630px" data-options="tabHeight:55">
        <div title="<span class='tt-inner'><img src='imagens/custom/home_32x32.png'/><br>Dados Gerais</span>" style="padding:10px; width: auto">
			<div style="border: 2px solid #95b8e7; width: auto; height: auto">    
				<table style="width: 95%">
					<tbody>
						<tr>
							<td class="flabel">Controla Aso:</td>
							<td class="fvalue">
								<% 
								String paramControlarASO[] = { "controlarASO" };
								Boolean controlaASO = controlaASO = QLPresencaUtils.contratoControlaASO(posto.getNumeroContrato(), posto.getNumeroPosto()); 
								String disabledControlaAso = "disabled";
								if ((Boolean) ReflectionUtils.invokeMethod("com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils", "checkPaperPermission", paramControlarASO )) {
									disabledControlaAso =  ("Encerrando".equals(posto.getSituacao()) ? "disabled" :"");
								} 
								%>
								<select <%= disabledControlaAso %> id="cbCtrAso" class="easyui-combobox" data-options="
							        		   		valueField:'vlctraso',
							        		   		textField:'ctraso',
							        		   		editable:false,
							        		   		multiple:false,
									           		onSelect: function(record){
										           		if ('<%= posto.getSituacao() %>' == 'Encerrando'){
										           			alert('Não é possível alterar ASO de postos Inativos ou em encerramento');
										           			return false;
										           		}else{
											           			var data = 'action=controlaASO&numposto=<%=posto.getNumeroPosto()%>&numctr=<%=posto.getNumeroContrato()%>&numemp=<%= posto.getNumemp() %>&numfil=<%= posto.getNumfil() %>&usuario=<%= PortalUtil.getCurrentUser().getCode() %>&codser=<%= posto.getCodSer() %>&ctraso=' +record.vlctraso;
											           			startProgress();
													            $.ajax({
													            	method: 'post',
													                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
													                data: data,
													                dataType: 'text',
													                success: function(result){
													                	stopProgress();
													                    alert('ASO atualizado');
													                }
													            });
											          	}
											        }">
											        <%
											        if (controlaASO){
											        %>
											        <option value="0" >Não</option>
											        <option value="1" selected="selected" >Sim</option>
											        <%}else{ %>
											        <option value="0" selected="selected" >Não</option>
											        <option value="1" >Sim</option>
											        <%} %>
										</select>
							</td>
						</tr>
						
						
						<% 
								Boolean isPostoSupervisao = QLPresencaUtils.isPostoSupervisao(posto.getNumeroLocal(), NeoUtils.safeLong(posto.getCentroCusto()));
								
								String disabledPostoSupervisao = "disabled";
								if ( isVincularSupervisaoPosto /*&& isPostoAdm */) {
									disabledPostoSupervisao =  "";
								} 
						%>
						
						<tr style="<%= isPostoAdm ? "":"" %>">
							<td class="flabel">É posto de Supervisão?</td>
							<td class="fvalue">
								
								<select <%= disabledPostoSupervisao %> id="cbPostoSupervisao" class="easyui-combobox" data-options="
							        		   		valueField:'isPostoSup',
							        		   		textField:'txtIsPostoSup',
							        		   		editable:false,
							        		   		multiple:false,
									           		onSelect: function(record){
										           		if ('<%= posto.getSituacao() %>' == 'Encerrando'){
										           			alert('Não é possível ativar/desativar supervisão em postos Inativos ou em encerramento');
										           			return false;
										           		}else{
											           			var data = 'action=atualizaPostoSup&codccu=<%=posto.getCentroCusto()%>&numloc=<%=posto.getNumeroLocal()%>&usuario=<%= PortalUtil.getCurrentUser().getCode() %>&isPostoSup=' +record.isPostoSup;
											           			startProgress();
													            $.ajax({
													            	method: 'post',
													                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
													                data: data,
													                dataType: 'text',
													                success: function(result){
													                	stopProgress();
													                	alert('Posto de supervisão alterado para ' + isPostoSup);
													                    
													                }
													            });
											          	}
											        }" style="width: 400px;">
											        <option value="0" <%= isPostoSupervisao == false ? "selected":"" %> > N&atilde;o </option>
											        <option value="1" <%= isPostoSupervisao == true ? "selected":"" %> > Sim </option>
								</select>
							</td>
						</tr>
						
						
								
						<tr>
							<td class="flabel">Cliente:</td>
							<td class="fvalue"><%=posto.getCliente().getCodigoCliente() + " - " + posto.getCliente().getNomeCliente()%></td>
						</tr>
						<tr>
							<td class="flabel">Obs. Comercial:</td>
							<td class="fvalue"><%= obsComercial %></td>
						</tr>
						<tr>
							<td class="flabel">CPF/CNPJ:</td>
							<td class="fvalue"><%=posto.getCliente().getCpfCnpj()%></td>
						</tr>
						<tr>
							<td class="flabel">Telefone Cliente:</td>
							<td class="fvalue"><%=posto.getCliente().getTelefone()%></td>
						</tr>
						<tr>
							<td class="flabel">Contrato:</td>
							<td class="fvalue"><%=posto.getNumeroContrato()%></td>
						</tr>
						<tr>
							<td class="flabel">Posto:</td>
							<td class="fvalue"><%=posto.getNumeroPosto()%></td>
						</tr>
						<tr>
							<td class="flabel">Descrição:</td>
							<td class="fvalue"><%=posto.getLotacao() + " - " + posto.getNomePosto()%></td>
						</tr>
						<tr>
							<td class="flabel">Situação:</td>
							<td class="fvalue"><%=posto.getSituacao()%></td>
						</tr>
					<% if(posto.getMotivoEncerramento() != null) { %>	
						<tr>
							<td class="flabel">Motivo Encerramento:</td>
							<td class="fvalue"><%=posto.getMotivoEncerramento()%></td>
						</tr>
						<tr>
							<td class="flabel">Observação do Encerramento:</td>
							<td class="fvalue"><%=posto.getObsEncerramento()%></td>
						</tr>
					<% } %>		
						<tr>
							<td class="flabel">Serviço:</td>
							<td class="fvalue"><%=posto.getServico() %></td>
						</tr>
						<tr>
							<td class="flabel">Endereço:</td>
							<td class="fvalue"><%=posto.getEndereco() %></td>
						</tr>
						
					<% if(posto.getNumero() != null) { %>
					
						<tr>
							<td class="flabel">Nº/Complemento:</td>
							<td class="fvalue"><%=posto.getNumero() %></td>
						</tr>
						
					<% } %>	
					
					<% if(posto.getBairro() != null) { %>
					
						<tr>
							<td class="flabel">Bairro:</td>
							<td class="fvalue"><%=posto.getBairro()%></td>
						</tr>
						
					<% } %>	
					
					<tr>
						<td class="flabel">Cidade:</td>
						<td class="fvalue"><%=posto.getCidade()%></td>
					</tr>
					<tr>
						<td class="flabel">Estado:</td>
						<td class="fvalue"><%=posto.getEstado()%></td>
					</tr>
					<tr>
						<td class="flabel">CEP:</td>
						<td class="fvalue"><%=posto.getCep()%></td>
					</tr>
					<tr>
						<td class="flabel">Telefone Presença:</td>
						<% if(posto.getTelefone() != null) { %>
							<td class="fvalue"><%=posto.getTelefone()%></td>
						<% } else {%>
							<td class="fvalue">Não cadastrado</td>
						<% } %>		
					</tr>
						
					<% if(isQlDiretoria || isQlValorPosto) { %>
						<tr>
							<td class="flabel">Valor:</td>
							<td class="fvalue"><%=valor%></td>
						</tr>
					<% } %>			
					
					<tr>
						<td class="flabel">Qde. Funcionários:</td>
						<td class="fvalue"><%=posto.getVagas()%></td>
					</tr>
							
					<% if(posto.getDataInicio() != null && !ano.format(posto.getDataInicio()).equals("1900")) { %>
						<tr>
							<td class="flabel">Início Faturamento:</td>
							<td class="fvalue"><%=NeoUtils.safeDateFormat(posto.getDataInicio(), "dd/MM/yyyy")%></td>
						</tr>
					<% } %>		
					
					<% if(posto.getDataFim() != null && !ano.format(posto.getDataFim()).equals("1900")) { %>
						<tr>
							<td class="flabel">Fim Faturamento:</td>
							<td class="fvalue"><%=NeoUtils.safeDateFormat(posto.getDataFim(), "dd/MM/yyyy")%></td>
						</tr>
					<% } %>	
					
					<% if(posto.getDataFimVigencia() != null && !ano.format(posto.getDataFimVigencia()).equals("1900")) { %>
						<tr>
							<td class="flabel">Fim Vigência:</td>
							<td class="fvalue"><%=NeoUtils.safeDateFormat(posto.getDataFimVigencia(), "dd/MM/yyyy")%></td>
						</tr>
					<% } %>	
					
						
							
					</tbody>
				</table>
			</div>	
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/moto.png'/><br>Supervisão</span>" style="padding:10px">
			<table id="dgEventoX8" title="Eventos Supervisão" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		                <th field="centralParticao" width="70">Conta</th>
		                <th field="dataRecebido" width="70">Recebido</th>
		                <th field="dataDeslocamento" width="70">Deslocado</th>
		                <th field="dataLocal" width="70">Local</th>
		                <th field="dataFechamento" width="70">Fechado</th>
		                <th field="nomeViatura" width="140">Colaborador</th>
		                <th field="observacaoFechamento" width="260">Observação</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_cobertura_posto.png'/><br>Coberturas</span>" style="padding:10px">
			<table id="dgCobertura" title="COBERTURAS" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="percob" width="100">Período</th>
		           		<th field="tipcob" width="150">Cobertura</th>
		           		<th field="nomcob" width="150">Colaborador</th>
		           		<th field="nomsub" width="150">Substituído</th>
		                <th field="obscob" width="210">Observação</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_solicitacoes.png'/><br>Solicitações</span>" style="padding:10px">
			<table id="dgSolicitacao" title="SOLICITAÇÕES" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		            	<th field="link" width="25"></th>
		           		<th field="codigoTarefa" width="60">Tarefa</th>
		           		<th field="solicitante" width="150">Solicitante</th>
		           		<th field="categoria" width="150">Categoria</th>
		                <th field="dataInicio" width="110">Início</th>
		                <th field="dataConclusao" width="110">Conclusão</th>
		                <th field="situacao" width="110">Situação</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_reclamacao.png'/><br>Reclamações</span>" style="padding:10px">
			<table id="dgReclamacao" title="RECLAMAÇÕES" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		            	<th field="link" width="25"></th>
		           		<th field="codigoTarefa" width="60">Tarefa</th>
		           		<th field="solicitante" width="150">Solicitante</th>
		                <th field="dataInicio" width="110">Início</th>
		                <th field="dataConclusao" width="110">Conclusão</th>
		                <th field="situacao" width="110">Situação</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_visitas.png'/><br>Visitas</span>" style="padding:10px">
			
			<table id="dgVisita" title="VISITAS" class="easyui-datagrid" style="width:auto;height:auto"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		            	<th field="link" width="25"></th>
		           		<th field="dataAgenda" width="80">Agendado</th>
		           		<th field="dataRelato" width="80">Relatado</th>
		                <th field="contatoCliente" width="150">Contato</th>
		                <th field="area" width="150">Área</th>
		                <th field="telefone" width="110">Telefone</th>
		                <th field="email" width="160">E-mail</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<!-- Inspecoes -->
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_checklist.png'/><br>Checklist</span>" style="padding:10px">
			<table id="dgInspecao" title="Inspeções da qualidade" class="easyui-datagrid" style="width:auto;height:auto"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		                <th field="linkTarefa" width="100">Tarefa</th>
		                <th field="startDate" width="100">Data</th>
		                <th field="nomePesquisa" width="100">Inspeção</th>
		                <th field="inspetor" width="200">Inspetor</th>
		                <th field="situacao" width="200">situação</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_bi.png'/><br>BI</span>" style="padding:10px">
			
			<% if (isBiPresenca){ %>
				<%
				String param = "?";
				boolean firstRecord = true;
				List<String> codigosCLi = QLPresencaUtils.retornaArvoresCCCliente(NeoUtils.safeLong(posto.getCliente().getCodigoCliente()));
				if (codigosCLi == null){
					param += "label.cliente.codcliente="+ posto.getCodigoLocal().replace(".", "").substring(0,9);
				}else{
					for(String x: codigosCLi){
						param += (firstRecord ? "label.cliente.codcliente=" : "&label.cliente.codcliente=" );
						param += x;
						firstRecord = false;
					}
				}
				System.out.println("[BI PRESENCA] - codloc posto atual:" + posto.getCodigoLocal().replace(".", "").substring(0,9) );
				System.out.println("[BI PRESENCA] - " + param);
				%>
				<iframe frameborder="0" src="https://analytics.totvs.com.br/dashboard.html<%= param %>#project=/gdc/projects/mgojc3usjcd2j1pdh3x8ft1w1ujjp8zc&dashboard=/gdc/md/mgojc3usjcd2j1pdh3x8ft1w1ujjp8zc/obj/75239&tab=a2a211769fde" width="100%" height="100%" allowTransparency="false" ></iframe>
			<% } %>
			
		</div>
	
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_financas.png'/><br>Financeiro</span>" style="padding:10px">
			<% if (!isPresencaSupervisao) {%>
			<table id="dgFinanceiro" title="Financeiro - Posição do cliente (CNPJ)" class="easyui-datagrid" style="width:auto;height:auto"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		                <th field="datEmi" width="100">Emissão</th>
		           		<th field="numTit" width="100">Titulo</th>
		           		<th field="sitTit" width="50">Situação</th>
		           		<th field="vlrBse" width="80">Valor Bruto</th>
		           		<th field="vlrLiq" width="80">Valor Liquido</th>
		                <th field="vctPro" width="100">Vencimento</th>
		                <th field="vlrAbe" width="80">Valor Aberto</th>
		           		<th field="vlrPag" width="80">Valor Pago</th>
		           		<!-- 
		                <th field="vlrOri" width="80">Valor Original</th>
		                <th field="datPag" width="100">Dat. Pag. Prev.</th>
		           		 -->
		                
		            </tr>
		        </thead>
		    </table>
		    <% } %>
		</div>
		
		
		
		
		<div title="<span class='tt-inner'><img src='imagens/custom/pistol.png' style='width:32px; height:32px;' /><br>Armas</span>" style="padding:10px;">
			<table id="dgArma" title="Armas no Posto" class="easyui-datagrid" style="width:auto;height:auto" rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		                <th field="descricaoEspecie" width="80">Espécie</th>
						<th field="descricaoMarca" width="80">Marca</th>
						<th field="modelo" width="60">Modelo</th>
						<th field="numeroArma" width="70">Numero</th>
						<th field="numeroSinarm" width="70">Nº Sinarm</th>
						<th field="situacao" width="70">Situação</th>
						<th field="dataValidade" width="80">Validade</th>
						<th field="dataLotacao" width="80">Data Lotação</th>
						<th field="linkDetalhes" >Detalhes</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_anotacoes.png'/><br>Faltas de Efetivo</span>" style="padding:10px">
			<table id="dgFaltaEfetivo" title="Faltas de Efetivo" class="easyui-datagrid" style="width:auto;height:auto"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		            	<th field="code" width="80">Tarefa</th>
		           		<th field="startDate" width="80">Data</th>
		           		<th field="link" width="60">Exibir</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_equipamentos.png'/><br>Equipamentos</span>" style="padding:10px">
			<table id="dgEquipamento" title="EQUIPAMENTOS" class="easyui-datagrid" style="width:auto;height:auto"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="sequencia" width="30">Seq.</th>
		                <th field="codigoProduto" width="80">Cod. Produto</th>
		                <th field="tipoProduto" width="30">Tipo</th>
		                <th field="descricaoProduto" width="240">Produto</th>
		                <th field="quantidade" width="30">Qde.</th>
		                <th field="nomeInstalador" width="100">Instalador</th>
		                <th field="dataInstalacao" width="100">Data Instalação</th>
		                <th field="requisicao" width="60">Requisicao</th>
		                <th field="rmc" width="30">RMC</th>
		            </tr>
		        </thead>
		    </table>			
		</div>
		
		<%-- 
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_checklist.png'/><br>Checklist</span>" style="padding:10px">
			<table id="dgChecklist" title="CHECKLIST" class="easyui-datagrid" style="width:auto;height:412px"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="sequencia" width="30">Seq.</th>
		                <th field="codigoItem" width="80">Cod. Item</th>
		                <th field="item" width="370">Item</th>
		                <th field="quantidade" width="30">Qde.</th>
		                <th field="prazo" width="100">Prazo</th>
		                <th field="tarefa" width="60">Tarefa</th>
		                <th field="rmc" width="30">RMC</th>
		            </tr>
		        </thead>
		    </table>			
		</div> 
		--%>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_sigma.png'/><br>Contas Sigma</span>" style="padding:10px">
			<% if (!isPresencaSupervisao) {%>
			<table id="dgContaSigma" title="CONTAS SIGMA" class="easyui-datagrid" style="width:auto;height:auto"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		                <th field="linkEventos" width="60"></th>
		           		<th field="cdCliente" width="80">Cod. Cliente</th>
		                <th field="centralParticao" width="100">Conta</th>
		                <th field="empresa" width="60">Empresa</th>
		                <th field="fantasia" width="440">Nome Fantasia</th>
		            </tr>
		        </thead>
		    </table>
		    <% } %>	
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_link_alarme.png'/><br>Vinculo Alarme</span>" style="padding:10px">
			<% if (!isPresencaSupervisao) {%>
			<div>
				
				<div style="margin:20px 0;">
			        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#w').dialog('open')">Consultar Conta Sigma</a>
			    </div>
			    
			    <!-- window -->
			    <div id="w" title="Selecionar Conta" class="easyui-dialog" data-options="modal:true,closed:true,iconCls:'icon-save'" style="width:610px;height:300px;padding:10px;">
			    
			            
			        <div id="tb" style="padding:3px">
					    <span>Pesquisar por:</span>
					    <select id="cbFiltroConta" name="cbFiltroConta">
					        <option value="1" selected="selected">Conta</option>
					        <option value="2">Fantasia</option>
						</select>
					    
					    <input id="conta" style="line-height:26px;border:1px solid #ccc">
					    <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="doSearch()">Consultar</a>
					</div>
			        <table  id="tt" class="easyui-datagrid" style="width:560px;height:250px" 
			        		url="servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet" 
			        		toolbar="#tb" title="Contas" iconCls="icon-save" rownumbers="true" pagination="false" singleSelect="true">
					    <thead>
					        <tr>
					            <th field="centralParticao" width="80">Conta</th>
					            <th field="fantasia" width="200">Nome Fantasia</th>
					            <th field="empresa" width="180" align="right">Empresa</th>
					            <th field="linkEventos" width="50">Opções</th>
					        </tr>
					    </thead>
					</table>
					
			    </div>
			    <!-- end window -->
			    
			    <table id="dgContasVinculadas" title="Contas Vinculadas" class="easyui-datagrid" style="width:auto;height:auto"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		            	<th field="idCentral" width="80">Conta</th>
		           		<th field="cdCliente" width="80">Cliente</th>
		           		<th field="linkExcluir" width="80">Excluir</th>
		            </tr>
		        </thead>
		    </table>
			    
			</div>
			
			<% } %>	
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/moto.png'/><br>Vinculo Supervisao</span>" style="padding:10px">
			<% 
			List<PostoSupervisaoVO> postosSupVO = QLPresencaUtils.listaPostosSupervisao(posto.getUsuCodreg());
			
			String disabledVInculoSupervisao = "disabled";
			if (isVincularSupervisaoPosto ) {
				System.out.println("Tem permissão de vincular postos de supervisao");
				//if ( !isPostoAdm ) {
					disabledVInculoSupervisao =  "";
				//}
				%>
				Selecione o posto de supervisão :
				<select <%= disabledVInculoSupervisao %> id="cbVinculoSupervisao" name="cbVinculoSupervisao" class="easyui-combobox" data-options="
									valueField:'vlccusup',
									textField:'txtsup',
									editable:false,
									multiple:false,
									onSelect: function(record){
											codccusup = record.vlccusup;
											console.log('olá '+ codccusup);	
										if ('<%= posto.getSituacao() %>' == 'Encerrando'){
											//alert('Não é possível vincular supervisão para postos Inativos ou em encerramento');
											return false;
										}else{
											
										}
									}" style="width: 400px;">
									<option value="0" selected="selected" >Nenhum</option>
									<%
									for (PostoSupervisaoVO psup: postosSupVO ){
										System.out.println("supervisao: " + posto.getUsuPossup() + "- posto_cmp:" + psup.getUsuCodccu() + "="+ psup.getUsuCodccu().equals(posto.getUsuPossup()));
									%>
										<option value="<%= psup.getUsuCodccu() %>" ><%= psup.getUsuCodccu() + " - " + psup.getNomLoc() %></option>
									<%
									}
									%>
									
				</select>
				
				
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="salvaVinculoSupervisaoPosto()">Adicionar</a>
				<br>
				<%
			} 
			%>
			

			    <table id="dgPostosVinculados" title="Postos Vinculados" class="easyui-datagrid" style="width:auto;height:auto" rownumbers="true" singleSelect="true" nowrap="false">
			        <thead>
			            <tr>
			            	<th field="usuCodccu" width="80">Centro Custo</th>
			           		<th field="nomLoc" width="160">Posto</th>
			           		<%
			           		if (isVincularSupervisaoPosto){
			           		%>
								<th field="linkExcluir" width="80">Excluir</th>
			           		<%} %>
			            </tr>
			        </thead>
			    </table>
			
			
			
			
		</div>
		
		
		
		
    </div>
    
    <div id="modalSolicitacao" class="easyui-window" title="Solicitação" data-options="modal:true,closed:true,resizable:true,collapsible:false,minimizable:false,maximizable:true" style="width:800px;height:530px">  
    
    </div>
    
    <script type="text/javascript">
    	var codccusup = 0;
    
	    $('#tabPosto').tabs({
	    	
			onSelect : function(title, index) {
				var id = "";
				var data = "";
				switch (index) {
				
				case 1:
					id = "#dgEventoX8";
					data = "action=listaEventoX8&numctr=<%=posto.getNumeroContrato()%>&numpos=<%=posto.getNumeroPosto()%>";
					break;

				case 2:
					id = "#dgCobertura";
					data = "action=listaCoberturaPosto&numloc=<%=posto.getNumeroLocal()%>&taborg=<%=posto.getCodigoOrganograma()%>";
					break;

				case 3:
					id = "#dgSolicitacao";
					data = "action=listaSolicitacao&codcli=<%=posto.getCliente().getCodigoCliente()%>";
					break;
					
				case 4:
					id = "#dgReclamacao";
					data = "action=listaReclamacao&codcli=<%=posto.getCliente().getCodigoCliente()%>";
					break;
					
				case 5:
					id = "#dgVisita";
					data = "action=listaVisita&codpai=<%=posto.getCodigoPai()%>";
					break;
					
				case 6: 
					id = "#dgInspecao";
					data = "action=listaInspecoes&numctr=<%= posto.getNumeroContrato() %>&numpos=<%= posto.getNumeroPosto() %>";
					break;
				case 7:
					//BI
					break;
				case 8:
					id = "#dgFinanceiro";
					data = "action=listaFinancasPosto&numctr=<%= posto.getNumeroContrato() %>&numpos=<%= posto.getNumeroPosto() %>";
					break;
				case 9: // armas
					id= "#dgArma";
					data = "action=listaArmasPosto&numctr=<%= posto.getNumeroContrato() %>&numpos=<%= posto.getNumeroPosto() %>&timeMillisDataAtual=<%= strTimeMillisDataAtual %>";
					break;
				case 10: // fakta efetivo
					id= "#dgFaltaEfetivo";
					data = "action=listaFaltaEfetivo&numctr=<%= posto.getNumeroContrato() %>&numpos=<%= posto.getNumeroPosto() %>&codccu=<%= posto.getCentroCusto() %>";
					break;
				case 11:
					id = "#dgEquipamento";
					data = "action=listaEquipamentos&numctr=<%=posto.getNumeroContrato()%>&numpos=<%=posto.getNumeroPosto()%>";
					break;
				case 12:
					id = "#dgContaSigma";
					data = "action=listaContaSigma&numctr=<%=posto.getNumeroContrato()%>&numpos=<%=posto.getNumeroPosto()%>";
					break;
				case 13: // consulta sigma
					id = "#dgContasVinculadas";
		        	data = "action=listatVinculoContaPosto&numctr=<%= posto.getNumeroContrato() %>&numpos=<%= posto.getNumeroPosto() %>";
					break;
				case 14: // vinculo supervisao
					id = "#dgPostosVinculados"
					data = "action=listaPostosSupervisaoVinculados&codccu=<%= posto.getCentroCusto() %>";
					break;
				}
				
				if (index != 7){ //bi
					startProgress();
					$.ajax({
		            	method: 'post',
		                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
		                data: data,
		                dataType: 'json',
		                success: function(result){
		                    $(id).datagrid('loadData',result);
		                    $(id).datagrid('load',result);
		                    stopProgress();
		                    	
	    					$('#dgFinanceiro').datagrid({
	    					    rowStyler:function(index,row){
	    					    	var dataAtual = new Date();	    					    	
	    					    	var parts = row.vctPro.split('/');
	    	    					var compdate = new Date(parts[2],parseInt(parts[1])-1,parts[0]);
	    	    					
	    	    					var timeDiff = (dataAtual.getTime() - compdate.getTime());
	    	    					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
									
	    					        if ( ( diffDays >= 4) && row.sitTit=='AB' ){
	    					            return 'background-color:pink;color:blue;font-weight:bold;';
	    					        }
	    					    }
	    					});
		                    
		                }
		            });
				}
				
			}
		});
	    
	    function showSolicitacao(neoid) {
	    	var url = '<%=PortalUtil.getBaseURL()%>bpm/workflow_search.jsp?id='+neoid;
	    	var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,'');
	    	var win = window.name; 
	    	
	        NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win);
	    }
	    
	    function startProgress(){
        	$('#dlgLoader').dialog('open');
        }
        
        function stopProgress(){
        	$('#dlgLoader').dialog('close');
        }
        
        
        function doSearch(){
            $('#tt').datagrid('load',{
            	action: 'buscaContaSigma',
            	filtro: $('#cbFiltroConta').val(),
                conta: $('#conta').val()
            });
        }
        
        function setVinculoContaPosto(cdCliente,cdConta){
        	id = "#dgContasVinculadas";
        	data = "action=setVinculoContaPosto&numctr=<%= posto.getNumeroContrato() %>&numpos=<%= posto.getNumeroPosto() %>&cdCliente="+cdCliente+"&idCentral="+cdConta;
        	startProgress();
			$.ajax({
            	method: 'post',
                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                data: data,
                dataType: 'json',
                success: function(result){
                    $(id).datagrid('loadData',result);
                    $(id).datagrid('load',result);
                    $('#w').dialog('close');
                    //$('#w').hide();
                    stopProgress();
                },
                error: function(result){
                	stopProgress();
                }
            });
        }
        
        function removerVinculoContaPosto(cdCliente,cdConta){
        	id = "#dgContasVinculadas";
        	data = "action=removerVinculoContaPosto&numctr=<%= posto.getNumeroContrato() %>&numpos=<%= posto.getNumeroPosto() %>&cdCliente="+cdCliente+"&idCentral="+cdConta;
        	startProgress();
			$.ajax({
            	method: 'post',
                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                data: data,
                dataType: 'json',
                success: function(result){
                    $(id).datagrid('loadData',result);
                    $(id).datagrid('load',result);
                    stopProgress();
                }
            });
        }
        
        function salvaVinculoSupervisaoPosto(){
        	id = "#dgPostosVinculados";
			console.log('vinculando centro de custo ' + $('#cbVinculoSupervisao').val() + ' de supervisao');        	
        	data = "action=salvaVinculoSupervisaoPosto&codccu=<%= posto.getCentroCusto() %>&numloc=<%= posto.getNumeroLocal() %>&usuario=<%= PortalUtil.getCurrentUser().getCode() %>&ccusup="+codccusup;
        	startProgress();
			$.ajax({
            	method: 'post',
                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                data: data,
                dataType: 'json',
                success: function(result){
                    $(id).datagrid('loadData',result);
                    $(id).datagrid('load',result);
                    stopProgress();
                },
                error: function(result){
                	stopProgress();
                }
            });
        }
        
        
        function removerVinculoSupervisaoPosto(codccusup){
        	id = "#dgPostosVinculados";
			console.log('removendo centro de custo ' + $('#cbVinculoSupervisao').val() + ' de supervisao');        	
        	data = "action=removerVinculoSupervisaoPosto&codccu=<%= posto.getCentroCusto() %>&numloc=<%= posto.getNumeroLocal() %>&usuario=<%= PortalUtil.getCurrentUser().getCode() %>&ccusup="+codccusup;
        	startProgress();
			$.ajax({
            	method: 'post',
                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                data: data,
                dataType: 'json',
                success: function(result){
                    $(id).datagrid('loadData',result);
                    $(id).datagrid('load',result);
                    stopProgress();
                },
                error: function(result){
                	stopProgress();
                }
            });
        }
        
        /*
         * Permite que sejam exibidos dados de qualquer eform dentro do fusion, basta passar o neoID do formulário
         */
        function viewItemFusion(id)
    	{
        	
    		var title ="vizualizar formulário";
			var uid;
			var entityType = '';
			var idx = '';
			
			var portlet = new FloatForm(title, uid, entityType, id, 'ellist_', idx, 0, 0, '', null, null, false, false, null, false, '');
    		portlet.open();
			
    	}
        
    </script>
    
    <style type="text/css">
        #fmAcesso{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
        .flabel{
        	text-align: right; 
        	height:20px; 
        	font-weight: bold; 
        	background-color: #e0ecff;
        	width: 140px;
        	padding-right: 2px; 
        }
        .fvalue{
        	padding-left: 8px; 
        }
        .tt-inner{
            display:inline-block;
            line-height:12px;
            padding-top:5px;
        }
        .tt-inner img{
            border:0;
        }
    </style>
</body>
</html>


	