<%@page import="com.neomind.fusion.custom.orsegups.utils.QlPresencaArquivoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ValeTransporteVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.JustificativaMarcacaoVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QlPresencaArquivoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.RelatorioActionsEnum"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.ql.servlet.QlArquivoServlet"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ClienteVO"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@taglib uri="/WEB-INF/form.tld" prefix="form"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>

 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>

<%
    final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.presenca.qlPresenca");
	
	String cpf = request.getParameter("numCpf");
	
	String colab = request.getParameter("colaborador");
	
	String matr = request.getParameter("matr");
	
	String empr = request.getParameter("empre");
	
	Long numCpf = 0L;
	
	Long mat = 0L;
	
	Long emp = 0L;
	
	if(cpf != null && !cpf.equals("0") && !cpf.equals(""))	{
		numCpf = Long.parseLong(cpf);
	} else {
		cpf = "";
	}
	
	if(colab != null && !colab.equals(""))	{
		colab = colab;
	} else {
		colab = "";
	}
	
	if(empr != null && !empr.trim().equals(""))	{
		emp = Long.parseLong(empr);
			if(matr != null && !matr.trim().equals(""))	{
				mat = Long.parseLong(matr);
			} else {
				matr = "";
				empr = "";
			}
	} else {
		empr = "";
		matr = "";
	} 
%>

<head>
	<meta charset="UTF-8">
</head>

<portal:head title="Quadro de Lotação - Presença Por Colaborador">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>

<script type="text/javascript" 	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>

<script type="text/javascript" 	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>

<link rel="stylesheet" type="text/css" 	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">

<link rel="stylesheet" type="text/css" 	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 

<script type="text/javascript" src="https://www.google.com/jsapi"></script> 

<style type="text/css">
.red{
    color:red;
    }
.form-area
{
    background-color: #FAFAFA;
	padding: 10px 40px 60px;
	margin: 10px 0px 60px;
	border: 1px solid GREY;
}
</style>    
<cw:main>
<cw:header title="Quadro de Lotação - Presença Por Colaborador" />
	<cw:body id="area_scroll">
		    <div class="container" style="width: 100%;height: 345px">
		    <div id="modalFichaColaboradorArquivo" class="easyui-window" title="Ficha do Colaborador" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:880px;height:530px">  
		    
		    </div>
			<div class="col-md-5">
			    <div name="formControles" class="form-area">  
			        <form role="form">
			        <br style="clear:both">
			                    <h3 style="margin-bottom: 25px; text-align: center;">Informações do colaborador</h3>
			    				<div class="form-group">
									<input id="numCpf" data-toggle="tooltip" title="Digite o CPF numero inteiro!" name="numCpf" value="<%=cpf%>"  type="text" class="form-control"  placeholder="CPF" >
								</div>
								<div class="form-group">
									<input id="colaborador" data-toggle="tooltip" title="Digite o nome do colaborador sem acentuação!" style="text-transform:uppercase"  name="colaborador" value="<%=colab%>" type="text" class="form-control"  placeholder="Colaborador" >
								</div>
								<div class="form-group">
									<input id="empre" name="empre" value="<%=empr%>"  title="Digite a empresa e matrícula!"  type="text" class="form-control" placeholder="Empresa" >
								</div>
									<div class="form-group">
									<input id="matr" name="matr" value="<%=matr%>" title="Digite a empresa e matrícula!"   type="text" class="form-control"  placeholder="Matrícula" >
								</div>
			            
			        <button type="submit" value="Aplicar" id="submit" name="submit" class="btn btn-primary pull-right">Pesquisar</button>
			        </form>
			    </div>
			</div>
			</div>
			</br>
			</br>
			</br>
			</br>
			</br>
			</br>
			</br>
			</br>
			</br>
		    <%if (!cpf.equals("") || !colab.equals("") || !matr.equals("") || !empr.equals("")) { %>
			<%
				// medir tempo execucao
				Long timeExecFinal = GregorianCalendar.getInstance().getTimeInMillis();
			
				// medir tempo execucao
				List<ColaboradorVO> colaboradores = QlPresencaArquivoUtils.getListaColaboradoresArquivo(numCpf, colab, mat, emp);
				
				
				
			%>			
						<FIELDSET class="fieldGroup">
						<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
						<tbody>						
								<%
								if (colaboradores != null && !colaboradores.isEmpty()) 
								{
								%>
								<tr>
									<td colspan="9" style="padding-top: 10px;padding-bottom: 10px;background-color: #dee3f7">
										<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
											<tbody>	
											<tr style="cursor: auto">
												<th style="cursor: auto">Colaborador</th>
											</tr>
											<%
											List<String> colaboradoresFiltrados = new ArrayList<String>();
											for(ColaboradorVO colaborador : colaboradores){
												if (!colaboradoresFiltrados.contains(colaborador.getNumeroEmpresa()+"/"+colaborador.getNumeroCadastro()+" - "+colaborador.getNomeColaborador().trim())){
												    colaboradoresFiltrados.add(colaborador.getNumeroEmpresa()+"/"+colaborador.getNumeroCadastro()+" - "+colaborador.getNomeColaborador().trim());
												    
												    String btnFichaColaborador = 				"<div class='iconCol' onclick='javascript:showFichaColaboradorArquivo("+ colaborador.getNumeroEmpresa() + "," + colaborador.getCpf() +"," + colaborador.getNumeroCadastro() +")'>";
													btnFichaColaborador = btnFichaColaborador + "	<div id='txt-" + colaborador.getNumeroEmpresa() + "-" + colaborador.getNumeroCadastro() + "' class='txtCol'> </div>";
													btnFichaColaborador = btnFichaColaborador + "</div>";
													%>
													<tr>
													<td><%= btnFichaColaborador + "&nbsp;" %>
													<%=	colaborador.getNumeroEmpresa()+"/"+colaborador.getNumeroCadastro()+" - "+colaborador.getNomeColaborador() %></td>
													</tr>
											<%
												}
																
											}
											%>
											</tbody>
											</table>
									</td>
								</tr>
							<%
							}
							%>
						</tbody>
						</table>
						</fieldset>
						<br/>
				
				<div style="width: 100%;height: 500px"></div>
			<%} %>
	</cw:body>
</cw:main>
</portal:head>

<script language="JavaScript">


$(function () {
$('input[type=text][name=secondname]').tooltip({ /*or use any other selector, class, ID*/
    placement: "right",
    trigger: "focus"
});
});

function clickBtn(bt)
{
	if (document.getElementById(bt).checked) {
		document.getElementById("mostrar" + bt).style.fontWeight = 'bold';
	} else {
		document.getElementById("mostrar" + bt).style.fontWeight = '';
	}
}

function showFichaColaboradorArquivo(numemp, cpf, numCad) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/fichaColaboradorArquivo.jsp?numemp='+numemp+'&cpf='+cpf+'&numCad='+numCad;

	$('#modalFichaColaboradorArquivo').window('open');
	$('#modalFichaColaboradorArquivo').window('refresh', caminho);
}

</script>
<style type="text/css">
   .iconCol {
  		cursor:pointer;
  		width:16px;
   		height:16px;
   		float: left;
   		background: url('<%=PortalUtil.getBaseURL()%>imagens/custom/papel_16x16.png') no-repeat;
	}
	.txtCol{
	    background:red; 
	    border-width:1px;
	    border-color:black;
	    color:white;
	    font-weight:bold;
	    font-size:8px;
	    margin-top:6px;
	    margin-left:5px;
	    text-align: center;
	    padding-right: 1px;
	}
</style>