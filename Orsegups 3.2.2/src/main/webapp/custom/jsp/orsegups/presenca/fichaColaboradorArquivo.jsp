<%@page import="com.neomind.fusion.custom.orsegups.utils.QlPresencaArquivoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.site.SiteUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.text.ParseException"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.AcessoVO"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page import="java.io.OutputStream"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   

<%
	String numemp = request.getParameter("numemp");
	String numcad = request.getParameter("numCad");
	String cpf = request.getParameter("cpf");
	Long numempL = Long.parseLong(numemp);
	Long numcadL = Long.parseLong(numcad);
	Long numCpf = Long.parseLong(cpf);
	
	ColaboradorVO colaborador = new ColaboradorVO();
	colaborador = QlPresencaArquivoUtils.buscaFichaColaboradorArquivo(numempL, numcadL, numCpf);
	
	String cpfColaborador = String.format ("%011d", colaborador.getCpf()); 
	String sexo = "";
	String comp = "";
	
	String numEmpCol = colaborador.getNumeroEmpresa().toString(); 
// 	if (!numempL.equals(colaborador.getNumeroEmpresa())){
// 		numemp = colaborador.getNumeroEmpresa().toString();
// 	}
	String numCadCol = colaborador.getNumeroCadastro().toString(); 
// 	if (!numcadL.equals(colaborador.getNumeroCadastro())){
// 		numcad = colaborador.getNumeroCadastro().toString();
// 	}
	
	try{
		cpfColaborador = OrsegupsUtils.formatarString(cpfColaborador, "###.###.###-##");
	}
	catch (ParseException e)
	{
		e.printStackTrace();
	}
	
	if (colaborador.getSexo().equals("M"))
	{
		sexo = "Masculino";
	}
	else
	{
		sexo = "Feminino";
	}
	
	String caminhoFoto = "custom/jsp/orsegups/utils/imageFromDB.jsp?numEmp=" + numEmpCol + "&tipCol=1&numCad=" + numCadCol;

%>

<html>
<body>
	
    <div id="tab" class="easyui-tabs" style="width: auto;height:494px" data-options="tabHeight:55">
        <div title="<span class='tt-inner'><img src='imagens/custom/ficha_dados_gerais.png'/><br>Gerais</span>" style="padding:10px; width: auto">
			<div style="border: 2px solid #95b8e7; width: auto; height: auto">    
				<div style="float: right;">
					<img src="<%=caminhoFoto%>" width="140px" height="180px" style="border-left: 2px solid #95b8e7;border-bottom: 2px solid #95b8e7;"></img>
				</div>    
				<table style="width: 75%;">
					<tbody>
						<tr>
							<td class="flabel" class="flabel">Nome:</td>
							<td class="fvalue"><%=colaborador.getNomeColaborador()%></td>
						</tr>
						<tr>
							<td class="flabel">Empresa/Matrícula:</td>
							<td class="fvalue"><%=colaborador.getNumeroEmpresa() + "/" + colaborador.getNumeroCadastro()%></td>
						</tr>
						<tr>
							<td class="flabel">Sexo:</td>
							<td class="fvalue"><%=sexo%></td>
						</tr>
							<tr>
								<td class="flabel">CPF:</td>
								<td class="fvalue"><%=cpf%></td>
							</tr>
							<tr>
								<td class="flabel">Nascimento:</td>
								<td class="fvalue"><%=NeoUtils.safeDateFormat(colaborador.getDataNascimento(),"dd/MM/yyyy")%></td>
							</tr>
							<tr>
								<td class="flabel">Admissão:</td>
								<td class="fvalue"><%=NeoUtils.safeDateFormat(colaborador.getDataAdmissao(),"dd/MM/yyyy")%></td>
							</tr>
							<tr>
								<td class="flabel">País:</td>
								<td class="fvalue"><%=colaborador.getEndereco() != null ? colaborador.getEndereco().getPais() : ""%></td>
							</tr>
							<tr>
								<td class="flabel">Estado:</td>
								<td class="fvalue"><%=colaborador.getEndereco() != null ? colaborador.getEndereco().getUf() : ""%></td>
							</tr>
							<tr>
								<td class="flabel">Endereço:</td>
								<td class="fvalue"><%=colaborador.getEndereco() != null ? colaborador.getEndereco() : ""%></td>
							</tr>
							<tr>
								<td class="flabel">Cidade:</td>
								<td class="fvalue"><%=colaborador.getEndereco() != null ? colaborador.getEndereco().getCidade() : ""%></td>
							</tr>
							<tr>
								<td class="flabel">Bairro:</td>
								<td class="fvalue"><%=colaborador.getEndereco() != null ? colaborador.getEndereco().getBairro() : ""%></td>
							</tr>
							<tr>
								<td class="flabel">CEP:</td>
								<td class="fvalue"><%=colaborador.getEndereco() != null ? colaborador.getEndereco().getCep() : ""%></td>
							</tr>
							
							<tr>
								<td class="flabel">Telefone:</td>
								<td class="fvalue"><%=colaborador.getTelefone() != null ? colaborador.getTelefone() : ""%></td>
							</tr>
					</tbody>
				</table>
			</div>	
		</div>
	
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_documentos.png'/><br>Documentos</span>" style="padding:10px">
			<table id="dgDocumento" title="DOCUMENTOS DE <%=colaborador.getNomeColaborador() %>" class="easyui-treegrid"></table>
		</div>
    </div>
    
    <script type="text/javascript">
        
        
        function onClickRow(index){
            if (editIndex != index){
                if (endEditing()){
                   // $('#dgAcessoApuracao').datagrid('selectRow', index).datagrid('beginEdit', index);
                    editIndex = index;
                } else {
                    //$('#dgAcessoApuracao').datagrid('selectRow', editIndex);
                }
            }
        }
        
        function startProgress(){
        	$('#dlgLoader').window('open');
        }
        
        function stopProgress(){
        	$('#dlgLoader').window('close');
        }
        
        function situacao(){
        	var cptVar = $('#cbCompetenciaApu').combobox('getValue');
        	loadPieChart(<%=colaborador.getNumeroEmpresa()%>,<%=colaborador.getTipoColaborador()%>,<%=colaborador.getNumeroCadastro()%>,cptVar);
        }
        
		$('#tab').tabs({
			onSelect : function(title, index) {
				var id = "";
				var data = "";
				switch (index) {
				case 1:
					$('#dgDocumento').treegrid({
						url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlArquivoServlet?action=listaDocumentosArqiuivo&cpf=<%=cpf%>&numCad=<%=numcad%>&numemp=<%=numemp%>',
					    idField:'id',
					    treeField:'nome',
					    columns:[[
					        {title:'Nome',field:'nome',width:400},
					        {title:'Competência',field:'data',width:80},
					        {title:'Abrir',field:'link',width:30},
					    ]]
					});
					break;	
				}
			}
		});
		
		function showTarefa(neoid) {
	    	var url = '<%=PortalUtil.getBaseURL()%>bpm/workflow_search.jsp?id='+neoid;
	    	var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,'');
	    	var win = window.name; 
	    	
	        NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win);
	    }
	</script>
    <style type="text/css">
        #fmAcesso{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
        .flabel{
        	text-align: right; 
        	height:20px; 
        	font-weight: bold; 
        	background-color: #e0ecff;
        	width: 120px;
        	padding-right: 2px; 
        }
        .fvalue{
        	padding-left: 8px; 
        }
        .tt-inner{
            display:inline-block;
            line-height:12px;
            padding-top:5px;
        }
        .tt-inner img{
            border:0;
        }
    </style>
</body>
</html>


	