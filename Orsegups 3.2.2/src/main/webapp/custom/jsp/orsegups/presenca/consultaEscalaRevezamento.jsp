<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
	String codesc = request.getParameter("codesc");
	String data = request.getParameter("data");
	String dataNew = data.substring(0, 10);
	
%>

<html>
<body>
	<table id="dgEscalaRevizamento" title="Revezamento de Escala"
		class="easyui-datagrid" style="width: auto; height: 412px"
		url="servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=listaEscalaRevezamento&codesc=<%=codesc%>&data=<%=dataNew%>"
		rownumbers="true" singleSelect="true" nowrap="false">
		<thead>
			<tr>
				<th field="data" width="80">Data</th>
				<th field="diaSemana" width="80">Dia Semana</th>
				<th field="descricao" width="200">Descri��o</th>
				<th field="turma1" width="50">Turma 1</th>
				<th field="turma2" width="50">Turma 2</th>
				<th field="turma3" width="50">Turma 3</th>
				<th field="turma4" width="50">Turma 4</th>
				<th field="turma5" width="50">Turma 5</th>
				<th field="turma6" width="50">Turma 6</th>
				<th field="turma7" width="50">Turma 7</th>
				<th field="turma8" width="50">Turma 8</th>
			</tr>
		</thead>
	</table>
</body>
</html>
