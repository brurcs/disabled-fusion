<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
	String cdCliente = request.getParameter("cdCliente");
	String conta = request.getParameter("conta");
	String fantasia = request.getParameter("fantasia");
%>

<html>
<body>
    <div id="tabContaSigma" class="easyui-tabs" style="width: auto;height:464px" data-options="tabHeight:55">
        <div title="<span class='tt-inner'><img src='imagens/custom/evento_32x32.png'/><br>Eventos</span>" style="padding:10px; width: auto">
			<div style="padding:5px;height:auto">
				Tipo de Evento:
				<select id="cbTipoEvento"  class="easyui-combobox" data-options="panelHeight:'160', editable:false" style="width: 200px;font-size:11px;">
					<option value=View_Historico>Todos</option>
					<option value=Historico_Alarme>Alarme</option>
					<option value=Historico_Teste_Automatico>Teste Automático</option>
					<option value=Historico_Arme>Armado</option>
					<option value=Historico_Desarme>Desarmado</option>
					<option value=Historico_Sem_Controle>Sem Controle</option>
					<option value=Historico_Ronda>Ronda</option>
				</select>
				Mostrar último(s):
				<select id="cbDias"  class="easyui-combobox" data-options="panelHeight:'160', editable:false" style="width: 100px;font-size:11px;">
					<option value=1>1</option>
					<option value=2>2</option>
					<option value=3>3</option>
					<option value=5>5</option>
					<option value=10>10</option>
					<option value=20>20</option>
					<option value=30>30</option>
				</select>
				dia(s)  
				<div style="float: right;">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick='atualizaEventos(<%= cdCliente %>)'>Pesquisar</a>
				</div>
			</div>	
			<div>
				<table id="dgEvento" title="Eventos" class="easyui-datagrid" style="width:auto;height:344px"
			           rownumbers="true" singleSelect="true" nowrap="false">
			        <thead>
			            <tr>
			                <th field="dataRecebido" width="70">Recebido</th>
			                <th field="tipoEvento" width="50">Evento</th>
			                <th field="dataDeslocamento" width="70">Deslocado</th>
			                <th field="dataLocal" width="70">Local</th>
			                <th field="dataFechamento" width="70">Fechado</th>
			                <th field="nomeViatura" width="140">Colaborador</th>
			                <th field="observacaoFechamento" width="240">Observação</th>
			            </tr>
			        </thead>
			    </table>
			</div>
		</div>
    </div>
    
    <script type="text/javascript">
    
	    function atualizaEventos(cdCliente){
			var dias = $('#cbDias').combobox('getValue');
			var tipoEvento = $('#cbTipoEvento').combobox('getValue');
			startProgress();
			$.ajax({
            	method: 'post',
                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                data: 'action=listaEventosSigma&cdCliente='+cdCliente+'&dias='+dias+'&tipoEvento='+tipoEvento,
                dataType: 'json',
                success: function(result){
                    $('#dgEvento').datagrid('loadData',result);
                    $('#dgEvento').datagrid('load',result);
                    stopProgress();
                }
            });
	    }
    
    </script>
</body>
</html>


	