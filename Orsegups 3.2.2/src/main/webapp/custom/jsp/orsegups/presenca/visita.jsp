<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.VisitaVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
	String neoid = request.getParameter("neoid");
	VisitaVO visita = QLPresencaUtils.buscaPorVisitaPorNeoid(Long.valueOf(neoid));
%>

<html>
<body>
	<div style="border: 2px solid #95b8e7; width: auto; height: auto; margin: 10px">
	    <table style="width: 100%;">
			<tbody>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Agendado:</td>
					<td class="fvalueVisita"><%=visita.getDataAgenda()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relatado:</td>
					<td class="fvalueVisita"><%=visita.getDataRelato()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Contato:</td>
					<td class="fvalueVisita"><%=visita.getContatoCliente()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Área:</td>
					<td class="fvalueVisita"><%=visita.getArea()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Telefone:</td>
					<td class="fvalueVisita"><%=visita.getTelefone()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">E-mail:</td>
					<td class="fvalueVisita"><%=visita.getEmail()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato Geral:</td>
					<td class="fvalueVisita"><%=visita.getRelatoGeral()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato Geral:</td>
					<td class="fvalueVisita"><%=visita.getRelatoGeral()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de X8:</td>
					<td class="fvalueVisita"><%=visita.getRelatoX8()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de Coberturas:</td>
					<td class="fvalueVisita"><%=visita.getRelatoCoberturas()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de Solicitações:</td>
					<td class="fvalueVisita"><%=visita.getRelatoSolicitacoes()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de Reclamações:</td>
					<td class="fvalueVisita"><%=visita.getRelatoReclamacoes()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de Equipamentos:</td>
					<td class="fvalueVisita"><%=visita.getRelatoEquipamentos()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de Checklist:</td>
					<td class="fvalueVisita"><%=visita.getRelatoChecklist()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de Contas Sigma:</td>
					<td class="fvalueVisita"><%=visita.getRelatoContasSigma()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de Eventos:</td>
					<td class="fvalueVisita"><%=visita.getRelatoEventos()%></td>
				</tr>
				<tr>
					<td class="flabelVisita" class="flabelVisita">Relato de Visitas:</td>
					<td class="fvalueVisita"><%=visita.getRelatoVisitas()%></td>
				</tr>
			</tbody>
		</table>	
	</div>	
	<style type="text/css">
       	.flabelVisita{
        	text-align: right; 
        	height:20px; 
        	font-weight: bold; 
        	background-color: #e0ecff;
        	width: 160px;
        	padding-right: 2px; 
        }
        .fvalueVisita{
        	padding-left: 8px; 
        }
    </style>	
</body>
</html>


	