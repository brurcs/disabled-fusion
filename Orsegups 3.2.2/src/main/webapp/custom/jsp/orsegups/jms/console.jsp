<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>JMS Console</title>
</head>
<body>

<div id="console-area">

</div>


<script type="text/javascript">

var amq = org.activemq.Amq;
amq.init({ 
  	uri: '<%=PortalUtil.getBaseURL()%>amq',
	logging : true,
	timeout: 45, 
	clientId:(new Date()).getTime().toString() 
});

var myHandler =
{
  rcvMessage: function(message)
  {
	  	var decodedMessage = decodeURIComponent(message.data.replace(/\+/g,  " "));
		var json =  JSON.parse(decodedMessage);
	  
	  	$("#console-area").append("<p> " + JSON.stringify(json) + "</p><br />");
  }
};


amq.addListener('callCenterListener',"topic://messageTopic" ,myHandler.rcvMessage, { selector:"identifier='6606'" });

</script>

</body>
</html>