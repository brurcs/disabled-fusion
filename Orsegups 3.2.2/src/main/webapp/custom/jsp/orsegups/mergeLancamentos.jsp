<%@page
	import="com.neomind.fusion.custom.orsegups.utils.ImportaEntregasCompras"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="javax.persistence.Query"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="java.lang.Object"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.RoleAvailableActivityData"%>


<body>
	<%
		/*
		Necessário criar 2 E-FOrms externos no ambiente
		
		Nome: SAPIENSLANCAMENTOSEPI
		Título : [SAPIENS] E207EME - Lançamentos EPI
		Banco externo : SAPIENS
		Tabela : E207EME
		
		Nome: SAPIENSSOLICITACAOCOMPRAEPI
		Título : [SAPEINS] E405SOL - Solicitações de Compra EPI
		Banco externo : SAPIENS
		Tabela : E405SOL
		
		Inserir no log4j.propreties:
		log4j.logger.com.neomind.fusion.custom.orsegups.utils=DEBUG
		
		 */
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLRawFilter(
				"obseme LIKE 'Integrado via processo R011 - Manutenção Ficha Uniforme e EPI%'"));

		ExternalEntityInfo z = (ExternalEntityInfo) EntityRegister.getInstance().getCache()
				.getByType("SAPIENSLANCAMENTOSEPI");
		Collection<NeoObject> entregas = (Collection<NeoObject>) PersistEngine.getObjects(
				z.getEntityClass(), gp);
		out.println("Iniciando analise e importação dos dados da tabela SAPIENSLANCAMENTOSEPI (Acompanhar detalhes no LOG). "
				+ entregas.size() + " Registros <br>");
		out.flush();

		if (ImportaEntregasCompras.merge(entregas))
		{
			out.println("SUCESSO. Todas as entregas analisadas e imprtadas sem erros.");
		}
		else
		{
			out.println("FRACASSO. Houveram erros na importação das entregas, verificar no log.");
		}

		out.flush();
		out.println("<br><br><br>");

		gp.clearFilterList();
		gp.addFilter(new QLRawFilter(
				"obsSol LIKE 'Integrado via processo R011 - Manutenção Ficha Uniforme e EPI%'"));
		ExternalEntityInfo y = (ExternalEntityInfo) EntityRegister.getInstance().getCache()
				.getByType("SAPIENSSOLICITACAOCOMPRAEPI");
		Collection<NeoObject> compras = (Collection<NeoObject>) PersistEngine.getObjects(
				y.getEntityClass(), gp);

		out.println("Iniciando analise e importação dos dados da tabela SAPIENSSOLICITACAOCOMPRAEPI (Acompanhar detalhes no LOG). "
				+ compras.size() + " Registros <br>");
		out.flush();
		if (ImportaEntregasCompras.merge(compras, true))
		{
			out.println("SUCESSO. Todas as solicitações de compras foram analisadas e imprtadas sem erros.");
		}
		else
		{
			out.println("FRACASSO. Houveram erros na importação das solicitações de compras, verificar no log.");
		}
	%>
</body>
