<%@page import="java.sql.Date"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>


<%
	DecimalFormat formatoReal = new DecimalFormat("#,##0.00");

	Long codigoCalculoAtual = NeoUtils.safeLong(request.getParameter("codigoCalculo"));
	if(codigoCalculoAtual == null ||codigoCalculoAtual <= 0L)	{
		codigoCalculoAtual = 0L;
	}
	// Obtendo usuario logado
	String userLogin = PortalUtil.getCurrentUser().getCode();
	
	QLEqualsFilter filtroTipoComissao = new QLEqualsFilter("usu_tipcal", "V");
	List<NeoObject> listCalculo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUCAL"),filtroTipoComissao,-1, 6,"usu_cptcal desc");
		
%>

<portal:head title="Vendas">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<cw:main>
		<cw:header title="Vendas" />
		<cw:body id="area_scroll">
		<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
		<tr style="cursor: auto">
		<%
			for (NeoObject objCalculo : listCalculo) 
			{
				EntityWrapper wCalculo = new EntityWrapper(objCalculo);
				GregorianCalendar dataCpt = (GregorianCalendar) wCalculo.findField("usu_cptcal").getValue();
				GregorianCalendar iniCpt = (GregorianCalendar) wCalculo.findField("usu_datini").getValue();
				GregorianCalendar fimCpt = (GregorianCalendar) wCalculo.findField("usu_datfim").getValue();
				Long codigoCalculo = (Long) wCalculo.findField("usu_codcal").getValue(); 
				if (codigoCalculoAtual == 0L) {
					codigoCalculoAtual = codigoCalculo; 
				}
		%>
			<th style="cursor: auto; white-space: normal; <%= codigoCalculo.equals(codigoCalculoAtual) ? "font-weight: bold;" : "" %>" width="10%"><a href="<%= PortalUtil.getBaseURL() %>/custom/jsp/orsegups/vendas/vendasComissoesMinhas.jsp?codigoCalculo=<%= codigoCalculo %>" title="<%= NeoUtils.safeDateFormat(iniCpt, "dd/MM")%> a <%= NeoUtils.safeDateFormat(fimCpt, "dd/MM")%>"><%=dataCpt == null ? "&nbsp" : NeoUtils.safeDateFormat(dataCpt, "MMM/yyyy")%></a></th>
		<%
			}
		%>	
		</tr>
		</table>
		<%
			QLGroupFilter filtroRep = new QLGroupFilter("AND");
			filtroRep.addFilter(new QLEqualsFilter("sitrep", "A"));
			filtroRep.addFilter(new QLEqualsFilter("usu_usufus", userLogin));
			List<NeoObject> listRep = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEREP"),filtroRep,-1, -1,"usu_codreg, nomrep");
			Connection conn = null;
			PreparedStatement st = null;
			ResultSet rs = null;
			if(listRep == null || listRep.isEmpty()){
			%>
			<script type="text/javascript"> 
    			$.messager.alert('Erro','O c�digo de usu�rio FUSION n�o foi encontrado na tabela E090REP - SAPIENS!.','error');
			</script> 
			<%
			}
			
			for (NeoObject objRep : listRep) 
			{
				EntityWrapper wRep = new EntityWrapper(objRep);
				Long codigoRep = (Long) wRep.findField("codrep").getValue(); 
				String nomeRep = (String) wRep.findField("nomrep").getValue(); 
				String tipoRep = (String) wRep.findField("usu_tiprep").getValue(); 
				String iconeRep = "<img src='imagens/icones_final/home_16x16.png' alt='Terceiro' align='absMiddle'/>";
				if (tipoRep != null && tipoRep.equals("C")) {
					iconeRep = "<img src='imagens/custom/user-worker-boss.png' alt='Colaborador' align='absMiddle'/>";
				}
				%>
							<fieldset class="fieldGroup" >
							<legend class="legend" style="font-size: 1.2em"><%= iconeRep %>&nbsp;<%= nomeRep %> - C�d.: <%= codigoRep %>&nbsp;
							</legend>
				<%
					
				try {
					String nomeFonteDados = "SAPIENS";
					conn = PersistEngine.getConnection(nomeFonteDados);
					StringBuffer sql = new StringBuffer();
					String nomeRepresentanteAtual = "";
					Long qtdePostos = 0L;
					Double valorComissoes = 0.0;
					Double valorVendas = 0.0;

					sql.append(" SELECT cli.NomCli,ct.usu_datcad, cvl.*  ");
					sql.append(" FROM USU_T240CVL cvl  ");
					sql.append(" INNER JOIN E085CLI cli ON cli.CodCli = cvl.USU_Codcli ");
					sql.append(" inner join dbo.usu_t160ctr ct on ct.usu_numctr = cvl.usu_numctr and ct.usu_codemp = cvl.usu_codemp and ct.usu_codfil = cvl.usu_codfil  ");
					sql.append(" WHERE cvl.USU_CodCal = " + codigoCalculoAtual + " ");
					sql.append(" AND cvl.USU_CodRep = " + codigoRep + " ");
					sql.append(" ORDER BY USU_CodEmp, USU_NumCtr, USU_NumPos, USU_SeqApo ");
					
					st = conn.prepareStatement(sql.toString());
					rs = st.executeQuery();
					%>			
					<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
						<tr style="cursor: auto">
							<th style="cursor: auto; white-space: normal" width="2%">Empresa</th>
							<th style="cursor: auto; white-space: normal" width="5%">Contrato/Posto</th>
							<th style="cursor: auto; white-space: normal" width="5%">Data Cadastramento</th>
							<th style="cursor: auto; white-space: normal" width="5%">Origem Aditivo</th>
							<th style="cursor: auto; white-space: normal" width="20%">Cliente</th>
						    <th style="cursor: auto; white-space: normal" width="18%">Objeto</th>
							<th style="cursor: auto; white-space: normal" width="15%">Hist�rico</th>
							<th style="cursor: auto; white-space: normal" width="5%" align="center">Pr�-rata</th>
							<th style="cursor: auto; white-space: normal" width="3%">Qtde</th>
							<th style="cursor: auto; white-space: normal" width="5%">Pre�o</th>
							<th style="cursor: auto; white-space: normal" width="5%">Seguro</th>
							<th style="cursor: auto; white-space: normal" width="5%">Base Calc.</th>
							<th style="cursor: auto; white-space: normal" width="5%">Percentual</th>
							<th style="cursor: auto; white-space: normal" width="5%">Comiss�o</th>   
						</tr>
						<tbody>	
					<%
					valorComissoes = 0.0;
					valorVendas = 0.0;
					qtdePostos = 0L;
					String extraStyle = "";
					while(rs.next()) {
						extraStyle = "";
						if(rs.getString("USU_HisCom").startsWith("CTR:") && rs.getDouble("USU_VlrBse") > 0) {
							qtdePostos++;
							valorVendas = valorVendas + rs.getDouble("USU_VlrBse");
							extraStyle = "style='background-color: #CAFFCA;'";
						}
						valorComissoes = valorComissoes + rs.getDouble("USU_VlrCom");
						
						Date dataCadTemp = rs.getDate("usu_datcad");
						GregorianCalendar dataCad = new GregorianCalendar();
						dataCad.setTimeInMillis(dataCadTemp.getTime());
						String strDataCad = "";
						if (dataCad.get(GregorianCalendar.YEAR) != 1900) {
							strDataCad = NeoUtils.safeDateFormat(dataCad, "dd/MM/yyyy");
						}
					%>
						<tr <%= extraStyle %>>
							<td style="white-space: normal"><%= rs.getString("USU_CodEmp") %></td>
							<td style="white-space: normal"><%= rs.getString("USU_NumCtr") %>/<%= rs.getString("USU_NumPos") %></td>
							<td style="white-space: normal"><%= strDataCad %></td>
							<td style="white-space: normal"><%= rs.getString("USU_PosOri") %></td>
							<td style="white-space: normal"><%= rs.getString("NomCli") %></td>
							<td style="white-space: normal"><%= rs.getString("USU_HisCom") %></td>
							<td style="white-space: normal">&nbsp;</td>
							<td style="white-space: normal"><%= NeoUtils.safeFormat(formatoReal, rs.getDouble("USU_ProRat")*100) %>%</td>
							<td style="white-space: normal; text-align: right"><%= NeoUtils.safeFormat(formatoReal, rs.getDouble("USU_QtdCvs")) %></td>
							<td style="white-space: normal; text-align: right"><%= NeoUtils.safeFormat(formatoReal, rs.getDouble("USU_PreUni")) %></td>
							<td style="white-space: normal; text-align: right"><%= NeoUtils.safeFormat(formatoReal, rs.getDouble("USU_VlrSeg")) %></td>
							<td style="white-space: normal; text-align: right"><%= NeoUtils.safeFormat(formatoReal, rs.getDouble("USU_VlrBse")) %></td>
							<td style="white-space: normal; text-align: right"><%= NeoUtils.safeFormat(formatoReal, rs.getDouble("USU_PerRep")) %>%</td>
							<td style="white-space: normal; text-align: right"><%= NeoUtils.safeFormat(formatoReal, rs.getDouble("USU_VlrCom")) %></td>
						</tr>				
						<%
					}
					
					
					StringBuffer sqlManual = new StringBuffer();
					sqlManual.append(" SELECT cvm.*,ct.usu_datcad  ");
					sqlManual.append(" FROM USU_T240CVM cvm  ");
					sqlManual.append(" left join dbo.usu_t160ctr ct on ct.usu_codemp = cvm.usu_codemp and ct.usu_codfil = cvm.usu_codfil and ct.usu_numctr = cvm.usu_numctr ");
					sqlManual.append(" WHERE cvm.USU_CodCal = " + codigoCalculoAtual + " ");
					sqlManual.append(" AND cvm.USU_CodRep = " + codigoRep + " ");
					sqlManual.append(" ORDER BY USU_SeqCom ");
					
					st = conn.prepareStatement(sqlManual.toString());
					rs = st.executeQuery();

					while(rs.next()) {
						extraStyle = "style='background-color: #FFEEDD;'";
						valorComissoes = valorComissoes + rs.getDouble("USU_VlrCom");
						
						Date dataCadTempAut = rs.getDate("usu_datcad");
						String strDataCadAul = "";
						if (dataCadTempAut != null) {
							GregorianCalendar dataCadAut = new GregorianCalendar();
							dataCadAut.setTimeInMillis(dataCadTempAut.getTime());
							if (dataCadAut.get(GregorianCalendar.YEAR) != 1900) {
								strDataCadAul = NeoUtils.safeDateFormat(dataCadAut, "dd/MM/yyyy");
							}
						}
						%>
						<tr <%= extraStyle %>>
							<td style="white-space: normal"><%= rs.getString("USU_CodEmp").equals("0") ? "":rs.getString("USU_CodEmp").concat("/") %><%= rs.getString("USU_CodFil").equals("0")?"":rs.getString("USU_CodFil") %></td>
							<td style="white-space: normal"><%= rs.getString("USU_NumCtr").equals("0") ? "":rs.getString("USU_NumCtr").concat("/") %><%= rs.getString("USU_NumPOs").equals("0")?"":rs.getString("USU_NumPOs") %></td>
							<td style="white-space: normal"><%= strDataCadAul.equals("0")?"":strDataCadAul %></td>
							<td style="white-space: normal">&nbsp;</td>
							<td style="white-space: normal"><%= rs.getString("USU_NomCli") %></td>
							<td style="white-space: normal">&nbsp;</td>
							<td style="white-space: normal"><%= rs.getString("USU_HisCom") %></td>
							<td style="white-space: normal">&nbsp;</td>
							<td style="white-space: normal">&nbsp;</td>
							<td style="white-space: normal">&nbsp;</td>
							<td style="white-space: normal">&nbsp;</td>
							<td style="white-space: normal"><%= rs.getString("USU_VlrBse") == null ?"":rs.getString("USU_VlrBse") %></td>
							<td style="white-space: normal"><%= rs.getString("USU_PerRep") == null?"":rs.getString("USU_PerRep").concat("%") %></td>
							<td style="white-space: normal; text-align: right"><%= NeoUtils.safeFormat(formatoReal, rs.getDouble("USU_VlrCom")) %></td>
						</tr>				
					<%
					}
					
					%>
						<tr>
							<td style="white-space: normal" colspan="6">&nbsp;</td>
							<td style="white-space: normal; text-align: right;background-color: #CAFFCA;" colspan="3">Valor Contratos: <%= NeoUtils.safeFormat(formatoReal, valorVendas) %></td>
							<td style="white-space: normal; text-align: right;background-color: #CAFFCA;" colspan="2">Qtde Contratos: <%= qtdePostos %></td>
							<td style="white-space: normal; text-align: right; font-weight: bold; background-color: #FFFFB9;" colspan="2">Total Comiss�es: <%= NeoUtils.safeFormat(formatoReal, valorComissoes) %></td>
						</tr>
						</tbody>
				</table>	
			<%
			
		} catch (Exception e) {
			e.printStackTrace();
			%>
				<script type="text/javascript" >
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
			<%			
		} finally {
		try {
			OrsegupsUtils.closeConnection(conn, st, rs);
		} catch (Exception e) {
			e.printStackTrace();
			%>
				<script type="text/javascript" >
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
			<%	
			}
		}
		%>
					</fieldset>
							<br/>
		<%
			}
		%>
		
		</cw:body>
	</cw:main>
</portal:head>