<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>


<%
	DecimalFormat formatoReal = new DecimalFormat("#,##0.00");
	Long regional = NeoUtils.safeLong(request.getParameter("regional"));

	Long codigoCalculoAtual = NeoUtils.safeLong(request.getParameter("codigoCalculo"));
	if(codigoCalculoAtual == null ||codigoCalculoAtual <= 0L)	{
		codigoCalculoAtual = 0L;
	}

	QLEqualsFilter filtroTipoComissao = new QLEqualsFilter("usu_tipcal", "V");
	List<NeoObject> listCalculo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUCAL"),filtroTipoComissao,-1, 6,"usu_cptcal desc");

	QLGroupFilter filtroReg = new QLGroupFilter("AND");
	filtroReg.addFilter(new QLRawFilter("usu_codreg NOT IN (0, 999)"));	
	if(regional != null && regional > 0)	{
		filtroReg.addFilter(new QLEqualsFilter("usu_codreg", regional));		
	} 
	List<NeoObject> listReg = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUREG"),filtroReg,-1, -1,"usu_codreg");
		
%>

<portal:head title="Vendas">
	<cw:main>
		<cw:header title="Vendas" />
		<cw:body id="area_scroll">
		<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
		<tr style="cursor: auto">
		<%
			for (NeoObject objCalculo : listCalculo) 
			{
				EntityWrapper wCalculo = new EntityWrapper(objCalculo);
				GregorianCalendar dataCpt = (GregorianCalendar) wCalculo.findField("usu_cptcal").getValue();
				GregorianCalendar iniCpt = (GregorianCalendar) wCalculo.findField("usu_datini").getValue();
				GregorianCalendar fimCpt = (GregorianCalendar) wCalculo.findField("usu_datfim").getValue();
				Long codigoCalculo = (Long) wCalculo.findField("usu_codcal").getValue(); 
				if (codigoCalculoAtual == 0L) {
					codigoCalculoAtual = codigoCalculo; 
				}
		%>
			<th style="cursor: auto; white-space: normal; <%= codigoCalculo.equals(codigoCalculoAtual) ? "font-weight: bold;" : "" %>" width="10%"><a href="<%= PortalUtil.getBaseURL() %>/custom/jsp/orsegups/vendas/vendasComissoesResumo.jsp?regional=<%= regional %>&codigoCalculo=<%= codigoCalculo %>" title="<%= NeoUtils.safeDateFormat(iniCpt, "dd/MM")%> a <%= NeoUtils.safeDateFormat(fimCpt, "dd/MM")%>"><%=dataCpt == null ? "&nbsp" : NeoUtils.safeDateFormat(dataCpt, "MMM/yyyy")%></a></th>
		<%
			}
		%>	
		</tr>
		</table>
		<br>	
<%
			for (NeoObject objReg : listReg) 
			{
				EntityWrapper wReg = new EntityWrapper(objReg);
				Long codigoReg = (Long) wReg.findField("usu_codreg").getValue(); 
				String nomeReg = (String) wReg.findField("usu_nomreg").getValue(); 
				
				if(PortalUtil.getCurrentUser().getCode().equals("joao.santos") && (codigoReg == 1 || codigoReg == 7 || codigoReg == 16 ) 
					|| (PortalUtil.getCurrentUser().getCode().equals("caroline.wrunski") 
						|| (PortalUtil.getCurrentUser().getCode().equals("bruno.oliveira")) 
							|| (PortalUtil.getCurrentUser().getCode().equals("fernanda.contini"))
								|| (PortalUtil.getCurrentUser().getCode().equals("diogo.silva"))))  
				{
				

				QLGroupFilter filtroRep = new QLGroupFilter("AND");
				filtroRep.addFilter(new QLEqualsFilter("sitrep", "A"));
				filtroRep.addFilter(new QLEqualsFilter("usu_codreg", codigoReg));
				List<NeoObject> listRep = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSEREP"),filtroRep,-1, -1,"usu_codreg, nomrep");
				%>	

				<fieldset class="fieldGroup" >
				<legend class="legend" style="font-size: 1.2em">REGIONAL:&nbsp;<%= nomeReg %>&nbsp;</legend>	
				<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="106%">
				<tr style="cursor: auto">
					<th style="cursor: auto; white-space: normal" width="50%">Executivo</th>
					<th style="cursor: auto; white-space: normal" width="25%">Matrícula RH</th>
					<th style="cursor: auto; white-space: normal" width="25%">Total Comissão</th>   
				</tr>
				<tbody>	
								
				<%
				for (NeoObject objRep : listRep) 
				{
					EntityWrapper wRep = new EntityWrapper(objRep);
 					Long codigoRep = (Long) wRep.findField("codrep").getValue(); 
					String nomeRep = (String) wRep.findField("nomrep").getValue(); 
					String tipoRep = (String) wRep.findField("usu_tiprep").getValue(); 
					String iconeRep = "<img src='imagens/icones_final/home_16x16.png' alt='Terceiro' align='absMiddle'/>";
					String dadosMatrícula = "&nbsp;";
					if (tipoRep != null && tipoRep.equals("C")) {
						iconeRep = "<img src='imagens/custom/user-worker-boss.png' alt='Colaborador' align='absMiddle'/>";
						dadosMatrícula = wRep.findField("usu_numemp").getValue() + " / " + wRep.findField("usu_numcad").getValue();
					}
			
					 try {
						String nomeFonteDados = "SAPIENS";
						Connection conn = PersistEngine.getConnection(nomeFonteDados);
						StringBuffer sql = new StringBuffer();
						Double valorComissoes = 0.0;

						
						sql.append(" SELECT SUM(com.TotCom) AS TotCom   ");
						sql.append(" FROM (  SELECT SUM(cvl.USU_VlrCom) AS Totcom ");
						sql.append(" 		FROM USU_T240CVL cvl   ");
						sql.append(" 		WHERE cvl.USU_CodCal = " + codigoCalculoAtual + " ");
						sql.append(" 		AND cvl.USU_CodRep = " + codigoRep + " ");
						sql.append(" 		UNION ALL ");
						sql.append(" 		SELECT SUM(cvm.USU_VlrCom) AS TotCom   ");
						sql.append(" 		FROM USU_T240CVM cvm  ");
						sql.append(" 		WHERE cvm.USU_CodCal = " + codigoCalculoAtual + " ");
						sql.append(" 		AND cvm.USU_CodRep = " + codigoRep + " ) AS com ");
						
						PreparedStatement st = conn.prepareStatement(sql.toString());
						ResultSet rs = st.executeQuery();
						%>			
						
						<%
						while(rs.next()) {
							valorComissoes = rs.getDouble("TotCom");
						}
						if (valorComissoes != 0) {
						%>
								<tr>
									<td style="white-space: normal"><%= iconeRep %>&nbsp;<%= nomeRep %> - Cód.: <%= codigoRep %></td>
									<td style="white-space: normal"><%= dadosMatrícula %></td>
									<td style="white-space: normal; text-align: right; font-weight: bold;"><%= NeoUtils.safeFormat(formatoReal, valorComissoes) %></td>
								</tr>
									
						<%
						}
						rs.close();
						st.close();		
					} catch (Exception e) {
						e.printStackTrace();	
					}
				 }
				%>
				</tbody>
						</table>
		</fieldset>
								<br/>					
				<%
				}
				
		
			}
			%>
				
			
		</cw:body>
	</cw:main>
</portal:head>