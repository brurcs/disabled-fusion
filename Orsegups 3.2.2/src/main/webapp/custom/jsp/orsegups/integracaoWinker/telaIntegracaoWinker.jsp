<!DOCTYPE html>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<html>
	<head>
	<%@page import="com.neomind.fusion.portal.PortalUtil"%>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="css/estilos.css">
	<title>Integra��o Winker</title>
	 	<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<link rel="stylesheet" 
		href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
		<script type="text/javascript" 
		src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" 
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
	<script src="js/jquery.growl.js" type="text/javascript"></script>
		<link href="css/jquery.growl.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="js/integracaoWinker.js" charset="UTF-8"></script>
	</head>
	<section id=main>
		<div class="container-fluid" align="center">
			<div class="row" align="center" width="98%">
				<div class="col-md-12">
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
										<div class="form-inline"style="text-align: left;">
											<input pattern="d{14}" type="text" class="form-control campoformulario"  id="cpfCnpj" name="codigo" data-options="required:true" placeholder="Informe o CNPJ do cliente">
											<button style="background-color:#6b7486;" title='Pesquisar portal' type="button" class="btn btn-primary" onclick="pesquisarCliente()"><span type="button" id="pesquisar" class="glyphicon glyphicon-search "></span></button>
											<button style="background-color:#6b7486;" title='Pesquisar Clientes' type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPesquisaCliente" onClick="limparTabelaPesquisaCliente()"><span type="button" class="glyphicon glyphicon-user"></span></button><br><br> 
											<div id="dvCarregar" class="progress progress-striped active" style="display:none">
												<div id="dvProgresso" class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 10%;" >
												</div>
											</div>
	                   					</div>
	                   					<div class="page-header" align="center">
											<h1>
												Portal <img src='imagens/portalWinker.png'></a><br>
											</h1>
										</div>
										<!-- Modal -->
										<div id="modalPesquisaCliente" class="modal" role="dialog">
  											<div id="modalCliente2" class="modal-dialog" style="width: 100%">
    											<!-- Modal content-->
    											<div class="modal-content">
      												<div class="modal-header">
        												<button type="button" class="close" data-dismiss="modal">&times;</button>
        												<h4 class="modal-title" align="center"><b>Consultar Clientes</b></h4>
      												</div>
      												<div class="modal-body">
        												<div class="form-inline"style="text-align: left;">
        													<input type="text" class="form-control campoformulario2"  id="nomeCliente" name="nomeCliente" data-options="required:true" placeholder="Informe o nome do cliente">
        													<button class="btn btn-primary campoBotao" onClick="pesquisarClienteSapiens()">Pesquisar  <div id="teste"><span id="load" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></div></button>
        													<br><br>
        												</div>
        												<div class="table-responsive">
        													<table id="tabelaPesquisaCliente" class="table table-condensed">
																<thead>
											    					<tr>
											    						<th style="width: 12px;">Cod. Sapiens</th>
											        					<th>Nome do Cliente</th>
											        					<th>Fantasia</th>
											        					<th>CNPJ</th>
		           														<th style="width: 12px;">Tipo</th>
		           														<th>Endereco</th>
		           														<th style="width: 12px;">Numero</th>
		           														<th>Complemento</th>
		           														<th>Bairro</th>
		           														<th>Cidade</th>
											      					</tr>
											    				</thead>
															</table>
														</div> 
													</div>
      												<div class="modal-footer">
        												<button id="closeModal" type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      												</div>
    											</div>
  											</div>
										</div>
										
										<div class="table-responsive">          
											<table id="tabelaClientes" class="table table-hover">
												<thead>
											    	<tr>
											        	<th>Nome do Condominio</th>
											        	<th>Endereco</th>
		           										<th>Cidade</th>
		           										<th>Data do Cadastro</th>
		           										<th>Data do Protocolo</th>
		           										<th>Sindico</th>
		           										<th>Retorno Integra��o</th>
											      	</tr>
											    </thead>
											</table>
										</div>
										</div>
	                   						<div class="page-header" align="center">
												<h1>
													S�ndico 
													<img display="hidden" src='imagens/sindicoWinker.png'>
													<a id="acaoSindico" title='Integrar Sindico' style='cursor:pointer' onClick="limparDadosSindico()" ALIGN='RIGHT' data-toggle="modal" data-target="#modalDadosSindico"><img src='imagens/integracao.png'></a><br>
												</h1>
										</div>										
										<div class="table-responsive">          
											<table id="tabelaSindico" class="table table-hover">
												<thead>
											    	<tr>
													    <th>Nome Completo</th>
											        	<th>E-mail</th>
											        	<th>Telefone</th>
											        	<th>Data do Cadastro</th>
		           										<th>Data do Integra��o</th>	
		           										<th>Retorno Integra��o</th>
											      	</tr>
											    </thead>
											</table>
										</div>
										</div>
	                   						<div class="page-header" align="center">
												<h1>
													Colaboradores <img src='imagens/colaboradorWinker.png'></a><br>
												</h1>
										</div>
										<div class="table-responsive">          
											<table id="tabelaColaboradores" class="table table-hover">
												<thead>
											    	<tr>
											        	<th>Nome Completo</th>
											        	<th>Ocupa��o</th>
											        	<th>CPF</th>
											        	<th>Data do Cadastro</th>										        	
											      	</tr>
											    </thead>
											</table>
										</div>
								 </div>
								 <button id="btnConfirmacaoIntegracao" style="display:none" class="btn btn-primary campoBotao" data-toggle="modal" data-target="#modalConfirmacao"><span type="button" class="glyphicon glyphicon-user"></span></button><br><br> 
	  							 <div id="modalConfirmacao" class="modal" role="dialog">
  									<div class="modal-dialog">
    									<div class="modal-content">
      										<div class="modal-header">
        										<button type="button" class="close" data-dismiss="modal">&times;</button>
        											<h4 class="modal-title">Integracao Winker</h4>
      										</div>
      										<div class="modal-body">
        										<table>
    												<label>O cliente n�o possui integra��o com a Winker, deseja integrar agora?</label>
    											</table>
											</div>
      										<div class="modal-footer">
      											<button id="integrar"   type="button" class="btn btn-default" onclick="integrarWinker()">Sim</button>							
  												<button id="closeModalPortal" type="button" class="btn btn-default" data-dismiss="modal">Nao</button>
      										</div>
    									</div>
  									</div>
								</div>
								<button id="btnConfirmacaoSindico" style="display:none" class="btn btn-primary campoBotao" data-toggle="modal" data-target="#modalConfirmacaoSindico"><span type="button" class="glyphicon glyphicon-user"></span></button><br><br> 
	  							 <div id="modalConfirmacaoSindico" class="modal" role="dialog">
  									<div class="modal-dialog">
    									<div class="modal-content">
      										<div class="modal-header">
        										<button type="button" class="close" data-dismiss="modal">&times;</button>
        											<h4 class="modal-title">Integracao Winker</h4>
      										</div>
      										<div class="modal-body">
        										<table>
    												<label>Deseja integrar o sindico?</label>
    											</table>
											</div>
      										<div class="modal-footer">
      											<button id="integrar"   type="button" class="btn btn-default" onclick="dadosSindico()">Sim</button>							
  												<button id="closeModalSindico" type="button" class="btn btn-default" data-dismiss="modal">Nao</button>
      										</div>
    									</div>
  									</div>
								</div>
							</div>
								<button id="btnDadosSindico" style="display:none" class="btn btn-primary campoBotao" data-toggle="modal" data-target="#modalDadosSindico" onClick="limparDadosSindico()"><span type="button" class="glyphicon glyphicon-user"></span></button><br><br> 
	  							 <div id="modalDadosSindico" class="modal" role="dialog">
  									<div class="modal-dialog">
    									<div class="modal-content">
      										<div class="modal-header">
        										<button type="button" class="close" data-dismiss="modal">&times;</button>
        											<h4 class="modal-title">Integracao Winker - Sindico</h4>
      										</div>
      										<div class="modal-body">
        										<table>
    												<label>Preencha todos os campos a baixo!</label>
    											</table>
											</div>
											<input type="text" class="form-control campoformulario2"  id="nomSin" name="nomSin" placeholder="Nome do Sindico"><br>
	    									<input type="text" class="form-control campoformulario2"  id="emaSin" name="emaSin" placeholder="E-mail"><br>
											<input type="text" class="form-control campoformulario2"  id="cpfSin" name="cpfSin" placeholder="CPF">
 											<div class="modal-footer">
      											<button id="integrar"   type="button" class="btn btn-default" onclick="salvarDadosSindico()">Salvar</button>							
  												<button id="closeModalDadosSindico" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      										</div>
    									</div>
  									</div>
								</div>
							</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	</section>
	<style>
	.campoformulario{
		max-width: 1220px;
		min-width: 320px;
	}
	
	.campoformulario2{
		max-width: 440px;
		min-width: 440px;
	}
	
	.camposCadastroColaborador{
		max-width: 420px;
		min-width: 420px;
	}
	.campoBotao{
		max-width: 120px;
		min-width: 120px;
		background-color:#6b7486;
	    
	}
	.tipoOk { background-image:url(imagens/certo2.png);   }
	.tipoFalta { background-image:url(imagens/exclamacao2.png); }
	

	
	.thtd {
    	text-align: left;
    	padding: 8px;
	}
	
	.glyphicon {
    	font-size: 14px;
	}
	.glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
}

@-webkit-keyframes spin2 {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}


	</style>
	<script>
  		$('#dvProgresso').css({'background-color': "green"})
	</script>
	
		
</body>
</html>



