<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">


<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.css"
	media="screen" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.easyui.min.js"></script>
	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>	
 	<script src="js/jquery.growl.js" type="text/javascript"></script>
<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />


</head>

<link rel="StyleSheet" type="text/css" href="css/formulario.css">



<body>
	<!-- 	campos do formulario -->
	<div class="container-fluid" align="center">
		<br>
		<div class="row" align="center" width="98%">
			<div class="col-md-12">
				<div class="panel with-nav-tabs panel-primary">
					<div class="panel-heading"
						style="background-color: #6b7486; max-height: 50px;">
						<h3>Mailing</h3>
						<span data-toggle="tooltip" title="Click aqui para obter informa��es sobre todos os campos" class="span-ajuda pull-right label label-default">
                             	<i data-toggle="modal" data-target="#modalAjudaGeral" class="ajuda glyphicon glyphicon-question-sign"></i>
                       </span> 
					</div>
					<br>

					<form>
						<fieldset>
							<div>
								<div class="col-md-4">
									<label>Data Faturamento(Inicio)</label> <input type="date"
										size="20" id="dataInicio" name="dataInicio"
										class="form-control campoformulario" />
								</div>

								<div class="col-md-4">
									<label>Data Faturamento(Fim)</label> <input type="date"
										size="20" id="dataFim" class="form-control campoformulario"
										name="dataFim" />
								</div>

								<div class="col-md-4">
									<label>Cidade Cliente</label> <input type="text" size="20"
										id="cidadeCliente" class="form-control campoformulario"
										name="cidadeCliente" placeholder="Cidade Cliente"> </br>
								</div>

							</div>
							</br>

							<div>
								<div class="col-md-4">
									<label>Data Cadastro(Inicio)</label> <input type="date"
										size="20" id="dataCadastroInicio"
										class="form-control campoformulario" name="dataCadastroInicio" />
								</div>

								<div class="col-md-4">
									<label>Data Cadastro(Fim)</label> <input type="date" size="20"
										id="dataCadastroFim" class="form-control campoformulario"
										name="dataCadastroFim" />
								</div>

								<div class="col-md-4">
									<label>Cidade Prest. Servi�o</label> <input type="text"
										size="20" id="cidadeContrato"
										class="form-control campoformulario" name="cidadeContrarto" placeholder="Cidade Prest. Servi�o" />
									</br>
								</div>

							</div>
							</br>

							<div>
								<div class="col-md-4">
									<label>Faturamento Contrato</label> <select id="cbFaturamento"
										class="form-control campoformulario">
										<option>Selecione Abaixo</option>
										<option>Corporativo</option>
										<option>Varejo</option>
									</select>
								</div>

							<div class="col-md-4">
									<label>Servi�os</label> <input type="button"
										size="20"
										class="form-control campoformulario" value="Selecione os Servi�os desejados" data-toggle="modal" data-target="#modalServico" />
							</div>

							<div class="col-md-4">
									<label>Regionais</label> <input type="button"
										size="20"
										class="form-control campoformulario" value="Selecione as Regionais desejadas" data-toggle="modal" data-target="#modalRegional" /></br>
							</div>

							</div>

							<div>
								<div class="col-md-3">
									<label>Raz�o Social</label> <span class="badge"
										data-toggle="modal" data-target="#modalAjuda"
										class="ajuda glyphicon glyphicon-question-sign"
										data-toggle="tooltip" title="Click aqui para obter informa��es sobre o campo 'Raz�o Social'"> ?</span> <select
										id="cbRazao" class="form-control campoformulario2">
										<option>Selecione Abaixo</option>
										<option>Condominio</option>
										<option>Comercial</option>
										<option>Outros</option>
									</select>
								</div>

								<div class="form-group col-md-3">
									<label>Situa��o</label> <select id="cbSituacao"
										class="form-control campoformulario2">
										<option>Selecione Abaixo</option>
										<option>Ativos</option>
										<option>Inativos</option>
									</select> </br>
								</div>

								<div class="form-group col-md-3">
									<label>Tipo Cliente</label> <select id="cbEmc"
										class="form-control campoformulario2">
										<option>Selecione Abaixo</option>
										<option>Privado</option>
										<option>P�blico</option>
									</select> </br>
								</div>

								<div class="form-group col-md-3">
									<label>Tipo Pessoa</label> <select id="cbCliente"
										class="form-control campoformulario2">
										<option>Selecione Abaixo</option>
										<option>F�sica</option>
										<option>Jur�dica</option>
									</select> </br>

								</div>

							</div>

							<div class="col-md-10">
								<input type="button" onclick="getListaContratos()"
									id="pesquisar"
									class="btn btn-primary pull-center btn-pesquisar"
									value="Pesquisar">
							</div>
							</br>
				</div>
			</div>
		</div>
		</fieldset>
		</form>
	</div><br/>

<!-- 	window bot�o carregando -->
	<div id="dlgLoader" title="Carregando..." class="easyui-window" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false,closable:false" style="width:250px;height:80px">  
		<div style="width:100%; text-align:center; margin-top: 10px">
			<img src="img/loader.gif">
		</div>	
	</div>
	
	 <!-- 	window ajuda campo Raz�o -->
	<div class="modal fade" id="modalAjuda" role="dialog">
	    <div class="modal-dialog">
	    
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Ajuda campo Raz�o Social</h4>
	        </div>
	        <div class="modal-body">
	          <p>
	          	<strong>Condominio:</strong> Lista todos os Clientes que possuem a palavra 'Condominio' e 'Residencial' em sua Raz�o Social.<br><br>
	          	<strong>Comercial:</strong> Lista todos os Cliente do tipo Pessoa Jur�ca, sem a palavra 'Condominio' e 'Residencial' em sua Raz�o Social.<br><br>
	          	<strong>Outros:</strong> Lista todos os Cliente do tipo Pessoa F�sica, sem a palavra 'Condominio' e 'Residencial' em sua Raz�o Social.<br>
	       </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        </div>
	      </div>
	      
	    </div>
	  </div>
	  
	   <!-- 	window ajuda geral -->
	   <div class="modal fade" id="modalAjudaGeral" role="dialog">
	    <div class="modal-dialog">
	    	     
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Ajuda campos em geral</h4>
	        </div>
	        <div class="modal-body">
	          <p>
	          	<strong>Data Faturamento:</strong> Filtra um per�odo entre as datas de Faturamento do Contrato do Cliente, � necessario preencher Inicio e Fim para listar o resultado desejado.<br><br>
	          	<strong>Data Cadastro:</strong> Filtra um per�odo entre as datas de Cadastro do Contrato do Cliente, � necessario preencher 'Inicio' e 'Fim' para listar o resultado desejado.<br><br>
	          	<strong>Faturamento Contrato:</strong> Filtra se o contrato � Corporativo ou Varejo, Corporativo s�o Clientes com faturamento maior que 1000 Reais e Varejo faturamento menor e igual a 1000 Reais.<br><br>
	          	<strong>Servi�os:</strong> Filtra os  Servi�os do Contrato, podem ser selecionados v�rios Servi�os para serem listados no resultado da pesquisa.<br><br>
	          	<strong>Situa��o:</strong> Filtra a situa��o do Contrato e do posto se os mesmos est�o Ativos ou Inativos.<br><br>
	          	<strong>Cidade Cliente:</strong> Filtra a cidade onde o Cliente � cadastrado.<br><br>
	          	<strong>Cidade Prest. Servi�o:</strong> Filtra a cidade onde o posto do Contrato � cadastrado.<br><br>
	          	<strong>Regionais:</strong> Filtra as  Regionais do Contrato, podem ser selecionadas v�rias Regionais para serem listadas no resultado da pesquisa.<br><br>
	          	<strong>Tipo Cliente:</strong> Filtra o tipo do Cliente do Contrato se o mesmo � cadastrado como P�blico ou Privado.<br><br>
	          	<strong>Tipo Pessoa:</strong> Filtra o tipo de Pessoa, se o Cliente � uma pessoa F�sica ou Jur�ca.<br><br>
	          	<strong>OBS:</strong> Todos os campos combobox que possuirem a palavra 'Selecione Abaixo' ir�o trazer todas as informa��es exemplo: Situa��o trar� Contratos Ativos e Inativos.<br>
	          </p>
	          
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        </div>
	      </div>
	      
	    </div>
	  </div>
	  

	<!-- 	window servi�os -->
	<div class="modal fade" id="modalServico" role="dialog" data-backdrop="static">
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Selecione abaixo os Servi�os desejados:</h4>
	        </div>
	        
	        <div class="modal-body">
	        	
	        	<div id="checkboxesServicos" style="width: 90%;">
				   <div id="esquerdaServico" style="float:left; width: 45%">

				   </div>
				   <div id="direitaServico" style="float:right; width: 45%">

				   </div>
				</div>
				<div style="clear:both"></div>       		
	      	    
	       </div>
	          
	       
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        </div>
	      </div>
	      
	    </div>
	 </div>
	 
	 <!-- 	window Regionais -->
	<div class="modal fade" id="modalRegional" role="dialog" data-backdrop="static">
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Selecione abaixo as Regionais desejadas:</h4>
	        </div>
	        
	        <div class="modal-body">
	        	
	        	<div id="checkboxesRegional" style="width: 90%;">
				   <div id="esquerdaRegional" style="float:left; width: 45%">

				   </div>
				   <div id="direitaRegional" style="float:right; width: 45%">

				   </div>
				</div>
				<div style="clear:both"></div>       		
	      	    
	       </div>
	          
	       
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        </div>
	      </div>
	      
	    </div>
	 </div>
	
	<!-- 	tabela -->
	<center>
	<table class="easyui-datagrid" id="listaContratos"
		 style="width:1470px;height:250px"
		data-options="singleSelect:true,collapsible:true" align = "center">
		<thead>
			<tr>
				
				<th data-options="field:'itemid',width:250" field="razao">Raz�o Social</th>
				<th data-options="field:'itemid',width:220" field="email">E-mail</th>
				<th data-options="field:'itemid',width:60" field="situacao">Situa��o</th>
				<th data-options="field:'itemid',width:120" field="regional">Regional</th>
				<th data-options="field:'itemid',width:100" field="cidadeContrato">Cidade P. Servi�o</th>
				<th data-options="field:'itemid',width:100" field="cidadeCliente">Cidade Cliente</th>
				<th data-options="field:'itemid',width:180" field="servico">Servi�o</th>
				<th data-options="field:'itemid',width:80" field="tipoCliente">Tipo Pessoa</th>
				<th data-options="field:'itemid',width:80" field="tipoEmc">Tipo Cliente</th>
				<th data-options="field:'itemid',width:80" field="dataInicio">Data Fat.</th>
				<th data-options="field:'itemid',width:80" field="dataCadastro">Data Cad.</th>
				<th data-options="field:'itemid',width:120" field="cpfCnpj">CPF/CNPJ</th>
		
			</tr>
		</thead>

	</table>
	</center>
	
	<div align="center">
					
			<input type="button" onclick="getExportadorXls()" id="exportar"
				class="btn btn-primary pull-center btn-pesquisar" value="Exportar">

	</div>
	
	

</body>
<!-- javascript -->
<script type="text/javascript">
   
    	function getListaContratos() {
    		
	    		var servicoString = [];
	    		var regionalString = [];
	    		$('#checkboxesServicos input:checked').each(function() {
	    			servicoString.push($(this).attr('id'));
	    		});
	    		
	    		$('#checkboxesRegional input:checked').each(function() {
	    			regionalString.push($(this).attr('id'));
	    		});
	    		
    		    startProgress();
    		    
    		    var razaoSocial = document.getElementById("cbRazao").value;
    	        var cidadeContrato = document.getElementById("cidadeContrato").value
    	        var cidadeCliente = document.getElementById("cidadeCliente").value
    	        var faturamentoContrato = document.getElementById("cbFaturamento").value;
    	        var situacao = document.getElementById("cbSituacao").value;
    	        var regional = regionalString.join();
    	        var servico = servicoString.join();
    	        var dataInicio = document.getElementById("dataInicio").value;
    	        var dataFim = document.getElementById("dataFim").value;    	        
    	        var tipoEmc = document.getElementById("cbEmc").value;
     	        var tipoCliente = document.getElementById("cbCliente").value;
    	        var dataCadastroInicio = document.getElementById("dataCadastroInicio").value;
    	        var dataCadastroFim = document.getElementById("dataCadastroFim").value;
        
        $.ajax({
        method: 'post',
        url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.mainlingcontratos.servlet.MainlingContratosServlet',
        data: 'action=listaContratos&razaoSocial=' + razaoSocial + '&faturamentoContrato=' + faturamentoContrato + '&cidadeContrato=' + cidadeContrato + '&situacao=' + situacao +'&regional=' + regional +'&servico=' + servico +'&cidadeCliente=' + cidadeCliente + '&dataInicio=' + dataInicio + '&dataFim=' + dataFim + '&tipoEmc=' + tipoEmc + '&tipoCliente=' + tipoCliente+ '&dataCadastroInicio=' + dataCadastroInicio + '&dataCadastroFim=' + dataCadastroFim,
                                                                                
     	 dataType: 'json',
 		success: function(result) {
				
 			stopProgress();
 			if(result==""){
 				$.growl.error({ message: "Nenhuma informa��o a listar!" });
 			}
 			else{
 				$.growl.notice({ message: "Pesquisa realizada com Sucesso!" });
 			}
 			
                                                            
          $('#listaContratos').datagrid('loadData', result);
                                                                                    
           },
            error: function(e) {
       		
       		alert("tesate error : "+e);
             }
          });
     
         }
    	
    	 $(document).ready(function getListaRegionais(){
    		
    		  $.ajax({
    		        method: 'post',
    		        url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.mainlingcontratos.servlet.MainlingContratosServlet',
    		        data: 'action=listaRegionais',
    		                                                                                
    		     	 dataType: 'json',
    		 		success: function(result) {
    		         
    		 			var count = 1;
    		 			
    		 			for(i in result){
    		 			
    		 				var resto = count % 2;
    		 				
    		 				if(resto==0){
    		 					
    		 					 $('#esquerdaRegional').append("<input type='checkbox' id="+ result[i].codigoRegional +"></input><b> "+result[i].nomeRegional+"</b><br>");		
 								
							}else{
								 $('#direitaRegional').append("<input type='checkbox' id="+ result[i].codigoRegional +"></input><b> "+result[i].nomeRegional+"</b><br>");
							} 
    		 				    		 				
    		 				count ++;
    		 			}
    		 		
    		 			
    		          $('#listaRegionais').datagrid('loadData', result);
    		                                                                                    
    		           },
    		            error: function(e) {
    		       		error();
    		             }
    		          });
    		
    	})
    	
    	 $(document).ready(function getListaServicos(){
    		
    		
    		  $.ajax({
    		        method: 'post',
    		        url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.mainlingcontratos.servlet.MainlingContratosServlet',
    		        data: 'action=listaServicos',
    		                                                                                
    		     	 dataType: 'json',
    		 		success: function(result) {
    		 			
    		            var count = 1;
    		 		    		 			
    		 			for(i in result){
    		 			   
    		 				var resto = count % 2;
							if (resto == 0){
								 $('#esquerdaServico').append("<input type='checkbox' id="+ result[i].abreviatura +"></input><b> "+result[i].descricaoServico+"</b><br>");		
								
							}else{
								 $('#direitaServico').append("<input type='checkbox' id="+ result[i].abreviatura +"></input><b> "+result[i].descricaoServico+"</b><br>");
							}    		 				

     		 				count ++;
     		 				
    		 			}
    		 		
    		 			
    		          $('#listaServicos').datagrid('loadData', result);
    		                                                                                    
    		           },
    		            error: function(e) {
    		       		error();
    		             }
    		          });
    		
    	})
    	
    	function getExportadorXls() {
    		
    		
    			var servicoString = [];
	    		var regionalString = [];
	    		$('#checkboxesServicos input:checked').each(function() {
	    			servicoString.push($(this).attr('id'));
	    		});
	    		
	    		$('#checkboxesRegional input:checked').each(function() {
	    			regionalString.push($(this).attr('id'));
	    		});
	    	    		    
    		    var razaoSocial = document.getElementById("cbRazao").value;
    	        var cidadeContrato = document.getElementById("cidadeContrato").value
    	        var cidadeCliente = document.getElementById("cidadeCliente").value
    	        var faturamentoContrato = document.getElementById("cbFaturamento").value;
    	        var situacao = document.getElementById("cbSituacao").value;
    	        var regional = regionalString.join();
    	        var servico = servicoString.join();
    	        var dataInicio = document.getElementById("dataInicio").value;
    	        var dataFim = document.getElementById("dataFim").value;    	        
    	        var tipoEmc = document.getElementById("cbEmc").value;
     	        var tipoCliente = document.getElementById("cbCliente").value;
    	        var dataCadastroInicio = document.getElementById("dataCadastroInicio").value;
    	        var dataCadastroFim = document.getElementById("dataCadastroFim").value;
 	       	    var  urL = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.mainlingcontratos.servlet.MainlingContratosServlet?action=exportadorXls&razaoSocial=' + razaoSocial + '&faturamentoContrato=' + faturamentoContrato + '&cidadeContrato=' + cidadeContrato + '&situacao=' + situacao +'&regional=' + regional +'&servico=' + servico +'&cidadeCliente=' + cidadeCliente + '&dataInicio=' + dataInicio + '&dataFim=' + dataFim + '&tipoEmc=' + tipoEmc + '&tipoCliente=' + tipoCliente + '&dataCadastroInicio=' + dataCadastroInicio +'&dataCadastroFim=' + dataCadastroFim;
 		        window.location.href=urL;
 		
    	
    	 }
    	 
    	  function startProgress(){
          	$('#dlgLoader').window('open');
          }
          
          function stopProgress(){
          	$('#dlgLoader').window('close');
          }
  
    	
    	
</script>




</html>