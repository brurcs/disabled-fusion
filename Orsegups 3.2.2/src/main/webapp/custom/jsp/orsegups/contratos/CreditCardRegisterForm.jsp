<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="css/CreditCardRegisterForm.css">
<script type="text/javascript" src="js/CreditCardRegister.js" charset="UTF-8"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<title>Insert title here</title>
</head>
<body>

	<%
	String codigoCliente = request.getParameter("codigoCliente");
	%>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">

				<!-- IN�CIO DO FORMUL�RIO -->
				
				<div class="panel panel-default credit-card-box">
					<div class="panel-heading display-table">
						<h3 class="panel-title display-td">Cadastro de Cart�o de
							Cr�dito</h3>
					</div>
					<!-- N�mero do Cart�o de Cr�dito -->
					<div class="panel-body">											   
							<div class="col-xs-12" id="divMensagemRetorno" style="display:none">
									<p id="mensagemRetorno"></p>
							</div>																				
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label for="numeroCartao">N�mero do Cart�o de Cr�dito</label>
										<div class="input-group">
											<input type="text" class="form-control" id="numeroCartao"
												name="numeroCartao" 
												placeholder="0000 0000 0000 0000" maxlength="16" 
												required autofocus 
												onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" /> 
												<span class="input-group-addon">
												<i class="glyphicon glyphicon-credit-card"></i></span>
										</div>
									</div>
								</div>
							</div>
							<!-- Nome no Cart�o -->
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label for="nomeCliente">Nome no Cart�o</label>
										<input
											type="text" class="form-control" id="nomeCliente"
											name="nomeCliente" maxlength="65"/>
									</div>
								</div>
							</div>
							<!-- Bandeira -->
							<div class="form-group">
								<label for="bandeiraCartao">Bandeira</label> <select
									class="form-control" id="bandeiraCartao" name="bandeiraCartao">
									<option>Visa</option>
									<option>MasterCard</option>
									<option>American Express</option>
									<option>Diners</option>
									<option>Elo</option>
								</select>
							</div>
							<!-- Data de Validade -->
							<div class="row">
								<div class="col-xs-7 col-md-7">
									<div class="form-group">
										<label for="dataValidade">Data de Validade</label> <input
											type="tel" class="form-control" name="dataValidade"
											onkeyup="formatDateValid()" id="dataValidade"											
											placeholder="MM/AA" maxlength="5" required 
											onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;"
											onpaste="return false"/>
									</div>
								</div>
								<!-- C�digo de Verifica��o -->
								<div class="col-xs-5 col-md-5 pull-right">
									<label for="cvv">CVV</label>
									<div class="input-group">
										<input type="password" class="form-control"
											name="codigoVerificacao" id="codigoVerificacao"
											pattern="[0-9]{3}" placeholder="000" maxlength="3" required 
											onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" on/>
										<span class="input-group-addon" id="eyeIcon"
											onmouseover="showPassword()" onmouseout="hidePassword()"><i
											class="glyphicon glyphicon-eye-open"></i></span>
									</div>
								</div>
							</div>
							<!-- Submit Button -->
							<div class="row">
								<div class="col-xs-12">
								<button class="botaoCadastrar"
										onClick="cadastrar(<%=codigoCliente%>)">Cadastrar</button>
								</div>
							</div>
					</div>
				</div>
				<!-- Fim do Formul�rio -->
			</div>

		</div>
	</div>


</body>
</html>