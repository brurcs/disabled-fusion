
app.controller('appcontrollercadastrochp1', ['$scope','$http','$timeout','$interval','$filter', function($scope, $http, $timeout, $interval, $filter){
	    
	$scope.documentos = [];
	$scope.documentosView = [];
	
	$scope.tiposDocumentos = [];
	$scope.tipoDocumentoSel = '';
	$scope.empresas = [];
	$scope.operadoras = [];
	$scope.operadoras2 = [];
	$scope.regionais = [];
	$scope.ativos = [];
	$scope.empresaSel = $scope.empresas[0];
	$scope.operadoraSel = $scope.operadoras[0];
	$scope.operadora2Sel = $scope.operadoras2[0];
	$scope.regionalSel = $scope.regionais[0];
	
	$scope.regionalSelecionada;
	
	$scope.ativoSel = $scope.ativos[0];
	$scope.operadora2Sel;
	$scope.competencia;
	$scope.tags = [];
	$scope.tags2 = [];
	
	$scope.pagination = {
		currentPage: 1,
		numPerPage:10,
		totalItems:0
	};
	
	$scope.replicars = [
	                 {id:0, value:'Nao', ativo:false},
	                 {id:1, value:'Sim', ativo:true}
	               ];
	$scope.replicar = $scope.replicars[1];
	
	$scope.operadoras = [];
	$scope.operadoras2 = [];
	
	$scope.regionais = [];
	
	$scope.chipes = [];
	$scope.chipClientes = [];
		
	$scope.totalItems = 0;
    $scope.totalRegistros = 0;
    $scope.totalRegistrosCliente = 0;
    $scope.totalAguardandoIdentificacao = 0;
    $scope.totalNaoIdentificados = 0;
    $scope.totalRastreamento = 0;
    $scope.contaTotalTI = 0;
    $scope.contaTotalTIaCancelar = 0;
    $scope.contaTotalRegionais = 0;
    $scope.contaNaoIdentificados = 0;
    
    $scope.cnpj;
    
    $scope.hInicio=1;
    $scope.hFim=11;
    
    $scope.contaSIGMA;
    $scope.chp1Dados;
    
    $scope.neoId;
    
    $scope.cdCliente;
    $scope.empresaConta;
    $scope.endereco;
    $scope.particao;
    $scope.razaoSocial;
    $scope.iccId;
    $scope.validIccId;    
    $scope.dataCancelamentoFormatada;
    
    $scope.habilitadoIccId = false;
    $scope.habilitadoDataCancelamento = false;
    
    $scope.totalChipContratado = 0;
    $scope.totalChipNext = 0;
    $scope.contaTotalDeggy = 0;
    
    $scope.botaoSalvarAtualizar = 'Salvar';
    
    $scope.contaTotalContratado = 0;
    $scope.contaTotalCancelado = 0;
    $scope.contaTotalGprs = 0;
    $scope.contaTotalContaGprs = 0;
    $scope.contaTotalInativo = 0;
    $scope.contaTotalAtivo = 0;
    $scope.contaIdentificados = 0;
    $scope.contaAguardandoIdentificacao = 0;
    $scope.contaNaoIdentificado = 0;
    $scope.contaTotalNexti = 0;
    $scope.contaTotalRastreamento = 0;
    $scope.contaTotalAC = 0;
    $scope.contaTotalFT = 0;
    $scope.contaValidacao = 0;
    $scope.contaValidacaoPositiva = 0;
    $scope.contaValidacaoNegativa = 0;
    $scope.contaValidacaoAguardando = 0;
    $scope.contaOSComplementar = 0;
    
    $scope.botaoRemover = true;
    
    $scope.totalIdentificado = 0;
    $scope.totalNaoIdentificado = 0;
    
    $scope.contaTotalEncaronada = 0
    $scope.contaTotalVincClientePorTarefa = 0;
    
    $scope.successTextAlert = "Chip salvo com sucesso!";
    $scope.warningTextAlert = "Chip já cadastrado!";
    $scope.showSuccessAlert = false;
    $scope.showWarningAlert = false;
    
    //Paginaion
    $scope.totalResults = 0;
    
    $scope.search = { empresa: "", operadora: "", iccId: "", linha:"", regional:"", regional:"", ativo:"" }
    
	$http.post('http://localhost:9090/fusion/services/eform/getChp1Empresa').success(function(data, status) {
		
		if (data){
			$scope.empresas = data;
		}else{
			alert('Não foi possível obter as empresas!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados1");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/getChp1Operadora').success(function(data, status) {
		
		if (data){
			$scope.operadoras = data;
			$scope.operadoras2 = data;
		}else{
			alert('Não foi possível obter as operadoras!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados2");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaChp1').success(function(data, status) {
		
		if (data){
			$scope.totalRegistros = data;
		}else{
			alert('Não foi possível obter as contas!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados3");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalContratado').success(function(data, status) {
		
		if (data){
			$scope.contaTotalContratado = data;
		}else{
			alert('Não foi possível contaTotalContratado!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalContratado");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalCancelado').success(function(data, status) {
		
		if (data){
			$scope.contaTotalCancelado = data;
		}else{
			alert('Não foi possível contaTotalCancelado!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalCancelado");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalAtivo').success(function(data, status) {
		
		if (data){
			$scope.contaTotalAtivo = data;
		}else{
			alert('Não foi possível contaTotalAtivo!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalContratado");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalInativo').success(function(data, status) {
		
		if (data){
			$scope.contaTotalInativo = data;
		}else{
			alert('Não foi possível contaTotalInativo!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalInativo");
	});
	
$http.post('http://localhost:9090/fusion/services/eform/contaTotalNexti').success(function(data, status) {
		
		if (data){
			$scope.contaTotalNexti = data;
		}else{
			alert('Não foi possível contaTotalNexti!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalContratado");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalGprs').success(function(data, status) {
		
		if (data){
			$scope.contaTotalGprs = data;
		}else{
			alert('Não foi possível obter as contaTotalGprs!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalGprs");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalContaGprs').success(function(data, status) {
		
		if (data){
			$scope.contaTotalContaGprs = data;
		}else{
			alert('Não foi possível obter as contaTotalContaGprs!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalContaGprs");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaIdentificados').success(function(data, status) {
		
		if (data){
			$scope.contaIdentificados = data;
		}else{
			alert('Não foi possível contaIdentificados!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaIdentificados");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaAguardandoIdentificacao').success(function(data, status) {
		
		if (data){
			$scope.contaAguardandoIdentificacao = data;
		}else{
			alert('Não foi possível contaAguardandoIdentificacao!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaAguardandoIdentificacao");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaNaoIdentificado').success(function(data, status) {
		
		if (data){
			$scope.contaNaoIdentificado = data;
		}else{
			alert('Não foi possível contaNaoIdentificado!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaNaoIdentificado");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalRastreamento').success(function(data, status) {
		
		if (data){
			$scope.contaTotalRastreamento = data;
		}else{
			alert('Não foi possível contaTotalRastreamento!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalRastreamento");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalTI').success(function(data, status) {
		
		if (data){
			$scope.contaTotalTI = data;
		}else{
			alert('Não foi possível contaTotalTI!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalTI");
	});
	
	$http.post('http://localhost:9090/fusion/services/eform/contaTotalTIaCancelar').success(function(data, status) {
		
		if (data){
			$scope.contaTotalTIaCancelar = data;
		}else{
			alert('Não foi possível contaTotalTIaCancelar!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalTIaCancelar");
	});

$http.post('http://localhost:9090/fusion/services/eform/contaTotalRegionais').success(function(data, status) {
		
		if (data){
			$scope.contaTotalRegionais = data;
		}else{
			alert('Não foi possível contaTotalRegionais!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível contaTotalRegionais");
	});

$http.post('http://localhost:9090/fusion/services/eform/contaNaoIdentificados').success(function(data, status) {
	
	if (data){
		$scope.contaNaoIdentificados = data;
	}else{
		alert('Não foi possível contaNaoIdentificados!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaNaoIdentificados");
});

$http.post('http://localhost:9090/fusion/services/eform/totalIdentificado').success(function(data, status) {
	
	if (data){
		$scope.totalIdentificado = data;
	}else{
		alert('Não foi possível totalIdentificado!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível totalIdentificado");
});
	

$http.post('http://localhost:9090/fusion/services/eform/totalNaoIdentificado').success(function(data, status) {
	
	if (data){
		$scope.totalNaoIdentificado = data;
	}else{
		alert('Não foi possível totalNaoIdentificado!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível totalNaoIdentificado");
});
	
$http.post('http://localhost:9090/fusion/services/eform/contaTotalEncaronada').success(function(data, status) {
	
	if (data){
		$scope.contaTotalEncaronada = data;
	}else{
		alert('Não foi possível contaTotalEncaronada!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaTotalEncaronada");
});

$http.post('http://localhost:9090/fusion/services/eform/contaTotalVisuVTR').success(function(data, status) {
	
	if (data){
		$scope.contaTotalVisuVTR = data;
	}else{
		alert('Não foi possível contaTotalVisuVTR!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaTotalVisuVTR");
});

$http.post('http://localhost:9090/fusion/services/eform/contaTotalVisuVTRImagem').success(function(data, status) {
	
	if (data){
		$scope.contaTotalVisuVTRImagem = data;
	}else{
		alert('Não foi possível contaTotalVisuVTRImagem!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaTotalVisuVTRImagem");
});	

$http.post('http://localhost:9090/fusion/services/eform/contaTotalDeggy').success(function(data, status) {
	
	if (data){
		$scope.contaTotalDeggy = data;
	}else{
		alert('Não foi possível contaTotalDeggy!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaTotalDeggy");
});	

$http.post('http://localhost:9090/fusion/services/eform/contaTotalAC').success(function(data, status) {
	
	if (data){
		$scope.contaTotalAC = data;
	}else{
		alert('Não foi possível contaTotalAC!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaTotalAC");
});	

$http.post('http://localhost:9090/fusion/services/eform/contaTotalFT').success(function(data, status) {
	
	if (data){
		$scope.contaTotalFT = data;
	}else{
		alert('Não foi possível contaTotalFT!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaTotalFT");
});	

$http.post('http://localhost:9090/fusion/services/eform/contaValidacao').success(function(data, status) {
	
	if (data){
		$scope.contaValidacao = data;
	}else{
		alert('Não foi possível contaValidacao!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaValidacao");
});	

$http.post('http://localhost:9090/fusion/services/eform/contaValidacaoPositiva').success(function(data, status) {
	
	if (data){
		$scope.contaValidacaoPositiva = data;
	}else{
		alert('Não foi possível contaValidacaoPositiva!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaValidacaoPositiva");
});

$http.post('http://localhost:9090/fusion/services/eform/contaValidacaoNegativa').success(function(data, status) {
	
	if (data){
		$scope.contaValidacaoNegativa = data;
	}else{
		alert('Não foi possível contaValidacaoNegativa!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaValidacaoNegativa");
});

$http.post('http://localhost:9090/fusion/services/eform/contaValidacaoAguardando').success(function(data, status) {
	
	if (data){
		$scope.contaValidacaoAguardando = data;
	}else{
		alert('Não foi possível contaValidacaoAguardando!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaValidacaoAguardando");
});


$http.post('http://localhost:9090/fusion/services/eform/contaOSComplementar').success(function(data, status) {
	
	if (data){
		$scope.contaOSComplementar = data;
	}else{
		alert('Não foi possível contaOSComplementar!!')
	}
	
	$('#processing-modal').modal('hide');
}).error(function() {
	alert("Não foi possível contaOSComplementar");
});


	/*$http.post('http://localhost:9090/fusion/services/eform/contaChp1Cliente').success(function(data, status) {
		
		if (data){
			$scope.totalRegistrosCliente = data;
			//$scope.totalAguardandoIdentificacao = $scope.totalRegistros - $scope.totalRegistrosCliente;
			//alert($scope.totalRegistros);
		}else{
			alert('Não foi possível obter as operadoras!!!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados22");
	});
		
	$http.post('http://localhost:9090/fusion/services/eform/getChp1;inicio='+$scope.hInicio+';fim='+$scope.hFim).success(function(data, status) {
				
		
		
		if (data){
						
			$scope.chipes = data;
			
			
			
			$scope.currentPage = 1;
			$scope.predicate = 'descricao';  
		    //$scope.reverse = true; 
		     
		    $scope.order = function (predicate) {  
		      $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;  
		      $scope.predicate = predicate;  
		    }; 
		    $scope.totalItems =  $scope.totalRegistros;
		    
		    $scope.numPerPage = 10;  
		    $scope.paginate = function (value) {  
		    	alert($scope.hInicio+','+$scope.hFim);
		    	console.log(value);
		      var begin, end, index;  
		      begin = ($scope.currentPage - 1) * $scope.numPerPage; 
		      $scope.hInicio = begin;
		      end = begin + $scope.numPerPage;
		      $scope.hFim = end;
		      index = $scope.chipes.indexOf(value); 
		      
		      return (begin <= index && index < end);  
		    };
		}else{
			alert('Não foi possível obter os chipes!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados5");
	});
	
    $http.post('http://localhost:9090/fusion/services/eform/getChp1Cliente').success(function(data, status) {
		
		
		if (data){
						
			$scope.chipClientes = data;
			
			$scope.currentPageCliente = 1;
			$scope.predicateCliente = 'razaoSocial';  
		    //$scope.reverse = true; 
		     
		    $scope.orderCliente = function (predicateCliente) {  
		      $scope.reverseCliente = ($scope.predicateCliente === predicateCliente) ? !$scope.reverseCliente : false;  
		      $scope.predicateCliente = predicateCliente;  
		    }; 
		    $scope.totalItemsCliente =  $scope.totalRegistrosCliente;
		    
		    $scope.numPerPageCliente = 10;  
		    $scope.paginateCliente = function (value) {  
		      var beginCliente, endCliente, indexCliente;  
		      beginCliente = ($scope.currentPageCliente - 1) * $scope.numPerPageCliente; 
		      $scope.hInicioCliente = beginCliente;
		      endCliente = beginCliente + $scope.numPerPageCliente;
		      $scope.hFimCliente = endCliente;
		      indexCliente = $scope.chipClientes.indexOf(value); 
		      
		      return (beginCliente <= indexCliente && indexCliente < endCliente);  
		    };
		}else{
			alert('Não foi possível obter os chipesCliente!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados25");
	});
	*/
	$http.post('http://localhost:9090/fusion/services/eform/getRegional').success(function(data, status) {
		
		if (data){
			$scope.regionais = data;
		}else{
			alert('Não foi possível obter as regionais!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados4");
	});
	
	$scope.inserir = function(){

		var dataCancelamentoValida = true;
		if ($scope.dataCancelamentoFormatada != null && $scope.dataCancelamentoFormatada != "" && $scope.dataCancelamentoFormatada != "undefined") 
		{
			$scope.dataCancelamentoFormatada = $scope.dataCancelamentoFormatada.replace('/','-').replace('/','-');
			
			//Formata a data informada pelo usuário]
			var tmpDate = $scope.dataCancelamentoFormatada;
			var d = tmpDate.split('-');
			var dataInformada = new Date(d[2], d[1] - 1,d[0]);

			// Pega a data de hoje
			var dataHoje = new Date();
			
			
			//Verifica data de cancelamento é futura
			if (dataInformada > dataHoje)
			{
				dataCancelamentoValida = false;
				
				$scope.warningTextAlert = "Data de cancelamento deve ser igual ou inferior a data de hoje.";
				$scope.showSuccessAlert = false;		
				$scope.showWarningAlert = true;			
				$scope.dataCancelamentoFormatada = "";
				
				$('#processing-modal').modal('hide');
				
			}
		}
		
		if (dataCancelamentoValida)
		{
			
			var iccIdTemp = $scope.iccid;		
			if ($scope.botaoSalvarAtualizar == 'Salvar')
			{
				$http.post('http://localhost:9090/fusion/services/eform/verificaChipExistente;IccId='+iccIdTemp).success(function(data, status) {
				
				// Retorna TRUE quando já existe o IccId cadastrado
				if (!data)
				{
					var oper2;
					var reg;
					
					if ($scope.operadora2Sel != null) {
						oper2 = $scope.operadora2Sel.id;
					}
					
					if ($scope.regionalSel != null) {
						reg = $scope.regionalSel.id;
					}
					
					$http.post('http://localhost:9090/fusion/services/eform/setChip;empresa='+$scope.empresaSel.id+';operadora='+$scope.operadoraSel.id+';operadora2='+oper2+';ativo='+$scope.ativoSel.id+';regional='+reg+';linha='+$scope.linha+';iccId='+$scope.iccid+';neoId='+$scope.neoId+';dataCancelamentoFormatada='+$scope.dataCancelamentoFormatada).success(function(data, status) {

						$scope.successTextAlert = "Chip salvo com sucesso!";
						$scope.showSuccessAlert = true;		
						$scope.showWarningAlert = false;
							
						if($scope.neoId == null || $scope.neoId == '') {
							$scope.iccid = '';
							$scope.linha = '';
							$scope.dataCancelamentoFormatada = '';
						}
						
						$('#processing-modal').modal('hide');

					}).error(function() {
						alert("Não foi possível salvar chip.");
					});				
				}
				else
				{
					$('#processing-modal').modal('hide');
					
					$scope.warningTextAlert = "Chip Id já cadastrado.";
					$scope.showSuccessAlert = false;
					$scope.showWarningAlert = true;
				}
								
				}).error(function() {
					alert("Não foi possível verificar IccId.");
					
					$('#processing-modal').modal('hide');				
				});		
				
			}
			else
			{
				var oper2;
				var reg;
				
				if ($scope.operadora2Sel != null) {
					oper2 = $scope.operadora2Sel.id;
				}
				
				if ($scope.regionalSel != null) {
					reg = $scope.regionalSel.id;
				}
				
				var iccIdTemp = $scope.iccid;

				$http.post('http://localhost:9090/fusion/services/eform/setChip;empresa='+$scope.empresaSel.id+';operadora='+$scope.operadoraSel.id+';operadora2='+oper2+';ativo='+$scope.ativoSel.id+';regional='+reg+';linha='+$scope.linha+';iccId='+$scope.iccid+';neoId='+$scope.neoId+';dataCancelamentoFormatada='+$scope.dataCancelamentoFormatada).success(function(data, status) {

				$scope.successTextAlert = "Chip atualizado com sucesso!";
				$scope.showSuccessAlert = true;		
				$scope.showWarningAlert = false;
				
				if($scope.neoId == null || $scope.neoId == '') {
					$scope.iccid = '';
					$scope.linha = '';
					$scope.dataCancelamentoFormatada = '';
				}
				
				$('#processing-modal').modal('hide');
				
				if ($scope.dataCancelamentoFormatada != null) 
					$scope.dataCancelamentoFormatada = $scope.dataCancelamentoFormatada.replace('-','/').replace('-','/');

				}).error(function() {
					
					$('#processing-modal').modal('hide');
					
					alert("Não foi possível atualizar chip.");
				});
			}
		}
		
	};	

	$scope.filtrar = function(search){
		
		var sEmpresa = (search.empresa != null) ? search.empresa : '';
		var sOperadora = (search.operadora != null) ? search.operadora : '';
		var sLinha = (search.linha != null) ? search.linha : '';
		var sIccid = (search.iccId != null) ? search.iccId : '';
		var sCancelado = (search.cancelado != null) ? search.cancelado : '';
		var sHabilitado = (search.habilitado != null) ? search.habilitado : '';
		var sEmCliente = (search.emCliente != null) ? search.emCliente : '';
		var sEmEstoque = (search.emEstoque != null) ? search.emEstoque : '';
		var sRegional = (search.regional != null) ? search.regional : '';
		var sAtivo = search.ativo;
		var sDataCancelamento = (search.dataCancelamentoFiltrada != null) ? search.dataCancelamentoFiltrada : '';
		
		var tempDataCancelamento = ""; 
		
		if (sDataCancelamento.length >= 10)
		{
			tempDataCancelamento = sDataCancelamento.replace("/","-").replace("/","-")	
		}
		
		$http.post('http://localhost:9090/fusion/services/eform/countChips;iccId='+sIccid+';empresa='+sEmpresa+';operadora='+sOperadora+';operadora2='+sOperadora+';regional='+sRegional+';linha='+sLinha+';dataCancelamento='+tempDataCancelamento+';cancelado='+sCancelado+';habilitado='+sHabilitado+';emCliente='+sEmCliente+';emEstoque='+sEmEstoque).success(function(data, status) {

			$scope.pagination.totalItems = data;
			
			$http.post('http://localhost:9090/fusion/services/eform/getChips;iccId='+sIccid+';empresa='+sEmpresa+';operadora='+sOperadora+';operadora2='+sOperadora+';regional='+sRegional+';linha='+sLinha+';dataCancelamento='+tempDataCancelamento+';cancelado='+sCancelado+';habilitado='+sHabilitado+';emCliente='+sEmCliente+';emEstoque='+sEmEstoque+';pageNumber='+$scope.pagination.currentPage+';pageResults='+$scope.pagination.numPerPage).success(function(data2, status) {

				$scope.chipes = data2;
				
				$scope.showSuccessAlert = false;		
				$scope.showWarningAlert = false;

			}).error(function() {
				alert("Não foi possível retornar os chips cadastrados.");
				
			});
			
			
		}).error(function() {
			alert("Não foi possível identificar a quantidade de chips cadastrados.");
			
		});

		$('#processing-modal').modal('hide');
		
	}
	
	$scope.verificaChipExistente = function (iccid) {
		
		$http.post('http://localhost:9090/fusion/services/eform/verificaChipExistente;IccId='+iccid).success(function(data, status) {
		
		return data;
					
		}).error(function() {
			alert("Não foi possível verificar IccId");
		});
	};
			
	
	$scope.filtrarCliente = function(empresa,iccid,instalacao,remocao){
		
		var sEmpresa = (empresa != null) ? empresa : '';
		var sIccid = (iccid != null) ? iccid : '';
		var sInstalacao = (instalacao != null) ? instalacao : '';
		var sRemocao = (remocao != null) ? remocao : ''; 
		
		$http.post('http://localhost:9090/fusion/services/eform/getChipsCliente;iccId='+sIccid+';empresa='+sEmpresa+';instalacao='+sInstalacao+';remocao='+sRemocao).success(function(data, status) {
		    
			$scope.chipClientes = data;
			console.log(data);
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados8");
		});		
	}
	
	$scope.editar = function(query){
				
		if (query.dataCancelamentoFormatada == null || query.dataCancelamentoFormatada == '') {
			$scope.habilitadoDataCancelamento = false;
			$scope.dataCancelamentoFormatada = null;
		} else {			
			$scope.habilitadoDataCancelamento = true;
			$scope.dataCancelamentoFormatada = query.dataCancelamentoFormatada.replace("-","/");
		}
		
		$scope.habilitadoIccId = true;
		
		
		if(query.id != null) {
			$scope.neoId = query.id;
			$scope.botaoSalvarAtualizar = 'Atualizar';
		} else {
			$scope.neoId = '';
			$scope.botaoSalvarAtualizar = 'Salvar';
		}
		$scope.empresaSel = query.empresa.id;
		$scope.operadoraSel = query.operadora.id;
		$scope.operadora2Sel = (query.operadora2 != null) ? query.operadora2.id : '';
		$scope.linha = query.linha;
		$scope.iccid = query.iccId;
		
		
		var resultado = $filter("filter")($scope.operadoras,{id:query.operadora.id}); 		
		var index = $scope.operadoras.indexOf(resultado[0]);		
		$scope.operadoraSel = $scope.operadoras[index];
		
		if($scope.operadora2Sel != '') {
			resultado = $filter("filter")($scope.operadoras2,{id:query.operadora2.id}); 		
			index = $scope.operadoras2.indexOf(resultado[0]);		
			$scope.operadora2Sel = $scope.operadoras2[index];
		}
		
		resultado = $filter("filter")($scope.empresas,{id:query.empresa.id}); 
		index = $scope.empresas.indexOf(resultado[0]);
		$scope.empresaSel = $scope.empresas[index]; 	
				
		if(query.ativo != null) {
			resultado = $filter("filter")($scope.replicars,{ativo:query.ativo}); 
			index = $scope.replicars.indexOf(resultado[0]);	
			$scope.ativoSel = $scope.replicars[index]; 
		}
		
		if(query.regional != null) {
			resultado = $filter("filter")($scope.regionais,{id:query.regional.id}); 
			index = $scope.regionais.indexOf(resultado[0]);
			$scope.regionalSel = $scope.regionais[index]; 		
		}
		
	    $scope.showSuccessAlert = false;
	    $scope.showWarningAlert = false;			
	}
	
	$scope.limpar = function(){

		$scope.empresaSel = null;
		$scope.operadoraSel = null;
		$scope.operadora2Sel = null;
		$scope.regionalSel = null;
		$scope.linha = null;
		$scope.iccid = null;
		$scope.ativoSel = null;
		$scope.habilitadoIccId = false;
		$scope.dataCancelamentoFormatada = null;
		$scope.habilitadoDataCancelamento = false;
		$scope.neoId = '';
		
		$scope.botaoSalvarAtualizar = 'Salvar';
	}
	
	
	
	$scope.pesquisarCliente = function(query){
					
		return $http.post('http://localhost:9090/fusion/services/eform/getCliente;cliente='+query).success(function(data, status) {
					
		}).error(function() {
			alert("Não foi possível carregar os dados9");
		});
	};
	
	$scope.pesquisarCdCliente = function(query){
		
		return $http.post('http://localhost:9090/fusion/services/eform/getCdCliente;cdCliente='+query).success(function(data, status) {
					
		}).error(function() {
			alert("Não foi possível carregar os dados10");
		});
	};
	
	$scope.pesquisarChip = function(query){
		
		return $http.post('http://localhost:9090/fusion/services/eform/getChip;chip='+query).success(function(data, status) {
						
		}).error(function() {
			alert("Não foi possível carregar os dados11");
		});
	};
	
		
	$scope.inserirClienteChip = function(){
		
		$http.post('http://localhost:9090/fusion/services/eform/setChp1Cliente;cdCLiente='+$scope.tags[0].cdCliente+';iccId='+$scope.tags[0].id+';replicar='+$scope.replicar.id).success(function(data, status) {
		
		$scope.iccid = '';
		$scope.linha = '';
		$scope.dataCancelamentoFormatada = '';
		
		/*
		$http.post('http://localhost:9090/fusion/services/eform/getChp1Cliente').success(function(data, status) {
			
			
			if (data){
							
				$scope.chipClientes = data;
				
				$scope.currentPageCliente = 1;
				$scope.predicateCliente = 'descricao';  
			    //$scope.reverse = true; 
			     
			    $scope.orderCliente = function (predicateCliente) {  
			      $scope.reverseCliente = ($scope.predicate === predicateCliente) ? !$scope.reverseCliente : false;  
			      $scope.predicateCliente = predicateCliente;  
			    }; 
			    $scope.totalItemsCliente =  $scope.totalRegistrosCliente;
			    
			    $scope.numPerPageCliente = 10;  
			    $scope.paginateCliente = function (value) {  
			      var beginCliente, endCliente, indexCliente;  
			      beginCliente = ($scope.currentPageCliente - 1) * $scope.numPerPageCliente; 
			      $scope.hInicioCliente = beginCliente;
			      endCliente = beginCliente + $scope.numPerPageCliente;
			      $scope.hFimCliente = endCliente;
			      indexCliente = $scope.chipClientes.indexOf(value); 
			      
			      return (beginCliente <= indexCliente && indexCliente < endCliente);  
			    };
			}else{
				alert('Não foi possível obter os chipes!')
			}
			
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados5");
		});
		*/
		
		}).error(function() {
			alert("Não foi possível carregar os dados12");
		});
		$('#processing-modal').modal('hide');
	};	
	
	
		
}]);
