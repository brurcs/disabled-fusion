<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>


<portlet:defineObjects />
<form:defineObjects />
<head>
	<meta charset="UTF-8">
</head>
<portal:head title="Relat�rio CEREC">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script language="JavaScript" type="text/javascript">
		
</script>
<cw:main>

	<cw:header title="Relat�rios Comercial P�blico" />
	
	<cw:body id="area_scroll">
	
	
	<div class="float_icos">
		<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioConciliacao()">
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>Concilia��o</strong>
	</p>
	
	<p>Relat�rio Concilia��o</p>
	</a>
	</a>
	</div>
    
    <div id="dlgCon" class="easyui-dialog" style="width:420px;padding:5px 50px"
            closed="true" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmCon" method="post" novalidate>
        
        	 <div class="fitem">
                <label>Relat�rios :</label>
            <select
				id="setRelCon" name="setRelCon" class="easyui-combobox" style="width: 260px;font-size:11px;" required="true"
				data-options="panelHeight:'70', editable:false">
					<option value="SapXSigPri">Sapiens X Sigma - Privado</option>
					<option value="SapXSigPub">Sapiens X Sigma - P�blico</option>
					<option value="SigXSap">Sigma X Sapiens</option>
			</select>
			 </div>
        	
            
            <div class="fitem">
                <label>Formato :</label></br>
            <select
				id="setForCon" name="setForCon" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
			 
        </form>
        <div id="dlg-buttons-CobHum">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="javascript:gerarRelatorioConciliacao()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgCon').dialog('close')">Cancelar</a>
    	</div>
    </div>
				
   	<script type="text/javascript">
        var url;
        function abrirRelatorioConciliacao(){
            $('#dlgCon').dialog('open').dialog('setTitle','&nbspConcilia��o');
            $('#fmCon').form('clear');
        }
 		function gerarRelatorioConciliacao(){
        	
			var setFor = $('#setForCon').combobox('getValue');
			var setRelCon = $('#setRelCon').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=conciliacao&setfor=+"+setFor+"+&setRelCon=+"+setRelCon;																
    		
            $('#fmCon').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlgDCon').dialog('close');        // close the dialog
                        $('#dgCon').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
    	
    	function alertaCampos(){
            $.messager.alert('Aten��o','Campos Obrigat�rios!','warning');
        }
    	
    	function progress(){
	            var win = $.messager.progress({
	                title:'Por favor, aguarde!',
	                msg:'Carregando dados...'
	            });
	            setTimeout(function(){
	                $.messager.progress('close');
	                result();
	            },40000)
	    }
    	 
    	function result(){
    			$.messager.alert('Aten��o','Relat�rio gerado com sucesso!','info');

    			
    	}
    		
    	function error(){
    			$.messager.alert('Aten��o','Erro ao listar informa��es!','error');
    			
    	}
    	
    	function errorDias(){
			$.messager.alert('Aten��o','Per�odo Maior que 31 Dias','error');
			
		}
    	
    </script>
    <style type="text/css">
        #fm{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
    </style>
	
	
	
	
	
<style type="text/css">
input,textarea {
	border: 1px solid #95B8E7;
	padding: 2px;
}

       
</style>
	</cw:body>
	
</cw:main>
</portal:head>
