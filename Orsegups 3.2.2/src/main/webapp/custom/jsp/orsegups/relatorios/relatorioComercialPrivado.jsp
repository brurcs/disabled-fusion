<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>


<portlet:defineObjects />
<form:defineObjects />
<head>
	<meta charset="UTF-8">
</head>
<portal:head title="Relat�rio Comercial Privado">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script language="JavaScript" type="text/javascript">
		
	
	function showPopUp() 
	{
		var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/relatorios/relComTer.jsp';	
	
		
		$('#modalComTer').window('refresh', caminho);
		$('#modalComTer').window('open');
		
	}
	</script>
<cw:main>

	<cw:header title="Relat�rios Comercial Privado" />
	
	<cw:body id="area_scroll">
	
	
	
	<div class="float_icos">
	<a href="javascript:nop();">
	
	 <a href="javascript:void(0)"  iconCls="icon-add" plain="true" onclick="newUser()">
	
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>Comiss�es de Terceirizados</strong>
	</p>
	
	<p>Relat�rio de Comiss�es de Terceirizados Sapiens</p>
	</a>
	</a>
	</div>
	
	<div class="float_icos">
		<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioConciliacao()">
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>Concilia��o</strong>
	</p>
	
	<p>Relat�rio Concilia��o</p>
	</a>
	</a>
	</div>
	
	
	   <div id="dlgCon" class="easyui-dialog" style="width:420px;padding:5px 50px"
            closed="true" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmCon" method="post" novalidate>
        
        	 <div class="fitem">
                <label>Relat�rios :</label>
            <select
				id="setRelCon" name="setRelCon" class="easyui-combobox" style="width: 260px;font-size:11px;" required="true"
				data-options="panelHeight:'70', editable:false">
					<option value="SapXSigPri">Sapiens X Sigma - Privado</option>
					<option value="SapXSigPub">Sapiens X Sigma - P�blico</option>
					<option value="SigXSap">Sigma X Sapiens</option>
			</select>
			 </div>
        	
            
            <div class="fitem">
                <label>Formato :</label></br>
            <select
				id="setForCon" name="setForCon" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
			 
        </form>
        <div id="dlg-buttons-CobHum">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="javascript:gerarRelatorioConciliacao()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgCon').dialog('close')">Cancelar</a>
    	</div>
    </div>
    
	<%
				
		NeoPaper papel = new NeoPaper();
		Boolean possui = Boolean.FALSE;
		papel = OrsegupsUtils.getPaper("Respons�veis Relatorio OS Cobradas");
		NeoUser usuarioResponsavel = PortalUtil.getCurrentUser();
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				if (user.getCode().equals(usuarioResponsavel.getCode()))
				{
					possui = Boolean.TRUE;
					break;
				}
			}
		}
		if (possui) {
			%>
				<div class="float_icos">
				<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioProdutividadeCERECOS()">
				<p>
				<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
				</p>
				<p>
				<strong>OS Cobradas</strong>
				</p>
				
				<p>Relat�rio OS Cobradas</p>
				</a>
				</a>
				</div>
			<%
		}
	 %>
	 
	 <%
				
		NeoPaper papels = new NeoPaper();
		Boolean poss = Boolean.FALSE;
		papels = OrsegupsUtils.getPaper("Respons�veis Relat�rio Superintend�ncia");
		NeoUser usuarioRespon = PortalUtil.getCurrentUser();
		if (papels != null && papels.getAllUsers() != null && !papels.getAllUsers().isEmpty())
		{
			for (NeoUser users : papels.getUsers())
			{
				if (users.getCode().equals(usuarioRespon.getCode()))
				{
					poss = Boolean.TRUE;
					break;
				}
			}
		}
		if (poss) {
			%>
				<div class="float_icos">
				<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioSuperintendEncia()">
				<p>
				<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
				</p>
				<p>
				<strong>Superintend�ncia</strong>
				</p>
				
				<p>Relat�rio Superintend�ncia</p>
				</a>
				</a>
				</div>
			<%
		}
	 %>
	
    <div id="dlg" class="easyui-dialog" style="width:520px;height:420px;padding:5px 10px"
            closed="true" buttons="#dlg-buttons" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fm" method="post" novalidate>
              <div class="fitem">
                <label>Empresa :</label>
                <input id="cbEmpresa" name="cbEmpresa" class="easyui-combobox"  style="width: 340px;font-size:11px;" data-options="
                    url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaEmpresas',
                    valueField:'codemp',method:'get',required:true,
                    textField:'codemp', multiple:true,formatter: formatItemEmpresa, editable:false">
            </div>
            <div class="fitem">
                <label>Filial :</label>
                <select 
				id="cbFilial" name="cbFilial" class="easyui-combobox" required="true"  style="width: 150px;font-size:11px;" 
				data-options="panelHeight:'46',multiple:true, editable:false">
					<option value="1">1</option>
					<option value="2">2</option>
			</select>
            </div>
            <div class="fitem">
                <label>Instalador :</label>
                <input id="cbInstalador" name="cbInstalador" class="easyui-combobox"    style="width: 340px;font-size:11px;" data-options="
			 		url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaInstaladores',
                    valueField:'codins',method:'get', hasDownArrow:false,multiple:true,required:false,
                    textField:'codins', formatter: formatItemInstaldor">
            </div>
            <div class="fitem">
                <label>Data Inicial:</label>
                <input
				id="datIni" name="datIni" type="text" class="easyui-datebox" required="true"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Data Final:</label>
               <input 
				id="datFim" name="datFim" type="text" class="easyui-datebox" required="true" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Pon / Prod :</label>
                <select
				 id="ponPro" name="ponPro" class="easyui-combobox" required="true"
				style="width: 150px;font-size:11px;" data-options="panelHeight:'46', editable:false">
					<option value="T">Todos</option>
					<option value="Z">Diferente de zero</option>
			</select>
            </div>
            <div class="fitem">
                <label>Tipo Ins :</label>
                <select
				id="tipIns" name="tipIns" class="easyui-combobox" style="width: 150px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="T">Terceirizado</option>
					<option value="C">Colaborador</option>
			</select>
            </div>
             <div class="fitem">
                <label>Detalhes :</label>
               <select
				id="conDet" name="conDet" class="easyui-combobox" style="width: 150px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="S">Mostrar detalhes</option>
					<option value="N">Simplificado</option>
			</select>
            </div>
            <div class="fitem">
                <label>Formato :</label>
            <select
				id="setFor" name="setFor" class="easyui-combobox" style="width: 150px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:saveUser()">Confirmar</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancelar</a>
    </div>
    
    <div id="dlgProdCerecOs" class="easyui-dialog" style="width:420px;height:260px;padding:5px 10px"
            closed="true" buttons="#dlg-buttons-Prod-Cer-Os" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmProdCerecOs" method="post" novalidate>
            <div class="fitem">
                <label>Data Inicial:</label>
                <input
				id="datIniProdCerOs" name="datIniProdCerOs" type="text" class="easyui-datebox" required="true"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Data Final:</label>
               <input 
				id="datFimProdCerOs" name="datFimProdCerOs" type="text" class="easyui-datebox" required="true" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Formato :</label>
            <select
				id="setForProdCerOs" name="setForProdCerOs" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
        </form>
    </div>
    <div id="dlg-buttons-Prod-Cer-Os">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="gerarRelatorioProdutividadeCERECOS()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgProdCerecOs').dialog('close')">Cancelar</a>
    </div>
    
    <div id="dlgSuperintendEncia" class="easyui-dialog" style="width:420px;height:260px;padding:5px 10px"
            closed="true" buttons="#dlg-buttons-SuperintendEncia" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmSuperintendEncia" method="post" novalidate>
            <div class="fitem">
                <label>Formato :</label>
            <select
				id="setForSuperintendEncia" name="setForSuperintendEncia" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="XLS">XLS</option>
			</select>
			 </div>

					<%
					Connection conn1 = null;
					PreparedStatement st1 = null;
					ResultSet rs2 = null;
					try {
						
							String nomeFonteDados1 = "SAPIENS";
							conn1 = PersistEngine.getConnection(nomeFonteDados1);
							StringBuffer sql1 = new StringBuffer();
							String nomeRepresentanteAtual1 = "";
					
							sql1.append(" select *  from dbo.usu_t240cal ");
							
							st1 = conn1.prepareStatement(sql1.toString());
							rs2 = st1.executeQuery();
						
					
							%>
								 <div class="fitem">
                				<label>Compet�ncia :</label>
								<select id="setSuperintendEncia" name="setSuperintendEncia" class="easyui-combobox" style="width: 170px;font-size:11px;" required="true"
										data-options="panelHeight:'96', editable:false">
								<option> Selecione Uma Compet�ncia</option>
								<%
									while (rs2.next()) {
										Long codigoCalculoBotton = (Long) rs2.getLong("usu_codcal"); 
								%>
								<option value="<%= codigoCalculoBotton %>"><%= NeoUtils.safeDateFormat(rs2.getDate("usu_cptcal"), "MMM/yyyy") %></option>
								<%} %>
								</select>
								</div>
								<%
						
					} catch (Exception ee) {
						ee.printStackTrace();
						%>
							<script type="text/javascript" >
					        	
			    			$.messager.alert('Erro',"<%=ee.getMessage()%>",'error');
						
										
							</script>
						<%			
					} finally {
					try {
						OrsegupsUtils.closeConnection(conn1, st1, rs2);
					} catch (Exception ex) {
						ex.printStackTrace();
						%>
							<script type="text/javascript" >
					        	
			    			$.messager.alert('Erro',"<%=ex.getMessage()%>",'error');
						
										
							</script>
						<%	
					}}
				%>
				
        </form>
    </div>
    
    <div id="dlg-buttons-SuperintendEncia">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="gerarRelatorioSuperintendEncia()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgSuperintendEncia').dialog('close')">Cancelar</a>
    </div>
		
	<%
				
		NeoPaper papelG002 = new NeoPaper();
		Boolean possuiG002 = Boolean.FALSE;
		papelG002 = OrsegupsUtils.getPaper("ResponsaveisRelatorioG002");
		NeoUser usuarioG002 = PortalUtil.getCurrentUser();
		if (papelG002 != null && papelG002.getAllUsers() != null && !papelG002.getAllUsers().isEmpty())
		{
			for (NeoUser users : papelG002.getUsers())
			{
				if (users.getCode().equals(usuarioG002.getCode()))
				{
					possuiG002 = Boolean.TRUE;
					break;
				}
			}
		}
		if (possuiG002) {
			%>
				<div class="float_icos">
				<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioG002()">
				<p>
				<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
				</p>
				<p>
				<strong>Tarefas Direcionadas</strong>
				</p>
				
				<p>[G002] Relat�rio de Tarefas Direcionadas</p>
				</a>
				</a>
				</div>
			<%
		}
	 %>
	 
	  <div id="dlgRelatorioG002" class="easyui-dialog" style="width:420px;height:260px;padding:5px 10px"
            closed="true" buttons="#dlg-buttons-G002" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmRelatorioG002" method="post" novalidate>
            <div class="fitem">
                <label>Setor:</label>
                <input id="setor" name="setor" class="easyui-combobox" style="width: 340px;font-size:11px;" data-options="
			 		url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaSetoresG002',
                    valueField:'setor',method:'get', hasDownArrow:true,multiple:false,required:false,
                    textField:'setor', formatter: formatItemSetor">
           </div>
           <div class="fitem">
                <label>Tipo da Solicita��o:</label>
                <input id="tipoSolicitacao" name="tipoSolicitacao" class="easyui-combobox" style="width: 340px;font-size:11px;" data-options="
			 		url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaTiposSolicitacaoG002',
                    valueField:'tipoSolicitacao',method:'get', hasDownArrow:true,multiple:false,required:false,
                    textField:'tipoSolicitacao', formatter: formatItemTipoSolicitacao">
            </div>
           <div class="fitem">
                <label>Data Inicial:</label>
                <input
				id="datIniG002" name="datIniG002" type="text" class="easyui-datebox" required="true"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Data Final:</label>
               <input 
				id="datFimG002" name="datFimG002" type="text" class="easyui-datebox" required="true" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Formato :</label>
            <select
				id="setForG002" name="setForG002" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
        </form>
    </div>
    <div id="dlg-buttons-G002">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="javascript:gerarRelatorioG002()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgRelatorioG002').dialog('close')">Cancelar</a>
    </div>	
				
   	<script type="text/javascript">
        var url;
        function newUser(){
            $('#dlg').dialog('open').dialog('setTitle','&nbspComiss�es de terceirizados');
            $('#fm').form('clear');
        }
        function saveUser(){
        	
        	var empresa = $('#cbEmpresa').combobox('getValues');
    		var filial = $('#cbFilial').combobox('getValues');
    		var codIns = $('#cbInstalador').combobox('getValues');
    		var datIni = $('#datIni').datebox('getValue');
    		var datFim =  $('#datFim').datebox('getValue');
    		var tipIns = $('#tipIns').combobox('getValue');
    		var ponPro = $('#ponPro').combobox('getValue');
    		var conDet = $('#conDet').combobox('getValue');
    		var setFor = $('#setFor').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=comissoesTerceirizados&codemp=+"+empresa+"+ &codfil=+"+filial+"+&datini=+"+datIni+"+&datfim=+"+datFim+"+&tipins=+"+tipIns+"+&ponpro=+"+ponPro+"+&condet=+"+conDet+"+&setfor=+"+setFor+"+&codins=+"+codIns;														
    		
            $('#fm').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlg').dialog('close');        // close the dialog
                        $('#dg').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        
        function abrirRelatorioProdutividadeCERECOS(){
            $('#dlgProdCerecOs').dialog('open').dialog('setTitle','&nbspProdutividade OS Cobradas');
            $('#fmProdCerecOs').form('clear');
        }
        
		function gerarRelatorioProdutividadeCERECOS(){
			var datIni = $('#datIniProdCerOs').datebox('getValue');
    		var datFim =  $('#datFimProdCerOs').datebox('getValue');
    		var setFor = $('#setForProdCerOs').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=produtividadeCerecOS&datini=+"+datIni+"+&datfim=+"+datFim+"+&setfor=+"+setFor;														
    		
            $('#fmProdCerecOs').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlgProdCerecOs').dialog('close');        // close the dialog
                        $('#dlgProdCerecOs').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
		
		function abrirRelatorioSuperintendEncia(){
            $('#dlgSuperintendEncia').dialog('open').dialog('setTitle','&nbspSuperint�ndencia');
            $('#fmSuperintendEncia').form('clear');
        }
        
		function gerarRelatorioSuperintendEncia(){
    		var setForSuperintendEncia = $('#setForSuperintendEncia').combobox('getValue');
    		var setSuperintendEncia = $('#setSuperintendEncia').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=superintendencia&setForSuperintendEncia=+"+setForSuperintendEncia+"+&setSuperintendEncia=+"+setSuperintendEncia;											
    		
            $('#fmSuperintendEncia').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlgSuperintendEncia').dialog('close');        // close the dialog
                        $('#dlgSuperintendEncia').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        
        function formatItemInstaldor(row){
            var s = '<span style="font-weight:bold">' + row.codins + '</span><br/>' +
                    '<span style="color:#888">' + row.nomins + '</span>';
            return s;
        }
        
        function formatItemEmpresa(row){
            var s = '<span style="font-weight:bold">' + row.codemp + '</span><br/>' +
                    '<span style="color:#888">' + row.nomemp + '</span>';
            return s;
        }
        
        function myformatter(date) {
    		var y = date.getFullYear();
    		var m = date.getMonth() + 1;
    		var d = date.getDate();
    		return (d < 10 ? ('0' + d) : d) + '/' + (m < 10 ? ('0' + m) : m) + '/'
    				+ y;

    	}
    	function myparser(s) {
    		if (!s)
    			return new Date();
    		var ss = (s.split('-'));
    		var y = parseInt(ss[0], 10);
    		var m = parseInt(ss[1], 10);
    		var d = parseInt(ss[2], 10);
    		if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
    			return new Date(d, m - 1, y);
    		} else {
    			return new Date();
    		}
    	}
    	
    	function alertaCampos(){
            $.messager.alert('Aten��o','Campos Obrigat�rios!','warning');
        }
    	
    	 function progress(){
	            var win = $.messager.progress({
	                title:'Por favor, aguarde!',
	                msg:'Carregando dados...'
	            });
	            setTimeout(function(){
	                $.messager.progress('close');
	                result();
	            },7000)
	        }
    	 
    	 function result(){
    			$.messager.alert('Aten��o','Relat�rio gerado com sucesso!','info');

    			
    	    }
    		
    	function error(){
    			$.messager.alert('Aten��o','Erro ao listar informa��es!','error');
    			
    	    }
    	
    	 function abrirRelatorioConciliacao(){
             $('#dlgCon').dialog('open').dialog('setTitle','&nbspConcilia��o');
             $('#fmCon').form('clear');
         }
  		function gerarRelatorioConciliacao(){
         	
 			var setFor = $('#setForCon').combobox('getValue');
 			var setRelCon = $('#setRelCon').combobox('getValue');
     		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=conciliacao&setfor=+"+setFor+"+&setRelCon=+"+setRelCon;																
     		
             $('#fmCon').form('submit',{
                 url: servlet,
                 onSubmit: function(){
                 	var isValid = $(this).form('validate');
                 	if (!isValid){
                 		alertaCampos();
             		}else{
             			progress();
             		}
                     return isValid;
                 },
                 success: function(result){
                     var result = eval('('+result+')');
                     if (result.errorMsg){
                     	error();
                     	$.messager.show({
                             title: 'Error',
                             msg: result.errorMsg
                         });
                     } else {
//                      	document.location.href=servlet;
                     	result();
                         $('#dlgDCon').dialog('close');        // close the dialog
                         $('#dgCon').datagrid('reload');    // reload the user data
                     }
                 }
             });
         }
    
  		
  		function abrirRelatorioG002(){
  			debugger;
            $('#dlgRelatorioG002').dialog('open').dialog('setTitle','&nbspTarefas Direcionadas');
            $('#fmRelatorioG002').form('clear');
        }
  		
		function gerarRelatorioG002(){
			debugger;
    		var datIni = $('#datIniG002').datebox('getValue');
    		var datFim =  $('#datFimG002').datebox('getValue');
    		var setor = $('#setor').combobox('getValue');
    		var tipoSolicitacao = $('#tipoSolicitacao').combobox('getValue');
    		var setFormatoG002 = $('#setForG002').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=tarefasDirecionadasG002&setor=+"+setor+"+&tipo=+"+tipoSolicitacao+"&formato=+"+setFormatoG002+"+&datini=+"+datIni+"+&datfim=+"+datFim;											
    		
            $('#fmRelatorioG002').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
                    	result();
                        $('#dlgRelatorioG002').dialog('close');        // close the dialog
                        $('#dlgRelatorioG002').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
		
  		function formatItemSetor(row){
            var s = '<span style="font-weight:bold">' + row.setor + '</span><br/>';
            return s;
        }
  		
  		function formatItemTipoSolicitacao(row){
            var s = '<span style="font-weight:bold">' + row.tipoSolicitacao + '</span><br/>';
            return s;
        }
  		
    </script>
    <style type="text/css">
        #fm{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
    </style>
	
	
	
	
	
<style type="text/css">
input,textarea {
	border: 1px solid #95B8E7;
	padding: 2px;
}

       
</style>
	</cw:body>
	
</cw:main>
</portal:head>
