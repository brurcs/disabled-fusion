
<%@page import="com.neomind.fusion.custom.orsegups.utils.RelatorioActionsEnum"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	    <script type="text/javascript"
 src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/demo/demo.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
    <script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
    <meta http-equiv="X-UA-Compatible"content="IE=EmulateIE7" />
	<meta http-equiv="X-UA-Compatible" content="IE=7,IE=9" >

</head>
<body>
<script type="text/javascript">



	
	function myformatter(date) {
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		var d = date.getDate();
		return (d < 10 ? ('0' + d) : d) + '/' + (m < 10 ? ('0' + m) : m) + '/'
				+ y;

	}
	
	function myformatterBD(date){
    	var y = date.getFullYear();
    	var m = date.getMonth()+1;
    	var d = date.getDate();
    	return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s) {
		if (!s)
			return new Date();
		var ss = (s.split('-'));
		var y = parseInt(ss[0], 10);
		var m = parseInt(ss[1], 10);
		var d = parseInt(ss[2], 10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
			return new Date(d, m - 1, y);
		} else {
			return new Date();
		}
	}
	
	 
	
	
	function formatItem(row){
        var s = '<span style="font-weight:bold">' + row.text + '</span><br/>' +
                '<span style="color:#888">' + row.desc + '</span>';
        return s;
    }
	
	$(document).ready(function MyReport(){
		$('#btGerarRelatorio').click(function() 
		   	 	{
	validar();
	var datIni = $('#periodo').datebox('getValue');
	var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=comissoesTerceirizados&codemp=+"+empresa+"+ &codfil=+"+filial+"+&datini=+"+datIni+"+&datfim=+"+datFim+"+&tipins=+"+tipIns+"+&ponpro=+"+ponPro+"+&condet=+"+conDet+"+&setfor=+"+setFor+"+&codins=+"+codIns;														

	$.ajax({
		type : "POST",
		url: servlet,
        contentType: 'application/x-download',
        mimeType: 'application/x-download',
	    success:function(response) {
	    	result();
	    	
 	    	location.href=servlet;
	    	
	    },
    error: function (jqXHR, exception,xhr, ajaxOptions, thrownError) {
    	var msg = ""; 
    	if (jqXHR.status === 0) {
    		msg +='Not connect.\n Verify Network.';
         } else if (jqXHR.status == 404) {
        	 msg +='Requested page not found. [404]';
         } else if (jqXHR.status == 500) {
        	 msg +='Internal Server Error [500].';
         } else if (exception === 'parsererror') {
        	 msg +='Requested JSON parse failed.';
         } else if (exception === 'timeout') {
        	 msg +='Time out error.';
         } else if (exception === 'abort') {
        	 msg +='Ajax request aborted.';
         } else {
        	 msg +='Uncaught Error.\n' + jqXHR.responseText;
         }
    	msg += xhr.statusText + "\n"+thrownError+ "\n"+exception;
    	error();
    },
    });

	
		   	 	}
		);
				}
			);
	
	
	function result(){
		$.messager.alert('Aten��o','Relat�rio gerado com sucesso!','info');

		
    }
	
	function error(){
		$.messager.alert('Aten��o','Erro ao listar informa��es!','error');
		
    }
	
	 function formatItemEmpresa(row){
         var s = '<span style="font-weight:bold">' + row.codemp + '</span><br/>' +
                 '<span style="color:#888">' + row.nomemp + '</span>';
         return s;
     }
	 
	 function formatItemInstaldor(row){
         var s = '<span style="font-weight:bold">' + row.codins + '</span><br/>' +
                 '<span style="color:#888">' + row.nomins + '</span>';
         return s;
     }
	
	
		function validar(){
		    var msg = "";
			if($('#cbEmpresa').combobox('getValues') == ''){
				msg += "O campo Empresa � obrigat�rio. \n ";
		    }
		    if($('#cbFilial').combobox('getValues') == ''){
		    	msg += "O campo Filial � obrigat�rio.\n ";
		    }
		    var dtini =  $('#datIni').datebox('getValue');
		    var dtfim =  $('#datFim').datebox('getValue');
		    
		    if ((dtini == '') && (dtfim == '')) {
		    	msg += "Complete os Campos.\n ";
		    }
		    
		    datInicio = new Date(dtini.substring(6,10), 
		                         dtini.substring(3,5), 
		                         dtini.substring(0,2));
		    datInicio.setMonth(datInicio.getMonth() - 1); 
		    
		    
		    datFim = new Date(dtfim.substring(6,10), 
		                      dtfim.substring(3,5), 
		                      dtfim.substring(0,2));
		                     
		    datFim.setMonth(datFim.getMonth() - 1); 

		    if(datInicio >= datFim){
		    	msg += " ATEN��O: Data Inicial � maior que Data Final ";
		    }    
		    if(msg != ''){
		    	alertaCampos();
			    return false;
		    }else{
		    	return true;
		    }
		}
		
		function alertaCampos(){
            $.messager.alert('Aten��o','Campos Obrigat�rios!','warning');
        }
		

		
</script>

<style type="text/css">
input,textarea {
	border: 1px solid #95B8E7;
	padding: 2px;
}

        #ff{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
        .flabel{
        	text-align: right; 
        	height:20px; 
        	font-weight: bold; 
        	background-color: #e0ecff;
        	width: 120px;
        	padding-right: 2px; 
        }
        .fvalue{
        	padding-left: 8px; 
        }
</style>

<form id="ff" method="post">

<div class="ftitle" >Informa��es</div>		
<table>
	<tbody>
		
			
			
        	<tr>

			<td
				style="text-align: right; font-weight: bold; padding-left: 8px; padding-bottom: 8px;font-size:11px;">Empresa
				:</td>

			<td
				style="color: blue; font-weight: bold; padding-left: 8px; padding-bottom: 8px;" >
				
				
				<input id="cbEmpresa" class="easyui-combobox"  style="width: 350px;font-size:11px;" data-options="
                    url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaEmpresas',
                    valueField:'codemp',method:'get',required:true,
                    textField:'codemp', multiple:true,formatter: formatItemEmpresa, editable:false">
                 
			</td>
		</tr>
		
		<tr>
			<td
				style="text-align: right; font-weight: bold; padding-left: 4px; padding-bottom: 8px;">Filial
				:</td>
			<td
				style="font-weight: bold; padding-left: 8px; padding-bottom: 8px;">
				<select 
				id="cbFilial" class="easyui-combobox"  style="width: 150px;font-size:11px;" 
				data-options="panelHeight:'46',multiple:true, editable:false">
					<option value="1">1</option>
					<option value="2">2</option>
			</select>
			</td>
		</tr>
		<tr>

			<td 
				style="text-align: right; font-weight: bold; padding-left: 4px; padding-bottom: 8px;font-size:11px;">Instalador:
				</td>

			<td
				style="color: blue; font-weight: bold; padding-left: 8px; padding-bottom: 8px;font-size:11px;" >
			<input id="cbInstalador" class="easyui-combobox"    style="width: 350px;font-size:11px;" data-options="
			 url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaInstaladores',
                    valueField:'codins',method:'get', hasDownArrow:false,multiple:true,maxRows: 20,
                    textField:'codins', formatter: formatItemInstaldor">
			</td>
		</tr>
		<tr>
			<td 
				style="text-align: right; font-weight: bold; padding-left: 4px; padding-bottom: 8px;font-size:11px;width: auto;">Data
				Inicial :</td>
			<td
				style="color: blue; font-weight: bold; padding-left: 8px; padding-bottom: 8px;font-size:11px;"><input
				id="datIni" type="text" class="easyui-datebox" required="required"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;"></td>
		</tr>
		<tr>
			<td 
				style="text-align: right; font-weight: bold; padding-left: 4px; padding-bottom: 8px;font-size:11px;">Data
				Final :</td>
			<td
				style="color: blue; font-weight: bold; padding-left: 8px; padding-bottom: 8px;font-size:11px;"><input 
				id="datFim" type="text" class="easyui-datebox" required="required" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;"></td>
		<tr>
			<td  style="text-align: right; font-weight: bold;font-size:11px;padding-left: 2px; ">Ponto /
				Produto :</td>
			<td
				style="color: blue; font-weight: bold; padding-left: 8px; padding-bottom: 8px;"><select
				id="ponPro" class="easyui-combobox"
				style="width: 150px;font-size:11px;" data-options="panelHeight:'46', editable:false">
					<option value="T">Todos</option>
					<option value="Z">Diferente de zero</option>
			</select></td>
		</tr>
		<tr>
			<td  style="text-align: right; font-weight: bold;font-size:11px;padding-left: 2px; ">Tipo
				Instala��o :</td>
			<td
				style="color: blue; font-weight: bold; padding-left: 8px; padding-bottom: 8px;"><select
				id="tipIns" class="easyui-combobox" style="width: 150px;font-size:11px;"
				data-options="panelHeight:'46', editable:false">
					<option value="T">Terceirizado</option>
					<option value="C">Colaborador</option>
			</select></td>
		</tr>
		
		<tr>
			<td style="text-align: right; font-weight: bold;font-size:11px;padding-left: 4px; ">Detalhado :</td>
			<td
				style="color: blue; font-weight: bold; padding-left: 8px; padding-bottom: 8px;"><select
				id="conDet" class="easyui-combobox" style="width: 150px;font-size:11px;"
				data-options="panelHeight:'46', editable:false">
					<option value="S">Mostrar detalhes</option>
					<option value="N">Simplificado</option>
			</select></td>
		</tr>
		
		<tr>
			<td style="text-align: right; font-weight: bold;font-size:11px;padding-left: 4px; ">Formato :</td>
			<td
				style="color: blue; font-weight: bold; padding-left: 8px; padding-bottom: 8px;"><select
				id="setFor" class="easyui-combobox" style="width: 150px;font-size:11px;"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select></td>
		</tr>
	</tbody>


</table>

 			<div align="center"  width="99%"  data-options="border:false" style="text-align:right;padding:5px 0 0;padding-right: 100px; ">
                <a id="btGerarRelatorio" class="easyui-linkbutton" target="_blank" data-options="iconCls:'icon-ok'" href="javascript:void(0)"  >Confirmar</a>
                <a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" href="javascript:void(0)" onclick="$('#modalComTer').window('close')">Cancelar</a>
            </div>
   
           
            
</form>

</body>
</html>



