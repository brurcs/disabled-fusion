<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.DateFormat"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.presenca.vo.SolicitacaoVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.site.SiteUtils"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.site.vo.SiteSolicitacaoVO"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.presenca.vo.ReclamacaoVO"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.google.gson.annotations.Since"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.io.*"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>


<%!public static List<String> listaDeTelefonesClientes = new ArrayList<String>();

	public void init() throws ServletException
	{
		this.loadList();
	}

	public void loadList()
	{
		Connection connSapiens = null;
		PreparedStatement pstmSapiens = null;
		ResultSet rsSapiens = null;
		try
		{
			connSapiens = PersistEngine.getConnection("SAPIENS");

			StringBuilder sqlSapiens = new StringBuilder();
			sqlSapiens.append(" SELECT usu_numfne FROM usu_t500fne ");

			//BUSCA-SE REGISTROS NA BASE DO SAPIENS
			pstmSapiens = connSapiens.prepareStatement(sqlSapiens.toString());
			rsSapiens = pstmSapiens.executeQuery();

			while (rsSapiens.next())
			{
				listaDeTelefonesClientes.add(rsSapiens.getString(1));
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connSapiens, pstmSapiens, rsSapiens);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}%>

<%!public List<String> getCodigoCliente(String telefone)
	{
		Connection connSapiens = null;
		PreparedStatement pstmSapiens = null;
		ResultSet rsSapiens = null;
		List<String> codCliList = null;
		try
		{
		    
			String telefoneRequest2 = "";
			
			if (telefone.length()>10){
			    String subFone = telefone.substring(3);
			    
			    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
				telefoneRequest2 = telefone.substring(0, 2)+subFone;
			    }
			    
			    System.out.println(telefoneRequest2);
			    
			}
		    
		    
			connSapiens = PersistEngine.getConnection("SAPIENS");

			StringBuilder sqlSapiens = new StringBuilder();
			sqlSapiens.append(" select usu_codcli from usu_t500fne where usu_numfne = ? OR usu_numfne = ? ");

			//BUSCA-SE REGISTROS NA BASE DO SAPIENS
			pstmSapiens = connSapiens.prepareStatement(sqlSapiens.toString());
			pstmSapiens.setString(1, telefone);
			pstmSapiens.setString(2, telefoneRequest2);
			rsSapiens = pstmSapiens.executeQuery();
			codCliList = new ArrayList<String>();
			while (rsSapiens.next())
			{
				codCliList.add(rsSapiens.getString("usu_codcli"));
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connSapiens, pstmSapiens, rsSapiens);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return codCliList;
		}
	}%>

<%
	String telefoneRequest = request.getParameter("foneOrigem");
	int prioridade = 20;
	//TELEFONE ENCONTRADO
	
	String telefoneRequest2 = telefoneRequest;
	
	if (telefoneRequest.length()>10){
	    String subFone = telefoneRequest.substring(3);
	    
	    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
		telefoneRequest2 = telefoneRequest.substring(0, 2)+subFone;
	    }	    
	}
	
	
	if (listaDeTelefonesClientes.contains(telefoneRequest) || listaDeTelefonesClientes.contains(telefoneRequest2))
	{
		List<String> codigoClienteList = getCodigoCliente(telefoneRequest);
		if (codigoClienteList != null && !codigoClienteList.isEmpty())
		{
			prioridade = 50;
			for (int i = 0; i < codigoClienteList.size(); i++)
			{
				String codCliStr = codigoClienteList.get(i);
				prioridade = getRSC(codCliStr,prioridade);
				if(prioridade == 100)
					break;
				prioridade = getRRC(codCliStr,prioridade);
				if(prioridade == 100)
					break;
			}
		}
		if (prioridade == 20)
			prioridade = 50;
			
		//out.print("{\"return\":\"TRUE\"}");
		out.print("{\"return\":\""+prioridade+"\"}");
		System.out.println("SNEP: � Cliente? [" + telefoneRequest + "]: SIM ***");

	} //REGARREGA LISTA
	else if (telefoneRequest.equals("reload"))
	{
		this.loadList();
		out.print("{\"return\":\"RELOAD FROM ROTINA FONES\"}");
	}//N�O ENCONTRADO
	else
	{
		//out.print("{\"return\":\"FALSE\",\"priority\":\"" + prioridade + "\"}");
		out.print("{\"return\":\""+prioridade+"\"}");
		System.out.println("SNEP: � Cliente? [" + telefoneRequest + "]: N�O ---");
	}
%>
<%!public Date formataData(String data) throws Exception
	{
		if (data == null || data.equals(""))
			return null;

		Date date = null;
		try
		{
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			date = new Date(((java.util.Date) formatter.parse(data)).getTime());
		}
		catch (ParseException e)
		{
			throw e;
		}
		return date;
	}%>
	
<%!public int getRSC(String codCli, int prioridade)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try
		{
			conn = PersistEngine.getConnection("FUSIONPROD");

			StringBuilder sql = new StringBuilder();
			/* 
			sql.append(" SELECT p.PROCESSSTATE ");
			sql.append(" FROM wfProcess AS p WITH (NOLOCK)  ");
			sql.append(" INNER JOIN D_RSCRelatorioSolicitacaoCliente AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId ");
			//sql.append(" INNER JOIN processmodel AS pm WITH (NOLOCK)  ON pm.neoid = p.model_neoid ");
			sql.append(" INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rsc.clienteSapiens_neoId ");
			sql.append(" WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relat�rio de Solicita��o de Cliente%')   ");
			sql.append(" AND cli.codcli = ? ");
			sql.append(" AND p.STARTDATE >= DATEADD(MONTH,-1,GETDATE()) AND p.STARTDATE <= GETDATE() ");
			sql.append(" AND p.PROCESSSTATE NOT IN(2,4) AND p.saved = 1 ORDER BY p.STARTDATE "); */
			
			sql.append(" declare @codcli as numeric ");
			sql.append(" select @codcli =  ? ");   
			sql.append(" select rscs.PROCESSSTATE from "); 
            sql.append("             ( ");        
            sql.append("                     ( ");
            sql.append("                     SELECT top 1000 p.PROCESSSTATE "); 
            sql.append("                     FROM wfProcess AS p WITH (NOLOCK) ");  
            sql.append("                     INNER JOIN D_RSCRelatorioSolicitacaoCliente AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId "); 
                                 
            sql.append("                     INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rsc.clienteSapiens_neoId "); 
            sql.append("                     WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relat�rio de Solicita��o de Cliente%') ");   
            sql.append("                     AND cli.codcli = @codcli ");
            sql.append("                     AND p.STARTDATE >= DATEADD(MONTH,-1,GETDATE()) AND p.STARTDATE <= GETDATE() "); 
            sql.append("                     AND p.PROCESSSTATE NOT IN(2,4) AND p.saved = 1 ORDER BY p.STARTDATE "); 
            sql.append("                     ) ");
            sql.append("                     union ");
            sql.append("                     ( ");
            sql.append("                      SELECT top 1000 p.PROCESSSTATE "); 
            sql.append("                     FROM wfProcess AS p WITH (NOLOCK) ");  
            sql.append("                     INNER JOIN D_RSCRelatorioSolicitacaoClienteNovo AS rsc WITH (NOLOCK) ON rsc.neoid = p.entity_neoId "); 
                     
            sql.append("                     INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rsc.clienteSapiens_neoId "); 
            sql.append("                     WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C027 - RSC - Relat�rio de Solicita��o de Cliente Novo%') ");   
            sql.append("                     AND cli.codcli = @codcli ");
            sql.append("                     AND p.STARTDATE >= DATEADD(MONTH,-1,GETDATE()) AND p.STARTDATE <= GETDATE() "); 
            sql.append("                     AND p.PROCESSSTATE NOT IN(2,4) AND p.saved = 1 ORDER BY p.STARTDATE "); 
            sql.append("                     ) ");
            sql.append("              ) as rscs ");
			
			//BUSCA-SE REGISTROS NA BASE
			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, codCli);
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String status = rs.getString("PROCESSSTATE");
				if(status != null && status.equals("0")){
					prioridade = 100;
					
				}else if(prioridade == 50){
					prioridade = 80;
					
				}
				
			}
			

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return prioridade;
		}
}%>

<%!public int getRRC(String codCli, int prioridade)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try
		{
			conn = PersistEngine.getConnection("FUSIONPROD");

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT p.PROCESSSTATE ");
			sql.append(" FROM wfProcess AS p WITH (NOLOCK)  ");
			sql.append(" INNER JOIN D_RRCRelatorioReclamacaoCliente AS rrc WITH (NOLOCK) ON rrc.neoid = p.entity_neoId ");
			//sql.append(" INNER JOIN processmodel AS pm WITH (NOLOCK)  ON pm.neoid = p.model_neoid ");
			sql.append(" INNER JOIN X_SAPIENS_Clientes AS cli WITH (NOLOCK)  ON cli.neoid = rrc.clienteSapiens_neoId ");
			sql.append(" WHERE p.model_neoid in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%Q005 - RRC - Relat�rio de Reclama��o de Cliente%') ");
			sql.append(" AND cli.codcli = ? ");
			sql.append(" AND p.STARTDATE >= DATEADD(MONTH,-1,GETDATE()) AND p.STARTDATE <= GETDATE() ");
			sql.append(" AND p.PROCESSSTATE NOT IN(2,4) AND p.saved = 1 ORDER BY p.STARTDATE ");
			
			//BUSCA-SE REGISTROS NA BASE
			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, codCli);
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String status = rs.getString("PROCESSSTATE");
				if(status != null && status.equals("0")){
					prioridade = 100;
				}else if(prioridade == 50){
					prioridade = 80;
				}
				
			}
	
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return prioridade;
		}
}%>