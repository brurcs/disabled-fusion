<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>

<%
	String cnpj = NeoUtils.safeOutputString((String) request.getAttribute("cnpj"));
	String nome =  NeoUtils.safeOutputString((String) request.getAttribute("nome"));
	String pis =  NeoUtils.safeOutputString((String) request.getAttribute("pis"));
	String data =  NeoUtils.safeOutputString((String) request.getAttribute("data"));
	String hora =  NeoUtils.safeOutputString((String) request.getAttribute("hora"));
	String nsr =  NeoUtils.safeOutputString((String) request.getAttribute("nsr"));
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Comprovante de Registro de Ponto do Trabalhador</title>
</head>
<body>
	<table width="100%"  cellspacing="0" cellpadding="0">
	  	<tbody>
	    	<tr>
	      	<td>
	        	<table width="400"  align="center" cellpadding="0" cellspacing="0" style="margin-top:10px;">
	          	<!-- Main Wrapper Table with initial width set to 60opx -->
	          		<tbody style="background-color:#FFFEB6;padding:10px;">
	            		<tr> 
	              		<!-- Introduction area -->
	              			<td>
	                        	<table width="96%"  align="left" cellpadding="0" cellspacing="0">
	                  				<tr> 
	                    			<!-- row container for TITLE/EMAIL THEME -->
	                    				<td align="center" style="font-size: 20px; font-weight: 900; line-height: 1em; color: #333; font-family:monospace;padding:20px;border-bottom:1px dashed #333;">COMPROVANTE DE REGISTRO DE PONTO DO TRABALHADOR</td>
	                  				</tr>
	                  				<tr>
	                    				<td style="font-size: 0; line-height: 0;" height="20"><table width="96%" align="left"  cellpadding="0" cellspacing="0">
	                        				<tr> 
	                          				<!-- HTML Spacer row -->
	                          					<td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
	                        				</tr>
	                      		</table></td>
	                  		</tr>
	                  		<tr> 
	                    		<!-- Row container for Intro/ Description -->
	                    		<td align="left" style="font-size: 18px; font-style: normal; font-weight: normal; color: #000; line-height: 1.5em; text-align:justify; padding:10px 20px 20px 20px; font-family:monospace;">
	                    		CNPJ: <%= cnpj %> <br />
	                    		NOME: <%= nome %> <br />
	                    		PIS: <%= pis %> <br />
	                    		DATA: <%= data %> <br>
	                    		HORA: <%= hora %> <br/>
	                    		NSR: <%= nsr %></td>
	                  		</tr>
	                		</table></td>
	            		</tr>
	          		</tbody>
	        	</table>
	        </td>
	    	</tr>
	  	</tbody>
	</table>
</body>
</html>