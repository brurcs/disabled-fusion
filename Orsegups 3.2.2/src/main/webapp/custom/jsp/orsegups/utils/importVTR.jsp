<%@page import="java.util.StringTokenizer"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>


<%
	try {
		NeoObject corObj = PersistEngine.getObject(AdapterUtils.getEntityClass("OTSCorLigacao"), new QLEqualsFilter("sufixoImagem", "verde"));
		ArrayList<String> vtrs = new ArrayList<String>();
		/* Novo insert de registros */
		vtrs.add("MKZ4083;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ4453;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ3423;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ4383;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ3693;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ3353;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ3903;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ4003;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ3593;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ3513;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ4433;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ4493;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ3523;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		vtrs.add("MKZ3463;Moto;NXR Bros 150 ESD Mix - Preto - 2013/2014");
		/*
		vtrs.add("MKQ1573;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ1623;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ1663;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ1723;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ1813;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ1853;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ1903;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ2063;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ2293;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ2413;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ2483;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKQ2543;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKW6903;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");
		vtrs.add("MKW6983;carro;Palio Economy Cel. 4 Pt. 1.0 Flex - branco - 2013/2014");*/
		
		/*@Script antigo@
		vtrs.add("MKD6997;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7217;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7357;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7487;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7607;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6297;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6547;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6607;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6977;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7147;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7167;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7327;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7427;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7447;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7467;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7567;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7367;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7377;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7507;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7517;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7537;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7547;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7557;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7587;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7627;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD5637;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6257;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6937;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD7197;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6687;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6697;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6747;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6797;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6827;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6847;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6867;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6877;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6647;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");
		vtrs.add("MKD6667;moto;NXR Bros 150 ESD Mix - Preto - 2012/2013");*/

		
		InstantiableEntityInfo vtrInfo = AdapterUtils.getInstantiableEntityInfo("OTSViatura");	
		for (String vtr : vtrs) {
			StringTokenizer st = new StringTokenizer(vtr, ";");
			String placa = st.nextToken();
			String tipo = st.nextToken();
			String descricao = st.nextToken();
			 
			NeoObject objVtr = vtrInfo.createNewInstance();
			EntityWrapper wrpVtr = new EntityWrapper(objVtr);
			wrpVtr.findField("placa").setValue(placa);
			wrpVtr.findField("titulo").setValue("Importado: " + placa);
			wrpVtr.findField("descricao").setValue(descricao);
			wrpVtr.findField("emUso").setValue(Boolean.FALSE);
			wrpVtr.findField("corLigacao").setValue(corObj);
			NeoObject tipoObj = PersistEngine.getObject(AdapterUtils.getEntityClass("OTSTipoViatura"), new QLEqualsFilter("nomeImagem", tipo));
			EntityWrapper wrpTipo = new EntityWrapper(tipoObj);
			out.println(wrpTipo.getValue("nome"));
			wrpVtr.findField("tipoViatura").setValue(tipoObj);
			PersistEngine.persist(objVtr);
			out.print(objVtr);
		}
    }  catch (Exception e) {
		e.printStackTrace();
		// erro desconhecido
		out.print("Erro: " + e.getMessage());
		return;    
    }
%>
