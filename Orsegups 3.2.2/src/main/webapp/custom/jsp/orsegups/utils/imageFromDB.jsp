<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.io.OutputStream"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%	
	
	String regra = request.getParameter("regra");
	String tokenWinker = request.getParameter("tokenWinker");
	
	
	String numcpf = request.getParameter("numcpf"); // winker
	String numEmp = request.getParameter("numEmp");
	String tipCol = request.getParameter("tipCol");
	String numCad = request.getParameter("numCad");
	StringBuffer sqlFoto = null;
	
	if (tokenWinker != null && tokenWinker.equals("6f4b8cbf39cb8161d91127889cb389af")){
		sqlFoto = new StringBuffer();
		sqlFoto.append("SELECT FotEmp FROM R034FOT fot inner join r034fun fun on fot.numemp = fun.numemp and fot.numcad = fun.numcad and fot.tipcol = fun.tipcol where numcpf = "+numcpf+" and fun.datafa = '1900-12-31 00:00:00'");
	}else if ((regra == null && PortalUtil.getCurrentUser() == null)) {
		out.print("Acesso Negado");
		return;
	}else{
		sqlFoto = new StringBuffer();
		sqlFoto.append(" SELECT FotEmp FROM R034FOT WHERE NumEmp = " + numEmp + " AND TipCol = "+ tipCol +" AND NumCad = " + numCad + " AND FotEmp IS NOT NULL");	
	}
	
	


    PreparedStatement st = null;
    try {
		String nomeFonteDados = "VETORH";
		Connection conn = PersistEngine.getConnection(nomeFonteDados);
		byte[] imgData = null;
		
		st = conn.prepareStatement(sqlFoto.toString());
		ResultSet rs = st.executeQuery();
		
		if(rs.next()) {
			imgData = rs.getBytes("FotEmp");
			response.setContentType("image/jpeg"); 
			response.setContentLength(imgData.length);
			OutputStream o = response.getOutputStream(); 
			o.write(imgData);
			o.flush();
			o.close();
			
		}
		if(imgData == null)
		{
			
			File file = new File(request.getRealPath("custom/jsp/orsegups/utils/images/no_pic.jpg"));
			response.setContentType("image/jpeg"); 
			response.setContentLength((int)file.length());
			FileInputStream fileInputStream = new FileInputStream(file);
			OutputStream o = response.getOutputStream(); 
			byte[] buf = new byte[1024];
			
		       int count = 0;
		       while ((count = fileInputStream.read(buf)) >= 0) {
		         o.write(buf, 0, count);
		      }
		    o.flush();
		    o.close();
		    fileInputStream.close();
		    
		}
		rs.close();
		st.close();

	} catch (Exception e) {
		e.printStackTrace();	
	}

%>