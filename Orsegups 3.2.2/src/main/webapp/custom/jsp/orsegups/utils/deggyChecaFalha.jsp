<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>

<%
	String check = request.getParameter("check");
	boolean isCheck = false;
	if (check != null && check.trim().equals("sim")) {
		isCheck = true;
	}

	String dbfile = "\\\\deggy\\Deggy\\Backup_03_11_11\\DeggyDB.mdb";
	String url = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=" + dbfile + ";ReadOnly=1";
	
	String username = "";
	String password = "";
	Connection conn = null;
	Connection connFusion = null;
	PreparedStatement st = null;
	PreparedStatement st2 = null;
	String cd_empresa = "";
	String cd_central = "";
	String particao = "";
	String nm_evento = "XXRD";
	Long lastAlarm = 434571L;

	try {
		StringBuffer sql1 = new StringBuffer();
		sql1.append(" SELECT MAX(alarmID)  ");
		sql1.append(" FROM DEGGYControleIDAlarme  ");
		sql1.append(" WHERE dataIntegracao IS NULL  ");
		//st = conn.prepareStatement(sql1.toString());

		InstantiableEntityInfo alarmInfo = AdapterUtils.getInstantiableEntityInfo("DEGGYControleIDAlarme");
		List listID = PersistEngine.getObjects(alarmInfo.getEntityClass(), null, 0, 1, "alarmID DESC");

		for(Object obj : listID) {
			NeoObject no = (NeoObject)obj;
			EntityWrapper wrpAlarm = new EntityWrapper(no);
			lastAlarm = (Long)wrpAlarm.findField("alarmID").getValue();
		}	
		
		//Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		conn = DriverManager.getConnection(url, username, password);
		
		sql1 = new StringBuffer();
		sql1.append(" SELECT s.ScheduleID, log.ID, log.DTAlarm ");
		sql1.append(" FROM tbAlarmLog log, Schedule s ");
		sql1.append(" WHERE log.ID > "+ lastAlarm +"   ");
		sql1.append(" AND log.SchedID = s.ScheduleID ");
		st = conn.prepareStatement(sql1.toString());
		ResultSet rs = st.executeQuery();

		Set<String> listaAlarms = new HashSet<String>();
		while(rs.next()) {
			Long sID = new Long(rs.getLong(1));
			Long logID = new Long(rs.getLong(2));
			out.print(logID + "<br>");
			out.print(sID + "<br>");
			out.print(rs.getTimestamp(3) + "<br>");
			NeoObject objAlarm = alarmInfo.createNewInstance();
			EntityWrapper wrpAlarm = new EntityWrapper(objAlarm);
			wrpAlarm.findField("alarmID").setValue(logID);
			wrpAlarm.findField("scheduleID").setValue(sID);
			PersistEngine.persist(objAlarm);	
			listaAlarms.add(sID.toString());
		}
		st.close();


		for(String sID : listaAlarms) {
			StringBuffer sql2 = new StringBuffer();
			sql2.append(" SELECT DISTINCT b.CEP, b.Fone, b.Contato  ");
			sql2.append(" FROM SchedulePlaces sp, Button b ");
			sql2.append(" WHERE sp.PlaceID = b.CodButton ");
			sql2.append(" AND sp.ScheduleID = " + sID);
			sql2.append(" AND b.CEP IS NOT NULL ");		

			st2 = conn.prepareStatement(sql2.toString());
			ResultSet rs2 = st2.executeQuery();

			while(rs2.next()) {
				cd_empresa = rs2.getString(1);
				if ((cd_empresa != null) && (cd_central != null) && (particao != null) ) {
					if (cd_empresa.equals("1001")) {
						cd_empresa = "10001";
					}
					cd_central = rs2.getString(2);
					particao = rs2.getString(3);
					nm_evento = "XXRD";
					String urlEvento = PortalUtil.getBaseURL() + "custom/jsp/orsegups/sigmaCriaEvento.jsp?cd_central=" + cd_central + "&cd_empresa="+ cd_empresa +"&particao="+ particao +"&nm_evento=" + nm_evento;
					out.print(urlEvento);
					out.print("<br>");
					if (!isCheck) {
						URL urlAux = new URL(urlEvento);
						URLConnection uc = urlAux.openConnection();
						BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
					}
				}
			}
			st2.close();
		}
		out.print("OK");		
		
	} catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #5 - Erro ao consultar base de dados do Deggy");
		return;
	} finally {
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			out.print("Erro #6 - Erro ao finalizar conex�o");
			return;
		}
	}
%>
