<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>

<%

	Class clazz = AdapterUtils.getEntityClass("TELECOMCelulares");
	QLGroupFilter comCCFilter = new QLGroupFilter("AND");
	comCCFilter.addFilter(new QLOpFilter("centrodeCusto", " NOT LIKE ", ""));
	comCCFilter.addFilter(new QLFilterIsNotNull("centrodeCusto"));

	List<NeoObject> celulares = PersistEngine.getObjects(clazz, comCCFilter);
	InstantiableEntityInfo ccInfo = AdapterUtils.getInstantiableEntityInfo("QLCentroCustoTelefone");	

	if(celulares != null && !celulares.isEmpty())
	{
		String total = String.valueOf(celulares.size());
		
		Integer count = 1;
		
		for(NeoObject cel : celulares)
		{
			EntityWrapper celWrapper = new EntityWrapper(cel);
			String ccStr = (String)celWrapper.findValue("centrodecusto");
			if (ccStr != null) {
				ccStr = ccStr.trim();
				if (!ccStr.equals("")) {
					NeoObject objCC = ccInfo.createNewInstance();
					EntityWrapper wrpCC = new EntityWrapper(objCC);
					wrpCC.findField("centroCusto").setValue(ccStr);
					//PersistEngine.persist(objCC);

					List<NeoObject> listaCC = new ArrayList<NeoObject>();
					listaCC.add(objCC);
					//celWrapper.setValue("centroCustoTelefone", listaCC);
					
				}
			}
			
			
			out.print(count+"/"+total);
			out.print("<br>");
			out.flush();
			
			count++;
		}
	}

%>
