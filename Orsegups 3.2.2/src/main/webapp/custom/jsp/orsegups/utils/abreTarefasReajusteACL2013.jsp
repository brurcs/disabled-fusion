<%@page import="java.util.StringTokenizer"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.net.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>

<%!
public Calendar addBusinessDays(Calendar cal, int numBusinessDays) {
	int numNonBusinessDays = 0;

	for(int i = 0; i < numBusinessDays; i++) {
	cal.add(Calendar.DATE, 1);

	if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
	  numNonBusinessDays++;
	}
	}

	if(numNonBusinessDays > 0) {
		cal.add(Calendar.DATE, numNonBusinessDays);
	}

	if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
		cal.add(Calendar.DATE, 2);
	}
	if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
		cal.add(Calendar.DATE, 1);
	}

  return cal;
}
%>
<%
	String testMode = request.getParameter("testMode");
	String solicitante = request.getParameter("solicitante");
	if(solicitante == null || solicitante.trim().equals(""))	{
		out.print("Erro #1 - Solicitante n�o encontrado");
		return;
	}
	String executor = "";
	
	Calendar cal15 = Calendar.getInstance();
	Calendar cal30 = Calendar.getInstance();
	cal15 = addBusinessDays(cal15, 15);
	cal30 = addBusinessDays(cal30, 30);
	
	SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yyyy");
	String prazo15 = formatter.format(cal15.getTime());
	String prazo30 = formatter.format(cal30.getTime());
	boolean isTest = false;
	if (testMode != null && testMode.trim().equals("sim")) {
		isTest = true;
	}
	//out.println(prazo15);
	//out.println(prazo30;
    //if (1==1)return;

	try {
		ArrayList<String> lista = new ArrayList<String>();
		lista.add("Realinhamento Contratual � Associa��o Magistrados|Em an�lise ao contrato de Asseio da Associa��o dos Magistrados Catarinenses, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  62,26% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|15");
		lista.add("Realinhamento Contratual � Cond. Villa Salomy|Em an�lise ao contrato de Asseio do Cond. Villa Salomy, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  26,25% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente. (Possui contrato de Vig. Humana)|30");
		lista.add("Realinhamento Contratual � Cond. Edif�cio Caravelle|Em an�lise ao contrato de Asseio do Cond. Edif�cio Caravelle, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  59,25% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|15");
		lista.add("Realinhamento Contratual � Cond. Residencial Quincio|Em an�lise ao contrato de Asseio do Cond. Residencial Quincio, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  29,31% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|30");
		lista.add("Realinhamento Contratual � Cond. Residencial Sergio Gil|Em an�lise ao contrato de Asseio do Cond. Residencial Sergio Gil, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  22,51% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente. (Possui contrato de Vig. Humana)|30");
		lista.add("Realinhamento Contratual � Cond. Residencial Aldebaran|Em an�lise ao contrato de Asseio do Cond. Residencial Aldebaran, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  22,46% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|30");
		lista.add("Realinhamento Contratual � Cond. Residencial Antares|Em an�lise ao contrato de Asseio do Cond. Residencial Antares, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  26,10% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|30");
		lista.add("Realinhamento Contratual � Cond. Residencial Recanto da Serra|Em an�lise ao contrato de Asseio do Cond. Residencial Recanto da Serra, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  92,97% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|15");
		lista.add("Realinhamento Contratual � NH Servi�os Cont�beis|Em an�lise ao contrato de Asseio do NH Servi�os Cont�beis, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  99,72% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|15");
		lista.add("Realinhamento Contratual � Cond. Residencial Walter Meyer|Em an�lise ao contrato de Asseio do Cond. Residencial Walter Meyer, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  29,56% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|30");
		lista.add("Realinhamento Contratual � Cond. Residencial Ana Lucia|Em an�lise ao contrato de Asseio do Cond. Residencial Ana Lucia, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  20,63% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|30");
		lista.add("Realinhamento Contratual � Femina (O BOTIC�RIO)|Em an�lise ao contrato de Asseio da Femina Com�rcio e Representa��es, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  317,17% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|15");
		lista.add("Realinhamento Contratual � CDL|Em an�lise ao contrato de Asseio do CDL, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  27,72% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|30");
		lista.add("Realinhamento Contratual � Floral Com�rcio de Cosm�ticos|Em an�lise ao contrato de Asseio da Floral Com�rcio de Cosm�ticos, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  66,43% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|15");
		lista.add("Realinhamento Contratual � Naiana da Nova|Em an�lise ao contrato de Asseio da Naiana da Nova, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  68,78% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|15");
		lista.add("Realinhamento Contratual � Carioca Cal�ados|Em an�lise ao contrato de Asseio da Carioca Cal�ados, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  82,94% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|15"); 
		lista.add("Realinhamento Contratual � Cia Latino Americana de Medicamentos|Em an�lise ao contrato de Asseio da Cia Latino Americana de Medicamentos, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  28,49% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente. (Possui contrato de Vig. Humana)|30");
		lista.add("Realinhamento Contratual � Cond. Residencial Atlantis|Em an�lise ao contrato de Asseio do Cond. Residencial Atlantis, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  164,70% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente. (Possui contrato de Vig. Humana)|15");
		lista.add("Realinhamento Contratual � Rodeio Bonito Hidrel�trica|Em an�lise ao contrato de Asseio do Rodeio Bonito Hidrel�trica, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  75,11% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente. (Possui contrato de Vig. Humana)|15");
		lista.add("Realinhamento Contratual � Cond. Residencial Padre Clemente|Em an�lise ao contrato de Asseio do Cond. Residencial Padre Clemente, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  20,38% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente. (Possui contrato de Vig. Humana)|30");
		lista.add("Realinhamento Contratual � Vert Com�rcio de Cosm�ticos|Em an�lise ao contrato de Asseio do Vert Com�rcio de Cosm�ticos, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  51,76% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente.|30");
		lista.add("Realinhamento Contratual � Cond. Residencial Mirante do Atl�ntico|Em an�lise ao contrato de Asseio do Cond. Residencial Mirante do Atl�ntico, foi visto que o valor contratual est� defasado. Mesmo com o reajuste da CCT 2013, o valor do contrato est�  20,38% abaixo do valor da nossa tabela de pre�os. Sendo assim, pe�o que seja renegociado o valor com o cliente. (Possui contrato de Vig. Humana)|30");

		for (String tarefa : lista) {
			StringTokenizer st = new StringTokenizer(tarefa, "|");
			String titulo = URLEncoder.encode(st.nextToken());
			String descricao = URLEncoder.encode(st.nextToken());
			String prazo = URLEncoder.encode(st.nextToken());
			String urlTarefa = PortalUtil.getBaseURL() + "custom/jsp/orsegups/iniciarTarefaSimples.jsp?avanca=sim&solicitante=" + solicitante + "&executor=rogerio&origem=1&descricao="+ descricao + "&titulo=" + titulo;
			
			if (prazo.equals("15")) {
				urlTarefa = urlTarefa + "&prazo=" + prazo15;
			} else {
				urlTarefa = urlTarefa + "&prazo=" + prazo30;
			}

			if (isTest) {
			    out.println(urlTarefa);
				out.println("<br>");
			} else {
				URL urlAux = new URL(urlTarefa);
				URLConnection uc = urlAux.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
				String retorno = in.readLine();
				//System.out.println(retorno);
				if (retorno == null || (retorno.trim().contains("Erro"))) {
					out.print("Erro #7 - Erro ao criar tarefa");
				}
				
			}
			
		}
		
		out.print("OK");		
		
	} catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #8 - Erro ao gerar OS");
		return;
	} finally {
	}
%>
