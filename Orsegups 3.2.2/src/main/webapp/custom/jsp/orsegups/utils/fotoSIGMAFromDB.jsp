<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.io.OutputStream"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%	
	if (PortalUtil.getCurrentUser() == null) {
		out.print("Acesso Negado");
		return;
	}
	
	String cdVtrImagem = request.getParameter("cdVtrImagem");
	String cdOsImagem = request.getParameter("cdOsImagem");

    PreparedStatement st = null;
    try {
		String nomeFonteDados = "SIGMA90";
		Connection conn = PersistEngine.getConnection(nomeFonteDados);
		byte[] imgData = null;

		StringBuffer sqlFoto = new StringBuffer();
		
		if (cdVtrImagem != null) {
			sqlFoto.append(" SELECT BL_IMAGEM FROM VTR_IMAGEM WHERE CD_VTR_IMAGEM = " + cdVtrImagem);	
		} else if (cdOsImagem != null) {
			sqlFoto.append(" SELECT BL_IMAGEM FROM OS_IMAGEM WHERE CD_OS_IMAGEM = " + cdOsImagem);	
		}
		st = conn.prepareStatement(sqlFoto.toString());
		ResultSet rs = st.executeQuery();
									
		if(rs.next()) {
			imgData = rs.getBytes("BL_IMAGEM");
			response.setContentType("image/jpeg"); 
			response.setContentLength(imgData.length);
			OutputStream o = response.getOutputStream(); 
			o.write(imgData);
			o.flush();
			o.close();
		}
		rs.close();
		st.close();

	} catch (Exception e) {
		e.printStackTrace();	
	}

%>