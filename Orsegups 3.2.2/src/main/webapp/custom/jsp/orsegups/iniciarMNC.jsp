<%@page import="com.neomind.fusion.workflow.simulation.WorkflowService"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>


<body><%


	String op = request.getParameter("op");
	if(op == null || op.trim().equals(""))	{
		out.print("Erro #1 - Opera��o inv�lida (#"+request.getParameter("op")+")");
		return;
	}
	Long numEmp = NeoUtils.safeLong(request.getParameter("numEmp"));
	if(numEmp == null || numEmp == 0L)	{
		out.print("Erro #2 - Empresa inv�lida (#"+request.getParameter("numEmp")+")");
		return;
	}
	Long tipCol = NeoUtils.safeLong(request.getParameter("tipCol"));
	if(tipCol == null || tipCol == 0L)	{
		out.print("Erro #3 - Tipo de Colaborador inv�lido (#"+request.getParameter("tipCol")+")");
		return;
	}
	Long numCad = NeoUtils.safeLong(request.getParameter("numCad"));
	if(numCad == null || numCad == 0L)	{
		out.print("Erro #4 - Matr�cula inv�lida (#"+request.getParameter("numCad")+")");
		return;
	}

	// filtro do colaborador
	QLGroupFilter colaboradorFilter = new QLGroupFilter("AND");
	colaboradorFilter.addFilter(new QLEqualsFilter("numemp", numEmp));
	colaboradorFilter.addFilter(new QLEqualsFilter("tipcol", tipCol));
	colaboradorFilter.addFilter(new QLEqualsFilter("numcad", numCad));
	
	NeoObject noColaborador = null;
	List<NeoObject> listaColaborador = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) (EntityRegister.getCacheInstance().getByType("VETORHUSUFUNFUSION"))).getEntityClass(), colaboradorFilter);
	if(listaColaborador != null && listaColaborador.size() > 0) {
		noColaborador = listaColaborador.get(0);
	}else {
		out.print("Erro #5 - Erro ao localizar o colaborador do Vetorh pelo filtro.");
		return;
	}
	if (op.equals("query")) {
		/*
		InstantiableEntityInfo eCancEletIna = AdapterUtils.getInstantiableEntityInfo("FCCcancelamentoEletrInadimplencia");

		List<NeoObject> listaRegCanc = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna.getEntityClass(), new QLEqualsFilter("contratoSapiens3", contrato));
		String result = "NA";
		if(listaRegCanc != null && listaRegCanc.size() > 0) {
			for(NeoObject noRegCanc : listaRegCanc) {
				EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
				WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();	
				if (wfpCanc.getProcessState().toString().equals("running")) {
					result = wfpCanc.getCode();
				}
			}
		} 
		out.print(result);
		*/
	} else if (op.equals("queryDetails")) {
		
		GregorianCalendar datAnt = AdapterUtils.getGregorianCalendar(request.getParameter("datAnt"), "dd/MM/yyyy");
		if(datAnt == null)
		{
			out.print("Erro #6 - Erro ao gerar Data Admissao do ano anterior (#"+request.getParameter("datAnt")+")");
			return;
		}

		GregorianCalendar datVig = AdapterUtils.getGregorianCalendar(request.getParameter("datVig"), "dd/MM/yyyy");
		if(datVig == null)
		{
			out.print("Erro #7 - Erro ao gerar data fim da admissao do ano atual (#"+request.getParameter("datVig")+")");
			return;
		}

		GregorianCalendar datCobIni = AdapterUtils.getGregorianCalendar(request.getParameter("datCobIni"), "dd/MM/yyyy");
		if(datCobIni == null)
		{
			out.print("Erro #7 - Erro ao gerar Data Admissao do ano anterior (#"+request.getParameter("datCobIni")+")");
			return;
		}

		GregorianCalendar datCobFim = AdapterUtils.getGregorianCalendar(request.getParameter("datCobFim"), "dd/MM/yyyy");
		if(datCobFim == null)
		{
			out.print("Erro #8 - Erro ao gerar data fim da admissao do ano atual (#"+request.getParameter("datCobFim")+")");
			return;
		}
		
		Long datIniCob = datCobIni.getTimeInMillis();
	    Long datFimCob = datCobFim.getTimeInMillis();		
		
		QLEqualsFilter filtroColaborador = new QLEqualsFilter("colaborador", noColaborador);
		QLOpFilter datefilterini = new QLOpFilter("wfprocess.startDate", ">=", (GregorianCalendar) datAnt);
		QLOpFilter datefilterfim = new QLOpFilter("wfprocess.startDate", "<=", (GregorianCalendar) datVig);
		QLEqualsFilter datefilterState = new QLEqualsFilter("wfprocess.processState",0);
		QLEqualsFilter filtroGeraPagamento = new QLEqualsFilter("pagarCobertura",Boolean.TRUE);
		
		QLGroupFilter filtroMnc = new QLGroupFilter("AND");
		
		filtroMnc.addFilter(filtroColaborador);
		filtroMnc.addFilter(datefilterini);
		filtroMnc.addFilter(datefilterfim);
		filtroMnc.addFilter(datefilterState);
		filtroMnc.addFilter(filtroGeraPagamento);
		
		List<NeoObject> listMnc = PersistEngine.getObjects(AdapterUtils.getEntityClass("MNCProcessoMovimentoNumerarioCobertura"), filtroMnc);		
		
		if(listMnc != null && !listMnc.isEmpty()){
						
			Long diasMNC = 0L;
			
			for(NeoObject noTarMnc : listMnc){
				EntityWrapper ewTarMnc = new EntityWrapper(noTarMnc);
				GregorianCalendar dataInicioCobertura = (GregorianCalendar) ewTarMnc.findValue("dataInicio");
				GregorianCalendar dataFimCobertura = (GregorianCalendar) ewTarMnc.findValue("dataFim");
				Long quantidadeFaltas = (Long) ewTarMnc.findValue("quantidadeFaltas");
				
				//Soma a quantidade de dias da MNC quando o colaborador tiver mais de uma tarefa de MNC, no mesmo per�odo aquisitivo.
				diasMNC = diasMNC + (((Long.valueOf(dataFimCobertura.getTimeInMillis()) - (Long.valueOf(dataInicioCobertura.getTimeInMillis()))) / 86400000L) - quantidadeFaltas);
				
				Long dataInicioCob = dataInicioCobertura.getTimeInMillis();
			    Long dataFimCob = dataFimCobertura.getTimeInMillis();
				
				if(dataInicioCob.toString().equals(datIniCob.toString()) && dataFimCob.toString().equals(datFimCob.toString())){
					out.print("Colaborador j� possui MNC aberta nesta data");
					return;	
				}
			}
			
			if(diasMNC >= 60L){
				out.print("Colaborador ja possui MNC com mais de 60 Dias, para o per�odo informado");
				return;
			} else {
				out.print("OK");
				return;
			}
		} else {
			out.print("OK");
			return;	
		}
		
		/*
		InstantiableEntityInfo eCancEletIna = AdapterUtils.getInstantiableEntityInfo("FCCcancelamentoEletrInadimplencia");

		List<NeoObject> listaRegCanc = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna.getEntityClass(), new QLEqualsFilter("contratoSapiens3", contrato));
		String result = "NA";
		if(listaRegCanc != null && listaRegCanc.size() > 0) {
			for(NeoObject noRegCanc : listaRegCanc) {
				EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
				WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();	
				if (wfpCanc.getProcessState().toString().equals("running")) {
			
					// filtro para pegar as atividades
					QLGroupFilter gAct = new QLGroupFilter("AND");
					gAct.addFilter(new QLFilterIsNotNull("startDate"));
					gAct.addFilter(new QLEqualsFilter("process",wfpCanc));
					
					// atividades em aberto
					Collection<Activity> colAct = PersistEngine.getObjects(Activity.class,gAct);
					result = wfpCanc.getCode() + "Tarefas/Atividades em aberto: <strong>" + colAct.size() + "</strong><br>";
					
					// tarefas em aberto
					for(Activity a : colAct)
					{			
						QLGroupFilter gTask = new QLGroupFilter("AND");
						gTask.addFilter(new QLFilterIsNull("finishDate"));
						gTask.addFilter(new QLFilterIsNotNull("startDate"));
						gTask.addFilter(new QLEqualsFilter("activity",a));
						
						Task task = (Task) PersistEngine.getObject(Task.class,gTask);
						
						// pode estar pool, dai nao tem Task ainda
						if(task != null) {
							result = result + "Tarefa: <strong>" + task.getActivityName() + "</strong> para o usu�rio: <strong>" + task.getUser().getFullName() + "</strong><br>";
						} else {
							result = result + "Atividade: <strong>" + a.getActivityName() + "</strong> em POOL para: <strong>";
							if (a instanceof UserActivity)
							{
								for (SecurityEntity sec : a.getInstance().getPotentialOwners())
								{
									if (sec instanceof NeoUser)
									{
										result = result + ", " + ((NeoUser) sec).getFullName();
									} 
									else if (sec instanceof NeoPaper)
									{
										result = result + ", " + ((NeoPaper) sec).getName();
									}
									else if (sec instanceof NeoGroup)
									{
										result = result + ", " + ((NeoGroup) sec).getName();
									}
									else if (sec instanceof NeoRole)
									{
										result = result + ", " + ((NeoRole) sec).getName();
									}
									
									if (a.getInstance().getPotentialOwners().size() > i)
									{
										result = result + ", " + ", ";
									}
								}
							}
							result = result + "</strong><br>";
						}
					}		
				}
			}
		} 
		
		out.print(result);
		*/
	} else if (op.equals("start")) {
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("solicitante")));
		if(solicitante == null)
		{
			out.print("Erro #6 - Solicitante n�o encontrado (#"+request.getParameter("solicitante")+")");
			return;
		}
		
		GregorianCalendar dataInicio = AdapterUtils.getGregorianCalendar(request.getParameter("datIni"), "dd/MM/yyyy");
		if(dataInicio == null)
		{
			out.print("Erro #7 - Erro ao gerar Data Inicio (#"+request.getParameter("datIni")+")");
			return;
		}

		GregorianCalendar dataFim = AdapterUtils.getGregorianCalendar(request.getParameter("datFim"), "dd/MM/yyyy");
		if(dataFim == null)
		{
			out.print("Erro #8 - Erro ao gerar Data Fim (#"+request.getParameter("datFim")+")");
			return;
		}

		final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class,	new QLEqualsFilter("name", "R031 - MNC - Movimento de Numer�rio de Cobertura"));
		
		if(pm == null)
		{
			out.print("Erro #9 - Erro ao recuperar modelo do processo ");
			return;
		}
		
		final EntityWrapper ewColaborador = new EntityWrapper(noColaborador);
		
		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 12/03/2015
		 */
		
		final WFProcess processo = WorkflowService.startProcess(pm, false, solicitante);
		final NeoObject wkfFCC = processo.getEntity();
		final EntityWrapper ewWkfFCC = new EntityWrapper(wkfFCC);
		
		ewWkfFCC.findField("dataInicio").setValue(dataInicio);
		ewWkfFCC.findField("dataFim").setValue(dataFim);
		ewWkfFCC.findField("colaborador").setValue(noColaborador);
		ewWkfFCC.findField("nomeColaborador").setValue(ewColaborador.findField("nomfun").getValue());
		ewWkfFCC.findField("numeroEmpresa").setValue(ewColaborador.findField("numemp").getValue());
		ewWkfFCC.findField("numeroCadastro").setValue(ewColaborador.findField("numcad").getValue());
		
		try
		{
			new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividadePool(solicitante, false);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			out.print("Erro #4 - Erro ao avan�ar a primeira tarefa");
			return;
		}
		
		/**
		 * FIM ALTERA��ES - NEOMIND
		 */
			
		// como a primeira atividade (inicial) do fluxo eh uma roteadora,
		// o fluxo avanca automaticamente para a segunda atividade.
		out.print("OK:" + processo.getCode());
	} else {
		out.print("Erro #10 - Opera��o inv�lida");
		return;
	}
%>
</body>
