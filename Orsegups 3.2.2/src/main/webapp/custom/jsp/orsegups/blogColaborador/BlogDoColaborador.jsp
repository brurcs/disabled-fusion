<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>

<portlet:defineObjects />
<form:defineObjects />
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/styleBlogColaborador.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<portal:head title="Blog do Colaborador">
	 	<!-- <iframe width="100%" height="100%" src="http://orsegupsnews.com.br/"/> -->
	 	<div class="panel panel-primary" id="panel-colab">
	 		<div class="panel-heading">
		 		<span>
		 			<b>PORTAL DO COLABORADOR ORSEGUPS NEWS</b>
		 		</span>
	 		</div>
	 		<a href="http://orsegupsnews.com.br/" target="_blank">
		 		<div class="panel-body" id="panel-img">
					<img src="imagens/custom/blog_colaborador.png" alt="Blog do Colaborador">
		 		</div>
		 	</a>
	 	</div>
</portal:head>