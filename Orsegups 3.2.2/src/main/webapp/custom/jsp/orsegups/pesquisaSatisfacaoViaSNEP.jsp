<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>

<%
	response.setCharacterEncoding("ISO8859-1");
	
	//Dados de requisi��o
	String fone = request.getParameter("fone");
	if (fone == null || fone.trim().equals(""))
	{
		out.print("Erro #1 - Telefone n�o informado");
		return;
	}
	
	String notaEficiencia = (String) request.getParameter("notaEficiencia");
	if(notaEficiencia == null || notaEficiencia.trim().equals(""))
	{
		out.print("Erro #2 - Resposta da efici�ncia n�o informada");
		return;
	}
	
	String satisfeito = request.getParameter("isSatisfeito");
	if (satisfeito == null || satisfeito.trim().equals(""))
	{
		out.print("Erro #3 - Resposta da satisfa��o n�o informada");
		return;
	}
	
	if (!(satisfeito.equals("N�o") || satisfeito.equals("Sim")))
	{
		out.print("Erro #4 - Resposta da satisfa��o inv�lida.");
		return;
	}
	
	String maisDeUmaTentativa = request.getParameter("isTentativa");
	if (maisDeUmaTentativa == null || maisDeUmaTentativa.trim().equals("")){
		out.print("Erro #5 - Resposta da tentativa inv�lida.");
		return;
	}
	
	out.print("OK");
%>

