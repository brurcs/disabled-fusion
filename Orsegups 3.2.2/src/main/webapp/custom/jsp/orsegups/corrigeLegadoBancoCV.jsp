<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%

	Class clazz = AdapterUtils.getEntityClass("CVbancoDeCurriculos");

	List<NeoObject> cvList = PersistEngine.getObjects(clazz);
	
	if(cvList != null && !cvList.isEmpty())
	{
		String total = String.valueOf(cvList.size());
		
		Integer count = 1;
		
		for(NeoObject cv : cvList)
		{
			EntityWrapper cvWrapper = new EntityWrapper(cv);
			
			cvWrapper.setValue("cidadeStr", cvWrapper.findValue("cidade.nomcid"));
			cvWrapper.setValue("bairroStr", cvWrapper.findValue("bairro.nombai"));
			
			out.print(count+"/"+total);
			out.print("<br>");
			out.flush();
			
			count++;
		}
	}

%>
