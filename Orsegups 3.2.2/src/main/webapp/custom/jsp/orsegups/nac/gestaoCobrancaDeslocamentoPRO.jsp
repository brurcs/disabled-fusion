<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 	 <link rel="stylesheet" href="css/estilos.css">
 	 <script src="js/custom-pro.js"></script>
	<title>Gest�o Cobran�a Deslocamento PRO</title>
	</head>
	<body>
	<section id=main>
		<div class="container-fluid" align="center">
			<div class="page-header">
				<h1>
					Gest�o de Cobran�a de Deslocamento PRO
						<span class="pull-right label label-default">PRO</span>
				</h1>
			</div>
			<div class="row" align="center" width="98%">
				<div class="col-md-12">
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1primary" data-toggle="tab">Informa��es Gerais</a></li>
								<li><a href="#tab2primary" data-toggle="tab">Exce��o Cliente</a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
										<div class="info">
											<h3>Ser� gerado a cobran�a autom�tica de Deslocamento quando:</h3>
											<ul>
												<li>Descri��o '#COBRAR'.</li>
												<li>Empresas 15, 17, 18, 19, 21.</li>
												<li>Cliente privado.</li>
												<li>Quando a data do fechamento for igual ao dia anterior</li>
												<li>Postos ativos ou inativos com data fim maior que a data do fechamento.</li>
												<li>Postos da familia SER102 - Eletr�nica.</li>												
												<li>Respeitar o cadastro de exce��es: clientes.</li>
												<br>										
											</ul>
										</div>
								</div>
								<div class="tab-pane fade" id="tab2primary">
									<div class="row">
										<div class="col-lg-4 pull-right">
										    <div class="input-group ">
										        <input type="number" id="txtBusca" class="form-control" placeholder="Pesquisar por c�digo do cliente..." onkeyup="return buscarCliente(event);">
										      	<span class="input-group-btn">
										        	<button class="btn btn-default" type="button" onclick="buscarCliente();">Pesquisar</button>
										      	</span>
										    </div><!-- /input-group -->
										</div>
									</div>
									<hr class="hr-default">
<!-- 									<input type="number" id="txtBusca" placeholder="Buscar..." onkeyup="return buscarCliente(event);"> -->
									<!-- E-form Exce��o Cliente -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<iframe id="frame-cliente" name="frame-cliente" class="embed-responsive-item" src="https://intranet.orsegups.com.br/fusion/portal/render/FormList?type=CadastroExcecoesCobrancaDeslocamentoPRO&showPages=true&edit=true&creatable=true"></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>