<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 	 <link rel="stylesheet" href="css/estilos.css">
 	 <script src="js/custom-os.js"></script>
	<title>Gest�o Cobran�a OS</title>
	</head>
	<body>
	<section id=main>
		<div class="container-fluid" align="center">
			<div class="page-header">
				<h1>
					Gest�o de Cobran�a de OS
						<span class="pull-right label label-default">OS</span>
				</h1>
			</div>
			<div class="row" align="center" width="98%">
				<div class="col-md-12">
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1primary" data-toggle="tab">Informa��es Gerais</a></li>
								<li><a href="#tab2primary" data-toggle="tab">Exce��o Cliente</a></li>
								<li><a href="#tab3primary" data-toggle="tab">Exce��o Defeito</a></li>
								<li><a href="#tab4primary" data-toggle="tab">Exce��o Servi�o</a></li>
								<li><a href="#tab5primary" data-toggle="tab">Hist�rico Cobran�a</a></li>	
							</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
										<div class="info">
											<h3>Ser� gerado a cobran�a autom�tica de Ordens de servi�o quando:</h3>
											<ul>
												<li>Empresas 15, 18, 19, 21.</li>
												<li>Cliente privado.</li>
												<li>Equipamento for do cliente.</li>
												<li>Quando a data de inicio da Ordem de Servi�o for superior h� 30 dias do inicio do contrato do cliente.</li>
												<li>Postos ativos ou inativos com data fim maior que a data da OS.</li>
												<li>Postos da familia SER102 - Eletr�nica.</li>
												<li>Descri��o de execu��o da OS n�o seja 'Fechamento Autom�tico'.</li>
												<li>Os campos data de inicio, fim da execu��o e data fim da Ordem de Servi�o estejam preenchidos.</li>
												<li>Respeitar o cadastro de exce��es: clientes, defeitos, servi�os e o hist�rico j� cobrado.</li>
												<br>
												<li>Aviso: <br>
													N�o s�o cobradas as Ordem de Servi�o abertas sobre o mesmo defeito dentro do per�odo de 30 dias. </li>											
											</ul>
										</div>
								</div>
								<div class="tab-pane fade" id="tab2primary">
									<div class="row">
										<div class="col-lg-4 pull-right">
										    <div class="input-group ">
										        <input type="number" id="txtBusca" class="form-control" placeholder="Pesquisar por c�digo do cliente..." onkeyup="return buscarCliente(event);">
										      	<span class="input-group-btn">
										        	<button class="btn btn-default" type="button" onclick="buscarCliente();">Pesquisar</button>
										      	</span>
										    </div><!-- /input-group -->
										</div>
									</div>
									<hr class="hr-default">
<!-- 									<input type="number" id="txtBusca" placeholder="Buscar..." onkeyup="return buscarCliente(event);"> -->
									<!-- E-form Exce��o Cliente -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<iframe id="frame-cliente" name="frame-cliente" class="embed-responsive-item" src="https://intranet.orsegups.com.br/fusion/portal/render/FormList?type=CadastroExcecoesCobrancaOS&showPages=true&edit=true&creatable=true"></iframe>
									</div>
									
								</div>
								<div class="tab-pane fade" id="tab3primary">
									<!-- E-form Exce��o Defeito  intranet.orsegups.com.br-->
									<div class="embed-responsive embed-responsive-16by9">
	  									<iframe class="embed-responsive-item" src="https://intranet.orsegups.com.br/fusion/portal/render/FormList?type=CadastroExcecoesDefeitoOS&showPages=true&edit=true&creatable=true"></iframe>
									</div>
									
								</div>

								<div class="tab-pane fade" id="tab4primary">
									<!-- E-form Exce��o Defeito -->
									<div class="embed-responsive embed-responsive-16by9">
										<iframe class="embed-responsive-item"
											src="https://intranet.orsegups.com.br/fusion/portal/render/FormList?type=CadastroExcecoesServico&showPages=true&edit=true&creatable=true"></iframe>
									</div>
								</div>

								<div class="tab-pane fade" id="tab5primary">
									<!-- E-form hist�rico cobran�a -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<iframe class="embed-responsive-item" src="https://intranet.orsegups.com.br/fusion/portal/render/FormList?type=HistoricoCobrancaOS&showPages=true&edit=false&creatable=false"></iframe>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>