<!DOCTYPE html>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.custom.orsegups.medicaoContratos.OrsegupsMedicaoUtilsMensal"%>
<%@page import="java.util.ArrayList"%>
<html>
	<head>
	<%@page import="com.neomind.fusion.portal.PortalUtil"%>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="css/estilos.css">
	<title>Controle Tarefas de Reciclagem</title>
	 <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<link rel="stylesheet" 
		href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
		<script type="text/javascript" 
		src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" 
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
	<script src="js/jquery.growl.js" type="text/javascript"></script>
		<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />
	</head>
	<section id=main>
		<div class="container-fluid" align="center">
			<div class="page-header">
				<h1>
					Tarefas de Reciclagem
				</h1>
			</div>
			<div class="row" align="center" width="98%">
				<div class="col-md-12">
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading" style="background-color:#6b7486;">
							<ul class="nav nav-tabs" style="background-color:#6b7486;">
								<li class="active"><a href="#tab1primary" data-toggle="tab">Tarefas</a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
										<div class="form-inline"style="text-align: left;">
															<input type="text" class="form-control campoformulario"  id="codigoTarefa" name="codigo" data-options="required:true" placeholder="C�digo da Tarefa">
															<input type="number" class="form-control campoformulario"  id="matricula" name="codigo" placeholder="Matricula do Colaborador">
															<input type="text" class="form-control campoformulario"  id="nomeColaborador" name="codigo" placeholder="Nome do Colaborador">
	                                         				<input type="button" class="btn btn-primary campoBotao" value="Pesquisar" id="pesquisar" name="pesquisar" onclick="pesquisarTarefa()"><br><br> 
															
	                   							</div>
										 <div class="table-responsive">          
											  <table id="tabelaTarefas" class="table tableNova">
											    <thead>
											      <tr>
											        <th>C�digo</th>
		           									<th>Empresa</th>
		           									<th>Matricula</th>
		           									<th>Colaborador</th>
		           									<th>Data Inicio do Curso</th>
		           									<th>Data Final do Curso</th>
		           									<th>Editar</th>
											      </tr>
											    </thead>
											  </table>
											  </div>
									     <input type="hidden" id="idTarefa"></input>
								 </div>
								</div>
								<div id="dvEditarTarefa" title="Informe novas datas" class="easyui-window" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false,closable:true" style="width:520px;height:250px">  
									<div class="form-inline"style="text-align: left;">
  																			<div class="field_container">
  																				<form>
  																					<fieldset class="field_group">
    																					<legend>Data do Curso&nbsp&nbsp
    																					</legend>
    																					<table>
    																						<div style="text-align: left;">
    																							<tr>
    																								<td style="width:420px">
  																											Data Inicio:<input id="dtIniCur" type="Date" style="width:200px" class="form-control"></input>
  																									</td>
  																									<td style="width:420px">
	    																									Data Fim:<input id="dtFimCur" type="Date" style="width:200px" class="form-control"></input>
  																									</td>
  																								</tr>
  																							</div>
  																						</table>
    																				</fieldset>
																				</form>
																				<br><br><input type="button" class="btn btn-primary campoBotao" value="Salvar" id="SalvarTarefa" name="Salvar" onclick="salvarTarefa()">
																			</div>
								</div>
								<div id="dlgLoader" title="Carregando..." class="easyui-window" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false,closable:false" style="width:220px;height:120px">  
											<div style="width:100%; text-align:center; margin-top: 10px">
												<img src="imagens/loader.gif">
											</div>	
										</div>
									<script type="text/javascript">
										$( document ).ready(function() {
											startProgress();
											data = "action=listaTarefasReciclagem";
                                           	jQuery.ajax({
                                            	method: 'post',
                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.reciclagem.servlet.ServletReciclagem',
                                                dataType: 'json',	
                                                data: data,
                                                success: function(result){
                                                	$("#tabelaTarefas > tbody").empty();
                                                	for(i in result){
                                                		$('#tabelaTarefas').append("<tr><td>" + result[i].code + 
                                            					"</td><td>" +result[i].numEmp + "</td>"+
                                            					"</td><td>" + result[i].numCad + "</td>"+
                                            					"</td><td>" + result[i].nomeFun + "</td>"+
                                            					"</td><td>" + result[i].dtIniCurString + "</td>"+
                                            					"</td><td>" + result[i].dtFinCurString + "</td>"+
                                            					"</td><td>" + result[i].linkEditar + "</td></tr>");
                                                	}
                                                	stopProgress();
                                                }
                                            });
                                        });
										 function editarTarefa(neoId,dtIniCur,dtFimCur){
											 document.getElementById("idTarefa").value = neoId;
											 console.log(neoId);
											
											if(dtIniCur != null){
												document.getElementById("dtIniCur").value = dtIniCur;	
											}
											if(dtFimCur != null){
												document.getElementById("dtFimCur").value = dtFimCur;												
											}
											$("#dvEditarTarefa").dialog("open");
										 }
										 
										 function salvarTarefa(){
											 var neoId = document.getElementById("idTarefa").value;
											 var dtIni = document.getElementById("dtIniCur").value;
											 var dtFim = document.getElementById("dtFimCur").value;
											 var data = 'action=salvarDataReciclagem&datIni='+dtIni+'&datFim='+dtFim+'&neoId='+neoId;
											 jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.reciclagem.servlet.ServletReciclagem',
	                                                dataType: 'json',	
	                                                data: data,
	                                                success: function(result){
	                                                	listarTarefas();
	                                                	$("#dvEditarTarefa").dialog("close");
	                                                	$.growl({ title: "", message: "Registro Alterado Com Sucesso!" });
	                                                }
	                                            });
										 }
										 
										 function pesquisarTarefa(){
											startProgress();
											var codigoTarefa = document.getElementById("codigoTarefa").value;
										 	var matricula = document.getElementById("matricula").value;
										 	var nomeColaborador = document.getElementById("nomeColaborador").value;
											
                              				var data = 'action=pesquisarTarefa&codigoTarefa='+codigoTarefa+'&matricula='+matricula+'&nomeColaborador='+nomeColaborador;
											 jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.reciclagem.servlet.ServletReciclagem',
	                                                dataType: 'json',	
	                                                data: data,
	                                                success: function(result){
	                                                	$('#tabelaTarefas').datagrid('loadData',result);
	                                                	$('#tabelaTarefas').datagrid('load',result);
	                                                	stopProgress();
	                                                }
	                                            });
											 stopProgress();
										 }
										 
										 function listarTarefas(){
											 data = "action=listaTarefasReciclagem";
	                                           	jQuery.ajax({
	                                            	method: 'post',
	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.reciclagem.servlet.ServletReciclagem',
	                                                dataType: 'json',	
	                                                data: data,
	                                                success: function(result){
	                                                	$('#tabelaTarefas').datagrid('loadData',result);
	                                                	$('#tabelaTarefas').datagrid('load',result);
	                                                }
	                                            });
										 }
										 
										 function startProgress(){
									        	$('#dlgLoader').window('open');
									        }
									        
									        function stopProgress(){
									        	$('#dlgLoader').window('close');
									        }
										 
                                  	</script>
								</div>
							</div>
						</div>
					</div>
				</div>
	</section>
	<style>
	.campoformulario{
		max-width: 220px;
		min-width: 220px;
	}
	
	.camposCadastroColaborador{
		max-width: 420px;
		min-width: 420px;
	}
	.campoBotao{
		max-width: 120px;
		min-width: 120px;
		background-color:#6b7486;
	    
	}
	.tipoOk { background-image:url(imagens/certo2.png);   }
	.tipoFalta { background-image:url(imagens/exclamacao2.png); }
	

	.table {
    	border-collapse: collapse;
    	width: 100%;
    	class: "";
   }

	.thtd {
    	text-align: left;
    	padding: 8px;
	}

	.tr:nth-child(even){background-color: #f2f2f2}

	th {
    	background-color: #6b7486;
    	color: white;
	}

	</style>	
</body>
</html>


