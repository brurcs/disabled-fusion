<%@page import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertEventEngine"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<!DOCTYPE html>
<html>


<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>

<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.css"
	media="screen" />
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.mask.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.callcenter.validade.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.qtip-1.0.0-rc3.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/moment.js"></script>	


<style type="text/css">
#bodyId div {
	color: #08285a;
	font-family: Tahoma, Arial, Verdana, "Trebuchet MS";
}

#detalhesContrato ul {
	margin-top: 5px;
	margin-left: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
	padding: 0;
}

#detalhesContrato ul li {
	margin: 0;
	padding: 0;
	float: left;
	width: 50%;
	list-style: none;
	font-size: 12px;
}

#detalhesContrato ul li span {
	font-weight: bold;
}

#detalhesContrato ul fieldset {
	border: 0;
	background: #e7efff;
	margin-bottom: 5px;
	padding-bottom: 10px;
	text-indent: 10px;
}

#detalhesContrato ul fieldset legend {
	font-weight: bold;
	font-size: 12px;
	background: transparent;
}

#buscaAvancadaWest ul {
	margin-top: 0px;
	margin-left: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	padding: 0;
}

#buscaAvancadaWest ul li {
	margin: 0;
	padding: 0;
	float: left;
	width: 100%;
	list-style: none;
	font-size: 12px;
}

#buscaAvancadaWest ul li span {
	font-weight: normal;
	width: 100px;
	display: inline-block;
}

#buscaAvancadaWest ul fieldset {
	border: 0;
	background: #e7efff;
	margin-bottom: 5px;
	padding-bottom: 10px;
	text-indent: 0px;
}

.destaque {
	font-weight: bold;
	font-size: 12px;
	color: #dd0005;
}

.ramal {
	padding: 5px;
	font-weight: bold;
	font-size: 12px;
}

.externalNumber {
	padding: 5px;
	font-weight: bold;
	font-size: 12px;
}

.modal {
	display: none;
	position: fixed;
	z-index: 1000;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background: rgba(255, 255, 255, .8)
		url('<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/images/loading.gif')
		50% 50% no-repeat;
}
/* When the body has the loading class, we turn
         the scrollbar off with overflow:hidden */
body.loading {
	overflow: hidden;
}

.helpContent /* Anytime the body has the loading class, our
         modal element will be visible */ 
         body.loading .modal {
	display: block;
}

.invisible {
	display: none;
}

.visible {
	display: block;
}

.left {
	float: left;
	margin-right: 3px;
}

.fieldContainer {
	display: inline-block;
}

.spanValue {
	margin-top: 3px;
}

.ico {
	width: 12px;
}

.calling {
	font-weight: bold;
	color: red;
}

#fm {
	margin: 0;
	padding: 10px 30px;
}

.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}

.fitem {
	margin-bottom: 5px;
}

.fitem label {
	display: inline-block;
	width: 80px;
}

.input {
	border: 1px solid #08285a;
	padding: 2px;
}

.textarea {
	border: 2px solid #765942;
	border-radius: 10px;
}

#fm {
	margin: 0;
	padding: 10px 30px;
}

.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}

.fitem {
	margin-bottom: 5px;
}

.fitem label {
	display: inline-block;
	width: 80px;
}

.video-list-thumbs {
	
}

.video-list-thumbs>li {
	margin-bottom: 12px;
}

.video-list-thumbs>li:last-child {
	
}

.video-list-thumbs>li>a {
	display: block;
	position: relative;
	background-color: #111;
	color: #fff;
	padding: 8px;
	border-radius: 3px transition:all 500ms ease-in-out;
	border-radius: 4px
}

.video-list-thumbs>li>a:hover {
	box-shadow: 0 2px 5px rgba(0, 0, 0, .3);
	text-decoration: none
}

.video-list-thumbs h2 {
	bottom: 0;
	font-size: 14px;
	height: 33px;
	margin: 8px 0 0;
}

.video-list-thumbs .glyphicon-play-circle {
	font-size: 60px;
	opacity: 0.6;
	position: absolute;
	right: 39%;
	top: 31%;
	text-shadow: 0 1px 3px rgba(0, 0, 0, .5);
	transition: all 500ms ease-in-out;
}

.video-list-thumbs>li>a:hover .glyphicon-play-circle {
	color: #fff;
	opacity: 1;
	text-shadow: 0 1px 3px rgba(0, 0, 0, .8);
}

.video-list-thumbs .duration {
	background-color: rgba(0, 0, 0, 0.4);
	border-radius: 2px;
	color: #fff;
	font-size: 11px;
	font-weight: bold;
	left: 12px;
	line-height: 13px;
	padding: 2px 3px 1px;
	position: absolute;
	top: 12px;
	transition: all 500ms ease;
}

.video-list-thumbs>li>a:hover .duration {
	background-color: #000;
}

@media ( min-width :320px) and (max-width: 480px) {
	.video-list-thumbs .glyphicon-play-circle {
		font-size: 35px;
		right: 36%;
		top: 27%;
	}
	.video-list-thumbs h2 {
		bottom: 0;
		font-size: 12px;
		height: 22px;
		margin: 8px 0 0;
	}
}

.btn3d, .btn3d:focus {
	position: relative;
	top: -6px;
	border: 0;
	-moz-outline-style: none;
	outline: medium none;
	transition: all 0.04s linear;
	margin-top: 10px;
	margin-bottom: 10px;
	margin-left: 2px;
	margin-right: 2px;
}

.btn3d:active, .btn3d.active {
	top: 2px;
}

.btn3d.btn-white {
	color: #666666;
	box-shadow: 0 0 0 1px #ebebeb inset, 0 0 0 2px rgba(255, 255, 255, 0.10)
		inset, 0 8px 0 0 #f5f5f5, 0 8px 8px 1px rgba(0, 0, 0, .2);
	background-color: #fff;
}

.btn3d.btn-white:active, .btn3d.btn-white.active {
	color: #666666;
	box-shadow: 0 0 0 1px #ebebeb inset, 0 0 0 1px rgba(255, 255, 255, 0.15)
		inset, 0 1px 3px 1px rgba(0, 0, 0, .1);
	background-color: #fff;
}

.btn3d.btn-default {
	color: #666666;
	box-shadow: 0 0 0 1px #ebebeb inset, 0 0 0 2px rgba(255, 255, 255, 0.10)
		inset, 0 8px 0 0 #BEBEBE, 0 8px 8px 1px rgba(0, 0, 0, .2);
	background-color: #f9f9f9;
}

.btn3d.btn-default:active, .btn3d.btn-default.active {
	color: #666666;
	box-shadow: 0 0 0 1px #ebebeb inset, 0 0 0 1px rgba(255, 255, 255, 0.15)
		inset, 0 1px 3px 1px rgba(0, 0, 0, .1);
	background-color: #f9f9f9;
}

.btn3d.btn-primary {
	box-shadow: 0 0 0 1px #417fbd inset, 0 0 0 2px rgba(255, 255, 255, 0.15)
		inset, 0 8px 0 0 #4D5BBE, 0 8px 8px 1px rgba(0, 0, 0, 0.5);
	background-color: #4274D7;
}

.btn3d.btn-primary:active, .btn3d.btn-primary.active {
	box-shadow: 0 0 0 1px #417fbd inset, 0 0 0 1px rgba(255, 255, 255, 0.15)
		inset, 0 1px 3px 1px rgba(0, 0, 0, 0.3);
	background-color: #4274D7;
}

.btn3d.btn-success {
	box-shadow: 0 0 0 1px #31c300 inset, 0 0 0 2px rgba(255, 255, 255, 0.15)
		inset, 0 8px 0 0 #5eb924, 0 8px 8px 1px rgba(0, 0, 0, 0.5);
	background-color: #78d739;
}

.btn3d.btn-success:active, .btn3d.btn-success.active {
	box-shadow: 0 0 0 1px #30cd00 inset, 0 0 0 1px rgba(255, 255, 255, 0.15)
		inset, 0 1px 3px 1px rgba(0, 0, 0, 0.3);
	background-color: #78d739;
}

.btn3d.btn-info {
	box-shadow: 0 0 0 1px #00a5c3 inset, 0 0 0 2px rgba(255, 255, 255, 0.15)
		inset, 0 8px 0 0 #348FD2, 0 8px 8px 1px rgba(0, 0, 0, 0.5);
	background-color: #39B3D7;
}

.btn3d.btn-info:active, .btn3d.btn-info.active {
	box-shadow: 0 0 0 1px #00a5c3 inset, 0 0 0 1px rgba(255, 255, 255, 0.15)
		inset, 0 1px 3px 1px rgba(0, 0, 0, 0.3);
	background-color: #39B3D7;
}

.btn3d.btn-warning {
	box-shadow: 0 0 0 1px #d79a47 inset, 0 0 0 2px rgba(255, 255, 255, 0.15)
		inset, 0 8px 0 0 #D79A34, 0 8px 8px 1px rgba(0, 0, 0, 0.5);
	background-color: #FEAF20;
}

.btn3d.btn-warning:active, .btn3d.btn-warning.active {
	box-shadow: 0 0 0 1px #d79a47 inset, 0 0 0 1px rgba(255, 255, 255, 0.15)
		inset, 0 1px 3px 1px rgba(0, 0, 0, 0.3);
	background-color: #FEAF20;
}

.btn3d.btn-danger {
	box-shadow: 0 0 0 1px #b93802 inset, 0 0 0 2px rgba(255, 255, 255, 0.15)
		inset, 0 8px 0 0 #AA0000, 0 8px 8px 1px rgba(0, 0, 0, 0.5);
	background-color: #D73814;
}

.btn3d.btn-danger:active, .btn3d.btn-danger.active {
	box-shadow: 0 0 0 1px #b93802 inset, 0 0 0 1px rgba(255, 255, 255, 0.15)
		inset, 0 1px 3px 1px rgba(0, 0, 0, 0.3);
	background-color: #D73814;
}

.btn3d.btn-magick {
	color: #fff;
	box-shadow: 0 0 0 1px #9a00cd inset, 0 0 0 2px rgba(255, 255, 255, 0.15)
		inset, 0 8px 0 0 #9823d5, 0 8px 8px 1px rgba(0, 0, 0, 0.5);
	background-color: #bb39d7;
}

.btn3d.btn-magick:active, .btn3d.btn-magick.active {
	box-shadow: 0 0 0 1px #9a00cd inset, 0 0 0 1px rgba(255, 255, 255, 0.15)
		inset, 0 1px 3px 1px rgba(0, 0, 0, 0.3);
	background-color: #bb39d7;
}

#snoAlertBox {
	position: absolute;
	z-index: 1400;
	top: 2%;
	right: 4%;
	margin: 0px auto;
	text-align: center;
	display: none;
}

#snoAlertBoxError {
	position: absolute;
	z-index: 1400;
	top: 2%;
	right: 4%;
	margin: 0px auto;
	text-align: center;
	display: none;
}

#snoAlertBoxInfo {
	position: absolute;
	z-index: 1400;
	top: 2%;
	right: 4%;
	margin: 0px auto;
	text-align: center;
	display: none;
}
#snoAlertBoxWarn {
	position: absolute;
	z-index: 1400;
	top: 2%;
	right: 4%;
	margin: 0px auto;
	text-align: center;
	display: none;
}

#snoAlertBoxWarnServer {
	position: absolute;
	z-index: 1400;
	top: 40%;
	right: 25%;
	margin: 0px auto;
	text-align: center;
	display: none;
}

#snoAlertBoxWarnLogin {
	position: absolute;
	z-index: 1400;
	top: 40%;
	right: 25%;
	margin: 0px auto;
	text-align: center;
	display: none;
}


.popover {
    z-index: 9900 !important;
}

hr {
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
}

.date-input{
line-height: 22px !important;
}
.btn-ajuste{
margin-bottom: 3px !important;

}

.avisos{
    position:absolute;
    z-index:1400;
   top:90%;
    right:50%;
    margin:0px auto;
	text-align:center;
    display:none;
}

</style>
<%
		
		if (PortalUtil.getCurrentUser() == null) {
			out.print("Erro - Acesso Negado");
			return;
		 }
		 String remoteIP = request.getRemoteAddr();
		 System.out.println(remoteIP);
		 String liberaAcesso = request.getParameter("liberaAcesso");
		 
		 boolean accessGranted = MapaSecurityAcess.verificaAcessoUrl(remoteIP, liberaAcesso);		

		EntityWrapper ewUser = new EntityWrapper(PortalUtil.getCurrentUser());
 		String extensionNumber = (String)ewUser.findField("ramal").getValue();
        List<NeoObject> ramalList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMFilaRamais"), new QLEqualsFilter("ramal", extensionNumber), 0, 1, "");
%>
<script type="text/javascript">

		 var accessGranted = <%=accessGranted%>;
         var extensionNumber = '<%=extensionNumber%>';
         var user = '<%=PortalUtil.getCurrentUser().getFullName()%>';
         var ramalList = '<%=ramalList.size()%>';
         var timeout;
         var historico = 0;
         var cdCliente;
         var timer;
         var timerReload;
         var codigoEvento;
         var codigoHistorico;
         var callOk;
         var callAt;
         var startTime;
         var pushStreamId = "";
         var endTime;
         var timerAux;
         var flag = false;
         var flagVideo = false;
         var cameraPrincipal;
         var dataInicial;
         var dataModificacao;
         var codigoClienteAux;
         var obsTempAux;
         var obsProvidenciaAux;
         var flagVar;
         var timerVar;
         var cam;
         //http://192.168.20.227/ https://192.168.20.34/"
         var baseUrl = "https://dguard.orsegups.com.br/";
         var username = "admin";
         var password = "seventh";
         var sessionId = "";
		 var camInicial;
         var cameraIds;
         var searching = false;
         var searchingNew = false;
         var playing = false;
         var playingLive = false;
         var subtitles;
         var imgs;
         var clocks = [new Date(), new Date(), new Date(), new Date()];
         var statusDiv;
         var sessionDiv;
         var velocidadeImg;
         var velocidadeImgNew;
         var dataVar;
		 var horaVar;
		 var t;
         var timer_is_on = 0;
         var tAux;
         var timer_is_onAux = 0;
		 var baseUrlIcon =  "http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/cm/imagens/camera-motion_not_found.png";
		 var iniciar = true;
		 var alteracao;
		 
		 function initialize() 
         {
         	velocidadeImg = 100;
			var amq = org.activemq.Amq;
         	  amq.init({ 
         	    uri: '<%=PortalUtil.getBaseURL()%>amq',
         		logging : true,
         		timeout: 20, 
         		clientId:(new Date()).getTime().toString() 
         	});
         	 
         	amq.addListener('callCenterListener',"topic://alertEventVideoTopic" ,myHandler.rcvMessage, { selector:"identifier='"+extensionNumber+"'" });
         	
         	//startCount();
         	this.loadData();  
         	startCount();
         	
         
         }
		 	 
         // ADICIONADO NOVOS METODOS
		 $(document).ready(function() {
             if (accessGranted) {
            	if(ramalList == null || ramalList == 0)
              	{
              		$("#pnDetalhes").panel({
              			title: "<img width=16px; height=16px; src='images/gyrophare.gif'/>&nbspEventos Monitoramento",
              			content: "O seu ramal cadastrado n�o foi encontrado!"
              		});
            
              	}
              	else
              	{
              		initialize();	
              		//startSubstituicao();
              	}
             } else {
            	 alert("Erro - Acesso Negado");
             } 
        	
             var interval = 1000;
			
				setInterval(function(){
				var horaCamera = $('#statusDiv').text();
				var horaCamera = horaCamera.trim();
				
				var dataHora = horaCamera.split(" ");
				
				var data = dataHora[0];
				
				var slitData = data.split("/");
				
				var dataFormatada = slitData[2]+"-"+slitData[1]+"-"+slitData[0];
								
				$('#datePicker').attr("value",dataFormatada);				
				//var time = moment().format('HH:mm:ss');
		
				if (iniciar){
					$('#timePicker').attr("value",dataHora[1]);
					$("#timePicker").val(dataHora[1]);
				}
				
				}, interval);
             
             
         });
         
		function startSubstituicao(){
				
				var substituicao = setInterval(function(){
                    console.log('Executando substitui��o!');
                	$.ajax({
                        type: "GET",
                        url: "<%=PortalUtil.getBaseURL()%>services/eventos/substituirEventosPorXVID"
                    });
                },2000);
				
		}
         
		 function pausar(){
				if (iniciar){
					iniciar = false;
					$("#spanStartStop").attr("class","glyphicon glyphicon-refresh");					
				}else{
					iniciar = true;
					$("#spanStartStop").attr("class","glyphicon glyphicon-time");
				}
		}
         
		 function registrarImagens(){
             
			 var data = document.getElementById("datePicker").value;
			 		 
			 var hora = document.getElementById("timePicker").value;
			 
             $.ajax({
             	method: 'GET',
                 url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet",
                 data:'action=registrarImagens&cdHistorico='+historico+'&data='+data+'&hora='+hora,                 
                 success: function(result){
                	 showMessage(0);                    		
                 },  
        			error: function(e){  
        				showMessage(1);
       			}
             });
             console.log("teste");
//              console.log('Iniciou grava��o');
//              document.getElementById('btnRegistrarImagem').disabled=true;
// 			$.ajax({ 
// 			    type: 'POST', 
// 			    url: 'https://apps.orsegups.com.br:8080/AtendimentoEventosServices/seventh/salvarImagensAoVivo/'+cdCliente+'/'+historico,  
// 			    headers: {'x-auth-token' : '682ee2548d4a73d8fadf72f32c52df790eb72aff'},
// 			    dataType: 'json',
// 			    success: function (resultado) {
// 			    	console.log('Gravou');
// 			    	document.getElementById('btnRegistrarImagem').disabled=false;
// 			    	if (resultado != null){
// 			    		console.log(resultado);
// 			    		showMessage(0);
// 			    	}		    			             
// 			    },
// 			    error: function(resultado){
// 			    	document.getElementById('btnRegistrarImagem').disabled=false;
// 			    	console.log('N�o gravou');
// 			    	showMessage(1);
// 			    	console.log(resultado);
// 			    }
// 			});
             	
         }
		 
		 function showMessage(codigo) {
			if (codigo == 0){
				$("#avisoSucesso").fadeIn();
			}else{
				$("#avisoErro").fadeIn();
			} 
			
			closeMessage(codigo);
		};
			  
		function closeMessage(codigo){
			window.setTimeout(function () {
			  if (codigo == 0){
				 $("#avisoSucesso").fadeOut(300)
			  }else{
				  $("#avisoErro").fadeOut(300)
			  }	
			}, 3000);
		} 
		      
         $(function() {
 			$('[data-toggle="tooltip"]').tooltip({
 				  trigger: 'hover',
 				 'placement': 'bottom',
 				  container: 'body' 
 			});
 		 });

 		 $(function() {
 			$('[data-toggle="popover"]').popover({
 			    trigger: 'hover',
 			    'placement': 'bottom',
 			     container: 'body' 
 			});
 		 });

 		 $(function() {
 			$('.dropdown-toggle').dropdown();
 		 });
         
         
 		 function showAtualizaCamera(id,cameraStr)
		 {
			if(id != null && cameraStr != null)
			{
		    var image = document.getElementById(id);
			if(cameraStr == 'cameraPrincial')
				cameraStr = cameraPrincial;
			
			image.src = "<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/seventh/mjpegStreamDGuard.jsp?camera="+cameraStr+"&a=" + new Date().getTime();
			}
			else
			{
				alert(id +" - "+cameraStr);
				
			}
			
		 }
		
		
		 
		 function timedCount() {
             console.log('Execuntando reloadData');
             var ramal = extensionNumber;
          	 $.ajax({
          		  type: "GET",
          		  url: "<%=PortalUtil.getBaseURL()%>services/eventosVideo/verificarLigacaoPendente/"+ramal
             });
             t = setTimeout(function(){ timedCount() }, 1000);
             
         }

         function startCount() {
             if (!timer_is_on) {
                 timer_is_on = 1;
                 timedCount();
             }
         }

         function stopCount() {
             clearTimeout(t);
             timer_is_on = 0;
         }
         
         function timedCountAux() {
             console.log('Execuntando reloadData aux');
             var ramal = extensionNumber;
          	 $.ajax({
          		  type: "GET",
          		  url: "<%=PortalUtil.getBaseURL()%>services/eventosVideo/verificarLigacaoPendenteHistorico/"+ramal
             });
             tAux = setTimeout(function(){ timedCountAux() }, 1000);
             
         }

         function startCountAux() {
             if (!timer_is_onAux) {
                 timer_is_onAux = 1;
                 timedCountAux();
             }
         }

         function stopCountAux() {
             clearTimeout(tAux);
             timer_is_onAux = 0;
         }
         
         function loadData()
         {
         	console.log('Buscando eventos!');
         	$.ajax({
         		type: "GET",
         		url: "<%=PortalUtil.getBaseURL()%>services/eventosVideo/verificaEventos"
         	});
         	
         	var load = 4000;
         	timeout = setTimeout("loadData()",load);
         }
         
         function reloadData(){
         	var ramal = extensionNumber;
         	$.ajax({
         		  type: "GET",
         		  url: "<%=PortalUtil.getBaseURL()%>services/eventosVideo/verificarLigacaoPendente/"+ramal
         	});
         	
         }
         
         function reloadSemEvento() {
             timerReload = setTimeout(reloadData(), 3000);
         }
         
         function cancelaReloadSemEvento() {
             clearTimeout(timerReload);
         }
         
         
         var myHandler = {
         	rcvMessage : function(message) {
         		var decodedMessage = decodeURIComponent(message.data.replace(/\+/g,  " "));
         		statusDiv = document.getElementById("statusDiv");
         		if(decodedMessage != null && decodedMessage != "")
         		{
         			
         			var alertVO = JSON.parse(decodedMessage);
         			
         			if(alertVO == null)
         			{
         				stopCountAux();
         				cleanSigmaData(); 
         				location.reload();
         				alertSucess();
         			}
         			else if(alertVO.type == "CallAlertEventVideoVO" )
         			{
         			
         				
         				$("#pnDetalhes").panel({
         					title: "<span class='glyphicon glyphicon-camera' aria-hidden='true' ></span> &nbsp <span id=\"codigoCentralTitulo\" class=\"label label-danger\"  style=\"font-family: verdana; font-size: 11px; font-weight: bold;\">",
							content: alertVO.infoWindow 
         				});
         				codigoEvento = alertVO.callEventoVO.codigoEvento;
         				if(historico != alertVO.callEventoVO.codigoHistorico){
	         				historico = alertVO.callEventoVO.codigoHistorico;
	         				cdCliente = alertVO.callEventoVO.codigoCliente;
	         				clearInterval(timer);
	         				clearInterval(timerAux);
	         				clearInterval(timerVar);
	         				flag = false;
	         				flagVar = false
							
	         				var date = alertVO.callEventoVO.dataRecebimento;
	         				var datearray = date.split("/");
	         				var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
	         				CountDownTimer(newdate,'clock');
	         				
	         				//scrollSlider(alertVO.minutosEvento);
         				}
         				tooltipInfo();
						stopCount();
						
						//NOVOS PARAMETROS REFERENTES IP DO D-GUARD
						
						
						var servidorCFTV = alertVO.callEventoVO.servidorCFTV;
						
						console.log('SERVIDOR CFTV: '+servidorCFTV);
						
						if (servidorCFTV == "192.168.20.216"){
							servidorCFTV = "monitoramento216.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento216.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV == "172.10.0.4"){
							servidorCFTV = "monitoramento4.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento4.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV == "172.10.0.5"){
							servidorCFTV = "monitoramento5.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento5.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV == "172.10.0.6"){
							servidorCFTV = "monitoramento6.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento6.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV == "172.10.0.7"){
							servidorCFTV = "monitoramento7.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento7.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV == "172.10.0.8"){
							servidorCFTV = "monitoramento8.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento8.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV.trim() === "172.10.0.12"){
							servidorCFTV = "monitoramento12.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento12.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV.trim() === "172.10.0.15"){
							servidorCFTV = "monitoramento15.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento15.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV.trim() === "172.10.0.17"){
							servidorCFTV = "monitoramento17.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento17.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}else  if (servidorCFTV.trim() === "172.10.0.18"){
							servidorCFTV = "monitoramento18.orsegups.com.br";
							alertVO.callEventoVO.servidorCFTV = "monitoramento18.orsegups.com.br";
							alertVO.callEventoVO.portaCFTV = 443;
						}
						
						
						if (servidorCFTV == "192.168.20.227" || servidorCFTV == "192.168.0.250" || servidorCFTV == "192.168.20.205" || servidorCFTV == "192.168.254.92" ){
							alertWarnServer();
						}
						
						if (servidorCFTV.length < 1){
							console.log('Cadastrado de dados servidor CFTV Sigma vazio!');
						}
						
						
						
						var prefixo = "http://";
						
						if (alertVO.callEventoVO.portaCFTV == 443){
							prefixo = "https://";
						}
						
						baseUrl = prefixo+servidorCFTV+':'+alertVO.callEventoVO.portaCFTV+'/';
        				username = alertVO.callEventoVO.usuarioCFTV;
         				password = alertVO.callEventoVO.senhaCFTV;
						
						//FIM NOVOS PARAMETROS
						
						//startCountAux();
         				//DADOS GERAIS DO CLIENTE
         				$("#codigoCliente").html(alertVO.callEventoVO.codigoCliente);
         				//$("#logCliente").text(alertVO.callEventoVO.observacaoFechamento);
         				
         				$("#codigoEmpresa").html(alertVO.callEventoVO.empresa +" - "+alertVO.callEventoVO.nomeEmpresa);
         				$("#razaoSocialSigma").html(alertVO.callEventoVO.razao);
         				$("#fantasiaSigma").html(alertVO.callEventoVO.fantasia);
         				
         				$("#cnpjCpfSigma").html(alertVO.callEventoVO.cgccpf);
         				
         				//DADOS CONTA/PARTICAO
         				$("#codigoCentral").html(alertVO.callEventoVO.codigoCentral+ " ["+alertVO.callEventoVO.particao+"]");
						$("#codigoCentralTitulo").html("Monitoramento de imagens. Conta - "+alertVO.callEventoVO.codigoCentral+ " ["+alertVO.callEventoVO.particao+"] - Evento - "+alertVO.callEventoVO.codigoEvento+" - Data Recebido - "+alertVO.callEventoVO.dataRecebimento);

         				$("#rota").html(alertVO.callEventoVO.rota);
         				$("#logradouro").html(alertVO.callEventoVO.endereco);
         				$("#bairroSigma").html(alertVO.callEventoVO.Bairro);
         				$("#cidadeSigma").html(alertVO.callEventoVO.nomeCidade);
         				$("#ufSigma").html(alertVO.callEventoVO.uf);
         				$("#cepSigma").html(alertVO.callEventoVO.cep);
         				//$("#cepSigma").mask('99999-999');
         				
         				//CONTATOS - TELEFONES/E-MAIL
         				
         				$("#telefoneCentral1").html(alertVO.callEventoVO.telefone1);
         				//$("#telefoneCentral1").mask('(99) 9999-9999');
         				$("#telefoneCentral2").html(alertVO.callEventoVO.telefone2);
         				//$("#telefoneCentral2").mask('(99) 9999-9999');
         				$("#emailCentral").html(alertVO.callEventoVO.emailResponsavel);
         				$("#responsavelCentral").html(alertVO.callEventoVO.responsavel);
         				
         				//PALAVRA CHAVE
         				$("#pergunta").html(alertVO.callEventoVO.pergunta);
         				$("#resposta").html(alertVO.callEventoVO.resposta);
         				$("#coacao").html(alertVO.callEventoVO.senhaCoacao);
         				
         				codigoClienteAux = alertVO.callEventoVO.codigoCliente;
         				showAtualizaProvidencias(alertVO.callEventoVO.codigoCliente);
         				//ANOTACAO
         				obsTempAux = alertVO.callEventoVO.obsTemp;
         				obsProvidenciaAux = alertVO.callEventoVO.obsProvidencia;
         				$("#taObsTemp").html(obsTempAux);
         				$("#taObsProvidencia").html(obsProvidenciaAux);
         				startTime = alertVO.dataEventoVideo;
         				dataModificacao = alertVO.callEventoVO.dataRecebimento;
         				
         				var idStart = alertVO.ultimaCameraVisualizada;
         				cameraPrincipal = idStart;
						camInicial = idStart;
         				var camerasClienteVar = alertVO.camerasCliente;
         				console.log(alertVO);
         				console.log(alertVO.camerasCliente);
         				if (camerasClienteVar){
         					cam = camerasClienteVar.split(";");
         				}
//          				else{
//          					alertWarnLogin();
//          				}
     					cameraIds = cam;
         				dataVar =  alertVO.dataVideo;
         				horaVar =  alertVO.horaVideo;
         				
         				sessionDiv = document.getElementById("sessionDiv");
         				
//          				statusDiv = document.getElementById("statusDiv");
         				
         				statusDiv.innerHTML = alertVO.callEventoVO.dataRecebimento;
         				login(dataVar, horaVar);
         				//showCamerasPopUp();
         				         				
//          			    var c = Number(cdCliente) + 20160819;
         			    
//          			    if (alertVO.callEventoVO.codigoEvento == "XPLE"){
// 	         			    console.log('Open window XPLE!');
	         			    		
// 							window.open('https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/monitoramentoImagens/MonitoramentoImagensLive.html#'+c, 'TelaAuxiliar')
	         				         			    	
//          			    }
         			    
         				

         			}
         			else
         			{
         				alert("Entrou senao 0");
         				
         			}
         			
         		}
         		else
         		{
         			
         			alert("Entrou senao 1");
         			
         		}
         	}
         };
         
         function tooltipInfo()
         {
	         
	        $('#toInfo').tooltip({
	                 position: 'right',
	                 content: '<span style="color:#fff">This is the tooltip message.</span>',
	                 onShow: function(){
	                     $(this).tooltip('tip').css({
	                         backgroundColor: '#666',
	                         borderColor: '#666'
	                     });
	              }
	        });
         }
         
         function adicionaLog(textoLog)
         {
           	
	           	var dt = new Date(); 
	           	var usuario = user;
	           	var diaLog =  dt.getDate();
	           	var mesLog =  dt.getMonth()+1;
	           	var horaLog = dt.getHours();
	           	var minutoLog = dt.getMinutes(); 
	           	var segundoLog = dt.getSeconds();
	           	
	           	
	            if (diaLog.toString().length == 1) {
	            	diaLog = "0" + diaLog;
	            }
	            if (mesLog.toString().length == 1) {
	            	mesLog = "0" + mesLog;
	            }
	            if (horaLog.toString().length == 1) {
	            	horaLog = "0" + horaLog;
	            }
	            if (minutoLog.toString().length == 1) {
	            	minutoLog = "0" + minutoLog;
	            }
	            if (segundoLog.toString().length == 1) {
	            	segundoLog = "0" + segundoLog;
	            }
	            
	           	var dataStr = diaLog + "/" + mesLog + "/" + dt.getFullYear() + " " + horaLog + ":" + minutoLog + ":" + segundoLog;
	           	if(textoLog != null && usuario != null && dataStr != null)
	           	{
	           		//$("#taLog").val('');
	           		document.getElementById("taLog").value += dataStr+ ": Op. CM - "+ usuario + " - " + textoLog +"; \r\n";
	           	}
          }
         
          function cleanSigmaData()
          {   
         	//DADOS GERAIS DO CLIENTE
         	$("#codigoCliente").html("");
         	$("#logCliente").html("");
         	$("#codigoEmpresa").html("");
         	$("#razaoSocialSigma").html("");
         	$("#cnpjCpfSigma").html("");
         	
         	//DADOS CONTA/PARTICAO
         	$("#codigoCentral").html("");
         	$("#particao").html("");
         	$("#rota").html("");
         	$("#logradouro").html("");
         	$("#bairroSigma").html("");
         	$("#cidadeSigma").html("");
         	$("#ufSigma").html("");
         	$("#cepSigma").html("");
         	
         	//CONTATOS - TELEFONES/E-MAIL
         	$("#telefoneCentral1").html("");
         	$("#telefoneCentral2").html("");
         	$("#emailCentral").html("");
         	$("#responsavelCentral").html("");
         	
         	//PALAVRA CHAVE
         	$("#pergunta").html("");
         	$("#resposta").html("");
         	
         	//PROVIDENCIA
//          	$('#dgProvidencias').datagrid('loadData', new Array());
//          	$('#dgHistorico').datagrid('loadData', new Array());
//          	$('#dgUsuarios').datagrid('loadData', new Array());
         	//ANOTACAO
         	$("#taObsTemp").html("");
         	$("#taObsProvidencia").html("");
         	
//          	$("#dtDataEvento").datebox('setValue', "");
//          	$('#nbHoraEvento').numberspinner('setValue', "");
//          	$('#nbMinEvento').numberspinner('setValue', "");
          }
         
          function CountDownTimer(dt, id)
          {
             var end = new Date(dt);
             var _second = 1000;
             var _minute = _second * 60;
             var _hour = _minute * 60;
             var _day = _hour * 24;
             
      
             function showRemaining() {
                 var now = new Date();
                 var distance =  now - end;
                 if (distance < 0) {
      
                     clearInterval(timer);
                     document.getElementById(id).innerHTML = 'ERRO!'; 
      
                     return;
                 }
                 var days = Math.floor(distance / _day);
                 var hours = Math.floor((distance % _day) / _hour);
                 var minutes = Math.floor((distance % _hour) / _minute);
                 var seconds = Math.floor((distance % _minute) / _second);
      
                 if(days != null)
                 document.getElementById(id).innerHTML = days + 'd ';
                 if(hours != null)
                 document.getElementById(id).innerHTML += hours + 'h ';
                 if(minutes != null)
                 document.getElementById(id).innerHTML += minutes + 'm ';
                 if(seconds != null)
                 document.getElementById(id).innerHTML += seconds + 's';
             }
      
             timer = setInterval(showRemaining, 1000);
          }
         
          function dial(numeroExterno) 
          {
          	
      		var codHistorico = historico;
      		var cliente = cdCliente;
      		var numeroInterno = extensionNumber;
      	 	$.ajax({
         		type: "GET",
         		url: "<%=PortalUtil.getBaseURL()%>services/eventosVideo/getLigacaoSnep/"+numeroInterno+"/"+numeroExterno+"/"+codHistorico+"/"+cliente,
         			success: function(result){
         				alertInfo();
                     },  
                     error: function(e){  
                    	 alertError();
               		 }
         	});
      		
          }
         
    	  function showAtualizaProvidencias(cdCliente){
    		 $('#contatos').html("<div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width:100%'>Aguarde...</div></div>");
    		 $.ajax({
   	           type:"GET",
   	           url:"<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet",
   	           data:'action=getDadosProvidencia&cdCliente='+cdCliente,
   	           dataType:'text',
   	           success: function(result) {
   	        	 
   	        	  $("#contatos").html(result);
   	        	  $(function () {
   	        		    $('#contatos a:last').tab('show');
   	        		});
   	        		$(function() {
   	        			$('[data-toggle="tooltip"]').tooltip({
   	        			    'placement': 'bottom',
   	        			 	 container: 'body' 
   	        			});
   	        		});

   	        		$(function() {
   	        			$('[data-toggle="popover"]').popover({
   	        			    trigger: 'hover',
   	        			        'placement': 'bottom',
   	        			         container: 'body' 
   	        			});
   	        		});
   	        		$(function() {
   	        			$('.dropdown-toggle').dropdown();
   	        		});
   	        		var valor = 1;
   	        		showHistoricoEventos(cdCliente,valor);
   	           },  
      			error: function(e){  
      				alertError();
          	 }
   	     	});
   		  }	
    	 
    	  function showHistoricoEventos(cdCliente,valor)
    	  {
          
                     $.ajax({
                     	method: 'GET',
                         url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet",
                         data:'action=getAtualizaEventos&cdCliente='+cdCliente+'&valor='+valor,
                         dataType:'text',
                         success: function(result){
                         	$('#dgHistorico').html(result);
                         	$(function() {
        	        			$('[data-toggle="tooltip"]').tooltip({
        	        			    'placement': 'bottom',
        	        			     container: 'body' 
        	        			});
        	        		});

        	        		$(function() {
        	        			$('[data-toggle="popover"]').popover({
        	        			    trigger: 'hover',
        	        			        'placement': 'bottom',
        	        			        container: 'body' 
        	        			});
        	        		});
        	        		$(function() {
        	        			$('.dropdown-toggle').dropdown();
        	        		});
        	        		showUsuarioAcesso(cdCliente);
                         },  
                			error: function(e){  
                				alertError();
               			}
                     });
            }
         
	    	function showUsuarioAcesso(cdCliente)
	    	{
	             
	             $.ajax({
	             	method: 'GET',
	                 url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet",
	                 data:'action=getDadosAcessoCliente&cdCliente='+cdCliente,
	                 dataType:'text',
	                 success: function(result){
	                 	$('#dgUsuarios').html(result);
	                 	$(function() {
		        			$('[data-toggle="tooltip"]').tooltip({
		        			    'placement': 'bottom',
		        			     container: 'body' 
		        			});
		        		});
	
		        		$(function() {
		        			$('[data-toggle="popover"]').popover({
		        			    trigger: 'hover',
		        			        'placement': 'bottom',
		        			         container: 'body' 
		        			});
		        		});
		        		$(function() {
		        			$('.dropdown-toggle').dropdown();
		        		});
		        		alertSucess();
		        		if(obsTempAux != null)
		        		{
		        			document.getElementById("taObsTemp").innerHTML = obsTempAux;
		        		}
		        		
		        		if(obsProvidenciaAux != null)
		        		{	
		        			document.getElementById("taObsProvidencia").innerHTML = obsTempAux;
		        		}
		        	
	                 },  
	        			error: function(e){  
	        				 alertError();
	       			}
	             });
	 	   }
    	 
	       function eventCallHandler(codigoHistorico, callOk, callAt, alteracao)
	       {
	         	
	         	 this.codigoHistorico = codigoHistorico;
	          	 this.callOk = callOk;
	          	 this.callAt = callAt; 
	          	 var ramal = extensionNumber;
	          	 this.alteracao = alteracao;
	             
	          	 var texto = document.getElementById("taLog").value;
			     var resultado = 0;
			     if(texto != null && texto != '')
				 {
					salvaLogEventoPopUp();
			  	    $.ajax({
		         	type: "GET",
		         	url: "<%=PortalUtil.getBaseURL()%>services/eventosVideo/contatoCliente/"+codigoHistorico+"/"+callOk+"/"+callAt+"/"+ramal+"/"+alteracao,
		         		success: function(result){
		         		alertSucess();	
		         		startCount();
		                },  
		                error: function(e){  
		                alertError();
		                }
		         	});
				 }
				 else
				 {
				    alert("Para fechar o evento insira o log sobre o acompanhamento!")
				 }
	
	       } 
    	 	
	       
	       function abrirOS(codigoCliente){
	    	   
		   		var startProcessUrl = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterStartProcessServlet?processType=OsSigma&codigoCliente="+codigoCliente;
				
				var taskUrl;
				
				sXmlHttp = new XMLHttpRequest();
				
				<%-- Evita cache de requisi��o http --%>	
				url = safeURL(startProcessUrl);	
					
				sXmlHttp.open("get", url, false);
			 	sXmlHttp.send(null);
			 	taskUrl = sXmlHttp.responseText;
				
				console.log('Foi');
				
				if(taskUrl != null)	{
					var result = taskUrl.split(";");
					
					if(result[0] == "1"){
						window.open(result[1], 'callcenterProcess');
					}
					else{
						alert(result[1]);
					}
				}
	       }
	       
	       function loadImage()
	       {
	    		alert("Image is loaded");
	       }
	    	 
	       function closeSnoAlertBox()
	       {
	    		window.setTimeout(function () {
	    		$("#snoAlertBox").fadeOut(600);
	    		}, 3000);
	       }  
	    		
	       function alertSucess()
	       {
	    			
	    		$("#snoAlertBox").fadeIn();
	    	    closeSnoAlertBox();
	       }
	    		
	       function closeSnoAlertBoxError()
	       {
	    		window.setTimeout(function () {
	    		$("#snoAlertBoxError").fadeOut(600);
	    		}, 3000);
	       }  
	    		
	       function alertError()
	       {
	    		$("#snoAlertBoxError").fadeIn();
	    		closeSnoAlertBoxError();
	       }
	    		
	       function closeSnoAlertBoxInfo()
	       {
	    		window.setTimeout(function () {
	    		$("#snoAlertBoxInfo").fadeOut(600);
	    		}, 3000);
	       }  
	    		
	       function alertInfo()
	       {
	    		$("#snoAlertBoxInfo").fadeIn();
	    		closeSnoAlertBoxInfo();
	       }
			
	       function closeSnoAlertBoxWarn()
	       {
	    		window.setTimeout(function () {
	    		$("#snoAlertBoxWarn").fadeOut(600);
	    		}, 3000);
	       }  
	    		
	       function alertWarn()
	       {
	    		$("#snoAlertBoxWarn").fadeIn();
	    		closeSnoAlertBoxWarn();
	       }
	       
	       function alertWarnServer()
	       {
	    		$("#snoAlertBoxWarnServer").fadeIn();
	    		//closeSnoAlertBoxWarnServer();
	       }
	       
	       function closeSnoAlertBoxWarnServer()
	       {
	    		window.setTimeout(function () {
	    		$("#snoAlertBoxWarnServer").fadeOut(600);
	    		}, 15000);
	       }  
	       
	       
	       function alertWarnLogin()
	       {
	    		$("#snoAlertBoxWarnLogin").fadeIn();
				//closeSnoAlertBoxWarnLogin();
	       }
	       
	       function closeSnoAlertBoxWarnLogin()
	       {
	    		window.setTimeout(function () {
	    		$("#snoAlertBoxWarnLogin").fadeOut(600);
	    		}, 15000);
	       } 
	        
	       function getTime(hours, minutes) 
	       {
	            var time = null;
	            minutes = minutes + "";
	            if (hours < 12) {
	                time = "min";
	            }else {
	                time = "min";
	            }
	            if (hours == 0) {
	                hours = 00;
	                time = "s";
	            }
	            if (hours > 12) {
	                hours = hours;
	            }
	            if (minutes.length == 1) {
	                minutes = "0" + minutes;
	            }
	            return hours + ":" + minutes + " " + time;
	       }
      
        
	       function dataAtualFormatada(dataAux)
	       {
	        	if(dataAux != null)
	        	{
	        		console.log('Data aux '+dataAux);	
		        	var data = new Date(dataAux);
		        	
		        	//TODO REMOVER APOS HORARIO DE VERAO
		        	
 		        	data.setHours(data.getHours() - 1);
		        	
		        	console.log('Data from aux '+data);
		            var dia = data.getDate();
		            if (dia.toString().length == 1)
		              dia = "0"+dia;
		            var mes = data.getMonth()+1;
		            if (mes.toString().length == 1)
		              mes = "0"+mes;
		            var ano = data.getFullYear();
		            
		            horas = formata_dois_digitos(data.getHours());
		            minutos = formata_dois_digitos(data.getMinutes());
		            segundos = formata_dois_digitos(data.getSeconds());
		            dataInicial = dia+"/"+mes+"/"+ano +" "+ horas + ":" + minutos + ":" + segundos
		            
		            console.log('data atual formatada '+dataInicial);
		            
		            return dataInicial;
	              
	        	}
	       }
	        
	       function dataFormatadaJavaScript(dataAux)
	       {
	        	if(dataAux != null)
	        	{
	        		console.log('data aux2 '+dataAux);	
		        	var data = new Date(dataAux);
		        	
		        	
	        	//TODO REMOVER APOS HORARIO DE VERAO
		        	
		        	data.setHours(data.getHours() - 1);
		        	
		        	
		        	console.log('data aux 2 new '+data);
		            var dia = data.getDate();
		            if (dia.toString().length == 1)
		              dia = "0"+dia;
		            var mes = data.getMonth()+1;
		            if (mes.toString().length == 1)
		              mes = "0"+mes;
		            var ano = data.getFullYear();
		            
		            horas = formata_dois_digitos(data.getHours());
		            minutos = formata_dois_digitos(data.getMinutes());
		            segundos = formata_dois_digitos(data.getSeconds());
		            dataInicial = mes+"/"+dia+"/"+ano +" "+ horas + ":" + minutos + ":" + segundos
		            
		            return dataInicial;
	              
	        	}
	        }
	       
	        function formata_dois_digitos(n) 
	        {
	            return n < 10 ? '0' + n : n;
	        }
	        
	        $(function() {
		        $("#slider").mousemove( function(e){
		            $("#slider-val").html($(this).val());
		            showData($(this).val());
		        });
	        });
	        
	        $(function() {
		        $("#slider").change( function(e){
		            $("#slider-val").html($(this).val());
		            showData($(this).val());
		        });
	        });
	        
	        
	        function comparaDatas(dataLast, dataAnt)
	        {	
	        	
	        	
	        	var array = dataAnt.split("/");
	        	
	        	dataAnt = array[1]+"/"+array[0]+"/"+array[2];
	        	
	        	console.log('dataLast '+dataLast);
	        	console.log('dataAnt '+dataAnt);
		        var data_1 = new Date(dataAnt);
		        var data_2 = new Date(dataLast);
		        console.log('Data 1 '+data_1+ 'Data 2 '+data_2);
		        if (data_1 < data_2) {
		            console.log("Data n�o pode ser maior que a data final");
		            return false;
		        } else {
		            return true
		        }
	        }
	        
         	function showCamerasPopUp() 
        	{
         		
         		var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/cm/consCamerasAoVivo.jsp?cameras='+cam;
        		
        		$('#modalInfo').window('open');
        		$('#modalInfo').window('refresh', caminho);

        	}
         	
         	function formatA(value,row,index)
            {
                var format = '<span data-p1='+index+' class="easyui-tooltip"> <img src=\"images/alert.png\"/> </span>';
                return format;	 
            }
             
         	 
         	function atualizaContatos(codigoCliente, codigoHistorico)
            {	
            	
         		historicoTabCdCliente = codigoCliente;
            	historicoTab = codigoHistorico;
            	newUser();
            	if(codigoCliente != null)
            	{
                       $.ajax({
                       	method: 'post',
                           url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet?action=atualizaContatos&cdCliente='+codigoCliente+'&cdHistorico='+codigoHistorico,
                           dataType: 'json',
                           success: function(result){
                           
                           	$('#dgProvidencia').datagrid('loadData', result);
                           },  
                  		   error: function(e){  
                  				alert(e);
                 		   }
                       });
            	}
            }
        	
        	function atualizaHisotircoEventos()
            {
            	$('#dgHistorico').datagrid('loadData', new Array());
            	var cdHistorico = historicoTabCdCliente;
            		var valor = $('#cbEvento').combobox('getValue');
                       $.ajax({
                       	method: 'post',
                           url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet?action=atualizaHistoricoEventos&historico='+cdHistorico+'&dias='+valor,
                           dataType: 'json',
                           success: function(result){
                           	refresh();
                           	$('#dgHistorico').datagrid('loadData', result);
                           	  createTooltipEvento();
                           },  
                  		   error: function(e){  
                  				alert(e);
                 		   }
                       });
            }
         	 
        	function newUser()
            {      
            	$('#dlg').dialog('open').dialog('setTitle','Dados do cliente');
            	$('#dgProvidencia').datagrid('loadData', new Array());
            	$('#dgHistorico').datagrid('loadData', new Array());
                $('#fm').form('clear'); 
            }
        	
        	function refresh()
            {
        		alertSucess();
            }
        	
        	function limpaLog()
            {
             	document.getElementById("taLog").value = "";
            }
        	 
        	function playCam(id) 
        	{
                  recordingsUrl =
                  	urlServerImg +
                      "/camera.cgi?camera=" +
                      id +
                      "&resolucao=320x240&qualidade=100&formato=jpg&ds="+new Date().getTime();
                 
                  //ajaxRequest(recordingsUrl, id)
                  document.getElementById(id).src = recordingsUrl;
            
            }

			///NOVA IMPLEMENTACAO
            function ajaxRequestXHR(camNumber, camData, camHora) {
				
				
                var xhr = new XMLHttpRequest();
                var camId = camNumber;
                var img = document.getElementById(camNumber);
              
                if ((!playing) && (!searching))
                    return;
                if (camData === '0' && camHora === '0')
                    return;
                // Retrieve a jpeg image using XMLHttpRequest 
                // Based on http://www.html5rocks.com/en/tutorials/file/xhr2/
				
                if (searching)
                {
                	console.log('Searching: '+baseUrl + "player/getimagem.cgi?camera=" + camId +"&resolucao=640x480&qualidade=70&formato=jpg&data="+camData+"&hora="+camHora);
                	xhr.open("GET", baseUrl + "player/getimagem.cgi?camera=" + camId +"&resolucao=640x480&qualidade=70&formato=jpg&data="+camData+"&hora="+camHora, true);
                }
                else
                {
                	console.log('Searching 2: '+baseUrl + "player/getimagem.cgi?camera=" + camId +"&resolucao=640x480&qualidade=70&formato=jpg&data="+camData+"&hora="+camHora);
                    xhr.open("GET", baseUrl + "player/getproxima.cgi?camera=" + camId +"&resolucao=640x480&qualidade=70&formato=jpg", true);
                }
				
                // Ask for the result as an ArrayBuffer.
                xhr.responseType = "arraybuffer";

                // This is the session id (for authentication)
                xhr.setRequestHeader('SessaoId', sessionId);

                xhr.onload = function (e) {
                    // This could be used to parse headers (including image time)
                    // console.log(this.getAllResponseHeaders());

                    if (this.status === 401) {
                    	console.log('Erro ao carregar imagens');
                        statusDiv.innerHTML = "Erro de login.";
                        setTextModalAjuda();
                    	//$('#modalAjuda').modal('show');
                        return;
                    }
                    
                    if (this.status === 404) {
                    	alert( xhr.responseText );
                        return;
                    }
                    console.log("Teste 1 "+status);

                    // This could be used to parse errors from the API. If the content-type is text, treat the response as a text error.
                    var contentType = this.getResponseHeader('content-type');
                    if (contentType.indexOf("text/plain") > -1) {
                        var errorText = String.fromCharCode.apply(null, new Uint8Array(this.response));
                        //statusDiv.innerHTML = errorText;
                        console.log("Teste 2 "+errorText);
                        if(errorText == "FIMDASIMAGENS")
                  	    {
                        	console.log('FIM :'+errorText);
                        	//img.src = baseUrlIcon;
                        	img.alt = errorText;
                        	if(camId == cameraPrincipal)
	                  	    {
                        		stopAllCam();
                            	if(velocidadeImg != null)
                            	window.clearTimeout(velocidadeImg);
                            	if(searchingNew)
                            		searchNewImage();
                            		//velocidadeImgNew = setTimeout(function(){ searchNewImage(); }, 8000);
                            	//searchNewImage();
                            	
                        	}
                        	//searchCamLive();
                  	    }
                        else if(errorText == "FRAMEVAZIO")
                        {
                        	//img.src = baseUrlIcon;
                        	img.alt = errorText;
                        	if(camId == cameraPrincipal)
                        	{
                        	
	                        	stopAllCam();
	                        	if(velocidadeImg != null)
	                        	window.clearTimeout(velocidadeImg);
	                        	if(searchingNew)
	                        		searchNewImage();
	                        		//velocidadeImgNew = setTimeout(function(){ searchNewImage(); }, 8000);
	                    		//searchNewImage();
                    		
							}
                        }
                        else if(errorText == "CAMERAINDISPONIVEL")
                        {
                        	img.src = baseUrlIcon;
                        	img.alt = errorText;
                        }
                        else if(errorText == "NAOHAIMAGENS")
                        {
                        	img.src = baseUrlIcon;
                        	img.alt = errorText;
                        }
                        

                    }
                    else if (contentType.indexOf("image/jpeg") > -1) 
                    {
                            // Obtain image date/time
                            var dateTime = this.getResponseHeader('Last-Modified');
                            
                            console.log('dateTime do response '+dateTime);
                            
                  	        if(dateTime != null)
                  	        {
								console.log(baseUrl + "player/getimagem.cgi?camera=" + camId +"&resolucao=320x240&qualidade=70&formato=jpg&data="+camData+"&hora="+camHora);
	                            // Obtain a blob: URL for the image data.
	                            var arrayBufferView = new Uint8Array(this.response);
	                            var blob = new Blob([arrayBufferView], { type: "image/jpeg" });
	                            var urlCreator = window.URL || window.webkitURL;
	                            var imageUrl = urlCreator.createObjectURL(blob);
	                            img.onload = function() {
	                                var urlCreator = window.URL || window.webkitURL;
	                                urlCreator.revokeObjectURL(this.src); // Free memory
	                            };
	                            
	                            if(statusDiv != null && camId == cameraPrincipal)
		                  	    {
		                  	    	statusDiv.innerHTML = dataAtualFormatada(dateTime);	    	  
		                  	    }
	                            console.log('date time '+dateTime);
	                            if(comparaDatas(dataFormatadaJavaScript(dateTime),statusDiv.innerHTML)) 
	                            {
	                            	img.src = imageUrl;
	                            }
									img.src = imageUrl;
	                            if (playing) 
	                            {
	                                window.setTimeout(function () {
	                                    if ((searching) || (playing))
	                                        ajaxRequestXHR(camNumber, camData, camHora);
	                                }, velocidadeImg);
	                            }
                          }
                    }
                };

                xhr.send();
				
            }
			
            function searchNewImage() 
            {
            	if ((!searching) && (!playing) && (searchingNew))
            	{
	            	alertWarn();
	            	setTimeout(function(){ getNewImage(); }, 2000);
            	}
            }
			
            
            function getNewImage()
            {
            	
            	var result = document.getElementById("statusDiv").innerHTML;
            	var res = result.split(" ");
            	var novaData = res[0].replace("/","-").replace("/","-"); 
            	var novaHora = res[1].replace(":","-").replace(":","-");
            	velocidadeImg = 200;
            	search(novaData, novaHora);
            	playAllCam();
            }
            
            function keepSessionAlive() 
            {
                if (sessionId === "")
                    return;

                console.log("Enviando sessaoId a cada 30 segundos para manter a sess�o aberta");

                var xhr = new XMLHttpRequest();
                console.log('Base: '+baseUrl);
                xhr.open("GET", baseUrl + "servidor.cgi", true);
                xhr.setRequestHeader('SessaoId', sessionId);

                xhr.onload = function (e) {
                    if (this.status === 401) {
                    	
                    	console.log('Erro 401');
                        statusDiv.innerHTML = "Erro de login.";
                        setTextModalAjuda();
                    	//$('#modalAjuda').modal('show');
                        return;
                    }
                }

                xhr.send();
            }

            function login(dataVar, horaVar) 
            {
			
                function makeBaseAuth(user, password) {
                    var tok = user + ':' + password;
                    var hash = btoa(tok);
                    return "Basic " + hash;
                }

                var xhr = new XMLHttpRequest();
                console.log('Base: '+baseUrl);
                console.log('Executou login');
                xhr.open("GET", baseUrl + "servidor.cgi", true);
                console.log('Usuario:'+username+' senha:'+password);
                xhr.setRequestHeader('Authorization', makeBaseAuth(username, password));

                xhr.onload = function (e) {
                    if (this.status === 401) {
                    	console.log('Entrou na tratativa de excess�o');
                    	setTextModalAjuda();
                    	//$('#modalAjuda').modal('show');
                    	console.log('Erro 401');
                        statusDiv.innerHTML = "Erro de login.";
                        return;
                    }

                    if (this.status === 200) {
                        // This could be used to parse headers (including image time)
                        // console.log(this.getAllResponseHeaders());

                        // This could be used to parse errors from the API. If the content-type is text, treat the response as a text error.
                        sessionId = this.getResponseHeader("SessaoId");
                        console.log(sessionId);
                        if (!sessionId){
                        	console.log('Erro no sesson id!');
                        	sessionDiv.innerHTML = "N�o foi poss�vel obter o header SessaoId. Atualize o D-Guard servidor.";
                        }else {
                        	console.log('Erro ao setar sucesso!');
                        	sessionDiv.innerHTML = "Sucesso. SessaoId: " + sessionId;
						
                        	search(dataVar, horaVar);
                            window.setInterval(function () { keepSessionAlive() }, 30000);
                        }
                    }
                };

                xhr.send();

            }
            
            function searchCamLive() 
            {
            	
           	 
           	   if (sessionId === "") {
                   sessionDiv.innerHTML = "Aguardando inicio da sess�o...";
                   return;
               }
           	   
           	   stopAllCam();
           	   window.clearTimeout(velocidadeImg);
               playingLive = true;
               velocidadeImg = 300;
               $('#btnLive').html('<span class=\"glyphicon glyphicon-unchecked\" onclick=\"stopLiveCam()\" style=\"color: white;\"></span>');	
               for (var i=0; i < cam.length; i++) 
        	   {   
            	   camerasAoVivo(cam[i]); 			
        	   }
              
             
            }
            
            function search(camData, camHora) 
            {
            	
           	 
           	   if (sessionId === "") {
                   sessionDiv.innerHTML = "Aguardando inicio da sess�o...";
                   return;
               }
           	   stopLiveCam();
           	   searching = true;
               playing = false;
            	
               for (var i=0; i < cam.length; i++) 
        	   {
               	   
               	   ajaxRequestXHR(cam[i], camData, camHora); 			
        	   }
              
             
            }
           
            function play(camData, camHora) 
            {
           	
	           	if (sessionId === "") 
	           	{
	                statusDiv.innerHTML = "Aguardando inicio da sess�o...";
	                return;
	            }
	           stopLiveCam();
	           searchingNew = true;
               searching = false;
               playing = true;
               for (var i=0; i < cam.length; i++) 
        	   {
               	
               	 ajaxRequestXHR(cam[i], camData, camHora);
               			
        	   }
            }

            function stop() 
            {
               searching = false;
               playing = false;
            }
			
            function playAllCam()
		    {			

			  $('#btnPlayStop').html('<span class=\"glyphicon glyphicon-pause\" onclick=\"stopAllCam()\"  style=\"color: white;\"></span>');
			  play(dataVar, horaVar);
	
		    }
            
            function stopAllCam()
            {
            
               stop();
               $('#btnPlayStop').html('<span class=\"glyphicon glyphicon-play\" onclick=\" playAllCam()\"  style=\"color: white;\"></span>');

            }
            
            function playAllCamTime(p) 
            {
               stopAllCam();
               if(p != null)
               {
            	  playCamTime(p);
               }
            }
            
            function playCamTime(minutos)
            {
		           
            	 
//              	 var dataAlterar = startTime;
             	 var dataAlterar = dataModificacao;
  		         var dataTimer = document.getElementById("statusDiv").innerHTML;
 		         if(minutos != 0 && dataTimer != "FIMDASIMAGENS")
 		         {
 		        	dataAlterar =  dataTimer;
 		        	console.log(dataAlterar);
		         }
		         
		        	$.ajax({
		            	method: 'GET',
		                url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet",
		                data:'action=getData&minutos='+minutos+'&data='+dataAlterar,
		                success: function(result){
		                	window.clearTimeout(velocidadeImg);
		                	stopAllCam();
		                	   
		                	var res = result.split(" ");
		                	console.log(res);
		                	flagVar = false;
		                	var array = cam;
		                	search(res[0], res[1]);
		                	var novaData = res[0].replace("-","/").replace("-","/") +" "+res[1].replace("-",":").replace("-",":");
		                	console.log('nova data '+novaData);
		                	document.getElementById("statusDiv").innerHTML = novaData;
		                	
		                	var teste = document.getElementById("statusDiv").innerHTML;
		                	
		                	console.log("teste mudanca "+teste);		                	
		                },  
		       			error: function(e){  
		       				alertError();
		       				console.log('erro '+e);
		      			}
		            });
			 }
            
             function trocarCamerasCliente(idCentral,empresa,idCamera)
             {
                
      		      	$('#gridCameras').html("<div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width:100%;color:white;'>Aguarde...</div></div>");

	            	var dataTroca = document.getElementById("statusDiv").innerHTML;
	                window.clearTimeout(velocidadeImg);
	                stopAllCam();
	            
	                var dataT = dataTroca.split(" ");
	                dataVar = dataT[0].replace("/","-").replace("/","-");
                	horaVar = dataT[1].replace(":","-").replace(":","-");
	                $.ajax({
	                	method: 'GET',
	                    url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet",
	                    data:'action=getCamerasCliente&idCamera='+idCamera+'&data='+dataVar+'&hora='+horaVar+'&idCentral='+idCentral+'&idEmpresa='+empresa+'&historico='+historico,
	                    success: function(result){
	                    	$('#gridCameras').html(result);
	                    	document.getElementById("statusDiv").innerHTML = dataTroca;
	                        if(!playingLive)
	                        {
	                        	search(dataVar, horaVar);
	                        }
	                        else
	                        {
	                        	searchCamLive();
	                        }
	                    		
	                    },  
	           			error: function(e){  
	           				alertError();
	          			}
	                });

    		 }
            
             function camerasAoVivo(camNumber) 
             {
    		
                var xhr = new XMLHttpRequest();
                var camId = camNumber;
                
                var img = document.getElementById(camNumber);
              
                // Retrieve a jpeg image using XMLHttpRequest 
                // Based on http://www.html5rocks.com/en/tutorials/file/xhr2/

                xhr.open("GET", baseUrl + "camera.cgi?camera="+camId+"&resolucao=640x480&qualidade=100", true);

                // Ask for the result as an ArrayBuffer.
                xhr.responseType = "arraybuffer";

                // This is the session id (for authentication)
                xhr.setRequestHeader('SessaoId', sessionId);

                xhr.onload = function (e) {
                    // This could be used to parse headers (including image time)
                    // console.log(this.getAllResponseHeaders());

                    if (this.status === 401) {                 	
                        statusDiv.innerHTML = "Erro de login.";
                        setTextModalAjuda();
                    	//$('#modalAjuda').modal('show');
                        return;
                    }
                    
                    if (this.status === 404) {
                    	alert( xhr.responseText );
                        return;
                    }

                    // This could be used to parse errors from the API. If the content-type is text, treat the response as a text error.
                    var contentType = this.getResponseHeader('content-type');
                    if (contentType.indexOf("text/plain") > -1) {
                        var errorText = String.fromCharCode.apply(null, new Uint8Array(this.response));
                        //statusDiv.innerHTML = errorText;
                        console.log(errorText);
                        if(errorText == "FIMDASIMAGENS")
                  	    {
                        	console.log('FIM :'+errorText);
                        	stopAllCam();
                        
                        	//search(camData, camHora);
                  	    }
                        else if(errorText == "FRAMEVAZIO")
                  	    {
                        	stopAllCam();
                        	camerasAoVivo(camNumber);
                  	    }
                        else if(errorText == "CAMERAINDISPONIVEL")
                        {
                        	img.src = baseUrlIcon;
                        }
                        else if(errorText == "NAOHAIMAGENS")
                        {
                        	img.src = baseUrlIcon;
                        }

                    }
                    else
                        if (contentType.indexOf("image/jpeg") > -1) {
                            // Obtain image date/time
                            var dateTime = this.getResponseHeader('Last-Modified');
                  	        if(dateTime != null)
                  	        {

	                  	        if(statusDiv != null && camId == cameraPrincipal)
	                  	        {

	                  	    	  	statusDiv.innerHTML = dataAtualFormatada(dateTime);
	                  	    	  
	                  	        }

                            // Obtain a blob: URL for the image data.
                            var arrayBufferView = new Uint8Array(this.response);
                            var blob = new Blob([arrayBufferView], { type: "image/jpeg" });
                            var urlCreator = window.URL || window.webkitURL;
                            var imageUrl = urlCreator.createObjectURL(blob);
                            img.onload = function() {
                                var urlCreator = window.URL || window.webkitURL;
                                urlCreator.revokeObjectURL(this.src); // Free memory
                             };
                            img.src = imageUrl;

                            if (playingLive) {
                                window.setTimeout(function () {
                                    if (playingLive)
                                    	camerasAoVivo(camNumber);
                                }, velocidadeImg);
                            }
                          }
                            
                        } 
                };

                xhr.send();
				

             }
          	
             function speedCam() 
             {
            	var sCam = document.getElementById("numSpeed").value;
          
            	if(sCam === "1")
            	{	
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "1000";
                }
            	else if(sCam === "2")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "800";
            	}
            	else if(sCam === "3")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "600";
            	}
            	else if(sCam === "4")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "400";
            	}
            	else if(sCam === "5")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "200";
            	}
            	else if(sCam === "6")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "100";
            	}
            	else if(sCam === "7")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "90";
            	}
            	else if(sCam === "8")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "60";
            	}
            	else if(sCam === "9")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "30";
            	}
            	else if(sCam === "10")
            	{
            		window.clearTimeout(velocidadeImg);
            		velocidadeImg = "10";
            	}
            	
            		
             }
            
             function stopLiveCam()
             {
            
            	playingLive = false;
            	$('#btnLive').html('<span class=\"glyphicon glyphicon-expand\" onclick=\"searchCamLive()\" style=\"color: white;\"></span>');

             }
			
			 function salvaLogEventoPopUp()
             {
             	var textoPopUp = document.getElementById("taLog").value;
             	var cdHistorico = historico;
             	var nomeCol = user;
             	if(textoPopUp != null && textoPopUp != "")
             	{
             		 $.ajax({
                        	method: 'post',
                            url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet?action=saveLogPopUp&historico='+cdHistorico+'&textoLog='+textoPopUp+'&nomeCol='+nomeCol,
                            dataType: 'text',
                            success: function(result){
                            	alertSucess();
                            	limpaLog();
                            },  
                   			error: function(e){  
                   				
                   				alert(e);
                  			}
                        });
             	}
             }
			 
             function salvaLogEventoPopUpFechar(textoStr)
             {
             	
             	var cdHistorico = historico;
             	var nomeCol =  '<%=PortalUtil.getCurrentUser().getFullName()%>';
             	
           		 $.ajax({
                      	method: 'GET',
                          url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventServlet?action=getLog&historico='+cdHistorico+'&textoLog='+textoStr+'&nomeCol='+nomeCol,
                          dataType: 'text',
                          success: function(result){
                        	  eventCallHandler(cdHistorico, true,true,false);
                          },  
                 			error: function(e){  
                 				alert(e);
                 				console.log('Erro salvar log n�o envia');
                			}
                      });
             	
             	
					
             }
			
			 function trocarLayoutCameras(idCentral,empresa,layout)
             {
                
      		      	$('#gridCameras').html("<div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width:100%;color:white;'>Aguarde...</div></div>");

	            	var dataTroca = startTime;
	                window.clearTimeout(velocidadeImg);
	                stopAllCam();
	                var dataT = dataTroca.split(" ");
	                dataVar = dataT[0].replace("/","-").replace("/","-");
                	horaVar = dataT[1].replace(":","-").replace(":","-");
	                $.ajax({
	                	method: 'GET',
	                    url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.call.servlet.OrsegupsAlertEventVideoServlet",
	                    data:'action=getCamerasClienteLayout&idCamera='+camInicial+'&data='+dataVar+'&hora='+horaVar+'&idCentral='+idCentral+'&idEmpresa='+empresa+'&layout='+layout+'&historico='+historico,
	                    success: function(result){
	                    	$('#gridCameras').html(result);
	                    	document.getElementById("statusDiv").innerHTML = dataTroca;
	                    	
	                        if(!playingLive)
	                        {
	                        	search(dataVar, horaVar);
	                        }
	                        else
	                        {
	                        	searchCamLive();
	                        }
	                    		
	                    },  
	           			error: function(e){  
	           				alertError();
	          			}
	                });

    		 }
			 
			 function stopCamNow()
			 {
				 stop();
				 window.clearTimeout(velocidadeImgNew);
				 searchingNew = false;
				 window.clearTimeout(velocidadeImg);
	             stopAllCam();
			 }
			 
			 function setTextModalAjuda(){
             	$('#modalAjudaText').append('<p> Erro ao obter informa��es de autentica��o do CFTV cadastradas no Sigma! </p>');
             	$('#modalAjudaText').append('<p> Favor verificar dados na conta Sigma > Configura��es > Servi�os Extras > CFTV </p>');
             	$('#modalAjudaText').append('<p> Informa��es s� ser�o atualizadas no pr�ximo evento! </p>');
			 }
			 
			 function setTextModalAjudaIP(){
	             	$('#modalAjudaText').append('<p> Erro ao obter c�meras do cliente! </p>');
	             	$('#modalAjudaText').append('<p> Favor verificar dados de endere�o do servidor na conta Sigma > Configura��es > Servi�os Extras > CFTV </p>');
	             	$('#modalAjudaText').append('<p> Informa��es s� ser�o atualizadas no pr�ximo evento! </p>');
				 }
           
            
        	 
      </script>

</head>
<body id="bodyId" oncontextmenu="return false;">
	<div id="avisoSucesso" class="alert alert-success avisos" data-alert="alert">Sucesso!</div>
	<div id="avisoErro" class="alert alert-danger avisos" data-alert="alert">Erro ao registrar imagem!</div>
	<div id="snoAlertBox" class="alert alert-success alert-dismissible"
		data-alert="alert">
		<span style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>
			Sucesso ao carregar as informa��es.
		</span>
	</div>
	<div id="snoAlertBoxError" class="alert alert-danger alert-dismissible"
		data-alert="alert">
		<span style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>
			Erro ao executar a a��o.
		</span>
	</div>
	<div id="snoAlertBoxInfo" class="alert alert-info alert-dismissible"
		data-alert="alert">
		<span style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>
			A��o realizada com sucesso.
		</span>
	</div>
	<div id="snoAlertBoxWarn" class="alert alert-warning alert-dismissible"
		data-alert="alert">
		<span style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>
			Buscando imagens.
		</span>
	</div>
	
	<div id="snoAlertBoxWarnServer" class="alert alert-warning alert-dismissible"	data-alert="alert">
		<span style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>
			Servidor n�o habilitado para grava��es de imagens.
		</span>
	</div>
	
	<div id="snoAlertBoxWarnLogin" class="alert alert-warning alert-dismissible"	data-alert="alert">
		<span style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>
			Usu�rio e senha do servidor CFTV incorretos.
		</span>
	</div>
	
	<form id="formPrincipal" action="">

		<div class="easyui-panel" id="pnDetalhes"
			title="Monitoramento de imagens"
			style="width: 100%; height: 900px; overflow: hidden;"
			data-options="footer:'#ft',fit:true, tools:[{
                    iconCls:'icon-reload',
                    handler:function(){
                    reloadData();
                    }
                }]
            ">

			<div class="easyui-layout" style="width: 100%; height: 800px;">
				<div id="p" data-options="region:'west'" title="Cliente"
					style="width: 30%; padding: 10px" data-options="fit:true">

					<div class="well well-sm bg-success"
						style="font-family: verdana; font-size: 10px; width: 100%; margin: 1px; padding: 2px;">

						<div align="left" style="float: left; width: 50%; height: 20px;">
							<span class="label label-default"
								style="cursor: pointer; font-family: verdana; font-size: 11px; font-weight: bold;">
								<span class="glyphicon glyphicon-info-sign" aria-hidden="true">
									<span
									style="font-family: verdana; font-size: 11px; font-weight: bold;">
								</span>
							</span>
							</span>
						</div>

						<div align="right" style="float: left; width: 50%; height: 20px;">
							<span class="label label-danger"><span
								class="glyphicon glyphicon-time" aria-hidden="true"> <span
									style="font-family: verdana; font-size: 11px; font-weight: bold;">
								</span>
							</span> </span>
						</div>

						<br>
					<div class="span1" style="margin:0px;"> 
					
					<button type="button" title="Informa��es adicionais" class="btn btn-danger btn-sm btn3d"><span class="glyphicon glyphicon-list"></span></button> 
					
					</div>


					</div>
					<div class="well well-sm"
						style="font-family: verdana; font-size: 10px; display: table; width: 100%; padding: 2px 2px; margin: 0px;">
						<h6 style="text-decoration: underline;">
							<span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
							Informa��es
						</h6>
						<div style="float: left;">
							<p>
								<strong>Empresa: </strong>
							</p>
							<p>
								<strong>C�digo Cliente: </strong>
							</p>
							<p>
								<strong>CPF/CNPJ: </strong>
							</p>
							<p>
								<strong>Raz�o Social: </strong>
							</p>
							<p>
								<strong>Fantasia: </strong>
							</p>
						</div>
					</div>

					<div class="well well-sm"
						style="font-family: verdana; font-size: 10px; display: table; width: 100%; padding: 2px 2px; margin: 0px;">
						<h6 style="text-decoration: underline;">
							<span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
							Dados Conta
						</h6>
						<div style="float: left;">
							<p>
								<strong>Conta/Parti��o: </strong><span
									class="label label-danger"
									style="font-family: verdana; font-size: 11px; font-weight: bold;">
								</span>
							</p>
							<p>
								<strong>Rota: </strong>
							</p>
							<p>
								<strong>CEP: </strong>
							</p>

							<p>
								<strong>Endere�o: </strong>
							</p>
							<p>
								<strong>Bairro: </strong>
							</p>
							<p>
								<strong>Cidade: </strong>
							</p>
							<p>
								<strong>Estado: </strong>
							</p>

						</div>
					</div>

					<div class="well well-sm"
						style="font-family: verdana; font-size: 10px; display: table; width: 100%; padding: 2px 2px; margin: 0px;">
						<h6 style="text-decoration: underline;">
							<span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
							Contato
						</h6>
						<div style="float: left;">
							<p>
								<strong>Respons�vel: </strong>
							</p>
							<p>
								<strong>Telefone 1: </strong> <span class="label label-primary"
									style="cursor: pointer; font-family: verdana; font-size: 11px; font-weight: bold;">
									<span class="glyphicon glyphicon-phone-alt" aria-hidden="true"><span
										style="font-family: verdana; font-size: 11px; font-weight: bold;">
									</span></span>
							</p>
							<p>
								<strong>Email: </strong>
							</p>
							<p>
								<strong>Telefone 2: </strong> <span class="label label-primary"
									style="cursor: pointer; font-family: verdana; font-size: 11px; font-weight: bold;">
									<span class="glyphicon glyphicon-phone-alt" aria-hidden="true"><span
										style="font-family: verdana; font-size: 11px; font-weight: bold;">
									</span></span>
							</p>
						</div>

					</div>

					<div class="well well-sm"
						style="font-family: verdana; font-size: 10px; display: table; width: 100%; padding: 2px 2px; margin: 0px;">
						<h6 style="text-decoration: underline;">
							<span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
							Palavra Chave
						</h6>
						<div style="float: left;">
							<p>
								<strong>Pergunta: </strong><span class="label label-danger"
									style="font-family: verdana; font-size: 11px; font-weight: bold;">
								</span>
							</p>
							<p>
								<strong>Resposta: </strong><span class="label label-danger"
									style="font-family: verdana; font-size: 11px; font-weight: bold;">
								</span>
							</p>
						</div>

					</div>
				</div>
				<div data-options="region:'center'" title="Cameras">
					<div class="container-fluid" align="center"
						style="margin: 2px; padding: 2px;">



						<ul class="list-unstyled video-list-thumbs row"
							style="width: 100%;">
							<li class="col-xs-8 col-sm-6" style="padding: 0px; margin: 0px;">
								<a href="#" title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail"
											style="margin: 0px; width: 99%; height: 99%;"></img>
									</div>
									<div align="left" style="padding: 0px; margin: 0px;">
										<button type="button" class="btn btn-danger btn-xs"
											style="margin: 0px;">
											<span class="glyphicon glyphicon-play" style="margin: 0px;"></span>
										</button>
										<input type="radio" title="-1min" name="optradio"
											onclick="showVal('-60')"> <input type="radio"
											title="-30s" name="optradio" onclick="showVal('-30')">
										<input type="radio" title="-20s" name="optradio"
											onclick="showVal('-20')"> <input type="radio"
											title="-10s" name="optradio" onclick="showVal('-10')">
										<input type="radio" title="-5s" name="optradio"
											onclick="showVal('-5')"> <input type="radio"
											title="Retornar ao in�cio" name="optradio"
											onclick="showVal('0')"> <input type="radio"
											title="5s" name="optradio" onclick="showVal('5')"> <input
											type="radio" title="10s" name="optradio"
											onclick="showVal('10')"> <input type="radio"
											title="20s" name="optradio" onclick="showVal('20')">
										<input type="radio" title="30s" name="optradio"
											onclick="showVal('30')"> <input type="radio"
											title="1min" name="optradio" onclick="showVal('60')">
									</div> <span class="duration" id="statusDiv">00:00</span>
							</a>
							</li>

							<li class="col-xs-8 col-sm-6" style="padding: 0px; margin: 0px;">
								<a href="#" title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail"
											style="margin: 0px; width: 99%; height: 99%;"></img>
									</div>
									<div align="left" style="padding: 0px; margin: 0px;">
										<button type="button" class="btn btn-danger btn-xs"
											style="margin: 0px;">
											<span class="glyphicon glyphicon-play" style="margin: 0px;"></span>
										</button>
										<input type="radio" title="-1min" name="optradio"
											onclick="showVal('-60')"> <input type="radio"
											title="-30s" name="optradio" onclick="showVal('-30')">
										<input type="radio" title="-20s" name="optradio"
											onclick="showVal('-20')"> <input type="radio"
											title="-10s" name="optradio" onclick="showVal('-10')">
										<input type="radio" title="-5s" name="optradio"
											onclick="showVal('-5')"> <input type="radio"
											title="Retornar ao in�cio" name="optradio"
											onclick="showVal('0')"> <input type="radio"
											title="5s" name="optradio" onclick="showVal('5')"> <input
											type="radio" title="10s" name="optradio"
											onclick="showVal('10')"> <input type="radio"
											title="20s" name="optradio" onclick="showVal('20')">
										<input type="radio" title="30s" name="optradio"
											onclick="showVal('30')"> <input type="radio"
											title="1min" name="optradio" onclick="showVal('60')">
									</div> <span class="duration" id="statusDiv">00:00</span>
							</a>
							</li>

							<li class="col-lg-3 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
							<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
								style="padding: 0px; margin: 0px;"><a href="#"
								title="Monitoramento de imagens">
									<div class="embed-responsive embed-responsive-16by9">
										<img class="embed-responsive-item"
											src="http://placehold.it/1920x1080" id="ifCam01"
											alt="Generic placeholder thumbnail" height="130px"></img>
									</div> <!-- 						<h2>Monitoramento de imagens</h2> --> <span
									class="glyphicon glyphicon-play-circle"></span> <span
									class="duration" id="statusDiv">00:00</span>
							</a></li>
						</ul>

					</div>
				</div>
			</div>
			<div style="width: 98%;" align="right">
				<div class="row">
					<button type="button" class="btn btn-danger btn3d">
						<span class="glyphicon glyphicon-off"></span> Fechar evento
					</button>

					<!-- Indicates a successful or positive action -->
					<button type="button" class="btn btn-warning btn3d">
						<span class="glyphicon glyphicon-warning-sign"></span> Deslocar
					</button>

					
				</div>
			</div>

		</div>
		<div id="ft" style="padding: 5px;">Orsegups Participa��es SA.</div>
		<div id="modalInfo" class="easyui-window" title="Cameras ao vivo" data-options="modal:false,closable:true,closed:true,resizable:true,collapsible:true,minimizable:false,maximizable:true" style="width:90%;height:90%">  
    
   		</div>
		
	</form>
	
	<!-- Modal -->
	  <div class="modal fade" id="modalAjuda" role="dialog" data-keyboard="false" data-backdrop="static">
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Erro!</h4>
	        </div>
	        <div class="modal-body" id="modalAjudaText">

	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        </div>
	      </div>
	      
	    </div>
	  </div>
	  <!-- FIM Modal -->

</body>
</html>