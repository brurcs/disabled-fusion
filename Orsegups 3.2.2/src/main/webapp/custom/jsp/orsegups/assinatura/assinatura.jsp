<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
//Config
String logoCasvig = PortalUtil.getBaseURL()+"custom/jsp/orsegups/assinatura/imagens/logo_nova.png";
String logoOrsegups = PortalUtil.getBaseURL()+"custom/jsp/orsegups/assinatura/imagens/logo_nova.png";
//String logoMetropolitana = PortalUtil.getBaseURL()+"custom/jsp/orsegups/assinatura/imagens/Metropolitana.png";
//String logoMetronic = PortalUtil.getBaseURL()+"custom/jsp/orsegups/assinatura/imagens/metronic.png";
String logoOrsegupsGD = PortalUtil.getBaseURL()+"custom/jsp/orsegups/assinatura/imagens/logo-assinatura-orsegups.png";
String logoOrsegupsParana = PortalUtil.getBaseURL()+"custom/jsp/orsegups/assinatura/imagens/logo_metropolitana.png"; 


String listaDepartamentos[][] = 
{
		 {"1","Departamento Administrativo"}
		,{"2","Departamento de Asseio Conserva��o e Limpeza"}
		,{"3","Departamento de Assessoria T�cnica"}
		,{"4","Central de Monitoramento"}
		,{"5","Departamento de RH Operacional"}
		,{"6","Departamento de Portaria Remota"}
		,{"7","Departamento Comercial Privado"}
		,{"8","Departamento Comercial P�blico"}
    	,{"9","Departamento Financeiro e Controladoria"}
		,{"10","Departamento Jur�dico"}
		,{"11","Departamento de Ressarcimento"}
		,{"12","NAC - N�cleo de Atendimento ao Cliente"}
		,{"13","CPO - Comercial Privado Operacional"}
		,{"14","Departamento de Gest�o Comercial Rastreamento"}
		,{"15","Departamento de Rastreamento e Gest�o de Frota"}
		,{"16","Departamento de Recupera��o de Cr�dito"}
		,{"17","Departamento Pessoal"}
		,{"18","Departamento de RH Estrat�gico"}
		,{"19","Departamento de Vigil�ncia Eletr�nica"}
		,{"20","Departamento Vigil�ncia Humana"}
		,{"21","Departamento de TI"}
		
};

String listaEmpresa[][] = 
{
		 {"1","Sede"}
		,{"2","Orsegups - Itaja� - SC"}
		,{"3","Orsegups - Brusque - SC"}
		,{"4","Orsegups - Blumenau - SC"}
		,{"5","Orsegups - Joinville - SC"}
		,{"6","Orsegups - Lages - SC"}
		,{"7","Orsegups - Crici�ma - SC"}
		,{"8","Orsegups - Tubar�o - SC"}
		,{"9","Orsegups - Chapec� - SC"}
		,{"10","Orsegups - Rio do Sul - SC"}
		,{"11","Orsegups - Jaragu� do Sul - SC"}
		,{"12","Orsegups - Curitiba - PR"}
		,{"12","Orsegups - Cascavel - PR"}
		,{"13","Orsegups - Campinas - SP"}
		,{"14","Orsegups - S�o Jos� do Rio Preto - SP"}
		,{"15","Orsegups - Goi�nia - GO"}
		,{"16","Orsegups - Palmas - TO"}
		,{"17","Orsegups - Novo Hamburgo - RS"}
		,{"18","Orsegups - Xangri-la - RS"}
		,{"19","Orsegups - Tramanda� - RS"}
		
		
};




String acao = "";
String nome = "";
String fullName = "";
String cargo = "";
String depto = "";
String telefone = "";
String empresa = "";

String assinaturaHtml = "";

if (request.getParameter("Acao") == null) {
	
	nome = fullName;
	cargo = ""; 
	depto = "";
	telefone = "";
	empresa = "";

// Limpeza
} else if (request.getParameter("Acao").equals("Limpar")) {
	
	nome = "";
	cargo = ""; 
	depto = "";
	telefone = "";
	empresa = "";

// Dados iniciais
}else{
	nome =  request.getParameter("nome") != null ?  request.getParameter("nome") : "";
	cargo = request.getParameter("cargo") != null ? request.getParameter("cargo") : ""; 
	depto = request.getParameter("depto") != null ? request.getParameter("depto") : "";
	//email = request.getParameter("email") != null ? request.getParameter("email") : "";
	fullName = request.getParameter("fullName") != null ? request.getParameter("fullName") : "";
	telefone = request.getParameter("telefone") != null ? request.getParameter("telefone") : "";
	
}

%>

<%!

public String getComboText(String value, String arr[][]){
	String ret = "";
	
	for (String linha[] : arr){
		if (linha[0].equals(value)){
			ret = linha[1];
			break;
		}
	}
	return ret;
}

public String montaSelect(String lista[][], String nomeSelect ,String chavePadrao ,int usaTitulo ,String TextoTitulo ,String OpcExtra) {
	String Select = "<select name='"+nomeSelect+"' "+OpcExtra+">";
	if (usaTitulo == 1) {
		Select += "\n <option value=\"0\">"+TextoTitulo+"</option>";     
	}
	if (lista.length > 0) {
		for(String array_linha[] : lista ) { 
			String codigo = array_linha[0];
			String descricao = array_linha[1];
			String flgPadrao="";
			if (codigo.equals(chavePadrao != null ? chavePadrao : "")) {
				flgPadrao = " SELECTED ";
			}
			Select += "\n <option value='"+codigo+"' "+flgPadrao+" >"+descricao+"</option>"; 
		}
	}
	Select += "</select>"; 
	return Select;
}

public String geraAssinaturaOrsegups(String logoOrsegups, String nome, String cargo, String depto, String listaDepartamentos[][], String telefone ){
	String assinaturaHtmlFile = 
		    "<html>"+
			" <head>"+
			"<script language='javascript'>"+	
			"</script>"+
			"<link rel='stylesheet' type='text/css'"+
			"href='https://intranet.orsegups.com.br:443/fusion/custom/jsp/orsegups/assinatura/css/assinatura.css'>"+
		    "</head>"+
			"<boby>"+
		    " <br> "+ 
			" <table border='0' cellpadding='5'> "+
			" <tr valign='middle'> "+
			"   <td> "+
			//"      <img align='absMiddle' border='0' style='width: 63px; height:78px;' src='" + logoOrsegups +"'/> "+
			"      <img align='absMiddle' border='0' src='" + logoOrsegups +"'/> "+
			"   </td> "+
			"   <td width='0px'>&nbsp; "+
			"   </td> "+
			"   <td valign='middle' width='500px' style='line-height:1.30'> "+
			"   <p> "+ 
			" 	<span style='font-family:Encode Sans;color:#132c85;font-size:12pt;'><b>" + nome + "</b></span><br>\n" +
			" 	<span style='font-family:Encode Sans;color:#6f6f6f;font-size:11pt;margin-top:10px'>"+ (cargo == null? "":cargo) +  getComboText(depto, listaDepartamentos) + "</span><br>\n "+
			//" 	<span style='font-family:Encode Sans;color:#6f6f6f;font-size:11pt;'>"+(email == null ? "": email) + "</span><br>\n "+
			"   <span style='font-family:Encode Sans;color:#6f6f6f;font-size:11pt;'><b>"+(telefone == null ? "": telefone) + "</b></span><br>\n "+
			"   <span style='font-family:Encode Sans;color:#6f6f6f;font-size:11pt;'><b>orsegups.com.br</b></span><br>\n  " +	
			" 	</p> "+
			"   </td> "+
			" </tr> "+
			" </table> "+ 
			" <br> "+
			" </body>"+
			"</html>";
			
			return assinaturaHtmlFile;
}


public String geraAssinaturaOrsegupsParana(String logoOrsegups, String logoOrsegupsParana, String nome, String cargo, String depto, String listaDepartamentos[][], String telefone ){
	String assinaturaHtmlFile = 
			" <br> "+
			" <table border='0' cellpadding='5'> "+
			" <tr valign='middle'> "+
			"   <td > "+
			"      <img align='absMiddle' border='0' style='margin-botton:70px;' src='" + logoOrsegups +"'/> "+
			"   </td> "+
			"   <td valign='middle' width='500px' style='font-size:8pt; font-family:Verdana; color:gray; line-height:1.35'> "+
			
			" <table style='margin: 0px; padding:0px;'> "+
			"	<tr> "+
			"		<td valign='middle' width='500px' style='font-size:8pt; font-family:Verdana; color:gray; line-height:1.35'> "+
					"	<br/>"+
					"   <p>  "+
					" 	<strong>" + nome + "</strong><br>\n" +
					" 	"+ (cargo == null? "":cargo) +  getComboText(depto, listaDepartamentos) + "\n "+
					" 	<br><span style='color:black;'>"+ (telefone == null ? "": telefone) + "</span><br>\n "+
					//" 	<a style='text-decoration: none'>" + (email == null ? "": email) +"</a> "+
					" 	</p> "+
			"		</td> "+
			"	</tr> "+
			"	<tr> "+
			"		<td width='200px'> "+
			"			<img align='absMiddle' border='0' style='width: 200px; height:30px;' src='" + logoOrsegupsParana +"'/> "+
			"		</td> "+
			"	</tr> "+
			" </table> "+
			
			
			"   </td> "+
			" </tr> "+
			" </table> "+
			" <br> "+
			" <br>";
			return assinaturaHtmlFile;
}

%>

<%
		
		if (request.getParameter("Acao") != null && request.getParameter("Acao").equals("Salvar")){
			String assinaturaHtmlFile = "";
			//if (request.getParameter("empresa") != null  && request.getParameter("empresa").equals("12")){
			//	assinaturaHtmlFile = geraAssinaturaOrsegupsParana(logoOrsegupsGD, logoOrsegupsParana, nome, cargo, depto, listaDepartamentos, telefone, email);
			//}else{
				assinaturaHtmlFile = geraAssinaturaOrsegups(logoOrsegups, nome, cargo, depto, listaDepartamentos, telefone);
			//}
			
					//response.setHeader("HTTP/1.1","200 OK");
					//response.setHeader("Status", "200 OK");
					response.setHeader("Content-Type","application/force-download"); 
					response.setHeader("Content-Disposition","attachment; filename='assinatura.htm'"); 
					response.setHeader("Content-Length", assinaturaHtmlFile.length()+"");
					response.getWriter().write(assinaturaHtmlFile);
			
		}else{
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gerador de Assinaturas</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache">
<meta name="robots" content="none">
<script language="javascript">
	function checa() {
		if (document.form.lider.value == 1) {
			document.form.cargo.disabled = false;
			document.form.depto.disabled = true;
			document.form.depto.options[0].text = '-- Ignore este campo --';
		} else {
			document.form.depto.options[0].text = '-- Escolha --';
			document.form.depto.disabled = false;
			document.form.cargo.disabled = true;
		}
	}
</script>
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/assinatura/css/assinatura.css">
</head>
<body topmargin="0" leftmargin="0" onLoad="javascript:checa();">
	<table width="100%" width="100%" border="0" cellpadding="0"
		cellspacing="0">
		<tr bgcolor="#FFFFFF">
			<td valign="top">
				<!-- Cabecalho do programa -->
				<table width="400" height="30" border="0">
					<tr>
						<!-- Logo -->
						<td></td>
						<td><font face="Verdana" size="2" color="#000000"> <b>Gerador
									de Assinatura</b>
						</font></td>
					</tr>
				</table>

			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td><hr
					style="color: #000000; background-color: #000000; height: 1px; valign: top"></td>
		</tr>
		<tr>
			<td>
				<!-- Formulario para Gerar assinatura --> <br>
				<form action="assinatura.jsp" name="form" method="POST">
					<input type="hidden" id="fullName" name="fullName"
						value="<%= (request.getParameter("fullName") != null ? request.getParameter("fullName") : "") %>">
					<font face="Verdana" size="1" color="#000000">Digite seus
						dados e clique no bot&atilde;o :<br> - "Visualizar" para ver
						sua assinatura<br> - "Salvar" para baixar o arquivo para usar
						no Outlook<br> - "Limpar" para limpar os campos<br> <br>
					</font>
					<table border="0">
						<tr>
							<td><font face="Verdana" size="1" color="#000000">Escrit&oacute;rio
									Regional </font></td>
							<td><%=  montaSelect(listaEmpresa, "empresa", request.getParameter("empresa"), 0, "-- --", "onChange='javascript:checa();' ")	%>
							</td>
						</tr>

						<tr>
							<td><font face="Verdana" size="1" color="#000000">Nome
							</font></td>
							<td><input
								style="border: solid 1 px; font-family: Verdana; font-size: 11px"
								size="45" name="nome" type="text" value="<%= nome %>"></td>
						</tr>
						<tr>
							<td><font face="Verdana" size="1" color="#000000">
									Cargo de Lideran&ccedil;a?</font></td>
							<td><font face="Verdana" size="1" color="#000000"> <select
									name="lider" onChange='javascript:checa();'>
										<option value="0" SELECTED>N&atilde;o</option>
										<option value="1">Sim</option>
								</select>
							</font></td>
						</tr>
						<tr>
							<td><font face="Verdana" size="1" color="#000000">Descri��o do cargo </font></td>
							<td><input
								style="border: solid 1 px; font-family: Verdana; font-size: 11px"
								size="45" name="cargo" type="text" value="<%= cargo %>"></td>
						</tr>
						<tr>
							<td><font face="Verdana" size="1" color="#000000">Depto
									Imediato</font></td>
							<td><%=  montaSelect(listaDepartamentos, "depto", request.getParameter("depto"), 1, "-- --", " border: solid 1 px; font-family:Verdana; font-size:11px;' ")	%>
							</td>
						</tr>
						<tr>
							<td><font face="Verdana" size="1" color="#000000">Telefone
							</font></td>
							<td><input
								style="border: solid 1 px; font-family: Verdana; font-size: 11px"
								size="45" name="telefone" type="text" value="<%= telefone %>">
								<font face="Verdana" size="1" color="#000000">Ex.: 47
									2104-9500 / 9994-4849</font></td>
						</tr>
						<%-- <tr>
							<td><font face="Verdana" size="1" color="#000000">E-mail
							</font></td>
							<td><input
								style="border: solid 1 px; font-family: Verdana; font-size: 11px"
								size="45" name="email" type="text" value="<%= email %>"></td>
						</tr> --%>
					</table>
					<br> &nbsp; <input type="submit" name="Acao"
						value="Visualizar"
						style="border: solid 1 px; font-family: Verdana; font-size: 11px">
					&nbsp; <input type="submit" name="Acao" value="Salvar"
						style="border: solid 1 px; font-family: Verdana; font-size: 11px">
					&nbsp; <input type="submit" name="Acao" value="Limpar"
						style="border: solid 1 px; font-family: Verdana; font-size: 11px">
				</form> <!-- ------------------------------ -->
			</td>
		</tr>
	</table>
	<br>
	<div>
		<%
		if (request.getParameter("Acao") != null && request.getParameter("Acao").equals("Visualizar")){
			
			String assinaturaHtmlFile = "";
			//if (request.getParameter("empresa") != null  && request.getParameter("empresa").equals("12")){
				//assinaturaHtmlFile = geraAssinaturaOrsegupsParana(logoOrsegupsGD, logoOrsegupsParana, nome, cargo, depto, listaDepartamentos, telefone, email);
			//	assinaturaHtmlFile = geraAssinaturaOrsegups(logoOrsegups, nome, cargo, depto, listaDepartamentos, telefone, email);
			//}else{
				assinaturaHtmlFile = geraAssinaturaOrsegups(logoOrsegups, nome, cargo, depto, listaDepartamentos, telefone);
			//}
			
			
			%>
		<%= assinaturaHtmlFile %>
		<%
		}
		%>
	</div>

</body>
</html>
<%
}
%>