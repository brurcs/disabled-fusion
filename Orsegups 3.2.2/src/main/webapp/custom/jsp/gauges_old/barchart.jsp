<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Random"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<!DOCTYPE html>
<html>

<%!

public class GraficoVO implements Comparable<GraficoVO>
{
	private int alertaComLigacao = 0;
	private double alertaComLigacaoTempo = 0.0;
	private int alertaSemLigacao = 0;
	private double alertaSemLigacaoTempo = 0.0;
	private int totalEvento = 0;
	private double totalEventoTempo = 0.0;
	private String nomeViatura;

	
	public int getAlertaComLigacao() {
		return alertaComLigacao;
	}


	public void setAlertaComLigacao(int alertaComLigacao) {
		this.alertaComLigacao = alertaComLigacao;
	}
	
	public double getAlertaComLigacaoTempo() {
		return alertaComLigacaoTempo;
	}


	public void setAlertaComLigacaoTempo(double alertaComLigacaoTempo) {
		this.alertaComLigacaoTempo = alertaComLigacaoTempo;
	}
	
	public int getAlertaSemLigacao() {
		return alertaSemLigacao;
	}


	public void setAlertaSemLigacao(int alertaSemLigacao) {
		this.alertaSemLigacao = alertaSemLigacao;
	}
	
	public double getAlertaSemLigacaoTempo() {
		return alertaSemLigacaoTempo;
	}


	public void setAlertaSemLigacaoTempo(double alertaSemLigacaoTempo) {
		this.alertaSemLigacaoTempo = alertaSemLigacaoTempo;
	}
	
	public int getTotalEvento() {
		return totalEvento;
	}


	public void setTotalEvento(int totalEvento) {
		this.totalEvento = totalEvento;
	}
	
	public double getTotalEventoTempo() {
		return totalEventoTempo;
	}


	public void setTotalEventoTempo(double totalEventoTempo) {
		this.totalEventoTempo = totalEventoTempo;
	}
	
	public String getNomeViatura() {
		return nomeViatura;
	}


	public void setNomeViatura(String nomeViatura) {
		this.nomeViatura = nomeViatura;
	}
	

	public int compareTo(GraficoVO o)
	{
		if(this.getTotalEventoTempo() > o.getTotalEventoTempo())
			return 1;
		// TODO Auto-generated method stub
		return  -1;
	}
	
	
}
	
%>


<%
	String regional = request.getParameter("regional");
	String kpi = request.getParameter("kpi");
	String width = request.getParameter("width");
	if (width == null) width = "300";		
	String height = request.getParameter("height");
	if (height == null) height = "220";		
	String redFrom = request.getParameter("redFrom");
	if (redFrom == null) redFrom = "8";		
	String redTo = request.getParameter("redTo");
	if (redTo == null) redTo = "10";		
	String yellowFrom = request.getParameter("yellowFrom");
	if (yellowFrom == null) yellowFrom = "5";		
	String yellowTo = request.getParameter("yellowTo");
	if (yellowTo == null) yellowTo = "8";		
	String greenFrom = request.getParameter("greenFrom");
	if (greenFrom == null) greenFrom = "0";		
	String greenTo = request.getParameter("greenTo");
	if (greenTo == null) greenTo = "5";		
	String min = request.getParameter("min");
	if (min == null) min = "0";		
	String max = request.getParameter("max");
	if (max == null) max = "10";
	String showMajorTicks = request.getParameter("showMajorTicks");
	String majorTicks = request.getParameter("majorTicks");
	if (showMajorTicks == null || showMajorTicks.equals("yes")) 
	{
		if (majorTicks == null) 
		{
		  majorTicks = max;
		}

		int iMax = Integer.parseInt(majorTicks);
		majorTicks = "";
		for (int i=0;i<=iMax;i++) 
		{
		  if (i == 0)
		  {
		  majorTicks = ", majorTicks: ['" + i + "'";	  
		  } 
		  else 
		  {
		  majorTicks = majorTicks + ", '"+i+"'";	  
		  }
		  if (i == iMax)
		  {
			majorTicks = majorTicks + "]";
		  } 
		  
		}
	}
	else 
	{
		majorTicks = "";
	}
	
	
	String jsonStr = "[['AIT','Alertas C/L',{ role: 'annotation' },{role: 'tooltip'} , 'Alertas S/L', { role: 'annotation' },{role: 'tooltip'}, 'Eventos sem alerta',  { role: 'annotation' } ,{role: 'tooltip'}  ],";
	String regionalTitulo = regional;
	if(regional == null || regional.trim().equals("") || regional.trim().equals("null"))	{
		regional = "'SOO', 'IAI', 'BQE', 'BNU', 'JLE', 'LGS', 'CUA', 'CCO', 'RSL', 'JGS', 'CTA', 'CSC' ";
		regionalTitulo = "Geral";
 	} else {
		regional = "'"+regional+"'";
	}
	
	double kpi_value = 0;
	int kpi_qtd = 0;
	String kpi_plc = "";
	StringBuilder select_EV = null;
	StringBuilder select_CL = null;
	StringBuilder select_SL = null;

 	if (kpi != null) {
 		
 		select_EV = new StringBuilder();
		  
 		select_EV.append(" SELECT COUNT(*) as TOTAL,VW.CD_VIATURA,V.NM_VIATURA  FROM VIEW_HISTORICO VW WITH(NOLOCK)  ");
 		select_EV.append(" INNER JOIN dbCENTRAL C WITH (NOLOCK) ON C.CD_CLIENTE = VW.CD_CLIENTE ");
 		select_EV.append(" INNER JOIN VIATURA V WITH (NOLOCK) ON VW.CD_VIATURA = V.CD_VIATURA ");
 		select_EV.append(" WHERE V.FG_ATIVO = 1 ");
 		select_EV.append(" AND SUBSTRING(V.NM_VIATURA, 1, 3)  IN ("+ regional +")  ");											  
 		select_EV.append(" AND VW.DT_FECHAMENTO >=   ");  
 		select_EV.append(" (CASE WHEN DATEPART(HOUR, GETDATE()) < 6  "); 
 		select_EV.append(" THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0))) "); 
 		select_EV.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  "); 
 		select_EV.append(" THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))    "); 
 		select_EV.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)    "); 
 		select_EV.append(" GROUP BY VW.CD_VIATURA,V.NM_VIATURA  "); 
 		select_EV.append(" ORDER BY TOTAL DESC  "); 

		select_CL = new StringBuilder();
				  
		select_CL.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60  , 2) AS DECIMAL(9,2)) AS KPI, placaViatura ,  ");
		select_CL.append("  SUBSTRING(V.NM_VIATURA,1 , 3 ) AS REG , V.NM_VIATURA, COUNT(*) AS QTD, V.CD_VIATURA   ");
		select_CL.append("  FROM d_SIGMALogViatura SL1  WITH (NOLOCK)   ");
		select_CL.append("  INNER JOIN X_SIGMA90VIATURA VF WITH (NOLOCK) ON SL1.VIATURA_NEOID = VF.NEOID  ");
		select_CL.append("  INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.VIATURA AS V WITH (NOLOCK)   ON V.CD_VIATURA = VF.CD_VIATURA  ");
		select_CL.append("  WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%' AND V.FG_ATIVO = 1   and textoLog not like  '%excesso de tempo no local.%' ");
		select_CL.append("  AND SUBSTRING(V.NM_VIATURA, 1, 3) IN ("+ regional +")  ");
		select_CL.append("  AND  EXISTS( SELECT SL2.placaViatura   ");
		select_CL.append("  FROM d_SIGMALogViatura   SL2  "); 
		select_CL.append("  INNER JOIN X_SIGMA90VIATURA VF2 WITH (NOLOCK) ON SL2.VIATURA_NEOID = VF2.NEOID  ");
		select_CL.append("  INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.VIATURA AS V2 WITH (NOLOCK)   ON V2.CD_VIATURA = VF.CD_VIATURA ");
		select_CL.append("  AND V.FG_ATIVO = 1  ");
		select_CL.append("  WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim ");
		select_CL.append("  AND textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' "); 
		select_CL.append("  AND SL2.placaViatura = SL1.placaViatura )  ");  
		select_CL.append("  AND dataLog >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");  
		select_CL.append("  WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))   ");
		select_CL.append("  ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)  ");
		select_CL.append("  GROUP BY placaViatura,  SUBSTRING(V.NM_VIATURA,1 , 3 ) , V.NM_VIATURA, V.CD_VIATURA  ");
		select_CL.append("  ORDER BY  KPI DESC ");
	
	
		select_SL = new StringBuilder();
				  
		select_SL.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60  , 2) AS DECIMAL(9,2)) AS KPI, placaViatura ,  ");
		select_SL.append("  SUBSTRING(V.NM_VIATURA,1 , 3 ) AS REG , V.NM_VIATURA, COUNT(DISTINCT textoLog) AS QTD, V.CD_VIATURA    ");
		select_SL.append("  FROM d_SIGMALogViatura SL1  WITH (NOLOCK)   ");
		select_SL.append("  INNER JOIN X_SIGMA90VIATURA VF WITH (NOLOCK) ON SL1.VIATURA_NEOID = VF.NEOID  ");
		select_SL.append("  INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.VIATURA AS V WITH (NOLOCK)   ON V.CD_VIATURA = VF.CD_VIATURA  ");
		select_SL.append("  WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%' AND V.FG_ATIVO = 1  and textoLog not like  '%excesso de tempo no local.%' ");
		select_SL.append("  AND SUBSTRING(V.NM_VIATURA, 1, 3) IN ("+ regional +")  ");
		select_SL.append("  AND NOT  EXISTS( SELECT SL2.placaViatura   ");
		select_SL.append("  FROM d_SIGMALogViatura   SL2  "); 
		select_SL.append("  INNER JOIN X_SIGMA90VIATURA VF2 WITH (NOLOCK) ON SL2.VIATURA_NEOID = VF2.NEOID  ");
		select_SL.append("  INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.VIATURA AS V2 WITH (NOLOCK)   ON V2.CD_VIATURA = VF.CD_VIATURA ");
		select_SL.append("  AND V.FG_ATIVO = 1  ");
		select_SL.append("  WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim ");
		select_SL.append("  AND textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' "); 
		select_SL.append("  AND SL2.placaViatura = SL1.placaViatura )  ");  
		select_SL.append("  AND dataLog >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");  
		select_SL.append("  WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))   ");
		select_SL.append("  ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)  ");
		select_SL.append("  GROUP BY placaViatura,  SUBSTRING(V.NM_VIATURA,1 , 3 ) , V.NM_VIATURA , V.CD_VIATURA   ");
		select_SL.append("  ORDER BY KPI DESC  ");
		
		  
	} 
	
	    Connection connSigma = null;
	    PreparedStatement stSeqSIG = null;
		ResultSet rsSeqSIG = null;
		Connection connFusion = null;	
		PreparedStatement stSeqCL = null;
		PreparedStatement stSeqSL = null;
		ResultSet rsSeqCL = null;
		ResultSet rsSeqSL = null;
		DecimalFormat decimalFormat = new DecimalFormat("0.00"); 
		List<GraficoVO> listaGraficoVO = new ArrayList<GraficoVO>();
	try {
		
		// KPI
		if (select_EV != null ) {
			connSigma = PersistEngine.getConnection("SIGMA90");
			
			stSeqSIG = connSigma.prepareStatement(select_EV.toString());
			rsSeqSIG = stSeqSIG.executeQuery();
			GraficoVO graficoVO = null;
			while(rsSeqSIG.next()) {
				graficoVO = new GraficoVO();
				graficoVO.setTotalEvento(rsSeqSIG.getInt("TOTAL"));
				graficoVO.setNomeViatura(rsSeqSIG.getString("NM_VIATURA"));
				listaGraficoVO.add(graficoVO);
			} 
			OrsegupsUtils.closeConnection(connSigma, stSeqSIG, rsSeqSIG);
			connFusion = PersistEngine.getConnection("FUSIONPROD");	
		
			stSeqCL = connFusion.prepareStatement(select_CL.toString());
			rsSeqCL = stSeqCL.executeQuery();
			  
			
			while (rsSeqCL.next()) {
				
				String nomeVtr = rsSeqCL.getString("NM_VIATURA");
				for(GraficoVO graficoVOObj: listaGraficoVO)
				{
					if(graficoVOObj.getNomeViatura().equals(nomeVtr))
					{
						graficoVOObj.setAlertaComLigacao(rsSeqCL.getInt("QTD"));
						graficoVOObj.setAlertaComLigacaoTempo(rsSeqCL.getDouble("KPI"));
					}
				}
			
			} 
			OrsegupsUtils.closeConnection(null, stSeqSL, rsSeqSL);
			stSeqSL = connFusion.prepareStatement(select_SL.toString());
			rsSeqSL = stSeqSL.executeQuery();
			
			while (rsSeqSL.next())
			{
				
				String nomeVtr = rsSeqSL.getString("NM_VIATURA");
				for(GraficoVO graficoVOObj: listaGraficoVO)
				{
					if(graficoVOObj.getNomeViatura().equals(nomeVtr))
					{
						graficoVOObj.setAlertaSemLigacao(rsSeqSL.getInt("QTD"));
						graficoVOObj.setAlertaSemLigacaoTempo(rsSeqSL.getDouble("KPI"));
					}
				}
			
			} 
			OrsegupsUtils.closeConnection(connFusion, stSeqSL, rsSeqSL);
		}
		int flag = 0;
		for(GraficoVO vo: listaGraficoVO)
		{
				double ida = (((vo.getAlertaComLigacao() * vo.getAlertaComLigacaoTempo() ) + (vo.getAlertaSemLigacao() * vo.getAlertaSemLigacaoTempo()))+1)/vo.getTotalEvento();
				vo.setTotalEventoTempo(ida);	
		}
		
		Collections.sort(listaGraficoVO);
		for(GraficoVO vo: listaGraficoVO)
		{
			if(flag > 0)
			{
				jsonStr +=",";
			}
				double ida = (((vo.getAlertaComLigacao() * vo.getAlertaComLigacaoTempo() ) + (vo.getAlertaSemLigacao() * vo.getAlertaSemLigacaoTempo()))+1)/vo.getTotalEvento();
				//int total =  (((((vo.getTotalEvento()  - vo.getAlertaComLigacao() ))) - (((vo.getTotalEvento() - vo.getAlertaSemLigacao()) ))));
				int total =  vo.getTotalEvento()  - ( vo.getAlertaComLigacao() + vo.getAlertaSemLigacao());
				
				if(total == 0)
				  total = vo.getTotalEvento();
				  else if(total < 0)
						 total = 0;
						 
				jsonStr += "['"+decimalFormat.format(vo.getTotalEventoTempo())+" - "+vo.getNomeViatura()+"',"+vo.getAlertaComLigacao()+","+vo.getAlertaComLigacao()+",'"+vo.getAlertaComLigacaoTempo()+" min',"+
							vo.getAlertaSemLigacao()+","+vo.getAlertaSemLigacao()+",'"+vo.getAlertaSemLigacaoTempo()+" min',"+
									total+","+ total +",'"+decimalFormat.format(vo.getTotalEventoTempo())+" IDA']";
				flag++;
		}
	} catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #5 - Erro ao realizar consulta no banco de dados do Sigma");
		return;    
	} finally{
		try{
			OrsegupsUtils.closeConnection(connFusion, stSeqCL, rsSeqCL);
			OrsegupsUtils.closeConnection(connFusion, stSeqSL, rsSeqSL);
			OrsegupsUtils.closeConnection(connSigma, stSeqSIG, rsSeqSIG);
		} catch (Exception e) {
			e.printStackTrace();
		}
		jsonStr +="]";
	}
%>

  <head>
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
	 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
   
	
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
		
		
		// Create our data table out of JSON data loaded from server.
		
		 var data =  google.visualization.arrayToDataTable(<%=jsonStr%>);
		
		var view = new google.visualization.DataView(data);
	    view.setColumns([0, 1,
	                       { calc: "stringify",
	                         sourceColumn: 1,
	                         type: "string",
	                         role: "annotation" },2]);
		
		var options = {
		title : 'Relatório AIT/EVENTO/ALERTA.',
		subtitle: 'Relatório analítico de produtividade AIT.',
        width: 1280,
        height: 960,
		is3D: true,
		colors: ['#FF0000', '#FFD700', '#32CD32'],
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
			annotations: {
			alwaysOutside:false,                                // x2, and y2 is the box. If false,
			textStyle: {
			fontName: 'Verdana',
			fontSize: 8,
			bold: true,
			color: '#005500',     // The color of the text.
			opacity: 0.8          // The transparency of the text.
		}
                            
  
    },

	vAxis:{title:'AIT',textStyle:{color: '#005500',bold: true,fontName: 'Verdana', fontSize: '10', paddingRight: '100',marginRight: '100'}},
    hAxis: { title: 'Total de eventos', textStyle: { color: '#005500', bold: true,fontName: 'Verdana',fontSize: '10', paddingRight: '100', marginRight: '100'} },
    chartArea: {left:550, width: 800} ,
		axes: {
            y: {
              distance: {label: 'Teste 1'}, // Left y-axis.
              brightness: {side: 'right', label: 'Teste 2'} // Right y-axis.
            }
          }
       
      };
		

		var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
		chart.draw(data, options);

		var timeoutGauge = setTimeout("drawChart()",3600000);
			
    }
	
    
  
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
  </body>
</html>