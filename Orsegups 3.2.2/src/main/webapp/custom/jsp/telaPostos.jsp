<%@page import="java.util.LinkedHashSet"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="java.io.IOException"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="javax.servlet.ServletException"%>
<%@page import="javax.servlet.http.HttpServlet"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="javax.servlet.http.HttpServletResponse"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.adapter.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
Calendar TEMPO = Calendar.getInstance();
System.out.println("TESTE ROTINA TELEFONES INICIO" + TEMPO.getTime());
Connection connSigma = null, connSapiens = null;
PreparedStatement pstmSapiens = null, pstmSigma = null;
LinkedHashSet<String> listaFones = new LinkedHashSet<String>();

try
{
	connSigma = PersistEngine.getConnection("SIGMA90");
	connSapiens = PersistEngine.getConnection("SAPIENS");
	connSapiens.setAutoCommit(false);
	
	StringBuilder sqlSapiens = new StringBuilder();
	sqlSapiens.append(" SELECT cli.foncli, cli.foncl2, cli.foncl3, cli.foncl4, cli.foncl5, ctr.usu_clifon  ");
	sqlSapiens.append(" FROM e085cli cli ");
	sqlSapiens.append(" INNER JOIN  usu_t160ctr ctr ON (cli.codcli  = ctr.usu_codcli)  ");
	sqlSapiens.append(" INNER JOIN usu_t160cvs cvs ON (cvs.usu_numctr = ctr.usu_numctr and ");
	sqlSapiens.append(" cvs.usu_codfil = ctr.usu_codfil and cvs.usu_codemp = ctr.usu_codemp) ");
	sqlSapiens.append(" WHERE (cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >=  GETDATE()) ");

	StringBuilder sqlSigma = new StringBuilder();
	sqlSigma.append(" SELECT DBC.FONE1, DBC.FONE2, DBP.FONE1, DBP.FONE2  "); 
	sqlSigma.append(" FROM dbPROVIDENCIA DBP  "); 
	sqlSigma.append(" RIGHT JOIN dbCENTRAL DBC ON (DBC.CD_CLIENTE =  DBP.CD_CLIENTE) ");  
	sqlSigma.append(" INNER JOIN [CACHOEIRA\\SQL01].Sapiens.dbo.USU_T160SIG AS SIG ON (sig.usu_codcli = DBC.CD_CLIENTE) "); 
	sqlSigma.append(" INNER JOIN [CACHOEIRA\\SQL01].Sapiens.dbo.E085CLI cli on (dbc.cd_cliente = cli.codcli) "); 
	sqlSigma.append(" INNER JOIN [CACHOEIRA\\SQL01].Sapiens.dbo.usu_t160ctr ctr ON (cli.codcli  = ctr.usu_codcli) ");  
	sqlSigma.append(" INNER JOIN [CACHOEIRA\\SQL01].Sapiens.dbo.usu_t160cvs cvs ON (cvs.usu_numctr = ctr.usu_numctr AND cvs.usu_codfil = ctr.usu_codfil and cvs.usu_codemp = ctr.usu_codemp) "); 
	sqlSigma.append(" WHERE ((cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' AND cvs.usu_datfim >=  GETDATE()) and "); 
	sqlSigma.append(" cli.tipemc = 1) AND (DBC.ctrl_central = 1) ");
	StringBuilder sqlLimparBase = new StringBuilder();
	sqlLimparBase.append(" truncate table usu_t160fne ");

	StringBuilder sqlPrencherBase = new StringBuilder();
	sqlPrencherBase.append(" INSERT INTO USU_T160FNE (USU_numfne) VALUES (?) ");

	//LIMPA-SE TODA A TABELA
	pstmSapiens = connSapiens.prepareStatement(sqlLimparBase.toString());
	pstmSapiens.execute();
	pstmSapiens = null;

	//BUSCA-SE REGISTROS NA BASE DO SAPIENS
	pstmSapiens = connSapiens.prepareStatement(sqlSapiens.toString());
	ResultSet rsSapiens = pstmSapiens.executeQuery();
	
	while (rsSapiens.next())
	{
		if (!(NeoUtils.safeIsNull(rsSapiens.getString(1)) || rsSapiens.getString(1).isEmpty()||listaFones.contains(rsSapiens.getString(1))))
		{
			listaFones.add(rsSapiens.getString(1));
		}
		if (!(NeoUtils.safeIsNull(rsSapiens.getString(2)) || rsSapiens.getString(2).isEmpty()||listaFones.contains(rsSapiens.getString(2))))
		{
			listaFones.add(rsSapiens.getString(2));
		}
		if (!(NeoUtils.safeIsNull(rsSapiens.getString(3)) || rsSapiens.getString(3).isEmpty()||listaFones.contains(rsSapiens.getString(3))))
		{
			listaFones.add(rsSapiens.getString(3));
		}
		if (!(NeoUtils.safeIsNull(rsSapiens.getString(4)) || rsSapiens.getString(4).isEmpty()||listaFones.contains(rsSapiens.getString(4))))
		{
			listaFones.add(rsSapiens.getString(4));
		}
		if (!(NeoUtils.safeIsNull(rsSapiens.getString(5)) || rsSapiens.getString(5).isEmpty()||listaFones.contains(rsSapiens.getString(5))))
		{
			listaFones.add(rsSapiens.getString(5));
		}
		if (!(NeoUtils.safeIsNull(rsSapiens.getString(6)) || rsSapiens.getString(6).isEmpty()||listaFones.contains(rsSapiens.getString(6))))
		{
			listaFones.add(rsSapiens.getString(6));

		}

	}
	
	pstmSapiens.close();

	//BUSCA-SE REGISTROS NA BASE DO SIGMA

	pstmSigma = connSigma.prepareStatement(sqlSigma.toString());
	ResultSet rsSigma = pstmSigma.executeQuery();
	while (rsSigma.next())
	{

		if (!(NeoUtils.safeIsNull(rsSigma.getString(1)) || rsSigma.getString(1).isEmpty()||listaFones.contains(rsSigma.getString(1))))
		{
			listaFones.add(rsSigma.getString(1));
		}
		if (!(NeoUtils.safeIsNull(rsSigma.getString(2)) || rsSigma.getString(2).isEmpty()||listaFones.contains(rsSigma.getString(2))))
		{
			listaFones.add(rsSigma.getString(2));
		}
		if (!(NeoUtils.safeIsNull(rsSigma.getString(3)) || rsSigma.getString(3).isEmpty()||listaFones.contains(rsSigma.getString(3))))
		{
			listaFones.add(rsSigma.getString(3));
		}
		if (!(NeoUtils.safeIsNull(rsSigma.getString(4)) || rsSigma.getString(4).isEmpty()||listaFones.contains(rsSigma.getString(4))))
		{
			listaFones.add(rsSigma.getString(4));
		}

	}
	
	pstmSigma.close();
	
	//INSERI-SE TODOS REGISTROS VALIDOS NA TABELA USU)T160FNE
	pstmSapiens = null;
	pstmSapiens = connSapiens.prepareStatement(sqlPrencherBase.toString());
	for (Object auxFone : listaFones)
	{
		String auxFoneStr = auxFone.toString();
		//VALIDACAO DA STRING
		if ((!auxFoneStr.equals(" ")))
		{
			auxFoneStr = auxFoneStr.replaceAll("[^0-9]", "");
			if (auxFoneStr.length() == 10)
			{
				pstmSapiens.setString(1, auxFoneStr);
				pstmSapiens.execute();
			}
		}

	}
	connSapiens.commit();
}
catch (SQLException e)
{
	e.printStackTrace();
	try
	{
		connSapiens.rollback();
	}
	catch (SQLException e1)
	{
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
}
finally
{
	try
	{
		if (connSapiens != null)
		{
			connSapiens.close();
		}
		if (connSigma != null)
		{
			connSigma.close();
		}
		if (pstmSapiens != null)
		{
			pstmSapiens.close();
		}
		if (pstmSigma != null)
		{
			pstmSigma.close();
		}
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}
	System.out.println("TESTE ROTINA TELEFONES Fim" + TEMPO.getTime());


}

 %>