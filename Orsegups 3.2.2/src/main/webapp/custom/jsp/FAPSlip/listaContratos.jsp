<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/menu.tld" prefix="m"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>


<%	System.out.println("LISTA CONTRATOS " + NeoUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
	Long usuarioAtualNeoid = PortalUtil.getCurrentUser().getNeoId();
	
	QLGroupFilter qlGroupAND = new QLGroupFilter("AND");
	qlGroupAND.addFilter(new QLFilterIsNotNull("codContrato"));
	qlGroupAND.addFilter(new QLRawFilter("codContrato <> 30"));
	/*qlGroupAND.addFilter(new QLEqualsFilter("ativo", true));*/
	
	QLGroupFilter qlGroupOR = new QLGroupFilter("OR");
	qlGroupOR.addFilter(new QLRawFilter("_vo.neoId in (select d.neoId from D_FAPSlipCadastroContrato d join d.listaPermissao p where p.neoId = " + usuarioAtualNeoid + ")"));
	qlGroupOR.addFilter(new QLEqualsFilter("_vo.responsavelContrato.neoId", usuarioAtualNeoid));
	qlGroupAND.addFilter(qlGroupOR);
	
	List<NeoObject> listContratos = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlipCadastroContrato"), qlGroupAND, "ativo desc");
	Integer size = listContratos.size();
	System.out.println("LISTA CONTRATOS2 " + NeoUtils.safeDateFormat(new GregorianCalendar(), "hh:mm:ss"));
%>


<portal:head title="">
<cw:main>
	<cw:header title="" />
	<cw:body id="area_scroll">
		<table id="tlist_" menucontextid="null" class="gridbox" cellpadding="0" cellspacing="0">
		    <thead>
		        <tr>
		        	<th typename="numContrato" style="cursor:pointer;">
		                Num. Contrato&nbsp;
		            </th>
		        	<th typename="titulo" style="cursor:pointer;">
		                Título do Contrato&nbsp;
		            </th>
		            <th typename="criterioAprovacao" style="cursor:pointer;">
		                Critério de Aprovação&nbsp;
		            </th>
		            <th typename="valor" style="cursor:pointer;">
		                Valor
		            </th>
		            <th typename="fimVigencia" style="cursor:pointer;">
		                Fim Vigência&nbsp;
		            </th>
		            <th typename="EmpresaPagto" style="cursor:pointer;">
		                Empresa Pagto&nbsp;
		            </th>
		            <th typename="diaProvavelPagto" style="cursor:pointer;">
		                Dia Vencimento&nbsp;
		            </th>
		            <th typename="fornecedor" style="cursor:pointer;">
		                Fornecedor&nbsp;
		            </th>
		            <th typename="ativo" style="cursor:pointer;">
		                Ativo&nbsp;
		            </th>
		            <th typename="responsavelContrato" style="cursor:pointer;">
		                Responsável pelo Contrato&nbsp;
		            </th>
		        </tr>
		    </thead>
		
		    <script language="javascript">
		    function viewItemFusion(id)
		    {
		    	var title ="Cadastro de Contrato";
		    	var uid;
		    	var entityType = '';
		    	var idx = '';
		    	
		    	var portlet = new FloatForm(title, uid, entityType, id, 'ellist_', idx, 0, 0, '', null, null, false, false, null, false, '');
		    	portlet.open();
		    	
		    }
		    </script>
		
		    <tbody id="tblist_" name="tblist_" class="gridbox" style="">
		    	<% for (int x=0; x<size; x++) {
		    		NeoObject noCadastroContrato = listContratos.get(x);
		    		EntityWrapper wCadastroContrato = new EntityWrapper(noCadastroContrato);
		    		
		    		Boolean ativo = wCadastroContrato.findGenericValue("ativo");
		    		ativo = ativo == null ? false : ativo;
		    		String backgroundLinha = "inherit";
		    		if (ativo)
		    			backgroundLinha = "#eff4f9";
		    		
		    		String corLinha = "black";
		    		GregorianCalendar fimVigencia = wCadastroContrato.findGenericValue("fimVigencia");
		    		GregorianCalendar today = new GregorianCalendar();
		    		if (fimVigencia != null){
		    			Long diasDiferenca = (fimVigencia.getTimeInMillis() - today.getTimeInMillis()) / (1000l * 60l * 60l * 24l);
		    			if (diasDiferenca <= 30l){
		    				corLinha = "red";
		    			}
		    		}
		    		
		    		Long processNeoId = noCadastroContrato.getNeoId();
		    		WFProcess process = PersistEngine.getNeoObject(WFProcess.class, new QLEqualsFilter("entity.novoContrato", noCadastroContrato));
		    		if (process != null)
		    		{
		    			processNeoId = process.getNeoId();
		    		}
		    	%>
			        <tr id="0" uid="list_" entitytype="FAPSlipCadastroContrato" entityid="1157171963" idx="0" style="visibility: visible; color: <%= corLinha%>; background: <%= backgroundLinha%>">
			            <td class=""><a href="javascript:viewItemFusion(<%=processNeoId %>);" style="margin-right:1px;"><img class="tableIcon" src="imagens/icones_final/properties_16x16-trans.png" title="Visualizar" align="absmiddle"></a>
			            <%=wCadastroContrato.findGenericValue("codContrato") %> </td>
			            <td class=""><%=wCadastroContrato.findGenericValue("titulo") %> </td>
			            <td class=""><%=wCadastroContrato.findGenericValue("criterioAprovacao.descricao") %> </td>
			            <td class=""><%=wCadastroContrato.findGenericValue("valorBase") %> </td>
			            <td class=""><%=NeoUtils.safeDateFormat(fimVigencia, "dd/MM/yyyy") %> </td>
			            <td class=""><%=wCadastroContrato.findGenericValue("filial.numcgc") %> </td>
			            <td class=""><%=wCadastroContrato.findGenericValue("diaProvavelPagto") %> </td>
			            <td class=""><%=wCadastroContrato.findGenericValue("fornecedor.nomfor") %> </td>
			            <td class=""><%=ativo ? "<b>Sim</b>" : "Não" %> </td>
			            <td class=""><input type="hidden" id="var_responsavelContrato__"><%=wCadastroContrato.findGenericValue("responsavelContrato.name") %></td>
			        </tr>
				<%} %>
		    </tbody>
		</table>

	</cw:body>
	</cw:main>
</portal:head>


    



	