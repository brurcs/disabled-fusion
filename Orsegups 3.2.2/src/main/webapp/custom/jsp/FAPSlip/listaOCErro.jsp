<%@page import="com.neomind.fusion.workflow.ProcessState"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="br.com.senior.services.fap.OrdemcomprabuscarPendentes4OutOrdemCompra"%>
<%@page import="br.com.senior.services.fap.OrdemcomprabuscarPendentes4Out"%>
<%@page import="br.com.senior.services.fap.G5SeniorServicesLocator"%>
<%@page import="br.com.senior.services.fap.OrdemcomprabuscarPendentes4In"%>
<%@page import="br.com.senior.services.vetorh.constants.WSConfigs"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/menu.tld" prefix="m"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>


<%	
	WSConfigs configs = new WSConfigs();
	
	OrdemcomprabuscarPendentes4In buscaPendentesIn = new OrdemcomprabuscarPendentes4In();
	buscaPendentesIn.setCodUsu(50);
	
	G5SeniorServicesLocator locator = new G5SeniorServicesLocator();
	OrdemcomprabuscarPendentes4Out buscaPendentesOut = null;
	try
	{
		buscaPendentesOut = locator.getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort().buscarPendentes_4(configs.getUserService(), configs.getPasswordService(), 0, buscaPendentesIn);
	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	
	
		
%>


<portal:head title="">
<cw:main>
	<cw:header title="" />
	<cw:body id="area_scroll">
		<table id="tlist_" menucontextid="null" class="gridbox" cellpadding="0" cellspacing="0">
		    <thead>
		        <tr>
		        	<th typename="solicitante" style="cursor:pointer;">
		                Solicitante
		            </th>
		        	<th typename="dataOC" style="cursor:pointer;">
		                Data OC
		            </th>
		            <th typename="codOC" style="cursor:pointer;">
		                Cód OC
		            </th>
		            <th typename="situacao" style="cursor:pointer;">
		                Situação
		            </th>
		             <th typename="tarefa" style="cursor:pointer;">
		                Cód. Tarefa
		            </th>
		        </tr>
		    </thead>
		
		    <script language="javascript">
		    function viewItemFusion(id)
		    {
		    	var title ="Cadastro de Contrato";
		    	var uid;
		    	var entityType = '';
		    	var idx = '';
		    	
		    	var portlet = new FloatForm(title, uid, entityType, id, 'ellist_', idx, 0, 0, '', null, null, false, false, null, false, '');
		    	portlet.open();
		    	
		    }
		    </script>
		
		    <tbody id="tblist_" name="tblist_" class="gridbox" style="">
		    	<% if ((buscaPendentesOut.getErroExecucao() == null) || (buscaPendentesOut.getErroExecucao().trim().length() == 0))
		    	{
		    		OrdemcomprabuscarPendentes4OutOrdemCompra[] solicitacoes = buscaPendentesOut.getOrdemCompra();
					for (OrdemcomprabuscarPendentes4OutOrdemCompra solicitacao : solicitacoes)
					{
						if ((solicitacao.getCodigoEmpresa().equals(1)) || (solicitacao.getCodigoEmpresa().equals(30)))
						{
							String solicitante = solicitacao.getNomeSolicitante();
							String dataOC = solicitacao.getDataEmissao();
							Integer codOC = solicitacao.getNumero();
							String situacao = "Não Importado";
							String codTarefa = "Não Importado";
							
							QLGroupFilter qlGroup = new QLGroupFilter("AND", new QLEqualsFilter("ordemCompra.ordemCompra", codOC.longValue()),new  QLEqualsFilter("ordemCompra.empresa.codemp", solicitacao.getCodigoEmpresa().longValue()));
							List<NeoObject> listOrdensCompraCadastradas = PersistEngine.getObjects(AdapterUtils.getEntityClass("FAPSlip"), qlGroup, "neoId desc");
							if (listOrdensCompraCadastradas != null && !listOrdensCompraCadastradas.isEmpty())
							{
								for (NeoObject noOrdemCadastrada : listOrdensCompraCadastradas)
								{
									WFProcess wfProcess = new EntityWrapper(noOrdemCadastrada).findGenericValue("wfprocess");
									if (!wfProcess.getProcessState().equals(ProcessState.running))
										continue;
									
									codTarefa = wfProcess.getCode();
									situacao = "Reavaliação";
										
									List<Task> listTasks = wfProcess != null ? wfProcess.getAllPendingTaskList() : null;
									if (listTasks != null && !listTasks.isEmpty())
									{
										for (Task task : listTasks)
										{
											String taskName = task.getActivityName();
											if (taskName.contains("Aprovar"))
												situacao = "Aprovação";
											else if (taskName.contains("Reavaliar"))
												situacao = "Reavaliação";
											else if (taskName.contains("Integração"))
												situacao = "Integrando";
											else if (taskName.contains("Pré Aprovar"))
												situacao = "Em análise";
											
											break;
										}
										
										break;
									}
								}
							}
		    	%>
			        <tr id="0" uid="list_" entitytype="" entityid="" idx="0" style="visibility: visible;">
			            <td class=""><%=solicitante %> </td>
			            <td class=""><%=dataOC %> </td>
			            <td class=""><%=codOC%> </td>
			            <td class=""><%=situacao %> </td>
			            <td class=""><%=codTarefa %> </td>
			        </tr>
					<%	}
			    	}
				}%>
		    </tbody>
		</table>

	</cw:body>
	</cw:main>
</portal:head>


    



	