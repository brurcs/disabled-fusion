var app = angular.module('pagamentosApp', ['ui.bootstrap']);
app.controller('ctrl2', function($scope, $http) {
	$scope.total = 0;
	$scope.currentPage = 1;
	$scope.maxSize = 20;
	
    $scope.getPagamentos = function ()
	{
    	$scope.pagamentos = [];
		$http({
			method: 'GET', 
			url: '../../../services/slip/getPagamentos'
		}).success(function(data, status, headers, config) {
			if(data != null) {
				$scope.showLoading = false;
				$scope.pagamentos = data.pagamentos;
				$scope.total = data.total;
	    	}
    	}).error(function(data, status, headers, config) {
    		
		});
	}
    
    $scope.showLoading = true;
	$scope.column = 'dataVencimento';
	$scope.reverse = false;
    
    $scope.sortColumn = function(col){
    	$scope.column = col;
    	if($scope.reverse){
    		$scope.reverse = false;
    		$scope.reverseclass = 'arrow-up';
    	}else{
    		$scope.reverse = true;
    		$scope.reverseclass = 'arrow-down';
    	}
	};
	
	$scope.sortClass = function(col){
		if($scope.column == col ){
			if($scope.reverse){
				return 'arrow-down'; 
			} else {
				return 'arrow-up';
			}
		}else{
			return '';
		}
	}
    
    $scope.getPagamentos();
    
});