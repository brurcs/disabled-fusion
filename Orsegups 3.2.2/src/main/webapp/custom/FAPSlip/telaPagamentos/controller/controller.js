var app = angular.module('pagamentosApp', ['ui.bootstrap']);
app.controller('ctrl', function($scope, $http, $timeout) {
	$scope.total = 0;
	$scope.maxSize = 20;
	
    $scope.getPagamentos = function (page, successFunction)
	{
    	$scope.pagamentos = [];
		$http({
			method: 'GET', 
			url: '../../../services/slip/getPagamentos'
		}).success(function(data, status, headers, config) {
			if(data != null) {
				$scope.showLoading = false;
				$scope.pagamentos = data.pagamentos;
				$scope.total = data.total;
				
				successFunction();
	    	}
    	}).error(function(data, status, headers, config) {
    		
		});
	}
    
    $scope.showLoading = true;
	$scope.column = 'dataVencimento';
	$scope.reverse = false;
    
    $scope.sortColumn = function(col){
    	$scope.column = col;
    	if($scope.reverse){
    		$scope.reverse = false;
    		$scope.reverseclass = 'arrow-up';
    	}else{
    		$scope.reverse = true;
    		$scope.reverseclass = 'arrow-down';
    	}
	};
	
	$scope.sortClass = function(col){
		if($scope.column == col ){
			if($scope.reverse){
				return 'arrow-down'; 
			} else {
				return 'arrow-up';
			}
		}else{
			return '';
		}
	}
    
    $scope.getPagamentos(1, function(){
    	$timeout( function(){
    	var timelines = $('.cd-horizontal-timeline');
	
		(timelines.length > 0) && initTimeline(timelines);
        }, 1000 );
    });
    
    $scope.showTooltip = function(show, index, etapa){
    	var element = jQuery("#panel_"+etapa+index);
    	if (show)
    		element.css("visibility", "visible");
    	else
    		element.css("visibility", "hidden");
    }
    
    $scope.getStatusAtividade = function(isFinished)    {
    	if (isFinished)
    		return "success";
    	else
    		return "secondary"
    }
    
    /*$scope.$watch('currentPage + numPerPage', function() {
	    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
	    , end = begin + $scope.numPerPage;
	    
	    $scope.filteredTodos = $scope.todos.slice(begin, end);
	});*/    
});