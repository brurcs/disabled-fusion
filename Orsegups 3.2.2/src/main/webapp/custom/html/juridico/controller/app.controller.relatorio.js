app
		.controller(
				'appcontrollerrelatorio',
				[
						'$scope',
						'$http',
						'$timeout',
						'$interval',
						function($scope, $http, $timeout, $interval) {
							$scope.escRecs = function(){
								$scope.ac = 'action=getEscritorios';
								$http
										.get(encodeURI('https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.juridico.relatorio.servlet.ProcessosServlet?' + $scope.ac))
										.then(
												function(data) {
													if (data) {
														$scope.escri = data.data;
													} else {
														$
																.growl({
																	title : "Erro!",
																	message : "Deu ruim em algum lugar"
																});
													}
												});
							};
							$scope.escRecs();
							$scope.qtdProcessos = 0;
							$scope.label2 = 'Data de Geração de ';
							$scope.headerVal = 'Finalização';
							$scope.escritorio;
							$scope.selectStatus = [ {
								'id' : 1,
								'desc' : 'Finalizados',
								'dat1' : 'Data Citação de ',
								'dat2' : 'Data Finalização de ',
								'opt' : 'getAllFinalizados',
								'cabes' : 'Finalização'
							}, {
								'id' : 2,
								'desc' : 'Abertos',
								'dat1' : 'Data Citação de ',
								'dat2' : 'Data Geração de         ',
								'opt' : 'getAllAbertos',
								'cabes' : 'Data Geração'
							}, ]
							$scope.status;
							$scope.out = {
								type : $scope.selectStatus[0].value
							};
							$scope.path = 'https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.juridico.relatorio.servlet.ProcessosServlet?';
							$scope.buscar = function() {
								var resultado = $scope.validateForm();
								if (resultado) {
									$scope.isLoading = true;
									$http
											.get(encodeURI($scope.path))
											.then(
													function(result) {
														if (result) {
															$scope.dados = result.data;
															$scope.isLoading = false;
														} else {
															$
																	.growl({
																		title : "Erro!",
																		message : "Deu ruim em algum lugar"
																	});
														}
													});
								}
								$scope.path = 'https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.juridico.relatorio.servlet.ProcessosServlet?';
							}
							$scope.changedValue = function(item) {
								status = item.opt;
								$scope.label2 = item.dat2;
								$scope.headerVal = item.cabes;
								if (status == 'getAllAbertos'){
									document.getElementById("datepicker3").style.visibility = "hidden";
									document.getElementById("datepicker4").style.visibility = "hidden";
									document.getElementById("label3").style.visibility = "hidden";
									document.getElementById("label4").style.visibility = "hidden";
									document.getElementById("datepicker3").value = "";
									document.getElementById("datepicker4").value = "";

								}else {
									document.getElementById("datepicker3").style.visibility = "visible";
									document.getElementById("datepicker4").style.visibility = "visible";
									document.getElementById("label3").style.visibility = "visible";
									document.getElementById("label4").style.visibility = "visible";
								}
							}
							$scope.dateToString = function(item) {
								var day = item.getUTCDate();
								var month = item.getUTCMonth() + 1;
								var year = item.getUTCFullYear();
								return day + '/' + month + '/' + year;
							}
							$scope.validateForm = function() {
								if($('#status2').val()){
									var escRec = '&escRec=' + $scope.escritorio.nome.neoId;
								}
								
								if($scope.teste1){
									var dat1 = new Date($scope.teste1.getTime());
								}
								if($scope.teste2){
									var dat2 = new Date($scope.teste2.getTime());
								}
								if($scope.teste3){
									var dat3 = new Date($scope.teste3.getTime());
								}
								if($scope.teste4){
									var dat4 = new Date($scope.teste4.getTime());
								}

								if(dat1 && dat2){
									$scope.path += 'action=' + status;

									if(dat1.getTime() <= dat2.getTime()) {
									$scope.path += '&dat1=' + $scope.dateToString(dat1);
									$scope.path += '&dat2=' + $scope.dateToString(dat2);
									}else{
										$.growl({
											title : "Erro!",
											message : "Data de citação inicial deve ser menor que final"
										});
										return false;
									}
								}
								if(dat3 && dat4){
									if(!dat1 && !dat2){
										$scope.path += 'action=' + status;
									}
									if(dat3.getTime() <= dat4.getTime()) {
									$scope.path += '&dat3=' + $scope.dateToString(dat3);
									$scope.path += '&dat4=' + $scope.dateToString(dat4);
									}else{
										$.growl({
											title : "Erro!",
											message : "Data de finalização inicial deve ser menor que final"
										});
										return false;
									}
								}
								if((!dat3 && !dat4) && (!dat1 && !dat2)){
									$scope.path += 'action=' + status;
									if(escRec){
										$scope.path += escRec;
									}
									return true;
								}
								if(escRec){
									$scope.path += escRec;
								}
								return true;
							}
						}

				]);
