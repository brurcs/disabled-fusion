app.controller('appController', function($scope, $http, $location){

	var query = location.search.slice(1);
	var partes = query.split('&');
	var params = {};
	var referencias = undefined;
	
	partes.forEach(function (parte) {
		var chaveValor = parte.split('=');
		var chave = chaveValor[0];
		var valor = chaveValor[1];

		if(chave == "imagemCamera") {
			$scope.imgCamera = valor;
		} else if(chave == "referencias") {
			referencias = valor;
		}	
	});
		
	if(referencias != undefined) {
		var refs = referencias.split(',');
		$scope.ref1 = refs[0];
		$scope.ref2 = refs[1];
		$scope.ref3 = refs[2];
		$scope.ref4 = refs[3];
		$scope.ref5 = refs[4];
	}		
});