<%@page import="java.util.List"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.eform.EForm"%>
<%@page import="com.neomind.fusion.eform.EFormField"%>
<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>

<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>

<portal:head title="">
<form:defineObjects />
<%


boolean showUI = false;
boolean blackAndWhite = false;
int resolution = 200;

String localeCountry = "BR";
String localeLang = "pt";


%>

<!DOCTYPE html>

<base href="<%= PortalUtil.getBaseURL() %>" />
<link rel="shortcut icon" href="<%= PortalUtil.getBaseURL() %>fusion.ico" />

<!-- CSS -->

<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>/js/jquery/jquery.js.jsp"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>/js/jquery/jquery.jstree.js.jsp"></script>
<link rel="stylesheet" type="text/css" media="all"	href="<%=PortalUtil.getBaseURL()%>css/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="<%=PortalUtil.getBaseURL()%>custom/wkf/Batismo/css/themes/neomind/jquery-ui-tree.css" />
<link rel="stylesheet" type="text/css" media="all"  href="<%= PortalUtil.getBaseURL()%>custom/wkf/Batismo/scan/css/scan.css" />
<link rel="stylesheet" type="text/css" media="all"  href="<%= PortalUtil.getBaseURL()%>custom/wkf/Batismo/scan/css/imgareaselect-default.css" />
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/wkf/Batismo/scan/css/jquery.ui.slider.css">



<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>js/jquery/jquery.js.jsp"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>custom/wkf/Batismo/js/jquery/jquery-ui-core.js.jsp"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>js/header.js.jsp"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/form/form.js.jsp"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>js/neo_ui/neo.js"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>js/neo_ui/neo_utils.js"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>js/neo_ui/modal/neo_modal_window.js"></script>


<%

	request.setAttribute("content", false);
	
	final EFormField fieldEntity = eForm.getField("processEntity");
	final EForm eformEntity =  eForm.getChildForm(fieldEntity.getFieldCode());
	
	NeoObject object = eformEntity.getObject();
	

%>		
			
<body style="background:#FFF">

<script> 
$(document).ready(function() {

	openBatismo();
});

function openBatismo()
{

	window.open('<%= PortalUtil.getBaseURL() +  "custom/wkf/Batismo/tree.jsp?id="+object.getNeoId()%> ');	
}


</script>
<input type="button" class="input_button" title="Realizar Batismo de Documentos" name="Realizar Batismo de Documentos" value="Realizar Batismo de Documentos" onclick="openBatismo();">

	<form  id="<%=eForm.getFormId() %>" action='<%= PortalUtil.getBaseURL()+"portal/action/TaskContent/view/normal?callBackURL="+PortalUtil.getBaseURL()+"/portal/render/TasksListHead?code=myTasks"%>' neoform ="true" enctype="multipart/form-data" method="post" name="<%=eForm.getFormId() %>">
		<input type='hidden' name='eForm' name='eForm' value="<%= eForm.getId() %>" />
		<input type='hidden' name='id_processEntity' value="<%= eformEntity.getObject().getNeoId() %>" />
		<input type='hidden' name='has_processEntity' value="true" />
	
	
	
	<form:head/>	
	
	</form>
</body>

</portal:head>

	