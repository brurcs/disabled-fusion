<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.eform.EForm"%>
<%@page import="com.neomind.fusion.eform.EFormField"%>
<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>

<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>

<portal:head title="">
	<form:defineObjects />
	<%
		boolean showUI = false;
			boolean blackAndWhite = false;
			int resolution = 200;
			Long primeirodocumento = null;
			String localeCountry = "BR";
			String localeLang = "pt";

			Locale local = PortalUtil.getCurrentLocale();
			if (local != null)
			{
				localeCountry = local.getCountry();
				localeLang = local.getLanguage();
			} 
			
			String para = request.getParameter("id");
			
			NeoObject batismo = (NeoObject) PersistEngine.getNeoObject(new Long(para));
			
			NeoUser digitalizador = (NeoUser) new EntityWrapper(batismo).findValue("digitalizador");
			String nomeDigitalizador = digitalizador!= null? digitalizador.getFullName(): ""; 
			List<NeoObject> documentos = (List<NeoObject>) new EntityWrapper(batismo)
					.findValue("documentos");
			Map<String, List<NeoObject>> map = new HashMap<String, List<NeoObject>>();
			for (NeoObject documento : documentos)
			{
				EntityWrapper wdoc = new EntityWrapper(documento);
				String label = "";
				String nomeColaborador = (String)wdoc.findValue("colaborador.nomfun");
				String documentoBatismo = (String)wdoc.findValue("batismo");
				if (documentoBatismo == null || documentoBatismo.equalsIgnoreCase("Sem Batismo"))
				{
					label = "Sem Batismo";
				} 
				else 
				{
					label = documentoBatismo + " - " + nomeColaborador;
				}

				List<NeoObject> docs = map.get(label);
				if (docs == null)
				{
					docs = new ArrayList<NeoObject>();
				}
				docs.add(documento);
				map.put(label, docs);
			}
			
	%>

	<!DOCTYPE html>

	<base href="<%=PortalUtil.getBaseURL()%>" />
	<link rel="shortcut icon"
		href="<%=PortalUtil.getBaseURL()%>fusion.ico" />

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" media="all"
		href="<%=PortalUtil.getBaseURL()%>css/reset.css" />
	<%-- <link rel="stylesheet" type="text/css" media="all"
		href="<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/css/themes/neomind/jquery-ui-tree.css" /> --%>
	<link rel="stylesheet" type="text/css" media="all"
		href="<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/css/scan.css" />
	<link rel="stylesheet" type="text/css" media="all"
		href="<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/css/imgareaselect-default.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/css/jquery.ui.slider.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/form_tags.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/form.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/login.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/core.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/header.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/window_default.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/window.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/buttons.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/search.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/titles.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/grid.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/forms_list.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/tabs.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/portal.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/portal-decorator.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/jquery.treeview.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/screen.css">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>css/themes/neomind/neo.ui.css.jsp">
	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/dist/themes/default/style.min.css" />

	<%-- <script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/js/jquery/jquery-ui-core.js.jsp"></script>  --%>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>js/header.js.jsp"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>core/form/form.js.jsp"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>js/neo_ui/neo.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>js/neo_ui/neo_utils.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>js/neo_ui/modal/neo_modal_window.js"></script>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/dist/libs/jquery.js"></script>
	
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/dist/jstree.js"></script>

	<script src="<%=PortalUtil.getBaseURL()%>core/autocomplete.js.jsp"></script>
	

	<body style="background: #FFF">
		<form id="<%="s"%>"
			action='<%=PortalUtil.getBaseURL() + "portal/action/TaskContent/view/normal?callBackURL="
						+ PortalUtil.getBaseURL() + "/portal/render/TasksListHead?code=myTasks"%>'
			neoform="true" enctype="multipart/form-data" method="post"
			name="<%=123%>">
			<input type='hidden' name='eForm' name='eForm'
				value="<%=21%>" /> <input type='hidden'
				name='id_processEntity'
				value="<%=324%>" /> <input
				type='hidden' name='has_processEntity' value="true" />

			<!-- MENU -->
			<div>
				<!-- Toolbar -->
				<div id='scan_toolbar'>
					<table id='tb_menu' class='tbmenu'>
						<tr>


							<!-- Separator -->
							<td style="text-align: center; vertical-align: middle;"><img
								src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/separator.png'
								class='sep_icon' /></td>


							<!-- Importar -->
							<td
								style="text-align: center; vertical-align: middle; visibility: visible;"
								onclick='unificarBatismo();'><a
								id="a-import-button" class='neo-button'> <img
									id="img-import-button"
									src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/import.png'
									title="Unificar Processo de Batismo" />
							</a></td>
			
							
							<td
								style="text-align: center; vertical-align: middle; visibility: visible;"
								onclick='solicitarNova();'><a
								id="a-restore-button" class='neo-button'> <img
									id="img-restore-button"
									src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/restore.png'
									title="Solicitar Nova Digitaliza��o" />
							</a></td>
							
						

							<!-- Separator -->
							<td style="text-align: center; vertical-align: middle;"><img
								src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/separator.png'
								class='sep_icon' /></td>

							<!-- Batismo -->
							<td
								style="text-align: center; vertical-align: middle; visibility: visible;"
								onclick='showBatismo()'><a
								id="a-baptism-button" class='neo-button'> <img
									id="img-baptism-button"
									src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/batismo.png'
									title="<%=I18nUtils.getString("baptism")%>" />
							</a></td>

							<!-- Excluir Pg. onclick='TreeActions.Nodes.remove();' -->
							<td
								style="text-align: center; vertical-align: middle; visibility: visible;"
								onclick='remover();'><a
								id="a-delete-button" class='neo-button'> <img
									id="img-delete-button"
									src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/excluir.png'
									title="<%=I18nUtils.getString("delete")%>" />
							</a></td>

							<!-- Move p/ cima 
							<td
								style="text-align: center; vertical-align: middle; visibility: visible;"
								onclick='loadIframe();'><a
								id="a-moveup-button" class='neo-button'> <img
									id="img-moveup-button"
									src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/subir.png'
									title="<%=I18nUtils.getString("moveup")%>" />
							</a></td>
-->
							<!-- Move p/ baixo
							<td
								style="text-align: center; vertical-align: middle; visibility: visible;"
								onclick='TreeActions.Nodes.moveDown();'><a
								id="a-movedown-button" class='neo-button'> <img
									id="img-movedown-button"
									src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/descer.png'
									title="<%=I18nUtils.getString("movedown")%>" />
							</a></td>
							 -->
							<!-- Visualizar Selecionados -->
							<td
								style="text-align: center; vertical-align: middle; visibility: visible;"
								onclick='visualizarDocumentos();'><a
								id="a-movedown-button" class='neo-button'> <img
									id="img-movedown-button"
									src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/autocomplete_search2.png'
									title="Visualizar documentos selecionados" />
							</a></td>
							<!-- Separator -->
							<td style="text-align: center; vertical-align: middle;"><img
								src='<%=PortalUtil.getBaseURL()%>custom/wkf/A028 - Batizar Documentos/scan/imagens/icones/separator.png'
								class='sep_icon' /></td>
								
							<td style="text-align: center; vertical-align: middle;">
								<span style="font: italic;"><%= "Digitalizador : " + nomeDigitalizador %></span>
							</td>
						</tr> 
					
					</table>
				</div>
				
				<input type="button" class="input_button" name="addListItemDossie" value='SELECIONAR TODOS' onclick="javascript:selecionarTodos();" />
				<input type="button" class="input_button" name="addListItemDossie" value='DESSELECIONAR TODOS' onclick="javascript:desselecionarTodos();" />
				
				
				<div id = 'div_unificar' style="display: none; text-align: center; vertical-align: middle; visibility: visible;background-color:#EFF4F9; width: 500px;">
						<fieldset class="fieldGroup">
						
							<div class="fieldSet" id="top_container">
								<br>
								<li>
									<label class="formFieldLabel"><a>Processo: </label>
									<input class='input_text' name='id_txt_process__' id='id_txt_process__'	required='true' onfocus="activeDeactiveObj(this)"	onblur="activeDeactiveObj(this)" autocomplete="off" value="" />
								</li>
								<li>
									<fieldset class='edit_buttons' id='dibButtons' width="100%" >
										<input type="button" class="input_button" name="addListItemDossie" value='Salvar' onclick="javascript:salvaUnificar();" />
										<input type="button" class="input_button" name="addListItemDossie" value='Fechar' onclick="javascript:fechaJanela1();" />
									</fieldset>
								</li>
							</div> 
						
						</fieldset>	
					</div>
					<div class="fieldSet" id = 'div_batismo' style="display: none; text-align: center; vertical-align: middle; visibility: visible;background-color:#EFF4F9; width: 500px;">
						<fieldset class="fieldGroup">
						<ol>
									<div class="fieldSet">
										<br>
										<li>
											<label class="formFieldLabel">Colaborador:&nbsp&nbsp&nbsp</label>
											<input class='input_text' name='id_txt_colabo__' id='id_txt_colabo__' required='true' onfocus="activeDeactiveObj(this)"	onblur="activeDeactiveObj(this)" value="" />
											<input id='id_colaborador__' type='hidden'	value="" />
											<SCRIPT type="text/javascript">	
												createAutoComplete(myId('id_colaborador__'), 'id_txt_colabo__', 'VETORHUSUFUNFUSION', 'numcad;nomfun',0);
											</SCRIPT>
											
										</li>
										<li>
											<label class="formFieldLabel">Documento: </label> 
											<input class='input_text' name='idDocumento' id='idDocumento'	required='true' onfocus="activeDeactiveObj(this)"	onblur="activeDeactiveObj(this)" value="" />
											<input id='id_documento__' type='hidden'	value="" />
											<SCRIPT type="text/javascript">	
												createAutoComplete(myId('id_documento__'), 'idDocumento', 'DocumentEntityInfo', 'title', 0);
											</SCRIPT>
											
										</li>
										
										<li id="fieldStartDate">
							                <label class="formFieldLabel">M&ecirc;s/Ano:</label>
							                
	                						<input class="input_text" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)" name="var_activity.process.startDate_ini" errorspan="err_var_activity.process.startDate_ini" id="var_activity.process.startDate_ini" mandatory="false" onchange="validate(&quot;var_activity.process.startDate_ini&quot;,&quot;err_var_activity.process.startDate_ini&quot;, this.value, &quot;/^((0?[1-9]|[12]\\d)\\/(0?[1-9]|1[0-2])|30\\/(0?[13-9]|1[0-2])|31\\/(0?[13578]|1[02]))\\/(19|20)?\\d{2}$/&quot;, &quot;Valor Inv&aacute;lido&quot;)"><span id="err_var_activity.process.startDate_ini" style="margin-left:0px;"></span>&nbsp;<img src="imagens/icones_final/date_16x16-trans.png" class="ico" id="btCalvar_activity.process.startDate_ini" onclick="return showNeoCalendar('var_activity.process.startDate_ini', '%d/%m/%Y','btCalvar_activity.process.startDate_ini',event);">
	           							</li>
										<li>
											<fieldset class='edit_buttons' id='dibButtons' width="100%" >
												<input type="button" class="input_button" name="addListItemDossie" value='Salvar' onclick="javascript:salvaBatismo();" />
												<input type="button" class="input_button" name="addListItemDossie" value='Fechar' onclick="javascript:fechaJanela();" />
											</fieldset>
										</li>
									</div> 
								</ol>
						</fieldset>	
						</div>
				<br>
				<div>
					<fieldset class="fieldGroup">
						<!-- JANELA DE VISUALIZA��O DE DOCUMENTO - TELA DIREITA 	-->
						<div id='div-scan-pages'
							style='width: 50%; display: table; border: none; float: right; background-color: #FFFFFF; border: 0px solid #333333; overflow-y: scroll; overflow-x: auto; height: 100%; text-align: center;'>
						</div>
						
	 						<!-- TREEVIEW - TELA ESQUERDA -->
						<div id='tree_containerId' class='tree_container'
							style='width: 40%; height:500px; display: table; overflow-y: scroll; border: none; float: left; display: block;'>
							<div id="jstree" >
								<ul id='treeroot'>
							<%
								Set<Entry<String, List<NeoObject>>> entrySet = map.entrySet();
					 			for (Entry<String, List<NeoObject>> entry : entrySet)
								{
									String labelBatismo = (String) entry.getKey();
									
									if(labelBatismo.equalsIgnoreCase("excluido"))
										continue; 
									List<NeoObject> listaD = entry.getValue();
									 
									%>
									<li valor= '<%=labelBatismo %>'  ><%=labelBatismo %>
										<ul valor="<%=labelBatismo %>">
											<% 
												for(NeoObject doc : listaD ) 
												{
													String nome = "Colaborador n�o selecionado";
													long neoidfile = 0;
													NeoFile neofile =  (NeoFile) new EntityWrapper(doc).findValue("arquivo");
													NeoObject colaborador =  (NeoObject) new EntityWrapper(doc).findValue("colaborador");
													if(neofile != null)
													{
														neoidfile = neofile.getNeoId();
														if(primeirodocumento == null) 
															primeirodocumento = neoidfile ; 
													} 
													
													if(colaborador != null)
													{
														nome = colaborador.getAsString();
													}
											%>
											<li  doc =  '<%=neoidfile %>'  valor = '<%=new EntityWrapper(doc).findValue("neoId") %>' title='<%=nome %>' ><%=new EntityWrapper(doc).findValue("arquivo") %></li>
											<% } %>
										</ul>
									</li>
								<%
									}
								%>
									
								</ul>
							</div>
							
						</div>
					 
						<div id="iframe_container" style="width: 60%; height: 100%;  float: left;">
							<iframe id="iframe_visualiza" src='<%= PortalUtil.getBaseURL()+"ged/viewer.jsp?docs="+primeirodocumento+"&isNeoFile=true&showContainer=true"%>' style="width: 100%; height: 90%;">
							</iframe>
						</div>
					 
				</div>
				</fieldset>

				<script>
				
						$(document).ready(function() 
						{
								buildTree();
						});
						
						
						function buildTree() {
							// 6 create an instance when the DOM is ready
							$('#jstree').jstree({
								"core" : {
									"themes" : {
										"variant" : "large"
									}
								},
								"checkbox" : {
									"keep_selected_style" : false,
									"whole_node": false
								},
								"plugins" : [ "checkbox", "crrm" ]
							});
							// 7 bind to events triggered on the tree
							$('#jstree').on("changed.jstree", function(e, data) {
								console.log(data.selected);
							});
							$('#jstree').on("click.jstree", function( e , data) 
							{
								console.log(e);
								console.log(data);
									
								
							});
							// 8 interact with the tree - either way is OK
							$('button').on(
									'click',
									function() {
										$('#jstree').jstree(true).select_node(
												'child_node_1');
										$('#jstree').jstree('select_node',
												'child_node_1');
										$.jstree.reference('#jstree').select_node(
												'child_node_1');
									});

						}
				
					function selecionarTodos()
					{
						var ref = $('#jstree').jstree(true);
						ref.select_all();	
					}
					
					function desselecionarTodos()
					{
						var ref = $('#jstree').jstree(true);
						ref.deselect_all();	
					}
				
					function unificarBatismo()
					{
						$("#div_unificar").css("display", "block");
					}
					
					function salvaUnificar()
					{
						var base = '<%=PortalUtil.getBaseURL()%>';
					    
						var processo = document.getElementById("id_txt_process__").value;
						var entity = '<%=para%>';
						if(processo != -1 )
						{
							var url = base +"servlet/com.neomind.fusion.webservice.autostore.OrsegoupsAutoStoreWS?action=unificarBatismo&processo="+processo+"&entity="+entity;
						
							callFunction (url, "callbackSalvarUnificar");
						}
					
						fechaJanela1();
					}
					
					function callbackSalvarUnificar(ok, response)
					{
						if(ok)
						{
							if(response == '0')
							{
								location.reload();
							}else if(response == '2')
							{
								alert('O Processo selecionado � inv�lido.');
							}
						}
						
					}
					
					
					function visualizarDocumentos()
					{
						
						var ref = $('#jstree').jstree(true);
						sel = ref.get_selected();
						
						
						if(!sel.length) 
						{
							return;
						}
						
						var arquivos = ''; 
						var size = sel.length;
						for( x = 0 ; x < size; x++ )
						{
							if (sel[x] != null && sel[x] != "" && sel[x] != 'null')
						 	{
								 
								var arq = document.getElementById(sel[x] ).getAttribute ('doc');
								arquivos = arquivos+','+arq;
							}
						}
						loadIframe(arquivos);
					}
				
					function loadIframe(docs) 
					{ 
						
					    var $iframe = $('#iframe_visualiza');
					    if ( $iframe.length ) 
					    {
					    	var base = '<%=PortalUtil.getBaseURL()%>';
					    	var url = base + 'ged/viewer.jsp?isNeoFile=true&docs='+docs+'&showContainer=true'; 
					        $iframe.attr('src',url);
					    }
					} 
					
					

					
					
					

					
					function create(label) 
					{
						var ref = $('#jstree').jstree(true);
						sel = ref.get_selected();
						if (!sel.length) {
							return false;
						}
						sel = sel[0]; 
						sel = ref.create_node(null, 
						{
							"type" : "file", 
							"text" : label
						});
						if (sel) {
							ref.edit(sel);
						}
						return sel;
					}

				
					function remover() 
					{
						var base = '<%=PortalUtil.getBaseURL()%>'; 
						var r = confirm("Tem certeza de que deseja excluir?");
						if (r == true) 
						{
							var arquivos = getArquivosSelecionados();
							var url = base +"servlet/com.neomind.fusion.webservice.autostore.OrsegoupsAutoStoreWS?action=excluirArquivo&arquivos="+arquivos;
							callFunction (url); 
 
							var ref = $('#jstree').jstree(true);
							sel = ref.get_selected();
	
							if (!sel.length) {
								return false;
							}
							ref.delete_node(sel);
							
							
						} 
						
					}
					
					function showBatismo() 
					{
						
						var ref = $('#jstree').jstree(true);
						sel = ref.get_selected();
						if(!sel.length) 
						{
							alert('Por favor, selecione a(s) p�gina(s) que deseja batizar!');
							return;
						}
						 
						$("#div_batismo").css("display", "block");
				
					}
				
					function fechaJanela() 
					{
						$("#div_batismo").css("display", "none");
					}
					
					function fechaJanela1() 
					{
						$("#div_unificar").css("display", "none");
					}
					
					function solicitarNova()
					{
						var base = '<%=PortalUtil.getBaseURL()%>';
						var arquivos = getArquivosSelecionados();
						
						var nomedocumento = "Solicitar Nova Digitaliza��o";
						
						moveDocumento(nomedocumento, arquivos); 
						
						var url = base +"servlet/com.neomind.fusion.webservice.autostore.OrsegoupsAutoStoreWS?action=solicitarNovaDigitalizacao&arquivos="+arquivos;
						callFunction (url);
					}
					
					function salvaBatismo() 
					{
						$("#div_batismo").css("display", "none");
						var base = '<%=PortalUtil.getBaseURL()%>';
						var arquivos = getArquivosSelecionados();
						
						var colaborador = document.getElementById("id_colaborador__").value;
						var nomecolaborador = document.getElementById("id_txt_colabo__").value;
						var documento = document.getElementById("id_documento__").value;
						var nomedocumento = document.getElementById("idDocumento").value;
 
						
						if(documento != -1 && nomedocumento.trim().length > 0)
						{
							label = nomedocumento + " - " + nomecolaborador;
							
							moveDocumento(label, arquivos); 
						}
						var url = base +"servlet/com.neomind.fusion.webservice.autostore.OrsegoupsAutoStoreWS?action=salvarBatismo&documento="+documento+"&colaborador="+colaborador+"&arquivos="+arquivos;
						callFunction (url)
					
						if(nomecolaborador!= null && typeof nomecolaborador != 'undefined')
						{
							var ref = $('#jstree').jstree(true);
							sel = ref.get_selected();
							if(!sel.length) 
							{
								return;
							}
							
							var size = sel.length;
							for( x = 0 ; x < size; x++ )
							{
								if (sel[x] != "" && sel[x] != null  )
								{
									$('#'+sel[x] ).attr('title', nomecolaborador) ;
								}
							}
						}
					}
		 		 
					
					function moveDocumento(documento, arquivos)
					{
						var el1 = $("li[valor='"+documento+"']");
						
						var id = el1.attr('id') ;

					
						var nodeParent = null;
						
						if(id == null && typeof id == 'undefined' )
						{
							nodeParent  = create(documento);	
							$('#'+nodeParent).attr('valor', documento);
							 
						}else
						{
							var ref = $('#jstree').jstree(true);
							nodeParent  = ref.get_node(id);
						}
						 
						var list = arquivos.split(";");
						var x; 
						var size = list.length; 
						 
						for( x = 0 ; x < size; x++ )
						{
							var arquivo = list[x];
							if ( arquivo != null && arquivo != "null" && arquivo != "" && !isNaN(arquivo))
							{
								move_tree(nodeParent , arquivo);
							}
						} 
					}
					
					// Adiciona um n� pai na tree.
					function move_tree(pai , filho) 
					{
						var ref = $('#jstree').jstree(true);
						var el2 = $("li[valor='"+filho+"']");
						ref.move_node( ref.get_node(el2.attr('id')), pai); 

					}
					
					function getArquivosSelecionados()
					{
						var ref = $('#jstree').jstree(true);
						sel = ref.get_selected();
						if(!sel.length) 
						{
							return;
						}
						
						var arquivos = ''; 
						var size = sel.length;
						for( x = 0 ; x < size; x++ )
						{
							if (sel[x] != "" && sel[x] != null  )
							{
								var arq = document.getElementById(sel[x] ).getAttribute ('valor');
							 	arquivos = arquivos+";"+arq;
							}
						}
						return arquivos;
					}
					
					function callbackBatismo(ok, response)
					{
						if(ok)
						{
							var el1 = $("li[valor='"+reponse+"']");
							if (el1 == null)
							{
								
							}
						}
					}
				
					
				</script>
			</div>

			</div>


		</form>
	</body>

</portal:head>


