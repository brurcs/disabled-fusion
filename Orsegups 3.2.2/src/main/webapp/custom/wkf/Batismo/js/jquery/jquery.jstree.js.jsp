<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
	
<%@ page import="com.neomind.util.CustomFlags"%>

<c:if test="<%= CustomFlags.DEVELOPMENT %>">
	<jsp:include page="/js/jquery/jquery.jstree.js" />
</c:if>
<c:if test="<%= !CustomFlags.DEVELOPMENT %>">
	<jsp:include page="/js/jquery/jquery.jstree.min.js" />
</c:if>

<%--
NEO.UI.TREE=function(){
	return{
		getCheckeds: function(treeSelector)
		{
			$(treeSelector).jstree('get_checked',null,true);
		},
		getSelected: function(treeSelector)
		{
			$.jstree._reference(treeSelector).get_selected();
		},
		deleteNode: function(treeSelector, id)
		{
			$.jstree._reference(treeSelector).delete_node(id);
		},
		renameNode: function(treeSelector, id, name)
		{
			$.jstree._reference(treeSelector).rename_node(id, name);
		},
		createNode: function(treeSelector, id, name)
		{
			$(treeSelector).jstree("create",null,'inside',{attr:{id: id,name: name},state:'close',data:name},true);
		},
		refresh: function(treeSelector)
		{
			$(treeSelector).jstree('refresh');
		}
	};
}();
--%>
