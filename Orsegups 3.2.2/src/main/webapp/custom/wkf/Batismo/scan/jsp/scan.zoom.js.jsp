<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
Baseado no Zoom do Viewer @reinaldo
--%>

var Zoom = {
		min: 10, // Zoom M�nimo
		max: 500, // Zoom m�ximo
		scale: 10, // Scala do zoom
		current: 100, // Zoom corrente
		imageCache: {},
		
		hasCachedImage: function(id)
		{
			if (Zoom.imageCache[id] != undefined)
				return true;
			else return false;
		},
		
		putImageInCache: function(id, bytes64)
		{
			Zoom.imageCache[id] = bytes64;
		},
		
		getImageFromCache: function(id)
		{
			return Zoom.imageCache[id];
		},
		
		scaleValue: function(value, toZoom)
	    {
	    	if(!toZoom)
	    		toZoom = this.current;
	        return parseFloat(value) * parseFloat(toZoom) / 100;
	    },

	    descaleValue: function(value, fromZoom)
	    {
	    	if(!fromZoom)
	    		fromZoom = this.current;
	        return parseFloat(value) * 100 / parseFloat(fromZoom);
	    },

	    roundDecimalUnit: function()
	    {
			this.current = Math.round(this.current / 10) * 10;
	    },

	    getFitScale: function(canvas)
	    {
	    	if(!canvas)
	    		canvas = document.getElementById(Scan.getCurrentViewingCanvasId());

	    	var pw = $(canvas).attr("source_width");
	    	var ph = $(canvas).attr("source_height");
	    	
	    	var container = Scan.getContainer();
	    	var refWidth = container.width();
	    	var refHeight = container.height();
	    	
	    	var w = refWidth - ($(canvas).outerWidth(true) - canvas.width) ;
	    	var h = refHeight - ($(canvas).outerHeight(true) - canvas.height);

	    	var delta = 1;
	    	
	    	// Valida eixo com maior propor��o que ser� usado como base no c�lculo 
	    	if( (pw - w) > (ph - h) )
	    	{
	    		delta = w / pw;
	    	}
	    	else
	    	{
	    		delta = h / ph;
	    	}

	    	return (delta * 100).toFixed(2);
	    	
	    },
	    zoom : function(value)
		{
			if(value == "fit")
			{
				this.zoomTo(this.getFitScale());
			}
			else if(value == "full")
			{
				Zoom.zoomTo(100);
			}
			else if(value == "plus")
			{
				this.roundDecimalUnit();
				if(Zoom.current + Zoom.scale <= Zoom.max)
					Zoom.zoomTo(Zoom.current + Zoom.scale);
			}
			else if(value == "minus")
			{
				this.roundDecimalUnit();
				if(Zoom.current - Zoom.scale >= Zoom.min)
					Zoom.zoomTo(Zoom.current - Zoom.scale);
			}
			else
			{
				// Melhoria: Permitir setar o zoom.
			}
		},
		<%--
			Aplica o zoom no canvas atual.
		--%>
		zoomTo : function(p)
		{
			if(!p)
				return
		
			
			var canvas = document.getElementById(Scan.getCurrentViewingCanvasId());
			var zoom_value = $(canvas).attr('zoom_value');
	 		if (canvas)
	 		{
	 			var img64 = canvas.toDataURL("image/jpeg");
	 			if (zoom_value != undefined && zoom_value == 100)
	 			{
	 				if (!Zoom.hasCachedImage(Scan.getCurrentViewingCanvasId()))
	 				{
	 					Zoom.putImageInCache(Scan.getCurrentViewingCanvasId(), img64);
	 				}
	 			}

	 			Zoom.current = p;
				Zoom.triggerZoomEvent(p);
	 			Zoom.zoomPageTo(canvas, p);	
			}
		},
		zoomPageTo : function(canvas, z)
		{
			if(!z)
				z = this.current;

			var oldHeight = canvas.height;
			var oldWidth = canvas.width;
			var newHeight = (z == 100) ? parseInt($(canvas).attr("source_height")) : Math.round(parseInt($(canvas).attr("source_height")) * z / 100)
			var newWidth = (z == 100) ? parseInt($(canvas).attr("source_width")) : Math.round(parseInt($(canvas).attr("source_width")) * z / 100)
			
			CanvasManager.drawZoomedImage(canvas, Scan.tempFolder + Scan.getCurrentViewingCanvasId(), newWidth, newHeight);
		},
		<%--
			Atualiza posicionamento da barra de rolagem vertical ap�s o zoom.
		--%>
		updateScrollTop : function (container, img, oldHeight, newHeight)
		{
			var fold = container.offset().top;
			
				// S� recalcular scrolltop para as imagens acima da posi��o atual...
				if(fold >= img.offset().top + img.height())
				{
					var p = (newHeight - oldHeight) + container.scrollTop();
					Log.log("[Zoom][updateScrollTop] old: " + oldHeight + " new: " + newHeight + " scrollTop: " + container.scrollTop() + " -> " + p);
					container.scrollTop(p);
					
				}
			
			// FIXME - Calcular um scroll adicional relativo a imagem que est� parcialmente exibida...
		},
		<%--
			Atualiza posicionamento da barra de rolagem horizontal ap�s o zoom.
		--%>
		updateScrollLeft : function (target, oldWidth, newWidth)
		{
			// FIXME - Corrigir scroll apenas da imagem selecionada
			//Log.log("[Zoom][updateScrollLeft] old: " + oldWidth + " new: " + newWidth + " scrollLeft: " + target.scrollTop() + " -> " + p);
		},
		<%--
			Atualiza o zoom da p�gina com o zoom corrente. Utilizado ap�s carregar a imagem.

			@param page A imagem.
		--%>
		update : function(page)
		{
			// Atualiza a p�gina corrente com o zoom corrente
			if(this.current && this.current > 0)
			{
				this.zoomPageTo(page, this.current);
			}
			// Sen�o inicializa o zoom da primeira p�gina
			else
			{
				<%-- Apenas a primeira p�gina inicia o zoom --%>
				if(page.attr("page") == "1")
				{
					this.current = this.getFitScale(page);
					
					Zoom.triggerZoomEvent(this.current);

					Log.log("[Zoom][update] page " + page.attr("id") + " start zoom: " + this.current);
				}
				<%-- FIXME - Monitorar o tamanho das p�ginas que abrem antes da primeira, pra validar se precisam de zoom --%>
			}
		},
		<%-- --%>
		registerPageZoomEvent : function (f)
	    {
	    	$("body").bind("PageZoomEvent", f);
	    },
	    <%-- Evento executado ap�s cria��o da �rea de visualiza��o das p�ginas --%>
	    triggerPageZoomEvent : function (page)
	    {
	    	$("body").trigger("PageZoomEvent", [page]);
	    },
	    registerZoomEvent : function (f)
	    {
	    	$("body").bind("ZoomEvent", f);
	    },
	    triggerZoomEvent : function (value)
	    {
	    	$("body").trigger("ZoomEvent", value);
	    }
};