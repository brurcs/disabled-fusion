<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

var Menu = {
	    toggle: function(elId, el)
	    {
	    	var menuRef = $('#'+elId);
	    	if (menuRef.length)
	    	{
	    		if ($(el).length)
	    		{
	    			var pressed = $(el).attr('pressed');
	    			if (pressed != undefined && pressed == 'false')
	    			{
	    				$(el).attr('pressed', 'true').addClass('neo-button-on');
	    			}
	    			else if (pressed != undefined && pressed == 'true')
	    			{
	    				$(el).attr('pressed', 'false').removeClass('neo-button-on');
	    			}
	    		}
	    		
	    		menuRef.toggle(400, function() {
	    			var disp = $(this).css('display');
	    			if (disp != 'block') // Esconde
	    			{
	    				$(el).attr('pressed', 'false').removeClass('neo-button-on');
	    				$('#scan_layout_right').css('left','0px');
	    				Scan.Page.repaint();
	    			}
	    			else // Exibe
	    			{
	    				$('#scan_layout_right').css('left','18%');
	    				Scan.Page.repaint();
	    			}
	    		});
	    		
	    		
	    	}
	    }
};