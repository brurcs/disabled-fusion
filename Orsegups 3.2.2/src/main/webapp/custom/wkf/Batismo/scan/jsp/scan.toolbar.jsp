<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>scan/jsp/scan.toolbar.js.jsp"></script>
<title>Insert title here</title>
</head>
<body>
	<div id='scan_toolbar'>
		<table id='tb_menu' class='tbmenu'>
			<tr>
				<!-- Abrir/fechar sidebar -->
				<td style="text-align: center; vertical-align: middle;">
				<a id='a-showhide-button' class='neo-button' style="margin-left: 2px;" onclick="Menu.toggle('scan_layout_left', this);" pressed='false'>
					<img id="imgMenuButton" style='width: 16px; height: 16px;' src='<%=PortalUtil.getBaseURL()%>imagens/icones_final/menu.png' title="<%=I18nUtils.getString("showMenu")%>" />
				</a>
				</td>
				
				

				<!-- Separator -->
				<td style="text-align: center; vertical-align: middle;" >
					<img src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/separator.png' class='sep_icon' />
				</td>
					
					
				<!-- Importar -->
				<td style="text-align: center; vertical-align: middle; visibility: visible;" onclick='ScanWindows.ImportDialog.open();'>  
				<a id="a-import-button" class='neo-button'>
					<img id="img-import-button" src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/import.png' title="<%=I18nUtils.getString("importFiles")%>" />
				</a>
				</td>
						
				<!-- Restaurar -->
				<td style="text-align: center; vertical-align: middle; visibility: visible;" onclick='ScanWindows.RestoreDialog.show(this);'>  
				<a id="a-restore-button" class='neo-button'>
					<img id="img-restore-button" src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/restore.png' title="<%=I18nUtils.getString("restoreScannedDocuments")%>" />
				</a>
				</td>
				
				<!-- Separator -->
				<td style="text-align: center; vertical-align: middle;" >
					<img src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/separator.png' class='sep_icon' />
				</td>
				
				<!-- Batismo -->
				<td style="text-align: center; vertical-align: middle; visibility: visible;" onclick='ScanWindows.BaptismPopup.toggle(this, true);'>
				<a id="a-baptism-button" class='neo-button'>
					<img id="img-baptism-button" src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/batismo.png' title="<%=I18nUtils.getString("baptism")%>" />
				</a>
				</td>
				
				<!-- Excluir Pg. -->
				<td style="text-align: center; vertical-align: middle; visibility: visible;" onclick='TreeActions.Nodes.remove();'>
				<a id="a-delete-button" class='neo-button'>
					<img id="img-delete-button" src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/excluir.png' title="<%=I18nUtils.getString("delete")%>" />
				</a>
				</td>
				
				<!-- Move p/ cima -->
				<td style="text-align: center; vertical-align: middle; visibility: visible;" onclick='TreeActions.Nodes.moveUp();'>
				<a id="a-moveup-button" class='neo-button'>
					<img id="img-moveup-button" src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/subir.png' title="<%=I18nUtils.getString("moveup")%>" />
				</a>
				</td>
				
				<!-- Move p/ baixo -->
				<td style="text-align: center; vertical-align: middle; visibility: visible;" onclick='TreeActions.Nodes.moveDown();'>
				<a id="a-movedown-button" class='neo-button'>
					<img id="img-movedown-button" src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/descer.png' title="<%=I18nUtils.getString("movedown")%>" />
				</a>
				</td>
				
				<!-- Separator -->
				<td style="text-align: center; vertical-align: middle;" >
					<img src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/separator.png' class='sep_icon' />
				</td>
				

				<td style="text-align: center; vertical-align: middle; padding-right: 80px;" width=100%>
					<center>
						<table id='scan_nav_bar' style='display: none;'>
							<tr>
								<td style="text-align: center; vertical-align: middle;">
								<a class='neo-button'><img src='<%=PortalUtil.getBaseURL()%>imagens/icones_final/first.png' onclick='Navigation.first()'
										title="<%=I18nUtils.getString("firstPage")%>" />
								</a>
								</td>

								<td style="text-align: center; vertical-align: middle;">
								<a class='neo-button'><img src='<%=PortalUtil.getBaseURL()%>imagens/icones_final/prev.png' onclick='Navigation.prev()'
										title="<%=I18nUtils.getString("prevPage")%>" />
								</a>
								</td>

								<td><span>&nbsp;<%=I18nUtils.getString("page")%>&nbsp;&nbsp;
								</span></td>

								<td><input id='scan_inp_page'
									style='text-align: center; width: 40px; border-radius: 1px; height: 20px; border: 1px solid #CACACA;'
									onblur='Navigation.page(this.value);'
									onkeydown="Navigation.processKey(event, this);" /><a
									id='totalPages'
									style='height: 20px; border: 0px solid #000000; vertical-align: middle; line-height: 20px;'></a>
								</td>

								<td><span>&nbsp;&nbsp;<%=I18nUtils.getString("of")%></span>&nbsp;<span
									id='scan_totalpages_span'>0</span>&nbsp;</td>

								<td style="text-align: center; vertical-align: middle;"><a
									class='neo-button'><img
										src='<%=PortalUtil.getBaseURL()%>imagens/icones_final/next.png'
										onclick='Navigation.next()'
										title="<%=I18nUtils.getString("nextPage")%>" /></a></td>

								<td style="text-align: center; vertical-align: middle;"><a
									class='neo-button'><img
										src='<%=PortalUtil.getBaseURL()%>imagens/icones_final/last.png'
										onclick='Navigation.last()'
										title="<%=I18nUtils.getString("lastPage")%>" /></a></td>
							</tr>
						</table>
					</center>
				</td>

				

			</tr>
		</table>
	</div>
</body>
</html>