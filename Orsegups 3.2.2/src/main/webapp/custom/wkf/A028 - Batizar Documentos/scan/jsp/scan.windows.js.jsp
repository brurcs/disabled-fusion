<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
Gerenciamento de janelas internas.
--%>

var ScanWindows = {

	 ResizeDialog: 
	 {
	 	id: 'div-resize-dialog',
	 	 
	 	getResizeDialog: function()
	 	{
	 		return $('#'+this.id);
		}, 
		 
	 	showResizeDialog: function()
	 	{
	 		var tempFolder = Scan.tempFolder;
	 		var currentCanvasId = Scan.getCurrentViewingCanvasId();
	 		
	 		ImageActions.clearCropArea(document.getElementById(currentCanvasId));
	 		
	 		this.getResizeDialog().dialog("open");
	 		this.getResizeDialog().dialog({
	 			position:[$('#div-scan-pages').offset().left+2, $('#div-scan-pages').offset().top+2]
	 		});
	 	
	 		$('#resize-input_largura').val(CanvasManager.getSourceWidth(currentCanvasId));
	 		$('#resize-input_altura').val(CanvasManager.getSourceHeight(currentCanvasId));
		},
		
		
		applyResizeDialog: function(id)
		{
	 		var resizeDialog = this.getResizeDialog().dialog(
	 		{
	 		modal: false,
	 		autoOpen: false,
	 		resizable: false,
	 		});
	 	
	 		resizeDialog.dialog( "option", "buttons", 
	 		[ 
	 		{ text: '<%=I18nUtils.getString("ok")%>', click: function() 
	 		{ 
	 			ImageActions.resize(Scan.getCurrentViewingCanvasId(), Scan.tempFolder + Scan.getCurrentViewingCanvasId(), $('#resize-input_largura').val(), $('#resize-input_altura').val());
	 			$( this ).dialog( "close" ); 
	 		}},
	 		{ text: '<%=I18nUtils.getString("cancel")%>', click: function() 
	 		{ 
	 			$( this ).dialog( "close" ); 
	 		}}
	 		]);
	 	
	 		$('#resize-input_largura').bind('blur keyup', function(e){
		
			var type = e.type;
			var code = e.which;
		
			var checked = $('input[name=checkbox-aspect]').is(':checked');
			if (checked == true)
			{
				var fixedHeight = ScanWindows.ResizeDialog.getAspectRatioHeight(CanvasManager.getSourceHeight(Scan.getCurrentViewingCanvasId()), CanvasManager.getSourceWidth(Scan.getCurrentViewingCanvasId()), $(this).val());
				$('#resize-input_altura').val(fixedHeight);
			}
			});
	
			$('#resize-input_altura').bind('blur keyup', function(e){
		
			var type = e.type;
			var code = e.which;
		
			var checked = $('input[name=checkbox-aspect]').is(':checked');
			if (checked == true)
			{
				var fixedWidth = ScanWindows.ResizeDialog.getAspectRatioWidth(CanvasManager.getSourceWidth(Scan.getCurrentViewingCanvasId()), CanvasManager.getSourceHeight(Scan.getCurrentViewingCanvasId()), $(this).val());
				$('#resize-input_largura').val(fixedWidth);
			}
			});	
			
			$('.ui-dialog-buttonpane').find('button').addClass('input_button');
		 },
	 
	 	// Obt�m uma nova altura, mantendo as propor��es.
		getAspectRatioHeight: function (height, width, newWidth)
		{
			return Math.round(height/width*newWidth);
		},

		// Obt�m uma nova largura, mantendo as propor��es.
		getAspectRatioWidth: function (width, height, newHeight)
		{
			return Math.round(width/height*newHeight);
		}
		
	},
	
	BrightnessDialog:
	{
		id: 'div-brightness-dialog',
		bright_input_id: 'bright_input_bright',
		contrast_input_id: 'bright_input_contrast',
		
		getBrightnessDialog: function()
	 	{
	 		return $('#'+this.id);
		}, 
		
		getBrightnessValue: function()
		{
			return $('#'+ScanWindows.BrightnessDialog.bright_input_id).val();
		},
		
		getContrastValue: function()
		{
			return $('#'+ScanWindows.BrightnessDialog.contrast_input_id).val();
		},
		
		showBrightnessDialog: function()
		{
   			$('#bright-slider-bright').slider(
   			{
   				min: -100, 
   				max: 100,
   				value: 0,
   				stop: function(event, ui)
   				{
   					$('#'+ScanWindows.BrightnessDialog.bright_input_id).val(ui.value);
   				}
   			});
   			$('#bright-slider-contrast').slider(
   			{
   				min: -100, 
   				max: 100,
   				value: 0,
   				stop: function(event, ui)
   				{
   					$('#'+ScanWindows.BrightnessDialog.contrast_input_id).val(ui.value);
   				}
   			});
			
			$('#'+ScanWindows.BrightnessDialog.contrast_input_id).val(0);
			$('#'+ScanWindows.BrightnessDialog.bright_input_id).val(0);
			
			ImageActions.clearCropArea(document.getElementById(Scan.getCurrentViewingCanvasId()));
			
   			this.getBrightnessDialog().dialog("open");
   			this.getBrightnessDialog().dialog({
	 			position:[$('#div-scan-pages').offset().left+2, $('#div-scan-pages').offset().top+2]
	 		});
		},
		
		applyBrightnessDialog: function(id)
		{
			var brightDialog = this.getBrightnessDialog().dialog(
	 		{
	 		modal: false,
	 		autoOpen: false,
	 		resizable: false,
	 		position:[$('#div-scan-pages').offset().left+2, $('#div-scan-pages').offset().top+2]
	 		});
	 		
	 		brightDialog.dialog( "option", "buttons", 
	 		[ 
	 		{ text: '<%=I18nUtils.getString("ok")%>', click: function() 
	 		{ 
	 			// Brilho
	 			if (ScanWindows.BrightnessDialog.getBrightnessValue() != 0)
	 			{
	 				ImageActions.brightness(ScanWindows.BrightnessDialog.getBrightnessValue());
	 			}
	 			
	 			// Contraste
	 			if (ScanWindows.BrightnessDialog.getContrastValue() != 0)
	 			{
	 				ImageActions.contrast(ScanWindows.BrightnessDialog.getContrastValue());
	 			}
	 			$( this ).dialog( "close" ); 
	 		}},
	 		{ text: '<%=I18nUtils.getString("cancel")%>', click: function() 
	 		{ 
	 			$( this ).dialog( "close" ); 
	 		}}
	 		] );
	 		
	 		$('#'+ScanWindows.BrightnessDialog.bright_input_id).bind('blur', function() 
	 		{
	 			$('#bright-slider-bright').slider(
   				{ 
   					value: $('#'+ScanWindows.BrightnessDialog.bright_input_id).val()
   				});
	 		});
	 		
	 		$('#'+ScanWindows.BrightnessDialog.contrast_input_id).bind('blur', function() 
	 		{
	 			$('#bright-slider-contrast').slider(
   				{ 
   					value: $('#'+ScanWindows.BrightnessDialog.contrast_input_id).val()
   				});
	 		});
		}
		
	},
	BaptismPopup:
	{
		id: 'div-baptism-popup-wrapper',
		txtInputId: 'id_txt_documentType__',
		inputId: 'id_documentType__',
		baptismDiv: null,
		
		getPopup: function()
		{
			if (this.baptismDiv == null)
			{
				this.baptismDiv = $('#'+ScanWindows.BaptismPopup.id);
				return this.baptismDiv;
			} else
				return this.baptismDiv;	
		},
		
		close: function()
		{
			
		},
		
		toggle: function(opener, checkNodes)
		{ 
			 if (checkNodes)
			 {
			 	if (!TreeData.hasCheckedNodes())
			 	{
			 		alert('Por favor, selecione a(s) p�gina(s) que deseja batizar!');
			 		return;
			 	}	
			 }
			 
			 if (opener)
			 {
			   	// Informa��es do elemento que recebeu o clique
    	       	var openerLeft = $(opener).offset().left;
    		   	var openerTop = $(opener).offset().top;

			   	// Posiciona relativo ao elemento clicado
   			  	this.getPopup().css('left', openerLeft).css('top', openerTop);
   			 }
    
			   	// Exibe/esconde
   	 		 this.getPopup().toggle(10,function() 
   	 		 {
    			var disp = $(this).css('display');
    			if (disp == 'block')
    			{
      						
    			}
    			else
    			{
    				$('#'+ScanWindows.BaptismPopup.txtInputId).val('');
    				$('#'+ScanWindows.BaptismPopup.inputId).val('');
   	 			}
    		});
		},
	},
	
	SettingsDialog:
	{
		id: 'div-settings-dialog',
		devices_select_id: 'settings-device-select',
		table_id: 'settings-options_table',
		adf_id: 'settings-device-adf',
		duplex_id: 'settings-device-duplex',
		checkbox_id: 'checkbox-settings',
		bw_id: 'settings_bw',
		showui_id: 'settings_showui',
		resolution_id: 'settings_input_dpi',
		
		getSettingsDialog: function()
	 	{
	 		return $('#'+this.id);
		},
		
		getSelectValue: function(id)
		{
			return $('#'+id).find('option:selected').val();
		},
		
		getSelectText: function(id)
		{
			return $('#'+id).find('option:selected').text();
		},
		
		show: function()
		{
   			this.getSettingsDialog().dialog("open");
   			this.getSettingsDialog().dialog({
	 			position:[$('#div-scan-pages').offset().left+2, $('#div-scan-pages').offset().top+2]
	 		});
		},
		
		applySettingsDialog: function(id)
		{
			var settingsDialog = this.getSettingsDialog().dialog(
	 		{
	 		modal: false,
	 		autoOpen: false,
	 		resizable: false,
	 		width: 400,
	 		open: function( event, ui ) {
	 			ScanWindows.SettingsDialog.loadScanSettings();
	 			ScanWindows.SettingsDialog.populateDevicesList();
	 		},
	 		position:[$('#div-scan-pages').offset().left+2, $('#div-scan-pages').offset().top+2]
	 		});
	 		
	 		settingsDialog.dialog( "option", "buttons",
	 		[ 
	 		{ text: '<%=I18nUtils.getString("ok")%>', click: function() 
	 		{ 
	 			ScanWindows.SettingsDialog.beforeSubmit();
	 			$( this ).dialog( "close" ); 
	 		}},
	 		{ text: '<%=I18nUtils.getString("cancel")%>', click: function() 
	 		{ 
	 			$( this ).dialog( "close" ); 
	 		}}
	 		] );
		},
		
		unblockSettingsTable: function()
		{
			 $('#'+ScanWindows.SettingsDialog.table_id).unblock().attr('blocked','false');
		},
		
		blockSettingsTable: function()
		{
			 $('#'+ScanWindows.SettingsDialog.table_id).block({
			 message:null,
			 overlayCSS : {
				 backgroundColor: '#cacaca',
	             opacity: 0.2
			 }
			 }).attr('blocked','true');
		},
		
		checkSettingsBox: function()
		{
			 $('#'+ScanWindows.SettingsDialog.checkbox_id).attr('checked','checked');
		},
		
		toggleSettings: function(el)
		{
			var table = $('#'+ScanWindows.SettingsDialog.table_id);
			var blocked = table.attr('blocked');
			
			if (blocked && blocked == 'true')
			{
				this.unblockSettingsTable();
				table.attr('blocked', 'false');
			}
				
			else if (blocked && blocked == 'false')
			{
				this.blockSettingsTable();
				table.attr('blocked', 'true');
			}
				
		},
		
		// Carrega as configura��es de digitaliza��o.
		// Prioridade: usu�rio -> sistema.
		loadScanSettings: function()
		{
			
			$.ajax({
				url: Scan.baseURL + 'scanServlet/?action=getScanSettings&code='+Scan.currentUserCode,
				dataType: 'json',
				type: 'GET',
				async: false,
				cache: false,
				success: function(data)
				{
					ScanWindows.SettingsDialog.afterLoad(data);
				},
				error: function (xhr, ajaxOptions, thrownError) 
				{
        			
      			}
			});
		},
		
		// Gerencia a tela ap�s carregar as configura��es.
		afterLoad: function(data)
		{
			var origin = data.origin;
			
			if (origin == 'system')
			{
				ScanWindows.SettingsDialog.blockSettingsTable();
				ScanWindows.SettingsDialog.fillSystemScanSettings(data);
			}
			else if (origin == 'user')
			{
				ScanWindows.SettingsDialog.checkSettingsBox();
				ScanWindows.SettingsDialog.fillSystemScanSettings(data);
			}
			// N�o foram encontradas Configura��es no usu�rio e no sistema
			// TODO: criar? No momento, passando valores padr�es
			else if (origin == '' || origin == undefined) 
			{
				ScanWindows.SettingsDialog.checkSettingsBox();
			}
		},
		
		// Preenche o nome do dispositivo padr�o na combo.
		fillDeviceName: function(name, adf, duplex)
		{
			$('#'+ScanWindows.SettingsDialog.devices_select_id).find("option[value='"+name+"']").attr("selected", "selected");
			// habilita/desabilita as checkbox quando preenche o nome do dispositivo pardr�o
   			var deviceSettings = applet.checkDeviceSettings(name);
   			var settingsSplit = deviceSettings.split(';');
   			if(settingsSplit[0] == 'false')
   				$('#'+ScanWindows.SettingsDialog.adf_id).attr('disabled', true);
   			else
   				$('#'+ScanWindows.SettingsDialog.adf_id).attr('disabled', false);
   			if(settingsSplit[1] == 'false')
   				$('#'+ScanWindows.SettingsDialog.duplex_id).attr('disabled', true);
   			else
   				$('#'+ScanWindows.SettingsDialog.duplex_id).attr('disabled', false);
		},
		
		// Preenche as configura��es de digitaliza��o do sistema nos campos relativos.
		fillSystemScanSettings: function(data)
		{
			$('#'+ScanWindows.SettingsDialog.bw_id).prop('checked', data.bw);
			$('#'+ScanWindows.SettingsDialog.showui_id).prop('checked', data.showui);
			$('#'+ScanWindows.SettingsDialog.resolution_id).val(data.resolution);
		},
		
		// Executado ap�s pressionar OK na dialog.
		beforeSubmit: function(bw,showui,resolution,override)
		{
				var checked = $('#'+ScanWindows.SettingsDialog.checkbox_id).is(':checked');
				var bw = $('#'+ScanWindows.SettingsDialog.bw_id).is(':checked');
				var resolution = 200;
				var showui = $('#'+ScanWindows.SettingsDialog.showui_id).is(':checked');			
				var checkAfd = $('#'+ScanWindows.SettingsDialog.adf_id).prop('checked');
				var checkDuplex = $('#'+ScanWindows.SettingsDialog.duplex_id).prop('checked');
				
				// Configura o dispositivo padr�o na applet.
				var deviceName = ScanWindows.SettingsDialog.getSelectText(ScanWindows.SettingsDialog.devices_select_id);
				applet.saveDefaultDevice(deviceName, checkAfd, checkDuplex);
				Scan.deviceName = deviceName;
				
				// Salva as configura��es para o Usu�rio.
				if (checked == true)
				{
					resolution = $('#'+ScanWindows.SettingsDialog.resolution_id).val();
					
					$.ajax({
					url: Scan.baseURL + 'scanServlet/?action=updateScanSettings&code='+Scan.currentUserCode+'&override='+checked+'&bw='+bw+'&showui='+'false'+'&resolution='+resolution,
					cache: false,
					success: function(data)
					{
					},
					error: function (xhr, ajaxOptions, thrownError) 
					{
        				alert(thrownError);
      				}
					});
				}
				else // Remove as configura��es do usu�rio e recarrega as do sistema.
				{
					$.ajax({
					url: Scan.baseURL + 'scanServlet/?action=updateScanSettings&code='+Scan.currentUserCode+'&override='+checked,
					cache: false,
					success: function(data)
					{
						bw = false;
						showui = false;
						resolution = 300;
						
					},
					error: function (xhr, ajaxOptions, thrownError) 
					{
        				alert(thrownError);
      				}
					});
				}
				
				// Atualiza na applet as op��es, caso contr�rio, ela continuar� usando as informa��es
				// passadas via par�metro na hora da inser��o.
				ScanApplet.setResolution(resolution);
				ScanApplet.setShowUI(showui);
				ScanApplet.setBlackAndWhite(bw);
				ScanApplet.setAdf(checkAfd);
				ScanApplet.setDuplex(checkDuplex);
		},
		
		populateDevicesList: function()
		{
			var defaultDevice = applet.getDefaultDevice().split('#');
			var devices = applet.getDeviceList();
			
			var spl = devices.split(';');
	
			$('#'+ScanWindows.SettingsDialog.devices_select_id).find('option').remove();
			for (var i=0;i<spl.length;i++)
			{
				if (spl[i] && spl[i].length>0)
				{
					var opt = $('<option/>', {
					value: spl[i],
					text: spl[i]
					});
				}
				
				$('#'+ScanWindows.SettingsDialog.devices_select_id).append(opt);
			}
			$('#'+ScanWindows.SettingsDialog.devices_select_id).unbind('change');
			$('#'+ScanWindows.SettingsDialog.devices_select_id).bind('change', function() {
     			var deviceName = $('#'+ScanWindows.SettingsDialog.devices_select_id).find(':selected').text();
     			var deviceSettings = applet.checkDeviceSettings(deviceName);
     			var settingsSplit = deviceSettings.split(';');
     			if(settingsSplit[0] == 'true')
     				$('#'+ScanWindows.SettingsDialog.adf_id).attr('disabled', false);
     			else
     				$('#'+ScanWindows.SettingsDialog.adf_id).attr('disabled', true);
     			if(settingsSplit[1] == 'true')
     				$('#'+ScanWindows.SettingsDialog.duplex_id).attr('disabled', false);
     			else
     				$('#'+ScanWindows.SettingsDialog.duplex_id).attr('disabled', true);
     		});
			ScanWindows.SettingsDialog.fillDeviceName(defaultDevice[0], defaultDevice[1], defaultDevice[2]);
		},
		
	},
	
	RestoreDialog:
	{
		id: 'div-restore-dialog',
		table_id: 'restore-options_table',
		
		getRestoreDialog: function()
	 	{
	 		return $('#'+this.id);
		},
		
		getRestoreTable: function()
		{
			return $('#'+this.table_id);
		},
		
		applyRestoreDialog: function(id)
		{
			var restoreDialog = this.getRestoreDialog().dialog(
	 		{
	 		modal: false,
	 		autoOpen: false,
	 		resizable: false,
	 		width: 700,
	 		height: 300,
	 		open: function( event, ui ) {
	 		
	 		if (Scan.tempFolder == '')
				Scan.tempFolder = applet.getTempFolder();
	 			ScanWindows.RestoreDialog.restoreScannedFiles();
	 		},
	 		position:[$('#div-scan-pages').offset().left+2, $('#div-scan-pages').offset().top+2]
	 		});
	 		
	 		restoreDialog.dialog( "option", "buttons",
	 		[
	 		{ text: '<%=I18nUtils.getString("ok")%>', click: function() 
	 		{
	 			ScanWindows.RestoreDialog.sendImagesToTree();
	 			$( this ).dialog( "close" ); 
	 		}},
	 		{ text: '<%=I18nUtils.getString("cancel")%>', click: function() 
	 		{
	 			$( this ).dialog( "close" ); 
	 		}},
	 		{ text: '<%=I18nUtils.getString("cleanTemp")%>', click: function() 
	 		{
	 			ScanWindows.RestoreDialog.cleanFusionTempFolder();
	 			ScanWindows.RestoreDialog.restoreScannedFiles();
	 		}},
	 		]);
		},
		
		show: function()
		{
   			this.getRestoreDialog().dialog("open");
   			this.getRestoreDialog().dialog({
	 			position:[$('#div-scan-pages').offset().left+2, $('#div-scan-pages').offset().top+2]
	 		});
		},
		
		// Restaura os arquivos TIF scaneados.
		restoreScannedFiles: function()
		{
			var fileList = applet.getFilesInTempFolder('tif');
			this.buildFileList(fileList);
		},
		
		cleanFusionTempFolder: function()
		{
			applet.cleanFusionCaptureDirectory();
		},
		
		deleteFiles: function(fileList)
		{
			var files = fileList.split(';');
			for (var i=0;i<fileList.length;i++)
			{
				var file = files[i];
				if (file)
					this.deleteFile(file);
			}
		},
		
		deleteFile: function(fileName)
		{
			applet.deleteFile(fileName);
		},
		
		// Constroi a lista de arquivos.
		buildFileList: function(fileList)
		{
			var filesArray = fileList.split(';');
			var tableRef = this.getRestoreTable().find('tbody');
			
			if (tableRef.length)
			{
				tableRef.find('tr').remove();
				for (var i=0;i<filesArray.length;i++)
				{
					var line = filesArray[i];
					if (line != '')
					{
						var row = this.createRow(line, i);
						tableRef.append(row);
					}
				}
			}
		},
		
		// Cria a linha na tabela.
		createRow: function(line, i)
		{
			var tr = $('<tr/>', {
				id: 'restore-tr-' + i
			});
			
			// Checkbox
			var numberTd = $('<td/>', {
				id: 'restore-td-checkbox-'+i,
				style: 'width: 5px;'
			});
			var numberCheckbox = $('<input/>',{
				type: 'checkbox',
				idx: i,
				id: 'restore-checkbox-'+i
			});
			numberTd.append(numberCheckbox);
			
			tr.append(numberTd);
			
			var lineSpl = line.split(',');
			for (var j=0;j<lineSpl.length;j++)
			{
				var id;
				if (j == 0)
					id = 'restore-td-fileName-'+i;
				else if (j == 1)
					id = 'restore-td-fileDate-'+i;
				else if (j == 2)
					id = 'restore-td-fileSize-'+i;
					
				var piece = lineSpl[j];
				var td = $('<td/>', {
					id: id,
					idx: i
				}).append(piece);
				tr.append(td);
			}
			
			return tr;
		},
		
		// Obt�m as linhas checadas.
		getCheckedRows: function()
		{
			var tableRef = this.getRestoreTable().find('tbody');
			if (tableRef.length)
			{
				var checkedRows = tableRef.find('input[type=checkbox]').filter(':checked');
				return checkedRows;
			}
		},
		
		// Envia as imagens selecionadas para a �rvore de arquivos digitalizados.
		sendImagesToTree: function()
		{
			// Obt�m as linhas selecionadas
			var rows = this.getCheckedRows();
			
			if (rows && rows.length>0)
			{
				var fileList='';
				
				// Monta a lista de arquivos
				for (var i=0;i<rows.length;i++)
				{
					var row = rows[i];
					var idx = $(row).attr('idx');
					var fileNameTdId = 'restore-td-fileName-' + idx;
					fileList = fileList + Scan.tempFolder + $('#'+fileNameTdId).html();
					if (i < rows.length-1)
					fileList = fileList + ";";
				}
					
				// Realiza convers�o dos TIFFs
				setTimeout(function() 
				{
					ScanStatus.showConversionMessage(Scan.layoutWrapper, 'Fusion ECM Suite', 
					function() 
					{
						// Esconde o container para n�o desalinhar o HTML.
						TreeData.getContainerUL().hide();
            			
						var convertedFiles = applet.breakMultiPartTiffs(fileList);
						ScanStatus.hideStatusMessage(Scan.layoutWrapper, function() 
						{
							ScanStatus.showTreeMessage(Scan.layoutWrapper, '<%=I18nUtils.getString("loading")%>', function() 
							{
								TreeData.buildTreeHTML(convertedFiles, 'li_recovered');
								ScanStatus.hideStatusMessage(Scan.layoutWrapper);
								ScanWindows.RestoreDialog.deleteFiles(fileList);
								Sidebar.updateBounds();
								
							});
						});
						});
				 }, 100);
				
				}
			}
		
	},
	
	ImportDialog:
	{
		open: function()
		{
			var retFiles = applet.showFileChooser();
			
			if (Scan.tempFolder == '')
				Scan.tempFolder = applet.getTempFolder();
			
			if (retFiles && retFiles.length>0)
			{
				this.sendImagesToTree(retFiles);
			}
		},
		
		sendImagesToTree: function (files)
		{
    		var filesSpl = files.split(';');

    		// Realiza convers�o dos TIFFs
    		setTimeout(function ()
    		{
        		ScanStatus.showConversionMessage(Scan.layoutWrapper, 'Fusion ECM Suite',
            	function ()
            		{
            			
            			// Esconde o container para n�o desalinhar o HTML.
						TreeData.getContainerUL().hide();
						
                		var convertedFiles = applet.breakMultiPartTiffs(files);
                		ScanStatus.hideStatusMessage(Scan.layoutWrapper, function ()
                		{
                    		ScanStatus.showTreeMessage(Scan.layoutWrapper, '<%=I18nUtils.getString("loading")%>', function ()
                    		{
                        		TreeData.buildTreeHTML(convertedFiles, 'li_digitalized');
                        		ScanStatus.hideStatusMessage(Scan.layoutWrapper);
                        		Sidebar.updateBounds();
                    		});
                		});
            		});
    		}, 100);
		}
	}
	  
};