<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>

var Tree = {
	separator: "_",
	id: 0,
	data : "", // json data structure
	container: "tree_container",
	nodePrefix: "node_",
	selectedNode: null,
	page: 1,
	triggerResize: true,
	
	nextId : function()
	{
		return this.id++;
	},
	setContainer : function(c)
	{
		this.container = c;
	},
	getContainer : function()
	{
		return $("#" + this.container);
	}
}