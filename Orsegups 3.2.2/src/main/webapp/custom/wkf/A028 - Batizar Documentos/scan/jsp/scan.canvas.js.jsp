<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
Gerenciamento de um Canvas HTML5.
--%>

var CanvasManager = {

	getById: function(id)
	{
		var canvas = document.getElementById(id);
		return canvas;
	},
	
	getWidth: function(id)
	{
		var canvas = this.getById(id);
		if (canvas)
			return canvas.width;
		else return -1;
	},
	
	getHeight: function(id)
	{
		var canvas = this.getById(id);
		if (canvas)
			return canvas.height;
		else return -1;
	},
	
	getSourceWidth: function(id)
	{
		var canvas = this.getById(id);
		if (canvas)
			return $(canvas).attr('source_width');
		else return -1;
	},
	
	getSourceHeight: function(id)
	{
		var canvas = this.getById(id);
		if (canvas)
			return $(canvas).attr('source_height');
		else return -1;
	},
	
	getContextById: function(id)
	{
		var canvas = CanvasManager.getById(id);
		if (canvas != undefined)
		{
			var ctx = canvas.getContext('2d');
			return ctx;
		}
	},
	
	// Converte a imagem para BASE64 e mostra no canvas.
	// Caso o Canvas n�o exista, cria.
	drawImage: function(id, src, save)
	{
		var img64 = 'data:image/jpeg;base64,'+applet.getImage(src);
		
		var img = new Image();
		img.onload = function()
		{
			var canvas = CanvasManager.getById(id);
			if (!canvas)
			{
				canvas = document.createElement('canvas');
				canvas.id = id;
				$(canvas).addClass('ui-neo-scan-canvas');
				$(canvas).attr('changed', 'false');
				$(canvas).attr('zoom_value', '100');
				Scan.getContainer().append(canvas);
			}
			
			Zoom.current = 100;
			var ctx = CanvasManager.getContextById(id);
			canvas.width = img.width;
    		canvas.height = img.height;
    		
    		$(canvas).attr('source_width', canvas.width); // para o Zoom
    		$(canvas).attr('source_height', canvas.height); // para o Zoom
    		
    		ctx.drawImage(img, 0, 0, img.width, img.height);
    		
    		// Esconde todos os CANVAS e mostra s� o clicado (correspondente a imagem)
    		Scan.getContainer().find('canvas').hide();
    		$(canvas).show();
    		
    		if (save && save == true)
    		{
    			CanvasManager.saveImageToDisk(canvas);
    			ScanUtils.setCanvasStatus(canvas, 'false');
    		}
	
		}
		img.src = img64;
	},
	
	// Desenha a imagem com Zoom.
	drawZoomedImage: function(canvas, src, zoomWidth, zoomHeight)
	{
		var img64 = 'data:image/jpeg;base64,'+applet.getImage(src);
		var img = new Image();
		
		img.onload = function()
		{
			var ctx = canvas.getContext('2d');
			canvas.width = zoomWidth;
    		canvas.height = zoomHeight;
    		
    		ctx.drawImage(img, 0, 0, zoomWidth, zoomHeight);
		}
		img.src = img64;
	},
	
	// Salva a imagem do Canvas no disco.
	saveImageToDisk: function(canvas)
	{
		var jpegUrl = canvas.toDataURL("image/jpeg");
		var fileName = Scan.tempFolder + canvas.id;
		applet.saveImage(jpegUrl, fileName);
	},
	
	// Remove o canvas relativo aos n�s checados removidos.
	remove: function(checkedNodes)
	{
		var canvas;
		var node;
		var url;
		$(checkedNodes).each(function() {
		node = $(this);
		url = node.attr('url');
			
		canvas = document.getElementById(url);
		if (canvas)
		{
			$(canvas).remove();
		}
		else { // se caiu aqui, � porqu� s� sobrou o n� pai sem filhos, entao remove o canvas atual.
			canvas = document.getElementById(Scan.getCurrentViewingCanvasId());
			if (canvas)
				$(canvas).remove();
		}
		});
	}
	 
};