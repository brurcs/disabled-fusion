<%@page import="java.util.Locale"%>
<%@page import="com.neomind.fusion.scan.ScanUtils"%>
<%@page import="com.neomind.fusion.scan.ScanSettings"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.io.File"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<portlet:defineObjects />

<%
	String callback = (String) request.getParameter("callback");
String inputId = (String) request.getParameter("inputId");
String baptism = (String) request.getParameter("baptism");
if (baptism == null)
	baptism = "false";

// Carrega as configurações de digitalização.


boolean showUI = false;
boolean blackAndWhite = false;
int resolution = 200;

String localeCountry = "BR";
String localeLang = "pt";

Locale local = PortalUtil.getCurrentLocale();
if (local != null)
{
	localeCountry = local.getCountry();
	localeLang = local.getLanguage();
}
%>

<!DOCTYPE html>

<head>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1"> 

<title><%= I18nUtils.getString("scan") %></title>
<base href="<%= PortalUtil.getBaseURL() %>" />
<link rel="shortcut icon" href="<%= PortalUtil.getBaseURL() %>fusion.ico" />

<!-- CSS -->
<link rel="stylesheet" type="text/css" media="all"	href="<%=PortalUtil.getBaseURL()%>css/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="<%=PortalUtil.getBaseURL()%>css/themes/neomind/jquery-ui-tree.css" />
<link rel="stylesheet" type="text/css" media="all"  href="<%= PortalUtil.getBaseURL()%>scan/css/scan.css" />
<link rel="stylesheet" type="text/css" media="all"  href="<%= PortalUtil.getBaseURL()%>scan/css/imgareaselect-default.css" />
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>scan/css/jquery.ui.slider.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/form_tags.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/form.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/login.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/core.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/header.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/window_default.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/window.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/buttons.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/search.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/titles.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/grid.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/forms_list.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/tabs.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/portal.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/portal-decorator.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/jquery.treeview.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/screen.css">
<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>css/themes/neomind/neo.ui.css.jsp">


<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>js/jquery/jquery.js.jsp"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>js/jquery/jquery-ui-core.js.jsp"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>js/jquery/ui/jquery.ui.resizable.min.js"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>js/jquery/ui/jquery.ui.dialog.min.js"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>js/jquery/ui/jquery.ui.draggable.min.js"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>js/header.js.jsp"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/form/form.js.jsp"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>js/neo_ui/neo.js"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>js/neo_ui/neo_utils.js"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>js/neo_ui/modal/neo_modal_window.js"></script>
<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/autocomplete2.js.jsp"></script>

<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>js/jquery/jquery.blockUI.js'></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>js/jquery/neo/jquery.neo.img.lazy.js"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>scan/js/jquery.color.js"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>scan/js/jquery.imgareaselect.pack.js"></script>
<script type="text/javascript" src="<%= PortalUtil.getBaseURL() %>scan/js/jquery.ui.slider.min.js"></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>js/jquery/jquery.jstree.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>js/log.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.json.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.cache.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.navigation.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.fullscreen.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.zoom.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.menu.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.sidebar.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.thumbnail.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.tree.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.image.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.canvas.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.pixels.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.servlets.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.applet.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.treedata.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.utils.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.status.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.windows.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.treeactions.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.publisher.js.jsp'></script>
<script type='text/javascript' src='<%= PortalUtil.getBaseURL() %>scan/jsp/scan.js.jsp'></script>

</head>
			
			
			<div id='scan_layout_wrapper' class="scan_layout_wrapper">
			<!-- Toolbar -->
			<div id='scan_layout_top' class="scan_layout_top">
			<jsp:include page="/scan/jsp/scan.toolbar.jsp" />	
			</div>
			
			<!-- Parte direita - Páginas do documento -->
			<div id='scan_layout_right' class="scan_layout_right">
			<jsp:include page="/scan/jsp/scan.pages.jsp" />
			</div>
			
			<!-- Parte esquerda - Menu tree/thumb -->
			<div id='scan_layout_left' class="scan_layout_left">
			<jsp:include page="/scan/jsp/scan.sidebar.jsp" />
			</div>


	