<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

var Toolbar = {
	 container: 'scan_layout_top',
	 navigation_bar: 'scan_nav_bar',
	 currentZoomDisplay: null,
	 zoom_input: 'scan_cur_zoom',
	 cad_button: 'cad_view_button',
	 sign_button: 'sign_view_button',
	 comments_button: 'viewer_comments_button',
	 page_number_inp: 'viewer_inp_page',
	 total_pages_span: 'scan_totalpages_span',
	 current_page_input: 'scan_inp_page',
	 hide_comments_button: 'viewer_hide_com_button',
	 
	 init: function(settings)
	 {
	 	if(settings.currentZoomDisplay)
	 		this.currentZoomDisplay = settings.currentZoomDisplay;
		
		// Registra o evento de zoom pra alterar o valor no input.	
		Zoom.registerZoomEvent(function(event, val)
		{
			var valStr = val.toString()+'%';
			$('#'+Toolbar.zoom_input).val(valStr);
		});
		
		
		Toolbar.toggleBaptismButton('disable');
		Toolbar.toggleImageButtons('disable');
		Toolbar.toggleSaveButton('disable');
		Toolbar.toggleTreeButtons('disable');
		Toolbar.toggleZoomButtons('hide');
	 },
	 
	 toggleSaveButton: function(action)
	 {
	 	Toolbar.toggleMenuIcon('img-save-button', action);
	 },
	 
	 toggleImageButtons: function(action)
	 {
	 	Toolbar.toggleMenuIcon('img-select-button', action);
		Toolbar.toggleMenuIcon('img-crop-button', action);
		Toolbar.toggleMenuIcon('img-bright-button', action);
		Toolbar.toggleMenuIcon('img-resize-button', action);
		Toolbar.toggleMenuIcon('img-rotateleft-button', action);
		Toolbar.toggleMenuIcon('img-rotateright-button', action);
		Toolbar.toggleMenuIcon('img-undo-button', action);
		Toolbar.toggleMenuIcon('img-undoall-button', action);
	 },
	 
	 setZoomValue: function(value)
	 {
	 	$('#'+Toolbar.zoom_input).val(value);
	 },
	 
	 toggleBaptismButton: function(action)
	 {
	 	Toolbar.toggleMenuIcon('img-baptism-button', action);
	 },
	 
	 toggleTreeButtons: function(action)
	 {
	 	Toolbar.toggleMenuIcon('img-delete-button', action);
		Toolbar.toggleMenuIcon('img-moveup-button', action);
		Toolbar.toggleMenuIcon('img-movedown-button', action);
	 },
	 
	 toggleZoomButtons: function(action)
	 {
	 	Toolbar.toggleMenuIcon('scan_cur_zoom', action);
	 	Toolbar.toggleMenuIcon('img-zoom-minus', action);
	 	Toolbar.toggleMenuIcon('img-zoom-plus', action);
	 	Toolbar.toggleMenuIcon('img-zoom-fit', action);
	 	Toolbar.toggleMenuIcon('img-zoom-full', action);
	 	Toolbar.toggleMenuIcon('img-fullscreen', action);
	 },
	 
	 updateZoomDisplay: function()
	 {
	 	if(this.currentZoomDisplay)
	 	{
	 		var el = $("#" + this.currentZoomDisplay);


	 		el.text(Zoom.current);
	 	}
	 },
	 
	checkShowNavBar: function()
	{
		var nodeCount = TreeData.getNodeCount();
		if (nodeCount > 0)
		{
			if (nodeCount > 1)
			{
				$('#'+Toolbar.navigation_bar).show();
			}
			else $('#'+Toolbar.navigation_bar).hide();		
			
			if (Scan.baptism == 'true')
				Toolbar.toggleBaptismButton('enable');
				
			Toolbar.toggleImageButtons('enable');
			Toolbar.toggleSaveButton('enable');
			Toolbar.toggleZoomButtons('show');
			Toolbar.toggleTreeButtons('enable');
		}
		else
		{
			
			if (Scan.baptism == 'true')
				Toolbar.toggleBaptismButton('disable');
			Toolbar.toggleTreeButtons('disable');
			Toolbar.toggleImageButtons('disable');
			Toolbar.toggleSaveButton('disable');
			$('#'+Toolbar.navigation_bar).hide();
			Toolbar.toggleZoomButtons('hide');
		}
		
	},
	
	updateTotalPages: function()
	{
		var nodeCount = TreeData.getNodeCount();
		$('#'+Toolbar.total_pages_span).text(nodeCount);
		Scan.Pages.total = parseInt(nodeCount);
	},
	
	updateCurrentPage: function(page)
	{
		$('#'+Toolbar.current_page_input).val(page);
		Scan.Pages.current = parseInt(page);
	},
	
	toggleMenuIcon: function(id, action)
	{
		var ref = $('#'+id).closest('td');
		
		try
		{
			if (action == 'disable')
				ref.fadeTo(1, 0.3).attr('grayed','true').attr('clickdisabled', ref.attr('onclick')).removeAttr('onclick');
			else if (action == 'enable')
				ref.fadeTo(1, 1).removeAttr('grayed').attr('onclick', ref.attr('clickdisabled')).removeAttr('clickdisabled');
			else if (action == 'hide')
				ref.hide();
			else if (action == 'show')
			{
				ref.show();
			}
		}catch(e){}
			
	},
	 
};

$(document).ready(function() {
	Toolbar.init({"currentZoomDisplay" : "viewer_cur_zoom"});
});