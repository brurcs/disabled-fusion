<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

function verificaDatas(eformId, eformPaiId) { 
	var valorData = document.getElementById('var_processEntity__DadosPagamento__DataPagto__').value;
	var url = "<%=PortalUtil.getBaseURL( )%>servlet/com.neomind.fusion.workflow.adapter.ems.EMSServletUtils?action=doVerificaDatas&eformId="+eformId+"&eformPaiId="+eformPaiId+"&valor="+valorData;
	var response = unescape(callSync(url)); 
	if (response != '' && response == ';alerta;') 
	{ 
		alert('Os pagamentos ser�o realizados com no m�nimo 48 horas ap�s a �ltima libera��o da Autoriza��o de Pagamento!'); 
	} 
}
function importTitulosSAP() {  
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var elEmpresa = document.getElementById("id_processEntity__Empresa__");
	var el = document.getElementById("id_processEntity__ClienteSAP__"); 
	var val = ""; var valEmpresa = ""; 
	if (elEmpresa) 
		valEmpresa = elEmpresa.value; 
	else 
		valEmpresa = NEO.neoUtils.returnParent(elEmpresa).value; 
	if (el) 
		val = el.value; 
	else
	val = NEO.neoUtils.returnParent(el).value; 
	showLoadScreen(true);
	callFunction(base+"servlet/com.neomind.fusion.integration.sap.ImportTitulosSAP?idCliente="+val+"&idEmpresa="+valEmpresa,'showLoadScreen(false)');
} 
function importTransportadoraRepresentanteSAP() { 
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var el = document.getElementById("id_processEntity__TituloSAP__tituloSAP__"); var
	val = ""; 
	if (el) val = el.value; 
	else val = NEO.neoUtils.returnParent(el).value; 
	showLoadScreen(true);
	callFunction(base+"servlet/com.neomind.fusion.integration.sap.ImportTransportadoraRepresentanteSAP?idTitulo="+val,'showLoadScreen(false)');
} 
function showLoadScreen(show)
{ 
	try { 
		var espera = document.getElementById("espera"); 
		if(espera != null && espera !=	undefined && espera.style) 
			if(show)
				espera.style.display='block';
			else 
				espera.style.display='none';
	} catch(e){ 
	} 
}

function fechaEdicaoCallback(idForm)
{
	if (idForm != '' && idForm != 'undefined' && idForm != null)
	{
		var urlServlet = '';
		if (confirm("Deseja recalcular os vencimentos das obriga��es j� iniciadas por esta regra?") == 1)
		{
			urlServlet = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.workflow.adapter.ems.EMSServletUtils?action=doAtualizaObrigacao&idRegra='+idForm;
			
			 $.ajax({
				  url: urlServlet,
				  success: function( data ) {
					alert('Opera��o Executada!');
					montaParametrosConsulta();
				  }
				});
	
		}		
	}
	window.close();
	window.opener.$("#botaoConsulta").click();
}

function atualizaMensagemSistema()
{
	var vetor = new Array();
	var vetorCartaoCorporativo = new Array();
	
 	var contadorCartaoCorporativo = ellist_processEntity__despesasCartaoCorporativo__.getRowCount();
 	var contador = ellist_processEntity__listaItinerarios__.getRowCount();

 	var i;
 	for(i=0;i < contador;i++)
	{
		var id = ellist_processEntity__listaItinerarios__._getRow(i);
		var entityIdAtual = id.entityId;
		vetor.push(entityIdAtual);
	}
	i=0;
	for(i=0;i < contadorCartaoCorporativo;i++)
	{
		var id = ellist_processEntity__despesasCartaoCorporativo__._getRow(i);
		var entityIdAtual = id.entityId;
		vetorCartaoCorporativo.push(entityIdAtual);
	}
	limparLista();
	refreshListaDespesas(document.getElementById('eForm').value, document.getElementById('hid_processEntity__').value, vetor, vetorCartaoCorporativo);
	refreshListaMensagens(document.getElementById('eForm').value, document.getElementById('hid_processEntity__').value, vetor, vetorCartaoCorporativo);
}

function atualizaGridDespesas()
{
	var vetor = new Array();
	var contador = ellist_processEntity__listaItinerarios__definicaoDespesas__.getRowCount();	
 	var itinerario =  document.getElementById('var_processEntity__listaItinerarios__itinerario__').value;
 	var data =  document.getElementById('var_processEntity__listaItinerarios__data__').value;	
 	 	
 	var i;
 	for(i=0;i < contador;i++)
	{
		var id = ellist_processEntity__listaItinerarios__definicaoDespesas__._getRow(i);
		var entityIdAtual = id.entityId;
		vetor.push(entityIdAtual);
	}
	

	var base = "<%=PortalUtil.getBaseURL( )%>";
	var rootId = document.getElementById('hid_processEntity__listaItinerarios__').value;
		
	callFunction(base+"servlet/com.neomind.fusion.workflow.adapter.ems.EMSServletUtils?action=doAtualizaListaDespesas&ids="+vetor+"&rootid="+rootId+"&itinerario="+itinerario+"&data="+data);
		
	setTimeout('location.reload(0)', 900);
}

function importaPlanilhaAdiantamentosExcel()
{
	var objId = 	document.getElementById('eForm').value;
	var objNeoId = 	document.getElementById('hid_processEntity__').value
	
	var base = "<%=PortalUtil.getBaseURL( )%>";
	
	var url = base + "servlet/com.neomind.fusion.workflow.adapter.ems.EMSServletUtils?action=doPopulaListaAdiantamento&objId="+objId+"&objNeoId="+objNeoId;
	var retorno = callSync(url);
	if(retorno == "") return;
	if(retorno == "erro1")
	{
		alert("� necess�rio SALVAR a atividade antes de importar a planilha.");
		return;
	}
	if(retorno == "erro2")
	{
		alert("Formato da planilha est� incorreto. \nColuna 1 Matricula formato texto.\nColuna 2 Valor formato 999,99.\nColuna 3 Motivo de Adto formato texto");
		return;
	}
	
	var count = ellist_processEntity__adiantamentosSolicitados__.getRowCount();
	if((count && count > 0))
	{
		var i = 0;
		try 
		{
			for(i=0;i < count;i++) 
			{
				ellist_processEntity__adiantamentosSolicitados__.tBody.deleteRow(i);
			}
		}
		catch(e){}
	}
	
	var ids = retorno.split(';');
	if(ids.length > 0)
	{
		var i = 0;
		for(i=0;i < ids.length;i++) 
		{
			try 
			{
				var id = ids[i];
				ellist_processEntity__adiantamentosSolicitados__._createRow(i);
				ellist_processEntity__adiantamentosSolicitados__.setId(i, id);
			}
			catch(e) {} 
		}
	}

}

function limpaListaDespesasCartao()
{
	var control = 0;
	var count = ellist_processEntity__despesasCartaoCorporativo__.getRowCount();
	if((count && count > 0))
	{
		var i = 0;
		try 
		{
			for(i=0;i < count;i++) 
			{
				ellist_processEntity__despesasCartaoCorporativo__.tBody.deleteRow(i);
			}
		}
		catch(e){}
	}else control = 1;
	
	if(control == 1) return;
	
	limpaListaDespesasCartao(count);
}

function importaDespesasCartao()
{
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var referencia = document.getElementById('id_processEntity__mesReferencia__').value;
	var objNeoId = 	document.getElementById('hid_processEntity__').value
	
	if(referencia == '-1')
	{
		alert("Selecione um m�s de refer�ncia.");
		return;
	}
	var url = base + "servlet/com.neomind.fusion.workflow.adapter.ems.EMSServletUtils?action=doPopulaListaDespesasCartao&objNeoid="+objNeoId+"&referencia="+referencia;
	var retorno = callSync(url);
	if(retorno == '')
	{
		alert("N�o h� despesas a serem importadas para este m�s de refer�ncia. Se houver problemas entre em contato com Coordena��o de RDV Financeira.");
		return;
	}
	
	var ids = retorno.split(';');
	if(ids.length > 0)
	{
		limpaListaDespesasCartao();
		var i = 0;
		for(i=0;i < ids.length;i++) 
		{
			try 
			{
				var id = ids[i];
				ellist_processEntity__despesasCartaoCorporativo__._createRow(i);
				ellist_processEntity__despesasCartaoCorporativo__.setId(i, id);
			}
			catch(e) {} 
		}
	}
	
	atualizaMensagemSistema();
}

function limparLista() 
{
	var control = 0;
	var count = ellist_processEntity__resumoGeralDespesas__.getRowCount();
	if((count && count > 0))
	{
		var i = 0;
		try 
		{
			for(i=0;i < count;i++) 
			{
				ellist_processEntity__resumoGeralDespesas__.tBody.deleteRow(i);
			}
		}
		catch(e){}
	}else control++;
	
	
	count = ellist_processEntity__mensagensSistema__.getRowCount();
	if((count && count > 0))
	{
	   i = 0;
	   try 
	   {
		   for(i=0;i < count;i++) 
		   {
			   ellist_processEntity__mensagensSistema__.tBody.deleteRow(i);
		   }
	   }
	   catch(e){}
	 }control++;
	if(control == 2) return;
	limparLista();
}
		
function refreshListaDespesas(objId, objNeoId, vetor1, vetor2)
{ 
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var url = base + "servlet/com.neomind.fusion.workflow.adapter.ems.EMSServletUtils?action=doPopulaMensagemSistema&ids="+vetor1+"&objId="+objId+"&objNeoId="+objNeoId+"&cartao="+vetor2;
	var retorno = callSync(url);
	if(retorno == "") return;
	var ids = retorno.split(';');
	if(ids.length > 0)
	{
		var i = 0;
		for(i=0;i < ids.length;i++) 
		{
			try 
			{
				var id = ids[i];
				ellist_processEntity__resumoGeralDespesas__._createRow(i);
				ellist_processEntity__resumoGeralDespesas__.setId(i, id);
			}
			catch(e) {} 
		}
	}
}

function refreshListaMensagens(objId, objNeoId, vetor1, vetor2)
{ 
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var url = base + "servlet/com.neomind.fusion.workflow.adapter.ems.EMSServletUtils?action=doAnalisaMensagemSistema&ids="+vetor1+"&objId="+objId+"&objNeoId="+objNeoId+"&cartao="+vetor2;
	var retorno = callSync(url);
	if(retorno == "") return;
	var ids = retorno.split(';');
	if(ids.length > 0)
	{
		var i = 0;
		for(i=0;i < ids.length;i++) 
		{
			try 
			{
				var id = ids[i];
				ellist_processEntity__mensagensSistema__._createRow(i);
				ellist_processEntity__mensagensSistema__.setId(i, id);
			}
			catch(e) {} 
		}
	}
}

function getMaskEMS(v)
{
	//R$ \#,\#\#0.00
	if (v == null || typeof v == 'undefined')
		return formatNumberBy3(0);
	else
	{
		v = new String(v);
		// #.## -> #,##
		if(v.indexOf(".") != -1 && v.indexOf(",") == -1)
		{
			v = v.replace('.', ',');
		}
	}
	if(v.indexOf(",") != -1){
		var x = v.split(",")
		if(x[1].length == 1)
			return formatNumberBy3(v + '0');
		else
			if(x[1].length == 2){
				return formatNumberBy3(v);
			}else
				return formatNumberBy3(newNumber(v).toFixed(2));
	}else
		return formatNumberBy3(v + ',00');
}

function newNumber(value)
{
	value = value.replace(/\./g, "");
	value = value.replace(",", ".");
		
	var number = new Number(value);
	if(isNaN(number))
		return new Number(0);
	return number;
}

function scanField(obj)
{	
	var obr = obj.getAttribute('mandatory');
	if(!obr) return;
	
	if(obj==null) return;
	
	var o = $(obj);
	var value = obj.value;
	
	
	if(!value  || value ==  null || value == '')
	{
	
		o.removeClass('input_text').addClass('mandatoryField');

	}else 
	{
		try
		{
			var n =  Number (value);
			if (n  == 0 || value == '0,00')
				o.addClass('mandatoryField');
		}catch(e)
		{
					
		}
	}
}

function addNewRowGrid(tableName, objId, fieldName, typeName, infoId, sourceId, eFormId, isChild, origin, id)
{
	var idLastRow = $('#' + tableName + ' tr:parent:last').attr('entityId');
	if(idLastRow  == id)
	{
		addNewRow(tableName, objId, fieldName, typeName, infoId, sourceId, eFormId, isChild, origin);
	}
}

function addNewRow(tableName, objId, fieldName, typeName, infoId, sourceId, eFormId, isChild, origin)
{
 	var url = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.ems.converter.EMSCustomGridActionServlet/?action=addNewObj&id=" + objId + "&field=" + fieldName + "&type=" + typeName + "&infoId=" + infoId + "&sourceId=" + sourceId + "&eFormId=" + eFormId + "&isChild=" + isChild + "&origin=" + origin ;
	var result = unescape(callSync(url, true));

	//Adiciona sempre ao final da tabela
	$('#' + tableName + ' tr:parent:last').after(result); // corrigir

	var idLastRow = $('#' + tableName + ' tr:parent:last').attr('entityId');
	$('#id_txt_Produto__' + idLastRow +'__').focus();
}

function returnScriptAddRow(script)
{
	return script;
}

function removeObjFromList(obj, id, path)
{

	if (confirm('Confirmar remo��o?'))
	{
		var parentObj = $(obj.parentNode);
		var url = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.ems.converter.EMSCustomGridActionServlet/?action=removeObjFromList&id=" + parentObj.attr('entityId') + "&parentId=" + $(obj.parentNode.parentNode.parentNode).attr('entityId') + "&fieldName=" + parentObj.attr('fieldName');
		
		var result = unescape(callSync(url, true));
						
		if (result == "1")
		{
			var parentObj = $(obj.parentNode);
			$('#' + parentObj.attr('id') + '_child').remove();
			$('#th_' + parentObj.attr('entityId')).remove();
			parentObj.remove();
		}
		else
			alert(result);
		
	}
}

function updateGridField(obj)
{
	if(obj==null) return;
	var o = $(obj);
	if(o.attr('changed') == "false" || o.attr('changed') == false)
		return;
	if(obj.tagName == "SELECT")
		obj = obj.options[obj.selectedIndex];
		
	var value = obj.value;
	var id = o.attr('parentId');
	var field = o.attr('fieldName');
	var formid =  o.attr('eformID');
	
	if(id && value && field)
	{
		var url = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.ems.converter.EMSCustomGridActionServlet/?action=update&id=" + id + "&value=" + value + "&field=" + field +"&formid="+formid;
		callFunction(url, 'callBackGridUpdate', true);
		o.changed = true;
		o.attr('changed',true);
	}
}

function moeda(z){ 
	v = z.value; 
	v=v.replace(/\D/g,"") // permite digitar apenas numero 
	v=v.replace(/(\d{1})(\d{14})$/,"$1.$2") // coloca ponto antes dos ultimos digitos 
	v=v.replace(/(\d{1})(\d{11})$/,"$1.$2") // coloca ponto antes dos ultimos 11 digitos 
	v=v.replace(/(\d{1})(\d{8})$/,"$1.$2") // coloca ponto antes dos ultimos 8 digitos 
	v=v.replace(/(\d{1})(\d{5})$/,"$1.$2") // coloca ponto antes dos ultimos 5 digitos 
	v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") // coloca virgula antes dos ultimos 2 digitos 
	z.value = v;
}

function onlyDigit(e, moeda) 
{
  var unicode = e.charCode ? e.charCode : e.keyCode;
  if (unicode != 8 && unicode != 9 && !(moeda && unicode == 44)) 
  {
  	if (unicode<48||unicode>57) 
  	{
    	return false
   	}
  }
  
  if(moeda)
  {
  	if(unicode == 44 && moeda.value.lastIndexOf(",") != -1)
  		return false;
  	
  	return moeda(moeda);
  }
}

function validaNan(value)
{
	var number = new Number(value);
	if(isNaN(number) ||number == 'Infinity')
	{
		return new Number(0);
	}
	else{
		return value;
	}
}


function formatNumberBy3(num, decpoint, sep) {
  if (arguments.length == 2) {
    sep = ".";
  }
  if (arguments.length == 1) {
  	decpoint = ",";
    sep = ".";
  }
  num = num.toString();
  a = num.split(decpoint);
  x = a[0]; // decimal
  y = a[1]; // fraction
  z = "";
  if (typeof(x) != "undefined") {
  	aux = x.replace('-','');
  	aux = aux.replace(/\./g,"");
    for (i=aux.length-1;i>=0;i--)
      z += aux.charAt(i);
	z = z.replace(/(\d{3})/g, "$1" + sep);
    if (z.slice(-sep.length) == sep)
      z = z.slice(0, -sep.length);
    if (x.substring(0,1) == '-')
    	x = "-";
    else
    	x = "";

    for (i=z.length-1;i>=0;i--)
      x += z.charAt(i);
    if (typeof(y) != "undefined" && y.length > 0)
      x += decpoint + y;
  }
  return x;
}

function hideShowObjChilds(obj, atividadeId)
{
	if (obj.getAttribute('esconde') == "false")
	{

		$("#" + obj.parentNode.id + "_child").hide();
		obj.innerHTML = "<img src='imagens/core/plus_checkbox.gif' />";
		obj.setAttribute('esconde', true);
		obj.esconde = "true";
	}
	else
	{
		$("#" + obj.parentNode.id + "_child").show();
		obj.innerHTML = "<img src='imagens/core/minus_checkbox.gif' />";
		obj.setAttribute('esconde', false);
		obj.esconde = "false";
	}
}

function calculaValoresGridRDV(rootObjId, parentId)
{
	var tipoDespesa = document.getElementById('id_txt_processEntity__listaItinerarios__definicaoDespesas__tipoDespesa__' + rootObjId).value;
	
	var quantidade = newNumber(document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__quantidade__' + rootObjId).value);
	var valor = newNumber(document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__valor__' + rootObjId).value);
	var totalValorKM = 0;
		
	var resultadoTotal = 0;
	var resultadoMedia = 0;
	if(tipoDespesa == 'Taxa Km Empresa')
	{
		var taxaKmRodado = newNumber(document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__taxaKmRodado__' + rootObjId).value);

		//totalValorKM = $taxaKmRodado * $quantidade
		totalValorKM = Math.round((taxaKmRodado * quantidade)*100)/100;
		totalValorKM = getMaskEMS(totalValorKM);
	
		//TOTAL
		resultadoTotal = totalValorKM; 
		
		//DESPESAS COM KM
		if(document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__totalValorKM__' + rootObjId).type == 'text'){
		document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__totalValorKM__' + rootObjId).value = totalValorKM;
		}else{
			document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__totalValorKM__' + rootObjId).innerHTML = totalValorKM;
		}
		document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__totalValorKM__' + rootObjId).changed = true;
		document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__totalValorKM__' + rootObjId).setAttribute('changed', true);
		document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__totalValorKM__' + rootObjId).onblur();
		
		//VALOR MEDIO
		//($totalValorKM / $quantidade)
		resultadoMedia = taxaKmRodado;
	
	}else{
		//TOTAL
	    resultadoTotal = getMaskEMS(valor);
	    
	    //VALOR MEDIO
	    //($valor / $quantidade)
		resultadoMedia = Math.round((valor/quantidade)*100)/100; 
	}
	
	resultadoMedia = validaNan(resultadoMedia);
	
	var resultado = getMaskEMS(resultadoMedia);
	
	//VALOR MEDIO
	if(document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__media__' + rootObjId).type == 'text'){
		document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__media__' + rootObjId).value = resultado;
	}else{
		document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__media__' + rootObjId).innerHTML = resultado;
	}
	document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__media__' + rootObjId).changed = true;
	document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__media__' + rootObjId).setAttribute('changed', true);
	document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__media__' + rootObjId).onblur();
	
	//VALOR TOTAL
	if(document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__valorTotal__' + rootObjId).type == 'text'){
		document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__valorTotal__' + rootObjId).value = resultadoTotal;
	}else{
		document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__valorTotal__' + rootObjId).innerHTML = resultadoTotal;
	}
	document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__valorTotal__' + rootObjId).changed = true;
	document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__valorTotal__' + rootObjId).setAttribute('changed', true);
	document.getElementById('var_processEntity__listaItinerarios__definicaoDespesas__valorTotal__' + rootObjId).onblur();
	
	//TOTAL DESPESAS REEMBOLSAVEIS
	jQuery.globalEval('updateprocessEntity__listaItinerarios__totalGeral_'+parentId+'()');
}

function callBackGridUpdate(ok, result)
{
	result = unescape(result);
	var results = result.split(';');
	if(results[0] == "0")
		alert(results[1]);
}



function exchangeInnerHTML(id1, id2, idUpdate, atividadeId){

	if (typeof atividadeId != undefined && atividadeId != null  && document.getElementById(id1+atividadeId)  && document.getElementById(id2+atividadeId))
	{
		if(idUpdate != "" && idUpdate != null){
			var innerTd = document.getElementById(id1+atividadeId).innerHTML;
			if(innerTd.indexOf('<span') == -1 && innerTd.indexOf('<SPAN') == -1)
				eval('update'+idUpdate+atividadeId+'()');
		}
		var remove = document.getElementById(id1+atividadeId);
		var insere = document.getElementById(id2+atividadeId);
		var html = remove.innerHTML;
		var htmlVazio = insere.innerHTML;
		remove.innerHTML = htmlVazio;
		insere.innerHTML = html;
	}
}
