<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

function gerarMinutaContrato()
{
	var idContrato = document.getElementById('hid_root').value;
	
	var url = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils?action=doGerarMinuta&idContrato='+idContrato;
	try
	{
		location.href=url;
	}
	finally
	{
		
	}
}

function gerarCroquiContrato()
{
	var idContrato = document.getElementById('hid_root').value;
	var url = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils?action=doGerarMinuta&isCroqui=true&idContrato='+idContrato;
	try
	{
		location.href=url;
	}
	finally
	{
		
	}
}

function historicoAfastamento(){
<!-- 	var dadosCol = document.getElementById('id_txt_colaborador__'); -->
	
<!-- 	if (!dadosCol){ -->
<!-- 		var dadosDiv = document.getElementById('div_colaborador__').textContent; -->
		
<!-- 		var arrayDados = dadosDiv.split(":"); -->
		
<!-- 		dadosCol = arrayDados[1]; -->
		
<!-- 	}else{ -->
<!-- 		dadosCol = document.getElementById('id_txt_colaborador__').value; -->
	
<!-- 	} -->
	
<!-- 	var possuiValor = document.getElementById('possuiValor').value; -->
<!-- 	if(possuiValor != dadosCol){ -->
<!-- 		var elmtTable = document.getElementById('historicoAfastamento'); -->
<!-- 		var tableRows = elmtTable.getElementsByTagName('tr'); -->
<!-- 		var rowCount = tableRows.length; -->
<!-- 		for (var x=rowCount-1; x>0; x--) { -->
<!-- 			elmtTable.deleteRow(x); -->
<!-- 		} -->
<!--     	var data = 'action=listaAfastamentosJ003&dadosCol='+dadosCol;  -->
    	
<!--     	var elementoTitulo = document.getElementById('headerTitle').innerHTML; -->
		
<!-- 		var arrayTitulo = elementoTitulo.split("-");    	    	 -->
    	    	
<!--     	var atividade = arrayTitulo[0].trim(); -->
    	
<!-- 		jQuery.ajax({ -->
<!-- 			method: 'post', -->
<%-- 			url: '<%=PortalUtil.getBaseURL()%>/servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet', --%>
<!-- 			dataType: 'json', -->
<!-- 			data: data, -->
<!-- 			success: function(result){ -->
<!--        			for(var i in result){ -->
       			
<!-- 	       			if (atividade == "Validar atestado m�dico"){ -->
<!-- 	       				if (result[i].sitAfa == 14 || result[i].sitAfa == 3 || result[i].sitAfa == 78){ -->
<!-- 		       				var texto = "<tr>"; -->
<!-- 		       				texto = texto + "<td>"+result[i].datAfa+"</td>"; -->
<!-- 		       				texto = texto + "<td>"+result[i].datTer+"</td>"; -->
<!-- 		       				texto = texto + "<td>"+result[i].sitAfa+" - "+result[i].desSit+"</td>"; -->
<!-- 		       				texto = texto + "</tr>"; -->
<!-- 							$('#historicoAfastamento').append(texto); -->
<!-- 			    		}	 -->
<!-- 	      			}else{ -->
<!-- 		      			var texto = "<tr>"; -->
<!-- 	       				texto = texto + "<td>"+result[i].datAfa+"</td>"; -->
<!-- 	       				texto = texto + "<td>"+result[i].datTer+"</td>"; -->
<!-- 	       				texto = texto + "<td>"+result[i].sitAfa+" - "+result[i].desSit+"</td>"; -->
<!-- 	       				texto = texto + "</tr>"; -->
<!-- 						$('#historicoAfastamento').append(texto); -->
	      			
<!-- 	      			} -->
<!--        			}     			 -->
<!-- 		    }	      -->
	        
           
<!--    		});			 -->
<!-- 		document.getElementById('possuiValor').value = dadosCol; -->
<!-- 	}else{ -->
<!-- 		alert("N�o h� mais dados a serem listados!");	 -->
<!-- 	} -->

} 

function historicoMedico(){
<!-- 	var crmMedico = document.getElementById('id_txt_medico__'); -->
	
<!-- 	if(!crmMedico){ -->
<!-- 		crmMedico = document.getElementById('div_medico__crm__').innerHTML; -->
		
<!-- 		var array = crmMedico.split('</div>'); -->
		
<!-- 		crmMedico = array[1]; -->
		
<!-- 	}else{ -->
<!-- 		crmMedico = document.getElementById('id_txt_medico__').value; -->
<!-- 	} -->
	
<!-- 	var possuiValor = document.getElementById('possuiValorCRM').value; -->
<!-- 	if(possuiValor != crmMedico){ -->
<!-- 		var elmtTable = document.getElementById('historicoMedico'); -->
<!-- 		var tableRows = elmtTable.getElementsByTagName('tr'); -->
<!-- 		var rowCount = tableRows.length; -->
<!-- 		for (var x=rowCount-1; x>0; x--) { -->
<!-- 			elmtTable.deleteRow(x); -->
<!-- 		} -->
<!-- 		var data = 'action=historicoMedicoJ003&crmMedico='+crmMedico;  -->
<!-- 		jQuery.ajax({ -->
<!-- 			method: 'post', -->
<%-- 			url: '<%=PortalUtil.getBaseURL()%>/servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet', --%>
<!-- 			dataType: 'json', -->
<!-- 			data: data, -->
<!-- 			success: function(result){ -->
<!--        			for(var i in result){ -->
<!--        				var texto = "<tr>"; -->
<!--        				texto = texto + "<td>"+result[i].code+"</td>"; -->
<!--        				texto = texto + "<td>"+result[i].numemp+"</td>"; -->
<!--        				texto = texto + "<td>"+result[i].numcad+"</td>"; -->
<!--        				texto = texto + "<td>"+result[i].nomfun+"</td>"; -->
<!--        				texto = texto + "<td>"+result[i].cid+" - "+result[i].desSub+"</td>"; -->
<!--        				texto = texto + "</tr>"; -->
<!-- 					$('#historicoMedico').append(texto); -->
<!-- 		    	}	      -->
<!-- 	        } -->
           
<!--    		});			 -->
<!-- 		document.getElementById('possuiValorCRM').value = crmMedico; -->
<!-- 	}else{ -->
<!-- 		alert("N�o h� mais dados a serem listados!");	 -->
<!-- 	} -->

} 

function historicoAnotacoes(){
<!-- 	var dadosCol = document.getElementById('id_txt_colaborador__'); -->
	
<!-- 	if (!dadosCol){ -->
<!-- 		var dadosDiv = document.getElementById('div_colaborador__').textContent; -->
		
<!-- 		var arrayDados = dadosDiv.split(":"); -->
		
<!-- 		dadosCol = arrayDados[1]; -->
		
<!-- 	}else{ -->
<!-- 		dadosCol = document.getElementById('id_txt_colaborador__').value; -->
	
<!-- 	} -->
	
<!-- 	var possuiValor = document.getElementById('possuiValorAnotacoes').value; -->
<!-- 	if(possuiValor != dadosCol){ -->
<!-- 		var elmtTable = document.getElementById('tableHistoricoAnotacoes'); -->
<!-- 		var tableRows = elmtTable.getElementsByTagName('tr'); -->
<!-- 		var rowCount = tableRows.length; -->
<!-- 		for (var x=rowCount-1; x>0; x--) { -->
<!-- 			elmtTable.deleteRow(x); -->
<!-- 		} -->
		
		
<!-- 		var arrayCol = dadosCol.split("-"); -->
		
<!-- 		var empresa = arrayCol[0].trim(); -->
		
<!-- 		var matricula = arrayCol[1].trim(); -->
		
<!-- 		var tipoCol = 1; -->
		
<!--     	var data = 'action=listaAnotacoes&numemp='+empresa+'&tipcol='+tipoCol+'&numcad='+matricula;  -->
<!-- 		jQuery.ajax({ -->
<!-- 			method: 'post', -->
<%-- 			url: '<%=PortalUtil.getBaseURL()%>/servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet', --%>
<!-- 			dataType: 'json', -->
<!-- 			data: data, -->
<!-- 			success: function(result){ -->
<!--        			for(var i in result){ -->
<!--        			var texto = "<tr>"; -->
<!--        				texto = texto + "<td>"+result[i].datnot+"</td>"; -->
<!--        				texto = texto + "<td>"+result[i].tipnot+"</td>"; -->
<!--        				texto = texto + "<td>"+result[i].desnot+"</td>"; -->
<!--        				texto = texto + "</tr>"; -->
<!-- 					$('#tableHistoricoAnotacoes').append(texto); -->
<!-- 		    	}	      -->
<!-- 	        } -->
           
<!--    		});			 -->
<!-- 		document.getElementById('possuiValorAnotacoes').value = dadosCol; -->
<!-- 	}else{ -->
<!-- 		alert("N�o h� mais dados a serem listados!");	 -->
<!-- 	} -->

}

function limparLista() 
{
	var control = 0;
	
	var objId = document.getElementById('eForm').value;
	
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var url = base + "servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils?action=doLimpaListaEquipamentos&objId="+objId;
	var retorno = callSync(url);
	if(retorno == "") return;
	var ids = retorno.split(';');
	
	var count = ellist_postosContrato__equipamentosItens__equipamentosPosto__.getRowCount();
	if((count && count > 0))
	{
		var i = 0;
		try 
		{
			var indice = 0;
			while(i < count) 
			{
				var id = ellist_postosContrato__equipamentosItens__equipamentosPosto__.tBody.rows[indice].entityId;
				if(retorno.indexOf(id) != -1)
				{
					ellist_postosContrato__equipamentosItens__equipamentosPosto__.tBody.deleteRow(indice);
				}else
				{
					indice++;
				}
				i++;
			}
		}
		catch(e){}
	}
}

var teste = 2;
function atualizaEquipamentosCheckList()
{
	//alert('teste 1');
	teste = this;
	var area_scroll = $(this.document).find("#area_scroll");
	area_scroll.block({ 
        message: '<h1>Processando lista de Equipamentos...</h1>', 
        timeout: 2500	
    });
	 
	var vetorEquipamentos 	= new Array();
	
	var contadorEquipamentos 	= ellist_postosContrato__equipamentosItens__kitsPosto__.getRowCount();
	
	//alert(contadorEquipamentos);
	if(ellist_postosContrato__equipamentosItens__kitsPosto__.deleted)
	{
		alert("deleta");
		limparLista();
	}else
	{
		//alert("insere");
		var i;
	 	for(i=0;i < contadorEquipamentos;i++)
		{
			var id 				= ellist_postosContrato__equipamentosItens__kitsPosto__._getRow(i);
			var entityIdAtual 	= id.entityId;
			vetorEquipamentos.push(entityIdAtual);
		}
		
		limparLista();
		refreshListaEquipamentos(document.getElementById('eForm').value, document.getElementById('hid_postosContrato__equipamentosItens__').value, vetorEquipamentos);
	}
}

function refreshListaEquipamentos(objId, objNeoId, vetor1)
{ 
	//alert('r� teste 1');
	console.log("[Fluxo Contratos] - refreshListaEquipamentos");
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var url = base + "servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils?action=doPopulaListaEquipamentos&ids="+vetor1+"&objId="+objId+"&objNeoId="+objNeoId;
	
	
	
	var retorno = callSync(url);
	if(retorno == "") return;
	var ids = retorno.split(';');
	
	var contadorEquipamentos 	= ellist_postosContrato__equipamentosItens__equipamentosPosto__.getRowCount();
	console.log("[Fluxo Contratos] - contadorEquipamentos " + contadorEquipamentos);
	if(ids.length > 0)
	{
		console.log("[Fluxo Contratos] - ids > 0 ");
		var idf = 0;
		for(i=contadorEquipamentos;i < ids.length+contadorEquipamentos;i++) 
		{
			console.log("e4789");
			try 
			{
				var id = ids[idf];
				idf++;
				ellist_postosContrato__equipamentosItens__equipamentosPosto__._createRow(i);
				ellist_postosContrato__equipamentosItens__equipamentosPosto__.setId(i, id);
			}
			catch(e) {console.log("[Fluxo Contratos] - Erro ao atualizar lista");} 
		}

	}
}

function calcularValorISS()
{
	var form = document.getElementById('eForm').value;
	var cep = document.getElementById('var_postosContrato__enderecoPosto__cep__').value;
	var codSer;
	if ( document.getElementById('div_postosContrato__complementoServico__codser__') != null ){
		codSer = document.getElementById('div_postosContrato__complementoServico__codser__').innerText;
	}
	
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var url = base + "servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils?action=doCalculaISS&form="+form+"&cep="+cep+"&codser="+codSer;
	var retorno = callSync(url);
	if(retorno == "") return;
	var ids = retorno.split(';');
	if(ids.length > 0)
	{
		var i = 0;
		if(ids[0] == '0')
		{
			document.getElementById('var_postosContrato__valorISS__').value = "0,00";
			alert(ids[1]);
			return;
		}else
		{
			var iss = ids[1];
			document.getElementById('var_postosContrato__valorISS__').value = iss;
		}
	}
}

function buscaCEP(id, rua, bairro, estado, cidade, eform)
{
	var base = "<%=PortalUtil.getBaseURL( )%>";
	var url = base + "servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils?action=doFindEndereco&cep="+id.value.replace("-","")+"&eform="+eform.value;
	$.ajax({
	  url: url,
	  /*dataType: 'text/html',*/
	  crossDomain: true,
	  /*contentType: "application/json",*/
	  statusCode: {
	    200: function(data) { 
	    	
	    	var retorno = data;
	    	if (retorno.indexOf('ORSEGERR-NEF') > -1){
	    		var dados = retorno.split(';');
	    		
	    		var x = 'Alerta: ' + dados[1] + "\n ";
	    		x += 'CEP: ' + dados [2];
	    		x += ' \nLogradouro: ' + dados [3].toUpperCase();
	    		x += ' \nBairro: ' + dados [4].toUpperCase();
	    		x += ' \nCidade: ' + dados [5];
	    		x += ' \nEstado: ' + dados [6];
	    		
	    		rua.value = '';
	    		bairro.value = '';
	    		alert(x);
	    	}if (retorno.indexOf('ORSEGERR-NE') > -1){
	    		var dados = retorno.split(';');
	    		
	    		var x = 'Alerta: ' + dados[1] + "\n ";
	    		
	    		rua.value = '';
	    		bairro.value = '';
	    		alert(x);
	    	}else{
	    		
		    	var ids = retorno.split(';');
		    	
		    	var ele=document.getElementById(estado.name);
		    	for(var ii=0; ii < ele.length; ii++)
				{
					if(ele.options[ii].text == ids[0]) 
					{ //Found!
					  ele.options[ii].selected=true;
					}
				}
				
				var campoCidade = cidade.name.replace("id_", "id_txt_");
				document.getElementById(cidade.name).value = ids[1];
				document.getElementById(campoCidade).setAttribute("selectedvalue", ids[2]);
				document.getElementById(campoCidade).value = ids[2];
				
				rua.value = ids[3].toUpperCase();
	    		bairro.value = ids[4].toUpperCase();
			}
		} // Ok
	    ,400: function(msg) { alert(msg);  } // Bad Request
	    ,404: function(msg) { alert("CEP n�o encontrado!!"); } // Not Found
	  }
	});
}

function listarPostosContrato()
{
	var contrato = document.getElementById('id_txt_numContrato__').value;
	if(contrato)
	{
		var empresaFil = document.getElementById('id_empresa__').value;
		var numCtr = contrato.split(" - ", 1)
		
		var base = "<%=PortalUtil.getBaseURL( )%>";
		var url = base + "servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils?action=doListarPostos&numctr="+numCtr+"&codemp="+empresaFil;
		var retorno = callSync(url);
		if(retorno == "") return;
		var ids = retorno.split(';');
		if(ids.length > 0)
		{
			for(i=0;i < ids.length;i++) 
			{
				try 
				{
					var id = ids[i];
					ellist_listaPostosResumo__._createRow(i);
					ellist_listaPostosResumo__.setId(i, id);
				}
				catch(e) {} 
			}
		}

	}else
	{
		alert("Favor selecionar um contrato!");
	}
}


function consultaSerasa(eform)
{
	
	var base = "<%=PortalUtil.getBaseURL()%>";
	var url = base + "servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils?action=doConsultaSerasa"+"&eform="+eform;
	//alert(url);
	$.ajax({
	  url: url,
	  dataType: 'text/plain',
	  crossDomain: true,
	  contentType: "text/plain",
	  statusCode: {
	    200: function(data) { 
	    	var retorno = data;
    		jQuery('#var_dadosGeraisContrato__serasa__').val('');
	    	if ( retorno.responseText.indexOf('Erro') > -1 ){
	    		alert('O servico retornou um Erro B900. Informe a TI.');
	    	}else{
	    		jQuery('#var_dadosGeraisContrato__serasa__').val(retorno.responseText);
    		}
	    	
		} // Ok
	    ,400: function(msg) { alert(msg);  } // Bad Request
	    ,404: function(msg) { alert("Servico nao encontrado!!"); } // Not Found
	    ,500: function(msg) { alert("Ocorreu um erro no servico. Tente novamente.") }
	  }
	});
}

function atualizaRSC()
{
	var codCliSpan = $( "#id_clienteSapiens__" ).val();
	if(codCliSpan != null)
	{
			var base = "<%=PortalUtil.getBaseURL()%>";
			var url = base + "servlet/com.neomind.fusion.custom.orsegups.servlets.OrsegupsServletUtils?action=doVerificaRSCRelacionado&codCli="+codCliSpan;
			var retorno = callSync(url); 
			if(retorno == '')
			{
				return;
			}
			var ids = retorno.split(';');
			var count = ellist_RSCRelacionados__.getRowCount(); 
			if((count && count > 0))
			{
				var i = 0; 
				try 
				{
					for(i=0;i < count;i++) 
					{
						ellist_RSCRelacionados__.tBody.deleteRow(i); 
					}
				}
				catch(e){}
			} 
			
			if(ids.length > 0)
			{
				var i = 0; 
				for(i=0;i < ids.length;i++) 
				{
					try
					{
						var id = ids[i]; 
						ellist_RSCRelacionados__._createRow(i); 
						ellist_RSCRelacionados__.setId(i, id); 
					}
					catch(e) {}
				}
		}	
	}
	else
	{
			alert('Nenhum cliente selecionado!');
	}
}