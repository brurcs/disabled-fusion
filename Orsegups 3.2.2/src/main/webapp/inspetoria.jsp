<%@ page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="application/vnd.android.package-archive; charset=ISO-8859-1">
	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="<%=PortalUtil.getBaseURL()%>apks/css/bootstrap.min.css">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="<%=PortalUtil.getBaseURL()%>apks/css/bootstrap-theme.min.css">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="<%=PortalUtil.getBaseURL()%>apks/js/jquery-2.1.1.min.js"></script>
	<script src="<%=PortalUtil.getBaseURL()%>apks/js/bootstrap.min.js"></script>
	<style type="text/css">
		.centered{
		    float: none;
		    margin: 0 auto;
		}
	</style>
	<script type="text/javascript">
		function openV23(){
			jQuery("#v23").show();
			jQuery("#v4").hide();
		} 
		function openV4(){
			jQuery("#v4").show();
			jQuery("#v23").hide();
		}
		
	</script>

<title>Inspetoria Mobile</title>
</head>
<body>
	<div class="container">
		<div class="panel panel-default">
			<a name="topo"></a>
			<div class="panel-heading"><center>Inspetoria Mobile</center></div>
			<div class="panel-body">
				<center>
					<%
						if ("1".equals(request.getParameter("teste"))){
						%>
						<a class="btn btn-primary btn-lg" role="button" href="<%=PortalUtil.getBaseURL()%>apks/teste.apk" >
							<img src="<%=PortalUtil.getBaseURL()%>apks/images/android.png" style="border: 0px; width: 32px; height: 32px;" >
							Instalar o Inspetoria Mobile Teste <br> (Requer desinstala��o da vers�o anterior)
						</a>
						<%
						}
					%>
					<a class="btn btn-primary btn-lg" role="button" href="<%=PortalUtil.getBaseURL()%>apks/inspection.apk" >
						<img src="<%=PortalUtil.getBaseURL()%>apks/images/android.png" style="border: 0px; width: 32px; height: 32px;" >
						Instalar o Inspetoria Mobile 1.9 <br> (Requer desinstala��o da vers�o anterior)
					</a>
					
					<br/>
					<br/>
					
					<a class="btn btn-primary btn-xs" role="button" href="#v23" onclick="javascript: openV23();" >
						<p>
							Tutorial de Instala��o at� Android 2.3
						</p>
					</a> 
					<a class="btn btn-primary btn-xs" role="button" href="#v4" onclick="javascript: openV4();">
						<p>
							Tutorial de Instala��o Android 4.0+ 
						</p>
					</a>
					<br/>
					<br/>
					<div class="panel panel-default">
						<div class="panel-heading"><center>Altera��es da Vers�o 1.9</center></div>
						<div class="panel-body">
									<ul style="text-align: left;">
										<li>
											Implementado a funcionalidade de capturar imagens para as respostas das inspe��es.
										</li>
									</ul> 
						</div>
					</div>
					<br/>
					<br/>
					<div class="panel panel-default">
						<div class="panel-heading"><center>Altera��es da Vers�o 1.8</center></div>
						<div class="panel-body">
									<ul style="text-align: left;">
										<li>
											Melhorias no importador de clientes. Agora o centro de custo vem concatenado ao nome do posto.
										</li>
										<li>
											Corre��es diversas de bugs.
										</li>
									</ul> 
							
						</div>
					</div>
					<br/>
					<br/>
					<div id="v23" style="display:none;">
						
						
						<div class="well well-sm">
							<h4><a name="v23">Tutorial de Instala��o at� Android 2.3</a></h4>
							<br />
							<p align="justify">
								Antes de baixar e instalar o aplicativo Inspetoria Mobile, seu android precisa estar preparado para aceitar esta instala��o.
								Para isso, siga os passos abaixo para ativar a instala��o apartir de fontes desconhecidas. Isso permitir� que o aplicativo hospedado 
								nos servidores da Orsegups possam ser instalados no seu dispositivo m�vel.
							</p>
							<p style="text-align: left">* Acesse as  configura��es (settings) </p> 
							<p style="text-align: left">* Selecione Aplica��es (Applications) </p>
							<p style="text-align: left">* Marque a op��o: Fontes Desconhecidas (Unknown sources) </p>
							<p>
								<img src="<%=PortalUtil.getBaseURL()%>apks/images/androidate23.jpg" class="img-responsive" >
							</p>
							<p align="justify">
								Agora v� at� o <a href="#topo">topo</a> e clique em <b>Instalar o Inspetoria Mobile</b>, ap�s o download do arquivo, ir� aparecer na �rea de notifica��es
								um seta indicando que o download foi concluido. Abra as notifica��es e clique no nome do arquivo para instalar.   
							</p>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p1.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p2.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p3.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p4.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p5.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p6.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p7.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p8.png" class="img-responsive" ></p><br/>
								
								<a href="#topo">Voltar ao topo</a>
							
						</div>
					</div>
					
					<div id="v4" style="display:none;">
						<div class="well well-sm">
								<h4><a name="v4">Tutorial de Instala��o Android 4.0+</a></h4>
								<br />
								<p align="justify">
									Antes de baixar e instalar o aplicativo Inspetoria Mobile, seu android precisa estar preparado para aceitar esta instala��o.
									Para isso, siga os passos abaixo para ativar a instala��o apartir de fontes desconhecidas. Isso permitir� que o aplicativo hospedado 
									nos servidores da Orsegups possam ser instalados no seu dispositivo m�vel.
								</p>
								<p style="text-align: left">* Acesse as  configura��es (settings) </p> 
								<p style="text-align: left">* Encontre e acesse a op��o seguran�a (security) </p>
								<p style="text-align: left">* Marque a op��o: Fontes Desconhecidas (Unknown sources) </p>
								<p>
									<img src="<%=PortalUtil.getBaseURL()%>apks/images/android4oumaior.png" class="img-responsive" > 
								</p>
								<p align="justify">
									Agora v� at� o <a href="#topo">topo</a> e clique em <b>Instalar o Inspetoria Mobile</b>, ap�s o download do arquivo, ir� aparecer na �rea de notifica��es
									um seta indicando que o download foi concluido. Abra as notifica��es e clique no nome do arquivo para instalar.   
								</p>
								
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p1.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p2.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p3.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p4.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p5.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p6.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p7.png" class="img-responsive" ></p><br/>
								<p><img src="<%=PortalUtil.getBaseURL()%>apks/images/p8.png" class="img-responsive" ></p><br/>
								
								<a href="#topo">Voltar ao topo</a>
						</div>
					</div>
					
				</center> 
			</div>
			<div class="panel-footer">
				<center>
					<img src = "<%=PortalUtil.getBaseURL()%>apks/images/logo_orsegups.jpg" style="border: 0px; width: 32px; height: 32px;" >
					Orsegups Participa��es S.A.
				</center>
				</div>
		</div>
	</div>
</body>
</html>