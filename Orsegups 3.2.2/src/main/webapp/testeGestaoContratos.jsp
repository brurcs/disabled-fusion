<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>    
<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="com.neomind.fusion.persist.QLFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.common.NeoRunnable"%>   
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="com.neomind.util.NeoUtils"%>

<%@page import="com.neomind.fusion.custom.orsegups.contract.ContractSIDClient"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESTE Adapter FGC</title>
</head>
<body>
<%
	//79718089
	String neoId = request.getParameter("id");

	NeoObject objPai = PersistEngine.getNeoObject(NeoUtils.safeLong(neoId));

	EntityWrapper processEntity = new EntityWrapper(objPai);
	
	//ContractSIDClient
	NeoObject noCliente = (NeoObject) processEntity.findField("novoCliente").getValue();
	NeoObject noDadosContrato = (NeoObject) processEntity.findField("dadosGeraisContrato").getValue();
	
	//Chama o SID para alterar a empresa e a filial
	ContractSIDClient.sidChangeEmpFil("2", "1");
	
	//Cadastra um novo cliente para a empresa selecionada
	String codCli = ContractSIDClient.sidClient(noCliente);

	if (codCli != null)
	{
		ContractSIDClient.sidClientDefinitions(noDadosContrato, codCli, 0);
		ContractSIDClient.sidClientContact(noCliente, codCli);
	}


%>

</body>
</html>