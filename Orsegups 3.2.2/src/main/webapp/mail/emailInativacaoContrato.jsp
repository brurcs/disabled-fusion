<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@ taglib uri="/WEB-INF/mail.tld" prefix="mail"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@page import="com.neomind.fusion.workflow.*"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.*"%>
<%@page import="com.neomind.fusion.common.*" %>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>

<%
	String mensagem = NeoUtils.safeOutputString(request.getParameter("textoEMail"));
	String titulo   = NeoUtils.safeOutputString(request.getParameter("titulo"));
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<table width="550" border="0" align="center" cellpadding="5" cellspacing="0">
	<tr>
		<p class="mail_tit">
			<mail:title title="<%=titulo %>" />
		</p>
	</tr>
	
	<tr>
	<strong><%=mensagem%></strong>
	</tr>

</table>
</body>
</html>