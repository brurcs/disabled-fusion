<%@ taglib uri="/WEB-INF/mail.tld" prefix="mail"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@page import="com.neomind.fusion.workflow.*"%>
<%@page import="java.util.*"%>
<%@page import="com.neomind.fusion.common.*" %>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%
	String cgccpf = NeoUtils.safeOutputString(request.getAttribute("cgccpf"));
	String senha =  NeoUtils.safeOutputString(request.getAttribute("senha"));
	String fantasia =  NeoUtils.safeOutputString(request.getAttribute("fantasia"));
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 

"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<portal:baseURL url="getURLPath" />

<link href="<%=getURLPath%>/css/portal-decorator.css" rel="stylesheet"	type="text/css" />
<style TYPE="text/css">
          p,pre,body,table
          {
          font-size:10.0pt;
          font-family:'Arial';
          }
</style>
</head>
<body bgcolor="#FFFFFF">
<table id="Table_01" width="550" border="0" cellpadding="0"
	cellspacing="0" align="center">
	<tr>
	<td class="corpo_mail">
			<p class="mail_tit"><mail:title title='Sua senha de acesso ao Portal do Cliente (Boletos, Notas Fiscais, Seus dados, etc...)' /></p>
	</td>
	</tr>
	<tr>
	<td class="corpo_mail">
		<img src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/imgPesquisa.jpg" >	<p class="mail_tit"><mail:title title='Sua senha de acesso ao Portal do Cliente (Boletos, Notas Fiscais, Seus dados, etc...)' /></p>
	</td>
	</tr>	
	<tr>
		<td class="corpo_mail" align="left">
			Prezado: <strong><%=fantasia %></strong>
			<p>Voc� est� recebendo este e-mail para recupera��o da senha de acesso ao Portal do Cliente da Orsegups Participa��es S/A.</p>
			<strong>Dados de Acesso:</strong><br/>
			C�digo de acesso (CPF/CNPJ): <strong><%= cgccpf %></strong><br>
			Senha: <strong><%=senha%></strong>
			<br>
	<p>Esta mensagem � autom�tica e as respostas para este endere�o n�o ser�o monitoradas nem respondidas.</p>
	
		</td>
	</tr>
	<tr>
	<td class="corpo_mail">
		<img src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/imgPesquisaRodaPe.jpg" >	
		<p>Para garantir que nossos comunicados cheguem em sua caixa de entrada, adicione o email fusion@orsegups.com.br ao seu cat�logo de endere�os.</p>		
		Orsegups Participa��es S/A<br/>
		<a href="http://www.orsegups.com.br">http://www.orsegups.com.br</a> 
	</td>
	</tr>	
</table>
<!-- End ImageReady Slices -->
</body>
</html>