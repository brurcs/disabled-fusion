<%@page import="com.neomind.fusion.workflow.model.ProcessModelProxy"%>
<%@page import="com.neomind.fusion.workflow.WFEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="com.neomind.fusion.workflow.workdesk.Box"%>
<%@page import="com.neomind.fusion.workflow.workdesk.BoxItem"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.workflow.workdesk.WorkDesk"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.security.NeoGroup"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.util.CustomFlags"%>

<portlet:defineObjects/>
<portal:head>

<% long time = System.currentTimeMillis(); %>

<style type="text/css">

.dragged
{
	filter:alpha(opacity=60);
	-moz-opacity:.60;
	opacity:.60;
}
</style>
	<script type="text/javascript" src="js/boxover.js"></script>
	<script type="text/javascript" src="js/neo_ui/portal/portal.js" ></script>
	<script type="text/javascript" src="js/neo_ui/drag_drop/dd_gad.js" ></script>
	<script type="text/javascript" src="js/neo_ui/drag_drop/dd_task.js" ></script>
	<script type="text/javascript" src="js/neo_ui/drag_drop/modular.js" ></script>
	<script type="text/javascript" src="js/header.js" ></script>
	<script type="text/javascript" src="js/doscroll.js" ></script>
	<script type="text/javascript" src="js/menu.js" ></script>
	<script type="text/javascript" src="js/neo_ui/css.js" ></script>
	<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
	<cw:main>
		<cw:header title="tasksAndPendings"/>
		<cw:body id="area_scroll">
			<%
			WorkDesk userWorkDesk = (WorkDesk) request.getAttribute("userWorkDesk");
			WorkDesk aliasWorkDesk = (WorkDesk) request.getAttribute("aliasWorkDesk");			
			Collection<ProcessModelProxy> modelList = WFEngine.getInstance().getStartableModels(PortalUtil.getCurrentUser());
			
			NeoUser user = PortalUtil.getCurrentUser();
			
			boolean isProcuradorChefe = false;
			boolean isProcurador = false;
			boolean isSub = false;
			
			%>
			<div id="espera" style="display: none;"><img src="imagens/icones_final/load_bar_220x19.gif"
				style="position: absolute; top: 50%; left: 50%; margin-left: -110px; margin-top: -10px; z-index: 1000111;" />
				<div	
					style="width: 100%; height: 100%; position: absolute; top: 0; left: 0; background: #FFFFFF; opacity: 0.5; filter: alpha(opacity = 50); filter: progid : DXImageTransform . Microsoft . Alpha(opacity =   50); z-index: 1000000; text-align: center; vertical-align: middle;">
				</div>
			</div>
			
			<div id="user_header" class="user_name">
			<div class="wOpt">
				<span>
					<img src="imagens/icones_final/user_green_16x16-trans.png" /> &nbsp;<%=userWorkDesk.getUser().getFullName()%>&nbsp;&nbsp;
				</span>
				<span class="hideShowEye">
					<img src="imagens/icones_final/eye_no_16x16-trans.png" style="display:none" alt='<%=I18nUtils.getString("hideShowSystemBoxes") %>' id="hideImgA" onclick="javascript:hideShowSystemBoxes('panelA');">
					<img src="imagens/icones_final/eye_16x16-trans.png" style="display:inline" alt='<%=I18nUtils.getString("hideShowSystemBoxes") %>' id="hideImgA_no" onclick="javascript:hideShowSystemBoxes('panelA');">
				</span>
				<span class="hideShowEye"><img src="imagens/icones_final/arrow_refresh-16x16-trans.png" alt='<%=I18nUtils.getString("updateDesktop") %>' onclick="javascript:window.location.reload(true);"></span>
			</div>

					<%
						NeoGroup curGroup = user == null ? null : user.getGroup();
						if (user != null && curGroup != null)
						{
							EntityWrapper wrapperUser = new EntityWrapper(user);
							Collection groups = (Collection) wrapperUser.findValue("GruposDisponiveis");
							if (groups != null && groups.size() > 0 )
							{
						%>
							<div class="wOpt">
							<span id="changeGroup">
								<i18n:string key="activeGroup"/>:&nbsp;
								<select onchange="callSync('<%= PortalUtil.getBaseURL() %>servlet/com.neomind.fusion.security.SecurityServlet?action=changeGroup&group=' + this.value + '&ts=' + new Date().getTime()); canRefresh();">
								<%
									for (Object o : groups)
									{
										NeoGroup group = (NeoGroup)o;
								%>
									<option value="<%= group.getNeoId() %>" <%= NeoUtils.safeEquals(curGroup, group) ? "selected" : "" %>><%= new EntityWrapper(group).getAsString() %></option>
								<%
									}
								%>
								</select></span>
							</div>
						<%
							}
						}
						%>
					<div class="wOpt">
					<c:if test="<%= CustomFlags.PGM %>">
						<span id="spanAnalyse">
							<a href="javascript:makeAnAnalyse()" ><img src="imagens/icones_final/visualize_orange_16x16-trans.png" />&nbsp;<%= I18nUtils.getString("makeAnAnalyse") %></a>
						&nbsp;&nbsp;</span>
					</c:if>
					<span id="spanSelect" >
						<select id="workDeskProcessList" style="width:auto">
							<!-- <option value="0">(Escolha um workflow para iniciar)</option>  -->
							<option value="0"> (<%= I18nUtils.getString("chooseWorkflowToStart") %>) </option>
							<c:forEach items="<%= modelList %>" var="oModel">
								<% ProcessModelProxy proxy = (ProcessModelProxy)oModel; 
									ProcessModel model = proxy.getVersionControl().getCurrent();
								%>
								<option value="<%= model.getNeoId() %>"><%= model.getTitle() %></option>
							</c:forEach>
						</select>
					</span>
					<span class="hideShowEye" title='<%= I18nUtils.getString("startHelp") %>'><a href="javascript:startNewProcess();" ><img src="imagens/icones_final/process_start_16x16-trans.png" />&nbsp;<i18n:string key="start"/></a></span>
					<span class="hideShowEye" title='<%= I18nUtils.getString("cascadeStartHelp") %>'><a href="javascript:startNewProcesses();" ><img src="imagens/icones_final/process_start_16x16-trans.png" />&nbsp;<i18n:string key="cascadeStart"/></a></span>
					</div>
			</div>

			<div id="panelA" class="desk_panel" ></div>
			<div id="user_bottom">&nbsp;</div>
			<div id="splitDesk" class="header_hideshow" onmouseover="activeDeactiveObj('splitDesk')" onmouseout="activeDeactiveObj('splitDesk')" onclick="hideShowObj('panelB');hideShowObj('alias_header');  resizeWorkdesk(); swapHideShow('header_hideshow_btn', 'header_hideshow_btn2', 'inline');testResize()">
			<img src="imagens/drag/arrow_drag_up-trans.png"  class="header_hideshow_btn" id="header_hideshow_btn" width="11" height="6" style="display:none" /><img src="imagens/drag/arrow_drag_down-trans.png" id="header_hideshow_btn2" width="11" height="6" style="display:inline" /></div>
			<c:if test="<%=aliasWorkDesk != null %>">
				<div id="alias_header" class="user_name">
					<span>
						<img src="imagens/icones_final/user_green_16x16-trans.png" /> &nbsp;<%=aliasWorkDesk.getUser().getFullName()%>
					</span>&nbsp;
					<span class="hideShowEye"><img src="imagens/icones_final/eye_no_16x16-trans.png" style="display:none" alt='<%=I18nUtils.getString("hideShowSystemBoxes") %>' id="hideImgB" onclick="javascript:hideShowSystemBoxes('panelB');">
					<img src="imagens/icones_final/eye_16x16-trans.png" style="display:inline" alt='<%=I18nUtils.getString("hideShowSystemBoxes") %>' id="hideImgB_no" onclick="javascript:hideShowSystemBoxes('panelB');">
					&nbsp;
					</span>
				</div>
				<div id="panelB" class="desk_panel"></div>
				<div id="alias_bottom">&nbsp;</div>
				
			</c:if>
			<c:if test="<%=aliasWorkDesk == null %>">
				<div id="panelB" class="desk_panel" style="display:none;"></div>
			</c:if>
			<script type="text/javascript">
				var current = null;
				var pa = myId("panelA");
				var pb = myId("panelB");
				var notLoading = true;

				addEvent(pa, "mouseover", function setCurrentContainer()
				{
					if(current != pa)
					{
						current = pa;
						NEO.neoPortal.currentContainer = pa;
					}
				});

				addEvent(pb, "mouseover", function setCurrentContainerB()
				{
					if(current != pb)
					{
						current = pb;
						NEO.neoPortal.currentContainer = pb;
					}
				});
				
				if(NEO.neoUtils.returnParent().oneCol != true){
					myId("spanAnalyse").style.display = 'none';
				}
				
				function resizeWorkdesk(){
					NEO.neoPortal.setPanelSize();
				}
				
				myId("splitDesk").style.display = myId("panelB").style.display;
				
				function startNewProcess(){
					if (myId('workDeskProcessList').value != 0) {
						notLoading = false;
						startProcessOnWorkDesk(myId('workDeskProcessList').value, null, null,false);
					}
				}
				
				function startNewProcesses()
				{
					if (myId('workDeskProcessList').value != 0) {
						notLoading = false;
						startProcessOnWorkDesk(myId('workDeskProcessList').value, null, null, true);
					}					
				}
				
				function hideShowSystemBoxes(container){

					for(i in NEO.neoPortal.hideableDivs){
						var o = NEO.neoPortal.hideableDivs[i];
						if(container == o[0]){
							if(o[1].style.display == ''){
								o[1].style.display = 'none';
								o[2].style.display = 'none';
								myId("hideImg"+container.substr(5,6)).style.display = "inline";
								myId("hideImg"+container.substr(5,6)+"_no").style.display = "none";
							}else{
								o[1].style.display = '';
								o[2].style.display = '';
								myId("hideImg"+container.substr(5,6)).style.display = "none";
								myId("hideImg"+container.substr(5,6)+"_no").style.display = "inline";
							}
						}
					}
				}

				function editAlias(id){
					var url = '<%=PortalUtil.getBaseURL()%>portal/render/Form?type=Alias&edit=true&showContainer=true&id='+id;
					openURL(url);
				}
				
				var modalId;
				function editTask(id, name, activeFunction, obj){
					if(obj != null && obj != undefined){
						ddTask.transferItem(obj);
					}
					var url = 'portal/render/Form?type=Task&edit=true&canEdit=true&id='+id+'&callBackURL=<%=PortalUtil.getBaseURL()%>portal/render/WorkDesk&name='+name;
					if(activeFunction != null && activeFunction != undefined)
					{
						url = url + "&activeFunction="+activeFunction;
					}
					openURL(url);
				}

				function openURL(url){
					if(NEO.neoUtils.returnParent().oneCol != true){
						changePortlet('rightCol', url);
					} else {
						changePortlet('onlyCol', url);
					}
				}
				
				function viewTask(id){
					var url = 'portal/render/Form/view?type=Task&canEdit=false&edit=false&id='+id;
					openURL(url);
				}
				
				function changeResponsible(taskId)
				{
					callFunction("<%= PortalUtil.getBaseURL() %>entity/create/WorkDeskChangeTaskResponsible?initialParams=task="+taskId, "startChange", false);
				}
			
				function startChange(status, resp)
				{
					if(status)
					{
						var data = resp.split(";");
			
						if(data[0] == "1")
						{
							var url = "<%= PortalUtil.getBasePortalURL() %>render/Form?type=WorkDeskChangeTaskResponsible/view/normal?rootPath=null&form=true&rootId=null&id=" + data[1] + "&formModel=0&edit=true&select=true&showContainer=true&full=true&root=true&path=&mutable=true";
							NEO.neoUtils.dialog().addModal(true,null,null,600,350,'<i18n:string key="changeResponsible" />');
							NEO.neoUtils.dialog().createModal(url);
						}
					}
					else
					{
						alert('<%=I18nUtils.getString("errorTryingToResetTaskResponsable")%>');
					}
				}
	
				function load_all()
				{
					<%=request.getAttribute("building")%>
					doScroll('window_default', 0);
					hideShowSystemBoxes('panelA');
					hideShowSystemBoxes('panelB');

					<c:if test="<%= (aliasWorkDesk != null) && (isProcurador || isProcuradorChefe || isSub) %>">
						hideShowObj('panelB');
						hideShowObj('alias_header');
						resizeWorkdesk();
						swapHideShow('header_hideshow_btn', 'header_hideshow_btn2', 'inline');
						testResize();					
					</c:if>

					
					<c:if test="<%=userWorkDesk.getRefreshTime() != null && !userWorkDesk.getRefreshTime().equals(new Integer(0))%>">
						window.setTimeout('canRefresh()', <%=userWorkDesk.getRefreshTime() * 1000%>);
					</c:if>
				}

				/*
					Function implemented to prevent screen refresh while user is dragging a task.
					It makes use of an attribute setted in dd_task.js (myId('area_scroll').style.cursor == 'move') which verifies if a user is interacting with the workdesk while system tries to refresh the screen. 
					If user is dragging something, workdesk loops over the refreshtime, if not, the screen will be refreshed.
				*/
				function canRefresh()
				{
					if(notLoading){
						if (myId('area_scroll').style.cursor == 'move')
							window.setTimeout('canRefresh()', <%=userWorkDesk.getRefreshTime() * 1000%>);				
						else
							window.location.reload(true);
					}
				}
				
				addLoadListener(load_all);
			</script>
		</cw:body>
	</cw:main>
</portal:head>