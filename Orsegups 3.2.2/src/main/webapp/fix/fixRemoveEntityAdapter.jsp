
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.engine.update.fix.Fix00078"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%    
    //Remocao dos EI em cascade pos reinicializacao
    
	//CasvigClienteSapiens
	List objects1 = PersistEngine.getObjects(AdapterUtils.getEntityClass("EntityInfo"), new QLRawFilter("typeName = 'CasvigClienteSapiens'"));
	for(Object l : objects1)
	{
		Object o = l;
		PersistEngine.remove(o);
	}
	
	//CasvigRetornaCheckPoint
	List objects2 = PersistEngine.getObjects(AdapterUtils.getEntityClass("EntityInfo"), new QLRawFilter("typeName = 'CasvigRetornaCheckPoint'"));
	for(Object l : objects2)
	{
		Object o = l;
		PersistEngine.remove(o);
	}
	
	//CasvigDistribuiTarefas
	List objects3 = PersistEngine.getObjects(AdapterUtils.getEntityClass("EntityInfo"), new QLRawFilter("typeName = 'CasvigDistribuiTarefas'"));
	for(Object l : objects3)
	{
		Object o = l;
		PersistEngine.remove(o);
	}
	
	//CasvigDistribuiValidar
	List objects4 = PersistEngine.getObjects(AdapterUtils.getEntityClass("EntityInfo"), new QLRawFilter("typeName = 'CasvigDistribuiValidar'"));
	for(Object l : objects4)
	{
		Object o = l;
		PersistEngine.remove(o);
	}
	
	//CasvigFinalDate
	List objects5 = PersistEngine.getObjects(AdapterUtils.getEntityClass("EntityInfo"), new QLRawFilter("typeName = 'CasvigFinalDate'"));
	for(Object l : objects5)
	{
		Object o = l;
		PersistEngine.remove(o);
	}
	
	//CasvigMobilePararAtividade
	List objects6 = PersistEngine.getObjects(AdapterUtils.getEntityClass("EntityInfo"), new QLRawFilter("typeName = 'CasvigMobilePararAtividade'"));
	for(Object l : objects6)
	{
		Object o = l;
		PersistEngine.remove(o);
	}
%>

<!-- 
QUERY PRE REINICIALIZACAO -- PRECISA SER EXECUTADA ANTES DE RODAR ESTE JSP

 --Inativa processos sem modelo
 update WFProcess set processState = 2, saved = 0 where model_neoId is null
    
--CasvigClienteSapiens
drop table CasvigClienteSapiens

insert into customactivityadapter (clazz, neoid)
select 'com.neomind.fusion.workflow.adapter.casvig.CasvigClienteSapiens', no.neoid from neoobject no 
inner join neotype nt on no.neoobjecttype_id = nt.id
where nt.neotype = 'Fworkflow.adapter.casvig.CasvigClienteSapiens' and no.neoid in (
select neoid from Activity)

update neoobject set neoobject.neoobjecttype_id = (select id from NeoType WHERE neoType = 'Fworkflow.adapter.CustomActivityAdapter')
where neoobject.neoobjecttype_id in (Select nt.id from neotype nt where nt.id = neoobject.neoobjecttype_id and nt.neotype = 'Fworkflow.adapter.casvig.CasvigClienteSapiens')

--CasvigRetornaCheckPoint
drop table CasvigRetornaCheckPoint

insert into customactivityadapter (clazz, neoid)
select 'com.neomind.fusion.workflow.adapter.casvig.CasvigRetornaCheckPoint', no.neoid from neoobject no 
inner join neotype nt on no.neoobjecttype_id = nt.id
where nt.neotype = 'Fworkflow.adapter.casvig.CasvigRetornaCheckPoint' and no.neoid in (
select neoid from Activity)

update neoobject set neoobject.neoobjecttype_id = (select id from NeoType WHERE neoType = 'Fworkflow.adapter.CustomActivityAdapter')
where neoobject.neoobjecttype_id in (Select nt.id from neotype nt where nt.id = neoobject.neoobjecttype_id and nt.neotype = 'Fworkflow.adapter.casvig.CasvigRetornaCheckPoint')


--CasvigDistribuiTarefas
drop table CasvigDistribuiTarefas

insert into customactivityadapter (clazz, neoid)
select 'com.neomind.fusion.workflow.adapter.casvig.CasvigDistribuiTarefas', no.neoid from neoobject no 
inner join neotype nt on no.neoobjecttype_id = nt.id
where nt.neotype = 'Fworkflow.adapter.casvig.CasvigDistribuiTarefas' and no.neoid in (
select neoid from Activity)

update neoobject set neoobject.neoobjecttype_id = (select id from NeoType WHERE neoType = 'Fworkflow.adapter.CustomActivityAdapter')
where neoobject.neoobjecttype_id in (Select nt.id from neotype nt where nt.id = neoobject.neoobjecttype_id and nt.neotype = 'Fworkflow.adapter.casvig.CasvigDistribuiTarefas')

--CasvigDistribuiValidar
drop table CasvigDistribuiValidar

insert into customactivityadapter (clazz, neoid)
select 'com.neomind.fusion.workflow.adapter.casvig.CasvigDistribuiValidar', no.neoid from neoobject no 
inner join neotype nt on no.neoobjecttype_id = nt.id
where nt.neotype = 'Fworkflow.adapter.casvig.CasvigDistribuiValidar' and no.neoid in (
select neoid from Activity)

update neoobject set neoobject.neoobjecttype_id = (select id from NeoType WHERE neoType = 'Fworkflow.adapter.CustomActivityAdapter')
where neoobject.neoobjecttype_id in (Select nt.id from neotype nt where nt.id = neoobject.neoobjecttype_id and nt.neotype = 'Fworkflow.adapter.casvig.CasvigDistribuiValidar')

--CasvigFinalDate
drop table CasvigFinalDate

insert into customactivityadapter (clazz, neoid)
select 'com.neomind.fusion.workflow.adapter.casvig.CasvigFinalDate', no.neoid from neoobject no 
inner join neotype nt on no.neoobjecttype_id = nt.id
where nt.neotype = 'Fworkflow.adapter.casvig.CasvigFinalDate' and no.neoid in (
select neoid from Activity)

update neoobject set neoobject.neoobjecttype_id = (select id from NeoType WHERE neoType = 'Fworkflow.adapter.CustomActivityAdapter')
where neoobject.neoobjecttype_id in (Select nt.id from neotype nt where nt.id = neoobject.neoobjecttype_id and nt.neotype = 'Fworkflow.adapter.casvig.CasvigFinalDate')

--CasvigMobilePararAtividade
drop table CasvigMobilePararAtividade

insert into customactivityadapter (clazz, neoid)
select 'com.neomind.fusion.workflow.adapter.casvig.CasvigMobilePararAtividade', no.neoid from neoobject no 
inner join neotype nt on no.neoobjecttype_id = nt.id
where nt.neotype = 'Fworkflow.adapter.casvig.CasvigMobilePararAtividade' and no.neoid in (
select neoid from Activity)

update neoobject set neoobject.neoobjecttype_id = (select id from NeoType WHERE neoType = 'Fworkflow.adapter.CustomActivityAdapter')
where neoobject.neoobjecttype_id in (Select nt.id from neotype nt where nt.id = neoobject.neoobjecttype_id and nt.neotype = 'Fworkflow.adapter.casvig.CasvigMobilePararAtividade')

 -->
</body>
</html>