<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="javax.persistence.Query"%>
<%@page import="com.neomind.fusion.webservice.autostore.OrsegoupsAutoStoreWS"%>
<%@page import="java.io.File"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>    
<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="com.neomind.fusion.persist.QLFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.common.NeoRunnable"%>   
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="com.neomind.util.NeoUtils"%>

<%@page import="com.neomind.fusion.custom.orsegups.contract.ContractSIDClient"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESTE Adapter FGC</title>
</head>
<body>
<%
	boolean all = false;
	if(request.getParameter("all") != null)
		all = true;
	
	String neoidArquivo = "";
	if(request.getParameter("neoidArquivo") != null)
		neoidArquivo = request.getParameter("neoidArquivo");

	String query = "";
	
	if(all)
	{
		query = "select b.neoId, b.arquivo.neoId, b.mesano, b.batismo, b.colaborador.neoId from D_GEDBatismoOrsegoups a "+
				"JOIN a.documentos b "+
				"where a.wfprocess.saved = 1 "+
				"and a.wfprocess.processState = 1 "+
				"and b.arquivo not in (select c.arquivo from D_arquivoGED c)";
	}
	
	if(!neoidArquivo.equals(""))
	{
		query = "select b.neoId, b.arquivo.neoId, b.mesano, b.batismo, b.colaborador.neoId from D_GEDBatismoOrsegoups a "+
				"JOIN a.documentos b "+
				"where a.wfprocess.saved = 1 "+
				"and a.wfprocess.processState = 1 "+
				"and b.arquivo.neoId = " + neoidArquivo;
	}
	
	if("".equals(query))
	{
		out.println("� necess�rio ao menos informar o par�metro all ou neoidArquivo.");
		return;
	}
	
	Query q = PersistEngine.getEntityManager().createQuery(query);
	List result = q.getResultList();

	OrsegoupsAutoStoreWS ws = new OrsegoupsAutoStoreWS();
	
	for (Object o : result)
	{
		Object[] res = (Object[]) o;
		
		String idArquivoDigitalizado = NeoUtils.safeOutputString(res[0]);
		String idArquivo = NeoUtils.safeOutputString(res[1]);
		String mesano = NeoUtils.safeOutputString(res[2]);
		String batismo = NeoUtils.safeOutputString(res[3]);
		String idColaborador = NeoUtils.safeOutputString(res[4]);
		
		NeoFile arquivo = PersistEngine.getObject(NeoFile.class, new QLEqualsFilter("neoId", NeoUtils.safeLong(idArquivo)));
		
		NeoObject colaborador = PersistEngine.getNeoObject(NeoUtils.safeLong(idColaborador));
		
		try
		{
			ws.insereArquivoColaborador(arquivo, mesano, batismo, colaborador);
		}catch(Exception e)
		{
			out.println("Erro ao posicionar o arquivo " + idArquivo + "<br>");
		}

	} 


%>

</body>
</html>