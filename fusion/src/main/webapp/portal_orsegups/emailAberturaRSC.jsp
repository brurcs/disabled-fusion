<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/mail.tld" prefix="mail"%>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body>
 <table width="640" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
<tbody><tr>
<td valign="top" align="left" style="font-family:Arial;font-size:11px;font-weight:normal;text-align:left;">
</td>
</tr>
</tbody></table>
<mail:title title="Registro de Solicita��o, RSC: ${requestScope.tarefa}"></mail:title>
 <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
          <tbody><tr>
            <td valign="center" align="right" colspan="11"><img style="display:block;" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="1" height="9"></td>
          </tr>
          <tr>
            <td align="center" valign="right" >
              <a href="http://www.orsegups.com.br" target="_blank">
              <img src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg"  border="0" alt="Orsegups Participa��es S.A."></a>
            </td>
            <td align="center" valign="right" >
              <img style="DISPLAY: inline" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="20" height="1">
            </td>
            
            <td valign="top" align="left" style="font-family:Arial; font-size:11px;font-weight:normal;">
			 <a href="http://www.orsegups.com.br/area-do-cliente/login?expirado" target="_blank" style="color:#0078d2; text-decoration:none;">
			Portal do Cliente</a>
			<img style="DISPLAY: inline" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="20" height="1">
			<a href="http://www.orsegups.com.br/area-do-cliente/login?expirado" target="_blank" style="color:#0078d2; text-decoration:none;">
			Relat�rio de Eventos</a>
			<img style="DISPLAY: inline" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="20" height="1">
			<a href="http://www.orsegups.com.br/orcamento.html" target="_blank" style="color:#0078d2; text-decoration:none;">
			Atualiza��o Cadastral</a> 
			<img style="DISPLAY: inline" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="20" height="1">
			</td>       
          </tr>
          <tr>
            <td valign="top" align="left" colspan="11"><img style="display:block;" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="1" height="9"></td>
         </tr>
     </tbody></table>
<table align="center" width="640" height="300" border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td align="center" colspan="3" width="640" height="78" style="color:#363F46; FONT-WEIGHT: bold; FONT-SIZE: 26px;FONT-FAMILY: Helvetica, Arial, sans-serif;">
		REGISTRO DE SOLICITA��O
		</td>
	</tr>
	<tr>
		<td align="left" style="FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;">
		<font style="FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;">Prezado(a) ${requestScope.nomePes},</font><br><br>
		</td>
	</tr>
	<tr>
		<td style="FONT-WEIGHT: regular; FONT-SIZE: 18px; FONT-FAMILY: Helvetica, Arial, sans-serif;">
		Obrigado(a) pelo seu contato. Informamos que seu protocolo de atendimento � <b>RSC: ${requestScope.tarefa}</b>. Para acompanhar o status do seu protocolo entre em contato com nosso canal de atendimento mais pr�ximo. <a href="http://intranet.orsegups.com.br/fusion/portal_orsegups/mapa.jsp" style="color:#0078d2; text-decoration:none;" target="_blank">Veja nossos escrit�rios regionais</a>.
	<br>
	<br>
	</td><td width="10"><img width="10" height="15" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt=""></td>
	</tr>

	</tbody>
</table>
<!--- main hero image with salutation ends--->
<!--- main body begins--->

<table width="640" cellspacing="0" cellpadding="0" align="center">
<tbody><tr>
                          <td valign="top" align="right">
                            <a href="http://www.facebook.com/orsegups" target="_blank">
                            <img src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logo_facebook.jpg" width="22" height="18" border="0" alt="Facebook"></a>
                           <a href="http://www.youtube.com/user/orsegupssc" target="_blank">
                            <img src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logo_youtube.jpg" width="22" height="18" border="0" alt="YouTube"></a>
                            <a href="http://www.twitter.com/orsegups" target="_blank">
                            <img src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logo_twitter.jpg" width="22" height="18" border="0" alt="Twitter"></a>
                          </td>
                        </tr>
</tbody>
</table>
<br>
<table width="640" cellspacing="0" cellpadding="0" align="center">
<tbody><tr>
<td width="5"><img width="9" height="22" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt=""></td>
<td valign="top" align="left" style="font-family:Arial; font-size:11px;font-weight:normal;">
 <a href="http://www.orsegups.com.br/area-do-cliente/login?expirado" target="_blank" style="color:#0078d2; text-decoration:none;">
Portal do Cliente</a>
<img style="DISPLAY: inline" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="20" height="1">
<a href="http://www.orsegups.com.br/area-do-cliente/login?expirado" target="_blank" style="color:#0078d2; text-decoration:none;">
Relat�rio de Eventos</a>
<img style="DISPLAY: inline" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="20" height="1">
<a href="http://www.orsegups.com.br/orcamento.html" target="_blank" style="color:#0078d2; text-decoration:none;">
Atualiza��o Cadastral</a> 
<img style="DISPLAY: inline" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="" width="20" height="1">
</td>
</tr>
<tr>
<td valign="top" align="left" colspan="2">
<img style="display:inline;" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/line.jpg" height="6" border="0">
</td>
</tr>
</tbody>
</table>
<table width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="font-family:Arial">
<tbody><tr>
<td width="5">
<img width="9" height="22" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="">
</td>
<td valign="top" style="FONT-WEIGHT: normal; FONT-SIZE: 10px; COLOR: #999999; font-family:Arial;valign:top;">
Este e-mail foi enviado a <span style="color:#0078d2; text-decoration:none;">${requestScope.email}</span> pois voc� est� cadastrado em nosso sistema operacional.<br><br>

Este e-mail foi enviado em nome da Orsegups Participa��es. Teremos satisfa��o em ajud�-lo com quaisquer d�vidas ou preocupa��es. Para qualquer esclarecimento desejado, visite <a href="http://www.orsegups.com.br/orcamento.html" style="color:#0078d2; text-decoration:none;" target="_blank">www.orsegups.com.br</a>, no link contato ou envie e-mail para <a href="mailto:sac@orsegups.com.br" style="color:#0078d2; text-decoration:none;" target="_blank">sac@orsegups.com.br</a>.<br><br>

� 2013 Orsegups Participa��es S.A. Todos os direitos reservados.<br>

<br>
Este email e todas as informa��es ou arquivos contidos nele s�o exclusivamente para uso confidencial do destinat�rio. Esta mensagem cont�m informa��es confidenciais e pertencentes � Orsegups (como dados corporativos, dos clientes e dos funcion�rios da Orsegups) que n�o podem ser lidas, pesquisadas, distribu�das ou utilizadas de outra forma por qualquer outro indiv�duo que n�o seja o destinat�rio pretendido. Caso tenha recebido este email por engano, por favor, notifique o remetente 
e exclua esta mensagem e seus anexos imediatamente.
</td>
<td width="5">
<img width="9" height="22" src="http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif" alt="">
</td>
</tr>
</tbody>
</table>
<img height="1" width="1" src="52"><img height="1" width="1" src="52">
</body></html>