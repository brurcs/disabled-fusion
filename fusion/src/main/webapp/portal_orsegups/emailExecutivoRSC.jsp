<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/mail.tld" prefix="mail"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
<body>
<mail:title title="Solicitação de Orçamento - RSC: ${requestScope.tarefa}"></mail:title>
<table align="center" border="0" cellpadding="10" cellspacing="0">
	<tbody>
	<tr>
		<td colspan="3" align="center" style="FONT-WEIGHT: bold; FONT-SIZE: 18px;FONT-FAMILY: Helvetica, Arial, sans-serif;">
			SOLICITAÇÃO DE ORÇAMENTO
			<br>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="left" style="FONT-WEIGHT: regular; FONT-SIZE: 12px; FONT-FAMILY: Helvetica, Arial, sans-serif;">
			${requestScope.mensagem}
		</td>
	</tr>
	</tbody>
</table>
<!--- main hero image with salutation ends--->
<!--- main body begins--->
<br>
</body>
</html>