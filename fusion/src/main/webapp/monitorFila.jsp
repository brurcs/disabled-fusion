
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.custom.orsegups.jms.*"%>
<html>
<head>
 <script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
 <script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>
 <script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.min.js"></script>
</head>
<body>
<h2>Monitor de consumo de fila!</h2>

<form>
  Quantidade consumida Callcenter:  <input type="text" name="quantidade" id="quantidade"><input type="button" value="Enviar Mensagem" onclick="sendMessage('callCenter')"><br>
  Quantidade consumida map Alerta AIT Parado:  <input type="text" name="quantidadeMap" id="quantidadeMap"><input type="button" value="Enviar Mensagem" onclick="sendMessage('mapAlertAitTopic')"><br>
  Quantidade consumida alertEvent (eventos de x2, x5 e x406):  <input type="text" name="quantidadeAlertEvent" id="quantidadeAlertEvent"><input type="button" value="Enviar Mensagem" onclick="sendMessage('alertEventTopic')"><br>
  Quantidade consumida alertEvent (eventos de video):  <input type="text" name="quantidadeAlertVideoEvent" id="quantidadeAlertVideoEvent"><input type="button" value="Enviar Mensagem" onclick="sendMessage('alertEventVideoTopic')"><br>
  
  </form>


<script type="text/javascript">

function callSync(url, headers) {
    var sXmlHttp;

    /** // Inicializa os objetos ajax **/
    sXmlHttp = new XMLHttpRequest();


    /** Evita cache de requisicao http **/
    url = safeURL(url);

    sXmlHttp.open("get", url, false);
    if (headers) {
        try {
            for (var header in headers) {
                sXmlHttp.setRequestHeader(header, headers[header]);
            }
        }
        catch (e) {
        }
    }
    sXmlHttp.send(null);
    return sXmlHttp.responseText;
}

function safeURL(u) {
    if (u.indexOf("?") > -1)
        u = u + "&"
    else
        u = u + "?";
    return (u + "fusionX=" + Math.floor(Math.random() * 100000) + new Date().getTime());
}



$(document).ready(function() {
    	initialize();
    })

	
function sendMessage(message) {
		callSync('<%= PortalUtil.getBaseURL() %>testejms?type=' + message);
	}
	
function initialize() {
    var amq = org.activemq.Amq;
    amq.init({
        uri: '<%=PortalUtil.getBaseURL()%>amq',
        logging: true,
        timeout: 20,
        clientId: (new Date()).getTime().toString()
    });
	
    //topic utilizado para o CallCenter
    amq.addListener('callCenterListener', "topic://messageTopic", myHandler.rcvMessage, {selector: "identifier='testeNeo'"});
    //topic utilizado para alerta do AIT parado
    amq.addListener('mapAlertListener', "topic://mapAlertAitTopic", myHandlerMap.rcvMessage, {selector: "identifier='testeNeo'"});
    //topic utilizado para alertEvent (eventos de x2, x5 e x406)
    amq.addListener('AlertEventListener', "topic://alertEventTopic", myHandlerAlert.rcvMessage, {selector: "identifier='testeNeo'"});
	//topic utilizado para os eventos de video
    amq.addListener('AlertEventVideoListener', "topic://alertEventVideoTopic", myHandlerVideo.rcvMessage, {selector: "identifier='testeNeo'"});


}

var myHandler = {
        rcvMessage: function(message) 
        {
			var teste = Number($('#quantidade').val());
			teste = teste + 1;
			$('#quantidade').val(teste);
            //alert(message.data);
        }
}        

var myHandlerMap = {
		rcvMessage: function(message) 
		{
			var teste = Number($('#quantidadeMap').val());
			teste = teste + 1;
			$('#quantidadeMap').val(teste);
		    //alert(message.data);
		}


}
var myHandlerAlert = {
		rcvMessage: function(message) 
		{
			var teste = Number($('#quantidadeAlertEvent').val());
			teste = teste + 1;
			$('#quantidadeAlertEvent').val(teste);
		    //alert(message.data);
		}
}

var myHandlerVideo = {
		rcvMessage: function(message) 
		{
			var teste = Number($('#quantidadeAlertVideoEvent').val());
			teste = teste + 1;
			$('#quantidadeAlertVideoEvent').val(teste);
		    //alert(message.data);
		}
}


</script>

</body>
</html>
<%
NeoJMSPublisher pub = NeoJMSPublisher.getInstance();

%>