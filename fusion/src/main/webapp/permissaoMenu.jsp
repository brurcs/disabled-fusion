<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page import="com.neomind.fusion.entity.SecurityPermission"%>
<%@page import="com.neomind.fusion.entity.SecurityController"%>
<%@page import="com.neomind.fusion.menu.NeoMenu"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>

<%@page import="javax.persistence.Query"%>
<%@page import="com.neomind.fusion.webservice.autostore.OrsegoupsAutoStoreWS"%>
<%@page import="java.io.File"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>    
<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="com.neomind.fusion.persist.QLFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.common.NeoRunnable"%>   
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="com.neomind.util.NeoUtils"%>

<%@page import="com.neomind.fusion.custom.orsegups.contract.ContractSIDClient"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESTE Adapter FGC</title>
</head>
<body>
<%
	

	String query = "select a.code,  c.securityEntity_neoId from neomenu a "+
				" INNER JOIN SecurityController b ON a.typeSecurityController_neoId = b.neoId"+
				" INNER JOIN SecurityPermission c ON b.neoId = c.controller_neoId "+
				" WHERE b.defaultPermission = 2";
	

	EntityManager entity = PersistEngine.getEntityManager("FUSION_LUCAS");
	
	if(entity != null)
	{
		Query q = entity.createNativeQuery(query);
		List result = q.getResultList();

		for (Object o : result)
		{
			Object[] res = (Object[]) o;
			
			String codeMenu = NeoUtils.safeOutputString(res[0]);
			String neoidSecurityEntity = NeoUtils.safeOutputString(res[1]);
			
			NeoMenu menu = PersistEngine.getObject(NeoMenu.class, new QLEqualsFilter("code", NeoUtils.safeLong(codeMenu)));
			
			if(menu != null)
			{
				SecurityController seguranca = menu.getTypeSecurityController();
				SecurityEntity entidade = PersistEngine.getObject(SecurityEntity.class, new QLEqualsFilter("neoId", NeoUtils.safeLong(neoidSecurityEntity)));
				if(seguranca != null && entidade != null)
				{
					SecurityPermission permissao = new SecurityPermission();
					permissao.setRead(true);
					permissao.setSecurityEntity(entidade);
					PersistEngine.persist(permissao);
					
					seguranca.addPermission(permissao);
					
					out.println("Criado um registro para o code "+codeMenu+", neoid criado "+permissao.getNeoId()+"<br>");
				}else
				{
					if(seguranca == null)
					{
						out.println("[0] - Erro para o code "+codeMenu+" <br>");
					}else
					{
						out.println("[1] - Erro para o code "+codeMenu+" <br>");
					}
					
				}
			}else
			{
				out.println("[2] - Erro para o code "+codeMenu+" <br>");
			}
			
			out.flush();
		} 
	}
	


%>

</body>
</html>