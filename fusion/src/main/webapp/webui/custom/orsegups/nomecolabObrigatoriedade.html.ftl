<script>

$(document).ready(function(){   
	$('#${fieldID}').keypress(function(){ 
		//debugger;
		var nomeCompleto = document.getElementById("var_nomCompletoFC__").value;
		var tamanho = nomeCompleto.length;
		if(tamanho > 40){
			var nomeAbreviado = document.getElementById("var_colaborador__")
			nomeAbreviado.setAttribute("mandatory", "true");
			var url = "https://intranet.orsegups.com.br/fusion/imagens/icones_final/required_red_16x16-trans.png";
			var divChild = $("#div_colaborador__").find(".field_label_separator img");
			divChild.attr("src", url);
			//document.getElementById("var_colaborador__").value = reduzirNome(nomeCompleto);
		}else{
			var nomeAbreviado = document.getElementById("var_colaborador__")
			nomeAbreviado.setAttribute("mandatory", "false");
			var url = "https://intranet.orsegups.com.br/fusion/imagens/core/transparent.gif";
			var divChild = $("#div_colaborador__").find(".field_label_separator img");
			divChild.attr("src", url);
		}
		
	})
	$('#${fieldID}').change(function(){ 
		//debugger;
		var nomeCompleto = document.getElementById("var_nomCompletoFC__").value;
		var tamanho = nomeCompleto.length;
		if(tamanho > 40){
			var nomeAbreviado = document.getElementById("var_colaborador__")
			nomeAbreviado.setAttribute("mandatory", "true");
			var url = "https://intranet.orsegups.com.br/fusion/imagens/icones_final/required_red_16x16-trans.png";
			var divChild = $("#div_colaborador__").find(".field_label_separator img");
			divChild.attr("src", url);
			document.getElementById("var_colaborador__").value = reduzirNome(nomeCompleto);
			document.getElementById("var_nomeCracha__").value = reduzirNomeCracha(nomeCompleto);
		}else{
			var nomeAbreviado = document.getElementById("var_colaborador__")
			nomeAbreviado.setAttribute("mandatory", "false");
			var url = "https://intranet.orsegups.com.br/fusion/imagens/core/transparent.gif";
			var divChild = $("#div_colaborador__").find(".field_label_separator img");
			divChild.attr("src", url);
			document.getElementById("var_colaborador__").value = document.getElementById("var_nomCompletoFC__").value;
			document.getElementById("var_nomeCracha__").value = reduzirNomeCracha(nomeCompleto);
		}
	})
});

	function reduzirNome(nomeInteiro) { 
	   	
        var nomePedacos = nomeInteiro.split(' ');
        
        var saida = '';
        for (k=0;k<nomePedacos.length;k++) {
        	if(k==0 || k==(nomePedacos.length-1)) {
        		if (k==0) {
        			saida = saida + nomePedacos[k];
        		} else {
        			saida = saida + ' ' + nomePedacos[k];
        		}       			
        	} else {
        		if (nomePedacos[k].charAt(0) != 'd' && nomePedacos[k].charAt(0) != 'D')
        			saida = saida + ' ' + nomePedacos[k].charAt(0);
        	}        		
        }
        return saida;
    }
    
    function reduzirNomeCracha(nomeInteiro) { 
	   	
        var nomePedacos = nomeInteiro.split(' ');
        var saida = '';        
        saida = nomePedacos[0] + ' ' + nomePedacos[nomePedacos.length-1];
        return saida;
    }

	

</script>