<script>

$(document).ready(function() {
	   
	var button = document.getElementsByName("action.send")[0];
	var oldOnClick = button.onclick;
	button.onclick = function(event){
		//debugger; //ativar quando quiser debuggar
		var acao = document.getElementById("id_validacao__acao__").value;
		
		//controle para só entrar nesse validador quando a ação for diferente de "Enviar para Sede" e "Efetuar a Integração"
		if(acao !== null && acao !== "849337900" && acao !== "849339923" && acao !== "849339924" && acao !== "849339926"){
			var ellist = window.ellist_validacao__listaCamposAprovacao__;
		 	var rowCount = ellist.getRowCount();
		    for(var i = 0; i < rowCount; i++) {
		        var row = $(ellist._getRow(i));
		        var text = row.find("td:nth-child(2)").text().replace(/\n/g, "").replace(/ /g, "");
		        var el = document.getElementById("var_" + text + "__");
		        if(el !== null)
		        	el.setAttribute('mandatory', "false");
		    }
	    }
   	return oldOnClick.call(button, event);
	};

});
</script>