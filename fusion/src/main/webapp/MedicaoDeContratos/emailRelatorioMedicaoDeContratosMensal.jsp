<%@ taglib uri="/WEB-INF/mail.tld" prefix="mail"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@page import="com.neomind.fusion.workflow.*"%>
<%@page import="java.util.*"%>
<%@page import="com.neomind.fusion.common.*" %>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoUtils"%>


<%
	GregorianCalendar data = new GregorianCalendar();
	data.add(GregorianCalendar.DATE, -1);
	String dataAtual = NeoUtils.safeDateFormat(data, "dd/MM/yyyy");
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<portal:baseURL url="getURLPath" />
<base href="<%= PortalUtil.getBaseURL() %>" />
<link href="<%=getURLPath%>/css/portal-decorator.css" rel="stylesheet"	type="text/css" />
<style TYPE="text/css">
          p,pre,body,table
          {
          font-size:10.0pt;
          font-family:'Arial';
          }
</style>
</head>
<body bgcolor="#FFFFFF">
<table id="Table_01" width="550" border="0" cellpadding="0"
	cellspacing="0">
	<tr>
	<td class="corpo_mail">
			<p class="mail_tit"><mail:title title='Relat�rio de Medi��o de contratos - Asseio' /></p>
	</td>
	</tr>	
	<tr>
		<td class="corpo_mail" align="left">
<p> Segue anexo o relat�rio de Medi��o de contratros referente ao m�s de <%=NeoUtils.safeDateFormat(dataAtual,"MM/yyyy") %>.</p>
<br>

Orsegups Participa��es S/A<br/>
<a href="http://www.orsegups.com.br">http://www.orsegups.com.br</a> 
			
		</td>
	</tr>
	
</table>
<!-- End ImageReady Slices -->
</body>
</html>