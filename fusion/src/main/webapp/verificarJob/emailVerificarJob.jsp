<%@page import="com.neomind.util.NeoDateUtils"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@ taglib uri="/WEB-INF/mail.tld" prefix="mail"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@page import="com.neomind.fusion.workflow.*"%>
<%@page import="java.util.*"%>
<%@page import="com.neomind.fusion.common.*" %>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoUtils"%>


<%
GregorianCalendar date = new GregorianCalendar();
date.set(GregorianCalendar.HOUR, 0);
date.set(GregorianCalendar.HOUR_OF_DAY, 0);
date.set(GregorianCalendar.MINUTE, 0);
date.set(GregorianCalendar.SECOND, 0);
date.set(GregorianCalendar.MILLISECOND, 0);
QLOpFilter dataFilter = new QLOpFilter("startDate", "=", (GregorianCalendar) date);
QLGroupFilter groupFilter = new QLGroupFilter("AND");
groupFilter.addFilter(dataFilter);
List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("tarefasJob"), groupFilter);
String nomeJob = "";
GregorianCalendar ultExecucao = null;
GregorianCalendar proExecucao = null;
String periodicidade = "";
String erro = "";
String tarefa = "";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<portal:baseURL url="getURLPath" />
<base href="<%= PortalUtil.getBaseURL() %>" />
<link href="<%=getURLPath%>/css/portal-decorator.css" rel="stylesheet"	type="text/css" />
<style TYPE="text/css">
          p,pre,body,table
          {
          font-size:10.0pt;
          font-family:'Arial';
          }
</style>
</head>
<body bgcolor="#FFFFFF">
<table id="Table_01" width="550" border="0" cellpadding="0"
	cellspacing="0">
	<tr>
	<td class="corpo_mail">
			<p class="mail_tit"><mail:title title='Rela��o das tarefas referente aos Jobs com problemas' /></p>
	</td>
	</tr>	
	<tr>
		<td class="corpo_mail" align="left">
<p> Segue tarefas abertas dos Job's no dia de hoje <%=NeoUtils.safeDateFormat(date, "dd/MM/yyyy") %>:</p>
<br>
	<TABLE BORDER=1 style="table-layout: fixed; width: 1200px; overflow: hidden;"> <! Inicia a tabela e coloca uma borda de espessura igual a 1>
		<TR> 
			<TD WIDTH=15%>NOME DO JOB </TD> <! Aqui foi criada uma c�lula>
			<TD WIDTH=7%>ULTIMA EXECU��O</TD>
			<TD WIDTH=7%>PROXIMA EXECU��O </TD>
			<TD WIDTH=7%>PERIODICIDADE</TD>
			<TD WIDTH=12%>ERRO</TD>
			<TD WIDTH=4%>Tarefa</TD>
		</TR>
	<% for (NeoObject neoObject : objs) {
		EntityWrapper wr = new EntityWrapper(neoObject);
		nomeJob = (String) wr.findField("nomeJob").getValue();
		ultExecucao =  (GregorianCalendar) wr.findField("ultExecucao").getValue();
		proExecucao =  (GregorianCalendar) wr.findField("proExecucao").getValue();
		periodicidade = (String) wr.findField("periodicidade").getValue();
		erro = (String) wr.findField("erro").getValue();
		tarefa = (String) wr.findField("tarefa").getValue();
		
	%>
		<TR>
			<TD><%=nomeJob%></TD>
			<TD><%=NeoUtils.safeDateFormat(ultExecucao, "dd/MM/yyyy")%></TD>
			<TD><%=NeoUtils.safeDateFormat(proExecucao, "dd/MM/yyyy")%></TD>
			<TD><%=periodicidade%></TD>
			<TD><%=erro%></TD>
			<TD><%=tarefa%></TD>
		</TR>
	<%} %>
	</TABLE>

Orsegups Participa��es S/A<br/>
<a href="http://www.orsegups.com.br">http://www.orsegups.com.br</a> 
		</td>
	</tr>
	
</table>
<!-- End ImageReady Slices -->
</body>
</html>