<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
	<div id='tree_container' class='tree_container' style='display: block;'>
		<ul>
		</ul>
	</div>
	<div id='div-baptism-popup-wrapper' class='ui-neo-baptism-popup-wrapper'>
		<div class="ui-neo-baptism-input-wrapper">
			<div style='text-align: center;'>
				<input  class='input_text' placeholder='Insira o Tipo de Documento' name='id_txt_documentType__' id='id_txt_documentType__' 
				mandatory='true' onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)" style='width: 160px; border: 1px solid #96a7ba; border-right: none; height: 19px; min-height: 19px;'/>
		        <img src='<%=PortalUtil.getBaseURL()%>scan/imagens/icones/autocomplete_search2.png' class='ui-neo-baptism-popup-img'>
		        <input id='id_documentType__' type='hidden' onchange='TreeActions.Baptism.baptismFiles(this.value);'/>
	        </div>
	    </div>
	    <div class="ui-neo-baptism-popup-content" id="area_TreeView">
	    </div>
	</div>
</body>
</html>

<script>

$( document ).ready(function() {
	var documentos = "";
	var urlPort = '<%=PortalUtil.getBaseURL()%>portal/render/JSP?jsp=/custom/renderizarPecas.jsp&documentos='+documentos;
	
	loadContent('area_TreeView', urlPort);
});

</script>