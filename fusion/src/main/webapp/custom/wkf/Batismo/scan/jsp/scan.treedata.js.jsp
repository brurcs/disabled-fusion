<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
A��es realizadas na estrutura HTML da treeview.
Objetos da Tree.
--%>
var TreeData = {
	 container: 'tree_container',
	 
	 init: function()
	 {

	 },
	 
	 Tree: function(id, nodes)
	 {
	 	this.id = id;
	 	this.nodes = nodes;
	 },
	 
	 TreeNode: function(id, name, type, url, page_nr, original_page_nr)
	 {
	 	this.id = id;
	 	this.name = name;
	 	this.type = type;
	 	this.url = url;
	 	this.page_nr = page_nr;
	 	this.original_page_nr = original_page_nr;
	 },
	 
	 buildJSONTree: function()
	 {
	 	var ref = $.jstree._reference('#' + TreeData.container);
	 	try
	 	{
	 		var data = ref.get_json(-1, ["id", "name", "type", "typeName", "url", "page_nr", "original_page_nr"]);
			var json = JSON.stringify(data);
			return json;
		}catch(e)
		{
			return "{}";
		}
	 },
	 
	 buildTree: function(files)
	 {
	 	if (files)
	 	{
	 		var nodes = new Array();
	 		
	 		var spl = files.split(';');
	 		for (var i=0;i<spl.length;i++)
	 		{
	 			if (spl[i] && spl[i].length>0)
	 			{
	 				var node = new TreeData.TreeNode('tree_node_'+Scan.Pages.total, '<%=I18nUtils.getString("page")%> '+parseInt(Scan.Pages.total+1), 'page', spl[i], Scan.Pages.total, Scan.Pages.total);
	 				nodes.push(node);
	 				Scan.Pages.total = Scan.Pages.total + 1;
	 			}
	 		}
	 		
	 		var tree = new TreeData.Tree('tree_', nodes);
	 		return tree;
	 	}
	 	
	 	return null;
	 },
	 
	 // Adiciona um n� pai na tree.
	 addParentNode: function(id, text, targetUl)
	 {
	 	var liPai = $('<li/>', 
	 	{
	 		 rel: 'root',
	 		 id: id,
	 		 typeName: text,
	 		 type: 'document'
	 	});
						
		var ulPai = $('<ul/>', {});
								
		var aPai = $('<a/>', 
		{
			text: text
		});
								
		liPai.append(aPai);
		liPai.append(ulPai);
		
		if (targetUl)
			targetUl.append(liPai);
			
		return liPai;
	 },
	 
	 // Verifica se existe um n� pai pelo id.
	 hasParentNode: function(id)
	 {
	 	var has = false;
	 	
	 	var lis = this.getContainerUL().children('li');
	 	if (lis.length)
	 	{
	 			lis.each(function() 
	 			{
	 				var idAttr = $(this).attr('id');
	 				if (idAttr && idAttr == id)
	 				{
	 					has = true;
	 					return;
	 				}
	 			});
	 	}
	 		
	 		return has;
	 },
	 
	 // Adiciona n�s filhos em um pai.
	 addChildNodes: function(parentId, nodes)
	 {
	 		var parentLi = $('#'+parentId);
	 		if (parentLi.length)
	 		{
	 			var ulParent = parentLi.children('ul');
	 			if (!ulParent.length)
	 			{
	 				ulParent = $('<ul/>', {});
	 				parentLi.append(ulParent);
	 			}
	 			if (ulParent.length)
	 			{
	 				for (var i=0;i<nodes.length;i++)
	 				{
	 					var li = $('<li/>', {
	 					url: nodes[i].url,
	 					id: nodes[i].id,
	 					type: nodes[i].type,
	 					page_nr: nodes[i].page_nr,
	 					original_page_nr: nodes[i].original_page_nr
						});
			
						var a = $('<a/>', {
						text: nodes[i].name
						});
			
						li.append(a);
						ulParent.append(li);
	 				}
	 				Toolbar.updateTotalPages();
	 				Toolbar.checkShowNavBar();
	 			}
	 		}
	 },
	 
	 // Transfere os n�s selecionados para outro pai.
	 transferNodes: function(parentId, checkedNodes)
	 {
	 	var parentLi = $('#'+parentId);
	 	var oldParents = '';
	 	
	 	var topParent;
	 	$(checkedNodes).each(function() {
	 		topParent = $(this).closest('li[rel=root]');
	 		oldParents = oldParents + topParent.attr('id')+';';
	 	});
	 	
	 	if (parentLi.length)
	 	{
	 		var ulParent = parentLi.children('ul');
	 		
	 		if (!ulParent.length)
	 		{
	 			ulParent = $('<ul/>', {});
	 			parentLi.append(ulParent);
	 		}
	 			
	 		for (var i=0;i<checkedNodes.length;i++)
	 		{
	 			var node = checkedNodes[i];
	 			var nodeId = node.id;
	 			var rel = $(node).attr('rel');
	 			
	 			if (rel && rel == 'root')
	 				continue;
	 			
	 			var toTransfer = $('li[id='+nodeId+']');
	 			if (toTransfer)
	 				toTransfer.appendTo(ulParent);
	 		}
	 		
	 		// Se sobrou algum pai sem filhos, deleta.
	 		var oldParIds = oldParents.split(';');
	 		for (var i=0;i<oldParIds.length;i++)
	 		{
	 			var old = oldParIds[i];
	 			if (old != undefined)
	 			{
	 				if (TreeData.countChildren(old) == 0)
	 					$('#'+old).remove();
	 			}
	 		}
	 	}
	 },
	 
	 // Obt�m um n� pai pelo Id.
	 getParentNode: function(parentId)
	 {
	 	var parent = this.getContainerUL().children('li[id='+parentId+']');
	 	return parent;
	 },
	 
	 // Nr. de n�s do tipo page (p�gina).
	 getNodeCount: function()
	 {
	 	var container = TreeData.getContainer();
	 	if (container.length)
	 	{
	 		var nodes = container.find('li[type=page]');
	 		return nodes.length;
	 	}
	 },
	 
	 // Atualiza a numera��o das p�ginas
	 updatePageNumbers: function()
	 {
	 	var nodes = Tree.getContainer().find('li[type=page]');
	 	if (nodes && nodes.length)
	 	{
	 		for (var i=0;i<nodes.length;i++)
	 		{
	 			var node = nodes[i];
	 			if (node)
	 			{
	 				$(node).attr('id', 'tree_node_'+i).attr('page_nr', i);
	 				Tree.getContainer().jstree('rename_node', node , '<%=I18nUtils.getString("page")%> '+ parseInt(i+1) );
	 			}
	 		}
	 	}
	 },
	 
	 canMove: function(checkedNodes)
	 {
	 	var documents = checkedNodes.filter('li[type=document]');
	 	var documents2 = Tree.getContainer().find('.jstree-undetermined');
	 	
	 	if ((documents && documents.length>1) || (documents2 && documents2.length>1)
	 	|| (documents && documents2 && (parseInt(documents.length)+parseInt(documents2.length)) > 1))
	 	{
	 		return false;
	 	}
	 	
	 	return true;
	 },
	 
	 // Move n�s para cima.
	 moveNodeUp: function(checkedNodes)
	 {	
	 	if (TreeData.canMove(checkedNodes))
	 	{
	 		var prev = $(checkedNodes[0]).prev();
	 		if (prev.length)
	 		{
	 			if (prev.is('li'))
	 			{
	 				$(checkedNodes).insertBefore(prev);
	 				TreeData.updatePageNumbers();
	 			}
	 		}
	 		else
	 		{
	 			// Insere no pr�ximo n� raiz, caso exista.
	 			
	 			var parentLi = $(checkedNodes).closest('[rel=root]');
	 			if (parentLi.length)
	 			{
	 				var prev = parentLi.prev('[rel=root]');
	 				if (prev.length)
	 				{
	 					var ul = prev.children('ul');
	 		
	 					if (!ul.length)
	 					{
	 						ul = $('<ul/>', {});
	 						prev.append(ul);
	 					}
	 					$(checkedNodes).appendTo(ul);
	 					TreeData.updatePageNumbers();
	 					
	 				}
	 			}
	 		}
	 	} else {
	 		alert('<%=I18nUtils.getString("invalidaction")%>');
	 	}
	 },
	 
	 // Move n�s para baixo.
	 moveNodeDown: function(checkedNodes)
	 {
	 	if (TreeData.canMove(checkedNodes))
	 	{
	 		var next = $(checkedNodes[checkedNodes.length-1]).next();
	 		if (next.length)
	 		{
	 			if (next.is('li'))
	 			{
	 				$(checkedNodes).insertAfter(next);
	 				TreeData.updatePageNumbers();
	 			}
	 		}
	 		else
	 		{
	 			// Insere no pr�ximo n� raiz, caso exista.
	 			var parentLi = $(checkedNodes).closest('[rel=root]');
	 			if (parentLi.length)
	 			{
	 				var next = parentLi.next('[rel=root]');
	 				if (next.length)
	 				{
	 					var ul = next.children('ul');
	 		
	 					if (!ul.length)
	 					{
	 						ul = $('<ul/>', {});
	 						next.append(ul);
	 					}
	 					$(checkedNodes).appendTo(ul);
	 					TreeData.updatePageNumbers();
	 				}
	 			}
	 			
	 		}
	 	}
	 },
	 
	 deleteNode: function(checked)
	 {
		 // Se existir algum n� selecionado na Tree, e estiver na lista de exclu�dos, atualiza a sele��o.
	 	var selected = $.jstree._reference('#' + TreeData.container).get_selected();
	 	var selectNext = false;
	 	var selectedId;
	 	var pageNr;
	 	
	 	var checkedId;
	 	if (selected.length)
	 	{
	 		selectedId = selected.attr('id');
	 		pageNr = parseInt(selected.attr('page_nr'));
	 	
	 		checked.each(function() {
	 			checkedId = $(this).attr('id');
	 			if (checkedId == selectedId)
	 			{
	 				selectNext = true;
	 				return false;
	 			}
	 		});
	 	}
	 		
	 	CanvasManager.remove(checked);
	 	$(checked).remove();
	 	TreeData.updatePageNumbers();
	 	Toolbar.updateTotalPages();
	 	Toolbar.checkShowNavBar();
	 	
	 	if (selectNext == true)
	 	{
	 		
	 		TreeData.getContainer().jstree("select_node", "#tree_node_"+pageNr); 
	 	}
	 },
	 
	 selectPage: function(page_nr)
	 {
	 	this.getContainer().jstree("deselect_all");
	 	this.getContainer().jstree("select_node", "#tree_node_"+page_nr); 
	 },
	 
	 setNodeType: function(id, type)
	 {
	 	// Localiza o ID do LI pelo ID do Canvas
	 	var li = $("li[url='"+id+"']");
	 	if (li.length)
	 	{
	 		var ins = li.find('a>.jstree-icon');
	 		if (ins.length)
	 		{
	 			if (type == 'default_red')
	 			{
	 				ins.css('background-image', 'url('+Scan.baseURL+'scan/imagens/icones/document_red.png)');
	 			}
	 			else if (type == 'default')
	 			{
	 				ins.css('background-image', 'url('+Scan.baseURL+'imagens/icones/document_p_16x16-trans.png)');
	 			}
	 		}
	 	}
	 	
	 },
	 
	 // Constr�i o HTML da tree, atrav�s dos objetos, e aplica JSTree.
	 buildTreeHTML: function(convertedFiles, parentId)
	 {
	 	
	 	var tree = TreeData.buildTree(convertedFiles);
	 	if (tree)
	 	{
	 		var container = TreeData.getContainer();
	 		if (container.length)
	 		{
	 			var treeUL = this.getContainerUL();
	 			if (treeUL.length)
	 			{
	 				var nodes = tree.nodes;
	 				if (nodes)
	 				{
	 					var liPai;
	 					
	 					// Adiciona um n� pai para todos os arquivos digitalizados ou importados, caso ainda n�o exista.
	 					
	 					// Update: adiciona tamb�m um n� pai caso os documentos tenham sido Recuperados.
	 					if (parentId && parentId != 'li_digitalized')
	 					{
	 						if (TreeData.hasParentNode(parentId) == false)
							{
								if (parentId == 'li_recovered')
									liPai = TreeData.addParentNode(parentId, '<%=I18nUtils.getString("recoveredDocuments")%>', treeUL);
							}
							else
								liPai = TreeData.getParentNode(parentId);
	 					}
	 					else
	 					{
	 						if (TreeData.hasParentNode('li_digitalized') == false)
							{
								liPai = TreeData.addParentNode('li_digitalized', '<%=I18nUtils.getString("scannedDocuments")%>', treeUL);
							}
							else
								liPai = TreeData.getParentNode('li_digitalized');
	 					}

						var ulPai = liPai.children('ul');
						
						// Adiciona os documentos digitalizados	
						this.addChildNodes(liPai.attr('id'), nodes);
	 				}
	 			}
	 		}
	 		
	 		// Aplica a JSTree
	 		TreeData.applyJSTree(container);
	 		
	 		setTimeout(function() {
	 			Navigation.page(1);
	 			if (Scan.baptism == 'true')
	 				Toolbar.toggleBaptismButton('enable');
	 			Toolbar.toggleSaveButton('enable');
	 		});
	 		
	 	}
	 },
	 
	 // Aplica JSTree.
	 applyJSTree: function(treeRef)
	 {
	 		if (treeRef.length)
	 		{
				treeRef.jstree({ 
				    "themes" : {
				        "theme" : "neomind",
				        "dots" : false,
				        "icons" : true,
				        "checkbox" : true
				    },
				    'crrm':
				    {
				    	'move':
				    	{
				    		'check_move': function(data)
				    		{
				    			var p = this._get_parent(data.o);
				    			
				    			if ($(data.np[0]).is('div'))
				    				return false;
				    			
				    			// N�o permite arrastar um n� filho para dentro de outro n� filho.
				    			if(this.get_path(data.np[0]).length > 1) 
                       				return false;
                       			  
                       			// N�o permite arrastar n�s raiz.
                       			if (p == -1)
                       				return false;
                       			  
                       			return true;
				    		}
				    	},
				    },
				      'types': {
        	'types' : {
            'root' : {
                'icon' : {
                    'image' : Scan.baseURL+'imagens/icones_final/jstree/document.png'
                },
            },
            'default_red' : {
                'icon' : {
                    'image' : Scan.baseURL+'scan/imagens/icones/document_red.png'
                }
            },
            'default' : {
                'icon' : {
                    'image' : Scan.baseURL+'imagens/icones/document_p_16x16-trans.png'
                },
            }
        		}

    		},
			"plugins" : [ "themes", "ui", "html_data", "types", "crrm", "dnd", "checkbox", "json_data" ]
				    })
				    .bind('select_node.jstree', function(event,data){
				    	var url = data.rslt.obj.attr("url");
				    	var page = data.rslt.obj.attr("page_nr"); 
				    	var type = data.rslt.obj.attr("type");
				    	
				    	if (type == 'document')
				    	return;

				    	ImageActions.clearCropArea(document.getElementById(Scan.getCurrentViewingCanvasId()));
				    	CanvasManager.drawImage(url, Scan.tempFolder + url);
				    	Scan.setCurrentViewingCanvasId(url);
				    	Toolbar.updateCurrentPage(parseInt(page)+1);
				    	Toolbar.toggleImageButtons('enable');
				    	Toolbar.setZoomValue('100%');
				    	Zoom.current = 100;
				    		
				}).bind('loaded.jstree', function(event,data){
				    	$(this).jstree("open_all");
						
				}).bind("move_node.jstree",function(event,data){
    					TreeData.updatePageNumbers();
    			}).bind("check_node.jstree",function(event,data){
    					
    					
    			});
	 		}
	 },
	 
	 getContainer: function()
	 {
	 	return $('#'+this.container);
	 },
	 
	 getContainerUL: function()
	 {
	 	return $('#'+this.container).children('ul');
	 },
	 
	 hasCheckedNodes: function()
	 {
	 	var checked = TreeData.getCheckedNodes();
	 	if (!checked || checked.length == 0)
	 		return false;
	 	
	 	return true;
	 },
	 
	 // Obt�m os n�s checados (checked) na JSTree
	 getCheckedNodes: function(params)
	 {
	 	var checkedNodes;
	 	
	 	// Evita que n�s pais retornem junto com a lista de n�s checados.
	 	if (params && params.onlyChildren)
	 	{
	 		checkedNodes = this.getContainer().jstree("get_checked", null, true);
	 		var newNodes = new Array();
	 		
	 		checkedNodes.each(function() {
	 			var type = $(this).attr('type');
	 			if (type != 'document')
	 				newNodes.push($(this).get(0));
	 		});
	 		
	 		checkedNodes = newNodes;
	 	}
	 	else
	 		checkedNodes = this.getContainer().jstree("get_checked", null, false);
	 	
	 	return checkedNodes;
	 },
	 
	 // Conta o nr. de n�s filhos de um pai.
	 countChildren: function(parentId)
	 {
	 	var li = $('#'+parentId);
	 	if (li.length)
	 	{
	 		return li.find('li').length;
	 	}
	 },
	 
};


$(document).ready(function() {
	TreeData.init();
});