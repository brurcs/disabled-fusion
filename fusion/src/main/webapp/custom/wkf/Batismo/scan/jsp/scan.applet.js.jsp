<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

var ScanApplet = {

	 appletRef: "",
	 loaded: false,
	 loadTimeout: 90000,
	 
	 init: function()
	 {
	 	
	 },
	 insertApplet: function (id, showUI, blackAndWhite, dpi, adf, duplex) 
	 {
    	if ($.browser.msie) {
            obj = document.createElement("Object");
        } else {
            obj = document.createElement("applet");
        }
        obj.id = id;
        obj.name = id;
        obj.archive= "twain/neoeztwain.jar,twain/jna.jar,twain/jai_imageio.jar,twain/jai_codec.jar,twain/jai_core.jar,twain/commons-httpclient-3.0.1.jar,twain/commons-codec-1.3.jar,twain/commons-logging-1.1.jar";
        obj.code="com.neomind.twain.LTwain";
        obj.width="1px";
        obj.height="1px";
        obj.scriptable="true";
        
        var p1 = document.createElement("param");
        p1.setAttribute("name", "codebase");
        p1.setAttribute("value", "<%= PortalUtil.getBaseURL() %>");
        obj.appendChild(p1);
        
        var p2 = document.createElement("param");
        p2.setAttribute("name", "showUI");
        p2.setAttribute("value", Scan.showUI);
        obj.appendChild(p2);
        
        var p3 = document.createElement("param");
        p3.setAttribute("name", "blackAndWhite");
        p3.setAttribute("value", Scan.blackAndWhite);
        obj.appendChild(p3);
        
        var p4 = document.createElement("param");
        p4.setAttribute("name", "resolution");
        p4.setAttribute("value", Scan.resolution);
        obj.appendChild(p4);
        
        var p5 = document.createElement("param");
        p5.setAttribute("name", "srcDLLURL");
        p5.setAttribute("value", "<%= PortalUtil.getBaseURL() %>twain/");
        obj.appendChild(p5);
        
        var p6 = document.createElement("param");
        p6.setAttribute("name", "uploadURL");
        p6.setAttribute("value", "<%= PortalUtil.getBaseURL() %>uploadScan");
        obj.appendChild(p6);
        
        // Par�metro de callback da Applet carregada.
        var p7 = document.createElement("param");
        p7.setAttribute("name", "loadedCallback");
        p7.setAttribute("value", "ScanApplet.setLoaded();");
        obj.appendChild(p7);
        
           
        var p8 = document.createElement("param");
        p8.setAttribute("name", "localeCountry");
        p8.setAttribute("value", Scan.localeCountry);
        obj.appendChild(p8);
        
          
        var p9 = document.createElement("param");
        p9.setAttribute("name", "localeLanguage");
        p9.setAttribute("value", Scan.localeLanguage);
        obj.appendChild(p9);
        
        
        var p10 = document.createElement("param");
        p10.setAttribute("name", "adf");
        p10.setAttribute("value", Scan.adf);
        obj.appendChild(p10);
        
        
        var p11 = document.createElement("param");
        p11.setAttribute("name", "duplex");
        p11.setAttribute("value", Scan.duplex);
        obj.appendChild(p11);
        
        document.body.appendChild(obj);
        ScanApplet.appletRef = obj;
        
        return obj;
    },
    callAcquire: function(appletRef, deviceName)
    {
    	var scanned = appletRef.acquire(deviceName);
    	if (scanned && scanned.length)
    		return scanned;
    	else
    	{
    		
    	}
    },
    getDevices: function(appletRef)
	{
		var devices = appletRef.getDeviceList();
		var spl = devices.split(';');
	
		for (var i=0;i<spl.length;i++)
		{
			//var opt = $('<option/>', 
			//{
				//value: spl[i],
				//text: spl[i]
			//});
		//$('#sl').append(opt);
		}
	},
	
	checkLoaded: function(id)
	{
		var applet = document.getElementById(id);

		 if ( typeof applet !='undefined' )
		 {
		   try
		   {
		   		if (applet.isActive()) 
		   		{
		    		return true;
		   		}
		   		else
		   		{
		     		return false;
		   		}
		   }catch(err) // se nao carregou a applet, d� erro na chamada da fun��o isActive
		   {
		  		return false;
		   }
		 }
		 else 
		 {
		  	return false;
		 }
    },
    
    // Callback executado pelo m�todo init() da applet via JSObject.
    setLoaded: function()
    {
    	ScanStatus.hideStatusMessage(Scan.layoutWrapper, function()
    	{
    		ScanApplet.loaded = true;
    	});
    },
    
    // Verifica se a Applet carregou depois de X segundos.
    // Se nao carregou, desbloqueia a tela e alerta.
    checkLoadedError: function()
    {
    	if (ScanApplet.loaded == false)
    	{
    		ScanStatus.hideStatusMessage(Scan.layoutWrapper);
    		Scan.showErrorPanel();
    	}
    },
    
    loadDefaultDevice: function()
	{
		var ret = applet.getDefaultDevice().split('#');
		return ret[0];
	},
	
	saveDefaultDevice: function(name)
	{
		applet.saveDefaultDevice(name);
	},
	
	hasDefaultDevice: function()
	{
		return applet.hasDefaultDevice();
	},
	
	setDefaultDevice: function()
	{
		Scan.deviceName = loadDefaultDevice();
	},
	
	setBlackAndWhite: function(value)
	{
		applet.setBlackAndWhite(value.toString());
	},
	
	setShowUI: function(value)
	{
		applet.setShowUI(value.toString());
	},
	
	setResolution: function(value)
	{
		applet.setResolution(value.toString());
	},
	
	setAdf: function(value)
	{
		applet.setAdf(value.toString());
	},
	
	setDuplex: function(value)
	{
		applet.setDuplex(value.toString());
	},
};

$(document).ready(function() {
	ScanApplet.init();
});