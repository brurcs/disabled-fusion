<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
Chamadas de servlets.
--%>

var ScanServlets = {

	 getDocumentTypeName: function(id)
	 {
	 		var url = Scan.baseURL + 'scanServlet/?action=getDocumentTypeName&docId='+id;
	 		var typeName = callSync(url);
	 		return typeName;
	 },
	 
	 getDocumentTitle: function(id)
	 {
	 		var url = Scan.baseURL + 'scanServlet/?action=getDocumentTitle&docId='+id;
	 		var title = callSync(url);
	 		return title;
	 },
};