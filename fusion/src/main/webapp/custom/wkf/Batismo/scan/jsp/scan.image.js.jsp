<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
A��es realizadas na imagem.
Brilho, contraste, rota��o e crop.
--%>

var ImageActions = {
	 
	 SelectedData:
	 {
	 	has: false,
	 	x1: null,
	 	y1: null,
	 	x2: null,
	 	y2: null,
	 	w: null,
	 	h: null
	 },
	 
	 clearSelectedData: function()
	 {
	 	ImageActions.SelectedData.x1 = null;
	 	ImageActions.SelectedData.y1 = null;
	 	ImageActions.SelectedData.x2 = null;
	 	ImageActions.SelectedData.y2 = null;
	 	ImageActions.SelectedData.w = null;
	 	ImageActions.SelectedData.h = null;
	 	ImageActions.SelectedData.has = false;
	 },
	 
	 hasSelection: function()
	 {
	 	return (ImageActions.SelectedData.has == true);
	 },
	 
	 // Brilho positivo e negativo.
	 brightness: function(adjustment)
	 {
	 	var canvas = document.getElementById(Scan.getCurrentViewingCanvasId());
	 	if (canvas)
	 	{
	 		var ctx = canvas.getContext('2d');
	 		var width = canvas.width;
    		var height = canvas.height;
    		
    		var pixels = PixelManager.getPixels(canvas, ctx);
	 		var data = pixels.data;
	 		
	 		var adjInt = parseInt(adjustment);
	 		for (var i=0; i<data.length; i+=4)
	 		{
	 			if (adjInt > 0)
	 			{
	 				data[i] = parseInt(data[i]+adjInt) < 255 ? parseInt(data[i]+adjInt) : 255;
	 				data[i+1] = parseInt(data[i+1]+adjInt) < 255 ? parseInt(data[i+1]+adjInt) : 255;
	 				data[i+2] = parseInt(data[i+2]+adjInt) < 255 ? parseInt(data[i+2]+adjInt) : 255;
    			}
    			else if (adjInt < 0)
    			{
    				data[i] = parseInt(data[i]-adjInt*-1) >= 0 ? parseInt(data[i]-adjInt*-1) : 0;
					data[i+1] = parseInt(data[i+1]-adjInt*-1) >= 0 ? parseInt(data[i+1]-adjInt*-1) : 0;
					data[i+2] = parseInt(data[i+2]-adjInt*-1) >= 0 ? parseInt(data[i+2]-adjInt*-1) : 0;
    			}
			}
			
			ctx.putImageData(pixels, 0, 0);
			CanvasManager.saveImageToDisk(canvas);
			ScanUtils.setCanvasStatus(canvas, 'true');
		}
	 },
	 
	 // Contraste negativo e positivo.
	 contrast: function(adjustment)
	 {
	 	var canvas = document.getElementById(Scan.getCurrentViewingCanvasId());
	 	if (canvas)
	 	{
	 		var ctx = canvas.getContext('2d');
	 		var width = canvas.width;
    		var height = canvas.height;
 
    		var adjInt = parseInt(adjustment);
    		
    		var pixels = PixelManager.getPixels(canvas, ctx);
	 		var data = pixels.data;
	 		
	 		var factor = (259 * (adjInt + 255)) / (255 * (259 - adjInt));
	 		
	 		for(var i=0;i<data.length;i+=4)
   			{
       			data[i] = (factor * ((data[i] - 128) + 128));
        		data[i+1] = (factor * ((data[i+1] - 128) + 128));
       			data[i+2] = (factor * ((data[i+2] - 128) + 128));
    		}
    			
	 		ctx.putImageData(pixels, 0, 0);
	 		CanvasManager.saveImageToDisk(canvas);
	 		ScanUtils.setCanvasStatus(canvas, 'true');
	 	}			
		
	 },
	 rotate: function(angle)
	 {
	 	var canvas = document.getElementById(Scan.getCurrentViewingCanvasId());
	 	
	 	if (canvas)
	 	{
	 		var ctx=canvas.getContext("2d");
	 		
	 		ImageActions.clearCropArea(canvas);
	 		
	 		// SRC do Canvas (base64)
			//var img64 = 'data:image/jpeg;base64,'+applet.getImage(Scan.tempFolder + Scan.getCurrentViewingCanvasId());
			var img64 = canvas.toDataURL("image/jpeg");
			
			var imageObj = new Image();
			imageObj.onload = function()
			{
				canvas.width = imageObj.height;
				canvas.height = imageObj.width;
				ctx.drawImage(imageObj,canvas.width/2-imageObj.width/2,canvas.height/2-imageObj.width/2);
				ctx.clearRect(0,0,canvas.width,canvas.height);
   				ctx.save();
    			ctx.translate(canvas.width/2,canvas.height/2);
    			ctx.rotate(angle*Math.PI/180);
    			ctx.drawImage(imageObj,-imageObj.width/2,-imageObj.height/2);
    			$(canvas).attr('source_width', canvas.width); // para o Zoom
    			$(canvas).attr('source_height', canvas.height); // para o Zoom
    			CanvasManager.saveImageToDisk(canvas);
    			ScanUtils.setCanvasStatus(canvas, 'true');
    			ctx.restore();
			}
			imageObj.src = img64;
			
			
	 	}
	 },
	 
	 resize: function(id, src, newWidth, newHeight)
	 {
	 	var canvas = document.getElementById(Scan.getCurrentViewingCanvasId());
	 	if (canvas)
	 	{
	 		var ctx=canvas.getContext("2d");
	 		
	 		// SRC do Canvas (base64)
			//var img64 = 'data:image/jpeg;base64,'+applet.getImage(Scan.tempFolder + Scan.getCurrentViewingCanvasId());
			var img64 = canvas.toDataURL("image/jpeg");
			
			Zoom.current = 100;
			var img = new Image();
			img.onload = function()
			{
				canvas.width = newWidth;
    			canvas.height = newHeight;
    			$(canvas).attr('source_width', canvas.width); // para o Zoom
    			$(canvas).attr('source_height', canvas.height); // para o Zoom
    			ctx.drawImage(img, 0, 0, newWidth, newHeight);
    			CanvasManager.saveImageToDisk(canvas);
    			ScanUtils.setCanvasStatus(canvas, 'true');
	 		}
	 		img.src = img64;
	 	}
	 },
	 
	 toggleSelect: function(el, forceRemove)
	 {
	 	var canvas = document.getElementById(Scan.getCurrentViewingCanvasId());
	 	if (canvas)
	 	{
	 		var isEnabled = $(el).children('a').hasClass('neo-button-on');
	 		if (isEnabled || forceRemove == true)
	 		{
	 			$(el).children('a').removeClass('neo-button-on');
	 			$(canvas).imgAreaSelect(
	 			{remove: true}
	 			);
	 			ImageActions.clearSelectedData();
	 		}
	 		else
	 		{
	 			$(el).children('a').addClass('neo-button-on');
	 			$(canvas).imgAreaSelect(
	 			{
	 				handles: true,
	 				onSelectEnd: function (img, selection) 
	 				{
        				ImageActions.SelectedData.x1 = selection.x1;
        				ImageActions.SelectedData.y1 = selection.y1;
        				ImageActions.SelectedData.x2 = selection.x2;
        				ImageActions.SelectedData.y2 = selection.y2;
        				ImageActions.SelectedData.w = selection.width;
        				ImageActions.SelectedData.h = selection.height;
        				
        				if (selection.height != 0 && selection.width != 0)
        					ImageActions.SelectedData.has = true;
        				else
        					ImageActions.SelectedData.has = false;
    				}
	 			}
	 			);
	 		}
	 		var ctx = canvas.getContext('2d');
	 		
	 	} else alert('Nenhum arquivo selecionado!');
	 },
	 
	 cropImpl: function(canvas, id, src, crop_x1, crop_y1, crop_w, crop_h, crop_x2, crop_y2, crop_w, crop_h)
	 {
	 	var img64 = canvas.toDataURL("image/jpeg");
		var fromCache = false;
		
		if ($(canvas).attr('zoom_value') != undefined)
			fromCache = true;
		
		var img = new Image();
		img.onload = function()
		{
			var ctx=canvas.getContext("2d");
			
			// Ajustar source_width e source_height de acordo caso exista zoom negativo ou positivo.
			var source_w, source_h;
			
			if (fromCache == false)
			{
				source_w = (crop_w*100) / Zoom.current;
				source_h = (crop_h*100)  / Zoom.current;
				$(canvas).attr('source_width', source_w);
				$(canvas).attr('source_height', source_h);
			}
			else
			{
				// Considera o Zoom no crop
				crop_w = crop_w * 100 / Zoom.current;
				crop_h = crop_h * 100 / Zoom.current;
				crop_x1 = crop_x1 * 100 / Zoom.current;
				crop_y1 = crop_y1 * 100 / Zoom.current;
			
				$(canvas).attr('source_width', crop_w);
				$(canvas).attr('source_height', crop_h);
			}
				
			ctx.clearRect ( crop_x1 , crop_y1 , crop_w , crop_h );
			canvas.width = crop_w;
    		canvas.height = crop_h;
			
    		ctx.drawImage(img, crop_x1, crop_y1, crop_w, crop_h, 0, 0, crop_w, crop_h );
    		CanvasManager.saveImageToDisk(canvas);
    		ScanUtils.setCanvasStatus(canvas, 'true');
    		
    		if (fromCache == true)
    		{
    			$(canvas).removeAttr('zoom_value');
    			Zoom.zoomPageTo(canvas, Zoom.current);
    		}
    			
		}
		
		if (fromCache == true)
		{
			img64 = Zoom.getImageFromCache(id);
		}
		img.src = img64;
	 },
	 
	 crop: function()
	 {
	 	var has = ImageActions.hasSelection();
	 	if (has)
	 	{
	 		var canvas = document.getElementById(Scan.getCurrentViewingCanvasId());
	 		if (canvas)
	 		{
	 			var crop_x1 = ImageActions.SelectedData.x1;
	 			var crop_y1 = ImageActions.SelectedData.y1;
	 			var crop_x2 = ImageActions.SelectedData.x2;
	 			var crop_y2 = ImageActions.SelectedData.y2;
	 			var crop_w = ImageActions.SelectedData.w;
	 			var crop_h = ImageActions.SelectedData.h;
	 			var destW = crop_w;
	 			var destH = crop_h;
	 			
	 			this.cropImpl(canvas, Scan.getCurrentViewingCanvasId(), Scan.tempFolder + Scan.getCurrentViewingCanvasId(), 
		 	 	crop_x1, crop_y1, crop_w, crop_h, 0, 0, crop_w, crop_h);
		 	 	
		 	 	ImageActions.clearCropArea(canvas);
		 	 	ImageActions.unselectCropButton();
	 		}
	 	}
	 	else
	 	{
	 		alert('Nenhuma �rea selecionada para recortar!');
	 	}
	 	
	 },
	 
	 // Limpa a �rea selecionada para crop.
	 clearCropArea: function(canvas)
	 {
	 	ImageActions.clearSelectedData();
	 	if (canvas)
	 	{
	 		$(canvas).imgAreaSelect(
	 		{
	 			remove: true
	 		}
	 		);
	 		
	 		ImageActions.unselectCropButton(true);
	 	}
	 },
	 
	 // Desmarca o bot�o de Crop.
	 unselectCropButton: function(force)
	 {
	 	ImageActions.toggleSelect(document.getElementById('td-select-button'), true);
	 },
	 
	 
};