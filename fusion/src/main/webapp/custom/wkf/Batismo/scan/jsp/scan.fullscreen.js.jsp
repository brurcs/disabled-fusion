<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

var Fullscreen = {
		
		fullScreen: false,
		
		enterFullScreen: function()
		{
			var divs = parent.document.getElementsByTagName('div');
			var onlyParent = false;
			
			for(var ind=0;ind < divs.length;ind++)
			{
				if($(divs[ind]).attr('class') == 'gadget')
				{
					divGadget = divs[ind];
					onlyParent = true;
					break;
				}
			}
			
			// Na tela do editar, precisa pegar um n�vel acima.
			if(!onlyParent)
			{
				var divs = parent.parent.document.getElementsByTagName('div');
				for(var ind=0;ind< divs.length;ind++)
				 {
					if($(divs[ind]).attr('class') == 'gadget')
					{
						divGadget = divs[ind];
						onlyParent = true;
						break;
					}
				}
			}
			
			divModal = divs[ind + 3];
			divGadgetInfo = new Array();
			divModalInfo = new Array();
			iframeModalInfo = new Array();
			
			divGadgetInfo.push(divGadget.style.height);
			divGadgetInfo.push(divGadget.style.width);
			divGadgetInfo.push(divGadget.style.top);
			divGadgetInfo.push(divGadget.style.left);
			divGadgetInfo.push(divGadget.style.marginLeft);
			divGadgetInfo.push(divGadget.style.marginTop);
			
			divModalInfo.push(divModal.style.height);
			divModalInfo.push(divModal.style.width);
			
			divGadget.style.height = "100%";
			divGadget.style.width  = "100%";
			divGadget.style.top    = "0px";
			divGadget.style.left   = "0px";
			divGadget.style.marginLeft = "0px";
			divGadget.style.marginTop  = "0px";
			
			divModal.style.height = "97%";
			divModal.style.width  = "100%";
			
			var iframes = parent.parent.document.getElementsByTagName('iframe');
			
			iframeModal = iframes[0];
			
			iframeModalInfo.push(iframeModal.style.height);
			iframeModalInfo.push(iframeModal.style.width);
			
			iframeModal.style.height = "100%";
			iframeModal.style.width  = "100%";
				
			$('#exitFullScreenButton').show();	
			$('#openFullScreenButton').hide();	
			
			Fullscreen.fullScreen = true;
		},
		
		exitFullScreen: function()
		{
			divGadget.style.height = divGadgetInfo[0];
			divGadget.style.width  = divGadgetInfo[1];
			divGadget.style.top    = divGadgetInfo[2];
			divGadget.style.left   = divGadgetInfo[3];
			divGadget.style.marginLeft = divGadgetInfo[4];
			divGadget.style.marginTop  = divGadgetInfo[5];
			
			divModal.style.height = divModalInfo[0];
			divModal.style.width  = divModalInfo[1];
			
			iframeModal.style.height = iframeModalInfo[0];
			iframeModal.style.width  = iframeModalInfo[1];
			
			$('#openFullScreenButton').show();	
			$('#exitFullScreenButton').hide();
			
			Fullscreen.fullScreen = false;
		}
};