<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
--%>

var ScanStatus = {
	
	updateStatus: function(text)
	{
		$('#span-scan-msg').text(text);
	},
	 
	showScanningMessage: function(element, deviceName, callback)
	{
		var messageContent = $('<div/>', {
		style: 'display: inline-block;padding: 5px; ',
	}).append($('<span/>', {
		text: '<%=I18nUtils.getString("scanning")%>...',
		style: 'font-family: tahoma; font-size: 13px;'
	})).append($('<div/>', {
		style: 'margin-top: 1px;'
	}).append($('<span/>', {
		text: deviceName,
		id: 'span-scan-msg',
		style: 'font-family: tahoma; font-size: 13px; font-weight: bold;'
	}))
	);

	if (element)
	{
		var el = document.getElementById(element);
		$(el).block({
			baseZ: 999999,
			message: $(messageContent),
			centerX: true,
            centerY: true,
            showOverlay: true, 
	        css: 
	        { 
	           border: '0px solid #dddddd',
	           'border-right': '1px solid #dddddd',
	           backgroundColor: '#fff', 
	           '-webkit-border-radius': '5px', 
	           'border-radius': '5px', 
	           '-moz-border-radius': '5px', 
	           color: '#000',
	           textAlign: 'center',
	           left: ($(window).width() - 270) /2 + 'px', 
	           width: '265px',
	           boxShadow: '0 0 1px 0 #000'
	        },
			overlayCSS: 
			{
	            backgroundColor: '#cacaca',
	            opacity: 0.2
	        },
			onBlock: callback
		});
	}
	
	},

	showTreeMessage: function(element, message, callback)
	{
		var messageContent = $('<div/>', {
		style: 'display: inline-block;padding: 5px; ',
	}).append($('<span/>', {
		text: message,
		style: 'font-family: tahoma; font-size: 13px;'
	})).append($('<div/>', {
		style: 'margin-top: 6px;'
	})).append($('<input/>', {
		type: 'button',
		'class': 'input_button',
		value: '<%=I18nUtils.getString("cancel")%>',
		style: 'margin-top: 6px; margin-bottom: 2px;'
	}));

	if (element)
	{
		var el = document.getElementById(element);
		$(el).block({
			baseZ: 999999,
			message: $(messageContent),
			centerX: true,
            centerY: true,
            showOverlay: true, 
	        css: 
	        { 
	           border: '0px solid #dddddd',
	           'border-right': '1px solid #dddddd',
	           backgroundColor: '#fff', 
	           '-webkit-border-radius': '5px', 
	           'border-radius': '5px', 
	           '-moz-border-radius': '5px', 
	           color: '#000',
	           textAlign: 'center',
	           left: ($(window).width() - 270) /2 + 'px', 
	           width: '265px',
	           boxShadow: '0 0 1px 0 #000'
	        },
			overlayCSS: 
			{
	            backgroundColor: '#cacaca',
	            opacity: 0.2
	        },
	        onBlock: callback
		});
	}
	},
	
	showConversionMessage: function(element, message, callback)
	{
		var messageContent = $('<div/>', {
		style: 'display: inline-block;padding: 5px; ',
	}).append($('<span/>', {
		text: '<%=I18nUtils.getString("convertingFiles")%>...',
		style: 'font-family: tahoma; font-size: 13px;'
	})).append($('<div/>', {
		style: 'margin-top: 6px;'
	})).append($('<input/>', {
		type: 'button',
		'class': 'input_button',
		value: '<%=I18nUtils.getString("cancel")%>',
		style: 'margin-top: 3px; margin-bottom: 2px;'
	}));

	if (element)
	{
		var el = document.getElementById(element);
		$(el).block({
			baseZ: 999999,
			message: $(messageContent),
			centerX: true,
            centerY: true,
            showOverlay: true, 
	        css: 
	        { 
	           border: '0px solid #dddddd',
	           'border-right': '1px solid #dddddd',
	           backgroundColor: '#fff', 
	           '-webkit-border-radius': '5px', 
	           'border-radius': '5px', 
	           '-moz-border-radius': '5px', 
	           color: '#000',
	           textAlign: 'center',
	           left: ($(window).width() - 270) /2 + 'px', 
	           width: '265px',
	           boxShadow: '0 0 1px 0 #000'
	        },
			overlayCSS: 
			{
	            backgroundColor: '#cacaca',
	            opacity: 0.2
	        },
	        onBlock: callback
		});
	}
	},
	
	showAppletLoadingMessage: function(element, message, callback)
	{
		var messageContent = $('<div/>', {
		style: 'display: inline-block;padding: 5px; ',
	}).append($('<span/>', {
		text: message,
		style: 'font-family: tahoma; font-size: 13px;'
	})).append($('<div/>', {
		style: 'margin-top: 4px;'
	}));

	if (element)
	{
		var el = document.getElementById(element);
		$(el).block({
			baseZ: 999999,
			message: $(messageContent),
			centerX: true,
            centerY: true,
            showOverlay: true, 
	        css: 
	        {
	           border: '0px solid #dddddd',
	           'border-right': '1px solid #dddddd',
	           backgroundColor: '#fff', 
	           '-webkit-border-radius': '5px', 
	           'border-radius': '5px', 
	           '-moz-border-radius': '5px', 
	           color: '#000',
	           textAlign: 'center',
	           left: ($(window).width() - 270) /2 + 'px', 
	           width: '265px',
	           boxShadow: '0 0 1px 0 #000'
	        },
			overlayCSS: 
			{
	            backgroundColor: '#cacaca',
	            opacity: 0.2
	        },
	        onBlock: callback
		});
	}
	},
		
	hideStatusMessage: function(element, callback)
	{
		if (element)
		{
			var el = document.getElementById(element);
			if (callback)
			{
				$(el).unblock({
					onUnblock: callback
				});
			}
			else $(el).unblock();
			
		}
	},
	
	 
	 
};