<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
Obten��o e altera��o de pixels.
--%>

var PixelManager = {
	 
	 getPixels: function(canvas, ctx)
	 {
    	 var width = canvas.width;
    	 var height = canvas.height;
    	
    	 var pixels = ctx.getImageData(0,0, width, height);
    	 return pixels;
	 },
	 
};