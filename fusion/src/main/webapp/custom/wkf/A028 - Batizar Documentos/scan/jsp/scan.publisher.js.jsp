<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
Salvamento das imagens digitalizadas, com e sem batismo;
--%>
var Publisher = {
	
	typeIds: "",
	
	// Valida alguns pr�-requisitos para Salvar.
	checkSaveConditions: function()
	{
		var isBaptism = Scan.baptism;
		
		var hasRecoveredParent = TreeData.hasParentNode('li_recovered');
		var hasDigitalizedParent = TreeData.hasParentNode('li_digitalized');
		
		if (isBaptism == 'false')
		{
			var recovered = TreeData.countChildren('li_recovered');
			if (recovered > 0)
			{
				// Se tiver o n� Documentos Digitalizados, obriga a eliminar ou transferir as p�ginas recuperadas.
				if (hasDigitalizedParent)
				{
					alert('<%=I18nUtils.getString("pendingRecoveredPages")%>');
					return false;
				}
			}
			else if (recovered == 0 && hasRecoveredParent)
			{
					alert('<%=I18nUtils.getString("removeRecoveredDocuments")%>');
					return false;
			}
		}
		else if (isBaptism == 'true')
		{
			// Se tiver Batismo, obriga a batizar tudo, ou seja, n�o pode ter Documentos Digitalizados ou Documentos Recuperados
			if ((hasDigitalizedParent && TreeData.countChildren('li_digitalized') > 0) || (hasRecoveredParent && TreeData.countChildren('li_recovered') > 0))
			{
				alert('<%=I18nUtils.getString("baptismPagesBeforeSaving")%>');
				return false;
			}
			
		}
		
		return true;
	},
	
	save: function()
	{
	
		if (this.checkSaveConditions())
		{
			var jsonTree = TreeData.buildJSONTree();
			var param = new Object();
			param['jsonTree'] = jsonTree;
			param['tempFolder'] = Scan.tempFolder;
			Publisher.typeIds = '';
	
			$.ajax({
			url: Scan.baseURL + 'scanServlet/?action=groupFiles',
			dataType: 'json',
			data: param,
			async: false,
			success: function(data)
			{
				// Um ou mais tipos de documento
				var listaTipos = data.files.split(';');
				
				var files=''; // IDS dos NeoFiles
				for (x in listaTipos)
				{
					// Converte os arquivos em um TIFF
					var convertedTiff = applet.convertFiles(listaTipos[x]);
					if (convertedTiff != null)
					{
						var aux = convertedTiff.split('#');
						Publisher.typeIds = Publisher.typeIds+aux[0]+";"; // usado em outra fun��o
						files = files+aux[1]+";";
					}
				}
				
				// Se for Batismo
				if (Scan.baptism == true || Scan.baptism == 'true')
				{
					applet.uploadFiles(files, "Publisher.afterUpload");
				}
				else
				{
					applet.uploadFiles(files, "Publisher.saveToFileField");
				}
					
			},
			error: function (xhr, ajaxOptions, thrownError) {
	        	alert(xhr.status);
	        	alert(thrownError);
	      	}
			});
		}
	},
	
	// Salva o arquivo scaneado no campo NeoFile que abriu o Digitalizador.
	// C�pia da fun��o original, levemente alterada.
	saveToFileField: function(data)
	{
		var valor = data.split(';');
		var i;
		var fr = parent.document.frames ? parent.document.frames : parent.frames;
		var doc;
		var id = Scan.inputId;

		for (i=0;i < fr.length;i++)
		{
			if (fr[i].document.getElementById(id+'_scan'))
			{
				doc = fr[i].document;
				break;
			}
		}

		if(!doc)
		{
			doc = parent.document;
		}

		if(doc)
		{
			var parDiv = doc.getElementById(id + '_scan');
			doc.getElementById(id+'_old').value = valor[0];
			parDiv.value = valor[0];
			if(parDiv.onchange)
				parDiv.onchange();
			parDiv = doc.getElementById(id + 'tagA');
			if(parDiv)
				parDiv.parentNode.removeChild(parDiv);
			parDiv = doc.getElementById('listFile_' + id);
			if(parDiv)
			{
				var textURL = "<%=I18nUtils.getString("fileOpen1")%>";
				var a = "<a class=\"acao\" href=\"<%= PortalUtil.getBaseURL()%>file/download/" + 
						doc.getElementById(id + '_scan').value + "\" title=\"" + textURL +  "\">" + 
	 					"<img src=\"imagens/icones_final/document_search_16x16-trans.png\" width=\"16\" height=\"16\" align=\"absmiddle\"/>" + 
	 					"&nbsp;" + textURL + "</a>";
				parDiv.innerHTML = a;
			}

			NEO.neoUtils.returnParent().NEO.neoModalWindow.delModal(window.name.replace("_iframe", ""));
		}
		else
		{
			alert('<%=I18nUtils.getString("fileFieldNotFound")%>');
		}
		
	},
	
	afterUpload: function(ids)
	{
		var isForm = false;
		$.ajax({
			url: Scan.baseURL + 'scanServlet/?action=buildDocuments&key=123&idTipos='+Publisher.typeIds+'&isForm='+isForm+'&neoFileIds='+ids,
			success: function(data)
			{
				ScanUtils.runFunction(Scan.callback, [data]);
				
				NEO.neoUtils.returnParent().NEO.neoModalWindow.delModal(window.name.replace("_iframe", ""));
			}
		});
		
	},
	
	
};
