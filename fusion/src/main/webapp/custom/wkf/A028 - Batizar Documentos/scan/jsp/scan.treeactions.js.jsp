<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
A��es realizadas na Tree (Batismo, Exclus�o, Movimenta��o de P�ginas).
--%>
var TreeActions = {

	 Baptism:
	 {
	 	// Batiza (aplica o tipo de documento) a uma lista de arquivos.
	 	baptismFiles: function(documentTypeId)
	 	{
	 		// Obt�m as p�ginas selecionadas na Tree
	 		var checkedNodes = TreeData.getCheckedNodes({onlyChildren: true});
	 		
	 		if (checkedNodes && checkedNodes.length>0)
	 		{
	 			if (documentTypeId != -1)
	 			{
	 				var documentTitle = ScanServlets.getDocumentTitle(documentTypeId);
	 				var parentLI;
	 			
	 				// Se j� existe o n� para este batismo, insere os arquivos batizados nele.
	 				if (TreeData.hasParentNode(documentTypeId))
	 				{
	 					parentLI = TreeData.getParentNode(documentTypeId);
	 				}
	 				else // cria o n� batizado e insere os arquivos.
	 				{
	 					parentLI = TreeData.addParentNode(documentTypeId, documentTitle, TreeData.getContainerUL());
	 				}

	 				TreeData.transferNodes(parentLI.attr('id'), checkedNodes);
	 				TreeData.updatePageNumbers();
	 				TreeData.applyJSTree(TreeData.getContainer());

	 				ScanWindows.BaptismPopup.toggle();
	 			} 
	 			}
	 	}
	 },
	 
	 Nodes:
	 {
	 	// Move um conjunto de n�s para cima.
	 	moveUp: function()
	 	{	
	 		  var checkedNodes = TreeData.getCheckedNodes();
	 		  
	 		  TreeData.moveNodeUp(checkedNodes);
	 	},
	 	
	 	// Move um conjunto de n�s para baixo.
	 	moveDown: function()
	 	{
	 		 var checkedNodes = TreeData.getCheckedNodes();
	 		  	
	 		 TreeData.moveNodeDown(checkedNodes);
	 	},
	 	
	 	// Remove um conjunto de n�s.
	 	remove: function()
	 	{
	 		var checkedNodes = TreeData.getCheckedNodes();
	 		TreeData.deleteNode(checkedNodes);
	 	}
	 }
	  
};