<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
--%>

var ScanUtils = {
	
	 // Desfaz altera��es na imagem atual.
	 undoImageChanges: function(canvasRef)
	 {
	 	var canvas = (canvasRef ? document.getElementById(canvasRef) : document.getElementById(Scan.getCurrentViewingCanvasId()));
	 	if (canvas)
	 	{
	 		var ctx = canvas.getContext('2d');
	 		
	 		var canvasId = canvas.id;
	 		var lastPt = canvasId.lastIndexOf('.');
	 		var left = canvasId.substring(0, lastPt);
	 		var right = canvasId.substring(lastPt, canvasId.length);
	 		var fileName = left + '_base' + right;
	 		
	 		CanvasManager.drawImage(canvasId, Scan.tempFolder + fileName, true);
	 	}
	 },
	 
	 // Desfaz altera��es em todas as imagens
	 undoAllChanges: function()
	 {
	 	var changedCanvas = $('canvas[changed=true]');
	 	changedCanvas.each(function() {
	 		ScanUtils.undoImageChanges($(this).attr('id'));
	 	});
	 },
	 
	 // Seta changed true/false em um Canvas
	 // Controla o �cone da tree relativa a ele
	 setCanvasStatus: function(canvas, status)
	 {
	 	$(canvas).attr('changed', status);
	 	if (status == 'true')
	 	{
	 		TreeData.setNodeType(canvas.id, 'default_red');
	 	}
	 	else if (status == 'false')
	 	{
	 		TreeData.setNodeType(canvas.id, 'default');
	 	}
	 },
	 
    runFunction: function(name, arguments)
	{
		var fn;
   	 	var p = NEO.neoUtils.returnParent();
   	 	if(p)
   	 		fn = p[name];
   	 	else if(parent)	
   	 		fn = parent[name];	

   		if(typeof fn !== 'function')
        	return;

    	fn.apply(window, arguments);
	},
	 
};