<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=UTF-8"
	pageEncoding="UTF-8"%>

var ScanCache = {

	DocumentCache:
	{
		cache: new Array(),
		
		put: function(id, value)
		{
			this.cache[id] = value;
		},
		get: function(id)
		{
			return this.cache[id];
		}
	},
	 
}