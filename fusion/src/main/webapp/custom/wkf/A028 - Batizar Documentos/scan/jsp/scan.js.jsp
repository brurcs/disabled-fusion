<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%--
@f�bio
Main do Digitalizador.
--%>
var Scan = {
	
	baseURL: "",
	showUI: "",
	blackAndWhite: "",
	resolution: "",
	enabled: true,
	triggerResize: true,
	containerId: "div-scan-pages",
	tempFolder: "",
	applet: null,
	layoutWrapper: 'scan_layout_wrapper',
	currentViewingCanvasId: null,
	callback: null,
	baptism: false,
	currentUserCode: "",
	deviceName: "",
	
	Data: {
		data: null,
	},
	
	Locale:
	{
		country: 'BR',
		language: 'pt'
	},
	
	Pages: {
		current: 0,
		total: 0,
		
		// Clica no n� (p) da tree.
		setCurrentPage: function(p)
		{
			this.getContainer().jstree("get_checked", null, false);
		},
	},
	
	init: function(defaultValues)
	{
		Scan.baseURL = defaultValues.baseURL;
		Scan.currentUserCode = defaultValues.currentUserCode;
		Scan.tempFolder = '';
		Scan.callback = defaultValues.callback;
		Scan.inputId = defaultValues.inputId;
		Scan.baptism = defaultValues.baptism;
		Scan.showUI = defaultValues.showUI;
		Scan.blackAndWhite = defaultValues.blackAndWhite;
		Scan.resolution = defaultValues.resolution;
		Scan.localeCountry = defaultValues.localeCountry;
		Scan.localeLanguage = defaultValues.localeLanguage;
		
		// Inicializa janelas internas
		ScanWindows.ResizeDialog.applyResizeDialog();
		ScanWindows.BrightnessDialog.applyBrightnessDialog();
		ScanWindows.SettingsDialog.applySettingsDialog();
		ScanWindows.RestoreDialog.applyRestoreDialog();
		//ScanWindows.ImportDialog.applyImportDialog();
		
		// Abre o menu
		Menu.toggle('scan_layout_left', document.getElementById('a-showhide-button'));
		
		Scan.Page.updateBounds();
		
		// Insere a applet e bloqueia a tela, aguardando o callback dela pra desbloquear.
		ScanStatus.showAppletLoadingMessage(Scan.layoutWrapper, '<%=I18nUtils.getString("startingScanner")%>...', function() 
		{
			applet = ScanApplet.insertApplet('scan_applet');
			setTimeout(function() {
				ScanApplet.checkLoadedError();
			}, ScanApplet.loadTimeout);
		});
		
		if (Scan.baptism == 'false')
		{
			Toolbar.toggleBaptismButton('hide');
		}
			
	},
	
	doScanImpl: function()
	{
		files = ScanApplet.callAcquire(applet, Scan.deviceName);
		if (files && files.length)
		{
			// Realiza convers�o do TIFF
			setTimeout(function() {
				ScanStatus.showConversionMessage(Scan.layoutWrapper, 'Fusion ECM Suite', Scan.doConvertImpl);
			}, 100);
		}
		else
		{
			alert('Nenhum arquivo digitalizado!');
		}

		ScanStatus.hideStatusMessage(Scan.layoutWrapper);
	},
	
	doConvertImpl: function()
	{
		// Esconde o container para n�o desalinhar o HTML.
		TreeData.getContainerUL().hide();
						
		var convertedFiles = applet.breakMultiPartTiff(files);
		ScanStatus.hideStatusMessage(Scan.layoutWrapper, function() 
		{
			ScanStatus.showTreeMessage(Scan.layoutWrapper, '<%=I18nUtils.getString("loading")%>', function() 
			{
				TreeData.buildTreeHTML(convertedFiles);
				ScanStatus.hideStatusMessage(Scan.layoutWrapper);
				Sidebar.updateBounds();
			});
		});
		
	},
	
	doScan: function()
	{
		// Applet carregada
		if (ScanApplet.checkLoaded('scan_applet') == true)
		{
			Scan.applet = applet;
			
			var hasDevice = applet.hasDefaultDevice();
			
			if (Scan.tempFolder == '')
			{
				Scan.tempFolder = applet.getTempFolder();
			}
				
			// Verifica se existe dispositivo padr�o cadastrado. Caso n�o exista, solicita o preenchimento ao usu�rio.
			if (hasDevice == false || hasDevice == 'false')
			{
				alert('<%=I18nUtils.getString("scanDeviceNotFound")%>');
				ScanWindows.SettingsDialog.show();
			}
			else
			{
				if (Scan.deviceName == '')
					Scan.deviceName = ScanApplet.loadDefaultDevice();
					
				// Exibe a mensagem de status e, ap�s callback, chama o scanner.
				ScanStatus.showScanningMessage(Scan.layoutWrapper, Scan.deviceName, Scan.doScanImpl);
			}
		}
		else alert('<%=I18nUtils.getString("scannerComponentsFail")%>');
	},
	
	Page:
	{
		repaint : function()
		{
			if (Scan.triggerResize)
				Scan.Page.updateBounds();
		},
		
		updateBounds : function()
		{
			var $body = $("body"); 
			var $c = Scan.getContainer();
			$c.height($body.height() - $c.offset().top);
			$c.width($body.width() -  $c.offset().left);
		},
	},
	
	getContainer : function()
	{
		return $("#" + this.containerId);
	},
	
	setCurrentViewingCanvasId: function(id)
	{
		Scan.currentViewingCanvasId = id;
	},
	
	getCurrentViewingCanvasId: function()
	{
		return Scan.currentViewingCanvasId;
	},
	
	showErrorPanel: function()
	{
		$('#'+Scan.layoutWrapper).html('<span class="span-nodoc-digitalized"><%=I18nUtils.getString("scannerComponentsFail")%></span>');
	}
};

// Evento de resize da janela
if( window ){
	$(window).bind('resize', function() {
		Scan.Page.repaint();
	});
};