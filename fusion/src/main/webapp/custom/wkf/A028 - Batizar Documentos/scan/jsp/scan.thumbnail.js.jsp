<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>

var Thumb = {
	thumbContainer: "thumb_container",
	triggerResize: true,
	
	init: function()
	{
	
	},
	getThumbContainer : function()
	{
		return $("#" + this.thumbContainer);
	},
	getPageURL : function(element)
	{
		Viewer.Data.getPageURL(element);
		element.attr("data-src", element.attr("data-src")+ "&thumbs");
	},
	// Constroi a lista.
	construct : function(fromPage)
	{
		this.buildThumbs();
	
	},
	// Constroi o HTML das thumbnails -> [Documentos]->[Arquivos]->[Imagens]
	buildThumbs : function()
	{

	},
	lazy : function()
	{
		this.getThumbContainer().find(".thumb_img").lazy({ 
			bind: "event",
			container: '#'+this.thumbContainer, 
			threshold : 0,
			beforeLoad: function(element) 
			{
				Thumb.getPageURL(element);
        	},
        	afterLoad: function(element)
        	{
        		element.addClass('thumb_shadowed');
        	}
		});
		
		
	},
	selected : function(event, img)
	{
		if (event != null && event != undefined) // nao propaga clique pra div pai
		event.stopPropagation();
		 
		$('.thumb_img').removeClass('thumb_selected');
		$(img).addClass('thumb_selected');
		
		Navigation.page($(img).attr('page'));
	},
	select: function(page)
	{
		 // Controla o fundo azul com sombra
		$('.thumb_img').removeClass('thumb_selected');
	    $('.thumb_img').eq(page-1).addClass('thumb_selected');
	    
		
		var thumbId = $('.thumb_img').eq(page-1).attr('id');
		var elemOffsetTop = document.getElementById(thumbId).offsetTop;
		
		if (elemOffsetTop == 0)
		{	
			// Timeout pra esperar a DOM carregar caso retorne 0
			setTimeout(function() 
			{
				elemOffsetTop = document.getElementById(thumbId).offsetTop;
				if (!Thumb.isScrolledIntoView(document.getElementById(thumbId)))
				{
					document.getElementById('thumb_container').scrollTop = elemOffsetTop-50;
				}
			},250);
		}
		
		if (!Thumb.isScrolledIntoView(document.getElementById(thumbId)))
		{
		document.getElementById('thumb_container').scrollTop = elemOffsetTop-50;
		}
		
	},
	toggleNode: function(node, evt)
	{
		$(node).children('div').slideToggle(400);
		
		var img = $(node).children('span').children('img');
		if (img.length)
		{
			if (img.hasClass('node_image_open'))
			{
				img.removeClass('node_image_open');
				img.addClass('node_image_closed');
				img.attr('src', Viewer.baseURL + 'imagens/icones_final/node_closed.png');
			}
			else if (img.hasClass('node_image_closed'))
			{
				img.removeClass('node_image_closed');
				img.addClass('node_image_open');
				img.attr('src', Viewer.baseURL + 'imagens/icones_final/node_open.png');
			}
		}
		
		evt.stopPropagation();
	},
	registerSelectedEvent : function (f)
	{
		$("body").bind("ThumbSelectedEvent", f);
	},
	triggerSelectedEvent : function (data)
	{
		$("body").trigger("ThumbSelectedEvent", data);
	},
	// Verifica se o elemento est� vis�vel dentro do scroll
	isScrolledIntoView: function (elem)
	{
    	var docViewTop = $(window).scrollTop();
    	var docViewBottom = docViewTop + $(window).height();

    	var elemTop = $(elem).offset().top;
    	var elemBottom = elemTop + $(elem).height();

    	return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom)
      	&& (elemBottom <= docViewBottom) &&  (elemTop >= docViewTop) );
	}
};

$(document).ready(function() {
	Thumb.init();
});
