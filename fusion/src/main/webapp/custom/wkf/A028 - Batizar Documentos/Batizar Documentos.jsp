<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@page import="com.neomind.fusion.workflow.service.TaskServiceImpl"%>
<%@page import="com.neomind.fusion.engine.runtime.RuntimeEngine"%>
<%@page import="com.neomind.fusion.workflow.TaskInstanceHelper"%>
<%@page import="com.neomind.util.TaskUtils"%>
<%@page import="com.neomind.fusion.eform.actions.EntityActionFactoryPortletActions"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.workflow.task.central.model.TaskMessageModel"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.neomind.fusion.workflow.TaskStatus"%>
<%@page import="com.neomind.fusion.entity.action.UrlAction"%>
<%@page import="com.neomind.fusion.entity.EntityAction"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedSet"%>
<%@page import="com.neomind.fusion.workflow.TaskInstance"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.doc.Approval"%>
<%@page
	import="com.neomind.fusion.eform.actions.EntityActionPortletButtonInterface"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.entity.EntityInfo"%>
<%@page import="com.neomind.fusion.workflow.ProcessAttachment"%>
<%@page import="com.neomind.fusion.workflow.xpdl.XPDLProcessModel"%>
<%@page import="com.neomind.fusion.portal.NeoPortletURL"%>
<%@page import="com.neomind.util.CustomFlags"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.util.ReflectionUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page
	import="com.neomind.fusion.eform.actions.EntityActionFactoryPortlet"%>
<%@page
	import="com.neomind.fusion.portal.taglib.webui.FusionMenuItemTag"%>
<%@page
	import="com.neomind.fusion.eform.actions.EntityActionFactoryParamsTask"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>

<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/menu.tld" prefix="m"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<portlet:defineObjects />
<portal:head title="" >

<div id="task_wrapper" class="task_wrapper" onclick="top.window.sharedMenuData && top.window.sharedMenuData.closeExpandedMenu(event);">
<link rel="stylesheet" type="text/css" href="css/menu_icons.css">
<link rel="stylesheet" type="text/css" href="css/task.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-grid-table.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-alert.css">

<style type="text/css">
div.area_scroll {border-left: none !important; border-right: none !important; border-bottom: none !important; padding: 0px !important;}
body {padding: 0}
.dropdown_actions ul li a:hover {
	background: none;
	background-color: transparent;
	color: #687488;
}

</style>

	<%

	boolean ok = true;

	String defaultAction="";
	NeoObject obj = (NeoObject) renderRequest.getAttribute("entity");
	if(obj == null) {
		String entityid = request.getParameter("entity");
		if(entityid != null) {
			obj = PersistEngine.getNeoObject(Long.valueOf(entityid));
		}
	}
	String taskInstanceId = request.getParameter("taskInstanceId");
	boolean showSummary = false;
	boolean showMarkers = false;
	boolean showTogglerBtn = false;
	boolean showNotesBtn = false;
	String notes = "";
	Boolean showAlert = ((String) request.getParameter("showAlert") != null ? (Boolean.valueOf((String) request.getParameter("showAlert"))) : false);
	Boolean outbox = ((String) request.getParameter("outbox") != null ? (Boolean.valueOf((String) request.getParameter("outbox"))) : false);
	String alertTxt = (String) request.getParameter("alertMsg");
	Boolean openSummary = ((String) request.getParameter("openSummary") != null ? (Boolean.valueOf((String) request.getParameter("openSummary"))) : false);
	String title = "";
	
	TaskInstance taskInstance = null;
	TaskServiceImpl taskService = (TaskServiceImpl) RuntimeEngine.getTaskService();
	
	if(taskInstanceId != null && !taskInstanceId.isEmpty())
	{
		taskInstance = PersistEngine.getObject(TaskInstance.class, Long.valueOf(taskInstanceId));
	}
	if(taskInstance != null)
	{
		
		if (taskInstance.getStatus() == TaskStatus.READY)
			obj = taskInstance.getActivity();
		else if (taskInstance.getStatus() == TaskStatus.RESERVED || taskInstance.getStatus() == TaskStatus.COMPLETED)
			obj = taskInstance.getTask();
		
		showSummary = (taskInstance.getSummaryFields() != null && !taskInstance.getSummaryFields().isEmpty());
		showMarkers = (taskInstance.getTags() != null && !taskInstance.getTags().isEmpty());
		showTogglerBtn = (showSummary == true || showMarkers == true);
		
		if (obj instanceof Task)
			showNotesBtn = ((taskInstance.getNotes() != null && !taskInstance.getNotes().isEmpty()) || ( ((Task) obj).getReasonToBackX() != null && !((Task) obj).getReasonToBackX().isEmpty() ));
		else
			showNotesBtn = (taskInstance.getNotes() != null && !taskInstance.getNotes().isEmpty());
		
		// Papeletas
		if (showNotesBtn)
		{
			if (((Task) obj).getReasonToBackX() != null && !((Task) obj).getReasonToBackX().isEmpty())
			{
				notes = ((Task) obj).getReasonToBackX();
			}	
				if (taskInstance.getNotes() != null && !taskInstance.getNotes().isEmpty())
				notes = notes + "<br><br>" + TaskUtils.notesToHtmlString(taskInstance.getNotes());
		}
		
	}
	%>

	<c:choose>
		<c:when test="<%=obj != null %>">
		<%
			NeoUser user = PortalUtil.getCurrentUser();
	
			Integer posicaoAbaDados = 1;
			
			Long modelId = null;
			ProcessModel model = null;
			NeoObject entity = null;

			String portletName = "TaskContent";
	
			boolean prevTaskAlreadyRead = true;
			boolean isDueDateChangeable = false;
	
			String callBackURL = request.getParameter("callBackURL");
			Long id = obj.getNeoId();
	
			Long processId = null;
			Long activityId = null;
			Long taskId = null;
			
			Boolean edit = null;
			if (request.getAttribute("edit") != null)
			{
				edit = Boolean.valueOf(request.getAttribute("edit").toString());
			}
			else
			{
				if(request.getParameter("edit") == null)
				{
					edit = true;
				}
				else 
				{
					edit = Boolean.valueOf(request.getParameter("edit"));
				}
			}
			
			// FIXME - Realizar implementa��es de seguran�a para o objeto task tamb�m!
			if(taskInstance != null)
			{
				if(edit)
					ok = taskService.canUpdate(taskInstance, user);
				else
					ok =  taskService.canRead(taskInstance, user);
				
				// Caixa de sa�da
				if(taskInstance.getStatus() == TaskStatus.COMPLETED)
					edit = false;
			}

			String isEditable = edit.toString();
			String showToolbar = "true";
			if (isEditable == null)
			{
				isEditable = "true";
			}
						
			Boolean readOnly = NeoUtils.safeBoolean(request.getParameter("readOnly"), false);			
			
			WFProcess process = null;
	
			// bruno.manzo, 27/04/2011: novo parametro pra validar a renderiza��o do bot�o
			// assumir atividade, se passar o edit como true a edi��o de campos fica liberada
			String view = isEditable;
			
			// Permiss�o para recusar tarefa
			boolean back = true;
			
			// Raz�o da recusa.
			String reasonToBack = null;

			// Se for edi��o, deve validar:
			//	no caso de tarefa, se � o dono, se � gestor ou se � atribui��o tempor�ria
			//  no caso de atividade, se est� no pool, se � gestor ou se � atribui��o tempor�ria
			// Se for visualzia��o
			//  se o processo � publico, ou se for privado se for o executor, gestor ou usu�rio consulta

			if (obj instanceof Task)
			{
				Task task = (Task) obj;
				modelId = task.getActivity().getProcess().getNeoId();
				model = task.getActivity().getProcess().getModel();
				entity = task.getProcessEntity();
				process = ((Task) obj).getProcess();
				processId = process.getNeoId();
				activityId = ((Task) obj).getActivity().getNeoId();
				taskId = ((Task) obj).getNeoId();
				isDueDateChangeable = task.getActivity().getTerm().isChangeableByUser();
				
				if ((task.getActivity() instanceof UserActivity && !((UserActivity) task.getActivity()).isCanReturn()) || readOnly)
					back = false;
				
				if(taskInstance == null)
					taskInstance = task.getInstance();
				//reasonToBackNotification = task.getReasonToBackNotification();
			}
			else if (obj instanceof Activity)
			{
				Activity activity = (Activity)obj;
				modelId = activity.getProcess().getNeoId();
				model = activity.getProcess().getModel();
				
				entity = activity.getProcessEntity();
				
				portletName = "ActivityContent";
	
				if (obj instanceof UserActivity)
					process = ((UserActivity) obj).getProcess();
				else
					process = ((Activity) obj).getProcess();
				
				processId = process.getNeoId();
				activityId = ((Activity) obj).getNeoId();
				
				isEditable = "false";
				showToolbar = "false";
				isDueDateChangeable = activity.getTerm().isChangeableByUser();

				back = false;
				
				if(taskInstance == null)
					taskInstance = activity.getInstance();
			}

			// Quando inicia o processo essa informacao nao esta persistida, por�m o correto � encontrar esse objeto da sess�o.
			// Performance
			if(taskInstance != null)
			{
				title = taskInstance.getTaskName() + " - " + taskInstance.getTitle();
			}
			if (obj instanceof Task)
			{
				title = ((Task)obj).getFullName();
			}
			else if (obj instanceof Activity)
			{
				if(obj instanceof UserActivity)
					title = ((UserActivity) obj).getFullName();
				else
					title = ((Activity)obj).getFullName();
			}

			boolean canExport = model.getMainAttachmentConfig() != null && model.getMainAttachmentConfig().isExportDataProcess();
			
			EntityInfo info = EntityRegister.getInstance().getCache().getByClass(obj.getClass());
	
			boolean head = NeoUtils.safeBoolean(request.getParameter("head") != null ? request.getParameter("head") : "true");
	
			Map<String, String[]> params = new HashMap<String, String[]>();
			params.put("type", new String[]	{ entity.getInfo().getTypeName() });
			params.put("view", new String[] { view });
			params.put("id", new String[] { Long.toString(entity.getNeoId()) });
			
			// Par�metros referente a tarefa, atividade e processo
			params.put("process_id", new String[] { Long.toString(processId) });
			params.put("activity_id", new String[] { Long.toString(activityId) });
			if(taskId != null)
				params.put("task_id", new String[] { Long.toString(taskId) });
			
			params.put("callBackURL", new String[] { callBackURL });
			params.put("edit", new String[] { isEditable });
			params.put("select", new String[] { Boolean.toString(true) });
			params.put("root", new String[] { Boolean.toString(true) });
			params.put("form", new String[] { Boolean.toString(true) });
			params.put("full", new String[] { Boolean.toString(true) });
			params.put("mutable", new String[] { Boolean.toString(false) });
			params.put("toolbar", new String[] { showToolbar });
			params.put("head", new String[]	{ Boolean.toString(head) });
			params.put("showContainer", new String[] { Boolean.toString(false) });
			params.put("win", new String[] { (String) renderRequest.getParameter("win") });
			if (((String) renderRequest.getParameter("idContainer")) != null)
				params.put("idContainer", new String[]{ (String) renderRequest.getParameter("idContainer") });

			if (((String) renderRequest.getParameter("showAttachment")) != null)
				params.put("showAttachment", new String[] { (String) renderRequest.getParameter("showAttachment") });
			params.put("readOnly", new String[] { Boolean.toString(readOnly) });
			params.put("outbox", new String[] { Boolean.toString(outbox) });
	
			Map<String, String[]> paramsToProcess = new HashMap<String, String[]>();
			paramsToProcess.putAll(params);
			paramsToProcess.put("imgprocess", new String[] { Boolean.toString(true) });
			
			
			// Anexo Principal
			ProcessAttachment attach = process.getMainAttachment();
			boolean hasAttach = attach != null && attach.getRootFolder() != null
						&& (attach.getRootFolder().getChildren().size() > 0 || attach.getRootFolder().getDocuments().size() > 0)
						&& !("false".equalsIgnoreCase(request.getParameter("showAttachment")));
			
			
			// Documenta��o da Atividade
			String instructions = null;
			if(obj instanceof Task)
				instructions = NeoUtils.encodeJS(((Task)obj).getActivity().getModel().getDescription(), true);
			else if(obj instanceof Activity)
				instructions = NeoUtils.encodeJS(((Activity)obj).getModel().getDescription(), true);
			
			
			// Transfer�ncia de tarefa
			boolean transfer = (!CustomFlags.NEOMIND && !CustomFlags.SALFER && !CustomFlags.FBB) 
				&& (obj instanceof Task) 
				&& ((Task)obj).getProcess().isSaved()
				&& !(entity instanceof Approval);
			
			// Processos Relacionados
			boolean related =  (obj instanceof Task)
				&& !((XPDLProcessModel)((Task) obj).getProcess().getModel()).getRelatedProcesses().isEmpty() 
				&& ((Task)obj).getProcess().isSaved();
			
			String nd = new String();
			// willian.mews: Permitir visualizar os documentos de processo de distribui��o
			// na tarefa
			StringBuilder loadScript = new StringBuilder();
			
			if (CustomFlags.BOMFUTURO)
			{
				String processName = process.getModel().getName();			
				if (obj instanceof Task){
					Task task = (Task)obj;
		
					if (processName.equals("Fluxo de Distribui��o de Documentos") 
							&& task.getActivity().getName().equals("Pend�ncia de Leitura"))
					{
						EntityWrapper wrapper = new EntityWrapper(process.getEntity());
						NeoDocument documento = wrapper.findGenericValue("documento");
						if (documento != null){
							//nd = nd.concat(String.valueOf(documento.getNeoId()));
							
							//cria script para selecionar a aba de documentos e trazer ela carregada
							//para garantir que o usu�rio visualize o documento.
							loadScript.append("<script type='text/javascript'>");
							loadScript.append("	loadContent(document.getElementById(\'area_scroll\'), 'portal/render/JSP?jsp=/flex/menuDocs.jsp?docs=" + nd + "');");							
							loadScript.append("</script>");
							
						}
					}
				}
			}
	%>

			<cw:main>
				<div id='task_alerts'></div>
				<cw:header title='<%=title%>' />
				<%
					if(CustomFlags.GOLDEN)
					{
				%>
					<script>$('#headerTitle').addClass('title_04');</script>
				<%
					}
				%>
				
				<c:choose>
				<c:when test="<%=(showTogglerBtn == false) %>">
					<script>$('.task_summary_btn_div').hide();</script>
				</c:when>
				<c:otherwise>
					<script>$('.task_summary_btn_div').show();</script>
				</c:otherwise>
				</c:choose>
				
				
				<c:if test="<%= ok %>">
				
				<!-- Marcadores da Tarefa -->
				<cw:markers taskId="<%=taskInstanceId != null ? taskInstanceId : \"-1\" %>"></cw:markers>
				<!-- Resumo da Tarefa -->
				<cw:summary taskId="<%=taskInstanceId != null ? taskInstanceId : \"-1\" %>"></cw:summary>
				<cw:menu>
					<m:fusionMenu label="menubardefaultlv1" css="menu_bar_default_lv1_task">
					<%
						params.put("activeFunction", new String[] { posicaoAbaDados.toString() });
					%>						
						<c:if test="<%=related%>">
							<script>
								function refreshRelatedWindow()
								{
									<%="loadContent(document.getElementById('related'), '"+ NeoPortletURL.buildAjaxPortletURL("TaskRelatedProcess", params)+"');"%>
									
									$('#related').show();
									$('#customDiv').hide();
									$('#formContent').hide();
									$('#history').hide();
									$('#flowDiv').hide();
									$('#processDoc').hide();	
								}
						
							</script>							
						</c:if>

						<%						
												
						///////////hugo.tsuda: parametros para valida��o dos botoes//////////////////////////
						EntityActionFactoryParamsTask paramsTaskActivity = new EntityActionFactoryParamsTask();
						paramsTaskActivity.setNeoId(Long.toString(obj.getNeoId()));
						paramsTaskActivity.setExport(canExport);
						paramsTaskActivity.setHistory(true);
						paramsTaskActivity.setTransfer(transfer);
						paramsTaskActivity.setInstructions(instructions);
						paramsTaskActivity.setDuedate(isDueDateChangeable);		
						paramsTaskActivity.setInfo(EntityRegister.getInstance().getCache().getByClass(obj.getClass()));
						paramsTaskActivity.setAttach(hasAttach);
						paramsTaskActivity.setBack(back);								
					
						EntityActionFactoryPortlet entityActionFactoryPortlet = null;
						
						if (obj instanceof Task)
						{
							Task task = (Task) obj;
							if(!((XPDLProcessModel) task.getProcess().getModel()).getRelatedProcesses().isEmpty())
								paramsTaskActivity.setRelatedProcessesEmpty(false);
							else
								paramsTaskActivity.setRelatedProcessesEmpty(true);
							entityActionFactoryPortlet = new EntityActionFactoryPortlet(task,paramsTaskActivity);
						}
						else if (obj instanceof Activity)	
						{
							Activity activity = (Activity)obj;
							entityActionFactoryPortlet = new EntityActionFactoryPortlet(activity,paramsTaskActivity);
						}	
						//printa o html dos bot�es
						out.println(entityActionFactoryPortlet.createButtons());
						//verifica se tem alguma aba/bot�o padr�o
						if(entityActionFactoryPortlet.getDefaultAction()!=null)
							defaultAction= entityActionFactoryPortlet.getDefaultAction();	
						
						%>							
						<!-- Papeletas -->
						<c:if test="<%=showNotesBtn%>">
						<li title='<%=I18nUtils.getString("notes") %>' id="notes" class="dropdown_notes pullRight" onclick="toggleSub(this, event);">
							<a href="javascript: nop();">
								<div style="float: left;">
									<i class="icon-info" style="float: left; margin: 0"></i>
								</div>
							</a>
							<ul class='dropdown_notes'>
							<li class="header">
							<p style="float: left;">
								Informa��o
							</p>
							<p style="float: right;">
								<i class="icon-times" title='<%=I18nUtils.getString("removeAll")%>' onclick="deleteAllNotes('<%=taskInstanceId%>', event);"></i>
							</p>
							</li>
							<li style="vertical-align: baseline;"><%=notes %></li>
							</ul>
						</li>
						</c:if>
						
						<!-- Instru��es -->
						<c:if test="<%=instructions != null && !instructions.isEmpty()%>">
						<li title='<%=I18nUtils.getString("instructions") %>' id="instructions" class="dropdown_instructions pullRight" onclick="toggleSub(this, event);">
							<a href="javascript: nop();">
								<div style="float: left;">
									<i class="icon-uniE600" style="float: left; margin: 0;"></i>
								</div>
							</a>
							<ul class='dropdown_instructions'>
							<li class="header">Instru��es</li>
							<li>
								<%=StringEscapeUtils.unescapeJava(instructions)%>
							</li>
							</ul>
						</li>
						</c:if>
						
						<!-- Menu customizado de A��es -->
						<%
						if (obj instanceof Task)
						{
							Task task = (Task) obj;
							entityActionFactoryPortlet = new EntityActionFactoryPortletActions(task,paramsTaskActivity);
						}
						else if (obj instanceof Activity)	
						{
							Activity activity = (Activity)obj;
							entityActionFactoryPortlet = new EntityActionFactoryPortletActions(activity,paramsTaskActivity);
						}		
						String popupItems = entityActionFactoryPortlet.createButtons();
						%>
						<c:if test="<%=!popupItems.isEmpty() && edit && !outbox %>">
						<li title='<%=I18nUtils.getString("actions") %>' id="actions" class="dropdown_actions pullRight" onclick="toggleSub(this, event);">
							<a href="javascript: nop();">
								<div style="float: left; margin-right: 5px; margin-left: 5px;">
									<%= I18nUtils.getString("actionset") %>
								</div>
								<i class="icon-arrow-down3" style="float: left;"></i>
							</a>
							<ul id='dropdown_actions' class='dropdown_actions' onclick="this.style.display='none'">
							<%=popupItems%>
							</ul>
						</li>
						</c:if>
					</m:fusionMenu>
				</cw:menu>
				</c:if>
				<cw:body id="area_scroll">
				
				<c:choose>
				<c:when test="<%= !ok %>">
						<div class='alert alert-danger alert-dismissable'>
							<%= (edit) ? I18nUtils.getString("nopermissiontorunthistask") 
									: I18nUtils.getString("nopermissiontoseethistask") %>
						</div>
				</c:when>
				<c:otherwise>

					<div>
						<input type="button" class="input_button" title="Realizar Batismo de Documentos" name="Realizar Batismo de Documentos" value="Realizar Batismo de Documentos" onclick="openBatismo();">
					</div>
					<div id="formContent" style="display: block; height: 100%; background: #fff;"></div>
					<div id="related" style="display: none; height: 100%;"></div>
					<div id="history" style="display: none; height: 100%;"></div>
					<div id="flowDiv" style="display: none; height: 100%;"></div>
					<div id="processDoc" style="display: none; height: 100%;"></div>
					<div id="customDiv" class="task_tab_custom" style="display: none; height: 100%; overflow: scroll;"></div>
					
					<script type="text/javascript">
					
				
				$(function() {
						$('#actions').click(function() {
							if (<%=instructions != null && !instructions.isEmpty() %>)
							{
								var actionsEl = document.getElementById('instructions');
								toggleSub(actionsEl, null, true);						
							}
						});
				});
				$(function() {
					$('#instructions').click(function() {
						if (<%=instructions != null && !instructions.isEmpty() %>)
						{
							var actionsEl = document.getElementById('actions');
							toggleSub(actionsEl, null, true);						
						}
					});
				});
					
				$(function() {
					$('body').click(function() {
						closeInnerDialogs();
					});
					
					// As papeletas j� aparecem vis�veis
					if (<%=showNotesBtn%>)
					{
						var notesEl = document.getElementById('notes');
							toggleSub(notesEl, null, false);	
					}
					<%--
					console.log('click');
					if (<%=instructions != null && !instructions.isEmpty() %>)
					{
						var actionsEl = document.getElementById('actions');
						toggleSub(actionsEl, null, false);						
					}
					
					if (<%=edit %>)
					{
						var instructionsEl = document.getElementById('instructions');
						toggleSub(instructionsEl, null, false);
						console.log('toggleInstructions');
					}--%>
					
					if (<%=showAlert%>)
					{
						showTaskAlert('<%=alertTxt%>', false, true,'success');
					}
					
					if (<%=openSummary%>)
					{
						toggleSummary();
						toggleMarkers();
					}
					
					// Alerta da Task aberta em nova janela
					
				});
					<%--
				function refreshWindow()
				{
					<%= "loadContent(document.getElementById('related'), '"+ NeoPortletURL.buildAjaxPortletURL(portletName, params)+"');" %>							
				}--%>
				
				function openBatismo()
				{

					window.open('<%= PortalUtil.getBaseURL() +  "custom/wkf/A028 - Batizar Documentos/tree.jsp?id="+entity.getNeoId()%> ');	
				}
				
				function toggleSummary()
				{
					var showSummary = <%=showSummary%>;
					var summaryEl = document.getElementById('cwsummarytag_td_<%=taskInstanceId%>');
					var summaryElTr = document.getElementById('cwsummarytag_<%=taskInstanceId%>');
					if (summaryEl)
					{
						var browserDif = 0;
						if ($.browser.msie)
							browserDif = 1;
						
						if (!showSummary)
							return;
						
						if (summaryEl.style.display != 'block')
						{
							summaryEl.style.display='block';
							summaryElTr.style.display = 'block';	
						}
						else
						{
							summaryEl.style.display='none';
							summaryElTr.style.display = 'none';
						}
						
						setTimeout(function() {
							doScroll('window_default');
						}, 500);
					}
				}
				
				function toggleMarkers()
				{
					var showMarkers = <%=showMarkers%>;
					var markersEl = document.getElementById('cwmarkers_td_<%=taskInstanceId%>');
					var markersElTr = document.getElementById('cwmarkers_<%=taskInstanceId%>');
					
					if (!showMarkers)
						return;
					
					if (markersEl)
					{
						var browserDif = 0;
						if ($.browser.msie)
							browserDif = 1;
						
						if (markersEl.style.display != 'block')
						{
							markersEl.style.display='block';
							markersElTr.style.display='block';
						}
						else
						{
							markersEl.style.display='none';
							markersElTr.style.display='none';
						}
						
						setTimeout(function() {
							doScroll('window_default');
						}, 500);
					}
				}
				
				function loadCustomDiv(url)
				{
					var urlFinal = url + '<%= NeoPortletURL.getMapAsString(params) %>';
					
					loadContent(document.getElementById('customDiv'), urlFinal);
					
					$('#customDiv').show();
					$('#formContent').hide();
					$('#history').hide();
					$('#flowDiv').hide();
					$('#processDoc').hide();
					$('#related').hide();
				}
				
				function loadFormContent()
				{
					if(document.getElementById('formContent').innerHTML == '') {						
						loadContent(document.getElementById('formContent'), '<%= NeoPortletURL.buildAjaxPortletURL(portletName, params) %>',
								function() {
							NEO.neoUtils.getRoot().$('body').unblock();
						}, null, false);
						
						if ($('#data').length)
							$('#data').parent('li').addClass('_active'); // torna ativo por padr�o
					}
					
					$('#formContent').show();
					$('#history').hide();
					$('#flowDiv').hide();
					$('#processDoc').hide();
					$('#customDiv').hide();
					$('#related').hide();
				}
				
				function loadHistory()
				{
					if(document.getElementById('history').innerHTML == '')
						loadContent(document.getElementById('history'), '<%= NeoPortletURL.buildPortletURL("ProcessReport", params) %>');
						
					$('#formContent').hide();
					$('#history').show();
					$('#flowDiv').hide();
					$('#processDoc').hide();
					$('#customDiv').hide();
					$('#related').hide();
				}
				
				function loadFlow()
				{
					if(document.getElementById('flowDiv').innerHTML == '')
						loadContent(document.getElementById('flowDiv'), '<%= NeoPortletURL.buildAjaxPortletURL("ProcessReport", paramsToProcess) %>');
					
					$('#formContent').hide();
					$('#history').hide();
					$('#flowDiv').show();
					$('#processDoc').hide();
					$('#customDiv').hide();
					$('#related').hide();
				}
				
				function loadProcessDoc() 
				{
					var url = '<%=PortalUtil.getBaseURL()%>portal/render/Viewer?processId=<%= processId %>';
					
					loadContent(document.getElementById('processDoc'), url, 
							function() {NEO.neoUtils.getRoot().$('body').unblock();}, null, false);
					
					$('#formContent').hide();
					$('#history').hide();
					$('#flowDiv').hide();
					$('#processDoc').show();
					$('#customDiv').hide();
					$('#related').hide();
					
					//TODO parametrizar 
					/*
					NEO.neoUtils.dialog().addModal(true,null,null,null,null,'<%= I18nUtils.getString("viewer")%>');
					NEO.neoUtils.dialog().createModal(url);
					*/
				}
				
				function printIFrame()
				{
					$("#area_scroll").attr("class", ""); // altera o nome da class do area_scroll para remover a barra de rolagem,
					NEO.neoUtils.returnParent().frames['rightCol'].focus(); // assim o IFrame � impresso por inteiro
					NEO.neoUtils.returnParent().frames['rightCol'].print(); // chama a fun��o print
					$("#area_scroll").attr("class", "area_scroll"); // devolve a barra de rolagem
				}


				
				$(document).ready(function () {
				<!--Variavel que tem o chamada da fun��o do aba/botao padr�o escolhida no Custom do Cliente-->				
				<%
				if(defaultAction!="")
					out.println(defaultAction.replace("javascript:", ""));
				else
					out.println("loadFormContent();");
				%>
				});	
			</script>

					<%=loadScript.toString() %>
					
					
					</c:otherwise>
				</c:choose>
					
				</cw:body>
				
				
				
			</cw:main>

			<%
		renderRequest.setAttribute("params", params);
		renderRequest.setAttribute("showFav", false);
		renderRequest.setAttribute("panel", "Task");
		renderRequest.setAttribute("screen", "task.id-" + id);
	%>

			<script type="text/javascript">
				setActiveFunction(this.name, '<%=request.getParameter("activeFunction")%>');
			
				function sendExport()
				{
					if (<%=canExport%>)
					{
						var url = '<%=PortalUtil.getBaseURL()%>adm/pgm/geraPDF.jsp?action=exportProcess&modelId=<%=modelId%>&force';
						window.open(url,"nova","height=300,width=500,left=20,top=20");
					} 
					else 
					{
						alert('<%= I18nUtils.getString("message10")%>');
					}
				}
			</script>

		</c:when>
		<c:otherwise>
			<cw:main>
				<cw:header title='<%=title%>' />
				<cw:body id="area_scroll">
			<div class='alert alert-danger alert-dismissable'>
				<%= I18nUtils.getString("tasknotfound") %>
			</div>
			</cw:body>
			</cw:main>
		</c:otherwise>
	</c:choose>

	<%-- 
	Quando vai importar os titulos do SAP, at� terminar a importa��o, 
	coloca a div em primeiro plano, n�o permitindo qualquer altera��o na tela
 --%>
	<div id="espera" style="display: none;">
		<img src="imagens/icones_final/load_bar_220x19.gif"
			style="position: absolute; top: 50%; left: 50%; margin-left: -110px; margin-top: -10px; z-index: 1000111;" />
		<div
			style="width: 100%; height: 100%; position: absolute; top: 0; left: 0; background: #FFFFFF; opacity: 0.5; filter: alpha(opacity = 50); filter: progid: DXImageTransform . Microsoft . Alpha(opacity =   50); z-index: 1000000; text-align: center; vertical-align: middle;">
		</div>
	</div>

</div>
</portal:head>