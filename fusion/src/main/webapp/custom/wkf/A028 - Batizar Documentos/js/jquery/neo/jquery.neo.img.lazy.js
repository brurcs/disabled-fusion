/*!
 * Neomind Lazy Component.
 * 
 * This component is a combination of the best features of both lazy plugins listed below.
 * 
 * 1) jQuery Lazy
 *
 * Copyright 2013, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL v2 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * Project home: http://jquery.eisbehr.de/lazy/
 * 
 * Version: 0.1.6
 *
 * 2) Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2013 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home: http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.8.4
 *
 */
(function($, window, document, undefined)
{
	var win = $(window);
	
	$.fn.lazy = function(settings)
	{
		/**
		 * settings and configuration data
		 * @var array
		 */
		var configuration =
		{
			// general
			bind            : "event", /*load*/
			threshold       : 100,
			fallbackHeight  : 2000,
			visibleOnly     : true,

			// delay
			delay           : -1,
			combined        : false,

			// attribute
			attribute       : "data-src",
			removeAttribute : true,

			// effect
			effect          : "show",
			effectTime      : 0,

			// throttle
			enableThrottle  : true,
			throttle        : 250,

			// callback
			beforeLoad      : null,
			onLoad          : null,
			afterLoad       : null,
			onError         : null,
			onCenter		: null, // center of container

			// container
			container       : window,
		}
		
		// overwrite configuration with custom user settings
		if( settings )
			$.extend(configuration, settings);
		
		// all given items by jQuery selector
		var items = this;
		
		// on first page load get initial images
		if( configuration.bind == "load" )
			$(window).load(_init);
		
		// if event driven don't wait for page loading
		else if( configuration.bind == "event" )
			_init();
		
		// bind error callback to images if wanted
		if( configuration.onError )
			items.bind("error", function() { configuration.onError($(this)); });
		
		/**
		 * lazyEvent(all)
		 * 
		 * check and load all needed images
		 * 
		 * @param boolean all
		 * @return void
		 */
		function lazyEvent(all)
		{
			if( typeof all != "boolean" )
				all = false;

			// TODO - Para otimizar performance, criar uma flag sequential, 
			// e fazer break no looping no primeiro false, pois os elementos serão sequenciais.
			
			items.each(function()
			{
				var element = $(this);

				if( element.is(":visible") || !configuration.visibleOnly)
				{
					 //if(console.log)
                     	//console.log("[jquery.neo.img.lazy][lazyEvent] id: " + element.attr("id"));
					
					if(element.data("loaded"))
						if(configuration.onCenter && _isInCenter(element, configuration) )
						{
						//Log.log("[jquery.neo.img.lazy][lazyEvent] id: " + element.attr("id") + " is in center area!");

							configuration.onCenter(element);
						}

					if(!element.data("loaded"))
					{
						if( _isInLoadableArea(element, configuration) || all  )
						{
							// bind after load callback to images if wanted
							if( configuration.onLoad )
								element.bind("load", function() { configuration.onLoad(element); element.unbind("load"); });
							
							// trigger function before loading image
							if( configuration.beforeLoad )
								configuration.beforeLoad(element);
							
							$("<img />")
	                        .bind("load", function() {
	                            element
	                                .hide()
	                                .attr("src", element.attr(configuration.attribute))
	                                [configuration.effect](configuration.effectTime);
	
	                            //if(console.log)
	                            //	console.log("[jquery.neo.img.lazy][lazyEvent] id: " + element.attr("id") + " width: " + this.width + " height: " + this.height);
	
	                            element.attr("source_width", this.width);
	                            element.attr("source_height", this.height)
	
	                            // trigger function after loading image
	                            if( configuration.afterLoad )
	    							configuration.afterLoad(element);
	                            
	                            // remove attribute after load
	                            if( configuration.removeAttribute )
	    							element.removeAttr(configuration.attribute);
	
	                        })
	                        .attr("src", element.attr(configuration.attribute));
							
							// mark image as loaded
	                        element.data("loaded", true);

	                        // With registered event center not remove from array
	                        
	                        if(!configuration.onCenter)
	                        {
	                        	// Remove image from array so it is not looped next time.
		                        var temp = $.grep(items, function(element) {
		                        	var e = $(element);
		                            return !e.data("loaded");
		                        });
		                        items = $(temp);
							}
						}
					}
				}
			});
		}
		
		/**
		 * _init()
		 *
		 * initialize lazy plugin
		 * bind loading to events or set delay time to load all images at once
		 *
		 * @return void
		 */
		function _init()
		{
			// if delay time is set load all images at once after delay time
			if( configuration.delay >= 0 )
				setTimeout(function() { lazyEvent(true); }, configuration.delay);
			
			// if no delay is set or combine usage is active bin events
			if( configuration.delay < 0 || configuration.combined )
			{
				// load initial images
				lazyEvent();
				
				// bind lazy load functions to scroll and resize event
				if (configuration.container === undefined || configuration.container === window)
					$(window).bind("scroll", _throttle(configuration.throttle, lazyEvent));
				else
					$(configuration.container).bind("scroll", _throttle(configuration.throttle, lazyEvent));	
				$(window).bind("resize", _throttle(configuration.throttle, lazyEvent));
			}
		}
		
		/**
		 * _isInLoadableArea(element)
		 * 
		 * check if the given element is inside the current viewport or threshold
		 * 
		 * @param jQuery element
		 * @return boolean
		 */
		function _isInLoadableArea(element, settings)
		{			
			return !_isRighToFold(element, settings) && !_isLefToBegin(element, settings) &&
            !_isBelowTheFold(element, settings) && !_isAboveTheTop(element, settings);;
		}

		function _isInCenter(element, settings)
		{			
			var center;

			// Tratamento apenas para vertical
			if (settings.container === undefined || settings.container === window) {
	            center = (win.height() / 2) + win.scrollTop();
	        } else {
	            center = $(settings.container).offset().top + ($(settings.container).height() / 2);
	        }
			
            //Log.log("[jquery.neo.img.lazy][_isInCenter] id: " + element.attr("id") + " top: " + $(element).offset().top + " height: " + $(element).height() + " center: " + center);
			
			return $(element).offset().top <= center && ($(element).offset().top + $(element).height()) >= center;
		}

		function _isBelowTheFold(element, settings) 
		{
	        var fold;
	        
	        if (settings.container === undefined || settings.container === window) {
	            fold = win.height() + win.scrollTop();
	        } else {
	            fold = $(settings.container).offset().top + $(settings.container).height();
	        }

	        return fold <= $(element).offset().top - settings.threshold;
	    };
	    
	    function _isRighToFold(element, settings)
	    {
	        var fold;

	        if (settings.container === undefined || settings.container === window) {
	            fold = win.width() + win.scrollLeft();
	        } else {
	            fold = $(settings.container).offset().left + $(settings.container).width();
	        }

	        return fold <= $(element).offset().left - settings.threshold;
	    };
	        
	    function _isAboveTheTop(element, settings) 
	    {
	        var fold;
	        
	        if (settings.container === undefined || settings.container === window) {
	            fold = win.scrollTop();
	        } else {
	            fold = $(settings.container).offset().top;
	        }

	        return fold >= $(element).offset().top + settings.threshold  + $(element).height();
	    };
	    
	    function _isLefToBegin(element, settings) 
	    {
	        var fold;
	        
	        if (settings.container === undefined || settings.container === window) {
	            fold = win.scrollLeft();
	        } else {
	            fold = $(settings.container).offset().left;
	        }

	        return fold >= $(element).offset().left + settings.threshold + $(element).width();
	    };

		/**
		 * _throttle(delay, call)
		 * 
		 * helper function to throttle down event triggering
		 * 
		 * @param integer delay
		 * @param object function call
		 * @return function object
		 */
		function _throttle(delay, call)
		{
			var _timeout;
			var _exec = 0;
			
			function callable()
			{
				var elapsed = +new Date() - _exec;
				
				function run()
				{
					_exec = +new Date();
					call.apply();
				};
				
				_timeout && clearTimeout(_timeout);
				
				if( elapsed > delay || !configuration.enableThrottle )
					run();
				else
					_timeout = setTimeout(run, delay - elapsed);
			}
			
			return callable;
		}
		
		return this;
	}
	
	// make lazy a bit more caseinsensitive :)
	$.fn.Lazy = $.fn.lazy;
}
)(jQuery, window, document);