<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ page import="com.neomind.util.CustomFlags"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<c:if test="<%= CustomFlags.DEVELOPMENT %>">
	<jsp:include page="/js/jquery/ui/jquery.ui.core.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.widget.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.mouse.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.position.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.tabs.custom.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.button.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.spinner.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.progressbar.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.accordion.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.autocomplete.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.menu.js" />
</c:if>
<c:if test="<%= !CustomFlags.DEVELOPMENT %>">
	<jsp:include page="/js/jquery/ui/jquery.ui.core.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.widget.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.mouse.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.position.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.tabs.custom.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.button.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.spinner.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.progressbar.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.accordion.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.autocomplete.min.js" />
	<jsp:include page="/js/jquery/ui/jquery.ui.menu.min.js" />
</c:if>