<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="java.rmi.RemoteException"%>
<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy"%>
<%@page import="br.com.segware.sigmaWebServices.webServices.EventoRecebido"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.vo.ContatoVO"%>
<%@page import="com.neomind.fusion.scheduler.JobException"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO"%>
<%@page import="com.neomind.fusion.common.NeoRunnable"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.JustificativaFaltaVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.FichaEpiVO"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.util.ReflectionUtils"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.scheduler.job.CustomJobAdapter"%>
<%@page import="com.neomind.fusion.scheduler.job.CustomJobContext"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Calendar"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESTE Adapter FGC</title>
</head>
<body>
<%!

private Connection conSigma90 = null;

/**
 * Verifica se h� colaboradores no posto no horario atual e retorna uma lista com os colabodores presentes.
 * @param numctr
 * @param numpos
 * @return
 */
private List<ColaboradorVO> getColaboradoresPresentes(String numctr, String numpos){
	System.out.println("getColaboradoresPresentes("+numctr+", " + numpos +")");
	List<ColaboradorVO> retorno = new ArrayList<ColaboradorVO>();
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pstColaboradores = null;
	ResultSet rs = null;
	
	StringBuilder sqlColaboradoresPresentes = new StringBuilder();
	
	sqlColaboradoresPresentes.append(" DECLARE @DatRef  Datetime  "); 
	sqlColaboradoresPresentes.append(" SELECT @DatRef = getdate() ");
	sqlColaboradoresPresentes.append(" SELECT fun.numemp, fun.numcad, fun.nomfun, orn.numloc, orn.usu_telloc ");
	sqlColaboradoresPresentes.append(" FROM R034FUN fun ");
	sqlColaboradoresPresentes.append(" INNER JOIN R038HCH hch ON  fun.numemp = hch.numemp and fun.numcad = hch.numcad and fun.tipcol = hch.tipcol ");   
	sqlColaboradoresPresentes.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp  AND hlo.TipCol = fun.TipCol ");   
	sqlColaboradoresPresentes.append("                                                                  AND hlo.NumCad = fun.NumCad ");   
	sqlColaboradoresPresentes.append("                                                                  AND hlo.DatAlt = ( ");
	sqlColaboradoresPresentes.append("                                                                                     SELECT MAX (DATALT) ");   
	sqlColaboradoresPresentes.append("                                                                                     FROM R038HLO TABELA001 ");    
	sqlColaboradoresPresentes.append("                                                                                     WHERE TABELA001.NUMEMP = hlo.NUMEMP ");   
	sqlColaboradoresPresentes.append("                                                                                     AND TABELA001.TIPCOL = hlo.TIPCOL ");   
	sqlColaboradoresPresentes.append("                                                                                     AND TABELA001.NUMCAD = hlo.NUMCAD ");   
	sqlColaboradoresPresentes.append("                                                                                     AND TABELA001.DATALT <= @DatRef ");   
	sqlColaboradoresPresentes.append("                                                                                   ) ");  
	sqlColaboradoresPresentes.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg "); 
	sqlColaboradoresPresentes.append(" LEFT JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= @DatRef) ");   
	sqlColaboradoresPresentes.append(" LEFT JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");   
	sqlColaboradoresPresentes.append(" left JOIN R070ACC ace WITH (NOLOCK) ON ace.TipAcc = 100 AND ace.NumCra = hch.NumCra AND ace.USU_NumLoc = orn.NumLoc AND ace.DirAcc = 'E' "); 
	sqlColaboradoresPresentes.append("                                 AND DATEADD(SECOND, ace.SeqAcc, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc)) = (SELECT MAX(DATEADD(SECOND, ace2.SeqAcc, DATEADD(MINUTE, ace2.HorAcc, ace2.DatAcc))) FROM R070ACC ace2 WITH (NOLOCK) WHERE ace2.TipAcc = 100 AND ace2.NumCra = ace.NumCra AND ace2.USU_NumLoc = ace.USU_NumLoc AND ace2.DirAcc = ace.DirAcc  ) ");  
	sqlColaboradoresPresentes.append(" left JOIN R070ACC acs WITH (NOLOCK) ON acs.TipAcc = 100 AND acs.NumCra = hch.NumCra and acs.USU_NumLoc = orn.NumLoc AND acs.DirAcc = 'S' "); 
	sqlColaboradoresPresentes.append("                                 AND DATEADD(SECOND, acs.SeqAcc, DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) = (SELECT MAX(DATEADD(SECOND, acs2.SeqAcc, DATEADD(MINUTE, acs2.HorAcc, acs2.DatAcc))) FROM R070ACC acs2 WITH (NOLOCK) WHERE acs2.TipAcc = 100 AND acs2.NumCra = acs.NumCra AND acs2.USU_NumLoc = acs.USU_NumLoc AND acs2.DatAcc = acs.DatAcc AND acs2.DirAcc = acs.DirAcc AND DATEADD(MINUTE, acs2.HorAcc, acs2.DatAcc) > DATEADD(MINUTE, ace.HorAcc, ace.DatAcc)) ");  
	sqlColaboradoresPresentes.append(" WHERE 1=1 "); 
	sqlColaboradoresPresentes.append(" and @DatRef >= DATEADD(MINUTE, ace.HorAcc, ace.DatAcc) "); 
	sqlColaboradoresPresentes.append(" and @DatRef <= (case when acs.HorAcc is not null then DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) else getdate() end ) "); 
	sqlColaboradoresPresentes.append(" and (car.titred like '%VIGILANTE%' OR car.titred like '%PORTEIRO%' OR car.titred like '%VIGIA%' OR car.titred like '%MONITOR DE ACESSO%') ");
	sqlColaboradoresPresentes.append(" and (orn.usu_numctr = ?  and orn.usu_numpos = ? and usu_codccu is not null and usu_codccu <> '' ) ");
	sqlColaboradoresPresentes.append(" and fun.sitafa <> 7 ");
	//sqlColaboradoresPresentes.append(" and fun.numloc = ? ");
	
	try{
	
		pstColaboradores = conVetorh.prepareStatement(sqlColaboradoresPresentes.toString());
		
		pstColaboradores.setLong(1, Long.parseLong(numctr));
		pstColaboradores.setLong(2, Long.parseLong(numpos));
		//pstColaboradores.setLong(3, Long.parseLong(numloc));
		
		rs = pstColaboradores.executeQuery();
		
		while (rs.next()){
			ColaboradorVO colaborador = new ColaboradorVO();
			
			colaborador.setNumeroCadastro(rs.getLong("numcad"));
			colaborador.setNomeColaborador(rs.getString("nomfun"));
			colaborador.setTelefone(rs.getString("usu_telloc"));
			
			retorno.add(colaborador);
		}
		
		return retorno;
	}catch(Exception e){
		System.out.println("[XPRE] - Erro ao consultar colaboradores presentes ");
		e.printStackTrace();
		return new ArrayList<ColaboradorVO>();
	}finally{
		OrsegupsUtils.closeConnection(conVetorh, pstColaboradores, rs);
	}
}

/**
 * Insere lista de colaboradores do presen�a como contatos no sigma. 
 * <br>Para cada colaborador presente, insere na conta sigma o contato com o telefone atual do presen�a, 
 * <br>caso j� exista o contato, atualiza o telefone.
 * @param cdCliente
 * @param colaboradores
 * @return
 */
private boolean insertColaboradoresAsContatoSigma(Long cdCliente, List<ColaboradorVO> colaboradores ){
	
	try{
		this.conSigma90 = PersistEngine.getConnection("SIGMA90"); // conexao unica utilizada dentro deste m�todo.
		
		for(ColaboradorVO col : colaboradores){
			
			ContatoVO contato = getContatoSigma(cdCliente, col.getNomeColaborador() );
			if (contato != null){
				updateContatoSigma(Long.parseLong(contato.getCdProvidencia()), cdCliente, col.getNomeColaborador(), truncaCampo(col.getTelefone(),12) );
			}else{
				inserirContatoSigma(cdCliente, col.getNomeColaborador(),truncaCampo(col.getTelefone(),12), "","");
			}
		}
		
		return true;
	}catch(Exception e){
		System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] - Erro ao inserir lista de contatos na conta sigma");
		e.printStackTrace();
		return false;
	}finally{
		OrsegupsUtils.closeConnection(this.conSigma90, null, null);
	}
}


/**
 * Atualiza telefone do contato do colaborador na conta sigma do cliente.
 * @param cdProvidencia
 * @param cdCliente
 * @param nomeColaborador
 * @param telefone
 */
private void updateContatoSigma(Long cdProvidencia, Long cdCliente, String nomeColaborador, String telefone) {
	PreparedStatement pst = null;
	
	StringBuilder sql = new StringBuilder();
	
	sql.append(" update dbProvidencia set FONE1 = ? where CD_PROVIDENCIA = ? and CD_CLIENTE = ?  ");
	
	try{
		pst = this.conSigma90.prepareStatement(sql.toString());
		pst.setString(1, telefone);
		pst.setLong(2, cdProvidencia);
		pst.setLong(3, cdCliente);
		
		pst.executeUpdate();
		
	}catch(Exception e){
		e.printStackTrace();
		System.out.println("update dbProvicendia: " +sql);
	}finally{
		OrsegupsUtils.closeConnection(null, pst, null);
	}
}

/**
 * Insere contato no sigma
 * @param CD_CLIENTE
 * @param NOME
 * @param FONE1
 * @param FONE2
 * @param EMAIL
 * @throws Exception 
 */
private void inserirContatoSigma(Long CD_CLIENTE, String NOME,String FONE1, String FONE2, String EMAIL) throws Exception {
	Integer CD_PROVIDENCIA = 0;
	Integer ID_FUNCAO = 10001;
	String NU_NEXTEL_PROVIDENCIA = "";
	int VIOLACAO = 1;
	int PANICO = 1;
	int MEDICA = 1;
	int INCENDIO = 1;
	int NU_PRIORIDADE = 0;
	int NU_PRIORIDADE_NIVEL2 = 1;
	int FG_REGISTRO_ALTERADO = 0;
	int FG_REGISTRO_INCLUSO = 1;
	int FG_ERRO_WEBALARME = 0;
	int FG_EFETUADA_LIGACAO_FONE1 = 0;
	int FG_EFETUADA_LIGACAO_FONE2 = 0;
	int NU_TENTATIVA_LIGACAO_FONE1 = 0;
	int NU_TENTATIVA_LIGACAO_FONE2 = 0;
	int FG_RECEBER_LIGACOES_URA = 0;

	//Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");

	//String queryCD_PROVIDENCIA = "select MAX(isnull( CD_PROVIDENCIA,0))+1 from dbPROVIDENCIA";

	StringBuilder query = new StringBuilder();
	

	query.append("INSERT INTO dbPROVIDENCIA (");
	query.append("	   CD_CLIENTE                  "); // CD_CLIENTE, int,
	query.append("     ,ID_FUNCAO                  "); // ID_FUNCAO, int,
	query.append("     ,NOME                       "); // NOME, varchar(50),
	query.append("     ,FONE1                      "); // FONE1,
	query.append("     ,FONE2                      "); // FONE2,
	query.append("     ,VIOLACAO                   "); // VIOLACAO, bit,
	query.append("     ,PANICO                     "); // PANICO, bit,
	query.append("     ,MEDICA                     "); // MEDICA, bit,
	query.append("     ,INCENDIO                   "); // INCENDIO, bit,
	query.append("     ,EMAIL                      "); // EMAIL,
	query.append("     ,NU_PRIORIDADE              "); // NU_PRIORIDADE,
	query.append("     ,NU_PRIORIDADE_NIVEL2       "); // NU_PRIORIDADE_NIVEL2,
	query.append("     ,FG_REGISTRO_ALTERADO       "); // FG_REGISTRO_ALTERADO,
	query.append("     ,FG_REGISTRO_INCLUSO        "); // FG_REGISTRO_INCLUSO,
	query.append("     ,FG_ERRO_WEBALARME          "); // FG_ERRO_WEBALARME,
	query.append("     ,FG_EFETUADA_LIGACAO_FONE1  "); // FG_EFETUADA_LIGACAO_FONE1,
	query.append("     ,FG_EFETUADA_LIGACAO_FONE2  "); // FG_EFETUADA_LIGACAO_FONE2,
	query.append("     ,NU_TENTATIVA_LIGACAO_FONE1 "); // NU_TENTATIVA_LIGACAO_FONE1,
	query.append("     ,NU_TENTATIVA_LIGACAO_FONE2 "); // NU_TENTATIVA_LIGACAO_FONE2,
	query.append("     ,FG_RECEBER_LIGACOES_URA    "); // FG_RECEBER_LIGACOES_URA,
	query.append(" )VALUES(                        ");
	query.append("	   " + CD_CLIENTE + " "); // CD_CLIENTE, int,
	query.append("     ," + ID_FUNCAO + " "); // ID_FUNCAO, int,
	query.append("     ,'" + NOME + "' "); // NOME, varchar(50),
	query.append("     ,'" + FONE1 + "' "); // FONE1, varchar(14),
	query.append("     ,'" + FONE2 + "' "); // FONE2, varchar(14),
	query.append("     ," + VIOLACAO + " "); // VIOLACAO, bit,
	query.append("     ," + PANICO + " "); // PANICO, bit,
	query.append("     ," + MEDICA + " "); // MEDICA, bit,
	query.append("     ," + INCENDIO + " "); // INCENDIO, bit,
	query.append("     ,'" + EMAIL + "' "); // EMAIL, varchar(50),
	query.append("     ," + NU_PRIORIDADE + " "); // NU_PRIORIDADE,
	query.append("     ," + NU_PRIORIDADE_NIVEL2 + " "); // NU_PRIORIDADE_NIVEL2,
	query.append("     ," + FG_REGISTRO_ALTERADO + " "); // FG_REGISTRO_ALTERADO,
	query.append("     ," + FG_REGISTRO_INCLUSO + " "); // FG_REGISTRO_INCLUSO,
	query.append("     ," + FG_ERRO_WEBALARME + " "); // FG_ERRO_WEBALARME,
	query.append("     ," + FG_EFETUADA_LIGACAO_FONE1 + " "); // FG_EFETUADA_LIGACAO_FONE1,
	query.append("     ," + FG_EFETUADA_LIGACAO_FONE2 + " "); // FG_EFETUADA_LIGACAO_FONE2,
	query.append("     ," + NU_TENTATIVA_LIGACAO_FONE1 + " "); // NU_TENTATIVA_LIGACAO_FONE1,
	query.append("     ," + NU_TENTATIVA_LIGACAO_FONE2 + " "); // NU_TENTATIVA_LIGACAO_FONE2,
	query.append("     ," + FG_RECEBER_LIGACOES_URA + " "); // FG_RECEBER_LIGACOES_URA,
	query.append("	) ");

	PreparedStatement st = null;
	try {
		//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] - Query contato sigma :" + query.toString());
		st = this.conSigma90.prepareStatement(query.toString());
		st.executeUpdate();

	}catch (Exception e) {
		System.out.println("insert dbProvicendia: " +query.toString());
		e.printStackTrace();
		throw new Exception(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] - Erro ao cadastrar contato no sigma."+ e.getMessage());
	} finally {
		OrsegupsUtils.closeConnection(null, st, null);
	}

}

/**
 * Verifica se o contato de um cliente existe e o retorna em caso positivo.
 * @param cdCliente
 * @param nomFun
 * @return
 * @throws Exception
 */
private ContatoVO getContatoSigma( Long cdCliente, String nomFun) throws Exception{
	
	//Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");

	StringBuilder query = new StringBuilder();
	query.append(" select cd_providencia, cd_cliente, nome from dbo.dbPROVIDENCIA where cd_cliente = ? and nome = ? ");
	
	PreparedStatement st = null;
	ResultSet rs = null;
	try {
		st = this.conSigma90.prepareStatement(query.toString());
		st.setLong(1, cdCliente);
		st.setString(2, nomFun);
		
		rs = st.executeQuery();
		
		if (rs.next()){
			ContatoVO contato = new ContatoVO();
			contato.setCdProvidencia(rs.getString("cd_providencia"));
			contato.setCdCliente(rs.getString("cd_cliente"));
			contato.setNome(rs.getString("nome"));
			return contato;
		}
		return null;
	}catch (Exception e) {
		e.printStackTrace();
		throw new Exception(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] - Erro ao cadastrar contato no sigma."+ e.getMessage());
	} finally {
		OrsegupsUtils.closeConnection(null, st, rs);
	}
}




private boolean fechaEventoSigma(Long cdHistorico){
	String updateSQLPai = " UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = 11010, CD_MOTIVO_ALARME = 55, TX_OBSERVACAO_FECHAMENTO = 'Fechado por processo automatico XPRE' WHERE CD_HISTORICO = ? ";
	PreparedStatement preparedStatementUpdate = null;
	Connection conSIGMA = null;
	
	try{
		conSIGMA = PersistEngine.getConnection("SIGMA90");
		preparedStatementUpdate = conSIGMA.prepareStatement(updateSQLPai.toString());
		preparedStatementUpdate.setLong(1, cdHistorico);
		
		int ret = preparedStatementUpdate.executeUpdate();
		if (ret > 0){
			return true;
		}else{
			return false;
		}
	}catch(Exception e){
		System.out.println("Erro ao finalizar evento: " + cdHistorico);
		e.printStackTrace();
		return false;
	}finally{
		OrsegupsUtils.closeConnection(conSIGMA, preparedStatementUpdate, null);
	}
}


public static String truncaCampo(String campo, int maxSize) {
	String retorno = "";
	if (campo != null && !campo.isEmpty()){
		retorno = campo.substring(0, (campo.length()>maxSize? maxSize:campo.length() ));
	}
	
	return retorno;
}

public static void abreEvt(){
	String idCentral1 = "B409";
	String particao1 = "001";
	Long cdEmpresa1 = 10001L;
	Integer cdCliente1 = 50931;
	
	EventoRecebido eventoRecebido = new EventoRecebido();

	eventoRecebido.setCodigo("XPRE");
	eventoRecebido.setData(new GregorianCalendar());
	eventoRecebido.setEmpresa(cdEmpresa1);
	eventoRecebido.setIdCentral(idCentral1);
	eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
	eventoRecebido.setParticao(particao1);
	eventoRecebido.setProtocolo(Byte.parseByte("2"));
	eventoRecebido.setCdCliente(cdCliente1);
	

	ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();
	try {
		String returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
		System.out.println(returnFromAccess);
	} catch (RemoteException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	System.out.println("OK");
}

public void registraLogXPRE(Long cdCliente, String idCentral, String particao, Long cdHistoricoE130, Long cdHistoricoXPRE){
	String sql = "INSERT INTO logXPRE (dataOcorrencia, cdCliente, IdCentral, particao, cdHistoricoE130, cdHistoricoXPRE )   "
			+ "VALUES    (getdate(),?,?,?,?,?) ";
	
	Long retorno = 0L;
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("SAPIENS");
	
	try{
		pst = conn.prepareStatement(sql);
	
		pst.setLong(1, cdCliente);
		pst.setString(2, idCentral);
		pst.setString(3,particao);
		pst.setLong(4, cdHistoricoE130);
		pst.setLong(5, cdHistoricoXPRE);
		pst.executeUpdate();
		
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		OrsegupsUtils.closeConnection(conn, pst, null);
	}
	
	
}
	public void adicionaHistorico(Long cdHistorico, Long cdCliente){
		
		InstantiableEntityInfo xpreHistorico = AdapterUtils.getInstantiableEntityInfo("xpreHistorico");
		NeoObject obj = xpreHistorico.createNewInstance();
		EntityWrapper wObj = new EntityWrapper(obj);
		
		wObj.findField("cdhistorico").setValue(cdHistorico);
		wObj.findField("cdcliente").setValue(cdCliente);
		
		PersistEngine.persist(obj);
		
	}
	
	/**
	 * Verifica se o evento xpre j� esta marcado como sendo tratado
	 *
	 */
	public boolean estaSendoTratado(Long cdHistorico, Long cdCliente){
		boolean retorno = false;
		try{
			InstantiableEntityInfo xpreHistorico = AdapterUtils.getInstantiableEntityInfo("xpreHistorico");
			
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("cdhistorico", cdHistorico )  );
			gp.addFilter(new QLEqualsFilter("cdcliente", cdCliente )  );
			
			List<NeoUtils> resultados = (List<NeoUtils>)  PersistEngine.getObjects( xpreHistorico.getEntityClass() , gp);
			
			if (resultados != null && !resultados.isEmpty()){
				retorno = true;
			}
			
			return retorno;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
%>
<%
if(request.getParameter("act").equals("testex")){
	
	String idChamadaJsp = request.getParameter("t");
	
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	StringBuilder strMailBody = new StringBuilder();
	
	System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") +"\n\n### Abertura de Eventos XPRE Iniciada [ID CHAMADA JSP => "+idChamadaJsp+"] ###");
	System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") +"\n\n### Abertura de Eventos XPRE Iniciada ["+key+"] ###");
	System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss")+"[DISPARO DE EVENTO XPRE] ["+key+"] - Iniciando processo autom�tico de verifica��o");
	
	strMailBody.append(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") +"\n\n### Abertura de Eventos XPRE Iniciada [ID CHAMADA JSP => "+idChamadaJsp+"] ###");
	strMailBody.append(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") +"\n\n### Abertura de Eventos XPRE Iniciada ["+key+"] ###<BR>");
	
	boolean enviaEmail = false;
	
	
	
	Connection con = PersistEngine.getConnection("VETORH");
	Connection conSIGMA = PersistEngine.getConnection("SIGMA90");
	
	PreparedStatement pstSIGMA = null;
	ResultSet rsSIGMA  = null;
	
	PreparedStatement pst = null;
	ResultSet rsVinculo  = null;
	
	
	
	try{
		
		
		
		StringBuilder sqlDisparos = new StringBuilder();
		StringBuilder sqlVinculos = new StringBuilder();
		
		
		/*encontra os disparos*/
		sqlDisparos.append(" select ha.CD_HISTORICO, ha.cd_cliente, ha.dt_recebido, ha.dt_processado, ha.DT_FECHAMENTO, central.id_central, central.particao, central.id_empresa, central.COMPLEMENTO ");
		sqlDisparos.append(" from SIGMA90.dbo.HISTORICO ha with (nolock)  ");
		sqlDisparos.append(" inner join SIGMA90.dbo.dbCentral central with (nolock) on (ha.cd_Cliente = central.CD_CLIENTE)");
		sqlDisparos.append(" where ha.CD_EVENTO = 'E130' and ha.dt_recebido >= DATEADD(minute,-300,getdate())  ");
		sqlDisparos.append(" and ha.CD_HISTORICO = (select max (ha1.CD_HISTORICO) from SIGMA90.dbo.HISTORICO ha1 where  ha1.cd_cliente = ha.cd_cliente and ha1.cd_evento = ha.CD_EVENTO )");
		sqlDisparos.append(" and ha.DT_VIATURA_NO_LOCAL is null ");
		
		/* busca postos vinculados ao cdCliente x*/
		sqlVinculos.append(" DECLARE @DatRef  Datetime  "); 
		sqlVinculos.append(" SELECT @DatRef = getdate() ");
		sqlVinculos.append(" select  sigpos.usu_numctr, sigpos.usu_numpos,  orn1.numloc, orn1.usu_numemp, orn1.nomloc ");
		sqlVinculos.append(" from dbo.usu_t160sigpos sigpos with (nolock)   ");
		sqlVinculos.append(" inner join r016orn orn1 with (nolock) on (orn1.usu_numctr = sigpos.usu_numctr and orn1.usu_numpos = sigpos.usu_numpos ) ");
		sqlVinculos.append(" where  1=1 ");
		sqlVinculos.append(" and sigpos.usu_CdCliente = ? ");
		
		
		pstSIGMA = conSIGMA.prepareStatement(sqlDisparos.toString());
		rsSIGMA = pstSIGMA.executeQuery();
		
		
		
		
		strMailBody.append("<BR>XPRE<BR>");
		while(rsSIGMA.next()){ // para cada disparo, verificar se tem conta vinculada ao posto de humana.
			
			
			
			Long cdHistorico = rsSIGMA.getLong("CD_HISTORICO");
			Integer cdCliente = rsSIGMA.getInt("cd_cliente");
			String idCentral = rsSIGMA.getString("id_central");
			String particao = rsSIGMA.getString("particao");
			Long idEmpresa = rsSIGMA.getLong("id_empresa");
			
			Timestamp tsDtRecebido = rsSIGMA.getTimestamp("dt_recebido");
			Timestamp tsDtProcessado = rsSIGMA.getTimestamp("dt_processado");
			Timestamp tsDtFechamento = rsSIGMA.getTimestamp("DT_FECHAMENTO");
			
			
				GregorianCalendar gcRecebido = new GregorianCalendar();
				GregorianCalendar gcProcessado = new GregorianCalendar();
				GregorianCalendar gcFechamento = new GregorianCalendar();
				
				gcRecebido.setTimeInMillis(tsDtRecebido.getTime());
				gcProcessado.setTimeInMillis(tsDtProcessado.getTime());
				
				if (tsDtFechamento != null){
					gcFechamento.setTimeInMillis(tsDtFechamento.getTime());
				}else{
					gcFechamento = null;
				}
				
				//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Disparo de E130 em cd_cliente:" + cdCliente + ", id_central:"+idCentral+"["+particao+"]");
				strMailBody.append("<BR>[XPRE] ["+key+"] - Disparo de E130 em cd_cliente:" + cdCliente + ", id_central:"+idCentral+"["+particao+"]");
				
				pst = con.prepareStatement(sqlVinculos.toString());
				pst.setLong(1, rsSIGMA.getLong("cd_cliente"));
				
				//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Verificando Vinculo");
				
				rsVinculo = pst.executeQuery();
				
				if (rsVinculo.next()){
					//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Vinculo encontrado");
					strMailBody.append("<BR>[XPRE] ["+key+"] - Vinculo encontrado");
					//do{
						
						String numctr = rsVinculo.getString("usu_numctr");
						String numpos = rsVinculo.getString("usu_numpos");
						List<ColaboradorVO> colaboradoresPresentes = getColaboradoresPresentes(numctr,numpos);
						
						if (colaboradoresPresentes != null && !colaboradoresPresentes.isEmpty()){ // colaborador presente
							
							StringBuilder strContato = new StringBuilder();
							for (ColaboradorVO colab : colaboradoresPresentes){
								strContato.append("\n"+colab.getNomeColaborador() + "-" + colab.getTelefone() + "   ");
							}
							
							System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Presenca Encontrada no posto" + strContato);
							strMailBody.append("<BR>[XPRE] ["+key+"] - Presenca encontrada no posto -> " + strContato);
							
							if (!estaSendoTratado(cdHistorico, (long) cdCliente)){
								
								adicionaHistorico(cdHistorico, (long) cdCliente); // adiciona no eform para evitar abertura de mais de um xpre
								
								//NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") +
								
								String strLog = " [XPRE] [" + key + "] [DADOS] - \nCD_HISTORICO:" + cdHistorico + ", CLIENTE:" + cdCliente + ", IDCENTRAL:" + idCentral
										+ ", \nRecebido:" + NeoUtils.safeDateFormat(gcRecebido,"dd/MM/yyyy hh:mm:ss") 
										+ ", \nProcessado:" + NeoUtils.safeDateFormat(gcProcessado,"dd/MM/yyyy hh:mm:ss")
										+ ", \nFechamento:" + NeoUtils.safeDateFormat(gcFechamento,"dd/MM/yyyy hh:mm:ss")
										+ ", \nContrato:" + rsVinculo.getLong("usu_numctr")+ ", Posto:" + rsVinculo.getLong("usu_numpos") ;
								
								strMailBody.append("<BR>"+strLog+"<BR>");
								System.out.println(strLog);
								
								
								//para testes 
								/*idCentral = "B409";
								particao = "001";
								idEmpresa = 10001L;*/
								
								
								EventoRecebido eventoRecebido = new EventoRecebido();
								System.out.println("Params Abertura EVT idEmpresa:" + idEmpresa + ", idCentral: " + idCentral + ", particao: " + particao + ", client: " + cdCliente );
								
								
								eventoRecebido.setCodigo("XPRE");
								eventoRecebido.setData(new GregorianCalendar());
								eventoRecebido.setEmpresa(idEmpresa);
								eventoRecebido.setIdCentral(idCentral);
								eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
								eventoRecebido.setParticao(particao);
								eventoRecebido.setProtocolo(Byte.parseByte("2"));
								eventoRecebido.setCdCliente(cdCliente);
								eventoRecebido.setDescricaoReceptora(truncaCampo("Sistema Fusion                       Contatos: " + strContato.toString(), 20));
			
								ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();
			
								String returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);
								
								if (NeoUtils.safeOutputString(returnFromAccess).contains("ACK")){ // se abriu XPRE, fecha E130
									enviaEmail = true;
									fechaEventoSigma(cdHistorico); // cdHistorico do E130
									insertColaboradoresAsContatoSigma(cdCliente.longValue(), colaboradoresPresentes);
									//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Evento aberto ["+returnFromAccess+"] "+ eventoRecebido);
									strMailBody.append("<BR>[XPRE] ["+key+"] - Evento aberto ["+returnFromAccess+"] "+ eventoRecebido.getCodigo());
									
								}else{

									//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Erro ao abrir Evento ["+returnFromAccess+"] "+ eventoRecebido);
									strMailBody.append("<BR>[XPRE] ["+key+"] - Erro ao abrir Evento ["+returnFromAccess+"] "+ eventoRecebido.getCodigo());
									//System.out.println("Descricao="+eventoRecebido.getDescricaoReceptora());
									//System.out.println("idCentral="+eventoRecebido.getIdCentral());
								}
								
								//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Evento aberto ["+returnFromAccess+"] "+ eventoRecebido);
								strMailBody.append("<BR>[XPRE] ["+key+"] - Evento aberto ["+returnFromAccess+"] "+ eventoRecebido.getCodigo());
								
							}

							
							
						}else{
							//enviaEmail = true;
							//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Presenca NAO Encontrada no posto");
							strMailBody.append("<BR>[XPRE] ["+key+"] - Presenca NAO encontrada no posto ");
						}
						
					//}while(rsVinculo.next());
					
				}else{
					//System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Vinculo  NAO encontrado");
				}
				OrsegupsUtils.closeConnection(null, pst, rsVinculo);
				strMailBody.append("<BR>");
				strMailBody.append("<BR>");				
			
		}
		OrsegupsUtils.closeConnection(con, pst, rsVinculo);
		OrsegupsUtils.closeConnection(conSIGMA, pstSIGMA, rsSIGMA);
		
		if (enviaEmail){
			OrsegupsUtils.sendEmail2Orsegups( "danilo.silva@orsegups.com.br", "nao.responda@orsegups.com.br", "EVENTOS XPRE", "", strMailBody.toString()  );
			OrsegupsUtils.sendEmail2Orsegups( "lucas.alison@orsegups.com.br", "nao.responda@orsegups.com.br", "EVENTOS XPRE", "", strMailBody.toString()  );
		}
		
		System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Finalizando processo autom�tico de verifica��o");
	}catch(Exception e){
		System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Erro no processo autom�tico de verifica��o");
		e.printStackTrace();
		throw new JobException(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss") + " [XPRE] ["+key+"] - Erro ao processa verifica��o para disparo de XPRE");
	}finally{
		System.out.println(NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss")+ "### Abertura de Eventos XPRE Finalizado ["+key+"] ###");
	}
	

	
	

	
}


%>

</body>
</html>