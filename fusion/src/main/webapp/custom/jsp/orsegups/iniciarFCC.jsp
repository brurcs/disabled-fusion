<%@page import="com.neomind.fusion.security.NeoRole"%>
<%@page import="com.neomind.fusion.security.NeoGroup"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.RoleAvailableActivityData"%>


<body><%
	String op = request.getParameter("op");
	if(op == null || op.trim().equals(""))	{
		out.print("Erro #1 - Opera��o inv�lida (#"+request.getParameter("op")+")");
		return;
	}
	
	String pk = request.getParameter("pk");
	if(pk == null)
	{
		out.print("Erro #2 - Erro ao gerar contrato do Sapiens (#"+request.getParameter("pk")+")");
		return;
	}
	//pk = '00150001160873' = CodEmp = 0015 CodFil = 0001 NumCtr = 160873
	ExternalEntityInfo eContrato = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("VFUSCTRSAP");
	NeoObject contrato = null;
	List<NeoObject> listacontrato = (List<NeoObject>) PersistEngine.getObjects(eContrato.getEntityClass(), new QLEqualsFilter("pk", pk));
	if(listacontrato != null && listacontrato.size() > 0) {
		contrato = listacontrato.get(0);
	}else {
		out.print("Erro #3 - Erro ao localizar o contrato do Sapiens pelo filtro.");
		return;
	}
	if (op.equals("query")) {
		InstantiableEntityInfo eCancEletIna = AdapterUtils.getInstantiableEntityInfo("FCCcancelamentoEletrInadimplencia");

		List<NeoObject> listaRegCanc = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna.getEntityClass(), new QLEqualsFilter("contratoSapiens3", contrato));
		String result = "NA";
		if(listaRegCanc != null && listaRegCanc.size() > 0) {
			for(NeoObject noRegCanc : listaRegCanc) {
				EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
				WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();	
				if (wfpCanc.getProcessState().toString().equals("running")) {
					result = wfpCanc.getCode();
				}
			}
		} 
		out.print(result);
	} else if (op.equals("queryDetails")) {
		InstantiableEntityInfo eCancEletIna = AdapterUtils.getInstantiableEntityInfo("FCCcancelamentoEletrInadimplencia");

		List<NeoObject> listaRegCanc = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna.getEntityClass(), new QLEqualsFilter("contratoSapiens3", contrato));
		String result = "NA";
		if(listaRegCanc != null && listaRegCanc.size() > 0) {
			for(NeoObject noRegCanc : listaRegCanc) {
				EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
				WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();	
				if (wfpCanc.getProcessState().toString().equals("running")) {
			
					// filtro para pegar as atividades
					QLGroupFilter gAct = new QLGroupFilter("AND");
					gAct.addFilter(new QLFilterIsNotNull("startDate"));
					gAct.addFilter(new QLEqualsFilter("process",wfpCanc));
					
					// atividades em aberto
					Collection<Activity> colAct = PersistEngine.getObjects(Activity.class,gAct);
					result = wfpCanc.getCode() + "Tarefas/Atividades em aberto: <strong>" + colAct.size() + "</strong><br>";
					
					// tarefas em aberto
					for(Activity a : colAct)
					{			
						QLGroupFilter gTask = new QLGroupFilter("AND");
						gTask.addFilter(new QLFilterIsNull("finishDate"));
						gTask.addFilter(new QLFilterIsNotNull("startDate"));
						gTask.addFilter(new QLEqualsFilter("activity",a));
						
						Task task = (Task) PersistEngine.getObject(Task.class,gTask);
						
						// pode estar pool, dai nao tem Task ainda
						if(task != null) {
							result = result + "Tarefa: <strong>" + task.getActivityName() + "</strong> para o usu�rio: <strong>" + task.getUser().getFullName() + "</strong><br>";
						} else {
							result = result + "Atividade: <strong>" + a.getActivityName() + "</strong> em POOL para: <strong>";
							if (a instanceof UserActivity)
							{
								for (SecurityEntity sec : a.getInstance().getPotentialOwners())
								{
									if (sec instanceof NeoUser)
									{
										result = result + ", " + ((NeoUser) sec).getFullName();
									} 
									else if (sec instanceof NeoPaper)
									{
										result = result + ", " + ((NeoPaper) sec).getName();
									}
									else if (sec instanceof NeoGroup)
									{
										result = result + ", " + ((NeoGroup) sec).getName();
									}
									else if (sec instanceof NeoRole)
									{
										result = result + ", " + ((NeoRole) sec).getName();
									}
									
									if (a.getInstance().getPotentialOwners().size() > i)
									{
										result = result + ", " + ", ";
									}
								}
							}
							result = result + "</strong><br>";
						}
					}		
				}
			}
		} 
		
		out.print(result);
		
	} else if (op.equals("start")) {
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("solicitante")));
		if(solicitante == null)
		{
			out.print("Erro #4 - Solicitante n�o encontrado (#"+request.getParameter("solicitante")+")");
			return;
		}
		
		GregorianCalendar dataVencOriginal = AdapterUtils.getGregorianCalendar(request.getParameter("VctOri"), "dd/MM/yyyy");
		if(dataVencOriginal == null)
		{
			out.print("Erro #5 - Erro ao gerar o Vencimento Original (#"+request.getParameter("VctOri")+")");
			return;
		}
		
		final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class,
				new QLEqualsFilter("name", "Q016 - FCC - Cancelamento de Contrato - ELETR - Inadimpl�ncia"));
		if(pm == null)
		{
			out.print("Erro #6 - Erro ao recuperar modelo do processo ");
			return;
		}
		final WFProcess pmProcess = pm.startProcess(false, solicitante);
		final NeoObject wkfFCC	= pmProcess.getEntity();
		final EntityWrapper ewWkfFCC = new EntityWrapper(wkfFCC);
		
		ewWkfFCC.findField("dataVencPrimeiroTitulo").setValue(dataVencOriginal);
		ewWkfFCC.findField("contratoSapiens3").setValue(contrato);
		
		pmProcess.setSaved(true);
		pmProcess.setRequester(solicitante);

		out.print(pmProcess.getCode());
	} else {
		out.print("Erro #9 - Opera��o inv�lida");
		return;
	}
%>
</body>
