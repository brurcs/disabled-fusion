app.controller('appcontrollereventochp1os', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
	    
	$scope.regionalRota = [];
	$scope.tecnicoTercerizado = [];
	
	$scope.regionalRotaSel = $scope.regionalRota[0];
	$scope.tecnicoTercerizadoSel = $scope.tecnicoTercerizado[0];
		
	$scope.tags = [];
	$scope.tags2 = [];
	
	$scope.tx_total;
	$scope.tx_regionalRota = '';
	
	$scope.txt_total_gerar = 20;
	
	$scope.pesquisarRR = function(query){
				
		$scope.tx_regionalRota = query;
		
		return $http.post('http://localhost:9090/fusion/services/eform/getRegionalRota;regionalRota='+query).success(function(data, status) {			
			
		}).error(function() {
			alert("Não foi possível carregar os dados (getRegionalRota)");
		});
	};
	
	$scope.pesquisarTT = function(query){
		
		return $http.post('http://localhost:9090/fusion/services/eform/getTecnicoTercerizado;codRegionalRota=;regionalRota='+$scope.tx_regionalRota+';tecnicoTercerizado='+query+';tecnicoTercerizado=').success(function(data, status) {
			
		}).error(function() {
			alert("Não foi possível carregar os dados (getTecnicoTercerizado)");
		});
	};
	
	$scope.pesquisar = function(){
		
		console.log($scope.tags);		
		
		var rotas = '';
		var rota = '';
		for(i=0;i<$scope.tags.length;i++) {
			rotas += $scope.tags[i].id+',';
			rota += $scope.tags[i].descricao;
		}
		
		var colaboradores = '';
		var colaborador = '';
		for(i=0;i<$scope.tags2.length;i++) {
			colaboradores += $scope.tags2[i].id+',';
			colaborador += $scope.tags2[i].descricao;
		}
		
		return $http.post('http://localhost:9090/fusion/services/eform/contaTecnicoTercerizado;colaboradores='+colaboradores+';textoColaboradores='+colaborador+';rotas='+rotas+';textoRotas='+rota).success(function(data, status) {
			$('#processing-modal').modal('hide');
			$scope.tx_total = data;
		}).error(function() {
			alert("Não foi possível carregar os dados (getTecnicoTercerizado)");
		});
		
	};
	
	$scope.gerarOS = function(){ 
		
		var rotas = '';
		var rota = '';
		for(i=0;i<$scope.tags.length;i++) {
			rotas += $scope.tags[i].id+',';
			rota += $scope.tags[i].descricao;
		}
		
		var colaboradores = '';
		var colaborador = '';
		for(i=0;i<$scope.tags2.length;i++) {
			colaboradores += $scope.tags2[i].id+',';
			colaborador += $scope.tags2[i].descricao;
		}
		
		return $http.post('http://localhost:9090/fusion/services/eform/gerarOS;colaboradores='+colaboradores+';textoColaboradores='+colaborador+';rotas='+rotas+';textoRotas='+rota+';qtdeGerar='+$scope.txt_total_gerar).success(function(data, status) {
			$('#processing-modal').modal('hide');
			$.growl({title:"Aviso", message: 'OS geradas!' });
		}).error(function() {
			alert("Não foi possível carregar os dados (gerarOS)");
		});
		
	};
	
}]);
