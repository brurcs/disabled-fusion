<!DOCTYPE html>
<%@page import="com.neomind.fusion.custom.orsegups.placa.vo.MunicipioVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.placa.OrsegupsPlacaXpl1"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<html>
	<head>
		<%@page import="com.neomind.fusion.portal.PortalUtil"%>
		<meta charset="ISO-8859-1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="css/estilos.css">
		<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
		<title>Eventos CHP1</title>
		<script src="js/jquery.growl.js" type="text/javascript"></script>
		<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">                                               
			var url = '';
		</script>
	</head>
	<body>
	<% 
	List<MunicipioVO> retorno = new ArrayList<MunicipioVO>();
	retorno = OrsegupsPlacaXpl1.municipios();

	%>
		<section id=main>
			<div class="container-fluid" align="center">
				<div class="page-header">
					<h1>Gera��o de Eventos - CHP1</h1>
				</div>
				<div class="row" align="center" width="98%">
					<div class="col-md-12">
						<div class="panel with-nav-tabs panel-primary">
							<div class="panel-heading">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#tab1primary" data-toggle="tab">Informa��es Gerais</a>
									</li>
									<li>
										<a href="#tab2primary" data-toggle="tab">Cadastro de Eventos</a>
									</li>
									<li>
										<a href="#tab3primary" data-toggle="tab">Cadastro de OS (Foto de Chip)</a>
									</li>
									<li>
										<a href="#tab4primary" data-toggle="tab">Inconsist�ncias</a>
									</li>
								</ul>
							</div>
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="tab1primary">
										<div class="info">
											<h3>Observa��es:</h3>
												<ul>
													Ser�o gerados eventos quando:
													<ul>
														<li>Empresas estiverem ativas;</li>
														<li>Eventos CHP1;</li>
														<li>Eventos n�o atendidos;</li>
													</ul>
													<b>Classes utilizadas:</b>
													<ul>
														<li>OCPAbreEventoSIGMAAPI</li>
														<li>ServletChp1</li>
													</ul>
													<b>Regras:</b>
													<ul>
														<li>FG_ATIVO = 1</li>
														<li>CTRL_CENTRAL = 1</li>
														<li>ID_CENTRAL NOT LIKE 'DDD%'</li>
														<li>ID_CENTRAL NOT LIKE 'FFF%'</li>
														<li>ID_CENTRAL NOT LIKE 'AA%'</li>
														<li>ID_CENTRAL NOT LIKE 'R%'</li>
														<li>NU_LATITUDE IS NOT NULL</li>
														<li>NU_LONGITUDE IS NOT NULL</li>
													</ul>
													<br>	
												</ul>
										</div>
									</div>
									<div class="tab-pane fade" id="tab2primary">
										<!-- E-form Exce��o Cliente -->
										<div class="embed-responsive embed-responsive-16by9">
											<div id="dlgCon" class="tab-pane fade in active" style="text-align: left;" closed="false" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:false">
												<div class="ftitle">
													<font size=4><b>Preencha as informa��es a seguir para o cadastro de eventos</b></font></font>
												</div>
												<br>
												<div>
													<a href="https://www.google.com/maps" target="_blank" class="">Busca no Mapa</a>
													
												</div>
												<form id="forcom" method="post" novalidate>
													<div>
														<label>Latitude</label> 
														<input type="text" class="form-control campoformulario" id="latitude" required="required" name="latitude" value="">
														<br>
													</div>
													<div>
														<br> 
														<label>Longitude</label> 
														<input type="text" class="form-control campoformulario" id="longitude" required="required" name="longitude" value="">
														<br>
													</div>
													<div>
														<br> 
														<label>Raio de pesquisa(Km)</label> 
														<input type="text" class="form-control campoformulario" id="raio" required="required" name="raio" value="1,00">
														<br>
													</div>
													<div>
														<br> 
														<label>Quatidade de eventos que ser�o gerados (M�ximo 100 eventos)</label> <input type="text" class="form-control campoformulario" disabled="disabled" id="quantidade" required="required" name="quantidade" value=""><br>
													</div>												
												</form>
											</div>
											<div style="text-align: left;">
												<a href="javascript:void(0)" class="btn btn-primary campoBotao" iconCls="icon-print" style="width: 100px; font-size: 11px;" onclick="javascript:contar()">Verificar Quantidade</a>
											</div>
											<div style="text-align: left;">
												<br> 
												<a href="javascript:void(0)" class="btn btn-primary campoBotao" iconCls="icon-print" style="width: 100px; font-size: 14px;" onclick="javascript:gerarEventos()">Gerar Eventos</a>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="tab3primary">
										<label>Munic�pio</label> 
										<select id="municipios" name="municipios" class="form-control campoformulario" required="required" style="width: 100px;font-size:11px;">
	                                    <% for(MunicipioVO obj : retorno){%>
	                                         <option value="<%= obj.getCdMunicipio() %>"><%= obj.getNmMunicipio() %></option>
	                                    <%} %>
	                                    </select>
	                                    <br>									
									</div>
									<div class="tab-pane fade" id="tab4primary">
										<!-- E-form hist�rico cobran�a -->
										<div class="embed-responsive embed-responsive-16by9">
	  										<iframe class="embed-responsive-item" src="http://localhost:9090/fusion/portal/render/FormList?type=clientesComErroAoGerarEventos&showPages=true&edit=true&creatable=true"></iframe>
										</div>									
									</div>
									<script type="text/javascript">                                               
	  									
	  									function contar() {
	  										var msg = 'Campos obrigat�rios:'; 	
                                          	var msgConfirma = 'O valor ultrapassa 100. Ser�o gerados 100 eventos. Deseja gerar?';
                                          	var raio = document.getElementById("raio").value;
                                          	var latitude = document.getElementById("latitude").value;
                                          	var longitude = document.getElementById("longitude").value;
                                          	if(latitude == '') {
			                                	msg = msg + '\n\n- Latitude';
			                                }
                                          	if(longitude == '') {
			                                	msg = msg + '\n\n- Longitude';
			                                }
                                          	if(raio == '') {
			                                	msg = msg + '\n\n- Raio';
			                                }
                                          	if (msg == 'Campos obrigat�rios:') {
                                          		var data = 'action=consultar&raio='+raio+'&latitude='+latitude+'&longitude='+longitude;
                                        		jQuery.ajax({
     	                                        	method: 'post',
     	                                            url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.evento.servlet.ServletChp1',
     	                                            dataType: 'json',	
     	                                            data: data,
     	                                            beforeSend: function() {
     	                                               	$('#processing-modal').modal('show');
     	                                            },
     	                                            success: function(result){     	                                                	
     	                                               	$("#quantidade").val(result.total);
     	                                             	console.log(result.total);
     	                                            },
    	                                            complete: function() {
    	                                               	$('#processing-modal').modal('hide');
    	                                            },
   	                                            });
	                                       	} else {
                                          		$('#processing-modal').modal('hide');
			                                	$.growl.warning({title:"Aviso", message: msg });
			                                }
	  									} 
                                               
                                        function gerarEventos() {
                                        	var msg = 'Campos obrigat�rios:'; 	
                                            var msgConfirma = 'O valor ultrapassa 100. Ser�o gerados 100 eventos. Deseja gerar?';
                                            var raio = document.getElementById("raio").value;
                                            var latitude = document.getElementById("latitude").value;
                                            var longitude = document.getElementById("longitude").value;
                                            var quantidade = document.getElementById("quantidade").value;
                                            if(latitude == '') {
			                                	msg = msg + '\n\n- Latitude';
			                                }
                                            if(longitude == '') {
			                                	msg = msg + '\n\n- Longitude';
			                                }
                                            if(raio == '') {
			                                	msg = msg + '\n\n- Raio';
			                                }
                                            if(quantidade == '') {
			                                	msg = msg + '\n- Quantidade';
			                                }
                                            if (msg == 'Campos obrigat�rios:') {                                            		   
                                            	var data = 'action=gerarEventos&quantidade='+quantidade+'+&raio='+raio+'&latitude='+latitude+'&longitude='+longitude;
		                                        jQuery.ajax({
		     	                                	method: 'post',
		     	                                    url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.evento.servlet.ServletChp1',
													dataType : 'json',
													data : data,
													beforeSend : function() {
														$('#processing-modal').modal('show');
													},
													success : function(result) {
														$.growl({
															title : "",
															message : result.mensagemSucesso
														});
													},
													complete : function() {
														$('#processing-modal').modal('hide');
													},
												});
		                                    } else {
												$.growl.warning({
													title : "Aviso",
													message : msg
												});
											}
										}

										function radio(valor) {
											document.getElementById("result").value = valor;
										}
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</section>
		<style>
			.campoformulario {
				max-width: 300px;
				min-width: 300px;
			}
			.campoBotao {
				max-width: 300px;
				min-width: 300px;
				background-color: #6b7486;
			}
			.tipoOk {
				background-image: url(imagens/certo2.png);
			}
			.tipoFalta {
				background-image: url(imagens/exclamacao2.png);
			}
		</style>
		<div class="modal modal-static fade" id="processing-modal" name="processing-modal" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<div class="text-center">
							<img src="imagens/loader.gif" style="height: 20px;" class="icon" />
							<h4>Processando...</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>