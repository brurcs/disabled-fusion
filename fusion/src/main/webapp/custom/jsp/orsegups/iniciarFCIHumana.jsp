<%@page import="com.neomind.fusion.security.NeoRole"%>
<%@page import="com.neomind.fusion.security.NeoGroup"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page import="java.util.Set"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper"%>
<%@page import="com.neomind.fusion.workflow.simulation.WorkflowService"%>
<%@page import="javax.persistence.Query"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%> 
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="java.lang.Object"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.RoleAvailableActivityData"%>


<body><%
	// Definicoes Gerais
	String nomeFonteDados = "SAPIENS";
	String op = request.getParameter("op");
	if(op == null || op.trim().equals(""))	{
		out.print("Erro #1 - Opera��o inv�lida (#"+request.getParameter("op")+")");
		return;
	}
	
	String pk = request.getParameter("pk");
	if(pk == null)
	{
		out.print("Erro #2 - Erro ao gerar contrato chave do Sapiens (#"+request.getParameter("pk")+")");
		return;
	}
	// Contrato
	//pk = '00150001160873' = CodEmp = 0015 CodFil = 0001 NumCtr = 160873
	NeoObject contrato = null;
	ExternalEntityInfo eContrato = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("VFUSCTRSAP");
	List<NeoObject> listacontrato = (List<NeoObject>) PersistEngine.getObjects(eContrato.getEntityClass(), new QLEqualsFilter("pk", pk));
	if(listacontrato != null && listacontrato.size() > 0) {
		contrato = listacontrato.get(0);
		
	}else {
		out.print("Erro #3 - Erro ao localizar o contrato do Sapiens pela chave.");
		return;
	}
	
	
	if (op.equals("query")) {
		InstantiableEntityInfo eCancEletIna = AdapterUtils.getInstantiableEntityInfo("FCIHumanaInadimplencia");

		List<NeoObject> listaRegCanc = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna.getEntityClass(), new QLEqualsFilter("contratoSapiens", contrato));
		String result = "NA";
		if(listaRegCanc != null && listaRegCanc.size() > 0) {
			for(NeoObject noRegCanc : listaRegCanc) {
				EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
				WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();	
				if (wfpCanc.getProcessState().toString().equals("running")) {
					result = wfpCanc.getCode();
				}
			}
		} 
		out.print(result);
	} else if (op.equals("queryDetails")) {
		//verifica se existe tarefa para o processo C028
		InstantiableEntityInfo eCancEletIna = AdapterUtils.getInstantiableEntityInfo("FCIHumanaInadimplencia");

		List<NeoObject> listaRegCanc = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna.getEntityClass(), new QLEqualsFilter("contratoSapiens", contrato));
		String result = "NA";
		if(listaRegCanc != null && listaRegCanc.size() > 0) {
			for(NeoObject noRegCanc : listaRegCanc) {
				EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
				WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();	
				if (wfpCanc.getProcessState().toString().equals("running")) {
			
					// filtro para pegar as atividades
					QLGroupFilter gAct = new QLGroupFilter("AND");
					gAct.addFilter(new QLFilterIsNotNull("startDate"));
					gAct.addFilter(new QLEqualsFilter("process",wfpCanc));
					
					// atividades em aberto
					Collection<Activity> colAct = PersistEngine.getObjects(Activity.class,gAct);
					result = wfpCanc.getCode() + "Tarefas/Atividades em aberto: <strong>" + colAct.size() + "</strong><br>";
					
					// tarefas em aberto
					for(Activity a : colAct)
					{			
						QLGroupFilter gTask = new QLGroupFilter("AND");
						gTask.addFilter(new QLFilterIsNull("finishDate"));
						gTask.addFilter(new QLFilterIsNotNull("startDate"));
						gTask.addFilter(new QLEqualsFilter("activity",a));
						
						Task task = (Task) PersistEngine.getObject(Task.class,gTask);
						
						// pode estar pool, dai nao tem Task ainda
						if(task != null) {
							result = result + "Processo: C028 Tarefa: <strong>" + task.getActivityName() + "</strong> para o usu�rio: <strong>" + task.getUser().getFullName() + "</strong><br>";
						} else {
							result = result + "Processo: C028 Atividade: <strong>" + a.getActivityName() + "</strong> em POOL para: <strong>";
							if (a instanceof UserActivity)
							{
								for (SecurityEntity sec : a.getInstance().getPotentialOwners())
								{
									if (sec instanceof NeoUser)
									{
										result = result + ", " + ((NeoUser) sec).getFullName();
									} 
									else if (sec instanceof NeoPaper)
									{
										result = result + ", " + ((NeoPaper) sec).getName();
									}
									else if (sec instanceof NeoGroup)
									{
										result = result + ", " + ((NeoGroup) sec).getName();
									}
									else if (sec instanceof NeoRole)
									{
										result = result + ", " + ((NeoRole) sec).getName();
									}
									
									if (a.getInstance().getPotentialOwners().size() > i)
									{
										result = result + ", " + ", ";
									}
								}
							}
							result = result + "</strong><br>";
						}
					}		
				}
			}
		} 

		//verifica se existe tarefa para o processo C029
		InstantiableEntityInfo eCancEletIna2 = AdapterUtils.getInstantiableEntityInfo("FCNCancelamentoHumanaInadimplencia");

		List<NeoObject> listaRegCanc2 = (List<NeoObject>) PersistEngine.getObjects(eCancEletIna2.getEntityClass(), new QLEqualsFilter("contratoSapiens", contrato));
		if(listaRegCanc2 != null && listaRegCanc2.size() > 0) {
			for(NeoObject noRegCanc : listaRegCanc2) {
				EntityWrapper ewRegCanc = new EntityWrapper(noRegCanc);
				WFProcess wfpCanc = (WFProcess) ewRegCanc.findField("wfprocess").getValue();	
				if (wfpCanc.getProcessState().toString().equals("running")) {
			
					// filtro para pegar as atividades
					QLGroupFilter gAct = new QLGroupFilter("AND");
					gAct.addFilter(new QLFilterIsNotNull("startDate"));
					gAct.addFilter(new QLEqualsFilter("process",wfpCanc));
					
					// atividades em aberto
					Collection<Activity> colAct = PersistEngine.getObjects(Activity.class,gAct);
					result = wfpCanc.getCode() + "Tarefas/Atividades em aberto: <strong>" + colAct.size() + "</strong><br>";
					
					// tarefas em aberto
					for(Activity a : colAct)
					{			
						QLGroupFilter gTask = new QLGroupFilter("AND");
						gTask.addFilter(new QLFilterIsNull("finishDate"));
						gTask.addFilter(new QLFilterIsNotNull("startDate"));
						gTask.addFilter(new QLEqualsFilter("activity",a));
						
						Task task = (Task) PersistEngine.getObject(Task.class,gTask);
						
						// pode estar pool, dai nao tem Task ainda
						if(task != null) {
							result = result + "Processo: C029 Tarefa: <strong>" + task.getActivityName() + "</strong> para o usu�rio: <strong>" + task.getUser().getFullName() + "</strong><br>";
						} else {
							result = result + "Processo: C029 Atividade: <strong>" + a.getActivityName() + "</strong> em POOL para: <strong>";
							if (a instanceof UserActivity)
							{
								for (SecurityEntity sec : a.getInstance().getPotentialOwners())
								{
									if (sec instanceof NeoUser)
									{
										result = result + ", " + ((NeoUser) sec).getFullName();
									} 
									else if (sec instanceof NeoPaper)
									{
										result = result + ", " + ((NeoPaper) sec).getName();
									}
									else if (sec instanceof NeoGroup)
									{
										result = result + ", " + ((NeoGroup) sec).getName();
									}
									else if (sec instanceof NeoRole)
									{
										result = result + ", " + ((NeoRole) sec).getName();
									}
									
									if (a.getInstance().getPotentialOwners().size() > i)
									{
										result = result + ", " + ", ";
									}
								}
							}
							result = result + "</strong><br>";
						}
					}		
				}
			}
		} 
		
		out.print(result);
		
	} else if (op.equals("start")) {
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("solicitante")));
		if(solicitante == null)
		{
			out.print("Erro #4 - Solicitante n�o encontrado (#"+request.getParameter("solicitante")+")");
			return;
		}

		EntityWrapper contratoSapiensWrapper = new EntityWrapper(contrato);
	
		final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class,
				new QLEqualsFilter("name", "C028 - FCI - Humana Inadimpl�ncia"));
		if(pm == null)
		{
			out.print("Erro #6 - Erro ao recuperar modelo do processo ");
			return;
		}
		
		
		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 12/03/2015
		 */
		
		final WFProcess processo = WorkflowService.startProcess(pm, false, solicitante);
		processo.setSaved(true);
	
		final NeoObject wkfFCC = processo.getEntity();
		final EntityWrapper ewWkfFCC = new EntityWrapper(wkfFCC);
		
		ewWkfFCC.findField("contratoSapiens").setValue(contrato);
		
		try
		{
			new OrsegupsWorkflowHelper(processo).avancaPrimeiraTarefa(solicitante);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			out.print("Erro #4 - Erro ao avan�ar a primeira tarefa");
			return;
		}
		
		/**
		 * FIM ALTERA��ES - NEOMIND
		 */

		out.print(processo.getCode());
	} else {
		out.print("Erro #9 - Opera��o inv�lida");
		return;
	}
%>
</body>
