function pesquisarCliente() {
	startProgress();
	var cpfCnpj = document.getElementById("cpfCnpj").value;
	if (!cpfCnpj == "") {
		var data = 'action=listarIntegracaoWinker&cgcCpf=' + cpfCnpj;
		jQuery
				.ajax({
					method : 'post',
					url : 'http://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.winker.servlet.ServletIntegracaoWinker',

					dataType : 'json',
					data : data,
					success : function(result) {
						limparTabelas();
						if (result.name !== undefined) {
							$('#tabelaClientes').append(
									"<tr><td>" + result.name + "</td><td>"
											+ result.address + "</td>"
											+ "</td><td>" + result.city
											+ "</td>" + "</td><td>"
											+ result.dt_cadastro + "</td>"
											+ "</td><td>" + result.dt_protocolo
											+ "</td>" + "</td><td>"
											+ result.manager + "</td>"
											+ "</td>" + "</td><td>"
											+ result.retorno_integracao
											+ "</td></tr>");
							$.growl({
								title : "",
								message : result.msg
							});
							pesquisarSindico();
							pesquisarColaboradores();
							progresso100();
							stopProgress();

						} else {
							if (result.status === 102) {
								var botao = document
										.getElementById("btnConfirmacaoIntegracao");
								botao.click();
							} else {
								$.growl.warning({
									title : "",
									message : result.msg
								});
							}
							stopProgress();

						}
					}
				});
	} else {
		limparTabelas();
		$.growl.warning({
			title : "",
			message : "CNPJ/CPF do cliente é obrigatório"
		});
		stopProgress();
	}

}

function fecharConfirmacao() {
	$('#dtConfirmação').window('close');
}

function fecharConfirmacaoSindico() {
	$('#dtConfirmaçãoSindico').window('close');
}

function dtInformacoesSindico() {
	$('#dtInformacoesSindico').window('close');
	document.getElementById("nomSin").value = "";
	document.getElementById("emaSin").value = "";
	document.getElementById("cpfSin").value = "";
}

function abrirInformacoesSindico() {
	$('#dtInformacoesSindico').window('open');
}

function salvarDadosSindico() {
	var cpfCnpj = document.getElementById("cpfCnpj").value;
	if (!cpfCnpj == "") {
		startProgress();
		var nomSin = document.getElementById("nomSin").value;
		var emaSin = document.getElementById("emaSin").value;
		var cpfSin = document.getElementById("cpfSin").value;

		progresso40();
		var data = 'action=salvarDadosSindico&cpfSin=' + cpfSin + "&nomSin="
				+ nomSin + "&emaSin=" + emaSin + "&cpfCnpj=" + cpfCnpj;
		jQuery
				.ajax({
					method : 'post',
					url : 'http://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.winker.servlet.ServletIntegracaoWinker',
					dataType : 'json',
					data : data,
					success : function(result) {
						$.growl({
							title : "",
							message : result.msg
						});
						document.getElementById("nomSin").value = "";
						document.getElementById("emaSin").value = "";
						document.getElementById("cpfSin").value = "";
						$('#closeModalDadosSindico').click();
					}
				});
		progresso100();
		stopProgress();
	} else {
		$.growl.warning({
			title : "",
			message : "CNPJ/CPF do cliente é obrigatório"
		});
		document.getElementById("nomSin").value = "";
		document.getElementById("emaSin").value = "";
		document.getElementById("cpfSin").value = "";
	}
}

function pesquisarSindico() {
	progresso40();
	var cpfCnpj = document.getElementById("cpfCnpj").value;
	var data = 'action=listarSindico&cgcCpf=' + cpfCnpj;
	if(cpfCnpj.lenght > 9){
		jQuery
				.ajax({
					method : 'post',
					url : 'http://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.winker.servlet.ServletIntegracaoWinker',
					dataType : 'json',
					data : data,
					success : function(result) {
						$("#tabelaSindico > tbody").empty();
						for (i in result) {
							$('#tabelaSindico').append(
									"<tr><td>" + result[i].nomeCompleto
											+ "</td><td>" + result[i].email
											+ "</td>" + "</td><td>"
											+ result[i].phone + "</td>"
											+ "</td><td>" + result[i].dt_cadastro
											+ "</td>" + "</td><td>"
											+ result[i].dt_integracao + "</td>"
											+ "</td>" + "</td><td>"
											+ result[i].retorno_integracao
											+ "</td></tr>");
						}
					}
				});
	}else{
		alert("Por favor digite um CNPJ válido");
	}
}

function pesquisarColaboradores() {
	progresso70();
	var cpfCnpj = document.getElementById("cpfCnpj").value;
	var data = 'action=listarColaboradores&cgcCpf=' + cpfCnpj;
	jQuery
			.ajax({
				method : 'post',
				url : 'http://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.winker.servlet.ServletIntegracaoWinker',
				dataType : 'json',
				data : data,
				success : function(result) {
					$("#tabelaColaboradores > tbody").empty();
					var i = 0;
					for (i in result) {
						$('#tabelaColaboradores').append(
								"<tr><td>" + result[i].nomeCompleto
										+ "</td><td>" + result[i].occupation
										+ "</td>" + "</td><td>"
										+ result[i].doc_cpf + "</td>"
										+ "</td><td>" + result[i].dt_cadastro
										+ "</td></tr>");
					}
				}
			});
}

function startProgress() {
	document.getElementById("dvCarregar").style.display = "block";
	document.getElementById("dvProgresso").style.width = "20%";
	document.getElementById("dvProgresso").innerHTML = "20%";

}

function progresso40() {
	document.getElementById("dvProgresso").style.width = "40%";
	document.getElementById("dvProgresso").innerHTML = "40%";

}

function progresso70() {
	document.getElementById("dvProgresso").style.width = "70%";
	document.getElementById("dvProgresso").innerHTML = "70%";

}

function progresso100() {
	document.getElementById("dvProgresso").style.width = "100%";
	document.getElementById("dvProgresso").innerHTML = "100%";
}

function stopProgress() {
	$('#dvCarregar').fadeOut(1500);
}

function dadosSindico() {
	$('#closeModalSindico').click();
	$('#btnDadosSindico').click();
}

function limparTabelas() {
	$("#tabelaClientes > tbody").empty();
	$("#tabelaSindico > tbody").empty();
	$("#tabelaColaboradores > tbody").empty();
}

function integrarWinker() {
	$("#closeModalPortal").click();
	startProgress();
	var cpfCnpj = document.getElementById("cpfCnpj").value;
	var data = 'action=integrarPortal&cgcCpf=' + cpfCnpj;
	progresso40();

	jQuery
			.ajax({
				method : 'post',
				url : 'http://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.winker.servlet.ServletIntegracaoWinker',
				dataType : 'json',
				data : data,
				success : function(result) {
					progresso70();
					var status = result.status;
					if (status >= 100) {
						if (status != 103) {
							$('#btnConfirmacaoSindico').click();
							$.growl({
								title : "",
								message : result.msg
							});
						} else {
							$.growl.warning({
								title : "",
								message : result.msg
							});
						}
					} else {
						$.growl.warning({
							title : "",
							message : result.msg
						});
					}
					progresso100();
				}
			});
	stopProgress();
}

function pesquisarClienteSapiens() {
	$("#tabelaPesquisaCliente > tbody").empty();
	var div = document.getElementById('teste');
	div.style.display = "block";
	
	action = "pesquisarClienteSapiens";
	var nomeCliente = document.getElementById("nomeCliente").value;
	if (nomeCliente !== "") {
		var data = 'action=' + action + '&nomeCliente=' + nomeCliente;
		jQuery
				.ajax({
					method : 'POST',
					url : 'http://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.winker.servlet.ServletIntegracaoWinker',
					dataType : 'json',
					data : data,
					success : function(result) {
						var j = 0;
						for (i in result) {
							$('#teste').delay(-1).hide(0);
							if (result[i].codcli !== undefined) {
								$('#tabelaPesquisaCliente')
										.append(
												"<tr><td>"
														+ result[i].codcli
														+ "</td><td>"
														+ result[i].nomcli
														+ "</td>"
														+ "</td><td>"
														+ result[i].apecli
														+ "</td>"
														+ "</td><td><a href='#' onClick='clienteSelecionado("
														+ result[i].cgccpf
														+ ");'>"
														+ result[i].cgccpf
														+ "</a></td>"
														+ "</td><td>"
														+ result[i].tipcli
														+ "</td><td>"
														+ result[i].EndCli
														+ "</td>"
														+ "</td><td>"
														+ result[i].NenCli
														+ "</td>"
														+ "</td><td>"
														+ result[i].CplEnd
														+ "</td>"
														+ "</td><td>"
														+ result[i].BaiCli
														+ "</td>"
														+ "</td><td>"
														+ result[i].CidCli
														+ "</td>" + "</tr>");
								if (j == 0) {
									j = 1;
									$.growl({
										title : "",
										message : result[i].msg
									});
								}
							} else {
								if (j == 0) {
									j = 1;
									$.growl.warning({
										title : "",
										message : result.msg
									});
								}
							}

						}
						$('#teste').delay(0).hide(0);
					}

				});
	} else {
		$.growl.warning({
			title : "",
			message : "Informe o nome do cliente!"
		});
		$("#tabelaPesquisaCliente > tbody").empty();
		$('#teste').delay(0).hide(0);
	}	 
}

function clienteSelecionado(cgcCpfSelec) {
	document.getElementById("closeModal").click();
	$("#tabelaPesquisaCliente > tbody").empty();
	document.getElementById("cpfCnpj").value = cgcCpfSelec;
	document.getElementById("nomeCliente").value = "";
	pesquisarCliente();
}

function limparTabelaPesquisaCliente(){
	$("#tabelaPesquisaCliente > tbody").empty();
	document.getElementById("nomeCliente").value = "";	
}

function limparDadosSindico(){
	document.getElementById("nomSin").value = "";
	document.getElementById("emaSin").value = "";
	document.getElementById("cpfSin").value = "";
}

$(document)
		.ready(
				function() {
					$.growl
							.warning({
								title : "",
								message : "Utilize o simbolo <img src='imagens/integracao.png' width='20' height='20'/> para incluir/atualizar o sindico!"
							});
					var spam = document.getElementById('teste');
					spam.style.display = "none";					
				});