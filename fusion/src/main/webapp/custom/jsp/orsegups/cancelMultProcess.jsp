<%@page import="java.math.BigInteger"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="com.neomind.fusion.engine.runtime.RuntimeEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cancela M�ltiplos Processo</title>
</head>
<body>
<%
		Integer cont = 1;
		String motivoCancelamento = "Problema ao finalizar tarefa.";
		
		String query = " select p.neoid from WFPRocess p with (nolock) " +
				" inner join dbo.ProcessModel pm on pm.neoid = p.model_neoId " +
				" where p.saved = 1 and p.processState = 0 " +
				" and pm.name = 'R030 - QLP - Quadro de Lota��o Presen�a' " +
				" and not exists ( " +
				"         select * from Activity a with (nolock) where a.process_neoId = p.neoId and a.finishDate is null " +
				" ) ";

		EntityManager entity = PersistEngine.getEntityManager();
		
		if(entity != null)
		{
			Query q = entity.createNativeQuery(query);
			List<BigInteger> result = (List<BigInteger>) q.getResultList();

			for (BigInteger i : result)
			{
				Long id = i.longValue();
				
				/**
				 * FIXME Cancelar tarefas.
				 */
				WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));
				
				if (proc != null)
				{
					RuntimeEngine.getProcessService().cancel(proc, motivoCancelamento);
					System.out.println(cont + " : Processo " + proc.getCode());
				}
				else
				{
					System.out.println(cont + " : ERRO: Processo n�o localizado!");
				}
				cont++;
			}	
		}
		
%>
</body>
</html>