<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%
	//CONEXAO STAGINGSIGMA OU SIGMA90

	String conexaoSelecionada = "STAGINGSIGMA";	
	String kpi = request.getParameter("kpi");
	String regional = request.getParameter("regional");
	String regionalTitulo = regional;
	String retorno = "";
	

    StringBuilder txtReg = new StringBuilder();

    List<NeoObject> regionais = new ArrayList<NeoObject>();

    if (regional != null && !regional.trim().equals("null")) {
	QLRawFilter filtroOS = new QLRawFilter("regional LIKE '%" + regional + "%'");
	regionais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SigmaPontosRegional"), filtroOS);
    } else {
	regionais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SigmaPontosRegional"));
    }

    if (regionais.size() == 1)
	txtReg.append("'POLYGON(");
    if (regionais.size() > 1)
	txtReg.append("'MULTIPOLYGON(");
    if (regionais != null) {
		int cont = 0;

		for (NeoObject reg : regionais) {
		    EntityWrapper regWrapper = new EntityWrapper(reg);
		    String regStr = (String) regWrapper.findValue("pontos");

		    if (regionais.size() > 1) {
			txtReg.append("((");
			txtReg.append(regStr);
			txtReg.append("))");
			if (cont < regionais.size() - 1) {
			    txtReg.append(",");
			}
			cont++;
		    } else {
			txtReg.append("(");
			txtReg.append(regStr);
			txtReg.append(")");
		    }
		}
    }

    txtReg.append(")'");
	
	
	if(regional == null || regional.trim().equals("") || regional.trim().equals("null"))	{
	    regional = "'SOO', 'IAI', 'BQE', 'BNU', 'JLE', 'LGS', 'CUA', 'CCO', 'RSL', 'JGS', 'CTA', 'CSC', 'TRI', 'CAS', 'GNA', 'PMJ', 'SRR', 'XLN' ";
		regionalTitulo = "Geral";
 	} else {
		regional = "'"+regional+"'";
	}
	
	double kpi_value = 0;
	int kpi_qtd = 0;
	StringBuffer select_kpi = null;
 	if (kpi != null && kpi.equals("X406")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_DESARME h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'X406' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010) ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_DESARME h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'X406' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010) ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");	
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			System.out.println("Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_DESARME h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'X406' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010) ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");	
		}
	} else if (kpi != null && kpi.equals("X5")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
	    	select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX5' AND CD_CODE = 'DST' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
	    	select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX5' AND CD_CODE = 'DST' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");	
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			System.out.println("Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
	    	select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX5' AND CD_CODE = 'DST' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");	
		}
	} else if (kpi != null && kpi.equals("X2")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX2' AND CD_CODE = 'NAR' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
	    	select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX2' AND CD_CODE = 'NAR' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			System.out.println("Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
	    	select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
			select_kpi.append(" WHERE CD_EVENTO = 'XXX2' AND CD_CODE = 'NAR' ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
			select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			select_kpi.append(" AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +")) ");
		}
	} else if (kpi != null && kpi.equals("DSL")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
		    select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
		    select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			System.out.println("Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
		    select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
	} else if (kpi != null && kpi.equals("ATD")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
		    select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			System.out.println("Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
	} else if (kpi != null && kpi.equals("ATD2")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 							  ");
		    select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND c.ID_RAMO NOT IN (10004,10011,10006,10007,10009,10010,10242,15004) ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 							  ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND c.ID_RAMO NOT IN (10004,10011,10006,10007,10009,10010,10242,15004) ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			System.out.println("Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 							  ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
			select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
			select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
			select_kpi.append(" AND cod.TIPO = 1  													  ");
			select_kpi.append(" AND c.ID_RAMO NOT IN (10004,10011,10006,10007,10009,10010,10242,15004) ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
	} else if (kpi != null && kpi.equals("IDC")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			System.out.println("Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
    } else if (kpi != null && kpi.equals("ESP")) {
		select_kpi = new StringBuffer();
		
		if(conexaoSelecionada != null && conexaoSelecionada.equals("STAGINGSIGMA"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90"))
		{
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
		else if(conexaoSelecionada != null && conexaoSelecionada.equals("SIGMA90REPL"))
		{
			System.out.println("Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    select_kpi.append(" DECLARE @g GEOMETRY; ");
		    select_kpi.append(" DECLARE @h GEOMETRY; ");
		    select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
			select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
			select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
			select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
			select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		    select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
			select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
			select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
			select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 																					  ");
			select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
			select_kpi.append(" AND DATEDIFF (SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) >= 0 																			  ");
			select_kpi.append(" AND cod.TIPO = 1  																									  ");
			select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
			select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
			select_kpi.append(" AND DT_RECEBIDO >= ( ");
			select_kpi.append("		CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 0, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 12  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 WHEN DATEPART(HOUR, GETDATE()) >= 12 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 12, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    		 ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
			select_kpi.append("    	END) ");
			select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		}
    } else if (kpi != null && kpi.equals("CALL_AIT")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
		select_kpi.append(" FROM d_SIGMALogViatura  WITH (NOLOCK) ");
		select_kpi.append(" WHERE dataLogFim is not null and textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' ");
		select_kpi.append(" AND dataLog >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0))) ");  
		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		select_kpi.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			
	} else if (kpi != null && kpi.equals("EVENT_AIT")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
		select_kpi.append(" FROM d_SIGMALogViatura  WITH (NOLOCK) ");
		select_kpi.append(" WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%' ");
		select_kpi.append(" AND dataLog >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0))) ");  
		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		select_kpi.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
			
	} else if (kpi != null && kpi.equals("CALL_AIT_T")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD   ");
		select_kpi.append("  FROM d_SIGMALogViatura SL1  WITH (NOLOCK)  ");
		select_kpi.append("  WHERE SL1.dataLogFim is not null and SL1.textoLog LIKE '%Sistema Fusion. Viatura em alerta%' ");
		select_kpi.append("  AND EXISTS( SELECT SL2.placaViatura  ");
		select_kpi.append(" FROM d_SIGMALogViatura SL2 ");
		select_kpi.append(" WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim AND SL2.textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' ");
		select_kpi.append(" AND SL2.placaViatura = SL1.placaViatura ) ");
		select_kpi.append(" AND SL1.dataLog >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))  ");  
		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))  ");
		select_kpi.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)  ");
			
	}else if (kpi != null && kpi.equals("NOT_CALL_AIT")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD   ");
		select_kpi.append("  FROM d_SIGMALogViatura SL1  WITH (NOLOCK)  ");
		select_kpi.append("  WHERE SL1.dataLogFim is not null and SL1.textoLog LIKE '%Sistema Fusion. Viatura em alerta%'  ");
		select_kpi.append("  AND NOT EXISTS( SELECT SL2.placaViatura  ");
		select_kpi.append(" FROM d_SIGMALogViatura SL2 ");
		select_kpi.append(" WHERE SL2.dataLog between SL1.dataLog  and SL1.dataLogFim AND SL2.textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' ");
		select_kpi.append(" AND SL2.placaViatura = SL1.placaViatura ) ");
		select_kpi.append(" AND SL1.dataLog >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))  ");  
		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))  ");
		select_kpi.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)  ");
		
			
	}else if (kpi != null && kpi.equals("AIT_CALL_REG")) {
		select_kpi = new StringBuffer();
	
		select_kpi.append("   SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60, 2) AS DECIMAL(9,2)) AS KPI ,COUNT(*) AS QTD   ");
		select_kpi.append("   FROM d_SIGMALogViatura SL1  WITH (NOLOCK)   ");  
		select_kpi.append("   inner JOIN X_SIGMA90VIATURA VF WITH (NOLOCK) ON SL1.VIATURA_NEOID = VF.NEOID   "); 
		select_kpi.append("   inner JOIN [FSOODB03\\SQL01].SIGMA90.dbo.VIATURA AS V WITH (NOLOCK)   ON V.CD_VIATURA = VF.CD_VIATURA    ");
		select_kpi.append("   WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%'   AND V.FG_ATIVO = 1 AND V.FG_CONECTADO = 1 and textoLog not like  '%excesso de tempo no local.%'   ");
		select_kpi.append("   AND (SUBSTRING(V.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(V.NM_VIATURA, 6, 3) IN ("+ regional +"))   ");  
		select_kpi.append("   AND  EXISTS( SELECT  SL2.placaViatura     ");
		select_kpi.append("   FROM d_SIGMALogViatura SL2    ");
		select_kpi.append("    inner JOIN X_SIGMA90VIATURA VF2 WITH (NOLOCK) ON SL2.VIATURA_NEOID = VF2.NEOID    ");
		select_kpi.append("   inner JOIN [FSOODB03\\SQL01].SIGMA90.dbo.VIATURA AS V2 WITH (NOLOCK)   ON V2.CD_VIATURA = VF.CD_VIATURA   ");
		select_kpi.append("   AND V.FG_ATIVO = 1  AND V.FG_CONECTADO = 1   "); 
		select_kpi.append("   WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim AND textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%'    ");
		select_kpi.append("   AND SL2.placaViatura = SL1.placaViatura )     "); 
		select_kpi.append("   AND dataLog >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))    ");   
		select_kpi.append("   WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))    "); 
		select_kpi.append("   ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)    ");
			
	}else if (kpi != null && kpi.equals("AIT_EVT_REG")) {
		select_kpi = new StringBuffer();
	
		select_kpi.append("   SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60, 2) AS DECIMAL(9,2)) AS KPI ,COUNT(*) AS QTD   ");
		select_kpi.append("   FROM d_SIGMALogViatura SL1  WITH (NOLOCK)   ");  
		select_kpi.append("   inner JOIN X_SIGMA90VIATURA VF WITH (NOLOCK) ON SL1.VIATURA_NEOID = VF.NEOID   "); 
		select_kpi.append("   inner JOIN [FSOODB03\\SQL01].SIGMA90.dbo.VIATURA AS V WITH (NOLOCK)   ON V.CD_VIATURA = VF.CD_VIATURA    ");
		select_kpi.append("   WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%'   AND V.FG_ATIVO = 1 AND V.FG_CONECTADO = 1 and textoLog not like  '%excesso de tempo no local.%'   ");
		select_kpi.append("   AND (SUBSTRING(V.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(V.NM_VIATURA, 6, 3) IN ("+ regional +"))   ");  
		select_kpi.append("   AND NOT EXISTS( SELECT  SL2.placaViatura     ");
		select_kpi.append("   FROM d_SIGMALogViatura SL2    ");
		select_kpi.append("    inner JOIN X_SIGMA90VIATURA VF2 WITH (NOLOCK) ON SL2.VIATURA_NEOID = VF2.NEOID    ");
		select_kpi.append("   inner JOIN [FSOODB03\\SQL01].SIGMA90.dbo.VIATURA AS V2 WITH (NOLOCK)   ON V2.CD_VIATURA = VF.CD_VIATURA   ");
		select_kpi.append("   AND V.FG_ATIVO = 1  AND V.FG_CONECTADO = 1   "); 
		select_kpi.append("   WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim AND textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%'    ");
		select_kpi.append("   AND SL2.placaViatura = SL1.placaViatura )     "); 
		select_kpi.append("   AND dataLog >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))    ");   
		select_kpi.append("   WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))    "); 
		select_kpi.append("   ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)    ");
			
	}else if (kpi != null && kpi.equals("RETORNO_LIG")) {
		select_kpi = new StringBuffer();
		select_kpi.append("     SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLigacao, dataRetorno))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD     ");
		select_kpi.append("     FROM d_CERECCallCenterRetornarLigacao SL1  WITH (NOLOCK)     ");
		select_kpi.append("     WHERE SL1.dataRetorno is not null    ");
		select_kpi.append("     AND dataLigacao >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))      "); 
		select_kpi.append("     WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))     ");
		select_kpi.append("     ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)     ");
		
	}else if (kpi != null && kpi.equals("RETORNO_TEN")) {
		select_kpi = new StringBuffer();
		select_kpi.append("     SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLigacao, dataPrimeiraLigacao))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD     ");
		select_kpi.append("     FROM d_CERECCallCenterRetornarLigacao SL1  WITH (NOLOCK)     ");
		select_kpi.append("     WHERE SL1.dataPrimeiraLigacao is not null    ");
		select_kpi.append("     AND dataLigacao >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))      "); 
		select_kpi.append("     WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))     ");
		select_kpi.append("     ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)     ");
	}
	
			
   
%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%
		Connection connSigma = null;
		PreparedStatement stSeq1 = null;
		ResultSet rsSeq1 = null;
		StringBuffer sqlSeq = new StringBuffer();
		Connection connFusion = null;	
		PreparedStatement stSeq2 = null;
		ResultSet rsSeq2 = null;
	try {
		
		// KPI
		if (select_kpi != null && !kpi.contains("AIT") && !kpi.contains("RETORNO_LIG") && !kpi.contains("RETORNO_TEN")) {
			connSigma = PersistEngine.getConnection(conexaoSelecionada);
			sqlSeq = select_kpi;
			stSeq1 = connSigma.prepareStatement(sqlSeq.toString());
			rsSeq1 = stSeq1.executeQuery();
			if (rsSeq1.next()) {
				kpi_value = rsSeq1.getDouble("KPI");
				kpi_qtd = rsSeq1.getInt("QTD");
				retorno +="[{ \"name\": \""+kpi+" - "+kpi_qtd+"\",  \"showInLegend\": \"+true+\", \"data\": ["+kpi_value+"] , \"tooltip\": \"valueSuffix: { "+kpi+" }\"}]"; 

			}
			System.out.println("Fim Indicadores CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		} else if (select_kpi != null && (kpi.contains("AIT") || kpi.contains("RETORNO_LIG") || kpi.contains("RETORNO_TEN") )) {
			connFusion = PersistEngine.getConnection("FUSIONPROD");	
			sqlSeq = select_kpi;
			stSeq2 = connFusion.prepareStatement(sqlSeq.toString());
			rsSeq2 = stSeq2.executeQuery();
			if (rsSeq2.next()) {
				kpi_value = rsSeq2.getDouble("KPI");
				kpi_qtd = rsSeq2.getInt("QTD");
				retorno +="[{ \"name\": \""+kpi+" - "+kpi_qtd+"\",  \"showInLegend\": \"+true+\", \"data\": ["+kpi_value+"] , \"tooltip\": \"valueSuffix: { "+kpi+" }\"}]"; 

			}
			
			System.out.println("Fim Indicadores 2 CM 6 Horas:" + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		} 
	} catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #5 - Erro ao realizar consulta no banco de dados do Sigma");
		return;    
	} finally{
		try{
			OrsegupsUtils.closeConnection(connSigma, stSeq1, rsSeq1);
			OrsegupsUtils.closeConnection(connFusion, stSeq2, rsSeq2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
%>
<%= retorno %> 