<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%
    String raw = request.getParameter("raw");
    boolean rawData = raw != null && raw.trim().equals("yes");

    String kpi = request.getParameter("kpi");
    String regional = request.getParameter("regional");
    String width = request.getParameter("width");
    if (width == null)
		width = "250";
    String height = request.getParameter("height");
    if (height == null)
		height = "200";
    String redFrom = request.getParameter("redFrom");
    if (redFrom == null)
		redFrom = "8";
    String redTo = request.getParameter("redTo");
    if (redTo == null)
		redTo = "10";
    String yellowFrom = request.getParameter("yellowFrom");
    if (yellowFrom == null)
		yellowFrom = "5";
    String yellowTo = request.getParameter("yellowTo");
    if (yellowTo == null)
		yellowTo = "8";
    String greenFrom = request.getParameter("greenFrom");
    if (greenFrom == null)
		greenFrom = "0";
    String greenTo = request.getParameter("greenTo");
    if (greenTo == null)
		greenTo = "5";
    String min = request.getParameter("min");
    if (min == null)
		min = "0";
    String max = request.getParameter("max");
    if (max == null)
		max = "10";
    String showMajorTicks = request.getParameter("showMajorTicks");
    String majorTicks = request.getParameter("majorTicks");
    String regionalTitulo = regional;

    StringBuilder txtReg = new StringBuilder();

    List<NeoObject> regionais = new ArrayList<NeoObject>();

    if (regional != null && !regional.trim().equals("null")) {
		QLRawFilter filtroOS = new QLRawFilter("regional LIKE '%" + regional + "%'");
		regionais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SigmaPontosRegional"), filtroOS);
    } else {
		regionais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SigmaPontosRegional"));
    }

    if (regionais.size() == 1)
		txtReg.append("'POLYGON(");
    if (regionais.size() > 1)
		txtReg.append("'MULTIPOLYGON(");
    if (regionais != null) {
		int cont = 0;

		for (NeoObject reg : regionais) {
		    EntityWrapper regWrapper = new EntityWrapper(reg);
		    String regStr = (String) regWrapper.findValue("pontos");

		    if (regionais.size() > 1) {
			txtReg.append("((");
			txtReg.append(regStr);
			txtReg.append("))");
			if (cont < regionais.size() - 1) {
			    txtReg.append(",");
			}
			cont++;
		    } else {
			txtReg.append("(");
			txtReg.append(regStr);
			txtReg.append(")");
		    }
		}
    }

    txtReg.append(")'");

    if (regional == null || regional.trim().equals("") || regional.trim().equals("null")) {
		regional = "'SOO', 'IAI', 'BQE', 'BNU', 'JLE', 'LGS', 'CUA', 'CCO', 'RSL', 'JGS', 'CTA', 'CSC', 'NHO', 'TRI', 'CAS', 'GNA', 'PMJ', 'SRR', 'XLN' ";
		regionalTitulo = "Geral";
    } else {
		regional = "'" + regional + "'";
    }

    double kpi_value = 0;
    int kpi_qtd = 0;
    StringBuffer select_kpi = null;
    if (kpi != null && kpi.equals("X406")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" DECLARE @g GEOMETRY; ");
		select_kpi.append(" DECLARE @h GEOMETRY; ");
		select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
		select_kpi
			.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD, ");
		select_kpi.append("  CASE ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=2 THEN '1ate20' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 2 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '220a25' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 THEN '3mais25' ");
		select_kpi.append("  END AS FAIXA    ");
		select_kpi.append("  FROM HISTORICO_DESARME h  WITH (NOLOCK)  ");
		select_kpi.append("  INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE  ");
		select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
		select_kpi.append("  INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA  ");
		select_kpi.append("  INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE    ");
		select_kpi.append("  WHERE  CD_EVENTO = 'X406' ");
		select_kpi.append("  AND CD_USUARIO_FECHAMENTO not in (9999,11010)   ");
		select_kpi.append("  AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
		select_kpi.append("  AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +"))  ");
		select_kpi.append("  AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0))) ");
		select_kpi.append("  WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		select_kpi.append("  ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		select_kpi.append("  GROUP BY CASE ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 2 THEN '1ate20' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND,h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 2 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '220a25' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 THEN '3mais25' ");
		select_kpi.append(" END ORDER BY FAIXA");

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
		// 		select_kpi.append(" FROM HISTORICO_DESARME h  WITH (NOLOCK) ");
		// 		select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		// 		select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
		// 		select_kpi.append(" WHERE CD_EVENTO = 'X406' ");
		// 		select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND CD_USUARIO_FECHAMENTO <> 9999 ");
		// 		select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
		// 		select_kpi.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") ");
    } else if (kpi != null && kpi.equals("X5")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" DECLARE @g GEOMETRY; ");
		select_kpi.append(" DECLARE @h GEOMETRY; ");
		select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
		select_kpi
			.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD,");
		select_kpi.append("CASE ");
		select_kpi
			.append("     WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=2 THEN '1ate20' ");
		select_kpi
			.append("    WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 2 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '220a25' ");
		select_kpi
			.append("     WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 THEN '3mais25'  ");
		select_kpi.append("  END AS FAIXA    ");
		select_kpi.append("  FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
		select_kpi.append("  INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
		select_kpi.append("  INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
		select_kpi.append("  WHERE  CD_EVENTO = 'XXX5' ");
		select_kpi.append("  AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
		select_kpi.append("  AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL  ");
		select_kpi.append("  AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +"))  ");
		select_kpi.append("  AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		select_kpi.append("  WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		select_kpi.append("  ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		select_kpi.append("  GROUP BY CASE ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 2 THEN '1ate20' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND,h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 2 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '220a25' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 THEN '3mais25' ");
		select_kpi.append(" END ORDER BY FAIXA");

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
		// 		select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
		// 		select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		// 		select_kpi.append(" INNER JOIN ROTA r ON r.CD_ROTA = c.ID_ROTA ");
		// 		select_kpi.append(" WHERE CD_EVENTO = 'XXX5' AND CD_CODE = 'EX5' ");
		// 		select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND CD_USUARIO_FECHAMENTO <> 9999  ");
		// 		select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
		// 		select_kpi.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") ");
    } else if (kpi != null && kpi.equals("X2")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" DECLARE @g GEOMETRY; ");
		select_kpi.append(" DECLARE @h GEOMETRY; ");
		select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
		select_kpi
			.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD, ");
		select_kpi.append("  CASE ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=2 THEN '1ate20' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 2 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '220a25' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 THEN '3mais25' ");
		select_kpi.append("  END AS FAIXA  ");
		select_kpi.append("  FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK)  ");
		select_kpi.append("  INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE  ");
		select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
		select_kpi.append("  INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA  ");
		select_kpi.append("  INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE   ");
		select_kpi.append("  WHERE CD_EVENTO = 'XXX2' ");
		select_kpi.append("  AND CD_USUARIO_FECHAMENTO not in (9999,11010)  ");
		select_kpi.append("  AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL   ");
		select_kpi.append("  AND (SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") OR SUBSTRING(r.NM_ROTA, 6, 3) IN ("+ regional +"))  ");
		select_kpi.append("  AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		select_kpi.append("  WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		select_kpi.append("  ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		select_kpi.append("  GROUP BY CASE ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 2 THEN '1ate20' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND,h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 2 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '220a25' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_FECHAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 THEN '3mais25' ");
		select_kpi.append(" END ORDER BY FAIXA");

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, DT_RECEBIDO, DT_FECHAMENTO))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
		// 		select_kpi.append(" FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK) ");
		// 		select_kpi.append(" INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
		// 		select_kpi.append(" INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
		// 		select_kpi.append(" WHERE CD_EVENTO = 'XXX2' AND CD_CODE = 'EX2' ");
		// 		select_kpi.append(" AND DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND CD_USUARIO_FECHAMENTO <> 9999  ");
		// 		select_kpi.append(" AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
		// 		select_kpi.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ("+ regional +") ");
    } else if (kpi != null && kpi.equals("DSL")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" DECLARE @g GEOMETRY; ");
		select_kpi.append(" DECLARE @h GEOMETRY; ");
		select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
		select_kpi
			.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD, ");
		select_kpi.append(" CASE  ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND,h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 10 THEN '1ate20'  ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 10 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 15 THEN '220a25'  ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 15 THEN '3mais25'   ");
		select_kpi.append(" 	END AS FAIXA   ");
		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)      ");
		select_kpi.append("   INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE   ");
		select_kpi.append("   AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		select_kpi.append("   AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA   ");
		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE   ");
		select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL     ");
		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL    ");
		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) >= 0     ");
		select_kpi.append(" AND cod.TIPO = 1    ");
		select_kpi.append(" AND h.CD_CODE <> 'EFM'        ");
		select_kpi.append(" AND h.CD_CODE <> 'X1A'      ");
		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))    ");
		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))  ");
		select_kpi.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)  ");
		select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		select_kpi.append(" AND (vtr.NM_VIATURA not like '%PARC%' or (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))) ");
		select_kpi.append(" GROUP BY CASE  ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 10 THEN '1ate20'  ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 10 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 15 THEN '220a25'  ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 15 THEN '3mais25'  ");
		select_kpi.append(" END  ORDER BY FAIXA");

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
		// 		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
		// 		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
		// 		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
		// 		select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
		// 		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
		// 		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_VIATURA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
		// 		select_kpi.append(" AND cod.TIPO = 1  													  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
		// 		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") ");

    } else if (kpi != null && kpi.equals("ATD")) {
		select_kpi = new StringBuffer();

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD, ");
		// 		select_kpi.append(" CASE ");
		// 		select_kpi.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=20 THEN '1ate20' ");
		// 		select_kpi.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 20 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 25 THEN '220a25' ");
		// 		select_kpi.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 25 THEN '3mais25' ");
		// 		select_kpi.append(" END AS FAIXA "); 
		// 		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");                                                                         
		// 		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA    ");                                          
		// 		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE   ");                                                                 
		// 		select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL    ");                                                               
		// 		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL ");                                                                       
		// 		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0    ");                                                
		// 		select_kpi.append(" AND cod.TIPO = 1    ");                                                                                   
		// 		select_kpi.append(" AND h.CD_CODE <> 'EFM'   ");                                                                                    
		// 		select_kpi.append(" AND h.CD_CODE <> 'X1A'   ");                                                                                    
		// 		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") ");
		// 		select_kpi.append(" GROUP BY CASE ");
		// 		select_kpi.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 20 THEN '1ate20' ");
		// 		select_kpi.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 20 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 25 THEN '220a25' ");
		// 		select_kpi.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 25 THEN '3mais25' ");
		// 		select_kpi.append(" END 	ORDER BY FAIXA");

		select_kpi.append(" DECLARE @g GEOMETRY; ");
		select_kpi.append(" DECLARE @h GEOMETRY; ");
		select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
		select_kpi.append(" SELECT ");
		select_kpi
			.append(" CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI          , COUNT(*) AS QTD,  ");
		select_kpi.append("   CASE  ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=20            THEN '1ate20'  ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 20		  AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 25 THEN '220a25'");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 25 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 40            THEN '3mais25' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 40 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 50            THEN '4mais40'	");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 50 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 60            THEN '5mais50' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 60 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 70            THEN '6mais60' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 70 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 90            THEN '7mais70' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 90 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 120  	  	  THEN '8mais90' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 120 ");
		select_kpi.append("   THEN '9mais120' ");
		select_kpi.append("   END AS FAIXA  ");
		select_kpi.append("   FROM VIEW_HISTORICO h WITH (NOLOCK) ");
		select_kpi.append("   INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE   ");
		select_kpi.append("   AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		select_kpi.append("   AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
		select_kpi.append("   INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA  ");
		select_kpi.append("   INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
		select_kpi.append("   WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL  ");
		select_kpi.append("   AND h.DT_VIATURA_NO_LOCAL IS NOT NULL ");
		select_kpi.append("   AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0    ");
		select_kpi.append("   AND cod.TIPO = 1    ");
		select_kpi.append("   AND h.CD_CODE <> 'EFM' ");
		select_kpi.append("   AND h.CD_CODE <> 'X1A'   ");
		select_kpi.append("   AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		select_kpi.append("   WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))  ");
		select_kpi.append("   ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)  ");
		select_kpi.append("   AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))  ");
		select_kpi.append(" AND (vtr.NM_VIATURA not like '%PARC%' or (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))) ");
		select_kpi.append("   GROUP BY  ");
		select_kpi.append("    CASE  ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=20            THEN '1ate20'  ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 20		  AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 25 THEN '220a25' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 25 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 40            THEN '3mais25' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 40 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 50            THEN '4mais40'	");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 50 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 60            THEN '5mais50' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 60 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 70            THEN '6mais60' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 70 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 90            THEN '7mais70' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 90 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 120  	  	  THEN '8mais90' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 120 ");
		select_kpi.append("   THEN '9mais120' ");
		select_kpi.append("   END ORDER BY FAIXA  ");

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
		// 		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
		// 		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
		// 		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
		// 		select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
		// 		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
		// 		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
		// 		select_kpi.append(" AND cod.TIPO = 1  													  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
		// 		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") ");
    } else if (kpi != null && kpi.equals("ATD2")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" DECLARE @g GEOMETRY; ");
		select_kpi.append(" DECLARE @h GEOMETRY; ");
		select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
		select_kpi
			.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND,  h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2)AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD, ");
		select_kpi.append("   CASE  ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=20            THEN '1ate20'  ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 20		  AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 25 THEN '220a25'");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 25 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 40            THEN '3mais25' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 40 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 50            THEN '4mais40'	");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 50 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 60            THEN '5mais50' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 60 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 70            THEN '6mais60' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 70 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 90            THEN '7mais70' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 90 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 120  	  	  THEN '8mais90' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 120 ");
		select_kpi.append("   THEN '9mais120' ");
		select_kpi.append("   END AS FAIXA  ");
		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
		select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE   ");
		select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA   ");
		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE     ");
		select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL  ");
		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 	 ");
		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 	 ");
		select_kpi.append(" AND cod.TIPO = 1  	 ");
		select_kpi.append(" AND c.ID_RAMO NOT IN (10004,10011,10006,10007,10009,10010,10242,15004)   ");
		select_kpi.append(" AND h.CD_CODE <> 'EFM'	 ");
		select_kpi.append(" AND h.CD_CODE <> 'X1A'	 ");
		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))  ");
		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)))  ");
		select_kpi.append("	ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END)  ");
		select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		select_kpi.append(" AND (vtr.NM_VIATURA not like '%PARC%' or (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))) ");
		select_kpi.append(" GROUP BY  ");
		select_kpi.append("    CASE  ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=20            THEN '1ate20'  ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 20		  AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 25 THEN '220a25' ");
		select_kpi
			.append("  WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 25 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 40            THEN '3mais25' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 40 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 50            THEN '4mais40'	");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 50 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 60            THEN '5mais50' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 60 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 70            THEN '6mais60' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 70 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 90            THEN '7mais70' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 90 ");
		select_kpi
			.append("   AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 120  	  	  THEN '8mais90' ");
		select_kpi
			.append("   WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 120 ");
		select_kpi.append("   THEN '9mais120' ");
		select_kpi.append(" END  ORDER BY FAIXA");

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  ");
		// 		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  											  ");
		// 		select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 							  ");
		// 		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 							  ");
		// 		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  			  ");
		// 		select_kpi.append(" WHERE h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 										  ");
		// 		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL 											  ");
		// 		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 								  ");
		// 		select_kpi.append(" AND cod.TIPO = 1  													  ");
		// 		select_kpi.append(" AND c.ID_RAMO NOT IN (10004,10011,10006,10007,10009,10010,10242,15004) ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'EFM'										  			  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
		// 		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") ");
    } else if (kpi != null && kpi.equals("IDC")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" DECLARE @g GEOMETRY; ");
		select_kpi.append(" DECLARE @h GEOMETRY; ");
		select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
		select_kpi
			.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2)AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD, ");
		select_kpi.append(" CASE ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <=3 THEN '1ate20' ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 4 THEN '220a25' ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 4 THEN '3mais25' ");
		select_kpi.append(" END AS FAIXA ");
		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  ");
		select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE ");
		select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA   ");
		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE    ");
		select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 	");
		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL   ");
		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0  ");
// 		select_kpi.append(" AND cod.TIPO = 1  ");
		select_kpi.append(" AND h.CD_CODE <> 'EFM' ");
		select_kpi.append(" AND h.CD_CODE <> 'X1A'  ");
		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))  ");
		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		select_kpi.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		select_kpi.append(" AND (vtr.NM_VIATURA not like '%PARC%' or (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))) ");
		select_kpi.append(" GROUP BY CASE ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '1ate20'");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 4 THEN '220a25'");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 4 THEN '3mais25' ");
		select_kpi.append(" END ORDER BY FAIXA");

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
		// 		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
		// 		select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
		// 		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
		// 		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
		// 		select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL 																					  ");
		// 		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
		// 		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO) >= 0 																			  ");
		// 		select_kpi.append(" AND cod.TIPO = 1  																									  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
		// 		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") ");
    } else if (kpi != null && kpi.equals("ESP")) {
		select_kpi = new StringBuffer();
		select_kpi.append(" DECLARE @g GEOMETRY; ");
		select_kpi.append(" DECLARE @h GEOMETRY; ");
		select_kpi.append(" SET @g = GEOMETRY::STGeomFromText(" + txtReg.toString() + ", 4326); ");
		select_kpi
			.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2)AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD, ");
		select_kpi.append(" CASE ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '1ate20' ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 4 THEN '220a25' ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 4 THEN '3mais25'  ");
		select_kpi.append(" END AS FAIXA  ");
		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)   ");
		select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE  ");
		select_kpi.append("  AND c.NU_LATITUDE IS NOT NULL AND c.NU_LONGITUDE IS NOT NULL ");
		select_kpi.append("  AND (@g.STIntersects(GEOMETRY::Point(c.NU_LATITUDE,c.NU_LONGITUDE, 4326)) = 1) ");
		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA  ");
		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE     ");
		select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 	");
		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL  ");
		select_kpi.append(" AND DATEDIFF (SECOND,h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) >= 0    ");
		select_kpi.append(" AND cod.TIPO = 1   ");
		select_kpi.append(" AND h.CD_CODE <> 'EFM' ");
		select_kpi.append(" AND h.CD_CODE <> 'X1A'  ");
		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		select_kpi.append(" WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		select_kpi.append(" ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		select_kpi.append(" AND (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +")) ");
		select_kpi.append(" AND (vtr.NM_VIATURA not like '%PARC%' or (SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") OR SUBSTRING(vtr.NM_VIATURA, 6, 3) IN ("+ regional +"))) ");
		select_kpi.append(" GROUP BY CASE ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 3 THEN '1ate20' ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 3 AND CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) <= 4 THEN '220a25' ");
		select_kpi
			.append(" WHEN CAST(ROUND(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO)-ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0)) /60, 2) AS DECIMAL(9,2)) > 4 THEN '3mais25'  ");
		select_kpi.append(" END ORDER BY FAIXA");

		// 		select_kpi.append(" SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) -ISNULL(cod.NU_SEGUNDOS_ATRASO_MOSTRAR_ESTACAO, 0))) /60, 2) AS DECIMAL(9,2)) AS KPI, COUNT(*) AS QTD  											");
		// 		select_kpi.append(" FROM VIEW_HISTORICO h WITH (NOLOCK)  																						  ");
		// 		select_kpi.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE 																		  ");
		// 		select_kpi.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA 																		  ");
		// 		select_kpi.append(" INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE 							  												  ");
		// 		select_kpi.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL 																					  ");
		// 		select_kpi.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL                                                                                        ");	
		// 		select_kpi.append(" AND DATEDIFF (SECOND, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_DESLOCAMENTO) >= 0 																			  ");
		// 		select_kpi.append(" AND cod.TIPO = 1  																									  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'EFM'										  														  ");
		// 		select_kpi.append(" AND h.CD_CODE <> 'X1A'										  			  ");
		// 		select_kpi.append(" AND h.DT_RECEBIDO >= (CASE WHEN DATEPART(HOUR, GETDATE()) < 6 THEN (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 1, GETDATE()), 0)))   ");
		// 		select_kpi.append("    WHEN DATEPART(HOUR, GETDATE()) >= 6 AND DATEPART(HOUR, GETDATE()) < 18  THEN (DATEADD(hh, 6, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) ");
		// 		select_kpi.append("    ELSE (DATEADD(hh, 18, DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0))) END) ");
		// 		select_kpi.append(" AND SUBSTRING(vtr.NM_VIATURA, 1, 3) IN ("+ regional +") ");
    }
%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%!public static String arrayToStringString(String arr[]) {
	String retorno = "";
	if (arr != null && arr.length > 0) {
	    for (String a : arr) {
		retorno += "'" + a + "',";
	    }
	    return retorno.substring(0, retorno.length() - 1);
	} else {
	    return retorno;
	}
    }

    public static String arrayToStringInt(String arr[]) {
	String retorno = "";
	if (arr != null && arr.length > 0) {
	    for (String a : arr) {
		retorno += "" + a + ",";
	    }
	    return retorno.substring(0, retorno.length() - 1);
	} else {
	    return retorno;
	}
    }

    public static String arrayMerged(String keys[], String values[]) {
	String retorno = "";
	int cont = 0;
	if (keys != null && keys.length > 0) {
	    for (String a : keys) {
		retorno += "['" + a + "'," + values[cont] + "],";
		cont++;
	    }
	    return retorno.substring(0, retorno.length() - 1);
	} else {
	    return retorno;
	}
    }%>
<%
    Connection connSigma = null;
    PreparedStatement stSeq1 = null;
    ResultSet rsSeq1 = null;
    String kpi_faixa = "";
    String resposta = "[[\"Faixa\", \"Eventos\"],";
    String respostaPieHigh = "[";
    try {
		connSigma = PersistEngine.getConnection("STAGINGSIGMA");
		StringBuffer sqlSeq = new StringBuffer();

		// KPI
		if (select_kpi != null) {
		    sqlSeq = select_kpi;
		    stSeq1 = connSigma.prepareStatement(sqlSeq.toString());
		    rsSeq1 = stSeq1.executeQuery();
		    int count = 0;
		    while (rsSeq1.next()) {
			if (count > 0) {
			    resposta += ",";
			    respostaPieHigh += ",";
			}

			String color = "";
			kpi_value = rsSeq1.getDouble("KPI");
			kpi_qtd = rsSeq1.getInt("QTD");
			kpi_faixa = rsSeq1.getString("FAIXA");

			if (kpi_faixa.equals("1ate20")) {
			    if (kpi.contains("ATD"))
				kpi_faixa = "[0.00,20.00]- ";
				else if (kpi.contains("DSL"))
					kpi_faixa = "[0.00,10.00]- ";
			    else
				kpi_faixa = "[0.00,2.00]- ";

			    kpi_faixa += kpi_qtd + " - TM " + kpi_value + "";
			    color = "#32CD32";
			} else if (kpi_faixa.equals("220a25")) {
			    if (kpi.contains("ATD"))
				kpi_faixa = "[20.01,25.00]- ";
				else if (kpi.contains("DSL"))
					kpi_faixa = "[10.01,15.00]- ";
			    else
				kpi_faixa = "[2.01,3.00]- ";

			    kpi_faixa += kpi_qtd + " - TM " + kpi_value + "";
			    color = "#FFD700";
			} else if (kpi_faixa.equals("3mais25")) {
			    if (kpi.contains("ATD"))
				kpi_faixa = "[25.01,40.00]- ";
				else if (kpi.contains("DSL"))
					kpi_faixa = "[15.01,40.00]- ";
			    else
				kpi_faixa = "[3.01,10.00]- ";

			    kpi_faixa += kpi_qtd + " - TM " + kpi_value + "";
			    color = "#FF0000";
			} else if (kpi_faixa.equals("4mais40")) {
			    kpi_faixa = "[40.01,50.00]- " + kpi_qtd + " - TM " + kpi_value + "";
			    color = "#D20000";
			} else if (kpi_faixa.equals("5mais50")) {
			    kpi_faixa = "[50.01,60.00]- " + kpi_qtd + " - TM " + kpi_value + "";
			    color = "#B30000";
			} else if (kpi_faixa.equals("6mais60")) {
			    kpi_faixa = "[60.01,70.00]- " + kpi_qtd + " - TM " + kpi_value + "";
			    color = "#8c0000";
			} else if (kpi_faixa.equals("7mais70")) {
			    kpi_faixa = "[70.01,90.00]- " + kpi_qtd + " - TM " + kpi_value + "";
			    color = "#750000";
			} else if (kpi_faixa.equals("8mais90")) {
			    kpi_faixa = "[90.01,120.00]- " + kpi_qtd + " - TM " + kpi_value + "";
			    color = "#590000";
			} else if (kpi_faixa.equals("9mais120")) {
			    kpi_faixa = "[120.01,+OO[- " + kpi_qtd + " - TM " + kpi_value + "";
			    color = "#3E0000";
			}

			resposta += "[\"" + kpi_faixa + "\", " + kpi_qtd + "]";
			respostaPieHigh += "{ \"name\": \"" + kpi_faixa + "\", \"y\" : " + kpi_qtd + " , \"color\" :  \"" + color + "\" }";

			count++;
		    }

		}
		resposta += "]";
		respostaPieHigh += "]";

    } catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #5 - Erro ao realizar consulta no banco de dados do Sigma");
		return;
    } finally {
		try {
		    OrsegupsUtils.closeConnection(connSigma, stSeq1, rsSeq1);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		System.out.println(respostaPieHigh);
    }
    if (rawData) {
%><%=kpi_value%>
<%
    } else {
%>

<%=respostaPieHigh%>

<%
    }
%>