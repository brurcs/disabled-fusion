<%@page import="java.util.Collection"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Random"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<!DOCTYPE html>
<html>

<%!

public class GraficoVO implements Comparable<GraficoVO>
{
	private int alertaComLigacao = 0;
	private double alertaComLigacaoTempo = 0.0;
	private int alertaSemLigacao = 0;
	private double alertaSemLigacaoTempo = 0.0;
	private int totalEvento = 0;
	private double totalEventoTempo = 0.0;
	private String nomeViatura;
	
	public int getAlertaComLigacao() {
		return alertaComLigacao;
	}


	public void setAlertaComLigacao(int alertaComLigacao) {
		this.alertaComLigacao = alertaComLigacao;
	}
	
	public double getAlertaComLigacaoTempo() {
		return alertaComLigacaoTempo;
	}


	public void setAlertaComLigacaoTempo(double alertaComLigacaoTempo) {
		this.alertaComLigacaoTempo = alertaComLigacaoTempo;
	}
	
	public int getAlertaSemLigacao() {
		return alertaSemLigacao;
	}


	public void setAlertaSemLigacao(int alertaSemLigacao) {
		this.alertaSemLigacao = alertaSemLigacao;
	}
	
	public double getAlertaSemLigacaoTempo() {
		return alertaSemLigacaoTempo;
	}


	public void setAlertaSemLigacaoTempo(double alertaSemLigacaoTempo) {
		this.alertaSemLigacaoTempo = alertaSemLigacaoTempo;
	}
	
	public int getTotalEvento() {
		return totalEvento;
	}


	public void setTotalEvento(int totalEvento) {
		this.totalEvento = totalEvento;
	}
	
	public double getTotalEventoTempo() {
		return totalEventoTempo;
	}


	public void setTotalEventoTempo(double totalEventoTempo) {
		this.totalEventoTempo = totalEventoTempo;
	}
	
	public String getNomeViatura() {
		return nomeViatura;
	}


	public void setNomeViatura(String nomeViatura) {
		this.nomeViatura = nomeViatura;
	}
	

	public int compareTo(GraficoVO o)
	{
		if(this.getTotalEventoTempo() > o.getTotalEventoTempo())
			return 1;
		// TODO Auto-generated method stub
		return  -1;
	}
	
	
}


%>
<%!public class EventosAitAlertaVO {
	
	private double total;
	private String cdViatura;
	private String nmViatura;
	private String idCentral;
	private String particao;
	private String cdHistorico;
	private boolean existeLog;
	
	public double getTotal() {
		return total;
	}


	public void setTotal(double total) {
		this.total = total;
	}
	
	public String getCdViatura() {
		return cdViatura;
	}


	public void setCdViatura(String cdViatura) {
		this.cdViatura = cdViatura;
	}
	
	public String getNmViatura() {
		return nmViatura;
	}


	public void setNmViatura(String nmViatura) {
		this.nmViatura = nmViatura;
	}
	
	public String getIdCentral() {
		return idCentral;
	}


	public void setIdCentral(String idCentral) {
		this.idCentral = idCentral;
	}
	
	public String getParticao() {
		return particao;
	}


	public void setParticao(String particao) {
		this.particao = particao;
	}
	
	public String getCdHistorico() {
		return cdHistorico;
	}


	public void setCdHistorico(String cdHistorico) {
		this.cdHistorico = cdHistorico;
	}
} %>

<%!
public class EventosAlertAitLogVO{
	
	private double kpi;
	private String placaViatura;
	private String reg;
	private String nmViatura;
	private int qtd;
	private String cdViatura;
	private String textoLog;
	
	public double getKpi() {
		return kpi;
	}


	public void setKpi(double kpi) {
		this.kpi = kpi;
	}
	
	public String getPlacaViatura() {
		return placaViatura;
	}


	public void setPlacaViatura(String placaViatura) {
		this.placaViatura = placaViatura;
	}
	
	public String getReg() {
		return reg;
	}


	public void setReg(String reg) {
		this.reg = reg;
	}
	
	public String getNmViatura() {
		return nmViatura;
	}


	public void setNmViatura(String nmViatura) {
		this.nmViatura = nmViatura;
	}
	
	public int getQtd() {
		return qtd;
	}


	public void setQtd(int qtd) {
		this.qtd = qtd;
	}
	
	public String getCdViatura() {
		return cdViatura;
	}


	public void setCdViatura(String cdViatura) {
		this.cdViatura = cdViatura;
	}
	
	public String getTextoLog() {
		return textoLog;
	}


	public void setTextoLog(String textoLog) {
		this.textoLog = textoLog;
	}
	
}
%>
<%
	String regional = request.getParameter("regional");
	String kpi = request.getParameter("kpi");
	String width = request.getParameter("width");
	if (width == null) width = "300";		
	String height = request.getParameter("height");
	if (height == null) height = "220";		
	String redFrom = request.getParameter("redFrom");
	if (redFrom == null) redFrom = "8";		
	String redTo = request.getParameter("redTo");
	if (redTo == null) redTo = "10";		
	String yellowFrom = request.getParameter("yellowFrom");
	if (yellowFrom == null) yellowFrom = "5";		
	String yellowTo = request.getParameter("yellowTo");
	if (yellowTo == null) yellowTo = "8";		
	String greenFrom = request.getParameter("greenFrom");
	if (greenFrom == null) greenFrom = "0";		
	String greenTo = request.getParameter("greenTo");
	if (greenTo == null) greenTo = "5";		
	String min = request.getParameter("min");
	if (min == null) min = "0";		
	String max = request.getParameter("max");
	if (max == null) max = "10";
	String showMajorTicks = request.getParameter("showMajorTicks");
	String majorTicks = request.getParameter("majorTicks");
	if (showMajorTicks == null || showMajorTicks.equals("yes")) 
	{
		if (majorTicks == null) 
		{
		  majorTicks = max;
		}

		int iMax = Integer.parseInt(majorTicks);
		majorTicks = "";
		for (int i=0;i<=iMax;i++) 
		{
		  if (i == 0)
		  {
		  majorTicks = ", majorTicks: ['" + i + "'";	  
		  } 
		  else 
		  {
		  majorTicks = majorTicks + ", '"+i+"'";	  
		  }
		  if (i == iMax)
		  {
			majorTicks = majorTicks + "]";
		  } 
		  
		}
	}
	else 
	{
		majorTicks = "";
	}
	
	
	String jsonStr = "[['AIT','Alertas C/L',{ role: 'annotation' },{role: 'tooltip'} , 'Alertas S/L', { role: 'annotation' },{role: 'tooltip'}, 'Eventos sem alerta',  { role: 'annotation' } ,{role: 'tooltip'}  ],";
	String regionalTitulo = regional;
	if(regional == null || regional.trim().equals("") || regional.trim().equals("null"))	{
		regional = "'SOO', 'IAI', 'BQE', 'BNU', 'JLE', 'LGS', 'CUA', 'CCO', 'RSL', 'JGS', 'CTA', 'CSC' ";
		regionalTitulo = "Geral";
 	} else {
		regional = "'"+regional+"'";
	}
	
	double kpi_value = 0;
	int kpi_qtd = 0;
	String kpi_plc = "";
	StringBuilder select_EV = null;
	StringBuilder select_CL = null;
	StringBuilder select_SL = null;
	
	String periodoInicial = "";
	String periodoFinal = "";
	Calendar periodo = Calendar.getInstance();
	periodoInicial = NeoUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");
	periodoFinal = NeoUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");
	
	if (periodo.get(Calendar.HOUR_OF_DAY) >= 6 && periodo.get(Calendar.HOUR_OF_DAY) < 18)
	{
		periodoInicial += " 06:00";
		periodoFinal += " 17:59";
	}
	else if ((periodo.get(Calendar.HOUR_OF_DAY) >= 18 && periodo.get(Calendar.HOUR_OF_DAY) <= 23) || (periodo.get(Calendar.HOUR_OF_DAY) >= 0 && periodo.get(Calendar.HOUR_OF_DAY) < 6))
	{
		
		GregorianCalendar date = new GregorianCalendar();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date.getTime());
		if(periodo.get(Calendar.HOUR_OF_DAY) >= 0 && periodo.get(Calendar.HOUR_OF_DAY) < 6 ){
		int diasParaDecrementar = -1;
		cal.add(Calendar.DATE, diasParaDecrementar);
		date.setTime(cal.getTime());
		periodoInicial = NeoUtils.safeDateFormat(date, "yyyy-MM-dd");
		}
		else if(periodo.get(Calendar.HOUR_OF_DAY) >= 18 && periodo.get(Calendar.HOUR_OF_DAY) <= 23){
		int diasParaIncrementar = 1;
		cal.add(Calendar.DATE, diasParaIncrementar);
		date.setTime(cal.getTime());
		periodoFinal = NeoUtils.safeDateFormat(date, "yyyy-MM-dd");
		}
		
		periodoInicial += " 17:00";
		periodoFinal += " 05:59";
	}
		
 	if (kpi != null) {
 		
 		
 		select_EV = new StringBuilder();
 		
 		select_EV.append(" SELECT VW.CD_VIATURA,V.NM_VIATURA,C.ID_CENTRAL,C.PARTICAO,VW.CD_HISTORICO FROM VIEW_HISTORICO VW WITH(NOLOCK)  ");
 		select_EV.append(" INNER JOIN dbCENTRAL C WITH (NOLOCK) ON C.CD_CLIENTE = VW.CD_CLIENTE  ");
 		select_EV.append(" INNER JOIN VIATURA V WITH (NOLOCK) ON VW.CD_VIATURA = V.CD_VIATURA  ");
 		select_EV.append("  WHERE V.FG_ATIVO = 1  ");
 		select_EV.append("  AND (SUBSTRING(V.NM_VIATURA, 1, 3)  IN ("+ regional +") or SUBSTRING(V.NM_VIATURA, 6, 3) in ("+ regional +"))  	 ");
 		if(!kpi.isEmpty() && kpi.equals("aMonthAgo"))
 	 	select_EV.append(" AND VW.DT_FECHAMENTO  BETWEEN   DATEADD(DAY, -30,GETDATE()) AND GETDATE()   ");
 	 	else
 	 	select_EV.append(" AND VW.DT_FECHAMENTO BETWEEN  '" + periodoInicial + "' AND '" + periodoFinal + "'  ");
 		select_EV.append("  GROUP BY VW.CD_VIATURA,V.NM_VIATURA ,C.ID_CENTRAL,C.PARTICAO,VW.CD_HISTORICO  ");
 		select_EV.append("  ORDER BY V.NM_VIATURA DESC    ");
		  
 		//select_EV.append(" SELECT VW.CD_VIATURA,V.NM_VIATURA,C.ID_CENTRAL,C.PARTICAO,VW.CD_HISTORICO  FROM [FSOODB04\\SQL02].Staging.dbo.VIEW_HISTORICO VW WITH(NOLOCK)  ");
 		//select_EV.append(" INNER JOIN [SSOOBI02].StagingSigma90.dbo.sigma90.dbCENTRAL C WITH (NOLOCK) ON C.CD_CLIENTE = VW.CD_CLIENTE ");
 		//select_EV.append(" INNER JOIN [SSOOBI02].StagingSigma90.dbo.sigma90.VIATURA V WITH (NOLOCK) ON VW.CD_VIATURA = V.CD_VIATURA ");
 		//select_EV.append(" WHERE V.FG_ATIVO = 1 ");
 		//select_EV.append(" AND SUBSTRING(V.NM_VIATURA, 1, 3)  IN ("+ regional +")  ");											  
 		//if(!kpi.isEmpty() && kpi.equals("aMonthAgo"))
 		//select_EV.append(" AND VW.DT_FECHAMENTO  BETWEEN   DATEADD(DAY, -30,GETDATE()) AND GETDATE()   ");
 		//else
 		//select_EV.append(" AND VW.DT_FECHAMENTO BETWEEN  '" + periodoInicial + "' AND '" + periodoFinal + "'  ");
 		//select_EV.append(" GROUP BY VW.CD_VIATURA,V.NM_VIATURA ,C.ID_CENTRAL,C.PARTICAO,VW.CD_HISTORICO   "); 
 		//select_EV.append(" ORDER BY V.NM_VIATURA DESC  "); 

		select_CL = new StringBuilder();
				  
		select_CL.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60  , 2) AS DECIMAL(9,2)) AS KPI, placaViatura ,  ");
		select_CL.append("  SUBSTRING(V.NM_VIATURA,1 , 3 ) AS REG , V.NM_VIATURA, COUNT(*) AS QTD, V.CD_VIATURA ,SL1.textoLog     ");
		select_CL.append("  FROM d_SIGMALogViatura SL1  WITH (NOLOCK)   ");
		select_CL.append("  INNER JOIN X_SIGMA90VIATURA VF WITH (NOLOCK) ON SL1.VIATURA_NEOID = VF.NEOID  ");
		select_CL.append("  INNER JOIN [SSOOBI03].Sigma90Repl.dbo.VIATURA AS V WITH (NOLOCK)   ON V.CD_VIATURA = VF.CD_VIATURA  ");
		select_CL.append("  WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%' AND V.FG_ATIVO = 1   and textoLog not like  '%excesso de tempo no local.%' ");
		select_CL.append("  AND (SUBSTRING(V.NM_VIATURA, 1, 3) IN ("+ regional +") or SUBSTRING(V.NM_VIATURA, 6, 3) in ("+ regional +")) ");
		select_CL.append("  AND  EXISTS( SELECT SL2.placaViatura   ");
		select_CL.append("  FROM d_SIGMALogViatura   SL2  "); 
		select_CL.append("  INNER JOIN X_SIGMA90VIATURA VF2 WITH (NOLOCK) ON SL2.VIATURA_NEOID = VF2.NEOID  ");
		select_CL.append("  INNER JOIN [SSOOBI03].Sigma90Repl.dbo.VIATURA AS V2 WITH (NOLOCK)   ON V2.CD_VIATURA = VF.CD_VIATURA ");
		select_CL.append("  AND V.FG_ATIVO = 1  ");
		select_CL.append("  WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim ");
		select_CL.append("  AND textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' "); 
		select_CL.append("  AND SL2.placaViatura = SL1.placaViatura )  ");  
		if(!kpi.isEmpty() && kpi.equals("aMonthAgo"))
		select_CL.append("  AND dataLog BETWEEN DATEADD(DAY, -30,GETDATE()) AND GETDATE()  ");
		else
		select_CL.append("  AND dataLog BETWEEN  '" + periodoInicial + "' AND '" + periodoFinal + "'  ");
		select_CL.append("  GROUP BY placaViatura,  SUBSTRING(V.NM_VIATURA,1 , 3 ) , V.NM_VIATURA , V.CD_VIATURA   ,textoLog   ");
		select_CL.append("  ORDER BY  NM_VIATURA  DESC ");
	
	
		select_SL = new StringBuilder();
				  
		select_SL.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60  , 2) AS DECIMAL(9,2)) AS KPI, placaViatura ,  ");
		select_SL.append("  SUBSTRING(V.NM_VIATURA,1 , 3 ) AS REG , V.NM_VIATURA, COUNT(DISTINCT textoLog) AS QTD, V.CD_VIATURA   ,SL1.textoLog    ");
		select_SL.append("  FROM d_SIGMALogViatura SL1  WITH (NOLOCK)   ");
		select_SL.append("  INNER JOIN X_SIGMA90VIATURA VF WITH (NOLOCK) ON SL1.VIATURA_NEOID = VF.NEOID  ");
		select_SL.append("  INNER JOIN [SSOOBI03].Sigma90Repl.dbo.VIATURA AS V WITH (NOLOCK)   ON V.CD_VIATURA = VF.CD_VIATURA  ");
		select_SL.append("  WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%' AND V.FG_ATIVO = 1  and textoLog not like  '%excesso de tempo no local.%' ");
		select_SL.append("  AND (SUBSTRING(V.NM_VIATURA, 1, 3) IN ("+ regional +") or SUBSTRING(V.NM_VIATURA, 6, 3) in ("+ regional +")) ");
		select_SL.append("  AND NOT  EXISTS( SELECT SL2.placaViatura   ");
		select_SL.append("  FROM d_SIGMALogViatura   SL2  "); 
		select_SL.append("  INNER JOIN X_SIGMA90VIATURA VF2 WITH (NOLOCK) ON SL2.VIATURA_NEOID = VF2.NEOID  ");
		select_SL.append("  INNER JOIN [SSOOBI03].Sigma90Repl.dbo.VIATURA AS V2 WITH (NOLOCK)   ON V2.CD_VIATURA = VF.CD_VIATURA ");
		select_SL.append("  AND V.FG_ATIVO = 1  ");
		select_SL.append("  WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim ");
		select_SL.append("  AND textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' "); 
		select_SL.append("  AND SL2.placaViatura = SL1.placaViatura )  ");  
		if(!kpi.isEmpty() && kpi.equals("aMonthAgo"))
		select_SL.append("  AND dataLog BETWEEN DATEADD(DAY, -30,GETDATE()) AND GETDATE()  ");
		else
		select_SL.append("  AND dataLog BETWEEN  '" + periodoInicial + "' AND '" + periodoFinal + "'   ");
		select_SL.append("  GROUP BY placaViatura,  SUBSTRING(V.NM_VIATURA,1 , 3 ) , V.NM_VIATURA , V.CD_VIATURA   ,textoLog  ");
		select_SL.append("  ORDER BY NM_VIATURA DESC  ");
		
		  
	} 
	
	    Connection connSigma = null;
	    PreparedStatement stSeqSIG = null;
		ResultSet rsSeqSIG = null;
		Connection connFusion = null;	
		PreparedStatement stSeqCL = null;
		PreparedStatement stSeqSL = null;
		ResultSet rsSeqCL = null;
		ResultSet rsSeqSL = null;
		DecimalFormat decimalFormat = new DecimalFormat("0.00"); 
		GraficoVO graficoVO = null;
		List<GraficoVO> listaGraficoVO = new ArrayList<GraficoVO>();
		List<EventosAitAlertaVO> listaEventosAitAlertaVO = new ArrayList<EventosAitAlertaVO>();
		List<EventosAlertAitLogVO> listaEventosAlertAitLogCLVO = new ArrayList<EventosAlertAitLogVO>();
		List<EventosAlertAitLogVO> listaEventosAlertAitLogSLVO = new ArrayList<EventosAlertAitLogVO>();
		List<String> contasList = new ArrayList<String>();
	try {
		
		// KPI
		if (select_EV != null ) {
			//connSigma = PersistEngine.getConnection("STAGING");
			connSigma = PersistEngine.getConnection("SIGMA90");
			
			stSeqSIG = connSigma.prepareStatement(select_EV.toString());
			rsSeqSIG = stSeqSIG.executeQuery();
			EventosAitAlertaVO eventosAitAlertaVO = null;
			while(rsSeqSIG.next()) {
				eventosAitAlertaVO = new EventosAitAlertaVO();
				eventosAitAlertaVO.setTotal(1);
				eventosAitAlertaVO.setIdCentral(rsSeqSIG.getString("ID_CENTRAL"));
				eventosAitAlertaVO.setCdViatura(rsSeqSIG.getString("CD_VIATURA"));
				eventosAitAlertaVO.setCdHistorico(rsSeqSIG.getString("CD_HISTORICO"));
				eventosAitAlertaVO.setNmViatura(rsSeqSIG.getString("NM_VIATURA"));
				
				eventosAitAlertaVO.setParticao(rsSeqSIG.getString("PARTICAO"));
				listaEventosAitAlertaVO.add(eventosAitAlertaVO);
			} 
		
			OrsegupsUtils.closeConnection(connSigma, stSeqSIG, rsSeqSIG);
			connFusion = PersistEngine.getConnection("FUSIONPROD");	
		
			stSeqCL = connFusion.prepareStatement(select_CL.toString());
			rsSeqCL = stSeqCL.executeQuery();
			  
			EventosAlertAitLogVO eventosAlertAitLogVO = null;
			
			while (rsSeqCL.next()) {
				
				String textoLog = rsSeqCL.getString("textoLog");
				for(EventosAitAlertaVO eventosAitAlertaVO2: listaEventosAitAlertaVO)
				{
					String conta = eventosAitAlertaVO2.getIdCentral() + "["+eventosAitAlertaVO2.getParticao()+"]";
					
					
					if(textoLog != null && textoLog.contains(conta) && !contasList.contains(conta))
					{  
						contasList.add(conta);
						eventosAlertAitLogVO = new EventosAlertAitLogVO();
						eventosAlertAitLogVO.setCdViatura(rsSeqCL.getString("CD_VIATURA"));
						eventosAlertAitLogVO.setKpi(rsSeqCL.getDouble("KPI"));
						eventosAlertAitLogVO.setNmViatura(rsSeqCL.getString("NM_VIATURA"));
						eventosAlertAitLogVO.setPlacaViatura(rsSeqCL.getString("placaViatura"));
						eventosAlertAitLogVO.setReg(rsSeqCL.getString("REG"));
						eventosAlertAitLogVO.setQtd(rsSeqCL.getInt("QTD"));
						eventosAlertAitLogVO.setTextoLog(rsSeqCL.getString("textoLog"));
						listaEventosAlertAitLogCLVO.add(eventosAlertAitLogVO);
					}
				}
			
			} 
			OrsegupsUtils.closeConnection(null, stSeqSL, rsSeqSL);
			stSeqSL = connFusion.prepareStatement(select_SL.toString());
			rsSeqSL = stSeqSL.executeQuery();
			int countSl= 0;
			List<String> contasAux = new ArrayList<String>();
			while (rsSeqSL.next())
			{
				
				String textoLog = rsSeqSL.getString("textoLog");
				
				if(contasList != null)
				{
					for(EventosAitAlertaVO eventosAitAlertaVO2: listaEventosAitAlertaVO)
					{
						countSl++;
						String conta = eventosAitAlertaVO2.getIdCentral() + "["+eventosAitAlertaVO2.getParticao()+"]";
						if(textoLog != null && textoLog.contains(conta) && !contasList.contains(conta))
						{
							contasList.add(conta);
							countSl = 0;
							break;
						}
						
						
					}
				}
				
				if(countSl == 0)
				{
					eventosAlertAitLogVO = new EventosAlertAitLogVO();
					eventosAlertAitLogVO.setCdViatura(rsSeqSL.getString("CD_VIATURA"));
					eventosAlertAitLogVO.setKpi(rsSeqSL.getDouble("KPI"));
					eventosAlertAitLogVO.setNmViatura(rsSeqSL.getString("NM_VIATURA"));
					eventosAlertAitLogVO.setPlacaViatura(rsSeqSL.getString("placaViatura"));
					eventosAlertAitLogVO.setReg(rsSeqSL.getString("REG"));
					eventosAlertAitLogVO.setQtd(rsSeqSL.getInt("QTD"));
					eventosAlertAitLogVO.setTextoLog(rsSeqSL.getString("textoLog"));
					listaEventosAlertAitLogSLVO.add(eventosAlertAitLogVO);
					
					
				}
			
			} 
			OrsegupsUtils.closeConnection(connFusion, stSeqSL, rsSeqSL);
			
		}
		//UNIFICAR CONSULTAS
		String nmVtr = "";
		int countQnt = 0;
	
		for(EventosAitAlertaVO eventosAitAlertaVO: listaEventosAitAlertaVO)
		{
			
			if(countQnt == 0)
			{
				nmVtr = eventosAitAlertaVO.getNmViatura();
			}
			if(nmVtr.equals(eventosAitAlertaVO.getNmViatura()))
			{
				countQnt++;
			}
			else
			{
				
				graficoVO = new GraficoVO();
				if(nmVtr != null)
				graficoVO.setNomeViatura(nmVtr);
				graficoVO.setTotalEvento(countQnt);
				if(graficoVO != null)
				listaGraficoVO.add(graficoVO);
				countQnt = 0;
				nmVtr = eventosAitAlertaVO.getNmViatura();
				countQnt++;
				
			}
			
			
		}
		
		graficoVO = new GraficoVO();
		if(nmVtr != null)
		graficoVO.setNomeViatura(nmVtr);
		graficoVO.setTotalEvento(countQnt);
		listaGraficoVO.add(graficoVO);
		
		int alerta = 0;
		double alertaTempo = 0.0;
		
		for(GraficoVO graficoVO2: listaGraficoVO)
		{
			for(EventosAlertAitLogVO eventosAlertAitLogVO: listaEventosAlertAitLogCLVO)
			{
				
				if(graficoVO2.getNomeViatura().equals(eventosAlertAitLogVO.getNmViatura()))
				{
					
					alerta++;
					alertaTempo += eventosAlertAitLogVO.getKpi();	
					if(graficoVO2.getTotalEvento() < alerta)
						break;
				}
			}
			graficoVO2.setAlertaComLigacao(alerta);
			graficoVO2.setAlertaComLigacaoTempo(alertaTempo);
			alerta = 0;
			alertaTempo = 0.0;
			
			for(EventosAlertAitLogVO eventosAlertAitLogVO: listaEventosAlertAitLogSLVO)
			{
				
				if(graficoVO2.getNomeViatura().equals(eventosAlertAitLogVO.getNmViatura()))
				{
				
					alerta++;
					alertaTempo += eventosAlertAitLogVO.getKpi();	
					if(graficoVO2.getTotalEvento() < alerta)
						break;
				}
			}
			graficoVO2.setAlertaSemLigacao(alerta);
			graficoVO2.setAlertaSemLigacaoTempo(alertaTempo);
			alerta = 0;
			alertaTempo = 0.0;
		}
		
		int flag = 0;
		for(GraficoVO vo: listaGraficoVO)
		{
				double ida = (((vo.getAlertaComLigacao() * vo.getAlertaComLigacaoTempo() ) + (vo.getAlertaSemLigacao() * vo.getAlertaSemLigacaoTempo()))+1)/vo.getTotalEvento();
				vo.setTotalEventoTempo(ida);	
		}
		
		Collections.sort(listaGraficoVO);
		for(GraficoVO vo: listaGraficoVO)
		{
			if(flag > 0)
			{
				jsonStr +=",";
			}
				//double ida = (((vo.getAlertaComLigacao() * vo.getAlertaComLigacaoTempo() ) + (vo.getAlertaSemLigacao() * vo.getAlertaSemLigacaoTempo()))+1)/vo.getTotalEvento();
				
				int total =  vo.getTotalEvento()  - ( vo.getAlertaComLigacao() + vo.getAlertaSemLigacao());
				 
				jsonStr += "['"+decimalFormat.format(vo.getTotalEventoTempo())+" - "+vo.getNomeViatura()+"',"+vo.getAlertaComLigacao()+","+vo.getAlertaComLigacao()+",'"+decimalFormat.format(vo.getAlertaComLigacaoTempo())+" min',"+
							vo.getAlertaSemLigacao()+","+vo.getAlertaSemLigacao()+",'"+decimalFormat.format(vo.getAlertaSemLigacaoTempo())+" min',"+
									total+","+ total +",'"+decimalFormat.format(vo.getTotalEventoTempo())+" IDA']";
				flag++;
		}
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
		out.print("Erro #5 - Erro ao realizar consulta no banco de dados \n"+e.getMessage());
		return;    
	} finally{
		try{
			OrsegupsUtils.closeConnection(connFusion, stSeqCL, rsSeqCL);
			OrsegupsUtils.closeConnection(connFusion, stSeqSL, rsSeqSL);
			OrsegupsUtils.closeConnection(connSigma, stSeqSIG, rsSeqSIG);
		} catch (Exception e) {
			e.printStackTrace();
		}
		jsonStr +="]";
	}
%>

  <head>
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
	 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
   
	
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
		
		
		// Create our data table out of JSON data loaded from server.
		var heightStr = 860;
		var widthStr = 1280;
		var topStr = "15%";
		var leftStr = "35%";
		var isKpi = '<%=kpi%>';
		var data =  google.visualization.arrayToDataTable(<%=jsonStr%>);
		
		
		
		if(isKpi == "aMonthAgo")
		{
		   widthStr = data.getNumberOfRows() * 45;
		   heightStr = data.getNumberOfRows() * 45;
		   topStr = "5%";
		   leftStr = "15%";
		}
		var view = new google.visualization.DataView(data);
	    view.setColumns([0, 1,
	                       { calc: "stringify",
	                         sourceColumn: 1,
	                         type: "string",
	                         role: "annotation" },2]);
		
		var options = {
		title : 'Relatório AIT/EVENTO/ALERTA.',
		subtitle: 'Relatório analítico de produtividade AIT.',
        width: widthStr,
        height: heightStr,
		is3D: true,
		colors: ['#FF0000', '#FFD700', '#32CD32'],
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
			annotations: {
			alwaysOutside:false,                                // x2, and y2 is the box. If false,
			textStyle: {
			fontName: 'Verdana',
			fontSize: 10,
			bold: true,
			color: '#005500',     // The color of the text.
			opacity: 0.8          // The transparency of the text.
		}
                            
  
    },

	vAxis:{title:'AIT',textStyle:{color: '#005500',bold: true,fontName: 'Verdana', fontSize: '10', paddingRight: '100',marginRight: '100'}},
    hAxis: { title: 'Total de eventos', textStyle: { color: '#005500', bold: true,fontName: 'Verdana',fontSize: '10', paddingRight: '100', marginRight: '100'} },
    chartArea: { left:leftStr, top: topStr, height: "90%", width: "90%"} ,
		axes: {
            y: {
              distance: {label: 'Teste 1'}, // Left y-axis.
              brightness: {side: 'right', label: 'Teste 2'} // Right y-axis.
            }
          }
       
      };
		
		
		var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
		chart.draw(data, options);

		var timeoutGauge = setTimeout("drawChart()",3600000);
			
    }
	
    
  
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
  </body>
</html>