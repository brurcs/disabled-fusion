<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<!DOCTYPE html>
<html>
<%
	String regional = request.getParameter("regional");
	String kpi = request.getParameter("kpi");
	String width = request.getParameter("width");
	if (width == null) width = "300";		
	String height = request.getParameter("height");
	if (height == null) height = "220";		
	String redFrom = request.getParameter("redFrom");
	if (redFrom == null) redFrom = "8";		
	String redTo = request.getParameter("redTo");
	if (redTo == null) redTo = "10";		
	String yellowFrom = request.getParameter("yellowFrom");
	if (yellowFrom == null) yellowFrom = "5";		
	String yellowTo = request.getParameter("yellowTo");
	if (yellowTo == null) yellowTo = "8";		
	String greenFrom = request.getParameter("greenFrom");
	if (greenFrom == null) greenFrom = "0";		
	String greenTo = request.getParameter("greenTo");
	if (greenTo == null) greenTo = "5";		
	String min = request.getParameter("min");
	if (min == null) min = "0";		
	String max = request.getParameter("max");
	if (max == null) max = "10";
	String showMajorTicks = request.getParameter("showMajorTicks");
	String majorTicks = request.getParameter("majorTicks");
	if (showMajorTicks == null || showMajorTicks.equals("yes")) 
	{
		if (majorTicks == null) 
		{
		  majorTicks = max;
		}

		int iMax = Integer.parseInt(majorTicks);
		majorTicks = "";
		for (int i=0;i<=iMax;i++) 
		{
		  if (i == 0)
		  {
		  majorTicks = ", majorTicks: ['" + i + "'";	  
		  } 
		  else 
		  {
		  majorTicks = majorTicks + ", '"+i+"'";	  
		  }
		  if (i == iMax)
		  {
			majorTicks = majorTicks + "]";
		  } 
		  
		}
	}
	else 
	{
		majorTicks = "";
	}
%>

  <head>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
  
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript"
         src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/highcharts-3d.js"></script>
	<script src="http://code.highcharts.com/highcharts-more.js"></script>
    <script type="text/javascript">
      
   $(function () {
		var jsonData = callSync("<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/gauges/getDataGaugeHighCharts.jsp?kpi=<%= kpi %>&greenFrom=<%=greenFrom%>&greenTo=<%=greenTo%>&yellowFrom=<%=yellowFrom%>&yellowTo=<%=yellowTo%>&redFrom=<%=redFrom%>&width=<%=width%>&height=<%=height%>&regional=<%= regional %>&raw=<%= "yes" %>");
		
		var resposta = jsonData.split(";");
		var tempoEvento = resposta[0];
		var qtdEvento = resposta[1];
		tempoEvento = jQuery.parseJSON(tempoEvento);
		var chart = new Highcharts.Chart({
		chart: {
				width:<%=width%>,
	        	height:<%=height%>,
				renderTo: 'container',
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
			labels: {
				step: 2,
				rotation: 'auto'
			},
            title: {
	            text: ""
	        },
			credits: {
				enabled: false,
				text: 'ORSEGUPS S.A.',
				href: 'http://www.orsegups.com.br'
			},
	        pane: {
	            startAngle: -150,
	            endAngle: 150,
	            background: [{
	                backgroundColor: {
	                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	                    stops: [
	                        [0, '#FFF'],
	                        [1, '#333']
	                    ]
	                },
	                borderWidth: 0,
	                outerRadius: '109%'
	            }, {
	                backgroundColor: {
	                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	                    stops: [
	                        [0, '#333'],
	                        [1, '#FFF']
	                    ]
	                },
	                borderWidth: 1,
	                outerRadius: '107%'
	            }, {
	                // default background
	            }, {
	                backgroundColor: '#DDD',
	                borderWidth: 0,
	                outerRadius: '105%',
	                innerRadius: '103%'
	            }]
	        },

	        // the value axis
	        yAxis: {
	            min: <%=min%>,
	            max: <%=max%>,

	            minorTickInterval: 'auto',
	            minorTickWidth: 1,
	            minorTickLength: 10,
	            minorTickPosition: 'inside',
	            minorTickColor: '#666',

	            tickPixelInterval: 30,
	            tickWidth: 2,
	            tickPosition: 'inside',
	            tickLength: 10,
	            tickColor: '#666',
	           
	            title: {
	                text: '<%=kpi%>',
					style: {
						'color': '#000000',
						'fontWeight': 'bold',
						'font-size': '280%'
					}
	            },
				labels: {
					style: {
						'color': '#000000',
						'fontWeight': 'bold',
						'font-size': '250%',
					},
					distance: -40,
					step: 5
				},
                plotBands: [{
	                from: <%=greenFrom%>,
	                to: <%=greenTo%>,
	                color: '#55BF3B', // green
					innerRadius: '95%',
	            }, {
	                from: <%=yellowFrom%>,
	                to: <%=yellowTo%>,
	                color: '#DDDF0D', // yellow
					innerRadius: '85%',
	            }, {
	                from: <%=redFrom%>,
	                to: <%=redTo%>,
	                color: '#DF5353', // red
					innerRadius: '80%',
	            }]
	        },
			plotOptions: {
            gauge: {
                
                wrap: false,
				dial: {
                    radius: '95%',
					backgroundColor: '#666666',
					borderWidth: 0,
					baseWidth: 7,
					topWidth: 1,
					baseLength: '90%', // of radius
					rearLength: '0%'
                },
                pivot: {
                    backgroundColor: '#666666',
                    radius: 3
                }
            }
            },

           series: [{
                name: '<%=kpi%>',
                data: [tempoEvento],
                dataLabels: {
                    formatter: function () {
                        var evento = this.y;
                        return '<span style="color:#339">' + tempoEvento + ' minuto(s)</span><br/>' +
                            '<span style="color:#933">' + qtdEvento + ' evento(s)</span>';
                    },
					y: 60,
                    enabled: true,
                    style: {
                        fontWeight:'bold',
                        fontSize: '32px'
                    },
					
                    backgroundColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, '#DDD'],
                            [1, '#FFF']
                        ]
                    }
                }
            }]

        },
            // Add some life
            function (chart) {
                if (!chart.renderer.forExport) {
                    setInterval(function () {
                        var point = chart.series[0].points[0];

                    	var jsonDataReturn = callSync("<%=PortalUtil.getBaseURL()%>/custom/jsp/orsegups/gauges/getDataGaugeHighCharts.jsp?kpi=<%= kpi %>&greenFrom=<%=greenFrom%>&greenTo=<%=greenTo%>&yellowFrom=<%=yellowFrom%>&yellowTo=<%=yellowTo%>&redFrom=<%=redFrom%>&width=<%=width%>&height=<%=height%>&regional=<%= regional %>&raw=<%= "yes" %>");
                    	jsonDataReturn = jsonDataReturn.split(";");
						tempoEvento = jsonDataReturn[0];
                		qtdEvento = jsonDataReturn[1];
						
						tempoEvento = jQuery.parseJSON(tempoEvento);
                        point.update(tempoEvento);
	
                    }, 120000);
                }
            });
    });
	
    
    
    </script>
  </head>

  <body>
    <div id="container" style="width: 100%; height: 100%;max-height: 1920px;float:center;"></div>
  </body>
</html>