<!DOCTYPE html>
<%@page import="com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoDateUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.custom.orsegups.medicaoContratos.OrsegupsMedicaoUtilsMensal"%>
<%@page import="java.util.ArrayList"%>
<html>
	<head>
	<%@page import="com.neomind.fusion.portal.PortalUtil"%>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="css/estilos.css">
	<title>Tarefas do fluxo de reciclagem</title>
	 <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<link rel="stylesheet" 
		href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
		<script type="text/javascript" 
		src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" 
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	</head>
	<body>
	<% 
	List<NeoObject> tarefasDeReciclagem = new ArrayList<NeoObject>();
	tarefasDeReciclagem = OrsegupsUtils.retornaTarefasDeReciclagemAtivas("",0L,0L);
	%>
	<section id=main>
		<div class="container-fluid" align="center">
			<div class="page-header">
				<h1>
					Tarefas do Fluxo de Reciclagem
				</h1>
			</div>
			<div class="row" align="center" width="90%" >
				<div class="col-md-12" >
					<div class="panel with-nav-tabs panel-primary" >
						<div class="panel-heading" style="background-color:#6b7486">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1primary" data-toggle="tab">Lista de tarefas</a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
										<table id="minhaTabela" class="table table-striped">
  											<thead>
    											<tr>
      												<th>Tarefa</th>
      											    <th>Empresa</th>
      												<th>Colaborador</th>
      												<th>Regional</th>
      												<th>Data Curso Reciclagem</th>
      												<th>A��o</th>
    											</tr>
  											</thead>
  											<tbody>
    											<% for(NeoObject obj : tarefasDeReciclagem){ 
    												EntityWrapper wReciclagem = new EntityWrapper(obj);
    												GregorianCalendar dataCurso = new GregorianCalendar();
    												dataCurso = (GregorianCalendar) wReciclagem.findField("dataCurso").getValue();
    												Long empresa = (Long)wReciclagem.findField("numemp").getValue();
    												Long numcad = (Long)wReciclagem.findField("numcad").getValue();
    												String nomfun = (String)wReciclagem.findField("nomfun").getValue();
    												String nomreg = (String)wReciclagem.findField("nomreg").getValue();
    												String numcpf = "";
    											    Long xCpf = (Long)wReciclagem.findField("numcpf").getValue();
    												numcpf = xCpf+"";
    												Long neoId = (Long)wReciclagem.findField("neoId").getValue();
    												String tarefaQ = "";
    												WFProcess processo = (WFProcess) PersistEngine.getNeoObject(WFProcess.class,new QLRawFilter("entity_neoId = " +neoId));
    												tarefaQ = processo.getCode();
    												Long datCurMil = 0L;
    												if(dataCurso != null){
    													datCurMil = dataCurso.getTimeInMillis();	
    												}
    											%>
    											<tr>
      												<td><%=tarefaQ%></td>
      												<td><%=empresa%></td>
      												<td><%=numcad%> - <%=nomfun%></td>
      												<td><%=nomreg%></td>
      												<%if(dataCurso != null){ %>
      												<td><%=NeoDateUtils.safeDateFormat(dataCurso, "dd/MM/yyyy") %></td>
      												<%}else{ %>
      												<td></td>
      												<%} %>
      												<td> 
      												<img type="button" class="btn btn-secondary" src="imagens/proibido.png" title="Cancelar tarefa" id="idTarefa"  onclick="cancelarTarefa(<%=neoId%>,this,<%=numcpf%>,<%=datCurMil%>)">
      												</td>
      											</tr>
      											<%} %>
    										</tbody>
										</table>
								 </div>
										<script type="text/javascript">
                                               var url;
                                               function cancelarTarefa(x,y,z,curso){
                                               var neoId = x;
                                               var cpf = z;
                                               	if(confirm("Deseja mesmo cancelar a tarefa?")){
		                                       		var servlet = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=cancelarReciclagem&neoId='+neoId+'&cpf='+cpf+'&curso='+curso;
		                                       		$.get(servlet, function(data) {
		                                       			var res = data.split(";");
		                                       			var i = 0;
		                                       			var msg = "";
		                                       			for(i = 0 ;i < res.length; i++){
		                                       			msg += "\n "+res[i];
		                                       			}
		                                       			if(data.indexOf("Tarefa cancelada com sucesso!") != -1){
		                                                  	RemoveTableRow(y);
		                                                  	alert(msg);
					                                     }else{
					                                    	alert(msg); 
					                                     }
		                                       		});
		                                       	 }
		                                       }
                                               (function($) {
                                               RemoveTableRow = function(handler) {
                                            	    var tr = $(handler).closest('tr');

                                            	    tr.fadeOut(400, function(){ 
                                            	      tr.remove(); 
                                            	    }); 
                                               	};
                                               })(jQuery);
                                               
                                               function pesquisa(){
                                            	   var numcad = document.getElementById('numcad').value;
                                            	   var numemp = document.getElementById('numemp').value;
                                               }
                                              
                                               $(document).ready(function(){
                                                   $('#minhaTabela').dataTable();
                                                   
                                               });
                                               $('#minhaTabela').dataTable({                              
                                            	   "oLanguage": {
                                            	      "sProcessing": "Aguarde enquanto os dados s�o carregados ...",
                                            	      "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                                            	      "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                                            	      "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                                            	      "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                                            	      "sInfoFiltered": "",
                                            	      "sSearch": "Procurar",
                                            	      "oPaginate": {
                                            	         "sFirst":    "Primeiro",
                                            	         "sPrevious": "Anterior",
                                            	         "sNext":     "Pr�ximo",
                                            	         "sLast":     "�ltimo"
                                            	      }
                                            	   }                              
                                            	  });
                         	          </script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<style>
	.campoformulario{
		max-width: 300px;
		min-width: 300px;
	}
	.campoBotao{
		max-width: 300px;
		min-width: 300px;
		background-color:#6b7486;
	    
	}
	.tipoOk { background-image:url(imagens/certo2.png);   }
	.tipoFalta { background-image:url(imagens/exclamacao2.png); }
	</style>	
</body>
</html>
