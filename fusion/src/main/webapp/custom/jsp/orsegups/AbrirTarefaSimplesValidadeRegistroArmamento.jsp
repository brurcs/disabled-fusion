<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.fusion.workflow.handler.HandlerFactory"%>
<%@page import="com.neomind.fusion.workflow.handler.TaskHandler"%>
<%@page import="com.neomind.fusion.workflow.task.rule.TaskRuleEngine"%>
<%@page import="com.neomind.fusion.workflow.TaskInstanceHelper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.TaskLog"%>
<%@page import="com.neomind.fusion.workflow.TaskInstanceActionType"%>
<%@page import="com.neomind.fusion.workflow.TaskLog.TaskLogType"%>
<%@page
	import="com.neomind.fusion.workflow.task.central.search.TaskCentralIndex"%>
<%@page import="com.neomind.fusion.workflow.BPMUtils.StatusTransferTask"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page
	import="com.neomind.fusion.workflow.exception.AssignmentException"%>
<%@page import="com.neomind.fusion.workflow.TaskInstance"%>
<%@page import="com.neomind.fusion.security.SecurityEngine"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoRunnable"%>
<%@page import="com.neomind.fusion.engine.runtime.RuntimeEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.GregorianCalendar"%>

<%@page import="org.apache.commons.logging.Log"%>
<%@page import="org.apache.commons.logging.LogFactory"%>

<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.common.NeoRunnable"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.scheduler.JobException"%>
<%@page import="com.neomind.fusion.scheduler.job.CustomJobAdapter"%>
<%@page import="com.neomind.fusion.scheduler.job.CustomJobContext"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.util.NeoDateUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>


<%@page import="java.io.*"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Abrir Tarefa Simples Validade Armamento</title>
</head>
<body>
	<%
		//Exemplo da URL a ser disparada manualmente quando necess�rio
		//http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/AbrirTarefaSimplesValidadeRegistroArmamento.jsp?dia=01&mes=01&ano=2016&diaPrazo=28&mesPrazo=00&anoPrazo=2016
		
		String dia = request.getParameter("dia");
		String mes = request.getParameter("mes");
		String ano = request.getParameter("ano");

		String diaPrazo = request.getParameter("diaPrazo");
		String mesPrazo = request.getParameter("mesPrazo");
		String anoPrazo = request.getParameter("anoPrazo");

		final GregorianCalendar dataValidade = new GregorianCalendar();
		final GregorianCalendar dataPrazo = new GregorianCalendar();

		/*dataValidade.add(Calendar.DAY_OF_MONTH, +60);
		dataValidade.set(Calendar.HOUR_OF_DAY, 0);
		dataValidade.set(Calendar.MINUTE, 0);
		dataValidade.set(Calendar.SECOND, 0);
		dataValidade.set(Calendar.MILLISECOND, 0);*/

		//para testes
		dataValidade.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dia));
		dataValidade.set(Calendar.MONTH, Integer.parseInt(mes));
		dataValidade.set(Calendar.YEAR, Integer.parseInt(ano));
		dataValidade.set(Calendar.HOUR_OF_DAY, 0);
		dataValidade.set(Calendar.MINUTE, 0);
		dataValidade.set(Calendar.SECOND, 0);
		dataValidade.set(Calendar.MILLISECOND, 0);
		System.out.println(NeoDateUtils.safeDateFormat(dataValidade, "dd/MM/yyyy HH:mm:ss.sss"));

		dataPrazo.set(Calendar.DAY_OF_MONTH, Integer.parseInt(diaPrazo));
		dataPrazo.set(Calendar.MONTH, Integer.parseInt(mesPrazo));
		dataPrazo.set(Calendar.YEAR, Integer.parseInt(anoPrazo));
		dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
		dataPrazo.set(Calendar.MINUTE, 59);
		dataPrazo.set(Calendar.SECOND, 59);
		dataPrazo.set(Calendar.MILLISECOND, 999);
		System.out.println(NeoDateUtils.safeDateFormat(dataPrazo, "dd/MM/yyyy HH:mm:ss.sss"));

		try
		{
			Collection<NeoObject> empresas = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("EEMP"));

			if (NeoUtils.safeIsNotNull(empresas) && !empresas.isEmpty())
			{
				for (NeoObject objItemEmpresa : empresas)
				{
					EntityWrapper itemEmpresaWrapper = new EntityWrapper(objItemEmpresa);
					final Long empresa = (Long) itemEmpresaWrapper.getValue("codemp");
					
					Collection<NeoObject> regionais = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCEscritorioRegional"));

					if (NeoUtils.safeIsNotNull(regionais) && !regionais.isEmpty())
					{
						for (NeoObject objItemRegional : regionais)
						{
							EntityWrapper itemRegionalWrapper = new EntityWrapper(objItemRegional);
							final Long regional = (Long) itemRegionalWrapper.getValue("codigo");

							QLGroupFilter filter = new QLGroupFilter("AND");
							QLEqualsFilter filtroData = new QLEqualsFilter("dataValidade", dataValidade);
							QLEqualsFilter filtroRegional = new QLEqualsFilter("regionalResponsavel.usu_codreg", regional);
							QLEqualsFilter filtroEmpresa = new QLEqualsFilter("empresa.codemp", empresa);
							QLGroupFilter statusGroupFilter = new QLGroupFilter("OR");
							QLEqualsFilter filtroSituacaoAtivo = new QLEqualsFilter("situacao.codigoSituacao", 1L);
							QLEqualsFilter filtroSituacaoVencido = new QLEqualsFilter("situacao.codigoSituacao", 4L);
							statusGroupFilter.addFilter(filtroSituacaoAtivo);
							statusGroupFilter.addFilter(filtroSituacaoVencido);
							filter.addFilter(filtroData);
							filter.addFilter(filtroRegional);
							filter.addFilter(filtroEmpresa);
							filter.addFilter(statusGroupFilter);

							final Collection<NeoObject> armas = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ARMArmamento"), filter, -1, -1, "numeroRegistro asc");
							PersistEngine.getEntityManager().flush();

							if (NeoUtils.safeIsNotNull(armas) && !armas.isEmpty())
							{
								final NeoRunnable work = new NeoRunnable()
								{
									public void run() throws Exception
									{
										//C�digo da sub-transa��o
										abrirTarefa(armas, regional, dataValidade, dataPrazo);
									}
								};
								try
								{
									PersistEngine.managedRun(work);
								}
								catch (final Exception e)
								{
									System.out.println("OK");
								}
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			System.out.print("OK");
		}
	%>

	<%!public void abrirTarefa(Collection<NeoObject> armas, Long regional, GregorianCalendar dataValidade, GregorianCalendar dataPrazo)
	{

		String empresa = "";
		
		StringBuilder html = new StringBuilder();
		html.append("Prezado Gestor,<br>");
		html.append("Por gentileza, providenciar a renova��o dos registros das armas em anexo, na qual nosso sistema aponta o vencimento em " + NeoDateUtils.safeDateFormat(dataValidade, "dd/MM/yyyy") + "<br><br>");

		try
		{
			String filename = "\\\\ssoovt09\\f$\\Sistemas\\Fusion\\Temp\\ListaDeArmamentosVencendo.xls";
			FileOutputStream fileOut = new FileOutputStream(filename);
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Armamentos Vencendo");

			HSSFRow rowhead = sheet.createRow((short) 0);
			rowhead.createCell(0).setCellValue("Esp�cie");
			rowhead.createCell(1).setCellValue("Marca");
			rowhead.createCell(2).setCellValue("Calibre");
			rowhead.createCell(3).setCellValue("N�mero Arma");
			rowhead.createCell(4).setCellValue("Lota��o");
			rowhead.createCell(5).setCellValue("Situa��o");
			rowhead.createCell(6).setCellValue("Validade");

			int i = 1;
			for (NeoObject objItemArma : armas)
			{
				EntityWrapper itemArmaWrapper = new EntityWrapper(objItemArma);
				empresa = String.valueOf(itemArmaWrapper.findValue("empresa.codemp"));
				String especie = (String) itemArmaWrapper.findValue("especie.descricaoEspecie");
				String marca = (String) itemArmaWrapper.findValue("marca.descricaoMarca");
				String calibre = (String) itemArmaWrapper.findValue("calibre");
				String numeroArma = (String) itemArmaWrapper.findValue("numeroArma");
				String lotacao = (String) itemArmaWrapper.findValue("posto.posto.nomloc");
				String situacao = (String) itemArmaWrapper.findValue("situacao.descricaoSituacao");
				String validade = NeoDateUtils.safeDateFormat((GregorianCalendar) itemArmaWrapper.findValue("dataValidade"), "dd/MM/yyyy"); 

				HSSFRow row = sheet.createRow((short) i);
				row.createCell(0).setCellValue(especie);
				row.createCell(1).setCellValue(marca);
				row.createCell(2).setCellValue(calibre);
				row.createCell(3).setCellValue(numeroArma);
				row.createCell(4).setCellValue(lotacao);
				row.createCell(5).setCellValue(situacao);
				row.createCell(6).setCellValue(validade);

				i++;
			}
			workbook.write(fileOut);
			fileOut.close();
		}
		catch (Exception ex)
		{
			System.out.println(ex);
		}

		NeoFile anexo = OrsegupsUtils.criaNeoFile(new File("\\\\ssoovt09\\f$\\Sistemas\\Fusion\\Temp\\ListaDeArmamentosVencendo.xls"));

		NeoPaper responsavel = new NeoPaper();
		String executor = "";

		if (regional > 0L)
		{
			responsavel = OrsegupsUtils.getPapelGerenteRegional(regional);
			if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
			{
				for (NeoUser user : responsavel.getUsers())
				{
					executor = user.getCode();
					break;
				}
			}
		}
		else
		{
			executor = "luana.martins";
		}
		String solicitante = "luana.martins";

		/*dataValidade.add(Calendar.DAY_OF_MONTH, -15);*/
		/*dataValidade = OrsegupsUtils.getSpecificWorkDay(dataValidade, 1L);*/

		String titulo = "Vencimento de Registro de Armamento da Empresa " + empresa + " - " + NeoDateUtils.safeDateFormat(dataValidade, "dd/MM/yyyy");
		String descricao = NeoUtils.safeString(html.toString());

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", dataPrazo, anexo);
	}%>
</body>
</html>