<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.custom.orsegups.fap.utils.FapUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.fap.utils.FAPEmailUtils"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

		<% 				
			String codes = request.getParameter("codigos");	
			List<String> codeList = Arrays.asList(codes.split(","));
		
			for(String code : codeList){
				QLEqualsFilter filterTarefa = new QLEqualsFilter("wfprocess.code",code );
			    NeoObject fapAgrupadora = PersistEngine.getObject(AdapterUtils.getEntityClass("FAPAutorizacaoDePagamento"), filterTarefa);
				EntityWrapper ewFap = new EntityWrapper(fapAgrupadora);
				
				Long codigoFornecedor = ewFap.findGenericValue("fornecedor.codfor");
				QLEqualsFilter filterFornecedor = new QLEqualsFilter("codfor",codigoFornecedor );
			    NeoObject fornecedor = PersistEngine.getObject(AdapterUtils.getEntityClass("ETABFOR"), filterFornecedor);
			    
			    EntityWrapper wrapperFornecedor = new EntityWrapper(fornecedor);
			    String emailForn = wrapperFornecedor.findGenericValue("fornecedor.intnet");
			   
			    String nomeRota = ewFap.findGenericValue("rotaSigma.rotaSigma.nm_rota");
			    String regional = FapUtils.getSiglaRegional(nomeRota);
			    NeoUser aprovador = FapUtils.getAprovador(regional);
			    String emailAprovador = aprovador.getEmail();
			    
			    String corpoEmail = FAPEmailUtils.autorizacaoFaturamentoFapTatico(fapAgrupadora);
			    FAPEmailUtils.enviaEmailFapTatico(corpoEmail, emailForn, emailAprovador);
			}
		%>
</body>
</html>