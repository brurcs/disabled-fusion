<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.vo.DBOrdemVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.SigmaUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	String cameras = request.getParameter("cameras");
%>

<html>
<head>

<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.css"
	media="screen" />
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.mask.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.callcenter.validade.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.qtip-1.0.0-rc3.min.js"></script>
<style type="text/css">
#bodyId div {
	color: #08285a;
	font-family: Tahoma, Arial, Verdana, "Trebuchet MS";
}

#detalhesContrato ul {
	margin-top: 5px;
	margin-left: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
	padding: 0;
}

#detalhesContrato ul li {
	margin: 0;
	padding: 0;
	float: left;
	width: 50%;
	list-style: none;
	font-size: 12px;
}

#detalhesContrato ul li span {
	font-weight: bold;
}

#detalhesContrato ul fieldset {
	border: 0;
	background: #e7efff;
	margin-bottom: 5px;
	padding-bottom: 10px;
	text-indent: 10px;
}

#detalhesContrato ul fieldset legend {
	font-weight: bold;
	font-size: 12px;
	background: transparent;
}

#buscaAvancadaWest ul {
	margin-top: 0px;
	margin-left: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	padding: 0;
}

#buscaAvancadaWest ul li {
	margin: 0;
	padding: 0;
	float: left;
	width: 100%;
	list-style: none;
	font-size: 12px;
}

#buscaAvancadaWest ul li span {
	font-weight: normal;
	width: 100px;
	display: inline-block;
}

#buscaAvancadaWest ul fieldset {
	border: 0;
	background: #e7efff;
	margin-bottom: 5px;
	padding-bottom: 10px;
	text-indent: 0px;
}

.destaque {
	font-weight: bold;
	font-size: 12px;
	color: #dd0005;
}

.ramal {
	padding: 5px;
	font-weight: bold;
	font-size: 12px;
}

.externalNumber {
	padding: 5px;
	font-weight: bold;
	font-size: 12px;
}

.modal {
	display: none;
	position: fixed;
	z-index: 1000;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background: rgba(255, 255, 255, .8)
		url('<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/images/loading.gif')
		50% 50% no-repeat;
}
/* When the body has the loading class, we turn
         the scrollbar off with overflow:hidden */
body.loading {
	overflow: hidden;
}

.helpContent /* Anytime the body has the loading class, our
         modal element will be visible */ 
         body.loading .modal {
	display: block;
}

.invisible {
	display: none;
}

.visible {
	display: block;
}

.left {
	float: left;
	margin-right: 3px;
}

.fieldContainer {
	display: inline-block;
}

.spanValue {
	margin-top: 3px;
}

.ico {
	width: 12px;
}

.calling {
	font-weight: bold;
	color: red;
}

#fm {
	margin: 0;
	padding: 10px 30px;
}

.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}

.fitem {
	margin-bottom: 5px;
}

.fitem label {
	display: inline-block;
	width: 80px;
}

.input {
	border: 1px solid #08285a;
	padding: 2px;
}

.textarea {
	border: 2px solid #765942;
	border-radius: 10px;
}

#fm {
	margin: 0;
	padding: 10px 30px;
}

.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}

.fitem {
	margin-bottom: 5px;
}

.fitem label {
	display: inline-block;
	width: 80px;
}

.popover {
	z-index: 9900 !important;
}
</style>
</head>
<body style="color: black;background-color: black;">
	<div style="width: 99%; padding: 2px; height: auto;color: black;background-color: black;" align="center">
		<form id="fmAlertas" method="post">
			<div id="imagens"  style="width: 99%; padding: 2px; height: auto;color: black;background-color: black;">

			</div>
    		<br/>
    		<br/>
    		<div style="display:none">
	    		<a href="#" onclick="searchPopUp()" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:80px">Buscar</a>
	    		<a href="#" onclick="playPopUp()"  class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px">Play</a>
	    		<a href="#" onclick="stopPopUp()"  class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" style="width:80px">Stop</a>
    		</div>
		</form>
	</div>


<script type="text/javascript">
	
	var camPopUp="<%=cameras%>";

	var baseUrlPopUp = "http://ssootv05/";
    var usernamePopUp = "admin";
    var passwordPopUp = "seventh";

    var cameraIdsPopUp = camPopUp.split(",");
    var searchingPopUp = false;
    var playingPopUp = false;
    var imgs;

	$(document).ready(function() {
		image();
		
	});
	
	function image()
	{

	    for (var i = 0; i < cameraIdsPopUp.length; i++) {
	    	var img=document.createElement("img");
		    img.id="Img"+i;
		    var foo = document.getElementById("imagens");
		    foo.appendChild(img);
		}
	    searchPopUp();
		playPopUp();
		
	}
	
    function make_base_authPopUp(user, password) {
        var tok = user + ':' + password;
        var hash = btoa(tok);
        return "Basic " + hash;
    }

    function ajaxRequestxhrPopUp(camNumber) {

        var xhrPopUp = new XMLHttpRequest();
        var camIdPopUp = cameraIdsPopUp[camNumber];
        var imgPopUp = document.getElementById('Img'+camNumber);

        if ((!playingPopUp) && (!searchingPopUp))
            return;

        // Retrieve a jpeg image using XMLHttpRequest 
        // Based on http://www.html5rocks.com/en/tutorials/file/xhrPopUp2/

        xhrPopUp.open("GET", baseUrlPopUp + "camera.cgi?camera=" + camIdPopUp + "&resolucao=320x240&qualidade=100", true);
        console.log(baseUrlPopUp + "camera.cgi?camera=" + camIdPopUp + "&resolucao=320x240&qualidade=100");
        // Ask for the result as an ArrayBuffer.
        xhrPopUp.responseType = "arraybuffer";
        xhrPopUp.setRequestHeader('Authorization', make_base_authPopUp(usernamePopUp, passwordPopUp));

        xhrPopUp.onload = function (e) {
            // This could be used to parse headers (including image time)
            // console.log(this.getAllResponseHeaders());

            // This could be used to parse errors from the API. If the content-type is text, treat the response as a text error.
            var contentType = this.getResponseHeader('content-type');
            console.log(contentType);
            if (contentType.indexOf("text/plain") > -1) {
                var errorText = String.fromCharCode.apply(null, new Uint8Array(this.response));
                
                console.log(errorText);
                if ((searchingPopUp) || (playingPopUp)) {
                    camNumber++;

                    if (camNumber >= cameraIdsPopUp.length) {
                        if (searchingPopUp)
                            searchingPopUp = false;
                        camNumber = 0;
                    }
                }

                if ((searchingPopUp) || (playingPopUp))
                    ajaxRequestxhrPopUp(camNumber);

            }
            else
                if (contentType.indexOf("image/jpeg") > -1) {
                    // Obtain image date/time
                    var dateTime = this.getResponseHeader('Last-Modified');
                   

                    // Obtain a blob: URL for the image data.
                    var arrayBufferView = new Uint8Array(this.response);
                    var blob = new Blob([arrayBufferView], { type: "image/jpeg" });
                    var urlCreator = window.URL || window.webkitURL;
                    var imageUrl = urlCreator.createObjectURL(blob);
                    
                    imgPopUp.onload = function() {
                        var urlCreator = window.URL || window.webkitURL;
                        urlCreator.revokeObjectURL(this.src); // Free memory
                    };
                    imgPopUp.src = imageUrl;
                    if ((searchingPopUp) || (playingPopUp)) {
                        camNumber++;

                        if (camNumber >= cameraIdsPopUp.length) {
                            if (searchingPopUp)
                                searchingPopUp = false;
                            camNumber = 0;
                        }
                    }

                    if ((searchingPopUp) || (playingPopUp))
                        ajaxRequestxhrPopUp(camNumber);

                }
        };

        xhrPopUp.send();

    }

    function searchPopUp() {
        searchingPopUp = true;
        playingPopUp = false;

        ajaxRequestxhrPopUp(0);
    };

    function playPopUp() {
        searchingPopUp = false;
        playingPopUp = true;

        ajaxRequestxhrPopUp(0);
    }


    function stopPopUp() {
        searchingPopUp = false;
        playingPopUp = false;
    }
</script>

</body>
</html>
