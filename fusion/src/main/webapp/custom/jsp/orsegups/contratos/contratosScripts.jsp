<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.css"
	media="screen" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jquery/jquery.easyui.min.js"></script>
	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>	
 	<script src="js/jquery.growl.js" type="text/javascript"></script>
<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/contratos-scripts.css">
<script src="js/contratos-scripts.js"></script>

<title>Contratos</title>
</head>
<body>
<div class="alert alert-danger collapse" id="warning-alert" role="alert">
 		<strong>Erro!</strong> Usuário de Origem ou Destino Não encontrados.
</div>
	
<div class="alert alert-success collapse" id="success-alert" role="alert">
  		<strong>Sucesso!</strong> Processo realizado com Sucesso.
</div>

<div class="container">    
	<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
    	<div class="panel panel-info" >
        	<div class="panel-heading">
                        <div class="panel-title">Contrato</div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
	                
	            <form id="loginform" method="post" class="form-horizontal" role="form">
	            
	            
	            
	            	<div style="margin-bottom: 25px" class="input-group">
	                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
	                    <input id="contrato" name="contrato" class="form-control" autocomplete="off" type="text" placeholder="Contrato">                                        
	               </div> 
	                        
	               	                 
	          
	                      
	       
	
	                    <div style="margin-top:10px" class="form-group" align="center">
	                        <!-- Button -->
	
	                        <div class="col-sm-12 controls">
	                          <a id="btn-login" onclick="getListaContratos()" class="btn btn-success" data-toggle="modal" data-target="#processing-modal">Processar  </a>                     
	                        </div>
	                    </div>
	
				
	                    
                </form>  
                
                        

        	</div> 
        	                    
		</div> 
		
	</div>			
<div id="Status" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">  
	<div class="panel panel-info" >
		<div class="panel-heading">
           	<div class="panel-title">Status RMC</div>
        </div> 
       		   
 		   <div style="padding-top:30px" class="panel-body" >
	   		<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
       	   
			<div style="margin-bottom: 25px" class="input-group">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                 <input id="empresa" name="empresa" class="form-control" autocomplete="off" type="text" field="empresa" placeholder="Empresa" disabled>
                                                        
           	</div> 
           	
           	<div style="margin-bottom: 25px" class="input-group">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                 <input id="filial" name="filial" class="form-control" autocomplete="off" type="text" field="filial" placeholder="Filial" disabled>
                                                        
           	</div> 
           	
           	<div style="margin-bottom: 25px" class="input-group">
	                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	                    <input id="contratoRmc" name="contratoRmc" class="form-control" autocomplete="off" type="text" placeholder="Contrato RMC" disabled>                                        
	       </div> 
	       
	       <div style="margin-bottom: 25px" class="input-group">
	       	<span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>						
			<select id="cbNumRmc" 
				class="form-control campoformulario">
				
			</select>
		  </div>
		  
		   <div style="margin-top:10px" class="form-group" align="center">
	                        <!-- Button -->
	
	                        <div class="col-sm-12 controls">
	                          <a id="btn-login" onclick="getAlterarStatus()" class="btn btn-success" data-toggle="modal" data-target="#processing-modal">Alterar Status</a>                     
	                        </div>
	      </div>
         
          </div>  
	</div>	
</div>			
    
<div class="modal modal-static fade" id="processing-modal" name="processing-modal" role="dialog" aria-hidden="true" >
  <div class="modal-dialog"  >
      <div class="modal-content"   >
          <div class="modal-body"  >
              <div class="text-center"   >	               
			<img src="img/loading.gif" style="height: 80px;" class="icon" />
            			<h4>Processando... </h4>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>