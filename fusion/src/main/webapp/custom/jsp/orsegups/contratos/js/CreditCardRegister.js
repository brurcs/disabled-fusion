function cadastrar(codigoCliente) {
	
    var xhr = new XMLHttpRequest();
    
    const urlConsulta = "https://intranet.orsegups.com.br/fusion/services/SapiensServices/consultarCartaoDeCredito/C/";
    const urlCadastro = "https://intranet.orsegups.com.br/fusion/services/SapiensServices/cadastrarCartaoDeCredito/I/";     
    const barra = '/';
       
    var retornoConsulta = '';
    var retornoCadastro = '';
    
    var mensagemRetorno = '';
    var divMensagemRetorno = document.getElementById('divMensagemRetorno');
    var paragrafoMensagemRetorno = document.getElementById('mensagemRetorno');
    
	var numeroCartao = document.getElementById('numeroCartao').value;
	var nomeCartao = document.getElementById('nomeCliente').value;
	var nomeBandeira = document.getElementById('bandeiraCartao').value;
	var dataValidade = document.getElementById('dataValidade').value;
	var codigoVerificacao = document.getElementById('codigoVerificacao').value;

    dataValidade = dataValidade.replace("/", "'");
     
	/* Início - Bloco de consulta*/
	xhr.open('GET', urlConsulta + codigoCliente, false);	
    
	xhr.onload = function(){
		
		console.log("Consultando!");
		retornoConsulta = xhr.response;
        console.log(retornoConsulta);
        
    };
    xhr.send();
    
    // retornoConsulta = 0 não achou, 1 achou, 2 erro ao executar consulta
    
    switch(retornoConsulta){
    case "1":
    	mensagemRetorno = "Cliente já possui cartão cadastrado";
    	divMensagemRetorno.style.display = "block";
    	break;
    case "2":
    	mensagemRetorno = "Problema ao verificar se o cliente já possui cartão cadastrado";
    	divMensagemRetorno.style.display = "block";
    	break;
    }
    /* Fim - Bloco de consulta */
   
    
    /* Início - Bloco de cadastro */
	if (retornoConsulta === "0") {

		console.log("Cadastrando!");

		let
		xhr2 = new XMLHttpRequest();
		/*xhr2.open('POST', urlCadastro + codigoCliente + barra + numeroCartao
				+ barra + dataValidade + barra + nomeCartao + barra
				+ codigoVerificacao + barra + nomeBandeira, false);*/
		
		xhr2.open('POST', urlCadastro + codigoCliente + barra + numeroCartao
		+ barra + dataValidade + barra + nomeCartao + barra	+ codigoVerificacao);
		
		xhr2.onload = function() {

			retornoCadastro = xhr2.response;
			console.log(retornoCadastro);
		};
		xhr2.send();
	}
    
    
    switch(retornoCadastro){
    case "0":
    	mensagemRetorno =  "Não foi possível efetuar o cadastro. Verifique o preenchimento dos campos";
    	divMensagemRetorno.style.display = "block";
    	break;
    case "1":
    	mensagemRetorno =  "Cartão cadastrado com sucesso";
    	divMensagemRetorno.style.display = "block";
    	divMensagemRetorno.style.background = "linear-gradient(to bottom, #bce07b 0%, #b2dc61 51%, #b0d962 100%)";
    	divMensagemRetorno.style.backgroundColor = "#b1da62";
    	divMensagemRetorno.style.borderColor = "#78932e";
    	divMensagemRetorno.style.color = "#4b6c00";
    	break;
    }
    
    /* Fim - Bloco de cadastro */
    paragrafoMensagemRetorno.innerHTML = mensagemRetorno;  
}

function showPassword(obj) {
	var obj = document.getElementById('codigoVerificacao');
	obj.type = "text";
}

function hidePassword(obj) {
	var obj = document.getElementById('codigoVerificacao');
	obj.type = "password";
}

function formatDateValid(obj){
	$('#dataValidade').on('keypress change', function () {
		$(this).val(function (index, value) {
			return value.replace(/\W/gi, '').replace(/(.{2})/, '$1/');
		});
	});
}

