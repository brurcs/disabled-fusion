<%@page import="org.hibernate.property.MapAccessor"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.11&sensor=false"
	type="text/javascript"></script>

<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0px;
	padding: 0px
}

#map_canvas {
	height: 100%
}

.labelsCars {
	color: black;
	background-color: white;
	font-family: "Tahoma";
	font-size: 10px;
	font-weight: bold;
	text-align: center;
	width: 24px;
	height: 13px;
	border: 1px solid black;
	white-space: nowrap;
}

.labelsEvents {
	color: black;
	background-color: white;
	font-family: "Tahoma";
	font-size: 10px;
	font-weight: bold;
	text-align: center;
	width: 28px;
	height: 13px;
	border: 1px solid black;
	white-space: nowrap;
}

.distanceLabels {
	color: black;
	background-color: white;
	font-family: "Tahoma";
	font-size: 10px;
	font-weight: bold;
	text-align: center;
	width: 40px;
	height: 13px;
	border: 1px solid black;
	white-space: nowrap;
}

.window_black_bold {
	font-family: "Tahoma";
	color: black;
	background-color: white;
	font-size: 12px;
	font-weight: bold
}

.window_black {
	font-family: "Tahoma";
	color: black;
	background-color: white;
	font-size: 12px;
}
</style>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript"
	src="https://maps.google.com/maps/api/js?sensor=false&language=pt-BR&region=BR"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/markerwithlabel.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/keydragzoom.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.qtip-1.0.0-rc3.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/clock/jquery.jclock.js"></script>

 <% 
	// seguranca: podem acessar usuarios logados e maquinas da rede Orsegups ou Apple devices com passagem de parametro secreto
	String remoteIP = request.getRemoteAddr();
	System.out.println(remoteIP);
	String liberaAcesso = request.getParameter("liberaAcesso");
	
	boolean accessGranted = MapaSecurityAcess.verificaAcessoUrl(remoteIP, liberaAcesso);

	String action = request.getParameter("action");
	String regional = request.getParameter("regional");
	String ramalOrigem = request.getParameter("ramalOrigem");
	String emUso = request.getParameter("emUso");
	String aplicacao = request.getParameter("aplicacao");
	String atualizaPosicao = request.getParameter("atualizaPosicao");
	String showDistanceOnMapStr = request.getParameter("showDistance");
	String filtrosConta = request.getParameter("filtrosConta");
	String filtrosNotConta = request.getParameter("filtrosNotConta");
	String x8IgnoreTags = request.getParameter("x8IgnoreTags");
	String showLogStr = request.getParameter("showLog");

	Boolean showLog = false;
	if(showLogStr != null && showLogStr.equalsIgnoreCase("true"))
	{
		showLog = true;
	}
	
	
	
	Boolean showDistanceOnMap = false;
	if(showDistanceOnMapStr != null && showDistanceOnMapStr.equalsIgnoreCase("true"))
	{
		showDistanceOnMap = true;
	}

	if(atualizaPosicao != null)
	{
		atualizaPosicao = atualizaPosicao.toLowerCase();
	}
	else
	{
		atualizaPosicao = "";
	}
%>

<script type="text/javascript">
	// Define your locations: HTML content for the info window, latitude, longitude
	var locations =  callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsRouteServlet?action=rotaViatura");

	// Setup the different icons and shadows
	var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

	var icons = [ iconURLPrefix + 'red-dot.png',
			iconURLPrefix + 'green-dot.png', iconURLPrefix + 'blue-dot.png',
			iconURLPrefix + 'orange-dot.png', iconURLPrefix + 'purple-dot.png',
			iconURLPrefix + 'pink-dot.png', iconURLPrefix + 'yellow-dot.png' ]
	var icons_length = icons.length;

	var shadow = {
		anchor : new google.maps.Point(15, 33),
		url : iconURLPrefix + 'msmarker.shadow.png'
	};

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom : 10,
		center : new google.maps.LatLng(-27.59694, -48.54889),
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		mapTypeControl : false,
		streetViewControl : false,
		panControl : false,
		zoomControlOptions : {
			position : google.maps.ControlPosition.LEFT_BOTTOM
		}
	});

	var infowindow = new google.maps.InfoWindow({
		maxWidth : 160
	});

	var marker;
	var markers = new Array();

	var iconCounter = 0;

	// Add the markers and infowindows to the map
	for ( var i = 0; i < locations.length; i++) {
		marker = new google.maps.Marker(
				{
					position : new google.maps.LatLng(locations[i][1],
							locations[i][2]),
					map : map,
					icon : icons[iconCounter],
					shadow : shadow
				});

		markers.push(marker);

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infowindow.setContent(locations[i][0]);
				infowindow.open(map, marker);
			}
		})(marker, i));

		iconCounter++;
		// We only have a limited number of possible icon colors, so we may have to restart the counter
		if (iconCounter >= icons_length) {
			iconCounter = 0;
		}
	}

	function AutoCenter() {
		//  Create a new viewpoint bound
		var bounds = new google.maps.LatLngBounds();
		//  Go through each...
		$.each(markers, function(index, marker) {
			bounds.extend(marker.position);
		});
		//  Fit these bounds to the map
		map.fitBounds(bounds);
	}
	AutoCenter();
</script>

</head>
<body id="bodyId">
	<div id="map_canvas" style="width: 100%; height: 100%"></div>
	<div class="jclock" id="jclockId"></div>
	<div id="objectsContainer" style="visibility: hidden;"></div>
</body>
</html>
