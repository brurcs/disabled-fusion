<%@page import="com.neomind.fusion.custom.orsegups.maps.vo.EventoEsperaVO"%>
<%@page import="br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebServiceProxy"%>
<%@page import="br.com.segware.sigmaWebServices.webServices.EventoRecebido"%>
<%@page import="java.text.ParseException"%>
<%@page import="javax.swing.text.MaskFormatter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="org.apache.commons.mail.HtmlEmail"%>
<%@page import="com.neomind.fusion.engine.FusionRuntime"%>
<%@page import="com.neomind.fusion.mail.MailSettings"%>
<%@page
	import="br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServiceProxy"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.net.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEvento");
	log.warn("##### INICIAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA FILA FUSION - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	List<NeoObject> eventosList = null;
	String returnFromAccess = "";
	List<EventoEsperaVO> list = null;
	String eventosAgrupados = "";

	try
	{
		 
		QLGroupFilter filterAnd = new QLGroupFilter("AND");
		eventosList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAnd, -1, -1, "codigo ASC");
		list = new ArrayList<EventoEsperaVO>();
		EventoEsperaVO eventosObj = null;
		List<String> listaEventos = new ArrayList<String>();
	    if (eventosList != null && !eventosList.isEmpty())
	    {
	    	listaEventos = verificaEventoHistorico();
	    	for(NeoObject neoObject: eventosList)
			{
			
		    	NeoObject noPS  = (NeoObject) neoObject;
							
				EntityWrapper psWrapper = new EntityWrapper(noPS);
				
				String cdHistorico = (String) psWrapper.getValue("codHistorico");
				String cdRota = (String) psWrapper.getValue("codRota");
				GregorianCalendar dataRetorno = (GregorianCalendar) psWrapper.getValue("dataEspera");
				String cdCliente = (String) psWrapper.getValue("cdCliente");
				String cdViatura = (String) psWrapper.getValue("cdViaturaSigma");
				
				
				if(NeoUtils.safeIsNotNull(list) && !eventosAgrupados.contains(cdViatura)  && listaEventos != null && !listaEventos.contains(cdViatura) )
				{
					eventosObj = new EventoEsperaVO();
					eventosObj.setCodHistorico(cdHistorico);
					eventosObj.setCodRota(cdRota);
					eventosObj.setCliente(cdCliente);
					eventosObj.setCodigoViatura(cdViatura);
					eventosObj.setDataEspera(NeoUtils.safeDateFormat(dataRetorno, "dd/MM/yyyy HH:mm:ss"));
					eventosAgrupados += cdViatura+":";
					list.add(eventosObj);
				}
			}
			
		
	    
	    for(EventoEsperaVO esperaVO: list)
	    {
			String idViaturaStr = (String) esperaVO.getCodigoViatura();
			String idEventoStr = (String) esperaVO.getCodHistorico();
			Long idEvento = Long.parseLong(idEventoStr);
			Long idViatura = Long.parseLong(idViaturaStr);
			returnFromAccess = executaServicoSegware(idViatura, idEvento);
			if (NeoUtils.safeIsNotNull(returnFromAccess) && (returnFromAccess.contains("ACK") || returnFromAccess.contains("EVENTO_NAO_ENCONTRADO")) &&  NeoUtils.safeIsNotNull(idEventoStr))
			{
				QLGroupFilter filterAnds = new QLGroupFilter("AND");
				filterAnds.addFilter(new QLEqualsFilter("codHistorico", idEventoStr));
				
				NeoObject evento = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SIGMAEventosFilaEspera"), filterAnds);
				
				if (evento != null)
				{
				
				PersistEngine.remove(evento);
				log.warn(" RETORNO ROTINA DESLOCAMENTO DE EVENTO EM ESPERA FILA FUSION - HISTORICO : "+idEvento+" VIATURA : "+idViaturaStr);
				
				}
				
				if (idViaturaStr != null && !idViaturaStr.isEmpty())
				{
				   List<String> eventosListFila = getEventosFila(idViaturaStr);
				        

					if(eventosListFila != null && !eventosListFila.isEmpty())
					{
						Long cont = 1L;
						for (String v : eventosListFila)
						{
							
						    boolean updateEvent = updateEventCode(v, cont);

						    int updateCount = 0;

						    while (!updateEvent && updateCount < 2) {
								updateEvent = this.updateEventCode(v, cont);
								updateCount++;
						    }
						    
							cont++;
							
						}
					}
				}
				
				
				
			}
			
			if(returnFromAccess.equals("ACK"))
			{
				conn = PersistEngine.getConnection("SIGMA90");

				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT  h.CD_EVENTO,");
				sql.append(" c.ID_CENTRAL, c.PARTICAO,  c.CD_CLIENTE, c.ID_EMPRESA, c.STSERVIDORCFTV  ");
				sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
				sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
				sql.append(" INNER JOIN rota r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
				sql.append(" INNER JOIN dbCODE cuc WITH (NOLOCK) ON cuc.ID_CODE = h.CD_CODE ");
				sql.append(" WHERE h.CD_HISTORICO = ? AND h.CD_HISTORICO_PAI IS NULL ");
				sql.append(" ORDER BY h.DT_RECEBIDO ");


				pstm = conn.prepareStatement(sql.toString());
				pstm.setString(1, idEventoStr);
				rs = pstm.executeQuery();
				int contador = 0;

				while (rs.next())
				{
					String central = rs.getString("ID_CENTRAL");
					String particao = rs.getString("PARTICAO");
					String evento = rs.getString("CD_EVENTO");
			        String cliente = rs.getString("CD_CLIENTE");
			        String empresa = rs.getString("ID_EMPRESA");
			        String stservcftv = rs.getString("STSERVIDORCFTV");
			      
					returnFromAccess = executaServicoSegware(idViatura, idEvento);
					
					if(returnFromAccess.equals("ACK") &&  stservcftv.equals("1") && !verificaEventoHistoricoView(cliente))
					{

						//returnFromAccess = executaServicoSegware(evento, cliente, empresa, central, particao);
					  //log.warn("##### ROTINA DESLOCAMENTO DE EVENTO AUTOMATICO GERAR EVENTO XVID :"+returnFromAccess+" Cliente : "+cliente+" : - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

					}
				}
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
		
	    }
	    }
	    
		log.warn("##### EXECUTAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA FILA FUSION HISOTRICO : "+returnFromAccess+" -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	}
	catch (Exception e)
	{
		e.printStackTrace();
		log.error("##### ERRO ROTINA DESLOCAMENTO DE EVENTO EM ESPERA FILA FUSION -  Data: " + e.getStackTrace() + " " + e.getMessage() + " " + e.getCause() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
	}
	finally
	{
		try
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO ROTINA DESLOCAMENTO DE EVENTO EM ESPERA FILA FUSION -  Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		log.warn("##### FINALIZAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA FILA FUSION -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
%>

<%!private List<String> getEventosFila(String cdViatura){
    
    final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEvento");
    
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	List<String> codigosEventosFila = new ArrayList<String>();

	try {

	    String sql = "SELECT codHistorico, neoId FROM D_SIGMAEventosFilaEspera WITH(NOLOCK) WHERE cdViaturaSigma=" + cdViatura + " ORDER BY codigo ASC";

	    conn = PersistEngine.getConnection("");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    while (rs.next()) {
			String codigoHistorico = rs.getString(1);
			codigosEventosFila.add(codigoHistorico);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("##### rotinaDeslocarEvento : Erro ao buscar eventos na fila do AIT -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}
	
	return codigosEventosFila;
    
}
%>

<%!private boolean updateEventCode(String cdHistorico, Long codigo) {
    final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEvento");
    
	Connection conn = null;
	PreparedStatement pstm = null;

	try {
	    String sql = "UPDATE D_SIGMAEventosFilaEspera SET codigo = ? WHERE codHistorico = ?";

	    conn = PersistEngine.getConnection("");

	    pstm = conn.prepareStatement(sql);

	    pstm.setLong(1, codigo);
	    pstm.setString(2, cdHistorico);

	    int rs = pstm.executeUpdate();

	    if (rs > 0) {
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("##### rotinaDeslocarEvento : Erro ao atualizar c�digo do evento -  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

	return false;
}
%>


<%!private String verificaRetornoWebService(String retorno)
	{

		if (retorno.equals("VIATURA_NAO_ENCONTRADA"))
			retorno = "N�o � poss�vel identificar o ve�culo atrav�s do par�metro idRastreador.";
		else if (retorno.equals("EVENTO_NAO_ENCONTRADO"))
			retorno = "N�o � poss�vel identificar o evento atrav�s de par�metro idEvento.";
		else if (retorno.equals("CLIENTE_NAO_PERMITE_DESLOCAMENTO"))
			retorno = "O cliente pertence ao evento � configurado para n�o permitir deslocamentos carro.";
		else if (retorno.equals("CUC_NAO_PERMITE_DESLOCAMENTO"))
			retorno = "O CUC qual o evento pertence est� configurado para n�o permitir deslocamentos de carro.";
		else if (retorno.equals("CLIENTE_E_CUC_NAO_PERMITEM_DESLOCAMENTO"))
			retorno = "Tanto o cliente e o CUC que pertence ao evento s�o configurados para n�o permitir deslocamentos carro.";
		else if (retorno.equals("ERRO_ATUALIZAR_STATUS_EVENTO"))
			retorno = "Ocorreu um erro inesperado ao atualizar o status do evento";
		else if (retorno.equals("OPERACAO_ILEGAL"))
			retorno = "A mudan�a do status atual do evento para o estado entrou n�o � permitido. Um evento no estado - On Site - ou - servi�o feito - n�o pode ter seu status alterado para - Offset.";
		else if (retorno.equals("VIATURA_JA_DESLOCADA"))
			retorno = "O carro foi movido para outro evento. Esta mensagem s� ser� devolvido se o nas configura��es do desktop Sigma - Ativar compensado por um carro limita��o - est� habilitado.";
		else if (retorno.equals("DESLOCAMENTO_DE_EVENTO_FILHO_NAO_PERMITIDO"))
			retorno = "A mensagem retornada quando o agrupamento de eventos habilitado, eo evento a ser movido � um evento em cluster.";
		else if (retorno.equals("ACK"))
			retorno = "Opera��o bem-sucedida.";
		else
			retorno = "WebServiceSigma n�o retornou uma mensagem v�lida!. Mensagem de retorno: " + retorno;

		return retorno;
	}%>
<%!private String executaServicoSegware(Long idViatura, Long idEvento)
	{
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaDeslocarEvento");
		String returnFromAccess = "";
		try
		{
			DeslocarEventoWebServiceProxy webServiceProxy = new DeslocarEventoWebServiceProxy();

			returnFromAccess = webServiceProxy.deslocarEvento(idEvento, null, idViatura);

			log.warn("##### ROTINA DESLOCAMENTO DE EVENTO AUTOMATICO :" + returnFromAccess + "VIATURA ---- "+idViatura+ " --- EVENTO "+idEvento+ " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			returnFromAccess = e.getMessage();
			log.error("##### ERRO AO EXECUTAR ROTINA DESLOCAMENTO DE EVENTO EM ESPERA PARA A VIATURA DA ROTA CORRESPONDENTE A CONTA - RETORNOU " + returnFromAccess + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		finally
		{
			return returnFromAccess;
		}
}%>


<%!private List<String> verificaEventoHistorico()
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Boolean flag = Boolean.FALSE;
		List<String> listaVerificaEventoHistorico = null;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");

			StringBuilder sql = new StringBuilder();
			sql.append("  SELECT h.CD_VIATURA " );
			sql.append("  FROM HISTORICO h WITH (NOLOCK)   ");
			sql.append("  WHERE h.FG_STATUS = 2  ");
			//sql.append("  AND h.CD_VIATURA = ? ");
	
			  
			pstm = conn.prepareStatement(sql.toString());
			//pstm.setString(1, idViatura);
			rs = pstm.executeQuery();
			int contador = 0;
			listaVerificaEventoHistorico = new ArrayList<String>();
			while(rs.next())
			{
				String cdVtr = rs.getString("CD_VIATURA");
				listaVerificaEventoHistorico.add(cdVtr);
			}
				
		}
		catch (Exception e)
		{
			e.printStackTrace();

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			return listaVerificaEventoHistorico;
		}
}%>

<%!private String executaServicoSegware(String evento, String cliente, String empresa, String idCentral, String particao)
	{
 		
		String returnFromAccess = "";
 		String eventoAux = "XVID";
  		try
 		 {
   		
    	EventoRecebido eventoRecebido = new EventoRecebido();
    
    	eventoRecebido.setCodigo(eventoAux);
    	eventoRecebido.setData(new GregorianCalendar());
    	eventoRecebido.setEmpresa(Long.parseLong(empresa));
    	eventoRecebido.setIdCentral(idCentral);
   	    eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
   		eventoRecebido.setParticao(particao);
    	eventoRecebido.setProtocolo(Byte.parseByte("2"));
    
    	ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();
    
   		returnFromAccess = webServiceProxy.receberEvento(eventoRecebido);

  }
 	 catch (Exception e)
  {
    e.printStackTrace();
    returnFromAccess = e.getMessage();
  }
  	finally
  {
    return returnFromAccess;
  }
}
%>

<%!

private Boolean verificaEventoHistoricoView(String cdCliente)
{
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	Boolean flag = Boolean.FALSE;
	try
	{
		conn = PersistEngine.getConnection("SIGMA90");
		StringBuilder sql = new StringBuilder();
		sql.append("  SELECT TOP(1)V.CD_EVENTO,* FROM VIEW_HISTORICO V WITH (NOLOCK) WHERE V.CD_CLIENTE = ?  ORDER BY DT_RECEBIDO DESC ");
		st = conn.prepareStatement(sql.toString());
		st.setString(1, cdCliente);
		
		rs = st.executeQuery();

		while (rs.next()){
		System.out.println("ROTINA DESLOCAMENTO AUTOMATICO VIEW COD EVENTO : ");
			if(rs.getString("CD_EVENTO").contains("XVID"))
			flag = Boolean.TRUE;
		}

	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		OrsegupsUtils.closeConnection(conn, st, rs);
		return flag;
	}

}

%>






 
