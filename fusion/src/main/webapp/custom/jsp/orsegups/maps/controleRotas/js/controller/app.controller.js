app.controller('appcontroller', ['$scope','$http','$interval','$filter',function($scope, $http, $interval, $filter){

	console.log('Iniciou controller');
	
	$scope.painel;
	
	$scope.moto = "fa fa-motorcycle faa-passing animated";
	
	$scope.easterCount = 0;
	
	$scope.modificadoManual = false;
	
	$scope.viaturaLista;
	
	$scope.exibicaoCompleta = false;
	
	$scope.showRotasClass = "fa fa-eye-slash";
		
	$http({
		method: 'GET',
		url: 'https://intranet.orsegups.com.br/fusion/services/maps/getViaturasDisponiveis/'
		}).then(function successCallback(response) {
			console.log(response.data);
			$scope.painel = response.data;
			$scope.moto = "fa fa-motorcycle faa-passing animated-hover";
		}, function errorCallback(response) {
			console.log('Erro ao carregar viaturas!');
		});
	
	
	$interval(function(){
		$scope.refreshViaturas();
	},60*1000);
	
	$scope.refreshViaturas = function (){
		
		
		$http({
			method: 'GET',
			url: 'https://intranet.orsegups.com.br/fusion/services/maps/getViaturasDisponiveis/'
			}).then(function successCallback(response) {
				console.log(response.data);
				if (!$scope.modificadoManual){
					$scope.painel = response.data;
				}
				$('#processing-modal').modal('hide');		
			}, function errorCallback(response) {
				console.log('Erro ao carregar viaturas!');
			});
		
		
		$scope.modificadoManual = false;
		

	};
	
	$scope.cleanSearch = function (){
		$scope.query = "";
		
		
	};
	
	$scope.showRotas = function (){
		
		if ($scope.exibicaoCompleta){
			$scope.exibicaoCompleta = false;
			$scope.showRotasClass = "fa fa-eye-slash";
		}else{
			$scope.exibicaoCompleta = true;
			$scope.showRotasClass = "fa fa-eye";
			
			
		}
		
	};
	
	$scope.eaterEgg = function(){
		
		if ($scope.easterCount >= 10){
			$('#easterEggModal').modal('show');
			$scope.easterCount = 0;
		}else{
			$scope.easterCount ++;
		}
		
	}
	
	$scope.setControleRotaRegional = function (regional){
				
		
		$scope.modificadoManual = true;
		
		var regional = $filter("filter")($scope.painel,{nomeRegional:regional});
		
		var controlarRota = regional[0].controlarRotaRegional;
				
		var viaturas = regional[0].viaturas;
		
		console.log(viaturas);
		
		angular.forEach(viaturas, function (viatura){
			viatura.controlarRota = controlarRota;
			$scope.setControleRota(viatura);
		});
		
	}
	
	$scope.filterActiveCars = function(viatura){
		
		if (!viatura.listaRotasEfetivas && viatura.controlarRota){
			return true;
		}
		
		return false;
	}
		
	$scope.setControleRota = function(viatura){
		
		$scope.modificadoManual = true;

		var acao;
		
		if (viatura.controlarRota){
			acao = 1;
		}else{
			acao = 0;
		}
		
		if (acao == 1 && !viatura.listaRotasEfetivas){
			viatura.controlarRota = false;
			$('#warning-title').text('Ops!');
			$('#warning-text').text('A viatura deve possuir uma rota!');
			$('#warning-alert').modal('show');
		}else{
			$http({
				method: 'POST',
				url: 'https://intranet.orsegups.com.br/fusion/services/maps/setControleRota/'+viatura.codigoViatura+'/'+acao
				}).then(function successCallback(response) {
					console.log(response);
					
					if (viatura.controlarRota){
						var regional = $filter("filter")($scope.painel,{nomeRegional:viatura.nomeMotorista.substring(0,3)});
						regional[0].controlarRotaRegional = viatura.controlarRota;
					}else{
					
						var regional = $filter("filter")($scope.painel,{nomeRegional:viatura.nomeMotorista.substring(0,3)});
						var viaturas = regional[0].viaturas;
						
						var checkControlarRota = false;
						
						angular.forEach(viaturas, function (v){
							if (v.controlarRota){
								checkControlarRota = true;
							} 
							
						});
						
						regional[0].controlarRotaRegional = checkControlarRota;
						
					}
					
				}, function errorCallback(response) {
					console.log(response);
				});
		}
		
	};
	
	$scope.setControleTodasRotas = function(acao){
		$scope.modificadoManual = true;
		
		$('#processing-modal').modal('show');
		
		$http({
			method: 'POST',
			url: 'https://intranet.orsegups.com.br/fusion/services/maps/setControleTodasRotas/'+acao
			}).then(function successCallback(response) {
				console.log(response);
				$scope.refreshViaturas();				
			}, function errorCallback(response) {
				console.log(response);
			});
		

	};
	
	$scope.setRotaEfetiva = function(viatura){
		
		$scope.modificadoManual = true;
		
		if(viatura.rotaEfetiva){
			$http({
				method: 'POST',
				url: 'https://intranet.orsegups.com.br/fusion/services/maps/setRotaEfetiva/'+viatura.codigoViatura+'/'+viatura.rotaEfetiva
				}).then(function successCallback(response) {					
					console.log(response);
					$('#warning-title').text('Sucesso!');
					$('#warning-text').text(response.data);
		    		$('#warning-alert').modal('show');
				}, function errorCallback(response) {
					console.log(response);
					viatura.rotaEfetiva = null;
					$('#warning-title').text('Ops!');
					$('#warning-text').text(response.data);
		    		$('#warning-alert').modal('show');
				});
		}else{
			$('#warning-title').text('Ops!');
			$('#warning-text').text('Você deve selecionar uma rota!');
			$('#warning-alert').modal('show');
		}
			

	};
	
	$scope.addRotaEfetiva = function(viatura){
		
		$scope.modificadoManual = true;
		
		if(viatura.rotaEfetiva){
			$http({
				method: 'POST',
				url: 'https://intranet.orsegups.com.br/fusion/services/maps/addRotaEfetiva/'+viatura.codigoViatura+'/'+viatura.rotaEfetiva
				}).then(function successCallback(response) {

					var obj = {nomeRota: ""};
					obj.nomeRota = viatura.rotaEfetiva;	
					
					if (!viatura.listaRotasEfetivas){
						viatura.listaRotasEfetivas = [];
					}

					viatura.listaRotasEfetivas.push(obj);
					
					viatura.rotaEfetiva = null;
					
					console.log(response);
					
					$('#warning-title').text('Sucesso!');
					$('#warning-text').text(response.data);
		    		$('#warning-alert').modal('show');
				}, function errorCallback(response) {
					console.log(response);
					viatura.rotaEfetiva = null;
					$('#warning-title').text('Ops!');
					$('#warning-text').text(response.data);
		    		$('#warning-alert').modal('show');
				});
		}else{
			$('#warning-title').text('Ops!');
			$('#warning-text').text('Você deve selecionar uma rota!');
			$('#warning-alert').modal('show');
		}
			

	};
	
	$scope.removerRotaEfetivaLista = function(viatura, rota){
		
		console.log('Chegou aqui');
		
		$scope.modificadoManual = true;
		
		if(rota.nomeRota){
			$http({
				method: 'POST',
				url: 'https://intranet.orsegups.com.br/fusion/services/maps/removerRotaEfetivaList/'+viatura.codigoViatura+'/'+rota.nomeRota
				}).then(function successCallback(response) {
					console.log(response);
										
					var index = viatura.listaRotasEfetivas.indexOf(rota);
					
					viatura.listaRotasEfetivas.splice(index, 1);
					
		    		$('#success-alert').show();
	    			setTimeout(function(){
	    				$('#success-alert').hide();
	    			}, 3500);
					
				}, function errorCallback(response) {
					console.log(response);

				});
		}		

	};
		
	
	$scope.showListaRotas = function (viatura){
		
		$scope.viaturaLista = viatura;
				
		$('#modalListaRotas').modal('show');
		
	};
	
}]);

