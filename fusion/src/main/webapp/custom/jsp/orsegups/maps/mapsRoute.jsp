<%@page import="org.hibernate.property.MapAccessor"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0px;
	padding: 0px
}

#map_canvas {
	height: 100%
}
.labelsCars {
     color: black;
     background-color: white;
     font-family: "Tahoma";
     font-size: 10px;
     font-weight: bold;
     text-align: center;
     width: 24px;
     height: 13px;
     border: 1px solid black;
     white-space: nowrap;
   } 
 .labelsEvents {
     color: black;
     background-color: white;
     font-family: "Tahoma";
     font-size: 10px;
     font-weight: bold;
     text-align: center;
     width: 28px;
     height: 13px;
     border: 1px solid black;
     white-space: nowrap;
   }     
.distanceLabels {
     color: black;
     background-color: white;
     font-family: "Tahoma";
     font-size: 10px;
     font-weight: bold;
     text-align: center;
     width: 40px;
     height: 13px;
     border: 1px solid black;
     white-space: nowrap;
   }
   
.window_black_bold {
	font-family: "Tahoma";
	color: black;
	background-color: white;
	font-size: 12px;
	font-weight: bold
}

.window_black {
	font-family: "Tahoma";
	color: black;
	background-color: white;
	font-size: 12px;
}

</style>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript"
	src="https://maps.google.com/maps/api/js?sensor=false&language=pt-BR&region=BR"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/markerwithlabel.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/keydragzoom.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery.qtip-1.0.0-rc3.min.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/clock/jquery.jclock.js"></script>
	
<% 
	// seguranca: podem acessar usuarios logados e maquinas da rede Orsegups ou Apple devices com passagem de parametro secreto
	String remoteIP = request.getRemoteAddr();
	System.out.println(remoteIP);
	String liberaAcesso = request.getParameter("liberaAcesso");
	
	boolean accessGranted = MapaSecurityAcess.verificaAcessoUrl(remoteIP, liberaAcesso);

	String action = request.getParameter("action");
	String regional = request.getParameter("regional");
	String ramalOrigem = request.getParameter("ramalOrigem");
	String emUso = request.getParameter("emUso");
	String aplicacao = request.getParameter("aplicacao");
	String atualizaPosicao = request.getParameter("atualizaPosicao");
	String showDistanceOnMapStr = request.getParameter("showDistance");
	String filtrosConta = request.getParameter("filtrosConta");
	String filtrosNotConta = request.getParameter("filtrosNotConta");
	String x8IgnoreTags = request.getParameter("x8IgnoreTags");
	String showLogStr = request.getParameter("showLog");

	Boolean showLog = false;
	if(showLogStr != null && showLogStr.equalsIgnoreCase("true"))
	{
		showLog = true;
	}
	
	
	
	Boolean showDistanceOnMap = false;
	if(showDistanceOnMapStr != null && showDistanceOnMapStr.equalsIgnoreCase("true"))
	{
		showDistanceOnMap = true;
	}

	if(atualizaPosicao != null)
	{
		atualizaPosicao = atualizaPosicao.toLowerCase();
	}
	else
	{
		atualizaPosicao = "";
	}
%>

<script type="text/javascript">
	
	var directionsService = new google.maps.DirectionsService();
	var mapsLayout;
	var map;
	var viaturasArray = [];
	var eventosArray = [];
	var infoWindowMouseOverControl = [];
	var distanceArray = [];
	//var ligacaoEventoArray = [];
	//var ligacaoViaturaArray = [];
	var refreshing = true;
	var timeout;
	var timeoutOS;
	var timeoutViaturaTecnica;
	var timeoutEventosCM;
	var timeoutViaturasCM;
	var filter = "<%=action%>";
	var regional = "<%=regional%>";
	var emUso = "<%=emUso%>";
	var ramalOrigem = "<%=ramalOrigem%>";
	var aplicacao = "<%=aplicacao%>";
	var atualizaPosicao = "<%=atualizaPosicao%>";
	var showDistanceOnMap = <%=showDistanceOnMap%>;
	var filtrosConta = "<%=filtrosConta%>";
	var filtrosNotConta = "<%=filtrosNotConta%>";
	var x8IgnoreTags = "<%=x8IgnoreTags%>";
	var tecnicoSelecionado;
	var atendenteSelecionadoDiv;
	var atendenteSelecionadoText;
	var accessGranted = <%=accessGranted%>;
	var showLog = <%=showLog%>;

	$(document).ready(function ()
	{
		if(accessGranted)
		{
			initialize();
		}
		else
		{
			insertApplet("orsegupsjApplet");
		}
	});
	
	// Possiveis valores do atributo mapTypeId google.maps.MapTypeId
	//google.maps.MapTypeId.ROADMAP exibe as blocos 2D normais, padr�o, do Google Maps.
	//google.maps.MapTypeId.SATELLITE exibe blocos fotogr�ficos.
	//google.maps.MapTypeId.HYBRID exibe uma mistura entre blocos fotogr�ficos e uma camada de blocos com recursos importantes (estradas, nomes de cidade).
	//google.maps.MapTypeId.TERRAIN exibe blocos de relevo f�sico para exibi��o de recursos de eleva��o e �gua (montanhas, rios etc.).
	
	
	function initialize() 
	{
		var latlng = new google.maps.LatLng(-27.59694, -48.54889);
		var myOptions = {
			zoom : 9,
			center : latlng,
			panControl: false,
			streetViewControl: false,
			zoomControl : true,
			mapTypeId : google.maps.MapTypeId.ROADMAP,
			streetViewControl: true,
			streetViewControlOptions: {
				position: google.maps.ControlPosition.LEFT_TOP
			}
		};
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
		// Cria o botao ZoomArea
		map.enableKeyDragZoom({
	          visualEnabled: true,
	          visualPosition: google.maps.ControlPosition.TOP_LEFT,
	          visualPositionOffset: new google.maps.Size(5, 5),
	          visualPositionIndex: 0,
	          visualSprite: "images/dragzoom_btn.png",
	          visualSize: new google.maps.Size(20, 20),
	          visualTips: {
	            off: "Zoom Area",
	            on: "Zoom Area"
	          }
	        });
		
		
		// Cria o botao  Exibir/Esconder Dist�ncias
		showDistancesButtonControl();
	
		// Cria o botao FitToWindow (enquadrar marcadores)
		fitToMarkersControl();
		
		
		
		if(filter == "viaturas")
		{
			loadViaturasCM();
		}
		else if(filter == "centralMonitoramento")
		{
			loadViaturasCM();
			loadEventosCM();
		}
		else if(filter == "areaTecnica")
		{
			//cria DIv onde sera mostrado o tecnico selecionado
			if(atendenteSelecionadoDiv == null)
			{
				atendenteSelecionadoDiv = document.createElement('DIV');
				atendenteSelecionadoDiv.index = 1;
				atendenteSelecionadoDiv.style.padding = '2px';
				atendenteSelecionadoDiv.style.height = '40px';
				
				// Set CSS for the control border
				var controlUI = document.createElement('DIV');
				controlUI.style.backgroundColor = 'white';
				controlUI.style.borderStyle = 'solid';
				controlUI.style.borderWidth = '2px';
				controlUI.style.cursor = 'pointer';
				controlUI.style.textAlign = 'center';
				atendenteSelecionadoDiv.appendChild(controlUI);
				
				// Set CSS for the control interior
				atendenteSelecionadoText = document.createElement('DIV');
				atendenteSelecionadoText.style.fontFamily = 'Tahoma';
				atendenteSelecionadoText.style.fontSize = '12px';
				atendenteSelecionadoText.style.paddingLeft = '4px';
				atendenteSelecionadoText.style.paddingRight = '4px';
				//atendenteSelecionadoText.style.fontWeight = 'bold';
				//controlText.style.height = '40px';
				//atendenteSelecionadoText.style.width = '80px';
				controlUI.appendChild(atendenteSelecionadoText);
				
				map.controls[google.maps.ControlPosition.TOP_CENTER].push(atendenteSelecionadoDiv);
			}
			loadAtendentes();
		}
		fitToMarkers();
		
		createClock();
	}
	
	function createClock()
	{
		var options = {
	       timeNotation: '24h',
	       am_pm: false,
	       fontFamily: 'Tahoma',
	       fontSize: '20px',
	       //foreground: 'yellow',
	       background: 'white'
		}; 
		$('.jclock').jclock(options);
		
		var clockDiv = document.getElementById('jclockId');
		
		map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(clockDiv);
	}
	
	function fitToMarkersControl()
	{

		var controlDiv = document.createElement('DIV');
		
		// Set CSS styles for the DIV containing the control
		// Setting padding to 5 px will offset the control
		// from the edge of the map
		controlDiv.style.padding = '5px';
		
		var controlImg = document.createElement('IMG');
		controlImg.src = 'images/enquadrar.png';
		controlImg.style.cursor = 'pointer';
		controlImg.title = 'Enquadrar Marcadores';
		controlDiv.appendChild(controlImg);
		
		
		// Setup the click event listeners: simply set the map to Chicago
		google.maps.event.addDomListener(controlImg, 'click', function() {
			fitToMarkers();	
		
		});
		
		controlDiv.index = 1;
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(controlDiv);
	}
	
	function showDistancesButtonControl() 
	{

		var controlDiv = document.createElement('DIV');
		
		// Set CSS styles for the DIV containing the control
		// Setting padding to 5 px will offset the control
		// from the edge of the map
		controlDiv.style.padding = '5px';
		
		var controlImg = document.createElement('IMG');
		controlImg.src = 'images/distancia.png';
		controlImg.style.cursor = 'pointer';
		controlImg.title = 'Exibir/Esconder Dist�ncias';
		controlDiv.appendChild(controlImg);
		
		// Setup the click event listeners: simply set the map to Chicago
		google.maps.event.addDomListener(controlImg, 'click', function() {
			
			//inverte o valor
			showDistanceOnMap = !showDistanceOnMap;
			
			controlDistancesVisualization();
		});
		
		controlDiv.index = 1;
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(controlDiv);
	}
	
	function barraAtendentes(atendenteObj)
	{

		var barraAtendentesDiv = document.createElement('DIV');
		
		barraAtendentesDiv.index = 1;
		
		// Set CSS styles for the DIV containing the control
		// Setting padding to 5 px will offset the control
		// from the edge of the map
		barraAtendentesDiv.style.padding = '2px';
		barraAtendentesDiv.style.height = '40px';
		
		// Set CSS for the control border
		var controlUI = document.createElement('DIV');
		controlUI.style.backgroundColor = 'white';
		controlUI.style.borderStyle = 'solid';
		controlUI.style.borderWidth = '2px';
		controlUI.style.cursor = 'pointer';
		controlUI.style.textAlign = 'center';
		controlUI.title = atendenteObj.nomeAtendente;
		barraAtendentesDiv.appendChild(controlUI);
		
		// Set CSS for the control interior
		var controlText = document.createElement('DIV');
		controlText.style.fontFamily = 'Tahoma';
		controlText.style.fontSize = '10px';
		controlText.style.paddingLeft = '4px';
		controlText.style.paddingRight = '4px';
		//controlText.style.height = '40px';
		//controlText.style.width = '80px';
		controlText.innerHTML = atendenteObj.nomeAtendente;
		controlUI.appendChild(controlText);
		
		
		// Setup the click event listeners: simply set the map to Chicago
		google.maps.event.addDomListener(controlUI, 'click', function() {
			loadTecnico(atendenteObj.codigoAtendente);
			
			atendenteSelecionadoText.innerHTML = atendenteObj.nomeAtendente;
		});
		
		map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(barraAtendentesDiv);
	}
	
	function atualizaPosicaoCliente(codigoCliente, placa)
	{
		var r = confirm("Deseja atualizar a posi��o do Evento com a posi��o da Viatura?");
		if(r == true)
		{
			var viaturaItem = viaturasArray[placa];
			if(viaturaItem != null)
			{
				viaturaObj = viaturaItem["viaturaObj"];
				if(viaturaObj != null)
				{
					var latitude = viaturaObj.latitude;
					var longitude = viaturaObj.longitude;
					
					var dataObj = new Object();
					dataObj.codigoCliente = codigoCliente;
					dataObj.latitude = latitude;
					dataObj.longitude = longitude;
				
					var dataJson = JSON.stringify(dataObj);
					
					var result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet10?action=atualizaPosicaoEvento&data="+dataJson);
					
					if(result != null && result != "")
					{
						alert(result);
					}
					
					if(filter == "centralMonitoramento")
					{
						clearTimeout(timeoutEventosCM);
						loadEventosCM();
					}
					else
					{
						clearTimeout(timeoutOS);
						loadOS();
					}
				}
			}
		}
	}
	
	function loadEventosCM()
	{
		var eventosJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet10?action=eventosCM&regional="+regional+"&atualizaPosicao="+atualizaPosicao+"&filtrosConta="+filtrosConta+"&filtrosNotConta="+filtrosNotConta);
		
		parseEventos(eventosJson);
		
		if(refreshing)
		{
			timeoutEventosCM = setTimeout("loadEventosCM()",15000);
		}
	}
	
	function loadViaturasCM()
	{
		var viaturasJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet10?action=viaturasCM&regional="+regional+"&ramalOrigem="+ramalOrigem+"&aplicacao="+aplicacao+"&emUso="+emUso+"&x8IgnoreTags="+x8IgnoreTags+"&filtrosConta="+filtrosConta+"&filtrosNotConta="+filtrosNotConta);
 		
		parseViaturas(viaturasJson);
		
		if(refreshing)
		{
			timeoutViaturasCM = setTimeout("loadViaturasCM()",15000);
		}
	}
	
	function loadTecnico(codigoTecnico)
	{
		if(codigoTecnico != null)
		{
			// remove os timeOuts que recaregam os dados a cada 15 segundos para nao sobrecarrega-los 
			clearTimeout(timeoutOS);
			clearTimeout(timeoutViaturaTecnica);
			
			//remove todos os itens do mapa pois ao chamar este metodo estaremos carregando viatura e os de outro tecnico
			for(e in eventosArray)
			{
				removeEvento(e);
			}
			for(v in viaturasArray)
			{
				removeViatura(v);
			}
			
			// atualiza quem � o tecnico atual para que o refresh saiba de quem carregar as OS
			tecnicoSelecionado = codigoTecnico; 
			
			// busca os novos dados
			loadViaturasTecnicas();
			loadOS();
		}
		fitToMarkers();
	}
	
	function loadOS()
	{
		var osJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet10?action=OS&codigoTecnico="+tecnicoSelecionado+"&atualizaPosicao="+atualizaPosicao);
		
		parseEventos(osJson);
		
		if(refreshing)
		{
			timeoutOS = setTimeout("loadOS()",15000);
		}
	}
	
	function loadViaturasTecnicas()
	{
		var viaturasJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet10?action=viaturasTecnicas&codigoTecnico="+tecnicoSelecionado+"&ramalOrigem="+ramalOrigem);
		
		parseViaturas(viaturasJson);
		
		if(refreshing)
		{
			timeoutViaturaTecnica = setTimeout("loadViaturasTecnicas()",15000);
		}
	}
	
	function loadAtendentes()
	{
		var atendentesJson = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet10?action=atendentes&regional="+regional);
		
		parseAtendentes(atendentesJson);
		
		if(refreshing)
		{
			//timeout = setTimeout("loadAtendentes()",15000);
		}
	}
	
	function parseAtendentes(atendentesJson)
	{
		if(atendentesJson != null && atendentesJson != "")
		{
			atendentes = JSON.parse(atendentesJson);
			
			for(i in atendentes) 
			{
				var atendenteObj = atendentes[i];
				
				barraAtendentes(atendenteObj);
			}
		}
	}
		
		
	function parseEventos(eventosJson)
	{
		if(eventosJson != null && eventosJson != "")
		{
			eventos = JSON.parse(eventosJson);
			
			var log = "Eventos: "+eventos.length;
			registerLog(log);
			
			for(i in eventos) 
			{
				var eventoObj = eventos[i];
				
				manageEvento(eventoObj);
			}
			
			for(k in eventosArray)
			{
				var exists = false;
				
				for(n in eventos) {
					if(eventos[n].codigoHistorico == k)
					{
						exists = true;
						break;
					}
				}
				
				if(!exists)
				{
					removeEvento(k);
				}
			}
		}
		
		if(refreshing)
		{
			timeout = setTimeout("parseEventos()",15000);
		}
	}
	
	function removeEvento(k)
	{
			if(eventosArray[k] != null)
			{
				if(eventosArray[k]["marker"] != null)
				{
					eventosArray[k]["marker"].setMap(null);
					eventosArray[k]["marker"] = null;
				}
				
				if(eventosArray[k]["info"] != null)
				{
					eventosArray[k]["info"].setMap(null);
					eventosArray[k]["info"] = null;
				}
				
				if(eventosArray[k]["line"] != null)
				{
					eventosArray[k]["line"].setMap(null);
					eventosArray[k]["line"] = null;
				}
				
				if(eventosArray[k]["distance"] != null)
				{
					eventosArray[k]["distance"].setMap(null);
					eventosArray[k]["distance"] = null;
				}
				
				var placa = eventosArray[k]["eventoObj"].placa;
				if(placa != null && placa != "" && viaturasArray[placa] != null)
				{
					if(viaturasArray[placa]["line"] != null && viaturasArray[placa]["line"][k] != null)
					{
						viaturasArray[placa]["line"][k].setMap(null);
						viaturasArray[placa]["line"][k] = null;
					}
					
					if(viaturasArray[placa]["marker"] != null)
					{
						viaturasArray[placa]["marker"].setIcon("images/"+viaturasArray[placa]["viaturaObj"].nomeImagem+".png");
					}
					
				}
				
				eventosArray[k] = null;
			}
	}
	
	function parseViaturas(viaturasJson)
	{
		if(viaturasJson != null && viaturasJson != "")
		{
			var viaturas = JSON.parse(viaturasJson);
			
			for(i in viaturas) {
				var viaturaObj = viaturas[i];
				
				manageViatura(viaturaObj);
		    }
			
			for(k in viaturasArray)
			{
				var exists = false;
				
				for(n in viaturas) {
					if(viaturas[n].placa == k)
					{
						exists = true;
						break;
					}
				}
				
				if(!exists)
				{
					removeViatura(k);
				}
			}
		}
	}
	
	function removeViatura(k)
	{
		if(viaturasArray[k] != null)
		{
			if(viaturasArray[k]["marker"] != null)
			{
				viaturasArray[k]["marker"].setMap(null);
				viaturasArray[k]["marker"] = null;
			}
			
			if(viaturasArray[k]["info"] != null)
			{
				viaturasArray[k]["info"].setMap(null);
				viaturasArray[k]["info"] = null;
			}
			
			if(viaturasArray[k]["line"] != null)
			{
				lines = viaturasArray[k]["line"];
				
				for(i in lines)
				{
					if(lines[i] != null)
					{
						lines[i].setMap(null);
						lines[i] = null;
						
						if(eventosArray[i] != null && eventosArray[i]["marker"] != null)
						{
							eventosArray[i]["marker"].setIcon("images/"+eventosArray[i]["eventoObj"].nomeImagem+".png");
							if(eventosArray[i]["line"] != null)
							{
								eventosArray[i]["line"].setMap(null);
								eventosArray[i]["line"] = null;
							}
							
							if(eventosArray[i]["distance"] != null)
							{
								eventosArray[i]["distance"].setMap(null);
								eventosArray[i]["distance"] = null;
							}
							
						}
					}
				}
			}
			viaturasArray[k] = null;
		}
	}
	
	function manageEvento(eventoObj)
	{
		var location = new google.maps.LatLng(eventoObj.latitude, eventoObj.longitude);
		var marker = null;
		var infowindow = null;
		var eventoItem = null;
		var eventoViaturaLine = null;
		var directionsDisplay = null;
		var distance = null;
		var showGoogleRoute = false;
		
		registerLog("	&nbsp &nbsp &nbsp	- codigoHistorico: "+eventoObj.codigoHistorico+" - placa: "+eventoObj.placa);
		
		eventoItem = eventosArray[eventoObj.codigoHistorico];
		
		if(eventoItem != null)
		{
			marker = eventoItem["marker"];
			infowindow = eventoItem["info"];
			eventoViaturaLine = eventoItem["line"];
			distance = eventoItem["distance"];
			directionsDisplay = eventoItem["directionsDisplay"];
			showGoogleRoute = eventoItem["showGoogleRoute"];
		}
		else
		{
			eventoItem = [];
			eventoItem["showGoogleRoute"] = false;
		}
		
		eventoItem["eventoObj"] = eventoObj;
		
		//cria o marcador do evento
		if(marker != null)
		{
			marker.setPosition(location);
			marker.set("labelStyle", {opacity: 1, backgroundColor: eventoObj.corPrioridade, color: eventoObj.corTexto});
		}
		else
		{
			
			marker = new MarkerWithLabel({
				position: location,
				map: map,
				title : eventoObj.codigoEvento + ' - ' + eventoObj.nomeEvento,
				icon : "images/"+eventoObj.nomeImagem+".png",
				zIndex : 1,
				labelContent: eventoObj.codigoEvento,
		       	labelClass: "labelsEvents", // the CSS class for the label
		       	labelAnchor: new google.maps.Point(15, 48),
		       	labelInBackground: false,
		       	labelStyle: {opacity: 1, backgroundColor: eventoObj.corPrioridade, color: eventoObj.corTexto}
			});
			
			
			if(eventoObj.type != null && eventoObj.type == "os")
			{
				marker.setTitle(eventoObj.nomeEvento);
				marker.set("labelContent", eventoObj.nomeEvento);
				//marker.set("labelVisible", false);
			}
			
			eventoItem["marker"] = marker;
		}
		
		var contentString = eventoObj.infoWindowContent;
		
		// se a janela ja existe atualiza o conteudo da janela, caso contrario cria ela
		if(infowindow != null)
		{
			infowindow.setContent(contentString);
		}
		else
		{
			infowindow = new google.maps.InfoWindow({
			    content: contentString
			});
			eventoItem["info"] = infowindow;
			
			if(eventoObj.status != 2) 
			//inicializa com true
			infoWindowMouseOverControl[eventoObj.codigoHistorico] = true;
			
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
				infoWindowMouseOverControl[eventoObj.codigoHistorico] = false;
			});
			
			google.maps.event.addListener(marker, 'dblclick', function() {
				infowindow.close();
				infoWindowMouseOverControl[eventoObj.codigoHistorico] = true;
			});
			
			google.maps.event.addListener(marker, 'mouseover', function() {
				if(infoWindowMouseOverControl[eventoObj.codigoHistorico])
				{
					infowindow.open(map,marker);
				}
			});
			
			google.maps.event.addListener(marker, 'mouseout', function() {
				if(infoWindowMouseOverControl[eventoObj.codigoHistorico])
				{
					infowindow.close();
				}
			});
			
			google.maps.event.addListener(infowindow, 'closeclick', function() {
				infoWindowMouseOverControl[eventoObj.codigoHistorico] = true;
			});
			
			google.maps.event.addListener(infowindow, 'domready', function() {
				if(!infoWindowMouseOverControl[eventoObj.codigoHistorico])
				{
					$('#content a[tooltip]').each(function()
					{
					   $(this).qtip({
					      content: $(this).attr('tooltip'), // Use the tooltip attribute of the element for the content
					      show: { 
					    	  when: { event: 'mouseenter' }, 
					    	  delay: 0,  
					    	  solo: true, 
					    	  effect: { length: 0,
					    		  		type: ""} 
					      }, 
					      hide: { 
					    	  when: { 
					    		  event: 'mouseleave', 
					    		  event: 'click', 
					    		  event: "unfocus", 
					    		  event: "mouseout" }, 
							  delay: 0,
							  effect: { length: 0,
				    		  			type: ""} 
					      },
					      style: { 
					    	  width: {
					    		  max : 800,
					    		  min : 100
					    	  },
					    	  border: {
					    	         width: 2
					    	  },
					    	  name: 'dark'
					      }
					   }); 
					});
				}
			});
		}
		
		// gerencia a ligacao entre o evento e a viatura
		if(eventoObj.placa != null && eventoObj.placa != '')
		{
			eventoLatLng = location;
			
			var viaturaItem = viaturasArray[eventoObj.placa];
			if(viaturaItem != null)
			{
				var viatura = viaturaItem["marker"]; 
				viaturaLatLng = viatura.getPosition();
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
				var eventoViaturaPath = [eventoLatLng, viaturaLatLng];
				
				
				if(showDistanceOnMap || showGoogleRoute)
				{
					//calcula distancia
					var request = {
						avoidHighways : false,
						avoidTolls : false,
						origin : viaturaLatLng,
						destination: eventoLatLng, 
						travelMode: google.maps.DirectionsTravelMode.DRIVING,
						provideRouteAlternatives : false
					};
					directionsService.route(request, function(response, status)
					{
						var routeDistance = 0;
						if (status == google.maps.DirectionsStatus.OK) 
						{
							
							var routes = response.routes;
							if(routes != null)
							{
								for(x in routes)
								{
									var route = routes[x];
									if(route != null)
									{
										var legs = route.legs;
										
										for(z in legs)
										{
											routeDistance += legs[z]["distance"]["value"];
										}
									}
								}
							}
							
							if(showDistanceOnMap)
								{
								var formatedDistance = null;
								if(routeDistance >= 100)
								{
									formatedDistance = Math.round((routeDistance/1000)*10)/10 + " Km";
								}
								else
								{
									formatedDistance = Math.round((routeDistance)*10)/10 + " M";
								}
								
								if(formatedDistance != null && formatedDistance != "")
								{
									if(distance == null)
									{
										distance = new MarkerWithLabel({
											position: location,
											map: map,
											title : formatedDistance,
											icon : "images/dot.png",
											zIndex : 1,
											labelContent: formatedDistance,
										labelClass: "distanceLabels", // the CSS class for the label
										labelAnchor: new google.maps.Point(-13, 30),
										labelInBackground: false,
										labelStyle: {opacity: 1, backgroundColor: viaturaItem["viaturaObj"].corLigacao, color: 'white', border: '1px solid '+viaturaItem["viaturaObj"].corLigacao}
										});
										
										eventoItem["distance"] = distance;
									}
									else	
									{
										distance.setPosition(location);
										distance.set("labelContent", formatedDistance);
									}
								}
								else
								{
									if(distance != null)
									{
										distance.setMap(null);
									}
								}
							}
							
							/*
							if(showGoogleRoute)
							{
								if(directionsDisplay == null)
								{
									var directionsDisplay = new google.maps.DirectionsRenderer();
								}
								
								var options = {
									polylineOptions : {strokeColor : viaturaItem["viaturaObj"].corLigacao, strokeOpacity : 0.5, strokeWeight : 5},
									preserveViewport : true
								};
								
								directionsDisplay.setMap(map);
								directionsDisplay.setDirections(response);
								directionsDisplay.setOptions(options);
								
								eventoItem["directionsDisplay"] = directionsDisplay;
							}
							*/
						}
					});
				}
				
				//verificaDeslocamento(viaturaItem["viaturaObj"],viaturaItem["marker"]);
				// gerencia a linha que liga o evento a viatura
					if(eventoViaturaLine != null)
					{
						
						if(eventoObj.status == 2) {
							eventoViaturaLine.getPath().clear();
						} else {
							eventoViaturaLine.setPath(eventoViaturaPath);
							eventoViaturaLine.setOptions({strokeColor: viaturaItem["viaturaObj"].corLigacao});	
							
							
							
						}
					}
					else
					{
						
							var eventoViaturaLine = new google.maps.Polyline({
								path: eventoViaturaPath,
								strokeColor: viaturaItem["viaturaObj"].corLigacao,
								strokeOpacity: 1.0,
								strokeWeight: 3
							});
						
							eventoViaturaLine.setMap(map);
								
							eventoItem["line"] = eventoViaturaLine;
							
							lines = viaturaItem["line"];
							
							if(lines == null)
							{
								lines = [];
								viaturaItem["line"] = lines;
							}
							lines[eventoObj.codigoHistorico] = eventoViaturaLine;
							
					}
				if(viaturaItem["viaturaObj"].sufixoLigacao != null)
				{
					marker.setIcon("images/"+eventoObj.nomeImagem+"_"+viaturaItem["viaturaObj"].sufixoLigacao+".png");
					viatura.setIcon("images/"+viaturaItem["viaturaObj"].nomeImagem+"_"+viaturaItem["viaturaObj"].sufixoLigacao+".png");
					
					
				}
			}
			viaturasArray[eventoObj.placa] = viaturaItem;
			
		}
			
		eventosArray[eventoObj.codigoHistorico] = eventoItem;
			
	}
	
	
	function manageViatura(viaturaObj)
	{
		var location = new google.maps.LatLng(viaturaObj.latitude, viaturaObj.longitude);
		var marker = null;
		var infowindow = null;
		var viaturaItem = null;
		var lines = null;
		
		viaturaItem = viaturasArray[viaturaObj.placa];
		
		
		
		if(viaturaItem != null)
		{
			marker = viaturaItem["marker"]; 
			infowindow = viaturaItem["info"];
			lines = viaturaItem["line"];
		}
		else
		{
			viaturaItem = [];
		}
		
		viaturaItem["viaturaObj"] = viaturaObj;
		
		if(marker != null)
		{
			marker.setPosition(location);
			marker.set("labelContent", viaturaObj.titulo);
			marker.set("labelStyle", {opacity: 1, backgroundColor: viaturaObj.corStatus, color: viaturaObj.corTexto});
		
			
		}
		else 
		{
			
			
			
			
			
			marker = new MarkerWithLabel({
				position: location,
				map: map, 
				title : viaturaObj.titulo,
				icon : "images/"+viaturaObj.nomeImagem+".png",
				zIndex : 100,
				labelContent: viaturaObj.titulo,
				labelAnchor: new google.maps.Point(13, 41),
		       	labelClass: "labelsCars", // the CSS class for the label  	
		       	labelInBackground: false,
		       	labelStyle: {opacity: 1, backgroundColor: viaturaObj.corStatus, color: viaturaObj.corTexto }
			});
				
		
			
			
			viaturaItem["marker"] = marker;
		/////////////////// MARKER //////////////////////
		
			
		}
		if(viaturaObj.deslocamentoLento){
			marker.setAnimation(google.maps.Animation.BOUNCE);
			
		}else{
			marker.setAnimation(null);
		}

		var contentString = viaturaObj.infoWindowContent;
		
		if(infowindow != null)
		{
			infowindow.setContent(contentString);
		}
		else
		{
			infowindow = new google.maps.InfoWindow({
			    content: contentString
			});
			viaturaItem["info"] = infowindow;
			
			//inicializa com true
			infoWindowMouseOverControl[viaturaObj.placa] = true;
			
			
			
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
				infoWindowMouseOverControl[viaturaObj.placa] = false;
			});
			
			google.maps.event.addListener(marker, 'dblclick', function() {
				infowindow.close();
				infoWindowMouseOverControl[viaturaObj.placa] = true;
			});
			
			google.maps.event.addListener(marker, 'mouseover', function() {
				if(infoWindowMouseOverControl[viaturaObj.placa])
				{
					infowindow.open(map,marker);
				}
			});
			
			google.maps.event.addListener(marker, 'mouseout', function() {
				if(infoWindowMouseOverControl[viaturaObj.placa])
				{
					infowindow.close();
				}
			});
			
			google.maps.event.addListener(infowindow, 'closeclick', function() {
				infoWindowMouseOverControl[viaturaObj.placa] = true;
			});
			
			google.maps.event.addListener(infowindow, 'domready', function() {
				if(!infoWindowMouseOverControl[viaturaObj.placa])
				{
					$('#content a[tooltip]').each(function()
					{
					   $(this).qtip({
					      content: $(this).attr('tooltip'), // Use the tooltip attribute of the element for the content
					      show: { 
					    	  when: { event: 'mouseenter' }, 
					    	  delay: 0,  
					    	  solo: true, 
					    	  effect: { length: 0,
					    		  		type: ""} 
					      }, 
					      hide: { 
					    	  when: { 
					    		  event: 'mouseleave', 
					    		  event: 'click', 
					    		  event: "unfocus", 
					    		  event: "mouseout" }, 
							  delay: 0,
							  effect: { length: 0,
				    		  			type: ""} 
					      },
					      style: { 
					    	  width: {
					    		  max : 800,
					    		  min : 100
					    	  },
					    	  border: {
					    	         width: 2
					    	  },
					    	  name: 'dark'
					      }
					   }); 
					});
				}
			});
		}
		
		// caso tenha uma linha associada a esta viatura atualiza o ponto
		if(lines != null)
		{
			
			for(i in lines)
			{
				var line = lines[i];
				if(line != null)
				{
					var path = line.getPath();
					path.removeAt(1);
					path.insertAt(1, location);
				}
			}
		}
			
		viaturasArray[viaturaObj.placa] = viaturaItem;
	}
	
	function controlDistancesVisualization()
	{
		for(e in eventosArray)
		{
			if(eventosArray[e] != null )
			{
				if(eventosArray[e]["distance"] != null)
				{
					if(showDistanceOnMap)
					{
						eventosArray[e]["distance"].setMap(map);
					}
					else
					{
						eventosArray[e]["distance"].setMap(null);
					}
				}
				else if(eventosArray[e]["eventoObj"] != null)
				{
					manageEvento(eventosArray[e]["eventoObj"]);
				}
			}
		}
	}
	
	function controlGoogleRouteVisualization(eventId)
	{
		var eventoItem = eventosArray[eventId];
		
		if(eventoItem != null && eventoItem["eventoObj"] != null)
		{
			eventoItem["showGoogleRoute"] = !eventoItem["showGoogleRoute"];
			eventosArray[eventId] = eventoItem;
			
			if(eventoItem["showGoogleRoute"])
			{
				manageEvento(eventoItem["eventoObj"]);
			}
			else
			{
				var directionsDisplay = eventoItem["directionsDisplay"];
				if(directionsDisplay != null)
				{
					directionsDisplay.setMap(null);
				}
			}
		}
	}
	
	function fitToMarkers()
	{
		var latLngBounds = null;
		
		for(v in viaturasArray)
		{
			if(viaturasArray[v] != null && viaturasArray[v]["marker"] != null)
			{
				var location = viaturasArray[v]["marker"].getPosition();
				
				if(latLngBounds == null)
				{
					latLngBounds = new google.maps.LatLngBounds(location, location);
				}
				else
				{
					latLngBounds.extend(location);
				}
			}
		}
		
		for(e in eventosArray)
		{
			if(eventosArray[e] != null && eventosArray[e]["marker"] != null)
			{
				var location = eventosArray[e]["marker"].getPosition();
				
				if(latLngBounds == null)
				{
					latLngBounds = new google.maps.LatLngBounds(location, location);
					
				}
				else
				{
					latLngBounds.extend(location);
				}
			}
		}
		if(latLngBounds != null)
		{
			map.fitBounds(latLngBounds);
		}
	}
	
	function insertApplet(id) {
	    if (document.getElementById(id) == null) {	
	        if (navigator.appName == 'Microsoft Internet Explorer') {
	            obj = document.createElement("Object");
	        } else {
	            obj = document.createElement("applet");	
	        }
            obj.id = id;
            obj.name = id;

            if (navigator.appVersion.indexOf("Win")!=-1) {
                obj.archive="OrsegupsJApplet.jar";
            } else {
            	obj.archive="OrsegupsJApplet.jar";
            }

            obj.code="com.neomind.orsegups.applet.OrsegupsJApplet";

            obj.width="1";
            obj.height="1";
            obj.scriptable="true";
            obj.codebase="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/";

            document.getElementById("objectsContainer").appendChild(obj);    
	    }
	}
	
	function loadMachineId(param)
	{
		if(callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet10?action=validateMachineId&param="+param) == "false"){
		  return "Acesso n�o autorizado!";
		}
		initialize();
	}
	
	function registerLog(texto)
	{
		if(showLog)
		{
			if(logWindow == null)
			{
				logWindow = window.open("", "_blank", "height=400,width=600,resizable=yes,scrollbars=yes,location=no,menubar=no,status=no,titlebar=no,toolbar=no");
				logWindow.document.write("Logs do Mapa <br/><br/>");
			}
			
			logWindow.document.write(new Date()+" - "+texto+"<br/>");
		}
	}
	
	function saveInputLog(placaViatura)
	{
		texto = prompt("Informar Log da Viatura:","");
		
		if(texto != null && texto != "")
		{
			var result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet10?action=saveLog&placa="+placaViatura+"&texto="+JSON.stringify(texto));
			
			alert(result);
		}
	}
	
	function verificaDeslocamento(viaturaObj,marker){
		if(viaturaObj.deslocamentoLento){
			marker.setAnimation(google.maps.Animation.BOUNCE);
		}else{
			marker.setAnimation(null);
		}
	}
	
	
	
	
	
</script>
</head>
<body id="bodyId">
	<div id="map_canvas" style="width: 100%; height: 100%"></div>
	<div class="jclock" id="jclockId" ></div>
	<div id="objectsContainer" style="visibility: hidden;"></div>
</body>
</html>
