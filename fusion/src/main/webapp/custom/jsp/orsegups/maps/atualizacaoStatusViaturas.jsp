<%@page import="com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.maps.vo.DeslocamentoVO"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNotNull"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.util.NeoDateUtils"%>

<%!

public class EventoVO
{
	private String placa = "";
	private Long codigoHistorico = 0L;
	private Long codigoCliente = 0L;
	private String codigoEvento = "";
	private GregorianCalendar dataDeslocamento;
	private Long codigoViatura = 0L;
	
	
	public String getPlaca() {
		return placa;
	}


	public void setPlaca(String placa) {
		this.placa = placa;
	}


	public Long getCodigoHistorico() {
		return codigoHistorico;
	}


	public void setCodigoHistorico(Long codigoHistorico) {
		this.codigoHistorico = codigoHistorico;
	}


	public Long getCodigoCliente() {
		return codigoCliente;
	}


	public void setCodigoCliente(Long codigoCliente) {
		this.codigoCliente = codigoCliente;
	}


	public String getCodigoEvento() {
		return codigoEvento;
	}


	public void setCodigoEvento(String codigoEvento) {
		this.codigoEvento = codigoEvento;
	}


	public GregorianCalendar getDataDeslocamento() {
		return dataDeslocamento;
	}


	public void setDataDeslocamento(GregorianCalendar dataDeslocamento) {
		this.dataDeslocamento = dataDeslocamento;
	}


	public Long getCodigoViatura() {
		return codigoViatura;
	}


	public void setCodigoViatura(Long codigoViatura) {
		this.codigoViatura = codigoViatura;
	}


	@Override
	public String toString()
	{
		return "EventoVO [placa=" + placa + ", cd_historico=" + codigoHistorico + "]";
	}
	
}


private NeoObject getStatusViatura(Long status)
{
	NeoObject statusViatura = null;

	try
	{
		/**
	 	* @author orsegups lucas.avila - Classe generica.
	 	* @date 08/07/2015
	 	*/			
		Class<? extends NeoObject> statusViaturaClazz = AdapterUtils.getEntityClass("OTSStatusViatura");

		QLEqualsFilter equalsFilter = new QLEqualsFilter("codigoStatus", status);

		statusViatura = PersistEngine.getObject(statusViaturaClazz, equalsFilter);
	}
	catch (Exception e)
	{
		e.printStackTrace();
	}

	return statusViatura;
}

private List<String> verificaAtrasoEvento()
{
	Connection conn = null;
	PreparedStatement preparedStatementHSelect = null;
	ResultSet rsH = null;
	Boolean flag = Boolean.FALSE;
	List<String> listaAtrasoEvento = null;
	try
	{

		int diferencaMinutos = -10;
		conn = PersistEngine.getConnection("SIGMA90");
		StringBuilder sqlSigma = new StringBuilder();
		sqlSigma.append(" SELECT h.CD_VIATURA ");
		sqlSigma.append(" FROM HISTORICO h WITH (NOLOCK)  ");
		sqlSigma.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON v.CD_VIATURA = h.CD_VIATURA ");
		sqlSigma.append(" WHERE h.FG_STATUS IN (3) 	 ");
		sqlSigma.append(" AND h.CD_EVENTO != 'XXX8'  ");
		sqlSigma.append(" AND h.DT_VIATURA_NO_LOCAL < DATEADD(MINUTE, " + diferencaMinutos + ", GETDATE()) ");
		sqlSigma.append(" AND h.CD_HISTORICO_PAI IS NULL  ");
		sqlSigma.append(" AND v.NM_PLACA IS NOT NULL ");
		sqlSigma.append(" AND v.NM_PLACA NOT LIKE '' ");
		//sqlSigma.append(" AND h.CD_VIATURA = ? ");

		sqlSigma.append(" ORDER BY h.DT_VIATURA_DESLOCAMENTO DESC ");

		preparedStatementHSelect = conn.prepareStatement(sqlSigma.toString());
		//preparedStatementHSelect.setString(1, cdViatura);
		rsH = preparedStatementHSelect.executeQuery();
		listaAtrasoEvento = new ArrayList<String>();
		while (rsH.next())
		{
			String cdVtr = rsH.getString("CD_VIATURA");
			listaAtrasoEvento.add(cdVtr);
		}

	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
		return listaAtrasoEvento;
	}

}

private Boolean getDeslocamentoLento(String placaStr, String regionalStr)
{

	Collection<ViaturaVO> lista = null;
	String nomeFonteDados = "SIGMA90";
	Boolean lowDeslocamento = Boolean.FALSE;
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	try
	{

		if (NeoUtils.safeIsNotNull(placaStr) && NeoUtils.safeIsNotNull(regionalStr))
		{
			lista = new ArrayList<ViaturaVO>();

			conn = PersistEngine.getConnection(nomeFonteDados);


				StringBuilder sql = new StringBuilder();
				String placa = placaStr;
				String regional = regionalStr;
				String x8IgnoreTags = "busy";
				String filtrosConta = "";
				String filtrosNotConta = "*:AAAA,*:AAA1,*:AAA2,*:AAA3,*:AAA4,*:AAA5,*:AAA6";

				String sqlEvento = "";
				String sqlAnd = "";
				String sqlAndH9 = "";

				//medir tempo execucao
				//Long timeExecFinal = GregorianCalendar.getInstance().getTimeInMillis();
				//Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

				//log.warn(" getDeslocamentoLento Orsegups Maps Tempo execu��o getDeslocamentoLento - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
				sql.append(" SELECT h2.CD_VIATURA, v.NM_PLACA ");
				sql.append(" FROM HISTORICO h2 ");
				sql.append(" INNER JOIN VIATURA v ON v.CD_VIATURA = h2.CD_VIATURA   ");
				sql.append(" INNER JOIN DBCENTRAL c ON C.CD_CLIENTE = h2.CD_CLIENTE   ");
				sql.append(" WHERE FG_STATUS = ? AND v.NM_PLACA = ?  AND  h2.DT_VIATURA_DESLOCAMENTO <= DATEADD(mi,-1,GETDATE()) ");

				if (regional != null && !regional.isEmpty() && !regional.equalsIgnoreCase("null"))
				{
					sqlAnd += "AND (  ";
					StringTokenizer st = new StringTokenizer(regional, ",");
					int count = 0;

					while (st.hasMoreElements())
					{
						String siglaRegional = (String) st.nextElement();
						if (count > 0)
							sqlAnd += " OR ";
						sqlAnd += " v.NM_VIATURA LIKE '" + siglaRegional + "%'  ";

						// para controle dos operadores do SQL dinamico (OR)
						count++;
					}
					sqlAnd += " ) ";

				}

				if (x8IgnoreTags != null && !x8IgnoreTags.isEmpty() && !x8IgnoreTags.equalsIgnoreCase("null"))
				{
					StringTokenizer stTag = new StringTokenizer(x8IgnoreTags, ",");
					int countToken = 0;
					while (stTag.hasMoreElements())
					{
						countToken++;
						String tag = (String) stTag.nextElement();
						if (countToken == 1)
						{
							sqlAnd += " AND (  ";
							sqlAndH9 += " AND (  ";
							sqlAnd += "  ISNULL (h2.TX_OBSERVACAO_GERENTE, '') NOT LIKE '%#" + tag + "%'  ";
							sqlAndH9 += " ISNULL (h9.TX_OBSERVACAO_GERENTE, '') NOT LIKE '%#" + tag + "%'  ";
						}
						else
						{
							sqlAnd += "  OR ISNULL (h2.TX_OBSERVACAO_GERENTE, '') NOT LIKE '%#" + tag + "%'  ";
							sqlAndH9 += "  OR ISNULL (h9.TX_OBSERVACAO_GERENTE, '') NOT LIKE '%#" + tag + "%' ";
						}
					}
					if (countToken > 0)
					{
						sqlAnd += " ) ";
						sqlAndH9 += " ) ";
					}
				}

				Map<String, Collection<String>> filtroContaMap = new HashMap<String, Collection<String>>();
				Map<String, Collection<String>> filtroNotContaMap = new HashMap<String, Collection<String>>();

				if (filtrosConta != null && !filtrosConta.isEmpty())
				{
					String contasArray[] = filtrosConta.split(",");
					for (String item : contasArray)
					{
						if (item != null)
						{
							String empresaConta[] = item.split(":");

							if (empresaConta != null && empresaConta.length == 2)
							{
								String empresa = empresaConta[0];
								String conta = empresaConta[1];

								if (empresa != null && conta != null)
								{
									Collection<String> contas = filtroContaMap.get(empresa);

									if (contas == null)
									{
										contas = new ArrayList<String>();
									}

									contas.add(conta);

									filtroContaMap.put(empresa, contas);
								}
							}
						}
					}
				}
				if (filtrosNotConta != null && !filtrosNotConta.isEmpty())
				{
					String notContasArray[] = filtrosNotConta.split(",");
					for (String itemNot : notContasArray)
					{
						if (itemNot != null)
						{
							String empresaNotConta[] = itemNot.split(":");

							if (empresaNotConta != null && empresaNotConta.length == 2)
							{
								String notEmpresa = empresaNotConta[0];
								String notConta = empresaNotConta[1];

								if (notEmpresa != null && notConta != null)
								{
									Collection<String> notContas = filtroNotContaMap.get(notEmpresa);

									if (notContas == null)
									{
										notContas = new ArrayList<String>();
									}

									notContas.add(notConta);

									filtroNotContaMap.put(notEmpresa, notContas);
								}
							}
						}
					}
				}

				//filtra por conta e empresa
				if (filtroContaMap != null && !filtroContaMap.isEmpty())
				{
					Boolean firstEmpresa = true;
					Boolean needClose = false;

					for (String empresa : filtroContaMap.keySet())
					{
						Collection<String> contas = filtroContaMap.get(empresa);

						if (contas != null && !contas.isEmpty())
						{
							if (firstEmpresa)
							{
								sqlEvento += "AND (";
								firstEmpresa = false;
								needClose = true;
							}
							else
							{
								sqlEvento += "OR ";
							}

							sqlEvento += "( ";

							if (!empresa.trim().equals("*"))
							{
								sqlEvento += " c.ID_EMPRESA = " + empresa + " AND ";
							}

							sqlEvento += " c.ID_CENTRAL IN (";
							Boolean first = true;
							for (String conta : contas)
							{
								if (first)
								{
									sqlEvento += "'" + conta + "'";
									first = false;
								}
								else
								{
									sqlEvento += ", '" + conta + "'";
								}
							}
							sqlEvento += ") ) ";
						}
					}

					if (needClose)
					{
						sqlEvento += ") ";
					}
				}

				//filtra (ELIMINA) contas (e empresa)
				if (filtroNotContaMap != null && !filtroNotContaMap.isEmpty())
				{
					Boolean firstNotEmpresa = true;
					Boolean needClose = false;

					for (String notEmpresa : filtroNotContaMap.keySet())
					{
						Collection<String> notContas = filtroNotContaMap.get(notEmpresa);

						if (notContas != null && !notContas.isEmpty())
						{
							if (firstNotEmpresa)
							{
								sqlEvento += "AND (";
								firstNotEmpresa = false;
								needClose = true;
							}
							else
							{
								sqlEvento += "OR ";
							}

							sqlEvento += "( ";

							if (!notEmpresa.trim().equals("*"))
							{
								sqlEvento += " c.ID_EMPRESA <> " + notEmpresa + " AND ";
							}

							sqlEvento += " c.ID_CENTRAL NOT IN (";
							Boolean first = true;
							for (String notConta : notContas)
							{
								if (first)
								{
									sqlEvento += "'" + notConta + "'";
									first = false;
								}
								else
								{
									sqlEvento += ", '" + notConta + "'";
								}
							}
							sqlEvento += ") ) ";
						}
					}

					if (needClose)
					{
						sqlEvento += ") ";
					}
				}

				sql.append(sqlAnd.toString());
				sql.append(sqlEvento.toString());
				sql.append(" AND NOT EXISTS (SELECT CD_HISTORICO  ");
				sql.append("  FROM HISTORICO h9 ");
				sql.append(" INNER JOIN  DBCENTRAL c ON c.CD_CLIENTE = h9.CD_CLIENTE " + new StringBuilder(sqlEvento).toString() + " " + new StringBuilder(sqlAndH9).toString());
				sql.append("  WHERE h9.FG_STATUS IN (9,3) ");
				sql.append(" AND h2.CD_VIATURA = h9.CD_VIATURA) ");

				pstm = conn.prepareStatement(sql.toString());
				pstm.setString(1, "2");
				pstm.setString(2, placa);
				//pstm.setString(3, "9");
				//pstm.setString(4, "3");

				rs = pstm.executeQuery();

				if (rs.next())
					lowDeslocamento = Boolean.TRUE;

		}
	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{

		try
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return lowDeslocamento;

	}

}

%>

<body><%
		
	Connection connSigma = null;
	PreparedStatement pstmSigma = null;
	ResultSet rsSigma = null;
	Map<String, EventoVO> listaEventos = new HashMap<String, EventoVO>();
    // Definindo par�metros para viatura offline e/ou atrasada
	int diferencaMinutos = -3;
	
	try
	{
		connSigma = PersistEngine.getConnection("SIGMA90");

		StringBuilder sqlSigma = new StringBuilder();
		sqlSigma.append(" SELECT v.NM_PLACA, h.CD_HISTORICO, h.CD_CLIENTE, h.CD_EVENTO, h.DT_VIATURA_DESLOCAMENTO, h.CD_VIATURA ");
		sqlSigma.append(" FROM HISTORICO h WITH (NOLOCK)  ");
		sqlSigma.append(" INNER JOIN VIATURA v WITH (NOLOCK) ON v.CD_VIATURA = h.CD_VIATURA ");
		sqlSigma.append(" WHERE h.FG_STATUS IN (9) 	 ");
		sqlSigma.append(" AND h.CD_EVENTO != 'XXX8'  ");
		sqlSigma.append(" AND h.DT_VIATURA_DESLOCAMENTO < DATEADD(MINUTE, "+diferencaMinutos+", GETDATE()) ");
		sqlSigma.append(" AND h.CD_HISTORICO_PAI IS NULL  ");
		sqlSigma.append(" AND v.NM_PLACA IS NOT NULL ");
 		sqlSigma.append(" AND v.NM_PLACA NOT LIKE '' ");
		sqlSigma.append(" ORDER BY h.DT_VIATURA_DESLOCAMENTO DESC ");
		
		// Recupera Eventos Deslocados
		pstmSigma = connSigma.prepareStatement(sqlSigma.toString());
		rsSigma = pstmSigma.executeQuery();
		EventoVO evento = null;
		
		while (rsSigma.next())
		{
			evento = new EventoVO();
			evento.setPlaca(rsSigma.getString("NM_PLACA"));
			evento.setCodigoHistorico(rsSigma.getLong("CD_HISTORICO"));
			evento.setCodigoCliente(rsSigma.getLong("CD_CLIENTE"));
			evento.setCodigoEvento(rsSigma.getString("CD_EVENTO"));
			GregorianCalendar gtime = new GregorianCalendar();
            gtime.setTime(rsSigma.getTimestamp("DT_VIATURA_DESLOCAMENTO"));
			evento.setDataDeslocamento(gtime);
			evento.setCodigoViatura(rsSigma.getLong("CD_VIATURA"));
			
			// Adicionando a lista
			listaEventos.put(evento.getPlaca(), evento);
			//System.out.println(evento);
		}
		
		
	}
	catch (Exception e)
	{
		e.printStackTrace();		
	}
	finally
	{
		try
		{
			OrsegupsUtils.closeConnection(connSigma, pstmSigma, rsSigma);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	// filtro de viaturas em Uso
	QLGroupFilter aplicacaoViaturaFilter = new QLGroupFilter("OR");
	aplicacaoViaturaFilter.addFilter(new QLEqualsFilter("aplicacaoViatura.sigla", "TAT"));
	aplicacaoViaturaFilter.addFilter(new QLEqualsFilter("aplicacaoViatura.sigla", "TSH"));
	aplicacaoViaturaFilter.addFilter(new QLEqualsFilter("aplicacaoViatura.sigla", "TSA"));
	
	QLGroupFilter viaturaFilter = new QLGroupFilter("AND");
	viaturaFilter.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	viaturaFilter.addFilter(new QLOpFilter("rastreador.modeloEquipamento.nomeModelo", " NOT LIKE ", "%SEGWARE%"));
	viaturaFilter.addFilter(aplicacaoViaturaFilter);
	
	GregorianCalendar dataHoraReferencia = new GregorianCalendar();
	dataHoraReferencia.add(GregorianCalendar.MINUTE, diferencaMinutos);
	
	
	// Objeto status viatura offline - c�digo: 1
	NeoObject statusOffline = getStatusViatura(0L);
	
	List<NeoObject> listaViaturas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getInstantiableEntityInfo("OTSViatura").getEntityClass(), viaturaFilter);
	List<String> listaExcessodeTempoNoLocal = verificaAtrasoEvento();
	for (NeoObject noViatura : listaViaturas) {
			EntityWrapper ewViatura = new EntityWrapper(noViatura);
			GregorianCalendar ultimaAtualizacao = (GregorianCalendar) ewViatura.findField("ultimaAtualizacao").getValue();
			GregorianCalendar ultimoDeslocamento = (GregorianCalendar) ewViatura.findField("ultimoStatusDeslocamento").getValue();
			GregorianCalendar horaReferencia = new GregorianCalendar();
			horaReferencia.add(GregorianCalendar.MINUTE, -3);

			Long status = (Long) ewViatura.findValue("statusViatura.codigoStatus");

			EventoVO evento = listaEventos.get((String) ewViatura.findField("placa").getValue());
			boolean flag = false;
			if (evento != null && (ultimoDeslocamento == null || ultimoDeslocamento.before(horaReferencia)))
			{
			    if (!ultimaAtualizacao.before(horaReferencia)){
				    System.out.println("atualizacaoStatusViatura Hora referencia: "+ewViatura.findField("placa").getValue()+" "+NeoDateUtils.safeDateFormat(horaReferencia, "dd/MM/yyyy HH:mm:ss"));
				    System.out.println("atualizacaoStatusViatura Ultimo deslocamento: "+ewViatura.findField("placa").getValue()+" "+NeoDateUtils.safeDateFormat(ultimoDeslocamento, "dd/MM/yyyy HH:mm:ss"));
					ewViatura.findField("atrasoDeslocamento").setValue(Boolean.TRUE);
					out.println(ewViatura.findField("placa").getValue() + " - " + ewViatura.findField("titulo").getValue() + " - " + ewViatura.findField("statusViatura.tituloStatus").getValue());
					out.println(evento.getPlaca() + " - " + evento.getCodigoEvento() + " - " + evento.getCodigoCliente() + " ----- ATRASADA");
					flag = true;
			    }
			}
			else
			{
				ewViatura.findField("atrasoDeslocamento").setValue(Boolean.FALSE);
			}
			
			String cdViatura = (String) ewViatura.findField("codigoViatura").getValue();
			
			if(!flag && listaExcessodeTempoNoLocal != null && !listaExcessodeTempoNoLocal.isEmpty() && listaExcessodeTempoNoLocal.contains(cdViatura))
			{
				ewViatura.findField("excessoDeTempoNoLocal").setValue(Boolean.TRUE);
				flag = true;
			}
			else
			{
				ewViatura.findField("excessoDeTempoNoLocal").setValue(Boolean.FALSE);
			}
			
			String placaStr = (String)ewViatura.findField("placa").getValue();
			String regionalStr = (String)ewViatura.findField("codigoRegional").getValue();
			if(!flag && getDeslocamentoLento(placaStr,regionalStr))
			{
				ewViatura.findField("deslocamentoNaoIniciado").setValue(Boolean.TRUE);
			}
			else
			{
				ewViatura.findField("deslocamentoNaoIniciado").setValue(Boolean.FALSE);
			}
			
	}

	
%>
</body>
