
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<html>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertEventEngine"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%
	String tipo = ((String)request.getParameter("tipo")) == null ? "1" : (String)request.getParameter("tipo");
%>

<!DOCTYPE html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.css" media="screen" />

<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.mask.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.callcenter.validade.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.js"></script>


<script type="text/javascript" >

<%final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.maps.rotinaEventosMonitoramento");%>
	
	var tipoAtendimento = "<%= tipo %>";
	$(document).ready(function() {
			initialize();
	});
	
	function initialize() 
	{
		this.loadData();
	}
	
	function loadData()
	{
		
		$.ajax({
		  type: "GET",
		  url: "<%=PortalUtil.getBaseURL()%>services/eventos/verificaEventos/"+tipoAtendimento
		});
		
	}
	
	
	<%log.debug("##### EXECUTAR ROTINA DE EVENTOS MONITORAMENTO  - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));%>

	
	
</script>
</head>

<body >
	
</body>

</html>