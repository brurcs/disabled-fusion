<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>    
<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="com.neomind.fusion.persist.QLFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.common.NeoRunnable"%>   
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="com.neomind.util.NeoUtils"%>

<%@page import="com.neomind.fusion.custom.orsegups.contract.ContractSIDClient"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESTE Adapter FGC</title>
</head>
<body>
<%
	try{
		if (request.getParameter("act") == null){
			%>
			<form method="post">
				<input type="hidden" id="act" name="act" value="init">
				<label>Codccu a inicializar no fusion:</label>
				<input type="text" name="ccu">
				<input type="submit" value="Processar">
			</form>
			<%
		}else if ( request.getParameter("act").equals("init") ) {
			if (request.getParameter("ccu") != null ){
				List<NeoObject> centrocusto = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"),
						new QLGroupFilter("AND", new QLEqualsFilter("codccu", "8098240")));
				%>
				<form method="post">
					<input type="hidden" id="act" name="act" value="init">
					<label>Codccu a inicializar no fusion:</label>
					<input type="text" name="ccu">
					<input type="submit" value="Processar">
				</form>
				<br>OK
				<%
			}
		}
		
	}catch(Exception e){
		e.printStackTrace();
		%>
		<%= e.getMessage() %>
		<%
	}


%>

</body>
</html>