<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.fusion.workflow.DeadLine"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="com.neomind.xpdl.elements.Deadline"%>
<%@page import="com.neomind.fusion.workflow.deadLine.DeadlineService"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.engine.runtime.RuntimeEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ajusta Deadline</title>
</head>
<body>
<%
		String taskId = request.getParameter("taskId");
		String deadId = request.getParameter("deadId");
		String prazo = request.getParameter("prazo");
		
		try
		{
			GregorianCalendar newPrazo = OrsegupsUtils.stringToGregorianCalendar(prazo, "dd/MM/yyyy HH:mm");
			DeadLine dead = PersistEngine.getObject(DeadLine.class, new QLRawFilter(" neoId = " + deadId));
			DeadlineService.changeDate(Long.valueOf(taskId), dead, newPrazo);
			out.print("OK");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			out.print("ERRO");
		}
%>
</body>
</html>