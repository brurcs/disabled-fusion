<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.doc.pdf.ListaPag"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.custom.orsegups.gestaopatrimonio.vo.PatrimonioVO"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@taglib uri="/WEB-INF/form.tld" prefix="form"%>

<%
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.gestaoPatrimonio.gestaoPatrimonio");
	NeoUser user = PortalUtil.getCurrentUser();
	List<PatrimonioVO> listPatrimonio = new ArrayList<PatrimonioVO>(); 

	QLRawFilter rawFilter = new QLRawFilter(" setor.descricao in (select s.descricao from com.neomind.fusion.entity.dyn.GDPSetor s join s.responsaveis u where u.code = '"+user.getCode() +"') and ativo = 1 ");
	List<NeoObject> patrimonios = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GDPPatrimonio"),rawFilter, -1, -1, "codigo asc");
	
	if (NeoUtils.safeIsNotNull(patrimonios)) {
		for (NeoObject obj : patrimonios) {
			
			EntityWrapper wrapperPatrimonio = new EntityWrapper(obj);
			GregorianCalendar data = (GregorianCalendar) wrapperPatrimonio.findValue("dataPatrimonio");
			PatrimonioVO patrimonioVO = new PatrimonioVO();
			patrimonioVO.setCodigo((String) wrapperPatrimonio.findValue("codigo"));
			patrimonioVO.setProduto((String) wrapperPatrimonio.findValue("produto.despro"));
			patrimonioVO.setDataPatrimonio(NeoUtils.safeDateFormat(data));
			patrimonioVO.setEmpresa((String) wrapperPatrimonio.findValue("empresas.nomemp"));
			patrimonioVO.setSetor((String) wrapperPatrimonio.findValue("setor.descricao"));
			patrimonioVO.setLocal((String) wrapperPatrimonio.findValue("posto"));
			patrimonioVO.setDescricao((String) wrapperPatrimonio.findValue("descricaoPatrimonio"));
			patrimonioVO.setCor((String) wrapperPatrimonio.findValue("cor"));
			patrimonioVO.setMarca((String) wrapperPatrimonio.findValue("marca"));
			patrimonioVO.setNumeroSerie((String) wrapperPatrimonio.findValue("numSerie"));
			patrimonioVO.setReponsavel((String) wrapperPatrimonio.findValue("responsavel.fullName"));
			patrimonioVO.setFornecedor((String) wrapperPatrimonio.findValue("fornecedor"));
			patrimonioVO.setNumeroNota((String) wrapperPatrimonio.findValue("numNotaFiscal"));
			listPatrimonio.add(patrimonioVO);
		}
	}
%>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<portal:head title="Gest�o de Patrim�nio">
		<cw:main>
			<cw:header title=" Gest�o de Patrim�nio " />
			<cw:body id="area_scroll">
				<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
					<tbody>	
						<tr id="patrimonio" style="cursor: auto">
							<th style="cursor: auto">C�digo</th>
							<th style="cursor: auto">Produto</th>
							<th style="cursor: auto">Data Patrim�nio</th>
							<th style="cursor: auto">Empresa</th>
							<th style="cursor: auto">Setor</th>
							<th style="cursor: auto">Local</th>
							<th style="cursor: auto">Descri��o</th>
							<th style="cursor: auto">Marca</th>
							<th style="cursor: auto">Cor</th>
							<th style="cursor: auto">N� de S�rie</th>
							<th style="cursor: auto">Respons�vel</th>
							<th style="cursor: auto">Fornecedor</th>
							<th style="cursor: auto">N� Nota Fiscal</th>
						</tr>					
						<%
							for (PatrimonioVO list : listPatrimonio)
							{
						%>
								<tr>
									<td><%= list.getCodigo() %></td>
									<td><%= list.getProduto() %></td>
									<td><%= list.getDataPatrimonio() %></td>
									<td><%= list.getEmpresa() %></td>
									<td><%= list.getSetor() %> </td>
									<td><%= list.getLocal() %></td>
									<td><%= list.getDescricao() %></td>
									<td><%= list.getMarca() %></td>
									<td><%= list.getCor()  %></td>
									<td><%= list.getNumeroSerie() %></td>
									<td><%= list.getReponsavel() %></td>
									<td><%= list.getFornecedor() %></td>
									<td><%= list.getNumeroNota()  %></td>
								</tr>
						<%
							}
						%>
				</tbody>
			</table>
			</cw:body>
		</cw:main>
	</portal:head>
	<body>
	</body>
</html>