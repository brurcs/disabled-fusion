

<%@page import="com.neomind.fusion.security.NeoGroup"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.custom.orsegups.ti.TarefasGrupoUsuarioVO"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.custom.orsegups.ti.TarefasUsuarioVO"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>

<%@page import="com.neomind.fusion.custom.orsegups.ti.TarefasVO"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.TarefasUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@taglib uri="/WEB-INF/form.tld" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="application/vnd.android.package-archive; charset=ISO-8859-1">

	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<portal:head title="Controle de tarefas">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap.min.css">
	
	<!-- Optional theme -->
	<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap-theme.min.css">
	
	<!-- Latest compiled and minified JavaScript -->
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
	
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>	
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.js"></script>
	

	<script type="text/javascript" src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>

	

	<style type="text/css">
	
	   body {
	    margin: 5px;
	    background: #A6A6A6;
	    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	    font-size: 14px;
	  }
	   .popover-title {
		    color: black;
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		    font-size: 15px;
		}
		.popover-content {
		    color: black;
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		    font-size: 10px;
		}
		/* Tab Navigation */
		.nav-tabs {
		    margin: 0;
		    padding: 0;
		    border: 0;    
		}
		.nav-tabs > li > a {
		    background: #DADADA;
		    border-radius: 0;
		    box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);5
		}
		.nav-tabs > li.active > a,
		.nav-tabs > li.active > a:hover {
		    background: #F5F5F5;
		    box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
		}
		
		/* Tab Content */
		.tab-pane {
		    background: #F5F5F5;
		    box-shadow: 0 0 4px rgba(0,0,0,.4);
		    border-radius: 0;
		    text-align: center;
		    padding: 10px;
		    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	   		font-size: 14px;
		}
		.topicKeywords {
        	min-height: 50px;
   	 	}
   	 	
   	 	#snoAlertBox{
			position:absolute;
		    z-index:1400;
		    top:2%;
		    right:4%;
		    margin:0px auto;
			text-align:center;
		    display:none;
		}
		#snoAlertBoxError{
			position:absolute;
		    z-index:1400;
		    top:2%;
		    right:4%;
		    margin:0px auto;
			text-align:center;
		    display:none;
		}
		#snoAlertBoxInfo{
			position:absolute;
		    z-index:1400;
		    top:2%;
		    right:4%;
		    margin:0px auto;
			text-align:center;
		    display:none;
		}
		
		.scrollable-menu {
	    height: auto;
	    max-height: 180px;
	    overflow-x: hidden;
		}
		.scrollable-menu::-webkit-scrollbar {
	    -webkit-appearance: none;
	    width: 4px;        
	    }    
	    .scrollable-menu::-webkit-scrollbar-thumb {
	    border-radius: 3px;
	    background-color: lightgray;
	    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);        
	    }		

	</style>

	<%
    final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.consControleTarefasTeste");
	String nomeUsuarioAtual = "";
	List<TarefasGrupoUsuarioVO> tarefasVOs = null;
	
	int count = 0;
	int total = 0;
	%>

	<script type="text/javascript">
	var timeOut;
	var json;
	var timeoutSistemas;
	var timeoutInfra;
	var timeoutChart;
	var timeoutNexti;
	var timeoutConsultoria;
	
	$(document).ready(function() {
		$('#mainTabs a').click(function (e) {
	        e.preventDefault();
	        $(this).tab('show');
	    });

	    // store the currently selected tab in the hash value
	    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	        var id = $(e.target).attr("href").substr(1);
	        window.location.hash = id;
	        
	    });

	    // on load of the page: switch to the currently selected tab
	    var hash = window.location.hash;
	    $('#mainTabs a[href="' + hash + '"]').tab('show');
	    
	    <%
			NeoUser usuario = PortalUtil.getCurrentUser();
	    	NeoGroup grupo = usuario.getGroup();
	    	
	    	if (grupo.getNeoId() == 18317794 || grupo.getNeoId() == 639014270 || grupo.getNeoId() == 638564840){ // SISTEMAS E NEXTI
	    	    //CARREGAR SISTEMAS,NEXTI E CONSULTORIA. OCULTAR INFRA
	    	    %>
				 loadTarefasSistemas();
				 loadTarefasNexti();
				 loadTarefasConsultoria();
				 $('#tabInfra').css('display','none');
				 <%
	    	}else if (grupo.getNeoId() == 18317811){ //INFRA
	    	    //CARREGAR INFRA. OCULTAR DEMAIS
	    	    %>
	
	    		$('#tabSistemas').css('display','none');
	    		$('#tabNexti').css('display','none');
	    		$('#tabConsultoria').css('display','none');
	    	    $('#tabSistemas').siblings('li').removeClass('active');
	    	    $('#linkInfra').click(); 
	    		loadTarefasInfra();
	    		<%
	    	}else if (grupo.getNeoId() == 638338425 || grupo.getNeoId() == 638347864 || grupo.getNeoId() == 647233104){ //CONSULTORIA
	    	    //CARREGAR CONSULTORIA. OCULTAR DEMAIS
	    	    %>
	    		$('#tabSistemas').css('display','none');
	    		$('#tabSistemas').siblings('li').removeClass('active');
	    		$('#tabInfra').css('display','none');
	    		$('#tabInfra').css('display','none');
	    		$('#tabNexti').css('display','none');
	    		 $('#linkConsultoria').click();
	    		loadTarefasConsultoria();
	    		<%
	    	}else if (grupo.getNeoId() == 81160258){ // DIRETORIA TI
	    	    //CARREGAR TODOS
	    	    %>
				 loadTarefasSistemas();
				 loadTarefasInfra();
				 loadTarefasNexti();
				 loadTarefasConsultoria();
				 <%
	    	}
	    
	    %>
	    
 		 //loadChart();
	});
	
	$(document).on('hidden.bs.modal', function (e) {
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			//save the latest tab; use cookies if you like 'em better:
			localStorage.setItem('selectedTab', $(e.target).attr('#tab'));
			});
	});
	
	
	$(function() {
		$('[data-toggle="tooltip"]').tooltip({
			  trigger: 'hover',
			'placement': 'top'
		});
	});

	$(function() {
		$('[data-toggle="popover"]').popover({
		    trigger: 'hover',
		        'placement': 'top'
		});
	});

	$(function() {
		$('.dropdown-toggle').dropdown();
	});
	$(document.body).on(
			'click',
			'.dropdown-menu li',
			function(event) {

				var $target = $(event.currentTarget);

				$target.closest('.btn-group').find('[data-bind="label"]').text(
						$target.text()).end().children('.dropdown-toggle')
						.dropdown('toggle');

				return false;

	});
	

	
	function showTarefa(neoid) {
		var url = '<%=PortalUtil.getBaseURL()%>bpm/workflow_search.jsp?id='+neoid;
		var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,'');
		var win = window.name; 
		
	    NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win);
	}
	
	 $(function(){
         $(".dropdown-menu").on("click", "li", function(event){
        	 $("#dropdown-menu option:selected").index();
         });
     });
	 
	 $(function(){
	 $(".dropdown-menu").on("click", "li a", function() {
		    var platform = $(this).text();
		    $(".dropdown_title").html(platform);
		}); 
	  });
	 
	 $(function(){
	 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		 var target = $(e.target).attr("href"); // activated tab
		 $(target).tab('show');
		 clearTimeout(timeOut);
		 releaseEvents();
		 });
	  });
	 
	 function showTipo(tarefa,tipo,comp,neoDiv,code,model){
		       $.ajax({
		           type:"post",
		           url:"servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet",
		           data:'action=trocarTipo&tarefa='+tarefa+'&tipo='+tipo+'&model='+model,
		           dataType:'text',
		           success: function(result) {
		        	   alertSucess();
		        	   if(model == 18888)
		        	  	  showAtualizaTabela(code,neoDiv);
		        	   else
		        		  showAtualizaTabelaChipGPRS(code,neoDiv);
		           },  
	      			error: function(e){  
	      				alertError();
	          	 }
		       });
	 }	
	 
	 function showSituacao(tarefa,situacao,comp,neoDiv,code,model){
	       $.ajax({
	           type:"post",
	           url:"servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet",
	           data:'action=trocarSituacao&tarefa='+tarefa+'&situacao='+situacao+'&model='+model,
	           dataType:'text',
	           success: function(result) {
	        	   alertSucess();
	        	   if(model == 18888)
		        	  	  showAtualizaTabela(code,neoDiv);
		        	   else
		        		  showAtualizaTabelaChipGPRS(code,neoDiv);
  
	           },  
    			error: function(e){  
    				alertError();
        	 }
	       });
	 }	
	 
	
	 function showSolicitante(idGrupo,solicitante,idGrupoSolicitante,grupoAux,model,neoDiv,grupoSolicitante)
	 {
		 var model = "18888";
	       $.ajax({
	           type:"post",
	           url:"servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet",
	           data:'action=montaTabelaTarefasGrupoSolicitante&wfProcess='+idGrupo+'&model='+model+'&grupo='+grupoAux+'&solicitante='+solicitante+'&idGrupo='+idGrupoSolicitante+"&grupoSolicitante="+grupoSolicitante,
	           dataType:'text',
	           success: function(result) {
	        	  $("#"+neoDiv).html(result);
	        	  $('#mainTabs a').click(function (e) {
	        	        e.preventDefault();
	        	        $(this).tab('show');
	        	    });

	        	    // store the currently selected tab in the hash value
	        	    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	        	        var id = $(e.target).attr("href").substr(1);
	        	        window.location.hash = id;
	        	    });

	        	    // on load of the page: switch to the currently selected tab
	        	    var hash = window.location.hash;
	        	    $('#mainTabs a[href="' + hash + '"]').tab('show');
	        		$(function() {
	        			$('[data-toggle="tooltip"]').tooltip({
	        			    'placement': 'top'
	        			});
	        		});

	        		$(function() {
	        			$('[data-toggle="popover"]').popover({
	        			    trigger: 'hover',
	        			        'placement': 'top'
	        			});
	        		});
	        		$(function() {
	        			$('.dropdown-toggle').dropdown();
	        		});
	           },  
 			error: function(e){  
 				alertError();
     	  }
	     });
	 }	
	 
	 
	 function showPosicao(tarefa,posicao,code,comp,neoDiv,model){
	       $.ajax({
	           type:"post",
	           url:"servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet",
	           data:'action=atualizaPosicaoTarefa&tarefa='+tarefa+'&posicao='+posicao+'&code='+code+'&model='+model,
	           dataType:'text',
	           success: function(result) {
	        	   alertSucess();
	        	   if(model == 18888)
		        	  	  showAtualizaTabela(code,neoDiv);
		        	   else
		        		  showAtualizaTabelaChipGPRS(code,neoDiv);
	           },  
      			error: function(e){  
      				alertError();
          	 }
	       });
	 }	
	
	 function showAtualizaTabela(code,neoDiv){
		  var wfProcess = "18317794";
		  var model = "18888";
	       $.ajax({
	           type:"post",
	           url:"servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet",
	           data:'action=montaTabelaTarefasUsuario&wfProcess='+wfProcess+'&model='+model+'&code='+neoDiv,
	           dataType:'text',
	           success: function(result) {
	        	  $("#"+neoDiv).html(result);
	        	  
	        	  $('#mainTabs a').click(function (e) {
	        	        e.preventDefault();
	        	        $(this).tab('show');
	        	    });

	        	    // store the currently selected tab in the hash value
	        	    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	        	        var id = $(e.target).attr("href").substr(1);
	        	        window.location.hash = id;
	        	    });

	        	    // on load of the page: switch to the currently selected tab
	        	    var hash = window.location.hash;
	        	    $('#mainTabs a[href="' + hash + '"]').tab('show');
	        	  
	        		$(function() {
	        			$('[data-toggle="tooltip"]').tooltip({
	        			    'placement': 'top'
	        			});
	        		});

	        		$(function() {
	        			$('[data-toggle="popover"]').popover({
	        			    trigger: 'hover',
	        			        'placement': 'top'
	        			});
	        		});
	        		$(function() {
	        			$('.dropdown-toggle').dropdown();
	        		});
	           },  
    			error: function(e){  
    				alertError();
        	 }
	       });
	 }	
	 
	 function showAtualizaTabelaChipGPRS(code,neoDiv){
		  var wfProcess = "18317811";
		  var model = "186043073";
		  var user = code;
	       $.ajax({
	           type:"post",
	           url:"servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet",
	           data:'action=montaTabelaChipGPRSUsuario&wfProcess='+wfProcess+'&model='+model+'&code='+user,
	           dataType:'text',
	           success: function(result) {
	 
	        	  $("#"+neoDiv).html(result);
	        	  $('#mainTabs a').click(function (e) {
	        	        e.preventDefault();
	        	        $(this).tab('show');
	        	    });

	        	    // store the currently selected tab in the hash value
	        	    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	        	        var id = $(e.target).attr("href").substr(1);
	        	        window.location.hash = id;
	        	    });

	        	    // on load of the page: switch to the currently selected tab
	        	    var hash = window.location.hash;
	        	    $('#mainTabs a[href="' + hash + '"]').tab('show');
	        	  $(function() {
	        			$('[data-toggle="tooltip"]').tooltip({
	        			    'placement': 'top'
	        			});
	        		});

	        		$(function() {
	        			$('[data-toggle="popover"]').popover({
	        			    trigger: 'hover',
	        			        'placement': 'top'
	        			});
	        		});
	        		$(function() {
	        			$('.dropdown-toggle').dropdown();
	        		});
	           },  
   			error: function(e){  
   				alertError();
       	 }
	       });
	 }	
	 
	 function showBarChart(){
	     var tarefa = '11111';
	     var tipo = '11111';
		 $.ajax({
	           type:"post",
	           url:"servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet",
	           data:'action=chartTarefasPorGrupo&tarefa='+tarefa+'&tipo='+tipo,
	           dataType:'text',
	           success: function(result) {
	        	   var obj = jQuery.parseJSON(result);
	        	   $(function () {
	       		    $('#containerChart').highcharts({
	       		        chart: {
	       		            type: 'column'
	       		        },
	       		        title: {
	       		            text: 'Totalizador de tarefas em atendimento.'
	       		        },
	       		        subtitle: {
	       		            text: 'Quantitativo T.I.'
	       		        },
	       		        xAxis: {
	       		            type: 'category',
	       		            labels: {
	       		                rotation: -45,
	       		                style: {
	       		                    fontSize: '13px',
	       		                    fontFamily: 'Verdana, sans-serif'
	       		                }
	       		            }
	       		        },
	       		        yAxis: {
	       		            min: 0,
	       		            title: {
	       		                text: 'Tarefas'
	       		            }
	       		        },
	       		        legend: {
	       		            enabled: false
	       		        },
	       		        tooltip: {
	       		            pointFormat: 'Tarefas: <b>{point.y} tarefas</b>'
	       		        },
	       		        series: [{
	       		            name: 'Tarefas',
	       		            data: obj,
	       		            dataLabels: {
	       		                enabled: true,
	       		                rotation: -90,
	       		                color: '#FFFFFF',
	       		                align: 'right',
	       		                y: 10, // 10 pixels down from the top
	       		                style: {
	       		                    fontSize: '13px',
	       		                    fontFamily: 'Verdana, sans-serif'
	       		                }
	       		            }
	       		        }]
	       		    });
	       		});
	        	  
	           },  
    			error: function(e){  
    				alert('Erro '+e);
        	 }
	       });
	}	
	 
	 
		
	 $('#mainTabs a').click(function (e) {
	        e.preventDefault();
	        $(this).tab('show');
	    });

	    // store the currently selected tab in the hash value
	    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	        var id = $(e.target).attr("href").substr(1);
	        window.location.hash = id;
	    });

	    // on load of the page: switch to the currently selected tab
	    var hash = window.location.hash;
	    $('#mainTabs a[href="' + hash + '"]').tab('show');
		
	 function reloadData()
	 {
		
		<% 
		tarefasVOs = new ArrayList<TarefasGrupoUsuarioVO>();
		tarefasVOs = TarefasUtils.pesquisaTarefasPorGrupo("18317794,18317811,639014270,647233104", "18888");
		%>	
		
		timeOut = setTimeout("reloadData()",30000);
	

	 }
      
	 
	function closeSnoAlertBox()
	{
		window.setTimeout(function () {
		$("#snoAlertBox").fadeOut(600);
		}, 3000);
	}  
	
	function alertSucess(){
		
		$("#snoAlertBox").fadeIn();
        closeSnoAlertBox();
	}
	
	function closeSnoAlertBoxError()
	{
		window.setTimeout(function () {
		$("#snoAlertBoxError").fadeOut(600);
		}, 3000);
	}  
	
	function alertError(){
		
		$("#snoAlertBoxError").fadeIn();
		closeSnoAlertBoxError();
	}
	
	function closeSnoAlertBoxInfo()
	{
		window.setTimeout(function () {
		$("#snoAlertBoxInfo").fadeOut(600);
		}, 9000);
	}  
	
	function alertInfo(){
		
		$("#snoAlertBoxInfo").fadeIn();
		closeSnoAlertBoxInfo();
	}
	
	 function showAtualizaTab(wfProcess,neoDiv){
		  var model = "18888";
		  $("#"+neoDiv).html("<div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width:100%'>Aguarde...</div></div>");

	       $.ajax({
	           type:"post",
	           url:"servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet",
	           data:'action=montaTabelaTarefasGrupo&wfProcess='+wfProcess+'&model='+model,
	           dataType:'text',
	           success: function(result) {
	        	  $("#"+neoDiv).html(result);
	        	  
	        	  $('#mainTabs a').click(function (e) {
	        	        e.preventDefault();
	        	        $(this).tab('show');
	        	    });

	        	    // store the currently selected tab in the hash value
	        	    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	        	        var id = $(e.target).attr("href").substr(1);
	        	        window.location.hash = id;
	        	    });

	        	    // on load of the page: switch to the currently selected tab
	        	    var hash = window.location.hash;
	        	    $('#mainTabs a[href="' + hash + '"]').tab('show');
	        	    
	        		$(function() {
	        			$('[data-toggle="tooltip"]').tooltip({
	        			    'placement': 'top'
	        			});
	        		});

	        		$(function() {
	        			$('[data-toggle="popover"]').popover({
	        			    trigger: 'hover',
	        			        'placement': 'top'
	        			});
	        		});
	        		$(function() {
	        			$('.dropdown-toggle').dropdown();
	        		});
	           },  
   			error: function(e){  
   				alertError();
       	 }
	     });
	 }	

	 
// 	function loadChart()
// 	{
	
// 		showBarChart();
// 		alertInfo();
// 		timeoutChart = setTimeout("loadChart()",3600000);
			
// 	}
	
	function loadTarefasSistemas()
	{
		
		showAtualizaTab("18317794","sistemas");
		alertInfo();
		timeoutSistemas = setTimeout("loadTarefasSistemas()",3600000);
		
	}

	function loadTarefasInfra()
	{
		
		showAtualizaTab("18317811","infra");
		alertInfo();
		timeoutInfra = setTimeout("loadTarefasInfra()",3600000);
		
	}
	
	function loadTarefasNexti()
	{
		
		showAtualizaTab("639014270","nexti");
		alertInfo();
		timeoutNexti = setTimeout("loadTarefasNexti()",3600000);
		
	}
	
	function loadTarefasConsultoria()
	{
		
		showAtualizaTab("647233104","consultoria");
		alertInfo();
		timeoutConsultoria = setTimeout("loadTarefasConsultoria()",3600000);
		
	}
	
	function getFluxo()
	{
		$('#myCarousel').carousel({
		    interval: 2000
		});
	}

	
	
	function reloadPage() 
	{
		 loadTarefasSistemas();
		 loadTarefasInfra();
		 loadTarefasNexti();
		 loadTarefasConsultoria();
	}
	
	
	
   
	</script>
	

<cw:main>
<cw:header title="Controle de  tarefas" />
<cw:body id="area_scroll">	
<div id="snoAlertBox" class="alert alert-success alert-dismissible" data-alert="alert"><span  style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>  Sucesso ao executar a altera��o. </span></div>
	<div id="snoAlertBoxError" class="alert alert-danger alert-dismissible" data-alert="alert"><span  style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>  Erro ao executar a altera��o. </span></div>
		<div id="snoAlertBoxInfo" class="alert alert-info alert-dismissible" data-alert="alert"><span  style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>  Informa��es atualizadas com sucesso. </span></div>
			<div class="container" disabled style='width:100%;height:100%;overflow: auto;display: table;' >
		    	<!-- Nav tabs -->
		    	<ul class="nav nav-tabs" id="mainTabs" role="tablist">
		     		
		      		<li id="tabSistemas">
		          		<a  class="active" href="#sistemas" role="tab" data-toggle="tab">
		            	    <p class="text-center"> <span  style="font-family: verdana; font-size: 10px;">  Sistemas </span></p>
		         		</a>
		      		</li>
		      		<li id="tabInfra">
		      			<a id="linkInfra" href="#infra" role="tab" data-toggle="tab">
		       				<p class="text-center"><span  style="font-family: verdana; font-size: 10px;"> Infraestrutura </span></p>
		         		</a>
		     		 </li>
		     		 <li id="tabNexti">
		     			<a id="linkNexti" href="#nexti" role="tab" data-toggle="tab">
		       				<p class="text-center"><span  style="font-family: verdana; font-size: 10px;"> Nexti </span></p>
		         		</a>
		     		 </li>
		     		 
		     		 <li id="tabConsultoria">
		     			<a id="linkConsultoria" href="#consultoria" role="tab" data-toggle="tab">
		       				<p class="text-center"><span  style="font-family: verdana; font-size: 10px;"> Consultoria </span></p>
		         		</a>
		     		 </li>
		     		 
		     		 <!--<li>
          				  <a  class="active" href="#settings" role="tab" data-toggle="tab">
             		   		<p class="text-center"> <span  style="font-family: verdana; font-size: 10px;">  Geral </span></p>
         			 	</a>
   		   			</li> -->
				</ul>
		    <!-- Tab panes -->
		    <div class="tab-content" id="tab" style="width: 100%; font-family: verdana; font-size: 10px;">
		    
		    <!--  <div class="tab-pane fade active in"  style="width: auto; font-family: verdana; font-size: 12px;" id="settings"> -->
         		
         	<!-- <table class="table" id="containerx" style="width: 100%; font-family: verdana;padding: 10px 10px;" disabled="disabled">
					<tr>
						<td>
         		 			<div id="containerChart" style="float: left; width: 100%;height: 100%; margin-left: 2px; max-width: 100%;min-width:800px;min-height:600px; overflow-y:auto;padding: 2px;">
								<div class="progress">
								    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
								      Aguarde...
								    </div>
								</div>
							</div>
          				</td>
					</tr>	
				</table>  -->
      		<!-- </div> -->
			    <div class="tab-pane  fade active in" style="width: 100%; font-family: verdana; font-size: 12px;"  id="sistemas">
			 	</div>
			 	
			 	<div class="tab-pane  fade" style="width: 100%; font-family: verdana; font-size: 12px;"  id="infra">
			 	</div>
			 	
			 	<div class="tab-pane  fade" style="width: 100%; font-family: verdana; font-size: 12px;"  id="nexti">
			 	</div>
			 	
			 	<div class="tab-pane  fade" style="width: 100%; font-family: verdana; font-size: 12px;"  id="consultoria">
			 	</div>
			</div>
		</div>
</cw:body>
</cw:main>
</portal:head>
</html>