<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="font-family: sans-serif;">
	<div class=divMensagem>
		<% 
		String mensagem = null;		
		mensagem = (String) request.getParameter("mensagem");
		String urlServlet = PortalUtil.getBaseURL()+"";
		if(mensagem != null){ %>	
			<p>${param.mensagem}</p>
		<%}%>
	</div>
	<fieldset id="principal"
		style="position: fixed; 
			   width: 25%; 
			   height: 18%; 
			   top: 35%; 
			   left: 35%; 
			   border: 1px solid black; 
    		   padding: 1em;
               border: 1px solid #CCC;
               border-radius: 0.3em;
               top: 35%;">
		<legend>Dados do usu�rio</legend>
		<form
			action="http://localhost:9090/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.DerrubarUsuarioSapiensServlet"
			style="padding: 5px; margin-top: 5px; margin-left: 20px">		
			<div style="padding: 8px">
				<label>Nome:</label>
				<input type="text"
				value="<%=PortalUtil.getCurrentUser().getFullName()%>" disabled
				size="18" ; style="text-align: center;font-family: sans-serif;">
			</div>
			
			<div style="padding: 8px">
				<label style="padding-right: 3px">Login:</label>
				<input
				type="text" value="<%=PortalUtil.getCurrentUser().getCode()%>"
				disabled size="18" ; style="text-align: center;font-family: sans-serif;">
			</div>
			
			<div style="padding: 12px; margin-left: 80px;">
				<button type="submit" style="text-align: center;font-family: sans-serif;">Derrubar</button>
			</div>
			
		</form>
	</fieldset>
</body>
</html>

