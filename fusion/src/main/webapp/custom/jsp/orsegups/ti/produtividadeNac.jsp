<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/produtividadeNac.css">
<link rel="stylesheet" href="css/bootstrap-tokenfield.min.css">
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
<script src="js/produtividadeNac.js"></script>


<script src="js/bootstrap-tokenfield.min.js"></script>
<script src="js/jquery-ui.min.js"></script>

<script src="js/jquery.growl.js" type="text/javascript"></script>
<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />

<title>Produtividade NAC</title>
</head>
<body>

<br><br>
<div class="container-fluid">

 <div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
           <div class="panel-heading">
           		<h3>Produtividade NAC</h3>                          
           </div>
                  
               <div class="panel-body">
					<form class="form-horizontal" id="formPesquisa">
						<fieldset>							
							<!-- Search input-->
							<div class="form-group col-md-12 col-sm-12">
								<label class="col-md-1 control-label" for="nome">Usu�rio *</label>
								<div class="col-md-5">
									<input id="usuario" name="nome" type="text"
										placeholder="Usu�rios" class="form-control input-md">
										
								</div>
								<label class="col-md-1 control-label" for="sexo">Fluxo</label>
								<div class="col-md-5">
									<select id="cbFluxo"
										class="form-control campoformulario">
										<option>G001</option>
										<option>C025</option>
									</select>
								</div>
							</div>
	
							<div class="form-group  col-md-12 col-sm-12">
								<label class="col-md-1 control-label">Data Inicio *</label>
								<div class="col-md-5">
									<input id="dataInicio" name="dataInicio" type="date"
										placeholder="Data Inicio" class="form-control input-md">
	
								</div>
							    <label class="col-md-1 control-label">Data Fim *</label>
								<div class="col-md-5">
									<input id="dataFim" name="dataFim" type="date"
										placeholder="Data Fim" class="form-control input-md">
	
								</div>	
							</div>
							
							<!-- Button -->
							<div class="form-group">
								<div class="col-md-12">
									<input type="button" onclick="enviar()" id="pesquisar" name="pesquisar" data-toggle="modal" data-target="#processing-modal"
									class="btn btn-primary pull-left btn-pesquisar" value="Pesquisar">
								</div>
							</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
		<!-- Table -->
		<div class="row">
			<div class="col-md-12">		
				<div class="panel panel-default">
					<div class="panel-heading">
                       Tarefas 
                    </div>
                    <div class="panel-body">
						<div class="table-responsive">
							<table id="tabela-resultado" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Usu�rio</th>
										<th>Processo</th>
										<th>Atividade</th>
										<th>Tarefa</th>
								   </tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="modal modal-static fade" id="processing-modal" name="processing-modal" role="dialog" aria-hidden="true" >
  <div class="modal-dialog"  >
      <div class="modal-content"   >
          <div class="modal-body"  >
              <div class="text-center"   >	               
			<img src="img/loading.gif" style="height: 80px;" class="icon" />
            			<h4>Processando... </h4>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>