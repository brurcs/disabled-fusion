<%@page import="java.util.TreeMap"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.util.SortedMap"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.MapaSecurityAcess"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.TecnicoConta"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>




<%
	
	if (PortalUtil.getCurrentUser() == null) {
		out.print("Erro - Acesso Negado");
		return;
	}
	final NeoUser currentUser = PortalUtil.getCurrentUser();
	final boolean isAdm = (currentUser!=null&&currentUser.isAdm())?true:false;
	final String none = "&nbsp;";
	
	String user = "";


	
%>

<portal:head title="Produtividade AA/SUP/FIS">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap.min.css">
	
	<!-- Optional theme -->
	<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/css/bootstrap-theme.min.css">
	
	<!-- Latest compiled and minified JavaScript -->
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>	
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/bootstrap/js/bootstrap.js"></script>
	
	<script type="text/javascript" src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	<style type="text/css">
		body,
		.modal-open .page-container,
		.modal-open .page-container .navbar-fixed-top,
		.modal-open .modal-container {
			overflow-y: scroll;
		}
		
		@media (max-width: 979px) {
			.modal-open .page-container .navbar-fixed-top{
				overflow-y: visible;
			}
		}
		.custab{
	    border: 1px solid #ccc;
	    padding: 1px;
	    margin:  0;
	    box-shadow: 3px 3px 2px #ccc;
	    transition: 0.5s;
	    }
		.custab:hover{
	    box-shadow: 3px 3px 0px transparent;
	    transition: 0.5s;
	    }
		
		.scrollable-menu {
	    height: auto;
	    max-height: 180px;
	    overflow-x: hidden;
		}
		.scrollable-menu::-webkit-scrollbar {
	    -webkit-appearance: none;
	    width: 4px;        
	    }    
	    .scrollable-menu::-webkit-scrollbar-thumb {
	    border-radius: 3px;
	    background-color: lightgray;
	    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);        
	    }
        
        #snoAlertBox{
			position:absolute;
		    z-index:1400;
		    top:2%;
		    right:4%;
		    margin:0px auto;
			text-align:center;
		    display:none;
		}
		#snoAlertBoxError{
			position:absolute;
		    z-index:1400;
		    top:2%;
		    right:4%;
		    margin:0px auto;
			text-align:center;
		    display:none;
		}
		#snoAlertBoxInfo{
			position:absolute;
		    z-index:1400;
		    top:2%;
		    right:4%;
		    margin:0px auto;
			text-align:center;
		    display:none;
		}
		.table {
		  border: 1px solid #dddddd;
		}
		
		
		
		.map {
        min-width: 300px;
        min-height: 300px;
        width: 100%;
        height: 100%;
	    }
	
	    .header {
	        background-color: #F5F5F5;
	        color: #36A0FF;
	        height: 70px;
	        font-size: 27px;
	        padding: 10px;
	    }
	</style>
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			

			$(document).ready(function() {
				var usuario = '<%=currentUser.getCode()%>';
				consUsuarioGrupo(usuario);
			});
			
			$(function() {
				$('[data-toggle="tooltip"]').tooltip({
					  trigger: 'hover',
					'placement': 'top'
				});
			});
	
			$(function() {
				$('[data-toggle="popover"]').popover({
				    trigger: 'hover',
				        'placement': 'top'
				});
			});
	
			$(function() {
				$('.dropdown-toggle').dropdown();
			});
			
			$(function() {
			$(".dropdown-menu li a").click(function(){
				  var selText = $(this).text();
				  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
				});

// 				$("#btnSearch").click(function(){
// 					alert($('.btn-select1').text()+''+$('.btn-select2').text()+''+$('.btn-select3').text());
// 				});
			});
			
			function abrePopUpDetalhes(){

				$(function() {
					$('#myModal').on('hidden', function(){
					    $(this).data('modal', null);
					});
				});
				
				$('#myModal').modal();
				$('#current_password').text('');
				$('#new_password').text('');
				$('#confirm_password').text('');

			}
			
			function abrePopUpPermissoes(usuario){
				
				$(function() {
					$('#myModalPermissao').on('hidden', function(){
					    $(this).data('modal', null);
					});
				});
				
				$('#myModalPermissao').modal();

			}
			
			function toggleChevron(e) {
			    $(e.target)
			        .prev('.panel-heading')
			        .find("i.indicator")
			        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
			}
			
			$(function() {
				$('#accordion').on('hidden.bs.collapse', toggleChevron);
			});
			
			$(function() {
				$('#accordion').on('shown.bs.collapse', toggleChevron);
			});
			
			function alteraSenha(){
				var senha = $('#current_password').val();
				var senhaAux = $('#new_password').val();
				var senhaCon = $('#confirm_password').val();
		
			       $.ajax({
			           type:"post",
			           url:"custom/jsp/orsegups/ti/cadeadoViaSNEP.jsp",
			           data:'action=alterarSenha&senhaAnt='+senha+'&senhaNov='+senhaAux+'&senhaCon='+senhaCon,
			           dataType:'text',
			           success: function(result) {
			        	   alert(result);
			        	   alertSucess();
		  
			           },  
		    			error: function(e){  
		    				alert(e);
		    				alertError();
		        	 }
			       });
			}	
			
			function gerarSenha(usuario){
			       $.ajax({
			           type:"post",
			           url:"custom/jsp/orsegups/ti/cadeadoViaSNEP.jsp",
			           data:'action=gerarSenha&usuario='+usuario,
			           dataType:'text',
			           success: function(result) {
			        	   alert(result);
			        	   alertSucess();
						   consUsuarioGrupo(usuario);
		  
			           },  
		    			error: function(e){  
		    				alert(e);
		    				alertError();
		        	 }
			       });
			}
			
			function recuperarSenha(usuario){
			       $.ajax({
			           type:"post",
			           url:"custom/jsp/orsegups/ti/cadeadoViaSNEP.jsp",
			           data:'action=recuperarSenha&usuario='+usuario,
			           dataType:'text',
			           success: function(result) {
			        	   alert(result);
			        	   alertSucess();
		  
			           },  
		    			error: function(e){  
		    				alert(e);
		    				alertError();
		        	 }
			       });
			}
			
			function consUsuarioGrupo(usuario){
			       $.ajax({
			           type:"post",
			           url:"custom/jsp/orsegups/ti/cadeadoViaSNEP.jsp",
			           data:'action=consUsuarioGrupo&usuario='+usuario,
			           dataType:'text',
			           success: function(result) {
			        	   $("#dvPrincipal").html(result);
			        	   $(function() {
			        			$('[data-toggle="tooltip"]').tooltip({
			        			    'placement': 'top'
			        			});
			        		});

			        		$(function() {
			        			$('[data-toggle="popover"]').popover({
			        			    trigger: 'hover',
			        			        'placement': 'top'
			        			});
			        		});
			        		
			        		$(function() {
			        			$('.dropdown-toggle').dropdown();
			        		});
			        		
			        		$(function() {
			        			$(".dropdown-menu li a").click(function(){
			        				  var selText = $(this).text();
			        				  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
			        				});

//			        				$("#btnSearch").click(function(){
//			        					alert($('.btn-select1').text()+''+$('.btn-select2').text()+''+$('.btn-select3').text());
//			        				});
			        		});
			    			
			    			$(function() {
			    				$('#accordion').on('hidden.bs.collapse', toggleChevron);
			    			});
			    			
			    			$(function() {
			    				$('#accordion').on('shown.bs.collapse', toggleChevron);
			    			});
			        	   alertSucess();
		  
			           },  
		    			error: function(e){  
		    				alert(e);
		    				alertError();
		        	 }
			       });
			}
			
			function closeSnoAlertBox()
			{
				window.setTimeout(function () {
				$("#snoAlertBox").fadeOut(600);
				}, 3000);
			}  
			
			function alertSucess(){
				
				$("#snoAlertBox").fadeIn();
		        closeSnoAlertBox();
		        $("#myModal").modal('hide');
		      
			}
			
			function closeSnoAlertBoxError()
			{
				window.setTimeout(function () {
				$("#snoAlertBoxError").fadeOut(600);
				}, 3000);
				
			}  
			
			function alertError(){
				
				$("#snoAlertBoxError").fadeIn();
				closeSnoAlertBoxError();
				$("#myModal").modal('hide');
				
			}
			
			function closeSnoAlertBoxInfo()
			{
				window.setTimeout(function () {
				$("#snoAlertBoxInfo").fadeOut(600);
				}, 9000);
				
			}  
			
			function alertInfo(){
				
				$("#snoAlertBoxInfo").fadeIn();
				closeSnoAlertBoxInfo();
				$("#myModal").modal('hide');
			
			}
			
			function reloadPage() 
			{
			    location.reload();
			}
				
		</script>
	<cw:main>
		<cw:header title="Cadeado Permiss�es" />
		<cw:body id="area_scroll">
		<div id="snoAlertBox" class="alert alert-success alert-dismissible" data-alert="alert"><span  style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong>  <label id="lbSucesso">Sucesso ao executar a altera��o.</label> </span></div>
		<div id="snoAlertBoxError" class="alert alert-danger alert-dismissible" data-alert="alert"><span  style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong> <label id="lbErro"> Erro ao executar a altera��o. </label></span></div>
		<div id="snoAlertBoxInfo" class="alert alert-info alert-dismissible" data-alert="alert"><span  style="font-family: verdana; font-size: 10px;"> <strong>Aten��o!</strong> <label id="lbInfo"> Informa��es atualizadas com sucesso.</label> </span></div>
		<div class="container" style='width:100%;height:100%;overflow: auto;display: table;' id="dvPrincipal">
		
		</div> 
		</cw:body>
	</cw:main>
</portal:head>