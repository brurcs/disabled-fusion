
function enviar(){
	
	
		 var usuario =  document.getElementById("usuario").value;
		 var tarefaSimples = document.getElementById("tarefaSimples").value;
		 var tarefasFap = document.getElementById("tarefasFap").value;
		 document.getElementById("texto").value="";
		
		 $.ajax({
	           type:"post",
	           // url:"https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.CancelarFapServlet",
	           url:"http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.CancelarFapServlet",
	           data:'action=cancelarFap&tarefasFap='+tarefasFap+'&tarefaSimples='+tarefaSimples+'&usuario='+usuario,
	           dataType:'text',
	           success: function(result) {
	               
	        
	        	   $('#processing-modal').modal('hide');
	        	 
	        	   if(result == "Favor Preencher todos os campos!"){
	        		   
	        		   $.growl.error({ title: "", message: result});
	        	   	}
	        	   	if(result == "Agrupadora"){
	        		   
	        	   		mensagemAgrupadora();
	        		   
	        	   	}
	        		if(result == "Cancelamento Processado com Sucesso!"){
		        		   
		        		   $.growl({ title: "", message: result});
		        		   respostaTarefa();
		        		   limparCampos();
		        	}
	        		
	        		if(result == "Nenhuma tarefa encontrada para cancelamento!"){
	        			
	        			$.growl.error({ title: "", message: result});
	        		}
	        	 
	           },  
	           
	           
      			error: function(e){
      				 $('#processing-modal').modal('hide');
      				$.growl.error({ title: "", message: "Erro"});
    				console.log(e);
          	 }
	       });	
		 
		
	 
}

function mensagemAgrupadora(){
	
	
	
	 var tarefasFap = document.getElementById("tarefasFap").value;
	
	
	 $.ajax({
          type:"post",
          url:"http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.CancelarFapServlet",
          data:'action=mensagemAgrupadora&tarefasFap='+tarefasFap,
          dataType:'text',
          success: function(result) {
              
        	  $('#processing-modal').modal('hide');
        	  $.growl.error({ title: "", message: result});
       	   },  
          
          
 			error: function(e){  
 				$.growl.error({ title: "", message: "Erro"});
 				 console.log(e);
     	 }
      });	
	 
	

}

function respostaTarefa(){
	
	
	 var usuario =  document.getElementById("usuario").value;
	 var tarefasFap = document.getElementById("tarefasFap").value;
	
	 $.ajax({
          type:"post",
          url:"http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.CancelarFapServlet",
          data:'action=respostaTarefa&tarefasFap='+tarefasFap+'&usuario='+usuario,
          dataType:'text',
          success: function(result) {
             
        	  document.getElementById("texto").value=result;
          },  
          
          
 			error: function(e){
 				
				console.log(e);
     	 }
      });	
	 
	

}

$(document).ready(function(e){
	 
	var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};

		
		var states = new Array();

		$.ajax({
		    type:"post",
		    url:"http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.CancelarFapServlet",
		    data:'action=getListaUsuarios',
		    dataType:"json",
		    success: function(msg) {
		    	states = msg;	
		  
				$('#usuario').typeahead({
					  hint: true,
					  highlight: true,
					  minLength: 1
					},
					{
					  name: 'states',
					  source: substringMatcher(states)
				});
			
				
				 	
		    },  
				error: function(e){  
					 console.log(e);
			 }
		});

});

function limparCampos(){
	
document.getElementById("usuario").value="";	
document.getElementById("tarefaSimples").value="";	
document.getElementById("tarefasFap").value="";
	
}
