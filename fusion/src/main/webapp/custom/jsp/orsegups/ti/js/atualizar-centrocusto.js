
function enviar(){
	
	
		 var codTar =  document.getElementById("codTar").value;
		 var codCcu =  document.getElementById("codCcu").value;
		  $.ajax({
	           type:"GET",
	           url:"https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils",
	           data:'action=atualizaCentroDeCusto&codTar='+codTar+'&codCcu='+codCcu,
	           dataType:'json',
	           success: function(result) {
	        	   $('#processing-modal').modal('hide');
	        	   if(result.msgRetorno === "Registros atualizados com Sucess!"){
	        		   listar();
	        		   $.growl({ title: "", message: result.msgRetorno});
	        	}else{
	        		   $.growl.error({ title: "", message: result.msgRetorno});   
	        	   }
	        	   
	           },  
      			error: function(e){  
		    		$('#warning-alert').show();
	    			setTimeout(function(){
	    				$('#warning-alert').hide();
	    			}, 5000);
      				 console.log(e);
          	 }
	       });	
}

function listar(){
	 var codTar =  document.getElementById("codTar").value;
	 jQuery.ajax({
  		method: 'GET',
  		url:"https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.servlets.ContratoServletUtils",
      	dataType: 'json',	
      	data:'action=listar&codTar='+codTar,
      	success: function(result){
      		$('#processing-modal').modal('hide');
      		var cont = [];
      		var contador = 0;
      		for(i in result){
      			$('#listaEform').dataTable().fnClearTable();
      			cont[contador] = result[i];
      			contador++;
      		}
      		
      		$('#listaEform').dataTable().fnAddData([
      		                                      cont[0],cont[1],cont[2],cont[3],cont[4]
      		                                      ]);
      	}
      });
}

  
$(document).ready(function(e){
	$('#listaEform').dataTable();
	var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};
});
