
function enviar(){
	
	
		 var usuario =  document.getElementById("usuario").value;
		 var fluxo = document.getElementById("cbFluxo").value;
		 var dataInicio = document.getElementById("dataInicio").value;
		 var dataFim = document.getElementById("dataFim").value;
		
		 $.ajax({
	           type:"post",
	           // url:"https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.CancelarFapServlet",
	           url:"http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.ProdutividadeNacServlet",
	           data:'action=listaUsuarios&usuario='+usuario+'&fluxo='+fluxo+'&dataInicio='+dataInicio+'&dataFim='+dataFim,
	           dataType:'json',
	           success: function(result) {
	               
	        
	        	   $('#processing-modal').modal('hide');
	        	 
	        	   if(result == ""){
	        		   
	        		   $.growl.error({ title: "", message: "nenhum registro retornado"});
	        	   	}
	        	   else{
	            		 
	            		 $.growl({ title: "", message: "tarefas retornadas com Sucesso!"});
	            		createTable(result);
	            	   }
	        		
	        			        	 
	           },  
	           
	           
      			error: function(e){
      				$('#processing-modal').modal('hide');
      				$.growl.error({ title: "", message: "Erro"});
    				console.log(e);
          	 }
	       });	
	
	 
}


function createTable(result){

     $("#tabela-resultado tbody").empty();
     
  	for(i in result){
  		
  		$('#tabela-resultado').append("<tr><td>" + result[i].nomeCompleto + 
					"</td><td>" +result[i].processo + "</td>"+
					"</td><td>" + result[i].atividade + "</td>"+
					"</td><td>" + result[i].codigoTarefa + "</td></tr>");
  	}
 
    $('#tabela-resultado').DataTable( {
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    } );
	
    tabelaIniciada = true;
    pastas = null;
    
    $('#processing-modal').modal('hide');

}


$(document).ready(function() {
	
	
	$.ajax({
	    type:"post",
	    url:"http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.ProdutividadeNacServlet",
	    data:'action=getListaUsuariosFusion',
	    dataType:"json",
	    success: function(msg) {
	    	
	    	$('#usuario').tokenfield({
	  		  autocomplete: {
	  		    source: msg,
	  		    delay: 100
	  		  },
	  		  showAutocompleteOnFocus: false
	  		})
		
			
			 	
	    },  
			error: function(e){  
				 console.log(e);
		 }
	});
	
	
});