<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>

<%
	String camera = ((String)request.getParameter("camera")) == null ? "FFFF01" : (String)request.getParameter("camera");

	if (PortalUtil.getCurrentUser() == null) {
		out.print("Erro - Acesso Negado");
		return;
	}
%>

<portlet:defineObjects />
<form:defineObjects />
<head>
	<meta charset="UTF-8">
</head>
<portal:head title="<%=camera%>">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript">

		var linkCamera;
		var camera = "<%= camera %>";
		
		function buscaCamera(camera)
		{
			
			switch (camera) {
			case "Cachoeira":
				linkCamera = "http://177.0.209.205";
				break;
			case "Coqueiros":
				linkCamera = "http://10.0.55.4";
				break;
			case "Brusque":
				linkCamera = "http://10.0.23.27:8880";
				break;
			case "Blumenau":
				linkCamera = "http://10.0.34.152:8080";
				break;
			case "Chapeco":
				linkCamera = "http://10.0.30.101:8080";
				break;
			case "Lages":
				linkCamera = "http://10.0.26.70:8080";
				break;
			case "Rio do Sul":
				linkCamera = "http://10.0.31.252:8080";
				break;
			case "Curitiba 02":
				linkCamera = "http://10.0.33.13:80";
				break;
			case "Tubarao":
				linkCamera = "http://10.0.27.200:8080";
				break;
			case "Criciuma":
				linkCamera = "http://10.0.29.200";
				break;
			case "Itajai":
				linkCamera = "http://10.0.22.83:8080";
				break;
			case "Jaragua":
				linkCamera = "http://10.0.32.180";
				break;
			case "Curitiba 01":
				linkCamera = "http://10.0.33.12:9090";
				break;
			case "Joinville":
				linkCamera = "http://10.0.25.153:8080";
				break;
			case "Casvig":
				linkCamera = "http://192.168.21.250";
				break;
			}
			document.getElementById('camImg').src = linkCamera;
			
		}
		
		$(document).ready(function() {
			
			buscaCamera(camera);
			
        });
	

		
</script>



	 	<iframe id="camImg" name="camImg" width="100%"  height="100%"/>
	

</portal:head>
