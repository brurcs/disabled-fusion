
var lista;
var lista2;

function listaUsuarios(sel){
	var usuarios = [], i, j, cur;
	for (i = 0; i < sel.options.length; i++) {
		cur = sel.options[i];
		if (cur.selected) {
			usuarios.push(cur.value);
		}
	}
	if (usuarios.length) {
		usuarios = usuarios.join("&" + sel.name + "=");
	} else {
		usuarios = null;
	}
	
	lista = usuarios;
	
}

function listaSolicitantes(sel){
	var solicitantes = [], i, j, cur;
	for (i = 0; i < sel.options.length; i++) {
		cur = sel.options[i];
		if (cur.selected) {
			solicitantes.push(cur.value);
		}
	}
	if (solicitantes.length) {
		solicitantes = solicitantes.join("&" + sel.name + "=");
	} else {
		solicitantes = null;
	}
	
	lista2 = solicitantes;
	
}

//Função para enviar requisições HTTP
function initXHR(){
	// criando xhr de acordo com o browser
	if(window.XMLHttpRequest){ // Nao microsoft
		xhr = new XMLHttpRequest();
	}else if (window.ActiveXObject){
		xhr = new ActiveXObject("Microsoft.XMLHttp");
	}
}
//Este codigo e o initXHR sempre serão os mesmo. Não há necessidade de criar outro
function sendRequest(url, handler){
	initXHR();
		
	// setando método de resposta no xhr
	xhr.onreadystatechange = handler;
	// abrindo requisição
	xhr.open("GET", url, true);
	// executando xhr
	xhr.send(null);
}

function pesquisaTarefa(){
	
	var dataInicio = document.getElementById("dataInicio").value;
	var dataFim =    document.getElementById("dataFim").value;
	var solicitante = lista2;
	var usuario = lista;
	
	var url = "https://intranet.orsegups.com.br/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.RelatorioServlet?dataInicio="+dataInicio+"&dataFim="+dataFim+"&usuario="+usuario+"&solicitante="+solicitante;
		
	sendRequest(url, ajaxBuscaTarefa);
}

function ajaxBuscaTarefa(){
	if(xhr.readyState == 4) { // requisicao concluida
		// status da resposta 200 = OK, (404 página não encontrada, 500 erro no servidor)
		if(xhr.status == 200){
			// setando valores de resposta do servlet na tabela inserindo as TD digitada no Servlet
			$('#processing-modal').modal('hide');

			$('#success-alert').show();
			setTimeout(function(){
				$('#success-alert').hide();
			}, 3500);
			//Selecionando o ID da linha onde irá começar a exibir o resultado
			var valor = window.document.getElementById("tabela-resultado");
			//salvo em uma variável o retorno do Servlet
			var retorno = xhr.responseText;
			//Exibo no HTML o que foi retornado do Servlet
			valor.innerHTML = retorno;
		}else{
			$('#warning-alert').show();
			setTimeout(function(){
				$('#warning-alert').hide();
			}, 3500);
			console.log();
		}
	}
}
