
function enviar(){
	
	
		 var usuarioDestino =  document.getElementById("typeahead").value;
		 var usuarioOrigem =  document.getElementById("typeahead2").value;
		 $.ajax({
	           type:"post",
	           url:"http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.ClonarPapelServlet",
	           data:'action=clonarPapel&cmb_usuario_origem='+usuarioOrigem+'&cmb_usuario_destino='+usuarioDestino,
	           dataType:'text',
	           success: function(result) {
	          document.getElementById("typeahead").value="";
	          document.getElementById("typeahead2").value="";	   
	        	   $('#processing-modal').modal('hide');
	        	   if(result == "erro"){
	        		   $('#warning-alert').show();
		    			setTimeout(function(){
		    				$('#warning-alert').hide();
		    			}, 5000); 
		    			
		    			
	        	   }
	        	   else{
	        		  
			    		$('#success-alert').show();
		    			setTimeout(function(){
		    				$('#success-alert').hide();
		    			}, 5000);
	        	   }  
	           },  
      			error: function(e){  
		    		$('#warning-alert').show();
	    			setTimeout(function(){
	    				$('#warning-alert').hide();
	    			}, 5000);
      				 console.log(e);
          	 }
	       });	
		 
		
	 
}

  
$(document).ready(function(e){
	 
	var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};

		
		var states = new Array();

		$.ajax({
		    type:"post",
		    url:"http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.ti.ClonarPapelServlet",
		    data:'action=getListaUsuarios',
		    dataType:"json",
		    success: function(msg) {
		    	states = msg;	
		  
				$('#typeahead').typeahead({
					  hint: true,
					  highlight: true,
					  minLength: 1
					},
					{
					  name: 'states',
					  source: substringMatcher(states)
				});
				
				$('#typeahead2').typeahead({
					  hint: true,
					  highlight: true,
					  minLength: 1
					},
					{
					  name: 'states',
					  source: substringMatcher(states)
				});
				
				 	
		    },  
				error: function(e){  
					 console.log(e);
			 }
		});

});
