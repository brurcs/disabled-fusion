<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/clonar-papeis.css">
<script src="js/clonar-papeis.js"></script>

<title>Clonar Papéis</title>
</head>
<body>
<div class="alert alert-warning collapse" id="warning-alert" role="alert">
 		<strong>Erro!</strong> Usuário de Origem ou Destino Não encontrados.
</div>
	
<div class="alert alert-success collapse" id="success-alert" role="alert">
  		<strong>Sucesso!</strong> Processo realizado com Sucesso.
</div>

<div class="container">    
	<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
    	<div class="panel panel-info" >
        	<div class="panel-heading">
                        <div class="panel-title">Clonar Papéis</div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
	                
	            <form id="loginform" method="post" class="form-horizontal" role="form">
	            
	            
	            
	            	<div style="margin-bottom: 25px" class="input-group">
	                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
	                    <input id="typeahead2" name="cmb_usuario_origem" class="form-control" autocomplete="off" type="text" placeholder="Usuário Origem">                                        
	                   </div> 
	                        
	                <div style="margin-bottom: 25px" class="input-group">
	                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
	                    <input id="typeahead" name="cmb_usuario_destino" class="form-control" autocomplete="off" type="text" placeholder="Usuário Destino">                                        
	                </div>
	                 
	                 
	                <div style="margin-bottom: 25px" class="input-group">
	                     <span class="input-group-addon"><i class="glyphicon glyphicon-tasks"></i></span>
	                    <input id="teste" class="form-control" autocomplete="off" type="text" placeholder="Clique no botão abaixo para clonar os papéis" disabled>                                        
	                </div>
	                      
	       
	
	                    <div style="margin-top:10px" class="form-group" align="center">
	                        <!-- Button -->
	
	                        <div class="col-sm-12 controls">
	                          <a id="btn-login" onclick="enviar()" class="btn btn-success" data-toggle="modal" data-target="#processing-modal">Clonar  </a>                     
	                        </div>
	                    </div>
	
	
	                    
                </form>     

        	</div>                     
		</div>  
	</div>
</div>
    
<div class="modal modal-static fade" id="processing-modal" name="processing-modal" role="dialog" aria-hidden="true" >
  <div class="modal-dialog"  >
      <div class="modal-content"   >
          <div class="modal-body"  >
              <div class="text-center"   >	               
			<img src="img/loading.gif" style="height: 80px;" class="icon" />
            			<h4>Processando... </h4>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>