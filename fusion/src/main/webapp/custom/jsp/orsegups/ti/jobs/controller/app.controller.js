app.controller('appontroller', ['$scope','DadosPainel','$http','$timeout','$interval', function($scope, DadosPainel, $http, $timeout, $interval){
     
	var inicio = true;
	var tabelaIniciada = false;
	$scope.preencheu = false;
	$scope.preencheuErro = false;
	$scope.atualizar = false;
	$scope.liberarAlert = false;
	
    $scope.dados;
    
    $scope.tarefas;    
    $scope.erros = [];
    $scope.errosDisplay = [];
    
    $scope.painelAlerta = "painel-visivel";
    $scope.msgAlerta = "alert alert-success fade in msg-alert hide";
    
    DadosPainel.getDados.then(function(records) {
		$scope.dados = records;
		$scope.preencheu = true;
	});
    
	$scope.tituloTarefa = function(neoId, sistema) {
		console.log(neoId);
		var sis = 0;
		if (sistema == "Vetorh"){
			sis = 1;
		}else if (erro.sistema == "Sapiens"){
			sis = 2;
		}
		$http.get('http://dsoo03:8080/fusion/services/jobs/buscarTarefasJob/'+neoId+'/'+sis).success(function(data, status) {
			if (data.length == 0){
				data = ["Não há tarefas cadastradas!"];
			}
			$scope.tarefas = data;
			console.log(data.length);
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	
	}
	
	$scope.tituloTarefaAgendador = function() {

		$http.get('http://dsoo03:8080/fusion/services/jobs/buscarTarefasAgendador/').success(function(data, status) {
			if (data.length == 0){
				data = ["Não há tarefas cadastradas!"];
			}
			$scope.tarefas = data;
			console.log(data.length);
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	
	}
	
	$scope.fecharErro = function(erro, idx){
		var idSistema = 0;
		if (erro.sistema == "Vetorh"){
			idSistema = 1;			
		}else if (erro.sistema == "Sapiens"){
			idSistema = 2;
		}
		$http.get('http://dsoo03:8080/fusion/services/jobs/gravarVerificacaoErro/'+erro.idJob+"/"+idSistema).success(function(data, status) {
			$scope.erros.splice(idx, 1);
			$scope.errosDisplay.splice(idx, 1);
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	}
	
	$scope.atualizarErro = function(){
		if ($scope.atualizar){
    		$scope.atualizar = false;
    	}else{
    		$scope.atualizar = true;
    	}
		$scope.liberarAlert = true;
		tabelaIniciada = true;
	}
	
    $scope.$watch('preencheu', function() {
        if ($scope.preencheu){
        	$timeout(function(){
        		$('#tabela-resultado').dataTable( {
            		"language": {
            			"sEmptyTable": "Nenhum registro encontrado",
            			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
            			"sInfoPostFix": "",
            			"sInfoThousands": ".",
            			"sLengthMenu": "_MENU_ resultados por página",
            			"sLoadingRecords": "Carregando...",
            			"sProcessing": "Processando...",
            			"sZeroRecords": "Nenhum registro encontrado",
            			"sSearch": "Pesquisar",
            			"oPaginate": {
            				"sNext": "Próximo",
            				"sPrevious": "Anterior",
            				"sFirst": "Primeiro",
            				"sLast": "Último"
            			},
            			"oAria": {
            				"sSortAscending": ": Ordenar colunas de forma ascendente",
            				"sSortDescending": ": Ordenar colunas de forma descendente"
            			},
            			"searchPlaceholder": "Pesquisar..."
            		},
            		"dom":' <"search"fl><"top">rt<"bottom"ip><"clear">'
            	} );
            	$('#processing-modal').modal('hide');
        	},500);
        }
    }); 
    
    $scope.$watch('preencheuErro', function() {
        if ($scope.preencheuErro){
        	$timeout(function(){
            	$('#processing-modal').modal('hide');
        	},500);
        }
    });
    
    $timeout(function(){
    	if ($scope.atualizar){
    		$scope.atualizar = false;
    	}else{
    		$scope.atualizar = true;
    	}
    },1000);

    
    $interval(function(){
    	if ($scope.atualizar){
    		$scope.atualizar = false;
    	}else{
    		$scope.atualizar = true;
    	}
    },300000);

	
    $scope.$watch('erros', function() {
    
    	if ($scope.erros){
    		$scope.painelAlerta = "painel-visivel";
    	}else{
    		$scope.painelAlerta = "painel-oculto";
    	}
    }); 
    
    $scope.$watch('atualizar', function(){
    	$scope.iconRefresh = "fa fa-refresh fa-spin";
    	$http.get('http://dsoo03:8080/fusion/services/jobs/atualizarErros/').success(function(data, status) {
    		$http.get('http://dsoo03:8080/fusion/services/jobs/buscarJobsComErro/').success(function(data, status) {
    			
        		if (!$scope.preencheuErro){
        			$scope.preencheuErro = true;
        		}
        		$scope.erros = [];
				$scope.errosDisplay = [];
        		if (data){
//        			$scope.erros.splice(0,$scope.erros.length);
//        			$scope.errosDisplay.splice(0,$scope.errosDisplay.length);
//        		}else{
	    			$.each(data, function(key, value){
	    				if (!contains($scope.erros,value)){
	    					$scope.erros.push(value);
	    					$scope.errosDisplay.push(value);
	    				}
	    			});
        		}
        		console.log($scope.erros);
        		$scope.iconRefresh = "fa fa-refresh";
        		if ($scope.liberarAlert){
        			$scope.msgAlerta = "alert alert-success fade in msg-alert mostrar";
        			$timeout(function(){
        				$scope.msgAlerta = "alert alert-success fade in msg-alert esconder";
        			},2000);
        		}
    		}).error(function(e) {
    			console.log(e);
    		});
		}).error(function(e) {
			console.log(e);
		}); 
    });
    
    console.log("Executou o controller appcontroller");
}]);

// metodo para verificar se objeto ja esta inserido no array
function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i].idJob === obj.idJob) {
            return true;
        }
    }
    return false;
}