<%@page import="java.util.Set"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>

<%!private void alterarSenha(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String senhaAnt = request.getParameter("senhaAnt");
		String senhaNov = request.getParameter("senhaNov");
		String senhaCon = request.getParameter("senhaCon");
		String retorno = "SUCESSO AO ALTERAR SENHA";

		try
		{
			if (senhaAnt != null && !senhaAnt.isEmpty() && senhaNov != null && !senhaNov.isEmpty() && senhaCon != null && !senhaCon.isEmpty() && PortalUtil.getCurrentUser() != null && senhaNov.equals(senhaCon) && senhaNov.length() == 4 && !senhaNov.startsWith("0"))
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("usuarioFusion", PortalUtil.getCurrentUser()));
				filter.addFilter(new QLEqualsFilter("senha", Integer.parseInt(senhaAnt.trim())));
				List<NeoObject> cadeadoList = null;
				cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);
				PersistEngine.getEntityManager().flush();

				if (cadeadoList != null && !cadeadoList.isEmpty())
				{
					NeoObject neoObject = (NeoObject) cadeadoList.get(0);

					EntityWrapper cadeadoWrapper = new EntityWrapper(neoObject);

					cadeadoWrapper.findField("senha").setValue(Long.parseLong(senhaNov.trim()));

					PersistEngine.persist(neoObject);

				}
				else
				{
					retorno = "ERRO SENHA N�O ENCONTRADA";
				}
			}
			else
			{
				retorno = "ERRO AO ALTERAR SENHA, VERIFIQUE SE A MESMA POSSUI APENAS NUMEROS E 4 DIGITOS.";
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "ERRO AO ALTERAR SENHA.";
		}
		finally
		{
			out.print(retorno);
		}
	}%>

<%!private void gerarSenha(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String usuario = request.getParameter("usuario");
		String retorno = "SUCESSO AO CRIAR SENHA";
		Integer numero = 0;
		Boolean flag = Boolean.FALSE;

		try
		{
			if (usuario != null && !usuario.isEmpty() && PortalUtil.getCurrentUser() != null && PortalUtil.getCurrentUser().getCode().equals(usuario))
			{
				QLGroupFilter filterCad = new QLGroupFilter("AND");
				filterCad.addFilter(new QLEqualsFilter("usuarioFusion", PortalUtil.getCurrentUser()));
				List<NeoObject> cadeadoListAux = null;
				cadeadoListAux = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filterCad);
				PersistEngine.getEntityManager().flush();

				if (cadeadoListAux != null && cadeadoListAux.isEmpty() || cadeadoListAux == null)
				{
					NeoUser neoUser = (NeoUser) PortalUtil.getCurrentUser();
					Random gerador = new Random();
					while (true)
					{
						numero = 1000 + gerador.nextInt(8999);

						QLGroupFilter filter = new QLGroupFilter("AND");
						filter.addFilter(new QLEqualsFilter("codigoLigador", Long.parseLong(numero.toString())));
						List<NeoObject> cadeadoList = null;
						cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);

						PersistEngine.getEntityManager().flush();
						if (cadeadoList != null && cadeadoList.isEmpty() || cadeadoList == null)
							break;

					}

					NeoObject cadeadoObj = AdapterUtils.createNewEntityInstance("TICadeado");

					EntityWrapper cadeadoWrapper = new EntityWrapper(cadeadoObj);

					if (cadeadoWrapper != null)
					{
						Integer senha = 1000 + gerador.nextInt(8999);
						cadeadoWrapper.findField("codigoLigador").setValue(Long.parseLong(numero.toString()));
						cadeadoWrapper.findField("senha").setValue(Long.parseLong(numero.toString()));
						cadeadoWrapper.findField("usuarioFusion").setValue(neoUser);

						QLGroupFilter filter = new QLGroupFilter("AND");
						filter.addFilter(new QLEqualsFilter("codigo", 1L));
						List<NeoObject> permissaoCadeadoList = null;
						permissaoCadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIPermissaoCadeado"), filter);

						if (permissaoCadeadoList != null && !permissaoCadeadoList.isEmpty())
							cadeadoWrapper.findField("listaPermissoes").setValue(permissaoCadeadoList);

						PersistEngine.persist(cadeadoObj);

						if (neoUser.getEmail() != null && !neoUser.getEmail().isEmpty() && numero != null && neoUser.getFullName() != null && !neoUser.getFullName().isEmpty())
							flag = enviaSenhaCadeado(neoUser.getEmail(), String.valueOf(numero), neoUser.getFullName());

						if (flag)
							retorno = "E-MAIL ENVIADO COM SUCESSO.";
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "ERRO AO CRIAR SENHA";
		}
		finally
		{
			out.print(retorno);
		}
	}%>

<%!private void recuperarSenha(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String usuario = request.getParameter("usuario");
		String retorno = "SUCESSO AO RECUPERAR SENHA";
		Boolean flag = Boolean.FALSE;

		try
		{
			if (usuario != null && !usuario.isEmpty() && PortalUtil.getCurrentUser() != null && PortalUtil.getCurrentUser().getCode().equals(usuario))
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("usuarioFusion", PortalUtil.getCurrentUser()));
				List<NeoObject> cadeadoList = null;
				cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);
				PersistEngine.getEntityManager().flush();

				if (cadeadoList != null && !cadeadoList.isEmpty())
				{
					NeoObject neoObject = (NeoObject) cadeadoList.get(0);

					EntityWrapper cadeadoWrapper = new EntityWrapper(neoObject);

					Long senha = (Long) cadeadoWrapper.findValue("senha");

					NeoUser neoUser = (NeoUser) PortalUtil.getCurrentUser();

					if (neoUser.getEmail() != null && !neoUser.getEmail().isEmpty() && senha != null && neoUser.getFullName() != null && !neoUser.getFullName().isEmpty())
						flag = enviaSenhaCadeado(neoUser.getEmail(), String.valueOf(senha), neoUser.getFullName());

					if (flag)
						retorno = "E-MAIL ENVIADO COM SUCESSO.";

				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "ERRO AO RECUPERAR SENHA";
		}
		finally
		{
			out.print(retorno);
		}

	}%>

<%!//Opera��o: 1-Consulta Login (a��o: pesquisarLiberacao - Sugest�o de nome: consulta)                                                                  
	private void pesquisarLiberacao(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String ramal = request.getParameter("ramal");
		String retorno = "";
		Long codigoLigador = 0L;
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");

		try
		{

			if (ramal != null && !ramal.isEmpty())
			{

				QLGroupFilter filterOk = new QLGroupFilter("AND");
				filterOk.addFilter(new QLEqualsFilter("ramal", ramal));
				List<NeoObject> cadeadoListOk = null;
				cadeadoListOk = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeadoLiberado"), filterOk);
				if (cadeadoListOk != null && !cadeadoListOk.isEmpty())
				{
					retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK\"}";
					log.debug("OK - Sem erro diretoria ou presid�ncia. Ramal " + ramal);
				}
				else
				{

					List<NeoUser> objExecutor = null;
					List<NeoObject> cadeadoListRamal = null;
					QLGroupFilter filterRamal = new QLGroupFilter("AND");

					filterRamal.addFilter(new QLEqualsFilter("ramal", ramal));
					QLOpFilter datefilter = new QLOpFilter("dataLiberacao", ">=", new GregorianCalendar());
					filterRamal.addFilter(datefilter);
					cadeadoListRamal = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filterRamal);

					if (cadeadoListRamal != null && !cadeadoListRamal.isEmpty())
					{
						NeoObject neoObject = (NeoObject) cadeadoListRamal.get(0);

						EntityWrapper neoUserWrapper = new EntityWrapper(neoObject);

						String usuario = (String) neoUserWrapper.findValue("usuarioFusion.code");
						codigoLigador = (Long) neoUserWrapper.findValue("codigoLigador");

						if (usuario != null && !usuario.isEmpty() && verificaUsuarioDiretoriaPresidencia(usuario))
						{
							retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK\"}";
							log.debug("OK - Sem erro diretoria ou presid�ncia. Ramal " + ramal);
						}
						else
						{
							GregorianCalendar dataHoraLiberacao = (GregorianCalendar) neoUserWrapper.findValue("dataLiberacao");

							if (dataHoraLiberacao != null && dataHoraLiberacao.after(new GregorianCalendar()))
							{
								retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK;" + codigoLigador + "\"}";
								log.debug("OK : " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss") + " Liberado at�:  " + NeoUtils.safeDateFormat(dataHoraLiberacao, "dd/MM/yyyy HH:mm:ss"));

							}
							else
							{
								retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
								log.debug("199-Ramal n�o liberado hora de utiliza��o menor doque a atual Data Atual: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss") + " Liberado at�:  " + NeoUtils.safeDateFormat(dataHoraLiberacao, "dd/MM/yyyy HH:mm:ss"));
							}
						}
					}
					else
					{
						retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
						log.debug("299 - Ramal n�o encontrado, sem cadeado vinculado   (Sem autorizacao) Ramal: " + ramal);
					}
				}
			}
			else
			{
				retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
				log.debug("299 - Ramal n�o encontrado ou sem cadeado vinculado. Ramal " + ramal);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "{\"id\":\"" + ramal + "\", \"return\":\"999\"}";
		}
		finally
		{

// 			if (ramal != null && !ramal.isEmpty())
// 			{
// 				QLGroupFilter filterBloq = new QLGroupFilter("AND");
// 				filterBloq.addFilter(new QLEqualsFilter("ramal", Long.parseLong(ramal)));
// 				List<NeoObject> cadeadoListBloq = null;
// 				cadeadoListBloq = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeadoBloqueado"), filterBloq);
// 				if ((cadeadoListBloq != null && cadeadoListBloq.isEmpty()) || cadeadoListBloq == null)
// 				{
// 					retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK;" + 0000 + "\"}";
// 				}
// 			}
			out.print(retorno);
		}

	}%>

<%!//Opera��o: 2-Autoriza��o (acao: autorizacao)
	//ou
	//Opera��o: 3-Login (a��o: efetuarLiberacao - Sugest�o de nome: login)
	private void efetuarLiberacao(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String retorno = "";
		String senha = request.getParameter("senha");
		String codigoLigador = request.getParameter("codigo");
		String ramal = request.getParameter("ramal");
		String tempo = request.getParameter("tempo");
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");
		synchronized (this)
		{
			try
			{
				if (ramal != null && !ramal.isEmpty() && !ramal.contains("null") && senha != null && !senha.isEmpty() && !senha.contains("null") && codigoLigador != null && !codigoLigador.isEmpty() && !codigoLigador.contains("null"))
				{

					QLGroupFilter filterOk = new QLGroupFilter("AND");
					filterOk.addFilter(new QLEqualsFilter("ramal", ramal));
					List<NeoObject> cadeadoListOk = null;
					cadeadoListOk = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeadoLiberado"), filterOk);
					if (cadeadoListOk != null && cadeadoListOk.isEmpty())
					{

						QLGroupFilter filterAux = new QLGroupFilter("AND");
						filterAux.addFilter(new QLEqualsFilter("ramal", ramal));
						QLOpFilter datefilter = new QLOpFilter("dataLiberacao", ">=", new GregorianCalendar());
						filterAux.addFilter(datefilter);
						List<NeoObject> cadeadoListAux = null;
						cadeadoListAux = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filterAux);
						if (cadeadoListAux != null && !cadeadoListAux.isEmpty())
						{
							NeoObject neoObject = (NeoObject) cadeadoListAux.get(0);

							EntityWrapper neoUserWrapper = new EntityWrapper(neoObject);

							neoUserWrapper.findField("dataLiberacao").setValue(new GregorianCalendar());
							neoUserWrapper.findField("ramal").setValue("0000");
							PersistEngine.persist(neoObject);
						}

						QLGroupFilter filter = new QLGroupFilter("AND");
						filter.addFilter(new QLEqualsFilter("codigoLigador", Long.parseLong(codigoLigador)));
						filter.addFilter(new QLEqualsFilter("senha", Long.parseLong(senha)));
						List<NeoObject> cadeadoList = null;
						cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);

						if (cadeadoList != null && !cadeadoList.isEmpty())
						{
							NeoObject neoObject = (NeoObject) cadeadoList.get(0);

							EntityWrapper neoUserWrapper = new EntityWrapper(neoObject);
							if (tempo != null && !tempo.isEmpty() && !tempo.contains("null"))
							{
								GregorianCalendar dataCalendar = new GregorianCalendar();

								switch (Integer.parseInt(tempo))
								{
									case 1:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +1);
										break;
									case 2:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +2);
										break;
									case 3:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +3);
										break;
									case 4:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +4);
										break;
									case 5:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +5);
										break;
									case 6:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +6);
										break;
									case 7:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +7);
										break;
									case 8:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +10);
										dataCalendar.set(Calendar.MINUTE, 18);
										break;
									case 9:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +9);
										break;
									case 10:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +10);
										break;
									case 11:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +11);
										break;
									case 12:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +12);
										break;
									default:
										dataCalendar.add(Calendar.HOUR_OF_DAY, +1);
										break;
								}

								neoUserWrapper.findField("dataLiberacao").setValue(dataCalendar);
								neoUserWrapper.findField("ramal").setValue(ramal);

								PersistEngine.persist(neoObject);
								if (retorno != null && retorno.isEmpty())
									log.debug("3-Libera��o de hor�rio efetuada. Ramal - " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha + " Tempo - " + tempo);
							}
							if (retorno != null && retorno.isEmpty())
								retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK\"}";

						}
						else
						{
							retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR3\"}";
							log.debug("99-Usu�rio senha n�o encontrado Ramal - " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);
						}
					}
					else if (cadeadoListOk != null && !cadeadoListOk.isEmpty())
					{
						retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK\"}";
						log.debug("O ramal j� possui libera��o " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);
					}
					else if (cadeadoListOk == null)
					{
						retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR2\"}";
						log.debug("99-Usu�rio senha n�o encontrado Ramal - " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);
					}
				}
				else
				{
					///VERIFICAR OK ENCONTRADO QUANDO N�O HOUVER DADOS
					retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR1\"}";
					log.debug("99-Usu�rio senha n�o encontrado Ramal - " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);

				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				retorno = "{\"id\":\"" + ramal + "\", \"return\":\"999\"}";
				log.debug("999 - Erro desconhecido.   (Sem retorno do Fusion)" + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);
			}
			finally
			{
				out.print(retorno);
			}
		}
	}%>

<%!//Opera��o: 4-Logoff (a��o: removerLiberacao - Sugest�o de nome: logoff)
	private void removerLiberacao(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String retorno = "";
		String senha = request.getParameter("senha");
		String codigoLigador = request.getParameter("codigo");
		String ramal = request.getParameter("ramal");
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");

		try
		{
			if ((ramal != null && !ramal.isEmpty() && !ramal.contains("null")) || senha != null && !senha.isEmpty() && !senha.contains("null") && codigoLigador != null && !codigoLigador.isEmpty() && !codigoLigador.contains("null"))
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				if (ramal != null && !ramal.isEmpty() && !ramal.contains("null"))
				{
					filter.addFilter(new QLEqualsFilter("ramal", ramal));
				}
				else
				{
					filter.addFilter(new QLEqualsFilter("codigoLigador", Long.parseLong(codigoLigador)));
					filter.addFilter(new QLEqualsFilter("senha", Long.parseLong(senha)));
				}

				List<NeoObject> cadeadoList = null;
				cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);

				if (cadeadoList != null && !cadeadoList.isEmpty())
				{
					NeoObject neoObject = (NeoObject) cadeadoList.get(0);

					EntityWrapper neoUserWrapper = new EntityWrapper(neoObject);

					GregorianCalendar dataCalendar = new GregorianCalendar();

					log.debug("6-Bloqueio de ramal efetuado. Ramal - " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);

					neoUserWrapper.findField("dataLiberacao").setValue(dataCalendar);
					neoUserWrapper.findField("ramal").setValue("0000");

					PersistEngine.persist(neoObject);

					retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK\"}";

				}
				else
				{
					retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
					log.debug("99-Usu�rio ou senha n�o encontrado. Ramal - " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);
				}
			}
			else
			{
				retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
				log.debug("99-Usu�rio ou senha n�o encontrado. Ramal - " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "{\"id\":\"" + ramal + "\", \"return\":\"999\"}";
			log.debug("999 - Erro desconhecido.   (Sem retorno do Fusion)" + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);
		}
		finally
		{
			out.print(retorno);
		}
	}%>

<%!//Opera��o: 5-Registro de log (a��o: salvarLogLigacao - Sugest�o de nome: salvarLogLigacao)
	private void salvarLogLigacao(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String codigoLigador = request.getParameter("codigo");
		String codigoLigacao = request.getParameter("uniqueid");
		String ramal = request.getParameter("ramal");
		String retorno = "";
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");

		try
		{
			if (codigoLigador != null && !codigoLigador.isEmpty() && !codigoLigador.contains("null") && codigoLigacao != null && !codigoLigacao.isEmpty() && !codigoLigacao.contains("null") && ramal != null && !ramal.isEmpty() && !ramal.contains("null"))
			{
				QLGroupFilter filterCad = new QLGroupFilter("AND");
				filterCad.addFilter(new QLEqualsFilter("codigoLigador", Long.parseLong(codigoLigador)));
				List<NeoObject> cadeadoListAux = null;
				cadeadoListAux = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filterCad);

				if (cadeadoListAux != null && !cadeadoListAux.isEmpty())
				{
					NeoObject cadeadoObj = AdapterUtils.createNewEntityInstance("TILogCadeado");

					EntityWrapper cadeadoWrapper = new EntityWrapper(cadeadoObj);

					if (cadeadoWrapper != null)
					{
						cadeadoWrapper.findField("cadeado").setValue(cadeadoListAux.get(0));
						cadeadoWrapper.findField("ramal").setValue(ramal);
						cadeadoWrapper.findField("uniqueid").setValue(codigoLigacao);
						cadeadoWrapper.findField("dataHora").setValue(new GregorianCalendar());

						PersistEngine.persist(cadeadoObj);

						log.debug("5-Log salvo com sucesso. Ramal - " + ramal + " C�digo - " + codigoLigador + " uniqueid - " + codigoLigacao);
						retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK\"}";
					}
				}
				else
				{
					log.debug("Erro ao salvar log c�digo do ligador n�o encontrado. Ramal - " + ramal + " C�digo - " + codigoLigador + " uniqueid - " + codigoLigacao);
					retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
				}
			}
			else
			{
				log.debug("Erro ao salvar log. Ramal - " + ramal + " C�digo - " + codigoLigador + " uniqueid - " + codigoLigacao);
				retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "{\"id\":\"" + ramal + "\", \"return\":\"999\"}";
			log.error("999-Erro desconhecido (Exception) Ramal - " + ramal + " C�digo - " + codigoLigador + " uniqueid - " + codigoLigacao);
		}
		finally
		{
			out.print(retorno);
		}
	}%>

<%!/**
	 * M�TODO PARA VERIFICAR PERMISS�ES EM DESENVOLVIMENTO
	 **/
	private void verificaPermissao(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String ramal = request.getParameter("ramal");
		String senha = request.getParameter("senha");
		String codigoLigador = request.getParameter("codigo");
		String retorno = "";
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");
		synchronized (this)
		{

			try
			{
				if (ramal != null && !ramal.isEmpty() && !ramal.contains("null") && senha != null && !senha.isEmpty() && !senha.contains("null") && codigoLigador != null && !codigoLigador.isEmpty() && !codigoLigador.contains("null"))
				{

					QLGroupFilter filter = new QLGroupFilter("AND");
					filter.addFilter(new QLEqualsFilter("codigoLigador", Long.parseLong(codigoLigador)));
					filter.addFilter(new QLEqualsFilter("senha", Long.parseLong(senha)));
					List<NeoObject> cadeadoList = null;
					cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);

					if (cadeadoList != null && !cadeadoList.isEmpty())
					{
						NeoObject neoObject = (NeoObject) cadeadoList.get(0);

						EntityWrapper neoUserWrapper = new EntityWrapper(neoObject);

						String usuario = (String) neoUserWrapper.findValue("usuarioFusion.code");

						List<NeoObject> objExecutor = null;

						if (usuario != null)
							objExecutor = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code", usuario));

						if (objExecutor != null && !objExecutor.isEmpty() && objExecutor.size() == 1)
						{
							NeoUser obj = (NeoUser) objExecutor.get(0);
							Boolean isDiretoria = ((obj.getGroup().getCode().contains("Diretoria")) || (obj.getGroup().getCode().contains("Presid�ncia")));
							if (isDiretoria)
							{
								retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK\"}";
								log.debug("800- Sem erro diretoria ou presid�ncia. Ramal " + ramal);
							}
							else
							{
								retorno = getPermissoesCadeado(obj, ramal);
							}

						}
						else if (objExecutor != null && !objExecutor.isEmpty() && objExecutor.size() >= 1)
						{
							retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
							log.debug("1-Mais de um usu�rio para o mesmo ramal. Ramal " + ramal);
						}
						else if (objExecutor != null && objExecutor.isEmpty() || objExecutor == null)
						{
							retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
							log.debug("2-Ramal sem usu�rio. Ramal " + ramal);
						}

					}
					else
					{
						retorno = "{\"id\":\"" + ramal + "\", \"return\":\"ERR\"}";
						log.debug("99-Usu�rio ou senha n�o encontrado. Ramal - " + ramal + " C�digo - " + codigoLigador + " Senha - " + senha);
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				retorno = "{\"id\":\"" + ramal + "\", \"return\":\"999\"}";
				log.error("999-Erro desconhecido (Exception). Erro " + e.getMessage());
			}
			finally
			{
				out.print(retorno);
			}
		}

	}%>

<%!public static String getPermissoesCadeado(NeoUser neoUser, String ramal)
	{

		String retorno = null;
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");
		try
		{
			if (neoUser != null)
			{

				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("usuarioFusion", neoUser));
				List<NeoObject> cadeadoList = null;
				cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);
				PersistEngine.getEntityManager().flush();

				if (cadeadoList != null && !cadeadoList.isEmpty())
				{

					NeoObject neoObject = (NeoObject) cadeadoList.get(0);

					EntityWrapper cadeadoWrapper = new EntityWrapper(neoObject);

					List<NeoObject> listaPermissoes = (List<NeoObject>) cadeadoWrapper.findValue("listaPermissoes");

					int count = 0;
					String permissao = "";
					if (listaPermissoes != null && !listaPermissoes.isEmpty())
					{
						for (NeoObject neoObjectAux : listaPermissoes)
						{
							if (count > 0)
								permissao += ";";

							NeoObject neoObjectPermissoes = (NeoObject) neoObjectAux;

							EntityWrapper permissaoWrapper = new EntityWrapper(neoObjectPermissoes);
							Long codigo = (Long) permissaoWrapper.findValue("codigo");
							permissao += codigo;
							count++;
						}
						retorno = "{\"id\":\"" + ramal + "\", \"return\":\"OK\";" + permissao + "\"}";
					}
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "{\"id\":\"" + ramal + "\", \"return\":\"999\"}";
		}
		finally
		{
			return retorno;
		}
	}%>

<%!public Boolean enviaSenhaCadeado(String email, String senha, String nome)
	{
		Boolean flag = Boolean.TRUE;
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");
		try
		{
			if (email != null && !email.isEmpty() && validEmail(email))
			{
				StringBuilder noUserMsg = new StringBuilder();

				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) < 12)
				{
					saudacaoEMail = "Bom dia, ";
				}
				else if (saudacao.get(Calendar.HOUR_OF_DAY) >= 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
				{
					saudacaoEMail = "Boa tarde, ";
				}
				else
				{
					saudacaoEMail = "Boa noite, ";
				}

				String title = "N�o Responda - E-mail contendo senha para efetuar liga��es. ";

				noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				noUserMsg.append("            </td>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail + " " + nome + ".<br/> Este e-mail cont�m a sua senha para efetuar liga��es.</strong><br><br><br>");
				noUserMsg.append(" <strong> Segue a sua senha para desbloquear as suas permiss�es para efetuar liga��es via ramal. </strong> <br>");
				noUserMsg.append(" <strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>");
				noUserMsg.append(" <strong> Senha:  </strong>" + senha + " <br>");
				noUserMsg.append(" <strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>");
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");

				OrsegupsUtils.sendEmail2Orsegups(email, "fusion@orsegups.com.br", title, "", noUserMsg.toString());
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			flag = Boolean.FALSE;
		}
		finally
		{
			return flag;
		}
	}%>

<%!public boolean validEmail(String email)
	{
		Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
		Matcher m = p.matcher(email);
		if (m.find())
		{
			return true;
		}
		else
		{
			return false;
		}
	}%>

<%!public String getGeradorSenha()
	{
		int cont = 0, b = 0, c = 0, num = 0;
		String i = "";
		String[] Vetor = new String[7];

		String[] ArrayString = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

		for (c = 0; c < 7; c++)
		{
			num = (int) (Math.random() * 10);

			cont = (int) (Math.random() * ArrayString.length);

			for (b = 0; b <= ArrayString.length; b++)
			{

				if (b == cont)
					i = ArrayString[b];

			}

			Vetor[c] = (i += num);
			i = "";
			num = 0;
		}

		String Senha = "";
		String p1 = "";
		String p2 = "";
		String p3 = "";
		String p4 = "";
		String p5 = "";
		String p6 = "";
		String p7 = "";

		p1 = Vetor[0];
		p2 = Vetor[1];
		p3 = Vetor[2];
		p4 = Vetor[3];
		p5 = Vetor[4];
		p6 = Vetor[5];
		p7 = Vetor[6];

		Senha += p1 + p2 + p3 + p4 + p5 + p6 + p7;

		return Senha;

	}%>


<%!private void consUsuarioGrupo(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String usuario = request.getParameter("usuario");
		StringBuilder builder = new StringBuilder();
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");
		try
		{

			NeoUser neoUser = (NeoUser) PortalUtil.getCurrentUser();
			if (neoUser != null && neoUser.getCode().equals(usuario))
			{
				Set<NeoUser> neoUserAuxList = (Set<NeoUser>) neoUser.getGroup().getResponsible().getAllUsers();
				NeoUser neoUserAux = null;
				for (NeoUser neoUser2 : neoUserAuxList)
				{
					neoUserAux = (NeoUser) neoUser2;
					break;
				}

				if (neoUserAux.getCode().equals(neoUser.getCode()) && 2 == 1)
				{
					Set<NeoUser> neoUsers = neoUser.getGroup().getAllUsers();
					if (neoUsers != null && !neoUsers.isEmpty())
					{

						builder.append("<div class=\"row\">");
						builder.append("       <div class=\"col-sm-6 col-lg-12\">");
						builder.append("        <div class=\"panel panel-primary\">");
						builder.append("            <div class=\"panel-heading\">");
						builder.append("                <h4 class=\"text-center\">Perfil De Respons�vel<span class=\"glyphicon glyphicon-user pull-right\"></span></h4>");
						builder.append("            </div>");
						builder.append("            <div class=\"panel-body text-center\">");
						builder.append(" <div class=\"container-fluid\" style=\"width:100%;height:100%;overflow: auto;margin:0px;padding:0px;\"> ");

						//Search box End 
						builder.append(" <table class=\"table table-striped table-condensed table-responsive\">");
						builder.append(" <thead>");
						builder.append(" <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu4\">");
						builder.append(" <li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" href=\"#\">Regular link</a></li>");
						builder.append(" <li role=\"presentation\" class=\"disabled\"><a role=\"menuitema\" tabindex=\"-1\" href=\"#\">Disabled link</a></li>");
						builder.append(" <li role=\"presentation\"><a role=\"menuitem\" tabindex=\"-1\" href=\"#\">Another link</a></li>");
						builder.append(" </ul>");
						builder.append(" <tr class=\"bg-primary\">");
						//builder.append(" <th class=\"text-center\">Hist�rico</th>
						builder.append(" <th class=\"text-center\"><b>C�digo Fusion</b></th>");
						builder.append(" <th class=\"text-center\"><b>Nome Completo</b></th>");
						builder.append(" <th class=\"text-center\"><b>E-mail</b></th>");
						builder.append(" <th class=\"text-center\"><b>Data de Nascimento</b></th>");
						builder.append(" <th class=\"text-center\"><b>Grupo</b></th>");
						builder.append(" <th class=\"text-center\"><b>Ramal</b></th>");
						builder.append(" <th class=\"text-center\"><b>C�digo Cadeado</b></th>");
						builder.append(" <th class=\"text-center\"><b>�ltimo Login</b></th>");
						builder.append(" <th class=\"text-center\"></th>");
						builder.append(" </tr>");
						builder.append(" </thead>");
						builder.append(" <tbody>");

						for (NeoUser neoUserCom : neoUsers)
						{
							builder.append(" <tr>");
							builder.append(" <td style=\"white-space: normal\">" + neoUserCom.getCode() + "</td>");
							builder.append(" <td style=\"white-space: normal\">" + neoUserCom.getFullName() + "</td>");
							builder.append(" <td style=\"white-space: normal\">" + neoUserCom.getEmail() + "</td>");
							builder.append(" <td style=\"white-space: normal; text-align: right\">" + NeoUtils.safeDateFormat(neoUserCom.getBornDate(), "dd/MM/yyyy HH:mm:ss") + "</td>");
							//builder.append(" <td style=\"white-space: normal\">" + aasupfisObj.getCdHistorico() + "</td>
							builder.append(" <td style=\"white-space: normal; text-align: right\">" + neoUserCom.getGroup().getName() + "</td>");
							EntityWrapper ewUserAux = new EntityWrapper(neoUserCom);
							String extensionNumberAux = (String) ewUserAux.findField("ramal").getValue();
							builder.append(" <td style=\"white-space: normal; text-align: right\">" + extensionNumberAux + "</td>");
							QLGroupFilter filter = new QLGroupFilter("AND");
							filter.addFilter(new QLEqualsFilter("usuarioFusion", neoUserCom));
							List<NeoObject> cadeadoList = null;
							cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);
							PersistEngine.getEntityManager().flush();

							if (cadeadoList != null && !cadeadoList.isEmpty())
							{
								NeoObject neoObject = (NeoObject) cadeadoList.get(0);

								EntityWrapper cadeadoWrapper = new EntityWrapper(neoObject);
								Long usuarioCodigoLigado = (Long) cadeadoWrapper.findValue("codigoLigador");
								builder.append(" <td style=\"white-space: normal; text-align: right\">" + usuarioCodigoLigado + "</td>");
							}
							else
							{
								builder.append(" <td style=\"white-space: normal; text-align: right\"> N�o encontrado </td>");
							}

							builder.append(" <td style=\"white-space: normal; text-align: right\">" + NeoUtils.safeDateFormat(neoUserCom.getLastLogin(), "dd/MM/yyyy HH:mm:ss") + "</td>");

							builder.append(" <td class=\"text-center\">");
							builder.append(" <a data-toggle=\"modal\" title=\"Inserir informa��o\" data-target=\"#myModalPermissao\" id=\"openModal\"  href=\"#myModalPermissao\" class=\"btn btn-primary btn-xs\" onclick=\"abrePopUpPermissoes('" + neoUser + "')\"  style=\"color: white;\"><span class=\"glyphicon glyphicon-search\"></span></a>");
							builder.append(" </td></tr> ");
						}
						builder.append(" </tbody>");
						builder.append(" </table>");

						builder.append("        </div>");
						builder.append("    </div>");
						builder.append("</div>");

						builder.append(" <div class=\"modal\" id=\"myModalPermissao\">");
						builder.append(" <div class=\"modal-dialog\">");
						builder.append(" <div class=\"modal-content\">");
						builder.append(" <div class=\"modal-header\">");
						builder.append(" <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">�</button>");
						builder.append(" <h4 class=\"modal-title\">Permiss�es do colaborador</h4>");
						builder.append(" </div>");
						builder.append(" <div class=\"modal-body\">");
						builder.append(" <form role=\"form\">");
						builder.append(" <div class=\"form-group\">");
						builder.append(" <label for=\"dadosEvento\">Nome</label>");
						builder.append(" <p class=\"help-block\" id=\"pNome\">Nome do colaborador</p>");
						builder.append(" <label for=\"dadosEvento\">Ramal</label>");
						builder.append(" <p class=\"help-block\" id=\"pRamal\">Ramal</p>");

						builder.append(" <ul class=\"list-group\"> ");
						builder.append(" <li class=\"list-group-item\">Interna</li> ");
						builder.append(" <li class=\"list-group-item\">Externa</li> ");
						builder.append(" <li class=\"list-group-item\">Internacional</li> ");
						builder.append(" <li class=\"list-group-item\">Interna Semanal</li> ");
						builder.append(" <li class=\"list-group-item\">Interna / Externa Semanal</li> ");
						builder.append(" </ul> ");

						builder.append(" </form>");
						builder.append(" </div>");
						builder.append(" <div class=\"modal-footer\">");
						builder.append(" <a  data-dismiss=\"modal\" class=\"btn btn-default\">Cancelar</a>");
						builder.append(" <a  class=\"btn btn-primary\" style=\"color: white;\" onclick=\"cadastrarInformacao()\">Salvar</a>");
						builder.append(" </div>");
						builder.append(" </div>");
						builder.append(" </div>");
						builder.append(" </div>");
						builder.append(" </div>");
					}
				}
				else
				{
					builder.append("<div class=\"row\">");
					builder.append("       <div class=\"col-sm-8 col-lg-12\">");
					builder.append("        <div class=\"panel panel-primary\">");
					builder.append("            <div class=\"panel-heading\">");
					builder.append("                <h4 class=\"text-center\">Perfil<span class=\"glyphicon glyphicon-user pull-right\"></span></h4>");
					builder.append("            </div>");
					builder.append("            <div class=\"panel-body text-center\">");
					builder.append("                <p class=\"lead\">");
					builder.append("                    <strong>" + neoUser.getFullName() + "</strong>");
					builder.append("                </p>");
					builder.append("            </div>");
					builder.append("            <ul class=\"list-group list-group-flush\">");
					// 				builder.append("                <li class=\"list-group-item liitem\"><strong>Login:</strong>");
					// 				builder.append("                    <span class=\"pull-right\">" + neoUser.getCode() + "</span>");
					// 				builder.append("                </li>");
					builder.append("                <li class=\"list-group-item liitem\"><strong>E-mail:</strong>");
					builder.append("                    <span class=\"pull-right\">" + neoUser.getEmail() + "</span>");
					builder.append("                </li>");
					// 				builder.append("                <li class=\"list-group-item liitem\"><strong>Grupo:</strong>");
					// 				builder.append("                    <span class=\"pull-right\">" + neoUser.getGroup().getName() + "</span>");
					// 				builder.append("                </li>");

					EntityWrapper ewUserAux = new EntityWrapper(neoUser);
					String extensionNumberAux = (String) ewUserAux.findField("ramal").getValue();

					// 				builder.append("                <li class=\"list-group-item liitem\"><strong>Ramal:</strong>");
					// 				builder.append("                    <span class=\"pull-right\">" + extensionNumberAux + "</span>");
					// 				builder.append("                </li>");

					QLGroupFilter filter = new QLGroupFilter("AND");
					filter.addFilter(new QLEqualsFilter("usuarioFusion", neoUser));
					List<NeoObject> cadeadoList = null;
					cadeadoList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filter);
					PersistEngine.getEntityManager().flush();

					if (cadeadoList != null && !cadeadoList.isEmpty())
					{
						NeoObject neoObject = (NeoObject) cadeadoList.get(0);

						EntityWrapper cadeadoWrapper = new EntityWrapper(neoObject);

						Long usuarioCodigoLigado = (Long) cadeadoWrapper.findValue("codigoLigador");

						builder.append("                <li class=\"list-group-item liitem\"><strong>C�digo Cadeado:</strong> ");
						builder.append("                    <span class=\"pull-right\"><span class=\"label label-danger\" style=\"font-family: verdana; font-size: 11px;font-weight: bold;\" >" + usuarioCodigoLigado + "</span></span>");
						builder.append("                </li>");
					}
					builder.append("                 <li class=\"list-group-item liitem\"><strong>�ltimo login:</strong>");
					builder.append("                    <span class=\"pull-right\">" + NeoUtils.safeDateFormat(neoUser.getLastLogin(), "dd/MM/yyyy HH:mm:ss") + "</span>");
					builder.append("                </li>");
					builder.append("                   <li class=\"list-group-item liitem\">");
					builder.append("                   <div align=\"right\">");

					QLGroupFilter filterAux = new QLGroupFilter("AND");
					filterAux.addFilter(new QLEqualsFilter("usuarioFusion", neoUser));
					System.out.println();
					List<NeoObject> cadeadoListAux = null;
					cadeadoListAux = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filterAux);
					PersistEngine.getEntityManager().flush();

					if (cadeadoListAux != null && cadeadoListAux.isEmpty() || cadeadoListAux == null)
					{

						builder.append("                    <a data-toggle=\"modal\" title=\"Gerar senha\" data-target=\"#myModal\" id=\"openModal\"  href=\"#myModal\" onclick=\"gerarSenha('" + neoUser.getCode() + "')\" class=\"btn btn-success btn-lg\"  style=\"color: white;\">");
						builder.append("                    <span class=\"glyphicon glyphicon-floppy-saved\"></span>");
						builder.append("                    </a>");
					}
					if (cadeadoListAux != null && !cadeadoListAux.isEmpty())
					{

						builder.append("                    <a data-toggle=\"modal\" title=\"Alterar senha\" data-target=\"#myModal\" id=\"openModal\"  onclick=\"abrePopUpDetalhes()\" class=\"btn btn-primary btn-lg\"  style=\"color: white;\">");
						builder.append("                    <span class=\"glyphicon glyphicon-floppy-open\"></span>");
						builder.append("                    </a>");
						builder.append("                    <a data-toggle=\"modal\" title=\"Esqueceu a senha\" data-target=\"#myModal\" id=\"openModal\" onclick=\"recuperarSenha('" + neoUser.getCode() + "')\" class=\"btn btn-warning btn-lg\"  style=\"color: white;\">");
						builder.append("                    <span class=\"glyphicon glyphicon-envelope\"></span>");
						builder.append("                    </a>");
					}
					builder.append("                   </div>");
					builder.append("                </li>");
					builder.append("            </ul>");
					builder.append("            <div class=\"panel-footer\">");
					builder.append("                <div class=\"row\">");
					builder.append("                    <div class=\"col-xs-4 col-sm-3 col-md-4 col-lg-2\"></div>");
					builder.append("                    <div class=\"col-xs-2 col-sm-4 col-md-4 col-lg-4\" id=\"qr1\"></div>");
					builder.append("                    <div class=\"col-xs-6 col-sm-5 col-md-4 col-lg-6\"></div>");
					builder.append("                </div>");
					builder.append("            </div>");
					builder.append("        </div>");
					builder.append("    </div>");
					builder.append("</div>");
					builder.append(" <div class=\"modal\" id=\"myModal\" > ");
					builder.append(" <div class=\"modal-dialog\"> ");
					builder.append("  <div class=\"modal-content\"> ");
					builder.append("   <div class=\"modal-header\"> ");
					builder.append("    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">�</button> ");
					builder.append("     <h4 class=\"modal-title\">Alterar a Senha <span class=\"extra-title muted\"></span></h4> ");
					builder.append("   </div> ");
					builder.append("    <div class=\"modal-body\"> ");
					builder.append("     <form role=\"form\"> ");
					builder.append("    <div class=\"form-group\"> ");
					builder.append("     <label for=\"dadosEvento\">Senha atual</label> ");
					builder.append("      <div class=\"controls\">");
					builder.append("           <input type=\"password\" id=\"current_password\" name=\"current_password\" maxlength=\"4\"  class=\"form-control\"  >");
					builder.append("                 </div>");
					builder.append("         <label for=\"dadosEvento\">Nova senha - (Entre 1000 � 9999)</label> ");
					builder.append("       <div class=\"controls\">");
					builder.append("           <input type=\"password\" id=\"new_password\"  name=\"new_password\" maxlength=\"4\"  class=\"form-control\" >");
					builder.append("                   </div>");
					builder.append("        <label for=\"dadosEvento\">Confirmar senha - (Entre 1000 � 9999)</label> ");
					builder.append("       <div class=\"controls\">");
					builder.append("                <input type=\"password\"  id=\"confirm_password\"  name=\"confirm_password\" maxlength=\"4\"  class=\"form-control\" >");
					builder.append("               </div>");
					builder.append("     </div> ");
					builder.append("    </form> ");
					builder.append("   </div> ");
					builder.append("    <div class=\"modal-footer\"> ");
					builder.append("   <a  data-dismiss=\"modal\" class=\"btn btn-default\">Cancelar</a> ");
					builder.append("  <a  class=\"btn btn-primary\" style=\"color: white;\" onclick=\"alteraSenha()\" >Salvar</a> ");
					builder.append("    </div> ");
					builder.append("   </div> ");
					builder.append("  </div> ");
					builder.append(" </div> ");

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("ERRO CADEADO " + e.getMessage());
			out.print("ERRO");

		}
		finally
		{
			if (builder != null && !builder.toString().isEmpty())
				out.print(builder.toString());
		}
	}%>

<%!private static Boolean verificaUsuarioDiretoriaPresidencia(String usuario)
	{
		List<NeoObject> objExecutor = null;
		Boolean flag = Boolean.FALSE;
		final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");
		try
		{
			if (usuario != null && !usuario.isEmpty())
			{

				objExecutor = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code", usuario));

				if (objExecutor != null && !objExecutor.isEmpty() && objExecutor.size() == 1)
				{
					NeoUser obj = (NeoUser) objExecutor.get(0);
					Boolean isDiretoria = ((obj.getGroup().getCode().contains("Diretoria")) || (obj.getGroup().getCode().contains("Presid�ncia")));
					if (isDiretoria)
					{
						flag = Boolean.TRUE;
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return flag;
		}

	}%>

<%!private static List<NeoUser> neoUsers()
	{

		List<NeoUser> users = new ArrayList<NeoUser>();

		DecimalFormat f = new DecimalFormat("000000000");
		ArrayList<HttpSession> hashMap = new ArrayList<HttpSession>();
		SortedMap<String, HttpSession> sortedUsers = new TreeMap<String, HttpSession>();
		for (HttpSession sessao : hashMap)
		{
			try
			{
				final String user = (String) sessao.getAttribute("user");
				long l = 999999999l - sessao.getLastAccessedTime();
				sortedUsers.put(f.format(l) + user.toLowerCase(), sessao);
			}
			catch (Exception e)
			{
				// Eat it
			}
		}

		for (Map.Entry<String, HttpSession> entry : sortedUsers.entrySet())
		{
			try
			{
				HttpSession sessao = entry.getValue();
				//final NeoUser user = (NeoUser) sessao.getAttribute("user");
				final String user = (String) sessao.getAttribute("user");

				NeoUser nuser = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", user));
				users.add(nuser);
				//				final Object fm = sessao.getAttribute(PortalKeys.E_FORM_MANAGER);
				//
				//				final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				//				final Long creationTime = sessao.getCreationTime();
				//				final Long lastAccessedTime = sessao.getLastAccessedTime();
				//				String sCreationTime = none;
				//				String sLastAccessedTime = none;
				//				String sDownTime = none;
				//				if (creationTime != null)
				//				{
				//					sCreationTime = dateFormat.format(new Date(creationTime));
				//				}
				//				if (lastAccessedTime != null)
				//				{
				//					sLastAccessedTime = dateFormat.format(new Date(lastAccessedTime));
				//					Date now = new Date();
				//					sDownTime = (now.getTime() - lastAccessedTime) + "ms";
				//				}
				//
				//				final String host = (String) ((sessao.getAttribute("RemoteHost") != null) ? sessao.getAttribute("RemoteHost") : none);
				//
				//				System.out.print("<tr>");
				//				System.out.print("<td>" + /*
				//										 * ((user.equals(session.getAttribute("user")))?"*":"") +
				//										 * user.getCode()
				//										 */user + "</td>");
				//				System.out.print("<td> " + sCreationTime + "</td>");
				//				System.out.print("<td> " + sLastAccessedTime + "</td>");
				//				System.out.print("<td> " + sDownTime + "</td>");
				//				System.out.print("<td> " + host + "</td>");

			}
			catch (IllegalStateException e)
			{
				e.printStackTrace();
			}

		}
		return users;
	}%>

<%
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.ti.cadeadoViaSNEP");

	response.setContentType("text/html");
	response.setCharacterEncoding("ISO-8859-1");

	PrintWriter outPrintWriter;
	try
	{
		outPrintWriter = response.getWriter();

		String action = "";

		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
		}

		if (action.equalsIgnoreCase("consUsuarioGrupo"))
		{
			this.consUsuarioGrupo(request, response, outPrintWriter);
		}

		if (action.equalsIgnoreCase("alterarSenha"))
		{
			this.alterarSenha(request, response, outPrintWriter);
		}

		if (action.equalsIgnoreCase("gerarSenha"))
		{
			this.gerarSenha(request, response, outPrintWriter);
		}
		if (action.equalsIgnoreCase("verificaPermissao"))
		{
			this.verificaPermissao(request, response, outPrintWriter);
		}
		if (action.equalsIgnoreCase("recuperarSenha"))
		{
			this.recuperarSenha(request, response, outPrintWriter);
		}
		if (action.equalsIgnoreCase("consulta"))
		{
			this.pesquisarLiberacao(request, response, outPrintWriter);
		}
		if (action.equalsIgnoreCase("autorizacao"))
		{
			this.efetuarLiberacao(request, response, outPrintWriter);
		}
		if (action.equalsIgnoreCase("logoff"))
		{
			this.removerLiberacao(request, response, outPrintWriter);
		}
		if (action.equalsIgnoreCase("salvarLogLigacao"))
		{
			this.salvarLogLigacao(request, response, outPrintWriter);
		}
		if (action.equalsIgnoreCase("login"))
		{
			this.efetuarLiberacao(request, response, outPrintWriter);
		}
		if (action.equalsIgnoreCase("inserirRamalRegional"))
		{
			this.inserirRamalRegional(request, response, outPrintWriter);
		}

	}
	catch (Exception e)
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>

<%!private void inserirRamalRegional(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String usuario = request.getParameter("usuario");
		String retorno = "SUCESSO AO INSERIR RAMAL";
		Integer numero = 0;
		Boolean flag = Boolean.FALSE;

		try
		{
			if (usuario != null && !usuario.isEmpty() && PortalUtil.getCurrentUser() != null && PortalUtil.getCurrentUser().getCode().equals(usuario))
			{

				for (int i = 6900; i <= 6999; i++)
				{
					NeoObject cadeadoObj = AdapterUtils.createNewEntityInstance("TICadeadoBloqueado");
					numero = i;
					EntityWrapper cadeadoWrapper = new EntityWrapper(cadeadoObj);

					if (cadeadoWrapper != null)
					{

						cadeadoWrapper.findField("ramal").setValue(Long.parseLong(numero.toString()));

						PersistEngine.persist(cadeadoObj);

					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "ERRO AO INSERIR RAMAL";
		}
		finally
		{
			out.print(retorno);
		}
	}%>
