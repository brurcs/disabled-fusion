<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.neomind.fusion.common.NeoObject"%>
<%@ page import="com.neomind.fusion.doc.NeoFile"%>
<%@ page import="com.neomind.fusion.bpa.*"%>
<%@ page import="com.neomind.fusion.common.*"%>
<%@ page import="com.neomind.fusion.entity.*"%>
<%@ page import="com.neomind.fusion.entity.form.*"%>
<%@ page import="com.neomind.fusion.persist.*"%>
<%@ page import="com.neomind.fusion.eform.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.neomind.fusion.workflow.Activity"%>
<%@ page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page import="com.neomind.util.NeoUtils"%>
<%@ page import="com.neomind.fusion.workflow.Task"%>
<%@ page import="com.neomind.fusion.i18n.I18nUtils"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.text.DecimalFormat"%>

<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>

<portlet:defineObjects />
<form:defineObjects />
<portal:head title="Impress�o do Relat�rio por CPF">

<%
String idPai = request.getParameter("idPai");
String cpf = request.getParameter("cpf");
String autorizacao = request.getParameter("autorizacao");

%>


<font size='10'face='verdana'>
<p align='justify'>
Gerando PDF...
</p>
<br>
<div align='center'>
<input type='button' value='Fechar' onClick='javascript:window.close();'>
</div>
</font>
<script type="text/javascript">

	var url = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.OrsegupsServletUtils?action=doImprimeFichaPorCPF&idPai=<%=idPai%>&cpf=<%=cpf%>&autorizacao=<%=autorizacao%>';
	try
	{
		location.href=url;
	}
	finally
	{}
	
</script> 

</portal:head>
