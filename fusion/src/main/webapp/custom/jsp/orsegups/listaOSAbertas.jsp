<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@ page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>

<portlet:defineObjects />
<form:defineObjects />
<%
	String codigoCliente = (String) request.getParameter("codigoCliente");

%>

<portal:head title="Lista de OS`s Abertas">
<cw:main>
	<cw:header title="Lista de OS`s Abertas" />
	
	<cw:body id="area_scroll">
	
	<div id="resultado">
	</div>
	

	<script type="text/javascript">

		$(document).ready(function() 
			{
			var result = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterSearchProcessServlet?codigoCliente=<%=codigoCliente%>';
	   			$.ajax({
		  		  url: result,
		  		  success: function( data ) {
		  			 $('#resultado').html(data);
		  			 
		  		  }
		  		});
			}
		);

	</script>

	</cw:body>
	
</cw:main>
</portal:head>