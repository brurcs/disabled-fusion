<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%

	String regional = request.getParameter("regional");

	if(regional == null || regional.trim().equals(""))	{
		out.print("Erro #1 - Regional n�o informada");
		return;
	}

	/*** Declaracoes globais ***/
	String codigoCategoria = "01";
	QLEqualsFilter filtroRegional = new QLEqualsFilter("destino.sequencia",regional);
	QLEqualsFilter filtroCategoria = new QLEqualsFilter("categoriasRsc.codigo",codigoCategoria);
	
	QLGroupFilter filtroRsc = new QLGroupFilter("AND");
	filtroRsc.addFilter(filtroRegional);
	filtroRsc.addFilter(filtroCategoria);
	
	List<NeoObject> listRsc = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoCliente"),filtroRsc,-1, -1,"executivoResponsavel.fullName");
	List<NeoUser> listUser = new ArrayList<NeoUser>();
	Map<NeoUser, ArrayList<NeoObject>> mapRsc = new HashMap<NeoUser, ArrayList<NeoObject>>();
	
	for (NeoObject rsc : listRsc)
	{
		EntityWrapper wRsc = new EntityWrapper(rsc);
		NeoUser user = (NeoUser) wRsc.findField("executivoResponsavel").getValue();
		
		if (!listUser.contains(user)) 
		{
			listUser.add(user);
		}
	}
	
	for (NeoUser user : listUser)
	{
		QLEqualsFilter filtroExecutivo = new QLEqualsFilter("executivoResponsavel",user);
		QLGroupFilter filtroRscUser = new QLGroupFilter("AND");
		filtroRscUser.addFilter(filtroExecutivo);
		filtroRscUser.addFilter(filtroRegional);
		filtroRscUser.addFilter(filtroCategoria);
		
		ArrayList<NeoObject> listRscUser = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoCliente"),filtroRscUser,-1, -1,"dataRegistro");
		mapRsc.put(user, listRscUser);
	}
%>



<portal:head title="Vendas">
	<cw:main>
		<cw:header title="Vendas" />
		<cw:body id="area_scroll">
			<%
				for (NeoUser user : listUser)
				{
			%>
			
			<fieldset class="fieldGroup" >
				<legend class="legend" style="font-size: 1.2em">&nbsp;<%= user.getFullName() %>&nbsp;</legend>
				<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
					<tr style="cursor: auto">
						<th style="cursor: auto; white-space: normal" width="10%">Data de Solicita��o</th>
						<th style="cursor: auto; white-space: normal" width="35%">Descri��o</th>
						<th style="cursor: auto; white-space: normal" width="10%">Data de Atendimento</th>
						<th style="cursor: auto; white-space: normal" width="35%">Provid�ncias Tomadas</th>
						<th style="cursor: auto; white-space: normal" width="10%">Grau de Satisfa��o</th>
					</tr>
					<tbody>
					<%
						for (NeoObject object : mapRsc.get(user)) 
						{
							EntityWrapper wRsc = new EntityWrapper(object);
							GregorianCalendar dataRegistro = (GregorianCalendar) wRsc.findField("dataRegistro").getValue();
							GregorianCalendar dataVerificacao = (GregorianCalendar) wRsc.findField("dataVerificacao").getValue();
							String descricao = (String) wRsc.findField("descricao").getValue();
							String providencias = (String) wRsc.findField("providenciasTomadas").getValue();
							NeoObject obj = (NeoObject) wRsc.findField("grauSatisfacao").getValue();
							String grauSatisfacao = null;
							if (obj != null)
							{
								grauSatisfacao = (String) wRsc.findField("grauSatisfacao.descricao").getValue();
							} 
					%>
						<tr>
							<td style="white-space: normal"><%=dataRegistro == null ? "&nbsp" : NeoUtils.safeDateFormat(dataRegistro, "dd/MM/yyyy HH:mm")%></td>
							<td style="white-space: normal"><%=descricao == null ? "&nbsp" : descricao%></td>
							<td style="white-space: normal"><%=dataVerificacao == null ? "&nbsp" : NeoUtils.safeDateFormat(dataVerificacao, "dd/MM/yyyy HH:mm")%></td>
							<td style="white-space: normal"><%=providencias == null ? "&nbsp" : providencias%></td>
							<td style="white-space: normal"><%=grauSatisfacao == null ? "&nbsp" : grauSatisfacao%></td>
						</tr>
					<%
						}
					%>	
					</tbody>
				</table>	
			</fieldset>
			<br/>
			<%
				}
			%>
		</cw:body>
	</cw:main>
</portal:head>