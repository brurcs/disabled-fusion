<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<portal:head title="Vendas">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
	<cw:main>
		<cw:header title="Rela��o de Liga��es SOO" />
		<cw:body id="area_scroll">
		<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
		</table>
		<%		
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
				
				try {
					String nomeFonteDados = "SNEP_PROD";
					conn = PersistEngine.getConnection(nomeFonteDados);
					StringBuffer sql = new StringBuffer();

					sql.append(" select reg.Codigo as nomeRegional, cdr_ors.CODIGO, cdr.src, cdr.dst, cdr.duration, cdr.calldate from cdr cdr ");
					sql.append(" inner join cdr_orsegups cdr_ors on cdr.uniqueid = cdr_ors.UID ");
					sql.append(" inner join Regionais reg on reg.idRegional = cdr.idRegional ");
					sql.append(" where cdr.disposition = 'ANSWERED' ");
					
					st = conn.prepareStatement(sql.toString());
					rs = st.executeQuery();
					%>			
					<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
						<tr style="cursor: auto">
						    <th style="cursor: auto; white-space: normal" width="15%">Regional</th>
							<th style="cursor: auto; white-space: normal" width="15%">Nome do Colaborador</th>
							<th style="cursor: auto; white-space: normal" width="15%">Ramal do Colaborador</th>
							<th style="cursor: auto; white-space: normal" width="15%">N�mero Discado</th>
							<th style="cursor: auto; white-space: normal" width="15%">Dura��o da Chamada</th>
							<th style="cursor: auto; white-space: normal" width="15%">Data da Liga��o</th>						       
						</tr>
						<tbody>	
						<%
						while (rs.next()) {
						%>	
						<tr>
							
							<% 
							
							String idUsuario = null;
							String nomeColaborador = null;
							
							QLEqualsFilter filtroCadeado = new QLEqualsFilter("codigoLigador", rs.getLong("codigo"));
							List<NeoObject> lstCadeado = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICadeado"), filtroCadeado);
							
							for (NeoObject objCadeado : lstCadeado) 
							{
								EntityWrapper wCadeado = new EntityWrapper(objCadeado);
								idUsuario = (String) wCadeado.findField("usuarioFusion.code").getValue();
							}
							
							if(idUsuario != null && !idUsuario.isEmpty())
							{														
								QLEqualsFilter filtroUsuario = new QLEqualsFilter("code", idUsuario);
								List<NeoObject> lstUsuario = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("NeoUser"), filtroUsuario);
								
								for (NeoObject objUsuario : lstUsuario) 
								{
									EntityWrapper wUsuario = new EntityWrapper(objUsuario);
									nomeColaborador = (String) wUsuario.findField("fullName").getValue();
								}
							}
							else
							{
								nomeColaborador = "Associa��o: Colaborador X Cadeado N�o Encontrada";
							}
							
							%>
						
							<td style="white-space: normal"><%= rs.getString("nomeRegional") %></td>
							<td style="white-space: normal"><%= nomeColaborador %></td>
							<td style="white-space: normal"><%= rs.getString("src") %></td>
							<td style="white-space: normal"><%= rs.getString("dst") %></td>							
							<td style="white-space: normal"><%= rs.getString("duration") %></td>
							<td style="white-space: normal"><%= NeoUtils.safeDateFormat(rs.getTimestamp("calldate"), "dd/MM/yyyy HH:mm")%></td>
						</tr>				
						<%
						}
						%>
						</tbody>
				</table>	
			<%
			
		} catch (Exception e) {
			e.printStackTrace();
			%>
				<script type="text/javascript" >
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
			<%			
		} finally {
		try {
			OrsegupsUtils.closeConnection(conn, st, rs);
		} catch (Exception e) {
			e.printStackTrace();
			%>
				<script type="text/javascript" >
		        	
    			$.messager.alert('Erro',"<%=e.getMessage()%>",'error');
			
							
				</script>
			<%	
			}
		}
		%>
					</fieldset>
							<br/>

		</cw:body>
	</cw:main>
</portal:head>