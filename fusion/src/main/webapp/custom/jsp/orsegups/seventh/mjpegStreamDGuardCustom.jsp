<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>
<%
	String resolucao = "320x240";
	String qualidade = "70";
	String formato = "jpg";
	String urlStr = request.getParameter("urlStr");
	
	
	String data = request.getParameter("data");
	String hora = request.getParameter("hora");
	String servidor = request.getParameter("servidor");
	String camera = ((String)request.getParameter("camera")) == null ? "FFFF01" : (String)request.getParameter("camera");
	byte[] imgData = new byte[8192];
	String userpass = "sigma:sigma";
	boolean ok = false;
	int count = 0;
	ByteArrayOutputStream output = null;
	InputStream is = null;
	HttpURLConnection urlc = null;
	try{
	output = new ByteArrayOutputStream();
	if(urlStr == null || urlStr.isEmpty())
		urlStr = "http://"+servidor+"/camera.cgi?camera="+camera+"&resolucao="+resolucao+"&qualidade="+qualidade+"&formato="+formato;
	  
	do {
		URL url = new URL(urlStr);
		urlc = (HttpURLConnection)url.openConnection();
		String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
		urlc.setRequestProperty ("Authorization", basicAuth);
		if (urlc.getResponseCode() == 200) {
		is = urlc.getInputStream();
		int bytesRead;

		while ((bytesRead = is.read(imgData)) != -1)
		{
			output.write(imgData, 0, bytesRead);
		}
		count++;
		ok = ( (imgData.length > 40000) || (count >= 10));
		}
	} while (ok);
	if(output != null)
	{
		imgData = output.toByteArray();
		response.setContentType("image/jpeg"); 
		response.setContentLength(imgData.length);	
		OutputStream o = response.getOutputStream(); 
		o.write(imgData);
		
		o.flush();
		o.close();
	}
	}catch(Exception e){
		e.printStackTrace();
	}
	finally
	{
		if(is != null)
			is.close();
		
		if(output != null)
		{
			output.flush();
			output.close();
		}
		
	}
%>