<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="java.util.HashSet"%>
<%

	/**
	* Altera��o para chamar o m�todo criado na servlet de cria��o de tarefa simples
	*
	* @author neomind - willian.mews
	* @date 11/03/2015
	*/

	String solicitante = request.getParameter("solicitante");
	String executor = request.getParameter("executor");
	String titulo = request.getParameter("titulo");
	String descricao = request.getParameter("descricao");
	String sPrazo = request.getParameter("prazo");
	String slPrazo = request.getParameter("prazoDias");
	String avanca = request.getParameter("avanca");
	String sOrigem = request.getParameter("origem");
	
	/* Tempor�rio para a��o de vendas. */
	NeoPaper papelGerente = null;
	String codigoRegional = request.getParameter("regional");
	String acao = request.getParameter("acaoConcorrencia");
	boolean acaoConcorrencia = false;
	
	if(NeoUtils.safeIsNotNull(acao)){
		acaoConcorrencia = (acao.equalsIgnoreCase("sim") ? true : false);
	}
	
	Long lPrazo = 0L;
	GregorianCalendar prazo = null;	
	GregorianCalendar dataHoje = new GregorianCalendar();
	
	if (NeoUtils.safeIsNotNull(slPrazo))
	{
		lPrazo = Long.parseLong(slPrazo);
	}
	
	if (lPrazo != 0L)
	{
		prazo = OrsegupsUtils.getSpecificWorkDay(dataHoje, lPrazo);
	}
	else if (sPrazo != null)
	{
		prazo = AdapterUtils.getGregorianCalendar(sPrazo, "dd/MM/yyyy");
	}
	
	/* Tempor�rio para a��o de vendas. */
	if(acaoConcorrencia){
	    Long codReg = Long.parseLong(codigoRegional);
	    GregorianCalendar gc = new GregorianCalendar();
	    gc.add(GregorianCalendar.DAY_OF_MONTH, Integer.parseInt(slPrazo));
	    prazo = gc;  
	    
	    prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
	    prazo.set(GregorianCalendar.MINUTE, 59);
	    prazo.set(GregorianCalendar.SECOND, 59);
	    prazo.set(GregorianCalendar.MILLISECOND, 59);
	    
	    // TODO
	    /* Regional 5 - alterar executor para Itamar Francez*/
	    if(codReg == 5L){
			executor = "itamar.francez";
	    } else if(codReg == 18L || codReg == 23L ){
			executor = "paulo.iopp";
	    }else {
		    papelGerente = OrsegupsUtils.getPapelGerenteRegional(codReg);
		    String sPapelGerente = papelGerente.getCode();
		    executor = OrsegupsUtils.getUserNeoPaper(sPapelGerente);
	    }
	}
	
	prazo.set(GregorianCalendar.HOUR, 23);
	prazo.set(GregorianCalendar.MINUTE, 59);
	prazo.set(GregorianCalendar.SECOND, 59);
	prazo.set(GregorianCalendar.MILLISECOND, 59);
	
	IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
	String retorno = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, sOrigem, avanca, prazo);
	out.print(retorno);

	/**
	 * FIM ALTERA��ES - NEOMIND
	 */
	
	
	 
	 
	 
	 
	/**

	NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("solicitante")));
	if(solicitante == null) {
		out.print("Erro #1 - Solicitante n�o encontrado (#"+request.getParameter("solicitante")+")");
		return;
	}
	String avanca = request.getParameter("avanca");
	boolean isAvanca = true;
	if (avanca != null && (avanca.trim().equals("nao") || avanca.trim().equals("0") || avanca.trim().equals("no")|| avanca.trim().equals("off") )) {
		isAvanca = false;
	}
	InstantiableEntityInfo eOrigem = AdapterUtils.getInstantiableEntityInfo("origemTarefa");	
	if(eOrigem == null) {
		out.print("Erro #5 - Eform origemTarefa n�o encontrado");
		return;
	}
	String sOrigem = request.getParameter("origem");
	if(sOrigem == null) {
		out.print("Erro #1 - Origem inv�lida (#"+sOrigem+")");
		return;
	}
	
	NeoObject origem = (NeoObject) PersistEngine.getObject(eOrigem.getEntityClass(), new QLEqualsFilter("codigo", Long.valueOf(sOrigem)));
	if(origem == null) {
		out.print("Erro #6 - Valor de origem para Tarefa n�o encontrado");
		return;
	}
	
	String exec = request.getParameter("executor");	
	
	String papel = request.getParameter("papel");
	if (NeoUtils.safeIsNotNull(papel))
	{
		NeoPaper objPapel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",papel));
		if (objPapel != null && objPapel.getAllUsers() != null && !objPapel.getAllUsers().isEmpty())
		{
			for (NeoUser user : objPapel.getUsers())
			{
				exec = user.getCode();
				break;
			}
		}
	}
	
	NeoUser executor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", exec));
	String titulo = request.getParameter("titulo");
	String descricao = request.getParameter("descricao");
	String sPrazo = request.getParameter("prazo");
	String slPrazo = request.getParameter("prazoDias");
	
	Long lPrazo = 0L;
	
	if (NeoUtils.safeIsNotNull(slPrazo))
	{
		lPrazo = Long.parseLong(slPrazo);
	}
	
	GregorianCalendar prazo = null;	
	GregorianCalendar dataHoje = new GregorianCalendar();

	if (isAvanca) {
		if(executor == null) {
			out.print("Erro #2 - Executor n�o encontrado (#"+request.getParameter("executor")+")");
			return;
		}
		if(sPrazo == null && lPrazo == null) {
			out.print("Erro #3 - Erro ao gerar o prazo");
			return;
		}
	} 
	if (lPrazo != 0L)
	{
		prazo = OrsegupsUtils.getSpecificWorkDay(dataHoje, lPrazo);
	}
	else if (sPrazo != null)
	{
		prazo = AdapterUtils.getGregorianCalendar(sPrazo, "dd/MM/yyyy");
	}
	
	
	
	final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class,
			new QLEqualsFilter("name", "G001 - Tarefa Simples"));
	final WFProcess pmProcess = pm.startProcess(false, solicitante);
	final NeoObject wkfTarefaSimples = pmProcess.getEntity();
	final EntityWrapper ewWkfTarefaSimples= new EntityWrapper(wkfTarefaSimples);
	
	ewWkfTarefaSimples.findField("Solicitante").setValue(solicitante);
	ewWkfTarefaSimples.findField("Executor").setValue(executor);
	ewWkfTarefaSimples.findField("Titulo").setValue(titulo);
	ewWkfTarefaSimples.findField("DescricaoSolicitacao").setValue(descricao);
	ewWkfTarefaSimples.findField("Prazo").setValue(prazo);
	ewWkfTarefaSimples.findField("origem").setValue(origem);
	ewWkfTarefaSimples.findField("dataSolicitacao").setValue(new GregorianCalendar());
	
	pmProcess.setRequester(solicitante);
	pmProcess.setSaved(true);
	
	
	// Trata a primeira tarefa
	Task task = null;
	final List acts = pmProcess.getOpenActivities();
	final Activity activity1 = (Activity) acts.get(0);
	if (activity1 instanceof UserActivity)
	{
		try
		{
			if (((UserActivity) activity1).getTaskList().size() <= 0)
			{
				task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, solicitante, new HashSet());
				if (isAvanca) {
					task.finish();
				}
			} else
			{
				task = ((UserActivity) activity1).getTaskList().get(0);
				if (isAvanca) {
					task.finish();
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			out.print("Erro #4 - Erro ao avan�ar a primeira tarefa");
			return;
		}
	}
	
	out.print(pmProcess.getCode());
	
	**/
%>