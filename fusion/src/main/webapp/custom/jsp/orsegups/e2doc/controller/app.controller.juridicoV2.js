app.controller('appControllerjuridicoV2', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.documentos = [];
    $scope.documentosDisplay = [];
    $scope.tipoProcessoSelecionado = {};
    
    $scope.tipoProcessoSelecionado;
    $scope.tipoProcesso;
    $scope.numeroProcesso;
    $scope.autor;
    $scope.reu;
    $scope.matricula;
    $scope.nossasEmpresasAutoras;
    $scope.cliente;
    $scope.empresa;
    $scope.filial;
    $scope.unidadeFederativa;
    
    $scope.divTrabalhista=false;
    $scope.divCivel=false;
    $scope.divTributario=false;
    
    $scope.itemsTipoProcesso = [{
    	  id: 1,
    	  label: 'Trabalhista'
    	}, {
    	  id: 2,
    	  label: 'Civel'
    	}, {
    	  id: 3,
    	  label: 'Tributario'
    	}];
    
    $scope.selected = {};
    
    $scope.selecionaTipoProcesso = function() {
    	
    	if ($scope.tipoProcessoSelecionado.id == 1) {
    		$scope.divTrabalhista=true;
    	    $scope.divCivel=false;
    	    $scope.divTributario=false;
    	    $scope.tipoProcesso='Trabalhista';
    	} else if ($scope.tipoProcessoSelecionado.id == 2) {
    		$scope.divTrabalhista=false;
    	    $scope.divCivel=true;
    	    $scope.divTributario=false;
    	    $scope.tipoProcesso='Civel';
    	} else if ($scope.tipoProcessoSelecionado.id == 3) {
    		$scope.divTrabalhista=false;
    	    $scope.divCivel=false;
    	    $scope.divTributario=true;
    	    $scope.tipoProcesso='Tributario';
    	} else {
    		$scope.divTrabalhista=false;
    	    $scope.divCivel=false;
    	    $scope.divTributario=false;
    	    $scope.tipoProcesso='';
    	}    	
    }
        
    if(!$scope.tipoProcessoSelecionado){
    	$scope.tipoProcessoSelecionado = '';
	}
    if(!$scope.numeroProcesso){
    	$scope.numeroProcesso = '';
	}
    if(!$scope.autor){
    	$scope.autor = '';
	}
    if(!$scope.reu){
    	$scope.reu = '';
	}
    if(!$scope.matricula){
    	$scope.matricula = '';
	}
    if(!$scope.nossasEmpresasAutoras){
    	$scope.nossasEmpresasAutoras = '';
	}
    if(!$scope.cliente){
    	$scope.cliente = '';
	}
    if(!$scope.empresa){
    	$scope.empresa = '';
	}
    if(!$scope.filial){
    	$scope.filial = '';
	}
    if(!$scope.unidadeFederativa){
    	$scope.unidadeFederativa = '';
	}
	
	$scope.pesquisar = function(){
		
		var link = "https://intranet.orsegups.com.br/fusion/services/documentosJuridicoV2/";
		
		if ($scope.tipoProcessoSelecionado.id == 1) {
    		link = link + "pesquisaJuridicoTrabalhista;tipoProcesso="+$scope.tipoProcesso+";numeroProcesso="+$scope.numeroProcesso+";autor="+$scope.autor+";reu="+$scope.reu+";matricula="+$scope.matricula;
    	} else if ($scope.tipoProcessoSelecionado.id == 2) {
    		link = link + "pesquisaJuridicoCivel;tipoProcesso="+$scope.tipoProcesso+";numeroProcesso="+$scope.numeroProcesso+";nossasEmpresasAutoras="+$scope.nossasEmpresasAutoras+";cliente="+$scope.cliente+";empresa="+$scope.empresa+";filial="+$scope.filial;
    	} else if ($scope.tipoProcessoSelecionado.id == 3) {
    		link = link + "pesquisaJuridicoTributario;tipoProcesso="+$scope.tipoProcesso+";numeroProcesso="+$scope.numeroProcesso+";nossasEmpresasAutoras="+$scope.nossasEmpresasAutoras+";unidadeFederativa="+$scope.unidadeFederativa+";empresa="+$scope.empresa+";filial="+$scope.filial;
    	} else {
    		alert('Selecione o Tipo de Processo');
    		return;
    	}  
				
		$http.post(link).success(function(data, status) {
			console.log(data);
			if (data){
				$.each(data.pasta, function(key, value){
					data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
				});
			}else{
				$('#warning-alert').show();
				setTimeout(function(){
					$('#warning-alert').hide();
				}, 5000);
			}
			$scope.documentos = data.pasta;
			$scope.documentosView = data.pasta;
			
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	}
	
	
	
	function getYears(){
		var d = new Date();
		var ano = d.getFullYear();
		var yearList = [];
		
		for (i= ano; i>=1990; i--){
			yearList.push(ano);
			ano--;
		}
		
		return yearList;
	}
	
}]);
