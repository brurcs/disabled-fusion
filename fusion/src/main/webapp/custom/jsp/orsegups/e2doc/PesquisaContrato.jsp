<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>

<script type="text/javascript" src="js/custom.js"></script>
<link rel="stylesheet" href="css/custom-style.css">

<title>Pesquisa Contrato</title>
</head>
<body>
	<br>
	
	<div class="alert alert-warning collapse" id="warning-alert" role="alert">
  		<strong>Aten��o!</strong> Nenhum resultado encontrado para os filtros indicados.
	</div>
	
	<div class="alert alert-success collapse" id="success-alert" role="alert">
  		<strong>Sucesso!</strong> Dados localizados.
	</div>
	
	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
                    <div class="panel-heading">
                             <h3>Pesquisa de Documentos Comercial Privado</h3>
                             <span data-toggle="tooltip" title="Ajuda" class="span-ajuda pull-right label label-default">
                             	<i data-toggle="modal" data-target="#modalAjuda" class="ajuda glyphicon glyphicon-question-sign"></i>
                           	 </span>                          
                    </div>
                    	<div class="panel-body">
							<form class="form-horizontal" id="formPesquisa">
								<fieldset>							
									<!-- Search input-->
									<div class="form-group col-md-12 col-sm-12">
										<label class="col-md-1 control-label" for="cgcCpf">CPF/CNPJ</label>
										<div class="col-md-5">
											<input id="cgcCpf" name="cgcCpf" type="number"
												placeholder="CPF/CNPJ" class="form-control input-md">
										</div>
										
										
										<label class="col-md-1 control-label" for="nomeCliente">Nome
											Cliente</label>
										<div class="col-md-5">
											<input id="nomeCliente" name="nomeCliente" type="text"
												placeholder="Nome Cliente" class="form-control input-md">
			
										</div>
									</div>
			
			
			
									<!-- Search input-->
									<div class="form-group  col-md-12 col-sm-12">
										<label class="col-md-1 control-label" for="contrato">Contrato</label>
										<div class="col-md-5">
											<input id="contrato" name="contrato" type="number"
												placeholder="Contrato" class="form-control input-md">
			
										</div>
										<label class="col-md-1 control-label" for="servico">Tipo
											Servi�o</label>
										<div class="col-md-5">
											<input id="servico" name="servico" type="text"
												placeholder="Tipo Servi�o" class="form-control input-md">
										</div>
									</div>
			
									<!-- Search input-->
									<div class="form-group  col-md-12 col-sm-12">
										<label class="col-md-1 control-label" for="status">Status</label>
										<div class="col-md-5">
											<select id="status" name="status" class="form-control input-md">
												<option></option>
												<option>ativo</option>
												<option>inativo</option>
											</select>
			
										</div>
									</div>
			
									<!-- Button -->
									<div class="form-group">
										<div class="col-md-12">
											<input type="button" onclick="doSearch()" id="pesquisar" name="pesquisar" data-toggle="modal" data-target="#processing-modal"
												class="btn btn-primary pull-left btn-pesquisar" value="Pesquisar">
										</div>
									</div>
			
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">		
				<div class="panel panel-default">
					<div class="panel-heading">
                       Resultados 
                    </div>
                    <div class="panel-body">
						<div class="table-responsive">
							<table id="tabela-resultado" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>CPF/CNPJ</th>
										<th>Nome do Cliente</th>
										<th>Numero do Contrato</th>
										<th>Tipo Servi�o</th>
										<th>Documento</th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
			</div>
		
		</div>
	</div>
	
	  <!-- Modal -->
	  <div class="modal fade" id="modalAjuda" role="dialog">
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Ajuda</h4>
	        </div>
	        <div class="modal-body">
	          <p>
	          	<strong>CPF/CNPJ:</strong> Informar CPF/CNPJ do cliente sem pontos e tra�os. <br>
	          	<strong>Nome Cliente:</strong> Informar nome do cliente (raz�o social). Permite busca parcial.<br>
	          	<strong>Contrato:</strong> Informar n�mero do contrato sem pontos e tra�os. <br>
	          	<strong>Tipo servi�o:</strong> Informar tipo de servi�o do contrato. Permite busca parcial<br>
	          </p>
	          <p>
	          	A pesquisa pode ser feita utilizando-se um ou mais filtros.<br> Exemplo: � poss�vel pesquisar tanto pelo CPF/CNPJ quanto pelo CPF/CNPJ mais o n�mero do contrato.
	          </p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        </div>
	      </div>
	      
	    </div>
	  </div>
	  
	  	<div class="modal modal-static fade" id="processing-modal" name="processing-modal" role="dialog" aria-hidden="true" >
	    <div class="modal-dialog"  >
	        <div class="modal-content"   >
	            <div class="modal-body"  >
	                <div class="text-center"   >	               
						<img src="img/loading.gif" style="height: 80px;" class="icon" />
             			<h4>Processando... </h4>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	  
	  
	  
			
</body>
</html>