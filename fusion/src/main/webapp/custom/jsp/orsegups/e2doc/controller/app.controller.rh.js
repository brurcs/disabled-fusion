app.controller('appontrollerrh', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.documentos = [];
	$scope.documentosView = [];
	
	$scope.tiposDocumentos = [];
	$scope.tipoDocumentoSel = '';
	$scope.empresas = [];
	$scope.empresaSel = '';
	$scope.competencia;
	
	
	$scope.listaDoc = [];
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentosRH/getTiposDocumentos').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.tiposDocumentos = data;
		}else{
			alert('Não foi possível obter os tipos de documentos!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentosRH/getEmpresas').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.empresas = data;
		}else{
			alert('Não foi possível obter as empresas!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	
	$scope.pesquisar = function(){
		
		var comp = '';
		
		if ($scope.competencia){
			var mes = $scope.competencia.getMonth()+1;
			
			if (mes < 10){
				mes = '0'+mes;
			}
			
			comp = encodeURIComponent(mes+'/'+$scope.competencia.getFullYear());
		}
		
		if(!$scope.tipoDocumentoSel){
			$scope.tipoDocumentoSel = '';
		}
		
		if(!$scope.empresaSel){
			$scope.empresaSel = '';
		}
			
		$http.post('https://intranet.orsegups.com.br/fusion/services/documentosRH/pesquisaRH;tiposDocumentos='+$scope.tipoDocumentoSel+';empresa='+$scope.empresaSel+';competencia='+comp).success(function(data, status) {
			console.log(data);
			if (data){
				$.each(data.pasta, function(key, value){
					data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
				});
			}else{
				$('#warning-alert').show();
				setTimeout(function(){
					$('#warning-alert').hide();
				}, 8*1000);
			}
			$scope.documentos = data.pasta;
			$scope.documentosView = data.pasta;
			
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	}
	
	$scope.showDocs = function(documentos) {
		$scope.listaDoc = documentos;	
	}
		
}]);
