app.controller('appontrollerComercialPublico', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.documentos = [];
	$scope.documentosView = [];
	
	$scope.status = [];
	$scope.statusSel = '';
	$scope.empresas = [];
	$scope.empresaSel = '';
	$scope.tipoDocumentos = [];
	$scope.tipoDocumentoSel = '';
	$scope.cgcCpf = '';
	$scope.nomeCliente = '';
	$scope.contrato = '';
	$scope.ano = '';
	
	
	$scope.listaDoc = [];
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentosComercialPublico/getStatus').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.status = data;
		}else{
			alert('Não foi possível obter os tipos de documentos!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentosComercialPublico/getEmpresas').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.empresas = data;
		}else{
			alert('Não foi possível obter as empresas!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentosComercialPublico/getTipoDocumento').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.tipoDocumentos = data;
		}else{
			alert('Não foi possível obter os Tipos de Documento!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	
	$scope.pesquisar = function(){
		
		var comp = '';
		
		if (!$scope.cgcCpf){
			$scope.cgcCpf = '';
		}
		
		if(!$scope.statusSel){
			$scope.statusSel = '';
		}
		
		if(!$scope.empresaSel){
			$scope.empresaSel = '';
		}
		
		if(!$scope.tipoDocumentoSel){
			$scope.tipoDocumentoSel = '';
		}
		
		if (!$scope.nomeCliente){
			$scope.nomeCliente = '';
		}
			
		if (!$scope.contrato){
			$scope.contrato = '';
		}
		
		if (!$scope.ano){
			$scope.ano = '';
		}
		
		$http.post('https://intranet.orsegups.com.br/fusion/services/documentosComercialPublico/pesquisaComercialPublico;status='+$scope.statusSel+';empresa='+$scope.empresaSel+';cgcCpf='+$scope.cgcCpf+';nomeCliente='+$scope.nomeCliente+';contrato='+$scope.contrato+';ano='+$scope.ano+';tipoDocumento='+$scope.tipoDocumentoSel).success(function(data, status) {
			console.log(data);
			if (data){
				$.each(data.pasta, function(key, value){
					data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
				});
			}else{
				$('#warning-alert').show();
				setTimeout(function(){
					$('#warning-alert').hide();
				}, 8*1000);
			}
			$scope.documentos = data.pasta;
			$scope.documentosView = data.pasta;
			
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	}
	
	$scope.showDocs = function(documentos) {
		$scope.listaDoc = documentos;	
	}
		
}]);
