app.controller('appontrollerfrota', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.documentos = [];
	$scope.documentosView = [];
	
	$scope.tiposDocumentos = [];
	$scope.tipoDocumentoSel = '';
	$scope.placa;
	$scope.ano;
	
	
	$scope.listaDoc = [];
	
	$http.post('https://intranet.orsegups.com.br/fusion/services/documentosFrota/getTiposDocumentos').success(function(data, status) {
		console.log(data);
		if (data){
			$scope.tiposDocumentos = data;
		}else{
			alert('Não foi possível obter os tipos de documentos!')
		}
		
		$('#processing-modal').modal('hide');
	}).error(function() {
		alert("Não foi possível carregar os dados");
	});
	
	
	$scope.pesquisar = function(){
		
		var comp = '';
		
		if(!$scope.tipoDocumentoSel){
			$scope.tipoDocumentoSel = '';
		}
		
		if(!$scope.placa){
			$scope.placa = '';
		}
		
		if(!$scope.ano){
			$scope.ano = '';
		}
			
		$http.post('https://intranet.orsegups.com.br/fusion/services/documentosFrota/pesquisaFrota;tiposDocumentos='+$scope.tipoDocumentoSel+';placa='+$scope.placa+';ano='+$scope.ano).success(function(data, status) {
			console.log(data);
			if (data){
				$.each(data.pasta, function(key, value){
					data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
				});
			}else{
				$('#warning-alert').show();
				setTimeout(function(){
					$('#warning-alert').hide();
				}, 8*1000);
			}
			$scope.documentos = data.pasta;
			$scope.documentosView = data.pasta;
			
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	}
	
	$scope.showDocs = function(documentos) {
		$scope.listaDoc = documentos;	
	}
		
}]);
