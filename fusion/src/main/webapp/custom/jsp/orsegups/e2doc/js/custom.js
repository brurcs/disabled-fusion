var tabelaIniciada = false;

function doSearch(){
		var cgcCpf = $('#cgcCpf').val();
		var nomeCliente = $('#nomeCliente').val();
		var contrato = $('#contrato').val().trim();
		var servico = $('#servico').val();
		var status = $('#status').val();
		
//		if (nomeCliente.length > 0 ){
//			nomeCliente = '%'+nomeCliente+'%';
//		}
//		
//		if (servico.length > 0 ){
//			servico = '%'+servico+'%';
//		}
		
				
       if (tabelaIniciada) {
    	   $('#tabela-resultado').dataTable().fnClearTable();
    	   $('#tabela-resultado').dataTable().fnDestroy();    	   
       }
		
		$.ajax({ 
		    type: 'POST', 
		    url: 'http://localhost:8080/fusion/services/documentos/pesquisarContrato;cgcCpf='+cgcCpf+';nomeCliente='+nomeCliente+';contrato='+contrato+';servico='+servico+';status='+status, 
		    dataType: 'json',
		    success: function (pastas) { 
		    	
		    	if (pastas != null){
		    		
		    		createTable(pastas);

		    	}else{
		    		
		    		cgcCpf = Number(cgcCpf);
		    		
		    		$.ajax({ 
		    		    type: 'POST', 
		    		    url: 'http://localhost:8080/fusion/services/documentos/pesquisarContrato;cgcCpf='+cgcCpf+';nomeCliente='+nomeCliente+';contrato='+contrato+';servico='+servico, 
		    		    dataType: 'json',
		    		    success: function (pastas) { 
		    		    	
		    		    	if (pastas != null){
		    		    		createTable(pastas);
		    		    	}else{
		    		    		$('#processing-modal').modal('hide');
		    		    		$('#warning-alert').show();
		    	    			setTimeout(function(){
		    	    				$('#warning-alert').hide();
		    	    			}, 5000);	    		    		
		    		    	}	    		    			             
		    		    }
		    		});		    		
		    	}		    			             
		    }
		});
		
		
};

function createTable(pastas){
	
	console.log(pastas);
    
	var listaPastas = pastas.pasta;
       
       $("#tabela-resultado tbody").empty();
       
       for (i=0; i< listaPastas.length; i++){
    	   for (j=0; j< listaPastas[i].doc.length; j++){
	    	   var tr = "<tr>"
	    	   var td = "<td>"+listaPastas[i].indices.i0+"</td>";
	    	   td +="<td>"+listaPastas[i].indices.i1+"</td>";
	    	   td += "<td>"+listaPastas[i].indices.i2+"</td>";
	    	   td += "<td>"+listaPastas[i].indices.i3+"</td>";
	    	   td += "<td>"+listaPastas[i].indices.i4+" <a href=https://intranet.orsegups.com.br/fusion/services/documentos/downloadImagem2/"+encodeURIComponent(listaPastas[i].doc[j].id)+">";
	    	   td += "<span class='glyphicon glyphicon-save-file' aria-hidden='true'</a></td>";
	    	   tr += td + "</td>";
	    	   $('#tabela-resultado').append(tr);
    	   }
    	     	   
       }
       
	    $('#tabela-resultado').DataTable( {
	        "language": {
	            "sEmptyTable": "Nenhum registro encontrado",
	            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
	            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
	            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
	            "sInfoPostFix": "",
	            "sInfoThousands": ".",
	            "sLengthMenu": "_MENU_ resultados por página",
	            "sLoadingRecords": "Carregando...",
	            "sProcessing": "Processando...",
	            "sZeroRecords": "Nenhum registro encontrado",
	            "sSearch": "Pesquisar",
	            "oPaginate": {
	                "sNext": "Próximo",
	                "sPrevious": "Anterior",
	                "sFirst": "Primeiro",
	                "sLast": "Último"
	            },
	            "oAria": {
	                "sSortAscending": ": Ordenar colunas de forma ascendente",
	                "sSortDescending": ": Ordenar colunas de forma descendente"
	            }
	        }
	    } );
    	
	    tabelaIniciada = true;
	    pastas = null;
	    
	    $('#processing-modal').modal('hide');
	    
//		$('#success-alert').show();
//		setTimeout(function(){
//			$('#success-alert').hide();
//		}, 5000);
	
	
}


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();    
});