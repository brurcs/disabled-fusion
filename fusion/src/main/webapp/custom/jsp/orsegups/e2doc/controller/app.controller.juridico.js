app.controller('appontrollerjuridico', ['$scope','$http','$timeout','$interval', function($scope, $http, $timeout, $interval){
    
	$scope.documentos = [];
	$scope.documentosView = [];
	
	$scope.numeroProcesso;
	$scope.autor;
	$scope.matricula;
	$scope.reu
	
	$scope.pesquisar = function(){
		
		var comp = '';
		
		if(!$scope.numeroProcesso){
			$scope.numeroProcesso = '';
		}
		
		if(!$scope.autor){
			$scope.autor = '';
		}
			
		if(!$scope.matricula){
			$scope.matricula = '';
		}
		
		if(!$scope.reu){
			$scope.reu = '';
		}
		
		$http.post('http://localhost:8080/fusion/services/documentosJuridico/pesquisaJuridico;numeroProcesso='+$scope.numeroProcesso+';autor='+$scope.autor+';matricula='+$scope.matricula+';reu='+$scope.reu).success(function(data, status) {
			console.log(data);
			if (data){
				$.each(data.pasta, function(key, value){
					data.pasta[key].doc.id = encodeURIComponent(value.doc.id);
				});
			}else{
				$('#warning-alert').show();
				setTimeout(function(){
					$('#warning-alert').hide();
				}, 8*1000);
			}
			$scope.documentos = data.pasta;
			$scope.documentosView = data.pasta;
			
			$('#processing-modal').modal('hide');
		}).error(function() {
			alert("Não foi possível carregar os dados");
		});
	}
		
}]);
