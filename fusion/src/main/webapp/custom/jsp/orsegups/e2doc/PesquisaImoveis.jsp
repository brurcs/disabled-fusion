<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>

<script type="text/javascript" src="js/imoveis/custom-imoveis.js"></script>
<script type="text/javascript" src="js/imoveis/jquery.treegrid.js"></script>

<link rel="stylesheet" href="css/custom-style.css">
<link rel="stylesheet" href="css/imoveis/jquery.treegrid.css">

<title>Pesquisa Im�veis</title>
</head>
<body>
	<br>
	
	<div class="alert alert-warning collapse" id="warning-alert" role="alert">
  		<strong>Aten��o!</strong> Nenhum resultado encontrado para os filtros indicados.
	</div>
	
	<div class="alert alert-success collapse" id="success-alert" role="alert">
  		<strong>Sucesso!</strong> Dados localizados.
	</div>
	
	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
                    <div class="panel-heading">
                             <h3>Tela Pesquisa - Im�veis (Pr�prios)</h3>
                             <span data-toggle="tooltip" title="Ajuda" class="span-ajuda pull-right label label-default">
                             	<i data-toggle="modal" data-target="#modalAjuda" class="ajuda glyphicon glyphicon-question-sign"></i>
                           	 </span>                          
                    </div>
                    	<div class="panel-body">
							<form class="form-horizontal" id="formPesquisa">
								<fieldset>							
									<!-- Search input-->
									<div class="form-group col-md-12 col-sm-12">
										<label class="col-md-1 control-label" for="numeroMatricula">N�mero Matr�cula</label>
										<div class="col-md-5">
											<input id="numeroMatricula" name="numeroMatricula" type="text"
												placeholder="N�mero da Matr�cula" class="form-control input-md"
												onkeypress="if ((window.event ? event.keyCode : event.which) == 13) { doSearch(); }">
										</div>
										
										
										<label class="col-md-1 control-label" for="nomeImovel">Nome do Im�vel</label>
										<div class="col-md-5">
											<input id="nomeImovel" name="nomeImovel" type="text"
												placeholder="Nome do Im�vel" class="form-control input-md"
												onkeypress="if ((window.event ? event.keyCode : event.which) == 13) { doSearch(); }">
			
										</div>
									</div>
			
			
			
									<!-- Search input-->
									<div class="form-group  col-md-12 col-sm-12">
										<label class="col-md-1 control-label" for="municipio">Munic�pio</label>
										<div class="col-md-5">
											<select id="municipio" name="municipio" class="form-control input-md">
												<option></option>
											</select>
			
										</div>
										<label class="col-md-1 control-label" for="tipoImovel">Tipo Im�vel</label>
										<div class="col-md-5">
											<select id="tipoImovel" name="tipoImovel" class="form-control input-md">
												<option></option>											
											</select>
										</div>
									</div>
									
									
									<!-- Search input-->
									<div class="form-group col-md-12 col-sm-12">
										<label class="col-md-1 control-label" for="propRegistroImoveis">Propriet�rio Registro Im�veis</label>
										<div class="col-md-5">
											<input id="propRegistroImoveis" name="propRegistroImoveis" type="text"
												placeholder="Propriet�rio no Registro de Im�veis" class="form-control input-md"
												onkeypress="if ((window.event ? event.keyCode : event.which) == 13) { doSearch(); }">
										</div>
										
										
										<label class="col-md-1 control-label" for="propFato">Propriet�rio de Fato</label>
										<div class="col-md-5">
											<input id="propFato" name="propFato" type="text"
												placeholder="Propriet�rio de Fato" class="form-control input-md"
												onkeypress="if ((window.event ? event.keyCode : event.which) == 13) { doSearch(); }">
			
										</div>
									</div>
			
									
			
									<!-- Button -->
									<div class="form-group">
										<div class="col-md-12">
											<input type="button" onclick="doSearch()" id="pesquisar" name="pesquisar" data-toggle="modal" data-target="#processing-modal"
												class="btn btn-primary pull-left btn-pesquisar" value="Pesquisar">
										</div>
									</div>
			
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">		
				<div class="panel panel-default">
					<div class="panel-heading">
                       Resultados 
                    </div>
                    <div class="panel-body">
						<div class="table-responsive">
				        	<table class="table tree" id="tabela-resultado">
				        	<thead>
								<tr>
									<th>Matr�cula / Nome do Im�vel</th>
									<th>Pasta</th>
									<th>Propriet�rio (RI)</th>
									<th>Propriet�rio de Fato</th>
									<th>Ano</th>
									<th>Documento</th>
								</tr>
				        	</thead>
       
            				</table>	
						</div>
					</div>
				</div>
				
			</div>
		
		</div>
	</div>
	
	  <!-- Modal -->
	  <div class="modal fade" id="modalAjuda" role="dialog">
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Ajuda</h4>
	        </div>
	        <div class="modal-body">
	          <p>
	          	<strong>N�mero Matr�cula:</strong> Informar N�mero da Matr�cula sem pontos e tra�os. <br>
	          	<strong>Nome do Im�vel:</strong> Informar Nome do Im�vel. Permite busca parcial.<br>
	          	<strong>Munic�pio:</strong> Escolher um dos munic�pios da lista. Tr�s os munic�pios que possuem um documento vinculado. <br>
	          	<strong>Tipo Im�vel:</strong> Escolher um Tipo de Im�vel da lista. Tr�s os Tipo de Im�veis que possuem um documento vinculado. <br>
	          	<strong>Propriet�rio Registro Im�veis:</strong> Informar Propriet�rio no Registro Im�veis. Permite busca parcial.<br>
	          	<strong>Propriet�rio de Fato:</strong> Informar Propriet�rio de Fato. Permite busca parcial.<br>
	          </p>
	          <p>
	          	A pesquisa pode ser feita utilizando-se um ou mais filtros.<br> 
	          </p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	        </div>
	      </div>
	      
	    </div>
	  </div>
	  
	  	<div class="modal modal-static fade" id="processing-modal" name="processing-modal" role="dialog" aria-hidden="true" >
	    <div class="modal-dialog"  >
	        <div class="modal-content"   >
	            <div class="modal-body"  >
	                <div class="text-center"   >	               
						<img src="img/loading.gif" style="height: 80px;" class="icon" />
             			<h4>Processando... </h4>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	  
	  
	  
			
</body>
</html>