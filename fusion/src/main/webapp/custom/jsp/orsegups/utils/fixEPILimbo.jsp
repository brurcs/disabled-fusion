<%@page import="com.neomind.fusion.workflow.Task.FinishBy"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Corrigir Atividade 19121885 que ficou no limbo.</title>
</head>
<body>
	Corrigir Atividade 19121885 que ficou no limbo.
	<br><br>
	<%
		Activity activity = (Activity) PersistEngine.getNeoObject(Activity.class, 19121885);
	
		if(activity != null){
			out.print("Activity: "+activity.getActivityName());
			
			activity.finish(FinishBy.SYSMANAGER);
		}
	%>
</body>
</html>