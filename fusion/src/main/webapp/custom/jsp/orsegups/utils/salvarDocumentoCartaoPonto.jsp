<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.site.vo.SiteClienteVO"%>
<%@page import="java.nio.channels.FileChannel"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="java.net.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
	String empresax = request.getParameter("empresa");
	String cptCom = request.getParameter("cpt");
	String cgccpfStr = request.getParameter("cgccpf");
	String tipDocStr = "FOPAG";
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	List<SiteClienteVO> lista = null;

	try
	{

		conn = PersistEngine.getConnection("SAPIENS");

		StringBuffer sql = new StringBuffer();
		sql.append(" select ctr.usu_codemp, ctr.usu_codfil, cli.CgcCpf ");
		sql.append(" from usu_t160ctr ctr "); 
		sql.append(" inner join e085cli cli on ctr.usu_codcli = cli.codcli ");
		sql.append(" inner join usu_t160doc doc on doc.USU_NumCtr = ctr.usu_numctr and doc.usu_codemp = ctr.usu_codemp and doc.USU_CodFil = ctr.usu_codfil and doc.USU_CodDoc in (1,16) ");
		sql.append(" where 1=1 and ctr.usu_codfil <> 0 "); 
		sql.append(" group by ctr.usu_codemp, ctr.usu_codfil, cli.CgcCpf ");

		st = conn.prepareStatement(sql.toString());
		rs = st.executeQuery();
		lista = new ArrayList<SiteClienteVO>();
		SiteClienteVO clienteVO = null;

		while (rs.next())
		{
			clienteVO = new SiteClienteVO();
			clienteVO.setEmpresa(rs.getString("USU_CODEMP"));
			clienteVO.setFilial(rs.getString("USU_CODFIL"));
			clienteVO.setCodigoCliente(rs.getString("CGCCPF"));
			lista.add(clienteVO);
		}

	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		OrsegupsUtils.closeConnection(conn, st, rs);
	}
	String pasta = null;
	String path = "\\\\Fsoofs01\\f$\\Site\\Orsegups\\CartaPontoTemp";

	if (path != null)
	{
		File folder = new File(path);

		if (!folder.exists())
		{
			System.out.print("N�o existe arquivo! 1");
		}

		if (folder.isDirectory())
		{
			DecimalFormat df = new DecimalFormat("00");

			//String empresaStr = df.format(Long.valueOf(empresax));

			File diretorio = null;
			File diretorioBKP = null;
			final File[] arquivos = folder.listFiles();
			
			int contador = 0;
			for (File arquivo : arquivos)
			{
				if (arquivo.isFile() && !arquivo.isHidden())
				{
					for (SiteClienteVO vo : lista)
					{
						if (arquivo.getName().contains(vo.getCodigoCliente()) && arquivo.getName().contains(vo.getEmpresa().concat("_")))
						{
							String timeMills = String.valueOf(new GregorianCalendar().getTimeInMillis());
							String caminhoStr = df.format(Long.valueOf(vo.getEmpresa())) + "\\" + vo.getFilial() + "\\" + vo.getCodigoCliente() + "\\PONTO\\" + cptCom;
							diretorio = new File("\\\\Fsoofs01\\f$\\Site\\Orsegups\\" + caminhoStr);
							diretorioBKP = new File("\\\\Fsoofs01\\f$\\Site\\Orsegups\\CartaPontoTemp30Dias\\");
							File arquivoNovo = new File(diretorio, "PONTO".concat("_" + vo.getCodigoCliente()).concat("_" + cptCom).concat(".pdf"));
							File arquivoBKP = new File(diretorioBKP, arquivo.getName().replace(" ","_"));
							//System.out.println("xFOPAGx " + diretorioBKP.getAbsolutePath() + "\\"+arquivo.getName().replace(" ","_"));
							
							File source = arquivo;
							File destination = diretorio;
							
							/*File xf = new File("\\\\Fsoofs01\\f$\\Site\\Orsegups\\Temp30Dias\\");
							f.mkdirs();*/

							//  ver o caso 15/1/11096279000175
							//intranet.orsegups.com.br/fusion/custom/jsp/orsegups/utils/salvarDocumentoArquivo2?cpt=092016
							if (!destination.exists())
							{
								destination.mkdirs();
							}
							if (!arquivoNovo.exists())
							{
								arquivoNovo.createNewFile();
							}
							if (!diretorioBKP.exists())
							{
								diretorioBKP.mkdirs();
							}
							if (!arquivoBKP.exists()){
								System.out.println("xPONTOx " + arquivoBKP.getAbsolutePath());
								arquivoBKP.createNewFile();
							}

							FileChannel sourceChannel = null;
							FileChannel destinationChannel = null;
							FileChannel backupChannel = null;

							try
							{
								sourceChannel = new FileInputStream(source).getChannel();
								destinationChannel = new FileOutputStream(arquivoNovo).getChannel();
								backupChannel = new FileOutputStream(arquivoBKP).getChannel(); // bkp de 30 dias
								sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
								sourceChannel.transferTo(0, sourceChannel.size(), backupChannel);
								
								//documentoWrapper.findField("siteParametrosUploadCliente.caminhoArquivos").setValue(arquivoNovo.getAbsolutePath());
							}
							catch (Exception e)
							{
								e.printStackTrace();
								System.out.print("N�o existe arquivo!!");
							}
							finally
							{
								if (sourceChannel != null && sourceChannel.isOpen())
									sourceChannel.close();
								if (destinationChannel != null && destinationChannel.isOpen())
									destinationChannel.close();
								if (backupChannel != null && backupChannel.isOpen())
									backupChannel.close();
								
								//PersistEngine.persist(documentos);
								

							} 
						}

					}
				}
			}
			
			if (arquivos != null)
			{
				Thread t = new Thread(new Runnable()
				{
					
					@Override
					public void run()
					{
						for (File file : arquivos)
							{
								file.delete();
								System.out.println("deletar arquivo: " + file.getAbsolutePath());
							}
					}
				});
				t.start();
					
			}
			
		}
		else
		{
			System.out.print("N�o existe arquivo! 2 ");
		}
	}
	else
	{
		System.out.print("N�o existe arquivo! 3 ");
	}
%>

