<%@page import="java.util.StringTokenizer"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>


<%
	try {
		NeoObject corObj = PersistEngine.getObject(AdapterUtils.getEntityClass("OTSModeloRastreador"), new QLEqualsFilter("nomeModelo", "Z�nite TK100"));
		System.out.println("entrou");
		ArrayList<String> vtrs = new ArrayList<String>();
		
		/*vtrs.add("283;13040401600458");
		System.out.println("entrou2");
		vtrs.add("284;13040401600456");
		vtrs.add("285;13040401600406");
		vtrs.add("286;13040401600452");
		vtrs.add("287;13040401600408");
		vtrs.add("288;13040401600455");
		vtrs.add("289;13040401600457");
		vtrs.add("290;13040401600451");
		vtrs.add("291;13040401600442");
		vtrs.add("292;13040401600415");*/
		vtrs.add("293;13011002100143");
		vtrs.add("294;13011002100131");
		vtrs.add("295;13011002100133");
		vtrs.add("297;13011001500171");
		vtrs.add("298;13011002100099");
		vtrs.add("299;13031001800013");
		vtrs.add("300;13011002100144");
		vtrs.add("301;13011002100117");
		vtrs.add("302;13011001900018");
		vtrs.add("303;12031001800076");
		vtrs.add("304;12031001800088");
		vtrs.add("305;12031001800091");
		/*vtrs.add("306;13030401600372");
		vtrs.add("307;13040401600412");
		vtrs.add("308;13040401600432");
		vtrs.add("309;13040401600423");
		vtrs.add("310;13040401600426");
		vtrs.add("311;13030401600379");
		vtrs.add("312;13030401600378");
		vtrs.add("313;13030401600377");
		vtrs.add("314;13040401600421");
		vtrs.add("315;13040401600424");
		vtrs.add("316;13030401600348");
		vtrs.add("317;13040401600420");
		vtrs.add("318;13040401600419");
		vtrs.add("319;13040401600416");
		vtrs.add("320;13040401600414");
		vtrs.add("321;13030401600376");
		vtrs.add("322;13040401600409");
		vtrs.add("323;13040401600425");
		vtrs.add("324;13040401600418");
		vtrs.add("325;13040401600411");
		vtrs.add("326;13040401600433");
		vtrs.add("327;13040401600440");
		vtrs.add("328;13040401600447");
		vtrs.add("329;13040401600407");
		vtrs.add("330;13040401600444");
		vtrs.add("331;13040401600430");
		vtrs.add("332;13040401600453");
		vtrs.add("333;13040401600441");
		vtrs.add("334;13040401600437");
		vtrs.add("335;13040401600413");
		vtrs.add("336;13040401600405");
		vtrs.add("337;13040401600431");
		vtrs.add("338;13040401600449");
		vtrs.add("339;13040401600446");
		vtrs.add("340;13040401600434");
		vtrs.add("341;13040401600438");
		vtrs.add("342;13040401600435");
		vtrs.add("343;13040401600445");
		vtrs.add("344;13040401600436");
		vtrs.add("345;13040401600428");*/
		
		
		InstantiableEntityInfo vtrInfo = AdapterUtils.getInstantiableEntityInfo("OTSRastreador");	
		for (String vtr : vtrs) {
			StringTokenizer st = new StringTokenizer(vtr, ";");
			String id = st.nextToken();
			/*String modelo = st.nextToken();*/
			String serial = st.nextToken();
			 
			NeoObject objVtr = vtrInfo.createNewInstance();
			EntityWrapper wrpVtr = new EntityWrapper(objVtr);
			wrpVtr.findField("id").setValue(id);
			wrpVtr.findField("modeloEquipamento").setValue(corObj);
			wrpVtr.findField("serial").setValue(serial);
			System.out.println("entrou3");
		
			PersistEngine.persist(objVtr);
			out.print(objVtr);
		}
    }  catch (Exception e) {
		e.printStackTrace();
		// erro desconhecido
		out.print("Erro: " + e.getMessage());
		return;    
    }
%>
