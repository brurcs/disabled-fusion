<!DOCTYPE html>
<%@page import="com.neomind.fusion.custom.orsegups.placa.vo.MunicipioVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.placa.OrsegupsPlacaXpl1"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<html>
	<head>
	<%@page import="com.neomind.fusion.portal.PortalUtil"%>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="css/estilos.css">
 	<link rel="stylesheet" type="text/css"
		href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
	<title>Placas XPL1</title>
	<script src="js/jquery.growl.js" type="text/javascript"></script>
		<link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
	<% 
	List<MunicipioVO> retorno = new ArrayList<MunicipioVO>();
	retorno = OrsegupsPlacaXpl1.municipios();

	%>
	<section id=main>
		<div class="container-fluid" align="center">
			<div class="page-header">
				<h1>
					Gera��o de Eventos - XPL1 Placas
				</h1>
			</div>
			<div class="row" align="center" width="98%">
				<div class="col-md-12">
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1primary" data-toggle="tab">Informa��es Gerais</a></li>
								<li><a href="#tab2primary" data-toggle="tab">Cadastro de Eventos</a></li>
								<li><a href="#tab3primary" data-toggle="tab">Relat�rio</a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
										<div class="info">
											<h3>Observa��es:</h3>
											<ul>
											    Ser� gerados eventos quando:
                                                <ul>
                                                	<li>  Empresas estiverem ativas;</li>
                                                	<li>  Eventos XPL1;</li>
                                                	<li>  Eventos n�o atendidos;</li>
                                                </ul>                                                
                                                <b>Classes utilizadas:</b>
                                                <ul>
                                                	<li>OCPAbreEventoSIGMAAPI</li>
                                                	<li>ServletPlaca</li>
                                                </ul>
                             					<br>
											</ul>
										</div>
								</div>
								<div class="tab-pane fade" id="tab2primary">
									<!-- E-form Exce��o Cliente -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<div id="dlgCon" class="tab-pane fade in active" style="text-align: left;"
												 closed="false" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:false">
                                                 <div class="ftitle"><font size=4><b>Preencha as informa��es a seguir para o cadastro de eventos</b></font></font></div><br>
                                                 <form id="forcom" method="post" novalidate>
                                                  		<div>
	                                                 		<label>Munic�pio</label> <select
	                                         				id="municipios2" name="municipios2" class="form-control campoformulario" required="required" style="width: 100px;font-size:11px;">
	                                         					<% for(MunicipioVO obj : retorno){%>
	                                         					<option value="<%= obj.getCdMunicipio() %>"><%= obj.getNmMunicipio() %></option>
	                                         					<%} %>
	                                         				</select><br>
	                                         			</div>
                                                  		<div><br>
                                                  			<label >Quatidade</label>
                                                  			<input type="text" class="form-control campoformulario"  id="quantidade" required="required" name="quantidade" value="100"><br>
                                                  		</div>                                               	
	                                         		</div>
	                                         	</form>
                                                 <div style="text-align: left;">
                                                 <a href="javascript:void(0)" class="btn btn-primary campoBotao"  iconCls="icon-print"style="width: 100px;font-size:11px;" onclick="javascript:gerarEventos()">Gerar Eventos</a>
                                               </div>
                                             </div>
                                          </div> 								
								<div class="tab-pane fade" id="tab3primary">
									<!-- E-form Exce��o Cliente -->
									<div class="embed-responsive embed-responsive-16by9">
	  									<div id="dlgCon" class="tab-pane fade in active" style="text-align: left;"
												 closed="false" buttons="dlg-buttons-Con" data-options="iconCls:'icon-print',modal:false">
                                                 <div class="ftitle"><font size=4><b>Preencha as informa��es a seguir para gerar o relat�rio</b></font></font></div><br>
                                                 <form id="forcom" method="post" novalidate>
                                                  		<div>
	                                                 		<label>Munic�pio</label> <select
	                                         				id="municipios" name="municipios" class="form-control campoformulario" required="required" style="width: 100px;font-size:11px;">
	                                         					<% for(MunicipioVO obj : retorno){%>
	                                         					<option value="<%= obj.getCdMunicipio() %>"><%= obj.getNmMunicipio() %></option>
	                                         					<%} %>
	                                         				</select><br>
	                                         			</div>
                                                  		<div><br>
                                                  			<label >De: </label>
                                                  			<input type="date" class="form-control campoformulario"  id="dataInicio" required="required" name="dataInicio">
                                                  		</div>	
                                                  		<div><br>
                                                  			<label > �: </label>
                                                  			<input type="date" class="form-control campoformulario"  id="dataFim" required="required" name="dataFim"><br>
                                                  		</div>	                                                 	
	                                         		</div>
	                                         	</form>
                                                 <div style="text-align: left;">
                                                 <a href="javascript:void(0)" class="btn btn-primary campoBotao"  iconCls="icon-print"style="width: 100px;font-size:11px;" onclick="javascript:gerarRelatorioPlacas()">Imprimir</a>
                                               </div>
                                             </div>
                                          </div>                                 
                                          
                                             
	  									<script type="text/javascript">
                                               
	  										   var url;
                                               
                                               function gerarRelatorioPlacas(){
                                            	   
	                                            	var msg = 'Campos obrigat�rios:'; 	            
	                                            	var cdMunicipio = jQuery('#municipios option:selected').val();
	                                            	var municipio = jQuery('#municipios option:selected').text();
			                                       	if(cdMunicipio == '0') {
			                                       		msg = msg + '\n\n- Munic�pio';
			                                       	}	                                               	
			                                       	var dataInicio = jQuery('#dataInicio').val();
			                                       	if(dataInicio == '') {
			                                       		msg = msg + '\n- Data In�cio';
			                                       	}			                                       	
			                                       	var dataFim = jQuery('#dataFim').val();			                                       	
			                                       	if(dataFim == '') {
			                                       		msg = msg + '\n- Data Fim';
			                                       	}
			                                       	if (msg == 'Campos obrigat�rios:') {
			                                       		var servlet = '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=relatorioXpl1Regional&dataInicio='+dataInicio+'&dataFim='+dataFim+'&cdMunicipio='+cdMunicipio+'&municipio='+municipio;			                                       	
		    	                                       	location.href=servlet; 
			                                       	} else {
			                                       		$.growl.warning({title:"Aviso", message: msg });
			                                       	}                                            
                                               } 
                                               
                                               function gerarEventos() {
                                            	   var msg = 'Campos obrigat�rios:'; 	
                                            	   var msgConfirma = 'O valor ultrapassa 100. Ser�o gerados 100 eventos. Deseja gerar?';
                                            	   var cdMunicipio = jQuery('#municipios2 option:selected').val();
                                            	   var municipio = document.getElementById("municipios2").value;
                                            	   var quantidade = document.getElementById("quantidade").value;
                                            	   
                                            	   
                                            	   if(cdMunicipio == '0') {
			                                       		msg = msg + '\n\n- Munic�pio';
			                                       }
                                            	   if(quantidade == '') {
			                                       		msg = msg + '\n- Quantidade';
			                                       	}
                                            	   
                                            	   if (msg == 'Campos obrigat�rios:') {
                                            		   
                                            		   if (quantidade <= 100) {
                                            			   msgConfirma = 'Ser�o gerados '+quantidade+' eventos. Deseja gerar?';
                                            		   } else {
                                            			   quantidade = 100;
                                            		   }    
                                            		   if (confirm(msgConfirma))
	                                                   {
                                            			   var data = 'action=gerarEventos&cdMunicipio='+cdMunicipio+'&municipio='+municipio+'&quantidade='+quantidade;
		                                            	   jQuery.ajax({
		     	                                            	method: 'post',
		     	                                                url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.placa.servlet.ServletPlaca',
		     	                                                dataType: 'json',	
		     	                                                data: data,
		     	                                                success: function(result){
		     	                                                	document.getElementById("quantidade").value = '100';
		     	                                                   	document.getElementById("municipios2").value = '0';
		     	                                                	$.growl({ title: "", message: result.mensagemSucesso });
		    	                                                }
		   	                                            	});
	                                                   }
	                                                   
                                            		} else {
			                                       		$.growl.warning({title:"Aviso", message: msg });
			                                       	}
   										 	   }
                                                                                             
                                               function radio(valor){
                                            	   document.getElementById("result").value = valor;
                                               }
                                         </script>
	  								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<style>
	.campoformulario{
		max-width: 300px;
		min-width: 300px;
	}
	.campoBotao{
		max-width: 300px;
		min-width: 300px;
		background-color:#6b7486;
	    
	}
	.tipoOk { background-image:url(imagens/certo2.png);   }
	.tipoFalta { background-image:url(imagens/exclamacao2.png); }
	</style>	
</body>
</html>