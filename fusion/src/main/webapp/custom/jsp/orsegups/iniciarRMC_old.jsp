<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="java.io.File"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.StringTokenizer"%>

<body><%

	long regional = NeoUtils.safeLong(request.getParameter("regional"));
	String departamento = request.getParameter("departamento");
	String solicitante = request.getParameter("solicitante");
	String arquivo = request.getParameter("arquivo");
	String nomeCliente = request.getParameter("nomeCliente");
	String numeroContrato = request.getParameter("numeroContrato");
	long codigoEmpresa = NeoUtils.safeLong(request.getParameter("codigoEmpresa"));
	long diaBase = NeoUtils.safeLong(request.getParameter("diaBase"));
	long codigoFilial = NeoUtils.safeLong(request.getParameter("codigoFilial"));
	String numerosPostos = request.getParameter("numerosPostos");
 
	String diretorio = "\\\\lagoa\\rmc\\";
	
	NeoUser uSolic = (NeoUser) PersistEngine.getObject(SecurityEntity.class, new QLEqualsFilter("code", solicitante));
	if(uSolic == null)
	{
		out.print("Erro #1 - Usu�rio Solicitante n�o encontrado (code #"+solicitante+")");
		return;
	}
	
	InstantiableEntityInfo infoRegional = AdapterUtils.getInstantiableEntityInfo("GCEscritorioRegional");
	InstantiableEntityInfo infoEmpresa = AdapterUtils.getInstantiableEntityInfo("GCEmpresa");
	InstantiableEntityInfo infoDepartamento = AdapterUtils.getInstantiableEntityInfo("RMCDepartamento");
	InstantiableEntityInfo infoResponsaveis = AdapterUtils.getInstantiableEntityInfo("RMCResponsaveisRecebimento");
	
	NeoObject eRegional = (NeoObject) PersistEngine.getObject(infoRegional.getEntityClass(), new QLEqualsFilter("codigo", regional));
	if(eRegional == null )
	{
		out.print("Erro #2 - Regional n�o encontrada (c�digo #"+regional+")");
		return;
	}
	
	NeoObject eEmpresa = (NeoObject) PersistEngine.getObject(infoEmpresa.getEntityClass(), new QLEqualsFilter("codigo", codigoEmpresa));
	if(eEmpresa == null )
	{
		out.print("Erro #9 - Empresa n�o encontrada (c�digo #"+codigoEmpresa+")");
		return;
	}
	
	if((diaBase <= 0) || (diaBase > 31))
	{
		out.print("Erro #0 - Dia Base inv�lido (#"+diaBase+")");
		return;
	}
	
	NeoObject eDepartamento = (NeoObject) PersistEngine.getObject(infoDepartamento.getEntityClass(), new QLEqualsFilter("sigla", departamento));
	if(eDepartamento == null)
	{
		out.print("Erro #3 - Departamento n�o encontrado (sigla #"+departamento+")");
		return;
	}
	
	QLGroupFilter gp = new QLGroupFilter("AND");
	gp.addFilter(new QLEqualsFilter("regional",eRegional));
	gp.addFilter(new QLEqualsFilter("departamento",eDepartamento));
	NeoObject eResponsaveis = (NeoObject) PersistEngine.getObject(infoResponsaveis.getEntityClass(), gp);
	if(eResponsaveis == null)
	{
		out.print("Erro #4 - Lista de respos�veis para a regional e departamento passados, n�o foi encontrada");
		return;
	}
	
	EntityWrapper ewResp = new EntityWrapper(eResponsaveis);
	Collection<NeoPaper> listaPapeisResp = (Collection<NeoPaper>) ewResp.findField("listaDestinatarios").getValues();
	if(listaPapeisResp == null || listaPapeisResp.size() <= 0) {
		out.print("Erro #5 - Lista de destinat�rios n�o existe ou est� vazia para Regional: " + regional + " e Departamento: " + departamento + ".");
		return;
	}
	
	// Cassiano 07/12/2011 - caso responsavel seja Direcao segue fluxo de aprovacao
	Boolean aprovacao = false;
	String codigoDepartamento = (String) ewResp.findValue("departamento.sigla");
	if (codigoDepartamento != null && codigoDepartamento.equalsIgnoreCase("DDIR"))
	{
		aprovacao = true;
	}
	
	Set<NeoUser> listaResp = new HashSet<NeoUser>();
	for(NeoPaper papel : listaPapeisResp)
	{
		listaResp.addAll(papel.getAllUsers());
	}
	
	QLEqualsFilter equal = new QLEqualsFilter("Name", "C005 - Distribui��o de RMC");
	ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
	if (pm == null)
	{
		out.print("Erro #8 - Modelo de processo n�o encontrado");
		return;
	}
	
	for(NeoUser user : listaResp)
	{
		if(user.isActive())
		{
			//File file = new File("S:\\RMC\\"+arquivo);
			File file = new File(diretorio+arquivo);
			NeoFile newFile = new NeoFile();
			newFile.setName(file.getName());
			newFile.setStorage(NeoStorage.getDefault());
			PersistEngine.persist(newFile);
			OutputStream outi = newFile.getOutputStream();
			InputStream in = null;
			
			try
			{
				in = new BufferedInputStream(new FileInputStream(file));
				NeoStorage.copy(in, outi);
				newFile.setOrigin(NeoFile.Origin.TEMPLATE);
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
				out.print("Erro #6 - Relat�rio RMC n�o encontrado #"+diretorio+arquivo);
				return;
			}

			NeoObject wkfDistribuicaoRMC = AdapterUtils.getInstantiableEntityInfo("RelatorioMovimentacaoContrato").createNewInstance();
			EntityWrapper ewWkfDistribuicaoRMC = new EntityWrapper(wkfDistribuicaoRMC);

			String anexos = request.getParameter("anexos");
			if((anexos != null) && (anexos != "")) {
				StringTokenizer st = new StringTokenizer(anexos, ",");
				String diretorioAnexo = "\\\\lagoa\\arqcvs\\";
				List<NeoObject> listaAnexos = new ArrayList();
				InstantiableEntityInfo rmcAnexos = AdapterUtils.getInstantiableEntityInfo("RMCAnexos");
				while (st.hasMoreTokens())
				{
					String token = st.nextToken();
					File anexo = new File(diretorioAnexo + token);
					NeoFile newAnexo = new NeoFile();
					NeoObject objAnexos = rmcAnexos.createNewInstance();
					EntityWrapper wAnexos = new EntityWrapper(objAnexos);
					newAnexo.setName(anexo.getName());
					newAnexo.setStorage(NeoStorage.getDefault());
					PersistEngine.persist(newAnexo);
					PersistEngine.persist(objAnexos);
					OutputStream outAnexo = newAnexo.getOutputStream();
					InputStream inAnexo = null;
					inAnexo = new BufferedInputStream(new FileInputStream(anexo));
					NeoStorage.copy(inAnexo, outAnexo);
					newAnexo.setOrigin(NeoFile.Origin.TEMPLATE);
					wAnexos.findField("anexos").setValue(newAnexo);
					listaAnexos.add(objAnexos);
				}
				ewWkfDistribuicaoRMC.findField("anexos").setValue(listaAnexos);
			}

			ewWkfDistribuicaoRMC.findField("arquivo").setValue(newFile);
			ewWkfDistribuicaoRMC.findField("solicitante").setValue(uSolic);
			ewWkfDistribuicaoRMC.findField("empresa").setValue(eEmpresa);
			ewWkfDistribuicaoRMC.findField("nomeCliente").setValue(nomeCliente);
			ewWkfDistribuicaoRMC.findField("numeroContrato").setValue(numeroContrato);
			ewWkfDistribuicaoRMC.findField("diaBase").setValue(diaBase);
			ewWkfDistribuicaoRMC.findField("gerente").setValue(user);
			ewWkfDistribuicaoRMC.findField("aprovacao").setValue(aprovacao);
			ewWkfDistribuicaoRMC.findField("filial").setValue(codigoFilial);
			ewWkfDistribuicaoRMC.findField("numerosPostos").setValue(numerosPostos);
			PersistEngine.persist(wkfDistribuicaoRMC);
			
			WFProcess pmProcess = pm.startProcess(wkfDistribuicaoRMC, true, null, null, null, null, uSolic);
			pmProcess.setRequester(uSolic);
			pmProcess.setSaved(true);
			
			//luizao, todos que tem acts.get tem que ter esse codigo antes !!!!
			PersistEngine.persist(pmProcess);
			PersistEngine.commit(true);
			
			// Finaliza a primeira tarefa
			Task task = null;
			final List acts = pmProcess.getOpenActivities();
			final Activity activity1 = (Activity) acts.get(0);
			if (activity1 instanceof UserActivity)
			{
				try
				{
					if (((UserActivity) activity1).getTaskList().size() <= 0)
					{
						task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, uSolic, new HashSet());
						task.finish();
					} 
					else
					{
						task = ((UserActivity) activity1).getTaskList().get(0);
						task.finish();
					}
				}catch(Exception e)
				{
					System.out.println("Erro #7 - Erro ao iniciar a atividade para o respons�vel");
					e.printStackTrace();
					out.print("Erro #7 - Erro ao iniciar a atividade para o respons�vel");
					return;
				}
			}
		}
	}
	
	out.print("OK");

%>
</body>

