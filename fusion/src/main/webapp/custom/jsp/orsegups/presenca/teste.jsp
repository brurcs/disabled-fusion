<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.presenca.vo.ClienteVO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>


<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>

<%
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.presenca.teste");

	Long codesc = Long.valueOf(request.getParameter("codesc"));
	Long codtma = Long.valueOf(request.getParameter("codtma"));
	GregorianCalendar dataAtual = new GregorianCalendar();

	List<EscalaVO> escalas = QLPresencaUtils.getEscalasColaboradores(dataAtual,codesc,codtma);

	for (EscalaVO e : escalas)
	{
		log.warn("ESCALA: " + e);
	}
%>