<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaSupervisaoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.SaldoHorasVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.site.vo.HorasVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.site.SiteUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.text.ParseException"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.AcessoVO"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page import="java.io.OutputStream"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   

<%
	NeoUser user = PortalUtil.getCurrentUser();

	NeoPaper papelQlDiretoria = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Quadro de Lotação Presença - Diretoria"));
	NeoPaper papelOperadorQlApuracao = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Operador do Quadro de Lotação Apuração Ponto"));
	NeoPaper papelQlContracheque = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Quadro de Lotação Presença - Contracheque"));
	NeoPaper papelGedConsultaDocumentosRh = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","gedConsultaDocumentosRh"));
	NeoPaper papelFichaDepartamento = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Ficha Básica - Departamento"));
	NeoPaper papelPresencaSupervisao = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","presencaSupervisao"));
	NeoPaper papelPresencaURA = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","URA"));
	NeoPaper papelObsHoristas = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelRemoveObsHoristas"));
	
	Boolean isQlDiretoria = Boolean.FALSE;
	Boolean isOperadorQlApuracao = Boolean.FALSE;
	Boolean isQlContracheque = Boolean.FALSE;
	Boolean isGerente = Boolean.FALSE;
	Boolean isCoordenador = Boolean.FALSE;
	Boolean isGedConsultaDocumentosRh = Boolean.FALSE;
	Boolean isFichaDepartamento = Boolean.FALSE;
	Boolean isPresencaSupervisao = Boolean.FALSE;
	Boolean isURA = Boolean.FALSE;
	Boolean isObsHorista = Boolean.FALSE;
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelQlDiretoria)) {
		isQlDiretoria = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelOperadorQlApuracao)) {
		isOperadorQlApuracao = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelQlContracheque)) {
		isQlContracheque = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelGedConsultaDocumentosRh)) {
		isGedConsultaDocumentosRh = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelFichaDepartamento)) {
		isFichaDepartamento = Boolean.TRUE;
	}
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelPresencaSupervisao)) {
		isPresencaSupervisao = Boolean.TRUE;
	}
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelPresencaURA)) {
		isURA = Boolean.TRUE;
	}
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelObsHoristas)) {
		isObsHorista = Boolean.TRUE;
	}
	
	
	if(user != null && user.getPapers() != null) {
		for (NeoPaper paper : user.getPapers())
		{
			if (paper.getCode().toUpperCase().contains("GERENTE"))
			{
				isGerente = Boolean.TRUE;
			}
			if (paper.getCode().toUpperCase().contains("COORDENADOR"))
			{
				isCoordenador = Boolean.TRUE;
			}
		}
	}
	
	GregorianCalendar dataAtual = new GregorianCalendar(); 

	String numemp = request.getParameter("numemp");
	String tipcol = request.getParameter("tipcol");
	String numcad = request.getParameter("numcad");
	String dep = request.getParameter("dep");
	String codccu = request.getParameter("codccu");
	Long numempL = Long.parseLong(numemp);
	Long tipcolL = Long.parseLong(tipcol);
	Long numcadL = Long.parseLong(numcad);
	
	ColaboradorVO colaborador = new ColaboradorVO();
	colaborador = QLPresencaUtils.buscaFichaColaborador(numempL, tipcolL, numcadL, dataAtual);
	
	ColaboradorVO supervisor = new ColaboradorVO();
	String caminhoFotoSupervisor = "";
	String sexoS = "";
	String cpfS = ""; 
	
	supervisor = QLPresencaSupervisaoUtils.retornaSupervisorPosto(numempL, numcadL, tipcolL, Long.parseLong(codccu));
	if(supervisor != null){
		supervisor = QLPresencaUtils.buscaFichaColaborador(supervisor.getNumeroEmpresa(), supervisor.getTipoColaborador(), supervisor.getNumeroCadastro(), dataAtual);
		caminhoFotoSupervisor = "custom/jsp/orsegups/utils/imageFromDB.jsp?numEmp=" + supervisor.getNumeroEmpresa() + "&tipCol=" + supervisor.getTipoColaborador() + "&numCad=" + supervisor.getNumeroCadastro();
		if (supervisor.getSexo().equals("M"))
		{
			sexoS = "Masculino";
		}
		else
		{
			sexoS = "Feminino";
		}
		cpfS = String.format ("%011d", supervisor.getCpf()); 
		try{
			cpfS = OrsegupsUtils.formatarString(cpfS, "###.###.###-##");
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
	}	
	
	boolean isDiretor = colaborador.getCargo().contains("DIRETOR");
	
	String cpf = String.format ("%011d", colaborador.getCpf()); 
	String sexo = "";
	String comp = "";
	
	try{
		cpf = OrsegupsUtils.formatarString(cpf, "###.###.###-##");
	}
	catch (ParseException e)
	{
		e.printStackTrace();
	}
	
	if (colaborador.getSexo().equals("M"))
	{
		sexo = "Masculino";
	}
	else
	{
		sexo = "Feminino";
	}
	
	String caminhoFoto = "custom/jsp/orsegups/utils/imageFromDB.jsp?numEmp=" + numemp + "&tipCol=" + tipcol + "&numCad=" + numcad;

%>

<html>
<body>
	<script type="text/javascript">
		var numcad = <%= colaborador.getNumeroCadastro() %>
		var numemp = <%= colaborador.getNumeroEmpresa() %>
		var tipcol = <%= colaborador.getTipoColaborador() %>
		var numcpf = <%= colaborador.getCpf() %>
	</script>
    <div id="tab" class="easyui-tabs" style="width: auto;height:630px" data-options="tabHeight:55">
        <div title="<span class='tt-inner'><img src='imagens/custom/ficha_dados_gerais.png'/><br>Gerais</span>" style="padding:10px; width: auto">
			<div style="border: 2px solid #95b8e7; width: auto; height: auto">    
				<div style="float: right;">
					<img src="<%=caminhoFoto%>" width="140px" height="180px" style="border-left: 2px solid #95b8e7;border-bottom: 2px solid #95b8e7;"></img>
				</div>    
				<table style="width: 75%;">
					<tbody>
						<tr>
							<td class="flabel" class="flabel">Nome:</td>
							<td class="fvalue"><%=colaborador.getNomeColaborador()%></td>
						</tr>
						<tr>
							<td class="flabel">Empresa/Matrícula:</td>
							<td class="fvalue"><%=colaborador.getNumeroEmpresa() + "/" + colaborador.getNumeroCadastro()%></td>
						</tr>
						<tr>
							<td class="flabel">Sexo:</td>
							<td class="fvalue"><%=sexo%></td>
						</tr>
						<tr>
							<td class="flabel">Cargo:</td>
							<td class="fvalue"><%=colaborador.getCargo()%></td>
						</tr>
						<tr>
							<td class="flabel">Aso:</td>
							<td class="fvalue"><%="Ultimo Exame: " + colaborador.getUltExm() + ("31/12/1900".equals(colaborador.getProExm()) ? "" : ("  Próximo Exame: " + colaborador.getProExm()) ) %></td>
						</tr>
						<tr>
							<td class="flabel">Reciclagem:</td>
							<td class="fvalue"><%= "31/12/1900".equals(colaborador.getUsuDatfor()) ? "" : NeoUtils.safeOutputString(colaborador.getUsuDatfor()) %></td>
						</tr>
					<% if(!isDiretor || isQlDiretoria) { %>
							<tr>
								<td class="flabel">CPF:</td>
								<td class="fvalue"><%=cpf%></td>
							</tr>
							<tr>
								<td class="flabel">Nascimento:</td>
								<td class="fvalue"><%=NeoUtils.safeDateFormat(colaborador.getDataNascimento(),"dd/MM/yyyy")%></td>
							</tr>
							<tr>
								<td class="flabel">Admissão:</td>
								<td class="fvalue"><%=NeoUtils.safeDateFormat(colaborador.getDataAdmissao(),"dd/MM/yyyy")%></td>
							</tr>
							<tr>
								<td class="flabel">Avos de Férias:</td>
								<% if(colaborador.getAvosFerias() < 0) { %>
									<td class="fvalue">0</td>
								<%}else{%>
									<td class="fvalue"><%=colaborador.getAvosFerias()%></td>
								<%}%>
							</tr>
							<tr>
								<td class="flabel">Escala:</td>
								<td class="fvalue"><%=colaborador.getEscala().getDescricao()%></td>
							</tr>
							<tr>
								<td class="flabel">Situação:</td>
								<td class="fvalue"><%=colaborador.getSituacao()%></td>
							</tr>
							<% if(isFichaDepartamento) { %>
								<tr>
									<td class="flabel">Departamento:</td>
									<td class="fvalue"><%=colaborador.getDepartamento()%></td>
								</tr>
							<% } %>	
						<% if(colaborador.getMatrizResponsabilidade() != null && !colaborador.getMatrizResponsabilidade().equals("") &&
							  !colaborador.getMatrizResponsabilidade().equals(" ")) { %>	
							<tr>
								<td class="flabel">Responsabilidade:</td>
								<td class="fvalue"><%=colaborador.getMatrizResponsabilidade()%></td>
							</tr>
						<% } %>	
						
						<% if(colaborador.getFiscal() != null && !colaborador.getFiscal().equals("") &&
							  !colaborador.getFiscal().equals(" ")) { %>
							<tr>
								<td class="flabel">Responsável:</td>
								<td class="fvalue"><%=colaborador.getFiscal()%></td>
							</tr>
						<% } %>		
						
						<% if(colaborador.getEndereco() != null && !colaborador.getEndereco().equals("")) { %>	
							<tr>
								<td class="flabel">País:</td>
								<td class="fvalue"><%=colaborador.getEndereco().getPais()%></td>
							</tr>
							<tr>
								<td class="flabel">Estado:</td>
								<td class="fvalue"><%=colaborador.getEndereco().getUf()%></td>
							</tr>
							<tr>
								<td class="flabel">Endereço:</td>
								<td class="fvalue"><%=colaborador.getEndereco()%></td>
							</tr>
							<tr>
								<td class="flabel">Cidade:</td>
								<td class="fvalue"><%=colaborador.getEndereco().getCidade()%></td>
							</tr>
							<tr>
								<td class="flabel">Bairro:</td>
								<td class="fvalue"><%=colaborador.getEndereco().getBairro()%></td>
							</tr>
							<tr>
								<td class="flabel">CEP:</td>
								<td class="fvalue"><%=colaborador.getEndereco().getCep()%></td>
							</tr>
							
						<% } %>	
							
							<tr>
								<td class="flabel">Telefone(s):</td>
								<%
								if( colaborador.getTelefone() != null && !colaborador.getTelefone().equals(" ") && (colaborador.getTelefone2() != null && !colaborador.getTelefone2().equals(" ")))
								{
								%>
									<td class="fvalue"><%=colaborador.getTelefone()+" / "+colaborador.getTelefone2()%></td>
								<%
								}else{
								%>	
									<td class="fvalue"><%=colaborador.getTelefone() != null ? colaborador.getTelefone() : "Sem Telefone"%></td>
								<%}%>
							</tr>
							
						<% if(colaborador.getEnderecoPosto() != null && !colaborador.getEnderecoPosto().equals("")) { %>	
							<tr>
								<td class="flabel">Endereço Posto:</td>
								<td class="fvalue"><%= colaborador.getEnderecoPosto() %> </td>
							</tr>
							<tr>
								<td class="flabel"></td>
								<td class="fvalue"><%=colaborador.getEnderecoPosto().getBairro().trim().equals("")? "":colaborador.getEnderecoPosto().getBairro()%> 
									<%=colaborador.getEnderecoPosto().getCidade()%> 
									- <%=colaborador.getEnderecoPosto().getUf()%> 
									- <%=colaborador.getEnderecoPosto().getPais()%>
								</td>
							</tr>
							
							
						<% } %>	
					<% }%>	
					</tbody>
				</table>
			</div>	
		</div>
		<div
			title="<span class='tt-inner'><img src='imagens/custom/obsPresenca.png'/><br>Observações</span>"
			style="padding: 10px">
			<%-- <% if(isQlDiretoria || isGedConsultaDocumentosRh || isPresencaSupervisao ) { %> --%>
			<%
			    if (isObsHorista) {
			%>
			<div>
				<a href="javascript:void(0)" class="easyui-linkbutton"
					onclick="$('#add').dialog('open')">Adicionar Observação</a><br>
			</div>
			<br>
			<!-- window -->

			<div id="add" title="Adicionar Observação" class="easyui-dialog"
				data-options="modal:true,closed:true,iconCls:'icon-save'"
				style="width: 610px; height: 300px; padding: 10px;">
				<div id="tbAdd" style="padding: 3px">
					<textarea id="obsHorista" name="obsHorista" rows="4" cols="50"></textarea>
					<br>
					<br> <a href="javascript:void(0)" class="easyui-linkbutton"
						onclick="salvaObsHorista(numcad, numemp,tipcol,numcpf)">Adicionar
						Observação</a><br>
				</div>
			</div>
			<%
			    }
			%>
			<table id="dgObservacoes"
				title="OBSERVAÇÕES <%=colaborador.getNomeColaborador()%>"
				class="easyui-datagrid" rownumbers="true" singleSelect="true"
				nowrap="false">
				<thead>
					<tr>
						<th field="dataRegistro" width="100">Data</th>
						<th field="horRegistro" width="100">Hora</th>
						<th field="obsHorista" width="600">Obs:</th>
						<%
						    if (isObsHorista) {
						%>
						<th field="linkExcluir" width="80">Excluir</th>
						<%
						    }
						%>
					</tr>
				</thead>
			</table>
			<%-- <% } %> --%>
		</div>
		<%
		    if (!isDiretor || isQlDiretoria) {
		%>
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_afastamentos.png'/><br>Ausências</span>" style="padding:10px">
			
			<table id="dgAfastamento" title="AUSÊNCIAS DE <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="datafa" width="80">Inicio</th>
		           		<th field="datter" width="80">Fim</th>
		                <th field="dessit" width="200">Situação</th>
		                <th field="obsafa" width="340">Observação</th>
		                <th field="link" width="40">Tarefa</th>
		                <th field="excluir" width="40">Excluir</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_anotacoes.png'/><br>Anotações</span>" style="padding:10px">
			
			<table id="dgAnotacao" title="ANOTAÇÕES DE <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="datnot" width="80">Data</th>
		                <th field="tipnot" width="120">Tipo Anotação</th>
		                <th field="desnot" width="580">Descrição</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_coberturas.png'/><br>Coberturas</span>" style="padding:10px">
			
			<table id="dgCobertura" title="COBERTURAS DE <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		            	<th field="percob" width="100">Período</th>
		           		<th field="tipcob" width="150">Cobertura</th>
		           		<th field="nomcob" width="150">Colaborador</th>
		                <th field="loccob" width="180">Local</th>
		                <th field="obscob" width="160">Observação</th>
		                <th field="perretcob" width="100">Retorno Inversão</th>
		                <th field="excluir" width="40">Excluir</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_documentos.png'/><br>Documentos</span>" style="padding:10px">
		<% if(isQlDiretoria || isGedConsultaDocumentosRh || isPresencaSupervisao ) { %>
			<table id="dgDocumento" title="DOCUMENTOS DE <%=colaborador.getNomeColaborador() %>" class="easyui-treegrid"></table>
		<% } %>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_locais.png'/><br>Locais</span>" style="padding:10px">
			
			<table id="dgHistoricoLocal" title="LOCAIS DE <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="datalt" width="80">Data</th>
		           		<th field="posto" width="600">Posto</th>
		           		<th field="cc" width="100">Centro de Custos</th>
		           		<th field="Observacoes" width="100">Observações</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/evento_32x32.png'/><br>X8</span>" style="padding:10px">
			<table style="width:auto;">
				<tr>
			        		<td class="flabel" style="background-color: white; width: auto;">Listar até :</td>
								<td class="fvalue">
									<select id="cbListaAte" class="easyui-combobox" data-options="
						        		   		valueField:'competencia',
						        		   		textField:'competencia',
						        		   		editable:false,
						        		   		multiple:false,
								           		onSelect: function(record){
								           			var data = 'action=listaX8Colaborador&numemp=<%=colaborador.getNumeroEmpresa()%>&numcad=<%=colaborador.getNumeroCadastro()%>&numcpf=<%=colaborador.getCpf() %>'+ '&meses=' +record.competencia+'&evt='+x8Evt;
								           			startProgress();
										            $.ajax({
										            	method: 'post',
										                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
										                data: data,
										                dataType: 'json',
										                success: function(result){
										                	stopProgress();
											                if (result != '' && result.erro !== undefined ){
											                	alert(result.erro);
											                }else{
											                	x8Mes = record.competencia;
											                    $('#dgX8').datagrid('loadData',result);
											                    $('#dgX8').datagrid('load',result);
											                }
										                }

										            });
										        }">
										        
										        <option value="1" selected="selected" >1</option>
										        <option value="2" >2</option>
										        <option value="3" >3</option>
										        <option value="4" >4</option>
										        <option value="5" >5</option>
										        <option value="6" >6</option>
										        
										</select>
										Mês(es)
										<br/>	  
									</td>
								<td class="flabel" style="background-color: white; width: auto;">Eventos:</td>
								<td class="fvalue">
									<select id="cbEvtSel" class="easyui-combobox" data-options="
						        		   		valueField:'evtSel',
						        		   		textField:'evtSelVal',
						        		   		editable:false,
						        		   		multiple:false,
								           		onSelect: function(record){
								           			var data = 'action=listaX8Colaborador&numemp=<%=colaborador.getNumeroEmpresa()%>&numcad=<%=colaborador.getNumeroCadastro()%>&numcpf=<%=colaborador.getCpf() %>'+ '&meses=' +x8Mes+'&evt='+record.evtSel;
								           			startProgress();
										            $.ajax({
										            	method: 'post',
										                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
										                data: data,
										                dataType: 'json',
										                success: function(result){
										                	stopProgress();
											                if (result != '' && result.erro !== undefined ){
											                	alert(result.erro);
											                }else{
											                	x8Evt = record.evtSel;
											                    $('#dgX8').datagrid('loadData',result);
											                    $('#dgX8').datagrid('load',result);
											                }
										                }

										            });
										        }">
										        
										        <option value="0" selected="selected" >Apenas X8</option>
										        <option value="1" >Todos Tipos</option>
										</select>
										<br/>	  
									</td>
			        	</tr>
			</table>
			<table id="dgX8" title="Relatório de X8 - <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;" rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<!-- <th field="dtRecebido" width="80">Recebido</th> -->
		           		<th field="codigo" width="80">Cód. Evt.</th>
		           		<th field="dtViaturaDeslocamento" width="80">Deslocado</th>
		                <th field="dtFechamento" width="80">Fechado</th>
		                <th field="central" width="80">Conta</th>
		                <th field="razao" width="150">Local</th>
		                <th field="txObservacaoFechamento" width="300">Observação</th>
		            </tr>
		        </thead>
		    </table>
		</div>	
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_escalas.png'/><br>Escalas</span>" style="padding:10px">
		
			<table id="dgHistoricoEscala" title="ESCALAS DE <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="datalt" width="80">Data</th>
		           		<th field="codesc" width="80">Código</th>
		           		<th field="desesc" width="580">Descrição</th>
		           		<th field="excluir" width="40">Excluir</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_apuracoes.png'/><br>Apuração</span>" style="padding:10px">
			<% 
			if(isOperadorQlApuracao ) { 
			%>
			<div style="height: 30px">
        		<table>
					<tbody>
						<tr>
							<td class="flabel" style="background-color: white; width: auto;">Competência:</td>
							<td class="fvalue">
								<input id="cbCompetenciaApu" class="easyui-combobox" data-options="
					        		   		valueField:'competencia',
					        		   		textField:'competencia',
					        		   		editable:false,
					        		   		multiple:false,
					        		   		url:'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=listaCompetenciasApuracao&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>',
							           		onSelect: function(record){
							           			startProgress();
									            $.ajax({
									            	method: 'post',
									                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
									                data: 'action=listaApuracoes&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>&cpt='+record.competencia,
									                dataType: 'json',
									                success: function(result){
									                	stopProgress();
									                    $('#dgApuracoes').datagrid('loadData',result);
									                    $('#dgApuracoes').datagrid('load',result);
									                    $('#cptApuracao').html(competencia);
									                }
									            });
									        }">
							</td>
						</tr>
					</tbody>
				</table>		
			</div>
			
			<table id="dgApuracoes" title="APURAÇÕES DE <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" toolbar="#toolbarApuracao"
		           data-options="rowStyler: function(index,row){
                    				if (row.qdeExcecao > 0){
                        				return 'background-color:#fcdadc;font-weight:bold;';
                    				}
                				}">
		        <thead>
		            <tr>
		            	<th field="diaApuracao" width="60">Dia</th>
		           		<th field="dataApuracao" width="80">Data</th>
		           		<th field="ocorrenciaApuracao" width="580">Ocorrências</th>
		           		<th field="qdeExcecao" width="60">Exceções</th>
		            </tr>
		        </thead>
		    </table>
		    
		    <div id="toolbarApuracao">
			    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-clock_pencil" plain="true" onclick="acerto()">Acerto</a>
			    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-calculator" plain="true" onclick="recalculo()">Recalcular</a>
			    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-chart-pie" plain="true" onclick="situacao()">Situação</a>
			</div>
			
			<div id="dlgAcerto" class="easyui-window" title="Acerto" data-options="modal:true,closed:true,resizable:true,collapsible:false,minimizable:false,maximizable:true" style="width:700px;height:auto;">  
				<div style="padding: 10px">	
					<input id="datapu" name="datapu" type="hidden"></input>
				
					<div>
		        		<table>
							<tbody>
								<tr style="height: 25px">
									<td class="flabel" style="background-color: white; width: auto;">Escala:</td>
									<td class="fvalue" style="width: 500px">
										<input id="txtEscala" name="txtEscala" type="text" style="width: 500px"></input>
									</td>
								</tr>
								<tr>
									<td class="flabel" style="background-color: white; width: auto;">Horário:</td>
									<td class="fvalue" style="width: 500px">
										<input id="txtHorario" name="txtHorario" type="text" style="width: 500px"></input>
									</td>
								</tr>	
							</tbody>
						</table>		
					</div>	
				
					<table id="dgAcessoApuracao" title="ACESSOS" class="easyui-datagrid" style="width:auto;height:auto"
					       rownumbers="true" singleSelect="true" toolbar="#toolbarAcesso"
					       data-options="onClickRow: onClickRow,rowStyler: function(index,row){
								                    				if (row.operacao == 'I'){
								                        				return 'background-color:#e7ffce;font-weight:bold;';
								                    				} else if (row.operacao == 'E'){
								                    					return 'background-color:#fcdadc;font-weight:bold;';
								                    				}
								                				}">
					    <thead>
					        <tr>
					        	<th data-options="field:'direcao',width:80,height:24">Direção</th>
					       		<th data-options="field:'dataAcesso',width:80,height:24,editor:{type:'text'}">Data</th>
					       		<th data-options="field:'horaAcesso',width:40,height:24,editor:{type:'text'}">Hora</th>
					       		<th data-options="field:'codigoJustificativa',width:400,
			                        formatter:function(value,row){
			                            return row.codJma;
			                        },
			                        editor:{
			                            type:'combobox',
			                            options:{
			                                valueField:'codJma',
			                                textField:'desJma',
			                                panelHeight:'69', 
			                                editable:false,
			                                url:'<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/justificativa.json',
			                                required:true
			                            }
			                        }">Justificativa</th>
					        </tr>
					    </thead>
					</table>
					
					<div id="toolbarAcesso" style="height:auto">
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="novoAcesso()">Novo</a>
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="excluiAcesso()">Excluir</a>
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="salvaAcesso()">Salvar</a>
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true" onclick="cancelaAcesso()">Cancelar</a>
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-back',plain:true" onclick="moverMarcacao('anterior')">Dia Anterior</a>
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-next',plain:true" onclick="moverMarcacao('seguinte')">Dia Seguinte</a>
				    </div>
				    
					<br/>
					
					<table id="dgExcecaoApuracao" title="EXCEÇÕES" class="easyui-datagrid" style="width:auto;height:auto" rownumbers="true"
					       data-options="rowStyler: function(index,row){
					                    				if (row.situacaoExcecao == 'S'){
					                        				return 'background-color:#fcdadc;font-weight:bold;';
					                    				}
					                				}">
					    <thead>
					        <tr>
					       		<th field="horasSituacao" width="80">Quantidade</th>
					       		<th field="codigoSituacao" width="120">Situação</th>
					            <th field="descricaoSituacao" width="360">Descrição</th>
					            <th data-options="field:'linkIntrajornada',width:40,
			                        formatter:function(row){
			                            return row;
			                        }">Editar</th>
					        </tr>
					        </tr>
					    </thead>
					</table>
				</div>	
		    </div>
		    <% 
		    }
		    %>	
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_acessos.png'/><br>Acessos</span>" style="padding:10px">
        	<div>
        		<table>
					<tbody>
						<tr style="height: 30px">
							<td class="flabel" style="background-color: white; width: auto;">Competência:</td>
							<td class="fvalue">
								<input id="cbCompetencia" class="easyui-combobox" data-options="
					        		   		valueField:'competencia',
					        		   		textField:'competencia',
					        		   		editable:false,
					        		   		multiple:false,
					        		   		url:'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=listaCompetencias&numcpf=<%=colaborador.getCpf()%>',
							           		onSelect: function(record){
							           			var data = 'action=listaAcessos&numcpf=<%=colaborador.getCpf()%>'+ '&cpt=' +record.competencia;
			
										        if (document.getElementById('cbExcecao').checked) {
										            data = data + '&exibirExcecao=Sim';
										        } else {
										        	data = data + '&exibirExcecao=Nao';
										        }
							           			startProgress();
									            $.ajax({
									            	method: 'post',
									                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
									                data: data,
									                dataType: 'json',
									                success: function(result){
									                	stopProgress();
									                    $('#dgAcesso').datagrid('loadData',result);
									                    $('#dgAcesso').datagrid('load',result);
									                }
									            });
									        }">
								<input id="cpt" type="hidden" name="cpt">	  
							</td>
						</tr>
						<tr>
							<td class="flabel" style="background-color: white; width: auto;">Exibir Erros:</td>
							<td class="fvalue">
								<input id="cbExcecao" type="checkbox" onclick="exibirExcecao()">
							</td>
						</tr>
						<%    			GregorianCalendar dataCorte = new GregorianCalendar();
									  dataCorte.set(GregorianCalendar.MONDAY,05);
									  dataCorte.set(GregorianCalendar.DATE,01);
									  
								      GregorianCalendar gca =  new GregorianCalendar();
								      GregorianCalendar gcb =  new GregorianCalendar();
								      GregorianCalendar gcaFim =  new GregorianCalendar();
								      gcb.set(GregorianCalendar.DATE, 1);
									  gcb.add(GregorianCalendar.DATE, -1);
									  
									  gcaFim = (GregorianCalendar) gca.clone(); 
									  gcaFim.add(GregorianCalendar.MONTH, 1);
									  gcaFim.set(GregorianCalendar.DATE, 1);
									  gcaFim.add(GregorianCalendar.DATE, -1);
						%>
						<%
							Boolean isColAdm = SiteUtils.isAdm(NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), 1L);
							if (isColAdm){
						%>
<!-- 						<tr> -->
<%-- 							<td class="flabel" style="background-color: white; width: auto;">Saldo de HE Ref. <%=NeoUtils.safeDateFormat(gca,"MM/yyyy") %>:</td> --%>
<!-- 							<td class="fvalue"> -->
<!-- 								<span id="horas"> 
<%-- 								<%     --%>
// 									  HorasVO horas = new HorasVO();
// 									  HorasVO horasAnt = new HorasVO();
// 									  SaldoHorasVO saldos = new SaldoHorasVO();
									  
// 									  horasAnt = SiteUtils.retornaSaldoHoras(NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), 1L, gcb , 0, true );
// 									  horas = SiteUtils.retornaSaldoHoras(NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), 1L, gca , -1, false );
// 									  saldos = SiteUtils.calculaSaldoFinal(horasAnt, horas);
									  
// 									  HashMap<String,Long> saldoBH = SiteUtils.saldoBancoHorasAdm(NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), 1L);
<%-- 								   %> --%>
<%-- 									<%  --%>
									
// // 										OrsegupsUtils.getHorarioBanco(saldos.getSaldo())
// 										if (saldoBH != null && saldoBH.get("saldoPositivo") != 0){
<%-- 									%> --%>
<%-- 									<%=	    OrsegupsUtils.getHorarioBanco(saldoBH.get("saldoPositivo"))%> --%>
<%-- 									<% --%>
// 										}else{
<%-- 									%> --%>-->
<!-- 										000:00 
<%-- 									<% --%>
// 										}
<%-- 									%> --%>-->
<!-- 								</span> -->
<!-- 							</td> -->
<!-- 						</tr> -->
<!-- 						<tr> 
<%-- 							<td class="flabel" style="background-color: white; width: auto;">Saldo de HE Ref. <%=NeoUtils.safeDateFormat(gcb,"MM/yyyy") %>:</td> --%>
--><!-- 							<td class="fvalue"> -->
<!-- 								<span id="horas2"> 
<%-- 								 <% if(gca.get(GregorianCalendar.DAY_OF_MONTH) == 1){%> --%>
--><!-- 									<font size="3" color="red">Em Processamento...</font> 
<%-- 								<%}else if(gca.getInstance().after(dataCorte)){%> --%>
<%-- 								    <%  --%>
// // 								   		OrsegupsUtils.getHorarioBanco(horasAnt.getTotal())
// 								   		if (saldoBH != null && saldoBH.get("saldoAnterior") != 0){
<%-- 									%> --%>
<%-- 									<%=		OrsegupsUtils.getHorarioBanco(saldoBH.get("saldoAnterior")) %> --%>
<%-- 									<% --%>
// 										}else{
<%-- 									%>	     --%>
--><!-- 											000:00 
<%-- 									<%     --%>
// 										}
<%-- 								   	%> --%>
<%-- 								 <% }else{%> --%>
<%-- 								    <% if(saldoBH == null || saldoBH.get("saldoAnterior") == 0) {%> --%>
--><!-- 								    000:00  
<%-- 								    <% }else{ %> --%>
<%-- 								    <%  --%>
// // 								    	OrsegupsUtils.getHorarioBanco(horasAnt.getTotal()) 
// 								    	if (saldoBH != null && saldoBH.get("saldoAnterior") != 0){
<%-- 								   	%> --%>
<%-- 								   	<%=		OrsegupsUtils.getHorarioBanco(saldoBH.get("saldoAnterior"))%> --%>
<%-- 								   	<% --%>
// 										}else{
<%-- 									%>	     --%>
--><!-- 											000:00 
<%-- 									<%   --%>
// 										}
<%-- 								    %> --%>
<%-- 								    <% }%> --%>
<%-- 								  <% }%> --%>
--><!-- 								</span> -->
<!-- 							</td> -->
<!-- 						</tr> -->
<!-- 						<tr> 
<%-- 							<td class="flabel" style="background-color: white; width: auto;">Saldo a vencer (<%=NeoUtils.safeDateFormat(gcaFim,"dd/MM/yyyy") %>):</td> --%>
--><!-- 							<td class="fvalue"> -->
<!-- 								<span id="horas3"> 
--><%-- 								<% if(gca.get(GregorianCalendar.DAY_OF_MONTH) == 1){%> --%>
<!-- 									<font size="3" color="red">Em Processamento...</font> 
<%-- 								<%}else if(gca.getInstance().after(dataCorte)){%> --%>
<%-- 								    <%  --%>
// // 								   		OrsegupsUtils.getHorarioBanco(saldos.getSaldoVencer())
// 								   		if (saldoBH != null && saldoBH.get("saldoAVencer") != 0){
<%-- 								    %> --%>
<%-- 									<%=    	OrsegupsUtils.getHorarioBanco(saldoBH.get("saldoAVencer"))%> --%>
<%-- 									<% --%>
// 										}else{
<%-- 									%>	     --%>
--><!-- 											000:00 
<%-- 									<%     --%>
// 										}
<%-- 								   	%> --%>
<%-- 								 <% }else{%> --%>
<%-- <%-- 								    <% if(saldos.getSaldoVencer() == 0 ) {%> --%> --%>
--><!-- 								    000:00   
<%-- 								    <% }else{ %> --%>
<%-- 								    <%= OrsegupsUtils.getHorarioBanco(saldos.getSaldoVencer()) %> --%> 
<%-- 								    <% }%> --%>
								    
<%-- 								    <% if(saldoBH == null || saldoBH.get("saldoAVencer") == 0) {%> --%>
--><!-- 								    000:00  
<%-- 								    <% }else{ %> --%>
<%-- 								    <%  --%>
// // 								    	OrsegupsUtils.getHorarioBanco(saldos.getSaldoVencer())
// 								    	if (saldoBH != null && saldoBH.get("saldoAVencer") != 0){
<%-- 								    %> --%>
<%-- 								    <%=		OrsegupsUtils.getHorarioBanco(saldoBH.get("saldoAVencer"))%> --%>
<%-- 									<% --%>
// 										}else{
<%-- 									%>	     --%>
--><!-- 											000:00 
<%-- 									<%   --%>
// 										}
<%-- 								    %> --%>
<%-- 								    <% }%> --%>
<%-- 								  <% }%> --%>
									 
--><!-- 								</span> -->
<!-- 							</td> -->
<!-- 						</tr> -->
						<% 
							} else {
							    
							    HashMap<String,Long> saldoBHOperacional = SiteUtils.saldoBancoHorasOperacional(NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), 1L);
							    Long saldo = 0L;
							    if (saldoBHOperacional != null && saldoBHOperacional.get("saldo") != null){
									saldo = saldoBHOperacional.get("saldo");
							    }
						%>
<!-- 						<tr> 
--><%-- 							<td class="flabel" style="background-color: white; width: auto;">Saldo de HE Total em <%=NeoUtils.safeDateFormat(gca,"MM/yyyy") %>:</td> --%>
<!-- 							<td class="fvalue"> -->
<!-- 								<span id="horas"> 
--><%-- 									<%=	    OrsegupsUtils.getHorarioBanco(saldo)%> --%>
<!-- 								</span> -->
<!-- 							</td> -->
<!-- 						</tr> -->
						<%
							}
						%>
					</tbody>
				</table>		
			</div>
			
			<div>
				<table id="dgAcesso" title="ACESSOS DE <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
			           rownumbers="true" singleSelect="true"
			           data-options= "	rowStyler: function(index,row){
		                    				if (row.tipoAcesso != 'Acesso Via Presença'){
		                        				return 'background-color:#fcdadc;font-weight:bold;';
		                    				}
		                				}">
			        <thead>
			            <tr> 
			           		<th field="diaSemana" width="55">Semana</th>
			           		<th field="dataAnt" width="120">Data</th>
			                <th field="direcao" width="50">Direção</th>
			                <th field="relogio" width="50">Relógio</th>
			                <th field="fone" width="80">Telefone</th>
			                <th field="tipoAcesso" width="120">Tipo de Acesso</th>
			                <th field="justificativaAcesso" width="140">Justificativa</th>
			                <th field="obsAcesso" width="120">Obs</th>
			                <th field="excluir" width="40">Excluir</th>
			            </tr>
			        </thead>
			    </table>
		    </div>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_financas.png'/><br>Contracheques</span>" style="padding:10px">
		<% System.out.println("colaborador:"+colaborador.toString());
		if(isQlDiretoria || 
				(	
					(isQlContracheque && NeoUtils.safeIsNotNull(colaborador.getSistemaCargo()) && colaborador.getSistemaCargo().equals("Operacional")) || 
					(isOperadorQlApuracao && NeoUtils.safeIsNotNull(colaborador.getSistemaCargo()) && colaborador.getSistemaCargo().equals("Operacional")) || 
				 	(isGerente && NeoUtils.safeIsNotNull(colaborador.getSistemaCargo()) && colaborador.getSistemaCargo().equals("Operacional")) || 
				 	//(isCoordenador && NeoUtils.safeIsNotNull(colaborador.getSistemaCargo()) && colaborador.getSistemaCargo().equals("Operacional")) ||
				 	(isGerente && NeoUtils.safeIsNotNull(dep)) || 
				 	(isCoordenador && NeoUtils.safeIsNotNull(colaborador.getSistemaCargo()) && colaborador.getSistemaCargo().equals("Operacional")) ||
				 	(isCoordenador && NeoUtils.safeIsNotNull(dep) && NeoUtils.safeIsNotNull(colaborador.getCargo()) && !colaborador.getCargo().contains("GERENTE")) ||
				 	(isPresencaSupervisao && QLPresencaUtils.isUsuario(PortalUtil.getCurrentUser().getCode(),colaborador.getNumeroCadastro()) ) || 
				 	(		
				 			isPresencaSupervisao 
				 			&& NeoUtils.safeIsNotNull(dep) 
				 			&& NeoUtils.safeIsNotNull(colaborador.getCargo()) 
				 			&&  (!colaborador.getCargo().contains("GERENTE") && !colaborador.getCargo().contains("COORDENADOR"))
				 			&& colaborador.getSistemaCargo().equals("Operacional")
		 			) 
				)
			) 
		{ %>
			<div style="float: right;">
				<table id="dgEventoChq" title="<%=colaborador.getNumeroEmpresa() + "/" + colaborador.getNumeroCadastro() + " - " +  colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:435;height:auto"
			           singleSelect="true">
			        <thead>
			            <tr>
			           		<th field="codEve">Cód</th>
			           		<th field="desEve" width="200">Evento</th>
			                <th field="refEve">Referência</th>
			                <th field="valPro">Provento</th>
			                <th field="valDes">Desconto</th>
			            </tr>
			        </thead>
			    </table>
			</div>
			
			<div>
				<table style="width: 43%;">
					<tbody>
						<tr>
							<td class="flabel" class="flabel">Competência:</td>
							<td class="fvalue">
								<input id="cbCompetenciasFinancas" class="easyui-combobox" data-options="
					        		   		valueField:'competencia',
					        		   		textField:'competencia',
					        		   		editable:false,
					        		   		multiple:false,
					        		   		url:'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=listaCompetenciasFinancas&numcpf=<%=colaborador.getCpf()%>',
							           		onSelect: function(record){
							           			startProgress();
									            $.ajax({
									            	method: 'post',
									                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
									                data: 'action=buscaContraCheque&numcpf=<%=colaborador.getCpf()%>&cpt='+record.competencia,
									                dataType: 'json',
									                success: function(result){
									                    stopProgress();
									                    $('#dgEventoChq').datagrid('loadData',result.eventosColaborador);
									                    $('#dgEventoChq').datagrid('load',result.eventosColaborador);
									                    $('#inNumEmp').html(result.colaborador.numeroEmpresa);
									                    $('#inApeEmp').html(' - ' + result.colaborador.nomeEmpresa);
									                    $('#inTxtBan').html(result.txtBan);
									                    $('#inTxtAge').html(result.txtAge);
									                    $('#inTxtCon').html(result.txtCon);
									                    $('#inSalBas').html('R$' + result.salBas);
									                    $('#inBasFgt').html('R$' + result.basFgt);
									                    $('#inSalIns').html('R$' + result.salIns);
									                    $('#inBasIrf').html('R$' + result.basIrf);
									                    $('#inFgtMes').html('R$' + result.fgtMes);
									                    $('#inTotVct').html('R$' + result.totVct);
									                    $('#inTotDsc').html('R$' + result.totDsc);
									                    $('#inVlrLiq').html('R$' + result.vlrLiq);
									                }
									            });
									        }">
							</td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Empresa:</td>
							<td class="fvalue"><span id="inNumEmp"></span><span id="inApeEmp"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Banco:</td>
							<td class="fvalue"><span id="inTxtBan"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Agencia:</td>
							<td class="fvalue"><span id="inTxtAge"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Conta:</td>
							<td class="fvalue"><span id="inTxtCon"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Salário Base:</td>
							<td class="fvalue"><span id="inSalBas"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Base Cálculo FGTS:</td>
							<td class="fvalue"><span id="inBasFgt"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Base Cálculo INSS:</td>
							<td class="fvalue"><span id="inSalIns"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Base Cálculo IRRF:</td>
							<td class="fvalue"><span id="inBasIrf"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">FGTS Mês:</td>
							<td class="fvalue"><span id="inFgtMes"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Total Proventos:</td>
							<td class="fvalue"><span id="inTotVct"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Total Descontos:</td>
							<td class="fvalue"><span id="inTotDsc"></span></td>
						</td>
						<tr>
							<td class="flabel" class="flabel">Valor Líquido:</td>
							<td class="fvalue"><span id="inVlrLiq"></span></td>
						</td>
					</tbody>
				</table>			
			</div>
		<% }%>		
		</div>
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_aleTransporte.png' style='width:32px; height:32px;' /><br>Vale Transporte</span>" style="padding:10px">
			
			<table id="dgValeTransporte" title="Vale Transposrtes <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="linha" width="80">Linha</th>
		           		<th field="quantidade" width="80">Quantidade</th>
		                <th field="data" width="200">Data</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_cargos.png'/><br>Cargos</span>" style="padding:10px">
		
			<table id="dgHistoricoCargo" title="CARGOS DE <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
				   rownumbers="true" singleSelect="true" nowrap="false">
				<thead>
					<tr>
						<th field="datalt" width="80">Data</th>
						<th field="titcar" width="700">Cargo</th>
					</tr>
				</thead>
			</table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/voice_mail.png' /><br>Correio de Voz</span>" style="padding:10px">
			<%
			if(isURA){
			%>
			<table id="dgMensagensURA" title="Mensagens da URA para o colaborador: <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		           		<th field="strDataCadastro" width="80">Cadastro</th>
		           		<th field="tipoMensagem" width="100">Tipo de mensagem</th>
		                <th field="htmlLeituras" width="600">Execuções das mensagem</th>
						<th field="quantidadeExecutada" width="600">Total de Execuções</th>
		                <!-- <th field="excluir" width="40">Excluir</th> -->
		            </tr>
		        </thead>
		    </table>
			<%} %>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_checklist.png'/><br>Fichas E.P.I.</span>" style="padding:10px">
			
			<table id="dgEPI" title="Fichas de EPI colaborador: <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		            	<th field="codProduto" width="130">Código</th>
		           		<th field="descProduto" width="360">Item</th>
		           		<th field="derivacaoProduto" width="60">Derivação</th>
		           		<th field="dataEntrega" width="80">Data Entrega</th>
		           		<th field="quantidadeEntregue" width="90">Qtde. Entregue</th>
		           		<th field="dataDevolvido" width="90">Data Devolução</th>
		           		<th field="quantidadeDevolvida" width="110">Qtde. Devolvida</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		<div title="<span class='tt-inner'><img src='imagens/custom/ocioso.png'/><br>Ociosidade RT</span>" style="padding:10px">
			<table id="dgOcioso" title="Registro de ociosidade do colaborador: <%=colaborador.getNomeColaborador() %>" class="easyui-datagrid" style="width:auto;height:auto;"
		           rownumbers="true" singleSelect="true" nowrap="false">
		        <thead>
		            <tr>
		            	<th field="tarefa" width="80">Tarefa</th>
		           		<th field="data" width="80">Data</th>
		           		<th field="link" width="60">Exibir</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		<%if(supervisor != null){ %>
		<div title="<span class='tt-inner'><img src='imagens/custom/ficha_dados_gerais.png'/><br>Supervisor</span>" style="padding:10px; width: auto">
			<div style="border: 2px solid #95b8e7; width: auto; height: auto">    
				<div style="float: right;">
					<img src="<%=caminhoFotoSupervisor%>" width="140px" height="180px" style="border-left: 2px solid #95b8e7;border-bottom: 2px solid #95b8e7;"></img>
				</div>    
				<table style="width: 75%;">
					<tbody>
						<tr>
							<td class="flabel" class="flabel">Nome:</td>
							<td class="fvalue"><%=supervisor.getNomeColaborador()%></td>
						</tr>
						<tr>
							<td class="flabel">Empresa/Matrícula:</td>
							<td class="fvalue"><%=supervisor.getNumeroEmpresa() + "/" + supervisor.getNumeroCadastro()%></td>
						</tr>
						<tr>
							<td class="flabel">Sexo:</td>
							<td class="fvalue"><%=sexoS%></td>
						</tr>
						<tr>
							<td class="flabel">Cargo:</td>
							<td class="fvalue"><%=supervisor.getCargo()%></td>
						</tr>
						<tr>
							<td class="flabel">Aso:</td>
							<td class="fvalue"><%="Ultimo Exame: " + supervisor.getUltExm() + ("31/12/1900".equals(supervisor.getProExm()) ? "" : ("  Próximo Exame: " + supervisor.getProExm()) ) %></td>
						</tr>
						<tr>
							<td class="flabel">Reciclagem:</td>
							<td class="fvalue"><%= "31/12/1900".equals(supervisor.getUsuDatfor()) ? "" : NeoUtils.safeOutputString(supervisor.getUsuDatfor()) %></td>
						</tr>
					<% if(!isDiretor || isQlDiretoria) { %>
							<tr>
								<td class="flabel">CPF:</td>
								<td class="fvalue"><%=cpfS%></td>
							</tr>
							<tr>
								<td class="flabel">Nascimento:</td>
								<td class="fvalue"><%=NeoUtils.safeDateFormat(supervisor.getDataNascimento(),"dd/MM/yyyy")%></td>
							</tr>
							<tr>
								<td class="flabel">Admissão:</td>
								<td class="fvalue"><%=NeoUtils.safeDateFormat(supervisor.getDataAdmissao(),"dd/MM/yyyy")%></td>
							</tr>
							<tr>
								<td class="flabel">Avos de Férias:</td>
								<% if(supervisor.getAvosFerias() < 0) { %>
									<td class="fvalue">0</td>
								<%}else{%>
									<td class="fvalue"><%=supervisor.getAvosFerias()%></td>
								<%}%>
							</tr>
							<tr>
								<td class="flabel">Escala:</td>
								<td class="fvalue"><%=supervisor.getEscala().getDescricao()%></td>
							</tr>
							<tr>
								<td class="flabel">Situação:</td>
								<td class="fvalue"><%=supervisor.getSituacao()%></td>
							</tr>
							<% if(isFichaDepartamento) { %>
								<tr>
									<td class="flabel">Departamento:</td>
									<td class="fvalue"><%=supervisor.getDepartamento()%></td>
								</tr>
							<% } %>	
						<% if(supervisor.getMatrizResponsabilidade() != null && !supervisor.getMatrizResponsabilidade().equals("") &&
							  !supervisor.getMatrizResponsabilidade().equals(" ")) { %>	
							<tr>
								<td class="flabel">Responsabilidade:</td>
								<td class="fvalue"><%=supervisor.getMatrizResponsabilidade()%></td>
							</tr>
						<% } %>	
						
						<% if(supervisor.getFiscal() != null && !supervisor.getFiscal().equals("") &&
							  !supervisor.getFiscal().equals(" ")) { %>
							<tr>
								<td class="flabel">Responsável:</td>
								<td class="fvalue"><%=supervisor.getFiscal()%></td>
							</tr>
						<% } %>		
						
						<% if(supervisor.getEndereco() != null && !supervisor.getEndereco().equals("")) { %>	
							<tr>
								<td class="flabel">País:</td>
								<td class="fvalue"><%=supervisor.getEndereco().getPais()%></td>
							</tr>
							<tr>
								<td class="flabel">Estado:</td>
								<td class="fvalue"><%=supervisor.getEndereco().getUf()%></td>
							</tr>
							<tr>
								<td class="flabel">Endereço:</td>
								<td class="fvalue"><%=supervisor.getEndereco()%></td>
							</tr>
							<tr>
								<td class="flabel">Cidade:</td>
								<td class="fvalue"><%=supervisor.getEndereco().getCidade()%></td>
							</tr>
							<tr>
								<td class="flabel">Bairro:</td>
								<td class="fvalue"><%=supervisor.getEndereco().getBairro()%></td>
							</tr>
							<tr>
								<td class="flabel">CEP:</td>
								<td class="fvalue"><%=supervisor.getEndereco().getCep()%></td>
							</tr>
							
						<% } %>	
							
							<tr>
								<td class="flabel">Telefone(s):</td>
								<%
								if(!supervisor.getTelefone().equals(" ") && (supervisor.getTelefone2() != null && !supervisor.getTelefone2().equals(" ")))
								{
								%>
									<td class="fvalue"><%=supervisor.getTelefone()+" / "+supervisor.getTelefone2()%></td>
								<%
								}else{
								%>	
									<td class="fvalue"><%=supervisor.getTelefone()%></td>
								<%}%>
							</tr>
							
						<% if(supervisor.getEnderecoPosto() != null && !supervisor.getEnderecoPosto().equals("")) { %>	
							<tr>
								<td class="flabel">Endereço Posto:</td>
								<td class="fvalue"><%= supervisor.getEnderecoPosto() %> </td>
							</tr>
							<tr>
								<td class="flabel"></td>
								<td class="fvalue"><%=supervisor.getEnderecoPosto().getBairro().trim().equals("")? "":supervisor.getEnderecoPosto().getBairro()%> 
									<%=supervisor.getEnderecoPosto().getCidade()%> 
									- <%=supervisor.getEnderecoPosto().getUf()%> 
									- <%=supervisor.getEnderecoPosto().getPais()%>
								</td>
							</tr>
							
							
						<% } %>	
					<% }%>	
					</tbody>
				</table>
			</div>	
		</div>
		<%}%>
			
		
		
		
		<!--
		<div title="Situação" style="padding:10px">
			<table id="dgSituacoesTotais" title="SITUAÇÕES" style="width:auto;height:auto" nowrap="false" class="easyui-datagrid" style="width:auto;height:412px"
		           url="servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=listaSituacaoCompetencia&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>"
		           rownumbers="true" singleSelect="true">
		        <thead>
		            <tr>
		            	<th field="codsit" width="100">Situação</th>
		           		<th field="dessit" width="400">Descrição</th>
		           		<th field="tothor" width="100">Quantidade</th>
		            </tr>
		        </thead>
		    </table>
		    <br/>
		    <div align="center" style="width:100%">
		    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="loadPieChart('<%= colaborador.getNumeroEmpresa()%>','<%= colaborador.getTipoColaborador()%>','<%= colaborador.getNumeroCadastro()%>')">Visualizar Gráfico</a>
		    </div>
		</div>  
		-->
		
	<% }%>
    </div>
    
    <script type="text/javascript">
        var url;
        var indexApuracao;
        var rowApuracao;
        
        var x8Mes = 1;
        var x8Evt = 0;
        
        $('#dgApuracoes').datagrid({
        	onDblClickRow: function acerto(){
        		var row = $('#dgApuracoes').datagrid('getSelected');
                if (row){
                	$('#dlgAcerto').dialog('open');
                	$('#dgAcessoApuracao').datagrid('loadData',row.acessos);
                	$('#dgExcecaoApuracao').datagrid('loadData',row.situacoes);
                	$('#txtEscala').val(row.escala);
                	$('#txtHorario').val(row.horario);
                	rowApuracao = row;
                }
            }
        });
        
        function acerto(){
            var row = $('#dgApuracoes').datagrid('getSelected');
            if (row){
            	$('#dlgAcerto').dialog('open');
            	$('#fmAcerto').form('load',row);
            	$('#dgAcessoApuracao').datagrid('loadData',row.acessos);
            	$('#dgExcecaoApuracao').datagrid('loadData',row.situacoes);
            	$('#datapu').val(row.datapu);
            }
        }
        
        var editIndex = undefined;
        
        function endEditing(){
            if (editIndex == undefined){return true}
            if ($('#dgAcessoApuracao').datagrid('validateRow', editIndex)){
                editIndex = undefined;
                return true;
            } else {
                return false;
            }
        }
        
        function onClickRow(index){
            if (editIndex != index){
                if (endEditing()){
                   // $('#dgAcessoApuracao').datagrid('selectRow', index).datagrid('beginEdit', index);
                    editIndex = index;
                } else {
                    //$('#dgAcessoApuracao').datagrid('selectRow', editIndex);
                }
            }
        }
        
        function novoAcesso(){
            if (endEditing()){
                $('#dgAcessoApuracao').datagrid('appendRow',{operacao:'I',dataAcesso:rowApuracao.dataApuracao,horaAcesso:'00:00'});
                editIndex = $('#dgAcessoApuracao').datagrid('getRows').length-1;
                $('#dgAcessoApuracao').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
            }
        }
        
        function excluiAcesso(){
        	if (endEditing()){
        		var row = $('#dgAcessoApuracao').datagrid('getSelected');
        		var rowIndex = $("#dgAcessoApuracao").datagrid("getRowIndex", row);
        		$('#dgAcessoApuracao').datagrid('updateRow',{index: rowIndex,row: {operacao: 'E'}});
        		$('#dgAcessoApuracao').datagrid('selectRow', rowIndex).datagrid('beginEdit', rowIndex);
        	} 
        }
        
        function recalculo(){
        	startProgress();
        	var cptVar = $('#cbCompetenciaApu').combobox('getValue');
        	
        	$.ajax({
            	method: 'post',
                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                data: 'action=recalcularApuracao&cpt='+ cptVar +'&numemp=' + <%=colaborador.getNumeroEmpresa()%> + '&numcad=' + <%=colaborador.getNumeroCadastro()%>,
                dataType: 'json',
                success: function(result){
                	
                	var value = result;
                	var row = undefined;
                	var count = 0;
                	
                	for (var i=0;i<value.length;i++)
                	{ 
                		row = value[i];
                		count = count + row.qdeExcecao;
                	}
                	
                	var id = "#txt-" + row.colaborador.numeroEmpresa + "-" + row.colaborador.numeroCadastro;
                	
                    $('#dgApuracoes').datagrid('loadData',value);
                    $('#dgApuracoes').datagrid('load',value);
                    
                    if (count == 0)
                    {
                    	$(id).html(null);
                    }
                    else
                    {
                    	$(id).html(count);
                    }
                    stopProgress();
                }
            });
        }
        
        function salvaAcesso(){
            if (endEditing()){
            	startProgress();
            	$('#dgAcessoApuracao').datagrid('acceptChanges');
            	var jsonAcesso = JSON.stringify($('#dgAcessoApuracao').datagrid('getData'));
            	
            	$.ajax({
                	method: 'post',
                    url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                    data: 'action=salvarAcerto&jsonAcesso='+jsonAcesso+'&datapu='+rowApuracao.dataApuracao+'&numemp='+rowApuracao.colaborador.numeroEmpresa+'&tipcol='+rowApuracao.colaborador.tipoColaborador+'&numcad='+rowApuracao.colaborador.numeroCadastro,
                    dataType: 'json',
                    success: function(row){
                    	var id = "#txt-" + row.colaborador.numeroEmpresa + "-" + row.colaborador.numeroCadastro;
	                	var count = row.colaborador.qdeExcecao;
	                    $('#dgAcessoApuracao').datagrid('loadData',row.acessos);
	                    $('#dgAcessoApuracao').datagrid('load',row.acessos);
	                    $('#dgAcessoApuracao').datagrid('reload'); 
	                    
	                    $('#dgExcecaoApuracao').datagrid('loadData',row.situacoes);
	                    $('#dgExcecaoApuracao').datagrid('load',row.situacoes);
	                    $('#dgExcecaoApuracao').datagrid('reload'); 
	                   
	                    $('#dgApuracoes').datagrid('reload'); 
	                    
	                    if (count == 0)
	                    {
	                    	$(id).html(null);
	                    }
	                    else
	                    {
	                    	$(id).html(count);
	                    }
	                    stopProgress();
                    }
            	});
            }
        }
        
        function cancelaAcesso(){
            $('#dgAcessoApuracao').datagrid('rejectChanges');
            editIndex = undefined;
        }
        
        function moverMarcacao(op){
        	if (endEditing()){
	        	$.messager.confirm('Confirmar', 'Você deseja mover esta marcação para o dia ' + op +'?', function(resp){
	                if (resp){
	                	startProgress();
	                	var rowAcesso = $('#dgAcessoApuracao').datagrid('getSelected');
	            		var rowIndex = $("#dgAcessoApuracao").datagrid("getRowIndex", rowAcesso);
	                	$.ajax({
	                    	method: 'post',
	                        url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
	                        data: 'action=moverMarcacao&datapu='+rowApuracao.dataApuracao+'&datacc=' + rowAcesso.dataAcesso +'&horacc=' + rowAcesso.horaAcesso +'&seqacc=' + rowAcesso.sequencia +'&numemp=' + <%=colaborador.getNumeroEmpresa()%> + '&tipcol=' + <%=colaborador.getTipoColaborador()%> + '&numcad=' + <%=colaborador.getNumeroCadastro()%> + '&op=' + op,
	                        dataType: 'json',
	                        success: function(result){
	                        	
	                        	var value = result;
	                        	var row = undefined;
	                        	var count = 0;
	                        	
	                        	for (var i=0;i<value.length;i++)
	                        	{ 
	                        		row = value[i];
	                        		count = count + row.qdeExcecao;
	                        	}
	                        	
	                        	var id = "#txt-" + row.colaborador.numeroEmpresa + "-" + row.colaborador.numeroCadastro;
	                        	
	                            $('#dgApuracoes').datagrid('loadData',value);
	                            $('#dgApuracoes').datagrid('load',value);
	                            $('#dgAcessoApuracao').datagrid('cancelEdit', editIndex).datagrid('deleteRow', rowIndex);
	                            
	                            if (count == 0)
	                            {
	                            	$(id).html('');
	                            }
	                            else
	                            {
	                            	$(id).html(count);
	                            }
	                            stopProgress();
	                        }
	                    });
	                }
	        	});    
        	}	
        }
        
        function startProgress(){
        	$('#dlgLoader').window('open');
        }
        
        function stopProgress(){
        	$('#dlgLoader').window('close');
        }
        
        function situacao(){
        	var cptVar = $('#cbCompetenciaApu').combobox('getValue');
        	loadPieChart(<%=colaborador.getNumeroEmpresa()%>,<%=colaborador.getTipoColaborador()%>,<%=colaborador.getNumeroCadastro()%>,cptVar);
        }
        
        function insereHE(numemp, tipcol, numcad, data){
        	$.messager.confirm('Confirmar', 'Você confirma a inclusão de hora extra para este colaborador?', function(resp){
                if (resp){
                	startProgress();
                	
                	$.ajax({
                    	method: 'post',
                        url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                        data: 'action=insereHE&numemp='+ numemp +'&tipcol=' + tipcol + '&numcad=' + numcad + '&data=' + data,
                        dataType: 'json',
                        success: function(row){
                            
                        	var id = "#txt-" + row.colaborador.numeroEmpresa + "-" + row.colaborador.numeroCadastro;
                        	var count = row.colaborador.qdeExcecao;
                            
                            $('#dgExcecaoApuracao').datagrid('loadData',row.situacoes);
                            $('#dgExcecaoApuracao').datagrid('load',row.situacoes);
                            $('#dgExcecaoApuracao').datagrid('reload'); 
                           
                            $('#dgApuracoes').datagrid('reload'); 
                            
                            if (count == 0)
                            {
                            	$(id).html(null);
                            }
                            else
                            {
                            	$(id).html(count);
                            }
                        	
                        	$.messager.show({
                                title:'Mensagem',
                                msg:'Dados inseridos com sucesso.',
                                timeout:5000,
                                showType:'slide'
                            });
                            stopProgress();
                        }
                    });
                }
        	});
        }
        
		function removeHE(numemp, tipcol, numcad, data){
			$.messager.confirm('Confirmar', 'Você confirma a exclusão de hora extra para este colaborador?', function(resp){
                if (resp){
                	startProgress();
                	
                	$.ajax({
                    	method: 'post',
                        url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                        data: 'action=removeHE&numemp='+ numemp +'&tipcol=' + tipcol + '&numcad=' + numcad + '&data=' + data,
                        dataType: 'json',
                        success: function(row){
                            
                        	var id = "#txt-" + row.colaborador.numeroEmpresa + "-" + row.colaborador.numeroCadastro;
                        	var count = row.colaborador.qdeExcecao;
                            
                            $('#dgExcecaoApuracao').datagrid('loadData',row.situacoes);
                            $('#dgExcecaoApuracao').datagrid('load',row.situacoes);
                            $('#dgExcecaoApuracao').datagrid('reload'); 
                           
                            $('#dgApuracoes').datagrid('reload'); 
                            
                            if (count == 0)
                            {
                            	$(id).html(null);
                            }
                            else
                            {
                            	$(id).html(count);
                            }
                        	
                        	$.messager.show({
                                title:'Mensagem',
                                msg:'Dados excluidos com sucesso.',
                                timeout:5000,
                                showType:'slide'
                            });
                            stopProgress();
                        }
                    });
                }
        	});
        }
		
		function validaExcecao(numemp, tipcol, numcad, codsit, data){
			
			$.messager.confirm('Confirmar', 'Você confirma a validação de Exceção para este colaborador?', function(resp){
                if (resp){
                	startProgress();
                	
                	$.ajax({
                    	method: 'post',
                        url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                        data: 'action=validaExcecao&op=S&numemp='+ numemp +'&tipcol=' + tipcol + '&numcad=' + numcad + '&codsit=' + codsit + '&data=' + data,
                        dataType: 'json',
                        success: function(result){
                        	var value = result;
                        	var rowAux = undefined;
                        	var count = 0;
                        	
                        	for (var i=0;i<value.length;i++)
                        	{ 
                        		rowAux = value[i];
                        		count = count + rowAux.qdeExcecao;
                        		
                        		if (rowAux.dataApuracao == data)
                        		{
                        			row = rowAux;
                        		}
                        	}
                        	
                        	var id = "#txt-" + row.colaborador.numeroEmpresa + "-" + row.colaborador.numeroCadastro;
                        	
                            $('#dgApuracoes').datagrid('loadData',value);
                            $('#dgApuracoes').datagrid('load',value);
                            
                            $('#dgExcecaoApuracao').datagrid('loadData',row.situacoes);
                            $('#dgExcecaoApuracao').datagrid('load',row.situacoes);
                            $('#dgExcecaoApuracao').datagrid('reload');
                            
                            if (count == 0)
                            {
                            	$(id).html('');
                            }
                            else
                            {
                            	$(id).html(count);
                            }
                        	$.messager.show({
                                title:'Mensagem',
                                msg:'Exceção validada com sucesso.',
                                timeout:5000,
                                showType:'slide'
                            });
                            stopProgress();
                        }
                    });
                }
        	});
        }
		
		function retornaExcecao(numemp, tipcol, numcad, codsit, data){
			
			$.messager.confirm('Confirmar', 'Você confirma o retorno de exceção para este colaborador?', function(resp){
                if (resp){
                	startProgress();
                	
                	$.ajax({
                    	method: 'post',
                        url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                        data: 'action=validaExcecao&op=N&numemp='+ numemp +'&tipcol=' + tipcol + '&numcad=' + numcad + '&codsit=' + codsit + '&data=' + data,
                        dataType: 'json',
                        success: function(result){
                            
                        	var value = result;
                        	var rowAux = undefined;
                        	var count = 0;
                        	
                        	for (var i=0;i<value.length;i++)
                        	{ 
                        		rowAux = value[i];
                        		count = count + rowAux.qdeExcecao;
                        		
                        		if (rowAux.dataApuracao == data)
                        		{
                        			row = rowAux;
                        		}
                        	}
                        	
                        	var id = "#txt-" + row.colaborador.numeroEmpresa + "-" + row.colaborador.numeroCadastro;
                        	
                            $('#dgApuracoes').datagrid('loadData',value);
                            $('#dgApuracoes').datagrid('load',value);
                            
                            $('#dgExcecaoApuracao').datagrid('loadData',row.situacoes);
                            $('#dgExcecaoApuracao').datagrid('load',row.situacoes);
                            $('#dgExcecaoApuracao').datagrid('reload');
                            
                            if (count == 0)
                            {
                            	$(id).html('');
                            }
                            else
                            {
                            	$(id).html(count);
                            }
                        	$.messager.show({
                                title:'Mensagem',
                                msg:'Exceção retornada com sucesso.',
                                timeout:5000,
                                showType:'slide'
                            });
                            stopProgress();
                        }
                    });
                }
        	});
        }
        
		function valida_horas(edit){
			if(event.keyCode<48 || event.keyCode>57){
		        event.returnValue=false;
		    }
		    if(edit.value.length==2 || edit.value.length==5){
		        edit.value+=":";
		    }
		}      
    
		$('#tab').tabs({
			onSelect : function(title, index) {
				var id = "";
				var data = "";
				switch (index) {
				case 1:
					id = "#dgObservacoes";
					data = "action=listaObsHorista&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>&numcpf=<%=colaborador.getCpf()%>";
					break;
				case 2:
					id = "#dgAfastamento";
					data = "action=listaAfastamentos&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>";
					break;
					
				case 3:
					id = "#dgAnotacao";
					data = "action=listaAnotacoes&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>";
					break;
					
				case 4:
					id = "#dgCobertura";
					data = "action=listaCoberturas&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>";
					break;	
					
				case 5:
					$('#dgDocumento').treegrid({
						url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=listaDocumentos&cpf=<%=colaborador.getCpf()%>',
					    idField:'id',
					    treeField:'nome',
					    columns:[[
					        {title:'Nome',field:'nome',width:400},
					        {title:'Competência',field:'data',width:80},
					        {title:'Abrir',field:'link',width:30},
					    ]]
					});
					break;	
					
				case 6:
					id = "#dgHistoricoLocal";
					data = "action=listaHistoricoLocal&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>";
					break;	
					
				case 7:
					id = "#dgX8";
					data = "action=listaX8Colaborador&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>&numcpf=<%=colaborador.getCpf()%>";
					break;	
					
				case 8:
					id = "#dgHistoricoEscala";
					data = "action=listaHistoricoEscala&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>";
					break;	
					
				case 9:
					var cptVar = $('#cbCompetenciaApu').combobox('getValue');
					id = "#dgApuracoes";
					data = "action=listaApuracoes&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>"+ "&cpt=" + cptVar;
					break;
					
				case 10:
					var cptVar = $('#cbCompetencia').combobox('getValue');
					id = "#dgAcesso";
					data = "action=listaAcessos&numcpf=<%=colaborador.getCpf()%>"+ "&cpt=" + cptVar;
					
					if (document.getElementById('cbExcecao').checked) {
			            data = data + "&exibirExcecao=Sim";
			        } else {
			        	data = data + "&exibirExcecao=Nao";
			        }
					
					break;
					
				case 11:
					var cptVar = $('#cbCompetenciasFinancas').combobox('getValue');
					
					data = "action=buscaContraCheque&numcpf=<%=colaborador.getCpf()%>"+ "&cpt=" + cptVar;
					startProgress();
					
					$.ajax({
		            	method: 'post',
		                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
		                data: data,
		                dataType: 'json',
		                success: function(result){
		                	stopProgress();
		                	$('#dgEventoChq').datagrid('loadData',result.eventosColaborador);
		                    $('#dgEventoChq').datagrid('load',result.eventosColaborador);
		                    $('#inNumEmp').html(result.colaborador.numeroEmpresa);
		                    $('#inApeEmp').html(' - ' + result.colaborador.nomeEmpresa);
		                    $('#inTxtBan').html(result.txtBan);
		                    $('#inTxtAge').html(result.txtAge);
		                    $('#inTxtCon').html(result.txtCon);
		                    $('#inSalBas').html('R$' + result.salBas);
		                    $('#inBasFgt').html('R$' + result.basFgt);
		                    $('#inSalIns').html('R$' + result.salIns);
		                    $('#inBasIrf').html('R$' + result.basIrf);
		                    $('#inFgtMes').html('R$' + result.fgtMes);
		                    $('#inTotVct').html('R$' + result.totVct);
		                    $('#inTotDsc').html('R$' + result.totDsc);
		                    $('#inVlrLiq').html('R$' + result.vlrLiq);
		                }
		            });
					break;
				
				case 12:
					id = "#dgValeTransporte";
					data = "action=listaValeTransporte&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>";
					break;
				case 13:
					id = "#dgHistoricoCargo";
					data = "action=listaHistoricoCargo&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>";
					
					break;
				case 14:
					id="#dgMensagensURA";
					data = "action=listaMensagensURA&numcpf=<%=colaborador.getCpf()%>";
					break;
				case 15:
					id="#dgEPI";
					data = "action=listaFichaEpi&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>&numcpf=<%=colaborador.getCpf()%>";
					break;
				case 16:
					id = "#dgOcioso";
					data = "action=listaOciosidade&numemp=<%=colaborador.getNumeroEmpresa()%>&tipcol=<%=colaborador.getTipoColaborador()%>&numcad=<%=colaborador.getNumeroCadastro()%>";
					break;
				}
				
				if (index != 5 && index != 11)
				{
					startProgress();
					$.ajax({
		            	method: 'post',
		                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
		                data: data,
		                dataType: 'json',
		                success: function(result){
		                	stopProgress();
		                	if (index == 7){
		                		//alert(result);
		                    	if (result != '' && result.erro !== undefined ){
				                	alert(result.erro);
				                }else{
					                $(id).datagrid('loadData',result);
					                $(id).datagrid('load',result);
				                	
				                }
				                
		                    }else{
			                    $(id).datagrid('loadData',result);
			                    $(id).datagrid('load',result);
		                    }
		                    
		                }
		            });
				}
			}
		});
		
		function showTarefa(neoid) {
	    	var url = '<%=PortalUtil.getBaseURL()%>bpm/workflow_search.jsp?id='+neoid;
	    	var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,'');
	    	var win = window.name; 
	    	
	        NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win);
	    }
		
		function exibirExcecao() {
			var cptVar = $('#cbCompetencia').combobox('getValue');
			var data = "action=listaAcessos&numcpf=<%=colaborador.getCpf()%>"+ "&cpt=" + cptVar;
			
	        if (document.getElementById('cbExcecao').checked) {
	            data = data + "&exibirExcecao=Sim";
	        } else {
	        	data = data + "&exibirExcecao=Nao";
	        }
	        
	        startProgress();
			$.ajax({
            	method: 'post',
                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                data: data,
                dataType: 'json',
                success: function(result){
                	stopProgress();
                	$('#dgAcesso').datagrid('loadData',result);
                    $('#dgAcesso').datagrid('load',result);
                }
            });
	    }
		
		function deleteAfastamento(numemp, tipcol, numcad, numcpf, datafa) {
			$.messager.confirm('Confirmar', 'Você confirma a exclusão do afastamento?', function(resp){
            	if (resp){
           		startProgress();
					$.ajax({
		            	method: 'post',
		                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
		                data: 'action=deleteAfastamento&numemp='+numemp+'&tipcol='+tipcol+'&numcad='+numcad+'&numcpf='+numcpf+'&datafa='+datafa,
		                dataType: 'json',
		                success: function(result){
	                		$('#dgAfastamento').datagrid('loadData',result);
                         	$('#dgAfastamento').datagrid('load',result);
                         	$('#dgAfastamento').datagrid('reload');
                         
                         	stopProgress();
                         	
                         	$.messager.show({
                               	title:'Mensagem',
                               	msg:'Afastamento excluído com sucesso!.',
                               	timeout:5000,
                               	showType:'slide'
                        	});
		                },
		                error: function(result){
		                	stopProgress();
                         	$.messager.show({
                               	title:'Mensagem',
                               	msg:'Erro ao excluir afastamento!.',
                               	timeout:5000,
                               	showType:'slide'
                        	});
		                }
		            });
                }
			});  
	    }
		
		function deleteAcesso(numcra,numcpf,datacc,seqacc) {
			$.messager.confirm('Confirmar', 'Você confirma a exclusão do acesso?', function(resp){
            	if (resp){
					startProgress();
            		var data = 'action=deleteAcesso&numcra='+numcra+'&numcpf='+numcpf+'&datacc='+datacc+'&seqacc='+seqacc;
            		if (document.getElementById('cbExcecao').checked) {
			            data = data + '&exibirExcecao=Sim';
			        } else {
			        	data = data + '&exibirExcecao=Nao';
			        }
					$.ajax({
		            	method: 'post',
		                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
		                data: data,
		                dataType: 'json',
		                success: function(result){
	                		$('#dgAcesso').datagrid('loadData',result);
                         	$('#dgAcesso').datagrid('load',result);
                         	$('#dgAcesso').datagrid('reload');
                         
                         	stopProgress();
                         	
                         	$.messager.show({
                               	title:'Mensagem',
                               	msg:'Acesso excluído com sucesso!.',
                               	timeout:5000,
                               	showType:'slide'
                        	});
		                },
		                error: function(result){
		                	stopProgress();
                         	$.messager.show({
                               	title:'Mensagem',
                               	msg:'Erro ao excluir acesso!.',
                               	timeout:5000,
                               	showType:'slide'
                        	});
		                }
		            });
                }
			});  
	    }
		
		function deleteCobertura(numEmp, tipCol, numCad, dataInicial, horaInicial, numEmpCob, tipColCob, numCadCob, numCpf, descCobertura, dataCoberturaFinal, dataCoberturaInicial){
			$.messager.confirm('Confirmar', 'Você confirma a exclusão da cobertura?', function(resp){
            	if (resp){
            		startProgress();
            		var data = 'action=deleteCobertura&numEmp='+numEmp+'&tipCol='+tipCol+'&numCad='+numCad+'&dataInicial='+dataInicial+'&horaInicial='+horaInicial+'&numEmpCob='+numEmpCob+'&tipColCob='+tipColCob+'&numCadCob='+numCadCob+'&numCpf='+numCpf+'&descCob='+descCobertura+'&dataCobFim='+dataCoberturaFinal+'&dataCobInicio='+dataCoberturaInicial;
					$.ajax({
		            	method: 'post',
		                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
		                data: data,
		                dataType: 'json',
		                success: function(result){
	                		$('#dgCobertura').datagrid('loadData',result);
                         	$('#dgCobertura').datagrid('load',result);
                         	$('#dgCobertura').datagrid('reload');
                         
                         	stopProgress();
                         	
                         	$.messager.show({
                               	title:'Mensagem',
                               	msg:'Cobertura excluída com sucesso!',
                               	timeout:5000,
                               	showType:'slide'
                        	});
		                },
		                error: function(result){
		                	stopProgress();
                         	$.messager.show({
                               	title:'Mensagem',
                               	msg:'Erro ao excluir cobertura!',
                               	timeout:5000,
                               	showType:'slide'
                        	});
		                }
		            });
                }
			});  
	    }
		
		function deleteHistoricoEscala(numemp,tipcol,numcad,numcpf,datalt) {
			$.messager.confirm('Confirmar', 'Você confirma a exclusão do histórico de escala?', function(resp){
            	if (resp){
            		startProgress();
            		var data = 'action=deleteHistoricoEscala&numemp='+numemp+'&tipcol='+tipcol+'&numcad='+numcad+'&numcpf='+numcpf+'&datalt='+datalt;
					$.ajax({
		            	method: 'post',
		                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
		                data: data,
		                dataType: 'json',
		                success: function(result){
	                		$('#dgHistoricoEscala').datagrid('loadData',result);
                         	$('#dgHistoricoEscala').datagrid('load',result);
                         	$('#dgHistoricoEscala').datagrid('reload');
                         
                         	stopProgress();
                         	
                         	$.messager.show({
                               	title:'Mensagem',
                               	msg:'Histórico de escala excluído com sucesso!',
                               	timeout:5000,
                               	showType:'slide'
                        	});
		                },
		                error: function(result){
		                	stopProgress();
                         	$.messager.show({
                               	title:'Mensagem',
                               	msg:'Erro ao excluir histórico de escala!',
                               	timeout:5000,
                               	showType:'slide'
                        	});
		                }
		            });
                }
			});  
	    }
		
		function salvaObsHorista(numcad,numemp,tipcol,numcpf){
        	id = "#dgObservacoes";
			console.log('Salvando Observacao do horista');  
			var obsHorista = jQuery('#obsHorista').val();
			jQuery('#obsHorista').val('');
			var data = 'action=salvaObsHorista&numcad='+numcad+'&numemp='+numemp+'&tipcol='+tipcol+'&obsHorista='+obsHorista+'&numcpf='+numcpf;
			
				startProgress();
				$.ajax({
            		method: 'post',
                	url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                	data: data,
                	dataType: 'json',
            	    success: function(result){
        	            $(id).datagrid('loadData',result);
    	                $(id).datagrid('load',result);
    	                $('#add').dialog('close');
    	                stopProgress();
	             	},
                	error: function(result){
                		stopProgress();
                	}
            	});
			
		}
		
		function removerObsHorista(numcad,numemp,tipcol,neoId,numcpf){
        	id = "#dgObservacoes";
        	var data = 'action=removerObsHorista&numcad='+numcad+'&numemp='+numemp+'&tipcol='+tipcol+'&neoId='+neoId+'&numcpf='+numcpf;
        	startProgress();
			$.ajax({
            	method: 'post',
                url: 'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
                data: data,
                dataType: 'json',
                success: function(result){
                    $(id).datagrid('loadData',result);
                    $(id).datagrid('load',result);
                    stopProgress();
                },
                error: function(result){
                	stopProgress();
                }
            });
        }
		
		function showGridMessage(target){
			var opts = $(target).datagrid('options');
			var vc = $(target).datagrid('getPanel').children('div.datagrid-view');
			vc.children('div.datagrid-empty').remove();
			if (!$(target).datagrid('getRows').length){
			    var d = $('<div class="datagrid-empty"></div>').html('No Records Found').appendTo(vc);
			    d.css({
			        position:'absolute',
			        left:0,
			        top:50,
			        width:'100%',
			        textAlign:'center'
			    });
			}else{
			      vc.children('div.datagrid-empty').remove();
			    }
	
			}
	</script>
    <style type="text/css">
        #fmAcesso{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
        .flabel{
        	text-align: right; 
        	height:20px; 
        	font-weight: bold; 
        	background-color: #e0ecff;
        	width: 120px;
        	padding-right: 2px; 
        }
        .fvalue{
        	padding-left: 8px; 
        }
        .tt-inner{
            display:inline-block;
            line-height:12px;
            padding-top:5px;
        }
        .tt-inner img{
            border:0;
        }
    </style>
</body>
</html>


	