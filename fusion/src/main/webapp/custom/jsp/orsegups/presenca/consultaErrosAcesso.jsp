<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.AcessoVO"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
	GregorianCalendar dataAtual = new GregorianCalendar(); 
	List<AcessoVO> acessos = new ArrayList<AcessoVO>();

	String numcra = request.getParameter("numcra");
	StringBuilder textoLog = new StringBuilder();
	
	acessos = QLPresencaUtils.getErrosAcesso(numcra, dataAtual);
	
	if (acessos != null & !acessos.isEmpty())
	{
		
			textoLog.append("<table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
				textoLog.append("<tr style=\"cursor: auto\">");
					textoLog.append("<th style=\"cursor: auto\">Data</th>");
					textoLog.append("<th style=\"cursor: auto\">Dire��o</th>");
					textoLog.append("<th style=\"cursor: auto\">Tipo</th>");
					textoLog.append("<th style=\"cursor: auto\">Erro</th>");
					textoLog.append("<th style=\"cursor: auto\">Telefone</th>");
				textoLog.append("</tr>");
				textoLog.append("<tbody>");	
			
		for (AcessoVO acc : acessos)
		{
				textoLog.append("<tr>");
					textoLog.append("<td>" + NeoUtils.safeDateFormat(acc.getData()) + "</td>");
					textoLog.append("<td>" + acc.getDirecao() + "</td>");
					textoLog.append("<td>" + acc.getTipoDirecao() + "</td>");
					textoLog.append("<td>" + acc.getTipoAcesso() + "</td>");
					textoLog.append("<td>" + acc.getFone() + "</td>");
				textoLog.append("</tr>");
		}
		
				textoLog.append("</tbody>");
				textoLog.append("</table>");
		
	}
	
	if (textoLog == null || textoLog.length() == 0) {
		textoLog.append("Nenhum Registro");
	} 
%>

<%=textoLog.toString() %>
	